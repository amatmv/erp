# -*- coding: utf-8 -*-
import unittest

from destral import testing
from destral.transaction import Transaction
from expects import *
from libfacturacioatr.pool.tarifes import *
from addons import get_module_resource
import tools

from addons.giscedata_telegestio_comer.telegestio_comer import get_uri
from cchloader.file import CchFile, PackedCchFile
from cchloader.backends import get_backend
from cchloader.compress import is_compressed_file
from dateutil.parser import parse as parse_date
from shutil import copyfile

HOLIDAYS = [
    # 2017
    date(2017, 1, 1),
    date(2017, 5, 1),
    date(2017, 8, 15),
    date(2017, 10, 12),
    date(2017, 11, 1),
    date(2017, 12, 6),
    date(2017, 12, 8),
    date(2017, 12, 25),
]

TARIFFS = {
    '2.0A': (Tarifa20A, Tarifa20APool),
    '2.0DHA': (Tarifa20DHA, Tarifa20DHAPool),
    '2.1A': (Tarifa21A, Tarifa21APool),
    '2.1DHA': (Tarifa21DHA, Tarifa21DHAPool),
    '3.0A': (Tarifa30A, Tarifa30APool),
    '3.1A': (Tarifa31A, Tarifa31APool),
    '3.1A LB': (Tarifa31ALB, Tarifa31ALBPool),
    '6.1A': (Tarifa61A, Tarifa61APool)
}


class IndexadaTest(testing.OOTestCase):
    ''' Test indexed AB tariffs '''
    def crear_modcon(self, cursor, uid, polissa_id, ini, fi):
        '''Creates a modcon in contract'''
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def get_curve(self):
        curve = []

        for day_num in range(1, 32):
            day = copy.copy([])
            for hour in range(0, 24):
                day.append(round(day_num * 1.0 + (hour / 100.0), 2))
            day.append(0.0)
            curve.append(day)
        return curve

    def load_curve_file(self, file):
        uri = get_uri()
        backend = get_backend(uri)
        processed_cups = []
        with backend(uri) as bnd:
            if is_compressed_file(file.name):
                with PackedCchFile(file.name) as psf:
                    for cch_file in psf:
                        for line in cch_file:
                            if not line:
                                continue
                            bnd.insert(line)
                            cups = self.get_cups(line)
                            if cups not in processed_cups:
                                processed_cups.append(cups)
            else:
                with CchFile(file.name) as sf:
                    for line in sf:
                        if not line:
                            continue
                        bnd.insert(line)
                        cups = self.get_cups(line)
                        if cups not in processed_cups:
                            processed_cups.append(cups)
        file.close()

    def prepare_idx_measures(self, cursor, uid):
        imd_obj = self.openerp.pool.get('ir.model.data')
        measure_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        contract_obj = self.openerp.pool.get('giscedata.polissa')

        # gets lectura 0001
        lectura_anterior_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'lectura_0001'
        )[1]

        measure_obj.write(
            cursor, uid, [lectura_anterior_id],
            {'name': '2016-09-30', 'lectura': 17}
        )

        # gets lectura 0002
        lectura_actual_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'lectura_0002'
        )[1]

        measure_obj.write(
            cursor, uid, [lectura_actual_id],
            {'name': '2016-10-31', 'lectura': 47, 'ajust': 0}
        )

        # polissa 0001
        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        contract_vals = {
            'data_alta': '2016-01-01',
            'data_baixa': False,
            'potencia': 4.600,
            'data_ultima_lectura': '2016-09-30',
            'facturacio': 1,
            'facturacio_potencia': 'icp',
            'tg': '1',
            'agree_tensio': 'E0',
            'agree_dh': 'E1',
            'agree_tipus': '05',
            'agree_tarifa': '2A',
            'autoconsumo': '00',
            'contract_type': '01',
            'lot_facturacio': False,
        }
        contract_obj.write(cursor, uid, contract_id, contract_vals)

        contract_obj.send_signal(
            cursor, uid, [contract_id], ['validar', 'contracte']
        )

    def add_additional_meter(self, cursor, uid):
        imd_obj = self.openerp.pool.get('ir.model.data')
        measure_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        # polissa 0001
        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        # gets lectura 0002
        lectura_actual_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'lectura_0002'
        )[1]

        measure_obj.write(
            cursor, uid, [lectura_actual_id],
            {'name': '2016-10-14', 'lectura': 47, 'ajust': 0}
        )

        # gets current meter
        old_meter_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'comptador_0001'
        )[1]

        # gets lectures to delete
        measures_to_delete = measure_obj.search(
            cursor, uid,
            [('comptador', '=', old_meter_id), ('name', '>', '2016-10-15')]
        )
        measure_obj.unlink(cursor, uid, measures_to_delete)

        meter_obj.write(
            cursor, uid,
            old_meter_id, {'data_baixa': '2016-10-14', 'active': False}
        )

        # gets new meter
        new_meter_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'comptador_0002'
        )[1]

        # gets lectures to delete
        measures_to_delete = measure_obj.search(
            cursor, uid,
            [('comptador', '=', new_meter_id)]
        )
        measure_obj.unlink(cursor, uid, measures_to_delete)

        meter_obj.write(
            cursor, uid,
            new_meter_id,
            {
                'data_alta': '2016-10-15',
                'data_baixa': False,
                'polissa': contract_id,
                'active': True
            }
        )

        # create new meter measures
        lectura_base_vals = measure_obj.read(
            cursor, uid, lectura_actual_id, ['origen_id', 'periode']
        )
        measure_obj.create(
            cursor, uid,
            {
                'comptador': new_meter_id,
                'name': '2016-10-14',
                'lectura': 0,
                'tipus': 'A',
                'origen_id': lectura_base_vals['origen_id'][0],
                'periode': lectura_base_vals['periode'][0],
            }
        )

        measure_obj.create(
            cursor, uid,
            {
                'comptador': new_meter_id,
                'name': '2016-10-31',
                'lectura': 70,
                'tipus': 'A',
                'origen_id': lectura_base_vals['origen_id'][0],
                'periode': lectura_base_vals['periode'][0],
            }
        )

    def prepare_idx_measures_31ALB(self, cursor, uid):
        imd_obj = self.openerp.pool.get('ir.model.data')
        measure_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        potencia_obj = self.openerp.pool.get('giscedata.lectures.potencia')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')

        tarifa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_31A_LB'
        )[1]

        # gets lectura 0001
        comptador_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'comptador_0001'
        )[1]
        # gets origin 10
        origen_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]

        measure_obj.read(cursor, uid, origen_id, ['origen_id'])

        periods = tarifa_obj.get_periodes(
            cursor, uid, tarifa_id, 'te'
        )

        for period in periods.values():
            measure_obj.create(
                cursor, uid, {
                    'name': '2016-09-30',
                    'lectura': 0,
                    'periode': period,
                    'comptador': comptador_id,
                    'tipus': 'A',
                    'origen_id': origen_id,
                    'ajust': 0,
                }
            )
            measure_obj.create(
                cursor, uid, {
                    'name': '2016-10-31',
                    'lectura': 1000,
                    'periode': period,
                    'comptador': comptador_id,
                    'tipus': 'A',
                    'origen_id': origen_id,
                    'ajust': 0,
                }
            )
        periods = tarifa_obj.get_periodes(
            cursor, uid, tarifa_id, 'tp'
        )

        for period in periods.values():
            potencia_obj.create(
                cursor, uid, {
                    'name': '2016-10-31',
                    'lectura': 44.6,
                    'periode': period,
                    'comptador': comptador_id,
                    'origen_id': origen_id,
                }
            )


        # polissa 0001
        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        contract_vals = {
            'tarifa': tarifa_id,
            'data_alta': '2016-01-01',
            'data_baixa': False,
            'potencia': 44.600,
            'data_ultima_lectura': '2016-09-30',
            'facturacio': 1,
            'facturacio_potencia': 'max',
            'tg': '1',
            'autoconsumo': '00',
            'contract_type': '01',
            'lot_facturacio': False,
            'trafo': 50,
        }
        contract_obj.write(cursor, uid, contract_id, contract_vals)

        contract_obj.send_signal(
            cursor, uid, [contract_id], ['validar', 'contracte']
        )

    def set_audit_fields(self, cursor, uid):
        conf_obj = self.openerp.pool.get('res.config')
        conf_obj.set(
            cursor, uid, 'fact_indexed_audit_fields', "['curve', 'phf']"
        )

    def setUp(self):


        P5D_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'P5D_888_9999_20170302.0'
        )
        self.P5D_file = open(P5D_path, 'r')

        P5D_full_curve_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'P5D_0316_0631_20170308.0'
        )
        self.P5D_full_file = open(P5D_full_curve_path, 'r')

        P5D_incomplete_curve_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'P5D_0316_0631_20170309.0'
        )
        self.P5D_incomplete_file = open(P5D_incomplete_curve_path, 'r')

        F5D_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'F5D_0316_0631_20170308.0'
        )
        self.F5D_file = open(F5D_path, 'r')

        F5D_incomplete_curve_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'F5D_0316_0631_20170309.0'
        )
        self.F5D_incomplete_file = open(F5D_incomplete_curve_path, 'r')

        P1_curve_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'P1_0189_20161001_20161107.zip'
        )
        self.P1_curve_file = open(P1_curve_path, 'r')

        F1_curve_path = get_module_resource(
            'giscedata_facturacio_indexada', 'tests', 'data',
            'F1_0189_20161001_20161111.zip'
        )
        self.F1_curve_file = open(F1_curve_path, 'r')

    @classmethod
    def setUpClass(self):
        super(IndexadaTest, self).setUpClass()
        # Copy ESIOS coefs to cache
        copyfile(
            get_module_resource(
                'giscedata_facturacio_indexada', 'tests', 'data',
                'C5_perd20A_20170101_20170131'
            ), '/tmp/C5_perd20A_20170101_20170131'
        )

        copyfile(
            get_module_resource(
                'giscedata_facturacio_indexada', 'tests', 'data',
                'C5_prgpncur_20170101_20170131'
            ), '/tmp/C5_prgpncur_20170101_20170131'
        )

        copyfile(
            get_module_resource(
                'giscedata_facturacio_indexada', 'tests', 'data',
                'C5_prmncur_20170101_20170131'
            ), '/tmp/C5_prmncur_20170101_20170131'
        )
        copyfile(
            get_module_resource(
                'giscedata_facturacio_indexada', 'tests', 'data',
                'C5_perd20A_20161001_20161031'
            ), '/tmp/C5_perd20A_20161001_20161031'
        )

        copyfile(
            get_module_resource(
                'giscedata_facturacio_indexada', 'tests', 'data',
                'C5_prgpncur_20161001_20161031'
            ), '/tmp/C5_prgpncur_20161001_20161031'
        )

        copyfile(
            get_module_resource(
                'giscedata_facturacio_indexada', 'tests', 'data',
                'C5_prmncur_20161001_20161031'
            ), '/tmp/C5_prmncur_20161001_20161031'
        )


    def tearDown(self):
        self.P5D_file.close()
        self.P5D_full_file.close()
        self.P5D_incomplete_file.close()
        self.F5D_file.close()
        self.F5D_incomplete_file.close()
        self.P1_curve_file.close()
        self.F1_curve_file.close()

    def test_facturador_get_tarifa_class(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        tarifa_obj = self.openerp.pool.get(
            'giscedata.polissa.tarifa'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            # gets contract 0001
            contract_id_index = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract_id_atr = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            # Test all available DSO access fare
            for tariff_name in TARIFFS:

                tariff_id = tarifa_obj.search(
                    cursor, uid, [('name', '=', tariff_name)]
                )[0]

                contract_obj.write(
                    cursor, uid, [contract_id_index, contract_id_atr],
                    {'tarifa': tariff_id}
                )

                contract_index = contract_obj.browse(
                    cursor, uid, contract_id_index
                )
                contract_atr = contract_obj.browse(
                    cursor, uid, contract_id_atr
                )

                tariff_class_index = facturador_obj.get_tarifa_class(
                    contract_index
                )
                tariff_class_atr = facturador_obj.get_tarifa_class(
                    contract_atr
                )

                expect(contract_atr.mode_facturacio).to(equal('atr'))
                expect(tariff_class_atr).to(be(TARIFFS[tariff_name][0]))

                expect(contract_index.mode_facturacio).to(equal('index'))
                expect(tariff_class_index).to(be(TARIFFS[tariff_name][1]))

    def test_facturador_versions_de_preus(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            # gets contract 0001
            contract_id_index = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract_id_atr = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]

            contract_obj.send_signal(
                cursor, uid, [contract_id_index], ['validar', 'contracte']
            )
            self.crear_modcon(
                cursor, uid, contract_id_index, '2017-01-01', '2017-12-31'
            )

            contract_obj.send_signal(
                cursor, uid, [contract_id_atr], ['validar', 'contracte']
            )
            self.crear_modcon(
                cursor, uid, contract_id_atr, '2017-01-01', '2017-12-31'
            )

            context = {'llista_preu': pricelist_id}
            preus = facturador_obj.versions_de_preus(
                cursor, uid, contract_id_index, '2017-01-01', '2017-01-31',
                context
            )

            for version, productes in preus.items():
                expect(productes).to(have_key('k'))
                expect(productes).to(have_key('d'))
                expect(productes).to(have_key('fe'))
                expect(productes).to(have_key('imu'))
                expect(productes).to(have_key('omie'))
                expect(productes).to(have_key('pc'))
                expect(productes).to(have_key('fe'))
                expect(productes).to(have_key('atr'))

            preus = facturador_obj.versions_de_preus(
                cursor, uid, contract_id_atr, '2017-01-01', '2017-01-31',
                context
            )

            for version, productes in preus.items():
                expect(productes).to(be_an((int, long)))
                expect(productes).to(equal(pricelist_id))

    def test_facturador_versions_de_preus_version_change(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        modcon_obj = self.openerp.pool.get(
            'giscedata.polissa.modcontractual'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            start_date = '2017-01-01'
            end_date = '2017-01-31'

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            # gets contract 0001
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]


            contract_obj.send_signal(
                cursor, uid, [contract_id], ['validar', 'contracte']
            )
            contract_vals = contract_obj.read(
                cursor, uid, contract_id, ['modcontractual_activa']
            )

            modcon_id = contract_vals['modcontractual_activa'][0]
            modcon_obj.renovar(cursor, uid, [modcon_id])

            contract_obj.write(cursor, uid, contract_id, {
                'coeficient_k': 7.75, 'coeficient_d': 1.75
            })
            self.crear_modcon(
                cursor, uid, contract_id, '2017-01-15', '2017-12-31'
            )

            context = {'llista_preu': pricelist_id}
            preus = facturador_obj.versions_de_preus(
                cursor, uid, contract_id, start_date, end_date,
                context
            )

            version = preus['2017-01-01']
            expect(version['fe']).to(equal(1.0))
            expect(version['k']).to(equal(0.006))
            expect(version['d']).to(equal(0.00075))

            version = preus['2017-01-15']
            expect(version['fe']).to(equal(1.0))
            expect(version['k']).to(equal(0.00775))
            expect(version['d']).to(equal(0.00175))

    def test_facturador_versions_de_preus_k_d_from_contract(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            # gets contract 0001
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]

            contract_obj.send_signal(
                cursor, uid, [contract_id], ['validar', 'contracte']
            )
            self.crear_modcon(
                cursor, uid, contract_id, '2017-01-01', '2017-12-31'
            )

            context = {'llista_preu': pricelist_id}
            preus = facturador_obj.versions_de_preus(
                cursor, uid, contract_id, '2017-01-01', '2017-01-31',
                context
            )

            for version, productes in preus.items():
                expect(productes['k']).to(equal(0.006))
                expect(productes['d']).to(equal(0.00075))

    def test_facturador_versions_de_preus_k_d_from_list_when_0(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')

            # gets contract 0001
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]

            contract_obj.write(cursor, uid, contract_id, {
                'coeficient_k': 0.0, 'coeficient_d': 0.0
            })

            contract_obj.send_signal(
                cursor, uid, [contract_id], ['validar', 'contracte']
            )
            self.crear_modcon(
                cursor, uid, contract_id, '2017-01-01', '2017-12-31'
            )

            context = {'llista_preu': pricelist_id}
            preus = facturador_obj.versions_de_preus(
                cursor, uid, contract_id, '2017-01-01', '2017-01-31',
                context
            )

            for version, productes in preus.items():
                expect(productes['k']).to(equal(1.000))
                expect(productes['d']).to(equal(1.000))

    def test_facturador_audit_data_from_config(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        tarifa_obj = self.openerp.pool.get(
            'giscedata.polissa.tarifa'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            conf_obj = self.openerp.pool.get('res.config')

            # gets contract 0001
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            tarifa_class = facturador_obj.get_tarifa_class(contract)

            curve_data = self.get_curve()

            consums = {
                'activa': {'2017-01-01': curve_data},
                'reactiva': {'P1': 2}
            }

            tarifa = tarifa_class(
                consums, {},
                '2017-01-01', '2017-01-31',
                facturacio=1, facturacio_potencia='icp',
                data_inici_periode='2017-01-01',
                data_final_periode='2017-01-31',
                potencies_contractades={'P1': 4.6},
                holidays=HOLIDAYS,
            )

            conf_key = 'fact_indexed_audit_fields'
            # bad key
            conf_obj.set(cursor, uid, conf_key, 3.3)
            facturador_obj.config_facturador(cursor, uid, tarifa, contract_id)
            expect(tarifa.conf['audit']).to(equal([]))

            # Correct list
            conf_obj.set(cursor, uid, conf_key, "['curve', 'pmd']")
            facturador_obj.config_facturador(cursor, uid, tarifa, contract_id)
            expect(tarifa.conf['audit']).to(equal(['curve', 'pmd']))

            # list with not available keys
            conf_obj.set(cursor, uid, conf_key, "['curve', 'pmd', 'no_key']")
            facturador_obj.config_facturador(cursor, uid, tarifa, contract_id)
            expect(tarifa.conf['audit']).to(equal(['curve', 'pmd']))

    def test_phf(self):
        ESIOS_TOKEN = tools.config['esios_token']
        token = ESIOS_TOKEN

        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            conf_obj = self.openerp.pool.get('res.config')
            attach_obj = self.openerp.pool.get('ir.attachment')

            # gets contract 0001
            contract_id_index = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]

            contract_obj.send_signal(
                cursor, uid, [contract_id_index], ['validar', 'contracte']
            )
            self.crear_modcon(
                cursor, uid, contract_id_index, '2017-01-01', '2017-12-31'
            )

            context = {'llista_preu': pricelist_id}
            versions = facturador_obj.versions_de_preus(
                cursor, uid, contract_id_index, '2017-01-01', '2017-01-31',
                context
            )


            curve_data = self.get_curve()
            curve = Curve(datetime(2017, 1, 1))
            curve.load(curve_data)

            consums = {
                'activa': {'2017-01-01': curve_data},
                'reactiva': {'P1': 2}
            }
            tarifa = Tarifa20APool(
                consums, {},
                '2017-01-01', '2017-01-31',
                facturacio=1, facturacio_potencia='icp',
                data_inici_periode='2017-01-01',
                data_final_periode='2017-01-31',
                potencies_contractades={'P1': 4.6},
                versions=versions,
                holidays=HOLIDAYS,
                esios_token=token
            )

            conf_obj.set(
                cursor, uid, 'fact_indexed_audit_fields', "['curve', 'pmd']"
            )
            facturador_obj.config_facturador(
                cursor, uid, tarifa, contract_id_index
            )

            component = tarifa.phf_calc(
                curve, date(2017, 1, 1)
            )
            # test component
            tarifa.factura_energia()
            assert tarifa.code == '2.0A'
            activa = tarifa.termes['activa']
            expect(tarifa.conf['audit']).to(equal(['curve', 'pmd']))

            facturador_obj.audit_data(cursor, uid, tarifa, invoice_id)
            attach_ids = attach_obj.search(
                cursor, uid, [
                    ('res_model', '=', 'giscedata.facturacio.factura'),
                    ('res_id', '=', invoice_id)
                ]
            )
            expect(len(attach_ids)).to(equal(2))
            attach_data = attach_obj.read(
                cursor, uid, attach_ids, ['name', 'datas_fname']
            )
            names = ['PMD_{0}_20160101_20160229.csv'.format(invoice_id),
                     'CURVE_{0}_20160101_20160229.csv'.format(invoice_id)]
            for attach in attach_data:
                expect(names).to(contain(attach['name']))
                expect(names).to(contain(attach['datas_fname']))

    def test_phf_d_0(self):
        ESIOS_TOKEN = tools.config['esios_token']
        token = ESIOS_TOKEN

        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            conf_obj = self.openerp.pool.get('res.config')
            attach_obj = self.openerp.pool.get('ir.attachment')

            # gets contract 0001
            contract_id_index = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            pricelist_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]

            contract_obj.write(cursor, uid, contract_id_index, {
                'coeficient_k': 5.0, 'coeficient_d': 0.0
            })

            contract_obj.send_signal(
                cursor, uid, [contract_id_index], ['validar', 'contracte']
            )
            self.crear_modcon(
                cursor, uid, contract_id_index, '2017-01-01', '2017-12-31'
            )

            context = {'llista_preu': pricelist_id}
            versions = facturador_obj.versions_de_preus(
                cursor, uid, contract_id_index, '2017-01-01', '2017-01-31',
                context
            )

            curve_data = self.get_curve()
            curve = Curve(datetime(2017, 1, 1))
            curve.load(curve_data)

            consums = {
                'activa': {'2017-01-01': curve_data},
                'reactiva': {'P1': 2}
            }
            tarifa = Tarifa20APool(
                consums, {},
                '2017-01-01', '2017-01-31',
                facturacio=1, facturacio_potencia='icp',
                data_inici_periode='2017-01-01',
                data_final_periode='2017-01-31',
                potencies_contractades={'P1': 4.6},
                versions=versions,
                holidays=HOLIDAYS,
                esios_token=token
            )

            conf_obj.set(
                cursor, uid, 'fact_indexed_audit_fields', "['curve', 'pmd']"
            )
            facturador_obj.config_facturador(
                cursor, uid, tarifa, contract_id_index
            )

            component = tarifa.phf_calc(
                curve, date(2017, 1, 1)
            )
            # test component
            tarifa.factura_energia()
            assert tarifa.code == '2.0A'
            activa = tarifa.termes['activa']
            expect(tarifa.conf['audit']).to(equal(['curve', 'pmd']))

    def get_curve_name(self, document):
        """Gets CUPS from parsed line"""
        for key in document.keys():
            if key not in ['orig']:
                return key

    def get_cups(self, document):
        """Gets CUPS from parsed line"""
        curve_name = self.get_curve_name(document)
        return document.get(curve_name).data['name']

    def unlink_tg_models(self, cursor, uid):
        models = ['tg.cchfact', 'tg.cchval', 'tg.cchcons', 'tg.p1', 'tg.f1']
        for model_name in models:
            cch_obj = self.openerp.pool.get(model_name)
            del_ids = cch_obj.search(cursor, uid, [])
            cch_obj.unlink(cursor, uid, del_ids)

    def test_validate_lack_of_curve(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')

            cch_ids, model = compt_obj.get_curve_ids(
                cursor, uid, 1, '2015-02-01', '2015-02-02'
            )

            self.assertFalse(cch_ids)

    def test_P5D_validate_existance_of_curve(self):
        self.load_curve_file(self.P5D_file)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
            cch_ids, model = compt_obj.get_curve_ids(
                cursor, uid, 1, '2017-02-01', '2017-02-28'
            )
            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

            self.assertTrue(cch_ids)

    def test_F5D_validate_existance_of_curve(self):
        self.load_curve_file(self.F5D_file)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
            cch_ids, model = compt_obj.get_curve_ids(
                cursor, uid, 1,'2016-10-01', '2016-11-02'
            )
            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

            self.assertTrue(cch_ids)

    def test_F5D_complete_curve_validation(self):

        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        self.load_curve_file(self.F5D_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001')[1]

            clot = lot_obj.browse(cursor, uid, lot_id)
            check_result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.assertIsNone(check_result)
            check_result = validator_obj.check_fullness_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.unlink_tg_models(cursor, uid)
            self.assertIsNone(check_result)

    def test_F5D_incomplete_curve_validation(self):

        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        self.load_curve_file(self.F5D_incomplete_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001')[1]

            clot = lot_obj.browse(cursor, uid, lot_id)
            check_result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.assertIsNone(check_result)
            check_result = validator_obj.check_fullness_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.unlink_tg_models(cursor, uid)
            self.assertEqual(
                check_result['message'],
                u'Es requereix corba completa per poder facturar.'
            )
            self.assertEqual(check_result['curve'], u'tg.cchfact')

    def test_P5D_complete_curve_validation(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        self.load_curve_file(self.P5D_full_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001')[1]

            clot = lot_obj.browse(cursor, uid, lot_id)
            check_result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.assertIsNone(check_result)
            check_result = validator_obj.check_fullness_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.unlink_tg_models(cursor, uid)
            self.assertIsNone(check_result)

    def test_P5D_incomplete_curve_validation(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot'
        )
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        self.load_curve_file(self.P5D_incomplete_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001')[1]

            clot = lot_obj.browse(cursor, uid, lot_id)
            check_result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.assertIsNone(check_result)
            check_result = validator_obj.check_fullness_of_curve(
                cursor, uid, clot, '2016-10-01', '2016-11-02', {}
            )
            self.unlink_tg_models(cursor, uid)
            self.assertEqual(
                check_result['message'],
                u'Es requereix corba completa per poder facturar.'
            )
            self.assertEqual(check_result['curve'], u'tg.cchval')

    def test_validate_fullness_when_no_curve(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001')[1]

            clot = lot_obj.browse(cursor, uid, lot_id)

            check_result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, '2015-02-01', '2015-02-02', {}
            )
            self.assertEqual(
                check_result['message'],
                u'Si es salta la validació es procedirà a perfilar.'
            )
            check_result = validator_obj.check_fullness_of_curve(
                cursor, uid, clot, '2015-02-01', '2015-02-02', {}
            )
            self.assertIsNone(check_result)

    def test_indexed_validations_atr_mode_contract(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # contract 0002 mode 'atr'
            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0002')[1]

            clot = lot_obj.browse(cursor, uid, lot_id)

            check_result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, '2015-02-01', '2015-02-02', {}
            )
            self.assertIsNone(check_result)
            check_result = validator_obj.check_fullness_of_curve(
                cursor, uid, clot, '2015-02-01', '2015-02-02', {}
            )
            self.assertIsNone(check_result)

    def test_get_consum_per_facturar_profile(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_idx_measures(cursor, uid)

            # només fins aquesta data
            ctx = {'fins_lectura_fact': '2016-11-01'}

            tarifa_id = tarifa_obj.search(
                cursor, uid, [('name', '=', '2.0A')]
            )[0]

            # meter 0001
            meter_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            res = meter_obj.get_consum_per_facturar(
                cursor, uid, meter_id, tarifa_id, context=ctx
            )
            expect(res['origin']).to(equal('REEPROFILE'))

    def test_get_consum_per_facturar_p5d(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')

        self.load_curve_file(self.P5D_full_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_idx_measures(cursor, uid)

            # només fins aquesta data
            ctx = {'fins_lectura_fact': '2016-11-01'}

            tarifa_id = tarifa_obj.search(
                cursor, uid, [('name', '=', '2.0A')]
            )[0]

            # meter 0001
            meter_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            res = meter_obj.get_consum_per_facturar(
                cursor, uid, meter_id, tarifa_id, context=ctx
            )
            expect(res['origin']).to(equal('CCHVAL'))

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_get_consum_per_facturar_f5d(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')

        self.load_curve_file(self.F5D_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_idx_measures(cursor, uid)

            # només fins aquesta data
            ctx = {'fins_lectura_fact': '2016-11-01'}

            tarifa_id = tarifa_obj.search(
                cursor, uid, [('name', '=', '2.0A')]
            )[0]
            # meter 0001
            meter_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            res = meter_obj.get_consum_per_facturar(
                cursor, uid, meter_id, tarifa_id, context=ctx
            )
            expect(res['origin']).to(equal('CCHFACT'))
            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_get_consum_per_facturar_P1(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')

        self.load_curve_file(self.P1_curve_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_idx_measures(cursor, uid)

            # només fins aquesta data
            ctx = {'fins_lectura_fact': '2016-11-01'}

            tarifa_id = tarifa_obj.search(
                cursor, uid, [('name', '=', '2.0A')]
            )[0]

            # meter 0001
            meter_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            res = meter_obj.get_consum_per_facturar(
                cursor, uid, meter_id, tarifa_id, context=ctx
            )
            expect(res['origin']).to(equal('P1'))

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_get_consum_per_facturar_F1(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')

        self.load_curve_file(self.F1_curve_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_idx_measures(cursor, uid)

            # només fins aquesta data
            ctx = {'fins_lectura_fact': '2016-11-01'}

            tarifa_id = tarifa_obj.search(
                cursor, uid, [('name', '=', '2.0A')]
            )[0]

            # meter 0001
            meter_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            res = meter_obj.get_consum_per_facturar(
                cursor, uid, meter_id, tarifa_id, context=ctx
            )
            expect(res['origin']).to(equal('F1'))

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_manual_invoice_profiling(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # clean curves
            self.unlink_tg_models(cursor, uid)

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            self.prepare_idx_measures(cursor, uid)

            meter = contract.comptadors[0]
            meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, False)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 1)
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 30)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 4.6)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 1)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 47)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 17)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2016-10-31')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2016-09-30')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 4.6)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)

    def test_manual_invoice_profiling_with_meter_change(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # clean curves
            self.unlink_tg_models(cursor, uid)

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            self.prepare_idx_measures(cursor, uid)

            self.add_additional_meter(cursor, uid)

            contract = contract_obj.browse(cursor, uid, contract_id)

            for meter in contract.comptadors:
                meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, False)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 2)
            self.assertEqual(len(invoice.lectures_potencia_ids), 2)
            self.assertEqual(len(invoice.linies_energia), 2)
            # comptador 1
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 30)
            # comptador 2
            self.assertEqual(invoice.linies_energia[1].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[1].invoice_line_id.quantity, 70)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 4.6)
            # Check lectures comptador 1
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 47)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 17)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2016-10-14')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2016-09-30')
            # Check lectures comptador 2
            self.assertEqual(invoice.lectures_energia_ids[1].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[1].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[1].lect_actual, 70)
            self.assertEqual(invoice.lectures_energia_ids[1].lect_anterior, 0)
            self.assertEqual(invoice.lectures_energia_ids[1].data_actual, '2016-10-31')
            self.assertEqual(invoice.lectures_energia_ids[1].data_anterior, '2016-10-14')
            # Check lectures potencia
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 4.6)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)

    def test_manual_invoice_profiling_31A_lb(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # clean curves
            self.unlink_tg_models(cursor, uid)

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            self.prepare_idx_measures_31ALB(cursor, uid)

            meter = contract.comptadors[0]
            meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, False)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 6)
            self.assertEqual(len(invoice.lectures_potencia_ids), 6)
            self.assertEqual(len(invoice.linies_energia), 3)
            for linia in invoice.linies_energia:
                self.assertEqual(linia.tipus, 'energia')
            # total curve raised because profiled
            total_quantity = sum([l.quantity for l in invoice.linies_energia])
            # curve raised
            expected_quantity = 5000 * 1.04 + (50 * 0.01 * (24 * 31 + 1))
            self.assertEqual(total_quantity, expected_quantity)
            self.assertEqual(len(invoice.linies_potencia), 3)
            for linia in invoice.linies_potencia:
                self.assertEqual(linia.tipus, 'potencia')
                self.assertEqual(linia.invoice_line_id.quantity, 44.6 * 1.04)
            # Check lectures
            self.assertEqual(invoice.lectures_energia_ids[0].name, '3.1A LB (P1)')
            for lectura in invoice.lectures_energia_ids:
                self.assertEqual(lectura.tipus, 'activa')
                self.assertEqual(lectura.lect_actual, 1000)
                self.assertEqual(lectura.lect_anterior, 0)
                self.assertEqual(lectura.data_actual, '2016-10-31')
                self.assertEqual(lectura.data_anterior, '2016-09-30')

            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            for potencia in invoice.lectures_potencia_ids:
                self.assertEqual(potencia.pot_contract, 44.6)
                self.assertEqual(potencia.pot_maximetre, 44.6)
                self.assertEqual(potencia.data_actual, '2016-10-31')

    def test_manual_invoice_p5d(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        self.load_curve_file(self.P5D_full_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.set_audit_fields(cursor, uid)

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            self.prepare_idx_measures(cursor, uid)

            meter = contract.comptadors[0]
            meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, True)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 1)
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 30.73)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 4.6)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 1)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 47)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 17)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2016-10-31')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2016-09-30')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 4.6)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_manual_invoice_p5d_31A_lb(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        self.load_curve_file(self.P5D_full_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            self.prepare_idx_measures_31ALB(cursor, uid)

            meter = contract.comptadors[0]
            meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, True)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 6)
            self.assertEqual(len(invoice.lectures_potencia_ids), 6)
            self.assertEqual(len(invoice.linies_energia), 3)
            for linia in invoice.linies_energia:
                self.assertEqual(linia.tipus, 'energia')
            # total curve raised because profiled
            total_quantity = round(
                sum([l.quantity for l in invoice.linies_energia])
                , 2
            )
            # curve raised
            expected_quantity = round(30.73, 2)  # total curve sum
            self.assertEqual(total_quantity, expected_quantity)
            self.assertEqual(len(invoice.linies_potencia), 3)
            for linia in invoice.linies_potencia:
                self.assertEqual(linia.tipus, 'potencia')
                self.assertEqual(linia.invoice_line_id.quantity, 44.6 * 1.04)
            # Check lectures
            self.assertEqual(invoice.lectures_energia_ids[0].name, '3.1A LB (P1)')
            for lectura in invoice.lectures_energia_ids:
                self.assertEqual(lectura.tipus, 'activa')
                self.assertEqual(lectura.lect_actual, 1000)
                self.assertEqual(lectura.lect_anterior, 0)
                self.assertEqual(lectura.data_actual, '2016-10-31')
                self.assertEqual(lectura.data_anterior, '2016-09-30')

            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            for potencia in invoice.lectures_potencia_ids:
                self.assertEqual(potencia.pot_contract, 44.6)
                self.assertEqual(potencia.pot_maximetre, 44.6)
                self.assertEqual(potencia.data_actual, '2016-10-31')

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_manual_invoice_P1_31A_lb(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        self.load_curve_file(self.P1_curve_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            self.prepare_idx_measures_31ALB(cursor, uid)

            meter = contract.comptadors[0]
            meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, True)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 6)
            self.assertEqual(len(invoice.lectures_potencia_ids), 6)
            self.assertEqual(len(invoice.linies_energia), 3)
            for linia in invoice.linies_energia:
                self.assertEqual(linia.tipus, 'energia')
            # total curve raised because profiled
            total_quantity = round(
                sum([l.quantity for l in invoice.linies_energia])
                , 2
            )
            # curve raised
            expected_quantity = round(
                27309 * 1.04 + (50 * 0.01 * (24 * 31 + 1))
                , 2
            )  # total curve sum
            self.assertEqual(total_quantity, expected_quantity)
            self.assertEqual(len(invoice.linies_potencia), 3)
            for linia in invoice.linies_potencia:
                self.assertEqual(linia.tipus, 'potencia')
                self.assertEqual(linia.invoice_line_id.quantity, 44.6 * 1.04)
            # Check lectures
            self.assertEqual(invoice.lectures_energia_ids[0].name, '3.1A LB (P1)')
            for lectura in invoice.lectures_energia_ids:
                self.assertEqual(lectura.tipus, 'activa')
                self.assertEqual(lectura.lect_actual, 1000)
                self.assertEqual(lectura.lect_anterior, 0)
                self.assertEqual(lectura.data_actual, '2016-10-31')
                self.assertEqual(lectura.data_anterior, '2016-09-30')

            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            for potencia in invoice.lectures_potencia_ids:
                self.assertEqual(potencia.pot_contract, 44.6)
                self.assertEqual(potencia.pot_maximetre, 44.6)
                self.assertEqual(potencia.data_actual, '2016-10-31')

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_manual_invoice_F1_31A_lb(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20160101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        self.load_curve_file(self.F1_curve_file)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contract = contract_obj.browse(cursor, uid, contract_id)

            self.prepare_idx_measures_31ALB(cursor, uid)

            meter = contract.comptadors[0]
            meter.write({'lloguer': False})

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2016-10-01',
                'date_end': '2016-10-31',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2016-10-01')
            self.assertEqual(invoice.data_final, '2016-10-31')
            # Check cch available
            self.assertEqual(invoice.cch_fact_available, True)
            # Check lines
            self.assertEqual(len(invoice.lectures_energia_ids), 6)
            self.assertEqual(len(invoice.lectures_potencia_ids), 6)
            self.assertEqual(len(invoice.linies_energia), 3)
            for linia in invoice.linies_energia:
                self.assertEqual(linia.tipus, 'energia')
            # total curve raised because profiled
            total_quantity = round(
                sum([l.quantity for l in invoice.linies_energia])
                , 2
            )
            # curve raised
            expected_quantity = round(18399, 2)
            self.assertEqual(total_quantity, expected_quantity)
            self.assertEqual(len(invoice.linies_potencia), 3)
            for linia in invoice.linies_potencia:
                self.assertEqual(linia.tipus, 'potencia')
                self.assertEqual(linia.invoice_line_id.quantity, 44.6 * 1.04)
            # Check lectures
            self.assertEqual(invoice.lectures_energia_ids[0].name, '3.1A LB (P1)')
            for lectura in invoice.lectures_energia_ids:
                self.assertEqual(lectura.tipus, 'activa')
                self.assertEqual(lectura.lect_actual, 1000)
                self.assertEqual(lectura.lect_anterior, 0)
                self.assertEqual(lectura.data_actual, '2016-10-31')
                self.assertEqual(lectura.data_anterior, '2016-09-30')

            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            for potencia in invoice.lectures_potencia_ids:
                self.assertEqual(potencia.pot_contract, 44.6)
                self.assertEqual(potencia.pot_maximetre, 44.6)
                self.assertEqual(potencia.data_actual, '2016-10-31')

            # unlink cchfact from mongo
            self.unlink_tg_models(cursor, uid)

    def test_check_invoicing_mode_in_json(self):
        # Add invoicing mode in json
        # We don't read the invoicing modes that are on `no_check_modes`
        # In this test we test with the invoicing_mode `index` and `atr`

        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get('giscedata.facturacio.contracte_lot')
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Load demo lot
            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001')[1]

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            # Set the policy to invoice method with indexed prices
            polissa_params = {'mode_facturacio': 'index'}

            pol_obj.write(cursor, uid, polissa_id, polissa_params)

            clot = lot_obj.browse(cursor, uid, lot_id)

            date_from = "2018-06-23 01:20:30"
            date_to = "2018-06-24 01:20:30"

            # The validator should not check index because is on
            # 'no_check_modes'
            parameters = {
                'no_check_modes': ['index'],
            }

            result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, date_from, date_to, parameters)

            # The result is None because there is not any mode to check
            self.assertIsNone(result)

            # Now the validator should check index mode because it is not on
            # 'no_check_modes'
            parameters = {
                'no_check_modes': [],
            }

            result = validator_obj.check_existance_of_curve(
                cursor, uid, clot, date_from, date_to, parameters)

            # The validator checked index mode so the result should be not none
            self.assertIsNotNone(result)