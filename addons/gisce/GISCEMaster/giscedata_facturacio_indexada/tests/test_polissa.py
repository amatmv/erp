# -*- coding: utf-8 -*-
import collections

from destral.transaction import Transaction
from destral import testing

from giscedata_polissa.tests import utils as utils_polissa
from giscedata_facturacio.tests.tests_polissa_facturacio import TestGetModcontractualIntervals


class TestGetModcontractualIntervalsIndexada(TestGetModcontractualIntervals):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.polissa_id = polissa_id
        polissa_obj.write(
            cursor, uid, [self.polissa_id], {'mode_facturacio': 'atr'})
        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def test_get_intervals_change_fields(self):
        super(
            TestGetModcontractualIntervalsIndexada, self
        ).test_get_intervals_change_fields()
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool
        Change = collections.namedtuple(
            'Change', ['field', 'value', 'start_date', 'end_date']
        )
        # Creem la segona modificació contractual fent un canvi de potencia
        mod_changes = [
            Change('mode_facturacio', 'index', '2016-11-02', '2017-12-01')
        ]
        polissa_obj = pool.get('giscedata.polissa')
        for change in mod_changes:
            utils_polissa.crear_modcon(
                pool, cursor, uid, self.polissa_id,
                {change.field: change.value}, change.start_date, change.end_date
            )
            changes = polissa_obj.get_modcontractual_intervals(
                cursor, uid, self.polissa_id,
                '{}-01'.format(change.start_date[:-3]),
                '{}-10'.format(change.end_date[:-3])
            )

            self.assertIn(change.start_date, changes)
            self.assertIn(change.field, changes[change.start_date]['changes'])


class TestModeFacturacioPVPC(testing.OOTestCaseWithCursor):

    def test_polissa_mode_facturacio_selection(self):

        cursor = self.cursor
        uid = self.uid

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        f = pol_obj.fields_get(cursor, uid, ['mode_facturacio'])
        self.assertIn(
            ('index', 'Indexada'),
            f['mode_facturacio']['selection'],
        )

        mc_obj = self.openerp.pool.get('giscedata.polissa.modcontractual')
        f = mc_obj.fields_get(cursor, uid, ['mode_facturacio'])
        self.assertIn(
            ('index', 'Indexada'),
            f['mode_facturacio']['selection'],
        )
