# -*- coding: utf-8 -*-
from giscedata_facturacio_indexada.report.report_indexada_helpers import colorPicker

with description("Color Picker"):
    with before.all:
        values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 5, 0, 12, 6, 3]
        colors = ['#000000', '#111111', '#222222', '#333333', '#444444']
        self.cp = colorPicker(colors, values)

    with context('5 color picker'):
        with it('gets correct min (0)'):
            assert self.cp.min_value == 0
        with it('gets correct max (12)'):
            assert self.cp.max_value == 12
        with it('gets correct slize (12/5 = 2.4)'):
            assert self.cp.slice == 2.4
        with it('gets #000000 for 0, 1 and 2'):
            for value in [0, 1, 2]:
                assert self.cp.pick_color(value) == '#000000'
        with it('gets #111111 for 2.4, 3 and 4'):
            for value in [2.4, 3, 4]:
                assert self.cp.pick_color(value) == '#111111'
        with it('gets #222222 for 4.8, 5, 6 and 7'):
            for value in [4.8, 5, 6, 7]:
                assert self.cp.pick_color(value) == '#222222'
        with it('gets #333333 for 7.2, 8, and 9'):
            for value in [7.2, 8, 9]:
                assert self.cp.pick_color(value) == '#333333'
        with it('gets #444444 for 9.6, 10, 11 and 12'):
            for value in [9.6, 10, 11, 12]:
                assert self.cp.pick_color(value) == '#444444'
