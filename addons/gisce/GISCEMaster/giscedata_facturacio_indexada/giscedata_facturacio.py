# -*- coding: utf-8 -*-
from osv import osv
from .tarifes import TARIFFS_FACT
import csv
import base64
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
from libfacturacioatr.pool.tarifes import TarifaPool
from tools import config
from .giscedata_lectures import INVOICE_WITH_CURVE


class GiscedataFacturacioFacturador(osv.osv):
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def get_tarifa_class(self, modcontractual):
        parent = super(GiscedataFacturacioFacturador, self).get_tarifa_class
        if modcontractual.mode_facturacio == 'index':
            return TARIFFS_FACT[modcontractual.tarifa.name]
        else:
            return parent(modcontractual)

    def versions_de_preus(self, cursor, uid, polissa_id, data_inici,
                          data_final, context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        pricelist_obj = self.pool.get('product.pricelist')
        imd_obj = self.pool.get('ir.model.data')

        # Fem un browse amb la data final per obtenir quina tarifa té
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context={
            'date': data_final
        })

        if polissa.mode_facturacio != 'index':
            res = super(GiscedataFacturacioFacturador, self).versions_de_preus(
                cursor, uid, polissa_id, data_inici, data_final, context
            )
        else:
            # indexed invoicing mode
            factor_k = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_indexada', 'product_factor_k'
            )[1]
            omie = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_indexada', 'product_omie'
            )[1]
            fondo_ef = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_indexada',
                'product_fondo_eficiencia'
            )[1]
            imu = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_indexada', 'product_imu'
            )[1]
            desvios = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_indexada', 'product_desvios'
            )[1]

            if context.get('llista_preu', False):
                llista_preu_id = context['llista_preu']
            else:
                llista_preu_id = polissa.llista_preu.id

            ctx = context.copy()
            tarifa = polissa.tarifa

            pc_pl = pricelist_obj.search(cursor, uid, [
                ('name', '=', 'PAGOS POR CAPACIDAD')
            ])
            pc_pl = pc_pl and pc_pl[0] or None

            # Get coeficients k/d from contract intervals
            ctx_i = {'ffields': ['coeficient_k', 'coeficient_d']}
            polissa_intervals = polissa_obj.get_modcontractual_intervals(
                cursor, uid, polissa_id, data_inici, data_final, context=ctx_i
            )
            # We have to get prices for every contract interval
            res = {}
            for interval, dades in polissa_intervals.items():
                inici = max(dades['dates'][0], data_inici)
                fi = min(dades['dates'][1], data_final)
                prices = super(
                    GiscedataFacturacioFacturador,
                    self
                ).versions_de_preus(
                    cursor, uid, polissa_id, inici, fi, context
                )
                res.update(prices)

            for date_version in res:
                res[date_version] = {}
                ctx['date'] = date_version
                periodes = tarifa.get_periodes_preus(
                    'te', llista_preu_id, context=ctx
                )
                res[date_version]['atr'] = periodes
                res[date_version]['k'] = pricelist_obj.price_get(
                    cursor, uid, [llista_preu_id], factor_k, 1, context=ctx
                )[llista_preu_id]
                res[date_version]['omie'] = pricelist_obj.price_get(
                    cursor, uid, [llista_preu_id], omie, 1, context=ctx
                )[llista_preu_id]
                res[date_version]['fe'] = pricelist_obj.price_get(
                    cursor, uid, [llista_preu_id], fondo_ef, 1, context=ctx
                )[llista_preu_id]
                res[date_version]['imu'] = pricelist_obj.price_get(
                    cursor, uid, [llista_preu_id], imu, 1, context=ctx
                )[llista_preu_id]
                res[date_version]['d'] = pricelist_obj.price_get(
                    cursor, uid, [llista_preu_id], desvios, 1, context=ctx
                )[llista_preu_id]
                res[date_version]['pc'] = pc = {}  # Short alias
                if pc_pl:
                    for periode in tarifa.periodes:
                        if periode.tipus != 'te':
                            continue
                        if periode.agrupat_amb:
                            prod = periode.agrupat_amb.product_id.id
                        else:
                            prod = periode.product_id.id
                        pc[periode.name] = pricelist_obj.price_get(
                            cursor, uid, [pc_pl], prod, 1, context=ctx
                        )[pc_pl]
                # coeficients k/d from contract
                pol_vals = polissa_obj.read(
                    cursor, uid, polissa_id, ['coeficient_d', 'coeficient_k'],
                    context={'date': date_version}
                )
                ck = pol_vals['coeficient_k']
                cd = pol_vals['coeficient_d']
                if round(ck + cd, 6) > 0.0:
                    # K/D in contract are in MWh
                    res[date_version]['k'] = ck * 0.001
                    res[date_version]['d'] = cd * 0.001

        return res

    def config_facturador(
            self, cursor, uid, facturador, polissa_id, context=None):
        """Gets audit data params from res.config `fact_indexed_audit_fields`
        :param facturador: Tariff Class
        :return: True
        """
        super(GiscedataFacturacioFacturador, self).config_facturador(
            cursor, uid, facturador, polissa_id, context=context
        )

        if not isinstance(facturador, TarifaPool):
            return True

        # ESIOS token to get esios data for indexed invoicing
        facturador.conf['esios_token'] = config.get('esios_token', '')

        import logging
        logger = logging.getLogger('openerp.indexada')

        conf_obj = self.pool.get('res.config')
        str_val = conf_obj.get(cursor, uid, 'fact_indexed_audit_fields', '[]')
        try:
            params = list(eval(str_val))
        except (NameError, TypeError) as e:
            # not a list
            params = []
            logger.info(
                u"Bad 'fact_indexed_audit_fields' param. "
                u"Must be a list: {0}".format(str_val)
            )

        audit_keys = [
            key for key in params
            if key in facturador.get_available_audit_coefs().keys()
        ]
        facturador.conf['audit'] = audit_keys

        logger.info(u"Audit fields: {0}".format(facturador.conf['audit']))

        return True

    def fact_energia(self, cursor, uid, facturador, invoice_id, context=None):
        """
        Invoices energia
        :param facturador: Tariff Class
        :param invoice_id: Id de factura
        """

        curve_origin = facturador.consums['activa'].get('origin', '')
        # When AT measured in BT, curve must be raised if origin is
        # * P1: measures in meter, not "Punto Frontera"
        # * Profiled: Uses measures in BT
        if curve_origin in ['P1', 'REEPROFILE'] and facturador.is_lbt:
            facturador.conf['ajustar_corba'] = True

        inv_obj = self.pool.get('giscedata.facturacio.factura')
        super(GiscedataFacturacioFacturador, self).fact_energia(
            cursor, uid, facturador, invoice_id, context=context
        )

        if not isinstance(facturador, TarifaPool):
            return

        # Sets cch_fact_available to true when real curve is used
        # origin values: REEPROFILE, P1, F1, CCHVAL, CCHFACT, CCHCONS
        if curve_origin in ['P1', 'F1', 'CCHVAL', 'CCHFACT', 'CCHCONS']:
            inv_obj.write(cursor, uid, invoice_id, {'cch_fact_available': True})

        self.audit_data(cursor, uid, facturador, invoice_id)

    def audit_data(self, cursor, uid, facturador, invoice_id):
        """Audit data. Adds audit data as attachment on invoice_id.
        returns True
        :param facturador: Tariff Class
        :param invoice_id: Invoice_id
        :return: True
        """

        if not isinstance(facturador, TarifaPool):
            return

        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        attach_obj = self.pool.get('ir.attachment')
        invoice_data = invoice_obj.read(
            cursor, uid, invoice_id, ['data_inici', 'data_final']
        )

        start_date = invoice_data['data_inici']
        end_date = invoice_data['data_final']
        start_str = start_date.replace('-', '')
        end_str = end_date.replace('-', '')
        for key in facturador.audit_data.keys():
            filename = "{0}_{1}_{2}_{3}.csv".format(
                key.upper(), invoice_data['id'], start_str, end_str,
            )
            data = facturador.get_audit_data(key)
            sio = StringIO()
            csvwriter = csv.writer(sio, delimiter=';')
            for row in data:
                # only invoiced vals
                row_date = row[0].split(' ')[0]
                if start_date <= row_date <= end_date:
                    csvwriter.writerow(row)

            b64data = base64.b64encode(sio.getvalue())
            sio.close()

            vals = {
                'res_model': 'giscedata.facturacio.factura',
                'res_id': invoice_id,
                'name': filename,
                'datas_fname': filename,
                'datas': b64data,
            }
            attach_obj.create(cursor, uid, vals)

        return True

GiscedataFacturacioFacturador()


class GiscedataFacturacioValidationValidator(osv.osv):
    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'

    def check_invoiced_energy(self, cursor, uid, fact, parameters):
        validation_result = super(
            GiscedataFacturacioValidationValidator, self
        ).check_invoiced_energy(cursor, uid, fact, parameters)
        mode_facturacio_indexada = (
            fact.polissa_id.mode_facturacio in INVOICE_WITH_CURVE
        )

        # Hi ha diferència entre energia facturada i consumida i el mode de
        # facturació és indexada
        if validation_result is not None and mode_facturacio_indexada:
            corba_real = fact.cch_fact_available
            # Si la corba és real hi pot haver una diferència de +-1 Kwh
            # Si la corba no és real (perfilada) no hi pot haver diferència
            diferencia_consumida_facturada = abs(
                validation_result['invoiced_energy'] - validation_result['measured_consumption']
            )
            if corba_real and diferencia_consumida_facturada <= 1:
                validation_result = None

        return validation_result


GiscedataFacturacioValidationValidator()
