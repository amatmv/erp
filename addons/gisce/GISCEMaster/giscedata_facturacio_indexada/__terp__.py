# -*- coding: utf-8 -*-
{
    "name": "Facturació indexada",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Facturació indexada
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_facturacio_comer",
        "giscedata_pagos_capacidad",
        "giscedata_telegestio_comer"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_facturacio_indexada_demo.xml"
    ],
    "update_xml":[
        "giscedata_facturacio_indexada_data.xml",
        "giscedata_validation_indexed_data.xml",
        "giscedata_polissa_data.xml",
        "giscedata_polissa_view.xml",
        "wizard/wizard_measures_from_curve_view.xml",
        "giscedata_facturacio_indexada_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
