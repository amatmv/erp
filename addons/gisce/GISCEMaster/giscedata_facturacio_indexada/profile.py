# -*- coding: utf-8 -*-
import csv
import logging
from datetime import datetime, timedelta, date

from enerdata.datetime.timezone import TIMEZONE
from enerdata.profiles.profile import REEProfile, Coefficent, EnergyMeasure
from enerdata.contracts.tariff import TariffPeriod
logger = logging.getLogger('openerp' + __name__)


def adapt_measures(lectures, start_date, end_date, last_measure=None, meter_start=None):
    '''
    Adapts GisceERP measures format to a EnergyMeasure list to
    generate a profiled curve
    [
      {
        'P1': {
          'actual': {
           'name': 'yyyy-mm-d',
           'lectura': nnn,
           'consum': nnn
          }
          'anterior: {'name': 'yyyy-mm-d' }
        },
        'P2': {...},
        ....
        'Pn': {...},
      },
      {
        'P1': {...},
        ...
      },
      ....
    ]
    :param last_invoiced: Last invoiced measure in contract
    :param meter_start: Start date of current meter
    :param lectures: struct with measures in GisceERP
    :param start_date: Start date of curve to generate
    :param end_date: End date of curve to generate

    :return: dict
    {
        'start': curve start date/hour (datetime),
        'end': curve end date/hour (datetime),
        'measures': list of EnergyMeasure elements to pass to profiler
    }
    '''
    start = datetime.strptime(start_date, '%Y-%m-%d')
    end = datetime.strptime(end_date, '%Y-%m-%d')
    curve_start = start
    curve_end = end

    measures = []
    for periode in lectures:
        for pos in ('actual', 'anterior'):
            vals = lectures[periode][pos]
            year, month, day = [
                int(x)
                for x in vals['name'].split('-')
            ]
            measure_date = date(year, month, day)
            dates_ult_set = (last_measure or '')
            add_day = (
                # Hem de perfilar per els dies que facturem,
                # no els ja facturats
                (pos == 'anterior' and vals['name'] in dates_ult_set) or
                # Hem de perfilar per els dies que el comptador està actiu,
                # La primera lectura és el dia anterior de la alta a les 24:00
                (pos == 'anterior' and vals['name'] < meter_start)
            )
            if add_day:
                # Hem de perfilar per els dies que facturem,
                # no els ja facturats
                measure_date += timedelta(days=1)
                curve_start = datetime.combine(
                    measure_date, datetime.min.time()
                )
            measures.append(
                EnergyMeasure(
                    measure_date,
                    TariffPeriod(periode, 'te'),
                    vals['lectura'] * 1000,
                    consumption=vals['consum'] * 1000
                ))
    res = {'start': curve_start, 'end': curve_end, 'measures': measures}
    return res


class REEInitalProfile(REEProfile):
    def __init__(self, path):
        self.path = path

    def get(self, year, month):
        key = '%(year)s%(month)02i' % locals()
        if key in self._CACHE:
            logger.debug('Using CACHE for REEProfile {0}'.format(key))
            return self._CACHE[key]
        with open(self.path, 'r') as m:
            reader = csv.reader(m, delimiter=';')
            header = 2
            cofs = []
            for vals in reader:
                if header:
                    header -= 1
                    continue
                if int(vals[0]) != month:
                    continue
                dt = datetime(
                    year, int(vals[0]), int(vals[1])
                )
                day = TIMEZONE.localize(dt) + timedelta(hours=int(vals[2]))
                cofs.append(Coefficent(
                    TIMEZONE.normalize(day), dict(
                        (k, float(vals[i])) for i, k in enumerate('ABCD', 3)
                    ))
                )
            self._CACHE[key] = cofs
            return cofs