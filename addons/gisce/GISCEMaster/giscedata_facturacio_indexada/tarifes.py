from enerdata.contracts.tariff import *
from libfacturacioatr.pool.tarifes import *


TARIFFS = {
    '2.0A': T20A,
    '2.0DHA': T20DHA,
    '2.0DHS': T20DHS,
    '2.1A': T21A,
    '2.1DHA': T21DHA,
    '2.1DHS': T21DHS,
    '3.0A': T30A,
    '3.1A': T31A,
    '3.1A LB': T31A,
}

TARIFFS_FACT = {
    '2.0A': Tarifa20APool,
    '2.0DHA': Tarifa20DHAPool,
    '2.1A': Tarifa21APool,
    '2.1DHA': Tarifa21DHAPool,
    '3.0A': Tarifa30APool,
    '3.1A': Tarifa31APool,
    '3.1A LB': Tarifa31ALBPool,
    '6.1A': Tarifa61APool
}

PERFS_COF = {
    '2.0DHS': 'D',
    '2.1DHS': 'D',
    '2.0A': 'A',
    '2.0DHA': 'B',
    '2.1A': 'A',
    '2.1DHA': 'B',
    '3.0A': 'C',
    '3.1A': 'C',
    '3.1A LB': 'C',
}