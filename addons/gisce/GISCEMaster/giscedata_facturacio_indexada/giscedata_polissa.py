# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse as parse_date
from datetime import datetime
from enerdata.profiles.profile import *

from giscedata_facturacio.giscedata_polissa import INTERVAL_INVOICING_FIELDS

INTERVAL_INVOICING_FIELDS += ['mode_facturacio']


def localize_season(dt, season):
    if isinstance(dt, basestring):
        dt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
    dst = bool(season == 1)
    return TIMEZONE.normalize(TIMEZONE.localize(dt, is_dst=dst))


def convert_to_profilehour(measure):
    ph = ProfileHour(
        localize_season(measure['datetime'], measure['season']),
        measure['ai'],
        1,
        0.0
    )
    return ph


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def validate_existance_of_curve(self, cursor, uid, polissa_id,
                                    date_from=None, date_to=None,
                                    context=None):

        compt_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        meters = polissa_obj.comptadors_actius(cursor, uid, polissa_id,
                                               date_from)

        for meter in meters:
            cch_ids, model_name = compt_obj.get_curve_ids(cursor, uid, meter,
                                                          date_from,
                                                          date_to)

        return cch_ids

    def validate_fullness_of_curve(self, cursor, uid, polissa_id,
                                   date_from=None, date_to=None,
                                   context=None):
        """
        Returns number of gaps in the first curve found
        :param polissa_id: contract id
        :param date_from: Start of curve (included)
        :param date_to: End of curve (NOT included)
        :param context:
        :return:
        """
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        meters = polissa_obj.comptadors_actius(cursor, uid, polissa_id,
                                               date_from)

        date_from = "{0} 01:00:00".format(date_from)
        domain_date_from = (
            parse_date(date_from)
        ).strftime("%Y-%m-%d %H:00:00")
        date_to = "{0} 00:00:00".format(date_to)
        domain_date_to = (
            parse_date(date_to)
        ).strftime("%Y-%m-%d %H:00:00")
        start = TIMEZONE.localize(datetime.strptime(domain_date_from,
                                                    '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(domain_date_to,
                                                  '%Y-%m-%d %H:00:00'))
        ph_measures = []
        model_name = 'unknown'
        for meter in meters:
            cch_ids, model_name = compt_obj.get_curve_ids(cursor, uid, meter,
                                                          date_from, date_to)
            if cch_ids and model_name:
                curve = []
                cch_obj = self.pool.get(model_name)

                for profile in cch_obj.read(cursor, uid, cch_ids,
                                            ['ai', 'datetime', 'season']):
                    ph_measures.append(convert_to_profilehour(profile))

        profile = Profile(start, end, ph_measures)
        logger.info(
                'Profile from meter {0}: {1} ({2} - {3}) have {4} gaps'.format(
                    model_name, meter, start, end, len(profile.gaps)
                ))

        return profile.gaps, model_name

    def localize_season(self, dt, season):
        if isinstance(dt, basestring):
            dt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        dst = bool(season == 'S')
        return TIMEZONE.normalize(TIMEZONE.localize(dt, is_dst=dst))

    def convert_to_profilehour(self, measure):
        ph = ProfileHour(
            self.localize_season(measure['datetime'], measure['season']),
            measure['ai'],
            1,
            0.0
        )
        return ph

    _columns = {
        'coeficient_d': fields.float(
            u'Coeficient D €/MWh',
            digits=(4, 2),
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False), ('required', True)],
                'modcontractual': [('readonly', False), ('required', True)]
            }
        ),
        'coeficient_k': fields.float(
            u'Coeficient K €/MWh',
            digits=(4, 2),
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False), ('required', True)],
                'modcontractual': [('readonly', False), ('required', True)]
            }
        )
    }

    _defaults = {
        'coeficient_d': lambda *a: 0.0,
        'coeficient_k': lambda *a: 0.0
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'coeficient_d': fields.float(
            u'Coeficient D €/MWh',
            digits=(4, 2)
        ),
        'coeficient_k': fields.float(
            u'Coeficient K €/MWh',
            digits=(4, 2)
        )
    }

    _defaults = {
        'coeficient_d': lambda *a: 0.0,
        'coeficient_k': lambda *a: 0.0
    }

GiscedataPolissaModcontractual()
