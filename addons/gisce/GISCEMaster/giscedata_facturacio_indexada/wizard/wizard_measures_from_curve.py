# -*- coding: utf-8 -*-

from osv import osv, fields
from osv.orm import except_orm
from datetime import datetime, date, timedelta
from collections import Counter
from tools.translate import _
from dateutil.parser import parse as parse_date
from libfacturacioatr.pool.tarifes import *

from giscedata_facturacio_indexada.tarifes import TARIFFS_FACT


class WizardMeasuresFromCurve(osv.osv_memory):
    """Wizard to load a measure to meter from cch curve on a date"""

    _name = 'wizard.measures.from.curve'

    def default_info(self, cursor, uid, context=None):
        """ Get default info """
        return ''

    def default_meter(self, cursor, uid, context=None):
        """ Gets default meter from context"""
        if context is None:
            context = {}

        model = context.get('model', False)

        if not model:
            return 0

        if model == 'giscedata.lectures.comptador':
            return context.get('active_id', 0)

        return 0

    def get_contract_info(self, cursor, uid, meter_id, context=None):
        """Gets contract info from meter"""
        contract_obj = self.pool.get('giscedata.polissa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if meter_id:
            contract_id = meter_obj.read(
                cursor, uid, meter_id, ['polissa']
            )['polissa'][0]
            contract_fields = [
                'data_alta', 'cups', 'data_ultima_lectura', 'tarifa'
            ]
            contract_vals = contract_obj.read(
                cursor, uid, contract_id, contract_fields, context=context
            )
            return contract_vals

    def default_fare(self, cursor, uid, context=None):
        """ Gets default meter from context"""
        meter_id = self.default_meter(cursor, uid, context)

        if meter_id:
            return self.get_contract_info(
                cursor, uid, meter_id, context=context
            )['tarifa']

        return False


    def default_cups(self, cursor, uid, context=None):
        """Default cups from cotract"""
        meter_id = self.default_meter(cursor, uid, context)

        if meter_id:
            return self.get_contract_info(
                cursor, uid, meter_id, context=context
            )['cups'][0]

        return False

    def default_start_date(self, cursor, uid, context=None):
        """ Gets last invoiced date from contract"""
        meter_id = self.default_meter(cursor, uid, context)

        if meter_id:
            contract_info = self.get_contract_info(
                cursor, uid, meter_id, context=context
            )
            data_ul = contract_info.get('data_ultima_lectura', False)
            data_alta = contract_info.get('data_alta', False)
            if data_ul:
                return (
                    parse_date(data_ul) + timedelta(days=1)
                ).strftime('%Y-%m-%d')
            elif data_alta:
                return (parse_date(data_alta).strftime('%Y-%m-%d'))

        return False

    def default_measure_date(self, cursor, uid, context=None):
        """ Gets todays as measure date"""
        return (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")

    def default_origin(self, cursor, uid, context=None):
        """ Gets default origin from last invoiced masure"""
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_id = self.default_meter(cursor, uid, context)

        if meter_id:
            contract_vals = self.get_contract_info(
                cursor, uid, meter_id, context=context
            )

            atr_fare_id = contract_vals['tarifa'][0]
            date = (contract_vals['data_ultima_lectura']
                    or contract_vals['data_alta'])

            measures = meter_obj.get_lectures(
                cursor, uid, meter_id, atr_fare_id, date, context=context
            )

            measure = measures.values()[0]
            if (
                    len(measure.get('actual', False))
                    and measure['actual'].get('origen_id', False)
            ):
                return measure['actual']['origen_id'][0]

        return False

    def onchange_meter(self, cursor, uid, ids, meter_id, measure_date,
                       context=None):
        """ Gets last invoiced date from contract """
        if context is None:
            context = {}
        res = {'value': {}, 'warning': {}, 'error': {}}

        if meter_id and measure_date:
            res['value']['cups_id'] = self.get_contract_info(
                cursor, uid, meter_id, context=context
            )['cups'][0]
            res['value']['atr_fare_id'] = self.get_contract_info(
                cursor, uid, meter_id, context=dict(context, date=measure_date)
            )['tarifa'][0]

        res['value']['state'] = 'init'

        return res

    def onchange_measure_date(self, cursor, uid, ids, meter_id, measure_date,
                              context=None):
        """ Gets ATR fare on measure date selected
        :param meter_id: Meter id
        :param measure_date: Date of new measure
        :return: onchange dict with new ATR fare
        """
        if context is None:
            context = {}
        res = {'value': {}, 'warning': {}, 'error': {}}

        if meter_id and measure_date:
            res['value']['atr_fare_id'] = self.get_contract_info(
                cursor, uid, meter_id, context=dict(context, date=measure_date)
            )['tarifa'][0]

        res['value']['state'] = 'init'

        return res

    def load_current_measures(self, cursor, uid, meter_id, mdate, magnitude='A',
                              context=None):
        """
        Loads active measures (if exists) of meter in date and returns a dict:
        {'P1': integer , 'P2': integer ...}
        :param meter_id: Meter id
        :param mdate: date of measures
        :return: a dict of measures
        """
        if context is None:
            context = {}

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        contract_obj = self.pool.get('giscedata.polissa')

        meter_vals = meter_obj.read(cursor, uid, meter_id, ['polissa'])

        contract_id = meter_vals['polissa'][0]
        contract_date = (
            parse_date(mdate) + timedelta(days=1)
        ).strftime('%Y-%m-%d')

        ctx = dict(context, date=contract_date)
        contract_vals = contract_obj.read(
            cursor, uid, contract_id, ['tarifa'], context=ctx
        )
        atr_fare_id = contract_vals['tarifa'][0]

        measures = meter_obj.get_lectures(
            cursor, uid, meter_id, atr_fare_id, mdate, magnitude,
            context=context
        )

        periods = Counter()
        for period, measure in measures.items():
            if measure.get('actual') and measure['actual']['name'] == mdate:
                periods[period] = measure['actual']['lectura']
            else:
                txt = _(u"No s'han trobat lectures per aquesta data: "
                        u"{0}").format(mdate)
                raise except_orm('ERROR', txt)

        return periods

    def load_curve_measures(self, cursor, uid, meter_id, curve_start,
                            curve_end, context=None):
        """
        Gets curve between dates and returns Counter of total periode use in kW:
        {'P1': integer , 'P2': integer ...}
        :param meter_id: Meter id
        :param curve_start: start date of curve (included)
        :param curve_end: end date of curve (included)
        :param context:
        :return: Counter with per period consume dependint on ATR fare
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        fest_obj = self.pool.get('giscedata.dfestius')

        info_txt = ''

        contract_data = self.get_contract_info(cursor, uid, meter_id, context)
        fare_name = contract_data['tarifa'][1]

        fare_class = TARIFFS_FACT[fare_name]

        holidays_search = [
            ('name', '>=', curve_start),
            ('name', '<=', curve_end)
        ]

        holidays_vals = fest_obj.search_reader(cursor, uid, holidays_search)
        holidays = [
            datetime.strptime(d['name'], '%Y-%m-%d').date()
            for d in holidays_vals
        ]

        fare = fare_class(
            {}, {},
            curve_start, curve_end,
            data_inici_periode=curve_start,
            data_final_periode=curve_end,
            holidays=holidays
        )

        components = fare._get_components(
            parse_date(curve_start), parse_date(curve_end)
        )

        curve_start_date = parse_date(curve_start).date()
        curve_end_date = parse_date(curve_end).date()

        periods = Counter()

        for component in components:
            component_curve_start_date = max(
                curve_start_date, component.start_date.date()
            )
            component_curve_end_date = min(
                curve_end_date, component.end_date.date()
            )
            component_curve_start = component_curve_start_date.strftime(
                "%Y-%m-%d"
            )
            component_curve_end = component_curve_end_date.strftime(
                "%Y-%m-%d"
            )
            component_curve_data, curve_origin = meter_obj.get_consum_curve(
                cursor, uid, meter_id,
                component_curve_start, component_curve_end, context=context
            )

            # TODO new libfacturacioatr function
            component_curve_len = len(component_curve_data)
            # num days includes both days
            date_diff = (component_curve_end_date - component_curve_start_date)
            days = date_diff.days + 1

            info_txt += _(
                u'Llegida corba {0} de {1} dies del mes {2} \n'
            ).format(curve_origin, days, component.start_date)

            if component_curve_len != days:
                info_txt += _('ATENCIÓ: corba incorrecte')
                raise except_orm('Validate error', _(info_txt))

            component_curve = Curve(component.start_date)
            component_curve.load(
                component_curve.complete(
                    component_curve_data,
                    curve_start_date,
                    curve_end_date
                )
            )
            component_curve.get_sub_component(
                component_curve_start_date.day,
                component_curve_end_date.day,
            )

            component_periods = {}

            for period in [p.code for p in fare.periods if p.type == 'te']:
                pcomp = fare.get_period_component(
                    component.start_date, period, holidays
                )
                pcurve = (pcomp * component_curve)
                component_periods.update({period: pcurve.total_sum / 1000.0})

            info_txt += _(u"consums: {0}\n").format(dict(component_periods))
            periods += Counter(component_periods)

        return periods

    def load_measures(self, cursor, uid, ids, context=None):
        """ Calcs measures """
        if context is None:
            context = {}

        info_txt = ''

        wiz = self.browse(cursor, uid, ids[0], context)
        meter_id = wiz.meter_id.id

        curve_start = wiz.start_date
        reference_measure_date = (
            parse_date(wiz.start_date) - timedelta(days=1)
        ).strftime('%Y-%m-%d')
        curve_end = wiz.measure_date

        curve_use = self.load_curve_measures(
            cursor, uid, meter_id, curve_start, curve_end
        )

        current_measures = self.load_current_measures(
            cursor, uid, meter_id, reference_measure_date
        )

        new_measures = curve_use + current_measures

        info_txt += _(
            u"Calculat un consum total de {0} kWh entre {1} i {2}:\n"
            u"Ara pots carregar les lectures al comptador\n"
        ).format(
            sum(curve_use.values()),
            parse_date(reference_measure_date).strftime("%d/%m/%Y 24:00:00"),
            (parse_date(curve_end) + timedelta(days=1)).strftime(
                "%d/%m/%Y 00:00:00")
        )

        vals = {'state': 'validate', 'info': info_txt}
        for p in xrange(1, 7):
            key = 'p{0}'.format(p)
            vals[key] = curve_use['P{0}'.format(p)]
            key = 'current_measure{0}'.format(p)
            vals[key] = current_measures['P{0}'.format(p)]
            key = 'new_measure{0}'.format(p)
            vals[key] = new_measures['P{0}'.format(p)]
        wiz.write(vals)

    def create_measures(self, cursor, uid, ids, context=None):
        """ Inserts measures in meter """
        if context is None:
            context = {}

        fare_obj = self.pool.get('giscedata.polissa.tarifa')
        period_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        retail_origin_obj = self.pool.get('giscedata.lectures.origen_comer')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        info_txt = ''

        wiz = self.browse(cursor, uid, ids[0], context)

        insert_reactive = wiz.insert_reactive
        insert_maximeter = wiz.insert_maximeters

        meter = (wiz.meter_id.id, wiz.meter_id.name)
        meter_id = meter[0]
        measure_date = wiz.measure_date
        origin = (wiz.measure_origin.id, wiz.measure_origin.name)
        retail_origin_id = retail_origin_obj.get_origin_by_code(
            cursor, uid, 'CC', context=context
        )
        retail_origin = (retail_origin_id, 'CCH')

        contract_info = self.get_contract_info(
            cursor, uid, meter_id,
            context=dict(context, date=measure_date)
        )

        atr_fare_id = contract_info['tarifa'][0]
        periods_energy_product = fare_obj.get_periodes(
            cursor, uid, atr_fare_id, 'te', context=context
        )

        start_date = wiz.start_date

        comments = _(u"Lectures CCH des de {0} ").format(start_date)

        measures = []
        maximeters = []

        if insert_reactive:
            reactive_current_measures = self.load_current_measures(
                cursor, uid, meter_id, start_date, magnitude='R',
                context=context
            )

        for p, v in periods_energy_product.items():
            measure_val = getattr(wiz, 'new_measure{0}'.format(p.lower()[1]))

            measure = {
                'name': measure_date,
                'comptador': meter,
                'tipus': ('A', 'active'),
                'periode': (v, p),
                'lectura': measure_val,
                'origen_id': origin,
                'origen_comer_id': retail_origin,
                'observacions': '',
                'incidencia_id': False,
            }

            measures.append(measure)

            if insert_reactive:
                period_id = measure['periode'][0]
                reactive_affected = period_obj.read(
                    cursor, uid, period_id, ['efecte_reactiva']
                )['efecte_reactiva']
                if reactive_affected:
                    measure_reactive = dict(measure)
                    measure_reactive.update(
                        {
                            'tipus': ('R', 'reactive'),
                            'lectura': reactive_current_measures[p]
                        }
                    )

                measures.append(measure_reactive)


        if insert_maximeter:
            product_type = 'tp'

            periods_power_product = fare_obj.get_periodes(
                cursor, uid, atr_fare_id, product_type, context=context
            )

            for p, v in periods_power_product.items():
                maximeter = {
                    'name': measure_date,
                    'comptador': meter,
                    'periode': (v, p),
                    'exces': 0,
                    'lectura': 0,
                    'origen_comer_id': retail_origin,
                    'observacions': '',
                    'incidencia_id': False,
                }
                maximeters.append(maximeter)

        res = meter_obj.inserta_lectures(
            cursor, uid, measures, maximeters, prefix_observacions=comments,
            context=context
        )

        info_txt += _(u"S'han carregat {lectures} lectures i "
                      u"{maxims} maximetres\n").format(**res)

        wiz.write({'info': info_txt})

        return True

    _columns = {
        'state': fields.char('init', size=16),
        'info': fields.text('Informació', readonly=True),
        'meter_id': fields.many2one(
            'giscedata.lectures.comptador', 'Comptador', required=True
        ),
        'cups_id': fields.many2one(
            'giscedata.cups.ps', 'CUPS', readonly=True,
        ),
        'atr_fare_id': fields.many2one(
            'giscedata.polissa.tarifa', 'Tarifa', readonly=True
        ),
        'start_date': fields.date('Data inicial', required=True),
        'measure_date': fields.date('Data de la mesura', required=True),
        'measure_origin': fields.many2one(
            'giscedata.lectures.origen', 'Origen', required=True
        ),
        'current_measure1': fields.integer('P1 actual'),
        'current_measure2': fields.integer('P2 actual'),
        'current_measure3': fields.integer('P3 actual'),
        'current_measure4': fields.integer('P4 actual'),
        'current_measure5': fields.integer('P5 actual'),
        'current_measure6': fields.integer('P6 actual'),
        'p1': fields.integer('P1'),
        'p2': fields.integer('P2'),
        'p3': fields.integer('P3'),
        'p4': fields.integer('P4'),
        'p5': fields.integer('P5'),
        'p6': fields.integer('P6'),
        'new_measure1': fields.integer('Lectura P1'),
        'new_measure2': fields.integer('Lectura P2'),
        'new_measure3': fields.integer('Lectura P3'),
        'new_measure4': fields.integer('Lectura P4'),
        'new_measure5': fields.integer('Lectura P5'),
        'new_measure6': fields.integer('Lectura P6'),
        'insert_reactive': fields.boolean(
            'Inserta lectures de reactiva',
            help=u'Inserta les lectures de reactiva amb la mateixa '
                 u'lectura actual'
        ),
        'insert_maximeters': fields.boolean(
            'Inserta lectures de potència',
            help=u'Inserta les lectures de maxímetres amb valor 0'
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': default_info,
        'start_date': default_start_date,
        'measure_date': default_measure_date,
        'meter_id': default_meter,
        'cups_id': default_cups,
        'atr_fare_id': default_fare,
        'insert_reactive': lambda *a: True,
        'insert_maximeters': lambda *a: True,
        'measure_origin': default_origin,
    }

WizardMeasuresFromCurve()
