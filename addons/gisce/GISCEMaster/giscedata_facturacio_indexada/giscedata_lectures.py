# -*- coding: utf-8 -*-
from datetime import date, datetime, timedelta, time

from osv import osv
from addons import get_module_resource
from tools.translate import _

from enerdata.profiles.profile import *
from enerdata.metering.measure import *
from .tarifes import TARIFFS, PERFS_COF, TARIFFS_FACT
from .profile import REEInitalProfile, adapt_measures
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse as parse_date


# Conversion to Wh depending on model
CURVE_TO_Wh = {
    'tg.cchfact': 1,
    'tg.cchval': 1,
    'tg.cchcons': 1000,
    'tg.f1': 1000,
    'tg.p1': 1000,
}


INVOICE_WITH_CURVE = ['index']

def array_padding(array, length, value=0):
    return array + ([value] * (length - len(array)))


class GiscedataLecturesComptador(osv.osv):
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_curve_ids(self, cursor, uid, meter_id, date_from=None,
                      date_to=None, context=None):
        contract_obj = self.pool.get('giscedata.polissa')

        model_name = False
        meter_vals = self.read(
            cursor, uid, meter_id, ['polissa'], context=context
        )
        if not meter_vals:
            raise osv.except_osv(
                'Error',
                _(u"No s'ha trobat la polissa pel comptador amb id "
                  u"{0}").format(meter_id)
            )

        # Curve dates
        # from: date_from at 01:00
        # to: date after date_to at 00:00
        domain_date_from = (
            parse_date(date_from)
        ).strftime("%Y-%m-%d 00:00:00")
        domain_date_to = (
            parse_date(date_to) + relativedelta(days=1)
        ).strftime("%Y-%m-%d 01:00:00")

        contract_id = meter_vals['polissa'][0]
        contract_vals = contract_obj.read(
            cursor, uid, contract_id, ['cups'], context=context
        )
        if not contract_vals:
            raise osv.except_osv(
                'Error',
                _(u"No s'ha trobat la cups per la pólissa amb id"
                  u"{0}").format(contract_id)
            )

        cups_name = contract_vals['cups'][1]

        search_vals = []
        if date_from:
            search_vals.append(('datetime', '>', domain_date_from))
        if date_to:
            search_vals.append(('datetime', '<', domain_date_to))

        for model_name in ['tg.f1', 'tg.p1', 'tg.cchfact', 'tg.cchval',
                           'tg.cchcons']:
            cch_obj = self.pool.get(model_name)

            cch_cups_name = cch_obj.get_curve_cups(cursor, uid, cups_name)
            cch_search_vals = [('name', '=', cch_cups_name)]
            cch_search_vals.extend(search_vals)

            cch_ids = cch_obj.search(
                cursor, uid,
                cch_search_vals, order='datetime asc, season desc',
                context=context
            )
            if cch_ids:
                break

        return cch_ids, model_name

    def get_consum_curve(self, cursor, uid, meter_id, date_from=None,
                         date_to=None, context=None):
        """ Search better curve (F5D, P5D or CCH CONS in this order)
        and returns the correct curve format between dates:
         * date_from included
         * date_to included
         * Raises an error if not found any complete curve
        :param meter_id: Id of Meter to get the consume from
        :param date_from: start date of the curve at 01:00:00
        :param date_to: En date of the curve at 24:00:00

        :return:
        (curve, origin)
          origin: One of 'F1', 'P1', 'CCHFACT', 'CCHVAL','CCHCONS'
          curve: The curve in one record per hour and sorted from
        """

        cch_ids, model_name = self.get_curve_ids(cursor, uid, meter_id,
                                                 date_from, date_to, context)
        origin = model_name.upper().split('.')[1]
        curve = []
        if cch_ids and model_name:
            cur_date = ''
            tz_offset = 0
            cur_season = ''
            current_day = []
            cch_obj = self.pool.get(model_name)
            empty_day = [0] * 25
            # Factor conver to Wh
            factor = CURVE_TO_Wh.get(model_name, 1)
            for cch_id in cch_ids:
                cch_vals = cch_obj.read(
                    cursor, uid, cch_id, ['ai', 'datetime', 'season']
                )
                cch_datetime = parse_date(cch_vals['datetime'])
                hour = cch_datetime.hour
                # hour 0 is 24 of previous day
                if hour == 0:
                    cch_datetime -= relativedelta(days=1)
                    hour = 24
                cch_date = cch_datetime.date().strftime('%Y-%m-%d')
                if cch_date != cur_date:
                    # date change
                    tz_offset = 0
                    cur_date = cch_date
                    if current_day:
                        curve.append(current_day)
                    cur_season = cch_vals['season']
                    current_day = empty_day[:]
                if cur_season != cch_vals['season']:
                    # season change
                    if cur_season == 0:
                        tz_offset = -1
                    else:
                        tz_offset = +1
                # 0 indexed day array
                measure_position = hour + tz_offset - 1
                current_day[measure_position] = cch_vals['ai'] * factor
                # print cch_vals
            if current_day:
                curve.append(current_day)
            # we have curve
            return curve, origin
        return curve, origin

    def get_consum_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                tipus='A', periodes=None, context=None):
        if context is None:
            context = {}
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        polissa_obj = self.pool.get('giscedata.polissa')

        parent = super(GiscedataLecturesComptador, self).get_consum_per_facturar
        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]
        compt = self.browse(cursor, uid, comptador_id)
        polissa_id = compt.polissa.id
        pol_ctx = context.copy()
        if 'fins_lectura_fact' in context:
            pol_ctx['date'] = context.get('fins_lectura_fact')
        if compt.data_baixa:
            mod_date = pol_ctx.get('date', False)
            if mod_date:
                mod_date = min(mod_date, compt.data_baixa)
            else:
                mod_date = compt.data_baixa
            pol_ctx['date'] = mod_date
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context=pol_ctx)
        if polissa.mode_facturacio in INVOICE_WITH_CURVE and tipus == 'A':
            lectures = self.get_lectures_per_facturar(
                cursor, uid, comptador_id, tarifa_id, tipus, periodes, context
            )
            start = '9999-99-99'
            end = ''
            for p, v in lectures.items():
                actual_date = v['actual'].get('name', False)
                # No measures protection
                if not actual_date:
                    return {start: 0}
                end = max(end, actual_date)
                start = min(start, v['anterior']['name'])
            if start > end:
                raise osv.except_osv('Error')

            # Si estem en les lectures inicials de la polissa el dia de la
            # lectura pot ser tant la data d'alta (antic criteri, data inclosa)
            # com el dia anterior a l'alta (nou criteri, data no inclosa).
            # Si estem en el nou criteri la data no es inclosa peró en les
            # curves es trevalla sempre amb data inicial inclosa, per tant
            # haurém de sumar un dia a la data d'inici per reflexar-ho
            dalta = datetime.strptime(polissa.data_alta, "%Y-%m-%d")
            dalta = (dalta - timedelta(days=1)).strftime("%Y-%m-%d")
            if dalta == start:
                start = datetime.strptime(start, "%Y-%m-%d")
                start = (start + timedelta(days=1)).strftime("%Y-%m-%d")

            tarifa = tarifa_obj.browse(cursor, uid, tarifa_id)

            # Try to find de curve
            dates_ult_set = (
                polissa.data_ultima_lectura,
                context.get('ult_lectura_fact', '')
            )
            curve_start = start
            curve_end = end
            if start in dates_ult_set:
                # Hem d'agafar la curva només dels dies que facturem
                # no els ja facturats
                curve_start = (
                    datetime.strptime(start, '%Y-%m-%d')
                    + timedelta(days=1)
                ).strftime('%Y-%m-%d')

            fare = TARIFFS_FACT[tarifa.name](
                {}, {},
                curve_start, curve_end,
                data_inici_periode=curve_start,
                data_final_periode=curve_end,
                holidays=[]
            )

            curve_start_date = parse_date(curve_start).date()
            curve_end_date = parse_date(curve_end).date()

            components = fare._get_components(
                parse_date(curve_start),
                parse_date(curve_end)
            )

            curve_ok = False
            res = {}
            for component in components:
                component_curve_start_date = max(
                    curve_start_date, component.start_date.date()
                )
                component_curve_end_date = min(
                    curve_end_date, component.end_date.date()
                )
                component_curve_start = component_curve_start_date.strftime(
                    "%Y-%m-%d"
                )
                component_curve_end = component_curve_end_date.strftime(
                    "%Y-%m-%d"
                )
                curve, origin = self.get_consum_curve(
                    cursor, uid, comptador_id,
                    component_curve_start, component_curve_end,
                    context=context
                )

                if curve:
                    curve_ok = True
                    key_str = (
                        component_curve_start_date + relativedelta(day=1)
                    ).strftime('%Y-%m-%d')
                    res.update({
                        'origin': origin,
                        key_str: {
                            'profile': curve,
                            'start': datetime.combine(
                                component_curve_start_date, datetime.min.time()
                            ),
                            'end': datetime.combine(
                                component_curve_end_date, datetime.min.time()
                            ),
                        }
                    })
                else:
                    curve_ok = False
                    break

            if curve_ok:
                return res

            # Some fare's may not be profiled
            not_profiled = (
                tarifa.tipus == 'AT'
                and tarifa.codi_ocsum != '011'
            )
            if not_profiled and not curve_ok:
                # Can NOT be profiled
                raise osv.except_osv(
                    'Error',
                    _(u"No es pot facturar la pòlissa {0} sense curva per "
                      u"la tarifa {1}").format(
                        polissa.name, polissa.tarifa.name
                    )
                )

            # When no curve, create curve profiling
            t = TARIFFS[tarifa.name]()
            t.cof = PERFS_COF[tarifa.name]

            last_invoiced_measure = (
                polissa.data_ultima_lectura,
                context.get('ult_lectura_fact', ''),
                dalta
            )
            m_dict = adapt_measures(
                lectures, start, end, last_invoiced_measure, compt.data_alta
            )
            measures = m_dict['measures']
            curve_start = m_dict['start']
            curve_end = m_dict['end']

            c = self.get_cofs(polissa, curve_start, curve_end)
            profiler = Profiler(c)

            consums = {}
            drag = self.get_drag_method(polissa)
            for hour, vals in profiler.profile(t, measures, drag_method=drag):
                d = hour.date()
                # Hacky patch to work for both systems REE Style number of hours
                # and Esios style datetimes
                first_hour = getattr(profiler.coefficient, 'first_hour', 1)
                if first_hour == 1:
                    if hour.hour == 0:
                        d -= timedelta(days=1)
                consums.setdefault(d, [])
                consums[d].append(vals['aprox'])
            monthly = {}
            for d, hours in sorted(consums.items()):
                key = d.strftime('%Y-%m-01')
                monthly.setdefault(key, {
                    'start': curve_start, 'end': curve_end, 'profile': []
                })
                monthly[key]['profile'].append(array_padding(hours, 25))
            # dates in month
            for k in monthly.keys():
                key = datetime.strptime(k, '%Y-%m-%d')
                # Last day current month
                ldcm = key + relativedelta(months=1, day=1) - timedelta(days=1)
                monthly[k]['start'] = max(monthly[k]['start'], key)
                monthly[k]['end'] = min(monthly[k]['end'], ldcm)
            monthly.update({'origin': 'REEPROFILE'})
            return monthly

        else:
            return parent(cursor, uid, comptador_id, tarifa_id, tipus, periodes,
                          context)

    def get_drag_method(self, polissa):
        return 'period'

    def get_cofs(self, polissa, start, end):
        c = Coefficients()

        while start <= end:
            try:
                cofs = REEProfile.get(start.year, start.month)
            except Exception:
                coef_path = get_module_resource(
                    'giscedata_facturacio_indexada',
                    'coeffs',
                    'profiles_{0}.csv'.format(start.year)
                )
                cofs = REEInitalProfile(coef_path).get(
                    start.year, start.month
                )
            c.insert_coefs(cofs)
            # Add one month but always day 1 to avoid month skip, ie
            # 2015-11-04 > 2015-11-02
            start += relativedelta(months=1, day=1)

        return c

GiscedataLecturesComptador()
