# -*- coding: utf-8 -*-
{
    "name": "Titular al llistat de CUPS",
    "description": """Afegeix el Titular al llistat de CUPS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cups",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_ps_view.xml"
    ],
    "active": False,
    "installable": True
}
