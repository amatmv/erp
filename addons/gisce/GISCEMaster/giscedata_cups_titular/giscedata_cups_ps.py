# -*- coding: utf-8 -*-

"""Ampliem CUPS amb el camp titular"""

from osv import osv, fields


class GiscedataCupsPs(osv.osv):
    """Extensió de CUPS amb el titular.
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def _ff_get_titular(self, cr, uid, ids, field, arg, context=None):
        """ Anem a buscar el titular de la pólissa assignada (si en té) """
        res = {}
        if not context:
            context = {}
        cups_obj = self.pool.get('giscedata.cups.ps')
        cupses = cups_obj.browse(cr, uid, ids)
        for c in cupses:
            res[c.id] = (c.polissa_polissa and c.polissa_polissa.titular.name
                          or False)
        return res

    def _titular_search(self, cr, uid, obj, field_name, arg, context=None):
        if not arg:
            return []
        else:
            polissa_obj = self.pool.get('giscedata.polissa')
            partner_obj = self.pool.get('res.partner')
            titulars = partner_obj.search(cr, uid,
                [('name', arg[0][1], arg[0][2])])
            pol_ids = polissa_obj.search(cr, uid,
                [('titular', 'in', titulars)])
            polisses = polissa_obj.browse(cr, uid, pol_ids)
            pol_names = []
            for p in polisses:
                pol_names.append(p.name)
            return [('polissa_polissa', 'in', pol_names)]

    _columns = {
        'titular': fields.function(_ff_get_titular, string='Titular',
                    fnct_search=_titular_search,
                    method=True, size='128', type='char')
    }

GiscedataCupsPs()
