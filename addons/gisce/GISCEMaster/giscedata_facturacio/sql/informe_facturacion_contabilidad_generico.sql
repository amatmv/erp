select
	distri.name as "Distribuidora",
	comer.name as "Comercializadora",
    pol.name as "Contrato",
    replace(tit.name,'"', '') as "Nombre cliente",
    tit.id AS "id_client",
	COALESCE(tit.vat, '') as "VAT/NIF",
    i.number AS "Factura",
    i.id AS "id_invoice",
    i.date_invoice AS "Fecha factura",
    f.data_inici AS "Fecha inicio",
    f.data_final AS "Fecha final",
    t.name AS "Tarifa de acceso",
    pricelist.name AS "Lista de precios",
    CASE
        WHEN i.type like '%%refund' THEN -COALESCE(ene.energia, 0.0)
        ELSE COALESCE(ene.energia, 0.0)
    END AS "Energia kWh",
	CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN -COALESCE(f.total_potencia,0.0)
		ELSE COALESCE(f.total_potencia,0.0)
	END AS "Importe potencia sin descuento",
	CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN -COALESCE(f.total_energia, 0.0)
        ELSE COALESCE(f.total_energia, 0.0)
    END AS "Importe energia sin descuento",

    %(select_descuento_energia)s
    %(select_descuento_potencia)s
    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN -COALESCE(f.total_reactiva,0.0)
		ELSE COALESCE(f.total_reactiva,0.0)
	END AS "Importe reactiva",
    CASE
      WHEN i.type = 'out_refund' THEN COALESCE(excesos.suma * -1, 0.0)
      ELSE COALESCE(excesos.suma, 0.0)
    END AS "importe exceso potencia",
	CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN -COALESCE(altres.altres, 0.0)
		ELSE COALESCE(altres.altres,0.0)
	END AS "Otros conceptos",
    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN -COALESCE(f.total_lloguers, 0.0)
        ELSE COALESCE(f.total_lloguers, 0.0)
    END AS "Importe alquiler",
    COALESCE(fianza.fianza, 0.0) * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END) AS fianza,
    CASE
        WHEN i.type like '%%refund' THEN -COALESCE(it_iese.base,0)
        ELSE COALESCE(it_iese.base,0.0)
    END AS "Base IESE"
    , CASE
        WHEN i.type like '%%refund' THEN -COALESCE(it_iese.amount,0.0)
        ELSE COALESCE(it_iese.amount,0.0)
    END AS "Cuota IESE",



    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-ig_3.base,NULL)
        ELSE COALESCE(ig_3.base,NULL)
    END AS "Base IGIC 3%%",

    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-ig_7.base,NULL)
        ELSE COALESCE(ig_7.base,NULL)
    END AS "Base IGIC 7%%",

    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-iv_21.base,NULL)
        ELSE COALESCE(iv_21.base,NULL)
    END AS "Base IVA",

    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-ig_3.amount,NULL)
        ELSE COALESCE(ig_3.amount,NULL)
    END AS "cuota IGIC 3%%",
    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-ig_7.amount,NULL)
        ELSE COALESCE(ig_7.amount,NULL)
    END AS "cuota IGIC 7%%",

    CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-iv_21.amount,NULL)
        ELSE COALESCE(iv_21.amount,NULL)
    END AS "cuota IVA",


    CASE
        WHEN i.type like '%%refund' THEN -i.amount_total
        ELSE i.amount_total
    END AS "Total Factura",
	CASE
        WHEN i.type like '%%refund' THEN -i.residual
        ELSE i.residual
    END AS "Residual"

FROM account_invoice i
LEFT JOIN giscedata_facturacio_factura f ON (i.id=f.invoice_id)
LEFT JOIN giscedata_cups_ps c ON (f.cups_id=c.id)
LEFT JOIN giscedata_polissa pol ON (f.polissa_id=pol.id)
LEFT JOIN giscedata_polissa_tarifa t ON (t.id=f.tarifa_acces_id)

%(join_energia)s

LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
  FROM giscedata_facturacio_factura_linia lf
  LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
  WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%'
  GROUP BY lf.factura_id
) AS refact ON (refact.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
  FROM giscedata_facturacio_factura_linia lf
  LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
  WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T4%%'
  GROUP BY lf.factura_id
) AS refact_T4 ON (refact_T4.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS refacturacio
  FROM giscedata_facturacio_factura_linia lf
  LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
  WHERE lf.tipus='altres' AND li.name LIKE 'Refact%%T1%%'
  GROUP BY lf.factura_id
) AS refact_T1 ON (refact_T1.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS altres
  FROM giscedata_facturacio_factura_linia lf
  LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
  WHERE lf.tipus='altres'
  GROUP BY lf.factura_id
) AS altres ON (altres.factura_id=f.id)
LEFT JOIN (
  SELECT lf.factura_id AS factura_id
  , SUM(li.price_subtotal) AS base
  , ROUND(SUM(li.price_subtotal) * 1.051130 * 0.04864,2) AS amount
  FROM giscedata_facturacio_factura_linia lf
  LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
  LEFT JOIN account_invoice_line_tax it_18 ON (li.id=it_18.invoice_line_id)
  LEFT JOIN account_tax t_18 ON (it_18.tax_id=t_18.id AND t_18.name LIKE 'IVA 18%%')
  LEFT JOIN account_invoice_line_tax iese ON (li.id=iese.invoice_line_id )
  LEFT JOIN account_tax t_iese ON (iese.tax_id=t_iese.id AND t_iese.name LIKE '%%electricidad%%')
  WHERE t_iese.id IS NOT NULL and t_18.id IS NOT NULL
  GROUP BY lf.factura_id
) AS iese_18 ON (iese_18.factura_id=f.id)
LEFT JOIN account_invoice_tax it_18 ON (it_18.invoice_id=i.id AND it_18.name LIKE 'IVA 18%%')
LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=i.id AND it_21.name LIKE 'IVA 21%%')
LEFT JOIN account_invoice_tax it_iese ON (it_iese.invoice_id=i.id AND it_iese.name LIKE '%%electricidad%%')
LEFT JOIN res_partner d ON (d.id=c.distribuidora_id)
LEFT JOIN res_partner p ON (p.id=i.company_id)
LEFT JOIN res_partner tit on (tit.id = i.partner_id)
LEFT JOIN payment_type pt on (pol.tipo_pago = pt.id)
LEFT JOIN giscedata_polissa polissa ON (f.polissa_id = polissa.id)
LEFT JOIN res_partner comer on comer.id = polissa.comercialitzadora
LEFT JOIN res_partner distri on distri.id = polissa.distribuidora
LEFT JOIN payment_order po ON (i.payment_order_id = po.id)
LEFT JOIN account_journal j ON (j.id = i.journal_id)
LEFT JOIN ir_sequence s ON (j.invoice_sequence_id = s.id)
LEFT JOIN (
  SELECT il.invoice_id AS inv_id, SUM(il.price_subtotal) AS base_costs, SUM(il.price_subtotal * it_21.amount) AS iva_costs
  FROM account_invoice_line il
  LEFT JOIN account_invoice_line_tax lt ON (lt.invoice_line_id = il.id)
  LEFT JOIN account_tax it_21 ON (lt.tax_id=it_21.id AND it_21.name LIKE 'IVA 21%%')
  LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.invoice_line_id = il.id)
  LEFT JOIN product_product pp ON (il.product_id = pp.id)
  LEFT JOIN product_template pt ON (pp.product_tmpl_id = pt.id)
  WHERE pt.name = 'Despeses devolució'
    AND fl.tipus = 'altres'
    AND it_21.id IS NOT NULL
  GROUP BY il.invoice_id, it_21.amount
) devolution_costs ON (devolution_costs.inv_id = i.id)
LEFT JOIN (
  SELECT il.invoice_id AS inv_id, SUM(il.price_subtotal) AS base_costs, 0 AS iva_costs
  FROM account_invoice_line il
  LEFT JOIN account_invoice_line_tax lt ON (lt.invoice_line_id = il.id)
  LEFT JOIN account_tax it_21 ON (lt.tax_id=it_21.id)
  LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.invoice_line_id = il.id)
  WHERE it_21.id IS NULL
    AND fl.tipus = 'altres'
  GROUP BY il.invoice_id, it_21.amount
) no_iva_concepts ON (no_iva_concepts.inv_id = i.id)
LEFT JOIN (
  SELECT il.invoice_id AS inv_id, SUM(il.price_subtotal) AS base_costs, SUM(il.price_subtotal * it_21.amount) AS iva_costs
  FROM account_invoice_line il
  LEFT JOIN account_invoice_line_tax lt ON (lt.invoice_line_id = il.id)
  LEFT JOIN account_tax it_21 ON (lt.tax_id=it_21.id AND it_21.name LIKE 'IVA 21%%')
  LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.invoice_line_id = il.id)
  LEFT JOIN product_product pp ON (il.product_id = pp.id)
  LEFT JOIN product_template pt ON (pp.product_tmpl_id = pt.id)
  WHERE it_21.id IS NOT NULL
    AND pt.name != 'Despeses devolució'
    AND fl.tipus = 'altres'
  GROUP BY il.invoice_id, it_21.amount
) iva_concepts ON (iva_concepts.inv_id = i.id)
LEFT JOIN account_fiscal_position fp ON (i.fiscal_position = fp.id)
LEFT JOIN product_pricelist AS pricelist ON (f.llista_preu = pricelist.id)
LEFT JOIN (
  SELECT li.invoice_id AS invoice_id, SUM(li.price_subtotal) AS fianza
  FROM account_invoice_line li
  JOIN product_product p ON (li.product_id = p.id)
  WHERE p.default_code in ('CON06', 'FIANZA')
  GROUP BY li.invoice_id
) AS fianza ON (fianza.invoice_id = i.id)

%(joins_descuentos)s

LEFT JOIN account_invoice_tax ig_3 ON (ig_3.invoice_id=i.id AND ig_3.name LIKE 'IGIC 3%%')
LEFT JOIN account_invoice_tax ig_7 ON (ig_7.invoice_id=i.id AND (ig_7.name LIKE 'IGIC 7%%'))
LEFT JOIN account_invoice_tax iv_21 ON
    (iv_21.invoice_id=i.id
    AND
    (iv_21.name LIKE 'IVA 21%%' OR iv_21.name LIKE '21%%%% IVA%%')
)
LEFT JOIN
  (
    SELECT gffl.factura_id,gffl.tipus, sum(ail.price_subtotal) AS suma
    FROM giscedata_facturacio_factura_linia gffl
    LEFT JOIN account_invoice_line ail ON gffl.invoice_line_id = ail.id
    WHERE gffl.tipus = 'exces_potencia'
    GROUP BY gffl.factura_id,gffl.tipus
  ) AS excesos ON f.id = excesos.factura_id

WHERE
  i.state not in %(states)s
  AND i.date_invoice >= %(dinici_inv)s
  AND i.date_invoice <= %(dfinal_inv)s
  AND (f.facturacio in %(tipo_facturacion)s OR f.id is null)
  AND s.code IN %(serie_codes)s
ORDER BY pol.name