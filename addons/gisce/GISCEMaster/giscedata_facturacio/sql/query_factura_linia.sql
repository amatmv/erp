SELECT
	fl.id
FROM giscedata_facturacio_factura_linia fl
INNER JOIN account_invoice_line il
ON il.id = fl.invoice_line_id
WHERE
	il.name = %s
	AND fl.factura_id = %s
	AND fl.tipus = %s
	AND fl.price_unit_multi::text = %s
	AND coalesce(il.product_id, 0) = %s
