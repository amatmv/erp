SELECT
  f.polissa_id,
  max(data_actual) as data_ultima_lectura
FROM giscedata_facturacio_factura f
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN account_journal journal
  ON journal.id = i.journal_id
INNER JOIN giscedata_facturacio_lectures_energia le
  ON le.factura_id = f.id
WHERE journal.code in ('ENERGIA', 'ENERGIA.R')
  AND i.type = 'out_invoice'
  AND f.id not in
    (SELECT distinct ref from giscedata_facturacio_factura f
    inner join account_invoice i on i.id = f.invoice_id
    WHERE coalesce(f.ref, 0) <> 0 and i.type = 'out_refund'
    AND i.state in ('open', 'paid'))
  AND i.state in ('open', 'paid')
  AND f.polissa_id in %s
GROUP BY f.polissa_id
