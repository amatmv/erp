SELECT
    i.number AS factura,
    pol.name AS poliza,
    li.name AS nombre_producto,
--    Added coalesce with line name, because some types of invoice
--        has lines with out product or products without code
    COALESCE(product.default_code, li.name) AS codigo_producto,
    SUM(li.price_subtotal * (CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END)) AS importe_producto
FROM account_invoice i
LEFT JOIN giscedata_facturacio_factura AS f ON (i.id = f.invoice_id)
LEFT JOIN giscedata_polissa AS pol ON (f.polissa_id = pol.id)
INNER JOIN account_journal AS journal ON journal.id = i.journal_id
INNER JOIN ir_sequence AS serie ON serie.id = journal.invoice_sequence_id
INNER JOIN account_invoice_line AS li ON (li.invoice_id = i.id)
LEFT JOIN giscedata_facturacio_factura_linia AS lf ON (li.id = lf.invoice_line_id)
LEFT JOIN product_product AS product on (product.id = li.product_id)
LEFT JOIN product_template AS pro_tmp on (pro_tmp.id = product.product_tmpl_id)
WHERE
    i.type in ('out_invoice', 'out_refund')
    AND (f.facturacio IN %(tipo_facturacion)s OR f.id is null)
    AND i.date_invoice between to_date(%(dinici_inv)s, 'YYYY-MM-DD') and to_date(%(dfinal_inv)s, 'YYYY-MM-DD')
    AND i.state not IN %(states)s
    AND serie.code IN %(serie_codes)s
    AND (lf.tipus='altres' OR lf.id is null)
GROUP BY nombre_producto, codigo_producto, factura, poliza
ORDER BY factura
