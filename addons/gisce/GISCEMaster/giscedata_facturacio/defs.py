# -*- coding: utf-8 -*-
from switching.defs import TABLA_106
# Definició de variables de mòdul
MAGNITUD_SELECTION = [('AE', 'Activa Entrant'),
                      ('AS', 'Activa Sortint'),
                      ('R1', 'Reactiva Q1'),
                      ('R2', 'Reactiva Q2'),
                      ('R3', 'Reactiva Q3'),
                      ('R4', 'Reactiva Q4')]

FACTURACIO_SELECTION = [
    (1, 'Mensual'),
    (2, 'Bimestral'),
]

TIPO_FACTURA_SELECTION = [('01', 'Normal'),
                          ('02', 'Modificación de Contrato'),
                          ('03', 'Baja de Contrato'),
                          ('04', 'Derechos de Contratacion'),
                          ('05', 'Deposito de garantía'),
                          ('06', 'Inspección - Anomalia'),
                          ('07', 'Atenciones (verificaciones, )'),
                          ('08', 'Indemnizacion'),
                          ('09', 'Intereses de demora'),
                          ('10', 'Servicios'),
                          ('11', 'Inspección - Fraude')]

TIPO_RECTIFICADORA_SELECTION = [('N', 'Normal'),
                                ('R', 'Rectificadora'),
                                ('A', 'Anuladora'),
                                ('B', 'Anuladora con sustituyente'),
                                ('C', 'Complementaria'),
                                ('G', 'Regularizadora'),
                                ('RA', 'Rectificadora sin anuladora'),
                                ('BRA', 'Anuladora (ficticia)')]

REFUND_RECTIFICATIVE_TYPES = ['A', 'B', 'RA']

RECTIFYING_RECTIFICATIVE_TYPES = ['R', 'RA']

SIGN = {'N': 1, 'R': 1, 'A': -1, 'B': -1, 'RA': 1, 'BRA': -1, 'G': 1, 'C': 1}

TIPO_FACTURACION_SELECTION = [('1','Regular (Periodo completo)'),
                              ('2','Irregular (Periodo incompleto)')]

TIPUS_SELECTION = [('activa', 'Activa'),
                   ('reactiva', 'Reactiva')]

MOTIUS_AJUST = TABLA_106

RECTIFICATIVE_FROM_F1_TO_DB = {
    'R': 'RA',
}

TRANSLATE_ORIGIN_TO_NEW_F1 = {
    '11': '10',
    '21': '20',
    '31': '30'
}
