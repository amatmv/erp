# -*- coding: utf-8 -*-
"""Classes de facturació relatives a les pòlisses"""

# pylint: disable-msg=E1101,W0223
from __future__ import absolute_import
from osv import osv, fields
from datetime import datetime, timedelta
import calendar
from tools.translate import _
from tools import config
from giscedata_facturacio.defs import FACTURACIO_SELECTION
from giscedata_polissa.giscedata_polissa import (
    CONTRACT_IGNORED_STATES
)
from giscedata_facturacio.giscedata_facturacio import (
    REFUND_RECTIFICATIVE_INVOICE_TYPES
)
from ast import literal_eval

NOTIFICACIO_SELECTION = [('titular', 'Titular'),
                         ('pagador', 'Fiscal'),
                         ('altre_p', 'Altra')]

FACTURACIO_POTENCIA_SELECTION = [('max', 'Maxímetro'),
                                 ('icp', 'ICP'),
                                 ('recarrec', 'Recàrrec ICP')]

INTERVAL_INVOICING_FIELDS = [
    'potencia', 'facturacio_potencia', 'tarifa', 'potencies_periode', 'autoconsumo'
]


def _get_polissa_from_invoice(self, cursor, uid, ids, context=None):
    """Returns ids of polissa in invoice passed as ids"""

    factura_obj = self.pool.get('giscedata.facturacio.factura')
    #Search factura associated to invoices in ids
    query = '''
        SELECT f.id
        FROM giscedata_facturacio_factura f
        INNER JOIN account_invoice i
        ON i.id = f.invoice_id
        WHERE i.id in %s
        AND i.type in ('out_invoice', 'out_refund')
        AND i.state in ('open', 'paid', 'cancel')
        '''
    cursor.execute(query, (tuple(ids),))
    factura_ids = [x[0] for x in cursor.fetchall()]
    vals = factura_obj.read(cursor, uid, factura_ids, ['polissa_id'])
    return [val['polissa_id'][0] for val in vals if val['polissa_id']]


def _get_polissa_from_energy_invoice(self, cursor, uid, ids, context=None):
    """Returns ids of polissa in invoice passed as ids"""

    factura_obj = self.pool.get('giscedata.facturacio.factura')
    # Search factura associated to invoices in ids
    from osv.expression import OOQuery
    q = OOQuery(factura_obj, cursor, uid)
    sql = q.select(['polissa_id']).where([
        ('type', 'in', ['out_invoice', 'out_refund']),
        ('state', 'in', ['open', 'paid']),
        ('invoice_id.journal_id.code', '=like', 'ENERGIA%'),
        ('tipo_factura', '=', '01'),
        ('invoice_id', 'in', tuple(ids))
    ])
    cursor.execute(*sql)
    return [x[0] for x in cursor.fetchall()]


class GiscedataPolissaTarifa(osv.osv):
    _name = 'giscedata.polissa.tarifa'
    _inherit = 'giscedata.polissa.tarifa'

    def get_periodes_preus(self, cursor, uid, tarifa_id, tipus, pricelist_id,
                           context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        pricelist_obj = self.pool.get('product.pricelist')
        periodes_producte = self.get_periodes_producte(cursor, uid, tarifa_id,
                                                       tipus, context=context)
        res = {}
        for periode, producte in periodes_producte.items():
            res[periode] = pricelist_obj.price_get(
                cursor, uid, [pricelist_id], producte, 1, context=ctx
            )[pricelist_id]
        return res

GiscedataPolissaTarifa()


class GiscedataPolissa(osv.osv):
    """Extensió de la pòlissa amb les dades bàsiques de facturació.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _polissa_states_selection = [
        ('facturacio', 'Facturació'),
    ]
    # ORM stuff

    _pagador_selection = [('titular', 'Titular'),
                          ('altre_p', 'Altre')]

    def get_related_attachments_fields(self, cursor, uid):
        return super(GiscedataPolissa, self).get_related_attachments_fields(cursor, uid) + ['pagador']

    def __init__(self, pool, cursor):
        """Init to add new states."""
        super(GiscedataPolissa, self).__init__(pool, cursor)
        for new_selection in self._polissa_states_selection:
            if new_selection not in self._columns['state'].selection:
                self._columns['state'].selection.append(new_selection)

    def _ff_fact_endarrerida(self, cursor, uid, ids, field_name, args,
                                         context=None):

        """ Marquem una factura com a endarrerida:
                * Fa més de 1.33 * facturacio dies que no es factura
                * La pólissa no té cap factura fa 1.33 * facturacio
                  dies que està facturada
        """
        res = dict.fromkeys(ids, False)
        cfg_obj = self.pool.get('res.config')
        nom_conf = 'periode_polissa_facturacio_endarrerida'
        periode = float(cfg_obj.get(cursor, uid, nom_conf, 1.33))

        vals_pol = self.read(cursor, uid, ids, ['facturacio',
                                                'data_ultima_lectura',
                                                'data_alta'])

        for pol in vals_pol:
            ult_fact = pol['data_ultima_lectura'] or pol['data_alta']
            if ult_fact:
                # calculem la data anterior
                # dies a restar
                dies = int(pol['facturacio']) * 30 * periode
                data_anterior = datetime.now() - timedelta(dies)
                if ult_fact < datetime.strftime(data_anterior, '%Y-%m-%d'):
                    res[pol['id']] = True

        return res


    def _search_fact_endarrerida(self, cursor, uid, obj, name, args,
                                 context=None):
        """
        Funció de cerca de facturacio endarrerida
        """
        trobats = []

        cfg_obj = self.pool.get('res.config')
        nom_conf = 'periode_polissa_facturacio_endarrerida'
        periode = float(cfg_obj.get(cursor, uid, nom_conf, 1.33))

        for facturacio in [1, 2]:
            dies = facturacio * 30 * periode
            data_anterior = datetime.now() - timedelta(dies)

            ids = self.search(cursor, uid,
                              [('facturacio', '=', facturacio),
                               '|',
                               '&', ('data_ultima_lectura', '<',
                                     data_anterior),
                               ('data_ultima_lectura', '!=', False),
                               '&', ('data_alta', '<', data_anterior),
                               ('data_ultima_lectura', '=', False)])
            trobats += ids

        return [('id', 'in', trobats)]

    def anullar_facturacions(self, cursor, uid, ids, context=None):
        """Anul·la les possibles facturacions posteriors de la pòlissa.

        Si es troba la pòlissa en lots de facturació futurs, es treu
        d'aquests.
        """
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        # s'ha d'esborrar la pòlissa del lot
        for pol in self.browse(cursor, uid, ids, context):
            if pol.lot_facturacio:
                search_params = [
                    ('polissa_id', '=', pol.id),
                    ('state', '=', 'esborrany')
                ]
                if pol.data_baixa:
                    from_date = pol.data_baixa
                    search_params.append(('lot_id.data_inici', '>', from_date))
                else:
                    from_date =  pol.lot_facturacio.data_inici
                    search_params.append(('lot_id.data_inici', '>=', from_date))

                clot_id = clot_obj.search(cursor, uid, search_params,
                                          context={'active_test': False})
                clot_obj.unlink(cursor, uid, clot_id, context)
                #If something was unlinked
                if clot_id:
                    #Assign last invoiced lot. This way the polissa will
                    #reflect the real last lot. Otherwise, it will be
                    #assigned to an incorrect lot that do not contain
                    #any clot for this polissa
                    cursor.execute(
                        "SELECT clot.id "
                        "FROM giscedata_facturacio_contracte_lot clot "
                        "JOIN giscedata_facturacio_lot lot ON lot.id=clot.lot_id "
                        "WHERE clot.polissa_id = %s "
                        "AND clot.state in %s "
                        "AND lot.data_final >= %s "
                        "ORDER BY lot.data_inici desc LIMIT 1",
                        (pol.id, ('facturat', 'finalitzat'), from_date)
                    )
                    clot_ids = cursor.fetchall()
                    if len(clot_ids) and len(clot_ids[0]):
                        clot_ids = clot_ids[0]
                        clot = clot_obj.browse(cursor, uid, clot_ids[0])
                        ctx = context.copy()
                        ctx.update({'sync': False, 'from_baixa': True})
                        pol.write({'lot_facturacio': clot.lot_id.id},
                                  ctx)

    def create(self, cursor, uid, vals, context=None):
        res_id = super(GiscedataPolissa,
                       self).create(cursor, uid, vals, context)
        if vals.get('lot_facturacio', False):
            self.assignar_al_lot(cursor, uid, [res_id], vals['lot_facturacio'],
                                 context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        """Quan guardem una pòlissa mirem si s'ha de reassignar a algun lot.
        """
        if 'lot_facturacio' in vals and not vals.get('lot_facturacio'):
            self.anullar_facturacions(cursor, uid, ids, context)
        if vals.get('lot_facturacio', False):

            self.assignar_al_lot(cursor, uid, ids, vals['lot_facturacio'],
                                 context)
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals,
                                                  context)
        return res

    # Workflow stuff
    def wkf_facturacio(self, cursor, uid, ids):
        """Estat facturació."""
        for polissa in self.read(cursor, uid, ids, ['state']):
            vals = {'state': 'facturacio',
                    'state_post_facturacio': polissa['state']}
            self.write(cursor, uid, polissa['id'], vals)

    def wkf_baixa(self, cursor, uid, ids):
        """Es dona de baixa la pòlissa

        L'esborrem dels lots de facturació futurs.
        """
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid)
        context = {'from_baixa': True}
        if user.context_lang:
            context.update({'lang': user.context_lang})                           

        #En el cas de baixes amb facturacio bimestral i
        #data de baixa del mes anterior al de la facturacio
        #anullar_facturacions la borra del lot quan no ho ha de fer
        #la tornem a assignar al lot, per que no la hem de perdre
        for polissa in self.browse(cursor, uid, ids):
            lot_facturacio = polissa.lot_facturacio
            if polissa.data_baixa <= polissa.data_ultima_lectura:
                self.anullar_facturacions(cursor, uid, [polissa.id], context=context)
            #Si es bimestral
            if polissa.facturacio == 2 and lot_facturacio:
                data_inici = datetime.strptime((lot_facturacio.
                                                data_inici),
                                               '%Y-%m-%d')
                data_final_ant = data_inici - timedelta(days=1)
                days = calendar.monthrange(data_final_ant.year,
                                           data_final_ant.month)[1]
                data_inici_ant = (data_final_ant
                                  - timedelta(days)
                                  + timedelta(days=1))
                if polissa.data_baixa >= datetime.strftime(data_inici_ant,
                                                           '%Y-%m-%d'):
                    polissa.write({
                        'lot_facturacio': lot_facturacio.id}, context=context
                    )

        super(GiscedataPolissa, self).wkf_baixa(cursor, uid, ids)

    def wkf_activa(self, cursor, uid, ids, context=None):
        '''On activate reassign the correct lot if assigned
        to a lot in the past. It is useful when reactivating
        or if an error introducing lot ocurred in new polisses.
        Only do it if is already invoiced in the assigned lot'''
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        super(GiscedataPolissa, self).wkf_activa(cursor, uid, ids)
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        facturacio_lot_obj = self.pool.get('giscedata.facturacio.lot')
        conf_obj = self.pool.get('res.config')

        polissa_lot_automatic = literal_eval(
            conf_obj.get(cursor, uid, 'polissa_lot_automatic', 'False')
        )
        for polissa in self.browse(cursor, uid, ids):
            if polissa_lot_automatic and not polissa.lot_facturacio and polissa.data_alta:
                lot_id = facturacio_lot_obj.get_next(
                    cursor, uid, polissa.data_alta, polissa.facturacio, context=context
                )
                if lot_id:
                    params = ['state']
                    lot_vals = facturacio_lot_obj.read(
                        cursor, uid, [lot_id], params, context=context
                    )[0]
                    if lot_vals['state'] == 'tancat':
                        params = [('state', '=', 'obert')]
                        lot_obert_mes_vell_id = facturacio_lot_obj.search(
                            cursor, uid, params, limit=1, order='data_inici ASC'
                        )
                        if lot_obert_mes_vell_id:
                            lot_id = lot_obert_mes_vell_id[0]
                        else:
                            lot_id = False
                polissa.write({'lot_facturacio': lot_id}, context=context)
            elif polissa.lot_facturacio:
                lot_id = polissa.lot_facturacio.id
                #Search if we have already invoiced this polissa
                #in the assigned lot
                search_params = [('lot_id', '=', lot_id),
                                 ('polissa_id', '=', polissa.id)]
                clot_ids = clot_obj.search(cursor, uid, search_params)
                if clot_ids:
                    clot = clot_obj.browse(cursor, uid, clot_ids[0])
                    if (clot.state == 'finalitzat' and
                        polissa.lot_facturacio.data_final < today):
                        polissa.assignar_seguent_lot({'data_final': today})
                else:
                    self.assignar_al_lot(cursor, uid, ids, lot_id)

    def cnd_activa_cancelar(self, cursor, uid, ids):
        ''' Condició per comprovar que es pot cancel·lar una pòlissa activa. '''

        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        for id in ids:
            factura_ids = factura_obj.search(cursor, uid, [
                ('polissa_id', '=', id)
            ])
            if factura_ids:
                error_msg = _(u'La pòlissa té assignada una o més '
                              u'factures i no es pot cancel·lar.')
                raise osv.except_osv(_(u'Error !'), error_msg)

        return True

    def moure_al_lot(self, cursor, uid, cont_lot_ori_id, polissa_id,
                     lot_desti_id, context=None):
        cont_obj = self.pool.get('giscedata.facturacio.contracte_lot')

        self.write(cursor, uid, polissa_id, {
            'lot_facturacio': lot_desti_id,
        })

        existeix = cont_obj.search_count(cursor, uid, [
            ('id', '=', cont_lot_ori_id),
        ])

        if existeix:
            cont_obj.unlink(cursor, uid, cont_lot_ori_id)

    # Object stuff
    def assignar_al_lot(self, cursor, uid, ids, lot_id, context=None):
        """Assigna la pòlissa al lot dessitjat treient-la dels que no toqui.
        """
        if not context:
            context = {}
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        contracte_lot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        lot_facturacio_obj = self.pool.get('giscedata.facturacio.lot')
        lot_facturacio = lot_facturacio_obj.browse(cursor, uid,
                                                   lot_id, context)
        from_baixa = context.get('from_baixa', False)

        for polissa in self.browse(cursor, uid, ids):
            if not from_baixa and lot_facturacio.state == 'tancat':
                raise osv.except_osv(
                    _('Error'),
                    _("No es pot assignar al lot %s degut a que aquest lot "
                      "està en estat tancat")
                    % lot_facturacio.name
                )
            # Busquem els contractes que estiguin associats a lots més
            # antics i que estiguin en estat esborrany
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('state', '=', 'esborrany'),
                ('lot_id.data_final', '<', lot_facturacio.data_inici),
            ]
            unlink_ids = contracte_lot_obj.search(
                cursor, uid, search_params, context={'active_test': False}
            )
            contracte_lot_obj.unlink(cursor, uid, unlink_ids, context)
            # Busquem els posteriors que estiguin en estat esborrany
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('state', '=', 'esborrany'),
                ('lot_id.data_inici', '>', lot_facturacio.data_final),
            ]
            unlink_ids = contracte_lot_obj.search(
                cursor, uid, search_params, context={'active_test': False}
            )
            contracte_lot_obj.unlink(cursor, uid, unlink_ids, context)
            # Si no existeix ja al lot actual li assignem
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('lot_id.id', '=', lot_facturacio.id),
            ]
            count = contracte_lot_obj.search_count(
                cursor, uid, search_params, context={'active_test': False}
            )
            if not count:
                vals = {
                    'polissa_id': polissa.id,
                    'lot_id': lot_facturacio.id,
                }
                contracte_lot_obj.create(cursor, uid, vals)
        return True

    def get_seguent_lot(self, cursor, uid, ids, data_final_factura):
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        data_final = (datetime.strptime(data_final_factura, '%Y-%m-%d')
                      + timedelta(days=1)).strftime('%Y-%m-%d')
        for polissa in self.browse(cursor, uid, ids):
            # Do not move polissa if state is baixa
            if polissa.state == 'baixa' and polissa.data_ultima_lectura >= polissa.data_baixa:
                return polissa.lot_facturacio.id
            ctx = {}
            if (polissa.modcontractual_activa and
                polissa.modcontractual_activa.data_final >= data_final):
                ctx.update({'date': data_final})
            # Refresh polissa browse with updated context
            pol = self.browse(cursor, uid, polissa.id, ctx)
            return lot_obj.get_next(
                cursor, uid, data_final_factura, pol.facturacio
            )

    def assignar_seguent_lot(self, cursor, uid, ids, context=None):
        """Assigna la pòlissa al següent Lot, segons el tipus de facturació.

        Si en el context hi tenim el paràmetre data_inici el movem al lot
        següent segons data_inici. Si no existeix aquest paràmetre el mourà
        segons el lot actual que tingui assignat a la pòlissa.
        """
        if not context:
            context = {}
        data_final = context.get('data_final', False)
        for polissa in self.browse(cursor, uid, ids, context):
            if (not data_final and not polissa.lot_facturacio):
                continue
            if not data_final:
                data_final = polissa.lot_facturacio.data_final
            if (not polissa.active and polissa.data_ultima_lectura >= polissa.data_baixa):
                continue
            lot_id = polissa.get_seguent_lot(data_final)
            if not lot_id:
                raise osv.except_osv(
                    _('Error'), _(u"No s'ha trobat el lot per assignar la "
                                  u"pòlissa a %s") % data_final
                )
            if polissa.lot_facturacio.id != lot_id:
                polissa.write({'lot_facturacio': lot_id},
                              context={'sync': False})
        return True

    def get_inici_final_a_facturar(self, cursor, uid, polissa_id,
                                   use_lot=False, context=None):
        """Busca l'inici i final del periode de facturació.
        """
        if not context:
            context = {}
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        polissa_obj = self.pool.get('giscedata.polissa')

        if use_lot:
            lot_obj = self.pool.get('giscedata.facturacio.lot')
            lot_facturacio = lot_obj.browse(cursor, uid, use_lot, context)
            dti = datetime.strptime(lot_facturacio.data_final, '%Y-%m-%d')
            ctx = context.copy()
            ctx['date'] = lot_facturacio.data_final
            # Utilitzem el context per tal que ens dongui una pòlissa amb els
            # valors de la data final del lot
            polissa = polissa_obj.browse(cursor, uid, polissa_id, ctx)
            facturacio = polissa.facturacio
            while facturacio:
                dies_del_mes = calendar.monthrange(dti.year, dti.month)[1]
                dti -= timedelta(days=dies_del_mes)
                facturacio -= 1
            dti += timedelta(days=1)
            data_inici = dti.strftime('%Y-%m-%d')
            data_final = lot_facturacio.data_final
        else:
            # No fem servir el les dates del lot per facturar si no que
            # utilitzem les dates de les lectures, això vol dir que sempre
            # s'haurà de sumar un dia si tenim una data anterior.
            polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
            # En anul·ladores i rectificadores tindrem les dates al context
            data_inici = context.get('ult_lectura_fact',
                                     polissa.data_ultima_lectura)
            data_final = context.get('fins_lectura_fact', False)
            if data_inici:
                comptadors = polissa.comptadors_actius(data_inici, data_final)
                dates_alta = [
                    c.data_alta for c in polissa.comptadors
                        if c.id in comptadors
                ]
                dates_alta.append(polissa.data_alta)
                if data_inici not in dates_alta:
                    # Les dates de lectures els hi hem de sumar un dia més.
                    data1 = (datetime.strptime(data_inici, '%Y-%m-%d')
                             + timedelta(days=1))
                    data1 = data1.strftime('%Y-%m-%d')
                else:
                    data1 = data_inici
                data2 = False
            else:
                data1 = False
                data2 = False
                if polissa.data_ultima_lectura_estimada:
                    data2 = (
                        datetime.strptime(polissa.data_ultima_lectura_estimada,
                                          '%Y-%m-%d') + timedelta(days=1)
                    )
                    data2 = data2.strftime('%Y-%m-%d')
            data_inici = max(data1, data2, polissa.data_alta)
            if not data_final:
                ctx = context.copy()
                ctx['data_inici_facturacio'] = data_inici
                data_final = polissa.data_utlima_lectura_entrada(ctx)

            if context.get('factura_manual', False):
                data_darrera_lect = polissa.data_utlima_lectura_entrada(context)
                if data_final > data_darrera_lect:
                    data_final = data_darrera_lect

        if data_final and data_inici and data_final < data_inici:
            if not context.get('validacio', False):
                raise osv.except_osv('Error',
                    _(u'Polissa: %s. La data final %s es mes petita que la '
                    u'data inicial %s' % (polissa.name, data_final,
                                          data_inici)))
        return (data_inici, data_final)

    def get_ultima_lectura_facturada(self, cursor, uid, polissa_id, data_inici,
                                     journal_code, context=None):
        '''Busquem la última lectura facturada. No podem mirar directament a la
        polissa per que pot ser una rectificacio anterior a factures posteriors
        i per tant aquesta data ja haura estat actualitzada. Anem a mirar
        factures anteriors fins que trobem una lectura facturada
        o en darrera instancia, tornem la data d'inici de la primera factura
        que sera igual a la data d'alta de la polissa'''
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        if not context:
            context = {}
        context.update({'active_test': False})

        # Busquem totes les factures anul·ladores d'aqueseta pòlissa
        search_params = [
            ('type', '=', 'out_refund'),
            ('polissa_id', '=', polissa_id),
            ('state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'like', journal_code),
            ('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES)
        ]
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                         context=context)
        refs = [x['ref'][0]
                for x in factura_obj.read(cursor, uid, factura_ids, ['ref'])]

        journal = journal_code
        if '.' in journal:
            journal = journal.split('.')[0]

        search_params = [
            ('data_final', '<', data_inici),
            ('type', '=', 'out_invoice'),
            ('polissa_id', '=', polissa_id),
            ('state', 'in', ('open', 'paid')),
            ('invoice_id.journal_id.code', 'ilike', journal),
        ]
        if refs:
            search_params += [('id', 'not in', refs)]
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                  order='data_final desc, id desc',
                                  limit=1,
                                  context=context)
        if factura_ids:
            factura_ant = factura_obj.browse(cursor, uid, factura_ids[0])
            if factura_ant.lectures_energia_ids:
                # D'una factura anterior la última lectura facturada serà la
                # mes gran
                ulf = max(l.data_actual for l in
                          factura_ant.lectures_energia_ids)
            else:
                ulf = self.get_ultima_lectura_facturada(
                    cursor, uid, factura_ant.polissa_id.id,
                    factura_ant.data_inici, factura_ant.journal_id.code,
                    context=context)
        else:
            #Hem arribat a la darrera primera factura emesa sense trobar
            #lectures, per tant retornem la data d'alta de la polissa
            ulf = data_inici
        return ulf

    def get_modcontractual_intervals(self, cursor, uid, polissa_id, data_inici,
                                     data_final, context=None):
        """Obté tots els intervals a facturar d'un periode de facturació.

        Aquests són:
          * Modificacions contractuals.
        """
        if not context:
            context = {}
        if not 'ffields' in context:
            context['ffields'] = list(set(INTERVAL_INVOICING_FIELDS))
        dates_de_tall = super(GiscedataPolissa,
                               self).get_modcontractual_intervals(cursor, uid,
                                        polissa_id, data_inici, data_final,
                                        context)
        return dates_de_tall

    # on_change functions (views related)
    def onchange_titular(self, cursor, uid, ids, titular, pagador_sel, pagador,
                         tipo_pago,  notificacio=False, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not titular:
            return res
        if not pagador:
            pagador = titular

        pagador_sel = 'titular'
        vals_pagador = self.onchange_pagador_sel(
            cursor, uid, ids, pagador_sel, titular, pagador, None, tipo_pago,
            False, context=context
        )
        res.update(vals_pagador)
        if notificacio == 'titular':
            vals_notificacio = self.onchange_notificacio(
                cursor, uid, ids, 'titular', titular,
                vals_pagador['value']['pagador'], False, context=context
             )
            res['value'].update(vals_notificacio['value'])
            res['domain'].update(vals_notificacio['domain'])

        if not tipo_pago:
            partner = self.pool.get('res.partner').browse(cursor, uid, titular)
            res['value'].update({'tipo_pago': (partner.
                                               payment_type_customer.id)})
        return res

    def onchange_direccio_fiscal(self, cursor, uid, ids, direccio_pagament, context=None):
        adress_obj = self.pool.get('res.partner.address')
        res = {'value': {}, 'domain': {}, 'warning': {}}

        fields_add = adress_obj.fields_get(cursor, uid, fields=['id_municipi']).keys()

        if fields_add and direccio_pagament:
            addr = adress_obj.browse(cursor, uid, direccio_pagament, context=context)

            warrning_msg = ''
            if not addr.id_municipi:
                warrning_msg += _('Municipi de la direcció fiscal no configurat\n')
            if not addr.street:
                warrning_msg += _('Carrer de la direcció fiscal no configurat\n')
            if not addr.name:
                warrning_msg += _(
                    'Nom de la direcció fiscal no configurat\n'
                )

            if warrning_msg:
                res['warning'].update(
                    {
                        'title': _('Avís'),
                        'message': warrning_msg
                    })
        return res

    def onchange_pagador_sel(self, cursor, uid, ids, pagador_sel, titular,
                             pagador, direccio_pagament, tipo_pago,
                             notificacio, context=None):
        """Actua quan hi ha un canvi al camp pagador_sel.
        """
        partner_obj = self.pool.get('res.partner')
        res = {'value': {}, 'domain': {}, 'warning': {}}
        res['value']['pagador_sel'] = pagador_sel
        # emplenem el domain i value per la direccio de pagament
        if pagador_sel == 'titular' and titular:
            domain = [('partner_id', '=', titular)]
            dir_pago = partner_obj.address_get(cursor, uid, [titular],
                                               ['invoice'])['invoice']

            partner = self.pool.get('res.partner').browse(cursor, uid, titular)
            if not partner.payment_type_customer:
                res['warning'].update({'title': _('Avis'),
                                    'message': _(u"El pagador no té cap tipus "
                                                 u"de pagament predefinit")})
            else:
                res.get('value').update({'tipo_pago':
                                         partner.payment_type_customer.id})
            res['domain'].update({'direccio_pagament': domain,
                                  'pagador': [('id', '=', titular)]})
            res['value'].update({'direccio_pagament': dir_pago,
                                 'pagador': titular,
                                 'notificacio': pagador_sel})

            vals = self.onchange_notificacio(cursor, uid, ids, pagador_sel, titular, '', '')
            res['value'].update(vals['value'])
            res['domain'].update(vals['domain'])

        elif pagador_sel == 'altre_p':
            if pagador:
                res['value'].update({'pagador': False})
            if direccio_pagament:
                res['value'].update({'direccio_pagament': False})
            res['domain'].update({'direccio_pagament': [],
                                  'pagador': []})
        else:
            res['domain'].update({'direccio_pagament': [],
                                  'pagador': []})
            res['value'].update({'direccio_pagament': False,
                                 'pagador': False})
        # Finalment si a notificació hi tenim posat que és el pagador ho
        # modifiquem per tal que quadri
        if notificacio == 'pagador':
            res['domain'].update({'direccio_notificacio':
                                  res['domain'].get('direccio_pagament', [])})
            res['value'].update({'direccio_notificacio':
                                 res['value'].get('direccio_pagament', False)})
        return res

    def onchange_altre_pagador(self, cursor, uid, ids, altre_pagador,
                               notificacio, context=None):
        """Actua quan hi ha un canvi del camp 'altre_pagador'.
        """
        partner_obj = self.pool.get('res.partner')
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if altre_pagador:
            domain = [('partner_id', '=', altre_pagador)]
            value = partner_obj.address_get(cursor, uid, [altre_pagador],
                                            ['invoice'])['invoice']
        else:
            value = False
            domain = []

        res['value'].update({'direccio_pagament': value})
        res['domain'].update({'direccio_pagament': domain})

        # Si la direcció de notificació és "pagador" l'hem de modificar
        if notificacio == 'pagador':
            res['value'].update({'direccio_notificacio': value})
            res['domain'].update({'direccio_notificacio': domain})
        return res

    def onchange_notificacio(self, cursor, uid, ids, notificacio, titular,
                             pagador, altre_p, context=None):
        """Actua quan es canvia la persona de notificació
        """
        partner_obj = self.pool.get('res.partner')
        if notificacio == 'titular' and titular:
            domain = [('partner_id', '=', titular)]
            value = partner_obj.address_get(cursor, uid, [titular],
                                            ['contact'])['contact']
        elif notificacio == 'pagador' and pagador:
            domain = [('partner_id', '=', pagador)]
            value = partner_obj.address_get(cursor, uid, [pagador],
                                            ['contact'])['contact']
        elif notificacio == 'altre_p' and altre_p:
            domain = [('partner_id', '=', altre_p)]
            value = partner_obj.address_get(cursor, uid, [altre_p],
                                            ['contact'])['contact']
        else:
            domain = []
            value = False
        res = {'domain': {'direccio_notificacio': domain},
                'value': {'direccio_notificacio': value}}
        if notificacio != 'altre_p':
            res['value'].update({'altre_p': False})
        return res

    def onchange_altre_p(self, cursor, uid, ids, altre_p, context=None):
        """Actua quan es canvia el contacte alternatiu
        """
        partner_obj = self.pool.get('res.partner')
        if altre_p:
            domain = [('partner_id', '=', altre_p)]
            value = partner_obj.address_get(cursor, uid, [altre_p],
                                            ['contact'])['contact']
        else:
            value = False
            domain = []
        return {
            'value': {
               'direccio_notificacio': value
            },
            'domain': {
                'direccio_notificacio': domain
            }
        }

    def onchange_tipo_pago(self, cursor, uid, ids, tipo_pago, pagador, context=None):
        """
        if payment type is Recibo_CBS (Recibo domiciliado)
          if only exist one bank account automatic add it in field, 
          the same for payment group (Sepa 19)
        oterwise is cash(CAJA)
          release bank account from field.
        """
        
        res = {'warning': {}, 'value': {}, 'domain': {}}
        if tipo_pago:
            tpg = self.pool.get('payment.type').browse(cursor, uid, tipo_pago)
            if tpg.code == 'RECIBO_CSB':
                partner_bank_obj = self.pool.get('res.partner.bank')
                bank_ids = partner_bank_obj.search(cursor, uid, [
                    ('partner_id', '=', pagador)
                ])
                if len(bank_ids) == 1:
                    res['value'].update({'bank': bank_ids[0]})
            else:
                res['value'].update({'bank': False})
        return res

    def onchange_bank(
            self, cursor, uid, ids, propietari_bank, bank, context=None
    ):
        res = {}
        if not bank:
            res['propietari_bank'] = ''
        else:
            bank_obj = self.pool.get('res.partner.bank')
            owner_name = bank_obj.read(
                cursor, uid, [bank], ['owner_name']
            )[0]['owner_name']
            res['propietari_bank'] = owner_name
        return{
            'value': res
        }

    def update_mandate(self, cursor, uid, polissa_id, context=None):

        if not context:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id, context=context)
        debtor_address = (polissa.direccio_pagament.
                          name_get(context=context)[0][1]).upper()
        debtor_contact = (polissa.direccio_pagament.name
                          and '%s, ' % polissa.direccio_pagament.name
                          or '').upper()
        if debtor_contact:
            debtor_address = debtor_address.replace(debtor_contact, '')
        debtor_state = (polissa.direccio_pagament.state_id
                        and polissa.direccio_pagament.state_id.name
                        or '').upper()
        debtor_country = (polissa.direccio_pagament.country_id
                          and polissa.direccio_pagament.country_id.name
                          or '').upper()
        notes = u"Contrato: %s\n" % polissa.name
        notes += u"CUPS: %s\n" % polissa.cups.name
        notes += u"Dirección de suministro: %s\n" % polissa.cups.direccio
        code_cnae = polissa.cnae and polissa.cnae.name or ''
        desc_cnae = polissa.cnae and polissa.cnae.descripcio or ''
        if code_cnae:
            notes += _(u"CNAE: (%s) %s\n") % (code_cnae, desc_cnae)

        if polissa.bank.owner_name:
            payment_data = u""
            bank_obj = self.pool.get('res.partner.bank')
            fields_bank = bank_obj.fields_get(
                cursor, uid, fields=['owner_id', 'owner_address_id']).keys()
            if fields_bank:
                if polissa.bank.owner_id:
                    payment_data += u" NIF: {0}".format(
                        polissa.bank.owner_id.vat
                    )
                else:
                    payment_data += u" NIF:"
                if polissa.bank.owner_address_id:
                    phone = polissa.bank.owner_address_id.phone
                    if not phone:
                        phone = polissa.bank.owner_address_id.mobile
                    payment_data += u" Teléfono: {0}".format(phone)
                else:
                    payment_data += u" Teléfono:"

            notes += u"Pagador:\n"
            notes += u"Nombre y apellidos: {0}{1}\n".format(
                polissa.bank.owner_name, payment_data
            )

        vals = {
            'debtor_name': polissa.pagador.name,
            'debtor_vat': polissa.pagador.vat,
            'debtor_address': debtor_address,
            'debtor_state': debtor_state,
            'debtor_country': debtor_country,
            'debtor_iban': polissa.bank.iban,
            'reference': '%s,%s' % ('giscedata.polissa', polissa_id),
            'notes': notes,
        }

        return vals

    def search_mandate(self, cursor, uid, polissa_id, iban, context=None):

        mandate_obj = self.pool.get('payment.mandate')

        # Search for mandate
        reference = 'giscedata.polissa,%s' % polissa_id
        search_params = [('reference', '=', reference),
                         ('debtor_iban', '=', iban)]
        mandate_ids = mandate_obj.search(cursor, uid, search_params, limit=1)
        if mandate_ids:
            return mandate_ids[0]
        return False

    def get_current_bank(self, cursor, uid, polissa_id, context=None):
        if context is None:
            context = {}
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]
        res = {'partner_bank': False, 'mandate_id': False}
        polissa = self.read(cursor, uid, polissa_id, ['bank'])
        if polissa['bank']:
            partner_bank_obj = self.pool.get('res.partner.bank')
            iban = partner_bank_obj.read(
                cursor, uid, polissa['bank'][0], ['iban']
            )
            mandate_id = self.search_mandate(
                cursor, uid, polissa_id, iban['iban'], context=context
            )
            res.update(
                {
                    'partner_bank': polissa['bank'][0],
                    'mandate_id': mandate_id
                 }
            )
        return res

    def get_invoices(self, cursor, uid, polissa_id,
                     date_from, date_to, date_field='date_invoice',
                     context=None):
        '''get invoices from polissa between date_from and date_to'''

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        journal_obj = self.pool.get('account.journal')

        search_params = [('code', 'like', 'ENERGIA')]
        journal_ids = journal_obj.search(cursor, uid, search_params)

        search_params = [
            ('polissa_id', '=', polissa_id),
            ('type', '=', 'out_invoice'),
            ('state', 'in', ('open', 'paid')),
            (date_field, '>=', date_from),
            (date_field, '<=', date_to),
        ]

        factura_ids = factura_obj.search(cursor, uid, search_params)
        if not factura_ids:
            return []
        factura_vals = factura_obj.read(cursor, uid, factura_ids,
                                        ['ref', 'journal_id'])
        # Rectified invoices
        rectified_ids = [x['ref'][0] for x in factura_vals
                         if x['ref']]
        # Not ENERGIA invoices
        not_journal_ids = [x['id'] for x in factura_vals
                           if x['journal_id'] and
                              x['journal_id'][0] not in journal_ids]
        # Return all not rectified invoices and with ENERGIA journal
        return list(set(factura_ids) -
                    (set(rectified_ids) | set(not_journal_ids)))

    def has_historic(self, cursor, uid, polissa_id, lot_id,
                     strict=True, context=None):
        """Check if a polissa has an invoice from
        last year using lot as reference
        returns: last year invoice if found"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        lot_obj = self.pool.get('giscedata.facturacio.lot')

        # Compute previous year periods
        prev_lot_id = lot_obj.get_previous_lot(cursor, uid, lot_id,
                                               delta={'years': -1},
                                               context=context)
        if not prev_lot_id:
            return False
        
        lot_dates = lot_obj.read(cursor, uid, prev_lot_id,
                                 ['data_inici', 'data_final'])
        lot_data_inici = lot_dates['data_inici']
        lot_data_final = lot_dates['data_final']
        # Search for an invoice including this dates
        search_params = [('polissa_id', '=', polissa_id),
                         ('tipo_rectificadora', '=', 'N'),
                         ('state', 'in', ('open', 'paid'))]
        # If estrict search for an invoice using the same lot
        # If not, search for an invoice embracing lot dates 
        if strict:
            search_params.extend([('lot_facturacio', '=', prev_lot_id)])
        else:
            search_params.extend([('data_inici', '<=', lot_data_inici),
                                  ('data_final', '>=', lot_data_final)])
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                         context=context)

        if len(factura_ids) == 1:
            factura_id = factura_ids[0]
            factura_rect = factura_obj.is_rectified(cursor, uid, factura_id)
            if factura_rect:
                factura_id = factura_rect

            factura = factura_obj.browse(cursor, uid, factura_id)
            polissa = self.browse(cursor, uid, polissa_id,
                                  context={'date': lot_data_final})
            # Check for differences between polissa and invoice
            if (polissa.tarifa.id == factura.tarifa_acces_id.id and
                polissa.potencia == factura.potencia):
                return factura_id

        return False

    def estimate_consumption(self, cursor, uid, polissa_id, lot_id,
                             days, context=None):
        """Estimate consumption based on resolucion 14 de septiembre 2009
        Returns a dict with {'period': consumption, ...}
        """
        if not context:
            context = {}

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        lot_obj = self.pool.get('giscedata.facturacio.lot')

        res = {}
        lot = lot_obj.browse(cursor, uid, lot_id)

        # 1 - Try to estimate by historic consumption
        # Get the invoice for the same lot from the last year
        fact_hist_id = self.has_historic(cursor, uid, polissa_id,
                                         lot_id, strict=False,
                                         context=context)
        if fact_hist_id:
            ctx = context.copy()
            ctx.update({'include_dates': True})
            activa = factura_obj.total_energia_kwh(cursor, uid,
                                            [fact_hist_id],
                                            context=ctx)[fact_hist_id]
            # If the invoice has no reads, we cannot estimate by historic
            if len(activa.keys()) > 0:
                activa_inici = min([x[0] for x in activa.itervalues()])
                activa_final = max([x[1] for x in activa.itervalues()])
                date_begin = datetime.strptime(activa_inici, '%Y-%m-%d')
                date_end = datetime.strptime(activa_final, '%Y-%m-%d')
                historic_days = (date_end - date_begin).days
                for period, value in activa.iteritems():
                    period_name = period.split('(')[1][:2]
                    res[period_name] = round(
                        ((value[2] / float(historic_days)) * days), 0)
                return res, 'historic'
        # 2 - Estimate by factor
        factors = {'2.0A': {'P1': 2},
                   '2.0DHA': {'P1': 1, 'P2': 2.7},
                   '2.1A': {'P1': 2},
                   '2.1DHA': {'P1': 1, 'P2': 2.7},
                  }
        polissa = self.browse(cursor, uid, polissa_id,
                              context={'date': lot.data_final})
        tarifa = polissa.tarifa
        for periode in tarifa.periodes:
            if periode.tipus != 'te':
                continue
            res[periode.name] = round((factors[tarifa.name][periode.name] *
                                       polissa.potencia *
                                       days), 0)
        return res, 'factor'

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'lot_facturacio': False,
            'data_ultima_lectura': False,
            'data_ultima_lectura_estimada': False,
        }
        default.update(default_values)
        res_id = super(
            GiscedataPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def _ff_prox_fact(self, cursor, uid, ids, field_name, arg, context=None):
        """Mètode per obtenir la data de pròxima facturació segons el lot.
        """
        lot_obj = self.pool.get('giscedata.facturacio.lot')

        res = {}
        fields_polissa = ['lot_facturacio']
        fields_lot = ['data_final']
        for polissa in self.read(cursor, uid, ids, fields_polissa):
            if polissa['lot_facturacio']:
                lot = lot_obj.read(cursor, uid, polissa['lot_facturacio'][0],
                                   fields_lot)
                res[polissa['id']] = lot['data_final']
            else:
                res[polissa['id']] = False
        return res

    def _get_prox_fact_ids(self, cursor, uid, ids, context=None):
        """Retorna els ids pels quals recalcular el camp proxima_facturacio
        """
        return ids

    def _ff_darrera_lectura(self, cursor, uid, ids, field_name, arg, context=None):
        '''Calcula la data de la darrera lectura facturada'''

        query_file = (u"%s/giscedata_facturacio/sql/"
                      u"darrera_lectura_facturada.sql"
                      % config['addons_path'])
        query = open(query_file).read()

        cursor.execute(query, (tuple(ids), ))
        res = dict([(x[0], x[1]) for x in cursor.fetchall()])
        #If some polissa do not have any invoices, it will not come
        #in query results. Write a false for all of them
        no_value_ids = list(set(ids) - set(res.keys()))
        no_value_res = dict.fromkeys(no_value_ids, False)
        res.update(no_value_res)
        return res

    def _ff_darrera_lectura_inv(self, cursor, uid, ids, name, value,
                            fnct_inv_arg, context=None):
        if isinstance(value, bool) and not value:
            value = None
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        query = '''
            UPDATE giscedata_polissa
            SET data_ultima_lectura = %s
            WHERE id = %s
            '''
        for id in ids:
            cursor.execute(query, (value, id))
            
        return True

    _store_darrera_lectura = {
        'account.invoice': (
            _get_polissa_from_energy_invoice, ['state'], 10
        )
    }

    def get_diposit_moves_polisses(self, cursor, uid, ids, tipo='income', context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        cfg = self.pool.get('res.config')
        product_obj = self.pool.get('product.product')
        product_id = int(cfg.get(cursor, uid, 'deposit_product_id', 0))
        if not product_id:
            return res
        acc_id = product_obj.get_account(cursor, uid, product_id, tipo)
        polisses = dict(
            (x['name'], x['id']) for x in self.read(cursor, uid, ids, ['name'])
            if x['name']
        )
        if not polisses:
            return res
        sql = (
            "select ref, sum(coalesce(debit, 0) - coalesce(credit, 0)) from account_move_line "
            "where product_id = %s and ref in %s and account_id = %s"
            "group by ref"
        )
        cursor.execute(sql, (product_id, tuple(polisses.keys()), acc_id))
        deposits = dict((polisses[x[0]], x[1]) for x in cursor.fetchall())
        res.update(deposits)
        return res

    def _ff_deposit(self, cursor, uid, ids, field_name, arg, context=None):
        return self.get_diposit_moves_polisses(
            cursor, uid, ids, context=context
        )

    def check_autofixs(self, cursor, uid, polissa_id, context=None):
        """ Executa els checks dels autofixs activats sobre polissa_id i retorna els ids amb els codis dels autofixs que no han passat"""
        if context is None:
            context = {}
        autofix_obj = self.pool.get("giscedata.polissa.autofix")
        res = []
        for autofix_info in autofix_obj.search_reader(cursor, uid, [('active', '=', True)], ['code', 'check_method', 'description']):
            method = getattr(self, autofix_info['check_method'])
            if method(cursor, uid, polissa_id, context):
                res.append((autofix_info['id'], "{0} {1}".format(autofix_info['code'], autofix_info['description'])))
        return res

    def do_autofixs(self, cursor, uid, polissa_id, context=None):
        """ Executa els fix dels autofixs activats sobre polissa_id i retorna els ids amb els codis dels autofixs que no han passat"""
        if context is None:
            context = {}
        autofix_obj = self.pool.get("giscedata.polissa.autofix")
        res = []
        for autofix_info in autofix_obj.search_reader(cursor, uid, [('active', '=', True)],
                                                      ['code', 'fix_method', 'description']):
            method = getattr(self, autofix_info['fix_method'])
            try:
                method(cursor, uid, polissa_id, context)
                res.append((autofix_info['id'], "{0} {1}".format(autofix_info['code'], autofix_info['description'])))
            except Exception, e:
                pass
        return res

    def check_sobreautolectura_sa01(self, cursor, uid, polissa_id, context=None):
        """
        Detecta quan tenim una lectura estimada que es superior a la següent
        lectura peró tenim lectures posteriors a pool que son superiors a
        l'estimada.
        """
        if context is None:
            context = {}
        pol_obj = self.pool.get("giscedata.polissa")
        compt_obj = self.pool.get("giscedata.lectures.comptador")
        lect_obj = self.pool.get("giscedata.lectures.lectura")
        lect_pool_obj = self.pool.get("giscedata.lectures.lectura.pool")
        orig_obj = self.pool.get("giscedata.lectures.origen")

        polissa = pol_obj.browse(cursor, uid, polissa_id, context=context)
        data_ultima_facturada = polissa.data_ultima_lectura
        for comptador in polissa.comptadors:
            # Check if the meter has the last invoiced readings
            l_estimada_ids = lect_obj.search(cursor, uid, [
                ('comptador', '=', comptador.name), ('name', '=', data_ultima_facturada)
            ])
            if not len(l_estimada_ids):
                continue

            # Check if the origin of the last invoiced readings is estimated
            # We only check it for one period (one reading), they should
            # have the same one
            lect_info = lect_obj.read(cursor, uid, l_estimada_ids[0], ['origen_id'])
            origen_info = orig_obj.read(cursor, uid, lect_info['origen_id'][0], ['codi'])
            if origen_info['codi'] != context.get('origen', '50'):
                continue

            estimated_lects = lect_obj.browse(cursor, uid, l_estimada_ids)

            # Check if we have readings (not pool) after the last invoiced that
            # have a lower value. It must happen in at least 1 period
            sobreestimacio = False
            for e_lect in estimated_lects:
                post_lect_ids = lect_obj.search(cursor, uid, [
                    ('comptador', '=', e_lect.comptador.id), ('periode', '=', e_lect.periode.id),
                    ('lectura', '<', e_lect.lectura), ('name', '>', e_lect.name)
                ])
                if post_lect_ids:
                    sobreestimacio = True
            if not sobreestimacio:
                continue

            # Check if we have readings of pool with all periods greater
            # than the estimated readings
            readings_dict_by_date = {}
            for e_lect in estimated_lects:
                l_post_ids = lect_pool_obj.search(cursor, uid, [
                    ('comptador', '=', e_lect.comptador.id), ('name', '>', e_lect.name), ('lectura', '>=', e_lect.lectura), ('periode', '=', e_lect.periode.id)
                ])
                for lpost_info in lect_pool_obj.read(cursor, uid, l_post_ids, ['name', 'periode']):
                    if lpost_info['periode'][0] not in readings_dict_by_date.get(lpost_info['name'], []):
                        readings_dict_by_date[lpost_info['name']] = readings_dict_by_date.get(lpost_info['name'], []) + [lpost_info['periode'][0]]

            # Now we check if in one of the dates from readings_dict_by_date
            # we have all periods
            correct_dates = []
            for date in readings_dict_by_date:
                if len(readings_dict_by_date[date]) == len(estimated_lects):
                    correct_dates.append(date)
            return sorted(correct_dates)

        # We don't have passed all checks
        return False

    def fix_sobreautolectura_sa01(self, cursor, uid, contract_id, context=None):
        """
        Soluciona el cas que d'una lectura sobrestimada (Ls) que
        * La lectura següent (Ls+1) és inferior
        * La lectura posterior a pool (Ls+2, Ls+3...Ls+jump) és superior

        Copia la lectura Ls a Ls+1

        :param polissa_id: Contract to fix
        :param jumps: Number of measures in pool from last invoiced to search
                      for
        :param context:
        :return: True
        """

        measure_dates = self.check_sobreautolectura_sa01(
            cursor, uid, contract_id, context
        )
        if not measure_dates:
            raise osv.except_osv(_('No és una sobreestimació'))

        pol_obj = self.pool.get('giscedata.polissa')
        lect_obj = self.pool.get('giscedata.lectures.lectura')
        contract = pol_obj.browse(cursor, uid, contract_id)

        last_invoiced_date = contract.data_ultima_lectura

        for meter in contract.comptadors:
            for lect in meter.lectures:
                if lect.name == last_invoiced_date:
                    # we've found the correct meter
                    # OVER-estimatd measure (Ls)
                    lect_ids = lect_obj.search(
                        cursor, uid,
                        [('name', '=', last_invoiced_date),
                         ('comptador', '=', meter.id)],
                    )
                    lects = lect_obj.read(
                        cursor, uid, lect_ids,
                        ['periode', 'tipus', 'lectura', 'origen',
                         'observacions']
                    )
                    # find next measure
                    next_lect_id = lect_obj.search(
                        cursor, uid,
                        [('name', '>', last_invoiced_date),
                         ('comptador', '=', meter.id)],
                         order='name ASC', limit=1
                    )
                    next_lect_name = lect_obj.read(
                        cursor, uid, next_lect_id[0], ['name']
                    )['name']

                    for lect in lects:
                        # find next measure
                        next_lect_ids = lect_obj.search(
                            cursor, uid,
                            [('name', '=', next_lect_name),
                             ('comptador', '=', meter.id),
                             ('periode', '=', lect['periode'][0]),
                             ('tipus', '=', lect['tipus']),
                             ]
                        )
                        next_lect = lect_obj.read(
                            cursor, uid, next_lect_ids[0],
                            ['lectura', 'observacions']
                        )
                        obs = _("Lectura copiada de l'anterior, per haver fet "
                                 "una sobreestimació. Anterior {0} kWh.""\n"
                                 "{1}").format(
                            next_lect['lectura'], next_lect['observacions']
                        )

                        vals = {
                            'lectura': lect['lectura'],
                            'observacions': obs,
                        }
                        lect_obj.write(cursor, uid, [next_lect_ids[0]], vals)

    def check_lectures_modcon_mc02(self, cursor, uid, polissa_id, context=None):
        """
        Detecta quan tenim una modcon en dia D amb lectures >= al dia  D carregades i sense lectures inicials en D-1.
        Si s'indica per context també comprova que a pool existeixin lectures correctes a pool en data D-1.
        """
        if context is None:
            context = {}
        pol_obj = self.pool.get("giscedata.polissa")
        lect_obj = self.pool.get("giscedata.lectures.lectura")

        polissa = pol_obj.browse(cursor, uid, polissa_id, context=context)
        # Ha de haver-hi una modcon nova
        if len(polissa.modcontractuals_ids) < 1:
            return False

        dini_modcon = polissa.modcontractual_activa.data_inici
        dini_ant = (
            datetime.strptime(dini_modcon, "%Y-%m-%d") - timedelta(days=1)
        ).strftime("%Y-%m-%d")
        data_ultima_facturada = polissa.data_ultima_lectura

        # Si ja està facturat continuem
        if data_ultima_facturada > dini_ant:
            return False

        tarifa = polissa.tarifa
        periodes_ids = tarifa.get_periodes(tipus='te').values()
        for comptador in polissa.comptadors:
            # Primer mirem que no tenim totes les lectures del dia D-1
            l_ids = lect_obj.search(cursor, uid, [
                ('comptador', '=', comptador.name), ('name', '=', dini_ant), ('periode', 'in', periodes_ids)
            ])
            if len(l_ids) == len(periodes_ids):
                continue

            # Si es necessari comprovem que tenim les lectures de D-1 al pool
            if context.get("check_pool", True):
                lect_pool_obj = self.pool.get("giscedata.lectures.lectura.pool")
                l_ids = lect_pool_obj.search(cursor, uid, [
                    ('comptador', '=', comptador.name), ('name', '=', dini_ant), ('periode', 'in', periodes_ids)
                ])
                if len(l_ids) != len(periodes_ids):
                    continue

            # Finalment mirem si tenim lectures posteriors a D-1 carregades
            l_ids = lect_obj.search(cursor, uid, [
                ('comptador', '=', comptador.name), ('name', '>', dini_ant), ('periode', 'in', periodes_ids)
            ])
            if not len(l_ids):
                continue

            # Es susceptible a fix
            return comptador.name

        return False

    def check_lectures_modcon_mc01(self, cursor, uid, polissa_id, context=None):
        """
        Comprova que hi ha una modificació contractual que NO és de tarifa per
        poder "estimar" la lectura D-1 del dia del canvi a partir de les
        lectures anteriors i posteriors.
        Evita generar factures de 0 lines quan hi ha un canvi de p.e. llista
        de preus entre lectures
        :param cursor:
        :param uid:
        :param polissa_id:
        :param context:
        :return: Num de comptador que compleix el check o False
        """
        if context is None:
            context = {}
        pol_obj = self.pool.get("giscedata.polissa")
        lect_obj = self.pool.get("giscedata.lectures.lectura")

        ctx = context.copy()
        ctx.update({'check_pool': False})
        meter_name = self.check_lectures_modcon_mc02(
            cursor, uid, polissa_id, context=ctx
        )
        # no hi ha modcon
        if not meter_name:
            return False

        polissa = pol_obj.browse(cursor, uid, polissa_id, context=context)
        modcon_actual = polissa.modcontractual_activa
        modcon_anterior = modcon_actual.modcontractual_ant

        if modcon_actual.tarifa != modcon_anterior.tarifa:
            return False

        # Validem que tenim lectura anterior i posterior per poder "estimar"
        change_day = polissa.modcontractual_activa.data_inici
        lect_ini_ids = lect_obj.search(cursor, uid, [('name', '<', change_day)])
        lect_end_ids = lect_obj.search(cursor, uid, [('name', '>', change_day)])

        if not lect_ini_ids or not lect_end_ids:
            return False

        return meter_name

    _columns = {
      'facturacio': fields.selection(FACTURACIO_SELECTION,
                                     u'Facturació', readonly=True,
                                  states={
                                     'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                  }),
      'state_post_facturacio': fields.char("Estat post facturació", size=64),
      'tarifa_codi': fields.related('tarifa', 'name', type='char',
                                    string='Codi Tarifa'),
      'lectura_en_baja': fields.boolean(
          'Lectura en baixa', readonly=True,
          states={
              'esborrany': [('readonly', False)],
              'validar': [('readonly', False)],
              'modcontractual': [('readonly', False), ('required', True)]
          }
      ),
      'trafo': fields.float('Trafo KVA', readonly=True,
                                  states={
                                     'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                  }),
      'proxima_facturacio': fields.function(_ff_prox_fact, method=True,
                                string=u'Proxima facturación',
                                store={
                                    'giscedata.polissa': (_get_prox_fact_ids,
                                                          ['lot_facturacio'],
                                                          10)
                                }, type='date'),
      'facturacio_potencia':
          fields.selection(FACTURACIO_POTENCIA_SELECTION,
                           u'Facturación Potencia', readonly=True,
                                  states={
                                     'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                  }),
      'property_unitat_potencia': fields.property(
            'product.uom',
            type='many2one',
            relation='product.uom',
            string=u"Unitat de facturació potència",
            method=True,
            view_load=True,
            domain="[('category_id.name', '=', 'POT ELEC')]",
            help=u"Amb quina unitat es vol facturar la potència",
            readonly=True,
            states={'esborrany': [('readonly', False)],
                    'validar': [('readonly', False), ('required', True)],
                    'modcontractual': [('readonly', False), ('required', True)]
                    }
       ),
      'lot_facturacio': fields.many2one('giscedata.facturacio.lot',
                                        u'Lot de facturació'),
      'data_ultima_lectura': fields.function(_ff_darrera_lectura,
                                fnct_inv=_ff_darrera_lectura_inv,
                                method=True, type='date',
                                string=u'Data última real facturada', 
                                store=_store_darrera_lectura),
      'data_ultima_lectura_estimada': fields.date(u'Data última estimada '
                                                  'facturada'),
      'pagador_sel': fields.selection(_pagador_selection, 'Persona fiscal',
                                  size=64, readonly=True,
                                  states={'esborrany': [('readonly', False)],
                                          'validar': [('readonly', False),
                                                      ('required', True)],
                                    'modcontractual': [('readonly', False),
                                                       ('required', True)]}),
      'pagador': fields.many2one('res.partner', u'Raó fiscal', select=True,
                                 readonly=True, size=40,
                                 states={
                                     'esborrany': [('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]}),
      'pagador_nif': fields.related('pagador', 'vat', type='char',
                                    string='NIF fiscal', readonly=True),
      'direccio_pagament': fields.many2one('res.partner.address',
                                  u'Adreça fiscal', readonly=True,
                                  ondelete='restrict',
                                  states={
                                    'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False),
                                                ('required', True)],
                                    'modcontractual': [('readonly', False),
                                                       ('required', True)]
                                  }),

      'notificacio': fields.selection(NOTIFICACIO_SELECTION,
                                         u'Persona notificació', readonly=True,
                                         states={
                                    'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False),
                                                ('required', True)],
                                    'modcontractual': [('readonly', False),
                                                       ('required', True)]
                                    }),
      'altre_p': fields.many2one('res.partner', 'Contacte alternatiu',
                                   ondelete='restrict', readonly=True,
                                   states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False)],
                                        'modcontractual': [('readonly', False)]
                                  }, size=40),
      'direccio_notificacio': fields.many2one(
                                    'res.partner.address',
                                    u'Adreça notificació',
                                    readonly=True,ondelete='restrict',
                                    states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                        }),
      'bank': fields.many2one('res.partner.bank', 'Compte bancari',
                                ondelete='restrict', readonly=True,
                                states={
                                    'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False)],
                                    'modcontractual': [('readonly', False)]
                                }),
      'propietari_bank': fields.related('bank', 'owner_name', type='char',
                                        size=128,
                                        string='Propietari compte bancari',
                                        readonly=True),
      'tipo_pago': fields.many2one('payment.type', 'Tipo de pago',
                                   readonly=True,
                                   states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                    }),
      'llista_preu': fields.many2one('product.pricelist',
                                     'Tarifa Comercialitzadora',
                                     domain=[('type', '=', 'sale')],
                                     readonly=True,
                                     states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                     }),
      'versio_primera_factura': fields.many2one('product.pricelist.version',
                                                u'Versió primera facturació',
                                                help="Estableix la versió de "
                                              "la llista de preus que "
                                              "s'utilitzarà per la primera "
                                              "facturació.",
                                                  readonly=True,
                                        states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', False)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', False)]
                                        }),
        'facturacio_endarrerida': fields.function(_ff_fact_endarrerida,
                                                  method=True, type='boolean',
                                                  string='Facturació '
                                                         'endarrerida',
                                                  fnct_search=_search_fact_endarrerida,
                                                  readonly=True),
        'expected_consumption': fields.float('Consum pactat', readonly=True,
             help="Aquest camp calcula el consum total pactat per contractes "
                  "eventuals sense comptador"),
        'deposit': fields.function(
            _ff_deposit, method=True, type='float', string=u'Dipòsit'
        )
    }

    _defaults = {
      'facturacio': lambda *a: 1,
      'facturacio_potencia': lambda *a: 'icp',
      'lectura_en_baja': lambda *a: False,
    }

    def _cnt_lot_data_alta(self, cursor, uid, ids, context=None):
        for polissa in self.browse(cursor, uid, ids, context=context):
            if (polissa.data_alta and
                polissa.lot_facturacio and
                polissa.data_alta >
                polissa.lot_facturacio.data_final):
                return False
        return True

    def _cnt_ccc_pagador(self, cursor, uid, ids, context=None):
        '''returns false if the ccc is not from the pagador'''
        for polissa in self.browse(cursor,  uid, ids, context=context):
            if (not polissa.state in CONTRACT_IGNORED_STATES +
                                          ['baixa', 'modcontractual'] and
                polissa.pagador and
                polissa.bank and
                polissa.pagador.id != polissa.bank.partner_id.id):
                return False
        return True

    _constraints = [(_cnt_lot_data_alta,
                     u"Error: No es pot assignar un lot "
                     u"anterior a la data d\'alta de la pòlissa",
                     ['data_alta', 'lot_facturacio']),
                    (_cnt_ccc_pagador,
                     _(u"Error: El compte bancari no pertany al pagador"),
                     ['bank', 'pagador']),
                    ]

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'facturacio': fields.selection(FACTURACIO_SELECTION,
                                     'Facturació', required=True),
        'lectura_en_baja': fields.boolean('Lectura en baixa', required=True),
        'trafo': fields.float('Trafo KVA', required=True),
        'facturacio_potencia':
          fields.selection(FACTURACIO_POTENCIA_SELECTION,
                           'Facturación Potencia', required=True),
        'property_unitat_potencia': fields.property(
            'product.uom',
            type='many2one',
            relation='product.uom',
            string="Unitat de facturació potència",
            method=True,
            view_load=True,
            domain="[('category_id.name', '=', 'POT ELEC')]",
            help="Amb quina unitat es vol facturar la potència",
            required=True
         ),
        'pagador': fields.many2one('res.partner', u'Raó fiscal', required=True),
        'notificacio': fields.selection(NOTIFICACIO_SELECTION,
                                         u'Persona notificació', required=True),
        'altre_p': fields.many2one('res.partner', 'Contacte alternatiu'),
        'direccio_notificacio': fields.many2one('res.partner.address',
                                                u'Adreça notificació'),
        'direccio_pagament': fields.many2one('res.partner.address',
                                             u'Adreça fiscal'),
        'tipo_pago': fields.many2one('payment.type', 'Tipo de pago',
                                     required=True),
        'bank': fields.many2one('res.partner.bank', 'Compte bancari'),
        'llista_preu': fields.many2one('product.pricelist',
                                       'Tarifa Comercialitzadora'),
        'expected_consumption': fields.float('Consum pactat', readonly=True,
                                     help="Aquest camp calcula el consum total pactat per contractes "
                                          "eventuals sense comptador")
    }

GiscedataPolissaModcontractual()


class GiscedataPolissaAutofix(osv.osv):

    _name = 'giscedata.polissa.autofix'

    _columns = {
        "code": fields.char("Codi", size=4, required=1),
        "active": fields.boolean("Activa"),
        "description": fields.text("Descripció"),
        "check_method": fields.char("Metode de test", size=60),
        "fix_method": fields.char("Metode de autocorrecció", size=60)
    }

GiscedataPolissaAutofix()
