# -*- coding: utf-8 -*-
import logging
from datetime import datetime

import click
from progressbar import ProgressBar, ETA, Percentage, Bar


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def get_invoices_to_update(connection):
    logger = logging.getLogger('update_potencia_kwdia')

    logger.info('Getting the ids of the invoices to update')
    fact_obj = connection.model('giscedata.facturacio.factura')
    fact_ids = fact_obj.search([])
    logger.info('Got the ids of the invoices to update')

    return fact_ids


def update_potencia_kwdia_on_id(connection, fact_id):
    fact_obj = connection.model('giscedata.facturacio.factura')
    line_obj = connection.model('giscedata.facturacio.factura.linia')

    fact_vals = fact_obj.read(fact_id, ['linia_ids'])
    if fact_vals['linia_ids']:
        # We update the quantity with the same value to prompt the trigger
        # without any actual change
        quantity = line_obj.read(
            fact_vals['linia_ids'][0], ['quantity']
        )['quantity']
        line_obj.write([fact_vals['linia_ids'][0]], {'quantity': quantity})


def update_potencia_kwdia_on_ids(connection, fact_ids):
    logger = logging.getLogger('update_potencia_kwdia')

    # define pbar
    widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
    total_inv = len(fact_ids)
    pbar = ProgressBar(widgets=widgets, maxval=total_inv).start()
    correct = 0
    incorrect = 0

    logger.info('Starting to update the invoices')
    logger.info('Total invoices to process: {0}'.format(total_inv))
    for fact_id in fact_ids:
        try:
            update_potencia_kwdia_on_id(connection, fact_id)
            logger.info('Succsessful with id {0}!'.format(fact_id))
            correct += 1
        except (KeyboardInterrupt, SystemExit):
            logger.error('Stoped at id {0}!'.format(fact_id))
            raise
        except Exception as e:
            logger.warning('Failed with id {0}: {1}'.format(fact_id, e))
            incorrect += 1
        finally:
            pbar.update(correct + incorrect)
    logger.info('Finished updating the invoices!')
    logger.info(
        '{0} correct and {1} incorrect of a total of {2}'.format(
            correct, incorrect, total_inv
        )
    )

    pbar.finish()


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', help='Contrasenya usuari ERP', prompt=True,
              hide_input=True)
@click.option('-d', '--database', help='Nom de la base de dades')
def update_potencia_kwdia(**kwargs):
    from erppeek import Client
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])

    setup_log('update_potencia_kwdia', '/tmp/update_potencia_kwdia.log')
    logger = logging.getLogger('update_potencia_kwdia')
    logger.info('#######Start now {0}'.format(datetime.today()))

    fact_ids = get_invoices_to_update(c)
    update_potencia_kwdia_on_ids(c, fact_ids)
    logger.info('#######End process at {0}'.format(datetime.today()))


if __name__ == '__main__':
    update_potencia_kwdia()
