# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import os
from datetime import datetime
from tools.translate import _

from osv import osv, fields
import tools

def _escollir_data(self, cr, uid, data=None, context=None):
    cr.execute("""select distinct period_id,p.name from account_invoice f inner
                join account_period p on f.period_id = p.id order by p.name desc
                limit 12""")
    return [(a[0], a[1]) for a in cr.fetchall()]

class WizardGiscedataFacturacioResum(osv.osv_memory):
    """Wizard per realitzar el resum de facturació.
    """
    _name = 'wizard.giscedata.facturacio.resum'
    def db_origin(self):
        raise NotImplementedError
        
    def action_exec_script(self, cursor, uid, ids, context=None):
        """Llença l'script per l'informe.
        """
        #./resum_distribuidora.py -o /tmp/resum_distri_201108.csv -f -e 8 -D -p 8059 -u root -w root -d LERSA_distribuidora
        wizard = self.browse(cursor, uid, ids[0], context)
        exe = 'addons/giscedata_facturacio/scripts/resum_distribuidora.py'
        periode = wizard.period 
        tmp = tempfile.mkstemp()[1]
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] 
        orig = self.db_origin()
        args = [exe, '-f', '-e', periode, '-%s' % orig, '-d', cursor.dbname, '-p', port]
        args += ['-u', user.login, '-w', user.password, '-o', tmp]
        subprocess.call(map(str, args))
        report = base64.b64encode(open(tmp).read())
        os.unlink(tmp)
        period_name = self.pool.get('account.period').read(cursor, uid, 
                                                      periode, ['name'])['name']
        filename = ('resumen_%s_%s.csv' % 
                        ((orig=='D' and 'distribuidora' or 'comercializadora'),
                        period_name.replace('/', '_')))
        wizard.write({'name': filename, 'file': report, 'state': 'done'})

    _columns = {
        'period': fields.selection(_escollir_data, 'Periode', 
                                   requiered=True),
        'file': fields.binary('Fitxer'),
        'state': fields.char('State', size=16),
        'name': fields.char('File name', size=64),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardGiscedataFacturacioResum()

