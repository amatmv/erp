# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardAutoFixLectures(osv.osv_memory):

    _name = 'wizard.auto.fix.lectures'

    def action_check_autofix(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        clot_ids = context.get('active_ids', [])
        #Discard the ones not in 'obert'
        search_params = [('state', 'in', ('obert',)), ('id', 'in', clot_ids)]
        final_clot_info = clot_obj.search_reader(cursor, uid, search_params, ['polissa_id'])
        pol_obj = self.pool.get("giscedata.polissa")
        res = {}
        pids = []
        for clot_info in final_clot_info:
            checks = pol_obj.check_autofixs(cursor, uid, clot_info['polissa_id'])
            if len(checks):
                pids.append(clot_info['polissa_id'])
                for check in checks:
                    res.setdefault(check[1], []).append(clot_info['polissa_id'])

        text_checks = ""
        for check_info in res:
            text_checks += " * {0} -> {1}\n".format(check_info, len(res[check_info]))
        text = _(
            u"De {0} contractes n'hi havia {1} en estat obert. Dels oberts se'n poden corregir {2}:\n\n{3}"
        ).format(len(clot_ids), len(final_clot_info), len(pids), text_checks)
        self.write(cursor, uid, ids, {'state': 'fix', 'info': text})
        return True

    def action_do_autofix(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        clot_ids = context.get('active_ids', [])
        #Discard the ones not in 'obert'
        search_params = [('state', 'in', ('obert',)), ('id', 'in', clot_ids)]
        final_clot_info = clot_obj.search_reader(cursor, uid, search_params, ['polissa_id'])
        pol_obj = self.pool.get("giscedata.polissa")
        res = {}
        pids = []
        for clot_info in final_clot_info:
            checks = pol_obj.do_autofixs(cursor, uid, clot_info['polissa_id'])
            if len(checks):
                pids.append(clot_info['polissa_id'])
                for check in checks:
                    res.setdefault(check[1], []).append(clot_info['polissa_id'])

        text_checks = ""
        for check_info in res:
            text_checks += " * {0} -> {1}\n".format(check_info, res[check_info])
        text = _(
            u"S'ha corregit els següents contractes:\n\n{0}"
        ).format(text_checks)
        self.write(cursor, uid, ids, {'state': 'end', 'info2': text})
        return True

    _columns = {
        'state': fields.selection([('init', 'Init'), ('fix', 'End'), ('end', 'End')], 'Estat'),
        'info': fields.text(_(u'Informació'), readonly=True),
        'info2': fields.text(_(u'Informació'), readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init'
    }

WizardAutoFixLectures()
