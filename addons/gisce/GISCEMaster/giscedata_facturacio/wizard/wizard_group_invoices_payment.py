# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class WizardGroupInvoicesPayment(osv.osv_memory):
    _name = 'wizard.group.invoices.payment'

    _columns = {
        'amount_total': fields.float('Total', readonly=True),
        'number_of_invoices': fields.integer('Number of invoices', readonly=True),
        'remove_lot': fields.boolean(
            'Borrar del lot', help=_(u'Marcar aquesta casella per treure les '
                                     u'factures agrupades del lot i '
                                     u'gestionar-les manualment.')
        )
    }

    def get_total_amount(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        group_ids = context.get('active_ids', [])
        obj = self.pool.get(context['model'])
        types = {
            'out_invoice': 1,
            'in_invoice': -1,
            'out_refund': -1,
            'in_refund': 1
        }
        amount = 0
        num = 0
        for invoice in obj.browse(cursor, uid, group_ids, context=context):
            direction = types[invoice.type]
            amount += (direction * invoice.amount_total)
            num += 1
        return num, amount

    def _default_number_of_invoices(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return len(context.get('active_ids', []))

    def _default_amount_total(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return self.get_total_amount(cursor, uid, [], context=context)[1]

    _defaults = {
        'number_of_invoices': _default_number_of_invoices,
        'amount_total': _default_amount_total,
        'remove_lot': lambda *a: False
    }

    def group_invoices(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        obj = self.pool.get(context['model'])
        group_ids = context.get('active_ids', [])
        num, amount = wiz.get_total_amount(context=context)
        obj.make_move_differences(cursor, uid, group_ids, context=context)
        wiz.write({'amount_total': amount, 'number_of_invoices': num})
        if wiz.remove_lot:
            obj.write(cursor, uid, group_ids, {'lot_facturacio': False})


WizardGroupInvoicesPayment()
