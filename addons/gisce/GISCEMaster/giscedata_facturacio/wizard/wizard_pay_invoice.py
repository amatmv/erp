# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    Copyright (C) 2011 GISCE Enginyeria (http://gisce.net). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

"""Wizard pel pagament de factures.

* Migrat a osv_memory.
* Ús de ResConfig per configurar el wizard.

"""

import time
from osv import fields, osv
from tools.translate import _

from account_invoice_base.account_invoice import  amount_grouped

class WizardPayInvoice(osv.osv_memory):
    """Wizard class
    """
    _name = "facturacio.pay.invoice"

    def _ff_parcials(self, cursor, uid, context=None):
        """Funció que mira si acceptem pagaments parcials o no.
        """
        if not context:
            context = {}
        config = self.pool.get('res.config')
        return int(config.get(cursor, uid, 'facturacio_pagaments_parcials',
                   '0'))

    def get_invoice_id(self, cursor, uid, ids, factura_id, context=None):
        """Retorna l'id de l'account.invoice
        """
        factura = self.pool.get('giscedata.facturacio.factura')
        invoice_id = factura.read(cursor, uid, factura_id,
                                  ['invoice_id'])
        if not invoice_id:
            return context.get('active_id', False)
        return invoice_id['invoice_id'][0]

    def _get_amount(self, cursor, uid, context=None):
        """Funció per obtenir el valor per defecte d'amount
        """
        if not context:
            context = {}

        factura_id = context.get('active_ids')
        if isinstance(factura_id, list) or isinstance(factura_id, tuple):
            factura_id = factura_id[0]

        factura = self.pool.get('giscedata.facturacio.factura').browse(
                    cursor, uid, factura_id)
        return amount_grouped(factura.invoice_id)

    def _get_visual_amount(self, cursor, uid, context=None):
        """Funció per obtenir el valor per defecte d'amount
        """
        if not context:
            context = {}

        factura_id = context.get('active_ids')
        if isinstance(factura_id, list) or isinstance(factura_id, tuple):
            factura_id = factura_id[0]


        factura = self.pool.get('giscedata.facturacio.factura').browse(
                    cursor, uid, factura_id)
        amount = amount_grouped(factura.invoice_id)
        return amount

    def _get_n_factura(self, cursor, uid, context=None):
        """Funció per obtenir el número de la factura que paguem
        """
        if not context:
            context = {}
        factura_id = context.get('active_ids')
        if isinstance(factura_id, list) or isinstance(factura_id, tuple):
            factura_id = factura_id[0]
        factura = self.pool.get('giscedata.facturacio.factura').browse(
                                cursor, uid, factura_id)
        if factura.group_move_id:
            name = _(u'Pagament agrupat {0}').format(factura.group_move_id.ref)
        else:
            name = factura.number
        return name

    def _get_periode(self, cursor, uid, context=None):
        """Funció per obtenir el període del pagament
        """
        if not context:
            context = {}
        if 'data_pagament' not in context:
            data_actual = time.strftime('%Y-%m-%d')
        else:
            data_actual = context['data_pagament']
        search_params = [
            ('date_start', '<=', data_actual),
            ('date_stop', '>=', data_actual),
        ]
        acc_per_obj = self.pool.get('account.period')
        period_id = acc_per_obj.search(cursor, uid, search_params)
        if not period_id:
            raise osv.except_osv(u'Error', 
                                 _(u"No s'ha trobat un període comptable "
                                   u"per la data actual."))
        return period_id[0]

    def onchange_date(self, cursor, uid, ids, date, context=None):
        """Funció onchange pel camp data i així actualitzad el període
        """
        res = {'value': {}, 'domain': {}}
        context.update({'data_pagament': date})
        res.update({'value': {'period_id': self._get_periode(cursor, uid,
                                                             context)}})
        return res

    def _get_journal(self, cursor, uid, context=None):
        """Funció per obtenir el diari de pagament
        """
        if not context:
            context = {}
        journal_obj = self.pool.get('account.journal')
        search_params = [
            ('code', '=', 'CAJA'),
        ]
        journal_id = journal_obj.search(cursor, uid, search_params)
        if len(journal_id):
            return journal_id[0]
        return False

    def _get_avis_remesada(self, cursor, uid, context=None):
        '''Comprova si la factura/es té remesa i no està retornada'''
        conf = self.pool.get('res.config')
        if int(conf.get(cursor, uid, 'fact_pagar_avis_remesada', 0)):
            fra_obj = self.pool.get('giscedata.facturacio.factura')
            params = [('payment_order_id', '!=', False),
                      ('devolucio_id', '=', False),
                      ('id', 'in', context.get('active_ids', []))]
            return fra_obj.search(cursor, uid, params)
        return []

    def action_pay_and_reconcile(self, cursor, uid, ids, context=None):
        """Pagament de la factura.
        """
        if not context:
            context = {}

        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]

        wiz = self.pool.get('facturacio.pay.invoice').browse(cursor, uid, ids)
        wiz.write({'state': 'pay_and_reconcile'})

        invoice_id = wiz.get_invoice_id(context.get('active_id', False),
                                        context)

        cur_obj = self.pool.get('res.currency')

        invoice = self.pool.get('account.invoice').browse(cursor, uid,
                                                          invoice_id, context)
        if invoice.group_move_id:
            # We will try to get the invoice of the group that has the same
            # account than the group move
            group_line = None
            for l in invoice.group_move_id.line_id:
                if not l.invoice:
                    group_line = l
                    break
            if group_line:
                account_id = group_line.account_id.id
                line_invoices = [l.invoice for l in invoice.group_move_id.line_id if l.invoice and l.invoice.account_id.id == account_id]
                if len(line_invoices):
                    invoice = line_invoices[0]

        journal = wiz.journal_id
        # Compute the amount in company's currency, with the journal currency
        # (which is equal to payment currency) 
        # when it is needed :  If payment currency (according to selected 
        # journal.currency) is <> from company currency
        cur_diff = False
        if journal.currency and \
            invoice.company_id.currency_id.id != journal.currency.id:
            ctx = {'date': wiz.date}
            amount = cur_obj.compute(cursor, uid, journal.currency.id, 
                                     invoice.company_id.currency_id.id,
                                     wiz.amount, context=ctx)
            currency_id = journal.currency.id
            # Put the paid amount in currency, and the currency, in the context
            # if currency is different from company's currency
            wiz.write({'amount': amount, 'currency_id': currency_id,
                      'company_currency_id': invoice.company_id.currency_id.id})
            cur_diff = True
            
        if not journal.currency and \
            invoice.company_id.currency_id.id != invoice.currency_id.id and \
            (not cur_diff):
            ctx = {'date': wiz.date}
            amount = cur_obj.compute(cursor, uid, invoice.currency_id.id, 
                                     invoice.company_id.currency_id.id, 
                                     amount, context=ctx)
            currency_id = invoice.currency_id.id
            # Put the paid amount in currency, and the currency, in the context
            # if currency is different from company's currency
            wiz.write({'amount': amount, 'currency_id': currency_id,
                      'company_currency_id': invoice.company_id.currency_id.id})
        acc_id = journal.default_credit_account_id and \
            journal.default_credit_account_id.id
        if not acc_id:
            raise "No acc_id"

        woff_acc = wiz.writeoff_acc_id and wiz.writeoff_acc_id.id or False
        woff_journal = wiz.writeoff_journal_id and \
                       wiz.writeoff_journal_id.id or False

        # posem la data del pagament
        context.update({'date_p': wiz.date})
        self.pool.get('account.invoice').pay_and_reconcile(cursor, uid,
                [invoice.id],
                wiz.amount, acc_id, wiz.period_id.id, wiz.journal_id.id, 
                woff_acc, wiz.period_id.id,
                woff_journal, context, wiz.name)
        wiz.write({'state': 'end'})

        return True

    def action_partial_payment(self, cursor, uid, ids, context=None):
        """Marquem que fem pagament parcial per mostrar la vista de
        desquadre
        """
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        wiz = self.pool.get('facturacio.pay.invoice').browse(cursor, uid, ids)
        wiz.write({'state': 'partial_payment'})
        return True

    def action_wo_check(self, cursor, uid, ids, context=None):
        """Acció per comprovar què es pot pagar sencera 
        """
        if not context:
            context = {}
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        wiz = self.pool.get('facturacio.pay.invoice').browse(cursor, uid, ids)
        invoice_id = wiz.get_invoice_id(context.get('active_id', False),
                                        context)
        invoice = self.pool.get('account.invoice').browse(cursor, uid,
                                                          invoice_id, context)

        invoice_not_payable = invoice.is_not_payable_manually()
        if invoice_not_payable:
            raise osv.except_osv(
                invoice_not_payable['title'], invoice_not_payable['message']
            )
        journal = wiz.journal_id

        cur_obj = self.pool.get('res.currency')
        # Here we need that:
        #    The invoice total amount in company's currency <> paid amount in i
        #    company currency (according to the correct day rate, invoicing rate
        #    and payment rate are may be different) => Ask to a write-off of the
        #    difference. This could happen even if both amount are equal,
        #    because if the currency rate
        # Get the amount in company currency for the invoice (according to move
        # lines)
        inv_amount_company_currency = 0
        if invoice.group_move_id:
            inv_amount_company_currency = amount_grouped(invoice)
        else:
            move_lines = invoice.move_id.line_id
            for aml in move_lines:
                if aml.account_id.id == invoice.account_id.id or \
                    aml.account_id.type in ('receivable', 'payable'):
                    inv_amount_company_currency += aml.debit
                    inv_amount_company_currency -= aml.credit
        inv_amount_company_currency = abs(inv_amount_company_currency)

        # Get the current amount paid in company currency
        if journal.currency and \
            invoice.company_id.currency_id.id != journal.currency.id:
            ctx = {'date': wiz.date}
            amount_paid = cur_obj.compute(cursor, uid, journal.currency.id,
                                          invoice.company_id.currency_id.id,
                                          wiz.amount, round=True,
                                          context=ctx)
        else:
            amount_paid = wiz.amount
        # Get the old payment if there are some
        if invoice.payment_ids and not invoice.group_move_id:
            debit=credit=0.0
            for payment in invoice.payment_ids:
                debit+=payment.debit
                credit+=payment.credit
            amount_paid+=abs(debit-credit)

        amount_paid = abs(amount_paid)
            
        # Test if there is a difference according to currency rouding setting
        if self.pool.get('res.currency').is_zero(cursor, uid,
                                            invoice.company_id.currency_id,
                (amount_paid - inv_amount_company_currency)):
            return wiz.action_pay_and_reconcile(context)
        return True

    _columns = {
        'amount': fields.float('Quantitat a pagar', required=True),
        'visual_amount': fields.float('Quantitat a pagar', readonly=True),
        'name': fields.char('Nom del registre', size=64, required=True),
        'date': fields.date(
            'Data de pagament', required=True,
            write=[
                'account.group_account_manager_dates'
            ]
        ),
        'journal_id': fields.many2one('account.journal', 'Diari',
                                      required=True,
                                      domain=[('type','=','cash')]),
        'period_id': fields.many2one(
            'account.period', 'Periode', required=True,
            write=[
                'account.group_account_manager_dates'
            ]
        ),
        'writeoff_journal_id': fields.many2one('account.journal',
                                               'Diari de desajustaments'),
        'writeoff_acc_id': fields.many2one('account.account',
                                           'Compte de desajustaments'),
        'comment': fields.char('Comentari', size=64),
        'analytic_id': fields.many2one('account.analytic.account',
                                       'Compte analític'),
        'parcials': fields.boolean('Parcials'),
        'avis_remesada': fields.boolean('Aviso'),
        'state': fields.selection([('init', 'Init'), ('end', 'Final'),
                                   ('pay_and_reconcile', 'Pagar i reconciliar'),
                                   ('wo_check', 'WriteOFf')],
                                   'Estat')
    }
    _defaults = {
        'date': lambda *a: time.strftime('%Y-%m-%d'),
        'amount': _get_amount,
        'visual_amount': _get_visual_amount,
        'state': lambda *a: 'init',
        'writeoff_journal_id': lambda *a: False,
        'writeoff_acc_id': lambda *a: False,
        'parcials': _ff_parcials,
        'name': _get_n_factura,
        'period_id': _get_periode,
        'journal_id': _get_journal,
        'avis_remesada': _get_avis_remesada,
    }

WizardPayInvoice()

