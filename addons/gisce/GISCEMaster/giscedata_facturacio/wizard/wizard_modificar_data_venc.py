# -*- coding: utf-8 -*-
from tools import config
from osv import osv, fields


class WizardModificarDataVenc(osv.osv_memory):
    _name = "wizard.modificar.data.venc"

    def modificar(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        wizard = self.browse(cursor, uid, ids[0], context)

        for factura_id in context['active_ids']:
            factura_obj.defer_payment(
                cursor, uid, factura_id, wizard.journal_id.id, wizard.nova_data
            )

        wizard.write({'state': 'end'})

        return True

    def _default_info(self, cursor, uid, context=None):
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        s=''

        for fact in factura_obj.read(cursor, uid, context['active_ids'], ['number']):
            s+='{0}\n'.format(fact['number'])

        return s

    def _defaut_journal_id(self, cursor, uid, context=None):
        if context is None:
            context = {}

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        ids = context.get('active_ids', [])

        if len(ids) == 1:
            factura = factura_obj.browse(cursor, uid, ids[0], context=context)
            return factura.journal_id.id
        return False

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'nova_data': fields.date('Nova data de venciment', required=True),
        'journal_id': fields.many2one(
            'account.journal', 'Diari', required=True
        ),
        'info': fields.text('Factures a modificar'),
    }

    _defaults = {
        'journal_id': _defaut_journal_id,
        'state': lambda *a: 'init',
        'info': _default_info,
    }

WizardModificarDataVenc()
