# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta


class WizardMarcarFacturat(osv.osv_memory):

    _name = 'wizard.marcar.facturat'

    def action_marcar_facturat(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        clot_obj = self.pool.get("giscedata.facturacio.contracte_lot")
        clot_obj.facturar_incident(cursor, uid, context.get('active_ids', []), context=context)
        return {}

    _columns = {
        'info': fields.text(),
    }

    _defaults = {
        'info': lambda cursor, uid, ids, context={}: _(
            u'Es passaran a "Facturada" les polisses seleccionades que es trobin en estat '
            u'"Facturat amb incidencies" i es marcaran com a "Incidencia Revisada".\n\n'
            u'S\'han seleccionat {0} polisses'
        ).format(len(context.get('active_ids', []))),
    }

WizardMarcarFacturat()
