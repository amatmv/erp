# coding=utf-8
from datetime import datetime

from osv import osv, fields
from tools.translate import _


class WizardRefundDeposit(osv.osv_memory):
    _name = 'wizard.refund.deposit'

    def _default_deposit(self, cursor, uid, context=None):
        if context is None:
            context = {}
        active_id = context.get('active_id')
        if active_id:
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.read(cursor, uid, active_id, ['deposit'])
            return abs(polissa['deposit'])
        return False

    def _default_polissa_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('active_id')

    def _default_partner_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        active_id = context.get('active_id')
        if active_id:
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.read(cursor, uid, active_id, ['titular'])
            return polissa['titular'][0]
        return False

    def _default_bank_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        active_id = context.get('active_id')
        if active_id:
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.read(cursor, uid, active_id, ['bank'])
            if polissa['bank']:
                return polissa['bank'][0]
        return False

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Titular'),
        'polissa_id': fields.many2one('giscedata.polissa', u'Pòlissa'),
        'date': fields.date('Data'),
        'deposit': fields.float(u'Dipòsit'),
        'bank_id': fields.many2one('res.partner.bank', 'Cuenta'),
        'payment_order_id': fields.many2one('payment.order', 'Remesa'),
        'journal_id': fields.many2one('account.journal', 'Diari'),
        'add_to_order': fields.boolean('Afegir a remesa'),
        'state': fields.char('Estat', size=50)
    }

    def refund(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        cfg_obj = self.pool.get('res.config')
        product_obj = self.pool.get('product.product')
        move_obj = self.pool.get('account.move')
        ml_obj = self.pool.get('account.move.line')
        period_obj = self.pool.get('account.period')
        payment_line_obj = self.pool.get('payment.line')
        product_id = int(cfg_obj.get(cursor, uid, 'deposit_product_id'))
        acc_id = product_obj.get_account(cursor, uid, product_id, 'income')
        name = _(u'Devolució deposit garantia {}').format(wiz.polissa_id.name)
        period_id = period_obj.find(cursor, uid, wiz.date, wiz.date)[0]
        if wiz.deposit < 0:
            raise osv.except_osv(
                _(u'Error Usuari'),
                _(u'No es pot retornar una quantitat negativa. '
                  u'La devolució del dipósit sempre ha de ser una '
                  u'quantitat positiva')
            )
        move_id = move_obj.create(cursor, uid, {
            'ref': wiz.polissa_id.name,
            'journal_id': wiz.journal_id.id,
            'period_id': period_id,
            'date': wiz.date
        }, context=context)
        ml_obj.create(cursor, uid, {
            'debit': wiz.deposit,
            'credit': 0,
            'account_id': acc_id,
            'partner_id': wiz.partner_id.id,
            'ref': wiz.polissa_id.name,
            'date': wiz.date,
            'name': name,
            'move_id': move_id,
            'product_id': product_id
        })
        payment_move_line = ml_obj.create(cursor, uid, {
            'debit': 0,
            'credit': wiz.deposit,
            'account_id': wiz.partner_id.property_account_receivable.id,
            'partner_id': wiz.partner_id.id,
            'ref': wiz.polissa_id.name,
            'date': wiz.date,
            'name': name,
            'move_id': move_id,
            'product_id': product_id
        })
        if wiz.add_to_order:
            payment_line_obj.create(cursor, uid, {
                'name': name,
                'order_id': wiz.payment_order_id.id,
                'move_line_id': payment_move_line,
                'amount_currency': wiz.deposit,
                'communication': name,
                'bank_id': wiz.bank_id.id,
                'partner_id': wiz.partner_id.id,
                'state': 'normal',
                'date':  wiz.payment_order_id.date_planned
            })
        wiz.write({'state': 'step2'})

    def button_show_order(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        return {
            'domain': [('id', '=', wiz.payment_order_id.id)],
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'payment.order',
            'type': 'ir.actions.act_window'
        }

    _defaults = {
        'state': 'step1',
        'deposit': _default_deposit,
        'polissa_id': _default_polissa_id,
        'partner_id': _default_partner_id,
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'bank_id': _default_bank_id,
        'add_to_order': 0
    }

WizardRefundDeposit()

