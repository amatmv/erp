# -*- coding: utf-8 -*-

import time
from osv import osv

import netsvc
import pooler
import wizard

pay_form = """<?xml version="1.0"?>
<form string="Pagar factures">
    <field name="amount"/>
    <field name="name"/>
    <field name="date"/>
    <field name="journal_id"/>
    <field name="period_id"/>
</form>
"""

pay_fields = {
    'amount': {'string': 'Amount to pay', 'type': 'float', 
               'digits': (16, 2), 'readonly': True, 'required': True},
    'name': {'string': 'Entry name', 'type': 'char', 'size': 64,
             'required': True,
             'default': lambda *a: 'Pagament de factures a caixa.'},
    'date': {'string': 'Payment date', 'type': 'date', 'required': True,
             'default': lambda *a: time.strftime('%Y-%m-%d')},
    'journal_id': {'string': 'Journal/Payment Mode', 'type': 'many2one',
                   'relation': 'account.journal', 'required': True,
                   'domain': [('type', '=', 'cash')]},
    'period_id': {'string': 'Period', 'type': 'many2one', 'required': True,
                  'relation': 'account.period'}
}

def _total_a_pagar(self, cursor, uid, data, context=None):
    """Funció per calcular quant s'ha de pagar en total.
    """
    amount = 0.0
    pool = pooler.get_pool(cursor.dbname)
    factures_obj = pool.get('giscedata.facturacio.factura')
    for factura in factures_obj.browse(cursor, uid, data['ids']):
        amount += factura.residual
    return {'amount': amount}

def _pagar_factures(self, cursor, uid, data, context=None):
    """Acció de pagar factures.

    Itera sobre cadascuna de les factures i executa el wizard de
    pagar factura.
    """
    if not context:
        context = {}
    form = data['form']
    pool = pooler.get_pool(cursor.dbname)
    factures_obj = pool.get('giscedata.facturacio.factura')
    invoice_o = pool.get('account.invoice')
    journal_obj = pool.get('account.journal')
    period_id = form['period_id']
    pay_journal_id = form['journal_id']
    journal = journal_obj.browse(cursor, uid, pay_journal_id)
    pay_account_id = journal.default_credit_account_id.id
    writeoff_acc_id = False
    writeoff_period_id = False
    writeoff_journal_id = False
    name = form['name']
    context['date_p'] = form['date']
    conf_o = pool.get('res.config')
    unique_account_move = bool(int(conf_o.get(
        cursor, uid, 'pay_invoices_unique_account_move', '0'
    )))
    if unique_account_move:
        inv_ids = factures_obj.read(cursor, uid, data['ids'], ['invoice_id'])
        inv_ids = [x['invoice_id'][0] for x in inv_ids]
        invoice_o.pay_with_unique_account_move(
            cursor, uid, inv_ids, name, pay_journal_id,
            date=form['date'],
            pay_account_id=pay_account_id,
            period_id=period_id,
            writeoff_acc_id=writeoff_acc_id,
            writeoff_period_id=writeoff_period_id,
            writeoff_journal_id=writeoff_journal_id,
            context=context
        )
        return {}

    for factura in factures_obj.browse(cursor, uid, data['ids']):
        invoice_not_payable = factura.invoice_id.is_not_payable_manually()
        if invoice_not_payable:
            raise osv.except_osv(
                invoice_not_payable['title'], invoice_not_payable['message']
            )
        pay_amount = factura.residual
        factura.invoice_id.pay_and_reconcile(pay_amount, pay_account_id,
                                             period_id, pay_journal_id,
                                             writeoff_acc_id,
                                             writeoff_period_id,
                                             writeoff_journal_id, context, name)
    return {}

class WizardPagarGrupFactures(wizard.interface):
    """Wizard per pagar un grup de factures alhora
    """

    states = {
        'init': {
            'actions': [_total_a_pagar],
            'result': {'type': 'form', 'arch': pay_form, 'fields': pay_fields,
                       'state': [('end', 'Cancel'), ('pay', 'Pagar')]}
        },
        'pay': {
            'actions': [_pagar_factures],
            'result': {'type': 'state', 'state': 'end'}

        }
    }

WizardPagarGrupFactures('facturacio_pagar_grup_factures')

