# -*- coding: utf-8 -*-

from osv import osv, fields


class WizardAfegirPolissesLot(osv.osv_memory):

    def action_assignar_lot_facturacio(self, cursor, uid, ids, context=None):
        polissa_obj = self.pool.get('giscedata.polissa')
        cont_lot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        polissa_ids = context['active_ids']
        self_fields = ['lot_facturacio']
        self_vals = self.read(
            cursor, uid, ids, self_fields, context=context
        )[0]
        lot_desti_id = self_vals['lot_facturacio']
        polissa_vals = polissa_obj.read(
            cursor, uid, polissa_ids, ['lot_facturacio'], context=context
        )
        for polissa in polissa_vals:
            polissa_id = polissa['id']
            polissa_lot = polissa['lot_facturacio']
            if polissa_lot:
                lot_origen_id = polissa_lot[0]
                # get contracte lot
                search_vals = [
                    ('lot_id', '=', lot_origen_id),
                    ('polissa_id', '=', polissa_id)
                ]
                cont_lot_ori_id = cont_lot_obj.search(
                    cursor, uid, search_vals, context=context
                )[0]
            else:
                # no current lot
                cont_lot_ori_id = False

            polissa_obj.moure_al_lot(
                cursor, uid, cont_lot_ori_id, polissa_id, lot_desti_id,
                context=context
            )
        return {}

    _name = 'wizard.afegir.polisses.lot'

    _columns = {
        'lot_facturacio': fields.many2one(
            'giscedata.facturacio.lot', 'Lot de facturació', required=True
        )
    }

    _defaults = {

    }


WizardAfegirPolissesLot()
