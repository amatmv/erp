# -*- coding: utf-8 -*-

from osv import osv
from osv import fields
from tools.translate import _

class WizardUndoPayment(osv.osv_memory):
    '''
    Wizard per desfer els pagos d'una factura
    '''
    _name = 'wizard.undo.payment'

    
    def undo_payments(self, cursor, uid, ids, context=None):
        
        if not context:
            context = {}

        model_ids = context.get('active_ids', [])

        if context.get('model',False):
            self.pool.get(context['model']).undo_payment(cursor, 
                                                uid, model_ids, context)    
        else:
            raise osv.except_osv(_('Error'),
                                 _('No es pot desfer el pago sense un model!'))

        return {}

WizardUndoPayment()
