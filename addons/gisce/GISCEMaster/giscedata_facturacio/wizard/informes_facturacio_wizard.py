# -*- coding: utf-8 -*-

"""Wizards d'informes de facturació
"""

import re

from osv import fields, osv
from tools.translate import _

class WizardInformeComptable(osv.osv_memory):
    """Wizard per llençar l'informe comptable
    """
    _name = "wizard.informe.comptable"
    _description = __doc__


    def action_generar_dades(self, cursor, uid, ids, context=None):
        """Omple les taules amb les dades generades
        """
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]

        self.write(cursor, uid, [ids], {'state': 'create'}, context)
        wiz = self.browse(cursor, uid, ids, context)
        informe_obj = self.pool.get('giscedata.facturacio.informe_comptable')
        linia_informe_obj = self.pool.get('giscedata.facturacio.'
                                          'informe_comptable.linia')

        search_params = [('name', '=', wiz.period_id.name)]
        informe = informe_obj.search(cursor, uid, search_params)
        if informe:
            informe_obj.unlink(cursor, uid, informe)
        vals = {
            'name': wiz.period_id.name,
            'period_id': wiz.period_id.id,
        }
        informe = informe_obj.create(cursor, uid, vals)
        wiz.write({'informe_id': informe})
        pat = re.compile('^.*\((?P<nom>.*)\)$')
        tipus_rectificadora = {'B': 'ANULCSUST', 'A': 'ANULADORA',
                               'N': 'NORMAL', 'R': 'RECTIFICADORA'}
        sign = {'B': -1, 'A': -1, 'N': 1, 'R': 1}
        journal_obj = self.pool.get('account.journal')
        search_params = [('code', 'ilike', 'CONCEPTES%')]
        journal = journal_obj.search(cursor, uid, search_params)

        search_params = [('period_id', '=', wiz.period_id.id)]
        if journal:
            search_params.append(('journal_id', 'not in', journal))

        o_factures = self.pool.get('giscedata.facturacio.factura')
        factures = o_factures.search(cursor, uid, search_params)
        for factura in o_factures.browse(cursor, uid, factures, context):
            vals = {}
            linies = {}
            _iter = factura.linia_ids
            energia = {}
            potencia = {}
            vals.setdefault('lloguers', 0)
            sign_ = sign[factura.tipo_rectificadora]
            for l in _iter:
                if l.tipus == 'energia':
                    energia.setdefault(l.name, 0)
                    energia[l.name] += l.quantity 
                elif l.tipus == 'potencia':
                    potencia.setdefault(l.name, 0)
                    potencia[l.name] += l.quantity
                elif l.tipus == 'lloguer':
                    vals['lloguers'] += l.price_subtotal

            taxes = {}
            for t in factura.tax_line:
                taxes[t.name] = t.amount
            vals['tipus_rect'] = tipus_rectificadora[factura.tipo_rectificadora]
            vals['name'] = factura.number
            vals['tarifa'] = factura.tarifa_acces_id.name
            vals['pricelist'] = unicode(
                                    factura.llista_preu.name).encode('utf-8')
            vals['partner'] = unicode(factura.partner_id.name).encode('utf-8')
            for p in ('P1', 'P2', 'P3', 'P4', 'P5', 'P6'):
                vals['ene_%s' % p.lower()] = energia.get(p, 0) * sign_
                vals['pot_%s' % p.lower()] = potencia.get(p, 0) * sign_
            vals['lloguers'] = vals['lloguers'] * sign_
            vals['base'] = factura.amount_untaxed * sign_
            vals['iese'] = taxes.get('Impuesto especial sobre la electricidad',
                                     0) * sign_
            vals['iva'] = taxes.get('IVA 18%', 0) * sign_
            vals['total'] = factura.amount_total * sign_
            vals['informe_id'] = informe
            linia_informe_obj.create(cursor, uid, vals, context)

    def action_obrir_informe(self, cursor, uid, ids, context=None):
        """Retorna el diccionari adient per obrir la fitxa de l'informe
        """
        if not context:
            context = {}

        o_wiz = self.pool.get('wizard.informe.comptable')
        wiz = o_wiz.browse(cursor, uid, ids[0], context)

        return {
            "domain": "[('id', '=', %i)]" % wiz.informe_id.id,
            "name": _('Informe generat'),
            "view_type": "form",
            "view_mode": "tree,form",
            "res_model": "giscedata.facturacio.informe_comptable",
            "type": "ir.actions.act_window"
        }

    _columns = {
        'state': fields.char('Estat', size=16, required=True),
        'period_id': fields.many2one('account.period', 'Període',
                                     required=True),
        'informe_id': fields.many2one('giscedata.facturacio.informe_comptable',
                                      'Informe'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardInformeComptable()

class WizardInformeMesures(osv.osv_memory):
    """Wizard per llençar l'informe de mesures per REE
    """
    _name = "wizard.informe.mesures"
    _description = __doc__


    def _mesos(self, cursor, uid, ids, context=None):
        """Funció auxiliar per obtenir el selection dels mesos
        """
        cursor.execute("""SELECT distinct to_char(data_final, 'MM/YYYY')
                          AS data
                          FROM giscedata_facturacio_factura
                          WHERE data_final IS NOT NULL
                          ORDER BY data""")
        mesos = cursor.fetchall()
        return [(mes[0] , mes[0]) for mes in mesos]

    def action_generar_dades(self, cursor, uid, ids, context=None):
        """Omple les taules amb les dades generades
        """
        if not context:
            context = {}

    _columns = {
        'state': fields.char('Estat', size=16, required=True),
        'mes': fields.selection(_mesos, 'Mes', required=True),
    }

WizardInformeMesures()

