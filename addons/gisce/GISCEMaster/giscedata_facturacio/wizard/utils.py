def tenim_mes_sequences(seq_id=None, sequencies=None):
    if not sequencies:
        return False
    if not seq_id:
        return True
    if seq_id not in sequencies:
        return True
    if sequencies.index(seq_id) + 1 >= len(sequencies):
        return False
    else:
        return True


def get_next(seq_id, sequences):
    if tenim_mes_sequences(seq_id, sequences):
        if seq_id:
            return sequences[sequences.index(seq_id) + 1]
        else:
            return sequences[0]
    return False