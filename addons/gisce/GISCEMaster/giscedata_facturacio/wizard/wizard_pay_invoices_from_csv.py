# -*- coding: utf-8 -*-

from osv import fields, osv
import base64
import csv
from datetime import datetime
import time
from tools.translate import _
import pooler
import traceback
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


from account_invoice_base.account_invoice import amount_grouped


class WizardPayInvoicesFromCSV(osv.osv_memory):
    """ This class is a wizard to pay multiple invoices generated or received"""
    _name = 'wizard.pay.invoices.from.csv'

    def _default_info(self, cursor, uid, context=None):
        # Return a description for correct use of wizard
        informacio = _(u"Es carregaràn les factures des d'un fitxer sleccionat"
                       u".\n"
                       u"El format del fitxer ha de ser CSV amb delimitador "
                       u"';', UTF-8, el fitxer ha d'estar formatat en dos "
                       u"columnes, la primera amb el numero de factura i la "
                       u"segona amb l'import.")
        return informacio

    def _default_date(self, *args):
        return datetime.strftime(datetime.now().date(), "%Y-%m-%d")

    def _default_file(self, *args):
        return False

    def _default_mode(self, *args):
        return False

    def _get_journal(self, cursor, uid, context=None):
        """Funció per obtenir el diari de pagament
        """
        if not context:
            context = {}
        journal_obj = self.pool.get('account.journal')
        search_params = [
            ('code', '=', 'CAJA'),
        ]
        journal_id = journal_obj.search(cursor, uid, search_params)
        if len(journal_id):
            return journal_id[0]
        return False

    def onchange_date(self, cursor, uid, ids, data_f, context=None):
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not data_f:
            return res

        if datetime.strptime(data_f, '%Y-%m-%d') <= datetime.strptime(time.strftime('%Y-%m-%d'), '%Y-%m-%d'):
            res['value'].update({'fact_date': data_f})
        else:
            res['value'].update({'fact_date': False})
            res['warning'].update(
                {'title': _(u'Error data no valida'),
                 'message': _(u"La data ha de ser més "
                              u"petita o igual que la data actual.")
                 })

        return res

    def onchange_tipo_facturas(self, cursor, uid, ids, f_mode, context=None):
        if f_mode != 'reb':
            self.write(cursor, uid, ids, {'partner_id': False}, context)

    def go_conf_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'conf'}, context)

    def go_init_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'init'}, context)

    def seguent(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        if not wiz.fact_mode or not wiz.journal_id:
            raise osv.except_osv('Error !',
                                 'Formulari invàlid, Tipus de facturació'
                                 ' i diari requerits')
        else:
            self.write(cursor, uid, ids, {'state': 'load'}, context)

    def anterior(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'conf'}, context)

    def importar(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)

        if wiz.fact_mode == 'reb' and not wiz.partner_id:
            raise osv.except_osv('Error !',
                                 'Formulari invàlid, El camp partner ha '
                                 'd\'estar ple')
        if not wiz.fact_date or not wiz.file_fact:
            raise osv.except_osv('Error !',
                                 'Formulari invàlid, Els camps data i fitxer '
                                 'han d\'estar plens')
        self.process_csv_file(cursor, uid, ids, wiz.file_fact, context)

    def process_csv_file(self, cursor, uid, ids, csv_file=None, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context)
        wizard_obj = self.pool.get("facturacio.pay.invoice")
        if wiz.file_fact:
            ctx = context.copy()
            ctx.update({'data_pagament': wiz.fact_date})
            period_id = wizard_obj._get_periode(cursor, uid, context=ctx)

            all_invoices = []
            invoices_to_pay = []
            amount_total_to_pay = 0.0
            correct_invoices = []
            incorrect_invoices_dict = {
                'no_paid_state': [],
                'different_amount': [],
                'not_found_invoices': [],
                'already_paid': [],
                'untracked_errors': []
            }
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            inv_obj = self.pool.get('account.invoice')
            conf_o = self.pool.get('res.config')
            unique_account_move = bool(int(conf_o.get(
                cursor, uid, 'pay_from_csv_unique_account_move', '0'
            )))
            types = {'out_invoice': -1, 'in_invoice': 1, 'out_refund': 1, 'in_refund': -1}
            fitxer = wiz.file_fact
            filename = wiz.filename
            txt = base64.decodestring(str(fitxer))

            csv_file = StringIO.StringIO(txt)
            reader = csv.reader(csv_file, delimiter=';')
            for row in reader:
                try:
                    tmp_cursor = cursor
                    if not context.get("avoid_tmp_cursor"):
                        db = pooler.get_db(cursor.dbname)
                        tmp_cursor = db.cursor()
                    fact_number = row[0].strip()
                    amount_file = round(float(row[1].replace(',', '.')), 2)

                    if fact_number:
                        all_invoices.append(fact_number)
                        s_params = [
                            ('state', '=', 'open')
                        ]
                        if wiz.fact_mode == 'reb':
                            s_params += [
                                ('origin', '=', fact_number),
                                ('partner_id', '=', wiz.partner_id.id)
                            ]
                            fact_ids = fact_obj.search(tmp_cursor, uid, s_params)
                        else:
                            s_params += [
                                ('number', '=', fact_number)
                            ]
                            fact_ids = fact_obj.search(tmp_cursor, uid, s_params)
                        if len(fact_ids) == 1:
                            ctx = context.copy()
                            ctx.update({'active_id': fact_ids[0],
                                       'active_ids': fact_ids})
                            factura = fact_obj.browse(
                                cursor, uid, fact_ids[0])

                            amount_total = amount_grouped(factura)
                            if abs(amount_file) == abs(amount_total):
                                if unique_account_move:
                                    invoices_to_pay.append(factura.invoice_id.id)
                                    direction = types[factura.type]
                                    # No fa falta mirar lo de posarho en negatiu perque el residual ja porta el simbol
                                    # if amount_total < 0:
                                    #     direction *= -1
                                    amount_total_to_pay += (amount_total * direction)
                                    continue
                                wizard_id = wizard_obj.create(tmp_cursor, uid, {
                                    'journal_id': wiz.journal_id.id,
                                    'date': wiz.fact_date,
                                    'name': filename,
                                    'period_id': period_id
                                }, ctx)
                                wizard = wizard_obj.browse(
                                    tmp_cursor, uid, wizard_id, ctx
                                )
                                res = wizard.action_wo_check(ctx)

                                fact_state = fact_obj.read(
                                    tmp_cursor, uid, fact_ids, ['state'],
                                    context=ctx
                                )
                                if fact_state[0]['state'] == u'paid':
                                    correct_invoices.append(fact_number)
                                    tmp_cursor.commit()
                                else:
                                    incorrect_invoices_dict['no_paid_state'].append(
                                        (
                                            row[0], amount_file,
                                            _('Estat actual de la factura {}'
                                              ).format(
                                                fact_state[0]['state']
                                            )
                                        )
                                    )
                            else:
                                incorrect_invoices_dict['different_amount'].append(
                                    (row[0], amount_file,
                                     _('Import fitxer {} - Import total {}'
                                       ).format(
                                         abs(amount_file), abs(amount_total)
                                     )
                                     )
                                )
                        else:
                            s_params[0] = ('state', '=', 'paid')
                            fact_ids = fact_obj.search(tmp_cursor, uid, s_params)
                            if len(fact_ids) == 1:
                                incorrect_invoices_dict['already_paid'].append(
                                    (row[0], amount_file, 'Ja estava pagada')
                                )
                            else:
                                incorrect_invoices_dict['not_found_invoices'].append(
                                    (row[0], amount_file, 'No trobada')
                                )
                except Exception as e:
                    traceback.print_exc()
                    try:
                        amount_file = round(float(row[1].replace(',', '.')), 2)
                    except ValueError as e:
                        amount_file = row[1]
                    incorrect_invoices_dict['untracked_errors'].append(
                        (row[0], amount_file, e.message)
                    )
                    if not context.get("avoid_tmp_cursor"):
                        tmp_cursor.rollback()
                finally:
                    if not context.get("avoid_tmp_cursor"):
                        tmp_cursor.close()

            if unique_account_move:
                try:
                    tmp_cursor = cursor
                    if not context.get("avoid_tmp_cursor"):
                        db = pooler.get_db(cursor.dbname)
                        tmp_cursor = db.cursor()
                    description = _(u"Pagament factures csv {0}. Import: {1}€").format(filename, amount_total_to_pay)
                    inv_obj.pay_and_reconcile_multiple(
                        tmp_cursor, uid, amount_total_to_pay, invoices_to_pay,
                        reference=description, pay_journal_id=wiz.journal_id.id,
                        date=wiz.fact_date, context=context
                    )
                    correct_invoices = invoices_to_pay
                except Exception as e:
                    traceback.print_exc()
                    if not context.get("avoid_tmp_cursor"):
                        tmp_cursor.rollback()
                finally:
                    if not context.get("avoid_tmp_cursor"):
                        tmp_cursor.commit()
                        tmp_cursor.close()

            res = self.generar_estadistiques(cursor, uid, ids,
                                             correct_invoices,
                                             incorrect_invoices_dict,
                                             all_invoices,
                                             context=context
                                             )
            vals = {
                'state': 'end',
                'info': _(u"La importació de factures ha finalitzat "
                          u"correctament.\n\n%s") % str(res)
            }
            self.write(cursor, uid, ids, vals, context=context)

    def generar_estadistiques(self, cursor, uid, ids, correct, not_correct, all_facts, context=None):
        res = u""
        res += _(u"Total fitxer: %s\n") % str(len(all_facts))
        res += _(u"Total pagades: %s\n") % str(len(correct))
        res += _(u"Total no pagades: %s\n\n") % str(len(all_facts)-len(correct))

        if list(filter(lambda ll: bool(ll), [bool(l) for l in not_correct.values()])):
            errors_file_csv = StringIO.StringIO()
            writer_errors = csv.writer(
                errors_file_csv, delimiter=';', quoting=csv.QUOTE_ALL
            )
            res += _(u"Llistat no pagades: \n\n")
            error_descriptions = {
                'no_paid_state': _('Factures amb estat no pagat:\n'),
                'different_amount': _('Totals difetents:\n'),
                'not_found_invoices': _('Factures no trobades:\n'),
                'already_paid': _('Factures pagades anteriorment:\n'),
                'untracked_errors': _('Errors no conteplats\n'),
            }
            for key_error in not_correct.keys():
                if not_correct[key_error]:
                    res += error_descriptions[key_error]
                    for error in not_correct[key_error]:
                        res += _(u"\t- %s\n") % str(error[0].decode('ISO-8859-1').encode('utf-8'))
                        writer_errors.writerow(error)
            today = datetime.today()
            self.write(cursor, uid, ids, {
                'file_errors': base64.b64encode(errors_file_csv.getvalue()),
                'file_errors_name': 'errores_pago_factura_{}_{}_{}.csv'.format(
                    today.day, today.month, today.year
                )
            }, context=context)
        return res

    _columns = {
            'state': fields.selection([('init', 'Init'), ('end', 'Final'),
                                       ('conf', 'Conf'),
                                       ('load', 'Load')],
                                      'Estat'),
            'info': fields.text('Informació', readonly=True),
            'file_fact': fields.binary('Fitxer', states={'load':[('requiered', True)]}),
            'filename': fields.char('Nom', size=1024),
            'fact_date': fields.date(
                'Data de cobrament', states={'load': [('requiered', True)]},
                write=['account.group_account_manager_dates']
            ),
            'fact_mode': fields.selection([('gen', 'Generades'),
                                   ('reb', 'Rebudes')], 'Tipus de factures',
                                     states={'conf':[('requiered', True)], 'load':[('requiered', True)]}),
            'journal_id': fields.many2one('account.journal', 'Diari',
                                          domain=[('type', '=', 'cash')],
                                          states={'conf':[('requiered', True)], 'load':[('requiered', True)]}),
            'partner_id': fields.many2one('res.partner', 'Partner', select=1),
            'file_errors': fields.binary('Fitxer d\'errors'),
            'file_errors_name': fields.char('Nom fitxer errors', size=30),
    }
    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'fact_date': _default_date,
        'file_fact': _default_file,
        'fact_mode': _default_mode,
        'journal_id': _get_journal,
        'filename': lambda *a: False,
    }


WizardPayInvoicesFromCSV()
