# -*- coding: utf-8 -*-
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
import time
import netsvc
from tools.translate import _
from osv import osv, fields
import wizard


class WizardCanviarDates(osv.osv_memory):

    """Wizard per modificar la data d'una modificació contractual
       considerant l'existència de factures
        """
    _name = 'wizard.canviar.dates'
    _inherit = 'wizard.canviar.dates'

    def get_invoices(self, cursor, polissa_id, data_inici, data_final):
        cursor.execute("""SELECT factura.id
            FROM giscedata_facturacio_factura factura
            LEFT JOIN account_invoice a ON (factura.invoice_id = a.id)
            LEFT JOIN account_journal j ON (a.journal_id = j.id)
            WHERE factura.polissa_id = %s
            AND a.type IN ('out_invoice','out_refund')
            AND j.code LIKE 'ENERGIA%%'
            AND ((factura.data_inici BETWEEN %s AND %s)
            OR (factura.data_final BETWEEN %s AND %s))""", (
            polissa_id,
            data_inici,
            data_final,
            data_inici,
            data_final
        ))
        return cursor.fetchall()

    def check_invoices(self, cursor, uid, ids, mod_id, context=None):
        polissa_obj = self.pool.get('giscedata.polissa')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        wiz = self.browse(cursor, uid, ids[0], context)
        facts = []
        dades = modcon_obj.read(
            cursor, uid, mod_id,
            ['polissa_id', 'name', 'data_inici', 'data_final']
        )
        polissa_id = dades['polissa_id'][0]
        codi_modcon = dades['name']
        polissa_name = polissa_obj.read(
            cursor, uid, polissa_id, ['name'])['name']
        ids_factures = self.get_invoices(
            cursor, polissa_id, wiz.data_inici, wiz.data_final
        )
        ids_factures_noves = self.get_invoices(
            cursor, polissa_id, dades['data_inici'], dades['data_final']
        )
        union = list(set(ids_factures_noves) | set(ids_factures))
        numeros = fact_obj.read(cursor, uid, union, ['number'])
        for elem in numeros:
            facts.append(elem['number'])
        return facts, codi_modcon, polissa_name

    def change_date(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        mod_id = context.get('active_id')
        wiz = self.browse(cursor, uid, ids[0], context)
        info = 'Correcte'
        estat = 'end'
        vals = {}
        facts, codi_modcon, polissa_name = self.check_invoices(
            cursor, uid, ids, mod_id, context
        )
        if facts:
            afectades = _('Factures afectades: \n\n')
            for fact in facts:
                afectades += "{}\n".format(fact)
        else:
            afectades = _("Cap factura de la modificació contractual amb "
                          "codi {0} de la pòlissa {1} ha sigut "
                          "afectada.").format(codi_modcon, polissa_name)
        if not wiz.force and facts:
            # S'han trobat factures pero no s'esta forçant el canvi.
            # No es fa res
            vals = {
                'state': 'error',
                'info': _("ERROR: modificar les dates afectarà a {0} factures. "
                          "Per continuar ha de forçar el canvi de "
                          "dates.\n\n").format(len(facts)) + afectades
            }
        elif not facts:
            # No s'han trobat factures a les dates entrades.
            # Modifiquem les dates
            super(WizardCanviarDates, self).change_date(
                cursor, uid, ids, context
            )
            wiz = self.browse(cursor, uid, ids[0])
            state = 'end'
            if wiz.state != 'error':
                informacio = wiz.info + afectades
            else:
                informacio = wiz.info
                state = 'error'
            vals = {
                'state': state,
                'info': informacio
            }
        else:
            # tot correcte, modifiquem les dates
            super(WizardCanviarDates, self).change_date(
                cursor, uid, ids, context
            )
            wiz = self.browse(cursor, uid, ids[0])
            informacio = wiz.info + _("La modificació de les dates de la "
                                      "modificació contactual amb codi {0} de "
                                      "la pòlissa {1} ha afectat a {2} "
                                      "factures.\n\n").format(
                codi_modcon,
                polissa_name,
                len(facts)
            ) + afectades
            state = 'end'
            if wiz.state != 'error':
                vals = {
                    'state': state,
                    'info': informacio
                }
        self.write(cursor, uid, ids, vals, context)

    _columns = {
        'force': fields.boolean(_('Forçar canvi de dates'),
                                help=_('Força el canvi de dates de la '
                                       'modificació contractual encara que '
                                       'existeixin factures en el nou rang de '
                                       'dates.'))
    }

    _defaults = {
        'force': lambda *a: False
    }

WizardCanviarDates()
