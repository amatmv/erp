# -*- coding: utf-8 -*-
from osv import osv, fields


class MultiSkipValidate(osv.osv_memory):

    _name = 'wizard.multi.skip.validate'

    def action_multi_skip(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')

        clot_ids = context.get('active_ids', [])
        #Discard the ones not in esborrany o obert state
        search_params = [('state', 'in', ('esborrany', 'obert')),
                         ('id', 'in', clot_ids)]
        final_clot_ids = clot_obj.search(cursor, uid, search_params)

        clot_obj.write(cursor, uid, final_clot_ids,
                       {'skip_validation': True})

        clot_obj.wkf_obert(cursor, uid, final_clot_ids,
                           context=context)

        return {}

MultiSkipValidate()
