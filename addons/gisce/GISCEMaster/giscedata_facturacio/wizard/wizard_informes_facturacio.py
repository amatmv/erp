# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
from giscedata_facturacio.giscedata_facturacio import \
                            _generar_resum_factures_csv
import pandas as pd
import numpy as np
from StringIO import StringIO
from base64 import b64encode
from addons import get_module_resource

INFORMES_DESC = {
        'export_resumen_otros_conceptos_agrupados':
        _(u"Resum d'altres conceptes agrupats en format CSV"),

        'informe_facturacion_contabilidad_generico':
        _(
            u"Informe que contiene el resumen detallado de la "
            u"contabilidad"
        )
}
MULTI_SEQUENCE = {
    'export_factures_desglossat': True,
    'export_resumen_otros_conceptos_agrupados': True,
    'informe_facturacion_contabilidad_generico': True
}

INFORMES_EXPORTABLES = [
    'export_factures_desglossat',
    'export_factures_desglossat_excel',
    'export_resumen_otros_conceptos_agrupados',
    'informe_facturacion_contabilidad_generico'
]


class WizardInformesFacturacio(osv.osv_memory):

    _name = "wizard.informes.facturacio"

    _get_facturacio = [('1', _('Mensual')),
                       ('2', _('Bimestral')),
                       ('12', _('Anual')),
                       ('0', _('Tot'))]

    _informes_csv = {}

    def _get_informe(self, cursor, uid, context=None):
        _opcions = [
            ('export_resumen_otros_conceptos_agrupados',
             _('Resum d\'altres conceptes agrupats(CSV)')),

            ('informe_facturacion_contabilidad_generico',
             _('Informe de facturación de contabilidad'))

        ]

        return _opcions

    def get_info_desc(self, cursor, informe, context=None):
        """ Retorna la informació de l'informe """
        return INFORMES_DESC.get(informe, False)

    def get_multi_sequence(self, informe):
        return MULTI_SEQUENCE.get(informe, False)

    def default_get(self, cursor, uid, fields, context=None):
        """Overridden to provide specific defaults depending on the context
           parameters.

           :param dict context: several context values will modify the behavior
                                of the wizard, cfr. the class description.
        """
        if context is None:
            context = {}

        result = super(WizardInformesFacturacio, self).default_get(
            cursor, uid, fields, context=context)
        if result.get('informe'):
            report_multi_sequence = self.get_multi_sequence(result['informe'])
            result.update({'report_multi_sequence': report_multi_sequence})
        return result

    def _get_sequence(self, cursor, uid, ids, context=None):
        '''retorna una llista amb totes les seqs associades
        a un diari de facturacio'''

        journal_obj = self.pool.get('account.journal')

        res = []

        search_params = [('invoice_sequence_id', '!=', None)]
        journal_ids = journal_obj.search(cursor, uid, search_params)
        for journal in journal_obj.browse(cursor, uid, journal_ids):
            repetit = [x[0] for x in res]
            if not journal.invoice_sequence_id.code in repetit:
                res.append((journal.invoice_sequence_id.code,
                        journal.invoice_sequence_id.name))

        return res

    def imprimir(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        if wizard.facturacio == '0':
            wizard.facturacio = '1,2,12'

        if wizard.include_draft:
            states = "'proforma2', 'cancel'"
        else:
            states = "'draft', 'proforma2', 'cancel'"
        mulit_seq = [x.invoice_sequence_id.code for x in wizard.multi_sequence]
        datas = {'form': {'data_inici': wizard.data_inici,
                          'data_final': wizard.data_final,
                          'serie': wizard.sequence,
                          'multi_serie': mulit_seq,
                          'states': '(%s)' % states,
                          'facturacio': '(%s)' % wizard.facturacio,
                        }
                }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': wizard.informe,
            'datas': datas,
        }

    def exportar_resum_factures(self, cursor, uid, ids, context=None):
        """ Exportació com a CSV """
        wizard = self.browse(cursor, uid, ids[0])

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_obj = self.pool.get('account.invoice')

        if wizard.facturacio == '0':
            facturacio_search = [1, 2, 12]
        else:
            facturacio_search = [int(wizard.facturacio)]
        fact_txt = [x[1] for x in wizard._get_facturacio if x[0] == '0'][0]

        mulit_seq = [x.invoice_sequence_id.code for x in wizard.multi_sequence]

        if wizard.include_draft:
            states = ['proforma2', 'cancel']
        else:
            states = ['draft', 'proforma2', 'cancel']

        search_params = [('state', 'not in', states),
                ('date_invoice', '>=', wizard.data_inici),
                ('date_invoice', '<=', wizard.data_final),
                ('journal_id.invoice_sequence_id.code',
                    'in', mulit_seq)]
        inv_ids = inv_obj.search(cursor, uid, search_params)
        _sqlfile = self._informes_csv[wizard.informe]['query']
        _sqlheader = self._informes_csv[wizard.informe]['header']
        if _sqlfile:
            _query = open(_sqlfile).read()

        search_params = [
            ('invoice_id', 'in', inv_ids),
            ('facturacio', 'in', facturacio_search)
        ]
        factures_ids = fact_obj.search(cursor, uid, search_params)

        if wizard.informe == 'export_factures_desglossat_excel':
            # not only different file type (xls vs csv) also different data
            res = fact_obj.generar_resum_factures_xls(cursor, uid, factures_ids,
                                                      _sqlheader,
                                                      context=context)
        else:
            res = _generar_resum_factures_csv(cursor,
                                              factures_ids, _query,
                                              _sqlheader, context=context)

        if res:
            info = _(u"Fitxer CSV generat.\nParàmetres:\n")
            info += _(u" * data entre %s i %s\n") % (wizard.data_inici,
                                                wizard.data_final)
            info += _(u" * facturació: %s\n") % fact_txt
            info += _(u" * diari(s): %s\n") % wizard.sequence
            info += _(u" * incloure esborranys: %s") % (wizard.include_draft
                                                        and 'Si' or 'No')
        else:
            info = _("Error generant CSV")

        return res, info 

    def onchange_informe(self, cursor, uid, ids, informe, context=None):
        result = {'value': {}}
        result['value']['desc'] = self.get_info_desc(cursor, informe, context)
        result['value']['report_multi_sequence'] = self.get_multi_sequence(
            informe)
        if informe in INFORMES_EXPORTABLES:
            result['value']['exportable'] = True
        else:
            result['value']['exportable'] = False

        return result

    def exportar(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        vals = {}
        if wizard.informe == 'export_resumen_otros_conceptos_agrupados':
            res = self.export_resumen_otros_conceptos(
                cursor, uid, ids, context=context
            )
            vals = {
                'file': res,
                'estat': 'done',
                'name': _('Resum_altres_conceptes_agrupats.csv')
            }
        elif wizard.informe == 'informe_facturacion_contabilidad_generico':

            df = self.get_df_generic_informe_contabilidad(
                cursor, uid, ids, context=context
            )
            res = self.exportar_informe_contabilidad(
                cursor, uid, ids, df, context=context
            )
            d_ini = wizard.data_inici
            d_fi = wizard.data_final

            f_name = (
                'Resumen_detallado_contabilidad_facturacion_{0}_{1}.csv'
            ).format(d_ini, d_fi)

            vals = {
                'file': res,
                'estat': 'done',
                'name': f_name
            }
        if vals:
            wizard.write(vals)

        # True if vals contains a val else false
        return bool(vals)

    def export_resumen_otros_conceptos(self, cursor, uid, ids, context=None):
        """
        Genera un resumen en formato csv de otros conceptos:
            Resumen de las lineas de las facturas agrupadas por tipo de concepto
            (codigo producto).
        """
        wizard = self.browse(cursor, uid, ids[0], context=context)
        if wizard.facturacio == '0':
            facturacio_search = (1, 2, 12)
        else:
            facturacio_search = (int(wizard.facturacio),)

        mulit_seq = tuple(
            [x.invoice_sequence_id.code for x in wizard.multi_sequence])

        if wizard.include_draft:
            states = ('proforma2',)
        else:
            states = ('draft', 'proforma2')

        d_inici = wizard.data_inici
        d_final = wizard.data_final

        sql_name = 'query_otros_conceptos.sql'
        sql_path = get_module_resource(
            'giscedata_facturacio', 'sql', sql_name
        )
        query = open(sql_path).read()
        params = {
            'dinici_inv': d_inici,
            'dfinal_inv': d_final,
            'states': states,
            'serie_codes': mulit_seq,
            'tipo_facturacion': facturacio_search
        }
        cursor.execute(query, params)
        res = cursor.dictfetchall()
        columns = [d.name for d in cursor.description]
        df = pd.DataFrame(res, columns=columns)
        if df.empty:
            multi_name = "\n".join(
                [x.invoice_sequence_id.name for x in wizard.multi_sequence]
            )
            raise osv.except_osv('Error',
                                 _(u'No s\'han trobat resultats per les '
                                   u'seqüencies:\n{0}').format(multi_name))

        total = df['importe_producto'].sum()

        default_void_values = {
            'poliza': 'Sin poliza enlazada',
        }
        df = df.fillna(value=default_void_values)

        pivot_table = pd.pivot_table(df,
                                     index=['poliza', 'factura'],
                                     columns='nombre_producto',
                                     values=['importe_producto'],
                                     aggfunc=np.sum,
                                     margins=True,
                                     margins_name='Total',
                                     ).drop('Total').stack()

        pivot_table.at['Total conceptos', 'importe_producto'] = total

        gen_file = StringIO()

        pivot_table.to_csv(
            gen_file, header=True, sep=';', encoding='utf-8-sig'
        )

        return b64encode(gen_file.getvalue())

    def parse_sql_contabilidad_depends_of_whereim(self, cursor, uid, ids, query_str, context=None):

        join_energia = """
        LEFT JOIN (
          SELECT lf.factura_id AS factura_id,SUM(li.quantity) AS energia
          FROM giscedata_facturacio_factura_linia lf
          LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
          WHERE lf.tipus='energia'
          GROUP BY lf.factura_id
        ) AS ene ON (ene.factura_id=f.id)
        """

        query_str = query_str.replace('%(select_descuento_energia)s', '')
        query_str = query_str.replace('%(select_descuento_potencia)s', '')
        query_str = query_str.replace('%(join_energia)s', join_energia)
        query_str = query_str.replace('%(joins_descuentos)s', '')

        return query_str

    def get_df_generic_informe_contabilidad(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        if wizard.facturacio == '0':
            facturacio_search = (1, 2, 12)
        else:
            facturacio_search = (int(wizard.facturacio),)

        mulit_seq = tuple(
            [x.invoice_sequence_id.code for x in wizard.multi_sequence])

        if wizard.include_draft:
            states = ('proforma2',)
        else:
            states = ('draft', 'proforma2')

        d_inici = wizard.data_inici
        d_final = wizard.data_final

        sql_name = 'informe_facturacion_contabilidad_generico.sql'
        sql_path = get_module_resource(
            'giscedata_facturacio', 'sql', sql_name
        )
        query = open(sql_path).read()

        params = {
            'dinici_inv': d_inici,
            'dfinal_inv': d_final,
            'states': states,
            'serie_codes': mulit_seq,
            'tipo_facturacion': facturacio_search
        }

        query = self.parse_sql_contabilidad_depends_of_whereim(
            cursor, uid, ids, query, context=context
        )

        cursor.execute(query, params)
        res = cursor.dictfetchall()
        columns = [d.name for d in cursor.description]

        df = pd.DataFrame(res, columns=columns)
        if df.empty:
            multi_name = "\n".join(
                [x.invoice_sequence_id.name for x in wizard.multi_sequence]
            )
            raise osv.except_osv('Error',
                                 _(u'No s\'han trobat resultats per les '
                                   u'seqüencies:\n{0}').format(multi_name))
        default_void_values = {
            'Contrato': '',
            'Fecha inicio': '',
            'Fecha final': '',
            'Tarifa de acceso': 'Sin Tarifa de acceso',
            'Lista de precios': 'Sin Lista de precios'
        }

        df = df.fillna(value=default_void_values)
        df = df.fillna(0.00)
        return df

    def exportar_informe_contabilidad(self, cursor, uid, ids, df, context=None):
        informe = self.read(
            cursor, uid, ids[0], ['informe'], context=context
        )[0]['informe']

        if informe in ('informe_facturacion_contabilidad_generico',):

            df = df.drop(['id_client', 'id_invoice'], axis=1)

            gen_file = StringIO()

            df.to_csv(
                gen_file, header=True, sep=';', encoding='utf-8-sig',
                index=None, decimal=',', float_format='%.2f'
            )

            return b64encode(gen_file.getvalue())

        return False

    _columns = {
        'data_inici': fields.date('Des de', required=True),
        'data_final': fields.date('Fins', required=True),
        'facturacio': fields.selection(_get_facturacio, 'Tipus'),
        'include_draft': fields.boolean('Incloure esborranys',
                            help=_(u"Marqui aquesta casella per incloure "
                                   u"les factures en estat esborrany dins "
                                   u"el resum de facturació")),
        'informe': fields.selection(_get_informe, 'Informe', required=True),
        'sequence': fields.selection(_get_sequence, 'Serie'),
        'multi_sequence': fields.many2many('account.journal',
                                      'account_journal',
                                      'id',
                                      'id',
                                      'Serie', domain=[
                ('invoice_sequence_id', '!=', False)]),
        'report_multi_sequence': fields.boolean(u'Informe multi seqüència'),
        'estat': fields.char('Estat', size=16),
        'info': fields.text('Info'),
        'name': fields.char('Fitxer CSV', size=64),
        'file': fields.binary('CSV',
              help='Fitxer CSV amb les factures segons criteris seleccionats'),
        'desc': fields.text('Descripció', readonly=True),
        'exportable': fields.boolean('Informe exportable')
    }

    _defaults = {
        'include_draft': lambda *a: False,
        'estat': lambda *x: 'init',
        'info': lambda *x: 'Fitxer CSV generat',
        'name': lambda *x: 'ResumFactures.csv',
        'report_multi_sequence': lambda *x: False,
    }

WizardInformesFacturacio()
