# -*- coding: utf-8 -*-
from datetime import timedelta, datetime
from tools.translate import _
from osv import osv


class GiscedataPolissaCrearContracte(osv.osv_memory):

    _inherit = 'giscedata.polissa.crear.contracte'

    def check_reading_on_date(self, cursor, uid, date, polissa_id, context=None):
        polissa_o = self.pool.get("giscedata.polissa")
        ctx = context.copy()
        ctx['active_only'] = False
        ctx['multi'] = True
        comptadors_ids = polissa_o.get_comptador_data(
            cursor, uid, [polissa_id], date, context=ctx
        )
        lectura_o = self.pool.get("giscedata.lectures.lectura")
        if not lectura_o:
            return True
        lectura_ids = lectura_o.search(cursor, uid, [
            ('name', '=', date),
            ('comptador', 'in', comptadors_ids)

        ], order="name desc", limit=1)
        return len(lectura_ids) != 0

    def check_change_needs_reading(self, cursor, uid, ids, context=None):
        if context is None:
            context = None
        polissa_id = context.get('active_id', False)
        if not polissa_id:
            return False
        from giscedata_facturacio.giscedata_polissa import INTERVAL_INVOICING_FIELDS
        polissa_obj = self.pool.get('giscedata.polissa')
        changes = polissa_obj.get_changes(cursor, uid, polissa_id)
        for field in changes.keys():
            if field in INTERVAL_INVOICING_FIELDS:
                return True
        return False

    def action_crear_contracte(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if self.check_change_needs_reading(cursor, uid, ids, context=context):
            wizard = self.browse(cursor, uid, ids[0], context)
            polissa = wizard.polissa_id
            data_inici_m1 = datetime.strptime(wizard.data_inici,  "%Y-%m-%d") - timedelta(days=1)
            data_inici_m1 = data_inici_m1.strftime("%Y-%m-%d")
            if not self.check_reading_on_date(
                    cursor, uid, data_inici_m1, polissa.id, context=context
            ):
                context['extra_observacions'] = _(
                    u"\nNo s'ha trobat cap lectura en data {0} (inici de la "
                    u"modcon - 1 dia). Es necessiten lectures per poder partir "
                    u"la facturació correctament."
                ).format(data_inici_m1)
        res = super(GiscedataPolissaCrearContracte, self).action_crear_contracte(cursor, uid, ids, context=context)
        return res

    def onchange_duracio(self, cursor, uid, ids, data_inici, duracio, context=None):
        if context is None:
            context = {}
        res = super(GiscedataPolissaCrearContracte, self).onchange_duracio(cursor, uid, ids, data_inici, duracio, context=context)
        if res['warning']:
            return res
        if not context.get("check_lectures"):
            return res
        if self.check_change_needs_reading(cursor, uid, ids, context=context):
            polissa_id = context.get('active_id', False)
            data_inici_m1 = datetime.strptime(data_inici,  "%Y-%m-%d") - timedelta(days=1)
            data_inici_m1 = data_inici_m1.strftime("%Y-%m-%d")
            if not self.check_reading_on_date(
                    cursor, uid, data_inici_m1, polissa_id, context=context
            ):
                res['warning'].update({
                    'title': _(u"Error Usuari"),
                    'message': _(
                        u"No s'ha trobat cap lectura en data {0} (inici de la "
                        u"modcon - 1 dia). Es necessiten lectures per poder "
                        u"partir la facturació correctament."
                    ).format(data_inici_m1)
                })
        return res

GiscedataPolissaCrearContracte()
