# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta
import json
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES

class WizardManualInvoice(osv.osv_memory):

    _name = 'wizard.manual.invoice'

    def get_autofill(self, cursor, uid, context=None):
        conf_obj = self.pool.get('res.config')

        value = False
        conf_id = conf_obj.search(
            cursor, uid, [('name', '=', 'fact_autofill_manual_wizard')]
        )
        if conf_id:
            vals = conf_obj.read(cursor, uid, conf_id[0], ['value'])
            value = (int(vals['value']) > 0)

        return value

    def _get_default_date_invoice(self, cursor, uid, context=None):

        if not context:
            context = {}

        if not self.get_autofill(cursor, uid, context=context):
            return False

        return datetime.today().strftime('%Y-%m-%d')

    def _get_default_force_date(self, cursor, uid, context=None):

        if not context:
            context = {}

        if not self.get_autofill(cursor, uid, context=context):
            return False

        return True

    def _get_default_journal_id(self, cursor, uid, context=None):
        if not context:
            context = {}

        if not self.get_autofill(cursor, uid, context=context):
            return False

        journal_obj = self.pool.get('account.journal')
        journal_ids = journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )

        if not journal_ids:
            return False
        else:
            return journal_ids[0]

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id, context=None):
        res = {}

        if not self.get_autofill(cursor, uid, context=context):
            return res

        pol_obj = self.pool.get('giscedata.polissa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        num_meters = meter_obj.search(cursor, uid,
                                      [('polissa', '=', polissa_id)])
        #Només polisses amb un comptador actiu
        if len(num_meters) == 1:
            try:
                polissa = pol_obj.browse(cursor, uid, polissa_id)
                meter = [meter for meter in polissa.comptadors
                         if meter.active][0]
                lectures = meter.get_lectures_per_facturar(polissa.tarifa.id)
                if lectures:
                    data_fi = max(l['actual']['name']
                                  for l in [p for p in lectures.values()] if l)

                    if polissa.data_ultima_lectura:
                        data_inici_min = (
                            datetime.strptime(
                                polissa.data_ultima_lectura, '%Y-%m-%d'
                            ) + timedelta(days=1)
                        )
                    else:
                        # When first invoice (no last mesure date)
                        data_inici_min = datetime.strptime(
                            min(l['anterior']['name']
                                for l in [p for p in lectures.values()]
                                if l),
                            '%Y-%m-%d'
                        )
                        # Compatibility old/new readings behaviour. If it's new,
                        # reading will be the day before alta so we add 1 day
                        pdata_alta = datetime.strptime(polissa.data_alta, "%Y-%m-%d")
                        pdata_alta -= timedelta(days=1)
                        if pdata_alta == data_inici_min:
                            data_inici_min += timedelta(days=1)

                    data_inici = data_inici_min.strftime('%Y-%m-%d')

                    res.update({'value': {'date_start': data_inici,
                                          'date_end': data_fi}
                                })
            except Exception, e:
                # impossible compute dates
                pass

        return res

    def onchange_concepts_invoice_check(self, cursor, uid, ids,
                                        extra_lines_invoice, polissa_id,
                                        context=None):
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if extra_lines_invoice:
            extra_obj = self.pool.get('giscedata.facturacio.extra')
            extra_ids = extra_obj.get_pending_extra_lines_from_contract(
                cursor, uid, polissa_id, context=context
            )
            if extra_ids:
                extra_dates = extra_obj.read(
                    cursor, uid, extra_ids,
                    ['date_from', 'date_to'],
                    context=context
                )
                min_date_from = min(
                    [datetime.strptime(extra['date_from'], '%Y-%m-%d')
                     for extra in extra_dates]
                )
                max_date_to = max(
                    [datetime.strptime(extra['date_to'], '%Y-%m-%d')
                     for extra in extra_dates]
                )

                res['value'].update(
                    {'date_start': datetime.strftime(min_date_from, '%Y-%m-%d'),
                     'date_end': datetime.strftime(max_date_to, '%Y-%m-%d')}
                )
            else:
                res['warning'].update(
                    {'title': 'Error',
                     'message': _('No s\'han trobat linies extra')
                     }
                )
                res['value'].update({'extra_lines_invoice': False})

        return res

    def action_manual_invoice(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        facturador = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        wizard = self.browse(cursor, uid, ids[0])

        context.update({'sync': False,
                        'factura_manual': True,
                        'data_inici_factura': wizard.date_start,
                        'data_final_factura': wizard.date_end,
                        'journal_id': wizard.journal_id.id})

        if wizard.polissa_id.state in CONTRACT_IGNORED_STATES:
            raise osv.except_osv(_('Error'),
                            _(u"No es pot facturar una pòlissa manualment "
                              u"en estat %s" % (wizard.polissa_id.state)))

        if context.get('is_factura_frau', False):
            factura_ids = facturador.fact_frau_lines(cursor, uid,
                                                     wizard.polissa_id.id,
                                                     False, context=context)
        elif wizard.extra_lines_invoice:
            factura_ids = facturador.fact_linies_extra(cursor, uid,
                                                       wizard.polissa_id.id,
                                                       False, context=context)
        else:
            factura_ids = facturador.fact_via_lectures(cursor, uid,
                                                       wizard.polissa_id.id,
                                                       False, context=context)
        if wizard.force_date_invoice:
            vals = {
                'date_invoice': wizard.date_invoice
            }
            # due_date calc when forcing date_invoice
            payment_term = factura_obj.read(
                cursor, uid, factura_ids, ['partner_id', 'payment_term']
            )[0]['payment_term']
            if payment_term:
                payment_term_id = payment_term[0]
                date_due_res = factura_obj.onchange_payment_term_date_invoice(
                    cursor, uid, [], payment_term_id, wizard.date_invoice
                )
                date_due = date_due_res.get('value', {})

                if date_due:
                    vals.update({
                        'date_due': date_due['date_due']
                    })

            factura_obj.write(cursor, uid, factura_ids, vals)

        wizard.write({
            'state': 'end',
            'invoice_ids': json.dumps(factura_ids)
        })

    def show_invoice(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context=context)
        factura_ids = json.loads(wizard.invoice_ids)
        return {
            'domain': [('id', 'in', factura_ids)],
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa',
                                      'Pòlissa', required=True),
        'date_start': fields.date('Data des de', required=True),
        'date_end': fields.date('Data fins', required=True),
        'force_date_invoice': fields.boolean('Forçar data factura'),
        'date_invoice': fields.date('Data factura'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')],
                                  'Estat'),
        'journal_id': fields.many2one('account.journal', 'Diari',
                                      required=True),
        'invoice_ids': fields.text('Invoices'),
        'extra_lines_invoice': fields.boolean('Factura de línies extra',
                                              help='Amb aquesta opció '
                                                   'marcada només es '
                                                   'facturaran línies extra'
                                              ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'date_invoice': _get_default_date_invoice,
        'journal_id': _get_default_journal_id,
        'autofill': get_autofill,
        'force_date_invoice': _get_default_force_date,
        'extra_lines_invoice': lambda *a: False
    }

WizardManualInvoice()
