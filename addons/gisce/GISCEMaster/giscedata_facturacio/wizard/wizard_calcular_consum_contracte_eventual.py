# -*- coding: utf-8 -*-
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
import time
import netsvc
from tools.translate import _
from osv import osv, fields
import wizard


class WizardCalcularConsumEventual(osv.osv_memory):
    """CA:Wizard per calcular el consum de contractes eventuals sense comptador.
        ES:Wizard para calcular el consumo de contratos eventuales sin contador.
          EN:Wizard to calculate consumption of eventual contract without meter.
            CH: 向导计算没有米的最终合同的消费
    """
    _name = 'wizard.calcular.consum.eventual'

    # Private method to get initial and final date.
    def _default_dates(self, cursor, uid, context=None):
        if context is None:
            context = {}
        pol_id = context.get('active_id')#context.get('polissa_id')
        if pol_id:
            pol_obj = self.pool.get('giscedata.polissa')
            res = pol_obj.read(cursor, uid, pol_id, ['data_alta', 'data_baixa'])
            return [res['data_alta'], res['data_baixa']]

        else:
            return False, False

    # Private method to get contracted power.
    def _default_potencia(self, cursor, uid, context=None):
        if context is None:
            context = {}
        pol_id = context.get('active_id')#context.get('polissa_id')
        if pol_id:
            pol_obj = self.pool.get('giscedata.polissa')
            res = pol_obj.read(cursor, uid, pol_id, ['potencia'])
            return res['potencia']
        else:
            return False

    def _default_data_inici(self, *args):
        return self._default_dates(*args)[0]

    def _default_data_final(self, *args):
        return self._default_dates(*args)[1]

    def _default_potencia_contractada(self, *args):
        return self._default_potencia(*args)

    def _calculate_consum(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        return (
            (datetime.strptime(wiz.data_final, '%Y-%m-%d') -
             datetime.strptime(wiz.data_inici, '%Y-%m-%d')).days * wiz.hores_diaries
            * wiz.potencia
        )

    def update_values(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        pol_id = context.get('active_id')
        if pol_id:
            valid_form = self.validate_data(cursor, uid, ids, context)
            if valid_form:
                pol_obj = self.pool.get('giscedata.polissa')
                wiz = self.browse(cursor, uid, ids[0], context)
                vals = {
                    'expected_consumption': self._calculate_consum(cursor, uid, ids, context),
                    'data_baixa': wiz.data_final
                }
                pol_obj.write(cursor, uid, pol_id, vals, context)
                return vals
            else:
                raise osv.except_osv('Error !',
                                     'Formulari invàlid, nombre d\'hores invalid'
                                     ' o data final invalida')


    def precalcular_consum(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        consum = self._calculate_consum(cursor, uid, ids, context)
        wiz.write({'consum': consum})

    def onchange_hours(self, cursor, uid, ids, active_hours, context=None):
        res = {'value': {}, 'warning': {}, 'domain': {}}

        if not active_hours:
            return res

        if (active_hours>0 and active_hours<=24):
            res['value'].update({'hores_diaries': active_hours})
        else:
            res['warning'].update({'title': _(u'Error hores no valides'),
                                   'message': _(u"El nombre d'hores diaries "
                                                u"ha de ser més gran que 0 "
                                                u"i més petit o igual que 24.")
                                   })
        return res

    def onchange_date_final(self, cursor, uid, ids, data_fi, data_inici, context=None):
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not data_fi:
            return res

        if datetime.strptime(data_fi, '%Y-%m-%d') > datetime.strptime(data_inici, '%Y-%m-%d'):
            res['value'].update({'data_final': data_fi})
        else:
            res['warning'].update({'title': _(u'Error data final no valida'),
                                   'message': _(u"La data final ha de ser més "
                                                u"gran que la data inicial.")
                                   })
        return res

    def validate_data(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        return ((wiz.hores_diaries > 0 and wiz.hores_diaries <= 24)
                and (wiz.potencia > 0) and (datetime.strptime(wiz.data_final, '%Y-%m-%d') > datetime.strptime(wiz.data_inici, '%Y-%m-%d')))

    _columns = {
        'data_inici': fields.date(_('Data inicial'), readonly=True,
                                 required=True),
        'data_final': fields.date(_('Data final'), required=True),
        'potencia': fields.float('Potència contractada (kW)', readonly=True,
                                 required=True),
        'hores_diaries': fields.integer(_('Hores diaries'), required=True),
        'consum': fields.float(_('Consum'), readonly=True),
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End')], 'Estat')
    }

    _defaults = {
        # todo rm dates + potencia maybe
        'data_inici': _default_data_inici,
        'data_final': _default_data_final,
        'potencia': _default_potencia_contractada,
        'hores_diaries': 8,
        'state': lambda *a: 'init'
    }


WizardCalcularConsumEventual()