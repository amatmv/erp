# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    Copyright (C) 2011 GISCE Enginyeria (http://gisce.net). All Rights Reserved
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

"""Wizard per abonament de factures.

Aquest wizard permet anul.lar i rectificar factures.
"""

import time
from osv import fields, osv
from tools.translate import _


class WizardRanasFactures(osv.osv_memory):
    """Factures generades pel Wizard
    """
    _name = "wizard.ranas.factures"

    _columns = {
        'factura': fields.many2one('giscedata.facturacio.factura', 'Factura'),
        'rana_id': fields.many2one('wizard.ranas', 'Wizard'),
    }


WizardRanasFactures()


class WizardRanas(osv.osv_memory):
    """Wizard class
    """
    _name = "wizard.ranas"
    _tipus_rectificadora = [('anulladora', 'Anul·ladora'),
                            ('rectificadora', 'Anul·ladora amb rectificadora')]

    def create(self, cr, user, vals, context=None):
        factura_ids = context.get('active_ids', False)
        if not factura_ids:
            return False

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        fact_fields = factura_obj.read(
            cr, user, factura_ids[0],
            ['refund_by_id']
        )

        refund = fact_fields['refund_by_id']

        if refund:
            raise osv.except_osv(_('Error!'),
                                 _(u"Factura ja rectificada anteriorment, "
                                   u"no es pot tornar a rectificar!"))

        return super(WizardRanas,
                     self).create(cr, user, vals, context=context)

    def action_confirmar(self, cursor, uid, ids, context):
        """Acció per escriure l'estat de confirmació
        """
        self.write(cursor, uid, ids, {'state': 'conf'})
        return True

    def action_confirmar_rect(self, cursor, uid, ids, context):
        """Acció per escriure l'estat de confirmació de la rectificadora
        """
        self.write(cursor, uid, ids, {'state': 'confrect'})
        return True

    def action_confirmar_rect_subs(self, cursor, uid, ids, context):
        """Acció per escrire l'estat de confirmació de rectificadora
        """
        self.write(cursor, uid, ids, {'state': 'confrectsubs'})
        return True

    def _action(self, cursor, uid, ids, action='anullar', context=None):
        """Acció per anul·lar o rectificar
        """
        if not context:
            context = {}
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        factura_mem = self.pool.get('wizard.ranas.factures')

        wiz_values = self.read(cursor, uid, ids, ['data', 'remove_lot'])[0]
        context.update({'data_factura': wiz_values['data']})

        factura_ids = context.get('active_ids', False)
        if not factura_ids:
            return False
        method = getattr(fact_obj, action)
        fanullada_ids = method(cursor, uid, factura_ids, context=context)
        for fanullada in fanullada_ids:
            factura_mem.create(cursor, uid, {'factura': fanullada,
                                             'rana_id': ids})
        self.write(cursor, uid, [ids], {'state': 'end'})
        if wiz_values['remove_lot']:
            # If has_lot is False means that the invoice hadn't a
            # invoicing lot, so it has been added now.
            fact_obj.write(cursor, uid, factura_ids, {'lot_facturacio': False})
        return fanullada_ids

    def action_anullar(self, cursor, uid, ids, context):
        """Acció per anul·lar les factures
        """
        return self.pool.get('wizard.ranas')._action(cursor, uid, ids,
                                                     'anullar', context)

    def action_rectificar(self, cursor, uid, ids, context):
        """Acció per rectificar les factures
        """
        return self.pool.get('wizard.ranas')._action(cursor, uid, ids,
                                                     'rectificar', context)

    def action_rectificar_substitucio(self, cursor, uid, ids, context):
        """Acció per rectificar les factures
        """
        return self.pool.get('wizard.ranas')._action(
            cursor, uid, ids, 'rectificar_substitucio', context
        )

    def action_end(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem les factures creades per repassar-les
        """
        wiz = self.browse(cursor, uid, ids[0])
        refund_ids = [a.factura.id for a in wiz.fact_generades]
        return {
            'domain': "[('id','in', %s)]" % str(refund_ids),
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'data': fields.date('Data factura', required=True),
        'fact_generades': fields.one2many('wizard.ranas.factures', 'rana_id',
                                          'Factures generades'),
        'state': fields.selection(
            [
                ('init', 'Init'), ('end', 'Final'), ('cancel', 'Cancel'),
                ('conf', 'Confirmar'), ('confrect', 'Confirmar rectificació'),
                ('confrectsubs', 'Confirmar rectificació per substitucio')
            ], 'Estat'
        ),
        'remove_lot': fields.boolean(
            'Treure factura del lot', help=_(u'Treure la factura abonada del '
                                             u'lot en acabar.')
        )
    }
    _defaults = {
        'tipus_rectificadora': lambda *a: 'anulladora',
        'data': lambda *a: time.strftime('%Y-%m-%d'),
        'state': lambda *a: 'init',
        'remove_lot': lambda *a: False
    }

WizardRanas()
