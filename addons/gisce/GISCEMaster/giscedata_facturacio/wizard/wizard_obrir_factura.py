# -*- encoding: utf-8 -*-
import wizard
import pooler
import netsvc
from osv import osv
from tools.translate import _

def _obrir(self, cursor, uid, data, context=None):
    pool = pooler.get_pool(cursor.dbname)
    factura_obj = pool.get('giscedata.facturacio.factura')
    data['ids'] = sorted(data['ids'])
    logger = netsvc.Logger()
    not_open_ids = []
    sorted_factures = sorted(
        factura_obj.read(cursor, uid, data['ids'], ['date_invoice']),
        key=lambda f: f['date_invoice']
    )
    for factura in sorted_factures:
        fact_id = factura['id']
        try:
            api = osv.osv_pool()
            api.execute(cursor.dbname, uid, 'giscedata.facturacio.factura',
                        'invoice_open', [fact_id], context=context)
        except Exception, e:
            txt = _(u"La factura amb id '%s' no s'ha obert: %s") % (fact_id,
                                                                    e)
            logger.notifyChannel('objects', netsvc.LOG_INFO, txt)
            not_open_ids.append(fact_id)
    if not_open_ids:
        txt = _(u"Aquestes factures no s'han pogut obrir: %s" % not_open_ids)
        raise osv.except_osv('Warn', txt)

    return {}

class wizard_obrir_factura(wizard.interface):
    states = {
        'init': {
            'actions': [_obrir],
            'result': {'type':'state', 'state':'end'}
        }
    }
wizard_obrir_factura('giscedata.facturacio.factura.obrir')
