# -*- coding: utf-8 -*-
from osv import osv


class PolissaReport(osv.osv):
    _auto = False
    _name = "giscedata.polissa.report"
    _inherit = "giscedata.polissa.report"

    def forma_pagament(self, cursor, uid, polissa, translate_call):
        """
        Obtains the HTML code that adds the payment data in the contract report
        :param cursor: DB Cursor
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param polissa: <giscedata.polissa> browse record.
        :type polissa: orm.browse_record
        :param translate_call: function that translates the report strings
        :return: HTML code with the payment data.
        :rtype: str
        """
        mako_path = "giscedata_facturacio/report/contracte.mako"
        variables = {
            '_': translate_call,    'cursor': cursor,
            'polissa': polissa,     'uid': uid,
        }
        html = self.render_mako_code(mako_path, variables)

        return html


PolissaReport()
