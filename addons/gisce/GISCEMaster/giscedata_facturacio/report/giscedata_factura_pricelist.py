# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class ReportWebKitHtml(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(ReportWebKitHtml, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'pool': self.pool
        })


webkit_report.WebKitParser(
    'report.report_preus_annex',
    'giscedata.polissa',
    'giscedata_facturacio/report/preus.mako',
    parser=ReportWebKitHtml
)
