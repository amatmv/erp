# -*- coding: utf-8 -*-

import jasper_reports

def _pricelist_report_print(cr, uid, ids, data, context):
    return {
        'ids': ids,
        'parameters': {'P1E': data['form']['P1E'],
                       'P2E': data['form']['P2E'],
                       'P3E': data['form']['P3E'],
                       'P4E': data['form']['P4E'],
                       'P5E': data['form']['P5E'],
                       'P6E': data['form']['P6E'],
                       'P1P': data['form']['P1P'],
                       'P2P': data['form']['P2P'],
                       'P3P': data['form']['P3P'],
                       'P4P': data['form']['P4P'],
                       'P5P': data['form']['P5P'],
                       'P6P': data['form']['P6P'],
                       'price_date': data['form']['price_date'],
                       'tarifa': data['form']['tarifa'],
                       'pricelist': data['form']['pricelist'],
                       'num_te': data['form']['num_te'],
                       'num_tp': data['form']['num_tp'],
                       },
            }

jasper_reports.report_jasper(
   'report.report_pricelist_tarifa',
   'wizard.pricelist.report',
   parser=_pricelist_report_print
)