<%
    from osv import osv
    from datetime import date
    from datetime import datetime
    import calendar
    mostrar_reactiva = [
        '003', '011', '012', '017', '013', '014', '015', '016'
    ]
    mostrar_exces = [
        '012', '017', '013', '014', '015', '016'
    ]

    ## ---------- Versió modificada de la de utils.py ----------
    ##  --- Agafa product_id diferent i retorna unitat_uom ---

    def get_atr_price(cursor, uid, pname, tipus, tarifa, id_preu, context):
        pricelist_obj = pool.get('product.pricelist')
        pricelistver_obj = pool.get('product.pricelist.version')
        pricelistitem_obj = pool.get('product.pricelist.item')

        product_uom_obj = pool.get('product.uom')
        product_id = polissa_tarifa_obj.get_periodes_producte(cursor, uid, tarifa, tipus)[pname]
        query = """
            SELECT product_uom.name
            FROM   product_uom
            INNER JOIN product_template ON product_template.uom_id = product_uom.id
            WHERE  product_template.id = %s
        """
        cursor.execute(query, (product_id,))
        uom_unitat = cursor.dictfetchall()[0]['name']
        if not context['date']:
            context = {
                'date': date.today()
            }

        if tipus == 'tp':
            uom_id = product_uom_obj.search(
                cursor, uid, [('factor', '=', 365), ('name', '=', 'kW/dia')])[0]
            if calendar.isleap(datetime.today().year):
                imd_obj = pool.get('ir.model.data')
                uom_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia_traspas'
                )[1]
                context['uom'] = uom_id
            context.update({
                'uom': uom_id
            })
        pricelist = pricelist_obj.read(cursor, uid, id_preu, ['visible_discount', 'id'])
        discount = 0
        if pricelist['visible_discount']:
            date_price = date.today().strftime('%Y-%m-%d')
            version = pricelistver_obj.search(cursor, uid,
                [
                    ('pricelist_id', '=', pricelist['id']),
                    '|',
                    ('date_start', '=', False),
                    ('date_start', '<=', date_price),
                    '|',
                    ('date_end', '=', False),
                    ('date_end', '>=', date_price)

                ], order='id', limit=1)
            if version:
                items = pricelistitem_obj.search(
                        cursor, uid, [
                            ('price_version_id', '=', version[0]),
                            ('product_id', '=', product_id)
                        ]
                )
                if items:
                    params_to_read = ['price_discount']
                    for item in pricelistitem_obj.read(
                            cursor, uid, items, params_to_read):
                        if item['price_discount']:
                            discount = item['price_discount']
                            break

        price_atr = pricelist_obj.price_get(cursor, uid, [pricelist['id']],
                                            product_id, 1,
                                            1,
                                            context)[pricelist['id']]
        if discount:
            price_atr = price_atr / (1.0 + discount)
            discount = abs(discount)
        return price_atr, uom_unitat, discount
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title> Anexo de precios </title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio/report/bootstrap.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio/report/stylesheet_preus.css"/>
    </head>
    <body>
        <%
            polissa_tarifa_obj = pool.get('giscedata.polissa.tarifa')
            product_template_obj = pool.get('product.template')
            product_pricelist_obj = pool.get('product.pricelist')
            if data and data.get('context', False):
                ctx = data["context"]
            else:
                raise osv.except_osv(_('ERROR'), _('No hi ha context!'))
            if ctx.get('pricelist', False):
                id_preus = [ctx['pricelist']]
            elif ctx.get('tarifa', False):
                query = """
                    SELECT pversion.pricelist_id as pricelist_id
                    FROM   giscedata_polissa_tarifa_pricelist tarifapricelist
                    JOIN   product_pricelist_version pversion ON pversion.pricelist_id = tarifapricelist.pricelist_id
                    JOIN   product_pricelist pr ON pr.id = tarifapricelist.pricelist_id
                    WHERE  tarifa_id = %s
                    AND    pr.type = 'sale'
                    AND    (date_start IS NULL OR date_start <= %s)
                    AND    (date_end IS NULL OR date_end >= %s)
                """
                cursor.execute(query, (ctx.get('tarifa'), ctx['date'], ctx['date']))
                id_preus = cursor.dictfetchall()
                id_preus = [v['pricelist_id'] for v in id_preus]
            else:
                query = """
                    SELECT pricelist_id
                    FROM   product_pricelist_version
                    WHERE  (date_start IS NULL OR date_start <= %s)
                    AND    (date_end IS NULL OR date_end >= %s)
                """
                cursor.execute(query, (ctx['date'], ctx['date']))
                id_preus = cursor.dictfetchall()
                id_preus = [v['pricelist_id'] for v in id_preus]
            if ctx.get('tarifa', False):
                tarifa_ids = [ctx['tarifa']]
            else:
                q = """
                    SELECT tarifa_id
                    FROM giscedata_polissa_tarifa_pricelist
                    WHERE pricelist_id in %s
                """
                cursor.execute(q, (tuple(id_preus),))
                tarifa_ids = [tarifa_value[0] for tarifa_value in cursor.fetchall()]
            pricelist = {}

            tipus = [('te', 'e'), ('tp', 'p'), ('tr', 'r'), ('ep', 'm')]
            q = """
                SELECT name, id
                FROM product_pricelist
                WHERE id IN %s
            """
            cursor.execute(q, (tuple(id_preus),))
            preus_values = cursor.dictfetchall()
            q_tarifa = """
                SELECT name, id
                FROM giscedata_polissa_tarifa
                WHERE id IN %s
            """
            cursor.execute(q_tarifa, (tuple(tarifa_ids),))
            tarifa_values = cursor.dictfetchall()
            for preu_val in preus_values:
                preu_name = preu_val['name']
                pricelist.setdefault(preu_name, {})
                for tarifa  in tarifa_values:
                    tarifa_name = tarifa['name']
                    tarifa_id = tarifa['id']
                    for tipu in tipus:
                        pricelist[preu_name].setdefault(tarifa_name, {})
                        periodes = polissa_tarifa_obj.get_periodes_producte(cursor, uid, tarifa_id, tipu[0], context=None)
                        for periode in periodes:
                            price = get_atr_price(cursor, uid, periode, tipu[0], [tarifa_id], preu_val['id'], ctx)
                            pricelist[preu_name][tarifa_name].setdefault(tipu[1], {})
                            pricelist[preu_name][tarifa_name][tipu[1]][periode] = (price[0], price[1])
        %>

        <div id="title" class="">
            <h1> ${_("Anexo de Precios")} </h1>
        </div>

        ## --- Aqui es pinten les tarifes ---
        %for preu in sorted(pricelist):
            <div>
                <table class="page">
                    <tr>
                        <td>
                            <h2>${_("LISTA DE PRECIOS: ")} ${preu}</h2>
                        </td>
                    </tr>
##                 </table>
                %for acces in sorted(pricelist[preu]):
##                     <table class="page">
                        <tr>
                            <td>
                                <div class="form_section container-fluid">
                                    %if acces in ['2.1DHS','3.0A', '3.1A', '3.1A LB', '6.1A', '6.1B', '6.2', '6.3', '6.4','6.5']:
                                        <div class="gran">
                                    %else:
                                        <div class="petit">
                                    %endif
                                        <div class="annex_section_title">
                                            <h3> ${_("TARIFA DE ACCESO: ")} ${acces} </h3>
                                        </div>
                                        <%
                                            id_tarifa = polissa_tarifa_obj.search(cursor, uid, [('name', '=', acces)], limit=1)
                                            codi_ocsum = polissa_tarifa_obj.read(cursor, uid, id_tarifa, ['codi_ocsum'])[0]['codi_ocsum']

                                            if codi_ocsum in mostrar_exces:
                                                span = 'span' + str(12 / len(pricelist[preu][acces]))
                                            elif codi_ocsum in mostrar_reactiva:
                                                span = 'span' + str(max(12/3, 12/len(pricelist[preu][acces])))
                                            else:
                                                span = 'span' + str(6)
                                        %>

                                        <div class="small_text row-fluid">
                                            %if pricelist[preu][acces].get('e', False):
                                                <div class="${span}">
                                                    <div class="annex_section_subtitle">
                                                        <h4> ${_("Energia")} </h4>
                                                    </div>
                                                    <div class="annex_section_content">
                                                        <table class="data_table">
                                                            %for product in sorted(pricelist[preu][acces]['e']):
                                                                %if pricelist[preu][acces]['e'][product][0]:
                                                                    <tr>
                                                                        <td>${product}</td>
                                                                        <td>
                                                                            ${pricelist[preu][acces]['e'][product][0]}
                                                                            € / ${pricelist[preu][acces]['e'][product][1]}
                                                                        </td>
                                                                    </tr>
                                                                %endif
                                                            %endfor
                                                        </table>
                                                    </div>
                                                </div>
                                            %endif
                                            %if pricelist[preu][acces].get('p', False):
                                                <div class="${span}">
                                                    <div class="annex_section_subtitle">
                                                        <h4> ${_("Potencia")} </h4>
                                                    </div>
                                                    <div class="annex_section_content">
                                                        <table class="data_table">
                                                            %for product in sorted(pricelist[preu][acces]['p']):
                                                                %if pricelist[preu][acces]['p'][product][0]:
                                                                    <tr>
                                                                        <td>${product}</td>
                                                                        <td>
                                                                            ${pricelist[preu][acces]['p'][product][0]}
                                                                            € / ${pricelist[preu][acces]['p'][product][1]}
                                                                        </td>
                                                                    </tr>
                                                                %endif
                                                            %endfor
                                                        </table>
                                                    </div>
                                                </div>
                                            %endif
                                            %if codi_ocsum in mostrar_reactiva:
                                                %if pricelist[preu][acces].get('r', False):
                                                    <div class="${span}">
                                                        <div class="annex_section_subtitle">
                                                            <h4> ${_("Reactiva")} </h4>
                                                        </div>
                                                        <div class="annex_section_content">
                                                            <table class="data_table">
                                                                %for product in sorted(pricelist[preu][acces]['r']):
                                                                    %if pricelist[preu][acces]['r'][product][0]:
                                                                        <tr>
                                                                            <td>${product}</td>
                                                                            <td>
                                                                                ${pricelist[preu][acces]['r'][product][0]}
                                                                                € / ${pricelist[preu][acces]['r'][product][1]}
                                                                            </td>
                                                                        </tr>
                                                                    %endif
                                                                %endfor
                                                            </table>
                                                        </div>
                                                    </div>
                                                %endif
                                            %endif
                                            %if codi_ocsum in mostrar_exces:
                                                %if pricelist[preu][acces].get('m', False):
                                                    <div class="${span}">
                                                        <div class="annex_section_subtitle">
                                                            <h4> ${_("Exceso de potencia")} </h4>
                                                        </div>
                                                        <div class="annex_section_content">
                                                            <table class="data_table">
                                                                %for product in sorted(pricelist[preu][acces]['m']):
                                                                    %if pricelist[preu][acces]['m'][product][0]:
                                                                        <tr>
                                                                            <td>${product}</td>
                                                                            <td>
                                                                                ${pricelist[preu][acces]['m'][product][0]}
                                                                                € / ${pricelist[preu][acces]['m'][product][1]}
                                                                            </td>
                                                                        </tr>
                                                                    %endif
                                                                %endfor
                                                            </table>
                                                        </div>
                                                    </div>
                                                %endif
                                            %endif
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                %endfor
            </div>
        %endfor
    </body>
</html>
