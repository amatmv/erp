# -*- coding: utf-8 -*-


def get_atr_price(cursor, uid, polissa, pname, tipus, context):
    from datetime import date
    from datetime import datetime
    import calendar
    if context is None:
        context = {}

    pricelist_obj = polissa.pool.get('product.pricelist')
    pricelistver_obj = polissa.pool.get('product.pricelist.version')
    pricelistitem_obj = polissa.pool.get('product.pricelist.item')

    product_uom_obj = polissa.pool.get('product.uom')
    product_id = polissa['tarifa'].get_periodes_producte(tipus)[pname]
    if not context['date']:
        context.update({
            'date': date.today()
        })

    if tipus == 'tp' and not context.get("potencia_anual", False):
        uom_id = product_uom_obj.search(
            cursor, uid, [('factor', '=', 365), ('name', '=', 'kW/dia')])[0]
        if calendar.isleap(datetime.today().year):
            imd_obj = polissa.pool.get('ir.model.data')
            uom_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia_traspas'
            )[1]
            context['uom'] = uom_id
        context.update({
            'uom': uom_id
        })
    pricelist = polissa['llista_preu']
    discount = 0
    if pricelist.visible_discount:
        date_price = date.today().strftime('%Y-%m-%d')
        version = pricelistver_obj.search(cursor, uid,
            [
                ('pricelist_id', '=', pricelist.id),
                '|',
                ('date_start', '=', False),
                ('date_start', '<=', date_price),
                '|',
                ('date_end', '=', False),
                ('date_end', '>=', date_price)

            ], order='id', limit=1)
        if version:
            items = pricelistitem_obj.search(
                    cursor, uid, [
                        ('price_version_id', '=', version[0]),
                        ('product_id', '=', product_id)
                    ]
            )
            if items:
                params_to_read = ['price_discount']
                for item in pricelistitem_obj.read(
                        cursor, uid, items, params_to_read):
                    if item['price_discount']:
                        discount = item['price_discount']
                        break

    price_atr = pricelist_obj.price_get(cursor, uid, [pricelist.id],
                                        product_id, 1,
                                        1,
                                        context)[pricelist.id]
    if discount:
        price_atr = price_atr / (1.0 + discount)
        discount = abs(discount)
    return price_atr, discount
