<div class="seccio">
    ${_(u"DADES DEL PAGADOR:")}
    %if polissa.tipo_pago.code == 'TRANSFERENCIA_CSB':
        ${_(u"PER TRANSFERENCIA")}
    %else:
        <br>
        <table style="margin-top: 5px;">
            <tr>
                <td style="width: 60%;">
                    <%
                    owner_b = ''
                    if polissa.bank.owner_name:
                        owner_b = polissa.bank.owner_name
                    %>
                    <span class="label">${_(u"Titular del Compte:")}</span>
                    <span class="field">${owner_b}</span>
                </td>
                <%
                        nif = ''
                        import pooler
                        pool = pooler.get_pool(cursor.dbname)
                        bank_obj = pool.get('res.partner.bank')
                        field = ['owner_id']
                        exist_field = bank_obj.fields_get(
                            cursor, uid, field)
                        if exist_field:
                            owner = polissa.bank.owner_id
                            if owner:
                                nif = owner.vat or ''
                            nif = nif.replace('ES', '')
                    %>
                % if exist_field:
                    <td style="width: 40%;">
                        <span class="label">${_(u"NIF.:")}</span>
                        <span class="field">${nif}</span>
                    </td>
                % endif
            </tr>
        </table>
        <table style="margin-top: -1px;">
            <% iban = polissa.bank and polissa.bank.iban[4:] or '' %>
            <td style="width: 100px;">
                <span class="label">${_(u"Entitat Financera:")}</span>
                <span class="field">${iban[0:4]}</span>
            </td>
            <td style="width: 100px;">
                <span class="label">${_(u"Sucursal:")}</span>
                <span class="field">${iban[4:8]}</span>
            </td>
            <td style="width: 100px;">
                <span class="label">${_(u"DC:")}</span>
                <span class="field">${iban[8:10]}</span>
            </td>
            <td style="width: 100px;">
                <span class="label">${_(u"Nº Compte:")}</span>
                <span class="field">${iban[10:]}</span>
            </td>
        </table>
    %endif
</div>