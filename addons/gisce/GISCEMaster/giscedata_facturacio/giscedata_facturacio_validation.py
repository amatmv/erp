# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta
from osv import osv, fields, orm
from tools.translate import _
import json
from defs import REFUND_RECTIFICATIVE_TYPES


class GiscedataFacturacioValidationWarningTemplate(osv.osv):
    """
    We can create templates here. The templates have a code, a description, and
    a linked method. The name of this method should be stored in the field
    method. Also, the method should be declared in the class
    giscedata.facturacio.validation.validator in order to work.
    """

    _name = 'giscedata.facturacio.validation.warning.template'
    _desc = u'Template dels warnings de validació'
    _rec_name = 'code'

    def get_warning(self, cursor, uid, code, context=None):
        if context is None:
            context = {}

        warning_data = None
        warning_id = self.search(
            cursor, uid, [('code', '=', code)], limit=1, context=context
        )
        if warning_id:
            warning_data = self.read(
                cursor, uid, warning_id[0], ['description'], context=context
            )

        return warning_data

    def check_correct_json(self, cursor, uid, ids, parameters_text):
        try:
            parameters = json.loads(parameters_text)
        except ValueError as e:
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres entrats no tenen un format '
                                 'correcte de JSON')
                }
            }
        if not isinstance(parameters, dict):
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres han de ser un diccionari')
                }
            }
        return {}

    def _ff_parameters(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}

        res = {}
        for import_vals in self.read(cursor, uid, ids, ['parameters'], context):
            res[import_vals['id']] = json.dumps(
                import_vals['parameters'], indent=4, sort_keys=True
            )
        return res

    def _fi_parameters(self, cursor, uid, ids, name, value, arg, context=None):
        if not context:
            context = {}

        try:
            parameters = json.loads(value)
            self.write(cursor, uid, ids, {'parameters': parameters})
        except ValueError as e:
            pass

    TIPUS_WARNING = [
        ('lot', 'Al validar el lot'),
        ('pre-facturacio', 'Al generar la factura'),
    ]

    _columns = {
        'code': fields.char('Codi', size=4, required=True, readonly=True),
        'description': fields.char(
            u'Descripció del warning', size=1024, required=True, readonly=True,
            translate=True
        ),
        'observation': fields.text(
            u'Observació', readonly=True, translate=True
        ),
        'method': fields.char(
            u'Mètode a executar per comprovar aquest warning', size=100,
            required=True, readonly=True
        ),
        'type': fields.selection(TIPUS_WARNING, string='Tipus', readonly=True),
        'active': fields.boolean('Actiu', readonly=False),
        'parameters': fields.json('Parametres'),
        'parameters_text': fields.function(
            _ff_parameters, type='text', method=True, string='Parametres',
            fnct_inv=_fi_parameters
        ),
    }
GiscedataFacturacioValidationWarningTemplate()


class GiscedataFacturacioValidationWarning(osv.osv):
    _name = 'giscedata.facturacio.validation.warning'
    _desc = u"Warning en la validació d'una factura"

    def get_warning_text(self, cursor, uid, warning_id, context=None):
        """
        Gets Warning text from invoice warnings
        :param cursor: Database's cursor
        :param uid: User's id
        :param warning_id: Id of the warning we want the text of
        :param context: OpenERP's context
        :return: Returns a text like "[code] message"
        """
        if context is None:
            context = {}

        vals = self.read(cursor, uid, warning_id, ['name', 'message'], context)
        return '[{0}] {1}'.format(vals['name'], vals['message'])

    def create_warning_from_code(self, cursor, uid, clot_id, fact_id,
                                 warning_code, values, context=None):
        if context is None:
            context = {}
        template_obj = self.pool.get(
            'giscedata.facturacio.validation.warning.template'
        )
        warning_temp = template_obj.get_warning(
            cursor, uid, warning_code, context=context
        )
        if warning_temp:
            vals = {
                'name': warning_code,
                'message': warning_temp['description'].format(**values),
                'warning_template_id': warning_temp['id'],
                'clot_id': clot_id,
                'factura_id': fact_id,
            }
            warning_id = self.create(cursor, uid, vals)

            return warning_id
        return False

    _columns = {
        'name': fields.char(
            'Codi error', size=14, required=True, readonly=True, select=True
        ),
        'message': fields.char(
            'Missatge del warning', size=2048, required=True, readonly=True
        ),
        'warning_template_id': fields.many2one(
            'giscedata.facturacio.validation.warning.template',
            string='Plantilla del warning', select=1, readonly=True
        ),
        'factura_id': fields.many2one(
            'giscedata.facturacio.factura',
            string='Factura', select=1, readonly=True, required=False,
            ondelete='cascade'
        ),
        'clot_id': fields.many2one(
            'giscedata.facturacio.contracte_lot',
            string='Contracte lot', select=1, readonly=True, required=False,
            ondelete='cascade'
        ),
        'active': fields.boolean('Actiu', readonly=True, select=1),
    }

    _defaults = {
        'active': lambda *a: True,
    }
GiscedataFacturacioValidationWarning()


class GiscedataFacturacioValidationValidator(osv.osv):
    _name = 'giscedata.facturacio.validation.validator'
    _desc = 'Validador de factures'

    def get_checks(self, cursor, uid, validation_type=None, context=None):
        if context is None:
            context = {}

        template_obj = self. pool.get(
            'giscedata.facturacio.validation.warning.template'
        )

        search_parameters = [('active', '=', True)]
        if validation_type:
            search_parameters.append(('type', '=', validation_type))

        template_ids = template_obj.search(
            cursor, uid, search_parameters, order='code'
        )

        return [template_obj.read(cursor, uid, t_id, ['code', 'method',
                                  'parameters'], context)
                for t_id in template_ids
                ]

    def get_invoice_warnings_text(self, cursor, uid, fact_id, context=None):
        if context is None:
            context = {}

        warning_obj = self.pool.get('giscedata.facturacio.validation.warning')

        warning_ids = warning_obj.search(
            cursor, uid, [('factura_id', '=', fact_id)]
        )
        warning_messages = [
            warning_obj.get_warning_text(cursor, uid, warn_id, context)
            for warn_id in warning_ids
        ]

        return '\n'.join(warning_messages)

    def validate_invoice(self, cursor, uid, fact_id, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        warn_obj = self.pool.get('giscedata.facturacio.validation.warning')

        fact = fact_obj.browse(cursor, uid, fact_id)

        warning_ids = []
        checks = self.get_checks(
            cursor, uid, validation_type='pre-facturacio', context=context
        )
        for template_vals in checks:
            vals = getattr(self, template_vals['method'])(
                cursor, uid, fact, template_vals['parameters']
            )
            if vals is not None:
                warning_ids.append(
                    warn_obj.create_warning_from_code(
                        cursor, uid, None, fact_id,
                        template_vals['code'], vals, context=context
                    )
                )

        return warning_ids

    def get_clot_warnings_texts(self, cursor, uid, clot_id, context=None):
        if context is None:
            context = {}

        warning_obj = self.pool.get('giscedata.facturacio.validation.warning')

        warning_ids = warning_obj.search(
            cursor, uid, [('clot_id', '=', clot_id)]
        )
        warning_messages = [
            warning_obj.get_warning_text(cursor, uid, warn_id)
            for warn_id in warning_ids
        ]

        return warning_messages

    def validate_clot(self, cursor, uid, clot_id, data_inici, data_fi,
                      context=None):
        if context is None:
            context = {}

        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        warn_obj = self.pool.get('giscedata.facturacio.validation.warning')

        clot = clot_obj.browse(cursor, uid, clot_id)

        old_warn_ids = warn_obj.search(cursor, uid, [('clot_id', '=', clot_id)])
        warn_obj.unlink(cursor, uid, old_warn_ids)

        warning_ids = []
        checks = self.get_checks(
            cursor, uid, validation_type='lot', context=context
        )
        for template_vals in checks:
            vals = getattr(self, template_vals['method'])(
                cursor, uid, clot, data_inici, data_fi,
                template_vals['parameters']
            )
            if vals is not None:
                warning_ids.append(
                    warn_obj.create_warning_from_code(
                        cursor, uid, clot_id, None,
                        template_vals['code'], vals, context=context
                    )
                )

        return warning_ids

    #######################
    # INVOICE VALIDATIONS #
    #######################
    def check_consume_by_percentage(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        if parameters.get('min_amount', False) and abs(fact.amount_total) <= parameters.get('min_amount', 0.0):
            return None

        n_months = parameters['n_months']
        min_periods = parameters.get('min_periods', False)
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        context = {}
        if parameters.get("min_invoice_len"):
            context['min_invoice_len'] = parameters.get("min_invoice_len")

        max_consume = fact_obj.get_max_consume_by_contract(
            cursor, uid, fact.polissa_id.id, from_date,
            to_date, min_periods, context=context
        )

        if max_consume:
            percentage_margin = parameters['overuse_percentage']

            inv_consume = fact.energia_kwh
            if inv_consume > (max_consume * (100.0 + percentage_margin))/100.0:
                return {
                    'invoice_consume': inv_consume,
                    'percentage': percentage_margin,
                    'maximum_consume': max_consume,
                    'n_months': n_months,
                }

        return None

    def check_power_by_amount(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        n_months = parameters['n_months']
        min_periods = parameters.get('min_periods', False)
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        max_power = fact_obj.get_max_power_by_contract(
            cursor, uid, fact.polissa_id.id, from_date, to_date, min_periods
        )

        if max_power:
            margin_amount = parameters['overuse_amount']

            inv_power = fact.potencia_kwdia
            if inv_power > max_power + margin_amount:
                return {
                    'invoice_power': inv_power,
                    'margin': margin_amount,
                    'maximum_power': max_power,
                    'n_months': n_months,
                }

        return None

    def check_consume_by_amount(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        n_months = parameters['n_months']
        min_periods = parameters.get('min_periods', False)
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        max_consume = fact_obj.get_max_consume_by_contract(
            cursor, uid, fact.polissa_id.id, from_date, to_date, min_periods
        )

        if max_consume:
            margin_amount = parameters['overuse_amount']

            inv_consume = fact.energia_kwh
            if inv_consume > max_consume + margin_amount:
                return {
                    'invoice_consume': inv_consume,
                    'margin': margin_amount,
                    'maximum_consume': max_consume,
                    'n_months': n_months,
                }

        return None

    def check_minimum_consume_import(self, cursor, uid, fact, parametres):
        if parametres.get("maxim_n_warings"):
            factura_o = self.pool.get("giscedata.facturacio.factura")
            check_invoices = parametres.get("maxim_n_warings", 0)
            last_invoices = factura_o.search(
                cursor, uid,
                [('type', '=', fact.type), ('id', '!=', fact.id), ('date_invoice', '<=', fact.date_invoice), ('polissa_id', '=', fact.polissa_id.id)],
                order="date_invoice DESC", limit=check_invoices
            )
            if len(last_invoices) >= check_invoices:
                skip = True
                for inv in factura_o.read(cursor, uid, last_invoices, ['total_energia']):
                    if inv['total_energia'] > parametres['minimum_consume']:
                        skip = False
                        break
                if skip:
                    return None

        if fact.total_energia <= parametres['minimum_consume']:
            return {
                'invoice_consume': fact.total_energia,
                'minimum_consume': parametres['minimum_consume'],
            }
        return None

    def check_minimum_power_import(self, cursor, uid, fact, parametres):
        if fact.total_potencia <= parametres['minimum_power']:
            return {
                'invoice_power': fact.total_potencia,
                'minimum_power': parametres['minimum_power'],
            }
        return None

    def check_minimum_import(self, cursor, uid, fact, parametres):
        if fact.amount_total <= parametres['minimum_import']:
            return {
                'invoice_import': fact.amount_total,
                'minimum_import': parametres['minimum_import'],
            }
        return None

    def check_maximum_import_by_tariff(self, cursor, uid, fact, parametres):
        tarifa_name = fact.tarifa_acces_id.name
        if tarifa_name in parametres:
            if fact.amount_total > parametres[tarifa_name]:
                return {
                    'invoice_import': fact.amount_total,
                    'tariff': tarifa_name,
                    'maximum_import': parametres[tarifa_name],
                }

    def check_maximum_import_diari_by_tariff(self, cursor, uid, fact, parametres):
        tarifa_name = fact.tarifa_acces_id.name
        if tarifa_name in parametres and fact.data_inici and fact.data_final:
            if parametres.get('min_amount', False) and abs(fact.amount_total) <= parametres.get('min_amount', 0.0):
                return None
            ndays = (datetime.strptime(fact.data_final, "%Y-%m-%d") - datetime.strptime(fact.data_inici, "%Y-%m-%d")).days
            if ndays <= 0:
                return None
            import_diari = round(fact.amount_total / ndays, 2)
            if import_diari > parametres[tarifa_name]:
                return {
                    'invoice_import': import_diari,
                    'tariff': tarifa_name,
                    'maximum_import': parametres[tarifa_name],
                }

    def check_consume_by_both_from_mean(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        n_months = parameters['n_months']
        min_periods = parameters.get('min_periods', False)
        to_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        from_date = to_date - relativedelta(months=n_months)

        mean_consume = fact_obj.get_mean_consume_by_contract(
            cursor, uid, fact.polissa_id.id, from_date, to_date, min_periods
        )

        if mean_consume:
            percentatge = parameters['percentage_margin']
            absolute = parameters['absolute_margin']

            margin = mean_consume * percentatge / 100
            if margin < absolute:
                margin = absolute

            inv_consume = fact.energia_kwh
            in_margins = \
                mean_consume - margin < inv_consume < mean_consume + margin
            if not in_margins:
                if (mean_consume - margin) < 0:
                    minimum_consume = 0
                else:
                    minimum_consume = int(round(mean_consume - margin))
                if not inv_consume:
                    inv_consume = 0
                return {
                    'invoice_consume': int(round(inv_consume)),
                    'minimum_consume': minimum_consume,
                    'maximum_consume': int(round(mean_consume + margin)),
                    'mean_consume': int(round(mean_consume)),
                    'n_months': n_months,
                }

        return None

    def check_exceding_days(self, cursor, uid, fact, parameters):
        trad_months_fact = {
            1: 'n_days_mensual',
            2: 'n_days_bimensual',
            12: 'n_days_anual'
        }

        months_fact = fact.polissa_id.facturacio
        increment = relativedelta(months=months_fact, days=-1)

        start_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')
        expected_final_date = start_date + increment

        expected = (expected_final_date - start_date).days
        actual = (end_date - start_date).days
        difference = actual - expected

        margin = parameters[trad_months_fact[months_fact]]
        if fact.polissa_id.facturacio_potencia != 'icp':
            margin += parameters.get('inc_days_maximeter', 0)

        if difference > margin:
            return {
                'margin': margin,
                'actual_days': actual,
                'expected_days': expected
            }

        return None

    def check_missing_days(self, cursor, uid, fact, parameters):
        trad_months_fact = {
            1: 'n_days_mensual',
            2: 'n_days_bimensual',
            12: 'n_days_anual'
        }

        months_fact = fact.polissa_id.facturacio
        increment = relativedelta(months=months_fact, days=-1)

        start_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')
        expected_final_date = start_date + increment

        expected = (expected_final_date - start_date).days
        actual = (end_date - start_date).days
        difference = expected - actual
        margin = parameters[trad_months_fact[months_fact]]

        if difference > margin:
            return {
                'margin': margin,
                'actual_days': actual,
                'expected_days': expected
            }

        return None

    def check_ending_outside_lot(self, cursor, uid, fact, parameters):
        if not fact.polissa_id.lot_facturacio.data_inici:
            return None
        lot_start_date = datetime.strptime(
            fact.polissa_id.lot_facturacio.data_inici,
            '%Y-%m-%d'
        )
        if not fact.polissa_id.lot_facturacio.data_final:
            return None
        lot_end_date = datetime.strptime(
            fact.polissa_id.lot_facturacio.data_final,
            '%Y-%m-%d'
        )
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')

        if end_date < lot_start_date or lot_end_date < end_date:
            return {'end_date':end_date}
        return None

    def check_invoiced_energy(self, cursor, uid, fact, parameters):
        total_consum = 0

        tarifa_o = self.pool.get('giscedata.polissa.tarifa')
        srch_dmn = [('name', '=', '3.1A LB')]
        tarifa_mesura_en_baixa_id = tarifa_o.search(cursor, uid, srch_dmn)[0]

        for lectura in fact.lectures_energia_ids:
            if lectura.tipus == 'activa' and lectura.magnitud == 'AE':
                total_consum += lectura.consum

        if fact.tarifa_acces_id and fact.tarifa_acces_id.id == tarifa_mesura_en_baixa_id or fact.polissa_id.lectura_en_baja:
            data_inici = datetime.strptime(fact.data_inici, "%Y-%m-%d")
            data_final = datetime.strptime(fact.data_final, "%Y-%m-%d")
            dies = (data_final - data_inici).days + 1
            total_consum = total_consum * 1.04 + 0.01 * fact.polissa_id.trafo * dies * 24

        res = {
            'invoiced_energy': fact.energia_kwh,
            'measured_consumption': total_consum
        }

        if total_consum - 1 < fact.energia_kwh < total_consum + 1:
            res = None

        return res

    def check_max_theoric_consume_by_power(self, cursor, uid, fact, parameters):
        min_percent = parameters['min_percent']
        max_percent = parameters['max_percent']

        start_date = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        end_date = datetime.strptime(fact.data_final, '%Y-%m-%d')

        num_days = (end_date - start_date).days + 1
        max_power = fact.potencia * num_days * 24.0

        total_energia = fact.energia_kwh
        res = {
            'total_consume': total_energia,
            'theoric_max': max_power,
            'percent': min_percent,
            'contracted_power': fact.potencia ,
        }
        min_val = max_power * min_percent / 100.0
        max_val = max_power * (max_percent or 0.0)/ 100.0

        if max_percent and min_val < total_energia < max_val:
            return res
        elif not max_percent and min_val < total_energia:
            return res
        else:
            return None # no error

    def check_missing_energy_lines(self, cursor, uid, fact, parameters):
        has_energy_lines = any(
            line.tipus == 'energia'
            for line in fact.linia_ids
            )
        if not has_energy_lines:
            return dict(error=True)
        return None

    def check_invoice_from_delayed_contract(self, cursor, uid, fact, parameters):
        pol_date = fact.polissa_id.data_ultima_lectura or fact.polissa_id.data_alta
        pol_date = datetime.strptime(pol_date, '%Y-%m-%d')

        days = parameters['max_delayed_days']
        today = parameters.get('today')
        if today:
            today = datetime.strptime(today, '%Y-%m-%d')
        else:
            today = datetime.today() # not test covered

        ref_date = today - relativedelta(days=days)
        if pol_date < ref_date:
            return {'delayed_days':(ref_date-pol_date).days}
        return None

    def check_missing_energy_measures(self, cursor, uid, fact, parameters):
        energy_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        has_energy_readings = energy_obj.search(cursor,uid,[
            ('factura_id','=',fact.id),
            ('tipus','=','activa'),
            ])
        if not has_energy_readings:
            return dict(error=True)
        return None

    def check_overlapping_or_duplicated_invoice(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        search_AB_params = [
            ('polissa_id', '=', fact.polissa_id.id),
            ('type', '=', 'out_refund'),
            ('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_TYPES),
            ('data_final', '>=', fact.data_inici),
            ('data_inici', '<=', fact.data_final),
        ]
        ab_fact_ids = fact_obj.search(cursor, uid, search_AB_params)

        search_R_params = [
            ('polissa_id', '=', fact.polissa_id.id),
            ('type', '=', 'out_invoice'),
            ('tipo_rectificadora', '=', 'R'),
            ('data_final', '>=', fact.data_inici),
            ('data_inici', '<=', fact.data_final),
        ]
        r_fact_ids = fact_obj.search(cursor, uid, search_R_params)

        abr_fact_ids = r_fact_ids + ab_fact_ids
        no_facts_vals = fact_obj.read(cursor, uid, abr_fact_ids,['ref'])
        no_facts_ids = [ a['ref'][0] for a in no_facts_vals if a['ref'] ]
        no_facts_ids.append(fact.id)

        search_params = [
            ('polissa_id', '=', fact.polissa_id.id),
            ('type', '=', 'out_invoice'),
            ('data_final', '>=', fact.data_inici),
            ('data_inici', '<=', fact.data_final),
            ('id','not in',no_facts_ids)
        ]
        fact_ids = sorted(fact_obj.search(cursor, uid, search_params))
        if fact_ids:
            def get_text(d,a,b):
                return str(d[a]) if f[a] else str(d[b])

            fact_vals = fact_obj.read(cursor, uid, fact_ids,['number'])
            fact_names = sorted([get_text(f,'number','id') for f in fact_vals])
            colisions = "[" + ", ".join(fact_names) + "]"
            return { 'colisions': colisions }
        return None

    def check_lineas_extra_posteriors(self, cursor, uid, fact, parametres):
        fdata_final = fact.data_final
        for linia in fact.linia_ids:
            if linia.tipus == "altres" and linia.data_desde > fdata_final:
                return {}
        return None
    def check_same_init_date_fact_ener_and_pot(self, cursor, uid, fact, parameters):
        """
        Checks if the minimum date among all energy lines is the same as the
        minimum energy date.
        :param fact:
        :param parameters:
        :return:
        """
        fail = dict(error=True)
        min_date_energy = datetime.max
        for energy_line in fact.linies_energia:
            if not energy_line.data_desde:
                return fail
            date_l = datetime.strptime(energy_line.data_desde, '%Y-%m-%d')
            if date_l < min_date_energy:
                min_date_energy = date_l

        for energy_pot in fact.linies_potencia:
            if not energy_pot.data_desde:
                return fail
            date_p = datetime.strptime(energy_pot.data_desde, '%Y-%m-%d')
            if date_p < min_date_energy:
                return fail
        return None

    def check_lectures_generacio_sense_autoconsum(self, cursor, uid, fact, parameters):
        generacio = False
        for lectura in fact.lectures_energia_ids:
            if lectura.tipus == 'activa' and lectura.magnitud == 'AS':
                generacio = True
                break
        if generacio and not fact.te_autoconsum() and not fact.polissa_id.contract_type == '05':  # 05 -> Regim Especial
            return {}
        return None

    def check_factura_autoconsum(self, cursor, uid, fact, parameters):
        if fact.te_autoconsum():
            return {}
        return None

    ############################
    # CONTRACT LOT VALIDATIONS #
    ############################
    def check_minimum_consume_value(self, cursor, uid, clot, data_inici,
                                    data_fi, parametres):
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')

        consum_total, consum_periodes, periodes_lect = clot_obj.get_consume(
            cursor, uid, clot.id, data_inici, data_fi
        )

        for tipus, consum in consum_total.items():
            if consum <= parametres['minimum_consume']:
                return {
                    'consume_limit': parametres['minimum_consume'],
                    'consume_type': tipus,
                    'consume': consum
                }

        return None

    def check_if_type_5_has_reactiva(self, cursor, uid, fact, parameters):
        if fact.tarifa_acces_id.name.startswith('2'):
            for linia in fact.linia_ids:
                if linia.tipus == 'reactiva':
                    return dict(error=True)
        return None


GiscedataFacturacioValidationValidator()
