# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestWizardAgruparFactures(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_group_different_ibans(self):
        pool = self.openerp.pool
        factura_obj = pool.get('giscedata.facturacio.factura')
        period_obj = pool.get('account.period')
        wz_agrupar = pool.get('wizard.group.invoices.payment')
        bank_obj = pool.get('res.partner.bank')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user

        bank_ids = bank_obj.search(cursor, uid, [], limit=2)
        bank_obj.write(cursor, uid, bank_ids[0], {'iban': "XXXXXXXXXXXXX"})
        bank_obj.write(cursor, uid, bank_ids[1], {'iban': "YYYYYYYYYYYYY"})

        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]
        factura_id2 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0005'
        )[1]
        factura = factura_obj.browse(cursor, uid, factura_id)
        factura2 = factura_obj.browse(cursor, uid, factura_id2)

        factura.write({'partner_bank': bank_ids[0]})
        factura2.write({'partner_bank': bank_ids[1]})

        factura = factura_obj.browse(cursor, uid, factura_id)
        factura2 = factura_obj.browse(cursor, uid, factura_id2)

        self.assertNotEquals(factura.partner_bank.id, factura2.partner_bank.id)

        ids = [factura.id, factura2.id]
        period_id = period_obj.search(cursor, uid, [], limit=1)[0]
        factura_obj.write(cursor, uid, ids, {'period_id': period_id})
        factura_obj.invoice_open(cursor, uid, ids)
        ctx = {'active_ids': ids, 'model': 'giscedata.facturacio.factura'}
        wz_agrupar_id = wz_agrupar.create(cursor, uid, {}, ctx)
        wz_agrupar = wz_agrupar.browse(cursor, uid, wz_agrupar_id, ctx)
        msg = ""
        try:
            wz_agrupar.group_invoices(context=ctx)
        except Exception as e:
            msg = e.message
        finally:
            self.assertTrue("YYYYYYYYYYYYY: [u'0005/F']" in msg)
            self.assertTrue("XXXXXXXXXXXXX: [u'0004/F']" in msg)

    def test_group_with_partial_payments(self):
        pool = self.openerp.pool
        factura_obj = pool.get('giscedata.facturacio.factura')
        period_obj = pool.get('account.period')
        wz_agrupar = pool.get('wizard.group.invoices.payment')
        journal_obj = pool.get('account.journal')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user

        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]
        factura_id2 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0005'
        )[1]
        factura = factura_obj.browse(cursor, uid, factura_id)
        factura2 = factura_obj.browse(cursor, uid, factura_id2)
        factura2.invoice_open()
        amount_fact2 = factura2.amount_total

        wizard_pay_invoice = pool.get('facturacio.pay.invoice')
        ctx = {
            'active_id': factura_id2, 'active_ids': [factura_id2]
        }
        journal_id = journal_obj.search(cursor, uid, [
            ('code', '=', 'CAJA')
        ])[0]
        wiz_id = wizard_pay_invoice.create(cursor, uid, {
            'state': 'init',
            'name': 'Partial payment',
            'amount': 2.0,
            'journal_id': journal_id
        }, context=ctx)
        wizard_pay_invoice.action_pay_and_reconcile(
            cursor, uid, wiz_id, context=ctx
        )
        factura2 = factura_obj.browse(cursor, uid, factura_id2)
        self.assertEqual(factura2.residual, amount_fact2-2.0)

        ids = [factura.id, factura2.id]

        period_id = period_obj.search(cursor, uid, [], limit=1)[0]
        factura_obj.write(cursor, uid, ids, {'period_id': period_id})
        factura_obj.invoice_open(cursor, uid, ids)
        ctx = {'active_ids': ids, 'model': 'giscedata.facturacio.factura'}

        wz_agrupar_id = wz_agrupar.create(cursor, uid, {}, ctx)
        wz_agrupar = wz_agrupar.browse(cursor, uid, wz_agrupar_id, ctx)
        wz_agrupar.group_invoices(context=ctx)
        total_grouped = factura2.residual + factura.residual
        group_move_ref = factura2.group_move_id.ref
        for move_line in factura2.group_move_id.line_id:
            if move_line.name == group_move_ref:
                self.assertEqual(move_line.debit, total_grouped)

        # pay group invoice
        wizard_pay_invoice = pool.get('facturacio.pay.invoice')
        ctx = {
            'active_id': factura_id2, 'active_ids': [factura_id2]
        }
        journal_id = journal_obj.search(cursor, uid, [
            ('code', '=', 'CAJA')
        ])[0]
        wiz_id = wizard_pay_invoice.create(cursor, uid, {
            'state': 'init',
            'name': 'Partial payment',
            'amount': total_grouped,
            'journal_id': journal_id
        }, context=ctx)
        wizard_pay_invoice.action_pay_and_reconcile(
            cursor, uid, wiz_id, context=ctx
        )
        factura2 = factura_obj.browse(cursor, uid, factura_id2)
        factura = factura_obj.browse(cursor, uid, factura_id)
        self.assertEqual(factura2.state, 'paid')
        self.assertEqual(factura.state, 'paid')
