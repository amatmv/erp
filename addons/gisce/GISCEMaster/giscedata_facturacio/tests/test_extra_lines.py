# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from collections import namedtuple
import json


class TestsInvoiceExtraLines(testing.OOTestCase):

    def facturar_periode(
            self, cursor, uid, polissa_id, data_inici, data_fi, context=None):

        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")

        wz_fact_id = wz_mi_obj.create(cursor, uid, {})
        wz_mi_obj.write(cursor, uid, [wz_fact_id], {
            'polissa_id': polissa_id,
            'date_start': data_inici,
            'date_end': data_fi,
            'journal_id': context['journal_id']
        })
        wz_mi_obj.action_manual_invoice(cursor, uid, [wz_fact_id])
        fact_id = json.loads(wz_mi_obj.read(
            cursor, uid, wz_fact_id, ['invoice_ids']
        )[0]['invoice_ids'])[0]
        period_id = self.imd_obj.get_object_reference(
            cursor, uid, 'account', 'period_1'
        )[1]
        self.fact_obj.write(cursor, uid, fact_id, {'period_id': period_id})
        self.fact_obj.invoice_open(cursor, uid, [fact_id])
        return fact_id

    def setUp(self):
        self.imd_obj = self.openerp.pool.get('ir.model.data')
        self.contract_obj = self.openerp.pool.get('giscedata.polissa')
        self.extra_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        self.journal_obj = self.openerp.pool.get('account.journal')
        self.meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        self.fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        contract_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        journal_id = self.journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]

        meter_id = self.contract_obj.read(
            cursor, uid, contract_id, ['comptadors']
        )['comptadors']

        self.meter_obj.write(cursor, uid, meter_id, {'lloguer': False})

        self.contract_obj.send_signal(cursor, uid, [contract_id], [
            'validar', 'contracte'
        ])

        Environment = namedtuple('Environment',
                                 ['cursor', 'uid', 'contract_id', 'journal_id'])
        self.env_variables = Environment(cursor, uid, contract_id, journal_id)

    def tearDown(self):
        self.txn.stop()

    def test_create_extra_line_with_lesser_price_term_with_single_quantity(self):
        """
        Tests that the extraline is correctly invoiced when the price invoiced
        per month is lesser than the price per term and the quantity of the
        extra line product is only 1.

        - In this case, the price per term will be: 0.124; while the price
        invoiced for each invoice will be 0.12, because is rounded with two
        decimals, and the quantity will be 1.
        """

        # Retrieving the environment variables from global named tuple
        cursor, uid, contract_id, journal_id = self.env_variables

        extraline_price = 0.372

        extraline_id = self.extra_obj.create(cursor, uid, {
            'polissa_id': contract_id,
            'name': 'TEST_EXTRALINE_1_1',
            'price_unit': extraline_price,
            'term': 3,
            'date_from': '2014-09-01',
            'date_to': '2016-05-31',
            'date_line_from': '2013-01-01',
            'date_line_to': '2013-12-31',
            'quantity': 1,
            'journal_ids': [(6, 0, [journal_id])]
        })

        periodes_a_facturar = (
            ('2016-02-01', '2016-02-29'), ('2016-03-01', '2016-03-31'),
            ('2016-04-01', '2016-04-30')
        )
        for data_inici, data_fi in periodes_a_facturar:
            self.facturar_periode(
                cursor, uid, contract_id, data_inici, data_fi,
                context={'journal_id': journal_id}
            )

        self.assertEquals(
            round(extraline_price, 2),
            self.extra_obj.read(
                cursor, uid, extraline_id, ['amount_invoiced']
            )['amount_invoiced']
        )

    def test_create_extra_line_with_greater_price_term_with_single_quantity(self):
        """
        Tests that the extraline is correctly invoiced when the price invoiced
        per month is greater than the price per term and the quantity of the
        extra line product is only 1.

        -In this case, the price per term will be: 0.125; while the price
        invoiced for each invoice will be 0.13, because is rounded with two
        decimals, and the quantity will be 1.
        """

        # Retrieving the environment variables from global named tuple
        cursor, uid, contract_id, journal_id = self.env_variables

        extraline_price = 0.375

        extraline_id = self.extra_obj.create(cursor, uid, {
            'polissa_id': contract_id,
            'name': 'TEST_EXTRALINE_1_2',
            'price_unit': extraline_price,
            'term': 3,
            'date_from': '2014-09-01',
            'date_to': '2016-05-31',
            'date_line_from': '2013-01-01',
            'date_line_to': '2013-12-31',
            'quantity': 1,
            'journal_ids': [(6, 0, [journal_id])]
        })

        periodes_a_facturar = (
            ('2016-02-01', '2016-02-29'), ('2016-03-01', '2016-03-31'),
            ('2016-04-01', '2016-04-30')
        )
        for data_inici, data_fi in periodes_a_facturar:
            self.facturar_periode(
                cursor, uid, contract_id, data_inici, data_fi,
                context={'journal_id': journal_id}
            )

        self.assertEquals(
            round(extraline_price, 2),
            self.extra_obj.read(
                cursor, uid, extraline_id, ['amount_invoiced']
            )['amount_invoiced']
        )

    def test_create_extra_line_with_lesser_price_term_with_multiple_quantity(self):
        """
        Tests that the extraline is correctly invoiced when the price invoiced
        per month is lesser than the price per term and the quantity of the
        extra line product is **greater** than 1.

        - In this case, the price per term will be: 0.124; while the price
        invoiced for each invoice will be 0.12, because is rounded with two
        decimals, and the quantity will be 5.
        """

        # Retrieving the environment variables from global named tuple
        cursor, uid, contract_id, journal_id = self.env_variables

        extraline_price = 0.372
        quantity = 5

        extraline_id = self.extra_obj.create(cursor, uid, {
            'polissa_id': contract_id,
            'name': 'TEST_EXTRALINE_2_1',
            'price_unit': extraline_price,
            'term': 3,
            'date_from': '2014-09-01',
            'date_to': '2016-05-31',
            'date_line_from': '2013-01-01',
            'date_line_to': '2013-12-31',
            'quantity': quantity,
            'journal_ids': [(6, 0, [journal_id])]
        })

        periodes_a_facturar = (
            ('2016-02-01', '2016-02-29'), ('2016-03-01', '2016-03-31'),
            ('2016-04-01', '2016-04-30')
        )
        for data_inici, data_fi in periodes_a_facturar:
            self.facturar_periode(
                cursor, uid, contract_id, data_inici, data_fi,
                context={'journal_id': journal_id}
            )

        self.assertEquals(
            round(extraline_price * quantity, 2),
            self.extra_obj.read(
                cursor, uid, extraline_id, ['amount_invoiced']
            )['amount_invoiced']
        )

    def test_create_extra_line_with_greater_price_term_with_multiple_quantity(self):
        """
        Tests that the extraline is correctly invoiced when the price invoiced
        per month is greater than the price per term, and the quantity of the
        extra line product is **greater** than 1.

        - In this case: the price per term will be: 0.125; while the price
        invoiced for each invoice will be 0.13, because is rounded with two
        decimals, and the quantity will be 5.
        """

        # Retrieving the environment variables from global named tuple
        cursor, uid, contract_id, journal_id = self.env_variables

        extraline_price = 0.375
        quantity = 5

        extraline_id = self.extra_obj.create(cursor, uid, {
            'polissa_id': contract_id,
            'name': 'TEST_EXTRALINE_2_2',
            'price_unit': extraline_price,
            'term': 3,
            'date_from': '2014-09-01',
            'date_to': '2016-05-31',
            'date_line_from': '2013-01-01',
            'date_line_to': '2013-12-31',
            'quantity': quantity,
            'journal_ids': [(6, 0, [journal_id])]
        })

        periodes_a_facturar = (
            ('2016-02-01', '2016-02-29'), ('2016-03-01', '2016-03-31'),
            ('2016-04-01', '2016-04-30')
        )
        for data_inici, data_fi in periodes_a_facturar:
            self.facturar_periode(
                cursor, uid, contract_id, data_inici, data_fi,
                context={'journal_id': journal_id}
            )

        self.assertEquals(
            round(extraline_price * quantity, 2),
            self.extra_obj.read(
                cursor, uid, extraline_id, ['amount_invoiced']
            )['amount_invoiced']
        )
