# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
import mock
import json


class TestsManualInvoiceWizard(testing.OOTestCase):

    def test_manual_invoice_without_lect_pot(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            lot_id = lot_obj.search(cursor, uid, [
                ('name', '=', '11/2017'),

            ], limit=1)[0]

            vals = {
                'data_alta': '2017-10-01',
                'data_baixa': False,
                'potencia': 8.050,
                'data_ultima_lectura': '2017-10-28',
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '00',
                'contract_type': '01',
                'lot_facturacio': lot_id
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                # Delete lloguer because it will fail in manual invoice because
                # it's implemented in comer/dist facturacio modules
                comptador.write({'lloguer': False})

            comptador = contract.comptadors[0]
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            vals = {
                'name': '2017-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            vals = {
                'name': '2017-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2017-11-01',
                'date_end': '2017-11-30',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2017-11-01')
            # Check lines
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 1000)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 8.05)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 1)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 9000)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 8000)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2017-10-28')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 8.05)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)
            self.assertFalse(invoice.lot_facturacio)

    def test_manual_invoice_with_lect_pot(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        lectura_pot_obj = self.openerp.pool.get('giscedata.lectures.potencia')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            lot_id = lot_obj.search(cursor, uid, [
                ('name', '=', '11/2017'),

            ], limit=1)[0]

            vals = {
                'data_alta': '2017-10-01',
                'data_baixa': False,
                'potencia': 8.050,
                'data_ultima_lectura': '2017-10-28',
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '00',
                'contract_type': '01',
                'lot_facturacio': lot_id
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                # Delete lloguer because it will fail in manual invoice because
                # it's implemented in comer/dist facturacio modules
                comptador.write({'lloguer': False})

            comptador = contract.comptadors[0]
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            vals = {
                'name': '2017-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)
            vals.pop('tipus')
            lectura_pot_obj.create(cursor, uid, vals)

            vals = {
                'name': '2017-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)
            vals.pop('tipus')
            lectura_pot_obj.create(cursor, uid, vals)

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2017-11-01',
                'date_end': '2017-11-30',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2017-11-01')
            # Check lines
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 1000)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 8.05)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 1)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 9000)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 8000)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2017-10-28')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 8.05)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 9000)
            self.assertEqual(invoice.lectures_potencia_ids[0].data_actual, '2017-11-28')
            self.assertFalse(invoice.lot_facturacio)

    def test_manual_invoice_extra_lines(self):

        self.openerp.install_module('giscedata_tarifas_peajes_20180101')

        imd_obj = self.openerp.pool.get('ir.model.data')
        extra_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            lot_obj.crear_lots_mensuals(cursor, uid, 2018)

            lot_id = lot_obj.search(cursor, uid, [
                ('name', '=', '12/2018'),

            ], limit=1)[0]

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            vals = {
                'data_alta': '2018-10-01',
                'data_baixa': False,
                'potencia': 8.050,
                'data_ultima_lectura': '2018-10-28',
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '00',
                'contract_type': '01',
                'lot_facturacio': lot_id
            }
            contract_obj.write(cursor, uid, polissa_id, vals)
            contract_obj.send_signal(cursor, uid, [polissa_id], [
                'validar', 'contracte'
            ])

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]

            extra_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'facturacio_extraline_01'
            )[1]

            # Set period for extraline
            extra_obj.write(cursor, uid, extra_id, {
                'date_from': '2018-12-10',
                'date_to': '2018-12-20',
                'journal_ids': [(4, journal_id)]
            })

            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': polissa_id,
                'date_start': '2018-12-01',
                'date_end': '2018-12-31',
                'journal_id': journal_id,
                'extra_lines_invoice': 1
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)

            self.assertEqual(len(invoice.linia_ids), 1)
            linia = invoice.linia_ids[0]
            self.assertEqual(linia.tipus, 'altres')
            self.assertFalse(invoice.lot_facturacio)

    def get_productes_autoconsum(self, cursor, uid, tipus="excedent", context=None):
        """
        Tipus esperats: ['autoconsum', 'generacio', 'excedent']
        """
        from gestionatr.input.messages.F1 import CODIS_AUTOCONSUM
        res = {}
        imd_o = self.openerp.pool.get("ir.model.data")
        for codi_producte, tipus_producte in CODIS_AUTOCONSUM.iteritems():
            if tipus_producte == tipus:
               res['P'+codi_producte[1]] = imd_o.get_object_reference(
                   cursor, uid, "giscedata_facturacio", "concepte_demo_{}".format(codi_producte)
               )[1]
        return res

    @mock.patch('giscedata_facturacio.giscedata_facturacio.PRODUCTES', {})
    @mock.patch('giscedata_facturacio.giscedata_facturacio.GiscedataFacturacioFacturador.get_productes_autoconsum')
    def test_manual_invoice_with_auotconsum(self, mock_function):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            lot_id = lot_obj.search(cursor, uid, [
                ('name', '=', '11/2017'),

            ], limit=1)[0]

            vals = {
                'data_alta': '2017-10-01',
                'data_baixa': False,
                'potencia': 8.050,
                'data_ultima_lectura': '2017-10-28',
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '41',
                'contract_type': '01',
                'lot_facturacio': lot_id,
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                # Delete lloguer because it will fail in manual invoice because
                # it's implemented in comer/dist facturacio modules
                comptador.write({'lloguer': False})

            comptador = contract.comptadors[0]
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            vals = {
                'name': '2017-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'lectura_exporta': 100,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            vals = {
                'name': '2017-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'lectura_exporta': 210,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2017-11-01',
                'date_end': '2017-11-30',
                'journal_id': journal_id
            })
            mock_function.return_value = self.get_productes_autoconsum(cursor, uid)
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2017-11-01')
            # Check lines
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 1000)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 8.05)
            self.assertEqual(len(invoice.linies_generacio), 1)
            self.assertEqual(invoice.linies_generacio[0].tipus, 'generacio')
            self.assertEqual(invoice.linies_generacio[0].invoice_line_id.quantity, -110)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 2)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].magnitud, 'AE')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 9000)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 8000)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2017-10-28')
            self.assertEqual(invoice.lectures_energia_ids[1].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[1].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[1].magnitud, 'AS')
            self.assertEqual(invoice.lectures_energia_ids[1].lect_actual, 210)
            self.assertEqual(invoice.lectures_energia_ids[1].lect_anterior, 100)
            self.assertEqual(invoice.lectures_energia_ids[1].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[1].data_anterior, '2017-10-28')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 8.05)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)
            self.assertFalse(invoice.lot_facturacio)
