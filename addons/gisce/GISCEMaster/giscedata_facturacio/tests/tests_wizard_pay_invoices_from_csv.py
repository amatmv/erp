# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm
import pandas as pd
from datetime import datetime
import netsvc
import time
import base64
from destral.patch import PatchNewCursors
from account_invoice_base.tests.tests_account_invoice import TestPayInvoice
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


class TestWizardPayInvoicesFromCsv(testing.OOTestCaseWithCursor):

    def check_get_invoice_to_be_paid(self, cursor, uid, invoice_id=None, invoice_ref=None, context=None):
        """
        Get an invoice that is opened without any partial/full payment made. Contability is check to be correct.
        """
        if context is None:
            context = {}
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        if not invoice_id:
            if not invoice_ref:
                invoice_ref = 'test_invoice_original_unpaid'
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', invoice_ref
            )[1]
        invoice_obj.write(cursor, uid, invoice_id, context.get("extra_vals", {}))
        if context.get('copy'):
            invoice_id = invoice_obj.copy(cursor, uid, invoice_id)
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cursor)
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        return invoice

    def make_payment(self, cursor, uid, ammount_to_pay, invoice, journal_id=None, period_id=None, context=None):
        imd_obj = self.openerp.pool.get('ir.model.data')
        if not journal_id:
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'bank_journal'
            )[1]

        acc_id = self.openerp.pool.get("account.journal").read(
            cursor, uid, journal_id, ['default_credit_account_id']
        )['default_credit_account_id'][0]

        if not period_id:
            period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'period_1'
            )[1]

        invoice.pay_and_reconcile(
            ammount_to_pay, acc_id, period_id, journal_id,
            False, period_id, False, context=context
        )
        invoice = invoice.browse()[0]
        return invoice

    def test_correct_wizard_process_csv_file_payment_multiple(self):
        """
        Testeja el pay_and_reconcile_multiple cridat desde pagar factures de CSV.
        El fitxer porta:
            - out_invoice positiva (a cobrar)
            - out_invoice negativa (a pagar)
            - out_refund positiva (a pagar)
            - out_refund negativa (a cobrar)
        El total de les factures es a cobrar.
        """

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wiz_obj = self.openerp.pool.get('wizard.pay.invoices.from.csv')

        cursor = self.cursor
        uid = self.uid
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_o = self.openerp.pool.get('res.config')
        conf_o.set(
            cursor, uid, 'pay_from_csv_unique_account_move', '1'
        )
        # Una factura normal
        invoice2 = self.check_get_invoice_to_be_paid(cursor, uid)
        factura2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        fact_obj.write(cursor, uid, factura2_id, {'invoice_id': invoice2.id})
        # Una factura normal negativa amb un pagament parcial fet
        invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
        invoice = self.make_payment(cursor, uid, -200, invoice)
        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        fact_obj.write(cursor, uid, factura_id, {'invoice_id': invoice.id})
        # Una factura abonadora amb pagament parcial fet
        invoice3 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2', context={'extra_vals': {'type': 'out_refund'}})
        invoice3 = self.make_payment(cursor, uid, 100, invoice3)
        factura3_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0003'
        )[1]
        fact_obj.write(cursor, uid, factura3_id, {'invoice_id': invoice3.id})

        # Una factura abonadora negativa
        invoice4 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2_negative', context={'extra_vals': {'type': 'out_refund'}})
        factura4_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]
        fact_obj.write(cursor, uid, factura4_id, {'invoice_id': invoice4.id})

        list_to_be_processed = []
        # Prepare array to be processed by pandas
        for inv in [invoice, invoice2, invoice3, invoice4]:
            list_to_be_processed.append(
                [inv.number, inv.residual]
            )

        # Generate csv file with padas library
        my_df = pd.DataFrame(list_to_be_processed)
        csv_file = StringIO.StringIO()
        my_df.to_csv(
            csv_file, index=False, header=False, sep=';'
        )

        # Sets wizard values
        with PatchNewCursors():
            ctx = {}
            values = {
                'partner_id': False,
                'journal_id': 1,
                'fact_mode': 'gen',
                'fact_date': datetime.strptime(
                    time.strftime('%Y-%m-%d'), '%Y-%m-%d'
                ),
                'file_fact': base64.b64encode(csv_file.getvalue()),
            }

            # Create wizard
            wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id, context=ctx)
            wiz.importar(context=ctx)
        for inv in [invoice, invoice2, invoice3, invoice4]:
            inv = inv.browse()[0]
            self.assertEqual(inv.residual, 0.0)
            self.assertEqual(inv.state, 'paid')
        mline_o = self.openerp.pool.get("account.move.line")
        self.assertEqual(
            len(mline_o.search(cursor, uid, [('ref', 'like', "Pagament factures csv"), ('debit', '=', 300.0)])),
            1
        )

    def test_correct_wizard_process_csv_file_payment_multiple_total_negative(self):
        """
        Testeja el pay_and_reconcile_multiple cridat desde pagar factures de CSV.
        El fitxer porta:
            - out_invoice positiva (a cobrar)
            - out_invoice negativa (a pagar)
            - out_refund positiva (a pagar)
            - out_refund negativa (a cobrar)
        El total de les factures es a pagar.
        """

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wiz_obj = self.openerp.pool.get('wizard.pay.invoices.from.csv')

        cursor = self.cursor
        uid = self.uid
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_o = self.openerp.pool.get('res.config')
        conf_o.set(
            cursor, uid, 'pay_from_csv_unique_account_move', '1'
        )
        # Una factura normal
        invoice2 = self.check_get_invoice_to_be_paid(cursor, uid)
        factura2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        fact_obj.write(cursor, uid, factura2_id, {'invoice_id': invoice2.id})
        # Una factura normal negativa
        invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        fact_obj.write(cursor, uid, factura_id, {'invoice_id': invoice.id})
        # Una factura abonadora amb pagament parcial fet
        invoice3 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2', context={'extra_vals': {'type': 'out_refund'}})
        invoice3 = self.make_payment(cursor, uid, -4000, invoice3)
        factura3_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0003'
        )[1]
        fact_obj.write(cursor, uid, factura3_id, {'invoice_id': invoice3.id})

        # Una factura abonadora negativa
        invoice4 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2_negative', context={'extra_vals': {'type': 'out_refund'}})
        factura4_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]
        fact_obj.write(cursor, uid, factura4_id, {'invoice_id': invoice4.id})

        list_to_be_processed = []
        # Prepare array to be processed by pandas
        for inv in [invoice, invoice2, invoice3, invoice4]:
            list_to_be_processed.append(
                [inv.number, inv.residual]
            )

        # Generate csv file with padas library
        my_df = pd.DataFrame(list_to_be_processed)
        csv_file = StringIO.StringIO()
        my_df.to_csv(
            csv_file, index=False, header=False, sep=';'
        )

        # Sets wizard values
        with PatchNewCursors():
            ctx = {}
            values = {
                'partner_id': False,
                'journal_id': 1,
                'fact_mode': 'gen',
                'fact_date': datetime.strptime(
                    time.strftime('%Y-%m-%d'), '%Y-%m-%d'
                ),
                'file_fact': base64.b64encode(csv_file.getvalue()),
            }

            # Create wizard
            wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id, context=ctx)
            wiz.importar(context=ctx)
        for inv in [invoice, invoice2, invoice3, invoice4]:
            inv = inv.browse()[0]
            self.assertEqual(inv.residual, 0.0)
            self.assertEqual(inv.state, 'paid')
        mline_o = self.openerp.pool.get("account.move.line")
        self.assertEqual(
            len(mline_o.search(cursor, uid, [('ref', 'like', "Pagament factures csv"), ('credit', '=', 4000.0)])),
            1
        )

    def test_wizard_process_correctly_csv_file(self):
        # This test generate a temporary CSV file with next format:
        # COLUMN_0: invoice_number, COLUMN_1: amount
        # with valid invoices to be payed and tries to pay them.
        # If the process ends correctly then test passed.

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        inv_obj = self.openerp.pool.get('account.invoice')
        wiz_obj = self.openerp.pool.get('wizard.pay.invoices.from.csv')

        cursor = self.cursor
        uid = self.uid

        invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, context={'copy': True})

        imd_obj = self.openerp.pool.get('ir.model.data')
        factura2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        fact_obj.write(cursor, uid, factura2_id, {'invoice_id': invoice2.id})

        list_to_be_processed = []
        # Prepare array to be processed by pandas
        for inv in [invoice2]:
            list_to_be_processed.append(
                [inv.number, inv.residual]
            )

        # Generate csv file with padas library
        my_df = pd.DataFrame(list_to_be_processed)
        csv_file = StringIO.StringIO()
        my_df.to_csv(
            csv_file, index=False, header=False, sep=';'
        )

        # Sets wizard values
        with PatchNewCursors():
            ctx = {}
            values = {
                'partner_id': False,
                'journal_id': 1,
                'fact_mode': 'gen',
                'fact_date': datetime.strptime(
                    time.strftime('%Y-%m-%d'), '%Y-%m-%d'
                ),
                'file_fact': base64.b64encode(csv_file.getvalue()),
            }

            # Create wizard
            wiz_id = wiz_obj.create(cursor, uid, values, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id, context=ctx)
            wiz.importar(context=ctx)
        invoice2 = invoice2.browse()[0]
        self.assertEqual(invoice2.residual, 0.0)
        self.assertEqual(invoice2.state, 'paid')
