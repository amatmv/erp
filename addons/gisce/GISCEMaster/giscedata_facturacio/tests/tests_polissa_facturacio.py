# -*- coding: utf-8 -*-
import collections
from expects import expect
from expects import raise_error

from ..defs import *
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm
from osv.osv import except_osv
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from giscedata_polissa.tests import utils as utils_polissa


class TestFacturaLecturesEnergia(testing.OOTestCase):
    def test_creating_factura_lectures_energia_copies_ajust(self):
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        lec_ene_obj = self.openerp.pool.get(
            'giscedata.facturacio.lectures.energia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            (comptador_name, comptador_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            lect_act = {
                'periode': (1, 'Periode_001'),
                'lectura': 10,
                'consum': 5,
                'ajust': 5,
                'motiu_ajust': MOTIUS_AJUST[0][0],
                'name': '2016-02-29',
                'comptador': (comptador_id, comptador_name),
                'tipus': 'A'
            }

            lect_ant = {
                'lectura': 5,
                'name': '2016-01-31',
            }

            lectures_ids = facturador_obj.crear_lectures_energia(
                cursor, uid, factura_id, {
                    1: {'actual': lect_act, 'anterior': lect_ant}
                }
            )

            lectures = lec_ene_obj.browse(cursor, uid, lectures_ids[0])
            assert lectures.ajust == 5
            assert lectures.motiu_ajust == MOTIUS_AJUST[0][0]

    def test_cancelacio_polissa_activa(self):
        """ Aquest test comprova que una pòlissa activa es tanca correctament,
        comprovant que no té cap factura associada a aquesta."""

        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            imd_obj = pool.get('ir.model.data')

            # Importing a contract that has some invices assigned to, so it
            # should NOT be able to cancel it.

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id, {'state': 'activa'})

            def condition_to_check():
                return polissa_obj.cnd_activa_cancelar(cursor, uid, polissa_id)

            self.assertRaises(except_osv, condition_to_check)

            polissa_obj.wkf_cancelar(cursor, uid, polissa_id)

            self.assertEquals(
                polissa_obj.read(
                    cursor, uid, polissa_id, ['state']
                )['state'], u'cancelada'
            )

            # Importing a contract that doesn't have some invices assigned to,
            # so it should be able to cancel it.

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id, {'state': 'activa'})
            self.assertTrue(
                polissa_obj.cnd_activa_cancelar(cursor, uid, polissa_id)
            )

    def test_factura_lectures_energia_ajust_is_0_by_default(self):
        lec_ene_obj = self.openerp.pool.get(
            'giscedata.facturacio.lectures.energia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            comptador_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            lec_ene_id = lec_ene_obj.create(
                cursor, uid, {
                    'name': 'P1',
                    'comptador_id': comptador_id,
                    'factura_id': factura_id,
                    'tipus': 'activa',
                    'magnitud': 'AE',
                    'data_actual': '2016-02-29',
                    'lect_actual': 10,
                    'data_anterior': '2016-01-31',
                    'lect_anterior': 5,
                    'consum': 5,
                }
            )
            lec_ene = lec_ene_obj.browse(cursor, uid, lec_ene_id)

            assert lec_ene.ajust == 0
            assert not lec_ene.motiu_ajust

class TestPolissaFacturacio(testing.OOTestCase):

    def test_no_update_ult_lectura_facturada_in_draft(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        polissa_obj = self.openerp.pool.get(
            'giscedata.polissa'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            factura = factura_obj.browse(cursor, uid, factura_id)
            polissa_id = factura.polissa_id.id
            data_ultima_lectura = '2018-01-01'
            polissa_obj.write(cursor, uid, [polissa_id], {'data_ultima_lectura': data_ultima_lectura})
            factura_obj.unlink(cursor, uid, [factura_id])
            data_ultima_lectura_post_unlink = polissa_obj.read(
                cursor, uid, polissa_id, ['data_ultima_lectura']
            )['data_ultima_lectura']
            self.assertEquals(data_ultima_lectura, data_ultima_lectura_post_unlink)


class TestMoveInvoicesLot(testing.OOTestCase):
    def test_we_can_move_cont_lots_in_esborrany(self):
        cont_lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot'
        )
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cont_lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001'
            )[1]
            lot_desti_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0002'
            )[1]
            cont_lot = cont_lot_obj.read(
                cursor, uid, cont_lot_id, ['polissa_id', 'lot_id']
            )
            polissa_id = cont_lot['polissa_id'][0]
            lot_ori_id = cont_lot['lot_id'][0]

            pol_obj.moure_al_lot(
                cursor, uid, cont_lot_id, polissa_id, lot_desti_id
            )

            lot_ori_contr_log = lot_obj.read(
                cursor, uid, lot_ori_id, ['contracte_lot_ids']
            )['contracte_lot_ids']
            assert cont_lot_id not in lot_ori_contr_log

            contr_lot_ids = lot_obj.read(
                cursor, uid, lot_desti_id, ['contracte_lot_ids']
            )['contracte_lot_ids']
            polisses_ids = cont_lot_obj.read(
                cursor, uid, contr_lot_ids, ['polissa_id']
            )
            polisses_lot_dest = [
                cont_lot['polissa_id'][0] for cont_lot in polisses_ids
            ]
            assert polissa_id in polisses_lot_dest

    def test_we_can_move_cont_lots_in_obert(self):
        cont_lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot'
        )
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cont_lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0002'
            )[1]
            lot_desti_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0002'
            )[1]
            cont_lot = cont_lot_obj.read(
                cursor, uid, cont_lot_id, ['polissa_id', 'lot_id']
            )
            polissa_id = cont_lot['polissa_id'][0]
            lot_ori_id = cont_lot['lot_id'][0]

            pol_obj.moure_al_lot(
                cursor, uid, cont_lot_id, polissa_id, lot_desti_id
            )

            lot_ori_contr_log = lot_obj.read(
                cursor, uid, lot_ori_id, ['contracte_lot_ids']
            )['contracte_lot_ids']
            assert cont_lot_id not in lot_ori_contr_log

            contr_lot_ids = lot_obj.read(
                cursor, uid, lot_desti_id, ['contracte_lot_ids']
            )['contracte_lot_ids']
            polisses_ids = cont_lot_obj.read(
                cursor, uid, contr_lot_ids, ['polissa_id']
            )
            polisses_lot_dest = [
                cont_lot['polissa_id'][0] for cont_lot in polisses_ids
            ]
            assert polissa_id in polisses_lot_dest

    def test_we_can_move_cont_lots_in_facturar(self):
        cont_lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot'
        )
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cont_lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0003'
            )[1]
            lot_desti_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0002'
            )[1]
            cont_lot = cont_lot_obj.read(
                cursor, uid, cont_lot_id, ['polissa_id', 'lot_id']
            )
            polissa_id = cont_lot['polissa_id'][0]
            lot_ori_id = cont_lot['lot_id'][0]

            pol_obj.moure_al_lot(
                cursor, uid, cont_lot_id, polissa_id, lot_desti_id
            )

            lot_ori_contr_log = lot_obj.read(
                cursor, uid, lot_ori_id, ['contracte_lot_ids']
            )['contracte_lot_ids']
            assert cont_lot_id not in lot_ori_contr_log

            contr_lot_ids = lot_obj.read(
                cursor, uid, lot_desti_id, ['contracte_lot_ids']
            )['contracte_lot_ids']
            polisses_ids = cont_lot_obj.read(
                cursor, uid, contr_lot_ids, ['polissa_id']
            )
            polisses_lot_dest = [
                cont_lot['polissa_id'][0] for cont_lot in polisses_ids
            ]
            assert polissa_id in polisses_lot_dest

    def test_we_cant_move_to_closed_lots(self):
        cont_lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot'
        )
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cont_lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001'
            )[1]
            lot_desti_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_tancat'
            )[1]
            cont_lot = cont_lot_obj.read(
                cursor, uid, cont_lot_id, ['polissa_id', 'lot_id']
            )
            polissa_id = cont_lot['polissa_id'][0]

            expect(
                lambda: pol_obj.moure_al_lot(
                    cursor, uid, cont_lot_id, polissa_id, lot_desti_id
                )
            ).to(
                raise_error(except_osv)
            )

    def test_we_cant_move_contract_to_before_data_alta(self):
        cont_lot_obj = self.openerp.pool.get(
            'giscedata.facturacio.contracte_lot'
        )
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cont_lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0001'
            )[1]
            lot_desti_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_antic'
            )[1]
            cont_lot = cont_lot_obj.read(
                cursor, uid, cont_lot_id, ['polissa_id', 'lot_id']
            )
            polissa_id = cont_lot['polissa_id'][0]

            expect(
                lambda: pol_obj.moure_al_lot(
                    cursor, uid, cont_lot_id, polissa_id, lot_desti_id
                )
            ).to(
                raise_error(except_orm)
            )


class TestGetModcontractualIntervals(testing.OOTestCase):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.polissa_id = polissa_id

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def tearDown(self):
        self.txn.stop()


    def test_get_intervals_change_fields(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        tarifa_20DHA_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
        )[1]

        tarifa_30A_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'tarifa_30A'
        )[1]

        Change = collections.namedtuple(
            'Change', ['field', 'value', 'start_date', 'end_date']
        )

        # Creem la segona modificació contractual fent un canvi de potencia
        mod_changes = [
            Change('potencia', 7.000,
                   '2016-02-02', '2017-03-01'),
            Change('facturacio_potencia', 'recarrec',
                   '2016-03-02',
                   '2017-04-01'),
            Change('tarifa', tarifa_20DHA_id, '2016-04-02', '2017-05-01')
        ]
        polissa_obj = pool.get('giscedata.polissa')
        for change in mod_changes:
            utils_polissa.crear_modcon(
                pool, cursor, uid, self.polissa_id,
                {change.field: change.value}, change.start_date, change.end_date
            )
            changes = polissa_obj.get_modcontractual_intervals(
                cursor, uid, self.polissa_id,
                '{}-01'.format(change.start_date[:-3]),
                '{}-10'.format(change.end_date[:-3])
            )

            self.assertIn(change.start_date, changes)
            self.assertIn(change.field, changes[change.start_date]['changes'])
        utils_polissa.crear_modcon(
            pool, cursor, uid, self.polissa_id,
            {'tarifa': tarifa_30A_id, 'potencia': 15.001},
            '2016-05-02', '2017-09-01'
        )
        polissa = polissa_obj.browse(cursor, uid, self.polissa_id)
        utils_polissa.crear_modcon(
            pool, cursor, uid, self.polissa_id,
            {'potencies_periode':
                [(1, polissa.potencies_periode[0].id, {'potencia': 16.000})]
            }, '2016-09-02', '2017-10-01'
        )
        # polissa = polissa_obj.browse(cursor, uid, self.polissa_id)
        # for p in polissa.potencies_periode:
        #     print p.potencia
        # for m in polissa.modcontractuals_ids:
        #     print m.name, m.data_inici, m.data_final, m.observacions

        changes = polissa_obj.get_modcontractual_intervals(
            cursor, uid, self.polissa_id, '2016-09-01', '2016-10-10'
        )
        self.assertIn('2016-09-02', changes)
        self.assertIn('potencies_periode', changes['2016-09-02']['changes'])


class TestPolissaFacturacio(testing.OOTestCase):
    def test_get_current_bank(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        partner_bank_obj = pool.get('res.partner.bank')
        mandate_obj = pool.get('payment.mandate')
        req_link_obj = pool.get('res.request.link')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            result = polissa.get_current_bank()
            self.assertEqual(polissa.bank.id, result['partner_bank'])
            bank_id = partner_bank_obj.create(cursor, uid, {
                'partner_id': polissa.pagador.id,
                'state': 'iban',
                'acc_country_id': 1,
                'iban': 'ES7712341234161234567890',
                'name': 'Test d'
            })
            polissa.write({'bank': bank_id})
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            result = polissa.get_current_bank()
            self.assertEqual(polissa.bank.id, result['partner_bank'])
            self.assertEqual(False, result['mandate_id'])
            req_link_obj.create(
                cursor, uid, {'name':'polissa', 'object':'giscedata.polissa'}
            )
            mandate_vals = polissa_obj.update_mandate(cursor, uid, polissa_id)
            mandate_vals.update({'creditor_code': '1111111'})
            mandate_id = mandate_obj.create(cursor, uid, mandate_vals)
            result = polissa.get_current_bank()
            self.assertEqual(polissa.bank.id, result['partner_bank'])
            self.assertEqual(mandate_id, result['mandate_id'])


class TestAssignarLotFacturacio(testing.OOTestCase):
    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        self.polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0006'
        )[1]
        # - Activate polissa
        polissa_obj.send_signal(cursor, uid, [self.polissa_id], [
            'validar', 'contracte'
        ])

    def tearDown(self):
        self.txn.stop()

    def test_automatic_lot_when_activate_polissa(self):
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        lot_obj = pool.get('giscedata.facturacio.lot')
        polissa_obj = pool.get('giscedata.polissa')

        polissa_inst = polissa_obj.browse(cursor, uid, self.polissa_id)

        # - Check wheter it has a lot assigned
        self.assertTrue(polissa_inst.lot_facturacio)

        suposed_data_lot = datetime.strptime(polissa_inst.data_alta, '%Y-%m-%d').date() + relativedelta(months=polissa_inst.facturacio)

        # - Search the oldest lot open with bigger inici date than the
        # polissa data alta
        params = [('state', 'in', ['obert', 'esborrany']),
                  ('data_inici', '>', polissa_inst.data_alta)]
        lot_id = lot_obj.search(
            cursor, uid, params, order='data_inici ASC',
            limit=1
        )
        lot_inst = lot_obj.browse(cursor, uid, lot_id)[0]

        # Check the lot found has the same or bigger data inici than
        # the polissa data alta
        self.assertGreaterEqual(
            datetime.strptime(lot_inst.data_inici, '%Y-%m-%d').date(), suposed_data_lot
        )


class TestWizardCrearContracte(testing.OOTestCase):

    def test_modcon_without_reading_shows_warning(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Activem un contracte
            polissa_id = imd_obj.get_object_reference(cursor, uid, "giscedata_polissa", "polissa_0001")[1]
            utils_polissa.activar_polissa(pool, cursor, uid, polissa_id)
            pol = polissa_obj.browse(cursor, uid, polissa_id)
            # Fem una modcon que requereixi lectura
            pol.send_signal(['modcontractual'])
            polissa_obj.write(cursor, uid, polissa_id, {'potencia': 0.1})
            # Cridem l'assistent de fer modcon
            wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa_id, 'check_lectures': True}
            params = {'duracio': 'nou'}
            wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
            # El onchange ens ha de donar l'avis de que falta lectura
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
                ctx
            )
            data_inici_m1 = datetime.today() - timedelta(days=1)
            data_inici_m1 = data_inici_m1.strftime("%Y-%m-%d")
            expected_m = u"No s\'ha trobat cap lectura en data {0} (inici" \
                         u" de la modcon - 1 dia). Es necessiten lectures" \
                         u" per poder partir la facturació correctament." \
                         u"".format(data_inici_m1)
            self.assertEqual(res['warning']['message'], expected_m)
            # Si creem la modcon el mateix avis ha de apareixre a les observacions
            wiz_mod.write(res['value'])
            wiz_mod.action_crear_contracte(ctx)
            pol = polissa_obj.browse(cursor, uid, polissa_id)
            self.assertTrue(expected_m in pol.modcontractual_activa.observacions)

    def test_modcon_with_reading_doesnt_show_warning(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Activem un contracte
            polissa_id = imd_obj.get_object_reference(cursor, uid, "giscedata_polissa", "polissa_0001")[1]
            utils_polissa.activar_polissa(pool, cursor, uid, polissa_id)
            pol = polissa_obj.browse(cursor, uid, polissa_id)
            # Fem una modcon que requereixi lectura
            pol.send_signal(['modcontractual'])
            polissa_obj.write(cursor, uid, polissa_id, {'potencia': 0.1})
            # Cridem l'assistent de fer modcon
            wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa_id, 'check_lectures': True}
            params = {'duracio': 'nou'}
            wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
            # Posem una lectura a inici -1
            data_inici_m1 = datetime.today() - timedelta(days=1)
            data_inici_m1 = data_inici_m1.strftime("%Y-%m-%d")
            lid = imd_obj.get_object_reference(cursor, uid, "giscedata_lectures", "lectura_0001")[1]
            pool.get("giscedata.lectures.lectura").write(cursor, uid, lid, {'name': data_inici_m1})
            # El onchange ens ha de donar l'avis de que falta lectura
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
                ctx
            )
            self.assertFalse(res['warning'])
            # Si creem la modcon el mateix avis ha de apareixre a les observacions
            wiz_mod.write(res['value'])
            wiz_mod.action_crear_contracte(ctx)
            pol = polissa_obj.browse(cursor, uid, polissa_id)
            self.assertFalse("No s\'ha trobat cap lectura" in pol.modcontractual_activa.observacions)

    def test_notificacio_modcon_without_reading_doesnt_show_warning(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Activem un contracte
            polissa_id = imd_obj.get_object_reference(cursor, uid, "giscedata_polissa", "polissa_0001")[1]
            utils_polissa.activar_polissa(pool, cursor, uid, polissa_id)
            pol = polissa_obj.browse(cursor, uid, polissa_id)
            # Fem una modcon que requereixi lectura
            pol.send_signal(['modcontractual'])
            polissa_obj.write(cursor, uid, polissa_id, {'direccio_notificacio': 1})
            # Cridem l'assistent de fer modcon
            wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa_id, 'check_lectures': True}
            params = {'duracio': 'nou'}
            wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
            # El onchange ens ha de donar l'avis de que falta lectura
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
                ctx
            )
            self.assertFalse(res['warning'])
            # Si creem la modcon el mateix avis ha de apareixre a les observacions
            wiz_mod.write(res['value'])
            wiz_mod.action_crear_contracte(ctx)
            pol = polissa_obj.browse(cursor, uid, polissa_id)
            self.assertFalse("No s\'ha trobat cap lectura" in pol.modcontractual_activa.observacions)
