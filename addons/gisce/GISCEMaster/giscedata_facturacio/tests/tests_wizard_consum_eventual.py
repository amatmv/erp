# -*- coding: utf-8 -*-

import unittest

from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from dateutil.relativedelta import relativedelta
from osv.osv import except_osv

class TestWizardConsumEventual(testing.OOTestCase):
    def test_wizard_calculate_eventual_consumption(self):
        pool = self.openerp.pool
        wiz_obj = pool.get(
            'wizard.calcular.consum.eventual'
        )
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Polissa 0005 contains d_inici 2016-01-01, d_baixa 2016-02-01
            # potencia 6000

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0005'
            )[1]

            ctx = {
                'active_id': polissa_id,
                'active_ids': [polissa_id],
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            res = wiz.update_values(ctx)['expected_consumption']

            pol_res = polissa_obj.read(
                cursor, uid, polissa_id, ['expected_consumption']
            )['expected_consumption']
            self.assertEqual(res, pol_res)