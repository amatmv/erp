# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm


class TestWizardAfegirContractesLot(testing.OOTestCase):
    def test_wizard_current_clot_exists(self):
        # An active contract moves to a new lot:
        #   * Old contract lot is unlinked
        #   * New contract_lot
        # An active contract without lot moves to a new lot:
        #   * New contract_lot
        wiz_obj = self.openerp.pool.get('wizard.afegir.polisses.lot')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_desti_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0002'
            )[1]
            polissa_id_1 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_id_2 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            polissa_ids = [polissa_id_1, polissa_id_2]

            polissa_obj.write(
                cursor, uid, polissa_id_2, {'lot_facturacio': False}
            )

            pol_vals = polissa_obj.read(
                cursor, uid, polissa_ids, ['lot_facturacio', 'estat']
            )

            ctx = {
                'active_ids': polissa_ids,
                'active_id': polissa_ids[0],
            }

            wiz_obj.create(
                cursor, uid, {'lot_facturacio': lot_desti_id}, context=ctx
            )

            wiz_obj.action_assignar_lot_facturacio(
                cursor, uid, 1, context=ctx
            )

            pol_vals = polissa_obj.read(cursor, uid, polissa_ids, ['lot_facturacio'])

            for polissa in pol_vals:
                self.assertEqual(polissa['lot_facturacio'][0], lot_desti_id)
