# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm

from datetime import datetime


class TestWizardTancamentAny(testing.OOTestCase):

    def test_create_fiscal_year(self):
        # Create fiscal year
        wiz_obj = self.openerp.pool.get('wizard.tancament.any')
        fyear_obj = self.openerp.pool.get('account.fiscalyear')
        fyearperiod_obj = self.openerp.pool.get('account.period')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            new_year = int(datetime.now().strftime('%Y')) + 1
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(0, len(new_year_id), 'No hay año fiscal')

            ctx = {}
            wiz_id = wiz_obj.create(
                cursor, uid, {'fiscal_year': new_year}, context=ctx
            )
            wiz_obj.create_fiscal_year(cursor, uid, [wiz_id])
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(
                1, len(new_year_id), 'Hay año fiscal {}'.format(new_year)
            )
            periods_ids = fyearperiod_obj.search(
                cursor, uid, [('fiscalyear_id', '=', new_year_id[0])]
            )
            self.assertEqual(
                12, len(periods_ids), 'Hay 12 periodos fiscales {}'.format(new_year)
            )

    def prepare_sequences(self, cursor, uid, current_year):
        seq_obj = self.openerp.pool.get('ir.sequence')
        imd_obj = self.openerp.pool.get('ir.model.data')
        seq_ids = []
        # Energy Sequence
        energy_seq_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'seq_energia_seq'
        )[1]
        seq_ids.append(energy_seq_id)
        energy_vals = {
            'prefix': 'FE{}'.format(current_year[-2:]),
            'number_next': 12345
        }
        seq_obj.write(cursor, uid, [energy_seq_id], energy_vals)
        seq_data = seq_obj.read(
            cursor, uid, energy_seq_id, ['prefix', 'number_next']
        )
        seq_data.pop('id')
        self.assertEqual(energy_vals, seq_data)
        # Energy Sequence
        refund_energy_seq_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'seq_energia_refund_seq'
        )[1]
        seq_ids.append(refund_energy_seq_id)
        refund_energy_vals = {
            'prefix': 'FRE{}'.format(current_year[-2:]),
            'number_next': 12345
        }
        seq_obj.write(
            cursor, uid, [refund_energy_seq_id], refund_energy_vals
        )
        seq_data = seq_obj.read(
            cursor, uid, refund_energy_seq_id, ['prefix', 'number_next']
        )
        seq_data.pop('id')
        self.assertEqual(refund_energy_vals, seq_data)
        return seq_ids

    def test_mantain_same_sequence_restart_numeration(self):
        # Create fiscal year
        wiz_obj = self.openerp.pool.get('wizard.tancament.any')
        fyear_obj = self.openerp.pool.get('account.fiscalyear')
        fyearperiod_obj = self.openerp.pool.get('account.period')
        imd_obj = self.openerp.pool.get('ir.model.data')
        seq_obj = self.openerp.pool.get('ir.sequence')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            current_year = datetime.now().strftime('%Y')
            new_year = int(current_year) + 1
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(0, len(new_year_id), 'No hay año fiscal')

            ctx = {}
            wiz_id = wiz_obj.create(
                cursor, uid, {'fiscal_year': new_year}, context=ctx
            )
            wiz_obj.create_fiscal_year(cursor, uid, [wiz_id])
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(
                1, len(new_year_id), 'Hay año fiscal {}'.format(new_year)
            )
            periods_ids = fyearperiod_obj.search(
                cursor, uid, [('fiscalyear_id', '=', new_year_id[0])]
            )
            self.assertEqual(
                12, len(periods_ids), 'Hay 12 periodos fiscales {}'.format(
                    new_year)
            )
            # Get Sequences
            seq_ids = self.prepare_sequences(cursor, uid, current_year)

            wiz_obj.write(
                cursor, uid, [wiz_id], {'sequences': [(6, 0, seq_ids)]}
            )
            # Start first go_next_sequence initializing wizard
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])

            # Update energy first go_next_sequence initializing wizard
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(
                'FE{}'.format(current_year[-2:]), wiz.sequence.prefix,
                'Sequencia energia'
            )
            seq_energy_id = wiz.sequence.id
            energy_vals = {
                'prefix': 'FE{}'.format(str(new_year)[-2:]),
                'number_next': 1,
                'padding': 10
            }
            wiz_obj.write(
                cursor, uid, [wiz_id], energy_vals
            )
            # Update current sequence and go_next_sequence takes refund sequence
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])
            seq_data = seq_obj.read(
                cursor, uid, seq_energy_id,
                ['prefix', 'number_next', 'padding']
            )
            seq_data.pop('id')
            self.assertEqual(energy_vals, seq_data)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(
                'FRE{}'.format(current_year[-2:]), wiz.sequence.prefix,
                'Sequencia refund energia'
            )
            seq_refund_energy_id = wiz.sequence.id
            refund_energy_vals = {
                'prefix': 'FRE{}'.format(str(new_year)[-2:]),
                'number_next': 1,
                'padding': 10
            }
            wiz_obj.write(
                cursor, uid, [wiz_id], refund_energy_vals
            )
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])
            seq_data = seq_obj.read(
                cursor, uid, seq_refund_energy_id,
                ['prefix', 'number_next', 'padding']
            )
            seq_data.pop('id')
            self.assertEqual(refund_energy_vals, seq_data)

    def test_independent_fiscalyear_reset_numeration_orig_seq_without_fy(self):
        # Create fiscal year
        wiz_obj = self.openerp.pool.get('wizard.tancament.any')
        fyear_obj = self.openerp.pool.get('account.fiscalyear')
        fyearperiod_obj = self.openerp.pool.get('account.period')
        imd_obj = self.openerp.pool.get('ir.model.data')
        seq_obj = self.openerp.pool.get('ir.sequence')
        seq_fy = self.openerp.pool.get('account.sequence.fiscalyear')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            current_year = datetime.now().strftime('%Y')
            new_year = int(current_year) + 1
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(0, len(new_year_id), 'No hay año fiscal')

            ctx = {}
            wiz_id = wiz_obj.create(
                cursor, uid, {'fiscal_year': new_year}, context=ctx
            )
            wiz_obj.create_fiscal_year(cursor, uid, [wiz_id])
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(
                1, len(new_year_id), 'Hay año fiscal {}'.format(new_year)
            )
            periods_ids = fyearperiod_obj.search(
                cursor, uid, [('fiscalyear_id', '=', new_year_id[0])]
            )
            self.assertEqual(
                12, len(periods_ids), 'Hay 12 periodos fiscales {}'.format(
                    new_year)
            )
            # Get current fiscal year
            current_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', 'FY{}'.format(current_year))] # take form addons/official/account/account_demo.xml
            )
            self.assertEqual(
                1, len(current_year_id), 'No hay año fiscal actual {}'.format(
                    current_year)
            )
            #  Get Sequences
            seq_ids = self.prepare_sequences(cursor, uid, current_year)

            wiz_obj.write(
                cursor, uid, [wiz_id], {'sequences': [(6, 0, seq_ids)]}
            )
            # Start first go_next_sequence initializing wizard
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])

            # Update energy first go_next_sequence initializing wizard
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(
                'FE{}'.format(current_year[-2:]), wiz.sequence.prefix,
                'Sequencia energia'
            )
            seq_energy_id = wiz.sequence.id
            energy_vals = {
                'prefix': 'FE{}'.format(str(new_year)[-2:]),
                'number_next': 1,
                'padding': 10,
            }
            wiz_obj.write(
                cursor, uid, [wiz_id], energy_vals
            )
            wiz_obj.write(
                cursor, uid, [wiz_id],
                {
                    'independent': True,
                    'independent_fiscal_year_id': current_year_id
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            old_name = '%s - %s' % (
                wiz.sequence.name, wiz.independent_fiscal_year_id.code
            )
            # Update current seq and then go_next_sequence takes refund sequence
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])
            seq_data = seq_obj.read(
                cursor, uid, seq_energy_id,
                ['prefix', 'number_next', 'padding', 'fiscal_ids']
            )
            # Testing energy sequence
            # Find and get fiscalyear
            fy_seq_ids = seq_data['fiscal_ids']
            fy_seq = seq_fy.read(
                cursor, uid, seq_data['fiscal_ids'], ['fiscalyear_id']
            )
            seq_data['fiscal_ids'] = [fy_seq[0]['fiscalyear_id'][0]]
            seq_data.pop('id')

            energy_vals.update({
                'fiscal_ids': current_year_id
            })
            self.assertEqual(
                energy_vals, seq_data
            )
            # Search old sequence
            old_seq_id = seq_obj.search(
                cursor, uid,
                [('name', '=', old_name)]
            )
            self.assertEqual(
                1, len(old_seq_id), 'No hay seq anterior {}'.format(
                    old_name)
            )
            old_energy_vals = {
                'prefix': 'FE{}'.format(current_year[-2:]),
                'number_next': 12345,
                'fiscal_ids': []
            }
            old_seq_data = seq_obj.read(
                cursor, uid, old_seq_id[0],
                ['prefix', 'number_next', 'fiscal_ids']
            )
            old_seq_data.pop('id')
            self.assertEqual(
                old_energy_vals, old_seq_data
            )
            # Testing refund sequence

            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(
                'FRE{}'.format(current_year[-2:]), wiz.sequence.prefix,
                'Sequencia refund energia'
            )
            seq_refund_energy_id = wiz.sequence.id
            old_name = '%s - %s' % (
                wiz.sequence.name, wiz.independent_fiscal_year_id.code
            )
            refund_energy_vals = {
                'prefix': 'FRE{}'.format(str(new_year)[-2:]),
                'number_next': 1,
                'padding': 10
            }
            wiz_obj.write(
                cursor, uid, [wiz_id], refund_energy_vals
            )

            # Update current seq and then go_next_sequence takes refund sequence
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])
            seq_data = seq_obj.read(
                cursor, uid, seq_refund_energy_id,
                ['prefix', 'number_next', 'padding', 'fiscal_ids']
            )
            fy_seq_ids = seq_data['fiscal_ids']
            fy_seq = seq_fy.read(
                cursor, uid, seq_data['fiscal_ids'], ['fiscalyear_id']
            )
            seq_data['fiscal_ids'] = [fy_seq[0]['fiscalyear_id'][0]]
            seq_data.pop('id')
            refund_energy_vals.update({
                'fiscal_ids': current_year_id
            })
            self.assertEqual(refund_energy_vals, seq_data)
            # Search old sequence
            old_seq_id = seq_obj.search(
                cursor, uid,
                [('name', '=', old_name)]
            )
            self.assertEqual(
                1, len(old_seq_id), 'No hay seq anterior {}'.format(
                    old_name)
            )
            old_energy_vals = {
                'prefix': 'FRE{}'.format(current_year[-2:]),
                'number_next': 12345,
                'fiscal_ids': []
            }
            old_seq_data = seq_obj.read(
                cursor, uid, old_seq_id[0],
                ['prefix', 'number_next', 'fiscal_ids']
            )
            old_seq_data.pop('id')
            # Find and get fiscalyear
            self.assertEqual(
                old_energy_vals, old_seq_data
            )

    def test_independent_fiscalyear_reset_numeration_orig_seq_with_fy(self):
        # Create fiscal year
        wiz_obj = self.openerp.pool.get('wizard.tancament.any')
        fyear_obj = self.openerp.pool.get('account.fiscalyear')
        fyearperiod_obj = self.openerp.pool.get('account.period')
        imd_obj = self.openerp.pool.get('ir.model.data')
        seq_obj = self.openerp.pool.get('ir.sequence')
        seq_fy = self.openerp.pool.get('account.sequence.fiscalyear')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            current_year = datetime.now().strftime('%Y')
            new_year = int(current_year) + 1
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(0, len(new_year_id), 'No hay año fiscal')

            ctx = {}
            wiz_id = wiz_obj.create(
                cursor, uid, {'fiscal_year': new_year}, context=ctx
            )
            wiz_obj.create_fiscal_year(cursor, uid, [wiz_id])
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(
                1, len(new_year_id), 'Hay año fiscal {}'.format(new_year)
            )
            periods_ids = fyearperiod_obj.search(
                cursor, uid, [('fiscalyear_id', '=', new_year_id[0])]
            )
            self.assertEqual(
                12, len(periods_ids), 'Hay 12 periodos fiscales {}'.format(
                    new_year)
            )
            # Get current fiscal year
            current_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', 'FY{}'.format(current_year))] # take form addons/official/account/account_demo.xml
            )
            self.assertEqual(
                1, len(current_year_id), 'No hay año fiscal actual {}'.format(
                    current_year)
            )
            #  Get Sequences
            seq_ids = self.prepare_sequences(cursor, uid, current_year)

            wiz_obj.write(
                cursor, uid, [wiz_id], {'sequences': [(6, 0, seq_ids)]}
            )
            # Start first go_next_sequence initializing wizard
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])

            # Update energy first go_next_sequence initializing wizard
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(
                'FE{}'.format(current_year[-2:]), wiz.sequence.prefix,
                'Sequencia energia'
            )
            seq_energy_id = wiz.sequence.id
            energy_vals = {
                'prefix': 'FE{}'.format(str(new_year)[-2:]),
                'number_next': 1,
                'padding': 10,
            }
            wiz_obj.write(
                cursor, uid, [wiz_id], energy_vals
            )
            wiz_obj.write(
                cursor, uid, [wiz_id],
                {
                    'independent': True,
                    'independent_fiscal_year_id': current_year_id
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            old_name = '%s - %s' % (
                wiz.sequence.name, wiz.independent_fiscal_year_id.code
            )
            # Update current seq and then go_next_sequence takes refund sequence
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])
            seq_data = seq_obj.read(
                cursor, uid, seq_energy_id,
                ['prefix', 'number_next', 'padding', 'fiscal_ids']
            )
            # Testing energy sequence
            # Find and get fiscalyear
            fy_seq_ids = seq_data['fiscal_ids']
            fy_seq = seq_fy.read(
                cursor, uid, seq_data['fiscal_ids'], ['fiscalyear_id']
            )
            seq_data['fiscal_ids'] = [fy_seq[0]['fiscalyear_id'][0]]
            seq_data.pop('id')

            energy_vals.update({
                'fiscal_ids': current_year_id
            })
            self.assertEqual(
                energy_vals, seq_data
            )
            # Search old sequence
            old_seq_id = seq_obj.search(
                cursor, uid,
                [('name', '=', old_name)]
            )
            self.assertEqual(
                1, len(old_seq_id), 'No hay seq anterior {}'.format(
                    old_name)
            )
            old_energy_vals = {
                'prefix': 'FE{}'.format(current_year[-2:]),
                'number_next': 12345,
                'fiscal_ids': []
            }
            old_seq_data = seq_obj.read(
                cursor, uid, old_seq_id[0],
                ['prefix', 'number_next', 'fiscal_ids']
            )
            old_seq_data.pop('id')
            self.assertEqual(
                old_energy_vals, old_seq_data, 'Error primera pasada'
            )


            ### Redo with
            ctx = {}
            current_year = '{}'.format(new_year)
            new_year += 1
            wiz_id = wiz_obj.create(
                cursor, uid, {'fiscal_year': new_year}, context=ctx
            )
            wiz_obj.create_fiscal_year(cursor, uid, [wiz_id])
            new_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(new_year))]
            )
            self.assertEqual(
                1, len(new_year_id), 'Hay año fiscal {}'.format(new_year)
            )
            periods_ids = fyearperiod_obj.search(
                cursor, uid, [('fiscalyear_id', '=', new_year_id[0])]
            )
            self.assertEqual(
                12, len(periods_ids), 'Hay 12 periodos fiscales {}'.format(
                    new_year)
            )
            # Get current fiscal year generate before
            current_original_year_id = current_year_id[0]
            current_year_id = fyear_obj.search(
                cursor, uid,
                [('code', '=', '{}'.format(current_year))]
                # take form addons/official/account/account_demo.xml
            )
            self.assertEqual(
                1, len(current_year_id), 'No hay año fiscal actual {}'.format(
                    current_year)
            )
            #  Get Sequences
            seq_ids = self.prepare_sequences(cursor, uid, current_year)

            wiz_obj.write(
                cursor, uid, [wiz_id], {'sequences': [(6, 0, seq_ids)]}
            )
            # Start first go_next_sequence initializing wizard
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])

            # Update energy first go_next_sequence initializing wizard
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(
                'FE{}'.format(current_year[-2:]), wiz.sequence.prefix,
                'Sequencia energia'
            )
            seq_energy_id = wiz.sequence.id
            energy_vals = {
                'prefix': 'FE{}'.format(str(new_year)[-2:]),
                'number_next': 1,
                'padding': 10,
            }
            wiz_obj.write(
                cursor, uid, [wiz_id], energy_vals
            )
            wiz_obj.write(
                cursor, uid, [wiz_id],
                {
                    'independent': True,
                    'independent_fiscal_year_id': current_year_id
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            old_name = '%s - %s' % (
                wiz.sequence.name, wiz.independent_fiscal_year_id.code
            )
            # Update current seq and then go_next_sequence takes refund sequence
            wiz_obj.go_next_sequence(cursor, uid, [wiz_id])
            seq_data = seq_obj.read(
                cursor, uid, seq_energy_id,
                ['prefix', 'number_next', 'padding', 'fiscal_ids']
            )
            # Testing energy sequence
            # Find and get fiscalyear
            fy_seq_ids = seq_data['fiscal_ids']
            fy_seq = seq_fy.read(
                cursor, uid, seq_data['fiscal_ids'], ['fiscalyear_id']
            )
            seq_data['fiscal_ids'] = sorted([x['fiscalyear_id'][0] for x in fy_seq])
            seq_data.pop('id')

            energy_vals.update({
                'fiscal_ids': sorted([current_year_id[0],current_original_year_id])
            })
            self.assertEqual(
                energy_vals, seq_data
            )
            # Search old sequence
            old_seq_id = seq_obj.search(
                cursor, uid,
                [('name', '=', old_name)]
            )
            self.assertEqual(
                1, len(old_seq_id), 'No hay seq anterior {}'.format(
                    old_name)
            )
            old_energy_vals = {
                'prefix': 'FE{}'.format(current_year[-2:]),
                'number_next': 12345,
                'fiscal_ids': [current_original_year_id]
            }
            old_seq_data = seq_obj.read(
                cursor, uid, old_seq_id[0],
                ['prefix', 'number_next', 'fiscal_ids']
            )
            # Find and get fiscalyear
            fy_seq_ids = old_seq_data['fiscal_ids']
            fy_seq = seq_fy.read(
                cursor, uid, old_seq_data['fiscal_ids'], ['fiscalyear_id']
            )
            old_seq_data['fiscal_ids'] = sorted(
                [x['fiscalyear_id'][0] for x in fy_seq])
            old_seq_data.pop('id')
            self.assertEqual(
                old_energy_vals, old_seq_data
            )