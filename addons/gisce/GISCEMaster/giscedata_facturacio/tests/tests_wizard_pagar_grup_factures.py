# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm
import pandas as pd
from datetime import datetime
import netsvc
import time
import base64
from account_invoice_base.tests.tests_account_invoice import TestPayInvoice
from giscedata_facturacio.wizard.pagar_grup_factures import _pagar_factures
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


class TestWizardPayGroupOfInvoices(testing.OOTestCaseWithCursor):

    def check_get_invoice_to_be_paid(self, cursor, uid, invoice_id=None, invoice_ref=None, context=None):
        """
        Get an invoice that is opened without any partial/full payment made. Contability is check to be correct.
        """
        if context is None:
            context = {}
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        if not invoice_id:
            if not invoice_ref:
                invoice_ref = 'test_invoice_original_unpaid'
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', invoice_ref
            )[1]
        invoice_obj.write(cursor, uid, invoice_id, context.get("extra_vals", {}))
        if context.get('copy'):
            invoice_id = invoice_obj.copy(cursor, uid, invoice_id)
        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cursor)
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        return invoice

    def make_payment(self, cursor, uid, ammount_to_pay, invoice, journal_id=None, period_id=None, context=None):
        imd_obj = self.openerp.pool.get('ir.model.data')
        if not journal_id:
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'bank_journal'
            )[1]

        acc_id = self.openerp.pool.get("account.journal").read(
            cursor, uid, journal_id, ['default_credit_account_id']
        )['default_credit_account_id'][0]

        if not period_id:
            period_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'period_1'
            )[1]

        invoice.pay_and_reconcile(
            ammount_to_pay, acc_id, period_id, journal_id,
            False, period_id, False, context=context
        )
        invoice = invoice.browse()[0]
        return invoice

    def test_correct_pay_group_of_invoices_payment_multiple(self):
        """
        Testeja el pay_and_reconcile_multiple cridat desde pagar factures de CSV.
        El fitxer porta:
            - out_invoice positiva (a cobrar)
            - out_invoice negativa (a pagar)
            - out_refund positiva (a pagar)
            - out_refund negativa (a cobrar)
        El total de les factures es a cobrar.
        """

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        cursor = self.cursor
        uid = self.uid
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_o = self.openerp.pool.get('res.config')
        conf_o.set(
            cursor, uid, 'pay_invoices_unique_account_move', '1'
        )
        # Una factura normal
        invoice2 = self.check_get_invoice_to_be_paid(cursor, uid)
        factura2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        fact_obj.write(cursor, uid, factura2_id, {'invoice_id': invoice2.id})
        # Una factura normal negativa amb un pagament parcial fet
        invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
        invoice = self.make_payment(cursor, uid, -200, invoice)
        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        fact_obj.write(cursor, uid, factura_id, {'invoice_id': invoice.id})
        # Una factura abonadora amb pagament parcial fet
        invoice3 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2', context={'extra_vals': {'type': 'out_refund'}})
        invoice3 = self.make_payment(cursor, uid, 100, invoice3)
        factura3_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0003'
        )[1]
        fact_obj.write(cursor, uid, factura3_id, {'invoice_id': invoice3.id})

        # Una factura abonadora negativa
        invoice4 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2_negative', context={'extra_vals': {'type': 'out_refund'}})
        factura4_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]
        fact_obj.write(cursor, uid, factura4_id, {'invoice_id': invoice4.id})

        self.pool = self.openerp.pool
        data = {
            'form': {
                'name': 'Pagament factures grup',
                'period_id': None,
                'journal_id': 1,
                'date': datetime.today().strftime("%Y-%m-%d")
            },
            'ids': [factura_id, factura2_id, factura3_id, factura4_id]
        }
        _pagar_factures(self, cursor, uid, data, {})
        for inv in [invoice, invoice2, invoice3, invoice4]:
            inv = inv.browse()[0]
            self.assertEqual(inv.residual, 0.0)
            self.assertEqual(inv.state, 'paid')
        mline_o = self.openerp.pool.get("account.move.line")
        self.assertEqual(
            len(mline_o.search(cursor, uid, [('ref', 'like', "Pagament factures grup"), ('debit', '=', 300.0)])),
            1
        )

    def test_correct_pay_group_of_invoices_payment_multiple_total_negative(self):
        """
        Testeja el pay_and_reconcile_multiple cridat desde pagar factures de CSV.
        El fitxer porta:
            - out_invoice positiva (a cobrar)
            - out_invoice negativa (a pagar)
            - out_refund positiva (a pagar)
            - out_refund negativa (a cobrar)
        El total de les factures es a pagar.
        """

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        cursor = self.cursor
        uid = self.uid
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_o = self.openerp.pool.get('res.config')
        conf_o.set(
            cursor, uid, 'pay_invoices_unique_account_move', '1'
        )
        # Una factura normal
        invoice2 = self.check_get_invoice_to_be_paid(cursor, uid)
        factura2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        fact_obj.write(cursor, uid, factura2_id, {'invoice_id': invoice2.id})
        # Una factura normal negativa
        invoice = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_negative')
        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        fact_obj.write(cursor, uid, factura_id, {'invoice_id': invoice.id})
        # Una factura abonadora amb pagament parcial fet
        invoice3 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2', context={'extra_vals': {'type': 'out_refund'}})
        invoice3 = self.make_payment(cursor, uid, -4000, invoice3)
        factura3_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0003'
        )[1]
        fact_obj.write(cursor, uid, factura3_id, {'invoice_id': invoice3.id})

        # Una factura abonadora negativa
        invoice4 = self.check_get_invoice_to_be_paid(cursor, uid, invoice_ref='test_invoice_original_unpaid_2_negative', context={'extra_vals': {'type': 'out_refund'}})
        factura4_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]
        fact_obj.write(cursor, uid, factura4_id, {'invoice_id': invoice4.id})

        self.pool = self.openerp.pool
        data = {
            'form': {
                'name': 'Pagament factures grup',
                'period_id': None,
                'journal_id': 1,
                'date': datetime.today().strftime("%Y-%m-%d")
            },
            'ids': [factura_id, factura2_id, factura3_id, factura4_id]
        }
        _pagar_factures(self, cursor, uid, data, {})
        for inv in [invoice, invoice2, invoice3, invoice4]:
            inv = inv.browse()[0]
            self.assertEqual(inv.residual, 0.0)
            self.assertEqual(inv.state, 'paid')
        mline_o = self.openerp.pool.get("account.move.line")
        self.assertEqual(
            len(mline_o.search(cursor, uid, [('ref', 'like', "Pagament factures grup"), ('credit', '=', 4000.0)])),
            1
        )

    def test_pay_group_of_invoices(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        inv_obj = self.openerp.pool.get('account.invoice')

        cursor = self.cursor
        uid = self.uid

        invoice2 = self.check_get_invoice_to_be_paid(cursor, uid, context={'copy': True})

        imd_obj = self.openerp.pool.get('ir.model.data')
        factura2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        fact_obj.write(cursor, uid, factura2_id, {'invoice_id': invoice2.id})

        self.pool = self.openerp.pool
        data = {
            'form': {
                'name': 'Pagament factures grup',
                'period_id': 1,
                'journal_id': 1,
                'date': datetime.today().strftime("%Y-%m-%d"),
            },
            'ids': [factura2_id]
        }
        _pagar_factures(self, cursor, uid, data, {})
        invoice2 = invoice2.browse()[0]
        self.assertEqual(invoice2.residual, 0.0)
        self.assertEqual(invoice2.state, 'paid')
