# coding=utf-8
from datetime import datetime, timedelta
from destral import testing
from destral.transaction import Transaction


class TestsLotFacturacio(testing.OOTestCase):

    def setUp(self):
        self.cnt_lot_obj = self.openerp.pool.get('giscedata.facturacio.contracte_lot')
        self.lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        self.imd_obj = self.openerp.pool.get('ir.model.data')
        self.comptador_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        self.pol_obj = self.openerp.pool.get('giscedata.polissa')
        self.measure_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        self.maximeter_obj = self.openerp.pool.get('giscedata.lectures.potencia')
        self.lectures_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        self.imd_obj = self.openerp.pool.get('ir.model.data')

    def test_validation_V006_lot(self):

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            _, periode_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )

            _, origen_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )

            _, cnt_lot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0002'
            )

            # We remove other lot_contracts associated with out invoicing lot
            lot_id = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['lot_id']
            )['lot_id'][0]
            cnt_lot_to_remove = self.lot_obj.read(
                cursor, uid, lot_id, ['contracte_lot_ids']
            )['contracte_lot_ids']

            cnt_lot_to_remove.remove(cnt_lot_id)
            self.cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)

            # Let's validate the contract
            _, demo_contract_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )
            self.pol_obj.send_signal(
                cursor, uid, [demo_contract_id], ['validar', 'contracte']
            )

            compt_id = self.comptador_obj.search(
                cursor, uid, [('polissa', '=', demo_contract_id)]
            )[0]

            # We will create two measures with a lower value on the second one,
            # so the validation V006 might pop

            lot_vals = self.lot_obj.read(
                cursor, uid, lot_id, ['data_inici', 'data_final']
            )

            fst_measure = self.measure_obj.create(cursor, uid, {
                'name': lot_vals['data_inici'],
                'periode': periode_id,
                'lectura': 1,
                'ajust': 0,
                'tipus': 'A',
                'comptador': compt_id,
                'observacions': '',
                'origen_id': origen_id,
            })

            last_measure = self.measure_obj.create(cursor, uid, {
                'name': lot_vals['data_final'],
                'periode': periode_id,
                'lectura': 0,
                'ajust': 0,
                'tipus': 'A',
                'comptador': compt_id,
                'observacions': '',
                'origen_id': origen_id,
            })

            # Next step, we want to validate the lot and the V006 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V006', lot_status)

            # But if we add an adjust to the second measure, the V006 will be
            # fixed
            self.measure_obj.write(cursor, uid, [last_measure], {'ajust': 1})

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertNotIn('V006', lot_status)

    def test_validation_V008_lot(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            _, periode_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )

            _, origen_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )

            _, cnt_lot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0002'
            )

            # We remove other lot_contracts associated with out invoicing lot
            lot_id = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['lot_id']
            )['lot_id'][0]
            cnt_lot_to_remove = self.lot_obj.read(
                cursor, uid, lot_id, ['contracte_lot_ids']
            )['contracte_lot_ids']

            cnt_lot_to_remove.remove(cnt_lot_id)
            self.cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)

            # Let's validate the contract
            _, demo_contract_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )
            self.pol_obj.send_signal(
                cursor, uid, [demo_contract_id], ['validar', 'contracte']
            )

            # Next step, we want to validate the lot and the V008 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V008', lot_status)

    def crear_modcon(self, txn, vals, ini, fi, tariff_id=None):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, vals)

        if tariff_id:
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)
        return polissa_id

    def prepare_for_test_validation_V021(self, cursor, uid):

        _, periode_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
        )

        _, origen_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )

        _, cnt_lot_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'cont_lot_0001'
        )

        # We remove other lot_contracts associated with out invoicing lot
        lot_id = self.cnt_lot_obj.read(
            cursor, uid, cnt_lot_id, ['lot_id']
        )['lot_id'][0]
        cnt_lot_to_remove = self.lot_obj.read(
            cursor, uid, lot_id, ['contracte_lot_ids']
        )['contracte_lot_ids']

        cnt_lot_to_remove.remove(cnt_lot_id)
        self.cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)

        # Let's validate the contract
        _, demo_contract_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )
        self.pol_obj.send_signal(
            cursor, uid, [demo_contract_id], ['validar', 'contracte']
        )

        compt_id = self.comptador_obj.search(
            cursor, uid, [('polissa', '=', demo_contract_id)]
        )[0]

        # We will create two maximeter measures for the same lot and meter,
        # so the validation V021 might pop

        lot_vals = self.lot_obj.read(
            cursor, uid, lot_id, ['data_inici', 'data_final']
        )

        data_inici = datetime.strptime(lot_vals['data_inici'], '%Y-%m-%d')
        data_inici += timedelta(days=1)

        first_maximeter = {
            'name': '2016-01-10',
            'periode': periode_id,
            'lectura': 1,
            'ajust': 0,
            'tipus': 'A',
            'comptador': compt_id,
            'observacions': '',
            'origen_id': origen_id,
        }

        last_maximeter = {
            'name': '2016-01-29',
            'periode': periode_id,
            'lectura': 1,
            'ajust': 0,
            'tipus': 'A',
            'comptador': compt_id,
            'observacions': '',
            'origen_id': origen_id,
        }

        first_lectura = {
            'name': '2016-01-04',  # data_inici.strftime('%Y-%m-%d'),
            'periode': periode_id,
            'lectura': 1,
            'ajust': 0,
            'tipus': 'A',
            'comptador': compt_id,
            'observacions': '',
            'origen_id': origen_id,
        }

        second_lectura = {
            'name': '2016-01-29',  # data_inici.strftime('%Y-%m-%d'),
            'periode': periode_id,
            'lectura': 1,
            'ajust': 0,
            'tipus': 'A',
            'comptador': compt_id,
            'observacions': '',
            'origen_id': origen_id,
        }

        self.maximeter_obj.create(cursor, uid, first_maximeter)
        self.maximeter_obj.create(cursor, uid, last_maximeter)
        self.lectures_obj.create(cursor, uid, first_lectura)
        self.lectures_obj.create(cursor, uid, second_lectura)

        res = {
            'cnt_lot_id': cnt_lot_id,
            'periode_id': periode_id,
            'compt_id': compt_id,
            'origen_id': origen_id,
        }

        return res

    def test_validation_V021_lot(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            vals = self.prepare_for_test_validation_V021(cursor, uid)

            # Next step, we want to validate the lot and the V021 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [vals['cnt_lot_id']])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, vals['cnt_lot_id'], ['status']
            )['status']
            self.assertIn('V021', lot_status)

    def test_validation_V021_lot_with_modcon(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            vals = self.prepare_for_test_validation_V021(cursor, uid)

            vals_modcon = {'potencia': 10}
            self.crear_modcon(txn, vals_modcon, '2016-01-15', '2016-12-31')

            # Next step, we want to validate the lot and the V021 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [vals['cnt_lot_id']])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, vals['cnt_lot_id'], ['status']
            )['status']
            print lot_status
            self.assertNotIn('V021', lot_status)

    def test_validation_V021_lot_with_modcon_more_than_one_maximeter(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            vals = self.prepare_for_test_validation_V021(cursor, uid)

            third_maximeter = {
                'name': '2016-01-09',  # data_inici.strftime('%Y-%m-%d'),
                'periode': vals['periode_id'],
                'lectura': 1,
                'comptador': vals['compt_id'],
                'observacions': '',
                'origen_id': vals['origen_id'],
            }

            self.maximeter_obj.create(cursor, uid, third_maximeter)

            vals_modcon = {'potencia': 10}
            self.crear_modcon(txn, vals_modcon, '2016-01-15', '2016-12-31')

            # Next step, we want to validate the lot and the V021 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [vals['cnt_lot_id']])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, vals['cnt_lot_id'], ['status']
            )['status']
            print lot_status
            self.assertIn('V021', lot_status)

    def test_validation_V021_lot_with_modcon_more_than_one_maximeter_diff_period(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            vals = self.prepare_for_test_validation_V021(cursor, uid)

            _, periode_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHA_new'
            )

            third_maximeter = {
                'name': '2016-01-09',  # data_inici.strftime('%Y-%m-%d'),
                'periode': periode_id,
                'lectura': 1,
                'comptador': vals['compt_id'],
                'observacions': '',
                'origen_id': vals['origen_id'],
            }

            self.maximeter_obj.create(cursor, uid, third_maximeter)

            vals_modcon = {'potencia': 10}
            self.crear_modcon(txn, vals_modcon, '2016-01-15', '2016-12-31')

            # Next step, we want to validate the lot and the V021 should pop
            self.cnt_lot_obj.validate_individual(cursor, uid, [vals['cnt_lot_id']])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, vals['cnt_lot_id'], ['status']
            )['status']
            print lot_status
            self.assertNotIn('V021', lot_status)

    def test_validation_V020_lot(self):

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            _, periode_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )

            _, origen_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )

            _, cnt_lot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'cont_lot_0002'
            )

            # We remove other lot_contracts associated with out invoicing lot
            lot_id = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['lot_id']
            )['lot_id'][0]
            cnt_lot_to_remove = self.lot_obj.read(
                cursor, uid, lot_id, ['contracte_lot_ids']
            )['contracte_lot_ids']

            cnt_lot_to_remove.remove(cnt_lot_id)
            self.cnt_lot_obj.unlink(cursor, uid, cnt_lot_to_remove)

            # Let's validate the contract
            _, demo_contract_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )

            self.pol_obj.send_signal(
                cursor, uid, [demo_contract_id],
                ['validar', 'contracte']
            )

            self.pol_obj.write(
                cursor, uid, demo_contract_id,
                {'data_baixa': '2019-01-31', 'renovacio_auto': False}
            )

            compt_id = self.comptador_obj.search(
                cursor, uid, [('polissa', '=', demo_contract_id)]
            )[0]

            self.comptador_obj.write(
                cursor, uid, compt_id,
                {'data_baixa': '2019-01-31', 'active': False}
            )

            self.pol_obj.send_signal(
                cursor, uid, [demo_contract_id],
                ['baixa']
            )

            # Next step, we want to validate the lot and the V020 should pop
            pol_data = {'data_ultima_lectura': '2019-02-01'}
            self.pol_obj.write(cursor, uid, demo_contract_id, pol_data)

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V000', lot_status)

            # Next step, we want to validate the lot and the V020 should pop
            pol_data = {'data_ultima_lectura': '2019-01-31'}
            self.pol_obj.write(cursor, uid, demo_contract_id, pol_data)

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertIn('V000', lot_status)
            self.assertIn(
                u"[V000] Contracte de baixa el 2019-01-31 ja facturat "
                u"fins 2019-01-31",
                lot_status
            )

            # But if we add an adjust to the second measure, the V006 will be
            # fixed
            pol_data = {'data_ultima_lectura': '2019-01-01'}
            self.pol_obj.write(cursor, uid, demo_contract_id, pol_data)

            self.cnt_lot_obj.validate_individual(cursor, uid, [cnt_lot_id])
            lot_status = self.cnt_lot_obj.read(
                cursor, uid, cnt_lot_id, ['status']
            )['status']
            self.assertNotIn('V000', lot_status)


class TestsMovePolissesLot(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def test_next_lot_weekly_lots(self):
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        cfg_obj = self.openerp.pool.get('res.config')

        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/01 - 8',
            'data_inici': '2017-01-01',
            'data_final': '2017-01-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/01 - 15',
            'data_inici': '2017-01-08',
            'data_final': '2017-01-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/01 - 22',
            'data_inici': '2017-01-15',
            'data_final': '2017-01-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 1',
            'data_inici': '2017-01-22',
            'data_final': '2017-01-31',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 8',
            'data_inici': '2017-02-01',
            'data_final': '2017-02-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 15',
            'data_inici': '2017-02-08',
            'data_final': '2017-02-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/02 - 22',
            'data_inici': '2017-02-15',
            'data_final': '2017-02-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 1',
            'data_inici': '2017-02-22',
            'data_final': '2017-02-28',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 8',
            'data_inici': '2017-03-01',
            'data_final': '2017-03-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 15',
            'data_inici': '2017-03-08',
            'data_final': '2017-03-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/03 - 22',
            'data_inici': '2017-03-15',
            'data_final': '2017-03-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 1',
            'data_inici': '2017-03-22',
            'data_final': '2017-03-31',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 8',
            'data_inici': '2017-04-01',
            'data_final': '2017-04-07',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 15',
            'data_inici': '2017-04-08',
            'data_final': '2017-04-14',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/04 - 22',
            'data_inici': '2017-04-15',
            'data_final': '2017-04-21',
        })
        lot_obj.create(self.cursor, self.uid, {
            'name': '2017/05 - 1',
            'data_inici': '2017-04-22',
            'data_final': '2017-04-30',
        })

        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-07', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/02 - 8',
            'data_inici': '2017-02-01',
            'data_final': '2017-02-07',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-14', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/02 - 15',
            'data_inici': '2017-02-08',
            'data_final': '2017-02-14',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-21', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/02 - 22',
            'data_inici': '2017-02-15',
            'data_final': '2017-02-21',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-31', 1)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 1',
            'data_inici': '2017-02-22',
            'data_final': '2017-02-28',
        }, lot)

        # Test with bimestral
        cfg_obj.set(
            self.cursor, self.uid, 'fact_bimestrals_sempre_lot_parell', '0'
        )
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-07', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 8',
            'data_inici': '2017-03-01',
            'data_final': '2017-03-07',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-14', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 15',
            'data_inici': '2017-03-08',
            'data_final': '2017-03-14',
        }, lot)
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-21', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 22',
            'data_inici': '2017-03-15',
            'data_final': '2017-03-21',
        }, lot)
        cfg_obj.set(
            self.cursor, self.uid, 'fact_bimestrals_sempre_lot_parell', '1'
        )
        lot_id = lot_obj.get_next(self.cursor, self.uid, '2017-01-31', 2)
        lot = lot_obj.read(self.cursor, self.uid, lot_id)
        self.assertDictContainsSubset({
            'name': '2017/03 - 1',
            'data_inici': '2017-02-22',
            'data_final': '2017-02-28',
        }, lot)


class TestsInvoicesNextLot(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)
        # self.cursor = self.txn.cursor
        # self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def enivironment_lot(self, cursor, uid, context=None):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        cnt_lot_obj = self.openerp.pool.get('giscedata.facturacio.contracte_lot')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        self.polissa_id = polissa_id

        pol_obj.write(cursor, uid, polissa_id, {'state': 'baixa', 'data_baixa': '2019-01-01'})

        pol_reads = pol_obj.read(cursor, uid, polissa_id, ['lot_facturacio', 'state', 'data_baixa'])

        self.assertEqual(pol_reads['state'], 'baixa')

        self.assertEqual(pol_reads['data_baixa'], '2019-01-01')

        self.data_baixa = pol_reads['data_baixa']

        self.assertTrue(pol_reads['lot_facturacio'])

        self.polissa_lot_id = pol_reads['lot_facturacio'][0]

    def test_remove_lot_on_invoice_open_if_invoice_not_in_contract_range(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        cfg_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        cursor = self.txn.cursor
        uid = self.txn.user

        self.enivironment_lot(cursor, uid)

        polissa_id = self.polissa_id

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]

        fact_obj.write(cursor, uid, fact_id, {'data_final': '2019-01-01', 'lot_facturacio': self.polissa_lot_id})

        f_data_f = fact_obj.read(
            cursor, uid, fact_id, ['data_final']
        )['data_final']

        self.assertEqual(f_data_f, '2019-01-01')

        fact_obj.invoice_open(cursor, uid, [fact_id], context=None)

        pol_reads = pol_obj.read(
            cursor, uid, polissa_id, ['lot_facturacio']
        )
        self.assertFalse(pol_reads['lot_facturacio'])

    def test_no_remove_lot_on_invoice_open_if_invoice_in_contract_range(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        cfg_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        cursor = self.txn.cursor
        uid = self.txn.user

        self.enivironment_lot(cursor, uid)

        polissa_id = self.polissa_id

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]

        fact_obj.write(
            cursor, uid, fact_id, {'lot_facturacio': self.polissa_lot_id}
        )

        f_data_f = fact_obj.read(
            cursor, uid, fact_id, ['data_final']
        )['data_final']

        self.assertTrue(f_data_f < self.data_baixa)

        fact_obj.invoice_open(cursor, uid, [fact_id], context=None)

        pol_reads = pol_obj.read(
            cursor, uid, polissa_id, ['lot_facturacio']
        )
        self.assertTrue(pol_reads['lot_facturacio'])
