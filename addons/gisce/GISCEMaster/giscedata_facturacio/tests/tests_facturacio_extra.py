# coding=utf-8
from datetime import datetime
from destral import testing
from destral.transaction import Transaction


class TestsExtraLines(testing.OOTestCase):
    def test_search_extra_lines_with_correct_range(self):
        ''' Test get_extra_lines_from_contract_and_date function
            gived an a date of end of period function should return
            all not invoiced extralines until end period date
        '''
        imd_obj = self.openerp.pool.get('ir.model.data')
        extra_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            extra_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'facturacio_extraline_01'
            )[1]

            # Set period for extraline
            extra_obj.write(cursor, uid, extra_id, {
                'date_from': '2018-12-10',
                'date_to':  '2018-12-20'
            })

            # Set end period date for function, should give us our extra line
            data_f_periode = datetime.strptime('2018-12-31', '%Y-%m-%d')

            extra_ids = extra_obj.get_extra_lines_from_contract_and_date(
                cursor, uid, polissa_id, data_f_periode
            )

            self.assertIn(extra_id, extra_ids)

            # Set end period date for function, should give us our extra line
            data_f_periode = datetime.strptime('2018-12-15', '%Y-%m-%d')

            extra_ids = extra_obj.get_extra_lines_from_contract_and_date(
                cursor, uid, polissa_id, data_f_periode
            )

            self.assertIn(extra_id, extra_ids)

            # Set end period date for function, should give us our extra line
            data_f_periode = datetime.strptime('2018-12-31', '%Y-%m-%d')

            extra_ids = extra_obj.get_extra_lines_from_contract_and_date(
                cursor, uid, polissa_id, data_f_periode
            )

            self.assertIn(extra_id, extra_ids)

            # Set end period date for function, should give us our extra line
            data_f_periode = datetime.strptime('2018-12-18', '%Y-%m-%d')

            extra_ids = extra_obj.get_extra_lines_from_contract_and_date(
                cursor, uid, polissa_id, data_f_periode
            )

            self.assertIn(extra_id, extra_ids)

            # Set end period date for function, should give us our extra line
            data_f_periode = datetime.strptime('2018-12-31', '%Y-%m-%d')

            extra_ids = extra_obj.get_extra_lines_from_contract_and_date(
                cursor, uid, polissa_id, data_f_periode
            )

            self.assertIn(extra_id, extra_ids)
