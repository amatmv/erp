# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from osv import osv
from datetime import datetime


class TestWizardReRectifiedInvoices(testing.OOTestCase):

    # A rectified invoice shouldn't be rectified again
    """
    This testing method try to re-rectify an invoice,
        if program allows re-rectification test fails
        otherwise test passed
    """
    def create_bra_invoice(self, cursor, uid):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        bank_obj = self.openerp.pool.get("res.partner.bank")

        factura_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        factura_obj.invoice_open(cursor, uid, [factura_id])
        lot_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'lot_0002'
        )[1]
        # Creem un res.partner.bank per posarli a la factura original
        # Al anular hauria de agafar el nou compte i no el de la original
        factura = factura_obj.browse(cursor, uid, factura_id)
        bank1_id = bank_obj.create(cursor, uid, {
            'name': "bank 1",
            'partner_id': factura.partner_id.id,
            'state': 'bank'
        })
        # Posem tipo pago 'RECIBO_CSB' perque així al anular intentara
        # copiar el partner_bank de la anulada
        payment_obj = self.openerp.pool.get("payment.type")
        payment_id = payment_obj.create(cursor, uid, {
            'name': "payment1",
            'code': "RECIBO_CSB"
        })
        factura_obj.write(cursor, uid, factura_id, {
            'lot_facturacio': lot_id,
            'partner_bank': bank1_id,
            'payment_type': payment_id
        })
        factura = factura_obj.browse(cursor, uid, factura_id)
        factura.lectures_energia_ids[0].write({
            'data_anterior': factura.data_inici,
            'data_actual': factura.data_final
        })

        # Creem un altre res.partner.bank diferent i la podem al contracte,
        # la rectificadora i l'anuladora haurien de portar el nou compte
        bank2_id = bank_obj.create(cursor, uid, {
            'name': "bank 2",
            'partner_id': factura.partner_id.id,
            'state': 'bank'
        })
        factura.polissa_id.write({
            'lot_facturacio': lot_id,
            'data_alta': factura.data_inici,
            'facturacio_potencia': 'icp',
            'facturacio': 2,
            'bank': bank2_id,
            'tipo_pago': payment_id
        })
        factura.polissa_id.comptadors[0].write({'lloguer': False})
        factura.polissa_id.comptadors[0].lectures[0].write({
            'name': factura.data_inici,
            'lectura': 0
        })
        factura.polissa_id.comptadors[0].lectures[1].write({
            'name': factura.data_final,
            'lectura': 1
        })
        factura.polissa_id.send_signal(['validar', 'contracte'])
        factura.lot_facturacio.write({
            'data_inici': factura.data_inici,
            'data_final': factura.data_final
        })

        # El contracte ja te unes lectures diferents a les de la factura
        # per tant la rectificadora tindrà valors diferents.
        # Rectificadora amb anuladora
        res_ids = factura.rectificar_substitucio(tipus='RA', context={})

        tipo_rect = factura_obj.read(
            cursor, uid, res_ids, ['tipo_rectificadora']
        )[1]['tipo_rectificadora']

        self.assertEqual(tipo_rect, u'BRA')

        date_move = datetime.now().strftime('%Y-%m-%d')
        period_obj = self.openerp.pool.get('account.period')
        period_id = period_obj.find(cursor, uid, date_move)[0]

        factura_obj.write(cursor, uid, res_ids, {'period_id': period_id})

        factura_obj.invoice_open(cursor, uid, res_ids)

        return res_ids

    def test_prevent_rerectified_invoices(self):

        # Install tarifas 2016 module
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )

        # Get policy's table from DB
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        # Get invoices table from DB.
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        # Link xml files with modules.
        imd_obj = self.openerp.pool.get('ir.model.data')

        # Get wizzard object.
        wiz_obj = self.openerp.pool.get('wizard.ranas')

        # Context reference to dict, now is void.
        context = {}

        # All at next of this line don't affect to other tests.
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Activate policy
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol_obj.send_signal(cursor, uid, [polissa_id], [
                'validar', 'contracte'
            ])

            base_invoice_refs = ['factura_0015', 'factura_0016', 'factura_0017']

            # Store id's of original invoices
            list_fact_ids = map(lambda x: (imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', x)[1]),
                                    base_invoice_refs)

            vals = {}

            context = {
                'active_ids': None,
                'active_id': None,
                'llista_preu': None,
                'model': 'giscedata.facturacio.factura'
            }

            # This internal function set context with invoice id and price list
            def set_context(fact_id):
                context['active_ids'] = [fact_id]
                context['active_id'] = fact_id
                context['llista_preu'] = factura_obj.read(cursor, uid, fact_id,
                                                          ['llista_preu']
                                                          )['llista_preu'][0]

            # This internal function modify context with corresponding fields
            # of invoice, execute create with new context and return
            # result of that
            def wiz_create():
                wiz_id = wiz_obj.create(cursor, uid, vals, context=context)
                return wiz_id

            for fac in list_fact_ids:
                set_context(fac)
                self.assertRaises(osv.except_osv, wiz_create)

    def test_dont_allow_abono_y_rectificacion_BRA(self):
        # Install tarifas 2016 module
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20160101'
        )

        # Get policy's table from DB
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        # Get invoices table from DB.
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        # Link xml files with modules.
        imd_obj = self.openerp.pool.get('ir.model.data')

        # Get wizzard object.
        wiz_obj = self.openerp.pool.get('wizard.ranas')

        # Context reference to dict, now is void.
        context = {}

        # All at next of this line don't affect to other tests.
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_ids = self.create_bra_invoice(cursor, uid)
            bra_id = res_ids[0]
            vals = {}

            context = {
                'active_ids': [bra_id],
                'active_id': bra_id,
                'llista_preu': factura_obj.read(
                    cursor, uid, bra_id, ['llista_preu']
                )['llista_preu'][0],
                'model': 'giscedata.facturacio.factura'
            }

            wiz_id = wiz_obj.create(cursor, uid, vals, context=context)
            wizard = wiz_obj.browse(cursor, uid, wiz_id, context=context)

            # Para comprobar que no abona y rectifica en el asistente
            def wiz_abonar_rectificar_gen_bra():
                return wizard.action_rectificar_substitucio(context=context)

            def wiz_abonar_i_rectificar():
                return wizard.action_rectificar(context=context)

            def wiz_anular():
                return wizard.action_anullar(context=context)

            self.assertRaises(osv.except_osv, wiz_abonar_rectificar_gen_bra)
            self.assertRaises(osv.except_osv, wiz_abonar_i_rectificar)
            self.assertRaises(osv.except_osv, wiz_anular)

            def abonar_desde_funcion_facturacion_amb_bra():
                return factura_obj.rectificar_substitucio(
                    cursor, uid, [bra_id], context=None
                )

            def abonar_i_rectificar_desde_funcion_facturacion():
                return factura_obj.rectificar_substitucio(
                    cursor, uid, [bra_id], context=None
                )

            def anular_desde_funcion_facturacion():
                return factura_obj.rectificar_substitucio(
                    cursor, uid, [bra_id], context=None
                )

            self.assertRaises(
                osv.except_osv, abonar_desde_funcion_facturacion_amb_bra
            )
            self.assertRaises(
                osv.except_osv, abonar_i_rectificar_desde_funcion_facturacion
            )

            self.assertRaises(
                osv.except_osv, anular_desde_funcion_facturacion
            )

            err_wiz = 'Random value x1'
            err_wiz_no_gen_bra = 'Random value x2'
            err_wiz_anular = 'Random value x3'
            err_fact = 'Random value y1'
            err_fact_no_gen_bra = 'Random value y2'
            err_fact_anular = 'Random value y3'

            try:
                wiz_abonar_rectificar_gen_bra()
            except Exception as e:
                err_wiz = e.message

            try:
                wiz_abonar_i_rectificar()
            except Exception as e:
                err_wiz_no_gen_bra = e.message

            try:
                wiz_anular()
            except Exception as e:
                err_wiz_anular = e.message

            try:
                abonar_desde_funcion_facturacion_amb_bra()
            except Exception as e:
                err_fact = e.message

            try:
                abonar_i_rectificar_desde_funcion_facturacion()
            except Exception as e:
                err_fact_no_gen_bra = e.message

            try:
                anular_desde_funcion_facturacion()
            except Exception as e:
                err_fact_anular = e.message

            self.assertEqual(err_wiz, err_fact)
            self.assertEqual(err_fact_no_gen_bra, err_wiz_no_gen_bra)
            self.assertEqual(err_wiz, err_wiz_no_gen_bra)
            self.assertEqual(err_wiz_anular, err_fact_anular)
            self.assertEqual(err_wiz_anular, err_wiz)
