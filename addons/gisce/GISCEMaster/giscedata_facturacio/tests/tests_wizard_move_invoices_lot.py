# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from osv.orm import except_orm


class TestWizardMoveInvoicesLot(testing.OOTestCase):
    def llegir_lots(self, txn, nom_lot_01, nom_lot_02):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')

        lot_01_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', nom_lot_01
        )[1]
        lot_02_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', nom_lot_02
        )[1]

        lot_01 = lot_obj.read(
            cursor, uid, lot_01_id, [
                'n_contracte_lots',
                'contracte_lot_ids'
            ]
        )
        lot_02 = lot_obj.read(
            cursor, uid, lot_02_id, [
                'n_contracte_lots',
                'contracte_lot_ids'
            ]
        )

        return lot_01, lot_02

    def test_wizard_rejects_no_context_or_no_active_ids_in_context(self):
        # Neither missing a context or having a context without active_ids
        # should work since it would mean we wouldn't be able to know what lot's
        # invoices are we supposed to move
        wiz_obj = self.openerp.pool.get(
            'wizard.move.invoices.lot'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            vals = {
                'lot_desti': None,
                'moure_oberts': True,
                'moure_esborrany': True,
                'moure_a_punt': True,
            }
            wiz_id = wiz_obj.create(cursor, uid, vals, context=None)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert not wiz.moure(context=None)

            useless_context = {'useless': 'context'}
            wiz_id_2 = wiz_obj.create(
                cursor, uid, vals, context=useless_context
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id_2)
            assert not wiz.moure(context=useless_context)

    def test_move_empty_lot_does_nothing(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.move.invoices.lot'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            lot_01_pre, lot_02_pre = self.llegir_lots(
                txn, 'lot_0001', 'lot_0002'
            )

            # Lot_02 must be empty at the start or the test won't make any
            # sense at all
            assert lot_02_pre['n_contracte_lots'] == 0
            assert lot_02_pre['contracte_lot_ids'] == []

            vals = {
                'lot_desti': lot_01_pre['id'],
                'moure_oberts': True,
                'moure_esborrany': True,
                'moure_a_punt': True,
            }
            ctx = {
                'active_ids': [lot_02_pre['id']]
            }
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.moure(context=ctx)

            lot_01_post, lot_02_post = self.llegir_lots(
                txn, 'lot_0001', 'lot_0002'
            )

            # Both lots should have the same contracts at the start and end
            # (lot_01 whichever he happened to have and lot_02 nothing)
            assert lot_01_pre == lot_01_post
            assert lot_02_pre == lot_02_post
