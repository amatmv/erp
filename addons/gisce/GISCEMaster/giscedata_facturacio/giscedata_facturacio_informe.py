# -*- coding: utf-8 -*-

"""
Models d'informes de facturació

"""

from osv import fields, osv

class GiscedataFacturacioInformeComptable(osv.osv):
    """Model d'informe comptable
    """
    _name = 'giscedata.facturacio.informe_comptable'
    _description = __doc__

    _columns = {
        'period_id': fields.many2one('account.period', 'Periode',
                                     required=True),
        'name': fields.char('Nom', size=10, required=True),
        'linies': fields.one2many('giscedata.facturacio.informe_comptable.linia',
                                  'informe_id', 'Línies')
    }

GiscedataFacturacioInformeComptable()

class GiscedataFacturacioInformeComptableLinia(osv.osv):
    """Model de línia d'informe comptable
    """
    _name = 'giscedata.facturacio.informe_comptable.linia'
    _descripcion = __doc__

    _columns = {
        'tipus_rect': fields.char('T. Rectificadora', size=16, required=True),
        'name': fields.char('N. Factura', size=32, required=True),
        'tarifa': fields.char('Tarifa', size=10, required=True),
        'pricelist': fields.char('Llista de preus', size=64, required=True),
        'partner': fields.char('Partner', size=256, required=True),
        'ene_p1': fields.float('Energia P1', digits=(9,3)),
        'ene_p2': fields.float('Energia P2', digits=(9,3)),
        'ene_p3': fields.float('Energia P3', digits=(9,3)),
        'ene_p4': fields.float('Energia P4', digits=(9,3)),
        'ene_p5': fields.float('Energia P5', digits=(9,3)),
        'ene_p6': fields.float('Energia P6', digits=(9,3)),
        'pot_p1': fields.float('Potencia P1', digits=(9,3)),
        'pot_p2': fields.float('Potencia P2', digits=(9,3)),
        'pot_p3': fields.float('Potencia P3', digits=(9,3)),
        'pot_p4': fields.float('Potencia P4', digits=(9,3)),
        'pot_p5': fields.float('Potencia P5', digits=(9,3)),
        'pot_p6': fields.float('Potencia P6', digits=(9,3)),
        'lloguers': fields.float('Total Lloguers', digits=(13,2), 
                                 required=True),
        'base': fields.float('Base Imposable', required=True, digits=(13,2)),
        'iese': fields.float('IESE', digits=(13,2)),
        'iva': fields.float('IVA', required=True, digits=(13,2)),
        'total': fields.float('Total factura', required=True, digits=(13,2)),
        'informe_id': fields.many2one('giscedata.facturacio.informe_comptable',
                                      'Informe', required=True,
                                      ondelete='cascade'),
    }

    _defaults = {
        'ene_p1': lambda *a: 0.0,
        'ene_p2': lambda *a: 0.0,
        'ene_p3': lambda *a: 0.0,
        'ene_p4': lambda *a: 0.0,
        'ene_p5': lambda *a: 0.0,
        'ene_p6': lambda *a: 0.0,
        'pot_p1': lambda *a: 0.0,
        'pot_p2': lambda *a: 0.0,
        'pot_p3': lambda *a: 0.0,
        'pot_p4': lambda *a: 0.0,
        'pot_p5': lambda *a: 0.0,
        'pot_p6': lambda *a: 0.0,
        'lloguers': lambda *a: 0.0,
        'base': lambda *a: 0.0,
        'iva': lambda *a: 0.0,
        'total': lambda *a: 0.0,

    }
GiscedataFacturacioInformeComptableLinia()


class GiscedataFacturacioInformeMesures(osv.osv):
    """Model d'informe de les mesures facturades
    """
    _name = 'giscedata.facturacio.informe_mesures'
    _description = __doc__

    _columbns = {
        # name és la data en format MM/YYYY
        'name': fields.char('Informe', size=7, required=True),
        'linies': fields.one2many('giscedata.facturacio.informe_mesures.linia',
                                  'informe_id', 'Línies'),
    }
GiscedataFacturacioInformeMesures()


class GiscedataFacturacioInformeMesuresLinia(osv.osv):
    """Model de línia d'informe de les mesures facturades
    """
    _name = 'giscedata.facturacio.informe_mesures.linia'
    _description = __doc__

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'tarifa': fields.char('Tarifa', size=10, required=True),
        'data_inici': fields.date('Data inici', required=True),
        'data_final': fields.date('Data final', required=True),
        'ene_p1': fields.integer('Energia P1'),
        'ene_p2': fields.integer('Energia P2'),
        'ene_p3': fields.integer('Energia P3'),
        'ene_p4': fields.integer('Energia P4'),
        'ene_p5': fields.integer('Energia P5'),
        'ene_p6': fields.integer('Energia P6'),
        'react_p1': fields.integer('Reactiva P1'),
        'react_p2': fields.integer('Reactiva P2'),
        'react_p3': fields.integer('Reactiva P3'),
        'react_p4': fields.integer('Reactiva P4'),
        'react_p5': fields.integer('Reactiva P5'),
        'react_p6': fields.integer('Reactiva P6'),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'pot_contract': fields.float('Potència contractada', required=False,
                                     digits=(4,2)),
        'num_client': fields.char('Client', size=10, required=True),
        'periode_resum': fields.char('Periode', size=7, required=True),
        'informe_id': fields.many2one('giscedata.facturacio.informe_mesures',
                                      'Informe', required=True,
                                      ondelete='cascade'),
    }
GiscedataFacturacioInformeMesures()

