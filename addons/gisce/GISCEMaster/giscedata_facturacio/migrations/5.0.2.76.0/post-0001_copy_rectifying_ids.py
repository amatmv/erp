# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Copying rectifying type and id')

    query = '''
        UPDATE account_invoice AS i
        SET rectificative_type = f.tipo_rectificadora,
            rectifying_id = rectif.invoice_id
        FROM giscedata_facturacio_factura f
        LEFT JOIN giscedata_facturacio_factura rectif ON f.ref = rectif.id
        WHERE i.id = f.invoice_id AND (
            i.rectificative_type != f.tipo_rectificadora
            OR i.rectifying_id != rectif.invoice_id
        )
    '''

    cursor.execute(query)

    logger.info('Copied successfully!!')


def down(cursor, installed_version):
    pass


migrate = up
