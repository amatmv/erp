# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info("Updating credit account journals")

    cursor.execute("""
        UPDATE account_journal set default_credit_account_id = default_debit_account_id
        WHERE
            default_credit_account_id != default_debit_account_id
            and code in (
            'ENERGIA', 'ENERGIA.A', 'ENERGIA.B', 'ENERGIA.BRA', 'ENERGIA.R'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
