# -*- coding: utf-8 -*-
import pooler
import netsvc
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES

def migrate(cursor, installed_version):
    """Migration to 2.35.0
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    polissa_obj = pool.get('giscedata.polissa')
    mandate_obj = pool.get('payment.mandate')
    #Search for all polissa with an associated bank account
    search_params = [('bank', '<>', False),
                     ('state', 'not in', CONTRACT_IGNORED_STATES + ['baixa'])]
    polissa_ids = polissa_obj.search(cursor, uid, search_params)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Creating mandates for "
                         u"%s active contracts" % len(polissa_ids))
    #Search for baixa polissa with data_baixa > '2013-11-01'
    context = {'active_test': False}
    search_params = [('bank', '<>', False),
                     ('state', '=', 'baixa'),
                     ('data_baixa', '>=', '2013-11-01')]
    pol_baixa_ids = polissa_obj.search(cursor, uid, search_params, context=context)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Creating mandates for "
                         u"%s unactive contracts" % len(pol_baixa_ids))
    polissa_ids.extend(pol_baixa_ids)
    for polissa_id in polissa_ids:
        vals = {
            'reference': 'giscedata.polissa,%s' % polissa_id,
            #SEPA says all mandates signed before
            #migration must have this date
            'date': '2009-10-31'
        }
        mandate_obj.create(cursor, uid, vals)
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
    