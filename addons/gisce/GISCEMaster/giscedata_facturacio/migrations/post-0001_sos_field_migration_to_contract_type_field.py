# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Set contract_type field in giscedata_polissa to 08 if sos field is True')

    query_rename = '''
        UPDATE giscedata_polissa SET contract_type = '08' WHERE sos = true;
    '''

    cursor.execute(query_rename)


def down(cursor, installed_version):
    pass


migrate = up