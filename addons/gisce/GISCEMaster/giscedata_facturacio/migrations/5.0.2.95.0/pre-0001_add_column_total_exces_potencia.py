# coding=utf-8
import logging
from tqdm import tqdm
from oopgrade.oopgrade import column_exists, add_columns, drop_columns


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    if column_exists(cursor, 'giscedata_facturacio_factura', 'total_exces_potencia'):
        logger.info('Column total_exces_potencia already exists. Passing')
        return

    add_columns(cursor, {
        'giscedata_facturacio_factura': [('total_exces_potencia', 'numeric(16, 2)')]
    })

    logger.info('Updating all to 0')
    cursor.execute("SELECT count(id) from giscedata_facturacio_factura where total_exces_potencia is null")
    total = cursor.fetchone()[0]
    with tqdm(total=total) as pbar:
        while total:
            cursor.execute(
                "UPDATE giscedata_facturacio_factura SET total_exces_potencia = 0 "
                "WHERE id in (SELECT id from giscedata_facturacio_factura where "
                "total_exces_potencia is null LIMIT 10000)"
            )
            pbar.update(cursor.rowcount)
            cursor.execute("SELECT count(id) from giscedata_facturacio_factura where total_exces_potencia is null")
            total = cursor.fetchone()[0]

    logger.info('Updating total_exces_potencia column')
    cursor.execute(
        "UPDATE giscedata_facturacio_factura SET total_exces_potencia = u.total_exces_potencia "
        "FROM ( "
        " SELECT SUM(il.price_subtotal) AS total_exces_potencia, f.id "
        " FROM giscedata_facturacio_factura_linia l "
        " INNER JOIN account_invoice_line il on l.invoice_line_id = il.id"
        " INNER JOIN giscedata_facturacio_factura f on l.factura_id = f.id"
        " WHERE l.tipus = 'exces_potencia'"
        " GROUP BY f.id"
        ") u "
        "WHERE giscedata_facturacio_factura.id = u.id"
    )
    logger.info('Updated %s records', cursor.rowcount)


def down(cursor, installed_version):
    drop_columns(cursor, ('giscedata_facturacio_factura', 'total_exces_potencia'))


migrate = up
