# -*- coding: utf-8 -*-

import netsvc

def migrate(cursor, installed_version):
  """Migration to 2.16.0
  """
  logger = netsvc.Logger()
  logger.notifyChannel('migration', netsvc.LOG_INFO, 
                       '''Removing workflows from 
                       giscedata.facturacio.lot and 
                       giscedata.facturacio.contracte_lot''')
  sql_delete = '''delete from wkf where osv in 
  ('giscedata.facturacio.lot', 
  'giscedata.facturacio.contracte_lot')'''
  cursor.execute(sql_delete)
  logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
