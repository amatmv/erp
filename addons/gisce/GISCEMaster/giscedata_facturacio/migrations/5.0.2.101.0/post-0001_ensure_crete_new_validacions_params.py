# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    pool = pooler.get_pool(cursor.dbname)
    val_o = pool.get('giscedata.facturacio.validation.warning.template')

    val_to_update = {
        'F001': {"min_amount": 0.0},
        'F004': {"maxim_n_warings": 0},
    }
    logger.info("Creacio dels nous parametres de les validacions del lot (no em fio dels noupdates del ERP)")
    for v_code, new_params in val_to_update.iteritems():
        val_id = val_o.search(cursor, 1, [("code", '=', v_code)], context={"active_test": False})
        if not len(val_id):
            logger.error("Validacio amb codi {0} no trobada. No s'han pogut actualitzar els seus parametres".format(v_code))
        elif len(val_id) > 1:
            logger.error("S'ha trobat mes de una validacio amb codi {0} no trobada. No s'han pogut actualitzar els seus parametres".format(v_code))
        else:
            try:
                val_id = val_id[0]
                val_params = val_o.read(cursor, 1, val_id, ['parameters'])['parameters']
                needs_update = False
                for new_param in new_params:
                    if new_param not in val_params.keys():
                        needs_update = True
                        break
                if needs_update:
                    val_params.update(new_params)
                    val_o.write(cursor, 1, val_id, {'parameters': val_params})
            except:
                logger.error("Hi ha hagut un problema al llegir els parametres de la validacio codi {0} no trobada. No s'han pogut actualitzar els seus parametres".format(v_code))
    logger.info("Migracio dels nous parametres de les validacions del lot finalitzada.")


migrate = up
