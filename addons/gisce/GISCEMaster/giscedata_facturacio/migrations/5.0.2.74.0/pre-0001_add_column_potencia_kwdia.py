# coding=utf-8
from oopgrade import oopgrade
import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Adding column potencia_kwdia to giscedata_facturacio_factura '
        'to prevent the migration from calculating it (because it takes '
        'a very long time).'
    )

    oopgrade.add_columns(
        cursor, {
            'giscedata_facturacio_factura': [
                ('potencia_kwdia', 'numeric(16,2)')
            ]
        }
    )

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully added!')


def down(cursor, installed_version):
    pass

migrate = up
