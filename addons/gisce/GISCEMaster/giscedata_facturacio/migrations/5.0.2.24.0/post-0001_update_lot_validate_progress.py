# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
    """Migration to 2.24.0
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    lot_obj = pool.get('giscedata.facturacio.lot')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating Validate progress bar in lot')
    #Search for all lots
    lot_ids = lot_obj.search(cursor, uid, [])
    #Update progress for each one
    lot_obj.update_progress(cursor, uid, lot_ids)

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
