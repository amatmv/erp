# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    """Migration to 2.22.0
    Regularize those polisses with bank/partner_id
    not belonging to pagador. A restriction is set for 2.22
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    polissa_obj = pool.get('giscedata.polissa')
    bank_obj = pool.get('res.partner.bank')
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Checking polisses with bank/"
                         u"partner_id different from pagador")

    query = '''select polissa.id from giscedata_polissa polissa
            inner join res_partner_bank bank
            on bank.id = polissa.bank
            where bank.partner_id <> polissa.pagador
            and polissa.state not in ('esborrany', 'validar', 'baixa')'''

    cursor.execute(query)
    polissa_ids = [x[0] for x in cursor.fetchall()]
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating %s polisses.' % len(polissa_ids))
    for polissa in polissa_obj.browse(cursor, uid, polissa_ids):
        bank = polissa.bank
        #Search for acc_number in pagador
        search_params = [('partner_id', '=', polissa.pagador.id),
                         ('acc_number', '=', bank.acc_number)]
        bank_ids = bank_obj.search(cursor, uid, search_params)
        if bank_ids:
            bank_id = bank_ids[0]   
        else:
            #If not found create bank for pagador
            vals = {'name': bank.name,
                    'acc_number': bank.acc_number,
                    'acc_country_id': (bank.acc_country_id and
                                       bank.acc_country_id.id or
                                       False),
                    'country_id': (bank.country_id and
                                  bank.country_id.id
                                  or False),
                    'partner_id': polissa.pagador.id,
                    'bank': (bank.bank and
                             bank.bank.id
                             or False),
                    'owner_name': polissa.pagador.name,
                    'state': bank.state or False}
            bank_id = bank_obj.create(cursor, uid, vals)
        polissa.write({'bank': bank_id}, {'sync': False})
        polissa.modcontractual_activa.write({'bank': bank_id},
                                            {'sync': False})
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done')
