# -*- coding: utf-8 -*-

import netsvc

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Migrating old workflows.')
    cursor.execute("""
UPDATE wkf_activity SET flow_stop = False where id in (
    SELECT a.id
    FROM wkf_activity a
    LEFT JOIN wkf w on a.wkf_id = w.id
    WHERE
        w.osv = 'account.invoice'
        AND a.name = 'paid'
        AND a.flow_stop = True)
    """)
    cursor.execute("""
UPDATE wkf_instance SET state = 'active' where id in (
    SELECT w.id
    FROM wkf_instance w 
    LEFT JOIN wkf_workitem wk on wk.inst_id = w.id
    LEFT JOIN wkf_activity a on wk.act_id = a.id 
    WHERE
        w.state = 'complete'
        AND w.res_type = 'account.invoice'
        AND a.name = 'paid')
    """)
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')