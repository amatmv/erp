# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    """Migration to 2.22.0
    Update fiscal position in invoices
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    invoice_obj = pool.get('account.invoice')
    factura_obj = pool.get('giscedata.facturacio.factura')
    fp_obj = pool.get('account.fiscal.position')
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Checking fiscal position invoices")
    context = {'sync': False}
    #Update query
    update_query = '''update account_invoice
                      set fiscal_position = %s
                      where id in %s'''
    #Check for invoices with 16% IVA
    query = '''select distinct invoice_id
                from account_invoice_tax
                where (name ilike '%IVA%16%' or name ilike '%16%Soportado%')
                and invoice_id not in (select distinct invoice_id
                from account_invoice_tax where name ilike '%IVA%18%'
                or name ilike '%18%Soportado%')'''
    
    cursor.execute(query)
    invoice_16_ids = [x[0] for x in cursor.fetchall()]
    
    #Check for invoices with 18% IVA
    query = '''select distinct invoice_id
                from account_invoice_tax
                where (name ilike '%IVA%18%' or name ilike '%18%Soportado%')
                and invoice_id not in (select distinct invoice_id
                from account_invoice_tax where name ilike '%IVA%21%'
                or name ilike '%21%Soportado%')'''
    
    cursor.execute(query)
    invoice_18_ids = [x[0] for x in cursor.fetchall()]
    
    #Check for invoices with 21% IVA
    query = '''select distinct invoice_id
                from account_invoice_tax
                where (name ilike '%IVA%21%' or name ilike '%21%Soportado%')'''
    
    cursor.execute(query)
    invoice_21_ids = [x[0] for x in cursor.fetchall()]

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating %s invoices with IVA 16.'
                         % len(invoice_16_ids))
    #Search for 2009 fiscal position
    if len(invoice_16_ids):
        search_params = [('name', '=', 'Régimen nacional 2009')]
        fp_16_id = fp_obj.search(cursor, uid, search_params)[0]
        cursor.execute(update_query, (fp_16_id, tuple(invoice_16_ids),))

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating %s invoices with IVA 18.'
                         % len(invoice_18_ids))
    #Search for 2010 fiscal position
    if len(invoice_18_ids):
        search_params = [('name', '=', 'Régimen nacional 2010')]
        fp_18_id = fp_obj.search(cursor, uid, search_params)[0]
        cursor.execute(update_query, (fp_18_id, tuple(invoice_18_ids),))

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating %s invoices with IVA 21.'
                         % len(invoice_21_ids))
    #Search for 2012 fiscal position
    if len(invoice_21_ids):
        search_params = [('name', '=', 'Régimen nacional 2012')]
        fp_21_id = fp_obj.search(cursor, uid, search_params)[0]
        cursor.execute(update_query, (fp_21_id, tuple(invoice_21_ids),))
    
    #Update refund invoices with correct fiscal position
    search_params = [('tipo_rectificadora', 'in', ('A', 'B', 'R'))]
    factura_ids = factura_obj.search(cursor, uid, search_params)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating %s refund invoices.'
                         % len(factura_ids))
    for factura in factura_obj.browse(cursor, uid, factura_ids):
        if factura.ref:
            factura.write({'fiscal_position': (factura.ref.fiscal_position and
                                               factura.ref.fiscal_position.id
                                               or False)},
                          context=context)
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done')
