# -*- coding: utf-8 -*-
import pooler
from datetime import datetime
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log


def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    chart_updater_obj = pool.get('wizard.update.charts.accounts')
    t_id = pool.get('account.chart.template').search(cursor, uid, 
                [('template_name', '=', 'PGCE 2008')])[0]
    vals = {
        'code_digits': 6,
        'chart_template_id': t_id
    }
    w_id = chart_updater_obj.create(cursor, uid, vals)
    updater = chart_updater_obj.browse(cursor, uid, w_id)
    updater.action_find_records()
    updater.action_update_records()
    updater = chart_updater_obj.browse(cursor, uid, w_id)

    # Fiquem les comptes correctes als partners
    properties = [('property_account_payable', '430000'),
                  ('property_account_receivable', '410000'),
                  ('property_account_expense_categ', '600000'),
                  ('property_account_income_categ', '700000')]
    ir_prop_obj = pool.get('ir.property')
    account_obj = pool.get('account.account')
    for prop_name, acc_code in properties:
        prop_ids = ir_prop_obj.search(cursor, uid, [('name', '=', prop_name)])
        first = True
        for prop in ir_prop_obj.browse(cursor, uid, prop_ids):
            if first:
                acc_id = account_obj.search(cursor, uid,
                                            [('code', '=', acc_code)])[0]
                prop.write({'value': 'account.account,%i' % acc_id,
                            'res_id': False})
                first = False
            else:
                prop.unlink()
    log("Netejant valors per defecte de comtpes antics...")
    # Tuples amb neteja i quants n'hem de deixar
    for to_clean, remove in (('property_account_income', 1),
                             ('property_account_tax', 0),
                             ('property_account_expense', 1)):
        ir_ids = ir_prop_obj.search(cursor, uid, [('name', '=', to_clean)])
        ir_prop_obj.write(cursor, uid, ir_ids, {
            'res_id': False,
            'value': False
        })
        # Els eliminem tots menys el primer
        ir_prop_obj.unlink(cursor, uid, ir_ids[remove:])
