# -*- coding: utf-8 -*-
import pooler
from datetime import datetime
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log


def migrate(cursor, installed_version):
    # Actualitzem a totes la llista de preu a TARIFAS ELECTRICIDAD i a
    # la primera versió que trobem després després de la data d'alta
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    # Arreglem períodes comptables incorrectes
    cursor.execute("""select
      y.id,
      y.code,
      y.date_start,
      y.date_stop,
      min(p.date_start) as p_date_start,
      max(p.date_stop) as p_date_stop
    from
      account_fiscalyear y,
      account_period p
    where
      p.fiscalyear_id = y.id
    group by 
      y.id,
      y.code,
      y.date_start,
      y.date_stop 
    having
      y.date_start != min(p.date_start)
      or y.date_stop != max(p.date_stop)""")
    fyerrors = cursor.dictfetchall()
    for fye in fyerrors:
        code = fye['p_date_stop'].split('-')[0]
        log("Updating fiscalyear (id:%s) to code: %s [%s -%s]" % (fye['id'], code, fye['p_date_start'], fye['p_date_stop']))
        cursor.execute("""update account_fiscalyear set code = %s,
        name = %s, date_start = %s, date_stop = %s where id = %s""",
        (code, code, fye['p_date_start'], fye['p_date_stop'], fye['id']))
    company_id = pool.get('res.users').browse(cursor, uid, uid).company_id.id
    fy_obj = pool.get('account.fiscalyear')
    cursor.execute("select min(date_invoice) from account_invoice")
    data = cursor.fetchone()[0]
    if not data:
        return
    start_year = int(data.split('-')[0])
    max_year = datetime.now().year
    while start_year <= max_year:
        num = fy_obj.search_count(cursor, uid, [('name', '=', start_year)])
        if not num:
            log("Creant periode fiscal per l'any %s." % start_year)
            vals = {
                'name': start_year,
                'code': start_year,
                'date_start': '%s-01-01' % start_year,
                'date_stop': '%s-12-31' % start_year
            }
            fy_id = fy_obj.create(cursor, uid, vals)
            fy_obj.create_period(cursor, uid, [fy_id])
        start_year += 1
