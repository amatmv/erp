# -*- coding: utf-8 -*-
import pooler
from datetime import datetime
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log

def check_n_dups(cursor):
    cursor.execute("""
select
  f.id,
  f.name,
  regexp_replace(f.name, '/' || to_char(i.date_invoice, 'YYYY')::int + 1, '/'
  || to_char(i.date_invoice, 'YYYY')) as new_name,
  i.date_invoice
from giscedata_factura f
left join account_invoice i on (f.factura = i.id)
where f.name in (select name from giscedata_factura
group by name having count(id) > 1)
and f.name != regexp_replace(f.name, '/'
|| to_char(i.date_invoice, 'YYYY')::int + 1, '/'
|| to_char(i.date_invoice, 'YYYY'))""")
    return cursor.rowcount


def fix_dups(cursor):
    cursor.execute("""
update giscedata_factura f set name=upd.new_name from (
select
  f.id,
  f.name,
  regexp_replace(f.name, '/' || to_char(i.date_invoice, 'YYYY')::int + 1, '/'
  || to_char(i.date_invoice, 'YYYY')) as new_name,
  i.date_invoice
from giscedata_factura f
left join account_invoice i on (f.factura = i.id)
where f.name in (select name from giscedata_factura
group by name having count(id) > 1)
and f.name != regexp_replace(f.name, '/'
|| to_char(i.date_invoice, 'YYYY')::int + 1, '/'
|| to_char(i.date_invoice, 'YYYY'))
) as upd
where f.id = upd.id""")
    return cursor.rowcount


def migrate(cursor, installed_version):
    n_dups = check_n_dups(cursor)
    if n_dups:
        log("Renaming %s duplicated invoices..." % n_dups)
        n_fixed = fix_dups(cursor)
        log("Renamed %s duplicated invoices" % fix_dups)
        n_dups = check_n_dups(cursor)
        if n_dups:
            raise Exception("Already %s duplicated invoices!" % n_dups)
