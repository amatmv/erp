# -*- coding: utf-8 -*-
"""Creem els lots per assignar a les pòlisses
"""
from datetime import datetime

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
import pooler

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    # Busquem la data més petita de factura
    cursor.execute("select min(date_invoice) from account_invoice")
    # Agafem l'any YYYY-MM-DD -> int(YYYY)
    data = cursor.fetchone()[0]
    if not data:
        return
    start_year = int(data.split('-')[0])
    max_year = datetime.now().year
    lot_obj = pool.get('giscedata.facturacio.lot')
    while start_year <= max_year:
        data_final = '%s-12-31' % start_year
        if not lot_obj.search_count(cursor, uid,
                                    [('data_final', '=', data_final)]):
            log("Creant lots per l'any %s." % start_year)
            lot_obj.crear_lots_mensuals(cursor, uid, start_year)
        start_year += 1
