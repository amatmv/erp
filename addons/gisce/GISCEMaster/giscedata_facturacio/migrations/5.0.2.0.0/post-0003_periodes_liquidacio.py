# -*- coding: utf-8 -*-
import pooler
from datetime import datetime
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log


def migrate(cursor, installed_version):
    # Actualitzem a totes la llista de preu a TARIFAS ELECTRICIDAD i a
    # la primera versió que trobem després després de la data d'alta
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    company_id = pool.get('res.users').browse(cursor, uid, uid).company_id.id
    fpd_obj = pool.get('giscedata.liquidacio.fpd')
    cursor.execute("select min(date_invoice) from account_invoice")
    data = cursor.fetchone()[0]
    if not data:
        return
    start_year = int(data.split('-')[0])
    max_year = datetime.now().year
    while start_year <= max_year:
        log("Creant lot facturacio per l'any %s." % start_year)
        for mes in range(1,13):
            inici = '%s-%s-16' % (start_year, str(mes).zfill(2))
            cursor.execute("SELECT to_char(date %s + interval '20 day', 'YYYY-MM') || '-15'", (inici,))
            final = cursor.fetchone()[0]
            vals = {
              'inici': inici,
              'final': final,
              'month': str(mes).zfill(2),
              'company_id': company_id,
              'year': start_year
            }
            fpd_obj.create(cursor, uid, vals)
        start_year += 1

