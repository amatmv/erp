# -*- coding: utf-8 -*-
"""Aquí tipus de pagament antics
"""
import pooler
import netsvc
from tools import cache, config
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
from progressbar import ProgressBar, ETA, Percentage, Bar
from datetime import datetime, timedelta
import calendar

def migrate(cursor, installed_version):
    # Actualitzem a totes la llista de preu a TARIFAS ELECTRICIDAD i a
    # la primera versió que trobem després després de la data d'alta
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    cursor.execute("""
INSERT INTO payment_type
    (create_uid,
    create_date,
    write_date,
    write_uid,
    code,
    name,
    note,
    active)
SELECT
    create_uid,
    create_date,
    write_date,
    write_uid,
    name,
    name,
    note,
    active
FROM account_paytype""")
