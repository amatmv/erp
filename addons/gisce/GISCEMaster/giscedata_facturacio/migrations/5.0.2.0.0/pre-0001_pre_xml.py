# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc

def migrate(cursor, installed_version):
    """Canvis a executar relatius al mòdul base
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # backup
    mig = GisceUpgradeMigratoor('giscedata_facturacio', cursor)
    mig.backup(only_load=True)
    mig.pre_xml(xmlfiles=['giscedata_facturacio_data.xml'])