# -*- coding: utf-8 -*-
"""Aquí migrarem tots els camps de facturació que tenen les pòlisses.
"""
import pooler
import netsvc
from tools import cache, config
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
from progressbar import ProgressBar, ETA, Percentage, Bar
from datetime import datetime, timedelta
import calendar

@cache()
def get_lot(fck, cursor, uid, data):
    if not data:
        return False
    lot_obj = pooler.get_pool(cursor.dbname).get('giscedata.facturacio.lot')
    search_params = [('data_inici', '<', data),
                     ('data_final', '>=', data)]
    lots_ids = lot_obj.search(cursor, uid, search_params)
    if lots_ids:
        return lots_ids[0]
    return False

@cache()
def get_direccio_notificacio(fck, cursor, uid, partner_id):
    partner_obj = pooler.get_pool(cursor.dbname).get('res.partner')
    return partner_obj.browse(cursor, uid, partner_id).address[0].id

@cache()
def get_direccio_pagament(fck, cursor, uid, partner_id):
    partner_obj = pooler.get_pool(cursor.dbname).get('res.partner')
    return partner_obj.browse(cursor, uid, partner_id).address[0].id

@cache()
def get_primera_versio(fck, cursor, uid, data_alta):
    plv_obj = pooler.get_pool(cursor.dbname).get('product.pricelist.version')
    pl_ids = plv_obj.search(cursor, uid, [
        ('date_start', '<=', data_alta),
        '|',
        ('date_end', '>=', data_alta),
        ('date_end', '=', False),
        ('pricelist_id.name', '=', 'TARIFAS ELECTRICIDAD')
    ], limit=1)
    if not pl_ids:
        pl_ids = plv_obj.search(cursor, uid, [
             ('pricelist_id.name', '=', 'TARIFAS ELECTRICIDAD')
        ], order="date_start asc", limit=1)
    return pl_ids and pl_ids[0] or False

def migrate(cursor, installed_version):
    # Actualitzem a totes la llista de preu a TARIFAS ELECTRICIDAD i a
    # la primera versió que trobem després després de la data d'alta
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imb_obj = pool.get('ir.model.data')
    imd_id = imb_obj._get_id(cursor, uid, 'giscedata_facturacio',
                             'pricelist_tarifas_electricidad')
    llista_preu = imb_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
    
    pol_obj = pool.get('giscedata.polissa')
    mc_obj = pool.get('giscedata.polissa.modcontractual')
    wc_obj = pool.get('giscedata.polissa.crear.contracte')
    pol_ids = pol_obj.search(cursor, uid,
                             [('state', 'not in', ('esborrany', 'validar'))],
                             context={'active_test': False})
    if not pol_ids:
        return
    # Obtenim valores per actualitzar
    cursor.execute("""SELECT 
                        f.polissa as polissa,
                        max(p.proxima_facturacio) as proxima_facturacio,
                        to_char(max(l.data_actual), 'YYYY-MM-DD') as data
                      FROM
                        giscedata_factura f,
                        giscedata_polissa p,
                        giscedata_factura_lectura_energia l
                      WHERE
                        f.polissa = p.id
                        and l.factura = f.id
                      GROUP by f.polissa""")
    updates = {}
    for res in cursor.dictfetchall():
        updates[res['polissa']] = {
            'data_ultima_lectura': res['data'],
            'lot_facturacio': get_lot(False, cursor, uid, res['proxima_facturacio']),
        }
    total = len(pol_ids)
    widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=total).start()
    done = 0
    # Arreglem totes les possibles factures que data_final sigui 
    # >= que data_inici
    cursor.execute("""SELECT
    id,
    data_inici,
    data_final,
    facturacio
    FROM giscedata_factura
    WHERE data_final <= data_inici
    """)
    factures = cursor.dictfetchall()
    for factura in factures:
        dti = datetime.strptime(factura['data_final'], '%Y-%m-%d')
        facturacio = factura['facturacio']
        while facturacio:
            dies_del_mes = calendar.monthrange(dti.year, dti.month)[1]
            dti -= timedelta(days=dies_del_mes)
            facturacio -= 1
        dti += timedelta(days=1)
        data_inici = dti.strftime('%Y-%m-%d')
        cursor.execute("""update giscedata_factura set data_inici = %s
        where id = %s""", (data_inici, factura['id']))
    for polissa_id in pol_ids:
        done += 1
        primera = True
        cursor.execute("""DELETE from giscedata_polissa_modcontractual
        where polissa_id = %s and id not in (
        select modcontractual_activa from giscedata_polissa where id = %s)""",
        (polissa_id, polissa_id))
        cursor.execute("""SELECT
        greatest(c.data_alta, f.data_inici) as data_inici_real,
        least(c.data_baixa, f.data_final) as data_final_real,
        f.id,
        f.tarifa,
        f.comercialitzadora,
        f.facturacio,
        i.partner_id,
        pt2.id as tipopago_id,
        max(pot_contract) as pot_contract
  FROM
        giscedata_factura f
  INNER JOIN account_invoice i on (i.id = f.factura)
  INNER JOIN res_partner rp on (i.partner_id  = rp.id)
  INNER JOIN giscedata_lectures_comptador c on (f.comptador = c.id)
  INNER JOIN giscedata_factura_lectura_potencia lp on (lp.factura = f.id)
  LEFT JOIN account_paytype pt on (rp.tipopago_id = pt.id)
  LEFT JOIN payment_type pt2 on (pt.name = pt2.code)
  WHERE
        f.tipo_rectificadora in ('N', 'R')
        and f.id not in (select ref from giscedata_factura where tipo_rectificadora = 'R')
        and f.polissa = %s
  GROUP BY
         data_inici_real, data_final_real, c.data_alta, c.data_baixa, f.data_inici,
         f.data_final, f.id, f.tarifa, f.comercialitzadora, f.facturacio,
         i.partner_id, pt2.id
  order by f.data_final asc""", (polissa_id,))
        factures =  cursor.dictfetchall()
        # Nou reduce per detectar canvis de comercialtizadores que tornen
        # però sense canvis
        res_done = []
        for r in factures:
            if not res_done:
                res_done.append(r)
            else:
                r1 = res_done[-1].copy()
                r2 = r.copy()
                # Busquem diferències
                if any([r1[k] != r2[k]
                        for k in ('tarifa', 'comercialitzadora', 'facturacio',
                                  'partner_id', 'tipopago_id', 'pot_contract')]):
                    res_done.append(r2)
                else:
                    res_done[-1]['data_inici_real'] = min(r1['data_inici_real'],
                                                          r2['data_inici_real'])
                    res_done[-1]['data_final_real'] = max(r1['data_final_real'],
                                                          r2['data_final_real'])
        for factura in res_done:
            # Refresquem la pòlissa
            polissa = pol_obj.browse(cursor, uid, polissa_id)
            mc_vals = mc_obj.get_vals_from_polissa(cursor, uid, polissa.id)
            if mc_vals.get('facturacio_potencia', '') == 'rec':
                mc_vals['facturacio_potencia'] = 'recarrec'
            vals = {
                'comercialitzadora': factura['partner_id'],
                'llista_preu': llista_preu,
                'pagador': factura['partner_id'],
                'direccio_pagament': get_direccio_pagament(False, cursor, uid, factura['comercialitzadora']),
                'direccio_notificacio': get_direccio_notificacio(False, cursor, uid, factura['partner_id']),
                'facturacio': factura['facturacio'],
                'tarifa': factura['tarifa'],
                'tipo_pago': factura['tipopago_id'],
            }
            if factura['pot_contract']:
                vals['potencia'] = factura['pot_contract']
            mc_vals.update(vals)
            if int(mc_vals['facturacio']) not in (1, 2):
                mc_vals['facturacio'] = 2
                mc_vals['observacions'] = 'Camp facturacio ' \
                                          'tenia valor %s' % (factura['facturacio'])
            # Si és la primera ho escrivim a al modcontractual directament
            polissa.send_signal(['modcontractual'])
            if primera:
                mc_vals.update({'data_inici': factura['data_inici_real'],
                                'data_final': factura['data_final_real'],
                                'name': '1'})
                polissa.modcontractual_activa.write(mc_vals)
                primera = False
                polissa.send_signal(['undo_modcontractual'])
                log(u"Pòlissa %s. Primera ronda"
                    % (polissa.name), netsvc.LOG_DEBUG)
                continue
            vals.update({'notificacio': 'titular'})
            polissa.write(vals)
            polissa.generar_periodes_potencia()
            changes = polissa.get_changes()
            obs = changes.get('observacions', '')
            if 'observacions' in changes:
                del changes['observacions']
            if not changes:
                del mc_vals['data_inici']
                mc_vals['data_final'] = factura['data_final_real']
                polissa.modcontractual_activa.write(mc_vals)
                log(u"Pòlissa %s. Mantenint contracte"
                    % (polissa.name), netsvc.LOG_DEBUG)
                polissa.send_signal(['undo_modcontractual'])
            else:
                # Creem el nou contracte
                context = {'active_id': polissa.id}
                w_vals = {'data_inici': factura['data_inici_real'],
                          'data_final': factura['data_final_real'],
                          'duracio': 'nou'}
                wid = wc_obj.create(cursor, uid, w_vals, context)
                wc_obj.action_crear_contracte(cursor, uid, [wid], context)
                wc_obj.unlink(cursor, uid, [wid])
                log(u"Pòlissa %s. "
                    u"Crean nova modificació contractual %s - %s"
                    % (polissa.name,
                       factura['data_inici_real'], factura['data_final_real']),
                    netsvc.LOG_DEBUG)
                # Actualitzem l'objecte pòlissa
                polissa = polissa.browse()[0]
                if obs:
                    polissa.modcontractual_activa.write({'observacions': obs})
                log("Modcontractual activa: %s" %
                    polissa.modcontractual_activa.name, netsvc.LOG_DEBUG)
        if polissa_id in updates:
            polissa = pol_obj.browse(cursor, uid, polissa_id)
            updates[polissa.id].update({
                'versio_primera_factura': get_primera_versio(False, cursor,
                                                             uid,
                                                             polissa.data_alta)
            })
            polissa.write(updates[polissa.id])
            polissa = pol_obj.browse(cursor, uid, polissa_id)
            # Allarguem la modificació contractual fins que passi la data
            # final del lot de facturació assignat
            if polissa.active and polissa.lot_facturacio:
                data_final = datetime.strptime(
                    polissa.modcontractual_activa.data_final,
                    '%Y-%m-%d'
                )
                data_final_lot = datetime.strptime(
                    polissa.lot_facturacio.data_final,
                    '%Y-%m-%d'
                )
                while data_final < data_final_lot:
                    data_final += timedelta(364)
                polissa.modcontractual_activa.write({
                    'data_final': data_final.strftime('%Y-%m-%d')
                })
            del updates[polissa_id]
        pbar.update(done)
    cursor.execute("update giscedata_polissa set pagador_sel = 'comercialitzadora'")
    cursor.execute("update giscedata_polissa set notificacio = 'pagador'")
    cursor.execute("update giscedata_polissa_modcontractual "
                   "set notificacio = 'pagador'")
    cursor.execute("update giscedata_polissa set "
                   "direccio_notificacio=direccio_pagament")
    cursor.execute("update giscedata_polissa_modcontractual set "
                   "direccio_notificacio=direccio_pagament")
    # Per defecte unitats kW/mes
    pbar.finish()
    uom_id = pool.get('product.uom').search(cursor, uid, [
        ('name', '=', 'kW/mes')
    ])[0]
    imd_obj = pool.get('ir.model.data')
    irp_obj = pool.get('ir.property')
    for xml_id in ('property_unitat_potencia_polissa',
                   'property_unitat_potencia_modcontractual'):
        imd_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio', xml_id)
        res_id = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
        log("Default uom for %s: kw/mes (id:%s)" % (xml_id, uom_id))
        irp_obj.write(cursor, uid, res_id,
                      {'value': '%s,%s' % ('product.uom', uom_id)})
    pool.get('res.config').set(cursor, uid, 'fact_uom_cof_to_multi', '0')
    cache.clean_caches_for_db(cursor.dbname)
