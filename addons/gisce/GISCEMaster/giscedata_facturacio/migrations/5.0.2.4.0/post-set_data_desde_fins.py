# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
  """Migration to 2.4.0
  """
  logger = netsvc.Logger()
  uid = 1
  pool = pooler.get_pool(cursor.dbname)
  # Assignem data_desde i data_fins a les línies de factura
  factura_obj = pool.get('giscedata.facturacio.factura')
  linia_obj = pool.get('giscedata.facturacio.factura.linia')
  # 1. Obtenim les diferents dates_inici, dates_final
  # 2. Obtenim les línies de factura per cadascuna i que a més no tenen
  #    data desde i data fins assignades
  # 3. Actualitzem massivament
  sql = ("select distinct data_inici, data_final "
         "from giscedata_facturacio_factura")
  cursor.execute(sql)
  for row in cursor.fetchall():
      search_params = [('data_inici', '=', row[0]),
                       ('data_final', '=', row[1])]
      fids = factura_obj.search(cursor, uid, search_params)
      search_params = ['|',
                       ('data_desde', '=', False),
                       ('data_fins', '=', False),
                       ('factura_id', 'in', fids)]
      lids = linia_obj.search(cursor, uid, search_params)
      logger.notifyChannel('migration', netsvc.LOG_INFO,
                           'Updating %s - %s:  %i lines.' % (row[0], row[1],
                                                            len(lids)))
      linia_obj.write(cursor, uid, lids, {'data_desde': row[0],
                                          'data_fins': row[1]})
      logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
