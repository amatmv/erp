# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
    """Migration to 2.21.0
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd = pool.get('ir.model.data')
    refact_xml_ids = {
        'ref_T42011': ('2011-10-01', '2011-12-31'),
        'ref_T12012': ('2012-01-01', '2012-03-31'),
        'ref_M42012': ('2012-04-01', '2012-04-30')
    }
    # Es localitzen els ids dels productes segons imd
    REFACT = {}
    for xml_id, dates in refact_xml_ids.items():
        md_id = imd.search(cursor, uid, [
            ('module', '=', 'giscedata_facturacio'),
            ('name', '=', xml_id)
        ])
        REFACT[imd.browse(cursor, uid, md_id[0]).res_id] = dates

    # S'actualitzen les dates de les linies extra                 
    ext_obj = pool.get('giscedata.facturacio.extra')
    search_params = [('product_id.id', 'in', REFACT.keys())]
    eids = ext_obj.search(cursor, uid, search_params)
    ext_refact = ext_obj.browse(cursor, uid, eids)
    vals = {}
    for num, refact in enumerate(ext_refact):
        vals.update({'date_line_from': REFACT[refact.product_id.id][0],
                     'date_line_to': REFACT[refact.product_id.id][1],
                    })
        refact.write(vals)
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating %s out of %s lines.' % (num + 1, len(eids)))
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
