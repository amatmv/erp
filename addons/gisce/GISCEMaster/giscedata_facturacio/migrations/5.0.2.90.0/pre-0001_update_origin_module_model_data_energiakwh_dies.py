# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Start update origin module from giscedata_facturacio_comer to '
                'giscedata_facturacio for energia_kwh and dies as ')

    query = '''
    UPDATE ir_model_data SET module = 'giscedata_facturacio' 
        WHERE
            (name = 'field_giscedata_facturacio_factura_energia_kwh'
                AND module = 'giscedata_facturacio_comer'
            )
            OR
            (
            name = 'field_giscedata_facturacio_factura_dies'
                AND module = 'giscedata_facturacio_comer'
            )
    '''

    cursor.execute(query)

    logger.info('Updated origin module from giscedata_facturacio_comer to '
                'giscedata_facturacio successfully!!')


def down(cursor, installed_version):
    pass


migrate = up
