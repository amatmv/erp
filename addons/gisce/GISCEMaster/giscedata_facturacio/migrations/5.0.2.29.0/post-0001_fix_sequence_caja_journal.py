# -*- coding: utf-8 -*-
import pooler
import netsvc

def migrate(cursor, installed_version):
    """Migration to 2.29.0
    """
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Updating wrong sequence for CAJA journal')
    # Obtenim el diari CAIXA
    imd_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio',
                             'facturacio_journal_caja')
    journal_id = imd_obj.browse(cursor, uid, imd_id).res_id
    # Obtenim la seqüència pels assentaments
    imd_id = imd_obj._get_id(cursor, uid, 'account', 'sequence_journal')
    seq_id = imd_obj.browse(cursor, uid, imd_id).res_id
    
    pool.get('account.journal').write(cursor, uid, [journal_id], {
        'sequence_id': seq_id
    })
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')
