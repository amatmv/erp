# coding=utf-8

import logging
from oopgrade.oopgrade import column_exists, add_columns, drop_columns


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    if column_exists(cursor, 'giscedata_facturacio_factura', 'total_altres'):
        logger.info('Column total_altres already exists. Passing')
        return

    add_columns(cursor, {
        'giscedata_facturacio_factura': [('total_altres', 'float')]
    })

    logger.info('Updating all to 0')
    cursor.execute("UPDATE giscedata_facturacio_factura SET total_altres = 0")
    logger.info('Updating total_altres column')
    cursor.execute(
        "UPDATE giscedata_facturacio_factura SET total_altres = u.total_altres "
        "FROM ( "
        " SELECT SUM(il.price_subtotal) AS total_altres, f.id "
        " FROM giscedata_facturacio_factura_linia l "
        " INNER JOIN account_invoice_line il on l.invoice_line_id = il.id"
        " INNER JOIN giscedata_facturacio_factura f on l.factura_id = f.id"
        " WHERE l.tipus = 'altres'"
        " GROUP BY f.id"
        ") u "
        "WHERE giscedata_facturacio_factura.id = u.id"
    )
    logger.info('Updated %s records', cursor.rowcount)


def down(cursor, installed_version):
    drop_columns(cursor, ('giscedata_facturacio_factura', 'total_altres'))


migrate = up
