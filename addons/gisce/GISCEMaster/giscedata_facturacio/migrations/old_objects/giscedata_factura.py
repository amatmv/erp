# *-* coding: utf-8 *-*
from dateutil.relativedelta import relativedelta

from osv import osv, fields
import math
import calendar
import time
import tools
from datetime import datetime, timedelta
from decimal import Decimal

from giscedata_facturacio.giscedata_facturacio \
    import REFUND_RECTIFICATIVE_INVOICE_TYPES, \
    RECTIFYING_RECTIFICATIVE_INVOICE_TYPES, \
    ALL_RECTIFICATIVE_INVOICE_TYPES

# HACK: per tal d'establir un llindar fins a quina data fem arrodoniment a 4 decimals
# i a partir de quina passem a arrodonir a dos.
_hardcoded_round_date = '2010-06-30'

class giscedata_facturacio_liquidacio_year(osv.osv):
    _name = "giscedata.facturacio.liquidacio.year"
    _module = "giscedata_facturacio_old"
    _description = "Año de liquidación de Energía"
    _columns = {
      'name': fields.char('Año liquidacion', size=64, required=True),
      'code': fields.char('Código', size=6, required=True),
      'date_start': fields.date('Fecha inicio', required=True),
      'date_stop': fields.date('Fecha final', required=True),
      'period_ids': fields.one2many('giscedata.facturacio.liquidacio.period', 'year_id', 'Periodos'),
      'state': fields.selection([('draft','Draft'), ('done','Done')], 'State', redonly=True),
    }

    _defaults = {
      'state': lambda *a: 'draft',
    }
    _order = "date_start"

    #def create_period3(self,cr, uid, ids, context={}):
    #  return self.create_period(cr, uid, ids, context, 3)

    def create_period(self,cr, uid, ids, context={}, interval=1):
        for fy in self.browse(cr, uid, ids, context):
            ds = datetime.strptime(fy.date_start, '%Y-%m-%d')
            while ds.strftime('%Y-%m-%d')<fy.date_stop:
                de = ds + relativedelta(months=interval, days=-1)
                self.pool.get('giscedata.facturacio.liquidacio.period').create(cr, uid, {
                  'name': de.strftime('%m/%Y'),
                  'code': de.strftime('%m/%Y'),
                  'date_start': ds.strftime('%Y-%m-%d'),
                  'date_stop': de.strftime('%Y-%m-%d'),
                  'year_id': fy.id,
                })
                ds = ds + relativedelta(months=interval)
        return True

    def find(self, cr, uid, dt=None, exception=True, context={}):
        if not dt:
            dt = time.strftime('%Y-%m-%d')
        ids = self.search(cr, uid, [('date_start', '<=', dt), ('date_stop', '>=', dt)])
        if not ids:
            if exception:
                raise osv.except_osv('Error !', 'No fiscal year defined for this date !\nPlease create one.')
            else:
                return False
        return ids[0]
giscedata_facturacio_liquidacio_year()

class giscedata_facturacio_liquidacio_period(osv.osv):
    _name = "giscedata.facturacio.liquidacio.period"
    _module = "giscedata_facturacio_old"
    _description = "Periodo de liquidación de energia"
    _columns = {
      'name': fields.char('Nombre', size=64, required=True),
      'code': fields.char('Código', size=12),
      'date_start': fields.date('Fecha inicio', required=True, states={'done':[('readonly',True)]}),
      'date_stop': fields.date('Fecha final', required=True, states={'done':[('readonly',True)]}),
      'year_id': fields.many2one('giscedata.facturacio.liquidacio.year', 'Año de liquidación', required=True, states={'done':[('readonly',True)]}, select=True),
      'state': fields.selection([('draft','Draft'), ('done','Done')], 'State', readonly=True)
    }
    _defaults = {
      'state': lambda *a: 'draft',
    }
    _order = "date_start"
    def next(self, cr, uid, period, step, context={}):
        ids = self.search(cr, uid, [('date_start','>',period.date_start)])
        if len(ids)>=step:
            return ids[step-1]
        return False

    def find(self, cr, uid, dt=None, context={}):
        if not dt:
            dt = time.strftime('%Y-%m-%d')
#CHECKME: shouldn't we check the state of the period?
        ids = self.search(cr, uid, [('date_start','<=',dt),('date_stop','>=',dt)])
        if not ids:
            raise osv.except_osv('Error !', 'No hay un periodo definido por la fecha %s !\nPor favor cree un año de liquidacion con los periodos correspondientes.' % (dt,))
        return ids
giscedata_facturacio_liquidacio_period()

class giscedata_factura(osv.osv):

    _name = 'giscedata.factura'
    _module = "giscedata_facturacio_old"
    _TIPO_FACTURA = [
      ('01', 'Normal'),
      ('02', 'Modificación de Contrato'),
      ('03', 'Baja de Contrato'),
      ('04', 'Derechos de Contratacion'),
      ('05', 'Deposito de garantía'),
      ('06', 'Inspección'),
      ('07', 'Atenciones (verificaciones, )'),
      ('08', 'Indemnizacion'),
      ('09', 'Intereses de demora'),
      ('10', 'Servicios')
    ]

    _TIPO_FACTURACION = [
      ('1','Regular (Periodo completo)'),
      ('2','Irregular (Periodo incompleto)'),
    ]

    _TIPO_RECTIFICADORA = [
      ('N','Normal'),
      ('R','Rectificadora'),
      ('A','Anuladora'),
      ('B','Anuladora con sustituyente'),
    ]

    # Funcions pels totals

    def _total_potencia(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        config_obj = self.pool.get('res.config')
        _hrd = config_obj.get(cr, uid, 'giscedata.factura.round2.date', _hardcoded_round_date)
        for id in ids:
            #Seleccionar totes les linies que son del tipus potencia
            res[id] = 0
            cr.execute("select id from giscedata_factura_linia where factura = %i and tipus = 'potencia'", (id,))
            for potencia in self.pool.get('giscedata.factura.linia').browse(cr, uid, [a[0] for a in cr.fetchall()]):
                res[id] += potencia.subtotal
                if potencia.factura.data_final < _hrd:
                    res[id] = round(res[id], 4)
                else:
                    res[id] = round(res[id], 2)
        return res

    def _total_energia(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        config_obj = self.pool.get('res.config')
        _hrd = config_obj.get(cr, uid, 'giscedata.factura.round2.date', _hardcoded_round_date)
        # Hem de separar les que són abans i després de la data d'arrodoniment
        # 1. Fer un select amb id i data_final
        # 2. Triar les que són abans i després de la data llindar
        # 3. Fer les dues consultes
        pre_ids = []
        post_ids = []
        cr.execute("select id,data_final from giscedata_factura where id in (%s)" % ','.join(map(str, map(int, ids))))
        for r in cr.fetchall():
            if r[1] < _hrd:
                pre_ids.append(r[0])
            else:
                post_ids.append(r[0])
        # Ja tenim les factures separades per la data llindar d'arrodoniment ara fem l'operació per cada una
        # Pre:
        if pre_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric*l.extra::numeric, 4)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'energia') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,pre_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        # Post:
        if post_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric*l.extra::numeric, 2)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'energia') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,post_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        return res

    def _total_reactiva(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        config_obj = self.pool.get('res.config')
        _hrd = config_obj.get(cr, uid, 'giscedata.factura.round2.date', _hardcoded_round_date)
        # Separació per l'arrodoniment dels decimals, explicat a la funció _total_energia
        pre_ids = []
        post_ids = []
        cr.execute("select id,data_final from giscedata_factura where id in (%s)" % ','.join(map(str, map(int, ids))))
        for r in cr.fetchall():
            if r[1] < _hrd:
                pre_ids.append(r[0])
            else:
                post_ids.append(r[0])
        # Ja tenim les factures separades per la data llindar d'arrodoniment ara fem l'operació per cada una
        # Pre:
        if pre_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric*l.extra::numeric, 4)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'reactiva') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,pre_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        # Post:
        if post_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric*l.extra::numeric, 2)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'reactiva') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,post_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        return res

    def _total_lloguer(self, cr, uid, ids, field_name, *args):
        res = {}
        config_obj = self.pool.get('res.config')
        _hrd = config_obj.get(cr, uid, 'giscedata.factura.round2.date', _hardcoded_round_date)
        # Separació per l'arrodoniment dels decimals, explicat a la funció _total_energia
        pre_ids = []
        post_ids = []
        cr.execute("select id,data_final from giscedata_factura where id in (%s)" % ','.join(map(str, map(int, ids))))
        for r in cr.fetchall():
            if r[1] < _hrd:
                pre_ids.append(r[0])
            else:
                post_ids.append(r[0])
        # Ja tenim les factures separades per la data llindar d'arrodoniment ara fem l'operació per cada una
        # Pre:
        if pre_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric, 4)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'lloguer') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,pre_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        # Post:
        if post_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric, 2)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'lloguer') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,post_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        return res

    def _total_exc_potencia(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        config_obj = self.pool.get('res.config')
        _hrd = config_obj.get(cr, uid, 'giscedata.factura.round2.date', _hardcoded_round_date)
        # Separació per l'arrodoniment dels decimals, explicat a la funció _total_energia
        pre_ids = []
        post_ids = []
        cr.execute("select id,data_final from giscedata_factura where id in (%s)" % ','.join(map(str, map(int, ids))))
        for r in cr.fetchall():
            if r[1] < _hrd:
                pre_ids.append(r[0])
            else:
                post_ids.append(r[0])
        # Ja tenim les factures separades per la data llindar d'arrodoniment ara fem l'operació per cada una
        # Pre:
        if pre_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric*l.extra::numeric, 4)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'exces_potencia') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,pre_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        # Post:
        if post_ids:
            cr.execute("SELECT f.id,f.data_final,coalesce(sum(round(l.preu::numeric*l.quantitat::numeric*l.extra::numeric, 2)),0.0) from giscedata_factura f left join giscedata_factura_linia l on (l.factura = f.id and l.tipus = 'exces_potencia') where f.id in (%s) group by f.id, f.data_final" % ','.join(map(str,map(int,post_ids))))
            for r in cr.fetchall():
                res[r[0]] = r[2]
        return res

    def _subtotal(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for id in ids:
            res[id] = 0
            cr.execute("select id from giscedata_factura_linia where factura = %s", (id,))
            for linia in self.pool.get('giscedata.factura.linia').browse(cr, uid, [a[0] for a in cr.fetchall()]):
                res[id] += linia.subtotal
            res[id] = round(res[id], 2)
        return res

    def _taxes(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            res[factura.id] = factura.tax.amount * factura.subtotal
            res[factura.id] = round(res[factura.id], 2)
        return res

    def _total(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            res[factura.id] = factura.taxes + factura.subtotal
            res[factura.id] = round(res[factura.id], 2)
        return res

    def _data_inici_real(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            cr.execute("select greatest(date %s, date %s)", (factura.data_inici, factura.comptador.data_alta))
            res[factura.id] = cr.fetchone()[0]
        return res

    def _data_final_real(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            if not factura.comptador.data_baixa:
                data_baixa = '9999-01-01'
            else:
                data_baixa = factura.comptador.data_baixa
            cr.execute("select least(date %s, date %s)", (factura.data_final, data_baixa))
            res[factura.id] = cr.fetchone()[0]
        return res

    def _dies_comptador(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            cr.execute("select date %s - date %s", (factura.data_final, factura.data_inici))
            cr.execute("select greatest(date %s, date %s)", (factura.data_inici, factura.comptador.data_alta))
            inici_comptador = cr.fetchone()[0]
            if not factura.comptador.data_baixa:
                data_baixa = '9999-01-01'
            else:
                data_baixa = factura.comptador.data_baixa
            cr.execute("select least(date %s, date %s)", (factura.data_final, data_baixa))
            final_comptador = cr.fetchone()[0]
            cr.execute("select date %s - date %s", (final_comptador, inici_comptador))
            res[factura.id] = cr.fetchone()[0] + 1
        return res

    def _dies_facturacio(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            cr.execute("select date %s - date %s", (factura.data_final, factura.data_inici))
            res[factura.id] = cr.fetchone()[0] + 1
        return res

    def _coficient(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            res[factura.id] = float(factura.dies_comptador)/float(factura.dies_facturacio)
        return res

    def _pricelist(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        pricelist_ver_obj = self.pool.get('product.pricelist.version')
        for factura in self.browse(cr, uid, ids, context):
            res[factura.id] = ''
            vers_ids = pricelist_ver_obj.search(cr, uid, [('pricelist_id.type', '=', 'elec'),
                                                          ('date_start', '<=', factura.data_final),
                                                          '|',
                                                          ('date_end', '>=', factura.data_final),
                                                          ('date_end', '=', False)])
            if len(vers_ids):
                res[factura.id] = pricelist_ver_obj.browse(cr, uid, vers_ids[0]).name
        return res

    def _data_boe_preus(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        pricelist = self._pricelist(cr, uid, ids, field_name, context, args)
        for p in pricelist:
            data = time.strptime(pricelist[p].split('-')[1].strip(), '%d/%m/%Y')
            data_str = time.strftime('%Y-%m-%d', data)
            res[p] = data_str
        return res

    def _linies_de(self, cr, uid, ids, field_name, arg, context={}):
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            cr.execute("select id from giscedata_factura_linia where factura = %s and tipus = %s", (factura.id, arg['tipus']))
            res[factura.id] = [a[0] for a in cr.fetchall()]
        return res

    def _account_invoice_date(self, cr, uid, ids, field_name, arg, context=None):
        """Camp funció per la data del model account.invoice.
        """
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            res[factura.id] = factura.factura.date_invoice
        return res

    def _account_invoice_date_search(self, cr, uid, obj, name, args):
        if not len(args):
            return []
        else:
            sp = [('date_invoice', a[1], a[2]) for a in args]
            s_ids = self.pool.get('account.invoice').search(cr, uid, sp)
            if not len(s_ids):
                return [('id','=','0')]
            else:
                return [('factura', 'in', s_ids)]

    def _polissa_partner(self, cr, uid, ids, field_name, arg, context=None):
        """Camp funció pel nom de l'abonat (pòlissa).
        """
        res = {}
        for factura in self.browse(cr, uid, ids, context):
            res[factura.id] = factura.polissa.partner_id.name
        return res

    def _polissa_partner_search(self, cr, uid, obj, name, args):
        if not len(args):
            return []
        else:
            sp = [('partner_id.name', a[1], a[2]) for a in args]
            s_ids = self.pool.get('giscedata.polissa').search(cr, uid, sp)
            if not len(s_ids):
                return [('id','=','0')]
            else:
                return [('polissa', 'in', s_ids)]

    def checked(self, cr, uid, ids, context={}):
        for factura in self.browse(cr, uid, ids, context):
            if factura.to_check:
                self.write(cr, uid, ids, {'to_check': 0, 'check_message': ''})

    _columns = {
      'name': fields.char('Número', size=50),
      'comptador': fields.many2one('giscedata.lectures.comptador', 'Comptador', required=True),
      'polissa': fields.many2one('giscedata.polissa', 'Polissa', required=True),
      'data_inici': fields.date('Inici'),
      'data_final': fields.date('Final'),
      'data_inici_real': fields.function(_data_inici_real, type='date', method=True, string='Inicio real'),
      'data_final_real': fields.function(_data_final_real, type='date', method=True, string='Final real'),

      'mes': fields.integer('Mes', required=True),
      'anyy': fields.integer('Any', required=True),
      'tarifa': fields.many2one('giscedata.polissa.tarifa', 'Tarifa', required=True),
      'facturacio': fields.selection([(1, 'Mensual'), (2, 'Bimestral')], 'Facturació', required=True),
      'dies_comptador': fields.function(_dies_comptador, type='integer', method=True, string='Dies comptador'),
      'dies_facturacio': fields.function(_dies_facturacio, type='integer', method=True, string='Dies Facturació'),
      'coficient': fields.function(_coficient, type='float', method=True, string='Coficient Proporcionalitat'),
      'factura': fields.many2one('account.invoice', 'Factura associada', ondelete='cascade'),
      'ref': fields.many2one('giscedata.factura', 'Factura Ref.', ondelete='restrict'),
      'linies': fields.one2many('giscedata.factura.linia', 'factura', 'Línies de factura'),
      'lectures_energia': fields.one2many('giscedata.factura.lectura.energia', 'factura', 'Lectures Energia'),
      'lectures_potencia': fields.one2many('giscedata.factura.lectura.potencia', 'factura', 'Lectura Potència'),
      'tax': fields.many2one('account.tax', 'Impost'),
      'comercialitzadora': fields.many2one('res.partner', 'Comercialitzadora/Client', required=True),
      'total_potencia': fields.function(_total_potencia, method=True, type='float', string='Total potencia', digits=(16,4)),
      'total_energia': fields.function(_total_energia, method=True, type='float', string='Total energía', digits=(16,4)),
      'total_reactiva': fields.function(_total_reactiva, method=True, type='float', string='Total reactiva', digits=(16,4)),
      'total_lloguer': fields.function(_total_lloguer, method=True, type='float', string='Total alquiler', digits=(16,4)),
      'total_exc_potencia': fields.function(_total_exc_potencia, method=True, type='float', string='Total excesos potencia', digits=(16,4)),
      'subtotal': fields.function(_subtotal, method=True, type='float', string='Subtotal', digits=(16,2)),
      'taxes': fields.function(_taxes, method=True, type='float', string='Tasas', digits=(16,2)),
      'total': fields.function(_total, method=True, type='float', string='Total', digits=(16,2)),
      'account_invoice_date': fields.function(_account_invoice_date, fnct_search=_account_invoice_date_search, method=True, type='date', string='Fecha factura asociada'),
      'polissa_partner': fields.function(_polissa_partner, fnct_search=_polissa_partner_search, method=True, type='char', size=256, string='Abonado'),
      'to_check': fields.boolean('Para comprobar', readonly=True),
      'check_message': fields.text('Mensaje', readonly=True),
      'pricelist': fields.function(_pricelist, type='char', size=255, method=True, string='Lista de precios'),
      'tipo_factura': fields.selection(_TIPO_FACTURA, 'Tipo de Factura', readonly=True),
      'tipo_facturacion': fields.selection(_TIPO_FACTURACION, 'Tipo de Facturación', readonly=True),
      'tipo_rectificadora': fields.selection(_TIPO_RECTIFICADORA, 'Rectificadora', readonly=True),
      'periode': fields.many2one('giscedata.facturacio.liquidacio.period', 'Periodo', ondelete='restrict'),
      'linies_potencia': fields.function(_linies_de, method=True, type='one2many', relation='giscedata.factura.linia', string="Línies de potència", arg = {'tipus': 'potencia'}),
      'linies_energia': fields.function(_linies_de, method=True, type='one2many', relation='giscedata.factura.linia', string="Línies d'energia", arg = {'tipus': 'energia'}),
      'linies_lloguer': fields.function(_linies_de, method=True, type='one2many', relation='giscedata.factura.linia', string="Línies de lloguer", arg = {'tipus': 'lloguer'}),
      'linies_exces': fields.function(_linies_de, method=True, type='one2many', relation='giscedata.factura.linia', string="Línies d'excés potència", arg = {'tipus': 'exces_potencia'}),
      'linies_reactiva': fields.function(_linies_de, method=True, type='one2many', relation='giscedata.factura.linia', string="Línies de reactiva", arg = {'tipus': 'reactiva'}),
      'linies_iee': fields.function(_linies_de, method=True, type='one2many', relation='giscedata.factura.linia', string="Línies d'Impost Elèctric", arg = {'tipus': 'iee'}),
      'data_boe_preus': fields.function(_data_boe_preus, method=True, type='char', string="Fecha BOE precios", size=10),
    }


    def _constraint_factura_normal(self, cr, uid, ids):
        """Constraint per les factures.

        Només hi pot haver una factura Normal per agrupació.
        Només hi pot haver una factura per tipus fent referència a una altre.
        """
        fields_to_read = ['polissa', 'anyy', 'mes', 'comptador', 'comercialitzadora',
                  'tipo_rectificadora']
        for factura in self.read(cr, uid, ids, fields_to_read):
            search_params = []
            if factura['tipo_rectificadora'] in ('R', 'A', 'B'):
                fields_to_read += ['ref']
                factura = self.read(cr, uid, [factura['id']], fields_to_read)[0]
                # Si és anuladora tan li fa si és amb substituient (B) o anuladora
                # normal (A), només n'hi pot haver una que ho faci.
                if factura['tipo_rectificadora'] in REFUND_RECTIFICATIVE_INVOICE_TYPES:
                    del factura['tipo_rectificadora']
                    search_params += [('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES)]
            del factura['id']
            for k,v in factura.items():
                if isinstance(v, type(())):
                    v = v[0]
                search_params += [(k, '=', v)]
            return not self.search_count(cr, uid, search_params) > 1

    _constraints = [(_constraint_factura_normal,
                     'Error: Ya existe una factura normal para este grupo.', '')]

    _defaults = {
          'name': lambda obj, cr, uid, context: obj.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura'),
      'to_check': lambda *a: 0,
      'check_message': lambda *a: '',
      'tipo_factura': lambda *a: '01',
      'tipo_facturacion': lambda *a: '1',
      'tipo_rectificadora': lambda *a: 'N',
    }

    def copy(self, cr, uid, id, default=None, context=None):
        if not context:
            context = {}
        context['from_copy'] = True
        if not default:
            default = {}
        default.update({
          'linies_potencia': [],
          'linies_energia': [],
          'linies_lloguer': [],
          'linies_exces': [],
          'linies_reactiva': [],
          'linies_iee': [],
        })
        id_copy = super(giscedata_factura, self).copy(cr, uid, id, default, context)
        return id_copy

    def anulacio(self, cr, uid, id, context={}):
        # S'ha d'anular la del sistema base de l'ERP per despres posar-la al camp 'factura'
        factura = self.browse(cr, uid, id)
        rid = self.pool.get('account.invoice').refund(cr, uid, [factura.factura.id])[0]
        # Fem una còpia de la factura actual
        default = {
          'name': self.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura.abonament'),
          'tipo_rectificadora': context.get('tipo_rectificadora', 'A'),
          'factura': rid,
          'ref': id,
        }
        a_id = self.copy(cr, uid, id, default, context)
        factura = self.browse(cr, uid, a_id)
        for linia in factura.linies:
            linia.write(cr, uid, [linia.id], {'quantitat': linia.quantitat * -1})

        # Modifiquem la data al dia actual i també posem el limit
        factura = self.browse(cr, uid, a_id)
        data_factura = context.get('date_invoice', time.strftime('%Y-%m-%d'))
        due_date = False
        if factura.comercialitzadora.property_payment_term:
            pt_obj = self.pool.get('account.payment.term')
            pterm_list= pt_obj.compute(cr, uid, factura.comercialitzadora.property_payment_term.id, value=1, date_ref=data_factura)

            if pterm_list:
                pterm_list = [line[0] for line in pterm_list]
                pterm_list.sort()
                due_date =  pterm_list[-1]
        # Posem dates límit i periode fiscal
        self.pool.get('account.invoice').write(cr, uid, [factura.factura.id], {'date_invoice': data_factura, 'date_due': due_date})
        return a_id

    def get_versions_preus(self, cursor, uid, tarifa, data_inici, data_final,
                             context=None):
        """Retorna les versions que s'han d'utilitzar donat un periode.
        """
        if not context:
            context = {}
        ver_obj = self.pool.get('product.pricelist.version')
        search_params = [
          ('pricelist_id.id', '=', tarifa),
          ('date_end', '>=', data_inici),
          ('date_start', '<=', data_final)
        ]
        ver_ids = ver_obj.search(cursor, uid, search_params,
                                 context={'active_test': False})
        read_fields = ['id', 'date_start', 'date_end']
        res = {}
        for versio in ver_obj.read(cursor, uid, ver_ids, read_fields, context):
            data_tall = max(data_inici, versio['date_start'])
            res[data_tall] = versio['id']
        return res

    def repartir_potencia(self, cursor, uid, ids, versions, context=None):
        """Calcula el coeficient a aplicar per la potència.
        """
        if not context:
            context = {}
        dies_periodes = dict.fromkeys(versions, 0)
        for factura in self.browse(cursor, uid, ids):
            # Si no hi ha canvi de preu (fem el coeficient normal:
            # dies_comptador * factuarcio/dies_facturacio)
            if len(versions.keys()) == 1:
                clau = versions.keys()[0]
                dies_periodes[clau] = round(factura.coficient, 2) * factura.facturacio
                return dies_periodes
            dia_iterator = datetime(*map(int, factura.data_final_real.split('-')))
            dia_stop = datetime(*map(int, factura.data_inici_real.split('-')))
            while dia_iterator >= dia_stop:
                for clau in sorted(dies_periodes.keys(), reverse=True):
                    if dia_iterator.strftime('%Y-%m-%d') >= clau:
                        break
                # Explicació: La potència es factura per 'mesos' no per dies
                # per tant han de ser períodes complets. És igual si un mes té
                # 30 dies o en té 31...
                # Suposem que el canvi de preus es produeix el dia 10/04/2011,
                # això vol dir que hem de buscar el coeficient a aplicar de cada
                # preu per cada mes. Preu 1 = 31/31 + 10/30. Preu 2 = 20/30
                dies_periodes[clau] += 1.0 / calendar.monthrange(dia_iterator.year,
                                                                 dia_iterator.month)[1]
                dia_iterator -= timedelta(days=1)
            for key, value in dies_periodes.items():
                dies_periodes[key] = round(value, 2)
            return dies_periodes

    def repartir_energia(self, cursor, uid, ids, versions, res_activa,
                         context=None):
        """Reperteix l'energia entre diferents dies.
        """
        if not context:
            context = {}
        # Repartim l'energia entre els periodes
        for factura in self.browse(cursor, uid, ids):
            if len(versions.keys()) == 1:
                return {versions.keys()[0]: res_activa}
            energia_periodes = dict.fromkeys(versions, 0)
            consum_dia = (res_activa * 1.0) / factura.dies_comptador
            dia_iterator = datetime(*map(int, factura.data_final_real.split('-')))
            dia_stop = datetime(*map(int, factura.data_inici_real.split('-')))
            while dia_iterator >= dia_stop:
                for clau in sorted(energia_periodes.keys(), reverse=True):
                    if dia_iterator.strftime('%Y-%m-%d') >= clau:
                        break
                energia_periodes[clau] += consum_dia
                dia_iterator -= timedelta(days=1)
            exponent = abs(Decimal(str(res_activa)).as_tuple()[2])
            delta = 0
            for key, value in energia_periodes.items():
                energia_periodes[key] = round(value, exponent)
                delta += energia_periodes[key]
            delta = res_activa - delta
            if delta != 0:
                energia_periodes[clau] += delta
            return energia_periodes

    def get_data_desde_hasta_linia(self, cursor, uid, ids, versions, data):
        """Busca la data inici i final per la línia de la factura.
        """
        versions_dates = sorted(versions.keys())
        for factura in self.browse(cursor, uid, ids):
            data_desde = max(data, factura.data_inici_real)
            idx = versions_dates.index(data) + 1
            if len(versions_dates) > idx:
                data_hasta = versions_dates[idx]
            else:
                data_hasta = factura.data_final_real
            return data_desde, data_hasta

    def t2_0_DHA(self, cr, uid, polissa_id, comptador_id, mes, anyy, tarifa, fact_client=False, data_factura=time.strftime('%Y-%m-%d'), tipo_rectificadora='N'):
        data = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        polissa = polissa_obj.browse(cr, uid, polissa_id)
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        partner_obj = self.pool.get('res.partner')
        comptador = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, comptador_id)
        factura_linia_obj = self.pool.get('giscedata.factura.linia')
        energia_obj = self.pool.get('giscedata.lectures.lectura')
        lectura_energia_obj = self.pool.get('giscedata.factura.lectura.energia')
        lectura_potencia_obj = self.pool.get('giscedata.factura.lectura.potencia')

        # Mirem si volem facturar al client o a la comercialitzadora,
        # per defecte sempre a la comercialitzadora

        if fact_client:
            partner = partner_obj.browse(cr, uid, polissa.partner_id.id)
        else:
            partner = partner_obj.browse(cr, uid, polissa.comercialitzadora.id)

        # Busquem la data límit:
        due_date = False
        if partner.property_payment_term:
            pt_obj = self.pool.get('account.payment.term')
            pterm_list= pt_obj.compute(cr, uid, partner.property_payment_term.id, value=1, date_ref=data_factura)

            if pterm_list:
                pterm_list = [line[0] for line in pterm_list]
                pterm_list.sort()
                due_date =  pterm_list[-1]

        # Creem la factura
        invoice_values = {
          'type': 'out_invoice',
          'reference': polissa.name,
          'partner_id': partner.id,
          'address_invoice_id': partner_obj.address_get(cr, uid, [partner.id], ['invoice'])['invoice'],
          'account_id': partner.property_account_receivable.id,
          'payment_term': partner.property_payment_term.id,
          'date_invoice': data_factura,
          'date_due': due_date,
        }
        invoice_id = invoice_obj.create(cr, uid, invoice_values)

        # Creem el nostre model de factures (report)
        # Si la facturació és bimestral la data d'inici és la d'un mes
        # enrera.
        if polissa.facturacio == 2:
            cr.execute("select to_char(date %s - interval %s, 'YYYY-MM-DD')", ('%i-%i-01' % (anyy, mes), '1 month'))
            data_inici = cr.fetchone()[0]
        else:
            data_inici = '%i-%i-01' % (anyy, mes)
        factura_values = {
          'comptador': comptador.id,
          'polissa': polissa.id,
          'data_inici': data_inici,
          'data_final': '%i-%i-%i' % (anyy, mes, calendar.monthrange(anyy, mes)[1]),
          'mes': mes,
          'anyy': anyy,
          'tarifa': polissa.tarifa.id,
          'facturacio': polissa.facturacio,
          'factura': invoice_id,
          'tax': partner.property_account_tax.id or None,
          'comercialitzadora': partner.id,
          'tipo_rectificadora': tipo_rectificadora
        }
        if tipo_rectificadora == 'R':
            factura_values['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura.rectificadora')

        factura_id = self.create(cr, uid, factura_values)
        factura = self.browse(cr, uid, factura_id)
        versions = self.get_versions_preus(cr, uid, tarifa, factura.data_inici_real,
                                           factura.data_final_real)

        ### CALCULEM ENERGIA ###

        comment = ""
        comment += '\n*** TERMES ENERGIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s", (polissa.tarifa.id, 'te'))
        periodes_ids = [a[0] for a in cr.fetchall()]

        # Assignem els coficients de la 2.0A o 2.0DHA segons els periodes
        # Els ids creats són: cof_a_20DHA_new i cof_b_20DHA_new
        # Coficients per defecte
        cofs = {
                'P1': 1,
                'P2': 0,
        }
        for periode in periode_obj.browse(cr, uid, periodes_ids):
            res_activa = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'A')
            energies_periode = self.repartir_energia(cr, uid, [factura_id], versions,
                                                     res_activa)
            for data_versio, res_activa in energies_periode.items():
                # Busquem el preu a traves de les llistes de preus
                price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa],
                                        periode.product_id.id, res_activa,
                                        context={'date': data_versio})[tarifa]
                if price:
                    # Si trobem un preu vàlid a la llista de preus fem servir aquest
                    # sino farem servir el que hi hagi a standard_price
                    periode.standard_price = price

                # Coeficients també poden canviar de preu
                if len(periodes_ids) == 2:
                    cofs_ids = {'P1': 'cof_a_20DHA_new', 'P2': 'cof_b_20DHA_new'}
                    for key, value in cofs_ids.items():
                        p_cof_ids = self.pool.get('ir.model.data').search(cr, uid,
                                                    [('module', '=', 'giscedata_lectures'),
                                                     ('name', '=', value)])
                        p_cof_id = self.pool.get('ir.model.data').read(cr, uid, p_cof_ids,
                                                                       ['res_id'])[0]
                        cofs[key] = self.pool.get('product.pricelist').price_get(cr, uid,
                                        [tarifa], p_cof_id['res_id'], 1,
                                        context={'date': data_versio})[tarifa]

                # Creem la línia de la factura (account)
                invoice_line_values = {}
                invoice_line_values = {
                  'name': periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': res_activa*cofs[periode.name],
                  'price_unit': periode.standard_price,
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                         [factura_id],
                                                                         versions,
                                                                         data_versio)
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'energia',
                  'extra': cofs[periode.name],
                  'quantitat': res_activa,
                  'preu': periode.standard_price,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)
                comment += '%s (%s - %s): %f kWh * %f * %f €/kWh = %f €\n' % (periode.name, data_desde, data_hasta, res_activa, cofs[periode.name], periode.standard_price, res_activa*periode.standard_price)

            # Guardem la lecura actual
            lectura_energia_vals = {}
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
            res = cr.dictfetchone()
            if res:
                # Si el comptador té data de baixa i la data de lectura és més gran
                # que la data de baixa, posarem la data de baixa a la lectura
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'activa',
                  'hora_actual': res['hora'],
                  'lect_actual': res['lectura'],
                  'data_actual': res['name']
                }
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
            res = cr.dictfetchone()
            if not res or not len(res):
                res = {'lectura': 0, 'name': comptador.data_alta, 'hora': '12:00:00'}
            # Si la data de la lectura és més petita que la data d'alta del comptador
            # assignarem la data d'alta a la lectura
            if res['name'] < comptador.data_alta:
                res['name'] = comptador.data_alta
            lectura_energia_vals['lect_anterior'] = res['lectura']
            lectura_energia_vals['data_anterior'] = res['name']
            lectura_energia_vals['hora_anterior'] = res['hora']
            lectura_energia_obj.create(cr, uid, lectura_energia_vals)

            if periode.efecte_reactiva:
                # Calculem la lectura
                res_reactiva = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'R')

                exces = res_reactiva - (res_activa * 0.5)
                if exces > 0:
                    cosfi = res_activa / (math.sqrt(math.pow(res_activa, 2) + math.pow(res_reactiva, 2)))
                    cosfi = round(cosfi, 2)
                    # Busquem el preu pel cosfi que ens surti
                    cosfi_obj = self.pool.get('giscedata.lectures.cosfi')
                    results = cosfi_obj.search(cr, uid, [('min', '<=', cosfi), ('max', '>', cosfi)])
                    reactiva_periode = self.repartir_energia(cr, uid, [factura_id],
                                                             versions, exces)
                    for data_versio, exces in reactiva_periode.items():
                        cosfi_price = self.pool.get('product.pricelist').price_get(cr, uid,
                              [tarifa], cosfi_obj.browse(cr, uid, results[0]).product_id.id,
                              cosfi, context={'date': data_versio})[tarifa]

                        # Creem la línia de factura (account)
                        invoice_line_values = {}
                        invoice_line_values = {
                          'name': '%s (r)' % periode.name,
                          'product_id': periode.product_id.id,
                          'quantity': exces,
                          'price_unit': cosfi_price,
                          'invoice_id': invoice_id,
                          'account_id': periode.property_account_income.id,
                          'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                        }
                        invoice_line_obj.create(cr, uid, invoice_line_values)

                        # Creem la línia de la factura (report)
                        data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                             [factura_id],
                                                                             versions,
                                                                             data_versio)
                        factura_linia_values = {
                          'name': periode.name,
                          'factura': factura_id,
                          'tipus': 'reactiva',
                          'quantitat': exces,
                          'preu': cosfi_price,
                          'data_desde': data_desde,
                          'data_hasta': data_hasta
                        }
                        factura_linia_obj.create(cr, uid, factura_linia_values)

                        comment += '%s(r) (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, exces, cosfi_price, exces*cosfi_price)

            # Guardem la lecura actual (reactiva)
            lectura_actual_reactiva = False
            lectura_energia_vals = {}
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
            res = cr.dictfetchone()
            if res:
                lectura_actual_reactiva = True
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'reactiva',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora']
                }
            lectura_anterior_reactiva = False
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
            res = cr.dictfetchone()
            if res:
                lectura_anterior_reactiva = True
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
            if lectura_actual_reactiva and lectura_anterior_reactiva:
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)

        ### CALCULEM POTENCIA ###
        comment += '\n*** TERMES DE POTÈNCIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s", (polissa.tarifa.id, 'tp'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            cr.execute("select potencia from giscedata_polissa_potencia_contractada_periode where periode_id = %i and polissa_id = %i", (periode.id, polissa.id))
            res = cr.fetchone()
            if not res:
                res = [0]

            pot_contract = res[0]
            pot_maximetre = 0
            if polissa.facturacio_potencia == 'max':
                # si la polissa te activada la facturació per maximetre s'ha d'aplicar la
                # regla del maximetre
                cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periode.id))
                potencia = cr.fetchone()
                pot_maximetre = potencia[0]
                if not pot_contract:
                    interval = 0
                else:
                    interval = (potencia[0]/pot_contract)*100
                if interval > 105:
                    nova_pot = potencia[0] + 2 * (potencia[0] - 1.05*pot_contract)
                elif interval >= 85 and interval <= 105:
                    nova_pot = potencia[0]
                elif interval < 85:
                    nova_pot = pot_contract * 0.85

                res = (nova_pot,)

            # Nou sistema per recàrrec d'ICP (20kW)
            if polissa.facturacio_potencia == 'rec':
                if pot_contract <= 5:
                    res = (10,)
                else:
                    res = (20,)
            # Si la quantitat és més gran que 0
            if res[0] > 0:
                # Busquem el preu a traves de les llistes de preus
                data_pepote = self.repartir_potencia(cr, uid, [factura_id], versions)
                for data_versio, pepote in data_pepote.items():
                    price = self.pool.get('product.pricelist').price_get(cr, uid,
                                                  [tarifa], periode.product_id.id, res[0],
                                                  context={'date': data_versio})[tarifa]
                    if price:
                        # Si trobem un preu vàlid a la llista de preus fem servir aquest
                        # sino farem servir el que hi hagi a standard_price
                        periode.standard_price = price

                    # Creem la línia de la factura (account)
                    invoice_line_values = {}
                    invoice_line_values = {
                      'name': periode.name,
                      'product_id': periode.product_id.id,
                      'quantity': res[0],
                      'price_unit': (periode.standard_price*pepote/12),
                      'invoice_id': invoice_id,
                      'account_id': periode.property_account_income.id,
                      'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                    }
                    invoice_line_obj.create(cr, uid, invoice_line_values)

                    # Creem la línia de la factura (report)
                    data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                           [factura_id],
                                                                           versions,
                                                                           data_versio)
                    factura_linia_values = {
                      'name': periode.name,
                      'factura': factura_id,
                      'tipus': 'potencia',
                      'quantitat': res[0],
                      'preu': periode.standard_price,
                      'extra': pepote,
                      'data_desde': data_desde,
                      'data_hasta': data_hasta
                    }
                    factura_linia_obj.create(cr, uid, factura_linia_values)

                    comment += '%s (%s - %s): %f kW * %f €/kW * %f/12 año = %f €\n' % (periode.name, data_desde, data_hasta, res[0], periode.standard_price, pepote, res[0]*periode.standard_price*pepote/12)



                # Guardem les lectures de potencia
                lectura_potencia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'pot_contract': pot_contract,
                  'exces': 0,
                  'pot_maximetre': pot_maximetre
                }
                lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)

        data['comment'] = comment
        # Calculem el lloguer
        invoice_obj.write(cr, uid, [invoice_id], {'comment': data['comment']})
        self.lloguer(cr, uid, factura_id, tarifa, data_factura, data)
        #invoice_obj.button_compute(cr, uid, [invoice_id], {}, True)
        #wf_service = netsvc.LocalService("workflow")
        #wf_service.trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        return factura_id


    def t2_1_DHA(self, cr, uid, polissa_id, comptador_id, mes, anyy, tarifa, fact_client=False, data_factura=time.strftime('%Y-%m-%d'), tipo_rectificadora='N'):
        data = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        polissa = polissa_obj.browse(cr, uid, polissa_id)
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        partner_obj = self.pool.get('res.partner')
        comptador = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, comptador_id)
        factura_linia_obj = self.pool.get('giscedata.factura.linia')
        energia_obj = self.pool.get('giscedata.lectures.lectura')
        lectura_energia_obj = self.pool.get('giscedata.factura.lectura.energia')
        lectura_potencia_obj = self.pool.get('giscedata.factura.lectura.potencia')

        # Mirem si volem facturar al client o a la comercialitzadora,
        # per defecte sempre a la comercialitzadora

        if fact_client:
            partner = partner_obj.browse(cr, uid, polissa.partner_id.id)
        else:
            partner = partner_obj.browse(cr, uid, polissa.comercialitzadora.id)

        # Busquem la data límit:
        due_date = False
        if partner.property_payment_term:
            pt_obj = self.pool.get('account.payment.term')
            pterm_list= pt_obj.compute(cr, uid, partner.property_payment_term.id, value=1, date_ref=data_factura)

            if pterm_list:
                pterm_list = [line[0] for line in pterm_list]
                pterm_list.sort()
                due_date =  pterm_list[-1]

        # Creem la factura
        invoice_values = {
          'type': 'out_invoice',
          'reference': polissa.name,
          'partner_id': partner.id,
          'address_invoice_id': partner_obj.address_get(cr, uid, [partner.id], ['invoice'])['invoice'],
          'account_id': partner.property_account_receivable.id,
          'payment_term': partner.property_payment_term.id,
          'date_invoice': data_factura,
          'date_due': due_date,
        }
        invoice_id = invoice_obj.create(cr, uid, invoice_values)

        # Creem el nostre model de factures (report)
        # Si la facturació és bimestral la data d'inici és la d'un mes
        # enrera.
        if polissa.facturacio == 2:
            cr.execute("select to_char(date %s - interval %s, 'YYYY-MM-DD')", ('%i-%i-01' % (anyy, mes), '1 month'))
            data_inici = cr.fetchone()[0]
        else:
            data_inici = '%i-%i-01' % (anyy, mes)
        factura_values = {
          'comptador': comptador.id,
          'polissa': polissa.id,
          'data_inici': data_inici,
          'data_final': '%i-%i-%i' % (anyy, mes, calendar.monthrange(anyy, mes)[1]),
          'mes': mes,
          'anyy': anyy,
          'tarifa': polissa.tarifa.id,
          'facturacio': polissa.facturacio,
          'factura': invoice_id,
          'tax': partner.property_account_tax.id or None,
          'comercialitzadora': partner.id,
          'tipo_rectificadora': tipo_rectificadora
        }
        if tipo_rectificadora == 'R':
            factura_values['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura.rectificadora')

        factura_id = self.create(cr, uid, factura_values)
        factura = self.browse(cr, uid, factura_id)
        versions = self.get_versions_preus(cr, uid, tarifa, factura.data_inici_real,
                                           factura.data_final_real)

        ### CALCULEM ENERGIA ###

        comment = ""
        comment += '\n*** TERMES ENERGIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s", (polissa.tarifa.id, 'te'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            res_activa = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'A')

            # Si la quantitat és més gran que 0
            energies_periode = self.repartir_energia(cr, uid, [factura_id], versions,
                                                     res_activa)
            for data_versio, res_activa in energies_periode.items():
                # Busquem el preu a traves de les llistes de preus
                price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, res_activa, context={'date': data_versio})[tarifa]
                if price:
                    # Si trobem un preu vàlid a la llista de preus fem servir aquest
                    # sino farem servir el que hi hagi a standard_price
                    periode.standard_price = price

                # Creem la línia de la factura (account)
                invoice_line_values = {}
                invoice_line_values = {
                  'name': periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': res_activa,
                  'price_unit': periode.standard_price,
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                         [factura_id],
                                                                         versions,
                                                                         data_versio)
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'energia',
                  'quantitat': res_activa,
                  'preu': periode.standard_price,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)
                comment += '%s (%s - %s): %f kWh * %f €/kWh = %f €\n' % (periode.name, data_desde, data_hasta, res_activa, periode.standard_price, res_activa*periode.standard_price)

            # Guardem la lecura actual
            lectura_energia_vals = {}
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
            res = cr.dictfetchone()
            if res:
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'activa',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora'],
                }
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
            res = cr.dictfetchone()
            if not res or not len(res):
                res = {'lectura': 0, 'name': comptador.data_alta, 'hora': '12:00:00'}
            if res['name'] < comptador.data_alta:
                res['name'] = comptador.data_alta
            lectura_energia_vals['lect_anterior'] = res['lectura']
            lectura_energia_vals['data_anterior'] = res['name']
            lectura_energia_vals['hora_anterior'] = res['hora']
            lectura_energia_obj.create(cr, uid, lectura_energia_vals)

            if periode.efecte_reactiva:
                # Calculem la lectura
                res_reactiva = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'R')

                exces = res_reactiva - (res_activa * 0.5)
                if exces > 0:
                    cosfi = res_activa / (math.sqrt(math.pow(res_activa, 2) + math.pow(res_reactiva, 2)))
                    cosfi = round(cosfi, 2)
                    # Busquem el preu pel cosfi que ens surti
                    cosfi_obj = self.pool.get('giscedata.lectures.cosfi')
                    results = cosfi_obj.search(cr, uid, [('min', '<=', cosfi), ('max', '>', cosfi)])
                    reactiva_periode = self.repartir_energia(cr, uid, [factura_id],
                                                             versions, exces)
                    for data_versio, exces in reactiva_periode.items():
                        cosfi_price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], cosfi_obj.browse(cr, uid, results[0]).product_id.id, cosfi, context={'date': data_versio})[tarifa]

                        # Creem la línia de factura (account)
                        invoice_line_values = {}
                        invoice_line_values = {
                          'name': '%s (r)' % periode.name,
                          'product_id': periode.product_id.id,
                          'quantity': exces,
                          'price_unit': cosfi_price,
                          'invoice_id': invoice_id,
                          'account_id': periode.property_account_income.id,
                          'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                        }
                        invoice_line_obj.create(cr, uid, invoice_line_values)

                        # Creem la línia de la factura (report)
                        data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                             [factura_id],
                                                                             versions,
                                                                             data_versio)
                        factura_linia_values = {
                          'name': periode.name,
                          'factura': factura_id,
                          'tipus': 'reactiva',
                          'quantitat': exces,
                          'preu': cosfi_price,
                          'data_desde': data_desde,
                          'data_hasta': data_hasta
                        }
                        factura_linia_obj.create(cr, uid, factura_linia_values)

                        comment += '%s(r) (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, exces, cosfi_price, exces*cosfi_price)

            # Guardem la lecura actual (reactiva)
            lectura_energia_vals = {}
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
            res = cr.dictfetchone()
            if not res or not len(res):
                res = {'lectura': 0, 'name': comptador.data_baixa, 'hora': '12:00:00'}
            if comptador.data_baixa and res['name'] > comptador.data_baixa:
                res['name'] = comptador.data_baixa
            lectura_energia_vals = {
              'name': periode.name,
              'factura': factura_id,
              'tipus': 'reactiva',
              'lect_actual': res['lectura'],
              'data_actual': res['name'],
              'hora_actual': res['hora'],
            }
            cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
            res = cr.dictfetchone()
            if not res or not len(res):
                res = {'lectura': 0, 'name': comptador.data_alta, 'hora': '12:00:00'}
            if res['name'] < comptador.data_alta:
                res['name'] = comptador.data_alta
            lectura_energia_vals['lect_anterior'] = res['lectura']
            lectura_energia_vals['data_anterior'] = res['name']
            lectura_energia_vals['hora_anterior'] = res['hora']
            lectura_energia_obj.create(cr, uid, lectura_energia_vals)



        ### CALCULEM POTENCIA ###
        comment += '\n*** TERMES DE POTÈNCIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s", (polissa.tarifa.id, 'tp'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            cr.execute("select potencia from giscedata_polissa_potencia_contractada_periode where periode_id = %i and polissa_id = %i", (periode.id, polissa.id))
            res = cr.fetchone()

            pot_contract = res[0]
            pot_maximetre = 0
            if polissa.facturacio_potencia == 'max':
                # si la polissa te activada la facturació per maximetre s'ha d'aplicar la
                # regla del maximetre
                cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periode.id))
                potencia = cr.fetchone()
                pot_maximetre = potencia[0]
                if not pot_contract:
                    interval = 0
                else:
                    interval = (potencia[0]/pot_contract)*100
                if interval > 105:
                    nova_pot = potencia[0] + 2 * (potencia[0] - 1.05*pot_contract)
                elif interval >= 85 and interval <= 105:
                    nova_pot = potencia[0]
                elif interval < 85:
                    nova_pot = pot_contract * 0.85

                res = (nova_pot,)

            # Nou sistema recàrrec ICP (20kW)
            if polissa.facturacio_potencia == 'rec':
                if pot_contract <= 5:
                    res = (10,)
                else:
                    res = (20,)
            # Si la quantitat és més gran que 0
            if res[0] > 0:
                # Busquem el preu a traves de les llistes de preus
                data_pepote = self.repartir_potencia(cr, uid, [factura_id], versions)
                for data_versio, pepote in data_pepote.items():
                    price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, res[0], context={'date': data_versio})[tarifa]
                    if price:
                        # Si trobem un preu vàlid a la llista de preus fem servir aquest
                        # sino farem servir el que hi hagi a standard_price
                        periode.standard_price = price

                    # Creem la línia de la factura (account)
                    invoice_line_values = {}
                    invoice_line_values = {
                      'name': periode.name,
                      'product_id': periode.product_id.id,
                      'quantity': res[0],
                      'price_unit': (periode.standard_price*pepote/12),
                      'invoice_id': invoice_id,
                      'account_id': periode.property_account_income.id,
                      'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                    }
                    invoice_line_obj.create(cr, uid, invoice_line_values)

                    # Creem la línia de la factura (report)
                    data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                           [factura_id],
                                                                           versions,
                                                                           data_versio)
                    factura_linia_values = {
                      'name': periode.name,
                      'factura': factura_id,
                      'tipus': 'potencia',
                      'quantitat': res[0],
                      'preu': periode.standard_price,
                      'extra': pepote,
                      'data_desde': data_desde,
                      'data_hasta': data_hasta
                    }
                    factura_linia_obj.create(cr, uid, factura_linia_values)

                    comment += '%s (%s - %s): %f kW * %f €/kW * %f/12 año = %f €\n' % (periode.name, data_desde, data_hasta, res[0], periode.standard_price, pepote, res[0]*periode.standard_price*pepote/12)

                    data['comment'] = comment

                    # Guardem les lectures de potencia
                    lectura_potencia_vals = {
                      'name': periode.name,
                      'factura': factura_id,
                      'pot_contract': pot_contract,
                      'exces': 0,
                      'pot_maximetre': pot_maximetre
                    }
                    lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)

        # Calculem el lloguer
        invoice_obj.write(cr, uid, [invoice_id], {'comment': data['comment']})
        self.lloguer(cr, uid, factura_id, tarifa, data_factura, data)
        #invoice_obj.button_compute(cr, uid, [invoice_id], {}, True)
        #wf_service = netsvc.LocalService("workflow")
        #wf_service.trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
        return factura_id

    def t3_1_A(self, cr, uid, polissa_id, comptador_id, mes, anyy, tarifa, fact_client=False, data_factura=time.strftime('%Y-%m-%d'), tipo_rectificadora='N'):
        data = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        polissa = polissa_obj.browse(cr, uid, polissa_id)
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        partner_obj = self.pool.get('res.partner')
        comptador = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, comptador_id)
        factura_linia_obj = self.pool.get('giscedata.factura.linia')
        energia_obj = self.pool.get('giscedata.lectures.lectura')
        lectura_energia_obj = self.pool.get('giscedata.factura.lectura.energia')
        lectura_potencia_obj = self.pool.get('giscedata.factura.lectura.potencia')

        # Mirem si volem facturar al client o a la comercialitzadora,
        # per defecte sempre a la comercialitzadora

        if fact_client:
            partner = partner_obj.browse(cr, uid, polissa.partner_id.id)
        else:
            partner = partner_obj.browse(cr, uid, polissa.comercialitzadora.id)

        # Busquem la data límit:
        due_date = False
        if partner.property_payment_term:
            pt_obj = self.pool.get('account.payment.term')
            pterm_list= pt_obj.compute(cr, uid, partner.property_payment_term.id, value=1, date_ref=data_factura)

            if pterm_list:
                pterm_list = [line[0] for line in pterm_list]
                pterm_list.sort()
                due_date =  pterm_list[-1]

        # Creem la factura (account)
        invoice_values = {
          'type': 'out_invoice',
          'reference': polissa.name,
          'partner_id': partner.id,
          'address_invoice_id': partner_obj.address_get(cr, uid, [partner.id], ['invoice'])['invoice'],
          'account_id': partner.property_account_receivable.id,
          'payment_term': partner.property_payment_term.id,
          'date_invoice': data_factura,
          'date_due': due_date
        }
        invoice_id = invoice_obj.create(cr, uid, invoice_values)

        # Creem el nostre model de factures (report)
        # Si la facturació és bimestral la data d'inici és la d'un mes
        # enrera.
        if polissa.facturacio == 2:
            cr.execute("select to_char(date %s - interval %s, 'YYYY-MM-DD')", ('%i-%i-01' % (anyy, mes), '1 month'))
            data_inici = cr.fetchone()[0]
        else:
            data_inici = '%i-%i-01' % (anyy, mes)
        factura_values = {
          'comptador': comptador.id,
          'polissa': polissa.id,
          'data_inici': data_inici,
          'data_final': '%i-%i-%i' % (anyy, mes, calendar.monthrange(anyy, mes)[1]),
          'mes': mes,
          'anyy': anyy,
          'tarifa': polissa.tarifa.id,
          'facturacio': polissa.facturacio,
          'factura': invoice_id,
          'tax': partner.property_account_tax.id or None,
          'comercialitzadora': partner.id,
          'tipo_rectificadora': tipo_rectificadora
        }
        if tipo_rectificadora == 'R':
            factura_values['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura.rectificadora')

        factura_id = self.create(cr, uid, factura_values)
        factura = self.browse(cr, uid, factura_id)
        versions = self.get_versions_preus(cr, uid, tarifa, factura.data_inici_real,
                                           factura.data_final_real)

        ### CALCULEM L'ENERGIA ###

        comment = ""
        comment += '\n*** TERMES ENERGIA ***\n'

        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s and agrupat_amb is null", (polissa.tarifa.id, 'te'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            res_activa = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'A')

            # Sumem l'energia dels periodes agrupats amb aquest
            for periodeagrupat in periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)]):
                res_activa += energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periodeagrupat, comptador.id, polissa.facturacio, 'A')

            energies_periode = self.repartir_energia(cr, uid, [factura_id], versions,
                                                     res_activa)
            for data_versio, res_activa in energies_periode.items():
                # Busquem el preu a traves de les llistes de preus
                price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, res_activa, context={'date': data_versio})[tarifa]
                if price:
                    # Si trobem un preu vàlid a la llista de preus fem servir aquest
                    # sino farem servir el que hi hagi a standard_price
                    periode.standard_price = price

                # Creem la línia de la factura (account)
                invoice_line_values = {}
                invoice_line_values = {
                  'name': periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': res_activa,
                  'price_unit': periode.standard_price,
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                         [factura_id],
                                                                         versions,
                                                                         data_versio)
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'energia',
                  'quantitat': res_activa,
                  'preu': periode.standard_price,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)
                comment += '%s (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, res_activa, periode.standard_price, res_activa*periode.standard_price)

                # Guardem la lecura actual (activa)
                lectura_energia_vals = {}
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
                res = cr.dictfetchone()
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'activa',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora'],
                }
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
                res = cr.dictfetchone()
                if not res or not len(res):
                    res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)

                # Posem la lectura dels periodes agrupats
                for periodeagrupat in periode.browse(cr, uid, periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)])):
                    lectura_energia_vals = {}
                    cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periodeagrupat.id))
                    res = cr.dictfetchone()
                    if res:
                        if comptador.data_baixa and res['name'] > comptador.data_baixa:
                            res['name'] = comptador.data_baixa
                        lectura_energia_vals = {
                          'name': periodeagrupat.name,
                          'factura': factura_id,
                          'tipus': 'activa',
                          'lect_actual': res['lectura'],
                          'data_actual': res['name'],
                          'hora_actual': res['hora']
                        }
                        cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periodeagrupat.id))
                        res = cr.dictfetchone()
                        if not res or not len(res):
                            res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                        if res['name'] < comptador.data_alta:
                            res['name'] = comptador.data_alta
                        lectura_energia_vals['lect_anterior'] = res['lectura']
                        lectura_energia_vals['data_anterior'] = res['name']
                        lectura_energia_vals['hora_anterior'] = res['hora']
                        lectura_energia_obj.create(cr, uid, lectura_energia_vals)


                if periode.efecte_reactiva:
                    # Agafem el marge de reactiva que ens toca
                    cr.execute("select name from giscedata_lectures_mreactiva where data_inici <= %s and data_final > %s", ('%s-%s-01' % (anyy, mes), '%s-%s-01' % (anyy, mes)))
                    marge = cr.fetchone()[0]

                    # Calculem la lectura
                    res_reactiva = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'R')

                    # Sumem l'energia dels periodes agrupats amb aquest
                    for periodeagrupat in periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)]):
                        res_reactiva += energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periodeagrupat, comptador.id, polissa.facturacio, 'R')

                    exces = res_reactiva - (res_activa * marge)
                    if exces > 0:
                        cosfi = res_activa / (math.sqrt(math.pow(res_activa, 2) + math.pow(res_reactiva, 2)))
                        cosfi = round(cosfi, 2)
                        # Busquem el preu pel cosfi que ens surti
                        cosfi_obj = self.pool.get('giscedata.lectures.cosfi')
                        results = cosfi_obj.search(cr, uid, [('min', '<=', cosfi), ('max', '>', cosfi)])
                        reactiva_periode = self.repartir_energia(cr, uid, [factura_id],
                                                               versions, exces)
                        for data_versio, exces in reactiva_periode.items():
                            cosfi_price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], cosfi_obj.browse(cr, uid, results[0]).product_id.id, cosfi, context={'date': data_versio})[tarifa]

                            # Creem la línia de factura (account)
                            invoice_line_values = {}
                            invoice_line_values = {
                              'name': '%s (r)' % periode.name,
                              'product_id': periode.product_id.id,
                              'quantity': exces,
                              'price_unit': cosfi_price,
                              'invoice_id': invoice_id,
                              'account_id': periode.property_account_income.id,
                              'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                            }
                            invoice_line_obj.create(cr, uid, invoice_line_values)

                            # Creem la línia de la factura (report)
                            data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                               [factura_id],
                                                                               versions,
                                                                               data_versio)
                            factura_linia_values = {
                              'name': periode.name,
                              'factura': factura_id,
                              'tipus': 'reactiva',
                              'quantitat': exces,
                              'preu': cosfi_price,
                              'data_desde': data_desde,
                              'data_hasta': data_hasta
                            }
                            factura_linia_obj.create(cr, uid, factura_linia_values)

                            comment += '%s(r) (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, exces, cosfi_price, exces*cosfi_price)

                # Guardem la lecura actual (reactiva)
                lectura_energia_vals = {}
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
                res = cr.dictfetchone()
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'reactiva',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora'],
                }
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
                res = cr.dictfetchone()
                if not res or not len(res):
                    res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)

                # Posem la lectura dels periodes agrupats
                for periodeagrupat in periode.browse(cr, uid, periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)])):
                    lectura_energia_vals = {}
                    cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periodeagrupat.id))
                    res = cr.dictfetchone()
                    if res:
                        if comptador.data_baixa and res['name'] > comptador.data_baixa:
                            res['name'] = comptador.data_baixa
                        lectura_energia_vals = {
                          'name': periodeagrupat.name,
                          'factura': factura_id,
                          'tipus': 'reactiva',
                          'lect_actual': res['lectura'],
                          'data_actual': res['name'],
                          'hora_actual': res['hora'],
                        }
                        cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periodeagrupat.id))
                        res = cr.dictfetchone()
                        if not res or not len(res):
                            res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                        if res['name'] < comptador.data_alta:
                            res['name'] = comptador.data_alta
                        lectura_energia_vals['lect_anterior'] = res['lectura']
                        lectura_energia_vals['data_anterior'] = res['name']
                        lectura_energia_vals['hora_anterior'] = res['hora']
                        lectura_energia_obj.create(cr, uid, lectura_energia_vals)

        ### CALCULEM LA POTENCIA ###
        comment += '\n*** TERMES DE POTÈNCIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s and agrupat_amb is null", (polissa.tarifa.id, 'tp'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            cr.execute("select potencia from giscedata_polissa_potencia_contractada_periode where periode_id = %i and polissa_id = %i", (periode.id, polissa.id))
            pot_contract = cr.fetchone()[0]
            cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periode.id))
            potencia = cr.fetchone()
            # Busquem el maxímetre agrupat (6 Periodes)
            for periodeagrupat in periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)]):
                cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periodeagrupat))
                pot_agr = cr.fetchone()
                potencia = (max(potencia[0], pot_agr[0]),)
            if not potencia[0]:
                potencia = (0,)
            # Comprovar si tenim exces de potencia o no
            if not pot_contract:
                interval = 0
            else:
                interval = (potencia[0]/pot_contract)*100
            if interval > 105:
                nova_pot = potencia[0] + 2 * (potencia[0] - 1.05*pot_contract)
            elif interval >= 85 and interval <= 105:
                nova_pot = potencia[0]
            elif interval < 85:
                nova_pot = pot_contract * 0.85

            data_pepote = self.repartir_potencia(cr, uid, [factura_id], versions)
            for data_versio, pepote in data_pepote.items():
                # Busquem el preu a traves de les llistes de preus
                price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, nova_pot, context={'date': data_versio})[tarifa]
                if price:
                    # Si trobem un preu vàlid a la llista de preus fem servir aquest
                    # sino farem servir el que hi hagi a standard_price
                    periode.standard_price = price

                # Creem la linia de la factura (account)
                invoice_line_values = {}
                invoice_line_values = {
                  'name': periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': nova_pot,
                  'price_unit': (periode.standard_price*pepote/12),
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                         [factura_id],
                                                                         versions,
                                                                         data_versio)
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'potencia',
                  'quantitat': nova_pot,
                  'preu': periode.standard_price,
                  'extra': pepote,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)

                comment += '%s (%s - %s): %f * %f * %f/12 = %f\n' % (periode.name, data_desde, data_hasta, nova_pot, periode.standard_price, polissa.facturacio, nova_pot*periode.standard_price*pepote/12)

                data['comment'] = comment

            # Guardem les lectures de potencia
            exces = potencia[0] - pot_contract
            if exces < 0:
                exces = 0
            lectura_potencia_vals = {
              'name': periode.name,
              'factura': factura_id,
              'pot_contract': pot_contract,
              'pot_maximetre': potencia[0],
              'exces': exces,
            }
            lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)
            # Posem la lectura dels periodes agrupats
            for periodeagrupat in periode.browse(cr, uid, periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)])):
                cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periodeagrupat.id))
                potencia = cr.fetchone()
                if potencia:
                    lectura_potencia_vals = {
                      'name': periodeagrupat.name,
                      'factura': factura_id,
                      'pot_contract': pot_contract,
                      'pot_maximetre': potencia[0],
                    }
                    lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)

        # Calculem el lloguer
        invoice_obj.write(cr, uid, [invoice_id], {'comment': data['comment']})
        self.lloguer(cr, uid, factura_id, tarifa, data_factura, data)

        return factura_id

    def t3_1_A_LB(self, cr, uid, polissa_id, comptador_id, mes, anyy, tarifa, fact_client=False, data_factura=time.strftime('%Y-%m-%d'), tipo_rectificadora='N'):
        data = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        polissa = polissa_obj.browse(cr, uid, polissa_id)
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        partner_obj = self.pool.get('res.partner')
        comptador = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, comptador_id)
        factura_linia_obj = self.pool.get('giscedata.factura.linia')
        energia_obj = self.pool.get('giscedata.lectures.lectura')
        lectura_energia_obj = self.pool.get('giscedata.factura.lectura.energia')
        lectura_potencia_obj = self.pool.get('giscedata.factura.lectura.potencia')

        # Mirem si volem facturar al client o a la comercialitzadora,
        # per defecte sempre a la comercialitzadora

        if fact_client:
            partner = partner_obj.browse(cr, uid, polissa.partner_id.id)
        else:
            partner = partner_obj.browse(cr, uid, polissa.comercialitzadora.id)

        # Busquem la data límit:
        due_date = False
        if partner.property_payment_term:
            pt_obj = self.pool.get('account.payment.term')
            pterm_list= pt_obj.compute(cr, uid, partner.property_payment_term.id, value=1, date_ref=data_factura)

            if pterm_list:
                pterm_list = [line[0] for line in pterm_list]
                pterm_list.sort()
                due_date =  pterm_list[-1]

        # Creem la factura (account)
        invoice_values = {
          'type': 'out_invoice',
          'reference': polissa.name,
          'partner_id': partner.id,
          'address_invoice_id': partner_obj.address_get(cr, uid, [partner.id], ['invoice'])['invoice'],
          'account_id': partner.property_account_receivable.id,
          'payment_term': partner.property_payment_term.id,
          'date_invoice': data_factura,
          'date_due': due_date
        }
        invoice_id = invoice_obj.create(cr, uid, invoice_values)

        # Creem el nostre model de factures (report)
        # Si la facturació és bimestral la data d'inici és la d'un mes
        # enrera.
        if polissa.facturacio == 2:
            cr.execute("select to_char(date %s - interval %s, 'YYYY-MM-DD')", ('%i-%i-01' % (anyy, mes), '1 month'))
            data_inici = cr.fetchone()[0]
        else:
            data_inici = '%i-%i-01' % (anyy, mes)
        factura_values = {
          'comptador': comptador.id,
          'polissa': polissa.id,
          'data_inici': data_inici,
          'data_final': '%i-%i-%i' % (anyy, mes, calendar.monthrange(anyy, mes)[1]),
          'mes': mes,
          'anyy': anyy,
          'tarifa': polissa.tarifa.id,
          'facturacio': polissa.facturacio,
          'factura': invoice_id,
          'tax': partner.property_account_tax.id or None,
          'comercialitzadora': partner.id,
          'tipo_rectificadora': tipo_rectificadora
        }
        if tipo_rectificadora == 'R':
            factura_values['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura.rectificadora')

        factura_id = self.create(cr, uid, factura_values)
        factura = self.browse(cr, uid, factura_id)
        versions = self.get_versions_preus(cr, uid, tarifa, factura.data_inici_real,
                                           factura.data_final_real)

        ### CALCULEM L'ENERGIA ###

        # Calculem els dies laborables i els dies festius del mes que anem a facturar
        cr.execute("select name from giscedata_lectures_perfils_dfestius")
        d_festius = [a[0] for a in cr.fetchall()]
        laborables = 0
        festius = 0

        for dia in range(1,calendar.monthrange(anyy, mes)[1]+1):
            if calendar.weekday(anyy, mes, dia) in (5,6) or '%s-%s-%s' % (anyy, str(mes).zfill(2), str(dia).zfill(2)) in d_festius:
                festius += 1
            else:
                laborables += 1

        dict_dies = {
          'P1': 6 * laborables,
          'P2': 10 * laborables + 6 * festius,
          'P3': 8 * laborables + 18 * festius
        }

        comment = ""
        comment += '\n*** TERMES ENERGIA ***\n'

        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s and agrupat_amb is null", (polissa.tarifa.id, 'te'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            res_activa = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'A')

            # Sumem l'energia dels periodes agrupats amb aquest
            for periodeagrupat in periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)]):
                res_activa += energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periodeagrupat, comptador.id, polissa.facturacio, 'A')

            energies_periode = self.repartir_energia(cr, uid, [factura_id], versions,
                                                     res_activa)
            for data_versio, res_activa in energies_periode.items():
                # Operacions especifiques de la 3.1A LB

                # Guardem l'activa actual per mirar despres si hi ha exces
                res_activa_tmp = res_activa

                res_activa = (res_activa * 1.04) + (0.01*dict_dies[periode.name]*polissa.trafo)

                # Busquem el preu a traves de les llistes de preus
                price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, res_activa, context={'date': data_versio})[tarifa]
                if price:
                    # Si trobem un preu vàlid a la llista de preus fem servir aquest
                    # sino farem servir el que hi hagi a standard_price
                    periode.standard_price = price

                # Creem la línia de la factura (account)
                invoice_line_values = {}
                invoice_line_values = {
                  'name': periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': res_activa,
                  'price_unit': periode.standard_price,
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                         [factura_id],
                                                                         versions,
                                                                         data_versio)
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'energia',
                  'quantitat': res_activa,
                  'preu': periode.standard_price,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)
                comment += '%s (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, res_activa, periode.standard_price, res_activa*periode.standard_price)

                # Guardem la lecura actual (activa)
                lectura_energia_vals = {}
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
                res = cr.dictfetchone()
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'activa',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora'],
                }
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
                res = cr.dictfetchone()
                if not res or not len(res):
                    res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)

                # Posem la lectura dels periodes agrupats
                for periodeagrupat in periode.browse(cr, uid, periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)])):
                    lectura_energia_vals = {}
                    cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periodeagrupat.id))
                    res = cr.dictfetchone()
                    if res:
                        if comptador.data_baixa and res['name'] > comptador.data_baixa:
                            res['name'] = comptador.data_baixa
                        lectura_energia_vals = {
                          'name': periodeagrupat.name,
                          'factura': factura_id,
                          'tipus': 'activa',
                          'lect_actual': res['lectura'],
                          'data_actual': res['name'],
                          'hora_actual': res['hora']
                        }
                        cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periodeagrupat.id))
                        res = cr.dictfetchone()
                        if not res or not len(res):
                            res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                        if res['name'] < comptador.data_alta:
                            res['name'] = comptador.data_alta
                        lectura_energia_vals['lect_anterior'] = res['lectura']
                        lectura_energia_vals['data_anterior'] = res['name']
                        lectura_energia_vals['hora_anterior'] = res['hora']
                        lectura_energia_obj.create(cr, uid, lectura_energia_vals)

                if periode.efecte_reactiva:
                    # Agafem el marge de reactiva que ens toca
                    cr.execute("select name from giscedata_lectures_mreactiva where data_inici <= %s and data_final > %s", ('%s-%s-01' % (anyy, mes), '%s-%s-01' % (anyy, mes)))
                    marge = cr.fetchone()[0]

                    # Calculem la lectura
                    res_reactiva = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'R')

                    # Sumem l'energia dels periodes agrupats amb aquest
                    for periodeagrupat in periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)]):
                        res_reactiva += energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periodeagrupat, comptador.id, polissa.facturacio, 'R')

                    exces = res_reactiva - (res_activa_tmp * marge)
                    if exces > 0:
                        # Excés augmentat un 4%
                        exces *= 1.04
                        cosfi = res_activa_tmp / (math.sqrt(math.pow(res_activa_tmp, 2) + math.pow(res_reactiva, 2)))
                        cosfi = round(cosfi, 2)
                        # Busquem el preu pel cosfi que ens surti
                        cosfi_obj = self.pool.get('giscedata.lectures.cosfi')
                        results = cosfi_obj.search(cr, uid, [('min', '<=', cosfi), ('max', '>', cosfi)])
                        reactiva_periode = self.repartir_energia(cr, uid, [factura_id],
                                                               versions, exces)
                        for data_versio, exces in reactiva_periode.items():
                            cosfi_price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], cosfi_obj.browse(cr, uid, results[0]).product_id.id, cosfi, context={'date': data_versio})[tarifa]

                            # Creem la línia de factura (account)
                            invoice_line_values = {}
                            invoice_line_values = {
                              'name': '%s (r)' % periode.name,
                              'product_id': periode.product_id.id,
                              'quantity': exces,
                              'price_unit': cosfi_price,
                              'invoice_id': invoice_id,
                              'account_id': periode.property_account_income.id,
                              'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                            }
                            invoice_line_obj.create(cr, uid, invoice_line_values)

                            # Creem la línia de la factura (report)
                            data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                               [factura_id],
                                                                               versions,
                                                                               data_versio)
                            factura_linia_values = {
                              'name': periode.name,
                              'factura': factura_id,
                              'tipus': 'reactiva',
                              'quantitat': exces,
                              'preu': cosfi_price,
                              'data_desde': data_desde,
                              'data_hasta': data_hasta
                            }
                            factura_linia_obj.create(cr, uid, factura_linia_values)

                            comment += '%s(r) (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, exces, cosfi_price, exces*cosfi_price)

                # Guardem la lecura actual (reactiva)
                lectura_energia_vals = {}
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
                res = cr.dictfetchone()
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'reactiva',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora']
                }
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
                res = cr.dictfetchone()
                if not res or not len(res):
                    res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)

                # Posem la lectura dels periodes agrupats
                for periodeagrupat in periode.browse(cr, uid, periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)])):
                    lectura_energia_vals = {}
                    cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periodeagrupat.id))
                    res = cr.dictfetchone()
                    if res:
                        if comptador.data_baixa and res['name'] > comptador.data_baixa:
                            res['name'] = comptador.data_baixa
                        lectura_energia_vals = {
                          'name': periodeagrupat.name,
                          'factura': factura_id,
                          'tipus': 'reactiva',
                          'lect_actual': res['lectura'],
                          'data_actual': res['name'],
                          'hora_actual': res['hora'],
                        }
                        cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periodeagrupat.id))
                        res = cr.dictfetchone()
                        if not res or not len(res):
                            res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                        if res['name'] < comptador.data_alta:
                            res['name'] = comptador.data_alta
                        lectura_energia_vals['lect_anterior'] = res['lectura']
                        lectura_energia_vals['data_anterior'] = res['name']
                        lectura_energia_vals['hora_anterior'] = res['hora']
                        lectura_energia_obj.create(cr, uid, lectura_energia_vals)

        ### CALCULEM LA POTENCIA ###
        comment += '\n*** TERMES DE POTÈNCIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s and agrupat_amb is null", (polissa.tarifa.id, 'tp'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            cr.execute("select potencia from giscedata_polissa_potencia_contractada_periode where periode_id = %i and polissa_id = %i", (periode.id, polissa.id))
            pot_contract = cr.fetchone()[0]
            cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periode.id))
            potencia = cr.fetchone()
            # Busquem el maxímetre agrupat (6 Periodes)
            for periodeagrupat in periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)]):
                cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periodeagrupat))
                pot_agr = cr.fetchone()
                potencia = (max(potencia[0], pot_agr[0]),)
            if not potencia[0]:
                potencia = (0,)
            # Comprovar si tenim exces de potencia o no
            potencia = (potencia[0] * 1.04,)
            if not pot_contract:
                interval = 0
            else:
                interval = (potencia[0]/pot_contract)*100
            if interval > 105:
                nova_pot = potencia[0] + 2 * (potencia[0] - 1.05*pot_contract)
            elif interval >= 85 and interval <= 105:
                nova_pot = potencia[0]
            elif interval < 85:
                nova_pot = pot_contract * 0.85

            # Si la quantitat és més gran que 0
            if nova_pot > 0 or True: # SEMPRE ENTRI
                # Busquem el preu a traves de les llistes de preus
                data_pepote = self.repartir_potencia(cr, uid, [factura_id], versions)
                for data_versio, pepote in data_pepote.items():
                    price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, nova_pot, context={'date': data_versio})[tarifa]
                    if price:
                        # Si trobem un preu vàlid a la llista de preus fem servir aquest
                        # sino farem servir el que hi hagi a standard_price
                        periode.standard_price = price

                    # Creem la linia de la factura (account)
                    invoice_line_values = {}
                    invoice_line_values = {
                      'name': periode.name,
                      'product_id': periode.product_id.id,
                      'quantity': nova_pot,
                      'price_unit': (periode.standard_price*pepote/12),
                      'invoice_id': invoice_id,
                      'account_id': periode.property_account_income.id,
                      'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                    }
                    invoice_line_obj.create(cr, uid, invoice_line_values)

                    # Creem la línia de la factura (report)
                    data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                           [factura_id],
                                                                           versions,
                                                                           data_versio)
                    factura_linia_values = {
                      'name': periode.name,
                      'factura': factura_id,
                      'tipus': 'potencia',
                      'quantitat': nova_pot,
                      'preu': periode.standard_price,
                      'extra': pepote,
                      'data_desde': data_desde,
                      'data_hasta': data_hasta
                    }
                    factura_linia_obj.create(cr, uid, factura_linia_values)

                    comment += '%s (%s - %s): %f * %f * %f/12 = %f\n' % (periode.name, data_desde, data_hasta, nova_pot, periode.standard_price, pepote, nova_pot*periode.standard_price*pepote/12)

                    data['comment'] = comment

            exces = potencia[0] - pot_contract
            if exces < 0:
                exces = 0
            # Guardem les lectures de potencia
            lectura_potencia_vals = {
              'name': periode.name,
              'factura': factura_id,
              'pot_contract': pot_contract,
              'pot_maximetre': potencia[0],
              'exces': exces,
            }
            lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)
            # Posem la lectura dels periodes agrupats
            for periodeagrupat in periode.browse(cr, uid, periode.search(cr, uid, [('agrupat_amb.id', '=', periode.id)])):
                cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periodeagrupat.id))
                potencia = cr.fetchone()
                if potencia:
                    lectura_potencia_vals = {
                      'name': periodeagrupat.name,
                      'factura': factura_id,
                      'pot_contract': pot_contract,
                      'pot_maximetre': potencia[0],
                    }
                    lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)


        # Calculem el lloguer
        invoice_obj.write(cr, uid, [invoice_id], {'comment': data['comment']})
        self.lloguer(cr, uid, factura_id, tarifa, data_factura, data)

        return factura_id

    def t6_1(self, cr, uid, polissa_id, comptador_id, mes, anyy, tarifa, fact_client=False, data_factura=time.strftime('%Y-%m-%d'), tipo_rectificadora='N'):
        data = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        polissa = polissa_obj.browse(cr, uid, polissa_id)
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        partner_obj = self.pool.get('res.partner')
        comptador = self.pool.get('giscedata.lectures.comptador').browse(cr, uid, comptador_id)
        factura_linia_obj = self.pool.get('giscedata.factura.linia')
        energia_obj = self.pool.get('giscedata.lectures.lectura')
        lectura_energia_obj = self.pool.get('giscedata.factura.lectura.energia')
        lectura_potencia_obj = self.pool.get('giscedata.factura.lectura.potencia')

        # Mirem si volem facturar al client o a la comercialitzadora,
        # per defecte sempre a la comercialitzadora

        if fact_client:
            partner = partner_obj.browse(cr, uid, polissa.partner_id.id)
        else:
            partner = partner_obj.browse(cr, uid, polissa.comercialitzadora.id)

        # Busquem la data límit:
        due_date = False
        if partner.property_payment_term:
            pt_obj = self.pool.get('account.payment.term')
            pterm_list= pt_obj.compute(cr, uid, partner.property_payment_term.id, value=1, date_ref=data_factura)

            if pterm_list:
                pterm_list = [line[0] for line in pterm_list]
                pterm_list.sort()
                due_date =  pterm_list[-1]

        # Creem la factura (account)
        invoice_values = {
          'type': 'out_invoice',
          'reference': polissa.name,
          'partner_id': partner.id,
          'address_invoice_id': partner_obj.address_get(cr, uid, [partner.id], ['invoice'])['invoice'],
          'account_id': partner.property_account_receivable.id,
          'payment_term': partner.property_payment_term.id,
          'date_invoice': data_factura,
          'date_due': due_date,
        }
        invoice_id = invoice_obj.create(cr, uid, invoice_values)

        # Creem el nostre model de factures (report)
        # Si la facturació és bimestral la data d'inici és la d'un mes
        # enrera.
        if polissa.facturacio == 2:
            cr.execute("select to_char(date %s - interval %s, 'YYYY-MM-DD')", ('%i-%i-01' % (anyy, mes), '1 month'))
            data_inici = cr.fetchone()[0]
        else:
            data_inici = '%i-%i-01' % (anyy, mes)
        factura_values = {
          'comptador': comptador.id,
          'polissa': polissa.id,
          'data_inici': data_inici,
          'data_final': '%i-%i-%i' % (anyy, mes, calendar.monthrange(anyy, mes)[1]),
          'mes': mes,
          'anyy': anyy,
          'tarifa': polissa.tarifa.id,
          'facturacio': polissa.facturacio,
          'factura': invoice_id,
          'tax': partner.property_account_tax.id or None,
          'comercialitzadora': partner.id,
          'tipo_rectificadora': tipo_rectificadora
        }
        if tipo_rectificadora == 'R':
            factura_values['name'] = self.pool.get('ir.sequence').get(cr, uid, 'giscedata.factura.rectificadora')

        factura_id = self.create(cr, uid, factura_values)
        factura = self.browse(cr, uid, factura_id)
        versions = self.get_versions_preus(cr, uid, tarifa, factura.data_inici_real,
                                           factura.data_final_real)

        ### CALCULEM L'ENERGIA ###
        comment = ""
        comment += '\n*** TERMES ENERGIA ***\n'

        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s", (polissa.tarifa.id, 'te'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            res_activa = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'A')
            energies_periode = self.repartir_energia(cr, uid, [factura_id], versions,
                                                     res_activa)
            for data_versio, res_activa in energies_periode.items():
                # Guardem la lecura actual (activa)
                lectura_energia_vals = {}
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
                res = cr.dictfetchone()
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'activa',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora'],
                }
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'A' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
                res = cr.dictfetchone()
                if not res or not len(res):
                    res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)


                # Busquem el preu a traves de les llistes de preus
                price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, res_activa, context={'date': data_versio})[tarifa]
                if price:
                    # Si trobem un preu vàlid a la llista de preus fem servir aquest
                    # sino farem servir el que hi hagi a standard_price
                    periode.standard_price = price

                invoice_line_values = {
                  'name': periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': res_activa,
                  'price_unit': periode.standard_price,
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                         [factura_id],
                                                                         versions,
                                                                         data_versio)
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'energia',
                  'quantitat': res_activa,
                  'preu': periode.standard_price,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)

                comment += '%s (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, res_activa, periode.standard_price, res_activa*periode.standard_price)

                if periode.efecte_reactiva:
                    # Agafem el marge de reactiva que ens toca
                    cr.execute("select name from giscedata_lectures_mreactiva where data_inici <= %s and data_final > %s", ('%s-%s-01' % (anyy, mes), '%s-%s-01' % (anyy, mes)))
                    marge = cr.fetchone()[0]

                    res_reactiva = energia_obj.get_energia_month_periode(cr, uid, mes, anyy, periode.id, comptador.id, polissa.facturacio, 'R')

                    exces_reactiva = res_reactiva - (res_activa * marge)

                    if exces_reactiva > 0:
                        cosfi = res_activa / (math.sqrt(math.pow(res_activa, 2) + math.pow(res_reactiva, 2)))
                        cosfi = round(cosfi, 2)
                        # Busquem el preu pel cosfi que ens surti
                        cosfi_obj = self.pool.get('giscedata.lectures.cosfi')
                        results = cosfi_obj.search(cr, uid, [('min', '<=', cosfi), ('max', '>', cosfi)])
                        reactiva_periode = self.repartir_energia(cr, uid, [factura_id],
                                                               versions, exces_reactiva)
                        for data_versio, exces_reactiva in reactiva_periode.items():
                            cosfi_price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], cosfi_obj.browse(cr, uid, results[0]).product_id.id, cosfi, context={'date': data_versio})[tarifa]

                            # Creem la linia de la factura (account)
                            invoice_line_values = {
                              'name': '%s(r)' % periode.name,
                              'product_id': periode.product_id.id,
                              'quantity': exces_reactiva,
                              'price_unit': cosfi_price,
                              'invoice_id': invoice_id,
                              'account_id': periode.property_account_income.id,
                              'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                            }
                            invoice_line_obj.create(cr, uid, invoice_line_values)

                            # Creem la línia de la factura (report)
                            data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                               [factura_id],
                                                                               versions,
                                                                               data_versio)
                            factura_linia_values = {
                              'name': periode.name,
                              'factura': factura_id,
                              'tipus': 'reactiva',
                              'quantitat': exces_reactiva,
                              'preu': cosfi_price,
                              'data_desde': data_desde,
                              'data_hasta': data_hasta,
                            }
                            factura_linia_obj.create(cr, uid, factura_linia_values)

                            comment += '%s(r) (%s - %s): %f * %f = %f\n' % (periode.name, data_desde, data_hasta, exces_reactiva, cosfi_price, exces_reactiva*cosfi_price)

                # Guardem la lecura actual (reactiva)
                lectura_energia_vals = {}
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = %i and date_part('year', name) = %i and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, mes, anyy, periode.id))
                res = cr.dictfetchone()
                if comptador.data_baixa and res['name'] > comptador.data_baixa:
                    res['name'] = comptador.data_baixa
                lectura_energia_vals = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'reactiva',
                  'lect_actual': res['lectura'],
                  'data_actual': res['name'],
                  'hora_actual': res['hora'],
                }
                cr.execute("select name,hora,lectura from giscedata_lectures_lectura where comptador = %i and date_part('month', name) = date_part('month', %s::date - INTERVAL '%i month') and date_part('year', name) = date_part('year', %s::date - INTERVAL '%i month') and periode = %i and tipus = 'R' order by name desc limit 1", (comptador.id, '%s-%s-01' % (anyy, mes), polissa.facturacio, '%s-%s-01' % (anyy, mes), polissa.facturacio, periode.id))
                res = cr.dictfetchone()
                if not res or not len(res):
                    res = {'lectura': 0, 'name': comptador.data_alta, 'hora':'12:00:00'}
                if res['name'] < comptador.data_alta:
                    res['name'] = comptador.data_alta
                lectura_energia_vals['lect_anterior'] = res['lectura']
                lectura_energia_vals['data_anterior'] = res['name']
                lectura_energia_vals['hora_anterior'] = res['hora']
                lectura_energia_obj.create(cr, uid, lectura_energia_vals)

        ### CALCULEM LA POTÈNCIA ###
        comment += '\n*** TERMES DE POTÈNCIA ***\n'
        cr.execute("select id from giscedata_polissa_tarifa_periodes where tarifa = %i and tipus = %s", (polissa.tarifa.id, 'tp'))
        for periode in periode_obj.browse(cr, uid, [a[0] for a in cr.fetchall()]):
            cr.execute("select potencia from giscedata_polissa_potencia_contractada_periode where periode_id = %i and polissa_id = %i", (periode.id, polissa.id))
            pot_contract = cr.fetchone()[0]
            cr.execute("select coalesce(max(lectura),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periode.id))
            potencia = cr.fetchone()
            if not potencia[0]:
                potencia = (0,)

            # Si la quantitat és més gran que 0
            if pot_contract > 0:

                # Busquem el preu a traves de les llistes de preus
                data_pepote = self.repartir_potencia(cr, uid, [factura_id], versions)
                for data_versio, pepote in data_pepote.items():
                    price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], periode.product_id.id, pot_contract, context={'date': data_versio})[tarifa]
                    if price:
                        # Si trobem un preu vàlid a la llista de preus fem servir aquest
                        # sino farem servir el que hi hagi a standard_price
                        periode.standard_price = price

                    # Creem la linia de la factura (account)
                    invoice_line_values = {}
                    invoice_line_values = {
                      'name': periode.name,
                      'product_id': periode.product_id.id,
                      'quantity': pot_contract,
                      'price_unit': (periode.standard_price*pepote/12),
                      'invoice_id': invoice_id,
                      'account_id': periode.property_account_income.id,
                      'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                    }
                    invoice_line_obj.create(cr, uid, invoice_line_values)

                    # Creem la línia de la factura (report)
                    data_desde, data_hasta = self.get_data_desde_hasta_linia(cr, uid,
                                                                           [factura_id],
                                                                           versions,
                                                                           data_versio)
                    factura_linia_values = {
                      'name': periode.name,
                      'factura': factura_id,
                      'tipus': 'potencia',
                      'quantitat': pot_contract,
                      'preu': periode.standard_price,
                      'extra': pepote,
                      'data_desde': data_desde,
                      'data_hasta': data_hasta
                    }
                    factura_linia_obj.create(cr, uid, factura_linia_values)


                    comment += '%s (%s - %s): %f * %f * %f/12 = %f\n' % (periode.name, data_desde, data_hasta, pot_contract, periode.standard_price, pepote, pot_contract*periode.standard_price*pepote/12)

            # Comprovar si tenim exces de potencia o no
            #exces = potencia[0] - pot_contract
            # Agafem directament el camp exces de la lectura
            cr.execute("select coalesce(max(exces),0) from giscedata_lectures_potencia where date_part('month', name) in (%i-%i, %i) and date_part('year', name) = %i and comptador = %i and periode = %i", (mes, polissa.facturacio-1, mes, anyy, comptador.id, periode.id))
            exces = cr.fetchone()
            if not exces[0]:
                exces = (0,)
            exces = exces[0]
            ki = {
              '1': 1,
              '2': 0.5,
              '3': 0.37,
              '4': 0.37,
              '5': 0.37,
              '6': 0.17,
            }
            if exces > 0:
                # Aplicar Ki * 1.4064 * Aei
                #TOTAL = ki[periode.default_code] * 1.4064 * exces

                # Creem la factura de la línia (account)
                invoice_line_values = {
                  'name': '%s (extra)' % periode.name,
                  'product_id': periode.product_id.id,
                  'quantity': ki[periode.default_code] * exces,
                  'price_unit': 1.4064,
                  'invoice_id': invoice_id,
                  'account_id': periode.property_account_income.id,
                  'invoice_line_tax_id': [(6, 0, [a.id for a in periode.taxes_id])],
                }
                invoice_line_obj.create(cr, uid, invoice_line_values)

                # Creem la línia de la factura (report)
                # TODO: No sabem repartir això amb dos preus diferents.
                data_desde, data_hasta = factura.data_inici_real, factura.data_final_real
                factura_linia_values = {
                  'name': periode.name,
                  'factura': factura_id,
                  'tipus': 'exces_potencia',
                  'quantitat': exces,
                  'extra': ki[periode.default_code],
                  'preu': 1.4064,
                  'data_desde': data_desde,
                  'data_hasta': data_hasta
                }
                factura_linia_obj.create(cr, uid, factura_linia_values)

                comment += '%s (extra) (%s - %s): %f * 1.4064 * %f = %f\n' % (periode.name, data_desde, data_hasta, ki[periode.default_code], exces, ki[periode.default_code] * 1.4064 * exces)
                data['comment'] = comment

            # Guardem les lectures de potencia
            lectura_potencia_vals = {
              'name': periode.name,
              'factura': factura_id,
              'pot_contract': pot_contract,
              'pot_maximetre': potencia[0],
              'exces': exces,
            }
            lectura_potencia_obj.create(cr, uid, lectura_potencia_vals)

        data['comment'] = comment
        # Calculem el lloguer
        invoice_obj.write(cr, uid, [invoice_id], {'comment': data['comment']})
        self.lloguer(cr, uid, factura_id, tarifa, data_factura, data)

        return factura_id


    def lloguer(self, cr, uid, factura, tarifa, data_factura, data={}):
        invoice_obj = self.pool.get('account.invoice')
        invoice_line_obj = self.pool.get('account.invoice.line')
        factura_linia_obj = self.pool.get('giscedata.factura.linia')
        factura = self.browse(cr, uid, factura)

        data['comment'] += '\n*** LLOGUERS ***\n'

        if factura.comptador.lloguer and factura.comptador.product_id:

            # Mirem si hem d'aplicar un coficient de proporcionalitat (equips que no hi han sigut durant
            # tot el periode de facturacio)
            quantity = factura.polissa.facturacio * factura.coficient

            # Busquem el preu a traves de les llistes de preus
            price = self.pool.get('product.pricelist').price_get(cr, uid, [tarifa], factura.comptador.product_id.id, quantity, context={'date': data_factura})[tarifa]
            if price:
                # Si trobem un preu vàlid a la llista de preus fem servir aquest
                # sino farem servir el que hi hagi a standard_price
                factura.comptador.product_id.standard_price = price


            # Creem la linia de la factura (account)
            invoice_line_values = {}
            invoice_line_values = {
              'name': factura.comptador.product_id.name,
              'product_id': factura.comptador.product_id.id,
              'quantity': quantity,
              'price_unit': factura.comptador.product_id.standard_price,
              'invoice_id': factura.factura.id,
              'account_id': factura.comptador.product_id.property_account_income.id,
              'invoice_line_tax_id': [(6, 0, [a.id for a in factura.comptador.product_id.taxes_id])],
            }
            invoice_line_obj.create(cr, uid, invoice_line_values)

            # Creem la linia de la factura (report)
            data_desde, data_hasta = factura.data_inici_real, factura.data_final_real
            factura_linia_values = {
              'name': factura.comptador.product_id.name,
              'factura': factura.id,
              'tipus': 'lloguer',
              'quantitat': quantity,
              'preu': factura.comptador.product_id.standard_price,
              'data_desde': data_desde,
              'data_hasta': data_hasta
            }
            factura_linia_obj.create(cr, uid, factura_linia_values)


            data['comment'] += '%s (%s - %s): %f * %i mes = %f\n' % (factura.comptador.product_id.name, data_desde, data_hasta, factura.comptador.product_id.standard_price, factura.polissa.facturacio, factura.comptador.product_id.standard_price * factura.polissa.facturacio)

            invoice_obj.write(cr, uid, [factura.factura.id], {'comment': data['comment']})


    # Funció facturació en mode 'BATCH'
    def batch_mode(self, cr, uid, data={}, context={}):
        import xmlrpclib

        pwd = self.pool.get('res.users').browse(cr, uid, uid).password
        host = tools.config['interface'] or 'localhost'
        port = tools.config['port']
        dbname = tools.config['db_name']

        wsock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/wizard' % (host,int(port)))
        rsock = xmlrpclib.ServerProxy('http://%s:%d/xmlrpc/report' % (host,int(port)))

        wiz_id = wsock.create(dbname, uid, pwd, 'giscedata.lectures.facturacio.massiva')

        params = {
          'form': {
            'tarifa': data['tarifa'],
            'force_date': data['force_date'],
            'print_pdf': 0,
            'mes': data['mes'],
            'periode': data['periode'],
          }
        }

        # Facturem
        data['start'] = time.strftime('%Y-%m-%d %H:%M:%S')
        res = wsock.execute(dbname, uid, pwd, wiz_id, params, 'comprovar')
        data['end'] = time.strftime('%Y-%m-%d %H:%M:%S')

        # Mirem si s'ha de generar PDF
        if data['print_pdf']:
            report = wsock.execute(dbname, uid, pwd, wiz_id, params, 'print_report')

            # Dades que rebem
            # {'report': 'giscedata.lectures.factura', 'get_id_from_action': True, 'type': 'print', 'state': 'end', 'datas': {'ids': [129, 130]}}
            if len(report['datas']['ids']):
                import base64
                report_id = rsock.report(dbname, uid, pwd, report['report'], report['datas']['ids'], {'model': 'giscedata.factura', 'id': report['datas']['ids'][0], 'report_type':'pdf'})
                time.sleep(5)
                state = False
                attempt = 0
                while not state:
                    pdf = rsock.report_get(dbname, uid, pwd, report_id)
                    state = report['state']
                    if not state:
                        time.sleep(1)
                        attempt += 1
                if state:
                    pdf_attach = [('FACTURAS_%s.pdf' % data['mes'].replace('/', '_'), base64.decodestring(pdf['result']))]

        # Resultats
        res = wsock.execute(dbname, uid, pwd, wiz_id, params, 'end')
        text = """
    Se ha generado la facturación del mes %s en modo BATCH correctamente

    Parámetros:
      - FECHA FACTURACIÓN: %s
      - RESUMEN: %s
      - CORREO ELECTRÓNICO: %s
      - GENERACIÓN PDF: %s

    Tiempo de ejectución:
      - Inicio: %s
      - Final: %s

    Resultado:
      - FACTURAS GENERADAS: %i

    """ % (data['mes'], data['force_date'], data['resumen'], data['email'], data['print_pdf'], data['start'], data['end'], res['action']['limit'])


        subject = 'FACTURACION MODO BATCH %s' % data['mes']
        body = text

        if data['resumen'] == 'email' and data['email']:
            # Enviem el resum via correu electrònic
            email_from = 'FACTURACION BATCH <%s>' % data['email'][0]
            emails_to = data['email'].split(',')
            if data['print_pdf'] and len(pdf_attach):
                tools.email_send_attach(email_from, emails_to, subject, body, attach=pdf_attach)
            else:
                tools.email_send(email_from, emails_to, subject, body)

        else:
            # Fem una request
            request = self.pool.get('res.request')
            vals = {
              'name': subject,
              'act_from': uid,
              'act_to': uid,
              'body': body,
            }
            req_id = request.create(cr, uid, vals)
            request.request_send(cr, uid, [req_id])
            """
            if data['print_pdf'] and len(pdf_attach):
              # Hem de crear un ir.attachment a aquest request
              attachment = self.pool.get('ir.attachment')
              vals = {
                'name': pdf_attach[0][0],
                'datas': pdf_attach[0][1],
                'datas_fname': pdf_attach[0][0],
                'res_model': 'res.request',
                'res_id': req_id,
              }
              attachment.create(cr, uid, vals)
            """

        return True

giscedata_factura()

class giscedata_factura_linia(osv.osv):

    _name = 'giscedata.factura.linia'
    _module = "giscedata_facturacio_old"

    def copy(self, cursor, uid, id, default=None, context=None):
        if not context:
            context = {}
        context['from_copy'] = True
        res_id = super(giscedata_factura_linia,
                       self).copy(cursor, uid, id, default, context)
        return res_id

    def create(self, cursor, uid, vals, context=None):
        """Agrupem les linies que no han canviat de preu.
        """
        if not context:
            context = {}
        search_params = [
          ('factura.id', '=', vals['factura']),
          ('tipus', '=', vals['tipus']),
          ('name', '=', vals['name']),
          ('preu', '=', vals['preu'])
        ]
        lids = self.search(cursor, uid, search_params)
        if not lids or context.get('from_copy', False):
            return osv.osv.create(self, cursor, uid, vals, context=context)
        for linia in self.browse(cursor, uid, lids):
            vals['data_desde'] = min(linia.data_desde, vals['data_desde'])
            vals['data_hasta'] = max(linia.data_hasta, vals['data_hasta'])
            if vals['tipus'] == 'potencia':
                # Recalculem extra per quan no canvia els preus que el coeficient sigui
                # el general per dies de facturació. Veure: http://goo.gl/iGahv
                dies = (datetime(*[int(a) for a in vals['data_hasta'].split('-')])
                        - datetime(*[int(a) for a in vals['data_desde'].split('-')])
                        ).days + 1
                vals['extra'] = ((float(dies) / float(linia.factura.dies_facturacio))
                                  * linia.factura.facturacio)
            else:
                vals['quantitat'] += linia.quantitat
            self.write(cursor, uid, [linia.id], vals)
            return linia.id

    def _subtotal(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        config_obj = self.pool.get('res.config')
        _hrd = config_obj.get(cr, uid, 'giscedata.factura.round2.date', _hardcoded_round_date)
        for l in self.browse(cr, uid, ids, context):
            if l.tipus in ['reactiva', 'lloguer']:
                res[l.id] = l.preu * l.quantitat
            elif l.tipus == 'energia':
                res[l.id] = l.preu * l.quantitat * l.extra
            elif l.tipus == 'potencia':
                res[l.id] = l.preu * round(l.quantitat, 3) * l.extra/12
            elif l.tipus == 'exces_potencia':
                res[l.id] = l.preu * round(l.quantitat, 3) * l.extra
            elif l.tipus == 'iee':
                res[l.id] = l.preu * l.quantitat * 0.04864

            # Per deixar-ho com abans només arrodonim a 2 si la data_final és més gran
            # que la variable del llindar (_hardcoded_round_date)
            if l.factura.data_final >= _hrd:
                res[l.id] =  round(res[l.id], 2)
            else:
                res[l.id] =  round(res[l.id], 4)
        return res

    _columns = {
      'name': fields.char('Nom', size=64),
      'factura': fields.many2one('giscedata.factura', 'Factura', required=True, ondelete='cascade'),
      'tipus': fields.selection([('energia', 'Termes d\'Energia'),
                                 ('potencia', 'Termes de Potència'),
                                 ('reactiva', 'Excés de Reactiva'),
                                 ('exces_potencia', 'Excés de Potència'),
                                 ('lloguer', 'Lloguer'),
                                 ('iee', 'Impuesto sobre la electricidad')],
                                'Tipus de línia', required=True),
      'quantitat': fields.float('Quantitat', digits=(16,3)),
      'preu': fields.float('Preu', digits=(16,6)),
      'extra': fields.float('Extra per operacions'),
      'subtotal': fields.function(_subtotal, type='float', method=True, string='Subtotal'),
      'data_desde': fields.date('Data desde'),
      'data_hasta': fields.date('Data hasta'),
    }

    _order = "tipus, name"

    _defaults = {
      'extra': lambda *a: 1.0,
    }

giscedata_factura_linia()

class giscedata_factura_lectura_energia(osv.osv):

    _name = 'giscedata.factura.lectura.energia'
    _module = "giscedata_facturacio_old"

    def _consum(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for l in self.browse(cr, uid, ids, context):
            consum = l.lect_actual - l.lect_anterior
            if consum < 0:
                consum += l.factura.comptador.giro
            res[l.id] = consum
        return res

    _columns = {
      'name': fields.char('Nom', size=64),
      'factura': fields.many2one('giscedata.factura', 'Factura', required=True, ondelete='cascade'),
      'tipus': fields.selection([('activa', 'Activa'), ('reactiva', 'Reactiva')], 'Tipus'),
      'data_actual': fields.date('Fecha actual'),
      'hora_actual': fields.char('Hora actual', size=8),
      'lect_actual': fields.integer('Lectura actual'),
      'data_anterior': fields.date('Fecha anterior'),
      'hora_anterior': fields.char('Hora anterior', size=8),
      'lect_anterior': fields.integer('Lectura anterior'),
      'consum': fields.function(_consum, method=True, type='integer', string='Consumo'),
    }

    _order = "tipus, name"

    _defaults = {
      'hora_actual': lambda *a: '12:00:00',
      'hora_anterior': lambda *a: '12:00:00'
    }

giscedata_factura_lectura_energia()

class giscedata_factura_lectura_potencia(osv.osv):

    _name = 'giscedata.factura.lectura.potencia'
    _module = "giscedata_facturacio_old"

    def _punta(self, cr, uid, ids, field_name, context={}, *args):
        res = {}
        for l in self.browse(cr, uid, ids, context):
            if l.pot_maximetre > l.pot_contract:
                res[l.id] = round(l.pot_maximetre, 3)
            else:
                res[l.id] = 0.000
        return res

    _columns = {
      'name': fields.char('Nom', size=64),
      'factura': fields.many2one('giscedata.factura', 'Factura', required=True, ondelete='cascade'),
      'pot_contract': fields.float('Potència contractada', digits=(16,3)),
      'pot_maximetre': fields.float('Potència maxímetre', digits=(16,3)),
      'exces': fields.float('Exces', digits=(16,3)),
      'punta': fields.function(_punta, method=True, type='float', string='Punta', digits=(16,3)),
    }

    _order = "name"

giscedata_factura_lectura_potencia()
