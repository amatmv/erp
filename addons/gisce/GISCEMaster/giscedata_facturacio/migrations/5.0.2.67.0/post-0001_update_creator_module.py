# coding=utf-8
from oopgrade import oopgrade
import netsvc


def up(cursor, installed_version):
    logger= netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Changing ir_model_data from giscedata_facturacio_comer to giscedata_facturacio')

    cursor.execute('UPDATE ir_model_data SET module=\'giscedata_facturacio\' '
                   'WHERE module=\'giscedata_facturacio_comer\' '
                   ' AND name=\'field_giscedata_polissa_facturacio_endarrerida\'')

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully changed')


def down(cursor):
    return

migrate = up
