# -*- coding: utf-8 -*-
from osv import osv, fields
import pandas as pd
import tempfile
from os import path, unlink
from base64 import b64encode, b64decode
from datetime import datetime
import csv


class GiscedataMeasuresIntegrity(osv.osv):
    _name = 'giscedata.profiles.lot'
    _inherit = 'giscedata.profiles.lot'

    _columns = {
        'clmag_integrity': fields.char('CLMAG', size=35, readonly=True),
        'clmag5a_integrity': fields.char('CLMAG5A', size=35, readonly=True),
        'clinme_integrity': fields.char('CLINME', size=256, readonly=True),
        'ultima_comprovacio_consums': fields.char(
            'Última comprovació consums', readonly=True, size=19
        ),
    }

    aggregations_columns = [
        'distribuidora', 'comercialitzadora', 'provincia', 'agree_tensio',
        'agree_tarifa', 'agree_dh', 'agree_tipo', 'agree_dh', 'consum'
    ]
    columns = [
        'distribuidora', 'comercialitzadora', 'provincia', 'agree_tensio',
        'agree_tarifa', 'agree_dh', 'agree_tipo'
    ]

    def check_files(self, cursor, uid, ids, context=None):
        """ Treu un sumatori amb els consums de: agregacions, CLMAG, CLMAG5A i
        CLINME d'aquest lot
        """
        df_clmag = self.check_clmag(cursor, uid, ids, context)
        df_clmag5a = self.check_clmag5a(cursor, uid, ids, context)
        df_clinme = self.check_clinme(cursor, uid, ids, context)
        df_clmag = df_clmag[df_clmag.magnitud != 'R1']
        #df_aggregations = self.check_aggregations(cursor, uid, ids, context)

        df_clmags = pd.concat([df_clmag, df_clmag5a])
        df_merged = pd.DataFrame.merge(df_clmags, df_clinme, how='outer')

        self.aggregations_columns.append('clmag_consumption')
        self.aggregations_columns.append('clinme_consumption')
        self.aggregations_columns.append('clmag5a_consumption')

        _id, filename = tempfile.mkstemp(suffix='.csv')

        df_merged.to_csv(
            filename, sep=';', columns=self.aggregations_columns
        )
        with open(filename) as f:
            file_content = f.read()

        self.pool.get('ir.attachment').create(cursor, uid, {
            'name': path.basename(filename),
            'datas': b64encode(file_content),
            'datas_fname': path.basename(filename),
            'res_model': 'giscedata.profiles.lot',
            'res_id': ids[0]
        })
        agg_val_obj = self.pool.get(
            'giscedata.profiles.aggregations.validations')
        agg_val_obj.create(cursor, uid,
                           {'filename': filename, 'lot_id': ids[0]})
        try:
            unlink(filename)
        except OSError:
            # the file has been deleted during the execution of the function
            raise osv.except_osv('Error', 'Arxiu de validacions eliminat')

    def check_aggregations(self, cursor, uid, ids, context=None):
        """ Retorna les agregacions i els seus consums
        :return: DataFrame amb agregació > consum
        """
        agregacions_ids = self.pool.get(
            'giscedata.profiles.aggregations').search(
            cursor, uid, [('lot_id', '=', ids)]
        )
        df_aggregations = pd.DataFrame(
            data=self.pool.get('giscedata.profiles.aggregations').read(
                cursor, uid, agregacions_ids, self.aggregations_columns
            )
        )

        return df_aggregations.drop('id', axis=1)

    def check_clmag(self, cursor, uid, ids, context=None):
        """ Retorna el sumatori de consums per agregació dels tipus 3, 4
        :return: DataFrame amb agregació tipus 3, 4 -> consum
        """
        if not context:
            context = {}

        attach_obj = self.pool.get('ir.attachment')

        columns = [
            'distribuidora', 'comercialitzadora', 'provincia', 'agree_tensio',
            'agree_tarifa', 'agree_dh', 'agree_tipo', 'magnitud'
        ]

        # Get last CLMAG file version
        attach_id = context.get('clmag', False)
        if not attach_id:
            distribuidora = self.default_distribuidora(cursor, uid, context)
            dfname = 'CLMAG_{distri}_%'.format(distri=distribuidora)
            attach_id = attach_obj.search(cursor, uid, [
                ('res_id', '=', ids[0]),
                ('datas_fname', '=like', dfname),
                ('res_model', '=', 'giscedata.profiles.lot')
            ], order='create_date desc')[0]
        content = attach_obj.read(cursor, uid, attach_id, ['datas'])['datas']
        lines = b64decode(content)
        lines = lines.split('\n')[:-1]

        # CLMAG consumptions positions
        positions = [x for x in xrange(11, 107, 4)]
        consumptions_clmag = []
        AE_code = ';AE;'

        clmag_aggs = [agg[11:38] for agg in lines]
        df_aggs = pd.DataFrame(data=clmag_aggs).drop_duplicates()
        clmag_aggs = [x[0] for x in df_aggs.values.tolist()]

        try:
            for agg in clmag_aggs:
                consumption_iter = 0
                consumption_clmag = 0
                for x in lines:
                    if agg in x and AE_code in x:
                        consumption_tmp = x.split(';')
                        for pos in positions:
                            try:
                                consumption_iter += int(consumption_tmp[pos])
                            except:
                                break
                        consumption_clmag += consumption_iter
                        consumption_iter = 0

                aux = dict(zip(columns, agg.split(';')))
                aux['clmag_consumption'] = consumption_clmag
                consumptions_clmag.append(aux)
        except:
            pass

        return pd.DataFrame(data=consumptions_clmag)

    def check_clmag5a(self, cursor, uid, ids, context=None):
        """ Retorna el sumatori de consums per agregació dels tipus 3, 4
        :return: DataFrame amb agregació tipus 5 -> consum
        """
        if not context:
            context = {}

        attach_obj = self.pool.get('ir.attachment')

        # Get last CLMAG5A file version
        attach_id = context.get('clmag5a', False)
        if not attach_id:
            distribuidora = self.default_distribuidora(cursor, uid, context)
            dfname = 'CLMAG5A_{distri}_%'.format(distri=distribuidora)
            attach_id = attach_obj.search(cursor, uid, [
                ('res_id', '=', ids[0]),
                ('datas_fname', '=like', dfname),
                ('res_model', '=', 'giscedata.profiles.lot')
            ], order='create_date desc')[0]
        content = attach_obj.read(cursor, uid, attach_id, ['datas'])['datas']
        res = b64decode(content)
        res = res.split('\n')[:-1]

        #res = clmag5a.build_file()
        data = []
        for line in res:
            line = line.split(';')
            vals = dict(zip(
                ['distribuidora', 'comercialitzadora', 'agree_tensio',
                 'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia'],
                line[:7]))
            vals['clmag5a_consumption'] = int(line[10])
            data.append(vals)
        df_clmag5a = pd.DataFrame(data=data).groupby(self.columns).aggregate(
            {'clmag5a_consumption': 'sum'})
        df_clmag5a = df_clmag5a.reset_index()

        return df_clmag5a

    def check_clinme(self, cursor, uid, ids, context=None):
        """ Agrega les dades del fitxer CLINME, reuinides per CUPS, en un
        sumatori de consums per agregació
        :return: DataFrame amb agregacions tipus 3, 4, 5 -> consum
        """
        if not context:
            context = {}

        attach_obj = self.pool.get('ir.attachment')

        # Get last CLINME file version
        attach_ids = context.get('clinmes', False)
        if not attach_ids:
            distribuidora = self.default_distribuidora(cursor, uid, context)
            dfname = 'CLINME_{distri}_%'.format(distri=distribuidora)
            attach_ids = attach_obj.search(cursor, uid, [
                ('res_id', '=', ids[0]),
                ('datas_fname', '=like', dfname),
                ('res_model', '=', 'giscedata.profiles.lot')
            ], order='create_date desc')

        lines_tot = []
        for clinme_id in attach_ids:
            content = attach_obj.read(
                cursor, uid, clinme_id, ['datas']
            )['datas']
            res = b64decode(content)
            res = res.split('\n')[:-1]

            for y in res:
                try:
                    res = y.split(';')
                    value = {
                        'cups': res[0],
                        'distribuidora': res[1],
                        'comercialitzadora': res[2],
                        'agree_tensio': res[3],
                        'agree_tarifa': res[4],
                        'agree_dh': res[5],
                        'agree_tipo': res[6],
                        'provincia': res[7],
                        'baixa': res[8],
                        'clinme_consumption': int(float(res[11])),  # todo int(res))
                    }
                    lines_tot.append(value)
                except Exception:
                    continue  # last clinme aggregation nothing to do
        df_clinme = pd.DataFrame(data=lines_tot).groupby(
            self.columns).aggregate(
            {'clinme_consumption': 'sum'})

        return df_clinme.reset_index()


GiscedataMeasuresIntegrity()


class GiscedataProfilesAggregationsValidations(osv.osv):

    _name = 'giscedata.profiles.aggregations.validations'
    _inherit = 'giscedata.profiles.aggregations.validations'

    _columns = {
        'diferencia': fields.integer('Diff kWh'),
        'percentual': fields.float('Diff %'),
    }

    def create(self, cursor, uid, vals, context=None):
        """
        Llegeix el fitxer de validacions de linies de consum i en crea els registres calculant les diferencies.
        Si hi ha lineas precalculades les esborra per no duplicarles amb les noves
        :param vals: {'filename': 'tmp_filename', 'lot_id': 'lot_id'}
        """
        perf_lot_obj = self.pool.get('giscedata.profiles.lot')
        agg_ids = self.search(cursor, uid, [('lot_id', '=', vals['lot_id'])])
        if agg_ids:
            self.unlink(cursor, uid, agg_ids, context)

        keys = ['clmag5a_consumption', 'clmag_consumption',
                'clinme_consumption', 'consum']
        with open(vals['filename']) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                row.pop('', None)
                row['lot_id'] = vals['lot_id']
                # Aggregate sum pandas generate a float number
                for key in keys:
                    try:
                        row[key] = int(row[key].split('.')[0])
                    except ValueError:
                        row[key] = 0
                if row['agree_tipo'] == '05':
                    clmag_consumption = row['clmag5a_consumption']
                else:
                    clmag_consumption = row['clmag_consumption']

                diff = clmag_consumption - row['clinme_consumption']
                try:
                    percentual = float((abs(diff) / float(max(clmag_consumption, row['clinme_consumption']))) * 100)
                except ZeroDivisionError:
                    percentual = 0
                row['percentual'] = percentual
                row['diferencia'] = abs(clmag_consumption - row['clinme_consumption'])

                super(GiscedataProfilesAggregationsValidations, self).create(
                    cursor, uid, row
                )
        perf_lot_obj.browse(cursor, uid, vals['lot_id']).write(
            {'ultima_comprovacio_consums': datetime.now().strftime(
                '%d-%m-%Y %H:%M:%S'
            )}
        )


GiscedataProfilesAggregationsValidations()
