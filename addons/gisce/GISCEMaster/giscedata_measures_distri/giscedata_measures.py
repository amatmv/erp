# -*- coding: utf-8 -*-
from __future__ import absolute_import
import pandas as pd
import numpy as np
from base64 import b64encode, b64decode
from datetime import datetime, timedelta
from enerdata.profiles import Dragger
import StringIO
from osv import osv, fields
from oorq.decorators import job
from oorq.oorq import JobsPool
from base_extended.base_extended import MultiprocessBackground, NoDependency
import os
import tempfile
import netsvc
import logging
from tools.translate import _
from giscedata_measures.utils import last_sunday
from giscedata_measures_distri.headers import CLMAG5A_HEADER, CLINME_HEADER, AGGREGATION
import six


MAGNITUDS = {'AE': 'A', 'R1': 'R', 'active_input': 'AE'}

TIPOS = [('1', 'Tipo 1'),
         ('2', 'Tipo 2'),
         ('3', 'Tipo 3'),
         ('4', 'Tipo 4'),
         ('5', 'Tipo 5')]

MEASURE_TYPE_FROM_PROFILE_TO_REPORT = {
    'AE': 'active_input',
    'R1': 'reactive_quadrant1',
    'R4': 'reactive_quadrant4'
}

MEASURE_TYPE_FROM_TG_PROFILE_TO_REPORT = {
    'ai': 'active_input',
    'r1': 'reactive_quadrant1',
    'r4': 'reactive_quadrant4'
}

INDICADOR_MESURA = {
    '31': 'A',
    '30': 'B',
    '21': 'B',
    '2A': 'B'
}

TECHNOLOGY_TYPE = {
    'tg': ('smmweb', 'prime'),
    'tm': ('electronic', 'telemeasure')
}

HOUR_CHANGE_MONTHS = [3]


def patch_date_to_complete_period(start_date, end_date):
    """
    Get first and last tg.profile slot timestamp from a period.

    :param start_date: first date of the period LIKE 'YYYY-MM-DD'
    :param end_date: last date of the period LIKE 'YYYY-MM-DD'
    :return: two str date_periods LIKE 'YYYY-MM-DD HH:mm:ss'
    """

    di = "{0} 01:00:00".format(start_date)
    df = "{0} 00:00:00".format(
        (datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
         ).strftime("%Y-%m-%d")
    )

    return di, df


class GiscedataProfilesLot(osv.osv):

    _name = 'giscedata.profiles.lot'

    def assign_batch_invoices(self, cursor, uid, id, context=None):
        """Assign invoices to profiling batch
        """
        if not context:
            context = {}

        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        batch_id = id[0]
        batch = self.read(cursor, uid, batch_id)
        di = batch['data_inici']
        df = batch['data_final']
        invoice_ids = invoice_obj.search(cursor, uid, [
            ('data_inici', '>=', di),
            ('data_final', '<=', df),
            ('polissa_tg', '!=', "1")
        ])
        invoice_obj.write(
            cursor, uid, invoice_ids, {'lot_perfilacio': batch_id},
            context=context
        )

    def unassign_batch_invoices(self, cursor, uid, id, context=None):
        """Unassign invoices from profiling batch
        """
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        batch_id = id[0]
        invoice_ids = invoice_obj.search(
            cursor, uid, [('lot_perfilacio', '=', batch_id)]
        )
        invoice_obj.write(
            cursor, uid, invoice_ids, {'lot_perfilacio': False}, context=context
        )

    def profiling_button(self, cursor, uid, id, context=None):
        """Profiling button
        """
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        inv_lot_obj = self.pool.get('giscedata.profiles.factura.lot')
        lot_id = id[0]
        inv_ids = inv_lot_obj.search(cursor, uid, [
            ('lot_id', '=', lot_id), ('state', '=', 'draft'),
            ('type', '=', 'perfil')
        ])
        for factura in inv_lot_obj.read(cursor, uid, inv_ids, ['factura_id']):
            factura_id = factura['factura_id'][0]
            invoice_obj.encua_perfilacio(cursor, uid, [factura_id])

    def get_dates_from_lot(self, cursor, uid, ids):
        """ Return init_date and end_date of batch
        :param ids: batch_id
        :return: (str di, str df)
        """
        dates = self.read(cursor, uid, ids, ['data_inici', 'data_final'])[0]

        return dates['data_inici'], dates['data_final']

    def _ff_n_polisses(self, cursor, uid, ids, field_name, _args, context=None):
        """Number of policies from profiling batch
        """
        res = {}
        contracte_lot_obj = self.pool.get('giscedata.profiles.contracte.lot')
        for batch_id in ids:
            res[batch_id] = contracte_lot_obj.search_count(
                cursor, uid, [('lot_id', '=', batch_id)]
            )
        return res

    def _ff_n_factures(self, cursor, uid, ids, field_name, _args, context=None):
        """ Count n invoices from measures batch
        """
        res = {}
        factura_lot_obj = self.pool.get('giscedata.profiles.factura.lot')
        for batch_id in ids:
            res[batch_id] = factura_lot_obj.search_count(
                cursor, uid, [('lot_id', '=', batch_id)]
            )
        return res

    def update_contracts_progress(self, cursor, uid, ids, context=None):
        """Update progres"""
        cl_obj = self.pool.get('giscedata.profiles.contracte.lot')
        pol_obj = self.pool.get('giscedata.polissa')
        for lot in self.browse(cursor, uid, ids):
            total = lot.n_polisses
            pol_ids = cl_obj.search(cursor, uid, [('lot_id', '=', lot.id)])
            search_params = [('id', 'in', pol_ids), ('data_ultima_lectura_perfilada', '>=', lot.data_inici)]
            perfilats = pol_obj.search_count(cursor, uid, search_params) * 1.0
            try:
                progres = (perfilats / total) * 100
            except ZeroDivisionError:
                progres = 0.0
            finally:
                lot.write({'progres': progres})

    def update_profiling_progress(self, cursor, uid, ids, context=None):
        """ Set update profiling progres
        Check that the invoices for the period have a profile with search_count
        """
        inv_lot_obj = self.pool.get('giscedata.profiles.factura.lot')
        for lot in self.browse(cursor, uid, ids):
            search_vals = [
                ('type', '=', 'perfil'), ('lot_id', '=', lot.id),
            ]
            perfilades = inv_lot_obj.search_count(
                cursor, uid, search_vals + [('state', '=', 'done')]
            )
            total = inv_lot_obj.search_count(
                cursor, uid, search_vals
            )
            try:
                progres = (float(perfilades) / float(total)) * 100
            except ZeroDivisionError:
                progres = 0.0
            finally:
                lot.write({'progres': progres})

    def update_tm_tg_progress(self, cursor, uid, ids, context=None):
        """ Set update telemeasure or telegestio progres curve
        Check that the invoices for the period have a cch_available
        :param ids: measures_period_ids
        :param context: need pass {'origin': 'tm' or 'tg'}
        """
        inv_lot_obj = self.pool.get('giscedata.profiles.factura.lot')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        origin = context.get('origin', [])
        if not origin:
            return False
        for lot in self.browse(cursor, uid, ids):
            search_vals = [
                ('type', '=', origin), ('lot_id', '=', lot.id),
            ]
            tm_invoice_ids = inv_lot_obj.search(
                cursor, uid, search_vals + [('state', '=', 'draft')]
            )
            for factura_id in inv_lot_obj.read(
                    cursor, uid, tm_invoice_ids, ['factura_id']
            ):
                if factura_obj.read(
                        cursor, uid, factura_id['factura_id'][0],
                        ['cch_fact_available']
                )['cch_fact_available']:
                    # Update state
                    inv_lot_obj.write(
                        cursor, uid, factura_id['id'], {'state': 'done'}
                    )
            # Update progress
            availables = inv_lot_obj.search_count(
                cursor, uid, search_vals + [('state', '=', 'done')]
            )
            total = inv_lot_obj.search_count(
                cursor, uid, search_vals
            )
            try:
                progres = (float(availables) / float(total)) * 100
            except ZeroDivisionError:
                progres = 0.0
            finally:
                key = origin + '_progress'
                lot.write({key: progres})

    def default_distribuidora(self, cursor, uid, context=None):
        """ Default distributor
        :return: distributor as str
        """
        users_obj = self.pool.get('res.users')
        user = users_obj.browse(cursor, uid, uid)
        return user.company_id.partner_id.ref

    def get_previous_lot(self, cursor, uid, ids, context=None):
        """ Get previous measures batch
        """
        if isinstance(ids, list):
            ids = ids[0]
        lot = self.read(cursor, uid, ids, ['data_inici'])
        previous_lot = self.search(
            cursor, uid, [('data_final', '<', lot['data_inici'])],
            limit=1, order='data_final desc'
        )

        if previous_lot:
            return previous_lot
        else:
            return False

    def get_meters_on_dates(self, cursor, uid, polissa, di, df, context=None):
        """
        Return a meters from contract by period.
        :param polissa: int pol_id or str pol_name
        :param di: str date
        :param df: str date
        :param context:
        'only_active': if not specified reads all, active and inactive
        'read_params': list of strs to read, if not specified return a list of ids
        :return: meter info
        """
        if not context:
            context = {}
        if isinstance(polissa, six.string_types):
            pol_obj = self.pool.get('giscedata.polissa')
            polissa = pol_obj.search(cursor, uid, [('name', '=', polissa)], context={'active_test': False})

        read_params = context.get('read_params', ['id'])
        only_active = context.get('only_active', False)
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        res = meter_obj.q(cursor, uid).read(
            read_params, only_active=only_active
        ).where(
            [
                ('polissa', '=', polissa),
                ('data_alta', '<=', df),
                '|',
                ('data_baixa', '>=', di),
                ('data_baixa', '=', None)
            ]
        )
        if read_params == ['id']:
            res = [x['id'] for x in res]
        return res

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def make_clmag5a(self, cursor, uid, ids, context=None):
        """
        Create a CLMAG5A file

        :param ids: batch_id
        :param context: Can pass a dict with the aggregation to generate it
        LIKE: context=
        {
            'comercialitzadora': comer, (required)
            'agree_tensio': voltage,
            'agree_tarifa': tariff,
            'agree_dh': dh,
            'agree_tipus': type
        }
        """
        clmag5a_obj = self.pool.get('giscedata.profiles.clmag5a')
        if isinstance(ids, int):
            ids = [ids]

        dates = self.read(cursor, uid, ids, ['data_inici', 'data_final'])[0]
        di = dates['data_inici']
        df = dates['data_final']

        _args = clmag5a_obj.gen_clmag5a(
            cursor, uid, ids[0], di, df, context=context
        )
        self.attach_measures_file_to_lot(
            cursor, uid, ids, _args, context=None)

        return True

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def make_clmag(self, cursor, uid, ids, context=None):
        """
        Create a CLMAG file

        :param ids: batch_id
        :param context: Can pass a dict with the aggregation to generate it
        LIKE: context=
        {
            'comercialitzadora': comer, (required)
            'agree_tensio': voltage,
            'agree_tarifa': tariff,
            'agree_dh': dh,
            'agree_tipus': type
        }
        """

        clmag_obj = self.pool.get('giscedata.profiles.clmag')
        if isinstance(ids, int):
            ids = [ids]

        dates = self.read(cursor, uid, ids, ['data_inici', 'data_final'])[0]
        di = dates['data_inici']
        df = dates['data_final']

        _args = clmag_obj.gen_clmag(cursor, uid, ids[0], di, df, context)
        self.attach_measures_file_to_lot(cursor, uid, ids, _args, context=None)

        # Create aggs
        # Todo: integrate with gen_aggregations function
        aggregations_obj = self.pool.get('giscedata.profiles.aggregations')
        df_clmag = self.check_clmag(cursor, uid, ids, context)
        df_clmag = df_clmag .rename(
            index=str, columns={"clmag_consumption": "consum"})
        df_clmag = df_clmag[df_clmag.magnitud != 'R1']

        # DF to dict
        profiles = df_clmag.T.to_dict().values()
        # Preventive delete and aggregations create
        for agg in profiles:
            agg['lot_id'] = ids[0]
            aggregations_obj.create(cursor, uid, agg, context)

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def make_clinme(self, cursor, uid, ids, context=None):
        """
        Create a CLINME files by comer, and attach file to batch

        :param ids: batch_id
        :param context: Can pass a dict with the aggregation to generate it
        LIKE: context=
        {
            'comercialitzadora': comer, (required)
            'agree_tensio': voltage,
            'agree_tarifa': tariff,
            'agree_dh': dh,
            'agree_tipus': type
        }
        """
        if not context:
            context = {}

        clinme_obj = self.pool.get('giscedata.profiles.clinme')
        if isinstance(ids, int):
            ids = [ids]

        dates = self.read(cursor, uid, ids, ['data_inici', 'data_final'])[0]
        di = dates['data_inici']
        df = dates['data_final']

        comers = context.get('comercialitzadora', None)
        if not comers:
            cursor.execute("""SELECT DISTINCT partner.ref
                    FROM giscedata_polissa_modcontractual modcon
                    INNER JOIN res_partner partner ON
                    modcon.comercialitzadora=partner.id""")
            comers = cursor.fetchall()
            comers = [comer[0] for comer in comers]  # get comer name
        else:
            comers = [comers]
        clinme_obj.gen_clinme(
            cursor, uid, ids[0], di, df, comers, context
        )

    def make_agcl(self, cursor, uid, ids, context=None):
        """
        Create a AGCL file and atach file to batch

        :param ids: batch_id
        """
        agcl_obj = self.pool.get('giscedata.profiles.agcl')
        if isinstance(ids, int):
            ids = [ids]

        _args = agcl_obj.gen_agcl(cursor, uid, ids[0], context=context)
        self.attach_measures_file_to_lot(cursor, uid, ids, _args, context=None)

    def make_aggregations(self, cursor, uid, ids, context=None):
        """Generate a aggregations from batch
        """
        aggregations_obj = self.pool.get('giscedata.profiles.aggregations')
        if isinstance(ids, int):
            ids = [ids]
        di, df = self.get_dates_from_lot(cursor, uid, ids)
        context['data_inici'] = di
        context['data_final'] = df
        # No pass data to create new aggregations
        aggregations_obj.gen_aggregations(cursor, uid, [], ids[0], context)

    @MultiprocessBackground.background(queue="enqueue_measures", skip_check=True)
    def enqueue_measures_file(self, cursor, uid, lot_id, file_name, aggs, context=None):
        """
        Enqueue measures file
        """
        logger_measures = logging.getLogger('openerp.{}'.format(__name__))

        j_pool = JobsPool()
        with NoDependency():
            for agg in aggs:
                j_pool.add_job(self.gen_measures_file(cursor, uid, lot_id, file_name, context=agg))

        j_pool.join()
        lot_obj = self.pool.get('giscedata.profiles.lot')
        logger_measures.info('Join measures files {}'.format(file_name))
        if file_name.lower() != 'clinme':
            temp_file = tempfile.mkstemp()[1]
            for result in j_pool.results.values():
                with open(result['filepath'], "rb") as file_read:
                    content = file_read.read()
                with open(temp_file, "a") as file_write:
                    file_write.write(content)
                try:
                    os.unlink(result['filepath'])
                except Exception:
                    continue
            result.update({'filepath': temp_file})
            logger_measures.info('end of measures file/s {}'.format(result))
            lot_obj.attach_measures_file_to_lot(cursor, uid, lot_id, result)
        else:
            by_comer = {}
            for res in j_pool.results.values():
                for result in res:
                    comer = result['comer']
                    if comer not in by_comer.keys():
                        by_comer[comer] = []
                    by_comer[comer].append(result)
            logger_measures.info('end of measures file/s {}'.format(by_comer))
            for comer in by_comer.values():
                temp_file = tempfile.mkstemp()[1]
                for result in comer:
                    with open(result['filepath'], "rb") as file_read:
                        content = file_read.read()
                    with open(temp_file, "a") as file_write:
                        file_write.write(content)
                    try:
                        os.unlink(result['filepath'])
                    except Exception:
                        continue
                result.update({'filepath': temp_file})
                lot_obj.attach_measures_file_to_lot(cursor, uid, lot_id, result)
        return True

    @job(queue='low', timeout=4*3600)
    def gen_measures_file(self, cursor, uid, lot_id, file_name, context=None):
        """
        Gen a measures file by one or more aggeregations
        :param lot_id: a batch_id
        :param file_name: clmag, clinme...
        :param context: {aggregation}
        :return: dict with file info
        """
        if isinstance(lot_id, (list, tuple)):
            lot_id = lot_id[0]

        lot_obj = self.pool.get('giscedata.profiles.lot')
        file_obj = self.pool.get('giscedata.profiles.{}'.format(file_name.lower()))
        func = getattr(file_obj, 'gen_{}'.format(file_name.lower()))

        lot = lot_obj.browse(cursor, uid, lot_id)
        if not context or not 'comercialitzadora' in context:
            raise osv.except_osv(_('Error'), _('No aggregation defined'))
        if func:
            comercialitzadora = context.get('comercialitzadora', [])
            context.update({'filename': tempfile.mkstemp()[1], 'comers': [comercialitzadora], 'by_agg': True})
            res = func(
                cursor, uid, lot_id=lot.id, data_inici=lot.data_inici,
                data_final=lot.data_final, context=context
            )
            return res
        else:
            raise NotImplementedError()

    def _get_related_attachments(self, cursor, uid, ids, field_name, arg,
                                 context=None):
        """Get the related attachments measures by batch
        """
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        attach_obj = self.pool.get('ir.attachment')
        for batch in self.read(cursor, uid, ids, ['id'], context=context):
            attach_ids = attach_obj.search(
                cursor, uid, [('res_id', '=', batch['id']),
                              ('res_model', '=', 'giscedata.profiles.lot')]
            )
            res[batch['id']] = attach_ids

        return res

    def attach_measures_file_to_lot(self, cursor, uid, ids, args, context=None):
        """
        Create an attachment related to lot with measure file
        :param cursor:
        :param uid:
        :param ids:
        :param args: LIKE: {
                        'filepath': absolute file path in disk',
                        'type': file_type like CLMAG5A,
                        'distri': str - distributor code,
                        'comer': str - comer code (optional),
                        'period': str - YYYYMM,
                        'data': str - YYYYMMDD,
                        'version': file_version
                    }
        :param context:
        :return: True
        """
        attach_obj = self.pool.get('ir.attachment')

        # Generate file
        if 'comer' in args:
            file_name = '{type}_{distri}_{comer}_{periode}_{data}.{version}'.format(**args)
        elif 'periode' not in args:
            file_name = '{type}_{distri}_{data}.{version}'.format(**args)
        else:
            file_name = '{type}_{distri}_{periode}_{data}.{version}'.format(**args)

        check_version = file_name.split('.')[0] + '_%'
        attach_ids = attach_obj.search(
            cursor, uid, [('datas_fname', '=like', check_version)]
        )
        if attach_ids:
            versions = [att['datas_fname'] for att in attach_obj.read(
                cursor, uid, attach_ids, ['datas_fname']
            )]
            version = int(max(versions).split('.')[1])
            version += 1
            file_name = file_name.split('.')[0] + '.' + str(version)

        with open(args['filepath'], "rb") as _file:
            content = b64encode(_file.read())

        if isinstance(ids, int or float):
            ids = [ids]

        vals = {
            'name': file_name,
            'datas_fname': file_name,
            'res_id': ids[0],
            'res_model': 'giscedata.profiles.lot',
            'datas': content,
        }
        attach_obj.create(cursor, uid, vals)

        return True

    def _cronjob_create_measures_periods(self, cursor, uid, ids, context=None):
        wiz_obj = self.pool.get('wizard.crear.lots.mesures')
        next_year = datetime.now().year + 1
        wiz_id = wiz_obj.create(cursor, uid, {'year': next_year})
        wiz = wiz_obj.browse(cursor, uid, wiz_id, context={})
        try:
            wiz.action_crear_lots()
        except osv.except_osv as err:
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            return str(err.message)
        return True

    _columns = {
        'name': fields.char(
            'Període de Mesures', size=8, required=True, readonly=True
        ),
        'data_inici': fields.date('Data inici', required=True, readonly=True),
        'data_final': fields.date('Data final', required=True, readonly=True),
        'aggregations_ids': fields.one2many(
            'giscedata.profiles.aggregations', 'lot_id', 'Nivells Agregació'
        ),
        'aggr_ree_ids': fields.one2many('giscedata.profiles.ree.aggregations',
                                        'lot_id', 'Nivells d\'agregació REE'),
        'aggregations_val_ids': fields.one2many(
            'giscedata.profiles.aggregations.validations', 'lot_id',
            'Comprovació de consums'
        ),
        'related_attachments': fields.function(
            _get_related_attachments,
            method=True,
            string='Fitxers generats',
            type='one2many',
            relation='ir.attachment'
        ),
        'n_polisses': fields.function(_ff_n_polisses, method=True,
                                      string="Número pòlisses", store=False,
                                      type='integer'),
        'n_factures': fields.function(_ff_n_factures, method=True,
                                      string="Número factures", store=False,
                                      type='integer'),
        'progres': fields.float('Progrés de Perfilació', readonly=True),
        'tm_progress': fields.float('Progrés de Telemesura', readonly=True),
        'tg_progress': fields.float('Progrés de Telegestió', readonly=True),
    }

    _defaults = {
        'n_polisses': lambda *a: 0,
        'n_factures': lambda *a: 0,
        'progres': lambda *a: 0.0,
        'tm_progress': lambda *a: 0.0,
        'tg_progress': lambda *a: 0.0,
    }

GiscedataProfilesLot()


class GiscedataProfilesContracteLot(osv.osv):
    """
    Cadascuna de les pòlisses a perfilar dins d'un lot.
    """

    _name = 'giscedata.profiles.contracte.lot'
    _rec_name = 'polissa_id'

    _states_selection = [
        ('draft', 'Esborrany'),
        ('done', 'Finalitzat')
    ]

    _origen_real_selection = [
        ('sense_determinar', 'Sense Determinar'),
        ('corba', 'Corba'),
        ('perfil', 'Perfil')
    ]

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True),
        'lot_id': fields.many2one('giscedata.profiles.lot', 'Lot',
                                  required=True),
        'metode': fields.char('Tipus', size=25, required=True),
        'state': fields.selection(_states_selection, 'Estat', size=32,
                                  required=True, readonly=True),
    }

    _defaults = {
        'metode': lambda *a: 'perfil',
        'state': lambda *a: 'draft',
    }

GiscedataProfilesContracteLot()


class GiscedataProfilesFacturaLot(osv.osv):
    """
    Cadascuna de les factures a perfilar dins d'un període de mesures.
    """

    _name = 'giscedata.profiles.factura.lot'
    _rec_name = 'factura_id'

    _type_selection = [
        ('tg', 'Telegestió'),
        ('perfil', 'Perfil'),
        ('tm', 'Telemesura')
    ]

    _states_selection = [
        ('draft', 'Esborrany'),
        ('done', 'Finalitzat')
    ]

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        """
        Busca factures de client en el periode de mesures per nom number de
        factura
        via erppeek: factura_id, '=', 1
        via erp: factura_id, 'ilike', '%F00'
        """
        new_args = []
        for arg in args:
            key = arg[0]
            operator = arg[1]
            res = arg[2]
            if key == 'factura_id' and operator != '=' and isinstance(res, str):
                new_args.append(('factura_id.number', operator, res))
                new_args.append(
                    ('factura_id.type', 'in', ['out_invoice', 'out_refund'])
                )
            else:
                new_args.append(arg)

        return super(GiscedataProfilesFacturaLot,self).search(
            cursor, uid, new_args, offset=offset, limit=limit, order=order,
            context=context, count=count
        )

    _columns = {
        'factura_id': fields.many2one('giscedata.facturacio.factura', 'Factura',
                                      required=True, ondelete='cascade'),
        'lot_id': fields.many2one('giscedata.profiles.lot', 'Lot',
                                  required=True),
        'type': fields.selection(_type_selection, 'Tipus', size=32,
                                 required=True, readonly=True),
        'state': fields.selection(_states_selection, 'Estat', size=32,
                                  required=True, readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'draft',
    }


GiscedataProfilesFacturaLot()


class GiscedataProfilesCLMAG(osv.osv):
    _name = 'giscedata.profiles.clmag'

    def include_hour_change(self, aux):
        """
        Add summer hour change 2 to 3
        :param aux: dict with content, modified by mutability
        :return:
        """
        aux['timestamp'] = aux['timestamp'].replace('03:00', '03:30')
        aux['ai_fact'] = 0.0
        aux['registrador'] = 0.0
        aux['r1'] = 0.0
        aux['registrador_count'] = 0

    def gen_clmag(self, cursor, uid, lot_id, data_inici, data_final,
                  context=None):
        """
        Generate a CLMAG file from aggregated DataFrame with Profiles,
        TG Profiles and TM Profiles.
        create a dataset with aggregated key {(agg_key): measures}, and write
        this data.
        Can pass a dict with aggregation params to create a file by aggregation

        :param lot_id: int - measures batch id
        :param data_inici: str - start file data
        :param data_final: str - end file data
        :param context: by aggregation example:
        {
            'comercialitzadora': 9999, 'agree_tensio': 'E0',
            'agree_tarifa': '30', 'agree_dh': 'E3', 'agree_tipo': '03',
            'provincia': 'AA'
        }
        :return: CLMAG attachment args LIKE:
        {
            'filename': str - file_name, 'distri': str - distributor code,
            'comer': str - comer code, 'period': str - YYYYMM,
            'data': str - YYYYMMDD, 'version': str - file_version
        }
        """

        if context is None:
            context = {}

        tm_obj = self.pool.get('tm.profile')
        tg_obj = self.pool.get('tg.profile')
        profiles_obj = self.pool.get('giscedata.profiles.profile')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        lot_obj = self.pool.get('giscedata.profiles.lot')
        pol_obj = self.pool.get('giscedata.polissa')
        _sorted = False

        logger = netsvc.Logger()
        logger_profiles = logging.getLogger('openerp.{}'.format(__name__))
        logger.notifyChannel(
            'objects', netsvc.LOG_INFO, 'Generating CLMAG from {} to {}'.format(
                data_inici, data_final
            )
        )
        hour_change_detected = False
        month_ = int(data_inici[5:7])
        year_ = int(data_inici[:4])
        if month_ in HOUR_CHANGE_MONTHS:
            day_change = last_sunday(year_, month_)
            day_change = '{year}{month}{day} 03'.format(
                year=str(year_), month=str(month_).zfill(2), day=str(day_change).zfill(2)
            )
        else:
            day_change = None

        distribuidora_name = lot_obj.default_distribuidora(cursor, uid, context)

        from osv.expression import OOQuery
        where_params = [
            ('agree_tipus', 'in', ('03', '04')),
            ('data_inici', '<=', data_final),
            ('data_final', '>=', data_inici),
            ('polissa_id.state', '!=', 'cancelada'),
        ]
        # File by aggregation
        if 'comercialitzadora' in context:
            where_params.append(
                ('comercialitzadora.ref', '=', context['comercialitzadora'])
            )
            where_params.append(('agree_tensio', '=', context['agree_tensio']))
            where_params.append(('agree_tarifa', '=', context['agree_tarifa']))
            where_params.append(('agree_dh', '=', context['agree_dh']))
            where_params.remove(('agree_tipus', 'in', ('03', '04')))
            where_params.append(('agree_tipus', '=', context['agree_tipo']))
            where_params.append(
                ('cups.id_municipi.state.ree_code', '=', context['provincia'])
            )

        q = OOQuery(modcon_obj, cursor, uid)
        sql = q.select([
            'id',
            'agree_tarifa',
            'agree_tensio',
            'agree_dh',
            'agree_tipus',
            'cups.name',
            'comercialitzadora.ref',
            'cups.id_municipi.state.ree_code',
            'data_inici',
            'data_final',
            'polissa_id',
            'tg'
        ], only_active=False).where(where_params)
        cursor.execute(*sql)

        # Get policy amendments data
        data = []
        for modcon in cursor.dictfetchall():
            # Not RE tariffs
            if not modcon['agree_tarifa'] or modcon['agree_tarifa'] == 'RE':
                continue
            modcon['cups'] = modcon['cups.name']
            modcon['comercialitzadora'] = modcon['comercialitzadora.ref']
            modcon['distribuidora'] = distribuidora_name
            modcon['provincia'] = modcon['cups.id_municipi.state.ree_code']
            modcon['modcon_id'] = modcon['id']
            for magnitud in ('AE', 'R1'):
                modcon_aux = modcon.copy()
                modcon_aux['magnitud'] = magnitud

                data.append(modcon_aux)
        df_modcons = pd.DataFrame(data=data)

        # Get CLMAG_data
        data_tm = []
        total_modcons = len(data)
        for n_modcon, modcon in enumerate(data, start=1):
            magn = modcon['magnitud']
            start_date = max(modcon['data_inici'], data_inici)
            end_date = min(modcon['data_final'], data_final)
            meters = lot_obj.get_meters_on_dates(
                cursor, uid, modcon['polissa_id'], start_date, end_date, context={
                    'read_params': ['id', 'tg', 'data_alta', 'data_baixa', 'name', 'technology_type']
                }
            )

            for n_meter, meter in enumerate(meters, start=1):
                # TM Profiles
                if meter['technology_type'] in TECHNOLOGY_TYPE['tm']:
                    di = max(start_date, meter['data_alta'])
                    df = end_date
                    if meter['data_baixa']:
                        df = min(meter['data_baixa'], end_date)
                    di_ts, df_ts = patch_date_to_complete_period(di, df)
                    tm_ids = tm_obj.search(cursor, uid, [
                        ('timestamp', '>=', di_ts),
                        ('timestamp', '<=', df_ts),
                        ('name', '=', meter['name']),
                        ('cch_fact', '=', True),
                        ('type', '=', 'p')
                    ])
                    if not tm_ids:
                        continue
                    res = tm_obj.read(cursor, uid, tm_ids, [
                        'timestamp', 'ai_fact', 'kind_fact', 'r1',
                        'cch_bruta'
                    ])
                    for m in res:
                        m['magnitud'] = magn
                        m['modcon_id'] = modcon['modcon_id']
                        m['registrador_count'] = 1
                        if magn == 'R1':
                            if not m['cch_bruta']:
                                m['ai_fact'] = 0
                                m['registrador'] = 0
                            else:
                                m['ai_fact'] = m['r1']
                                m['registrador'] = m['r1']
                        else:
                            m['ai_fact'] = m['ai_fact']
                            m['registrador'] = m['ai_fact']
                        if m['timestamp'][:13] == day_change:
                            aux = m.copy()
                            self.include_hour_change(aux)
                            hour_change_detected = aux['timestamp']
                            data_tm.append(aux)
                        data_tm.append(m)
                # TG Profiles
                elif (meter['technology_type'] in TECHNOLOGY_TYPE['tg']
                      and not (modcon['agree_tipus'] == '05' and modcon['tg'] != '1')):
                    dragger = Dragger()
                    reactive_dragger = Dragger()
                    meter_name = meter_obj.build_name_tg(
                        cursor, uid, meter['id']
                    )
                    di = max(meter['data_alta'], start_date)
                    df = end_date
                    if meter['data_baixa']:
                        df = min(meter['data_baixa'], end_date)
                    di_ts, df_ts = patch_date_to_complete_period(di, df)
                    search_params = [('name', '=', meter_name),
                                     ('cch_fact', '=', True),
                                     ('timestamp', '>=', di_ts),
                                     ('timestamp', '<=', df_ts)]
                    tg_ids = tg_obj.search(cursor, uid, search_params)
                    if not tg_ids:
                        continue
                    for res in tg_obj.read(cursor, uid, tg_ids, [
                        'timestamp', 'ai_fact', 'kind_fact', 'r1',
                        'cch_bruta', 'magn'
                    ]):
                        consumption = dragger.drag(
                            res['ai_fact'] * res['magn'] / 1000)
                        res['ai_fact'] = consumption
                        res['magnitud'] = magn
                        res['modcon_id'] = modcon['modcon_id']
                        res['registrador_count'] = 1
                        if magn == 'R1':
                            if not res['cch_bruta']:
                                res['ai_fact'] = 0
                                res['registrador'] = 0
                            else:
                                res['r1'] = reactive_dragger.drag(
                                    res['r1'] * res['magn'] / 1000)
                                res['ai_fact'] = res['r1']
                                res['registrador'] = res['r1']
                        else:
                            res['ai_fact'] = res['ai_fact']
                            res['registrador'] = res['ai_fact']
                        if res['timestamp'][:13] == day_change:
                            aux = res.copy()
                            self.include_hour_change(aux)
                            hour_change_detected = aux['timestamp']
                            data_tm.append(aux)
                        data_tm.append(res)
                # Profiles
                else:
                    di = max(meter['data_alta'], start_date)
                    df = end_date
                    if meter['data_baixa']:
                        df = min(meter['data_baixa'], end_date)
                    di_ts, df_ts = patch_date_to_complete_period(di, df)
                    perf_ids = profiles_obj.search(cursor, uid, [
                        ('cups', '=', modcon['cups']),
                        ('timestamp', '>=', di_ts),
                        ('timestamp', '<=', df_ts)])
                    if not perf_ids:
                        continue
                    for perf in profiles_obj.read(
                            cursor, uid, perf_ids, ['timestamp', 'lectura']
                    ):
                        perf['ai_fact'] = perf.pop('lectura')
                        perf['magnitud'] = magn
                        perf['modcon_id'] = modcon['modcon_id']
                        perf['registrador_count'] = 1
                        perf['r1'] = 0.0
                        if magn == 'R1':
                            perf['ai_fact'] = 0
                            perf['registrador'] = 0
                        else:
                            perf['registrador'] = perf['ai_fact']
                        if perf['timestamp'][:13] == day_change:
                            aux = perf.copy()
                            self.include_hour_change(aux)
                            hour_change_detected = aux['timestamp']
                            data_tm.append(aux)
                        data_tm.append(perf)
            logger_profiles.info('CLMAG Generation: Modcon #{} of {}'.format(n_modcon, total_modcons))

        df_profiles = pd.DataFrame(data=data_tm).rename(
            columns={'ai_fact': 'measure'})

        df_profiles['timestamp_patched'] = df_profiles['timestamp'].astype('datetime64[ns]') - timedelta(hours=1)
        df_profiles['measure_day'] = df_profiles['timestamp_patched'].dt.day
        df_profiles['measure_month'] = df_profiles['timestamp_patched'].dt.month
        df_profiles['measure_year'] = df_profiles['timestamp_patched'].dt.year

        # New field registrador with measure if field fact == 1 (REAL)
        # Np were condifion, value if, value else
        # Registrador_count a 1
        """df_profiles['registrador_count'] = np.where(
            df_profiles['kind_fact'] == 1, 1, 1
        )"""

        df = pd.merge(
            df_modcons, df_profiles, on=['magnitud', 'modcon_id'], how='inner'
        )

        df_aggregated = df.groupby(
            ["measure_day", "measure_month", "measure_year", "agree_dh",
             "agree_tarifa", "agree_tensio", "agree_tipus", "timestamp_patched",
             "comercialitzadora", "distribuidora", "magnitud",
             "provincia"]).aggregate(
            {'measure': 'sum', 'modcon_id': 'count', 'registrador': 'sum',
             'registrador_count': 'sum'})
        df_aggregated.reset_index(inplace=True)
        df_aggregated.measure = df_aggregated.measure.astype(int)
        df_aggregated.registrador = df_aggregated.registrador.astype(int)

        aggregation = ["measure_day", "measure_month", "measure_year", "distribuidora",
             "comercialitzadora", "provincia", "agree_tensio", "agree_tarifa",
             "agree_dh", "agree_tipus", "magnitud"]
        grouped = df_aggregated.groupby(aggregation)

        hours_by_row = 24
        values_to_sort = ['timestamp_patched', 'measure', 'modcon_id', 'registrador',
                          'registrador_count']
        elements_to_integrate = values_to_sort[1:]
        if hour_change_detected:
            # Adapt hour_change_detected - 1 hour
            hour_change_detected = (
                datetime.strptime(hour_change_detected, '%Y-%m-%d %H:%M:%S')
                - timedelta(hours=1)
            ).strftime('%Y-%m-%d %H:%M:%S')

        # Write the file
        file_name = context.get('filename', '/tmp/CLMAG')
        with open(file_name, 'w') as f:
            for name, group in grouped:
                group_key = name
                measures_df = group[values_to_sort].sort_values(
                    by=['timestamp_patched'], ascending=True)
                for key in ('measure', 'modcon_id', 'registrador', 'registrador_count'):
                    measures_df.loc[measures_df['timestamp_patched'] == hour_change_detected, key] = 0

                number_of_hours = len(measures_df)
                if number_of_hours != hours_by_row:
                    _msg = _('Group {group_key} do not have {hours_by_row} hours (#{number_of_hours})!!').format(group_key=group_key, hours_by_row=hours_by_row, number_of_hours=number_of_hours)
                    logger.notifyChannel('objects', netsvc.LOG_INFO, _msg)

                group_elements = []
                for element in elements_to_integrate:
                    group_elements.append(list(measures_df[element]))

                final_grouped = zip(*group_elements)

                # TODO handle hour 25!!
                hour25 = ";0;0;0;0"

                # Parse date zfill
                group_key_parse = []
                for i, elem in enumerate(group_key):
                    if (i == 0 or i == 1) and len(str(elem)) == 1:
                        elem = str(elem).zfill(2)
                    group_key_parse.append(elem)
                group_key = tuple(group_key_parse)

                line = (str(group_key) + str(final_grouped)).replace("u'","").replace("'", "").replace("(","").replace(")", "").replace("[", ";").replace("]", "").replace(',',';').replace('"', '').replace(' ', '') + hour25 + ';\n'
                f.write(line)

        # Sort CLMAG
        if _sorted:
            blocks = 4
            aggregation += [str(x[1]) + str(x[0]) for x in enumerate(blocks * range(25))]
            aggregation.append('nan')

            df_clmag = pd.read_csv(
                file_name, sep=';', names=aggregation,
                dtype={'distribuidora': 'str', 'comercialitzadora': 'str', 'agree_tipus': 'str', 'measure_day': 'str',
                       'measure_month': 'str'}
            )
            df_clmag.drop('nan', axis=1, inplace=True)
            df_clmag = df_clmag.sort_values([
                'comercialitzadora', 'agree_tensio', 'agree_tarifa', 'agree_dh', 'agree_tipus', 'measure_day',
                'measure_month', 'measure_year'
            ])
            df_clmag.to_csv(file_name, sep=';', header=False, columns=aggregation, index=False, line_terminator=';\n')

        periode = data_inici.replace('-', '')[:6]
        res = {
            'type': 'CLMAG',
            'filepath': file_name,
            'distri': distribuidora_name,
            'periode': periode,
            'data': datetime.now().strftime('%Y%m%d'),
            'version': 0
        }

        return res

    _columns = {

    }


GiscedataProfilesCLMAG()


class GiscedataProfilesCLMAG5A(osv.osv):
    _name = 'giscedata.profiles.clmag5a'

    def chunks(self, _list, n):
        """Yield successive n-sized chunks from _list."""
        for i in range(0, len(_list), n):
            yield _list[i:i + n]

    def read_policy_amendments(self, cursor, uid, data_inici, data_final,
                               context=None):
        """ Read policy amendments for dates and get measures data
        :return: a report data with measures data info
        """
        if context is None:
            context = {}

        start = datetime.now()
        logger = netsvc.Logger()
        logger_profiles = logging.getLogger('openerp.{}'.format(__name__))
        modcontractual_obj = self.pool.get('giscedata.polissa.modcontractual')
        users_obj = self.pool.get('res.users')
        user = users_obj.browse(cursor, uid, uid)
        distribuidora = user.company_id.partner_id.ref

        where_params = [
            ('potencia', '<=', 15),
            ('agree_tarifa', 'in', ('21', '2A')),
            ('data_inici', '<=', data_final),
            ('data_final', '>=', data_inici),
            ('polissa_id.state', '!=', 'cancelada'),
        ]
        # File by aggregation
        if 'comercialitzadora' in context:
            where_params.append(
                ('comercialitzadora.ref', '=', context['comercialitzadora'])
            )
            where_params.append(('agree_tensio', '=', context['agree_tensio']))
            where_params.append(('agree_tarifa', '=', context['agree_tarifa']))
            where_params.append(('agree_dh', '=', context['agree_dh']))
            where_params.append(('agree_tipus', '=', context['agree_tipo']))
            where_params.append(
                ('cups.id_municipi.state.ree_code', '=', context['provincia'])
            )
            where_params.remove(('agree_tarifa', 'in', ('21', '2A')))
            where_params.append(('agree_tarifa', '=', context['agree_tarifa']))

        from osv.expression import OOQuery

        q = OOQuery(modcontractual_obj, cursor, uid)
        sql = q.select([
            'id',
            'data_inici',
            'data_final',
            'polissa_id',
            'cups.name',
            'comercialitzadora.ref',
            'agree_tipus',
            'agree_dh',
            'agree_tarifa',
            'agree_tensio',
            'cups.id_municipi.state.ree_code',
            'tg'
        ], only_active=False).where(where_params)

        data = {}
        cursor.execute(*sql)
        logger.notifyChannel('objects', netsvc.LOG_INFO,
                             """Generating CLMAG5A from {di} to {df}. #Pols:
                             {n_pol}""".format(di=data_inici, df=data_final,
                                               n_pol=cursor.rowcount))
        for n_modcons, modcon in enumerate(cursor.dictfetchall(), start=1):
            # Not RE tariffs
            if not modcon['agree_tarifa'] or modcon['agree_tarifa'] == 'RE':
                continue
            if modcon['tg'] == '1':
                fields = {'season': 'season'}
                measures_data = self.read_tg_profiles(cursor, uid, modcon, data_inici, data_final)
            else:
                fields = {'season': 'estacio'}
                measures_data = self.read_profiles(cursor, uid, modcon, data_inici, data_final)

            for measure in measures_data:
                key = (
                    distribuidora,
                    modcon['comercialitzadora.ref'],
                    modcon['agree_tensio'],
                    modcon['agree_tarifa'],
                    modcon['agree_dh'],
                    modcon['agree_tipus'],
                    modcon['cups.id_municipi.state.ree_code'],
                    measure['timestamp'],
                    measure[fields['season']],
                    MAGNITUDS['active_input']
                )
                if key not in data:
                    data[key] = {}
                    data[key]['consum'] = measure['lectura']
                    data[key]['n_punts'] = 1
                    data[key]['m_real'] = measure['m_real']
                    data[key]['n_real'] = measure['n_real']
                    data[key]['m_estimada'] = measure['m_estimada']
                    data[key]['n_estimada'] = measure['n_estimada']
                else:
                    data[key]['consum'] += measure['lectura']
                    data[key]['n_punts'] += 1
                    data[key]['m_real'] += measure['m_real']
                    data[key]['n_real'] += measure['n_real']
                    data[key]['m_estimada'] += measure['m_estimada']
                    data[key]['n_estimada'] += measure['n_estimada']
            logger_profiles.info(
                'CLMAG5A Generation: End modcon {}, #{}'.format(modcon['cups.name'], n_modcons))

        logger.notifyChannel('objects', netsvc.LOG_INFO,
                             """Dump policy amendments and TG data time:
                             {}""".format(datetime.now() - start))

        return data

    def read_tg_profiles(self, cursor, uid, modcon, data_inici, data_final, context=None):
        """ Read TG Profiles for dates, check estimated and real consumptions
        :return: TG Profiles data
        """
        tg_profiles_obj = self.pool.get('tg.profile')
        logger_profiles = logging.getLogger('openerp.{}'.format(__name__))
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if modcon['data_inici'] > data_inici:
            start_date = modcon['data_inici']
        else:
            start_date = data_inici
        if modcon['data_final'] < data_final:
            end_date = modcon['data_final']
        else:
            end_date = data_final

        from osv.expression import OOQuery
        q = OOQuery(meter_obj, cursor, uid)
        sql = q.select(
            ['id', 'tg', 'data_alta', 'data_baixa'], only_active=False
        ).where([
            ('polissa', '=', modcon['polissa_id']),
            ('data_alta', '<=', end_date),
            '|',
            ('data_baixa', '>=', start_date),
            ('data_baixa', '=', None)
        ])
        cursor.execute(*sql)

        if not cursor.rowcount:
            # polissa sense comptador per aquest periode (di, df)
            msg_err = _("""Polissa sense comptador per aquest periode {di},
            {df}""").format(di=end_date, df=start_date)
            logger_profiles.info(msg_err)

        data_tg = []
        for meter in cursor.dictfetchall():
            if not meter['tg']:
                # el compt de la pol no es telegestionat
                msg_err = _("""El comptador de la Polissa {pol}, no és
                telegestionat""").format(
                    pol=modcon['polissa_id']
                )
                logger_profiles.info(msg_err)

            dragger = Dragger()
            meter_name = meter_obj.build_name_tg(cursor, uid, meter['id'])
            di = start_date
            df = end_date
            if meter['data_alta'] > start_date:
                di = meter['data_alta']
            if meter['data_baixa']:
                df = min(meter['data_baixa'], end_date)
            di_ts, df_ts = patch_date_to_complete_period(di, df)

            search_params = [
                ('name', '=', meter_name),
                ('cch_fact', '=', True),
                ('timestamp', '>=', di_ts),
                ('timestamp', '<=', df_ts),
            ]
            read_params = [
                'ai_fact', 'kind_fact', 'magn', 'season', 'timestamp'
            ]
            tg_ids = tg_profiles_obj.search(cursor, uid, search_params)
            if not tg_ids:
                # No tg_profiles for this modcon - create CRM case
                crm_obj = self.pool.get('crm.case')
                lot_obj = self.pool.get('giscedata.profiles.lot')
                periode = data_inici.replace('-', '')[:6]
                distri = lot_obj.default_distribuidora(cursor, uid, context)
                filename = 'CLMAG5A_{}_{}'.format(distri, periode)
                msg = _("""No es troben dades de Telegestió per el comptador
                {comptador}, en dates {di} - {df}""").format(
                    comptador=meter_name , di=di_ts, df=df_ts
                )
                crm_obj.create_measures_crm_case(
                    cursor, uid, filename, msg
                )
                continue

            for x in tg_profiles_obj.read(cursor, uid, tg_ids, read_params):
                consumption = dragger.drag(x['ai_fact'] * x['magn'] / 1000)
                x['lectura'] = consumption
                for key in ['m_real', 'n_real', 'm_estimada', 'n_estimada']:
                    x[key] = 0
                # Real Measure
                if x['kind_fact'] == '1':
                    x['m_real'] = consumption
                    x['n_real'] = 1
                # Estimated Measure
                else:
                    x['m_estimada'] = consumption
                    x['n_estimada'] = 1
                if x['season'] == 'S':
                    x['season'] = 1
                else:
                    x['season'] = 0
                for key in ['ai_fact', 'magn', 'kind_fact']:
                    del x[key]
                data_tg.append(x)

        return data_tg

    def read_profiles(self, cursor, uid, modcon, data_inici,
                      data_final, context=None):
        """ Read Profiles for dates
        :return: Profiles data
        """
        profiles_obj = self.pool.get('giscedata.profiles.profile')

        di = data_inici
        df = data_final
        if modcon['data_inici'] > data_inici:
            di = modcon['data_inici']
        if modcon['data_final'] < data_final:
            df = modcon['data_final']
        di_ts, df_ts = patch_date_to_complete_period(di, df)
        perf_ids = profiles_obj.search(cursor, uid, [
            ('cups', '=', modcon['cups.name']),
            ('timestamp', '>=', di_ts),
            ('timestamp', '<=', df_ts)])
        if not perf_ids:
            # No profiles for this modcon - create CRM case
            crm_obj = self.pool.get('crm.case')
            lot_obj = self.pool.get('giscedata.profiles.lot')
            periode = data_inici.replace('-', '')[:6]
            distri = lot_obj.default_distribuidora(cursor, uid, context)
            filename = 'CLMAG5A_{}_{}'.format(distri, periode)
            msg = _("""No es troben Perfils del CUPS {cups}, en dates {di} -
            {df}""").format(cups=modcon['cups.name'], di=di_ts, df=df_ts)
            crm_obj.create_measures_crm_case(cursor, uid, filename, msg)
            return []

        data_prof = []
        columns_to_read = ['lectura', 'estacio', 'timestamp']
        for perf in profiles_obj.read(cursor, uid, perf_ids, columns_to_read):
            for key in ['m_real', 'n_real', 'm_estimada', 'n_estimada']:
                perf[key] = 0
            data_prof.append(perf)

        return data_prof

    def gen_clmag5a(self, cursor, uid, lot_id, data_inici, data_final,
                    context=None):
        """ Generate a CLMAG5A file from aggregated DataFrame with profiles and
        TG Profiles.
        Create a dataset with aggregated key {(agg_key): measures}, and write
        this data. Can pass a dict with aggregation params to create a file by
        aggregation

        :param lot_id: int - measures batch id
        :param data_inici: str - start file data
        :param data_final: str - end file data
        :param context: by aggregation example:
        {
            'comercialitzadora': 9999, 'agree_tensio': 'E0',
            'agree_tarifa': '2A', 'agree_dh': 'e1', 'agree_tipo': '05',
            'provincia': 'AA'
        }
        :return: CLMAG5A attachment args LIKE:
        {
            'filename': str - file_name, 'distri': str - distributor code,
            'comer': str - comer code, 'period': str - YYYYMM,
            'data': str - YYYYMMDD, 'version': str - file_version
        }
        """
        if context is None:
            context = {}

        total_start = datetime.now()
        logger = netsvc.Logger()
        lot_obj = self.pool.get('giscedata.profiles.lot')
        file_name = context.get('filename', '/tmp/CLMAG5A')

        data = self.read_policy_amendments(
            cursor, uid, data_inici, data_final, context
        )
        report = []
        for key, value in data.iteritems():
            agg_key = dict(zip(CLMAG5A_HEADER[:10], key))
            value.update(agg_key)
            report.append(value)
        df_clmag5a = pd.DataFrame(report)
        df_clmag5a['timestamp'] = pd.to_datetime(df_clmag5a.timestamp)
        df_clmag5a['timestamp'] = df_clmag5a['timestamp'].dt.strftime(
            '%Y/%m/%d %H:%M')

        # Fill possible NaNs and cast measures to int
        df_clmag5a = df_clmag5a.fillna(0)
        for measure in ('consum', 'n_real', 'm_real', 'n_estimada', 'm_estimada'):
            df_clmag5a[measure] = df_clmag5a[measure].astype('int')

        # Sort file
        df_clmag5a = df_clmag5a.sort_values(
            ['comercialitzadora', 'agree_tensio', 'agree_tarifa', 'agree_dh',
             'agree_tipo', 'timestamp']
        )

        # Create CLMAG5A file
        df_clmag5a.to_csv(
            file_name, sep=';', header=False, columns=CLMAG5A_HEADER,
            index=False, line_terminator=';\n'
        )
        logger.notifyChannel('objects', netsvc.LOG_INFO,
                             """Total time: 
                             {}""".format(datetime.now() - total_start))

        # Gen aggregations
        aggregations_obj = self.pool.get('giscedata.profiles.aggregations')
        data = df_clmag5a.T.to_dict().values()
        aggregations_obj.gen_aggregations(cursor, uid, data, lot_id)

        # Return dict with file info
        periode = data_inici.replace('-', '')[:6]
        res = {
            'type': 'CLMAG5A',
            'filepath': file_name,
            'distri': lot_obj.default_distribuidora(cursor, uid, context),
            'periode': periode,
            'data': datetime.now().strftime('%Y%m%d'),
            'version': 0
        }

        return res

    _columns = {
        'name': fields.char("Nom", size=30, required=True),
        'periode': fields.char("Període", size=6, required=True),
    }


GiscedataProfilesCLMAG5A()


class GiscedataProfilesCLINME(osv.osv):
    _name = 'giscedata.profiles.clinme'

    def gen_clinme(self, cursor, uid, lot_id, data_inici, data_final, comers=None,
                   context=None):
        """
        Generate a CLINME file from aggregated DataFrame with profiles,
        TG Profiles and TM profiles.
        This process update a aggregations info with dates and measure
        Can pass a dict with aggregation params to create a file by aggregation

        :param lot_id: int - batch_id
        :param data_inici: str - date
        :param data_final: str - date
        :param comers: list of strs with comers
        :param context: by aggregation example:
        {
            'comercialitzadora': 9999, 'agree_tensio': 'E0',
            'agree_tarifa': '2A', 'agree_dh': 'e1', 'agree_tipo': '05',
            'provincia': 'AA'
        }
        """
        if context is None:
            context = {}
        if comers is None:
            comers = context.get('comers', None)
            if comers is None:
                raise osv.except_osv(
                    "Error", "No s'ha passat la llista de comercialitzadores"
                )

        logger = netsvc.Logger()
        logger_profiles = logging.getLogger('openerp.{}'.format(__name__))

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        profile = self.pool.get('giscedata.profiles.profile')
        lot_obj = self.pool.get('giscedata.profiles.lot')
        tg_profile = self.pool.get('tg.profile')
        tm_profile = self.pool.get('tm.profile')
        crm_obj = self.pool.get('crm.case')

        distribuidora = lot_obj.default_distribuidora(cursor, uid, context)
        by_agg = context.get('by_agg', False)
        total_files = []

        for comer in comers:
            where_params = [
                ('agree_tipus', 'in', ['03', '04', '05']),
                ('data_inici', '<=', data_final),
                ('data_final', '>=', data_inici),
                ('polissa_id.state', '!=', 'cancelada'),
                ('comercialitzadora.ref', '=', comer),
            ]
            if 'comercialitzadora' in context:
                # Search by params
                where_params.append(
                    ('agree_tensio', '=', context['agree_tensio'])
                )
                where_params.append(
                    ('agree_tarifa', '=', context['agree_tarifa'])
                )
                where_params.append(('agree_dh', '=', context['agree_dh']))
                where_params.append(('agree_tipus', '=', context['agree_tipo']))
                where_params.append(
                    ('cups.id_municipi.state.ree_code', '=',
                     context['provincia'])
                )
                where_params.remove(('agree_tipus', 'in', ['03', '04', '05'])),
                where_params.append(
                    ('agree_tarifa', '=', context['agree_tarifa'])
                )

            from osv.expression import OOQuery
            q = OOQuery(modcon_obj, cursor, uid)
            sql = q.select([
                'agree_tarifa',
                'agree_tensio',
                'agree_dh',
                'agree_tipus',
                'data_inici',
                'data_final',
                'comercialitzadora.ref',
                'polissa_id',
                'cups.name',
                'cups.id_municipi.state.ree_code',
                'tg'
            ], only_active=False).where(where_params)
            cursor.execute(*sql)

            data = {}
            for nmcon, modcon in enumerate(cursor.dictfetchall(), start=1):
                # Not RE tariffs
                if not modcon['agree_tarifa'] or modcon['agree_tarifa'] == 'RE':
                    continue
                start_date = max(modcon['data_inici'], data_inici)
                end_date = min(modcon['data_final'], data_final)

                qc = OOQuery(meter_obj, cursor, uid)
                sql = qc.select([
                    'id', 'tg', 'data_alta', 'data_baixa', 'name',
                    'technology_type'
                ], only_active=False).where([
                    ('polissa', '=', modcon['polissa_id']),
                    ('data_alta', '<=', end_date),
                    '|',
                    ('data_baixa', '>=', start_date),
                    ('data_baixa', '=', None)
                ])
                cursor.execute(*sql)
                for meter in cursor.dictfetchall():
                    measures_data = []
                    # TM Profiles
                    if meter['technology_type'] in TECHNOLOGY_TYPE['tm']:
                        di = max(start_date, meter['data_alta'])
                        df = end_date
                        if meter['data_baixa']:
                            df = min(meter['data_baixa'], end_date)
                        di_ts, df_ts = patch_date_to_complete_period(di, df)
                        tm_ids = tm_profile.search(cursor, uid, [
                            ('name', '=', meter['name']),
                            ('timestamp', '>=', di_ts),
                            ('timestamp', '<=', df_ts),
                            ('cch_fact', '=', True),
                            ('type', '=', 'p')
                        ])
                        if not tm_ids:
                            # No tm_profiles for this modcon
                            periode = data_inici.replace('-', '')[:6]
                            filename = 'CLINME_{}_{}_{}'.format(
                                distribuidora, comer, periode
                            )
                            msg = _("""No es troben dades de Telemesura per el
                            comptador {comptador}, en dates {di} -
                            {df}""").format(
                                comptador=meter['name'], di=di_ts,
                                df=df_ts
                            )
                            crm_obj.create_measures_crm_case(
                                cursor, uid, filename, msg
                            )
                            continue

                        for tm in tm_profile.read(cursor, uid, tm_ids, [
                            'ai_fact', 'timestamp', 'r1', 'cch_bruta'
                        ]):
                            if not tm['cch_bruta']:
                                tm['r1'] = 0
                            measures_data.append(tm)
                    # TG Profiles
                    elif (meter['technology_type'] in TECHNOLOGY_TYPE['tg']
                          and not (modcon['agree_tipus'] == '05' and modcon['tg'] != '1')):
                        if not meter['tg']:
                            # el compt de la pol no es telegestionat
                            msg_err = _("""El comptador de la Polissa {pol}, no
                            és telegestionat""").format(
                                pol=modcon['polissa_id']
                            )
                            logger_profiles.info(msg_err)

                        dragger = Dragger()
                        reactive_dragger = Dragger()
                        meter_name = meter_obj.build_name_tg(
                            cursor, uid, meter['id']
                        )
                        di = max(meter['data_alta'], start_date)
                        df = end_date
                        if meter['data_baixa']:
                            df = min(meter['data_baixa'], end_date)
                        di_ts, df_ts = patch_date_to_complete_period(di, df)
                        search_params = [('name', '=', meter_name),
                                         ('cch_fact', '=', True),
                                         ('timestamp', '>=', di_ts),
                                         ('timestamp', '<=', df_ts)]

                        tg_ids = tg_profile.search(cursor, uid, search_params)
                        if not tg_ids:
                            # No tg_profiles for this modcon - create CRM case
                            periode = data_inici.replace('-', '')[:6]
                            filename = 'CLINME_{}_{}_{}'.format(
                                distribuidora, comer, periode
                            )
                            msg = _("""No es troben dades de Telegestió per el
                            comptador {comptador}, en dates
                            {di} - {df}""").format(
                                comptador=meter_name, di=di_ts, df=df_ts
                            )
                            crm_obj.create_measures_crm_case(
                                cursor, uid, filename, msg
                            )
                            continue

                        for tg in tg_profile.read(cursor, uid, tg_ids, [
                            'ai_fact', 'timestamp', 'r1', 'magn', 'cch_bruta'
                        ]):
                            tg['ai_fact'] = dragger.drag(
                                tg['ai_fact'] * tg['magn'] / 1000
                            )
                            if not tg['cch_bruta']:
                                tg['r1'] = 0
                            else:
                                tg['r1'] = reactive_dragger.drag(
                                    tg['r1'] * tg['magn'] / 1000
                                )
                            measures_data.append(tg)
                    # Profiles
                    else:
                        di = max(meter['data_alta'], start_date)
                        df = end_date
                        if meter['data_baixa']:
                            df = min(meter['data_baixa'], end_date)
                        di_ts, df_ts = patch_date_to_complete_period(di, df)
                        perf_ids = profile.search(cursor, uid, [
                            ('cups', '=', modcon['cups.name']),
                            ('timestamp', '>=', di_ts),
                            ('timestamp', '<=', df_ts)])
                        if not perf_ids:
                            # No profiles for this modcon - create CRM case
                            periode = data_inici.replace('-', '')[:6]
                            filename = 'CLINME_{}_{}_{}'.format(
                                distribuidora, comer, periode
                            )
                            msg = _("""No es troben Perfils del CUPS {cups}, en
                                            dates {di} - {df}""").format(
                                cups=modcon['cups.name'], di=di_ts, df=df_ts
                            )
                            crm_obj.create_measures_crm_case(
                                cursor, uid, filename, msg
                            )
                            continue

                        for perf in profile.read(cursor, uid, perf_ids, [
                            'lectura', 'timestamp'
                        ]):
                            perf['r1'] = 0
                            perf['ai_fact'] = perf['lectura']
                            measures_data.append(perf)

                    if measures_data:
                        data_inici_agg = min([x['timestamp'] for x in measures_data])
                        data_final_agg = max([x['timestamp'] for x in measures_data])
                    else:
                        # No data for this modcon
                        continue

                    for measure in measures_data:
                        key = (
                            modcon['cups.name'],
                            distribuidora,
                            comer,
                            modcon['agree_tensio'],
                            modcon['agree_tarifa'],
                            modcon['agree_dh'],
                            modcon['agree_tipus'],
                            modcon['cups.id_municipi.state.ree_code'],
                            INDICADOR_MESURA[modcon['agree_tarifa']],
                            data_inici_agg,
                            data_final_agg,
                        )
                        if key not in data:
                            data[key] = {}
                            data[key]['ai_fact'] = measure['ai_fact']
                            data[key]['r1'] = measure['r1']
                        else:
                            data[key]['ai_fact'] += measure['ai_fact']
                            data[key]['r1'] += measure['r1']

                    logger_profiles.info('CLINME Generation: End Modcon: #{}'.format(nmcon))
            report = []
            for key, value in data.iteritems():
                agg_key = dict(zip(CLINME_HEADER[:11], key))
                value.update(agg_key)
                report.append(value)

            df_clinme = pd.DataFrame(data=report)
            if df_clinme.empty:
                # No dfs for this comer
                continue

            # Parse dataframe
            for label in ('m_estimada', 'n_estimada', 'r4'):
                df_clinme[label] = df_clinme[label] = 0
            df_clinme['timestamp'] = pd.to_datetime(df_clinme.timestamp)
            df_clinme['timestamp'] = df_clinme['timestamp'].dt.strftime(
                '%Y/%m/%d %H')
            df_clinme['timestamp_end'] = pd.to_datetime(df_clinme.timestamp_end)
            df_clinme['timestamp_end'] = df_clinme['timestamp_end'].dt.strftime(
                '%Y/%m/%d %H')

            # Cast measures to int
            for measure in ('ai_fact', 'm_estimada', 'r1', 'r4'):
                df_clinme[measure] = df_clinme[measure].astype('int')

            # join metter changes
            df_clinme = df_clinme.groupby([
                'cups', 'distribuidora', 'comercialitzadora', 'agree_tensio',
                'agree_tarifa', 'agree_dh', 'agree_tipus', 'provincia',
                'indicador_mesura'
            ]).aggregate(
                {
                    'timestamp': 'min', 'timestamp_end': 'max',
                    'ai_fact': 'sum', 'r1': 'sum', 'r4': 'sum',
                    'n_estimada': 'sum', 'm_estimada': 'sum'
                 }
            ).reset_index()

            # sort file
            df_clinme = df_clinme.sort_values(
                ['comercialitzadora', 'agree_tensio', 'agree_tarifa',
                 'agree_dh', 'agree_tipus', 'timestamp']
            )

            periode = data_inici.replace('-', '')[:6]
            filename = 'CLINME_{0}_{1}_{2}_{3}.0'.format(
                distribuidora, comer, periode, datetime.now().strftime('%Y%m%d')
            )
            logger_profiles.info('filename {}'.format(filename))
            file_name = context.get('filename', False)
            if isinstance(file_name, (list, tuple)):
                file_name = file_name[1]
            elif not file_name:
                file_name = tempfile.mkstemp()[1]
            _args = {
                'type': 'CLINME',
                'filepath': file_name,
                'distri': distribuidora,
                'comer': comer,
                'periode': periode,
                'data': datetime.now().strftime('%Y%m%d'),
                'version': 0
            }
            df_clinme.to_csv(
                file_name, sep=';', header=False, columns=CLINME_HEADER,
                index=False, line_terminator=';\n'
            )
            logger.notifyChannel('objects', netsvc.LOG_INFO,
                                 'end CLINME: {}'.format(comer))
            if by_agg:
                total_files.append(_args)
            else:
                lot_obj.attach_measures_file_to_lot(
                    cursor, uid, lot_id, _args, context
                )

            df_aggregations = df_clinme.groupby(
                ['distribuidora', 'comercialitzadora', 'agree_tensio',
                 'agree_tarifa', 'agree_dh', 'agree_tipus', 'provincia']
            ).aggregate(
                {'timestamp': 'min', 'timestamp_end': 'max'}
            ).reset_index()

            aggregations_obj = self.pool.get('giscedata.profiles.aggregations')
            aggregations = df_aggregations.T.to_dict().values()
            for agg in aggregations:
                search_params = [
                    ('distribuidora', '=', agg['distribuidora']),
                    ('comercialitzadora', '=', agg['comercialitzadora']),
                    ('agree_tensio', '=', agg['agree_tensio']),
                    ('agree_tarifa', '=', agg['agree_tarifa']),
                    ('agree_dh', '=', agg['agree_dh']),
                    ('agree_tipo', '=', agg['agree_tipus']),
                    ('provincia', '=', agg['provincia']),
                    ('lot_id', '=', lot_id),
                ]
                agg['lot_id'] = lot_id
                agg_id = aggregations_obj.search(cursor, uid, search_params)
                if agg_id:
                    agg['timestamp'] = agg['timestamp'].split(' ')[0].replace('/', '-')
                    agg['timestamp_end'] = agg['timestamp_end'].split(' ')[0].replace('/', '-')

                    aggregations_obj.write(cursor, uid, agg_id, {
                        'data_inici': agg['timestamp'],
                        'data_final': agg['timestamp_end']
                    })
        return total_files

    _columns = {

    }


GiscedataProfilesCLINME()


class GiscedataProfilesAGCL(osv.osv):
    _name = 'giscedata.profiles.agcl'

    def gen_agcl(self, cursor, uid, lot_id, df_new=None, df_old=None, context=None):
        """ Generate a AGCL measures file by batch
        df_new: DF with aggregations data
        df_old: DF with AGCL OS data
        :return: AGCL attachment args LIKE:
        {
            'filename': str - file_name, 'distri': str - distributor code,
            'period': str - YYYYMM, 'data': str - YYYYMMDD,
            'version': str - file_version
        }
        """
        if context is None:
            context = {}
        agg_obj = self.pool.get('giscedata.profiles.aggregations')
        lot_obj = self.pool.get('giscedata.profiles.lot')
        logger_profiles = logging.getLogger('openerp.{}'.format(__name__))

        if isinstance(lot_id, list):
            lot_id = lot_id[0]

        previous_lot_id = lot_obj.get_previous_lot(cursor, uid, lot_id)
        agcl_os_mode = True
        if df_new is None and df_old is None:
            agcl_os_mode = False
            new_agg_ids = agg_obj.search(cursor, uid, [('lot_id', '=', lot_id)])
            old_agg_ids = agg_obj.search(cursor, uid, [('lot_id', '=', previous_lot_id)])
            new_agg = agg_obj.read(cursor, uid, new_agg_ids)
            old_agg = agg_obj.read(cursor, uid, old_agg_ids)
            df_new = pd.DataFrame(data=new_agg)
            df_old = pd.DataFrame(data=old_agg)
        df_new['origin'] = 'A'
        df_old['origin'] = 'B'
        df_aggregations = pd.concat([df_new, df_old])
        columns = [
            'comercialitzadora', 'agree_tarifa', 'agree_tensio', 'agree_dh',
            'agree_tipo', 'provincia'
        ]
        df_aggregations.drop_duplicates(
            subset=columns, keep=False, inplace=True
        )

        if df_aggregations.empty:
            raise osv.except_osv(
                'Warning', "No s'han detectat canvis en les agregacions"
            )
        if agcl_os_mode:
            d_final_periode = lot_obj.read(cursor, uid, lot_id, ['data_final'])['data_final']
            df_aggregations['data_final_ag'] = df_aggregations.apply(
                lambda agg: self.get_df(cursor, uid, previous_lot_id, agg, d_final_periode), axis=1
            )
        else:
            # Set init aggregation date
            df_aggregations = df_aggregations.replace(to_replace=False, value='')
            df_aggregations['data_inici_ag'] = (
                    df_aggregations['data_inici_ag'].str.replace('-', '/') + ' 00'
            )
            # Set end aggregation date
            # Up: A - without end date
            # Down: B - data_final_ag if exists otherwise data_final
            df_aggregations['data_final_ag'] = np.where(
                (df_aggregations['origin'] == 'B') &
                (df_aggregations['data_final_ag'] != ''),
                df_aggregations['data_final_ag'].str.replace('-', '/') + ' 00',
                df_aggregations['data_final'].str.replace('-', '/') + ' 00')
            df_aggregations['data_final_ag'] = np.where(
                df_aggregations['origin'] == 'A', '',
                df_aggregations['data_final_ag']
            )
        columns = [
            'origin', 'operation_type', 'distribuidora', 'comercialitzadora',
            'agree_tensio', 'agree_tarifa', 'agree_dh', 'agree_tipo',
            'provincia', 'data_inici_ag', 'data_final_ag'
        ]

        # Generate AGCL
        file_name = context.get('filename', '/tmp/AGCL')
        df_aggregations.to_csv(
            file_name, sep=';', header=False, columns=columns, index=False,
            line_terminator=';\n'
        )
        res = {
            'type': 'AGCL',
            'filepath': file_name,
            'distri': lot_obj.default_distribuidora(cursor, uid, context),
            'data': datetime.now().strftime('%Y%m%d'),
            'version': 0
        }

        return res

    def get_df(self, cursor, uid, previous_lot_id, agg, dfinal_periode, context=None):
        """
        Get last date if exists, otherwise return formated end lot date
        :param lot_id: period_id
        :param agg: dict with agg information
        :return: End agg consumption date
        """
        if context is None:
            context = {}
        if agg['origin'] == 'A':
            return ''
        agg_obj = self.pool.get('giscedata.profiles.aggregations')
        search_params = [
            ('comercialitzadora', '=', agg['comercialitzadora']),
            ('distribuidora', '=', agg['distribuidora']),
            ('agree_tensio', '=', agg['agree_tensio']),
            ('agree_tarifa', '=', agg['agree_tarifa']),
            ('agree_dh', '=', agg['agree_dh']),
            ('agree_tipo', '=', agg['agree_tipo']),
            ('provincia', '=', agg['provincia']),
            ('lot_id', '=', previous_lot_id),
        ]
        past_agg_id = agg_obj.search(cursor, uid, search_params, context=context)
        if past_agg_id:
            dfinal_periode = agg_obj.read(
                cursor, uid, past_agg_id[0], ['data_final']
            )['data_final']
        else:
            dfinal_periode = (datetime.strptime(dfinal_periode, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
        return dfinal_periode.replace('-', '/') + ' 00'


GiscedataProfilesAGCL()


class GiscedataProfilesAggregations(osv.osv):

    _name = 'giscedata.profiles.aggregations'
    _inherit = 'giscedata.profiles.aggregations'

    def gen_aggregations(self, cursor, uid, data, lot_id, context=None):
        """Generate a aggregations by batch
        """
        self.pool.get('giscedata.profiles.profile')
        lot_obj = self.pool.get('giscedata.profiles.lot')
        previous_lot_id = lot_obj.get_previous_lot(cursor, uid, lot_id)

        # Gen aggregations without previous data
        if not data:
            modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
            distribuidora = lot_obj.default_distribuidora(cursor, uid, context)

            where_params = [
                ('agree_tipus', 'in', ['03', '04', '05']),
                ('data_inici', '<=', context['data_inici']),
                ('data_final', '>=', context['data_final']),
            ]
            from osv.expression import OOQuery
            q = OOQuery(modcon_obj, cursor, uid)
            sql = q.select([
                'agree_tarifa',
                'agree_tensio',
                'agree_dh',
                'agree_tipus',
                'comercialitzadora.ref',
                'cups.id_municipi.state.ree_code',
            ], only_active=False).where(where_params)
            cursor.execute(*sql)
            data = cursor.dictfetchall()

            df_aggregations = pd.DataFrame(data=data).drop_duplicates(
                subset=['comercialitzadora.ref', 'agree_tensio',
                        'agree_tarifa', 'agree_dh', 'agree_tipus',
                        'cups.id_municipi.state.ree_code'])

            df_aggregations.rename(columns={
                'comercialitzadora.ref': 'comercialitzadora',
                'cups.id_municipi.state.ree_code': 'provincia',
                'agree_tipus': 'agree_tipo',
            }, inplace=True)
            df_aggregations['agree_tensio'].replace('None', np.nan, inplace=True)
            df_aggregations['agree_tarifa'].replace('None', np.nan, inplace=True)
            df_aggregations = df_aggregations.dropna()
            data = df_aggregations.T.to_dict().values()
            for agg in data:
                agg['distribuidora'] = distribuidora
                agg['lot_id'] = lot_id
                agg['magnitud'] = 'AE'
                agg = self.set_init_date(cursor, uid, agg, previous_lot_id,
                                         context={'lot_id': lot_id})
                self.create(cursor, uid, agg, context)

            return True

        # Gen aggregations with previous data
        agg = [
            'distribuidora', 'comercialitzadora', 'agree_tensio',
            'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia', 'magnitud'
        ]

        df_profiles = pd.DataFrame(data=data)
        df_profiles = df_profiles.rename(
            index=str, columns={'timestamp': 'data_inici'}
        )
        df_profiles['data_final'] = df_profiles['data_inici']
        df_profiles = df_profiles.groupby(agg).aggregate(
            {'consum': 'sum', 'data_inici': 'min', 'data_final': 'max'}
        )
        df_profiles = df_profiles.reset_index()

        for date in ('data_inici', 'data_final'):
            df_profiles[date] = df_profiles[date].map(
                lambda x: x.replace('-', '/')
            )
            df_profiles[date] = df_profiles[date].map(
                lambda x: '{}'.format(x[:11])
            )

        # DF to dict
        profiles = df_profiles.T.to_dict().values()
        for agg in profiles:
            agg['lot_id'] = lot_id
            agg = self.set_init_date(cursor, uid, agg, previous_lot_id,
                                     context={'lot_id': lot_id})
            self.create(cursor, uid, agg, context)

    def set_init_date(self, cursor, uid, agg, lot_id, context=None):
        """
        Check old aggregations for get data_inici_agregacio
        :param cursor: db cursor
        :param uid: user id
        :param agg: dict LIKE
        {
            'comercialitzadora': ree_comer_code,
            'distribuidora':  ree_distri_code,
            'agree_tarifa': ree_tariff_code,
            'agree_dh': ree_dh_code,
            ...
        }
        :return: agg dict with data_inici_ag
        """
        search_params = [
            ('comercialitzadora', '=', agg['comercialitzadora']),
            ('distribuidora', '=', agg['distribuidora']),
            ('agree_tensio', '=', agg['agree_tensio']),
            ('agree_tarifa', '=', agg['agree_tarifa']),
            ('agree_dh', '=', agg['agree_dh']),
            ('agree_tipo', '=', agg['agree_tipo']),
            ('provincia', '=', agg['provincia']),
            ('lot_id', '=', lot_id),
        ]
        agg_id = self.search(cursor, uid, search_params)
        if agg_id:
            old_agg = self.read(cursor, uid, agg_id, ['data_inici_ag'])[0]
            agg['data_inici_ag'] = old_agg['data_inici_ag']
        else:
            if 'data_inici' in agg:
                agg['data_inici_ag'] = agg['data_inici']
            else:
                actual_lot_id = context.get('lot_id', False)
                if actual_lot_id:
                    lot_obj = self.pool.get('giscedata.profiles.lot')
                    lot = lot_obj.read(
                        cursor, uid, actual_lot_id, ['data_inici'])
                    agg['data_inici_ag'] = lot['data_inici']

        return agg

    def name_get(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        if not len(ids):
            return []

        res = []
        fields_to_read = [
            'distribuidora', 'comercialitzadora', 'agree_tensio',
            'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia'
        ]
        for aggr in self.read(cursor, uid, ids, fields_to_read):
            vals = ';'.join([aggr[k] for k in fields_to_read])
            res.append((aggr['id'], vals))
        return res

    _rec_name = 'comercialitzadora'

    _columns = {
        'lot_id': fields.many2one(
            'giscedata.profiles.lot', 'Lot', required=True
        ),
        'checked_agcl': fields.boolean('Comprovat AGCL'),
        'validat_ree': fields.boolean('Comprovat REE'),
    }

    _defaults = {
        'checked_agcl': lambda *a: False,
        'validat_ree': lambda *a: False,
    }


GiscedataProfilesAggregations()


class GiscedataProfilesREEAggregations(osv.osv):

    _name = 'giscedata.profiles.ree.aggregations'

    _columns = {
        'lot_id': fields.many2one('giscedata.profiles.lot', 'Lot',
                                  required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'magnitud': fields.char('Magnitud', size=2),
        'data_inici_ag': fields.date('Data inici (aggr)', required=True),
        'data_final_ag': fields.date('Data final (aggr)'),
        'consum': fields.integer('Consum'),
        'consum_ree': fields.integer('Consum REE'),
        'consum_diff': fields.integer('Diferència de consum'),
        'validat_ree': fields.boolean('Comprovat REE'),
    }

    _defaults = {
        'consum': lambda *a: 0,
        'consum_diff': lambda *a: 0,
        'validat_ree': lambda *a: False,
    }


GiscedataProfilesREEAggregations()


class GiscedataProfilesAggregationsValidations(osv.osv):

    _name = 'giscedata.profiles.aggregations.validations'

    _rec_name = 'distribuidora'

    _columns = {
        'lot_id': fields.many2one('giscedata.profiles.lot', 'Lot',
                                  required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'consum': fields.integer('Consum'),
        'clmag_consumption': fields.integer('Consum CLMAG'),
        'clmag5a_consumption': fields.integer('Consum CLMAG5A'),
        'clinme_consumption': fields.integer('Consum CLINME'),
    }


GiscedataProfilesAggregationsValidations()


class GiscedataProfilesTancaments(osv.osv):
    _name = 'giscedata.profiles.tancaments'
    _rec_name = 'mes'

    def check_deadline(self, cursor, uid, ids, month, context=None):

        tancament_id = self.search(cursor, uid, [('mes', '=', month)])
        date = self.read(cursor, uid, tancament_id, ['definitiu'])['definitiu']

        if datetime.now() > datetime.strptime(date, '%Y-%m-%d'):
            raise osv.except_osv('Tancament definitiu superat',
                                 'Entrega fora de plaç')

    _columns = {
        'mes': fields.char('Mes', size=7, required=True, readonly=True),
        'm1': fields.date('M+1', readonly=True),
        'm3': fields.date('M+3', readonly=True),
        'provisional': fields.date('Tancament Provisional', readonly=True),
        'definitiu': fields.date('Tancament Definitiu', readonly=True),
    }


GiscedataProfilesTancaments()
