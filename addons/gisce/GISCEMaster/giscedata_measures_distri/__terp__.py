# -*- coding: utf-8 -*-
{
  "name": "",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Mesures REE a distribuidora
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      "giscedata_measures",
      "giscedata_profiles_distri",
      "giscedata_telemesures_distri",
      "crm",
  ],
  "init_xml": [],
  "demo_xml": [
      "giscedata_measures_demo.xml",
  ],
  "update_xml": [
      "wizard/wizard_aggregations_export_view.xml",
      "wizard/wizard_validacions_mesures_view.xml",
      "wizard/wizard_measures_integrity_view.xml",
      "wizard/wizard_agcl_view.xml",
      "giscedata_measures_view.xml",
      "giscedata_measures_data.xml",
      "crm_data.xml",
      "wizard/wizard_measures_file_by_aggregation_view.xml",
      "wizard/wizard_crear_lots_mesures_view.xml",
      "wizard/wizard_compare_aggregations_view.xml",
      "wizard/wizard_informe_consums_mesures_view.xml",
      "security/ir.model.access.csv",
  ],
  "active": False,
  "installable": True
}
