# -*- coding: utf-8 -*-
from osv import osv

class CrmCase(osv.osv):

    _inherit = 'crm.case'
    _name = 'crm.case'

    def create_measures_crm_case(self, cursor, uid, _file, msg):
        """
        Create a crm_case with measure error
        :return: crm_case_id
        """
        crm_obj = self.pool.get('crm.case')
        crm_section_obj = self.pool.get('crm.case.section')
        section_id = crm_section_obj.search(
            cursor, uid, [('code', '=', 'MEASURES')]
        )[0]
        name = 'Measures case from file: {}'.format(_file)
        crm_id = crm_obj.search(cursor, uid,
            [('section_id', '=', section_id), ('name', '=', name)]
        )
        if crm_id:
            crm_id = self.append_description_to_case(cursor, uid, crm_id, msg)
        else:
            crm_case_vals = {
                'name': name,
                'section_id': section_id,
                'user_id': uid,
                'description': msg,
            }
            # Create crm case
            crm_id = crm_obj.create(cursor, uid, crm_case_vals)

        return self.case_open(cursor, uid, [crm_id])

    def append_description_to_case(self, cursor, uid, case_id, msg):
        """
        Append info to crm case
        :return crm_case_id
        """
        crm_obj = self.pool.get('crm.case')
        if isinstance(case_id, (list, tuple)):
            case_id = case_id[0]

        case_data = crm_obj.read(cursor, uid, case_id, ['description'])
        if case_data['description']:
            description = case_data['description'] + '\n' + msg
            crm_obj.write(cursor, uid, case_id, {'description': description})
        else:
            crm_obj.write(cursor, uid, case_id, {'description': msg})

        return int(case_id)


CrmCase()
