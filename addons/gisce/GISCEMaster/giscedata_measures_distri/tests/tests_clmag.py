# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from dateutil.relativedelta import relativedelta
from giscedata_measures.utils import last_sunday

GOOD_DATES = [
    '2019-03-31 03:00:00', '2020-03-29 03:00:00',
    '2021-03-28 03:00:00', '2022-03-27 03:00:00',
    '2023-03-26 03:00:00', '2024-03-31 03:00:00',
    '2025-03-30 03:00:00', '2026-03-29 03:00:00',
    '2027-03-28 03:00:00', '2028-03-26 03:00:00'
]


class TestsClmag(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    @staticmethod
    def get_10_years_hours():
        dates = []
        timestamp = '2019-01-01 01:00:00'
        for i in range(365 * 24 * 10):
            timestamp = (
                datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S') +
                relativedelta(hours=1)
            ).strftime('%Y-%m-%d %H:%M:%S')
            dates.append(timestamp)
        return dates

    def test_check_hour_change(self):
        """ Test if choose the time changes well """
        dates = self.get_10_years_hours()
        for date_ in dates:
            month_ = int(date_[5:7])
            year_ = int(date_[:4])
            if month_ == 3:
                day_change = last_sunday(year_, month_)
                day_change = '{year}-{month}-{day} 03'.format(
                    year=str(year_), month=str(month_).zfill(2), day=str(day_change).zfill(2)
                )
            else:
                day_change = False
            if date_[:13] == day_change:
                self.assertIn(date_, GOOD_DATES)
            else:
                self.assertNotIn(date_, GOOD_DATES)
