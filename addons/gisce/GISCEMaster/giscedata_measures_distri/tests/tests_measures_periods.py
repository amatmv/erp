# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from osv.osv import except_osv

TEST_YEARS = [2020, 2021, 2022, 2023, 2024, 2025]

class TestsMeasuresPeriods(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_profile_lot(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_measures_distri', 'lot_01')[1]

        return lot_id

    def test_create_measures_periods(self):
        """
        Test create a measures periods
        """
        wiz_obj = self.openerp.pool.get('wizard.crear.lots.mesures')
        wiz_id = wiz_obj.create(self.cursor, self.uid, {'year': 2019})
        wiz = wiz_obj.browse(self.cursor, self.uid, wiz_id, context={})
        res = wiz.action_crear_lots()
        self.assertTrue(res, "Periodes no creats correctament")

    def test_create_measures_multiple_years_periods(self):
        """
        Test create a multiple years measures periods
        """
        wiz_obj = self.openerp.pool.get('wizard.crear.lots.mesures')
        for year in TEST_YEARS:
            wiz_id = wiz_obj.create(self.cursor, self.uid, {'year': year})
            wiz = wiz_obj.browse(self.cursor, self.uid, wiz_id, context={})
            res = wiz.action_crear_lots()
            self.assertTrue(res, "Periodes no creats correctament")

    def test_no_create_measures_duplicate_periods(self):
        """
        Test check no create a duplicate measures periods
        """
        period_obj = self.openerp.pool.get('giscedata.profiles.lot')
        wiz_obj = self.openerp.pool.get('wizard.crear.lots.mesures')
        period_id = self.get_profile_lot()
        p_inici = period_obj.read(self.cursor, self.uid, period_id, ['data_inici'])['data_inici']
        p_year = p_inici[:4]
        wiz_id = wiz_obj.create(self.cursor, self.uid, {'year': int(p_year)})
        wiz = wiz_obj.browse(self.cursor, self.uid, wiz_id, context={})
        with self.assertRaises(except_osv):
            res = wiz.action_crear_lots()

    def test_cronjob_create_measures_periods(self):
        period_obj = self.openerp.pool.get('giscedata.profiles.lot')
        res = period_obj._cronjob_create_measures_periods(self.cursor, self.uid, [], {})
        self.assertTrue(res, "Periodes desde el cronjob no creats correctament")
