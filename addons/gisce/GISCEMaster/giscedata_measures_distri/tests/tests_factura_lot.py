# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from giscedata_polissa.tests.utils import activar_polissa
from datetime import datetime
from dateutil.relativedelta import relativedelta


def convert_string_to_datetime(string):
    """
    Auxiliar method desired to simulate the expected parse of a datetime
    """
    return datetime.strptime(string, "%Y-%m-%d %H")


class TestsFacturaLot(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_invoice(self, num):
        imd_obj = self.openerp.pool.get('ir.model.data')
        factura_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_facturacio',
            'factura_000{num}'.format(num=str(num))
        )[1]

        return factura_id

    def get_pricelist_generation_id(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        pricelist_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_facturacio_distri',
            'pricelist_tarifas_generacion'
        )[1]

        return pricelist_id

    def get_profile_lot(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        lot_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_measures_distri', 'lot_01')[1]

        return lot_id

    def test_assign_invoices_to_batch(self):
        """
        Test assign invoices to batch.
        Will allocate tg billings, so they will not have to be outdated.
        Only the logic of assigning and calculating is calculated
        """
        prof_lot_obj = self.openerp.pool.get('giscedata.profiles.lot')
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        data_inici = '2018-01-01'
        data_final = '2018-02-28'
        for x in range(1, 2):
            factura_id = self.get_invoice(x)
            factura = factura_obj.browse(self.cursor, self.uid, factura_id)
            factura.write({
                'polissa_tg': '1', 'data_inici': data_inici,
                'data_final': data_final
            })
            factura_obj.create_profiles_contract_invoice_batch(self.cursor, self.uid, factura.id)
        lot_id = prof_lot_obj.search(self.cursor, self.uid, [
            ('data_inici', '=', '2018-01-01')
        ])[0]
        expected = 1
        self.assertEqual(
            prof_lot_obj.browse(self.cursor, self.uid, lot_id).n_factures,
            expected, 'invoices not allocated to the profiling batch correctly'
        )
        self.assertEqual(
            prof_lot_obj.browse(self.cursor, self.uid, lot_id+1).n_factures,
            expected, 'invoices not allocated to the profiling batch correctly'
        )
        expected = 0.0
        prof_lot_obj.update_profiling_progress(self.cursor, self.uid, [lot_id])
        self.assertEqual(
            prof_lot_obj.browse(self.cursor, self.uid, lot_id).progres,
            expected, 'profiling progress not calculated correctly'
        )

    def test_check_inv_to_batch(self):
        """
        Test to check invoice/contract can create
        :return:
        """
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        factura_id = self.get_invoice(1)
        pricelist_generation_id = self.get_pricelist_generation_id()

        # Convert to generation invoice
        factura_obj.write(self.cursor, self.uid, factura_id, {
            'llista_preu': pricelist_generation_id
        })
        self.assertFalse(
            factura_obj.check_inv_contract_batch(
                self.cursor, self.uid, factura_id
            ), 'invoice would not create invoice lot'
        )

        factura_id = self.get_invoice(2)
        # Change inv type
        factura_obj.write(self.cursor, self.uid, factura_id, {
            'tipo_factura': '03'
        })
        self.assertFalse(
            factura_obj.check_inv_contract_batch(
                self.cursor, self.uid, factura_id
            ), 'invoice would not create invoice lot'
        )

        factura_obj.write(self.cursor, self.uid, factura_id, {
            'tipo_factura': '01'
        })
        self.assertTrue(
            factura_obj.check_inv_contract_batch(
                self.cursor, self.uid, factura_id
            ), 'invoice would not create invoice lot'
        )

    def test_update_tm_progress(self):
        inv_lot_obj = self.openerp.pool.get('giscedata.profiles.factura.lot')
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lot_obj = self.openerp.pool.get('giscedata.profiles.lot')
        factura_id = self.get_invoice(1)
        lot_id = self.get_profile_lot()
        vals = {
            'lot_id': lot_id,
            'factura_id': factura_id,
            'type': 'tm',
            'state': 'draft'
        }
        inv_lot_obj.create(self.cursor, self.uid, vals)
        lot_obj.update_tm_tg_progress(
            self.cursor, self.uid, [lot_id], context={'origin': 'tm'}
        )
        progres = lot_obj.read(
            self.cursor, self.uid, lot_id, ['tm_progress', 'n_factures']
        )
        self.assertEqual(progres['tm_progress'], 0.0)
        factura_obj.write(
            self.cursor, self.uid, factura_id, {'cch_fact_available': True}
        )
        lot_obj.update_tm_tg_progress(
            self.cursor, self.uid, [lot_id], context={'origin': 'tm'}
        )
        # Refresh progress
        progres = lot_obj.read(
            self.cursor, self.uid, lot_id, ['tm_progress']
        )
        self.assertEqual(progres['tm_progress'], 100.0)

    def test_update_tg_progress(self):
        inv_lot_obj = self.openerp.pool.get('giscedata.profiles.factura.lot')
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lot_obj = self.openerp.pool.get('giscedata.profiles.lot')
        factura_id = self.get_invoice(1)
        lot_id = self.get_profile_lot()
        vals = {
            'lot_id': lot_id,
            'factura_id': factura_id,
            'type': 'tg',
            'state': 'draft'
        }
        inv_lot_obj.create(self.cursor, self.uid, vals)
        lot_obj.update_tm_tg_progress(
            self.cursor, self.uid, [lot_id], context={'origin': 'tg'}
        )
        progres = lot_obj.read(
            self.cursor, self.uid, lot_id, ['tg_progress', 'n_factures']
        )
        self.assertEqual(progres['tg_progress'], 0.0)
        factura_obj.write(
            self.cursor, self.uid, factura_id, {'cch_fact_available': True}
        )
        lot_obj.update_tm_tg_progress(
            self.cursor, self.uid, [lot_id], context={'origin': 'tg'}
        )
        # Refresh progress
        progres = lot_obj.read(
            self.cursor, self.uid, lot_id, ['tg_progress']
        )
        self.assertEqual(progres['tg_progress'], 100.0)
