CLMAG5A_HEADER = [
    'distribuidora', 'comercialitzadora', 'agree_tensio', 'agree_tarifa',
    'agree_dh', 'agree_tipo', 'provincia', 'timestamp', 'estacio',
    'magnitud', 'consum', 'n_punts', 'm_real', 'n_real', 'm_estimada',
    'n_estimada',
]

CLINME_HEADER = [
    'cups', 'distribuidora', 'comercialitzadora', 'agree_tensio',
    'agree_tarifa', 'agree_dh', 'agree_tipus', 'provincia', 'indicador_mesura',
    'timestamp', 'timestamp_end', 'ai_fact', 'r1', 'r4', 'n_estimada',
    'm_estimada'
]

ACUM_HEADER = [
    'cups', 'magnitud', 'm_estimada', 'n_estimada', 'm_redundant',
    'n_redundant', 'm_principal', 'n_principal', 'consum', 'n_hores'
]

AGGREGATION = [
    'distribuidora', 'comercialitzadora', 'agree_tensio', 'agree_tarifa',
    'agree_dh', 'agree_tipo', 'provincia'
]

CLMAG_HEADER = [
    'distribuidora', 'comercialitzadora', 'provincia',
    'agree_tensio', 'agree_tarifa', 'agree_dh', 'agree_tipo'
]

AGCL_HEADER = [
    'distribuidora', 'comercialitzadora', 'agree_tensio', 'agree_tarifa',
    'agree_dh', 'agree_tipo', 'provincia', 'data_inici_ag', 'data_final_ag'
]