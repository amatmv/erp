# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        return False

    logger = logging.getLogger('openerp.migration')
    logger.info('Updating giscedata_profiles_factura_lot')

    update_query = """
        UPDATE giscedata_profiles_factura_lot
        SET type=subquery.tipuscomptadorprofile
        FROM (
            SELECT pfact.id AS profile_factura_id,
                CASE WHEN fact.potencia <= 15 and fact.polissa_tg = '1' THEN 'tg'
                WHEN fact.potencia <= 15 and fact.polissa_tg != '1' THEN 'perfil'
                WHEN fact.potencia > 15 and fact.potencia <= 450 and compt.technology_type in ('prime', 'smmweb') THEN 'tg'
                WHEN fact.potencia > 15 and fact.potencia <= 450 and compt.technology_type in ('telemeasure', 'electronic') THEN 'tm'
                ELSE 'perfil'
                END AS tipuscomptadorprofile
            FROM giscedata_profiles_factura_lot pfact
            LEFT JOIN giscedata_facturacio_factura fact ON pfact.factura_id=fact.id
            LEFT JOIN giscedata_polissa_tarifa tarif ON fact.tarifa_acces_id=tarif.id
            LEFT JOIN giscedata_facturacio_lectures_energia lenergia ON fact.id=lenergia.factura_id
            LEFT JOIN giscedata_lectures_comptador compt ON lenergia.comptador_id=compt.id
            WHERE tarif.name != 'RE' and tarif.name != 'RE12' AND fact.tipo_factura = '01'
            GROUP BY pfact.id, fact.id, tarif.name, fact.tipo_factura, compt.technology_type
            ) AS subquery
        WHERE giscedata_profiles_factura_lot.id=subquery.profile_factura_id"""

    cursor.execute(update_query)
    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
