from osv import osv, fields
import base64
from tools.translate import _
import pandas as pd
import StringIO
import bz2
from ..headers import AGCL_HEADER, AGGREGATION


class WizardAGCL(osv.osv_memory):

    _name = 'wizard.agcl'

    def action_agcl(self, cursor, uid, ids, context=None):
        """ Importa un fitxer d'Objeccions """
        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        period_obj = self.pool.get('giscedata.profiles.lot')
        agg_obj = self.pool.get('giscedata.profiles.aggregations')
        agcl_obj = self.pool.get('giscedata.profiles.agcl')

        lot_id = context.get('active_id', [])
        if not wizard.file:
            period_obj.make_agcl(cursor, uid, lot_id, context=context)
            wizard.write({'state': 'done', 'info': _(u"AGCL generat correctament")})
            return True
        try:
            content = StringIO.StringIO(bz2.decompress(base64.b64decode(wizard.file)))
        except IOError:
            content = StringIO.StringIO(base64.b64decode(wizard.file))

        df_agcl_os = pd.read_csv(content, sep=';', names=AGCL_HEADER, dtype={
            'distribuidora': 'str', 'comercialitzadora': 'str', 'agree_tipo': 'str', 'agree_tarifa': 'str'
        })

        agg_ids = period_obj.read(cursor, uid, lot_id, ['aggregations_ids'])['aggregations_ids']
        agg_data = agg_obj.read(cursor, uid, agg_ids, AGGREGATION + ['data_inici_ag', 'data_final_ag'])
        df_aggs = pd.DataFrame(data=agg_data)

        # Prepare dfs
        df_agcl_os['agree_tipo'] = df_agcl_os['agree_tipo'].apply(lambda x: x.zfill(2))
        df_aggs.replace(False, '', inplace=True)
        df_aggs['data_inici_ag'] = df_aggs['data_inici_ag'].apply(lambda x: x.replace('-', '/') + ' 00')
        df_aggs['data_final_ag'] = df_aggs['data_final_ag'].apply(lambda x: x.replace('-', '/') + ' 00')

        res = agcl_obj.gen_agcl(cursor, uid, lot_id, df_new=df_aggs, df_old=df_agcl_os, context=context)
        period_obj.attach_measures_file_to_lot(cursor, uid, lot_id, res, context=context)

        wizard.write({'state': 'done', 'info': _(u"AGCL generat correctament")})
        return True

    _columns = {
        'file': fields.binary('AGCL'),
        'filename': fields.binary('Nom', size=32),
        'state': fields.char('State', size=4),
        'info': fields.text('Info', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardAGCL()