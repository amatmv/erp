# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardMeasuresIntegrity(osv.osv_memory):

    _name = 'wizard.measures.integrity'

    def _default_clmag_file(self, cursor, uid, context=None):
        return self._default_file(cursor, uid, 'CLMAG', context=context)

    def _default_clmag5a_file(self, cursor, uid, context=None):
        return self._default_file(cursor, uid, 'CLMAG5A', context=context)

    def _default_file(self, cursor, uid, filename, context=None):
        """Si existeix un sol fitxer per període el retorna per defecte,
        altrament retorna un fitxer no existent, per activar l'onchange de
        selection_file i així filtrar els fitxers del període. Això es fa per
        no confondre el fitxer pel qual es vol revisar dades
        :return: clmag_id o clmag5a_id o clinmes_ids depen del model
        """
        period_id = context.get('active_id', [])
        attach_obj = self.pool.get('ir.attachment')
        periode_obj = self.pool.get('giscedata.profiles.lot')
        distribuidora = periode_obj.default_distribuidora(cursor, uid, context)
        file_ids_ = attach_obj.search(cursor, uid, [
            ('res_model', '=', 'giscedata.profiles.lot'),
            ('res_id', '=', period_id),
            ('name', '=like', '{}_{}%'.format(filename, distribuidora))
        ])
        if file_ids_:
            if len(file_ids_) > 1:
                return False
            else:
                return file_ids_[0]
        else:
            return False

    def selection_file(self, cursor, uid, ids, filename, context=None):
        """Retorna unicament els fitxers de mesures d'aquest periode
        :param context: dict with active_ids
        :return: dict like {
            'value': {},
            'domain': {'file': list_of_attachment_ids},
            'warning': {}
            }
        """
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        periode = context.get('active_ids', [])
        file_obj = self.pool.get('ir.attachment')
        periode_obj = self.pool.get('giscedata.profiles.lot')
        distribuidora = periode_obj.default_distribuidora(cursor, uid, context)
        file_column = filename.lower()
        if periode:
            periode = periode[0]
            file_ids = file_obj.search(cursor, uid, [
                ('res_model', '=', 'giscedata.profiles.lot'), ('res_id', '=', periode),
                ('name', '=like', '{}_{}%'.format(filename, distribuidora))
            ])

            res['domain'].update(
                {file_column: [('id', 'in', file_ids)]}
            )
        else:
            res['domain'].update({file_column: []})

        return res

    def action_measures_integrity(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        periode_obj = self.pool.get('giscedata.profiles.lot')
        measures_lot_obj = self.pool.get('giscedata.profiles.lot')
        context['clmag'] = wizard.clmag.id
        context['clmag5a'] = wizard.clmag5a.id
        context['clinmes'] = [x.id for x in wizard.clinmes]
        periode_id = context.get('active_id', [])
        measures_lot_obj.check_files(cursor, uid, [periode_id], context=context)
        periode_obj.write(
            cursor, uid, periode_id, {
                'clmag_integrity': wizard.clmag.name, 'clmag5a_integrity': wizard.clmag5a.name,
                'clinme_integrity': ', '.join([x.name for x in wizard.clinmes])
            }
        )
        msg_ = _(
            "{}\n\nCLMAG:\n{}\n\nCLMAG5A:\n{}\n\nCLINMES:\n{}".format(
                wizard.info, wizard.clmag.name, wizard.clmag5a.name, '\n'.join([x.name for x in wizard.clinmes])
            )
        )
        wizard.write({'state': 'end', 'info': msg_})

    _columns = {
        'clmag': fields.many2one(
            'ir.attachment', 'CLMAG', required=True
        ),
        'clmag5a': fields.many2one(
            'ir.attachment', 'CLMAG5A', required=True
        ),
        'clinmes': fields.many2many(
            'ir.attachment', 'giscedata_measures_integrity_clinmes',
            'clinme_id', 'id', 'CLINMES', required=True
        ),
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State', select=True, readonly=True),
        'info': fields.text('info', readonly=True),
    }

    _defaults = {
        'clmag': _default_clmag_file,
        'clmag5a': _default_clmag5a_file,
        'info': lambda *a: _('Consumos generados correctamente, revisar pantalla de comprobaciones'),
        'state': lambda *a: 'init'
    }


WizardMeasuresIntegrity()
