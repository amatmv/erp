# -*- coding: utf-8 -*-
from osv import osv, fields
from base64 import b64decode
import pandas as pd
from tools.translate import _
from tempfile import mkstemp
from base64 import b64encode
from ..headers import AGGREGATION, CLMAG5A_HEADER


class WizardMeasuresFileByAggregation(osv.osv_memory):

    _name = 'wizard.measures.file.by.aggregation'

    FILE_SELECTION = [('clmag', 'CLMAG'),
                      ('clmag5a', 'CLMAG5A'),
                      ('clinme', 'CLINME')]

    STATES = [('init', 'Init'), ('end', 'End')]

    AGREE_TYPES = {
        'clinme': ('agree_tipo', 'in', ['03', '04', '05']),
        'clmag': ('agree_tipo', 'in', ['03', '04']),
        'clmag5a': ('agree_tipo', '=', '05')
    }

    def _default_file_selector(self, cursor, uid, context=None):
        if context is None:
            context = {}

        attach_obj = self.pool.get('ir.attachment')
        lot_id = context.get('active_ids', [])
        if not lot_id:
            return False

        att_ids = attach_obj.search(cursor, uid, [
            ('res_model', '=', 'giscedata.profiles.lot'),
            ('res_id', '=', lot_id[0])])
        if att_ids:
            return att_ids
        else:
            return False

    def seleccionar_file_name(self, cursor, uid, ids, file_name, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if file_name:
            lot_obj = self.pool.get('giscedata.profiles.lot')
            attach_obj = self.pool.get('ir.attachment')
            lot_id = context.get('active_ids', [])
            distribuidora = lot_obj.default_distribuidora(cursor, uid, context)
            check_file = '{fname}_{distri}_%'.format(
                fname=file_name.upper(), distri=distribuidora
            )
            att_ids = attach_obj.search(cursor, uid, [
                ('res_id', '=', lot_id[0]),
                ('datas_fname', '=like', check_file),
                ('res_model', '=', 'giscedata.profiles.lot'),
            ])

            agg_obj = self.pool.get('giscedata.profiles.aggregations')
            agree_tipo = self.AGREE_TYPES[file_name]
            search_params = [agree_tipo, ('lot_id', '=', lot_id)]
            agg_ids = agg_obj.search(cursor, uid, search_params)

            res['domain'].update({
                'file_selector': [('id', 'in', att_ids)],
                'agregacions': [('id', 'in', agg_ids)]
            })
        else:
            res['domain'].update({
                'file_selector': [('res_model', '=', 'giscedata.profiles.lot')]
            })

        return res

    @staticmethod
    def parse_clmag5a(file):
        lines = []
        res = b64decode(file.datas)
        res = res.split('\n')[:-1]
        for line in res:
            line = line.split(';')
            vals = dict(zip(CLMAG5A_HEADER, line))
            lines.append(vals)
        return pd.DataFrame(data=lines)

    def action_file_creation(self, cursor, uid, ids, context=None):
        """
        Create a measures file by one or more aggregations
        :return: file creating in background
        """
        if context is None:
            context = {}

        aggr_obj = self.pool.get('giscedata.profiles.aggregations')
        lot_obj = self.pool.get('giscedata.profiles.lot')
        attach_obj = self.pool.get('ir.attachment')
        wiz_obj = self.browse(cursor, uid, ids[0], context)

        lot_id = context.get('active_ids', [])
        lot = lot_obj.browse(cursor, uid, lot_id[0])
        agg_ids = [x.id for x in wiz_obj.agregacions]
        aggs = aggr_obj.read(cursor, uid, agg_ids, [
            'comercialitzadora', 'agree_tensio', 'agree_tarifa', 'agree_dh',
            'agree_tipo', 'provincia'
        ])
        lot_obj.enqueue_measures_file(cursor, uid, lot_id, wiz_obj.file_name, aggs, context=context)

        wiz_obj.write({'state': 'end', 'info': _('Fitxer generat correctament')})

        return True

    _columns = {
        'file_name': fields.selection(FILE_SELECTION, 'Fitxer', required=True),
        'file_selector': fields.many2one(
            'ir.attachment', 'Fitxers',
            domain="[('res_model', '=', 'giscedata.profiles.lot')]"
        ),
        'agregacions': fields.many2many(
            'giscedata.profiles.aggregations', 'giscedata_measures_by_file_aggregations',
            'aggregation_id', 'id', 'Agregacions', required=True
        ),
        'merge': fields.boolean('Incluir a fitxer actual',
                                help="Añade los datos de esta agregación al "
                                     "contenido del fichero seleccionado a "
                                     "continuación."
                                     "Si los datos de esta agregación ya "
                                     "existen, se substituyen"),
        'state': fields.selection(STATES, 'states', select=True, required=True),
        'info': fields.text('Info', readonly=True),
    }

    _defaults = {
        'state': lambda *x: 'init',
        'background': lambda *x: True,
    }


WizardMeasuresFileByAggregation()
