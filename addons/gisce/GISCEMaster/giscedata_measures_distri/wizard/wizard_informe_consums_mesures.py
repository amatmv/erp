# -*- coding: utf-8 -*-
from osv import osv, fields
import base64
import pandas as pd
import numpy as np
from tools.translate import _
from ..headers import CLINME_HEADER, ACUM_HEADER
import StringIO
from zipfile import ZipFile, BadZipfile


class WizardInformeConsumsMesures(osv.osv_memory):

    _name = 'wizard.informe.consum.mesures'

    STATES = [('init', 'Init'), ('end', 'End')]

    @staticmethod
    def read_clinme(data):
        """ Parse and read CLINME file
        :param data: list of dicts with CLINME info
        :return: DataFrame with CLINME info
        """
        df_clinme = pd.read_csv(
            data, sep=';', names=CLINME_HEADER + ['res'],
            dtype={
                'distribuidora': 'str',
                'comercialitzadora': 'str',
                'agree_tipo': 'str',
                'agree_tarifa': 'str',
            }
        )

        return df_clinme

    @staticmethod
    def read_acum(data):
        """ Parse and read ACUM file
        :param data: list of dicts with ACUM info
        :return: DataFrame with ACUM info
        """
        df_acum = pd.read_csv(data, sep=';', names=ACUM_HEADER + ['res'])

        return df_acum

    @staticmethod
    def read_zip(_file):
        """ Read a zip file
        :param _file: b64 file
        :return: ZipFile
        """
        data = base64.b64decode(_file)
        fileHandle = StringIO.StringIO(data)
        try:
            return ZipFile(fileHandle, "r")
        except BadZipfile:
            raise osv.except_osv(_('Error'), _('El fitxer ha de ser un .zip'))

    def action_crear_informe_consum(self, cursor, uid, ids, context=None):
        """ create a report with consumption by tariff
        :return: binary file with content
        """
        if context is None:
            context = {}

        wiz_obj = self.browse(cursor, uid, ids[0], context)

        # CLINMES
        df_clinme = pd.DataFrame(data={})
        zip_content = self.read_zip(wiz_obj.clinme_file)
        for _file in zip_content.infolist():
            data = StringIO.StringIO(zip_content.read(_file.filename))
            month = _file.filename[17:23]
            dummy_df = self.read_clinme(data)
            if df_clinme.empty:
                df_clinme = dummy_df.copy()
            else:
                df_clinme = df_clinme.append(dummy_df)
        df_result = df_clinme.groupby(
            ['agree_tarifa', 'agree_dh']
        ).aggregate({'ai_fact': 'sum'}).reset_index()
        df_result = df_result.replace('21', '2.1')
        df_result = df_result.replace('2A', '2.0')
        df_result = df_result.replace('30', '3.0')
        df_result = df_result.replace('31', '3.1')
        df_result['agree_tarifa'] = np.where(
            df_result['agree_dh'] == 'E2',
            df_result['agree_tarifa'] + ' DHA',
            df_result['agree_tarifa'] + ' A'
        )
        df_result.drop(['agree_dh'], axis=1, inplace=True, errors='ignore')
        df_result.rename(
            columns={'agree_tarifa': 'tarifa', 'ai_fact': 'consumo'},
            inplace=True
        )

        if wiz_obj.acum_file:
            # ACUM (6.1 Tariffs)
            data = StringIO.StringIO(base64.b64decode(wiz_obj.acum_file))
            df_acum = self.read_acum(data)
            acum_res = sum(df_acum[df_acum['magnitud'] == 'AE']['m_principal'])
            df_result = df_result.append(
                pd.DataFrame(data=[{'tarifa': '6.1', 'consumo': acum_res}])
            )

        # Calc total
        total = df_result['consumo'].sum()
        df_result = df_result.append(
            pd.DataFrame(data=[{'tarifa': 'TOTAL', 'consumo': total}])
        )
        consumo_mes = 'Consumo a {month}'.format(month=month)
        df_result.rename(columns={'consumo': consumo_mes}, inplace=True)

        # Write result
        str_data = df_result.to_csv(
            sep=';', index=None, columns=['Tarifa', consumo_mes]
        )
        base64_file = base64.b64encode(str_data)
        filename = 'Informe_consumos_{month}.csv'.format(month=month)
        wiz_obj.write({'result_file': base64_file, 'result_fname': filename, 'state': 'end'})

    _columns = {
        'clinme_file': fields.binary(
            'CLINMES', help=_('Fitxer .zip'), required=True
        ),
        'acum_file': fields.binary('ACUM'),
        'result_file': fields.binary('Resultat'),
        'result_fname': fields.char('consumptions', size=256),
        'state': fields.selection(STATES, 'states', select=True, required=True),
    }

    _defaults = {
        'state': lambda *x: 'init',
    }


WizardInformeConsumsMesures()
