# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
import calendar
from giscedata_profiles.utils import crear_lots_perfilacio

class WizardCrearLotsMesures(osv.osv_memory):

    _name = 'wizard.crear.lots.mesures'

    STATES = [('init', 'Init'), ('end', 'End')]

    def action_crear_lots(self, cursor, uid, ids, context=None):
        """
        Create measures period by year
        :param year: wizard_param - Can specify the year. Default: current
        year +1, since batches are created before
        """
        if context is None:
            context = {}

        lot_obj = self.pool.get('giscedata.profiles.lot')
        wiz_obj = self.browse(cursor, uid, ids[0], context)

        lot_id = lot_obj.search(
            cursor, uid, [('name', '=', '01/' + str(wiz_obj.year))]
        )
        if lot_id:
            raise osv.except_osv(
                _('Error!'),
                _('Ya existen lotes para este año')
            )

        lots = crear_lots_perfilacio(year=wiz_obj.year)
        for lot in lots:
            lot_obj.create(cursor, uid, lot)
        wiz_obj.write({'state': 'end', 'info': _('Períodes creats')})

        return True

    _columns = {
        'year': fields.integer("Any"),
        'state': fields.selection(STATES, 'states', select=True, required=True),
        'info': fields.text('Info'),
    }

    def _get_default_year(self, cursor, uid, context=None):
        year = calendar.datetime.datetime.now().year
        return year + 1

    _defaults = {
        'year': _get_default_year,
        'state': lambda *a: 'init',
    }


WizardCrearLotsMesures()
