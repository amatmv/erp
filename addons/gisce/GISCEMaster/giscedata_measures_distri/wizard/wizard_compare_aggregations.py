# -*- coding: utf-8 -*-
from osv import osv, fields
import pandas as pd
from ..headers import AGGREGATION
import sys
from tools.translate import _


class WizardCompareAggregations(osv.osv_memory):
    _name = 'wizard.compare.aggregations'

    STATES = [('init', 'Init'), ('end', 'End')]

    def _default_periode(self, cursor, uid, context=None):
        """Get default last period"""
        periode_id = context.get('active_id', None)
        periode_obj = self.pool.get('giscedata.profiles.lot')
        if periode_id:
            periode_id -= 1
            if periode_obj.read(cursor, uid, periode_id, ['name']):
                return periode_id
            else:
                return False
        else:
            return False

    def action_compare_aggregations(self, cursor, uid, ids, context=None):
        """Create a comparative aggregations"""
        if not context:
            context = {}

        wiz_obj = self.browse(cursor, uid, ids[0], context)
        periode_obj = self.pool.get('giscedata.profiles.lot')
        aggregations_obj = self.pool.get('giscedata.profiles.aggregations')
        agg_comparative_obj = self.pool.get(
            'giscedata.profiles.aggregations.comparative'
        )
        periode_id = context.get('active_id', None)

        agg_ids = aggregations_obj.search(
            cursor, uid, [('lot_id', '=', periode_id)]
        )
        df_new = pd.DataFrame(
            aggregations_obj.read(cursor, uid, agg_ids,
                                  AGGREGATION + ['consum'])
        )
        agg_ids = aggregations_obj.search(
            cursor, uid, [('lot_id', '=', wiz_obj.periode.id)]
        )
        df_old = pd.DataFrame(
            aggregations_obj.read(cursor, uid, agg_ids,
                                  AGGREGATION + ['consum'])
        )
        df_new['new'] = 'A'
        df_old['old'] = 'B'
        df = pd.merge(df_old, df_new, on=AGGREGATION, how='right')
        df['diferencia'] = abs(df['consum_x'] - df['consum_y'])
        for agg in df.T.to_dict().values():
            try:
                diff = abs(agg['diferencia'] / max(agg['consum_x'], agg['consum_y'])) * 100
            except ZeroDivisionError:
                diff = 0
            finally:
                if diff >= wiz_obj.percentual:
                    res = {
                        'as_id': wiz_obj.id,
                        'comercialitzadora': agg['comercialitzadora'],
                        'agree_tensio': agg['agree_tensio'],
                        'agree_tarifa': agg['agree_tarifa'],
                        'agree_dh': agg['agree_dh'],
                        'agree_tipo': agg['agree_tipo'],
                        'provincia': agg['provincia'],
                        'consum_actual': agg['consum_y'],
                        'consum_anterior': agg['consum_x'],
                        'diferencia': diff
                    }
                    agg_comparative_obj.create(cursor, uid, res)

    _columns = {
        'periode': fields.many2one(
            'giscedata.profiles.lot', 'Període', required=True
        ),
        'percentual': fields.integer('% Diferéncia'),
        'agregacions': fields.one2many(
            'giscedata.profiles.aggregations.comparative', 'as_id',
            'Aggregacions'
        ),
        'state': fields.selection(STATES, 'states', select=True, required=True),
    }

    _defaults = {
        'periode': _default_periode,
        'state': lambda *a: 'init',
    }


WizardCompareAggregations()


class GiscedataProfilesAggregationsComparative(osv.osv_memory):
    """Polisses a treure.
    """
    _name = 'giscedata.profiles.aggregations.comparative'
    # _inherit = 'giscedata.profiles.aggregations'
    _max_count = sys.maxint

    _columns = {
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True
        ),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'consum_actual': fields.integer('Consum Actual'),
        'consum_anterior': fields.integer('Consum Anterior'),
        'diferencia': fields.integer('% Diff', help=_(
                'Mostra les agregacions amb diferéncia igual o '
                'superior al percentatge indicat.\n Si es deixa a 0: Mostra '
                'totes les agregacions')),
        'as_id': fields.many2one('wizard.compare.aggregations', 'Comparativa'),
    }


GiscedataProfilesAggregationsComparative()
