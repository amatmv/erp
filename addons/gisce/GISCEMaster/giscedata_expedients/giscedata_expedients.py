# -*- coding: utf-8 -*-

from osv import osv, fields

class giscedata_expedients_projecte_tipus(osv.osv):

    _name = "giscedata.expedients.projecte.tipus"
    _description = "Tipus de projecte"

    _columns = {
      'name': fields.char('Tipus', size=60),
    }

    _defaults = {

    }

    _order = "name, id"

giscedata_expedients_projecte_tipus()

class giscedata_expedients_expedient(osv.osv):
    def _categoria(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("SELECT e.id,p.tipus from giscedata_expedients_expedient e left join giscedata_expedients_projecte p on (p.expedient = e.id) where e.id in (%s)" % (','.join(map(str, map(int, ids)))))
        # Many2many widget
        for exp in cr.fetchall():
            if res.has_key(exp[0]):
                res[exp[0]].append(exp[1])
            else:
                res[exp[0]] = []
                if exp[1]:
                    res[exp[0]].append(exp[1])
        return res

    def _categoria_search(self, cr, uid, obj, name, args, context=None):
        if not args:
            return []
        else:
            cr.execute("select p.expedient from giscedata_expedients_projecte p, giscedata_expedients_projecte_tipus t where p.tipus = t.id and t.name ilike %s", ('%'+args[0][2]+'%',))
            return [('id', 'in', [a[0] for a in cr.fetchall()])]

    def _rel_ct(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict([(x, 0) for x in ids])
        cursor.execute("select distinct expedient_id from giscedata_cts_expedients_rel")
        exp = dict([(x[0], 1) for x in cursor.fetchall()])
        res.update(exp)
        return res

    def _rel_ct_search(self, cursor, uid, obj, name, args, context=None):
        if not args:
            return []
        cursor.execute("select distinct expedient_id from giscedata_cts_expedients_rel")
        ids = [x[0] for x in cursor.fetchall()]
        return [('id', 'in', ids)]

    def _rel_lat(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict([(x, 0) for x in ids])
        cursor.execute("select distinct expedient_id from giscedata_at_tram_expedient")
        exp = dict([(x[0], 1) for x in cursor.fetchall()])
        res.update(exp)
        return res

    def _rel_lat_search(self, cursor, uid, obj, name, args, context=None):
        if not args:
            return []
        cursor.execute("select distinct expedient_id from giscedata_at_tram_expedient")
        ids = [x[0] for x in cursor.fetchall()]
        return [('id', 'in', ids)]

    def name_get(self, cr, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for expedient in self.browse(cr, uid, ids):
            name = '%s' % (expedient.empresa)
            res.append((expedient.id, name))
        return res


    _name = "giscedata.expedients.expedient"
    _description = "Expedients"

    _columns = {
      'name': fields.integer('Codi'),
      'descripcio': fields.text('Descripció'),
      'industria': fields.char('Expedient Indústria', size=25),
      'industria_data': fields.date('Data d\'autorització'),
      'empresa': fields.char('Expedient Intern', size=25),
  #    'empresa_data': fields.function(_data_pm, type='char', method=True, string='Data posada en marxa'),
      'observacions': fields.text('Observacions'),
      'data_inici': fields.date('Data d\'inici tramitació'),
      'taxes': fields.float('Import taxes'),
      'fianca': fields.float('Import fiança'),
      'sol_devolucio': fields.boolean('Devolució sol·licitada'),
      'data_sol_devolucio': fields.date('Data sol·licitud'),
      'fianca_dev': fields.boolean('Fiança retornada'),
      'data_visat': fields.date('Data visat final d\'obra'),
      'n_visat': fields.char('Número visat final d\'obra', size=25),
      'import_visat': fields.float('Import visat final d\'obra'),
      'data_connexio_prov': fields.date('Data sol·licitud connexió'),
      'data_acta': fields.date('Data acta definitiva'),
      'canvi_titularitat': fields.boolean('Canvi de titularitat'),
      'data_sol_canvi_titularitat': fields.date('Data sol·licitud canvi titularitat'),
      'societat_ben_canvi_titul': fields.integer('Societat beneficiària'),
      'data_canvi_titularitat': fields.date('Data resolució canvi titularitat'),
      'seguiment': fields.text('Seguiment del procediment'),
      'caract': fields.text('Característiques'),
      'descct': fields.char('Desct', size=60),
      'data_cfo': fields.date('Data CFO'),
      'doc_exp': fields.binary('Document'),
      'projectes': fields.one2many('giscedata.expedients.projecte', 'expedient', 'Projectes'),
      'categoria': fields.function(_categoria, fnct_search=_categoria_search, type='many2many', relation='giscedata.expedients.projecte.tipus', method=True, string='Categoria'),
      'rel_ct': fields.function(_rel_ct, fnct_search=_rel_ct_search,
                                type='boolean', method=True,
                                string="Relacionat amb CTs"),
      'rel_lat': fields.function(_rel_lat, fnct_search=_rel_lat_search,
                                type='boolean', method=True,
                                string="Relacionat amb LATs"),
      'active': fields.boolean('Actiu'),
      'tipus': fields.many2one('giscedata.expedients.projecte.tipus', 'Tipus'),
    }

    _defaults = {
      'active': lambda *a: 1,
    }

    _sql_constraints = [
            ('empresa_uniq', 'unique (empresa)', 'Ja existeix un expedient amb aquest número intern')
    ]

    _order = "empresa, name, id"

giscedata_expedients_expedient()


class giscedata_expedients_projecte(osv.osv):

    _name = "giscedata.expedients.projecte"
    _description = "Projectes"

    _columns = {
      'name': fields.char('Nº de Projecte', size=25),
      'codi': fields.integer('Codi'),
      'data': fields.date('Data'),
      'n_visat': fields.char('Nº de Visat', size=25),
      'data_visat': fields.date('Data Visat'),
      'import_visat': fields.float('Import Visat'),
      'data_enviament': fields.date('Data d\'enviament'),
      'n_copies': fields.integer('Nº de còpies'),
      'descripcio': fields.char('Descripció', size=512),
      'observacions': fields.text('Observacions'),
      'control': fields.boolean('Control'),
      'expedient': fields.many2one('giscedata.expedients.expedient', 'Expedient'),
      'tipus': fields.many2one('giscedata.expedients.projecte.tipus', 'Tipus'),
      'planols': fields.one2many('giscedata.expedients.projecte.planol', 'projecte', 'Plànols'),
    }

    _defaults = {

    }

    _order = "name, id"

giscedata_expedients_projecte()

class giscedata_expedients_projecte_planol_tipus(osv.osv):

    _name = 'giscedata.expedients.projecte.planol.tipus'

    _columns = {
        'name': fields.char('Tipus plànol', size=256, required=True),
    }

giscedata_expedients_projecte_planol_tipus()


class giscedata_expedients_projecte_planol(osv.osv):

    _name = "giscedata.expedients.projecte.planol"
    _description = "Plànols d'un projecte"

    def _tipus_selection(self, cr, uid, context=None):
        res = []
        tipus_obj = self.pool.get('giscedata.expedients.projecte.planol.tipus')
        tipus_ids = tipus_obj.search(cr, uid, [])
        for tipus_val in tipus_obj.read(cr, uid, tipus_ids, ['id', 'name']):
            res.append((tipus_val['id'], tipus_val['name']))
        return res

    _columns = {
      'name': fields.char('Referència', size=60),
      'data': fields.date('Data'),
      'autor': fields.char('Autor', size=60),
      'fitxer': fields.binary('Fitxer'),
      'observacions': fields.text('Observacions'),
      'projecte': fields.many2one('giscedata.expedients.projecte', 'Projecte'),
      'tipus': fields.many2one('giscedata.expedients.projecte.planol.tipus', 'Tipus', widget='selection', selection=_tipus_selection),
    }

    _defaults = {

    }

    _order = "name, id"

giscedata_expedients_projecte_planol()
