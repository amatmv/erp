# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients",
    "description": """Expedients 


 $Id: __terp__.py 1247 2008-01-21 08:53:07Z raul $""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Expedients",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_view.xml",
        "giscedata_expedients_report.xml",
        "ir.model.access.csv",
        "security/giscedata_expedients_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
