<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="projectes"/>
  </xsl:template>

  <xsl:template match="projectes">
    <document>
      <template>
        <pageTemplate id="main">
          <pageGraphics>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="1cm" y="1cm">Data impressi� <xsl:value-of select="data_impressio" /></drawString>
          	<drawRightString x="20cm" y="1cm">P�gina <pageNumber /></drawRightString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle
        	name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
          leading="20"
        />


        <paraStyle
          name="text"
          fontName="Helvetica"
          fontSize="10"
        />
        
        <blockTableStyle id="taula_contingut">
          <blockValign value="TOP" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="projectes">
          <blockFont name="Helvetica" size="10" />
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="-1,0" />
        	<lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <para style="titol">Llistat de projectes</para>
        <blockTable repeatRows="1" style="projectes" colWidths="1.5cm,2.5cm,2.5cm,10cm,3.5cm">
          <tr>
            <td>N�</td>
            <td>Data</td>
            <td>Tipus</td>
            <td>Descripci�</td>
            <td>Exp. Ind�stria</td>
          </tr>
          <xsl:apply-templates select="projecte" mode="story">
          	<xsl:sort select="numero" />
          </xsl:apply-templates>
        </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="projecte" mode="story">
    <tr>
      <td><xsl:value-of select="numero" /></td>
      <td><xsl:if test="data!=0"><xsl:value-of select="concat(substring(data, 9, 2),'/',substring(data, 6, 2),'/',substring(data, 1, 4))" /></xsl:if></td>
      <td><para style="text"><xsl:value-of select="tipus" /></para></td>
      <td><para style="text"><xsl:value-of select="descripcio" /></para></td>
      <td><xsl:value-of select="expedient" /></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
