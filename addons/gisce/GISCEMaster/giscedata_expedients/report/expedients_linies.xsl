<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="expedients"/>
  </xsl:template>

  <xsl:template match="expedients">
    <document>
      <template pageSize="(29.7cm, 21cm)">
        <pageTemplate id="main">
          <pageGraphics>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="1cm" y="1cm">Data impressi� <xsl:value-of select="data_impressio" /></drawString>
          	<drawRightString x="28.7cm" y="1cm">P�gina <pageNumber /></drawRightString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="27.7cm" height="19cm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle
        	name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
          leading="20"
        />


        <paraStyle
          name="text"
          fontName="Helvetica"
          fontSize="9"
        />
        
        <blockTableStyle id="taula_contingut">
          <blockValign value="TOP" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="expedients">
          <blockFont name="Helvetica" size="9" />
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="-1,0" />
        	<lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <para style="titol">Llistat d'expedients per L�nies</para>
        <blockTable repeatRows="1" style="expedients" colWidths="1.5cm,5cm,1.5cm,2cm,11cm,2cm,2cm,2cm">
          <tr>
            <td>L�nia</td>
            <td>Descripci�</td>
            <td>Intern</td>
            <td>Ind�stria</td>
            <td>Descripci�</td>
            <td>Data inici</td>
            <td>Data CFO</td>
            <td>Data P.M.</td>
          </tr>
          <!--<xsl:apply-templates select="expedient" mode="story" />-->
          <xsl:for-each select="//expedients/expedient/trams/tram">
          <xsl:sort select="linia" />
          <tr>
          	<td><para style="text"><xsl:value-of select="linia" /></para></td>
          	<td><para style="text"><xsl:value-of select="linia_desc" /></para></td>
          	<td><para style="text"><xsl:value-of select="../../exp_intern" /></para></td>
          	<td><para style="text"><xsl:value-of select="../../exp_industria" /></para></td>
          	<td><para style="text"><xsl:value-of select="../../descripcio" /></para></td>
          	<td><xsl:if test="../../data_inici!=0"><xsl:value-of select="concat(substring(../../data_inici, 9, 2),'/',substring(../../data_inici, 6, 2),'/',substring(../../data_inici, 1, 4))" /></xsl:if></td>
      <td><xsl:if test="../../data_cfo!=0"><xsl:value-of select="concat(substring(../../data_cfo, 9, 2),'/',substring(../../data_cfo, 6, 2),'/',substring(../../data_cfo, 1, 4))" /></xsl:if></td>
      <td><xsl:if test="../../data_industria!=0"><xsl:value-of select="concat(substring(../../data_industria, 9, 2),'/',substring(../../data_industria, 6, 2),'/',substring(../../data_industria, 1, 4))" /></xsl:if></td>
          </tr>
          </xsl:for-each>
        </blockTable>
      </story>
    </document>
  </xsl:template>


</xsl:stylesheet>
