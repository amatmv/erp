<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="expedients"/>
  </xsl:template>

  <xsl:template match="expedients">
    <document>
      <template>
        <pageTemplate id="main">
          <pageGraphics>
          	<setFont name="Helvetica" size="8" />
          	<drawString x="1cm" y="1cm">Data impressi� <xsl:value-of select="data_impressio" /></drawString>
          	<drawRightString x="20cm" y="1cm">P�gina <pageNumber /></drawRightString>
          </pageGraphics>
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle
        	name="titol"
        	fontName="Helvetica-Bold"
        	fontSize="14"
          leading="20"
        />


        <paraStyle
          name="text"
          fontName="Helvetica"
          fontSize="10"
        />
        
        <blockTableStyle id="taula_contingut">
          <blockValign value="TOP" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="expedients">
          <blockFont name="Helvetica" size="10" />
        	<blockBackground colorName="grey" start="0,0" stop="5,0" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="5,0" />
        	<lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
        <para style="titol" t="1">Llistat d'expedients</para>
        <blockTable repeatRows="1" style="expedients" colWidths="1.5cm,3cm,8cm,2.5cm,2.5cm,2.5cm">
          <tr>
            <td t="1">Intern</td>
            <td t="1">Ind�stria</td>
            <td t="1">Descripci�</td>
            <td t="1">Data inici</td>
            <td t="1">Data CFO</td>
            <td t="1">Data P.M.</td>
          </tr>
          <xsl:apply-templates select="expedient" mode="story">
          	<xsl:sort select="exp_intern" />
          </xsl:apply-templates>
        </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="expedient" mode="story">
    <tr>
      <td><xsl:value-of select="exp_intern" /></td>
      <td><para style="text"><xsl:value-of select="exp_industria" /></para></td>
      <td><para style="text"><xsl:value-of select="descripcio" /></para></td>
      <td><xsl:if test="data_inici!=0"><xsl:value-of select="concat(substring(data_inici, 9, 2),'/',substring(data_inici, 6, 2),'/',substring(data_inici, 1, 4))" /></xsl:if></td>
      <td><xsl:if test="data_cfo!=0"><xsl:value-of select="concat(substring(data_cfo, 9, 2),'/',substring(data_cfo, 6, 2),'/',substring(data_cfo, 1, 4))" /></xsl:if></td>
      <td><xsl:if test="data_industria!=0"><xsl:value-of select="concat(substring(data_industria, 9, 2),'/',substring(data_industria, 6, 2),'/',substring(data_industria, 1, 4))" /></xsl:if></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
