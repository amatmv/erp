# -*- coding: utf-8 -*-
from osv import osv


class GiscedataSwitchingModConWizard(osv.osv_memory):
    _name = 'giscedata.switching.mod.con.wizard'
    _inherit ='giscedata.switching.mod.con.wizard'

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        res = super(GiscedataSwitchingModConWizard, self).genera_casos_atr(cursor, uid, ids, context=context)

        pol_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)
        pol_id = wizard.contract.id
        change_type = self.get_change_type(wizard.change_atr,
                                           wizard.change_adm)
        if wizard.proces == 'M1' and change_type == 'owner' \
           and wizard.owner_change_type in ['T', 'S']:
            imd_obj = self.pool.get('ir.model.data')
            categ_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching_polissa_category',
                "giscedata_polissa_m1_baixa"
            )
            if categ_id:
                old_categ = pol_obj.read(cursor, uid, pol_id, ['category_id'])
                categs = (old_categ['category_id'] or []) + [categ_id[1]]
                pol_obj.write(cursor, uid, pol_id, {'category_id': [(6, 0, categs)]})

        return res

GiscedataSwitchingModConWizard()
