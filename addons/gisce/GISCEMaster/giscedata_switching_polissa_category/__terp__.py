# -*- coding: utf-8 -*-
{
  "name": "Switching Polissa Category",
  "description": "",
  "version": "0-dev",
  "author": "GISCE",
  "category": "GISCEMaster",
  "depends": ['giscedata_polissa_category', 'giscedata_switching'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['giscedata_switching_data.xml'],
  "active": False,
  "installable": True
}
