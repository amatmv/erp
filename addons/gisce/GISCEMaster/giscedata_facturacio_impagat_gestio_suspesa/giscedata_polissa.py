# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'gestio_baixa_suspesa': fields.boolean(string="Gestió de Cobrament Suspesa")
    }

    _defaults = {
        'gestio_baixa_suspesa': lambda *a: False
    }

GiscedataPolissa()
