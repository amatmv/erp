# -*- coding: utf-8 -*-
{
    "name": "Gestió Impagats Suspesa",
    "description": """Gestió Impagats Suspesa.
        Per aturar la suspensió i baixa de determinats contractes marcats amb "Gestió de cobraments suspesa".""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_facturacio_impagat",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscedata_polissa_view.xml",
    ],
    "active": False,
    "installable": True
}
