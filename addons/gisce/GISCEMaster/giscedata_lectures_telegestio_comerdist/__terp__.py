# -*- coding: utf-8 -*-
{
    "name": "Lectures Telegestio Comerdist",
    "description": """
    Manages the comerdist parameters for Lectures Telegestio module
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_lectures_telegestio"
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
