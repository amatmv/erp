# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class TgReader(osv.osv):
    _name = 'tg.reader'
    _inherit = 'tg.reader'

    def insert_values_S06(self, cursor, uid, meter, version, context=None):

        context.update({'sync': False})
        return super(TgReader, self).insert_values_S06(cursor, uid, meter,
                                                       version, context=context)

TgReader()
