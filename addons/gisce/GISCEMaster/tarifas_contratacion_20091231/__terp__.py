# -*- coding: utf-8 -*-
{
    "name": "Tarifas Contractació Gener 2010",
    "description": """
Actualització de les tarifes de contractació segons el BOE nº 315 - 31/12/2009.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_contractacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "tarifa_contratacion_20091231_data.xml"
    ],
    "active": False,
    "installable": True
}
