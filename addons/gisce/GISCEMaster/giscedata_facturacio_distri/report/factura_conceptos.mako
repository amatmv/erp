<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_distri/report/stylesheet_factura.css"/>
    <title>Factura</title>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0

        def get_giscedata_fact(invoice):
            sql = """SELECT * FROM giscedata_facturacio_factura WHERE id=%s""" % invoice.id
            cursor.execute(sql)
            return cursor.dictfetchall()

        def clean_vat(vat):
            if vat:
                return vat.replace('ES', '')
            else:
                return ''
    %>

    %for inv in objects :

    <div class="background">
    </div>
    <div class="wrapper">
        <div class="page_princial" >

            <%
                factura = get_giscedata_fact(inv)[0]
                try:
                    polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id,
                                                 context={'date': inv.date_invoice})
                except:
                    polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id)


                direccio = '%s %s %s %s %s' %(inv.polissa_id.cups.nv or '',
                                             inv.polissa_id.cups.pnp or '',
                                             inv.polissa_id.cups.es or '',
                                             inv.polissa_id.cups.pt or '',
                                             inv.polissa_id.cups.pu or ''
                                             )
            %>

            <% setLang(inv.partner_id.lang) %>
            <!-- capcalera -->
            <div class="capcalera">
                <div class="banner">
                    <div class="logo_container float_l">
                        <table width="100%" height="100%" align="center" valign="center">
                            <tr><td>
                                <img class="logo" src="data:image/jpeg;base64,${company.logo}"/>
                            </td></tr>
                        </table>
                    </div>
                    <div class="title">
                        <%
                            partner_title = company.partner_id.title
                            partner_name = company.partner_id.name
                            if partner_title:
                                partner_title = partner_title.upper()
                            endif
                            if partner_name:
                                partner_name = partner_name.upper()
                            endif
                        %>
                        <h1>${partner_title or ''|entity} ${partner_name |entity}</h1>
                    </div>
                </div>
                <div class="direccions">
                    <%
                        phone = company.partner_id.address[0].phone
                        fax = company.partner_id.address[0].fax
                        email = company.partner_id.address[0].email
                        company_address = company.partner_id

                        if phone:
                            phone = "- Tel: " + phone
                        else:
                            phone = ''
                        endif

                        if fax:
                            fax = "- Fax: " + fax
                        else:
                            fax = ''
                        endif

                        if email:
                            email = " Correo: " + email
                        else:
                            email = ''
                        endif
                    %>
                    ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''}
                    ${company_address.address[0].zip or ''} ${company_address.city or ''}
                    ${phone} ${fax} <div style="display: block"> ${email} </div>
                </div>
                <!--<div class="clear fi_bloc"></div>-->
            </div><!-- Fi capcalera -->
            <!-- part central -->
            <div id="outer">
                <p id="vertical_text">
                    ${company.rml_footer2}
                </p>
            </div>
            <div class="row">
                <div class="column left">
                    <div class="box">
                        <%
                            factura = get_giscedata_fact(inv)[0]
                            if factura['data_inici']:
                                date_periodo_inicio = datetime.strptime(factura['data_inici'], '%Y-%m-%d')
                            if factura['data_final']:
                                date_periodo_fin = datetime.strptime(factura['data_final'], '%Y-%m-%d')
                            date_emision = datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                        %>
                        <table class="dades">
                            <tr class="header">
                                <td>${_(u"Fecha emisión:")}</td>
                                <td>${("{}/{}/{}".format(date_emision.day,date_emision.month,date_emision.year)) or ''}</td>
                            </tr>
                            <tr>
                                <td>${_(u"Nº Factura:")}</td>
                                <td>${inv.number or ''|entity}</td>
                            </tr>
                            <tr>
                                <td>${_(u"NIF/CIF:")}</td>
                                <td>${clean_vat(inv.partner_id.vat)}</td>
                            </tr>
                            <tr>
                                <td>${_(u"C.N.A.E.:")}</td>
                                <td>${polissa.cnae.name}</td>
                            </tr>
                            <tr class="response">
                                <td><b>${_(u"TOTAL:")}</b></td>
                                <td><b>${formatLang(inv.amount_total, digits=2)} €</b></td>
                            </tr>
                            %if inv.tipo_rectificadora in ['B', 'A', 'BRA']:
                                <tr class="response"><td colspan="4">${_(u"Esta factura anula la factura")} ${inv.ref.number or ''}</td></tr>
                            %endif

                            %if inv.tipo_rectificadora in ['R', 'RA']:
                                <tr class="response"><td colspan="4">${_(u"Esta factura rectifica la factura")} ${inv.ref.number or ''}</td></tr>
                            %endif
                        </table>
                    </div>
                </div>
                <div class="right column">
                    <div class="direccio_comer center">
                       <table class="dades">
                            <tr><td><b>${inv.partner_id.title or ''|entity}  ${inv.partner_id.name |entity}</b></td></tr>
                            <tr><td>${inv.address_invoice_id.street or ''|entity}</td></tr>
                            <tr><td>${inv.address_invoice_id.street2 or ''|entity}</td></tr>
                            <tr><td>${inv.address_invoice_id.zip or ''|entity} ${inv.address_invoice_id.city or ''|entity}</td></tr>
                       </table>
                    </div>
                </div>
            </div>
            <div class="m_sides_20">
                <div class="row">
                    <div class="column left_conceptes">
                        <div class="box">
                            <div class="box_title">
                                ${_(u"Datos del punto de suministro")}
                                <hr/>
                            </div>
                            <table class="dades">
                                <tr>
                                    <td><b>Titular:</b></td>
                                    <td>
                                        ${inv.polissa_id.titular.name.upper() |entity}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Dirección:</b></td>
                                    <td>
                                        ${inv.polissa_id.cups.nv or ''}
                                        ${inv.polissa_id.cups.pnp or ''}
                                        ${inv.polissa_id.cups.es or ''}
                                        ${inv.polissa_id.cups.pt or ''}
                                        ${inv.polissa_id.cups.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Población:</b></td>
                                    <td>
                                        ${inv.polissa_id.cups.id_municipi.name or ''}, ${inv.polissa_id.cups.id_poblacio.name or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>CUPS:</b></td>
                                    <td>
                                        ${inv.polissa_id.cups.name or '-'}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="column right_conceptes">
                        <div class="box">
                            <div class="box_title">
                                ${_(u"Datos de contratación")}
                                <hr/>
                            </div>

                            <table class="dades_conceptes">
                                <tr class="header">
                                   <td><b>${_(u"Tarifa:")}</b></td>
                                   <td>${polissa.tarifa.name or ''|entity}</td>
                                </tr>
                                <tr>
                                    <td><b>${_(u"BOE:")}</b></td>
                                    <td>
                                        ${inv.date_boe or ''|entity}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>${_(u"Tensión:")}</b></td>
                                    <td>
                                        ${polissa.tensio or ''|entity} V
                                    </td>
                                </tr>
                                <tr>
                                <td><b>${_(u"Potencia:")}</b></td>
                                    <td>
                                        ${formatLang(polissa.potencia) or ''|entity} kW
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>${_(u"Póliza:")}</b></td>
                                    <td>
                                        ${inv.polissa_id.name}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="p_10">
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Facturación")}
                            <hr/>
                        </div>
                        ## <div class="second_box_title">
                        ##     ${_(u"Otros conceptos")}
                        ## </div>
                        <%
                            i = 0
                            total_otros = 0
                        %>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 85%"/>
                                <col style="width: 15%"/>
                            </colgroup>
                            <tbody>
                                <%
                                    lines = [l for l in inv.linia_ids if l.tipus not in 'lloguer']
                                    lines_lloguer = [l for l in inv.linia_ids if l.tipus in 'lloguer']
                                %>
                                %for i, linia in enumerate(lines):
                                    <tr>
                                        <td class="align_left">${linia.name}</td>
                                        <td>${formatLang(linia.price_subtotal, 2)} €</td>
                                        <% total_otros = total_otros + linia.price_subtotal %>
                                    </tr>
                                %endfor
                            </tbody>
                        </table>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 39%"/>
                                <col style="width: 20%"/>
                                <col style="width: 20%"/>
                                <col style="width: 20%"/>
                            </colgroup>
                            <tbody>
                                %for i, linia in enumerate(lines_lloguer):
                                    <tr>
                                        <td class="align_left">${linia.name}</td>
                                        <td>${formatLang(linia.price_unit_multi, digits=2)} €/dia</td>
                                        <td>${formatLang(linia.quantity, digits=0)} dias</td>
                                        <td>${formatLang(linia.price_subtotal, 2)} €</td>
                                        <% total_otros = total_otros + linia.price_subtotal %>
                                    </tr>
                                %endfor
                            </tbody>
                        </table>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Impuestos")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 15%"/>
                                <col style="width: 68%"/>
                                <col style="width: 13%"/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>CONCEPTO</td>
                                    <td>BASE</td>
                                    <td>TOTAL</td>
                                </tr>
                                %if inv.tax_line :
                                    %for t in inv.tax_line :
                                        %if 'especial sobre la electricidad'.upper() in t.name.upper():
                                            <tr>
                                                <td>${_(u"IESE")}</td>
                                                <td>${ formatLang(t.base, digits=2)|entity}</td>
                                                <td>${ formatLang(t.amount, digits=2) }</td>
                                            </tr>
                                        %elif 'IVA' in t.name.upper():
                                            <tr>
                                                <td>${ t.name.upper()}</td>
                                                <td>${ formatLang(t.base, digits=2)|entity}</td>
                                                <td>${ formatLang(t.amount, digits=2) }</td>
                                            </tr>
                                        %endif
                                    %endfor
                                %endif
                            </tbody>
                        </table>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 70%"/>
                                <col style="width: 30%"/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><b>TOTAL FACTURA</b></td>
                                    <td><b>${formatLang(inv.amount_total, digits=2)} €</b></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Datos de pago")}
                            <hr/>
                        </div>
                        <div class="text_box">
                            ${inv.polissa_id.tipo_pago.name}
                        </div>
                        <div class="second_box_title">
                            Información adicional
                        </div>
                        <hr/>
                        <div class="text_box">
                            ${inv.comment or ''}
                        </div>
                    </div>
                </div>
            </div>
        </div>

                <div class="clear fi_bloc"></div>
            </div> <!-- part central -->
        </div>
        <%
            comptador_factures += 1;
        %>
        % if comptador_factures<len(objects):
            <p style="page-break-after:always"></p>
        % endif
    </div>
    %endfor
</body>
</html>
