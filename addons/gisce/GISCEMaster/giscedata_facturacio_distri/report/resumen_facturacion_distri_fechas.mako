<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<head>
		<style type="text/css">
	    ${css}
	    </style>
        <style type="text/css">
            body {
                font-family: helvetica;
                font-size: 12px;
                margin-right: 10px;
                margin-left: 10px;
                margin-top: 20px;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            table th,table td {
                text-align: center;
                font-size: 10px;
                vertical-align: top;
            }

            .text_capcalera table td, .text_capcalera table th {
                text-align: left;
                font-size: 10px;
            }
            table tfoot{
                border-top: double 1px #9F9F9F;
                background-color: #dddddd;
            }
            td.empty{
                padding-bottom: 20px;
            }
            .split{
                border-left: 1px solid #9F9F9F;
            }
            thead.header{
                border-bottom: 1px solid #9F9F9F;
            }

            #hor-minimalist-a {
                background: #fff;
                margin: 45px;
                border-collapse: collapse;
                text-align: left;
            }

            #hor-minimalist-a th {
                font-size: 10px;
                font-weight: normal;
                color: #039;
                padding: 10px 8px;
                border-bottom: 2px solid #6678b1;
            }

            #hor-minimalist-a td {
                color: #669;
                padding: 9px 8px 0px 8px;
                min-width: 100px;
            }

            #hor-minimalist-a tfoot td {
                padding-top: 15px;
                font-weight: bold;
            }

            #header {
                margin-top: 50px;
                margin-bottom: 15px;
            }

            #header .company {
                font-size: 12px;
                text-transform: uppercase;
                font-weight: bold;
            }

            #header .text_capcalera {
                margin-top: 20px;
            }

            #header .text_vigencia {
                margin-top: 20px;
                font-weight: bold;
            }
            #logo img {
                margin-top: 10px;
                height: 60px;
            }
            #error{
                position: relative;
                width: 100%;
                padding-top: 200px;
                text-align: center;
                font-size: 16pt;
            }

        </style>
        <%block name="custom_css" />

	</head>
    <%
        from datetime import datetime
        from collections import namedtuple
        from addons import get_module_resource
        import pandas as pd
        from babel.numbers import format_currency, format_number,format_decimal

        def clean_text(text):
            return text or ''
        comptador_polisses = 0

        fields_to_sum_activa = [
             'p1_consumo',
             'p2_consumo',
             'p3_consumo',
             'p4_consumo',
             'p5_consumo',
             'p6_consumo',]
        fields_to_sum_fact = [
             'lp_euros',
             'le_euros',
             'excesos_euros',
             'lloguers_euros',
             'altres_euros',
             'amount_untaxed',
             'amount_tax',
             'amount_total']

        fields_to_sum_reactiva = [
             'p1_consumor',
             'p2_consumor',
             'p3_consumor',
             'p4_consumor',
             'p5_consumor',
             'p6_consumor'
             ]

        def get_results():
            sql_file_path = get_module_resource(
                'giscedata_facturacio_distri', 'sql',
                 'resumen_facturacion_distri_por_tarifas.sql'
            )
            query = open(sql_file_path, 'r').read()
            if len(data['form']['states'].split(',')) == 1:
            	query_states = tuple(data['form']['states'].split(','))
            else:
            	query_states = eval(data['form']['states'])
            cursor.execute(query, (data['form']['data_inici'],data['form']['data_final'],
                                   eval(data['form']['facturacio']),
                                   query_states,
                                   tuple(data['form']['multi_serie'])))
            df = pd.DataFrame(cursor.dictfetchall())
            df = df.fillna(0.0)
            return df


    %>
	<%def name="clean(text)">
		${text or ''}
	</%def>

    <% df = get_results() %>
	<body>
        %if not df.empty:
            <div id="header">
                <div style="float:left; width: 25%;">
                    <div id="logo">
                        <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                    </div>
                </div>
                <div style="float:left; width: 45%; margin-top: 20px;">
                    <div class="company">
                        ${company.name}
                        <p>Peajes por tarifas detallado (distribuidora)</p>
                    </div>
                </div>
                <div style="float:right; width: 25%; margin-top: 10px;">
                    <div class="text_capcalera">
                        <span class="text_vigencia">${_(u"Datos")}</span>
                        <table class="hor-minimalist-a">
                            <tbody>
                                <tr>
                                    <td>${_(u"Fecha inicio")}</td>
                                    <td>${data['form']['data_inici']}</td>
                                </tr>
                                <tr>
                                    <td>${_(u"Fecha final")}</td>
                                    <td>${data['form']['data_final']}</td>
                                </tr>
                                % for name, group in df.groupby('nom_serie'):
                                    <tr>
                                        <td colspan="2">${name}</td>
                                    </tr>
                                %endfor
                            </tbody>
                        </table>
                    </div>
                </div>

                <div style="clear:both"></div>
            </div>
            <div class="titol">
                <table>
                    <thead class="header">
                        <tr>
                            <th colspan="3"></th>
                            <th colspan="8" class="split">Consumos</th>
                            <th colspan="8" class="split">Facturación</th>
                        </tr>
                        <tr>
                            <th>Tarifa</th>
                            <th>Facturas</th>
                            <th>Pot. Fact</th>
                            <th class="split">E</th>
                            <th>P1</th>
                            <th>P2</th>
                            <th>P3</th>
                            <th>P4</th>
                            <th>P5</th>
                            <th>P6</th>
                            <th>Total</th>
                            <th class="split">Potencia</th>
                            <th>Energia</th>
                            <th>Exces.</th>
                            <th>Alquileres</th>
                            <th>Otros</th>
                            <th>Base</th>
                            <th>Impuesto</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                % for name, group in df.groupby('tarifa'):
                    <tr class="activa">
                        <td>${name}</td>
                        <td>${format_number(group['number'].count(), locale='es_ES')}</td>
                        <td>${format_decimal(group['lp_kw'].sum(), locale='es_ES')}</td>
                        <td class="split">A</td>
                        % for field in fields_to_sum_activa:
                            <td>${format_decimal(group[field].sum(), locale='es_ES')}</td>
                        % endfor
                        <td>
                            ${format_decimal(
                               sum(((group[fields_to_sum_activa]).sum(axis=1)))
                              , locale='es_ES')}
                        </td>
                        % for index,field in enumerate(fields_to_sum_fact):
                                %if index == 0:
                                   <td class="split">
                                %else:
                                    <td>
                                %endif
                                ${format_currency(
                                group[field].sum(),'EUR', locale='es_ES'
                            )}
                            </td>
                        % endfor
                    </tr>
                    <tr class="reactiva">
                        <td colspan="3"></td>
                        <td class="split">R</td>
                        % for field in fields_to_sum_reactiva:
                            <td>${format_decimal(group[field].sum(), locale='es_ES')}</td>
                        % endfor
                        <td>
                            ${format_decimal(
                               sum(((group[fields_to_sum_reactiva]).sum(axis=1)))
                              , locale='es_ES')}
                        </td>
                        <td class="empty split"></td>
                        <td>${format_currency(group['lr_euros'].sum(), 'EUR', locale='es_ES')}</td>
                        <td colspan="6"></td>
                    </tr>
                % endfor
                    <tfoot>
                        <tr class="activa">
                          <td><strong>TOTAL</strong></td>
                            <td>${format_number(df['number'].count(), locale='es_ES')}</td>
                            <td>${format_decimal(df['lp_kw'].sum(), locale='es_ES')}</td>
                            <td class="split">A</td>
                            % for field in fields_to_sum_activa:
                                <td>${format_decimal(df[field].sum(), locale='es_ES')}</td>
                            % endfor
                            <td>
                                ${format_decimal(
                                   sum(((df[fields_to_sum_activa]).sum(axis=1)))
                                  , locale='es_ES')}
                            </td>
                            % for index, field in enumerate(fields_to_sum_fact):
                                %if index == 0:
                                   <td class="split">
                                %else:
                                    <td>
                                %endif
                                ${format_currency(
                                    df[field].sum(),'EUR', locale='es_ES'
                                )}</td>
                            % endfor
                        </tr>
                        <tr class="reactiva">
                            <td colspan="3"></td>
                            <td class="split">R</td>
                            % for field in fields_to_sum_reactiva:
                               <td>
                                ${format_decimal(df[field].sum(), locale='es_ES')}</td>
                            % endfor
                            <td>
                                ${format_decimal(
                                   sum(((df[fields_to_sum_reactiva]).sum(axis=1)))
                                  , locale='es_ES')}
                            </td>
                            <td class="empty split"></td>
                            <td>${format_currency(df['lr_euros'].sum(), 'EUR', locale='es_ES')}</td>
                            <td colspan="6"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        %else:
            <div id="error">
                No se han encontrado facturas.
            </div>
        %endif
	</body>
</html>
