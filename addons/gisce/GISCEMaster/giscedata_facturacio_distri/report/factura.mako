<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    import json
%>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    <style type="text/css">
        ${css}
    </style>
    <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_distri/report/stylesheet_factura.css"/>
    <title>Factura</title>
</head>
<body>
    <%
        pool = objects[0].pool
        polissa_obj = objects[0].pool.get('giscedata.polissa')
        comptador_factures = 0;
        def get_mes(month):
          months = {
            '1': 'ENERO','2': 'FEBRERO','3': 'MARZO',
            '4': 'ABRIL','5': 'MAYO','6': 'JUNIO',
            '7': 'JULIO','8': 'AGOSTO','9': 'SEPTIEMBRE',
            '10': 'OCTUBRE','11': 'NOVIEMBRE','12': 'DICIEMBRE',
          }
          return months[str(month)]

        def pretty_date(data, type='normal'):
            any, mes, dia = data.split('-')
            if type == 'normal':
                return '%s/%s/%s' %(dia, mes, any)
            elif type == 'text':
                return '%s de %s de %s' %(dia, get_mes(int(mes)).lower(), any)

        def get_discriminador(factura_id):
            sql = """SELECT count(*) as total FROM giscedata_facturacio_factura_linia
            WHERE tipus='energia' and factura_id = %s
            """ % factura_id
            cursor.execute(sql)
            return cursor.dictfetchall()[0]['total']

        def get_historics(polissa_id, data_inici, data_final):
            sql = """SELECT * FROM (
                        SELECT mes AS mes,
                        periode AS periode,
                        sum(suma_fact) AS facturat,
                        sum(suma_consum) AS consum,
                        min(data_ini) AS data_ini,
                        max(data_fin) AS data_fin
                        FROM (
                        SELECT f.polissa_id AS polissa_id,
                               to_char(f.data_inici, 'YYYY/MM') AS mes,
                               pt.name AS periode,
                               COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
                               COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
                               MIN(f.data_inici) data_ini,
                               MAX(f.data_final) data_fin
                               FROM
                                    giscedata_facturacio_factura f
                                    LEFT JOIN account_invoice i on f.invoice_id = i.id
                                    LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
                                    LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
                                    LEFT JOIN product_product pp on il.product_id=pp.id
                                    LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
                               WHERE
                                    fl.tipus = 'energia' AND
                                    f.polissa_id = %(p_id)s AND
                                    f.data_inici <= %(data_inicial)s AND
                                    f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
                                    AND i.type in('out_invoice', 'out_refund')
                               GROUP BY
                                    f.polissa_id, pt.name, f.data_inici
                               ORDER BY f.data_inici DESC ) AS consums
                        GROUP BY polissa_id, periode, mes
                        ORDER BY mes DESC, periode ASC
                        ) consums_ordenats
                        ORDER BY mes ASC
                """
            cursor.execute(
                sql,
                {'p_id':polissa_id,
                 'data_inicial': data_inici,
                 'data_final': data_final,
                 })
            return cursor.dictfetchall()


        def get_linies_tipus(factura_id, tipus):
            sql = """SELECT
                     giscedata_facturacio_factura_linia."price_unit_multi" AS giscedata_facturacio_factura_linia_price_unit_multi,
                     giscedata_facturacio_factura_linia."factura_id" AS giscedata_facturacio_factura_linia_factura_id,
                     giscedata_facturacio_factura_linia.tipus AS giscedata_facturacio_factura_linia_tipus,
                     giscedata_facturacio_factura_linia.multi AS giscedata_facturacio_factura_linia_multi,
                     giscedata_facturacio_factura_linia."uom_multi_id" AS giscedata_facturacio_factura_linia_uom_multi_id,
                     account_invoice_line."name" AS account_invoice_line_name,
                     account_invoice_line."discount" AS account_invoice_line_discount,
                     account_invoice_line."quantity" AS account_invoice_line_quantity,
                     coalesce(product_uom."name",'') AS product_uom_name,
                     account_invoice_line."price_subtotal" AS account_invoice_line_price_subtotal
                FROM
                     "public"."account_invoice_line" account_invoice_line INNER JOIN "public"."giscedata_facturacio_factura_linia" giscedata_facturacio_factura_linia ON account_invoice_line."id" = giscedata_facturacio_factura_linia."invoice_line_id"
                     LEFT JOIN "public"."product_uom" product_uom ON account_invoice_line."uos_id" = product_uom."id"
                WHERE giscedata_facturacio_factura_linia.factura_id  = %s
                      and giscedata_facturacio_factura_linia.tipus in (%s)
                ORDER BY giscedata_facturacio_factura_linia.tipus,account_invoice_line.name

                """ % (factura_id,"'"+"','".join(tipus)+"'")
            cursor.execute(sql)
            return cursor.dictfetchall()


        def get_num_comptador(id_polissa):
            polissa_obj = pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, uid, id_polissa)
            for comptador in polissa.comptadors:
                if comptador.active:
                   return comptador.name
            return '-'

        def get_lectures_periodes(factura_id):
            sql = """SELECT giscedata_facturacio_lectures_energia.name as periode
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s
            GROUP BY giscedata_facturacio_lectures_energia.name
            ORDER BY giscedata_facturacio_lectures_energia.name""" % factura_id
            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_lectures(factura_id):
            sql = """SELECT
                 giscedata_facturacio_lectures_energia.factura_id AS giscedata_facturacio_lectures_energia_factura_id,
                 giscedata_facturacio_lectures_energia.name AS giscedata_facturacio_lectures_energia_name,
                 giscedata_facturacio_lectures_energia.lect_actual AS giscedata_facturacio_lectures_energia_lect_actual,
                 giscedata_facturacio_lectures_energia.consum AS giscedata_facturacio_lectures_energia_consum,
                 giscedata_facturacio_lectures_energia.tipus AS giscedata_facturacio_lectures_energia_tipus,
                 giscedata_facturacio_lectures_energia.lect_anterior AS giscedata_facturacio_lectures_energia_lect_anterior
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s
            ORDER BY giscedata_facturacio_lectures_energia.name,giscedata_facturacio_lectures_energia.tipus""" % factura_id

            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_lectura_periode_tipus(factura_id, tipus, periode):
            sql = """SELECT
                 giscedata_facturacio_lectures_energia.factura_id AS giscedata_facturacio_lectures_energia_factura_id,
                 giscedata_facturacio_lectures_energia.name AS giscedata_facturacio_lectures_energia_name,
                 giscedata_facturacio_lectures_energia.lect_actual AS lect_actual,
                 giscedata_facturacio_lectures_energia.consum AS giscedata_facturacio_lectures_energia_consum,
                 giscedata_facturacio_lectures_energia.tipus AS giscedata_facturacio_lectures_energia_tipus,
                 giscedata_facturacio_lectures_energia.lect_anterior AS lect_anterior
            FROM
                 giscedata_facturacio_lectures_energia giscedata_facturacio_lectures_energia
            WHERE giscedata_facturacio_lectures_energia.factura_id = %s and giscedata_facturacio_lectures_energia.name = '%s'
                  and giscedata_facturacio_lectures_energia.tipus = '%s'
            ORDER BY giscedata_facturacio_lectures_energia.name,giscedata_facturacio_lectures_energia.tipus""" % (
            factura_id, periode, tipus)
            cursor.execute(sql)
            return cursor.dictfetchall()

        def get_emergency_num(partner_id):
            sql = """SELECT phone
                FROM res_partner_address
                WHERE res_partner_address.id =
                    (SELECT id
                     FROM res_partner_address
                     WHERE partner_id = %s and type='contact'
                     ORDER BY id asc limit 1)"""
            cursor.execute(sql, (partner_id,))
            phones = cursor.dictfetchall()
            return phones and phones[0]['phone'] or ''

        def get_giscedata_fact(invoice):
            sql = """SELECT * FROM giscedata_facturacio_factura WHERE id=%s""" % invoice.id
            cursor.execute(sql)
            return cursor.dictfetchall()

        def clean_vat(vat):
            if vat:
                return vat.replace('ES', '')
            else:
                return ''
     %>
    <%
        from datetime import datetime
        def days_between(d1, d2):
            try:
                d1 = datetime.strptime(d1, "%d-%m-%Y")
                d2 = datetime.strptime(d2, "%Y-%m-%d")
                return abs((d2 - d1).days)
            except:
                return 0
    %>
    %for inv in objects :
        <%
            def get_origen_lectura(cursor, uid, lectura):
                """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
                res = {lectura.data_actual: '',
                       lectura.data_anterior: ''}

                lectura_obj = lectura.pool.get('giscedata.lectures.lectura')
                tarifa_obj = lectura.pool.get('giscedata.polissa.tarifa')
                origen_obj = lectura.pool.get('giscedata.lectures.origen')

                # Recuperar l'estimada
                estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

                #Busquem la tarifa
                if lectura and lectura.name:
                    tarifa_id = tarifa_obj.search(cursor, uid, [('name', '=',
                                                            lectura.name[:-5])])
                else:
                    tarifa_id = False
                if tarifa_id:
                    tipus = lectura.tipus == 'activa' and 'A' or 'R'

                    search_vals = [('comptador', '=', lectura.comptador_id.id),
                                   ('periode.name', '=', lectura.name[-3:-1]),
                                   ('periode.tarifa', '=', tarifa_id[0]),
                                   ('tipus', '=', tipus),
                                   ('name', 'in', [lectura.data_actual,
                                                   lectura.data_anterior])]
                    lect_ids = lectura_obj.search(cursor, uid, search_vals)
                    lect_vals = lectura_obj.read(cursor, uid, lect_ids,
                                                 ['name', 'origen_comer_id', 'origen_id'])
                    for lect in lect_vals:
                        # En funció dels origens, escrivim el text
                        # Si Estimada (40)
                        # La resta: Real
                        origen_txt = _(u"real")
                        if lect['origen_id'][0] == estimada_id:
                            origen_txt = _(u"estimada")

                        res[lect['name']] = "%s" % (origen_txt)

                return res
            lectures_a = {}
            lectures_r = {}
            lectures_m = []

            for lectura in inv.lectures_energia_ids:
                origens = get_origen_lectura(cursor, uid, lectura)
                if lectura.tipus == 'activa':
                    lectures_a.setdefault(lectura.comptador_id.name,[])
                    lectures_a[lectura.comptador_id.name].append((lectura.name[-3:-1],
                                                        lectura.lect_anterior,
                                                        lectura.lect_actual,
                                                        lectura.consum,
                                                        lectura.data_anterior,
                                                        lectura.data_actual,
                                                        origens[lectura.data_anterior],
                                                        origens[lectura.data_actual],
                                                        ))
                elif lectura.tipus == 'reactiva':
                    lectures_r.setdefault(lectura.comptador_id.name,[])
                    lectures_r[lectura.comptador_id.name].append((lectura.name[-3:-1],
                                                        lectura.lect_anterior,
                                                        lectura.lect_actual,
                                                        lectura.consum,
                                                        lectura.data_anterior,
                                                        lectura.data_actual,
                                                        origens[lectura.data_anterior],
                                                        origens[lectura.data_actual],
                                                        ))

            for lectura in inv.lectures_potencia_ids:
                lectures_m.append((lectura.name, lectura.pot_contract,
                                   lectura.pot_maximetre, lectura.exces ))

            altres_lines = [l for l in inv.linia_ids if l.tipus in 'altres']
            total_altres = sum([l.price_subtotal for l in altres_lines])
            lines_lloguer = [l for l in inv.linia_ids if l.tipus in 'lloguer']
            total_lloguer = sum([l.price_subtotal for l in altres_lines])
        %>
    <div class="background">
    </div>
    <div class="wrapper">
        <div class="page_princial" >

            <%
                factura = get_giscedata_fact(inv)[0]

                dies_factura = 1 + (datetime.strptime(inv.data_final, '%Y-%m-%d') - datetime.strptime(inv.data_inici, '%Y-%m-%d')).days
                diari_factura_actual_eur = inv.total_energia / (dies_factura or 1.0)
                diari_factura_actual_kwh = (inv.energia_kwh * 1.0) / (dies_factura or 1.0)

                data_inici  = datetime.strptime(factura['data_inici'],'%Y-%m-%d')
                data_fi = datetime.strptime(factura['data_final'],'%Y-%m-%d')
                delta = data_fi - data_inici
                dies = delta.days
                periodes_a = sorted(list(set([lectura.name[-3:-1]
                                for lectura in inv.lectures_energia_ids
                                if lectura.tipus == 'activa'])))
                polissa = polissa_obj.browse(cursor, uid, inv.polissa_id.id,
                                 context={'date': inv.data_final.val})
            %>

            <% direccio = '%s %s %s %s %s' %(inv.polissa_id.cups.nv or '',
                                             inv.polissa_id.cups.pnp or '',
                                             inv.polissa_id.cups.es or '',
                                             inv.polissa_id.cups.pt or '',
                                             inv.polissa_id.cups.pu or ''
                                             )
            %>

            <% setLang(inv.partner_id.lang) %>
            <!-- capcalera -->
            <div class="capcalera">
                <div class="banner">
                    <div class="logo_container float_l">
                        <table width="100%" height="100%" align="center" valign="center">
                            <tr><td>
                                <img class="logo" src="data:image/jpeg;base64,${company.logo}"/>
                            </td></tr>
                        </table>
                    </div>
                    <div class="title">
                        <%
                            partner_title = company.partner_id.title
                            partner_name = company.partner_id.name
                            if partner_title:
                                partner_title = partner_title.upper()
                            endif
                            if partner_name:
                                partner_name = partner_name.upper()
                            endif
                        %>
                        <h1>${partner_title or ''|entity} ${partner_name |entity}</h1>
                    </div>
                </div>
                <div class="direccions">
                    <%
                        phone = company.partner_id.address[0].phone
                        fax = company.partner_id.address[0].fax
                        email = company.partner_id.address[0].email
                        company_address = company.partner_id

                        emergency_phone = get_emergency_num(company_address.id)
                        if emergency_phone:
                            emergency_phone = "- Incidencias Seguridad: " + emergency_phone
                        else:
                            emergency_phone = ""

                        if phone:
                            phone = "- Tel: " + phone
                        else:
                            phone = ''
                        endif

                        if fax:
                            fax = "- Fax: " + fax
                        else:
                            fax = ''
                        endif

                        if email:
                            email = " Correo: " + email
                        else:
                            email = ''
                        endif
                    %>
                    ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''}
                    ${company_address.address[0].zip or ''} ${company_address.city or ''}
                    ${phone} ${fax} <div style="display: block"> ${emergency_phone} </div> <div style="display: block"> ${email} </div>
                </div>
                <!--<div class="clear fi_bloc"></div>-->
            </div><!-- Fi capcalera -->
            <!-- part central -->
            <div id="outer">
                <p id="vertical_text">
                    ${company.rml_footer2}
                </p>
            </div>
            <div class="row">
                <div class="column left">
                    <div class="box">
                        <%
                            date_periodo_inicio = datetime.strptime(factura['data_inici'], '%Y-%m-%d')
                            date_periodo_fin = datetime.strptime(factura['data_final'], '%Y-%m-%d')
                            date_emision = datetime.strptime(inv.date_invoice, '%Y-%m-%d')
                            date_due = datetime.strptime(inv.date_due, '%Y-%m-%d')
                        %>

                        <table class="dades">
                            <tr class="header">
                                <td>${_(u"Fecha emisión:")}</td>
                                <td>${"{}/{}/{}".format(date_emision.day,date_emision.month,date_emision.year)}</td>
                            </tr>
                            <tr class="header">
                                <td>${_(u"Fecha vencimiento:")}</td>
                                <td>${"{}/{}/{}".format(date_due.day,date_due.month,date_due.year)}</td>
                            </tr>
                            <tr>
                                <td>${_(u"Periodo:")}</td>
                                <td>Del ${"{}/{}/{}".format(date_periodo_inicio.day,date_periodo_inicio.month,date_periodo_inicio.year)} al ${"{}/{}/{}".format(date_periodo_fin.day,date_periodo_fin.month,date_periodo_fin.year)}</td>
                            </tr>
                            <tr>
                                <td>${_(u"Nº Factura:")}</td>
                                <td>${inv.number or ''|entity}</td>
                            </tr>
                            <tr>
                                <td>${_(u"NIF/CIF:")}</td>
                                <td>${clean_vat(inv.partner_id.vat)}</td>
                            </tr>
                            <tr>
                                <td>${_(u"C.N.A.E.:")}</td>
                                <td>${polissa.cnae.name}</td>
                            </tr>
                            <tr class="response">
                                <td><b>${_(u"TOTAL:")}</b></td>
                                <td><b>${formatLang(inv.amount_total, digits=2)} €</b></td>
                            </tr>
                            %if inv.tipo_rectificadora in ['B', 'A', 'BRA']:
                                <tr class="response"><td colspan="4">${_(u"Esta factura anula la factura")} ${inv.ref.number or ''}</td></tr>
                            %endif

                            %if inv.tipo_rectificadora in ['R', 'RA']:
                                <tr class="response"><td colspan="4">${_(u"Esta factura rectifica la factura")} ${inv.ref.number or ''}</td></tr>
                            %endif
                        </table>
                    </div>
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Datos del punto de suministro")}
                            <hr/>
                        </div>
                        <table class="dades">
                            <tr>
                                <td><b>Titular:</b></td>
                            </tr>
                            <tr>
                                <td>
                                    ${inv.polissa_id.titular.name.upper() |entity}
                                </td>
                            </tr>
                            <tr>
                                <td><b>Dirección:</b></td>
                            </tr>
                            <tr>
                                <td>
                                    ${inv.polissa_id.cups.nv or ''}
                                    ${inv.polissa_id.cups.pnp or ''}
                                    ${inv.polissa_id.cups.es or ''}
                                    ${inv.polissa_id.cups.pt or ''}
                                    ${inv.polissa_id.cups.pu or ''}
                                </td>
                            </tr>
                            <tr>
                                <td><b>Población:</b></td>
                            </tr>
                            <tr>
                                <td>
                                    ${inv.polissa_id.cups.id_municipi.name or ''}, ${inv.polissa_id.cups.id_poblacio.name or ''}
                                </td>
                            </tr>
                            <tr>
                                <td><b>CUPS:</b></td>
                            </tr>
                            <tr>
                                <td>
                                    ${inv.polissa_id.cups.name or '-'}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Datos de contratación")}
                            <hr/>
                        </div>

                        <table class="dades">
                            <tr class="header">
                                <td><b>${_(u"Tarifa:")}</b></td>
                                <td>${polissa.tarifa.name or ''|entity}</td>
                            </tr>
                            <tr>
                                <td><b>${_(u"BOE:")}</b></td>
                                <td>
                                    ${inv.date_boe or ''|entity}
                                </td>
                            </tr>
                            <tr>
                                <td><b>${_(u"Tensión:")}</b></td>
                                <td>
                                    ${polissa.tensio or ''|entity} V
                                </td>
                            </tr>
                            <tr>
                                <td><b>${_(u"Potencia:")}</b></td>
                                <td>
                                    ${formatLang(polissa.potencia) or ''|entity} kW
                                </td>
                            </tr>
                            <tr>
                                <td><b>${_(u"Póliza:")}</b></td>
                                <td>
                                    ${inv.polissa_id.name}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Lecturas")}
                            <hr/>
                        </div>

                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                              <col style="width: 30%"/>
                              <col style="width: 23%"/>
                              <col style="width: 23%"/>
                              <col style="width: 23%"/>
                            </colgroup>
                            <tr>
                                <td class="align_left">
                                    ${_(u"CONTADOR")}
                                </td>
                                <td>
                                    ${_(u"ANTERIOR")}
                                </td>
                                <td>
                                    ${_(u"ACTUAL")}
                                </td>
                                <td>
                                    ${_(u"CONSUMO")}
                                </td>
                            </tr>
                            %for comptador in sorted(lectures_a):
                                <% lect = lectures_a[comptador][0]    %>
                                <tr>
                                    <td class="align_left">
                                        ${comptador or ''|entity}
                                    </td>
                                    <%
                                        date_anterior = datetime.strptime(lect[4], '%Y-%m-%d')
                                        date_actual =  datetime.strptime(lect[5], '%Y-%m-%d')
                                    %>
                                    <td >${"{}/{}/{}".format(date_anterior.day,date_anterior.month,date_anterior.year)}</td>
                                    <td >${"{}/{}/{}".format(date_actual.day,date_actual.month,date_actual.year)}</td>
                                    <td>
                                    </td>
                                </tr>
                            %endfor

                        </table>

                        <br/>
                        <div class="second_box_title">
                            ${_(u"Activa")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                              <col style="width: 30%"/>
                              <col style="width: 23%"/>
                              <col style="width: 23%"/>
                              <col style="width: 23%"/>
                            </colgroup>
                            <tbody>
                                % for comptador, lects in lectures_a.items():
                                    %for lect in lects:
                                    <tr>
                                        <td class="align_left">${polissa.tarifa.name or ''|entity} (${lect[0]})</td>
                                        <td>${formatLang(int(lect[1]), 0)}</td>
                                        <td>${formatLang(int(lect[2]), 0)}</td>
                                        <td class="border-right">${formatLang(int(lect[3]), 0)}</td>
                                    </tr>
                                    %endfor
                                %endfor
                            </tbody>
                        </table>

                        <br/>
                        <div class="second_box_title">
                            ${_(u"Reactiva")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 30%"/>
                                <col style="width: 23%"/>
                                <col style="width: 23%"/>
                                <col style="width: 23%"/>
                            </colgroup>
                            <tbody>
                                % for comptador, lects in lectures_r.items():
                                    %for lect in lects:
                                    <tr>
                                        <td class="align_left">${polissa.tarifa.name or ''|entity} (${lect[0]})</td>
                                        <td>${formatLang(int(lect[1]), digits=0)}</td>
                                        <td>${formatLang(int(lect[2]), 0)}</td>
                                        <td class="border-right">${formatLang(int(lect[3]), 0)}</td>
                                    </tr>
                                    %endfor
                                %endfor
                           </tbody>
                        </table>

                        <br/>
                        <div class="second_box_title">
                            ${_(u"Potencia")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                               <col style="width: 30%"/>
                               <col style="width: 23%"/>
                               <col style="width: 23%"/>
                               <col style="width: 23%"/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td class="align_left">
                                        ${_(u"Periodo")}
                                    </td>
                                    <td>
                                        ${_(u"Potencia")}
                                    </td>
                                    <td>
                                        ${_(u"Max")}
                                    </td>
                                    <td>
                                        ${_(u"Exceso")}
                                    </td>
                                </tr>
                                % for lect in lectures_m:
                                    <tr>
                                        <td class="align_left">${lect[0]}</td>
                                        <td>${formatLang(lect[1], digits=2)}</td>
                                        <td>${formatLang(lect[2], digits=2)}</td>
                                        <td class="border-right">${formatLang(lect[3], digits=2)}</td>
                                    </tr>
                                %endfor
                           </tbody>
                        </table>
                    </div>
                </div>
                <div class="right column">
                    <div class="direccio_comer center">
                       <table class="dades">
                            <tr><td class="align_left"><b>${inv.partner_id.name |entity}</b></td></tr>
                            <tr><td>${inv.address_invoice_id.street or ''|entity}</td></tr>
                            <tr><td>${inv.address_invoice_id.street2 or ''|entity}</td></tr>
                            <tr><td>${inv.address_invoice_id.zip or ''|entity} ${inv.address_invoice_id.city or ''|entity}</td></tr>
                       </table>
                    </div>
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Facturación")}
                            <hr/>
                        </div>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Término de Potencia")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <tbody>
                                % for pot in get_linies_tipus(factura['id'], ['potencia']):

                                    <%
                                        uom = pot['giscedata_facturacio_factura_linia_uom_multi_id']
                                        if uom:
                                            sql = """SELECT name
                                                     FROM product_uom
                                                     WHERE id = '%s'
                                            """ % (uom)
                                            cursor.execute(sql)
                                            valor = cursor.dictfetchall()
                                            valor_nom = valor['name']
                                    %>

                                    <tr>
                                        <td class="align_left">${pot['account_invoice_line_name']}</td>
                                        <td>${formatLang(pot['account_invoice_line_quantity'])} KW</td>
                                        <td>${formatLang(pot['giscedata_facturacio_factura_linia_price_unit_multi'], 6)} ${pot['product_uom_name']} </td>
                                        %if uom:
                                            <td>${formatLang(pot['giscedata_facturacio_factura_linia_multi'], 0)} ${valor_uom}</td>
                                        %endif
                                        <td>${formatLang(pot['account_invoice_line_price_subtotal'], digits=2)}</td>
                                    </tr>
                                % endfor
                           </tbody>
                        </table>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Excesos de potencia (Maximetros)")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <tbody>
                                <% x = 0 %>
                                    %for pot in get_linies_tipus(factura['id'], ['exces_potencia']):
                                        <% x += 1 %>
                                        <tr>
                                            <td class="align_left">${pot['account_invoice_line_name']}</td>
                                            <td colspan="2">${pot['account_invoice_line_quantity']} ${pot['product_uom_name']} x ${formatLang(pot['giscedata_facturacio_factura_linia_multi'])} x ${formatLang(pot['giscedata_facturacio_factura_linia_price_unit_multi'])} €</td>
                                            <td class="total">${pot['account_invoice_line_price_subtotal']}</td>
                                        </tr>
                                    %endfor
                                    % if x == 0:
                                        <tr> <td></td><td></td><td></td><td></td></tr>
                                    % endif
                           </tbody>
                        </table>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Término de Energía")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <tbody>
                                <% x = 0 %>
                                % for ene in get_linies_tipus(factura['id'], ['energia']):
                                    <% x += 1 %>
                                    <tr>
                                       <td class="align_left">${ene['account_invoice_line_name']}</td>
                                       <td>${formatLang(ene['account_invoice_line_quantity'], digits=2)} kWh </td>
                                       <td>${formatLang(ene['giscedata_facturacio_factura_linia_price_unit_multi'], digits=6)} ${ene['product_uom_name']}</td>
                                       <td>
                                           %if ene['account_invoice_line_discount'] != 0:
                                               ${formatLang(ene['account_invoice_line_discount'], digits=2)} % Dto
                                           %endif
                                       </td>
                                       <td class="total">${formatLang(ene['account_invoice_line_price_subtotal'], digits=2)}</td>
                                    </tr>
                                % endfor
                                % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td><td></td> </tr>
                                % endif
                           </tbody>
                        </table>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Término de Reactiva")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <tbody>
                                <% x = 0 %>
                                % for ene in get_linies_tipus(factura['id'], ['reactiva']):
                                    <% x += 1 %>
                                    <tr>
                                       <td class="align_left">${ene['account_invoice_line_name']}</td>
                                       <td>${formatLang(ene['account_invoice_line_quantity'], digits=2)} kVArh </td>
                                       <td>${formatLang(ene['giscedata_facturacio_factura_linia_price_unit_multi'], digits=6)} ${pot['product_uom_name']}  </td>
                                       <td>
                                           %if ene['account_invoice_line_discount'] != 0:
                                               ${formatLang(ene['account_invoice_line_discount'], digits=2)} % Dto
                                           %endif
                                       </td>
                                       <td class="total">${formatLang(ene['account_invoice_line_price_subtotal'], digits=2)}</td>
                                    </tr>
                                % endfor
                                % if x == 0:
                                    <tr> <td></td><td></td><td></td><td></td><td></td> </tr>
                                % endif
                           </tbody>
                        </table>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Otros conceptos")}
                        </div>
                        <hr/>
                        <%
                            i = 0
                            total_otros = 0
                        %>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 85%"/>
                                <col style="width: 15%"/>
                            </colgroup>
                            <tbody>
                                %for i, linia in enumerate(altres_lines):
                                    <tr>
                                        <td class="align_left">${linia.name}</td>
                                        <td>${formatLang(linia.price_subtotal, 2)} €</td>
                                        <% total_otros = total_otros + linia.price_subtotal %>
                                    </tr>
                                %endfor
                           </tbody>
                        </table>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                                <col style="width: 39%"/>
                                <col style="width: 20%"/>
                                <col style="width: 20%"/>
                                <col style="width: 20%"/>
                            </colgroup>
                            <tbody>
                                %for i, linia in enumerate(lines_lloguer):
                                    <tr>
                                        <td class="align_left">${linia.name}</td>
                                        <td>${formatLang(linia.price_unit_multi, digits=2)} €/dia</td>
                                        <td>${formatLang(linia.quantity, digits=0)} dias</td>
                                        <td>${formatLang(linia.price_subtotal, 2)} €</td>
                                        <% total_otros = total_otros + linia.price_subtotal %>
                                    </tr>
                                %endfor
                           </tbody>
                        </table>
                        <br/>
                        <div class="second_box_title">
                            ${_(u"Impuestos")}
                        </div>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                               <col style="width: 15%"/>
                               <col style="width: 68%"/>
                               <col style="width: 13%"/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td>CONCEPTO</td>
                                    <td>BASE</td>
                                    <td>TOTAL</td>
                                </tr>
                                %if inv.tax_line :
                                    %for t in inv.tax_line :
                                        %if 'especial sobre la electricidad'.upper() in t.name.upper():
                                            <tr>
                                                <td>${_(u"IESE")}</td>
                                                <td>${ formatLang(t.base, digits=2)|entity}</td>
                                                <td>${ formatLang(t.amount, digits=2) }</td>
                                            </tr>
                                        %elif 'IVA' in t.name.upper():
                                            <tr>
                                                <td>${ t.name.upper()}</td>
                                                <td>${ formatLang(t.base, digits=2)|entity}</td>
                                                <td>${ formatLang(t.amount, digits=2) }</td>
                                            </tr>
                                        %endif
                                    %endfor
                                %endif
                           </tbody>
                        </table>
                        <hr/>
                        <table class="dades_calcul" style="width: 100%">
                            <colgroup>
                               <col style="width: 70%"/>
                               <col style="width: 30%"/>
                            </colgroup>
                            <tbody>
                                <tr>
                                    <td><b>TOTAL FACTURA</b></td>
                                    <td><b>${formatLang(inv.amount_total, digits=2)} €</b></td>
                                </tr>
                           </tbody>
                        </table>

                    </div>
                    <div class="box">
                        <div class="box_title">
                            ${_(u"Datos de pago")}
                            <hr/>
                        </div>
                        <div class="text_box">
                            ${polissa.tipo_pago.name or ''|entity}
                        </div>
                        <div class="second_box_title">
                            Información adicional
                        </div>
                        <hr/>
                        <div class="text_box">
                            ${inv.comment or ''}
                        </div>
                    </div>
                </div>

                <div class="clear fi_bloc"></div>
            </div> <!-- part central -->
        </div>
        <%
            comptador_factures += 1;
        %>
        % if comptador_factures<len(objects):
            <p style="page-break-after:always"></p>
        % endif
    </div>
    %endfor
</body>
</html>
