# -*- coding: utf-8 -*-

import jasper_reports
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


def informes(cr, uid, ids, data, context):
        
    return {
        'parameters': {'data_inici': data['form']['data_inici'],
                       'data_final': data['form']['data_final'],
                       'serie': data['form']['serie'],
                       'states': data['form']['states'],
                       'facturacio': data['form']['facturacio'], },
    }


jasper_reports.report_jasper(
   'report.report_facturacio_distri_pertarifes_fechas',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_percomercial_fechas',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_cne_fechas',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_municipi_fechas',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_comercial_municipio_fechas',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.resumen_facturacion_distri_impuestos_comer_fecha',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.detalle_facturacion_distri_otros_comer_fechas',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_tipus_punt',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_comer_tipus_punt',
   'wizard.informes.facturacio.distri',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_facturacio_distri_municipi_tipus_punt',
   'wizard.informes.facturacio.distri',
   parser=informes
)


class ReportWebkitFacturas(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(ReportWebkitFacturas, self).__init__(cursor, uid, name,
                                                   context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
        })


webkit_report.WebKitParser(
    'report.report_factura',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_distri/report/factura.mako',
    parser=ReportWebkitFacturas
)

webkit_report.WebKitParser(
    'report.report_factura_conceptos',
    'giscedata.facturacio.factura.conceptos',
    'giscedata_facturacio_distri/report/factura_conceptos.mako',
    parser=ReportWebkitFacturas
)