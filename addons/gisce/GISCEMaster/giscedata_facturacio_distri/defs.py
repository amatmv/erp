MCP = {
    'icp': 1,
    'recarrec': 1,
    'max': 2
}

MAGNITUDS = {
    'activa': 'AE',
    'reactiva': 'R1',
}

MULTI_CONCEPTS = {
    '02': 1000,
    '03': 1000
}
