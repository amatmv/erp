# -*- coding: utf-8 -*-
import time

from osv import osv, fields
from tools.translate import _
from tools import config
from collections import namedtuple
from datetime import datetime
from giscedata_facturacio_distri.lib.energia import Normalizer
from tools import cache
from calendar import monthrange

import netsvc

SIGN = {
    'in_refund': -1,
    'out_refund': -1,
    'in_invoice': 1,
    'out_invoice': 1
}


LIQUIDACIONS_DEFAULT_DICT_FPD = {
    'n_clients': 0,
    'pc_ph1': 0,
    'pc_ph2': 0,
    'pc_ph3': 0,
    'pc_ph4': 0,
    'pc_ph5': 0,
    'pc_ph6': 0,
    'pf_ph1': 0,
    'pf_ph2': 0,
    'pf_ph3': 0,
    'e_ph1': 0,
    'e_ph2': 0,
    'e_ph3': 0,
    'e_ph4': 0,
    'e_ph5': 0,
    'e_ph6': 0,
    'energia_facturada': 0,
    'facturacio_potencia': 0.0,
    'facturacio_energia': 0.0,
    'facturacio_energia_r': 0.0,
    'facturacio_excessos_pot': 0.0,
    'total_facturacio': 0.0,
}


LIQUIDACIONS_DEFAULT_DICT_FPCC = {
    'any_fact': 0,
    'mes_fact': 0,
    'any_cons': 0,
    'mes_cons': 0,
    'periode_tarifari': 0,
    'name': 0,
    'n_clients': 0,
    'pc_ph1': 0,
    'pc_ph2': 0,
    'pc_ph3': 0,
    'pc_ph4': 0,
    'pc_ph5': 0,
    'pc_ph6': 0,
    'pf_ph1': 0,
    'pf_ph2': 0,
    'pf_ph3': 0,
    'e_ph1': 0,
    'e_ph2': 0,
    'e_ph3': 0,
    'e_ph4': 0,
    'e_ph5': 0,
    'e_ph6': 0,
    'energia_facturada': 0,
    'facturacio_potencia': 0.0,
    'facturacio_energia': 0.0,
    'facturacio_energia_r': 0.0,
    'facturacio_excessos_pot': 0.0,
    'total_facturacio': 0.0,
}

LIQUIDACIONS_DEFAULT_DICT_FPFR = {
    'tipo_identificador': '1',
    'identificador': 0,
    'pc_ph1': 0,
    'pc_ph2': 0,
    'pc_ph3': 0,
    'pc_ph4': 0,
    'pc_ph5': 0,
    'pc_ph6': 0,
    'pf_ph1': 0,
    'pf_ph2': 0,
    'pf_ph3': 0,
    'e_ph1': 0,
    'e_ph2': 0,
    'e_ph3': 0,
    'e_ph4': 0,
    'e_ph5': 0,
    'e_ph6': 0,
    'energia_facturada': 0,
    'facturacio_potencia': 0.0,
    'facturacio_energia': 0.0,
    'facturacio_energia_r': 0.0,
    'facturacio_excessos_pot': 0.0,
    'total_facturacio': 0.0,
}

sql_fpcc_fields = [
    'any_facturacio', 'mes_facturacio', 'name', 'codigo_tarifa', 'quantity',
    'price_subtotal', 'tipus', 'date_invoice', 'data_inici', 'data_final',
    'periode', 'periode_liquidacio', 'invoice_type', 'rectificative_type',
    'f_id', 'factura', 'polissa_id', 'tarifa_name'
]


FPCC_Line = namedtuple('FPCC_Line', sql_fpcc_fields)

Contract_Power = namedtuple(
    'Contract_Power', ['data_inici', 'data_final', 'potencia']
)


class WizardGenerarLiquidacions(osv.osv_memory):
    """Generem les liquidacions a través de les factures.
    """
    _name = 'wizard.generar.liquidacions'

    def default_periode_liquidacio_fpd(self, cursor, uid, context=None):
        """Retornem el periode de liquidació per defecte segons la data.
        """
        fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
        date = time.strftime('%Y-%m-%d')
        search_params = [('inici', '<=', date),
                         ('final', '>=', date)]
        periode_ids = fpd_obj.search(cursor, uid, search_params)
        if periode_ids:
            return periode_ids[0]
        else:
            return False

    def default_periode_liquidacio_fpcc(self, cursor, uid, context=None):
        """Retornem el periode de liquidació per defecte segons la data.
        """
        fpcc_obj = self.pool.get('giscedata.liquidacio.fpcc')
        date = time.strftime('%Y-%m-%d')
        search_params = [('inici', '<=', date),
                         ('final', '>=', date)]
        periode_ids = fpcc_obj.search(cursor, uid, search_params)
        if periode_ids:
            return periode_ids[0]
        else:
            return False

    def default_periode_liquidacio_fpfr(self, cursor, uid, context=None):
        """Retornem el periode de liquidació per defecte segons la data.
        """
        fpfr_obj = self.pool.get('giscedata.liquidacio.fpfr')
        date = time.strftime('%Y-%m-%d')
        search_params = [('inici', '<=', date),
                         ('final', '>=', date)]
        periode_ids = fpfr_obj.search(cursor, uid, search_params)
        if periode_ids:
            return periode_ids[0]
        else:
            return False

    def clean(self, cursor, uid, ids, context=None):
        """Eliminem les línies.
        """
        wiz = self.browse(cursor, uid, ids[0], context)
        if wiz.fitxer_liquidacio == 'fpfr':
            periode_liquidacio = self.get_periode_liquidacio_fpfr(cursor, uid, ids, wiz.periode_liquidacio_fpd.id)
        else:
            periode_liquidacio = self.get_wiz_liquidacio_field(wiz)
        for tarifa in periode_liquidacio.tarifa:
            tarifa.unlink()
        wiz.write({'state': 'init'})

    def get_wiz_liquidacio_field(self, wiz):
        return getattr(
            wiz, 'periode_liquidacio_{0}'.format(wiz.fitxer_liquidacio)
        )

    def generar_liquidacions_per_factura_fpd(
            self, cursor, uid, factura, sign, liq, clients, refact, context=None
    ):
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

        tarifa = factura.tarifa_acces_id
        key = tarifa.tarifa_liquidacio.id
        liq.setdefault(key, LIQUIDACIONS_DEFAULT_DICT_FPD.copy())
        clients.setdefault(key, [])

        if factura.polissa_id.id not in clients[key] and \
                factura.tipo_rectificadora in ('N'):
            clients[key].append(factura.polissa_id.id)
            liq[key]['n_clients'] += 1
            if tarifa.name in ('2.0A', '2.1A', '2.0DHA', '2.1DHA'):
                liq[key]['pc_ph1'] += factura.potencia * sign
            else:
                ctx = {'date': factura.data_final}
                polissa = polissa_obj.browse(cursor, uid,
                                             factura.polissa_id.id,
                                             context=ctx)
                for p_cont in polissa.potencies_periode:
                    p_cont.potencia *= sign
                    ph = int(p_cont.periode_id.product_id.name[-1])
                    if ph > 3 and tarifa.name.startswith('3'):
                        continue
                    liq[key]['pc_ph%s' % ph] += p_cont.potencia
        pot_done = []
        for linia in factura.linia_ids:
            linia.quantity *= sign
            linia.price_subtotal *= sign
            if linia.tipus == 'potencia':
                liq[key]['facturacio_potencia'] += linia.price_subtotal
                if tarifa.name.startswith('3'):
                    if linia.product_id:
                        ph = int(linia.product_id.name[-1])
                    else:
                        ph = int(linia.name[-1])
                    # If two lines with same period in invoice
                    # (perhaps the price has changed)
                    # We do not have to sum the quantity for every line
                    if ph > 3 or ph in pot_done:
                        continue
                    liq[key]['pf_ph%s' % ph] += linia.quantity
                    pot_done.append(ph)
            elif linia.tipus == 'energia':
                if linia.product_id:
                    ph = int(linia.product_id.name[-1])
                else:
                    ph = int(linia.name[-1])
                e_ph = 'e_ph%s' % ph
                if e_ph in liq[key]:
                    liq[key][e_ph] += linia.quantity
                liq[key]['energia_facturada'] += linia.quantity
                liq[key]['facturacio_energia'] += linia.price_subtotal
                if tarifa.name in ('2.0A', '2.1A'):
                    liq[key][e_ph] = 0
            elif linia.tipus == 'reactiva':
                liq[key]['facturacio_energia_r'] += linia.price_subtotal
            elif linia.tipus == 'exces_potencia':
                liq[key]['facturacio_excessos_pot'] += linia.price_subtotal
            # Refacturació
            elif linia.tipus == 'altres' and linia.product_id.id in refact:
                liq[key]['facturacio_energia'] += linia.price_subtotal
        liq[key]['total_facturacio'] = (
            liq[key]['facturacio_potencia'] +
            liq[key]['facturacio_energia'] +
            liq[key]['facturacio_excessos_pot'] +
            liq[key]['facturacio_energia_r']
        )

    def generar_liquidacions_per_factura_fpfr(
            self, cursor, uid, factura, sign, liq, context=None
    ):
        if context is None:
            context = {}

        tarifa = factura.tarifa_acces_id

        key = factura.cups_id.name
        liq.setdefault(key, LIQUIDACIONS_DEFAULT_DICT_FPFR.copy())
        liq[key]['tipo_identificador'] = '1'
        liq[key]['name'] = tarifa.tarifa_liquidacio.id

        pot_done = []
        for linia in factura.linia_ids:
            linia.quantity *= sign
            linia.price_subtotal *= sign
            if linia.tipus == 'potencia':
                liq[key]['facturacio_potencia'] += linia.price_subtotal
                if tarifa.name.startswith('3'):
                    if linia.product_id:
                        ph = int(linia.product_id.name[-1])
                    else:
                        ph = int(linia.name[-1])
                    # If two lines with same period in invoice
                    # (perhaps the price has changed)
                    # We do not have to sum the quantity for every line
                    if ph > 3 or ph in pot_done:
                        continue
                    liq[key]['pf_ph%s' % ph] += linia.quantity
                    pot_done.append(ph)
            elif linia.tipus == 'energia':
                if linia.product_id:
                    ph = int(linia.product_id.name[-1])
                else:
                    ph = int(linia.name[-1])
                e_ph = 'e_ph%s' % ph
                if e_ph in liq[key]:
                    liq[key][e_ph] += linia.quantity
                liq[key]['energia_facturada'] += linia.quantity
                liq[key]['facturacio_energia'] += linia.price_subtotal
                if tarifa.name in ('2.0A', '2.1A'):
                    liq[key][e_ph] = 0
            elif linia.tipus == 'reactiva':
                liq[key]['facturacio_energia_r'] += linia.price_subtotal
            elif linia.tipus == 'exces_potencia':
                liq[key]['facturacio_excessos_pot'] += linia.price_subtotal
        liq[key]['total_facturacio'] = (
            liq[key]['facturacio_potencia'] +
            liq[key]['facturacio_energia'] +
            liq[key]['facturacio_excessos_pot'] +
            liq[key]['facturacio_energia_r']
        )

    def generar_liquidacions_per_linia_fpcc(
            self, cursor, uid, linia, context=None
    ):
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

    def generar_liquidacions(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)

        if wiz.fitxer_liquidacio == 'fpd':
            self.generar_liquidacions_fpd(cursor, uid, ids, context=None)
        elif wiz.fitxer_liquidacio == 'fpcc':
            self.generar_liquidacions_fpcc(cursor, uid, ids, context=None)
        else:
            self.generar_liquidacions_fpfr(cursor, uid, ids, context=None)

    @cache(timeout=24 * 3600)
    def get_periode_tarifari(self, cursor, uid, year, month):
        per_tar_obj = self.pool.get('giscedata.liquidacio.periodes_tarifaris')
        search_date = '{0}-{1:02d}-01'.format(year, month)
        per_tar_id = per_tar_obj.search(
            cursor, uid,
            [('inici', '<=', search_date), ('final', '>', search_date)]
        )
        if not per_tar_id:
            raise osv.except_osv(
                _("Error"),
                _(u"No s'ha trobat el periode tarifar per {0}").format(
                    search_date
                )
            )
        return per_tar_id[0]

    def generar_liquidacions_fpd(self, cursor, uid, ids, context=None):
        """Generem les liquidacions FPD
        """
        refact_xml_ids = ['ref_T42011', 'ref_T12012', 'ref_M42012']
        imd = self.pool.get('ir.model.data')
        REFACT = []
        for xml_id in refact_xml_ids:
            md_id = imd._get_id(cursor, uid,
                                'giscedata_facturacio',
                                xml_id)
            REFACT.append(imd.browse(cursor, uid, md_id).res_id)
        wiz = self.browse(cursor, uid, ids[0], context)
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fpdt_obj = self.pool.get('giscedata.liquidacio.fpd.tarifa')
        tarifes_obj = self.pool.get('giscedata.liquidacio.tarifes')

        liq = {}
        clients = {}

        if wiz.periode_liquidacio_fpd.tarifa:
            if wiz.periode_liquidacio_fpd.state == 'borrador':
                wiz.write({'state': 'clean'})
                return False
            else:
                raise osv.except_osv(
                    _("Error"),
                    _(u"Aquesta liquidació té línies entrades i no està en "
                      u"estat esborrany")
                )

        # Liquidació de factures de frau
        num_defraudadors = 0
        frau_vals = LIQUIDACIONS_DEFAULT_DICT_FPD.copy()
        frau_liq = self.get_liquidacions_factures_frau(cursor, uid, ids, wiz.periode_liquidacio_fpd, False,
                                                       context=None)
        for cups, vals in frau_liq.items():
            num_defraudadors += 1
            for key in (keys for keys in vals.keys() if keys in frau_vals.keys()):
                frau_vals[key] += vals[key]

        if num_defraudadors:
            tarifa_id = tarifes_obj.search(cursor, uid, [('codi', '=', 428)])
            frau_vals.update({
                'name': tarifa_id[0],
                'n_clients': num_defraudadors,
                'liquidacio_id': wiz.periode_liquidacio_fpd.id,
            })
            fpdt_obj.create(cursor, uid, frau_vals)

        query_file = ('%s/giscedata_facturacio_distri/sql/query_liquidacions.sql'
                      % config['addons_path'])
        query = open(query_file).read() 
        cursor.execute(query, (wiz.periode_liquidacio_fpd.id, ))
        factures = [x[0] for x in cursor.fetchall()]
        if not factures and not num_defraudadors:
            raise osv.except_osv(_("Error"),
                _(u"No s'ha trobat cap factura pel període de "
                  u"liquidació: %s") % wiz.periode_liquidacio_fpd.name_get()[0][1])
        for factura in fact_obj.browse(cursor, uid, factures):
            sign = SIGN[factura.type]
            self.generar_liquidacions_per_factura_fpd(
                cursor, uid, factura, sign, liq, clients, REFACT, context
            )

        for tarifa, vals in liq.items():
            vals.update({
                'name': tarifa,
                'liquidacio_id': wiz.periode_liquidacio_fpd.id
            })
            #Before writing, round those float values
            #that have to be stored as integers. Otherwise
            #they will be truncated
            fields = fpdt_obj.fields_get(cursor, uid, vals.keys())
            for field in vals.keys():
                if (field in fields and
                    fields[field]['type'] == 'integer' and
                    isinstance(vals[field], float)):
                    vals[field] = int(round(vals[field]))
            fpdt_obj.create(cursor, uid, vals)

        wiz.write({
            'state': 'done',
            'periode_liquidacio_fpd': wiz.periode_liquidacio_fpd.id
        })
        return True

    def get_contract_power_norm(self, cursor, uid, nmline):
        contract_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        contract_data = contract_obj.read(
            cursor, uid, nmline.polissa_id, ['data_alta']
        )
        # get period contracted power
        data_inici = max(nmline.data_inici, contract_data['data_alta'])
        modcon_id = contract_obj._get_modcon_date(
            cursor, uid, nmline.polissa_id, data_inici
        )

        power_dict = modcon_obj.get_potencies_dict(
            cursor, uid, modcon_id
        )
        contract_power = power_dict.get(nmline.periode, 0)

        nmpower = Contract_Power(
            *[nmline.data_inici, nmline.data_final, contract_power]
        )
        power_norm = Normalizer([nmpower], 'potencia')
        return power_norm.normalize_month()

    def get_nclients(self, cursor, uid, year, month, tarifa_code):
        """
        Gets active contracts on last day of month
        :param year: Year
        :param month: Month
        :return:
        """
        last_day = '{0:04d}-{1:02d}-{2:02d}'.format(
            year, month, monthrange(year, month)[1]
        )
        sql = """SELECT COUNT(*) FROM giscedata_polissa_modcontractual mc
        LEFT JOIN giscedata_polissa_tarifa t ON t.id=mc.tarifa
        LEFT JOIN giscedata_liquidacio_tarifes tliq ON tliq.id=t.tarifa_liquidacio
        WHERE  mc.data_inici <= %s AND mc.data_final >= %s
        AND tliq.codi = %s """
        cursor.execute(sql, (last_day, last_day, tarifa_code))
        res = cursor.fetchall()
        return res[0][0]

    def generar_liquidacions_fpcc(self, cursor, uid, ids, context=None):
        """Generem les liquidacions FPCC
        """
        wiz = self.browse(cursor, uid, ids[0], context)

        if wiz.periode_liquidacio_fpcc.tarifa:
            if wiz.periode_liquidacio_fpcc.state == 'borrador':
                wiz.write({'state': 'clean'})
                return False
            else:
                raise osv.except_osv(
                    _("Error"),
                    _(u"Aquesta liquidació té línies entrades i no està en "
                      u"estat esborrany"))

        logger = netsvc.Logger()
        logger.notifyChannel("FPCC", netsvc.LOG_INFO,
                             "Starting CNMC FPCC export")

        fpcct_obj = self.pool.get('giscedata.liquidacio.fpcc.tarifa')
        query_file = (
            '%s/giscedata_facturacio_distri/sql/query_liquidacions_fpcc.sql'
            % config['addons_path']
        )

        query = open(query_file).read()
        cursor.execute(query, (wiz.periode_liquidacio_fpcc.year, ))
        linies = cursor.fetchall()

        query_file = (
                '%s/giscedata_facturacio_distri/sql/query_liquidacions_fpfr_consum.sql'
                % config['addons_path']
        )

        query = open(query_file).read()
        cursor.execute(query, (wiz.periode_liquidacio_fpcc.year,))
        linies_frau = cursor.dictfetchall()

        if not linies and not linies_frau:
            raise osv.except_osv(
                _("Error"),
                _(u"No s'ha trobat cap factura pel període de "
                  u"liquidació: %s")
                % wiz.periode_liquidacio_fpcc.name_get()[0][1]
            )
        n_lines = len(linies)
        logger.notifyChannel("FPCC", netsvc.LOG_INFO,
                             "Exporting {} lines".format(n_lines))
        liq_lines = {}
        n_clients_dict = {}
        line_count = 0
        count_log_interval = n_lines / 100 or 1
        for linia in linies:
            line_count += 1
            nmline = FPCC_Line(*linia)

            print nmline.factura
            if line_count % count_log_interval == 1:
                logger.notifyChannel("FPCC", netsvc.LOG_INFO,
                                     "Exporting {}/{} lines".format(
                                         line_count, n_lines
                                     )
                )

            sign = SIGN[nmline.invoice_type]
            power_lines = self.get_contract_power_norm(cursor, uid, nmline)

            consum_norm = Normalizer([nmline], 'quantity')
            consum_lines = consum_norm.normalize_month()

            consum_fact = Normalizer([nmline], 'price_subtotal')
            fact_lines = consum_fact.normalize_month(decimals=2)

            for line_key, power_line in power_lines.items():
                tariff_period = self.get_periode_tarifari(
                    cursor, uid, power_line[0], power_line[1]
                )
                dict_key = [
                    nmline.any_facturacio,
                    nmline.mes_facturacio,
                    power_line[0],  # year
                    power_line[1],  # month
                    tariff_period,
                    nmline.name,
                ]

                key = '-'.join([str(x) for x in dict_key])

                tarifa_name = nmline.tarifa_name
                n_clients_key = '{0:04d}{1:02d}{2}'.format(
                    nmline.any_facturacio,
                    nmline.mes_facturacio,
                    nmline.codigo_tarifa
                )
                if n_clients_key not in n_clients_dict.keys():
                    n_clients = self.get_nclients(
                        cursor, uid,
                        nmline.any_facturacio,
                        nmline.mes_facturacio,
                        nmline.codigo_tarifa
                    )
                    n_clients_dict[n_clients_key] = n_clients

                default_liqui = LIQUIDACIONS_DEFAULT_DICT_FPCC.copy()
                default_liqui['any_fact'] = dict_key[0]
                default_liqui['mes_fact'] = '{0:02d}'.format(dict_key[1])
                default_liqui['any_cons'] = dict_key[2]
                default_liqui['mes_cons'] = '{0:02d}'.format(dict_key[3])
                default_liqui['periode_tarifari'] = dict_key[4]
                default_liqui['name'] = dict_key[5]
                default_liqui['n_clients'] = n_clients_dict[n_clients_key]

                liq_lines.setdefault(key, default_liqui)

                periode = nmline.periode[-1:]
                power = power_line[2] * sign
                consum = consum_lines[line_key][2] * sign
                fact = fact_lines[line_key][2] * sign

                if tarifa_name.startswith('RE'):
                    periode = '1'
                if nmline.tipus == 'energia':
                    liq_lines[key]['e_ph{0}'.format(periode)] += consum
                    liq_lines[key]['energia_facturada'] += consum
                    liq_lines[key]['facturacio_energia'] += fact
                elif nmline.tipus == 'potencia':
                    liq_lines[key]['pc_ph{0}'.format(periode)] += power
                    if nmline.tarifa_name.startswith('3'):
                        liq_lines[key]['pf_ph{0}'.format(periode)] += consum
                    liq_lines[key]['facturacio_potencia'] += fact
                elif nmline.tipus == 'reactiva':
                    liq_lines[key]['facturacio_energia_r'] += fact
                elif nmline.tipus == 'exces_potencia':
                    liq_lines[key]['facturacio_excessos_pot'] += fact
                else:
                    # lifejacket
                    continue
                liq_lines[key]['total_facturacio'] += fact

        # Liquidació de frau
        tarifes_obj = self.pool.get('giscedata.liquidacio.tarifes')
        tarifa_id = tarifes_obj.search(cursor, uid, [('codi', '=', 428)])

        liq_frau = {}
        for linia_frau in linies_frau:
            any_facturacio = linia_frau['any_facturacio']
            mes_facturacio = linia_frau['mes_facturacio']
            mes_consum = int(linia_frau['data_final'].split('-')[1])
            any_consum = int(linia_frau['data_final'].split('-')[0])
            tariff_period = self.get_periode_tarifari(cursor, uid, any_facturacio, mes_facturacio)

            key = '{}-{}-{}-{}-{}-{}'.format(any_facturacio, mes_facturacio,
                                             any_consum, mes_consum,
                                             tariff_period, tarifa_id[0])
            
            if not liq_frau.get(key, False):
                default_frau_liq = LIQUIDACIONS_DEFAULT_DICT_FPCC.copy()
                default_frau_liq.update({
                    'any_fact': any_facturacio,
                    'mes_fact': '{0:02d}'.format(mes_facturacio),
                    'any_cons': any_consum,
                    'mes_cons': '{0:02d}'.format(mes_consum),
                    'periode_tarifari': tariff_period,
                    'name': tarifa_id[0],
                    'n_clients': 1,
                    'cups_list': [linia_frau['cups']],
                })
                liq_frau.setdefault(key, default_frau_liq)
            elif linia_frau['cups'] not in liq_frau[key]['cups_list']:
                liq_frau[key]['n_clients'] += 1
                liq_frau[key]['cups_list'].append(linia_frau['cups'])

            consum = liq_frau[key]['e_ph1'] + linia_frau['quantity']
            facturacio = liq_frau[key]['facturacio_energia'] + linia_frau['price_subtotal']

            liq_frau[key].update({
                'e_ph1': consum,
                'energia_facturada': consum,
                'facturacio_energia': facturacio,
                'total_facturacio': facturacio,
            })

        liq_lines.update(liq_frau)

        for key, vals in liq_lines.items():
            vals.update({
                'liquidacio_id': wiz.periode_liquidacio_fpcc.id
            })
            #Before writing, round those float values
            #that have to be stored as integers. Otherwise
            #they will be truncated
            fields = fpcct_obj.fields_get(cursor, uid, vals.keys())
            for field in vals.keys():
                if (field in fields and
                    fields[field]['type'] == 'integer' and
                    isinstance(vals[field], float)):
                    vals[field] = int(round(vals[field]))
            fpcct_obj.create(cursor, uid, vals)
        wiz.write({
            'state': 'done',
            'periode_liquidacio_fpcc': wiz.periode_liquidacio_fpcc.id
        })
        logger.notifyChannel("FPCC", netsvc.LOG_INFO,
                             "End off CNMC FPCC export")
        return True

    def generar_liquidacions_fpfr(self, cursor, uid, ids, context=None):
        """Generem les liquidacions FPFR
        """
        wiz = self.browse(cursor, uid, ids[0], context)
        fpfrt_obj = self.pool.get('giscedata.liquidacio.fpfr.tarifa')

        periode_fpfr = self.get_periode_liquidacio_fpfr(cursor, uid, ids, wiz.periode_liquidacio_fpd.id)

        if periode_fpfr.tarifa:
            if periode_fpfr.state == 'borrador':
                wiz.write({'state': 'clean'})
                return False
            else:
                raise osv.except_osv(
                    _("Error"),
                    _(u"Aquesta liquidació té línies entrades i no està en "
                      u"estat esborrany")
                )

        liq = self.get_liquidacions_factures_frau(cursor, uid, ids, wiz.periode_liquidacio_fpd, True)
        for cups, vals in liq.items():
            vals.update({
                'identificador': cups,
                'liquidacio_id': periode_fpfr.id
            })

            # Before writing, round those float values
            # that have to be stored as integers. Otherwise
            # they will be truncated
            fields = fpfrt_obj.fields_get(cursor, uid, vals.keys())
            for field in vals.keys():
                if field in fields \
                        and fields[field]['type'] == 'integer' \
                        and isinstance(vals[field], float):
                    vals[field] = int(round(vals[field]))
            fpfrt_obj.create(cursor, uid, vals)
        wiz.write({
            'state': 'done',
            'periode_liquidacio_fpd': wiz.periode_liquidacio_fpd.id
        })
        return True

    def get_periode_liquidacio_fpfr(self, cursor, uid, ids, periode_liquidacio_fpd_id, context=None):
        fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
        fpfr_obj = self.pool.get('giscedata.liquidacio.fpfr')

        periode_fpd_vals = fpd_obj.read(cursor, uid, periode_liquidacio_fpd_id,
                                        ['month', 'year', 'inici', 'final'])
        del periode_fpd_vals['id']

        res = []
        for key, value in periode_fpd_vals.items():
            res.append((key, '=', value))

        periode_fpfr_id = fpfr_obj.search(cursor, uid, res)
        if not periode_fpfr_id:
            raise osv.except_osv(
                _("Error de configuració"),
                _(u"No s'ha trobat cap periode de liquidació de Factures de frau que coincideixi amb "
                  u"el periode de liquidació CNE sel.leccionat. S'ha de crear un de nou abans de "
                  u"poder continuar.")
            )
        return fpfr_obj.browse(cursor, uid, periode_fpfr_id[0])

    def get_liquidacions_factures_frau(self, cursor, uid, ids, periode_liquidacio_fpd, wizard, context=None):

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        query_file = ('%s/giscedata_facturacio_distri/sql/query_liquidacions_fpfr.sql'
                      % config['addons_path'])
        query = open(query_file).read()
        cursor.execute(query, (periode_liquidacio_fpd.id,))
        factures = [x[0] for x in cursor.fetchall()]
        if not factures and wizard:
            raise osv.except_osv(_("Error"),
                                 _(u"No s'ha trobat cap factura pel període de "
                                   u"liquidació: %s") % periode_liquidacio_fpd.name_get()[0][1])
        liq = {}
        for factura in fact_obj.browse(cursor, uid, factures):
            sign = SIGN[factura.type]
            self.generar_liquidacions_per_factura_fpfr(
                cursor, uid, factura, sign, liq, context
            )
        return liq

    def mostrar_liquidacio(self, cursor, uid, ids, context=None):
        """Mostrem la liquidació generada.
        """
        wiz = self.browse(cursor, uid, ids[0])
        if wiz.fitxer_liquidacio == 'fpfr':
            periode_liquidacio = self.get_periode_liquidacio_fpfr(cursor, uid, ids, wiz.periode_liquidacio_fpd.id)
        else:
            periode_liquidacio = self.get_wiz_liquidacio_field(wiz)

        liqui_model = 'giscedata.liquidacio.{0}'.format(wiz.fitxer_liquidacio)
        return {
            'domain': "[('id', '=', %s)]" % periode_liquidacio.id,
            'name': _('Liquidació generada'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': liqui_model,
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'fitxer_liquidacio': fields.selection(
            [('fpd', 'Peajes Desglosado'),
             ('fpcc', 'Peajes Desglosado por Consumos'),
             ('fpfr', 'Factures de frau')], 'Fitxer a generar',
            required=True
        ),
        'periode_liquidacio_fpd': fields.many2one(
            'giscedata.liquidacio.fpd', 'Periode liquidació CNE',
        ),
        'periode_liquidacio_fpcc': fields.many2one(
            'giscedata.liquidacio.fpcc', 'Periode liquidació CNMC',
        ),
        'state': fields.char('Pas', size=50),
    }

    _defaults = {
        'fitxer_liquidacio': lambda *a: 'fpd',
        'periode_liquidacio_fpd': default_periode_liquidacio_fpd,
        'periode_liquidacio_fpcc': default_periode_liquidacio_fpcc,
        'state': lambda *a: 'init',
    }


WizardGenerarLiquidacions()
