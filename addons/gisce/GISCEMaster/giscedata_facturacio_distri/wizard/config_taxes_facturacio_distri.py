# -*- coding: utf-8 -*-
from osv import fields,osv
import pooler

TARIFES_XML_IDS = [('giscedata_polissa', 'categ_t20A_new'),
                   ('giscedata_polissa', 'categ_t20DHA_new'),
                   ('giscedata_polissa', 'categ_t20DHS'),
                   ('giscedata_polissa', 'categ_t20A'),
                   ('giscedata_polissa', 'categ_t20DHA'),
                   ('giscedata_polissa', 'categ_t21DHS'),
                   ('giscedata_polissa', 'categ_t30A'),
                   ('giscedata_polissa', 'categ_t31A'),
                   ('giscedata_polissa', 'categ_t31A_LB'),
                   ('giscedata_polissa', 'categ_t61'),
                   ('giscedata_polissa', 'categ_t61a'),
                   ('giscedata_polissa', 'categ_t61b'),
                   ('giscedata_polissa', 'categ_t62'),
                   ('giscedata_polissa', 'categ_t63'),
                   ('giscedata_polissa', 'categ_t64'),
                   ('giscedata_polissa', 'categ_t65'),
                   ('giscedata_lectures', 'categ_alq_conta')]

class config_taxes_facturacio_distri(osv.osv_memory):
    _name = 'config.taxes.facturacio.distri'
    
    # Definim les columnes, ho farem bastant 'cutre', només es podran escollir 3 impostos:
    # IVA venda, IESE i IVA Compra. Després nosaltres ja sabrem a quins productes s'han 
    # d'aplicar aquests impostos 
    _columns = {
        'iva_venda': fields.many2one('account.tax', 'IVA (venta)', domain=[('type_tax_use', '=', 'sale')]),
        'iva_compra': fields.many2one('account.tax', 'IVA (compra)', domain=[('type_tax_use', '=', 'purchase')])
    }
    
    def action_set(self, cr, uid, ids, context=None):
        
        config = self.browse(cr, uid, ids[0], context)
        # Per cada periode de les tarifes hi apliquem aquests impostos
        product_obj = self.pool.get('product.product')
        categ_ids = []
        for xml_module, xml_id in TARIFES_XML_IDS:
            md_obj = pooler.get_pool(cr.dbname).get('ir.model.data')
            md_id = md_obj.search(cr, uid, [('module', '=', xml_module),
                                            ('name', '=', xml_id)])
            categ_ids.append(md_obj.browse(cr, uid, md_id[0]).res_id)
        vals = {
            'taxes_id': [(6, 0, [config.iva_venda.id])],
            'supplier_taxes_id': [(6, 0, [config.iva_compra.id])]
        }
        search_params = [('categ_id', 'child_of', categ_ids)]
        productes_ids = product_obj.search(cr, uid, search_params)
        product_obj.write(cr, uid, productes_ids, vals)
                
        return {
                'view_type': 'form',
                "view_mode": 'form',
                'res_model': 'ir.actions.configuration.wizard',
                'type': 'ir.actions.act_window',
                'target':'new',
         }
        
config_taxes_facturacio_distri()
