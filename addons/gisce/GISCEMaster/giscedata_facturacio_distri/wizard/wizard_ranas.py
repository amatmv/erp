# -*- coding: utf-8 -*-
from osv import osv, fields
import time

class WizardRanas(osv.osv_memory):
    """Customització de distribució anul·ladores/rectificadores.
    """
    _name = 'wizard.ranas'
    _inherit = 'wizard.ranas'

    def default_periode_liquidacio(self, cursor, uid, context=None):
        """Retornem el periode de liquidació per defecte segons la data.
        """
        fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
        date = time.strftime('%Y-%m-%d')
        search_params = [('inici', '<=', date),
                         ('final', '>=', date)]
        periode_ids = fpd_obj.search(cursor, uid, search_params)
        if periode_ids:
            return periode_ids[0]
        else:
            return False

    def action_end(self, cursor, uid, ids, context=None):
        """Acció per anul·lar les factures
        """
        res = super(WizardRanas, self).action_end(cursor, uid, ids, context)
        wiz = self.browse(cursor, uid, ids[0])
        refund_ids = [a.factura.id for a in wiz.fact_generades]
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_obj.write(cursor, uid, refund_ids,
                          {'periode_liquidacio': wiz.periode_liquidacio.id})
        return res

    _columns = {
        'periode_liquidacio': fields.many2one('giscedata.liquidacio.fpd',
                                              'Periode liquidació CNE',
                                              required=True)
    }

    _defaults = {
        'periode_liquidacio': default_periode_liquidacio,
    }
WizardRanas()
