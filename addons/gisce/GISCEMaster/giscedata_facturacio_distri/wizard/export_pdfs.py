# -*- coding: utf-8 -*-

"""Wizard per generar PDFs de factures
"""

import base64
import time
import zipfile
import re
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from datetime import datetime

from osv import fields, osv
from service.web_services import report_spool
from tools.translate import _
from lxml import etree
import netsvc
from osv.expression import OOQuery

ESTATS = [
    ('init', 'Inicial'),
    ('generant', 'Generant PDFs'),
    ('end_mail', 'Final Email'),
    ('end_pdf', 'Final PDF')
]


def filter_comer_field_names(fields_to_filter):
    # regex_ref: busca codi REE per les comers
    regex_ref = re.compile('^comer[0-9]{4}$')
    # regex_ref: busca VAT pels que no tenen ref(REE)
    regex_vat = re.compile('^comer*.{5,}$')
    for field in fields_to_filter:
        if regex_ref.match(field):
            yield field
        elif regex_vat.match(field):
            yield field


class WizardExportPdfs(osv.osv_memory):
    """Wizard class
    """
    _name = 'wizard.export.pdfs'

    def do_pdf(self, cursor, uid, ids, report_name, context=None):
        """Crida al report
        """
        if context is None:
            context = {}
        state = False
        reporter = report_spool()
        user_obj = self.pool.get('res.users')
        pwd = user_obj.read(cursor, uid, uid, ['password'])
        id_report = reporter.report(cursor.dbname, uid, pwd['password'],
                                    report_name,
                                    ids,
                                    {'model': 'giscedata.facturacio.factura',
                                     'id': ids[0], 'report_type': 'pdf'})
        time.sleep(0.2)
        wait = 0.1
        while not state:
            pdf = reporter.report_get(cursor.dbname, uid, pwd['password'],
                                      id_report)
            state = pdf['state']
            if not state:
                time.sleep(wait)
        return base64.b64decode(pdf['result'])

    def do_pdf_zip(
            self, cursor, uid, ids, comer_facts, zipfile_io,
            fulls, report_name, context=None
    ):
        """
        Generate a ZIP with all invoices in the comer_facts list using the
        report in report_name, merging fulls number of facts in the same report.
        :param cursor:      OpenERP Cursor
        :param uid:         Current User ID
        :type uid:          int
        :param ids:         Wizard IDs - Not used
        :type ids:          list
        :param comer_facts: GiscedataFacturacioFactura IDs to generate PDFs
        :type comer_facts:  dict
        :param zipfile_io:  IOString to create the Zipfile
        :type zipfile_io:   StringIO.StringIO
        :param fulls:       Number of reports in each PDF
        :type fulls:        int
        :param report_name: Report to build
        :type report_name:  str
        :param context:     OpenERP Context
        :type context:      dict
        :return:            Zipfile with all fact. reports in a zipfile
        :rtype:             StringIO.StringIO
        """
        zipfile_ = zipfile.ZipFile(
            zipfile_io, 'w', compression=zipfile.ZIP_DEFLATED
        )
        logger = netsvc.Logger()
        for comer_reference, invoice_ids_ref in comer_facts.items():
            start = 1
            invoice_ids = [i for i in invoice_ids_ref]
            n_invoices = len(invoice_ids)
            while len(invoice_ids) > 0:
                logger.notifyChannel(
                    'export_pdf', netsvc.LOG_INFO,
                    "factures_id: {}-{}/{}".format(
                        start, start + fulls, n_invoices
                    )
                )
                pdf = self.do_pdf(
                    cursor, uid, invoice_ids[:fulls],
                    report_name, context
                )
                fname = "FAC_{}_{}_{}_{}.pdf".format(
                    comer_reference, start,
                    start + min(fulls, n_invoices),
                    datetime.now().strftime('%Y%m%d%H%M%S%f')
                )
                pdfile = StringIO.StringIO(pdf)
                zipfile_.writestr(fname, pdfile.getvalue())
                invoice_ids = invoice_ids[fulls:]
                start += fulls
            logger.notifyChannel(
                'export_pdf', netsvc.LOG_INFO,
                "Exported partner ref: {}".format(comer_reference)
            )
        zipfile_.close()
        return zipfile_io

    def do_email(
            self, cursor, uid, ids, comer,
            factura_ids=False, fulls=False, report=False, context=None
    ):
        """
        Do an email from the factura_ids GiscedataFacturacioFactura's using the
        comer Partner data as recipient
        :param cursor:      OpenERP Cursor
        :param uid:         OpenERP User ID
        :type uid:          int
        :param ids:         OpenERP Wizard ID
        :type ids:          list
        :param comer:       Ref of the Comer to find (Partner.ref)
        :type comer:        str
        :param factura_ids: Dict of GiscedataFacturacioFactura IDs for each REF
        :type factura_ids:  dict
        :param fulls:       Number of Invoices per PDF
        :type fulls:        int
        :param report:      Name of the Report to use when exporting to PDF
        :type report:       str
        :param context:     OpenERP Context
        :type context:      dict
        :return:            PoweremailMailbox ID
        """
        if context is None:
            context = {}

        imd_obj = self.pool.get('ir.model.data')
        pwswz_obj = self.pool.get('poweremail.send.wizard')
        pwetmpl_obj = self.pool.get('poweremail.templates')
        mailbox_obj = self.pool.get('poweremail.mailbox')
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        zipfile_io = StringIO.StringIO()
        wiz = self.browse(cursor, uid, ids, context)[0]
        if not factura_ids:
            factura_ids = wiz.get_factures_id(comers=[comer], context=context)
        if not report:
            report = wiz.report.report_name
        if not fulls:
            fulls = wiz.fulls
        comer_facts = []
        for comer_ref, fact_list in factura_ids.items():
            comer_facts += fact_list

        wiz.do_pdf_zip(
            factura_ids, zipfile_io, fulls, report, context=context
        )

        zipcontent = base64.b64encode(zipfile_io.getvalue())
        dbname = ''.join([c for c in cursor.dbname if c.isalnum()])
        zipname = 'FACTURAS_{dbname}_{comer}.zip'.format(**locals())
        zipfile_io.close()

        template_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_distri',
            'poweremail_enviament_factures_distri'
        )[1]

        template = pwetmpl_obj.browse(
            cursor, uid, template_id, context=context
        )
        if not template.enforce_from_account:
            raise osv.except_osv(
                _(u"Error"),
                _(u"No hay un correo electronico desde el cual enviar las "
                  u"facturas o la plantilla no está correctamente"
                  u"configurada.")
            )
        mail_from = template.enforce_from_account.id
        ctx = {
            'active_ids': comer_facts, 'active_id': comer_facts[0],
            'template_id': template_id, 'src_rec_ids': [comer_facts[0]],
            'src_model': 'giscedata.facturacio.factura', 'from': mail_from,
            'state': 'single', 'priority': '0', 'folder': 'outbox',
            'save_async': False,
        }
        params = {'state': 'single', 'priority': '0', 'from': ctx['from']}
        pwswz_id = pwswz_obj.create(cursor, uid, params, ctx)
        pwemb_id = pwswz_obj.save_to_mailbox(cursor, uid, [pwswz_id], ctx)

        attachment_obj = self.pool.get('ir.attachment')
        attach_id = attachment_obj.create(cursor, uid, {
            'description': zipname,
            'name': zipname,
            'datas_fname': zipname,
            'datas': zipcontent,
            'res_model': 'poweremail.mailbox',
            'res_id': pwemb_id[0]
        })

        mailbox_obj.write(
            cursor, uid, pwemb_id,
            {'pem_attachments_ids': [(6, 0, [attach_id])]}
        )
        params = {
            'envio_pendiente': False,
            'mailbox': pwemb_id[0]
        }

        # If the mail could be created, mark all facts as sent
        fact_obj.write(
            cursor, uid, comer_facts, params, context=context
        )
        return pwemb_id[0]

    def get_comers(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids, context)[0]
        wiz_fields = wiz.read()[0]
        if wiz.what_to_print == 'some':
            include_refs = []
            for field in filter_comer_field_names(wiz_fields.keys()):
                if wiz_fields[field]:
                    comer = field[5:]
                    if comer == '0000':
                        comer = None
                    include_refs.append(comer)
        else:
            include_refs = []
            for field in filter_comer_field_names(wiz_fields.keys()):
                comer = field[5:]
                if comer == '0000':
                    comer = None
                include_refs.append(comer)

        return include_refs

    def get_factures_id(
            self, cursor, uid, wizard_id, comers=False, context=None):
        """
        Get all GiscedataFacturacioFactura IDs using wizard filters
        :param cursor:      OpenERP Cursor
        :param uid:         OpenERP User ID
        :type uid:          int
        :param wizard_id:   OpenERP Wizard ID
        :type wizard_id:    list
        :param comers:      List of Comers to get facts
        :type comers:       list
        :param context:     OpenERP Context
        :type context:      dict
        :return:            Dict containing all factures_id grouped by comer ref
        :rtype:             dict
        """
        if not context:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        model = context.get('model', 'giscedata.facturacio.lot')

        wiz = self.browse(cursor, uid, wizard_id, context)[0]
        if model == 'giscedata.facturacio.factura':
            search = [('id', 'in', context.get('active_ids', []))]
        else:
            search = [('lot_facturacio.id', '=', wiz.lot.id)]
        comer_facts = {}

        if not comers:
            comers = wiz.get_comers()

        for ref in comers:
            if len(ref) > 4:
                ref_or_vat = 'invoice_id.partner_id.vat'
                search_params = search + [
                    '|',
                    ('invoice_id.partner_id.vat', '=', ref),
                    ('invoice_id.partner_id.ref', '=', ref)
                ]
            else:
                search_params = search + [
                    ('invoice_id.partner_id.ref', '=', ref)
                ]

            # ADD Date Filter
            if wiz.date_filter:
                search_params += [
                    ('invoice_id.date_invoice', '<=', wiz.end_date),
                    ('invoice_id.date_invoice', '>=', wiz.start_date)
                ]
            # Get IDS from OOQuery
            q = OOQuery(fact_obj, cursor, uid)
            sql = q.select(['id']).where(search_params)
            cursor.execute(*sql)

            factures_id = [result[0] for result in cursor.fetchall()]
            comer_facts[ref] = factures_id

        return comer_facts

    def action_generar_emails(self, cursor, uid, ids, context=None):
        """Acció que genera els PDFs i envia els mails
        """
        logger = netsvc.Logger()
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids, context)[0]

        model = context.get('model', 'giscedata.facturacio.lot')

        if model != 'giscedata.facturacio.factura':
            wiz.write({'lot': context.get('active_id')})
            wiz = self.browse(cursor, uid, ids, context)[0]

        comers = wiz.get_comers()
        pem_ids = []
        for ref in comers:
            if not ref:
                continue

            pem_ids.append(wiz.do_email(comer=ref, context=context))

            logger.notifyChannel(
                'export_pdf_to_mail', netsvc.LOG_INFO,
                "Created mail for partner ref: {}".format(ref)
            )

        logger.notifyChannel(
            'export_pdf', netsvc.LOG_INFO,
            "Finish PDF and Mail Generation on comers: {}".format(comers)
        )
        wiz.write({
            'state': 'end_mail',
            'pem_ids': pem_ids,
        })

    def action_show_mails(self, cursor, uid, wizard_id, context=None):
        if context is None:
            context = {}
        if isinstance(wizard_id, (tuple, list)):
            wizard_id = wizard_id[0]
        wiz = self.browse(cursor, uid, wizard_id, context=context)
        return {
            'domain': [('id', 'in', wiz.pem_ids)],
            'name': _('Correos Generados'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'poweremail.mailbox',
            'type': 'ir.actions.act_window'
        }

    def action_generar_pdfs(self, cursor, uid, ids, context=None):
        """Acció que genera els PDFs
        """
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids, context)[0]

        model = context.get('model', 'giscedata.facturacio.lot')

        if model == 'giscedata.facturacio.factura':
            extra_name = datetime.now().strftime('%Y%m%d%H%M%S')
        else:
            wiz.write({'lot': context.get('active_id')})
            wiz = self.browse(cursor, uid, ids, context)[0]
            extra_name = ''.join([c for c in wiz.lot.name if c.isalnum()])

        zipfile_io = StringIO.StringIO()
        comer_facts = wiz.get_factures_id(context=context)

        wiz.do_pdf_zip(
            comer_facts, zipfile_io,
            wiz.fulls, wiz.report.report_name, context=context
        )
        dbname = ''.join([c for c in cursor.dbname if c.isalnum()])

        wiz.write({'zipcontent': base64.b64encode(zipfile_io.getvalue()),
                   'name': 'FAC_%s_%s.zip' % (dbname, extra_name),
                   'state': 'end_pdf'})
        zipfile_io.close()

    def default_get(self, cr, uid, fields_list, context=None):
        if context is None:
            context = {}

        res = super(WizardExportPdfs, self).default_get(
            cr, uid, fields_list, context=context
        )

        for field in filter_comer_field_names(fields_list):
            res[field] = True

        model = context.get('model', 'giscedata.facturacio.lot')
        from_lot = model == 'giscedata.facturacio.factura'

        if 'lot' in fields_list:
            if from_lot:
                res['lot'] = context.get('active_id', False)
            else:
                res['lot'] = False

        return res

    def uncheck_all(self, cursor, uid, ids, context):
        comers = {field: False for field in self._columns if 'comer' in field}
        self.write(cursor, uid, ids, comers)
        return True

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        if context is None:
            context = {}

        res = super(WizardExportPdfs, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        model = context.get('model', 'giscedata.facturacio.lot')

        if model == 'giscedata.facturacio.factura':
            cursor.execute("SELECT DISTINCT COALESCE(p.ref, COALESCE(p.vat, '0000')) "
                           "FROM account_invoice i "
                           "LEFT JOIN giscedata_facturacio_factura f "
                           "    ON f.invoice_id = i.id "
                           "LEFT JOIN res_partner p "
                           "    ON i.partner_id = p.id "
                           "WHERE f.id in %s", (
                tuple(context.get('active_ids', [])), ))
        else:
            lot_id = context.get('active_id', False)
            if not lot_id:
                return res
            cursor.execute("SELECT DISTINCT COALESCE(p.ref, COALESCE(p.vat, '0000')) "
                           "FROM account_invoice i "
                           "LEFT JOIN giscedata_facturacio_factura f "
                           "    ON f.invoice_id = i.id "
                           "LEFT JOIN res_partner p "
                           "    ON i.partner_id = p.id "
                           "WHERE f.lot_facturacio=%s", (lot_id, ))
        comer_refs = [i[0] for i in cursor.fetchall()]

        self._columns.update(
            dict(
                # Field name -> Field declaration
                ('comer' + z, fields.boolean(string=z))
                for z in comer_refs
            )
        )

        res['fields'].update(
            dict(
                # Field name -> {Field type, Field shown string}
                ('comer' + z, {'type': 'boolean', 'string': z})
                for z in comer_refs
            )
        )
        xml = etree.fromstring(res['arch'])
        xpath_to_find = "//group[@name='comer_list']"
        comers_xml = xml.xpath(xpath_to_find)
        comers_xml[0].append(etree.fromstring(
            '<button type="object" name="uncheck_all" string="Desmarcar"/>'
        ))
        comers_xml[0].append(etree.fromstring('<newline/>'))
        for z in comer_refs:
            comers_xml[0].append(etree.fromstring(
                '<field name="{0}"/>'.format('comer' + z)
            ))
        res['arch'] = etree.tostring(xml)
        return res

    _columns = {
        'report': fields.many2one(
            'ir.actions.report.xml', 'Model de factura', required=True,
            domain=[('model', '=', 'giscedata.facturacio.factura')]
        ),
        'lot': fields.many2one('giscedata.facturacio.lot', 'Lot'),
        'fulls': fields.integer('Factures per PDF', required=True),
        'name': fields.char('Nom fitxer', size=128),
        'state': fields.selection(ESTATS, 'Estat'),
        'zipcontent': fields.binary('Fitxer ZIP'),
        'what_to_print': fields.selection(
            [('all', _('Totes')), ('some', _('Seleccionades'))],
            string='Comercialitzadores a imprimir'
        ),
        'date_filter': fields.boolean('Filtrar per data'),
        'start_date': fields.date('Data d\'inici'),
        'end_date': fields.date('Data Final'),
        'pem_ids': fields.text('Text to save facts by comer ref')
    }

    def _default_report(self, cursor, uid, context=None):
        if context is None:
            context = {}
        imd_obj = self.pool.get('ir.model.data')
        report_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_distri', 'report_factura'
        )[1]
        return report_id

    _defaults = {
        'report': _default_report,
        'fulls': lambda *a: 1000,
        'state': lambda *a: 'init',
        'what_to_print': lambda *a: 'all',
        'date_filter': lambda *a: False
    }

WizardExportPdfs()
