# -*- coding: utf-8 -*-
from libfacturacioatr.tarifes import repartir_energia
from osv import osv, fields
from datetime import datetime, timedelta
from calendar import monthrange


class WizardQuotesCne(osv.osv_memory):
    """Assistent que generarà les quotes a presentar per la CNE.
    """
    _name = 'wizard.quotes.cne'

    _columns = {
        'periode_id': fields.many2one('giscedata.liquidacio.fpd', 'Període',
                                      required=True),
        'data_tall': fields.date('Data de tall',
                                 help=(
            u"Quan hi ha un canvi de quotes en la CNE, podem indicar en "
            u"quina data es produeix aquest canvi i el programa farà el "
            u"càlcul corresponent.")
        ),
        'linies_ids': fields.one2many('wizard.quotes.cne.linia', 'quota_id',
                                      'Línies', readonly=True)
    }

    def mostrar_resultat(self, cursor, uid, ids, context):
        l_obj = self.pool.get('wizard.quotes.cne.linia')
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        for l in wizard.linies_ids:
            l.unlink()
        res = wizard.exportar(context)
        # Creem les línies de les quotes
        for k, v in res.items():
            if k == 'total':
                continue
            vals = {'name': k, 'quota_id': wizard.id}
            vals.update(v)
            l_obj.create(cursor, uid, vals)
        return True

    def exportar(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        f_obj = self.pool.get('giscedata.facturacio.factura')
        wizard = self.browse(cursor, uid, ids[0], context)
        # Busquem les factures
        search_params = [
            ('periode_liquidacio.id', '=', wizard.periode_id.id),
            ('invoice_id.journal_id.code', 'ilike', 'ENERGIA%'),
            ('invoice_id.state', 'in', ('open', 'paid'))
        ]
        if 'tarifa' in context:
            search_params += [
                ('tarifa_acces_id.name', '=', context['tarifa'])
            ]
        fids = f_obj.search(cursor, uid, search_params)
        fpd = wizard.periode_id
        data_final = '%s-%s-%s' % (fpd.year, fpd.month,
                            monthrange(int(fpd.year), int(fpd.month))[1])
        # Diccionari on hi ha la data de la quota i
        quotes = {}
        if wizard.data_tall:
            quotes['0000-00-00'] = {'kwh': 0, 'eur': 0}
            quotes[wizard.data_tall] = {'kwh': 0, 'eur': 0}
            dia_anterior = (datetime.strptime(wizard.data_tall, '%Y-%m-%d')
                            - timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            quotes[data_final] = {'kwh': 0, 'eur': 0}
        versions = dict([(k, 0) for k in quotes.keys()])
        for factura in f_obj.browse(cursor, uid, fids, context):
            if 'invoice' in factura.type:
                sign = 1
            elif 'refund' in factura.type:
                sign = -1
            for linia in factura.linia_ids:
                if linia.tipus not in ('energia', 'potencia', 'reactiva',
                                       'exces_potencia'):
                    continue
                # A nivell informatiu es presenta el consum kWh
                # Modifiquem en memòria les línies data_fins
                if wizard.data_tall and linia.data_fins == wizard.data_tall:
                    linia.data_fins = dia_anterior
                if linia.tipus == 'energia':
                    res = repartir_energia(versions, linia.quantity,
                                           linia.data_desde, linia.data_fins)
                    for k, v in res.items():
                        quotes[k]['kwh'] += v * sign
                res = repartir_energia(versions, linia.price_subtotal,
                                       linia.data_desde, linia.data_fins)
                for k, v in res.items():
                    quotes[k]['eur'] += v * sign
        quotes['total'] = {'eur': 0, 'kwh': 0}
        for k in versions:
            quotes['total']['eur'] += quotes[k]['eur']
            quotes['total']['kwh'] += quotes[k]['kwh']
        # Substituïm les claus
        if wizard.data_tall:
            q = {}
            for k, v in quotes.items():
                if k == '0000-00-00':
                    k = dia_anterior
                elif k == wizard.data_tall:
                    k = data_final
                q[k] = v
            res = q
        else:
            res = quotes
        return res


WizardQuotesCne()


class WizardQuotesCneLinia(osv.osv_memory):
    """Linia de cada quota
    """
    _name = 'wizard.quotes.cne.linia'
    _columns = {
        'quota_id': fields.many2one('wizard.quotes.cne', 'Quota'),
        'name': fields.date('Data'),
        'kwh': fields.char('kWh', size=25),
        'eur': fields.char('Eur', size=25),
    }
    _defaults = {
        'kwh': lambda *a: 0,
        'eur': lambda *a: 0
    }

WizardQuotesCneLinia()
