# -*- coding: utf-8 -*-
from osv import osv, fields
from tqdm import tqdm
from cini.models import Contador
import datetime

YEAR_SELECTION = [
    (str(x), str(x)) for x in range(
        datetime.datetime.now().year-5, datetime.datetime.now().year
    )
]


class WizardRecalculCinisTis(osv.osv_memory):

    _name = 'wizard.recalcul.cinis.tis'
    _inherit = 'wizard.recalcul.cinis.tis'

    def action_update_data_circular(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)

        year = wiz.year_circular
        last_day_year = '{}-12-31'.format(year)

        prod_obj = self.pool.get("product.product")
        modcon_obj = self.pool.get("giscedata.polissa.modcontractual")
        tarifa_obj = self.pool.get("giscedata.polissa.tarifa")
        contadors_obj = self.pool.get("giscedata.lectures.comptador")
        cups_obj = self.pool.get("giscedata.cups.ps")

        mod_all_year = modcon_obj.search(
            cursor, uid, [
                ("data_inici", "<=", "{}-01-01".format(year)),
                ("data_final", ">=", "{}-12-31".format(year)),
                ("tarifa.name", 'not ilike', '%RE%'),
                ('polissa_id.state', 'in', ['tall', 'activa', 'baixa'])
            ], 0, 0, False, {"active_test": False}
        )
        mods_ini = modcon_obj.search(
            cursor, uid, [
                ("data_inici", ">=", "{}-01-01".format(year)),
                ("data_inici", "<=", "{}-12-31".format(year)),
                ("tarifa.name", 'not ilike', '%RE%'),
                ('polissa_id.state', 'in', ['tall', 'activa', 'baixa'])
            ], 0, 0, False, {"active_test": False}
        )
        mods_fi = modcon_obj.search(
            cursor, uid, [
                ("data_final", ">=", "{}-01-01".format(year)),
                ("data_final", "<=", "{}-12-31".format(year)),
                ("tarifa.name", 'not ilike', '%RE%'),
                ('polissa_id.state', 'in', ['tall', 'activa', 'baixa'])
            ], 0, 0, False, {"active_test": False}
        )
        modcons_in_year = set(mods_fi + mods_ini + mod_all_year)

        search_params = [
            ('active', '=', True),
            '|',
            ('create_date', '<=', last_day_year),
            ('create_date', '=', False)
        ]
        cups_ids = cups_obj.search(
            cursor, uid, search_params, 0, 0, False, {'active_test': False}
        )
        cups_data_tmp = cups_obj.read(
            cursor, uid, cups_ids, ["id", "polisses"]
        )
        recalc_data = []
        total = len(cups_data_tmp)
        for cups in tqdm(cups_data_tmp, total=total,
                         desc="Filtrant ModCon-CUPS {}".format(year)):
            if set(cups['polisses']).intersection(modcons_in_year):
                search_modcon = [
                    ('id', 'in', cups['polisses']),
                    ('data_inici', '<=', last_day_year),
                    ('polissa_id.state', 'in', ['tall', 'activa', 'baixa'])
                ]
                modcons = modcon_obj.search(
                    cursor, uid, search_modcon, 0, 1, 'data_inici desc',
                    {'active_test': False}
                )
                if modcons:
                    recalc_data.append(
                        {'cups_id': cups['id'], 'modcon_id': modcons[0]}
                    )

        data_to_write = []
        total = len(recalc_data)
        info = "Recalculant CINI a {}".format(last_day_year)
        for elem in tqdm(recalc_data, total=total, desc=info):
            contador = Contador()
            modcon_id = elem['modcon_id']

            modcon_fields_to_read = [
                'comptador', 'agree_tipus', 'tensio_normalitzada', 'tarifa'
            ]
            tarifa_fields_to_read = ['tipus']
            comptador_fields_to_read = [
                'product_lloguer_id', 'technology_type', 'tg', 'propietat'
            ]

            modcon = modcon_obj.read(
                cursor, uid, modcon_id, modcon_fields_to_read
            )

            comptador_id = contadors_obj.search(
                cursor, uid, [('name', '=', modcon['comptador'])], None, None,
                None, {'active_test': False}
            )

            if comptador_id:
                comptador = contadors_obj.read(
                    cursor, uid, comptador_id[0], comptador_fields_to_read
                )

                tarifa_id = tarifa_obj.search(
                    cursor, uid, [('id', '=', modcon['tarifa'][0])]
                )[0]
                tarifa = tarifa_obj.read(
                    cursor, uid, tarifa_id, tarifa_fields_to_read
                )['tipus']

                if comptador['product_lloguer_id']:
                    prod_id = comptador['product_lloguer_id'][0]
                    prod = prod_obj.read(cursor, uid, prod_id, ['default_code'])
                    if not prod['default_code']:
                        prod['default_code'] = ''
                else:
                    prod = {'default_code': ''}

                if prod['default_code'] == 'ALQ19':
                    contador.fases = 1
                elif prod['default_code'] == 'ALQ20':
                    contador.fases = 3
                elif '3x' in modcon['tensio_normalitzada'][1]:
                    contador.fases = 3
                elif '1x' in modcon['tensio_normalitzada'][1]:
                    contador.fases = 1
                elif int(modcon['tensio_normalitzada'][1]) < 1000:
                    contador.fases = 1

                contador.tecnologia = comptador['technology_type']

                contador.telegestionado = comptador['tg']

                contador.tipo_agree = modcon['agree_tipus'][1]

                contador.tipo_tarifa = tarifa

                contador.propiedad_cliente = (
                        comptador['propietat'] == 'client'
                )

                data_to_write.append(
                    ([comptador['id']], {'cini': str(contador.cini)}))

        total = len(data_to_write)
        info = "Actualitzant CINI a {}".format(last_day_year)
        for elem in tqdm(data_to_write, total=total, desc=info):
            contadors_obj.write(
                cursor, uid, elem[0], elem[1]
            )

        info = ""
        if wiz.mode != 'cini':
            info = (
                "ATENCIÓ: Del model Contadors només es pot recalcular "
                "el camp CINI!\n\n"
            )
        info = (
            "S'han actualitzat un total de {} CINI del  model "
            "Comptadors a data de {}.\n\n".format(
                len(data_to_write), last_day_year
            ) + info + wiz.info
        )
        wiz.write({
            'mode': 'cini',
            'state': 'done',
            'info': info
        })

    def action_update_data(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        module_obj = self.pool.get(wiz.model)
        if wiz.model == "giscedata.lectures.comptador":
            if wiz.cini_circular:
                self.action_update_data_circular(cursor, uid, ids, context)
            else:
                search_params = [('bloquejar_cini', '=', False)]
                ids = module_obj.search(
                    cursor, uid, search_params, 0, 0, False,
                    {'active_test': False}
                )
                module_obj.write(cursor, uid, ids, {"cini_auto": 0})
                module_obj.write(cursor, uid, ids, {"cini_auto": 1})

                info = ""
                if wiz.mode != 'cini':
                    info = (
                        "ATENCIÓ: Del model Contadors només es pot recalcular "
                        "el camp CINI!\n\n"
                    )
                info = (
                    "S'han actualitzat un total de {} CINI del model "
                    "Comptadors.\n\n".format(len(ids))
                ) + info + wiz.info

                wiz.write({
                    'mode': 'cini',
                    'state': 'done',
                    'info': info
                })
        else:
            super(WizardRecalculCinisTis, self).action_update_data(
                cursor, uid, ids, context=None
            )

    _columns = {
        'cini_circular': fields.boolean(
            "Actualitzar per la Circular",
            help="Si es marca aquesta opció, es recalculara els CINI per els "
                 "comptadors a data 31/12 de l'any especificat en el "
                 "desplegable"
        ),
        'year_circular': fields.selection(
            YEAR_SELECTION, 'Any', required=True,
            help="Any de recalcul del CINI"
        )
    }

    _defaults = {
        'cini_circular': lambda *a: False,
        'year_circular': lambda *a: (
            str(datetime.datetime.now().year-1),
            str(datetime.datetime.now().year-1)
        ),
    }


WizardRecalculCinisTis()
