# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from datetime import datetime as dt
from tools import config
from tools.translate import _
from giscedata_facturacio.giscedata_facturacio import _generar_resum_csv
from giscedata_facturacio.wizard.wizard_informes_facturacio import (
    MULTI_SEQUENCE, INFORMES_DESC, INFORMES_EXPORTABLES
)
from giscedata_facturacio_distri.sql.sql_column_titles import *
import pandas as pd
from StringIO import StringIO
from base64 import b64encode

INFORMES_DESC.update(
    {
        'report_facturacio_distri_pertarifes_fechas':
        _(u"Resum de facturació per tarifes amb el desglosament dels consums "
          u"segons els periodes i energia.\nL'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_facturacio_distri_percomercial_fechas':
        _(u"Resum de facturació per comercialitzadora i tarifa amb el "
          u"desglosament dels consums segons els periodes i energia.\n"
          u"L'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_facturacio_distri_cne_fechas':
        _(u"Resum de facturació per tarifes que inclou el detall de l'energia "
          u"facturada, potència facturada i potència contractada per periode"),

        'report_facturacio_distri_municipi_fechas':
        _(u"Resum de facturació per municipi i tarifa. Inclou el desglosament "
          u"dels consums segons els periodes i energia.\nL'informe inclou el "
          u"resum en kWh, potència total, euros facturats i nombre de clients "
          u"afectats"),

        'report_facturacio_distri_comercial_municipio_fechas':
        _(u"Resum de facturació per comercialitzadora, municipi i tarifa. "
          u"Inclou el desglosament "
          u"dels consums segons els periodes i energia.\nL'informe inclou el "
          u"resum en kWh, potència total, euros facturats i nombre de clients "
          u"afectats"),

        'resumen_facturacion_distri_impuestos_comer_fecha':
        _(u"Resum de facturació per comercialitzadora. Detalla base, impostos "
          u"i total en euros facturats" ),

        'detalle_facturacion_distri_otros_comer_fechas':
        _(u"Resum de facturació per comercialitzadora que inclou únicament els "
          u"productes de tipus 'Altres'. El resum mostra els totals facturats."),

        'report_facturacio_distri_tipus_punt':
        _(u"Resum de facturació per tipus de punt amb el desglosament "
          u"dels consums "
          u"segons els periodes i energia.\nL'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_facturacio_distri_comer_tipus_punt':
        _(u"Resum de facturació per comercialitzadora i tipus de punt amb el "
          u"desglosament dels consums segons els periodes i energia.\n"
          u"L'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_facturacio_distri_municipi_tipus_punt':
        _(u"Resum de facturació per municipi i tipus de punt. Inclou el "
          u"desglosament dels consums segons els periodes i "
          u"energia.\nL'informe "
          u"inclou el resum en kWh, potència total, euros facturats i nombre "
          u"de clients afectats"),

        'export_factures_desglossat':
        _(u"Resum de les factures en CSV."),

        'export_trams_potencia_i_municipi':
        _(u"Resum CSV per municipi, tarifa i trams de pòtencia. El llistat "
          u"inclou les pòlisses actives i potència facturada en la "
          u"data final i l'energia total consumida en el periode"),

        'export_factures_desglossat_excel':
        _(u"Resum de les factures en Excel."),

        'informe_facturacion_contabilidad_distri':
        _(
            u"Informe que contiene el resumen detallado de la "
            u"contabilidad de la distribuidora agrupado por tarifa de "
            u"acceso"
        )
    }
)

INFORMES_CSV = {
    'export_factures_desglossat': {
            'header': QUERY_RESUM_DISTRI_TITLES,
            'query': ("%s/giscedata_facturacio_distri/sql/query_resum_xml.sql"
                      % config['addons_path'])
    },
    'export_trams_potencia_i_municipi': {
            'header': RESUM_TRAMS_POT_I_MUN_TITLES,
            'query': (
                "%s/giscedata_facturacio_distri/sql/resum_trams_pot_i_mun.sql"
                % config['addons_path'])
    },
    'export_factures_desglossat_excel': {
            'header': [
                _(u"N Abonado"),
                _(u"CP"),
                _(u"Tipo Facturació"),
                _(u"Comercialitzadora"),
                _(u"Tarifa peatge"),
                _(u"Tarifa comercial"),
                _(u"P P1"),
                _(u"P P2"),
                _(u"P P3"),
                _(u"P P4"),
                _(u"P P5"),
                _(u"P P6"),
                _(u"E P1 €"),
                _(u"E P1 kW"),
                _(u"E P2 €"),
                _(u"E P2 kW"),
                _(u"E P3 €"),
                _(u"E P3 kW"),
                _(u"E P4 €"),
                _(u"E P4 kW"),
                _(u"E P5 €"),
                _(u"E P5 kW"),
                _(u"E P6 €"),
                _(u"E P6 kW"),
            ],
            'query': ''
    },
}

MULTI_SEQUENCE = MULTI_SEQUENCE.update(
    {
        'report_facturacio_distri_pertarifes_fechas': True,
        'informe_facturacion_contabilidad_distri': True
    }
)

INFORMES_EXPORTABLES.extend(
    [
        'export_trams_potencia_i_municipi',
        'informe_facturacion_contabilidad_distri'
    ]
)


class WizardInformesFacturacioDistri(osv.osv_memory):

    _name = "wizard.informes.facturacio"

    _inherit = "wizard.informes.facturacio"

    _informes_csv = INFORMES_CSV

    def _get_informe(self, cursor, uid, context=None):
        """ Com a funció perquè si no no tradueix"""
        _opcions = super(WizardInformesFacturacioDistri, self)._get_informe(
            cursor, uid, context=context)
        _opcions += [
            ('report_facturacio_distri_pertarifes_fechas', _('Resum per tarifes')),
            ('report_facturacio_distri_percomercial_fechas', _('Resum per comercialitzadora')),
            ('report_facturacio_distri_cne_fechas', _('Resum CNE')),
            ('report_facturacio_distri_municipi_fechas', _('Resum per municipi i tarifa')),
            ('report_facturacio_distri_comercial_municipio_fechas', _('Resum per municipi, comercialitzadora i tarifa')),
            ('resumen_facturacion_distri_impuestos_comer_fecha', _('Resum per impostos i comercialitzadora')),
            ('report_facturacio_distri_tipus_punt', _('Resum per tipus de punt')),
            ('report_facturacio_distri_comer_tipus_punt', _('Resum per comercialitzadora i tipus de punt')),
            ('report_facturacio_distri_municipi_tipus_punt', _('Resum per municipi i tipus de punt')),
            ('detalle_facturacion_distri_otros_comer_fechas', _('Detall d\'altres conceptes')),
            ('export_factures_desglossat', _('Resum Factures (CSV)')),
            ('export_trams_potencia_i_municipi', _(u'Resum per trams de potència i municipi (CSV)')),
            ('export_factures_desglossat_excel', _(u"Resum factures Excel")),
            ('informe_facturacion_contabilidad_distri',
             _('Informe de facturación de contabilidad agrupado por '
               'tarifas de acceso')
             )
        ]
        return _opcions

    def exportar_csv_params(self, cursor, uid, ids, params, params_header='',
                            context=None):
        """ Exportació com a CSV """
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        _sqlfile = INFORMES_CSV[wizard.informe]['query']
        _sqlheader = INFORMES_CSV[wizard.informe]['header']
        if '%s' in str(_sqlheader): 
            _sqlheader = eval(str(_sqlheader) % params_header)
        _query = open(_sqlfile).read()
        res = _generar_resum_csv(cursor, params, _query, _sqlheader, context)
        info = 'Resum generat correctament'
        return res, info

    def exportar(self, cursor, uid, ids, context=None):
        """ Retorna el CSV sol·licitat """
        if not context:
            context = {}
        is_exported = super(WizardInformesFacturacioDistri, self).exportar(
            cursor, uid, ids, context=context
        )
        if not is_exported:
            vals = {}
            wizard = self.browse(cursor, uid, ids[0])
            if (wizard.informe == 'export_factures_desglossat' or
                wizard.informe == 'export_factures_desglossat_excel'):
                res, info = self.exportar_resum_factures(cursor, uid, ids, context)
                vals = {
                    'file': res['csv'],
                    'estat': 'done',
                    'info': info,
                }
            elif wizard.informe == 'export_trams_potencia_i_municipi':
                params = (wizard.data_final, wizard.data_final, wizard.data_inici,
                          wizard.data_final)
                params_header = (dt.strftime(dt.strptime(wizard.data_final,
                                                         '%Y-%M-%d'),
                                             '%d/%M/%Y'),)
                res, info = self.exportar_csv_params(
                    cursor, uid, ids, params, params_header, context=context
                )
                vals = {
                    'file': res['csv'],
                    'estat': 'done',
                    'info': info,
                }
            elif wizard.informe in ('informe_facturacion_contabilidad_distri',):
                df = self.get_df_generic_informe_contabilidad(
                    cursor, uid, ids, context=context
                )
                res = self.exportar_informe_contabilidad(
                    cursor, uid, ids, df, context=context
                )
                d_ini = wizard.data_inici
                d_fi = wizard.data_final

                f_name = 'Resumen_agrupado_por_tarifa_contabilidad_facturacion_{0}_{1}.csv'.format(d_ini, d_fi)
                vals = {
                    'file': res,
                    'estat': 'done',
                    'name': f_name
                }
            else:
                return is_exported

            if wizard.informe == 'export_factures_desglossat_excel':
                vals.update({'name': _('ResumFactures.xls')})

            if vals:
                wizard.write(vals)

            return bool(vals)

        return is_exported

    def exportar_informe_contabilidad(self, cursor, uid, ids, df, context=None):
        res = super(
            WizardInformesFacturacioDistri, self
        ).exportar_informe_contabilidad(
            cursor, uid, ids, df, context=context
        )

        if not res:
            informe = self.read(
                cursor, uid, ids[0], ['informe'], context=context
            )[0]['informe']

            if informe in ('informe_facturacion_contabilidad_distri',):
                df_count_clients = df[
                    ['Tarifa de acceso', 'id_client', 'Lista de precios']
                ].groupby(['Tarifa de acceso', 'Lista de precios'])['id_client'].apply(
                    lambda x: len(x.unique())
                ).reset_index().rename(index=str, columns={
                    "id_client": "Numero de clientes"})

                df_count_facturas = df[
                    ['Tarifa de acceso', 'id_invoice', 'Lista de precios']
                ].groupby(['Tarifa de acceso', 'Lista de precios'])['id_invoice'].apply(
                    lambda x: len(x.unique())
                ).reset_index().rename(index=str, columns={
                    "id_invoice": "Numero de facturas"})

                df_tariff_group = df.groupby(
                    ['Tarifa de acceso', 'Lista de precios']
                ).sum().reset_index()

                df_tariff_group = pd.merge(
                    df_tariff_group, df_count_clients,
                    how="inner", on=["Tarifa de acceso", "Lista de precios"]
                )

                df_tariff_group = pd.merge(
                    df_tariff_group, df_count_facturas,
                    how="inner", on=["Tarifa de acceso", "Lista de precios"]
                )

                cols = df_tariff_group.columns.tolist()

                col_n_clients = "Numero de clientes"
                col_n_factures = "Numero de facturas"

                # Get position after group
                new_position = df_tariff_group.columns.get_loc("Lista de precios") + 1

                cols.insert(new_position, cols.pop(cols.index(col_n_factures)))
                cols.insert(new_position, cols.pop(cols.index(col_n_clients)))
                cols.pop(cols.index("id_client"))
                cols.pop(cols.index("id_invoice"))

                df_tariff_group = df_tariff_group[cols]

                gen_file = StringIO()

                df_tariff_group.to_csv(
                    gen_file, header=True, sep=';', encoding='utf-8-sig',
                    index=None, decimal=',', float_format='%.2f'
                )

                return b64encode(gen_file.getvalue())

        return res

    _columns = {
        'informe': fields.selection(_get_informe, 'Informe', required=True),
    }

WizardInformesFacturacioDistri()
