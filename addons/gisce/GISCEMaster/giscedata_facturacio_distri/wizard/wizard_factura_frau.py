# -*- coding: utf-8 -*-
import json
from datetime import datetime

from osv import osv, fields
from tools.translate import _

class WizardFacturaFrau(osv.osv_memory):
    """Customització de l'asistent Factura manual per incloure
        la factura frau i les seves opcions.
    """
    _name = 'wizard.manual.invoice'
    _inherit = 'wizard.manual.invoice'

    def onchange_is_factura_frau(self, cursor, uid, ids, polissa_id, is_factura_frau):
        res = {'value': {'date_start': '', 'date_end': ''}}

        polissa_obj = self.pool.get('giscedata.polissa')
        journal_obj = self.pool.get('account.journal')
        journal_ids = journal_obj.search(cursor, uid, [('code', '=', 'ENERGIA')])

        if journal_ids:
            res['value'].update({'journal_id': journal_ids[0]})

        tarifa = polissa_obj.read(cursor, uid, polissa_id, ['tarifa'])['tarifa']
        if is_factura_frau and ('3.1' in tarifa[1] or '6.' in tarifa[1]):
            raise osv.except_osv(_(u"No es generaràn les línies"),
                                 _(u"Per la tarifa seleccionada no es generen "
                                   u"les línies automàticament. Les haurà d'entrar "
                                   u"manualment un cop generada la factura."))

        return res

    def onchange_dates(self, cursor, uid, ids, date_start, date_end, polissa_id):
        res = {'value': {'energia_frau': 0}}

        if not date_start or not date_end:
            return True

        if date_start and date_end:
            polissa_obj = self.pool.get('giscedata.polissa')

            data_inici_datetime = datetime.strptime(date_start, '%Y-%m-%d')
            data_final_datetime = datetime.strptime(date_end, '%Y-%m-%d')
            days_count = data_final_datetime - data_inici_datetime
            potencia = polissa_obj.read(cursor, uid, polissa_id, ['potencia'])['potencia']
            res['value'].update({'energia_frau': 6 * days_count.days * potencia})
            return res

    def action_manual_invoice(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])

        if wiz.is_factura_frau:
            if not context:
                context = {}

            context.update({'is_factura_frau': True,
                            'codi_expedient_frau': wiz.codi_expedient,
                            'account_id': wiz.account.id,
                            'energia_frau': wiz.energia_frau,
                            })

        super(WizardFacturaFrau, self).action_manual_invoice(cursor, uid, ids, context=context)

    _columns = {
        'is_factura_frau': fields.boolean(string="Es una factura de frau"),
        'account': fields.many2one('account.account', 'No. de compte comptable'),
        'codi_expedient': fields.char('Codi de l\'expedient', size=64),
        'force_account': fields.boolean(string="Forçar compte comptable"),
        'energia_frau': fields.integer('Energia a facturar (kWh)'),
    }

    _defaults = {
        'potencia_frau': lambda *a: 0,
    }


WizardFacturaFrau()