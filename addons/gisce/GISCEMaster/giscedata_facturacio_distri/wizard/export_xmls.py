# -*- coding: utf-8 -*-
from __future__ import absolute_import
import base64
import zipfile
import logging
from datetime import datetime

from tools import sign_document_with_certificate

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from osv import fields, osv
from tools.translate import _
from giscedata_facturacio.giscedata_facturacio import \
                            _generar_resum_factures_csv
from giscedata_facturacio_distri.wizard.wizard_informes_facturacio_distri \
                                                        import INFORMES_CSV
from giscedata_facturacio_distri.giscedata_facturacio import EXPORT_BLACK_LIST


_ESTATS = [('init', 'Inicial'),
           ('generant', 'Generant XMLs'),
           ('end', 'Final')]


class WizardExportXmls(osv.osv_memory):
    """Wizard per exportar factures en XML

    Es llençarà des del lot i demanarà la comercialitzadora
    """
    
    def _comer_selection(self, cursor, uid, context=None):
        """Busca les comercialitzadores del lot
        """
        if not context:
            context = {}
        active_id = context.get('active_id', False)
        if not active_id:
            return ()
        cursor.execute("""SELECT distinct partner_id FROM account_invoice
                          WHERE id IN (
                            SELECT invoice_id FROM giscedata_facturacio_factura
                            WHERE lot_facturacio = %s
                          )""", (active_id,))
        partner_ids = cursor.fetchall()
        partner_obj = self.pool.get('res.partner')
        comers = partner_obj.read(cursor, uid, partner_ids, ['id', 'ref',
                                                             'name'], context)
        res = [(c['ref'], c['name']) for c in comers]
        res = [('%', _('Totes'))] + res
        return res

    def _default_es_lot(self, cursor, uid, context=None):
        if not context:
            context = {}

        return context['factures'] == '1'

    def _default_can_sign(self, cursor, uid, context=None):
        if not context:
            context = {}

        conf_obj = self.pool.get('res.config')
        conf_id = conf_obj.search(cursor, uid, [('name', '=', 'f1_sign_with_certificate')], context=context)

        if conf_id:
            conf_val = conf_obj.read(cursor, uid, conf_id[0], ['value'], context=context)['value']
            return conf_val == '1'
        else:
            return False

    def persist_zip(self, cursor, uid, res, lot_id, context=None):

        if context['factures'] != '1' and lot_id:
            logger = logging.getLogger(
                'openerp.{0}.generar_xmls'.format(__name__))
            # export ZIP to
            with open("/tmp/{}".format(res['name']), "wb") as fh:
                fh.write(base64.b64decode(res['zipcontent']))
                logger.info('Export ZIP with F1 {}'.format(
                    "/tmp/{}".format(res['name'])))
            # desem en un adjunt al lot
            attach_obj = self.pool.get('ir.attachment')
            vals = {
                'name': res['name'],
                'datas': res['zipcontent'],
                'datas_fname': res['name'],
                'res_model': 'giscedata.facturacio.lot',
                'res_id': lot_id,
            }
            attch_id = attach_obj.create(cursor, uid, vals)
        return attch_id

    def generar_xmls(self, cursor, uid, ids, context=None):
        """Crida al mètode export_f1 per cada una de les factures que calgui
        """
        logger = logging.getLogger('openerp.{0}.generar_xmls'.format(__name__))
        if not context:
            context = {}
        _sqlheader = INFORMES_CSV['export_factures_desglossat']['header']
        _sqlfile = INFORMES_CSV['export_factures_desglossat']['query']

        wiz = self.browse(cursor, uid, ids, context)[0]
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        partner_obj = self.pool.get('res.partner')
        conf_obj = self.pool.get('res.config')
        comer_ref = '0000'

        search_params = []

        # mirem si hem d'imprimir factures o lot segons context
        if context['factures'] == '1':
            # tenim factures
            factures_ids = context.get('active_ids')
            wiz.write({'comer': '%'})
            lot_name = 'fact'
            search_params.append(
                ('id', 'in', factures_ids)
            )
        else:
            # és un lot
            # podem guardar el lot_id al wizard
            wiz.write({'lot': context.get('active_id')})

            lot_obj = self.pool.get('giscedata.facturacio.lot')
            lot = lot_obj.browse(cursor, uid, wiz.lot.id)
            lot_name = ''.join([x for x in lot.name if x.isalnum()])

            search_params = [
                ('lot_facturacio.id', '=', wiz.lot.id),
                ('state', 'in', ['open', 'paid']),
                ('tipo_rectificadora', 'not in', EXPORT_BLACK_LIST),
            ]
            if wiz.comer != '%':
                search_params.append(
                    ('invoice_id.partner_id.ref', '=', wiz.comer))
                comer_ref = wiz.comer
            if wiz.entre_dates:
                if wiz.data_inici:
                    search_params.append(
                        ('date_invoice', '>=', wiz.data_inici))
                if wiz.data_final:
                    search_params.append(
                        ('date_invoice', '<=', wiz.data_final))

        if not wiz.force_mark_exported:
            # Wiz this condition avoid already exported invoices
            search_params.append(
                ('f1_exported', '=', False)
            )
        if search_params:
            factures_ids = factura_obj.search(cursor, uid, search_params)

        if not factures_ids:
            raise osv.except_osv(
                'ERROR',
                _('No s\'han trobat factures per exportar:\n'
                  'Es possible que si no ha marcat forçar l\'exportació '
                  'les factures seleccionades estiguesin marcades com '
                  'exportades')
            )
        # re-llegim per tenir no tenir ensurts
        wiz = self.browse(cursor, uid, ids, context)[0]

        if len(factures_ids) == 1 and context['factures'] == '1':
            # no fem ZIP, només un XML
            active_id = context.get('active_id')
            fname, xml = factura_obj.export_new_f1(
                cursor, uid, active_id, context
            )
            if wiz.signar:
                xml = sign_document_with_certificate(xml)

            if wiz.mark_as_exported:
                # Marquem com a exportada la factura
                data_actual = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

                factura_obj.write(
                    cursor, uid, active_id,
                    {'f1_exported': True, 'f1_export_date': data_actual},
                    context=context
                )

            res = {'zipcontent': base64.b64encode(xml),
                   'name': fname, 'state': 'end'}
            if wiz.resum:
                _query = open(_sqlfile).read()
                resum_csv = _generar_resum_factures_csv(cursor,
                    factures_ids, _query, _sqlheader, context)
                res.update({
                'namecsv': "%stxt" % (fname[:-3])
                })
                res.update({'resumcontent': resum_csv['csv']})
            wiz.write(res)
            return True

        zipfiles = {}
        sios = {}
        partners_ref = {}
        initial_time = datetime.now()
        factures_data = factura_obj.read(
            cursor, uid, factures_ids, ['partner_id'])
        unique_partners = set()
        logger.info('Start to export F1 time at: {0}'.format(initial_time))
        for factura_data in factures_data:
            unique_partners.add(factura_data['partner_id'][0])
        unique_partners = list(unique_partners)
        # Create folders and create relation partner_id -> ref
        for partner in partner_obj.read(cursor, uid, unique_partners, ['ref']):
            ref = partner['ref'] or "P%s" % partner['id']
            partners_ref[partner['id']] = ref
            sios[ref] = StringIO.StringIO()
            zipfiles[ref] = zipfile.ZipFile(sios[ref], 'w',
                                            compression=zipfile.ZIP_DEFLATED)
        total_facts = len(factures_data)
        for i, factura in enumerate(factures_data):
            ti = datetime.now()
            ref = partners_ref[factura['partner_id'][0]]
            fname, xml = factura_obj.export_new_f1(cursor, uid, factura['id'])

            if wiz.mark_as_exported:
                data_actual = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
                factura_obj.write(
                    cursor, uid, factura['id'],
                    {'f1_exported': True, 'f1_export_date': data_actual},
                    context=context
                )

            if wiz.signar:
                xml = sign_document_with_certificate(xml)

            zipfiles[ref].writestr(fname, xml)
            logger.info('Export F1 time {0} / {1} {2}'.format(i, total_facts,
                str(datetime.now() - ti)
            ))
        the_zip_io = StringIO.StringIO()
        the_zip = zipfile.ZipFile(the_zip_io, 'w',
                                  compression=zipfile.ZIP_DEFLATED)
        res = {}
        if wiz.resum:
            _query = open(_sqlfile).read()
            resum_csv = _generar_resum_factures_csv(cursor,
                factures_ids, _query, _sqlheader, context)
            res = {
            'namecsv': 'facturacion_resumen_%s_%s.txt' % (comer_ref, lot_name)
            }
            res.update({'resumcontent': resum_csv['csv']})
        # si només fem una comer, no fem 2 zips un dins de l'altre
        finish_time = datetime.now()
        logger.info(
            'Finish export F1 at {0} in :{1}'.format(
                finish_time, str(finish_time - initial_time))
        )
        zip_name = conf_obj.get(cursor, uid, 'atr_f1_zip_file_template',
                                'facturacion_xml_{comer}_{lote}.zip')
        if len(zipfiles) == 1:
            ref = zipfiles.keys()[0]
            zipfiles[ref].close()
            res.update({
                 'zipcontent': base64.b64encode(sios[ref].getvalue()),
                 'name': 'facturacion_xml_%s_%s.zip' % (comer_ref, lot_name),
                 'state': 'end'})

            wiz.write(res)
            sios[ref].close()
            if context['factures'] != '1':
                self.persist_zip(cursor, uid, res, lot.id, context=context)
            return True

        for fname, zipf in zipfiles.items():
            zipf.close()
            the_zip.writestr(zip_name.format(comer=fname, lote=lot_name),
                             sios[fname].getvalue())
            sios[fname].close()
        the_zip.close()
        res.update({
                   'zipcontent': base64.b64encode(the_zip_io.getvalue()),
                   'name': 'facturacion_xml_%s_%s.zip' % (comer_ref, lot_name),
                   'state': 'end'})
        the_zip_io.close()
        if context['factures'] != '1':
            self.persist_zip(cursor, uid, res, lot.id, context=context)
        wiz.write(res)
        return True

    _name = 'wizard.export.xmls'
    _columns = {
        'lot': fields.many2one('giscedata.facturacio.lot', 'Lot'),
        'comer': fields.selection(_comer_selection, 'Comercialitzadora',
                                  required=True),
        'name': fields.char('Filename', size=128),
        'namecsv': fields.char('Filename CSV', name="index.txt",
            filters='*.txt', size=128),
        'resum': fields.boolean('Genera resum'),
        'signar': fields.boolean('Signar electrònicament'),
        'zipcontent': fields.binary('Fitxer ZIP'),
        'resumcontent': fields.binary('Fitxer CSV'),
        'state': fields.selection(_ESTATS, 'Estat'),
        'es_lot': fields.boolean('Es Lot'),
        'can_sign': fields.boolean('Pot signar electrònicament'),
        'entre_dates': fields.boolean(
            'Filtrar per dates',
            help=u'Habilita la funcionalitat per poder filtrar les factures a '
                 u'exportar entre dates'
        ),
        'data_inici': fields.date(
            'Data inici',
            help=u'Filtra factures amb la data més gran o igual'
                 u' que la data indicada'
        ),
        'data_final': fields.date(
            'Data final',
            help=u'Filtra factures amb la data menor o igual a la data indicada'
        ),
        'mark_as_exported': fields.boolean(
            'Marcar com exportat',
            help=u'Marcara com a exportades les factures que no ho estiguesin '
                 u'i posara com a data d\'exporació la data actual'
        ),
        'force_mark_exported': fields.boolean(
            'Forçar exportació',
            help=u'Marcara com a exportades amb data actual, '
                 u'totes les factures independentment de si ja '
                 u'estaven marcades'

        ),
        'info': fields.text('Info',)

    }

    def _default_info(self, cursor, uid, context=None):
        msg = u''
        # Factures
        if context.get('factures', False) == '1':
            msg = 'S\'exportaran {} F1'.format(
                len(context.get('active_ids', False) or [])
            )
        else:
            lot_obj = self.pool.get('giscedata.facturacio.lot')
            lot_id = context.get('active_id', False)
            name = lot_obj.read(
                cursor, uid, lot_id, ['name'], context=context
            )['name']

            msg = _(u'S\'exportaran les factures del '
                    u'lot {} que no siguin de tipus {}'
                    ).format(name, ','.join(EXPORT_BLACK_LIST))
        return msg

    _defaults = {
        'state': lambda *x: 'init',
        'resum': lambda *x: 0,
        'signar': lambda *x: 0,
        'es_lot': _default_es_lot,
        'can_sign': _default_can_sign,
        'comer': lambda *x: '%',
        'mark_as_exported': lambda *a: True,
        'force_mark_exported': lambda *a: False,
        'info': _default_info

    }

WizardExportXmls()
