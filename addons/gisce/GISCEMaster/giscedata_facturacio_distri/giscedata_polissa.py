# -*- coding: utf-8 -*-
"""Classes de facturació relatives a les pòlisses"""

# pylint: disable=E1101,W0223
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from giscedata_facturacio.defs import FACTURACIO_SELECTION
from giscedata_facturacio.giscedata_polissa import INTERVAL_INVOICING_FIELDS

INTERVAL_INVOICING_FIELDS += ['comercialitzadora', 'titular']

FACTURACIO_SELECTION.append(
    (12, 'Anual')
)


class GiscedataPolissa(osv.osv):
    """Extensió de la pòlissa amb les dades bàsiques de facturació
    (distribuïdora)."""

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _pagador_selection = [('comercialitzadora', 'Comercialitzadora')]

    def __init__(self, pool, cursor):
        """Afegim nous pagadors
        """
        super(GiscedataPolissa, self).__init__(pool, cursor)
        # _pagador_selection
        for new_p_selection in self._pagador_selection:
            if new_p_selection not in self._columns['pagador_sel'].selection:
                self._columns['pagador_sel'].selection.append(new_p_selection)

    def onchange_titular(self, cursor, uid, ids, titular, pagador_sel, pagador,
                         tipo_pago, notificacio=False, context=None):
        """En distribució no passa res al canviar de titular.
        """
        res = {'value': {}, 'domain': {}, 'warning': {}}
        return res

    def onchange_comercialitzadora(self, cursor, uid, ids, comercialitzadora,
                                   pagador_sel=None, notificacio=None,
                                   context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = super(GiscedataPolissa,
                     self).onchange_comercialitzadora(cursor, uid, ids,
                                                      comercialitzadora,
                                                      context)
        res.update(vals)
        vals = self.onchange_pagador_sel(cursor, uid, ids, pagador_sel,
                                         titular=None, pagador=None,
                                         direccio_pagament=None,
                                         tipo_pago=None,
                                         comercialitzadora=comercialitzadora,
                                         notificacio=notificacio,
                                         context=context)
        res.update(vals)
        return res

    def onchange_pagador_sel(self, cursor, uid, ids, pagador_sel, titular,
                             pagador, direccio_pagament, tipo_pago,
                             comercialitzadora, notificacio, context=None):
        """Actua quan hi ha un canvi al camp pagador_sel.

        Com que només hem afegit el nou tipus de comercialitzadora. Cridem
        l'onchange base i després si el tipus és comercialitzadora fem les
        nostres modificacions, sino ja ho haurà fet el base.
        """
        partner_obj = self.pool.get('res.partner')
        res = {'value': {}, 'domain': {}, 'warning': {}}
        vals = super(GiscedataPolissa,
                     self).onchange_pagador_sel(cursor, uid, ids, pagador_sel,
                                                titular, pagador,
                                                direccio_pagament, tipo_pago,
                                                notificacio, context)
        # Actualitzem el resultat amb la funció base
        res.update(vals)
        # emplenem el domain i value per la direccio de pagament
        # TODO: Funció genèrica pels valors d'un pagador.
        if pagador_sel == 'comercialitzadora':
            if comercialitzadora:
                domain = [('partner_id', '=', comercialitzadora)]
                dir_pago = partner_obj.address_get(cursor, uid, [comercialitzadora],
                                               ['invoice'])['invoice']
                partner = self.pool.get('res.partner').browse(cursor, uid,
                                                          comercialitzadora)
                if not partner.payment_type_customer:
                    res['warning'].update({'title': _('Avis'),
                        'message': _('Aquesta comercialitzadora no té cap tipus '
                                                 'de pagament predefinit')})
                else:
                    payment_type = partner.payment_type_customer
                    res.get('value').update({
                        'tipo_pago': payment_type.id,
                        'bank': False,
                    })
                    if payment_type.code == 'RECIBO_CSB':
                        default_partner_bank_id = partner_obj.get_default_bank(
                            cursor, uid, partner.id
                        )
                        res.get('value').update({
                            'bank': default_partner_bank_id
                        })
                res['domain'].update({'direccio_pagament': domain,
                                  'pagador': [('id', '=', comercialitzadora)]})
                res['value'].update({'direccio_pagament': dir_pago,
                                 'pagador': comercialitzadora})
            else:
                #Hem seleccionat comercialitzadora pero no tenim comercialitzadora
                res['domain'].update({'direccio_pagament': [],
                                  'pagador': []})
                res['value'].update({'direccio_pagament': False,
                                 'pagador': False})
            if notificacio == 'pagador':
                res['domain'].update({'direccio_notificacio':
                        res['domain'].get('direccio_pagament', [])}
                )
                res['value'].update({'direccio_notificacio':
                                 res['value'].get('direccio_pagament', False)}
                )
        return res

    def _check_tipo_pago(self, cursor, uid, ids, context=None):
        '''check if payment type and bank combination is correct'''
        mode_obj = self.pool.get('payment.mode')

        for polissa_id in ids:
            polissa = self.browse(cursor, uid, polissa_id)
            # Do not check if esborrany
            if polissa.state == 'esborrany':
                continue
            # if not payment type continue
            if not polissa.tipo_pago:
                continue
            # Check if there is a payment mode associated
            search_params = [('type', '=', polissa.tipo_pago.id)]
            mode_ids = mode_obj.search(cursor, uid, search_params)
            if len(mode_ids) == 0:
                continue
            mode = mode_obj.browse(cursor, uid, mode_ids[0])
            if mode.require_bank_account and not polissa.bank:
                return False
        return True

    def _cnt_tg_facturacio(self, cursor, uid, ids, context=None):
        """Returns false if tg > 0 and not monthly invoicing"""
        res_config = self.pool.get('res.config')
        check_monthly = int(res_config.get(cursor, uid, 'tg_check_monthly', 1))
        states_ok = tuple(CONTRACT_IGNORED_STATES + ['baixa', 'modcontractual'])
        pol_fields = ['state', 'tg', 'facturacio']
        for polissa_vals in self.read(cursor, uid, ids, pol_fields,
                                      context=context):
            if polissa_vals['state'] in states_ok:
                return True
            # When TG and not monthly invoicing, Error
            if (
                polissa_vals['tg'] != '2'
            ) and (
                polissa_vals['facturacio'] == 2
            ) and (
                check_monthly
            ):
                return False
        return True

    def _cnt_facturacio_12(self, cursor, uid, ids, context=None):
        for polissa in self.browse(cursor, uid, ids, context=context):
            if polissa.facturacio == 12 and polissa.tarifa.name != 'RE':
                return False
        return True

    _constraints = [
        (_check_tipo_pago,
         _(u"Error: No es pot validar la combinació de tipus "
           u"de pagament i compte bancari"),
         ['tipo_pago', 'bank']),
        (_cnt_tg_facturacio,
         _(u"Error: Les pòlisses telegestionades s'han de "
           u"facturar mensualment"),
         ['facturacio', 'tg']),
        (_cnt_facturacio_12,
         (u"Error: No es pot assignar facturació anual si no és "
          u"de regim especial"),
         ['facturacio'])
    ]

    _sql_constraints = [('percentatge_positiu', 'CHECK (re_perdidas >= 0)',
                         _(u'El % de perdues ha de ser positiu.'))
                        ]

    _columns = {
        're_perdidas': fields.float('% de perdues', readonly=True,
                                    states={'esborrany': [('readonly', False)],
                                            'modcontractual': [('readonly', False)],
                                            'validar': [('readonly', False)]
                                            },
                                    digits=(4, 2),
                                    help='% de perdues en el consum mesurat'
                                    )
    }



GiscedataPolissa()


class GiscedataPolissaTarifa(osv.osv):
    """Modificació de la tarifa per la distribuidora.
    """
    _name = 'giscedata.polissa.tarifa'
    _inherit = 'giscedata.polissa.tarifa'

    _columns = {
        'tarifa_liquidacio': fields.many2one(
                'giscedata.liquidacio.tarifes',
                'Codi liquidació',
                readonly=True,
                ondelete='restrict'
        )
    }

GiscedataPolissaTarifa()


class GiscedataPolissaModcontractual(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def _cnt_tg_facturacio(self, cursor, uid, ids, context=None):
        """Returns false if tg > 0 and not monthly invoicing"""
        res_config = self.pool.get('res.config')
        check_monthly = int(res_config.get(cursor, uid, 'tg_check_monthly', 1))
        modcon_fields = ['tg', 'facturacio']
        for modcon_vals in self.read(cursor, uid, ids, modcon_fields,
                                      context=context):
            # When TG and not monthly invoicing, Error
            if (
                modcon_vals['tg'] != '2'
            ) and (
                modcon_vals['facturacio'] == 2
            ) and (
                check_monthly
            ):
                return False
        return True

    _constraints = [(_cnt_tg_facturacio,
                 _(u"Error: Les pòlisses telegestionades s'han de "
                   u"facturar mensualment"),
                 ['facturacio', 'tg'])
                ]
    _columns = {
        're_perdidas': fields.float('% de perdues')
    }
GiscedataPolissaModcontractual()
