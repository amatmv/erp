# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields

import calendar
import time
from datetime import datetime, timedelta
import re
from collections import namedtuple
from copy import deepcopy

import StringIO
import base64

from switching.output.messages import facturacio as f1
from switching.output.messages import mesures as m
from switching.helpers.funcions import *
from tools.translate import _

from gestionatr.output.messages import sw_f1 as new_f1
from gestionatr.defs import \
    CONCEPTOS_CON_FECHA_OPERACION, REQUIRE_PERSON_TYPE, TARIFES_SEMPRE_MAX

from base_extended.excel import Sheet
from libfacturacioatr.tarifes import insert_data_traspas
from gestionatr.defs import CONV_T109_T111
from giscedata_facturacio.giscedata_facturacio import TARIFES
from giscedata_facturacio.defs import SIGN, TRANSLATE_ORIGIN_TO_NEW_F1
from libfacturacioatr.tarifes import TarifaRE

from .defs import MCP, MAGNITUDS, MULTI_CONCEPTS

from giscedata_facturacio.giscedata_facturacio \
    import RECTIFYING_RECTIFICATIVE_INVOICE_TYPES, \
    ALL_RECTIFICATIVE_INVOICE_TYPES

from json import loads, dumps

TARIFES['RE'] = TarifaRE
TARIFES['RE12'] = TarifaRE

EXPORT_BLACK_LIST = ['R', 'B', 'BRA']

TIPUS_LINIA_IESE = ['energia', 'potencia', 'reactiva', 'exces_potencia']



def generar_csv_cne_1_2005_7na():
    pass


class GiscedataFacturacioFacturador(osv.osv):
    """Implementació d'alguns aspectes específics de distribució.
    """
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def config_facturador(self, cursor, uid, facturador, polissa_id,
                          context=None):

        super(GiscedataFacturacioFacturador, self).config_facturador(
            cursor, uid, facturador, polissa_id, context=context
        )
        pol_obj = self.pool.get('giscedata.polissa')
        polissa = pol_obj.read(cursor, uid, polissa_id, ['re_perdidas'],
                               context=context)
        if polissa['re_perdidas']:
            facturador.conf['re_perdidas'] = polissa['re_perdidas']

    def create_invoice(self, cursor, uid, modcontractual_id, data_inici,
                       data_final, context=None):
        """Sobreescrivim la factura pels casos de canvi de comercialitzadora.
        """
        f_id = super(GiscedataFacturacioFacturador,
                     self).create_invoice(cursor, uid, modcontractual_id,
                                          data_inici, data_final, context)
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        if isinstance(modcontractual_id, list) or isinstance(modcontractual_id,
                                                             tuple):
            modcontractual_id = modcontractual_id[0]
        modcontract = modcont_obj.browse(cursor, uid, modcontractual_id,
                                         context)
        payment_term = (modcontract.pagador.property_payment_term and
                        modcontract.pagador.property_payment_term.id or False)
        vals = {
            'partner_id': modcontract.pagador.id,
            'address_invoice_id': modcontract.direccio_pagament.id,
            'address_contact_id': modcontract.direccio_notificacio.id,
            'partner_bank': modcontract.bank.id,
            'payment_type': modcontract.tipo_pago and modcontract.tipo_pago.id,
            'payment_term': payment_term
        }
        factura_obj.write(cursor, uid, [f_id], vals)
        return f_id

    def crear_linies_lloguer(self, cursor, uid, factura_id,
                             vals, context=None):
        uom_obj = self.pool.get('product.uom')
        uom_id = vals['uos_id']
        uom = uom_obj.browse(cursor, uid, uom_id, context=context)
        # Mirem si la facturació del lloguer és per dies. Si és per dies
        # s'ha d'agafar la unitat que toqui segons sigui any de traspàs o no
        if not uom.factor % 365 or not uom.factor % 366:
            uom_id = self.get_uoms_leaps(
                    cursor, uid, uom_id, vals['data_desde']
            )
            vals['uos_id'] = uom_id
        return super(GiscedataFacturacioFacturador, self).crear_linies_lloguer(
            cursor, uid, factura_id, vals, context=context
        )

    def fact_lloguer(self, facturador, comptador, data_inici_periode_f,
                     data_final_periode_f, context=None):
        """Facturem el lloguer.
        """
        if context is None:
            context = {}
        if not comptador.product_lloguer_id:
            return {}
        lloguer_id = comptador.product_lloguer_id.id
        lloguer_name = comptador.product_lloguer_id.name
        facturador.conf['lloguers'] = [lloguer_name]
        data_alta_c = max(comptador.data_alta, data_inici_periode_f)
        data_baixa_c = min(comptador.data_baixa or '3000-01-01',
                           data_final_periode_f)
        start_rent_date = comptador.start_rent_date
        if start_rent_date:
            if data_alta_c < start_rent_date < data_baixa_c:
                data_alta_c = comptador.start_rent_date
            elif start_rent_date > data_baixa_c:
                return {}
        base = 'mes'
        factor = comptador.property_uom_id.factor
        if not factor % 365 or not factor % 366:
            base = 'dia'
        # Com que pot ser que la potencia no es facturi per dies pero el lloguer
        # sí que s'ha de facturar per dies, i a l'hora de repartir els dies es
        # fa segons les versions de llistes de preus, hem de fer un facturador
        # de mentida (flloguer) on hi posarem les versions inventades només
        # per repartir si hi ha any de traspàs
        flloguer = deepcopy(facturador)
        fake_end_v = (
            datetime.strptime(data_baixa_c, '%Y-%m-%d') + timedelta(days=1)
        ).strftime('%Y-%m-%d')
        # Ens creem un diccionari de versions on fem servir data inici i data
        # final + 1 dia i si és any de traspas ens insertarà a mig una nova
        # versió amb la data de canvi d'any
        flloguer.conf['versions'] = insert_data_traspas({
            data_alta_c: 1, fake_end_v: 2
        })
        flloguer.factura_lloguer(data_alta_c, data_baixa_c, base=base)
        # Facturem el lloguer i ho assignem al facturador original
        facturador.termes['lloguer'] = flloguer.termes['lloguer'].copy()
        for v in facturador.termes['lloguer']:
            idx = facturador.termes['lloguer'][v]
            idx[lloguer_name]['uos_id'] = comptador.property_uom_id.id
        return {lloguer_name: lloguer_id}

    def factura_aplicable_iese(self, cursor, uid, pagador_sel):
        if pagador_sel == 'titular':
            return True
        return False

    def linia_aplicable_iese(self, cursor, uid, pagador_sel, linia):
        '''
        :param cursor:
        :type cursor: sql_db.Cursor
        :param uid:
        :type uid: int
        :param pagador_sel:
        :type  pagador_sel:
        :param linia: giscedata.facturacio.linia browse
        :type linia: osv.orm.browse_record
        :return:
        '''
        if pagador_sel == 'titular':
            if linia.tipus in TIPUS_LINIA_IESE:
                return True
        return False

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """Facturem la pòlissa a través de les lectures.
        """
        fact_ids = super(GiscedataFacturacioFacturador, self).fact_via_lectures(
            cursor, uid, polissa_id, lot_id, context
        )
        tax_obj = self.pool.get('account.tax')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        iese_id = tax_obj.search(cursor, uid, [
            ('name', '=ilike', '%electricidad')
        ])
        for factura in fact_obj.browse(cursor, uid, fact_ids):
            # No cobrar IESE si són contractes de règim especial
            if factura.tarifa_acces_id.name in ('RE', 'RE12'):
                continue
            reset_taxes = False
            pagador_sel = factura.polissa_id.pagador_sel
            if self.factura_aplicable_iese(cursor, uid, pagador_sel):
                # Hem d'afegir l'impost elèctric a les línes d'energia,
                # potència, reactiva i excés potència
                assert len(iese_id) == 1
                for linia in factura.linia_ids:
                    if self.linia_aplicable_iese(cursor, uid, pagador_sel, linia):
                        linia.write({'invoice_line_tax_id': [(4, iese_id[0])]})
                        reset_taxes = True
            if reset_taxes:
                factura.button_reset_taxes()

        return fact_ids



GiscedataFacturacioFacturador()


class GiscedataFacturacioFactura(osv.osv):
    """Customització per distribució.
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def default_periode_liquidacio(self, cursor, uid, context=None):
        """Retornem el periode de liquidació per defecte segons la data.
        """
        fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
        date = time.strftime('%Y-%m-%d')
        search_params = [('inici', '<=', date),
                         ('final', '>=', date)]
        periode_ids = fpd_obj.search(cursor, uid, search_params)
        if periode_ids:
            return periode_ids[0]
        else:
            return False

    def export_f1(self, cursor, uid, ids, context=None):
        """Retorna la factura en format XML F1
        """
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        fac = self.browse(cursor, uid, ids, context)
        if not fac:
            return None
        if not fac.journal_id.code.startswith('ENERGIA'):
            msg = _(u"No està soportada l'exportació de factures del tipus"
                    u" %s") % fac.journal_id.name
            raise osv.except_osv('Error', msg)
        if not fac.number:
            msg = _(u"Cal obrir la factura per poder generar l'XML")
            raise osv.except_osv('Error', msg)

        poli_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        ctx = context.copy()
        ctx['date'] = fac.data_final
        poli = poli_obj.browse(cursor, uid, fac.polissa_id.id, context=ctx)
        modcon_id = poli_obj._get_modcon_date(cursor, uid, poli.id,
                                              fac.data_final)
        modcon_vals = modcon_obj.read(cursor, uid, modcon_id, ['tg'])

        # capçalera
        codi_r1 = fac.company_id.cups_code
        msg = f1.MensajeFacturacion()
        partner_ref = fac.partner_id.ref or "P%s" % fac.partner_id.id
        msg.set_agente(partner_ref)
        cod_sol = "%s%s" % (partner_ref, fac.number)
        cabecera = f1.Cabecera()
        cabecera.feed({
            'ree_emisora': codi_r1,
            'ree_destino': partner_ref,
            'proceso': 'F1',
            'paso': '01',
            'solicitud': cod_sol,
            'secuencia': '01',
            'codigo': fac.cups_id.name.ljust(22),
            'fecha': fac.date_invoice,
        })

        cli_id = fac.partner_id.vat
        cifnif = 'NI'
        if cli_id[0].isalnum():
            cifnif = 'CI'
        cliente = f1.Cliente()
        cliente.feed({
            'identificador': fac.partner_id.vat,
            'cifnif': cifnif,
        })

        dirsuministro = f1.DireccionSuministro()
        dirsuministro.feed({
            'dirsuministro': fac.cups_id.direccio,
            'cups': fac.cups_id.name,
            'municipio': fac.cups_id.id_municipi.ine,
        })

        # dades factura
        cif = fac.company_id.partner_id.vat
        datosgrl = f1.DatosGeneralesFactura()
        dades_grl = {
            'numero': fac.number,
            'tipo': fac.tipo_factura,
            'rectificadora': fac.tipo_rectificadora,
            'fecha': fac.date_invoice,
            'cif': cif[:2] == 'ES' and cif[2:] or cif,
            'codigo': fac.number,
            'obs': '. ',
            'importe': fac.amount_total,
            'saldo': fac.residual,
            'saldocobro': fac.residual,
        }
        if fac.tipo_rectificadora in ALL_RECTIFICATIVE_INVOICE_TYPES:
            dades_grl.update({'ref': fac.ref.number})
        datosgrl.feed(dades_grl)

        # calculem numero de mesos
        data_inici = datetime.strptime(fac.data_inici, '%Y-%m-%d')
        data_final = datetime.strptime(fac.data_final, '%Y-%m-%d')
        n_meses = 0
        while data_final >= data_inici:
            n_meses += 1.0 / calendar.monthrange(data_final.year,
                                                 data_final.month)[1]
            data_final -= timedelta(days=1)

        periodo = f1.Periodo()
        periodo.feed({
            'fecha_desde': fac.data_inici,
            'fecha_hasta': fac.data_final,
            'meses': round(n_meses, 2),
        })

        # We calc ennergy term to fill PeriodoCCH if is necessary
        term_energia = {}
        for lene in fac.linies_energia:
            key = '%s|%s' % (lene.data_desde, lene.data_fins)
            term_energia.setdefault(key, {})
            term_energia[key].setdefault(lene.name, {})
            term_energia[key][lene.name].update({
                'valor': lene.quantity,
                'precio': lene.price_unit,
            })

        t_energia = []
        for key in sorted(term_energia):
            tene = f1.TerminoEnergiaActiva()
            penes = []
            for per in sorted(term_energia[key]):
                pene = f1.PeriodoEnergiaActiva()
                pene.feed(term_energia[key][per])
                penes.append(pene)
            te_inici, te_final = key.split('|')
            tene.feed({
                'fecha_desde': te_inici,
                'fecha_hasta': te_final,
                'periodos': penes,
            })
            t_energia.append(tene)

        mcp = {'icp': 1, 'recarrec': 1, 'max': 2}
        tg = '{0:02d}'.format(int(modcon_vals.get('tg', 2)))
        icch = CONV_T109_T111.get(tg, '03')
        # icch value not only depends on polissa_tg
        # value but contracted power
        if icch == '03' and fac.potencia < 15:
            icch = '02'
        datosatr = f1.DatosFacturaATR()

        datosatr_dict = {
            'tipo': fac.tipo_facturacion,
            'boe': fac.date_boe,
            'tarifa': fac.tarifa_acces_id.codi_ocsum,
            'mcp': mcp.get(poli.facturacio_potencia, 1),
            'imab': 'S' if 'LB' in fac.tarifa_acces_id.name else 'N',
            'indicativo_curva_carga': icch,
            'periodo': periodo,
        }

        # We've got CCH
        if icch == '01':
            periodecch = f1.PeriodoCCH()
            cch_desde = min([e.fecha_desde.value for e in t_energia])
            cch_hasta = max([e.fecha_hasta.value for e in t_energia])
            periodecch.feed({
                'fecha_desde': cch_desde,
                'fecha_hasta': cch_hasta,
            })
            datosatr_dict.update({
                'periodocch': periodecch
            })

        datosatr.feed(datosatr_dict)

        datosgrlatr = f1.DatosGeneralesFacturaATR()
        datosgrlatr.feed({
            'direccion': dirsuministro,
            'cliente': cliente,
            'contrato': fac.polissa_id.name,
            'datosgrles': datosgrl,
            'datosatr': datosatr,
        })

        # dades lectures i línies
        lect_potencies = {}
        if (poli.facturacio_potencia == 'max' or fac.potencia > 15):
            fact_max = True
        else:
            fact_max = False
            pot_maximetre = 0
        pots_poli = modcon_obj.get_potencies_dict(cursor, uid, modcon_id)
        # init lect_potencies
        for periode, potencia in pots_poli.items():
            lect_potencies.setdefault(periode, {})
            lect_potencies[periode].update({
                'pot_contract': potencia * 1000,
                'pot_maximetre': 0,
            })
        grouped = poli.tarifa.get_grouped_periods()
        for lpot in fac.lectures_potencia_ids:
            lpot_name = grouped.get(lpot.name, lpot.name)
            if fact_max:
                pot_maximetre = lpot.pot_maximetre * 1000
            lect_potencies[lpot_name]['pot_maximetre'] = max(
                lect_potencies[lpot_name]['pot_maximetre'], pot_maximetre
            )

        term_potencia = {}
        for lpot in fac.linies_potencia:
            key = '%s|%s' % (lpot.data_desde, lpot.data_fins)
            term_potencia.setdefault(key, {})
            term_potencia[key].setdefault(lpot.name, {})
            term_potencia[key][lpot.name].update({
                'precio': lpot.price_unit_multi / 1000,
                'afacturar': lpot.quantity * 1000,
                'contratada': lect_potencies[lpot.name]['pot_contract'],
                'maxdemandada': lect_potencies[lpot.name]['pot_maximetre'],
            })

        t_potencia = []
        for key in sorted(term_potencia):
            tpot = f1.TerminoPotencia()
            ppotes = []
            for per in sorted(term_potencia[key]):
                ppot = f1.PeriodoPotencia()
                ppot.feed(term_potencia[key][per])
                ppotes.append(ppot)
            tp_inici, tp_final = key.split('|')
            tpot.feed({
                'fecha_desde': tp_inici,
                'fecha_hasta': tp_final,
                'periodos': ppotes,
            })
            t_potencia.append(tpot)

        icp = 'N'
        if fac.polissa_id.facturacio_potencia == 'rec':
            icp = 'S'

        potencia = f1.Potencia()
        potencia.feed({
            'icp': icp,
            'importe': fac.total_potencia,
            'termino': t_potencia,
        })

        lin_obj = self.pool.get('giscedata.facturacio.factura.linia')
        l_ids = fac._ff_linies_de('', {'tipus': 'exces_potencia'})[ids]
        lin_exc = lin_obj.browse(cursor, uid, l_ids, context)
        import_exces = 0
        p_exces = []
        for lexc in lin_exc:
            pexc = f1.PeriodoExcesoPotencia()
            pexc.feed({'valor': lexc.quantity})
            import_exces += lexc.price_subtotal
            p_exces.append(pexc)

        exces = False
        if p_exces:
            exces = f1.ExcesoPotencia()
            exces.feed({
                'periodos': p_exces,
                'importe': import_exces
            })

        term_energia = {}
        for lene in fac.linies_energia:
            key = '%s|%s' % (lene.data_desde, lene.data_fins)
            term_energia.setdefault(key, {})
            term_energia[key].setdefault(lene.name, {})
            term_energia[key][lene.name].update({
                'valor': lene.quantity,
                'precio': lene.price_unit,
            })

        t_energia = []
        for key in sorted(term_energia):
            tene = f1.TerminoEnergiaActiva()
            penes = []
            for per in sorted(term_energia[key]):
                pene = f1.PeriodoEnergiaActiva()
                pene.feed(term_energia[key][per])
                penes.append(pene)
            te_inici, te_final = key.split('|')
            tene.feed({
                'fecha_desde': te_inici,
                'fecha_hasta': te_final,
                'periodos': penes,
            })
            t_energia.append(tene)

        energia = False
        if t_energia:
            energia = f1.EnergiaActiva()
            energia.feed({
                'termino': t_energia,
                'importe': fac.total_energia,
            })

        term_reactiva = {}
        for lrea in fac.linies_reactiva:
            key = '%s|%s' % (lrea.data_desde, lrea.data_fins)
            term_reactiva.setdefault(key, {})
            term_reactiva[key].setdefault(lrea.name, {})
            term_reactiva[key][lrea.name].update({
                'valor': lrea.quantity,
                'precio': lrea.price_unit,
            })

        t_reactiva = []
        for key in sorted(term_reactiva):
            trea = f1.TerminoEnergiaReactiva()
            preas = []
            for per in sorted(term_reactiva[key]):
                prea = f1.PeriodoEnergiaReactiva()
                prea.feed(term_reactiva[key][per])
                preas.append(prea)
            tr_inici, tr_final = key.split('|')
            trea.feed({
                'fecha_desde': tr_inici,
                'fecha_hasta': tr_final,
                'periodos': preas,
            })
            t_reactiva.append(trea)

        reactiva = False
        if t_reactiva:
            reactiva = f1.EnergiaReactiva()
            reactiva.feed({
                'termino': t_reactiva,
                'importe': fac.total_reactiva,
            })

        # guardem impostos
        iese = f1.ImpuestoElectrico()
        vals_iese = {}
        iva = []
        tax_obj = self.pool.get('account.tax')
        for tax in fac.tax_line:
            if 'IVA' in tax.name:
                tax_id = tax_obj.search(cursor, uid, [('name', '=', tax.name)],
                                        context={'active_test': False})
                if tax_id:
                    _tax = tax_obj.browse(cursor, uid, tax_id[0])
                else:
                    msg = _(u"No s'ha pogut identificar la taxa %s") % tax.name
                    raise osv.except_osv('Error', msg)
                niva = f1.IVA()
                pcent = _tax.amount * 100
                niva.feed({
                    'base': tax.base_amount,
                    'porcentaje': pcent,
                    'importe': tax.amount,
                })
                # s'ordenen decreixentment pel valor de la taxa
                _pos = len(iva)
                for pos, val in enumerate(iva):
                    if pcent > float(val.porcentaje.get_value()):
                        _pos = pos
                        break
                iva.insert(_pos, niva)
            elif 'Impuesto especial sobre la electricidad' in tax.name:
                vals_iese.update({
                    'base': tax.base_amount,
                    'coef': 1.05113,
                    'percent': 4.864,
                    'importe': tax.amount,
                })
        if not vals_iese:
            vals_iese.update({
                'base': 0,
                'coef': 0,
                'percent': 0,
                'importe': 0,
            })
        iese.feed(vals_iese)

        alquileres = f1.Alquileres()
        alquileres.feed({
            'importe': fac.total_lloguers,
        })

        # medidas
        magnituds = {
            'activa': 'AE',
            'reactiva': 'R1',
        }
        aparatos = []
        for comptador in fac.comptadors:
            aparato = m.Aparato()
            integradors = []
            lect_obj = self.pool.get('giscedata.facturacio.lectures.energia')
            search_params = [('factura_id.id', '=', fac.id),
                             ('comptador_id.id', '=', comptador.id),]
            n_lectures = lect_obj.search_count(cursor, uid, search_params +
                                               [('tipus', '=', 'activa')],
                                               context={'active_test': False})
            cod_dh = codi_dh(fac.tarifa_acces_id.codi_ocsum, n_lectures)
            vals = {
                'tipo': 'CC',
                'marca': 199,
                'numserie': comptador.name,
                'codigodh': cod_dh,
            }

            # date / origin cache to fill PM measures origin
            origin_dict = {}
            for tipus in ('activa', 'reactiva'):
                lectures = lect_obj.search(cursor, uid, search_params +
                                           [('tipus', '=', tipus)],
                                           context={'active_test': False})
                for lene in lect_obj.browse(cursor, uid, lectures):
                    f_desde = m.LecturaDesde()
                    origin_from = (
                        lene.origen_anterior_id and lene.origen_anterior_id.codi
                        or u'30'
                    )
                    origin_dict.setdefault(lene.data_anterior, origin_from)
                    f_desde.feed({
                        'fechahora': lene.data_anterior,
                        'procedencia': origin_from,
                        'lectura': lene.lect_anterior,
                    })
                    f_hasta = m.LecturaHasta()
                    origin_to = (
                        lene.origen_id and lene.origen_id.codi
                        or u'30'
                    )
                    origin_dict.setdefault(lene.data_actual, origin_to)
                    f_hasta.feed({
                        'fechahora': lene.data_actual,
                        'procedencia': origin_to,
                        'lectura': lene.lect_actual,
                    })
                    integrador = m.Integrador()
                    integrador.feed({
                        'magnitud': magnituds[lene.tipus],
                        'codperiodo': codi_periode(cod_dh,
                                                   extreu_periode(lene.name)),
                        'multi': 1.0,
                        'enteras': rodes(comptador.giro),
                        'decimales': 0,
                        'consumo': lene.consum,
                        'desde': f_desde,
                        'hasta': f_hasta,
                    })
                    integradors.append(integrador)
            lpot_obj = self.pool.get('giscedata.facturacio.lectures.potencia')
            lpots = lpot_obj.search(cursor, uid, search_params,
                                    context={'active_test': False})
            for lpot in lpot_obj.browse(cursor, uid, lpots):
                if lpot.exces:
                    mag = 'EP'
                    consum = lpot.exces
                else:
                    if fac.polissa_id.facturacio_potencia == 'icp':
                        continue
                    mag = 'PM'
                    consum = lpot.pot_maximetre
                f_desde = m.LecturaDesde()
                # guessed from AE measures
                origin_from = origin_dict.get(fac.data_inici, u'30')
                f_desde.feed({
                    'fechahora': fac.data_inici,
                    'procedencia': origin_from,
                    'lectura': 0
                })
                f_hasta = m.LecturaHasta()
                origin_to = origin_dict.get(fac.data_final, u'30')
                f_hasta.feed({
                    'fechahora': fac.data_final,
                    'procedencia': origin_to,
                    'lectura': consum,
                })
                integrador = m.Integrador()
                integrador.feed({
                    'magnitud': mag,
                    'codperiodo': codi_periode(cod_dh,
                                               extreu_periode(lpot.name)),
                    'multi': 1.0,
                    'enteras': rodes(comptador.giro),
                    'decimales': 0,
                    'consumo': consum,
                    'desde': f_desde,
                    'hasta': f_hasta,
                })
                integradors.append(integrador)

            vals.update({'integradores': integradors})
            aparato.feed(vals)
            aparatos.append(aparato)

        # Buscar les línies de refacturació
        conceptos = []
        refacturaciones = []
        l_ids = [x.id for x in fac.linia_ids]
        linies = lin_obj.browse(cursor, uid, l_ids, context)
        for linia in linies:
            if codi_reg_refact(linia.product_id.default_code):
                concept = f1.ConceptoIVA()
                concept.feed({
                    'concepto': codi_reg_refact(linia.product_id.default_code),
                    'importe': linia.price_unit_multi,
                })
                conceptos.append(concept)
            elif codi_refact(linia.product_id.default_code):
                concept = f1.ConceptoIVA()
                concept.feed({
                    'concepto': codi_refact(linia.product_id.default_code),
                    'importe': linia.price_unit_multi,
                })
                conceptos.append(concept)
                refact = f1.Refacturacion()
                consum, total = parse_totals_refact(linia.name)
                refact.feed({
                    'tipo': codi_refact(linia.product_id.default_code),
                    'fecha_desde': linia.data_desde,
                    'fecha_hasta': linia.data_fins,
                    'consumo': consum,
                    'importe_total': total
                })
                refacturaciones.append(refact)
            elif linia.tipus == 'altres':
                # Mirem si tenim la relacio entre codis
                # de conceptes f1 i contractacio
                # En cas contrari tot a varios
                concepte_f1 = '14'  # Varios per defecte
                concepte_f1_obj = self.pool.get('facturacio.concepte.f1')
                if concepte_f1_obj is not None:
                    categ_id = linia.product_id.categ_id.id
                    concepte_f1 = concepte_f1_obj.get_concepte(cursor, uid,
                                                               categ_id)
                concept = f1.ConceptoIVA()
                concept.feed({'concepto': concepte_f1,
                              'importe': linia.price_subtotal,
                              'observaciones': linia.name[:100]
                             })
                conceptos.append(concept)

        factura = f1.FacturaATR()
        factura.feed({
            'datosatr': datosgrlatr,
            'potencia': potencia,
            'exceso': exces,
            'energia': energia,
            'reactiva': reactiva or False,
            'iese': iese,
            'alquileres': alquileres,
            'conceptoiva': conceptos,
            'iva': iva,
            'refacturaciones': refacturaciones,
        })
        if aparatos:
            # Only add Medidas child if we have "aparatos"
            medidas = m.Medidas()
            medidas.feed({
                'cups': fac.cups_id.name,
                'aparatos': aparatos,
            })
            factura.feed({'medidas': medidas})

        # Obtenim el número de compte
        banks = fac.company_id.partner_id.bank_ids
        if banks:
            # per defecte, el primer que hi hagi
            bank = banks[0]
            # si en té algun de 'default' el pillem
            for bank_ in banks:
                if bank_.default_bank:
                    bank = bank_
                    break
        else:
            bank = namedtuple('Bank', ['iban'])
            bank.iban = ''

        registrofin = f1.RegistroFin()
        registrofin.feed({
            'importe': fac.amount_total,
            'sfacturacion': fac.residual,
            'scobro': fac.residual,
            'totalrec': 1,
            'tipomoneda': '02',
            'fvalor': fac.date_invoice,
            'flimite': fac.date_due,
            'idremesa': '0',
            'iban': bank.iban
        })

        msg.feed({
            'cabecera': cabecera,
            'facturas': [factura, registrofin],
        })
        msg.build_tree()

        date_invoice = datetime.strptime(fac.date_invoice, '%Y-%m-%d')
        # netejem el número de factura
        fnum = ''.join([x for x in fac.number if x.isalnum()])
        fname = u'F1_%s_%s_%s_%s.xml' % (cursor.dbname,
                                         date_invoice.strftime('%Y%m'),
                                         partner_ref, fnum)
        return (fname, str(msg))

    def generate_header(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        seq_obj = self.pool.get('ir.sequence')

        seq_code = 'giscedata.facturacio.f1'
        codi_sol = seq_obj.get_next(cursor, uid, seq_code)

        codi_r1 = fact.company_id.cups_code

        partner_ref = fact.partner_id.ref or 'P{0}'.format(fact.partner_id.id)

        header = new_f1.Cabecera()
        header.feed(
            {
                'codigo_ree_empresa_emisora': codi_r1,
                'codigo_ree_empresa_destino': partner_ref,
                'codigo_del_proceso': 'F1',
                'codigo_del_paso': '01',
                'codigo_de_solicitud': codi_sol,
                'secuencial_de_solicitud': '01',
                'fecha': fact.date_invoice,
                'cups': fact.cups_id.name,
            }
        )

        return header

    def generate_supply_address(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        cups = fact.cups_id

        municipi = cups.id_municipi

        poblacio = None  # We don't currently have the INE code for this
        # tipo_via = cups.tv.codi
        tipo_via = None  # Since tipo via is not compulsory and we don't have
        # correct codes we don't send it
        duplicador_finca = None
        escalera = cups.es
        piso = cups.pt
        puerta = cups.pu
        tipo_aclarador_finca = None
        aclarador_finca = cups.aclarador

        supply_address = new_f1.DireccionSuministro()

        supply_address.feed(
            {
                'pais': u'España',
                'provincia': municipi.state.code,
                'municipio': municipi.ine,
                'poblacion': poblacio,
                'tipo_via': tipo_via,
                'cod_postal': cups.dp or '00000',
                'calle': cups.nv or 'ND',
                'numero_finca': cups.pnp or 'S/N',
                'duplicador_finca': duplicador_finca,
                'escalera': escalera,
                'piso': piso,
                'puerta': puerta,
                'tipo_aclarador_finca': tipo_aclarador_finca,
                'alcarador_finca': aclarador_finca,
            }
        )

        return supply_address

    def generate_client(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        country_obj = self.pool.get('res.country')

        partner = fact.partner_id
        tipo_identificador = partner.get_vat_type()

        client = new_f1.Cliente()

        vat = partner.vat

        country_code = vat[:2]
        search_par = [('code', '=', country_code)]
        if country_code == 'ES' or country_obj.search(cursor, uid, search_par):
            vat = vat[2:]

        to_feed = {
            'tipo_identificador': tipo_identificador,
            'identificador': vat,
        }

        if tipo_identificador in REQUIRE_PERSON_TYPE:
            tipo_persona = 'F'
            if partner.vat_es_empresa(partner.vat):
                tipo_persona = 'J'

            to_feed.update(
                {
                    'tipo_persona': tipo_persona,
                }
            )

        client.feed(to_feed)

        return client

    def generate_case_file(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        expediente = new_f1.Expediente()
        if fact.tipo_rectificadora == 'C' and fact.tipo_factura == '11':
            other_info = fact.comment
            num_exp = '0000'

            if 'Expedient_frau' in other_info:
                num_exp = other_info.split(': ')[1]

            expediente.feed(
                {
                    'numero_expediente': num_exp,
                    # 'codigo_solicitud': '000000000000'
                    # TODO: It requires a J case, so we have to wait until we have
                    # implemented them
                }
            )

            return expediente
        else:
            return False

    def generate_general_invoice_data(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        cif = fact.company_id.partner_id.vat

        factura_rectificada = fact.ref

        general_invoice_data = new_f1.DatosGeneralesFactura()
        case_file = self.generate_case_file(cursor, uid, fact, context)

        importe_total = fact.amount_total * SIGN[fact.tipo_rectificadora]

        to_feed = {
            'codigo_fiscal_factura': fact.number,
            'tipo_factura': fact.tipo_rectificadora[0],
            # We just get the first letter of tipo_rectificadora to shadow our
            # difference between R and RA (since it was originally R as in CNMC
            # and then they changed it to the way we use RA)
            'motivo_facturacion': fact.tipo_factura,
            'fecha_factura': fact.date_invoice,
            'identificador_emisora': cif[:2] == 'ES' and cif[2:] or cif,
            'comentarios': '. ',
            'importe_total_factura': importe_total,
            'tipo_moneda': '02',
        }

        if fact.tipo_rectificadora in RECTIFYING_RECTIFICATIVE_INVOICE_TYPES:
            to_feed.update({'saldo_factura': fact.saldo})

        if factura_rectificada:
            to_feed.update(
                {
                    'codigo_factura_rectificada_anulada':
                        factura_rectificada.number,
                }
            )
        if case_file:
            to_feed.update({'expediente': case_file})

        general_invoice_data.feed(to_feed)

        return general_invoice_data

    def generate_period(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        data_inici = datetime.strptime(fact.data_inici, '%Y-%m-%d')
        # We use inclusive dates but in the F1 the start date should be
        # exclusive, so we subtract a day.
        # This also makes num_dies calculation correct
        data_inici -= timedelta(days=1)
        data_final = datetime.strptime(fact.data_final, '%Y-%m-%d')
        num_dies = (data_final - data_inici).days

        periodo = new_f1.Periodo()
        periodo.feed({
            'fecha_desde_factura': data_inici.strftime('%Y-%m-%d'),
            'fecha_hasta_factura': data_final.strftime('%Y-%m-%d'),
            'numero_dias': num_dies,
        })

        return periodo

    def generate_cch_period(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        cch_period = new_f1.PeriodoCCH()
        if fact.linies_energia:
            cch_desde = min([linia.data_desde for linia in fact.linies_energia])
            cch_hasta = max([linia.data_fins for linia in fact.linies_energia])
        else:
            cch_desde = fact.data_inici
            cch_hasta = fact.data_final

        data_inici = datetime.strptime(cch_desde, '%Y-%m-%d')
        # We use inclusive dates but in the F1 the start date should be
        # exclusive, so we subtract a day.
        data_inici -= timedelta(days=1)
        data_inici = data_inici.strftime('%Y-%m-%d')

        cch_period.feed(
            {
                'fecha_desde_cch': data_inici,
                'fecha_hasta_cch': cch_hasta,
            }
        )

        return cch_period

    def generate_atr_data(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        ctx = context.copy()
        ctx['date'] = fact.data_final
        polissa = polissa_obj.browse(
            cursor, uid, fact.polissa_id.id, context=ctx
        )
        modcon_id = polissa_obj._get_modcon_date(
            cursor, uid, polissa.id, fact.data_final
        )
        modcon_vals = modcon_obj.read(cursor, uid, modcon_id, ['tg'])

        tg = '{0:02d}'.format(int(modcon_vals.get('tg', 2)))

        power_control_mode = MCP.get(polissa.facturacio_potencia, 1)
        icch = CONV_T109_T111.get(tg, '03')
        # icch value not only depends on polissa_tg
        # value but contracted power
        if icch == '03' and fact.potencia < 15:
            icch = '02'

        medido_en_baja = 'LB' in polissa.tarifa.name
        if medido_en_baja:
            marca_perdidas = 'S'
            porcentaje_perdidas = 4.0
            vas_trafo = (polissa.trafo or 0) * 1000
        else:
            marca_perdidas = 'N'
            porcentaje_perdidas = None
            vas_trafo = None

        atr_data = new_f1.DatosFacturaATR()
        period = self.generate_period(cursor, uid, fact, context)

        atr_vals = {
            'fecha_boe': fact.date_boe,
            'tarifa_atr_fact': fact.tarifa_acces_id.codi_ocsum,
            'modo_control_potencia': power_control_mode,
            'marca_medida_con_perdidas': marca_perdidas,
            'vas_trafo': vas_trafo,
            'porcentaje_perdidas': porcentaje_perdidas,
            'indicativo_curva_carga': icch,
            'periodo': period,
        }

        # We've got CCH
        if icch == '01':
            cch_period = self.generate_cch_period(cursor, uid, fact, context)
            atr_vals.update(
                {
                    'periodo_cch': cch_period
                }
            )

        atr_data.feed(atr_vals)

        return atr_data

    def generate_general_data_atr(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        general_data_atr = new_f1.DatosGeneralesFacturaATR()
        address = self.generate_supply_address(cursor, uid, fact, context)
        client = self.generate_client(cursor, uid, fact, context)
        general_invoice_data = self.generate_general_invoice_data(
            cursor, uid, fact, context
        )
        atr_data = self.generate_atr_data(cursor, uid, fact, context)

        filtered_name = filter(lambda x: x.isdigit(), fact.polissa_id.name)
        general_data_atr.feed(
            {
                'direccion_suministro': address,
                'cliente': client,
                'cod_contrato': filtered_name,
                'datos_generales_factura': general_invoice_data,
                'datos_factura_atr': atr_data,
            }
        )

        return general_data_atr

    def generate_power_term(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')
        poli_obj = self.pool.get('giscedata.polissa')

        if calendar.isleap(int(fact.data_final.split('-')[0])):
            uom_pot_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'uom_pot_elec_wat_dia_traspas'
            )[1]
        else:
            uom_pot_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_wat_dia'
            )[1]

        ctx = context.copy()
        ctx['date'] = fact.data_final

        poli = poli_obj.browse(cursor, uid, fact.polissa_id.id, context=ctx)
        lect_potencies = {}
        if poli.facturacio_potencia == 'max' or  fact.potencia > 15:
            fact_max = True
        else:
            fact_max = False

        # init lect_potencies
        if fact.lectures_potencia_ids:  # Use pots from lectures
            for lectura_potencia in fact.lectures_potencia_ids:
                lect_potencies[lectura_potencia.name] = {
                    'pot_contract': lectura_potencia.pot_contract * 1000,
                    'pot_maximetre': (lectura_potencia.pot_maximetre or
                                      0) * 1000,
                }
        else:
            # Use potencia from contract
            modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
            modcon_id = poli_obj._get_modcon_date(
                cursor, uid, poli.id, fact.data_final
            )
            pots_poli = modcon_obj.get_potencies_dict(cursor, uid, modcon_id)
            for periode, potencia in pots_poli.items():
                lect_potencies[periode] = {
                    'pot_contract': potencia * 1000,
                    'pot_maximetre': 0,
                }

        grouped = fact.tarifa_acces_id.get_grouped_periods()
        for lectures_potencia in fact.lectures_potencia_ids:
            grouped_period = grouped.get(
                lectures_potencia.name, lectures_potencia.name
            )
            if fact_max:
                pot_maximetre = lectures_potencia.pot_maximetre * 1000
                lect_potencies[grouped_period]['pot_maximetre'] = max(
                    lect_potencies[grouped_period]['pot_maximetre'],
                    pot_maximetre
                )

        tariff_periods = fact.tarifa_acces_id.get_periodes_producte(
            tipus='tp'
        ).keys()
        default_periods = {
            period: {
                'potencia_contratada': 0,
                'potencia_max_demandada': 0,
                'potencia_a_facturar': 0,
                'precio_potencia': 0,
            }
            for period in tariff_periods
        }

        term_potencia = {}
        for linia_potencia in fact.linies_potencia:
            key = (linia_potencia.data_desde, linia_potencia.data_fins)
            # Fem el round i el int perque abans no es feia res i ens trobavem amb:
            #
            # pot_cont      ->      1001.0
            # int(pot_cont) ->      1000
            #
            # Internament detectava el 1001.0 com a 1000.9999999... i al fer
            # el casting a enter tallava i quedava 1000
            pot_cont = int(round(lect_potencies[linia_potencia.name]['pot_contract']))
            pot_max = int(round(lect_potencies[linia_potencia.name]['pot_maximetre']))
            pot_fact = int(round(linia_potencia.quantity * 1000))

            # If we don't have a max reading we have to print 0
            # power (according to what the Excel defining the format says)
            pot_max = pot_max or 0

            d_ini = datetime.strptime(linia_potencia.data_desde, '%Y-%m-%d')
            d_ini -= timedelta(days=1)
            d_fi = datetime.strptime(linia_potencia.data_fins, '%Y-%m-%d')
            ndays = (d_fi - d_ini).days

            total_price = (linia_potencia.price_unit_multi * linia_potencia.multi) / 1000
            precio_potencia = total_price / ndays

            term_potencia.setdefault(key, deepcopy(default_periods))
            term_potencia[key][linia_potencia.name].update(
                {
                    'potencia_contratada': pot_cont,
                    'potencia_max_demandada': pot_max,
                    'potencia_a_facturar': pot_fact,
                    'precio_potencia': precio_potencia,
                }
            )

        power_terms = []
        for key in sorted(term_potencia):
            power_term = new_f1.TerminoPotencia()
            periodos_potencia = []
            for per in sorted(term_potencia[key]):
                periodo_potencia = new_f1.PeriodoPotencia()
                periodo_potencia.feed(term_potencia[key][per])
                periodos_potencia.append(periodo_potencia)
            tp_inici, tp_final = key
            data_inici = datetime.strptime(tp_inici, '%Y-%m-%d')
            # We use inclusive dates but in the F1 the start date should be
            # exclusive, so we subtract a day.
            data_inici -= timedelta(days=1)
            power_term.feed(
                {
                    'fecha_desde': data_inici.strftime('%Y-%m-%d'),
                    'fecha_hasta': tp_final,
                    'periodo': periodos_potencia,
                }
            )
            power_terms.append(power_term)

        return power_terms

    def generate_power(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        rec_icp = 'N'
        if fact.polissa_id.facturacio_potencia == 'recarrec':
            rec_icp = 'S'

        power = new_f1.Potencia()
        power_term = self.generate_power_term(cursor, uid, fact, context)

        power.feed(
            {
                'termino_potencia': power_term,
                'penalizacion_no_icp': rec_icp,
                'importe_total_termino_potencia': fact.total_potencia,
            }
        )

        return power

    def generate_power_excess(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        linies_exces = [
            linia
            for linia in fact.linia_ids
            if linia.tipus == 'exces_potencia'
        ]
        import_exces = 0

        tariff_periods = fact.tarifa_acces_id.get_periodes_producte(
            tipus='ep'
        ).keys()

        periodes_exces_potencia = []
        excessos_potencia = {
            period: {
                'valor_exceso_potencia': 0,
            }
            for period in tariff_periods
        }
        for linia_exces in linies_exces:
            excessos_potencia[linia_exces.name].update(
                {'valor_exceso_potencia': linia_exces.price_subtotal}
            )
            import_exces += linia_exces.price_subtotal

        for period in sorted(excessos_potencia):
            periodo_exc_pot = new_f1.PeriodoExcesoPotencia()
            periodo_exc_pot.feed(excessos_potencia[period])
            periodes_exces_potencia.append(periodo_exc_pot)

        exces = False
        if periodes_exces_potencia:
            exces = new_f1.ExcesoPotencia()
            exces.feed({
                'periodo': periodes_exces_potencia,
                'importe_total_excesos': import_exces
            })

        return exces

    def generate_energy_terms(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        tariff_periods = fact.tarifa_acces_id.get_periodes_producte(
            tipus='te'
        ).keys()
        default_periods = {
            period: {
                'valor_energia_activa': 0,
                'precio_energia': 0,
            }
            for period in tariff_periods
        }

        # We calc ennergy term to fill PeriodoCCH if is necessary
        term_energia = {}
        for lene in fact.linies_energia:
            key = (lene.data_desde, lene.data_fins)
            term_energia.setdefault(key, deepcopy(default_periods))
            term_energia[key][lene.name].update({
                'valor_energia_activa': lene.quantity,
                'precio_energia': lene.price_unit,
            })

        terminos_energia = []
        for key in sorted(term_energia):
            termino_eneriga = new_f1.TerminoEnergiaActiva()
            periodos_energia = []
            for per in sorted(term_energia[key]):
                periodo_energia = new_f1.PeriodoEnergiaActiva()
                periodo_energia.feed(term_energia[key][per])
                periodos_energia.append(periodo_energia)
            te_inici, te_final = key
            data_inici = datetime.strptime(te_inici, '%Y-%m-%d')
            # We use inclusive dates but in the F1 the start date should be
            # exclusive, so we subtract a day.
            data_inici -= timedelta(days=1)
            termino_eneriga.feed({
                'fecha_desde': data_inici.strftime('%Y-%m-%d'),
                'fecha_hasta': te_final,
                'periodos': periodos_energia,
            })
            terminos_energia.append(termino_eneriga)

        return terminos_energia

    def generate_active_energy(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        active_energy = False
        energy_terms = self.generate_energy_terms(cursor, uid, fact, context)

        if energy_terms:
            active_energy = new_f1.EnergiaActiva()
            active_energy.feed(
                {
                    'termino_energia_activa': energy_terms,
                    'importe_total_energia_activa': fact.total_energia,
                }
            )

        return active_energy

    def generate_reactive_terms(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        tarifa = fact.tarifa_acces_id

        tariff_periods = tarifa.get_periodes_producte(tipus='tr').keys()
        if not tariff_periods:
            tariff_periods = tarifa.get_periodes_producte(tipus='te').keys()
        if not tariff_periods:
            tariff_periods = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6']
        default_periods = {
            period: {
                'valor_energia_reactiva': 0,
                'precio_energia': 0,
            }
            for period in tariff_periods
        }

        term_reactiva = {}
        for reactive_line in fact.linies_reactiva:
            key = (reactive_line.data_desde, reactive_line.data_fins)
            term_reactiva.setdefault(key, default_periods)
            term_reactiva[key][reactive_line.name].update({
                'valor_energia_reactiva': reactive_line.quantity,
                'precio_energia': reactive_line.price_unit,
            })

        reactive_terms = []
        for key in sorted(term_reactiva):
            react_term = new_f1.TerminoEnergiaReactiva()
            react_periods = []
            for period in sorted(term_reactiva[key]):
                preact_per = new_f1.PeriodoEnergiaReactiva()
                preact_per.feed(term_reactiva[key][period])
                react_periods.append(preact_per)
            tr_inici, tr_final = key
            data_inici = datetime.strptime(tr_inici, '%Y-%m-%d')
            # We use inclusive dates but in the F1 the start date should be
            # exclusive, so we subtract a day.
            data_inici -= timedelta(days=1)
            react_term.feed({
                'fecha_desde': data_inici.strftime('%Y-%m-%d'),
                'fecha_hasta': tr_final,
                'periodos': react_periods,
            })
            reactive_terms.append(react_term)

        return reactive_terms

    def generate_reactive_energy(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        reactive_terms = self.generate_reactive_terms(
            cursor, uid, fact, context
        )

        reactive = False
        if reactive_terms:
            reactive = new_f1.EnergiaReactiva()
            reactive.feed(
                {
                    'termino_energia_reactiva': reactive_terms,
                    'importe_total_energia_reactiva': fact.total_reactiva,
                }
            )

        return reactive

    def generate_electric_tax(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        electric_tax = new_f1.ImpuestoElectrico()
        vals_iese = {}
        for tax in fact.tax_line:
            if 'Impuesto especial sobre la electricidad' in tax.name:
                vals_iese.update({
                    'base_imponible': tax.base_amount,
                    'porcentaje': 5.113,
                    'importe': tax.amount,
                })
        if not vals_iese:
            vals_iese.update({
                'base_imponible': 0,
                'porcentaje': 0,
                'importe': 0,
            })
        electric_tax.feed(vals_iese)

        return electric_tax

    def generate_daily_price_rent(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        daily_price_rents = []

        for rent_line in fact.linies_lloguer:
            daily_price_rent = new_f1.PrecioDiarioAlquiler()

            daily_price_rent.feed(
                {
                    'precio_dia': rent_line.price_unit,
                    'numero_dias': rent_line.quantity,
                }
            )

            daily_price_rents.append(daily_price_rent)

        return daily_price_rents

    def generate_rent(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        rent = new_f1.Alquileres()
        if fact.linies_lloguer:
            daily_price_rent = self.generate_daily_price_rent(
                cursor, uid, fact, context
            )

            rent.feed({
                'precio_diario_alquiler': daily_price_rent,
                'importe_facturacion_alquileres': fact.total_lloguers,
            })

        return rent

    def generate_repercussible_concepts(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        conceptos = []

        # dades lectures i línies
        lin_obj = self.pool.get('giscedata.facturacio.factura.linia')
        extra_line_obj = self.pool.get('giscedata.facturacio.extra')

        l_ids = [x.id for x in fact.linia_ids]
        linies = lin_obj.browse(cursor, uid, l_ids, context)
        for linia in linies:
            if linia.tipus == 'altres':
                # Mirem si tenim la relacio entre codis
                # de conceptes f1 i contractacio
                # En cas contrari tot a varios
                concepte_f1 = '14'  # Varios per defecte
                concepte_f1_obj = self.pool.get('facturacio.concepte.f1')
                if concepte_f1_obj is not None:
                    categ_id = linia.product_id.categ_id.id
                    concepte_f1 = concepte_f1_obj.get_concepte(
                        cursor, uid, categ_id
                    )

                fecha_operacion = None
                fecha_desde = None
                fecha_hasta = None

                if concepte_f1 in CONCEPTOS_CON_FECHA_OPERACION:
                    fecha_operacion = linia.data_desde or fact.date_invoice
                else:
                    fecha_desde = linia.data_desde
                    fecha_hasta = linia.data_fins
                    if not (fecha_desde or fecha_hasta):
                        raise osv.except_osv(
                            'Error',
                            _(
                                'El concepte {0} no te dates d\'inici o final'
                            ).format(linia.name)
                        )

                    date_from = datetime.strptime(fecha_desde, '%Y-%m-%d')
                    date_from = date_from - timedelta(days=1)
                    fecha_desde = date_from.strftime('%Y-%m-%d')

                concept_price = linia.price_unit_multi
                concept_imp = linia.price_subtotal
                concept_units = linia.quantity

                concept_units *= MULTI_CONCEPTS.get(concepte_f1, 1)

                comment = linia.name or ''

                # Suplementos
                if concepte_f1 == '48':
                    polissa_id = fact.polissa_id.id
                    extra_line = extra_line_obj.search(
                        cursor, uid, [
                            ('polissa_id', '=', polissa_id),
                            ('product_id', '=', linia.product_id.id)
                        ]
                    )

                    if extra_line:
                        fields_to_read = [
                            'notes', 'term', 'quantity', 'price_subtotal'
                        ]
                        extra_line_vals = extra_line_obj.read(
                            cursor, uid, extra_line[0], fields_to_read
                        )
                        extra_quantity = extra_line_vals['quantity']
                        extra_term = extra_line_vals['term']
                        concept_units = round(extra_quantity / extra_term, 2)
                        concept_price = extra_line_vals['price_subtotal']
                        if extra_line_vals['notes']:
                            term_actual = extra_line_obj.get_next_term(
                                cursor, uid, id_extraline=extra_line[0],
                                date=linia.factura_id.data_final,
                                fact_id=fact.id
                            )
                            term_actual = str(term_actual).zfill(2)
                            comment = '{}{}{}'.format(
                                '0000', term_actual, extra_line_vals['notes']
                            )
                if concepte_f1 == '49':
                    date_from = datetime.strptime(fecha_desde, '%Y-%m-%d')
                    date_from = date_from + timedelta(days=1)
                    fecha_desde = date_from.strftime('%Y-%m-%d')
                    polissa_id = fact.polissa_id.id
                    extra_line = extra_line_obj.search(
                        cursor, uid, [
                            ('polissa_id', '=', polissa_id),
                            ('product_id', '=', linia.product_id.id),
                            ('price_term', '>', 0.0)
                        ]
                    )

                    if extra_line:
                        fields_to_read = [
                            'notes', 'term', 'quantity', 'price_subtotal'
                        ]
                        extra_line_vals = extra_line_obj.read(
                            cursor, uid, extra_line[0], fields_to_read
                        )
                        extra_quantity = extra_line_vals['quantity']
                        extra_term = extra_line_vals['term']
                        concept_units = round(extra_quantity / extra_term, 2)
                        concept_price = extra_line_vals['price_subtotal']
                        if extra_line_vals['notes']:
                            term_actual = extra_line_obj.get_next_term(
                                cursor, uid, id_extraline=extra_line[0],
                                date=linia.factura_id.data_final,
                                fact_id=fact.id
                            )

                            start = '<JSON>'
                            end = '</JSON>'
                            # This finds substring between JSON brackets
                            json_parsed_from_note_str = (
                                extra_line_vals['notes'][
                                    extra_line_vals['notes'].find(start) + len(start):extra_line_vals['notes'].rfind(end)
                                ]
                            )
                            # Convert string json format to dict
                            json_parsed_from_note = loads(
                                json_parsed_from_note_str
                            )

                            original_nota = json_parsed_from_note[
                                u'comentari_original'
                            ]

                            total_terminos = json_parsed_from_note[
                                u'termes_totals'
                            ]

                            importe_total = json_parsed_from_note[
                                u'import_total'
                            ]

                            is_liquidacio = json_parsed_from_note[
                                u'liquidacio'
                            ]

                            if is_liquidacio:
                                term_actual = total_terminos

                            term_actual = str(term_actual).zfill(2)

                            comment = '{}{}{}'.format(
                                '0000', term_actual, original_nota
                            )

                            json_parsed_from_note[u'comentari_liquidacio'] = comment

                            json_back_to_string_update_notas = dumps(
                                json_parsed_from_note
                            )

                            to_write_note = extra_line_vals['notes'].replace(
                                json_parsed_from_note_str, json_back_to_string_update_notas
                            )

                            if is_liquidacio:
                                terme_que_tocaria_facturar = json_parsed_from_note[
                                    u'terme_a_facturar'
                                ]

                                concept_units = round(
                                    float(
                                        float(terme_que_tocaria_facturar)
                                        / float(total_terminos)
                                    ),
                                    2
                                )
                            else:
                                concept_units = round((1.0 / float(total_terminos)), 2)

                            concept_price = round(importe_total, 2)

                            extra_line_obj.write(
                                cursor, uid, extra_line[0],
                                {'notes': to_write_note}, context=context
                            )

                concept = new_f1.ConceptoRepercutible()
                concept.feed(
                    {
                        'concepto_repercutible': concepte_f1,
                        'tipo_impositivo_concepto_repercutible': 1,
                        'fecha_desde': fecha_desde,
                        'fecha_hasta': fecha_hasta,
                        'fecha_operacion': fecha_operacion,
                        'unidades_concepto_repercutible': concept_units,
                        'precio_unidad_concepto_repercutible': concept_price,
                        'importe_total_concepto_repercutible': concept_imp,
                        'comentarios': comment[:4000],
                    }
                )
                conceptos.append(concept)

        return conceptos

    @staticmethod
    def is_iva_exento(tax):
        '''
        :param tax: tax browse record
        :return: True if iva exento else False
        '''
        return tax.amount == 0 and tax.type == 'percent'

    @staticmethod
    def get_tax_sign(fact, tax):
        sign = tax.base_sign or 1.0
        if fact.type.endswith('refund'):
            sign = tax.ref_base_sign or sign
        return sign

    def generate_iva(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        tax_obj = self.pool.get('account.tax')

        ivas = []
        for tax in fact.tax_line:
            if 'IVA' in tax.name:
                if self.is_iva_exento(tax.tax_id):
                    continue
                tax_id = tax_obj.search(
                    cursor, uid, [('name', '=', tax.name)],
                    context={'active_test': False}
                )
                if tax_id:
                    _tax = tax_obj.browse(cursor, uid, tax_id[0])
                else:
                    msg = _(u"No s'ha pogut identificar la taxa %s") % tax.name
                    raise osv.except_osv('Error', msg)
                pcent = _tax.amount * 100
                sign = self.get_tax_sign(fact, tax.tax_id)
                ivas.append(
                    {
                        'base_imponible': sign * tax.base_amount,
                        'porcentaje': pcent,
                        'importe': tax.amount,
                    }
                )
        if not ivas:
            ivas.append(
                {
                    'base_imponible': 0,
                    'porcentaje': 21,
                    'importe': 0,
                }
            )
        # We sort the ivas in descending order
        ivas = sorted(ivas, key=lambda k: k['porcentaje'], reverse=True)
        res = []
        for iva_vals in ivas:
            iva = new_f1.IVA()

            iva.feed(iva_vals)

            res.append(iva)

        return res

    def generate_reduced_iva(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        ivas = []
        res = []
        for inv_tax in fact.tax_line:
            tax = inv_tax.tax_id
            if 'IVA' in inv_tax.name and self.is_iva_exento(tax):
                sign = self.get_tax_sign(fact, tax)
                base_iva = inv_tax.base
                base_imponible = sign * base_iva
                pcent = tax.amount * 100

                ivas.append(
                    {
                        'base_imponible': base_imponible,
                        'porcentaje': pcent,
                        'importe': inv_tax.tax_amount
                    }
                )
        for iva_vals in ivas:
            iva = new_f1.IVAReducido()

            iva.feed(iva_vals)

            res.append(iva)

        return res
    
    def generate_initial_reading_for_energy(self, cursor, uid, lect,
                                            origin_dict, context=None):
        if context is None:
            context = {}

        initial_reading = new_f1.LecturaDesde()

        origin_from = u'30'
        if lect.origen_anterior_id:
            origin_from = lect.origen_anterior_id.codi

        origin_dict.setdefault(lect.data_anterior, origin_from)
        initial_reading.feed(
            {
                'fecha': lect.data_anterior,
                'procedencia': TRANSLATE_ORIGIN_TO_NEW_F1.get(
                    origin_from, origin_from
                ),
                'lectura': lect.lect_anterior,
            }
        )

        return initial_reading

    def generate_final_reading_for_energy(self, cursor, uid, lect, origin_dict,
                                          context=None):
        if context is None:
            context = {}

        final_reading = new_f1.LecturaHasta()
        ajuste = new_f1.Ajuste()

        origin_to = u'30'
        if lect.origen_id:
            origin_to = lect.origen_id.codi

        origin_dict.setdefault(lect.data_actual, origin_to)
        final_reading.feed(
            {
                'fecha': lect.data_actual,
                'procedencia': TRANSLATE_ORIGIN_TO_NEW_F1.get(
                    origin_to, origin_to
                ),
                'lectura': lect.lect_actual,
            }
        )
        if lect.ajust:
            ajuste.feed(
                {
                    'codigo_motivo_ajuste': lect.motiu_ajust,
                    'ajuste_por_integrador': lect.ajust
                }
            )
        else:
            ajuste = None

        return final_reading, ajuste

    def generate_integrator_from_energy(self, cursor, uid, fact, meter,
                                        lectura_energia, cod_dh, origin_dict,
                                        context=None):
        if context is None:
            context = {}
            
        integrator = new_f1.Integrador()
        initial_reading = self.generate_initial_reading_for_energy(
            cursor, uid, lectura_energia, origin_dict, context
        )
        final_reading, ajuste = self.generate_final_reading_for_energy(
            cursor, uid, lectura_energia, origin_dict, context
        )

        integrator.feed(
            {
                'magnitud': MAGNITUDS[lectura_energia.tipus],
                'codigo_periodo': codi_periode(
                    cod_dh, extreu_periode(lectura_energia.name)
                ),
                'constante_multiplicadora': 1.0,
                'numero_ruedas_enteras': rodes(meter.giro),
                'numero_ruedas_decimales': 0,
                'consumo_calculado': lectura_energia.consum,
                'lectura_desde': initial_reading,
                'lectura_hasta': final_reading,
                'ajuste': ajuste,
                'anomalia': None,
                'fecha_hora_maximetro': None,
            }
        )

        return integrator

    def generate_initial_reading_for_power(self, cursor, uid, fact, meter, lect,
                                           origin_dict, context=None):
        if context is None:
            context = {}

        initial_reading = new_f1.LecturaDesde()

        origin_from = origin_dict.get(fact.data_inici, u'30')

        date = lect.data_anterior
        if not date:
            date = max(fact.data_inici, meter.data_alta)

        initial_reading.feed(
            {
                'fecha': date,
                'procedencia': TRANSLATE_ORIGIN_TO_NEW_F1.get(
                    origin_from, origin_from
                ),
                'lectura': 0,
            }
        )

        return initial_reading

    def generate_final_reading_for_power(self, cursor, uid, fact, meter, lect,
                                         origin_dict, consum, context=None):
        if context is None:
            context = {}

        final_reading = new_f1.LecturaHasta()

        origin_to = origin_dict.get(fact.data_final, u'30')

        fecha = lect.data_actual
        if not fecha:
            fecha = fact.data_final
            if not meter.active and meter.data_baixa:
                fecha = min(fecha, meter.data_baixa)

        final_reading.feed(
            {
                'fecha': fecha,
                'procedencia': TRANSLATE_ORIGIN_TO_NEW_F1.get(
                    origin_to, origin_to
                ),
                'lectura': consum,
            }
        )

        return final_reading

    def generate_integrator_with(self, cursor, uid, fact, meter,
                                 lectura_potencia, cod_dh, consum, mag,
                                 origin_dict, context=None):
        if context is None:
            context = {}

        integrator = new_f1.Integrador()

        initial_reading = self.generate_initial_reading_for_power(
            cursor, uid, fact, meter, lectura_potencia, origin_dict, context
        )
        final_reading = self.generate_final_reading_for_power(
            cursor, uid, fact, meter, lectura_potencia, origin_dict, consum, context
        )

        integrator.feed({
            'magnitud': mag,
            'codigo_periodo': codi_periode(
                cod_dh, extreu_periode(lectura_potencia.name)
            ),
            'constante_multiplicadora': 1.0,
            'numero_ruedas_enteras': rodes(meter.giro),
            'numero_ruedas_decimales': 0,
            'consumo_calculado': consum,
            'lectura_desde': initial_reading,
            'lectura_hasta': final_reading,
            'ajuste': None,
            'anomalia': None,
            'fecha_hora_maximetro': None,
        })

        return integrator

    def generate_integrator_from_power(self, cursor, uid, fact, meter,
                                       lectura_potencia, cod_dh, origin_dict,
                                       context=None):
        if context is None:
            context = {}

        integrators = []

        if lectura_potencia.exces:
            mag = 'EP'
            consum = lectura_potencia.exces

            consum *= 1000

            integrators.append(
                self.generate_integrator_with(
                    cursor, uid, fact, meter, lectura_potencia, cod_dh, consum,
                    mag, origin_dict, context
                )
            )

        with_icp = fact.polissa_id.facturacio_potencia == 'icp'
        ocsum = fact.tarifa_acces_id.codi_ocsum
        non_max_tariff = ocsum not in TARIFES_SEMPRE_MAX
        if with_icp and non_max_tariff:
            return integrators

        mag = 'PM'
        consum = lectura_potencia.pot_maximetre

        # We convert from kW to W
        consum *= 1000

        integrators.append(
            self.generate_integrator_with(
                cursor, uid, fact, meter, lectura_potencia, cod_dh, consum, mag,
                origin_dict, context
            )
        )

        return integrators

    def generate_integrators(self, cursor, uid, fact, meter, cod_dh,
                             context=None):
        if context is None:
            context = {}

        lect_ener_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        lect_pot_obj = self.pool.get('giscedata.facturacio.lectures.potencia')

        search_params = [
            ('factura_id.id', '=', fact.id),
            ('comptador_id.id', '=', meter.id),
        ]

        integrators = []

        # date / origin cache to fill PM measures origin
        origin_dict = {}
        for tipus in ('activa', 'reactiva'):
            lectures = lect_ener_obj.search(
                cursor, uid, search_params + [('tipus', '=', tipus)],
                context={'active_test': False}
            )
            for lect_ene in lect_ener_obj.browse(cursor, uid, lectures):
                integrator = self.generate_integrator_from_energy(
                    cursor, uid, fact, meter, lect_ene, cod_dh, origin_dict,
                    context
                )
                integrators.append(integrator)

        lpots = lect_pot_obj.search(
            cursor, uid, search_params, context={'active_test': False}
        )
        for lect_pot in lect_pot_obj.browse(cursor, uid, lpots):
            pow_integrators = self.generate_integrator_from_power(
                cursor, uid, fact, meter, lect_pot, cod_dh, origin_dict, context
            )
            if pow_integrators:
                integrators += pow_integrators

        return integrators

    def generate_device_model(self, cursor, uid, fact, meter, context=None):
        if context is None:
            context = {}

        lect_obj = self.pool.get('giscedata.facturacio.lectures.energia')

        search_params = [
            ('factura_id.id', '=', fact.id),
            ('comptador_id.id', '=', meter.id),
            ('tipus', '=', 'activa')
        ]
        n_lectures = lect_obj.search_count(
            cursor, uid, search_params, context={'active_test': False}
        )
        # TODO: Revise and Improve this
        cod_dh = codi_dh(fact.tarifa_acces_id.codi_ocsum, n_lectures)

        device = new_f1.ModeloAparato()

        integrators = self.generate_integrators(
            cursor, uid, fact, meter, cod_dh, context
        )

        to_feed = {
            'tipo_aparato': 'CC',
            'marca_aparato': 199,
            'numero_serie': meter.name,
            'tipo_dhedm': cod_dh,
            'integrador': integrators
        }

        device.feed(to_feed)

        return device

    def generate_readings(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        readings = new_f1.Medidas()

        devices = []
        for meter in fact.comptadors:
            devices.append(
                self.generate_device_model(cursor, uid, fact, meter, context)
            )

        if devices:
            cups_name = fact.polissa_id.cups.name
            if len(cups_name) == 20:
                cups_name = cups_name + '0F'

            readings.feed(
                {
                    'cod_pm': cups_name,
                    'modelo_aparato': devices
                }
            )

        return readings

    def generate_atr_invoices(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        atr_invoices = new_f1.FacturaATR()

        general_data_atr = self.generate_general_data_atr(
            cursor, uid, fact, context
        )
        power = self.generate_power(cursor, uid, fact, context)
        power_excess = self.generate_power_excess(cursor, uid, fact, context)
        active_energy = self.generate_active_energy(cursor, uid, fact, context)
        reactive_energy = self.generate_reactive_energy(
            cursor, uid, fact, context
        )
        electric_tax = self.generate_electric_tax(cursor, uid, fact, context)
        rent = self.generate_rent(cursor, uid, fact, context)
        repercussible_concept = self.generate_repercussible_concepts(
            cursor, uid, fact, context
        )
        iva = self.generate_iva(cursor, uid, fact, context)
        reduced_iva = self.generate_reduced_iva(cursor, uid, fact, context)
        readings = self.generate_readings(cursor, uid, fact, context)

        atr_invoices.feed({
            'datos_generales_factura_atr': general_data_atr,
            'potencia': power,
            'exceso_potencia': power_excess,
            'energia_activa': active_energy,
            'energia_reactiva': reactive_energy,
            'impuesto_electrico': electric_tax,
            'alquileres': rent,
            'importe_intereses': None,
            'concepto_repercutible': repercussible_concept,
            'iva': iva,
            'iva_reducido': reduced_iva,
            'medidas': readings,
        })

        return atr_invoices

    def generate_other_invoices(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        # This will be overwritten on inherited methods in order to generate
        # other invoices (by default we don't have them)
        return None

    def generate_end_register(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        # We get the banck account
        banks = fact.company_id.partner_id.bank_ids
        if banks:
            # by default, we get the first one
            bank = banks[0]
            # if there is a bank in default, we get that one
            for current_bank in banks:
                if current_bank.default_bank:
                    bank = current_bank
                    break
        else:
            bank = namedtuple('Bank', ['iban'])
            bank.iban = ''

        multiply_sign = SIGN[fact.tipo_rectificadora]

        end_register = new_f1.RegistroFin()
        end_register.feed(
            {
                'importe_total': fact.amount_total * multiply_sign,
                'saldo_total_facturacion': fact.saldo * multiply_sign,
                'total_recibos': 1,
                'tipo_moneda': '02',
                'fecha_valor': fact.date_invoice,
                'fecha_limite_pago': fact.date_due,
                'iban': bank.iban,
                'id_remesa': '0',
            }
        )

        return end_register

    def generate_invoices(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        invoices = new_f1.Facturas()

        atr_invoices = self.generate_atr_invoices(
            cursor, uid, fact, context
        )
        other_invoices = self.generate_other_invoices(
            cursor, uid, fact, context
        )
        end_register = self.generate_end_register(
            cursor, uid, fact, context
        )
        invoices.feed(
            {
                'factura_atr': atr_invoices,
                'otras_facturas': other_invoices,
                'registro_fin': end_register,
            }
        )

        return invoices

    def generate_other_data_invoice(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        other_data_invoice = new_f1.OtrosDatosFactura()

        # TODO: Fill this (it's actually optional)
        other_data_invoice.feed(
            {
                'sociedad_mercantil_emisora': None,
                'sociedad_mercantil_destino': None,
                'direccion_emisora': None,
                'direccion_destino': None,
            }
        )

        return other_data_invoice

    def generate_signature(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        pass

    def generate_f1(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        f1_file = new_f1.Facturacion()

        header = self.generate_header(cursor, uid, fact, context=context)
        invoices = self.generate_invoices(cursor, uid, fact, context=context)
        other_data_invoice = self.generate_other_data_invoice(
            cursor, uid, fact, context=context
        )
        signature = self.generate_signature(
            cursor, uid, fact, context=context
        )
        f1_file.feed(
            {
                'cabecera': header,
                'facturas': invoices,
                'otros_datos_factura': other_data_invoice,
                'firmar': signature
            }
        )

        return f1_file

    def validate_journal_is_supported(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        return fact.journal_id.code.startswith('ENERGIA')

    def validate_is_exportable(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}
        if fact.tipo_rectificadora in EXPORT_BLACK_LIST:
            error_msg = _(
                u"No està soportada l'exportació de factures del tipus {}"
            ).format(fact.tipo_rectificadora)
            raise osv.except_osv('Error', error_msg)
        if not self.validate_journal_is_supported(cursor, uid, fact, context):
            error_msg = _(
                u"No està soportada l'exportació de factures del tipus {}"
            ).format(fact.journal_id.name)
            raise osv.except_osv('Error', error_msg)
        if not fact.number:
            error_msg = _(u"Cal obrir la factura per poder generar l'XML")
            raise osv.except_osv('Error', error_msg)

    def export_new_f1(self, cursor, uid, ids, context=None):
        """Retorna la factura en format XML F1
        """
        if context is None:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        fact = self.browse(cursor, uid, ids, context)
        if not fact:
            return None

        self.validate_is_exportable(cursor, uid, fact, context)

        f1_file = self.generate_f1(cursor, uid, fact, context)
        f1_file.build_tree()

        date_invoice = datetime.strptime(fact.date_invoice, '%Y-%m-%d')

        partner_ref = fact.partner_id.ref or 'P{}'.format(fact.partner_id.id)

        # netejem el número de factura
        fnum = ''.join([x for x in fact.number if x.isalnum()])
        fname = u'F1_%s_%s_%s_%s.xml' % (cursor.dbname,
                                         date_invoice.strftime('%Y%m'),
                                         partner_ref, fnum)
        return fname, str(f1_file)

    def generar_resum_factures_xls(self, cursor, uid, ids, header, context=None):
        line_obj = self.pool.get('giscedata.facturacio.factura.linia')
        cups_obj = self.pool.get('giscedata.cups.ps')

        output = StringIO.StringIO()
        sheet = Sheet(_('Resum factures'))
        sheet.add_row([_(h).encode('utf8') for h in header])
        for fact_id in ids:
            read_fields = ['date_invoice', 'polissa_id', 'facturacio',
                           'tarifa_acces_id', 'llista_preu',
                           'linies_potencia', 'linies_energia', 'name',
                           'partner_id', 'cups_id',
                           ]

            fact = self.read(cursor, uid, fact_id, read_fields)
            ctx = context.copy()
            ctx.update({'date': fact['date_invoice']})

            cups = cups_obj.read(cursor, uid, fact['cups_id'][0], ['dp'])

            if fact['facturacio'] == 1:
                facturacio = _('Mensual')
            else:
                facturacio = _('Bimensual')
            row = [
                   fact['name'],
                   cups['dp'],
                   facturacio,
                   fact['partner_id'][1],
                   fact['tarifa_acces_id'][1],
                   fact['llista_preu'][1].replace(' (EUR)', '')
            ]

            #            P1 P2 P3 P4 P5 P6
            potencies = [0, 0, 0, 0, 0, 0]
            for line in line_obj.read(cursor, uid, fact['linies_potencia']):
                if (len(line['product_id'][1]) >= 2 and
                    line['product_id'][1][-2] == 'P'):
                    pos = int(line['product_id'][1][-1]) - 1
                    # Only added once
                    if not potencies[pos]:
                        # name Px where x in 1..6
                        potencies[pos] += line['quantity']

            #           P1 P1 P2 P2 P3 P3 P4 P4 P5 P5 P6 P6
            #           €  kw €  kw €  kw €  kw €  kw €  kw
            energies = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            for line in line_obj.read(cursor, uid, fact['linies_energia']):
                if (line['product_id'] and len(line['product_id'][1]) >= 2 and
                        line['product_id'][1][-2] == 'P'):
                    # €
                    #     |---           Px          ---|          |-- € are even
                    pos = (int(line['product_id'][1][-1]) - 1) * 2
                    energies[pos] += line['price_subtotal']
                    # kW
                    pos = (int(line['product_id'][1][-1]) - 1) * 2 + 1
                    energies[pos] += line['quantity']

            row.extend(potencies)
            row.extend(energies)
            sheet.add_row(row)
        sheet.save(output)

        return {'csv': base64.b64encode(output.getvalue())}

    def _trg_totals(self, cursor, uid, ids, context=None):
        """Funció per especificar els IDs a recalcular
        """
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_ids = [a['invoice_id'][0] for a in self.read(cursor, uid, ids,
                                                         ['invoice_id'])]
        return fact_obj.search(cursor, uid, [('invoice_id', 'in', inv_ids)])

    def search_mandate(self, cursor, uid, polissa, context=None):
        result = super(GiscedataFacturacioFactura, self).search_mandate(
            cursor, uid, polissa, context=context
        )
        if not result:
            mandate_obj = self.pool.get('payment.mandate')
            if polissa.bank:
                #Search for mandate
                reference = 'res.partner,%s' % polissa.pagador.id
                search_params = [('reference', '=', reference),
                                 ('debtor_iban', '=', polissa.bank.iban)]
                mandate_ids = mandate_obj.search(
                    cursor, uid, search_params, limit=1
                )
                if mandate_ids:
                    result = mandate_ids[0]
        return result

    def _ff_total_energia(self, cursor, uid, ids, field, arg, context=None):

        res = {}
        for factura in self.browse(cursor, uid, ids):
            res[factura.id] = 0
            for linia in factura.linies_energia:
                res[factura.id] += linia.quantity
            res[factura.id] = int(res[factura.id])
        return res

    _STORE_TOTALS = {'account.invoice.line': (_trg_totals, ['quantity'], 20)}

    _columns = {
        'periode_liquidacio': fields.many2one('giscedata.liquidacio.fpd',
                                              'Periode liquidació CNE',
                                              required=True, readonly=True,
                                              states={'draft':
                                                      [('readonly', False)]}),
        'envio_pendiente': fields.boolean(
            'Pendiente de enviar por email', readonly=True
        ),
        'mailbox': fields.many2one(
            'poweremail.mailbox', 'Últim e-mail enviat', readonly=True
        ),
        'folder': fields.related(
            'mailbox', 'folder', type='selection', selection=[
                ('inbox', 'Inbox'), ('drafts', 'Drafts'), ('outbox', 'Outbox'),
                ('trash', 'Trash'), ('followup', 'Follow Up'),
                ('sent', 'Sent Items'),
            ], readonly=True, string='Directori'
        ),
        'f1_export_date': fields.datetime(
            u'Data exportació F1',
            readonly=True,
            help=u'Data en la que es va exportar la factura actual'
        ),
        'f1_exported': fields.boolean(
            u'F1 exportat',
            readonly=True, help=u'Indica si s\'ha exportat la factura'
        )
    }

    _defaults = {
        'periode_liquidacio': default_periode_liquidacio,
        'envio_pendiente': lambda *a: False,
        'f1_exported': lambda *a: False
    }

    def init(self, cursor):
        """Inicialitzem la classe i preparem la migració.
        """
        fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
        uid = 1
        # Busquem totes les factures que no tenen periode de liquidació
        # i ho assignem segons la data de la factura
        # Busquem les diferents dates per tal de fer un update massiu per
        # periode de liquidacio
        cursor.execute("select distinct i.date_invoice "
                       "from account_invoice i, giscedata_facturacio_factura f "
                       "where f.invoice_id = i.id "
                       "and f.periode_liquidacio is null")
        dates = [a[0] for a in cursor.fetchall()]
        for date in dates:
            search_params = [('inici', '<=', date),
                             ('final', '>=', date)]
            pl_ids = fpd_obj.search(cursor, uid, search_params)
            if not pl_ids:
                continue
            ids = self.search(cursor, uid, [('date_invoice', '=', date)])
            self.write(cursor, uid, ids, {'periode_liquidacio': pl_ids[0]})
        return True

    def copy_data(self, cursor, uid, ids, default=None, context=None):
        if default is None:
            default = {}

        data, x = super(GiscedataFacturacioFactura, self).copy_data(
            cursor, uid, ids, default, context)

        period = self.default_periode_liquidacio(cursor, uid)
        if period:
            data.update({
                'periode_liquidacio': period,
            })
        return data, x

    def invoice_open(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        res = super(GiscedataFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context
        )
        self.write(cursor, uid, ids, {'envio_pendiente': True},
                   context=context)
        for factura in self.browse(cursor, uid, ids, context):
            pl = factura.periode_liquidacio
            if pl and not pl.inici <= factura.date_invoice <= pl.final:
                fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
                date = factura.date_invoice
                search_params = [('inici', '<=', date),
                                 ('final', '>=', date)]
                periode_ids = fpd_obj.search(cursor, uid, search_params)
                if periode_ids:
                    factura.write({'periode_liquidacio': periode_ids[0]})

            if factura.rectificative_type == 'RA' and factura.rectifying_id:
                # Search for a BRA invoice to open and use it to pay the
                # difference
                bra_ids = self.search(cursor, uid, [
                    ('rectificative_type', '=', 'BRA'),
                    ('rectifying_id', '=', factura.rectifying_id.id)
                ])
                # Raise and Exception if is not equal to 1
                if len(bra_ids) == 1:
                    bra = self.browse(cursor, uid, bra_ids[0])
                    pl = bra.periode_liquidacio
                    if pl and not pl.inici <= bra.date_invoice <= pl.final:
                        fpd_obj = self.pool.get('giscedata.liquidacio.fpd')
                        date = bra.date_invoice
                        search_params = [('inici', '<=', date),
                                         ('final', '>=', date)]
                        periode_ids = fpd_obj.search(cursor, uid, search_params)
                        if periode_ids:
                            bra.write(
                                {'periode_liquidacio': periode_ids[0]})
                else:
                    raise Exception('More than one BRA, not supported')
        return res


GiscedataFacturacioFactura()
