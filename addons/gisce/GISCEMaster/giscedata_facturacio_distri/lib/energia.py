import calendar
from datetime import date, datetime, timedelta
import json
import re
from operator import attrgetter


PERIODES = ['P%s' % p for p in range(0, 7)]


def extreure_periode(str):
    res = re.findall('P[0-7]{1}', str)
    if res and res[0] in PERIODES:
        return res[0]
    else:
        return None


class GapsException(Exception):
    pass


class InersectionException(Exception):
    pass


def check_gaps_dates(items, value_data_inici='data_inici',
                     value_data_final='data_final'):
    data_ant = None
    sorted_items = []
    for item in sorted(items, key=attrgetter(value_data_inici)):
        data_inici = getattr(item, value_data_inici)
        data_final = getattr(item, value_data_final)
        if data_ant is not None and data_inici is not None:
            days = (data_inici - data_ant).days
            if days > 1:
                raise GapsException
            if days < 0:
                raise InersectionException
            if days == 0:
                li_data_final = getattr(sorted_items[-1], value_data_final)
                setattr(sorted_items[-1], value_data_final,
                        li_data_final - timedelta(days=1))
        if data_inici is None and sorted_items:
            li_data_final = getattr(sorted_items[-1], value_data_final)
            setattr(item, value_data_inici, li_data_final + timedelta(days=1))
        data_ant = data_final
        sorted_items.append(item)
    return sorted_items


class Normalizer(object):
    def __init__(self, items, value_attr):
        assert isinstance(items, list)
        self.items = check_gaps_dates(items)
        self.value_attr = value_attr
        self.value_data_inici = 'data_inici'
        self.value_data_final = 'data_final'

    def normalize(self, decimals=0):
        norm = []
        for item in self.items:
            data_inici = getattr(item, self.value_data_inici)
            if not isinstance(data_inici, datetime):
                data_inici = datetime.strptime(data_inici, '%Y-%m-%d')

            data_final = getattr(item, self.value_data_final)
            if not isinstance(data_final, datetime):
                data_final = datetime.strptime(data_final, '%Y-%m-%d')

            number = (data_final - data_inici).days + 1
            if (data_final < data_inici):
                number = 1
            med = (getattr(item, self.value_attr) * 1.0) / number
            d = data_inici
            delta = timedelta(days=1)
            total = 0
            drag = 0
            while d <= data_final:
                x = med + drag
                if decimals:
                    aprox = round(x, decimals)
                else:
                    aprox = int(round(x))
                drag = x - aprox
                norm.append([d, aprox])
                d += delta
                total += aprox
        return norm

    def normalize_month(self, decimals=0):
        normalized_res = self.normalize(decimals=decimals)
        res = {}
        for day, quantity in normalized_res:
            key = day.strftime('%Y%m')
            res[key] = res.get(key, [day.year, day.month, 0])
            res[key][2] += quantity
        # round result
        if decimals:
            for key, val in res.items():
                res[key][2] = round(val[2], decimals)
        return res


def date_handler(o):
        return str(o) if isinstance(o, (date, datetime)) else o


class DictSerializer(object):

    def as_dict(self):
        return json.loads(json.dumps(vars(self), default=date_handler))


class CarteraComercial(DictSerializer):

    def __init__(self, dt, cups, previst, facturat):
        self.datetime = dt,
        self.cups = cups
        self.previst = previst
        self.facturat = facturat


class Factura(DictSerializer):
    def __init__(self, cups, data_inici, data_final, total):
        self.cups = cups
        self.data_inici = data_inici
        self.data_final = data_final
        self.total = total


class Consum(DictSerializer):
    def __init__(self, cups, periode, data_inici=None, data_final=None,
                 consum=0):
        if data_inici:
            assert isinstance(data_inici, date)
        if data_final:
            assert isinstance(data_final, date)
        assert isinstance(consum, (int, float))
        assert periode in PERIODES
        self.cups = cups
        self.periode = periode
        self.data_inici = data_inici
        self.data_final = data_final
        self.consum = consum

    @classmethod
    def totalizer(cls, consums):
        totals = []
        for consum in sorted(consums, key=attrgetter('data_inici')):
            inserted = False
            for total in totals:
                if total[0] <= consum.data_inici and consum.data_final <= total[1]:
                    total[2].append(consum)
                    inserted = True
                    break
            if not inserted:
                totals.append((consum.data_inici, consum.data_final, [consum]))
        for idx, total in enumerate(totals):
            totals[idx] = Consum(
                total[2][0].cups, 'P0', total[0], total[1],
                sum(c.consum for c in total[2])
            )
        return totals


class ConsumPrevist(Consum):

    @classmethod
    def from_sips(cls, list_of_vals):
        for value in list_of_vals:
            value['data_inici'] = datetime.strptime(value['data_inici'], '%Y-%m-%d').date()
            value['data_final'] = datetime.strptime(value['data_final'], '%Y-%m-%d').date()
            c = cls(**value)
            return c

    def reproduce(self, years):
        vals = self.as_dict()
        for d in ('data_inici', 'data_final'):
            self_date = getattr(self, d)
            year = self_date.year + years
            day = self_date.day
            if calendar.monthrange(self_date.year, self_date.month)[1] == self_date.day:
                day = calendar.monthrange(year, self_date.month)[1]
            data_c = date(year, self_date.month, day)
            vals[d] = data_c
        return ConsumPrevist(**vals)


class ConsumFacturat(Consum):
    pass


