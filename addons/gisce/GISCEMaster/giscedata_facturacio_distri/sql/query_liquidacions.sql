SELECT
    f.id
FROM giscedata_facturacio_factura f
INNER JOIN account_invoice i
ON i.id = f.invoice_id
INNER JOIN account_journal journal
ON journal.id = i.journal_id
INNER JOIN giscedata_liquidacio_fpd fpd
ON fpd.id = f.periode_liquidacio
WHERE
    fpd.id = %s
    and journal.code ilike 'ENERGIA%%'
    and i.state in ('open', 'paid')
    and f.tipo_factura = '01'
ORDER BY i.date_invoice, f.id DESC
