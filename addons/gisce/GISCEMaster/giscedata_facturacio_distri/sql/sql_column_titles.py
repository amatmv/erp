# -*- coding: utf-8 -*-

from tools.translate import _

QUERY_RESUM_DISTRI_TITLES = [
    _(u"Distribuidora"),
    _(u"Ref. Distribuidora"),
    _(u"Comercialitzadora"),
    _(u"Cod. Comer."),
    _(u'Pòlissa'),
    _(u'Titular'),
    _(u"CUPS"),
    _(u"Direcció"),
    _(u"Tarifa d'accés"),
    _(u"Factura"),
    _(u"Data factura"),
    _(u"tipus factura"),
    _(u"Data inicial"),
    _(u"Data final"),
    _(u"núm. díes"),
    _(u"vat"),
    _(u"energia_kw"),
    _(u"potencia"),
    _(u"energia"),
    _(u"reactiva"),
    _(u"excesos"),
    _(u"altres conceptes"),
    _(u"refacturacio"),
    _(u"refacturacio_t4"),
    _(u"refacturacio_t1"),
    _(u"alquiler"),
    _(u"fiança"),
    _(u"conceptes sense impostos"),
    _(u"total conceptes"),
    _(u"iva1"),
    _(u"Base IVA1"),
    _(u"total iva1"),
    _(u"iva2"),
    _(u"Base IVA2"),
    _(u"total iva2"),
    _(u"base"),
    _(u"iva"),
    _(u"total"),
]

RESUM_TRAMS_POT_I_MUN_TITLES = [
    _(u"Municipi"),
    _(u"Tarifa"),
    _(u"Trams de potència"),
    _(u"Pòlisses actives a %s"),
    _(u"Estimació de la potència (pòlisses actives "
      u"* promig potència facturada) kW"),
    _(u"Energia kWh")
]


def add_fields_query_resum(existing_field, fields_to_add):
    """
    Insert the list of fields: *fields_to_add* after the field *field*
    in the columns.
    """
    global QUERY_RESUM_DISTRI_TITLES
    pos = QUERY_RESUM_DISTRI_TITLES.index(existing_field) + 1
    for field_name in reversed(fields_to_add):
        QUERY_RESUM_DISTRI_TITLES.insert(pos, field_name)
