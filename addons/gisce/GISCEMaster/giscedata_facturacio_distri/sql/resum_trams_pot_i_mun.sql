select
    m.name as municipi,
    t.name as tarifa,
    case
        when f.potencia between 0 and 3 then '0 - 3'
        when f.potencia between 3.1 and 5 then '> 3 - 5'
        when f.potencia between 5.1 and 7.5 then '> 5 - 7.5'
        when f.potencia between 7.6 and 10 then '> 7 - 10'
        when f.potencia between 10.1 and 15 then '> 10 - 15'
        when f.potencia between 15.1 and 50 then '> 15 - 50'
        else '> 50' end as rang,
    count(distinct(polisses_actives.id)) as polisses,
    round((sum(f.potencia) / count(*)) * count(distinct(polisses_actives.id)), 6) as potencia_kW,
    sum(ene.energia) - coalesce(sum(anuladora.energia), 0) as energia_kWh
from giscedata_facturacio_factura f
left join giscedata_cups_ps c on f.cups_id = c.id
left join res_municipi m on m.id = c.id_municipi
left join (
    select lf.factura_id as factura_id, sum(li.quantity) as energia
    from giscedata_facturacio_factura_linia lf
    left join account_invoice_line li on li.id = lf.invoice_line_id
    left join giscedata_facturacio_factura fact on fact.id = lf.factura_id
    where lf.tipus = 'energia' and fact.tipo_rectificadora in ('N', 'R', 'RA')
    group by lf.factura_id) as ene on ene.factura_id = f.id
left join (
    select lf.factura_id as factura_id, sum(li.quantity) as energia
    from giscedata_facturacio_factura_linia lf
    left join account_invoice_line li on li.id = lf.invoice_line_id
    left join giscedata_facturacio_factura fact on fact.id = lf.factura_id
    where lf.tipus = 'energia' and fact.tipo_rectificadora in ('A', 'B', 'BRA')
    group by lf.factura_id) as anuladora on anuladora.factura_id = f.id
left join giscedata_polissa_tarifa t on t.id = f.tarifa_acces_id
left join account_invoice ai on ai.id = f.invoice_id
left join (
    select p.id, p.data_alta, p.data_baixa
    from giscedata_polissa p
    where p.data_alta <= '%s' and (p.data_baixa is Null or p.data_baixa > '%s'))
    as polisses_actives on polisses_actives.id = f.polissa_id
where ai.date_invoice > '%s' and ai.date_invoice < '%s'
group by rang, t.name, m.name
order by m.name, t.name, rang;
