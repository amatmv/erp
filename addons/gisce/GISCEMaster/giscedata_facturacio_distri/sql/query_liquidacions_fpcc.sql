SELECT
    date_part('year', i.date_invoice)::integer any_facturacio,
    date_part('month', i.date_invoice)::integer mes_facturacio,
    tliq.id id_codigo_tarifa, tliq.codi codigo_tarifa,
    li.quantity quantity , li.price_subtotal price_subtotal,
    lf.tipus tipus, i.date_invoice date_invoice, lf.data_desde data_inici, lf.data_fins data_final,
    pt.name periode,f.periode_liquidacio periode_liquidacio,
    i.type invoice_type, i.rectificative_type rectificative_type,
    f.id f_id, i.number factura, f.polissa_id polissa_id, t.name tarifa_name
FROM giscedata_facturacio_factura_linia lf
INNER JOIN account_invoice_line li ON li.id=lf.invoice_line_id
INNER JOIN product_product p ON p.id= li.product_id
INNER JOIN product_template pt ON pt.id= p.product_tmpl_id
INNER JOIN giscedata_facturacio_factura f ON lf.factura_id=f.id
INNER JOIN giscedata_polissa_tarifa t ON t.id=f.tarifa_acces_id
INNER JOIN giscedata_liquidacio_tarifes tliq ON tliq.id=t.tarifa_liquidacio
INNER JOIN account_invoice i ON i.id = f.invoice_id
INNER JOIN account_journal journal ON journal.id = i.journal_id
INNER JOIN giscedata_liquidacio_fpd fpd ON fpd.id = f.periode_liquidacio
WHERE
    fpd.year = %s
    and journal.code ilike 'ENERGIA%%'
    and f.tipo_factura = '01'
    and i.state in ('open', 'paid')
    and lf.tipus in ('energia', 'potencia','reactiva', 'exces_potencia')
ORDER BY any_facturacio, mes_facturacio, codigo_tarifa, periode
