from expects import expect, equal
from destral import testing
from destral.transaction import Transaction
from giscedata_polissa.tests import activar_polissa
from osv.orm import ValidateException
from osv.osv import except_osv


class TestInvoicing(testing.OOTestCase):

    def test_only_re_tariffs_can_assing_12_facturacio(self):
        """Only can assign facturacio=12 in polisses with RE
        """

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            def write_facturacio_12():
                pol_obj.write(cursor, uid, [pol_id], {'facturacio': 12})

            self.assertRaises(ValidateException, write_facturacio_12)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            re_tariff = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa_distri', 'tarifa_re'
            )[1]

            pol_obj.write(cursor, uid, [pol_id], {'tarifa': re_tariff})

            self.assertTrue(
                pol_obj.write(cursor, uid, [pol_id], {'facturacio': 12})
            )

    def test_fraud_invoice_success(self):
        pool = self.openerp.pool
        wiz_obj = pool.get('wizard.manual.invoice')
        journal_obj = pool.get('account.journal')
        factura_obj = pool.get('giscedata.facturacio.factura')
        pl_vers_obj = pool.get('product.pricelist.version')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            journal_id = journal_obj.search(cursor, uid, [('code', '=', 'ENERGIA')])
            pl_vers_obj.write(cursor, uid, 4, {'pricelist_id': 4, 'name': 'BOE - 01/01/2016'})
            activar_polissa(pool, cursor, uid, 1)
            vals = {
                'polissa_id': 1,
                'is_factura_frau': 1,
                'journal_id': journal_id,
                'date_start': '2016-01-01',
                'date_end': '2016-12-31',
                'codi_expedient': '1234',
                'account': 1
            }

            ctx = {}
            before_ids = factura_obj.search(cursor, uid, [('polissa_id', '=', 1)])
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz_obj.action_manual_invoice(cursor, uid, [wiz_id])
            after_ids = factura_obj.search(cursor, uid, [('polissa_id', '=', 1)])
            new_ids = list(set(after_ids) - set(before_ids))
            factura_data = factura_obj.read(cursor, uid, new_ids[0], ['tipo_factura', 'tipo_rectificadora',
                                                                      'comment', 'total_rebuts', 'account_id'])
            expect(len(new_ids)).to(equal(1))
            expect(factura_data['tipo_factura']).to(equal('11'))
            expect(factura_data['tipo_rectificadora']).to(equal('C'))
            expect(factura_data['comment']).to(equal('Expedient_frau: 1234'))
            expect(factura_data['account_id'][0]).to(equal(1))

    def test_fraud_invoice_not_possible_when_ht(self):
        pool = self.openerp.pool
        wiz_obj = pool.get('wizard.manual.invoice')
        journal_obj = pool.get('account.journal')
        polissa_obj = pool.get('giscedata.polissa')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            journal_id = journal_obj.search(cursor, uid, [('code', '=', 'ENERGIA')])
            polissa_obj.write(cursor, uid, 1, {'tarifa': 8})

            vals = {
                'polissa_id': 1,
                'is_factura_frau': 1,
                'journal_id': journal_id,
                'date_start': '2016-01-01',
                'date_end': '2016-12-31',
                'codi_expedient': '1234',
                'account': 1
            }

            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)

            def toggle_is_factura_frau():
                wiz_obj.onchange_is_factura_frau(cursor, uid, [wiz_id], 1, 1)

            self.assertRaises(except_osv, toggle_is_factura_frau)
