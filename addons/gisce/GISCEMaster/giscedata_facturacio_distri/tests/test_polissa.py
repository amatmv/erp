# -*- coding: utf-8 -*-
import collections

from destral.transaction import Transaction

from giscedata_polissa.tests import utils as utils_polissa
from giscedata_facturacio.tests.tests_polissa_facturacio import TestGetModcontractualIntervals


class TestGetModcontractualIntervalsDistri(TestGetModcontractualIntervals):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.polissa_id = polissa_id
        polissa_obj.write(
            cursor, uid, [self.polissa_id], {'comercialitzadora': 1})
        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def tearDown(self):
        self.txn.stop()

    def test_get_intervals_change_fields(self):
        super(
            TestGetModcontractualIntervalsDistri, self
        ).test_get_intervals_change_fields()
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool
        Change = collections.namedtuple(
            'Change', ['field', 'value', 'start_date', 'end_date']
        )
        # Creem la segona modificació contractual fent un canvi de potencia
        mod_changes = [
            Change('comercialitzadora', 2, '2016-11-02', '2017-12-01')
        ]
        polissa_obj = pool.get('giscedata.polissa')
        for change in mod_changes:
            utils_polissa.crear_modcon(
                pool, cursor, uid, self.polissa_id,
                {change.field: change.value}, change.start_date, change.end_date
            )
            changes = polissa_obj.get_modcontractual_intervals(
                cursor, uid, self.polissa_id,
                '{}-01'.format(change.start_date[:-3]),
                '{}-10'.format(change.end_date[:-3])
            )

            self.assertIn(change.start_date, changes)
            self.assertIn(change.field, changes[change.start_date]['changes'])

    def test_change_titular_generate_two_invoices(self):
        cursor = self.txn.cursor
        uid = self.txn.user
        pool = self.openerp.pool

        polissa_obj = pool.get('giscedata.polissa')
        partner_obj = pool.get('res.partner')
        fact_obj = pool.get('giscedata.facturacio.factura')

        titular_id = polissa_obj.read(
            cursor, uid, self.polissa_id, ['titular']
        )['titular'][0]

        from random import choice
        rand_partner_id = choice(
            partner_obj.search(cursor, uid, [('id', '!=', titular_id)])
        )

        mod_dates = ('2017-08-14', '2017-12-01')
        utils_polissa.crear_modcon(
            pool, cursor, uid, self.polissa_id,
            {'titular': rand_partner_id}, mod_dates[0], mod_dates[1]
        )
        changes = polissa_obj.get_modcontractual_intervals(
            cursor, uid, self.polissa_id,
            '{}-01'.format(mod_dates[0][:-3]),
            '{}-10'.format(mod_dates[0][:-3])
        )

        expected_key = '{}-01'.format(mod_dates[0][:-3])

        self.assertIn(
            expected_key, changes.keys(), 'Error, modcon not generated'
        )
