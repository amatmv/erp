# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from addons.giscedata_liquidacio.wizard.crear_fpd import *


class TestWizardLiquidacioFromInvoice(testing.OOTestCase):

    def test_wizard_creation(self):
        wiz_obj = self.openerp.pool.get('wizard.generar.liquidacions')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            wiz_id = wiz_obj.create(cursor, uid, {})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            expect(wiz.fitxer_liquidacio).to(be('fpd'))

