# coding=utf-8
from destral import testing
from destral.transaction import Transaction
from expects import *


class TestsLiquidacio(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def generate_periodes_fpd(self, year, start_day):
        # Asignamos el valor de inicio de las liquidaciones el dia 1
        from giscedata_liquidacio.wizard.crear_fpd import _generar
        cfg_obj = self.openerp.pool.get('res.config')
        user_obj = self.openerp.pool.get('res.users')

        cfg_obj.set(self.cursor, self.uid, 'liq_fpd_start_day', str(start_day))

        user = user_obj.browse(self.cursor, self.uid, self.uid)

        data = {
            'form': {
                'company_id': user.company_id.id,
                'year': year
            },
            'contador': 0
        }
        _generar(None, self.cursor, self.uid, data)
        return True

    def test_invoice_open_should_move_periode_liquidacio(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fpd_obj = self.openerp.pool.get('giscedata.liquidacio.fpd')

        factura_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        factura = factura_obj.browse(self.cursor, self.uid, factura_id)

        year = int(factura.date_invoice.split('-')[0])

        self.generate_periodes_fpd(year, 1)

        fpd_id = fpd_obj.search(
            self.cursor, self.uid, [], limit=1, order='inici asc'
        )[0]
        factura.write({'periode_liquidacio': fpd_id})


        fpd_id_correcte = fpd_obj.search(self.cursor, self.uid, [
            ('inici', '<=', factura.date_invoice),
            ('final', '>=', factura.date_invoice)
        ], limit=1, order='inici asc'
        )[0]
        factura.invoice_open()

        factura = factura_obj.browse(self.cursor, self.uid, factura_id)
        expect(factura.periode_liquidacio.id).to(equal(fpd_id_correcte))
