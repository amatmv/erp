# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta
from destral.patch import PatchNewCursors
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from gestionatr.utils import validate_xml
from osv import osv
from base64 import b64decode
import unittest


def assertXmlEqual(self, got, want):
    from lxml.doctestcompare import LXMLOutputChecker
    from doctest import Example

    checker = LXMLOutputChecker()
    if checker.check_output(want, got, 0):
        return
    message = checker.output_difference(Example("", want), got, 0)
    raise AssertionError(message)

testing.OOTestCase.assertXmlEqual = assertXmlEqual
testing.OOTestCase.__str__ = testing.OOTestCase.id


class TestsExportF1(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        newest_name = None
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscedata_tarifas_peajes_20160101'),
                    ('state', '!=', 'installed'),
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestsExportF1, self).setUp()

    def activar_polissa(self, txn, polissa_ref='polissa_0001'):
        cursor = txn.cursor
        uid = txn.user
        today = datetime.now()
        year = today.year
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', polissa_ref
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        final_date = datetime.strptime(
            polissa.modcontractual_activa.data_final, '%Y-%m-%d'
        )
        current_year = final_date.year
        deltayears = (year - current_year)
        if deltayears >= 0:
            final_date = final_date + relativedelta(years=deltayears)
            if final_date < today:
                final_date = final_date + relativedelta(years=1)
            if final_date.year == today.year and final_date.month == today.month:
                final_date = final_date + relativedelta(years=1)
            polissa.modcontractual_activa.write(
                {'data_final': final_date.strftime('%Y-%m-%d')}
            )

    def add_taxes_to_periods(self, txn):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.openerp.pool.get('giscedata.polissa.tarifa.periodes')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        tax_ids = tax_obj.search(
            cursor, uid, [
                ('name', '=', 'IVA 21%'),
            ]
        )

        product_ids = []
        for tarifa_id in tarifa_obj.search(cursor, uid, []):
            periode_ids = tarifa_obj.read(
                cursor, uid, tarifa_id, ['periodes']
            )['periodes']
            for periode_vals in periode_obj.read(cursor, uid, periode_ids):
                product_ids.append(periode_vals['product_id'][0])
                if periode_vals['product_reactiva_id']:
                    product_ids.append(periode_vals['product_reactiva_id'][0])
                if periode_vals['product_exces_pot_id']:
                    product_ids.append(periode_vals['product_exces_pot_id'][0])

        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, tax_ids)]
            }
        )

    def add_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA {}%'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def set_reduced_tax_to_invoice_lines(self, txn):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA 0% Exportaciones'

        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        for linia_id in linia_obj.search(cursor, uid, []):
            linia_vals = linia_obj.read(
                cursor, uid, linia_id, ['product_id', 'tipus']
            )
            if linia_vals.get('product_id', False):
                product_vals = product_obj.read(
                    cursor, uid, linia_vals['product_id'][0], ['taxes_id']
                )
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, product_vals['taxes_id'])
                        ]
                    }
                )
            elif linia_vals.get('tipus', '') == 'lloguer':
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, iva_id)
                        ]
                    }
                )

    def set_taxes_to_invoice_lines(self, txn):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA 21%'
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        for linia_id in linia_obj.search(cursor, uid, []):
            linia_vals = linia_obj.read(
                cursor, uid, linia_id, ['product_id', 'tipus']
            )
            if linia_vals.get('product_id', False):
                product_vals = product_obj.read(
                    cursor, uid, linia_vals['product_id'][0], ['taxes_id']
                )
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, product_vals['taxes_id'])
                        ]
                    }
                )
            elif linia_vals.get('tipus', '') == 'lloguer':
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, iva_id)
                        ]
                    }
                )

    def test_exportation_returns_correct_xml(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_no_rent_exportation_returns_correct_xml(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'no_rent_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            fact = factura_obj.browse(cursor, uid, fact_id)

            for rent_line in fact.linies_lloguer:
                rent_line.unlink()

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            invoice_id = fact.invoice_id.id
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_exportation_removes_non_numbers_from_polissa_number(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id, {'name': '0A0B0C1/'})

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_generate_iva_no_lines_with_iva(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]

            factura_obj.write(cursor, uid, fact_id, {'partner_id': partner_id})

            fact = factura_obj.browse(cursor, uid, fact_id)
            fact.partner_id.write({'ref': '0002'})

            factura_obj.write(cursor, uid, [fact_id], {
                'date_invoice': '2017-08-31',
                'data_inici': '2017-08-01',
                'data_final': '2017-08-31'
            })

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            xml_iva_tag = '<IVA><BaseImponible>{0}</BaseImponible>' \
                          '<Porcentaje>{1}</Porcentaje>' \
                          '<Importe>{2}</Importe></IVA>'
            expected_xml_tag = xml_iva_tag.format(
                "{0:.2f}".format(0), "{0:.2f}".format(21), "{0:.2f}".format(0)
            )

            self.assertIn(expected_xml_tag, generated_f1)

    def test_exportation_no_medidas_xml(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'no_medidas_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            # Delte all lectures_energia_ids
            for line in fact.lectures_energia_ids:
                line.unlink()

            factura_obj.write(cursor, uid, fact_id, {
                'lectures_energia_ids': [(6, 0, [])]
            })

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
                ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)
            validation = validate_xml(generated_f1)
            self.assertTrue(validation.valid)

    def test_exportation_diff_info_invoice_and_contract(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        facturador_obj = self.openerp.pool.get(
            'giscedata.facturacio.facturador'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'f1_lect_30A.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
                ]
            iban_banks[0].write({'default_bank': True})

            # modify tariff of invoice. The generation of F1 must use invoice
            # info instead of contract info, if it uses contract info test will
            # fail because it will find a diferent tarif and periods. If it uses
            # invoice info it will generate the expected F1
            tarifa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_30A'
            )[1]
            fact.lectures_energia_ids[0].unlink()
            fact.lectures_potencia_ids[0].unlink()
            fact.write({
                "tarifa_acces_id": tarifa_id,
                "lectures_energia_ids": [(6, 0, [])],
                "lectures_potencia_ids": [(6, 0, [])]
            })

            (comptador_name, comptador_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )

            l1 = {
                'periode': (1, 'P1'),
                'lectura': 10,
                'consum': 5,
                'ajust': 5,
                'name': '2016-02-29',
                'comptador': (comptador_id, comptador_name),
                'tipus': 'A'
            }

            l2 = l1.copy()
            l2.update({'periode': (2, 'P2')})
            l3 = l1.copy()
            l3.update({'periode': (3, 'P3')})
            l4 = l1.copy()
            l4.update({'periode': (4, 'P4')})
            l5 = l1.copy()
            l5.update({'periode': (5, 'P5')})
            l6 = l1.copy()
            l6.update({'periode': (6, 'P6')})

            lect_ant = {
                'lectura': 5,
                'name': '2016-01-31',
            }

            facturador_obj.crear_lectures_energia(
                cursor, uid, fact_id, {
                    1: {'actual': l1, 'anterior': lect_ant},
                    2: {'actual': l2, 'anterior': lect_ant},
                    3: {'actual': l3, 'anterior': lect_ant},
                    4: {'actual': l4, 'anterior': lect_ant},
                    5: {'actual': l5, 'anterior': lect_ant},
                    6: {'actual': l6, 'anterior': lect_ant}
                }
            )

            p1 = {
                'name': 'P1',
                'pot_contract': 17000,
                'pot_maximetre': 0,
                'exces': 0,
                'comptador_id': comptador_id,
                'factura_id': fact_id,
                'data_actual': '2016-02-29',
                'data_anterior': '2016-01-31'
            }
            p2 = p1.copy()
            p2.update({'name': 'P2'})
            p3 = p1.copy()
            p3.update({'name': 'P3'})
            p4 = p1.copy()
            p4.update({'name': 'P4'})
            p5 = p1.copy()
            p5.update({'name': 'P5'})
            p6 = p1.copy()
            p6.update({'name': 'P6'})

            facturador_obj.crear_lectures_potencia(
                cursor, uid, fact_id, {
                    'P1': p1, 'P2': p2, 'P3': p3, 'P4': p4, 'P5': p5, 'P6': p6
                }
            )

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_exportation_with_ajuste(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'ajuste_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                polissa_obj = self.openerp.pool.get('giscedata.polissa')
                comp_obj = self.openerp.pool.get('giscedata.lectures.comptador')
                lect_obj = self.openerp.pool.get('giscedata.facturacio.lectures.energia')

                pol_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'polissa_0001'
                )[1]

                comptador_id = comp_obj.search(
                    cursor, uid, [('polissa', '=', pol_id)]
                )[0]

                self.activar_polissa(txn)
                self.add_taxes_to_periods(txn)
                self.add_iva_to_concepts(txn)
                self.set_taxes_to_invoice_lines(txn)

                fact_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_facturacio', 'factura_0008'
                )[1]
                fact = factura_obj.browse(cursor, uid, fact_id)

                invoice_id = fact.invoice_id.id
                invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])
                #self.add_taxes_to_periods(txn)

                lectures_ids = lect_obj.search(cursor, uid,
                                               [('comptador_id', '=', comptador_id)])
                upd_dict_readings = {
                    'motiu_ajust': '02',
                    'ajust': 123,
                }

                # write ajuste for ol readings
                lect_obj.write(cursor, uid, lectures_ids, upd_dict_readings)

                invoice_id = fact.invoice_id.id
                invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

                fact.partner_id.write({'ref': '0002'})
                iban_banks = [
                    bank
                    for bank in fact.company_id.partner_id.bank_ids
                    if bank.iban
                ]
                iban_banks[0].write({'default_bank': True})
                generated_f1 = factura_obj.export_new_f1(
                    cursor, uid, fact_id, context=None
                )[1]

                self.assertXmlEqual(generated_f1, expected_f1)
                validation = validate_xml(generated_f1)
                self.assertTrue(validation.valid)

    def test_exportation_returns_correct_xml_with_iva_reducido(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1_iva_reducido.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_reduced_tax_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_wiz_dont_export_if_invoice_already_exported_and_not_force(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'no_rent_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.write({'f1_exported': True})

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertTrue(exported)

            for rent_line in fact.linies_lloguer:
                rent_line.unlink()

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            invoice_id = fact.invoice_id.id
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            wiz_xml_obj = self.openerp.pool.get('wizard.export.xmls')

            ctx = {
                'active_id': fact_id, 'active_ids': [fact_id], 'factures': '1'
            }

            wiz_id = wiz_xml_obj.create(
                cursor, uid,
                {'mark_as_exported': True, 'force_mark_exported': False},
                context=ctx
            )

            wiz = wiz_xml_obj.browse(cursor, uid, wiz_id)

            with self.assertRaises(osv.except_osv) as e:
                wiz.generar_xmls(context=ctx)
            self.assertIn(
                'No s\'han trobat factures per exportar', e.exception.message
            )

            # self.assertXmlEqual(generated_f1, expected_f1)

    def test_wiz_export_and_mark_if_invoice_not_exported_and_not_force(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'no_rent_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.write({'f1_exported': False})

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertFalse(exported)

            for rent_line in fact.linies_lloguer:
                rent_line.unlink()

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            invoice_id = fact.invoice_id.id
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            wiz_xml_obj = self.openerp.pool.get('wizard.export.xmls')

            ctx = {
                'active_id': fact_id, 'active_ids': [fact_id], 'factures': '1'
            }

            wiz_id = wiz_xml_obj.create(
                cursor, uid,
                {'mark_as_exported': True, 'force_mark_exported': False},
                context=ctx
            )

            wiz = wiz_xml_obj.browse(cursor, uid, wiz_id)

            wiz.generar_xmls(context=ctx)

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertTrue(exported)

            generated_f1 = b64decode(wiz.read(['zipcontent'])[0]['zipcontent'])

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_wiz_export_if_invoice_exported_and_force(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'no_rent_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.write({'f1_exported': True})

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertTrue(exported)

            for rent_line in fact.linies_lloguer:
                rent_line.unlink()

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            invoice_id = fact.invoice_id.id
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            wiz_xml_obj = self.openerp.pool.get('wizard.export.xmls')

            ctx = {
                'active_id': fact_id, 'active_ids': [fact_id], 'factures': '1'
            }

            wiz_id = wiz_xml_obj.create(
                cursor, uid,
                {'mark_as_exported': False, 'force_mark_exported': True},
                context=ctx
            )

            wiz = wiz_xml_obj.browse(cursor, uid, wiz_id)

            wiz.generar_xmls(context=ctx)

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertTrue(exported)

            generated_f1 = b64decode(wiz.read(['zipcontent'])[0]['zipcontent'])

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_wiz_export_if_invoice_not_exported_and_dont_mark_if_wiz_mark_false(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'no_rent_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            fact = factura_obj.browse(cursor, uid, fact_id)
            # partner_obj = self.openerp.pool.get('res.partner')
            # p_ids = partner_obj.search(cursor, uid, [])
            #
            # partner_obj.write(cursor, uid, p_ids, {'vat': 'ATU00000024'})

            fact.write({'f1_exported': False})

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertFalse(exported)

            for rent_line in fact.linies_lloguer:
                rent_line.unlink()

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            invoice_id = fact.invoice_id.id
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            wiz_xml_obj = self.openerp.pool.get('wizard.export.xmls')

            ctx = {
                'active_id': fact_id, 'active_ids': [fact_id], 'factures': '1'
            }

            wiz_id = wiz_xml_obj.create(
                cursor, uid,
                {'mark_as_exported': False, 'force_mark_exported': False},
                context=ctx
            )

            wiz = wiz_xml_obj.browse(cursor, uid, wiz_id)

            wiz.generar_xmls(context=ctx)

            exported = factura_obj.read(
                cursor, uid, fact_id, ['f1_exported']
            )['f1_exported']

            self.assertFalse(exported)

            generated_f1 = b64decode(wiz.read(['zipcontent'])[0]['zipcontent'])

            self.assertXmlEqual(generated_f1, expected_f1)