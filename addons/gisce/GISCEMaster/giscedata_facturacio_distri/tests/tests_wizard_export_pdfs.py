# -*- coding: utf-8 -*-
from giscedata_facturacio_distri.wizard.export_pdfs import \
    filter_comer_field_names
from destral import testing
from destral.transaction import Transaction


class TestWizardExportPdfs(testing.OOTestCase):
    def assignar_factures_i_obtenir_comers(self, cursor, uid, lot_id):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')

        factura01 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        factura02 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0002'
        )[1]
        factura03 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0003'
        )[1]

        factura04 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0004'
        )[1]

        factures = [factura01, factura02, factura03]

        # We assign lot facturacio to all invoices but we don't update ref
        # for the invoice 0004
        fact_obj.write(cursor, uid, factures + [factura04], {
            'lot_facturacio': lot_id
        })

        comer_read = fact_obj.read(cursor, uid, factures, ['partner_id'])
        comer_ids = set(comer['partner_id'][0] for comer in comer_read)
        for comer in comer_ids:
            # We make sure that all the comers we will be checking will
            # have a value in ref (we don't really care which)
            partner_obj.write(
                cursor, uid,  [comer], {'ref': str(comer).zfill(4)}
            )

        return comer_ids

    def test_filter_comer_field_names_filters_correctly(self):
        fields_to_filter = ['djfakl', 'comer', 'comer111', 'comerES123456789E',
                            'comer1234', 'comer4357', 'mocer1234', 'comer12345',
                            ]
        expected_return = ['comerES123456789E', 'comer1234',
                           'comer4357', 'comer12345']

        returned_list = list(filter_comer_field_names(fields_to_filter))
        assert returned_list == expected_return

    def test_fields_view_get_adds_all_comers_everywhere(self):
        wiz_obj = self.openerp.pool.get('wizard.export.pdfs')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            view_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_distri',
                'view_wizard_export_pdf_form'
            )[1]

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0001'
            )[1]

            comer_ids = self.assignar_factures_i_obtenir_comers(
                cursor, uid, lot_id
            )

            ctx = {'active_id': lot_id}

            wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            res = wiz_obj.fields_view_get(
                cursor, uid, view_id=view_id, view_type='form', context=ctx
            )

            partner_refs = partner_obj.read(
                cursor, uid, list(comer_ids), ['ref']
            )
            for comer in partner_refs:
                comer_field_name = 'comer' + comer['ref']
                assert wizard._columns.get(comer_field_name)
                assert comer_field_name in res['fields']
                assert comer_field_name in res['arch']

    def test_default_get_returns_all_comers_with_true_value(self):
        wiz_obj = self.openerp.pool.get('wizard.export.pdfs')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            view_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_distri',
                'view_wizard_export_pdf_form'
            )[1]

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0001'
            )[1]

            comer_ids = self.assignar_factures_i_obtenir_comers(
                cursor, uid, lot_id
            )

            ctx = {'active_id': lot_id}

            wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            wiz_obj.fields_view_get(  # Necessary to update _columns
                cursor, uid, view_id=view_id, view_type='form', context=ctx
            )

            res = wiz_obj.default_get(
                cursor, uid, wizard._columns.keys(), context=ctx
            )

            partner_refs = partner_obj.read(
                cursor, uid, list(comer_ids), ['ref']
            )
            for comer in partner_refs:
                comer_field_name = 'comer' + comer['ref']
                assert comer_field_name in res
                assert res[comer_field_name]

    def test_get_comers_returns_partners_with_null_ref(self):
        wiz_obj = self.openerp.pool.get('wizard.export.pdfs')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0001'
            )[1]

            partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]

            self.assignar_factures_i_obtenir_comers(
                cursor, uid, lot_id
            )

            ctx = {'active_id': lot_id}

            wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            comers = wizard.get_comers()

            self.assertSetEqual(
                set(comers), {str(partner_id).zfill(4), None}
            )

    def test_default_report_is_invoice(self):
        wiz_obj = self.openerp.pool.get('wizard.export.pdfs')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            lot_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'lot_0001'
            )[1]

            self.assignar_factures_i_obtenir_comers(
                cursor, uid, lot_id
            )

            ctx = {'active_id': lot_id}

            report_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_distri', 'report_factura'
            )[1]

            wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wizard.report.id, report_id)
