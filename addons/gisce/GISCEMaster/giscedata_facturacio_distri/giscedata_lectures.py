# -*- coding: utf-8 -*-
import re

from osv import osv, fields
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION

from giscedata_lectures_distri.giscedata_lectures import CARACTERISTIQUES_VALIDES

MODELS_SELECTION.append(
    ('giscedata.lectures.comptador', 'Comptadors')
)

CARACTERISTIQUES_VALIDES.append('alquiler')


class GiscedataLecturesComptador(osv.osv):
    """Comptador d'una pòlissa en facturació distribuidora.
    """
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def _cini(self, cursor, uid, ids, field_name, args, context=None):
        """
        Calculates the CINI of the comptador

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param field_name:
        :param args: Arguments
        :param context: OpenERP context
        :return: Tuple id,cini
        """

        from cini.models import Contador
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = dict.fromkeys(ids, False)

        # Models
        prod_obj = self.pool.get('product.product')
        pol_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        comptadors = self.read(
            cursor, uid, ids, [
                'product_lloguer_id', 'cini_auto', 'propietat', 'tg', 'polissa',
                'bloquejar_cini', 'cini', 'technology_type'
            ]
        )

        for comptador in comptadors:
            if comptador['cini_auto'] and not comptador['bloquejar_cini']:
                # Possibilitat de millorar aquest part fent un sql per evitar
                # fer multiples reads i optimitazar el rendimetn a l'hora de fer
                # el recalcul.
                # Polissa
                polissa = pol_obj.read(
                    cursor, uid, comptador['polissa'][0],
                    ['tarifa', 'agree_tipus', 'tensio_normalitzada']
                )
                # ModCon
                modcon_id = modcon_obj.search(
                    cursor, uid, [
                        ('polissa_id', '=', comptador['polissa'][0]),
                        ('active', '=', True)
                    ]
                )

                contador = Contador()
                # Fases
                if comptador['product_lloguer_id']:
                    prod_id = comptador['product_lloguer_id'][0]
                    prod = prod_obj.read(cursor, uid, prod_id, ['default_code'])
                    if not prod['default_code']:
                        prod['default_code'] = ''
                else:
                    prod = {'default_code': ''}
                if prod['default_code'] == 'ALQ19':
                    contador.fases = 1
                elif prod['default_code'] == 'ALQ20':
                    contador.fases = 3
                elif '3x' in polissa['tensio_normalitzada'][1]:
                    contador.fases = 3
                elif '1x' in polissa['tensio_normalitzada'][1]:
                    contador.fases = 1
                elif int(polissa['tensio_normalitzada'][1]) < 1000:
                    contador.fases = 1

                # Tecnologia
                contador.tecnologia = comptador.get('technology_type')

                # Telegestionat
                contador.telegestionado = comptador.get('tg')

                # Tipus contador
                if modcon_id:
                    contador.tipo_agree = modcon_obj.read(
                        cursor, uid, modcon_id[0], ['agree_tipus']
                    )['agree_tipus'][1]
                else:
                    contador.tipo_agree = polissa['agree_tipus'][1]

                # Tarifa AT/BT
                tarifa_id = tarifa_obj.search(
                    cursor, uid, [('id', '=', polissa['tarifa'][0])]
                )[0]
                contador.tipo_tarifa = tarifa_obj.read(
                    cursor, uid, tarifa_id, ['tipus']
                )['tipus']

                # Propietat
                if comptador['propietat'] == 'client':
                    contador.propiedad_cliente = True
                else:
                    contador.propiedad_cliente = False

                res[comptador['id']] = str(contador.cini)
            else:
                res[comptador['id']] = comptador['cini']

        return res

    def _cini_inv(self, cursor, uid, ids, name, value, fnct_inv_arg,
                  context=None):
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        upd = []
        for comptador in self.read(cursor, uid, ids, ['bloquejar_cini']):
            if not comptador['bloquejar_cini']:
                upd.append(comptador['id'])
        cursor.execute("UPDATE giscedata_lectures_comptador SET cini = %s, "
                       "cini_auto = False WHERE id in %s", (value, tuple(upd)))
        return True

    def update_values(self, cursor, uid, res, name, value):
        """
        Depenent del name rebut per paràmetre, actualizta el valor corresponent al formulari
        Hereda de: giscedata_lectures_distri.giscedata_lectures
        :param cursor:
        :param uid:
        :param res:
        :param name: nom del camp a modificar
        :param value: valor a assignar
        :return:
        """
        if name == 'alquiler':
            prod_obj = self.pool.get('product.product')
            prod_data = prod_obj.search(cursor, uid, [('default_code', '=', value)])
            if prod_data:
                res['value'].update({'product_lloguer_id': prod_data[0]})
                res['value'].update({'lloguer': True})
        super(GiscedataLecturesComptador, self).update_values(cursor, uid, res, name, value)

    _columns = {
        'product_lloguer_id': fields.many2one('product.product',
                                              'Producte de Lloguer',
                                              domain=[('rental', '=', 1)],
                                              ondelete='restrict'),
        'cini_auto': fields.boolean('Calcular CINI (auto.)'),
        'cini': fields.function(
            _cini,
            type='char',
            size=256,
            string='CINI',
            store={
                'giscedata.lectures.comptador': (
                    lambda *a: a[3],
                    ['product_lloguer_id', 'cini_auto', 'propietat',
                     'tg', 'bloquejar_cini', 'cini'], 10
                ),
            },
            fnct_inv=_cini_inv,
            method=True,
        ),
        'property_uom_id': fields.property(
            'product.uom',
            type='many2one',
            relation='product.uom',
            string=u"Unitat de facturació lloguer",
            method=True,
            view_load=True,
            domain="[('category_id.name', '=', 'ALQ ELEC')]",
            help=u"Amb quina unitat es vol facturar el lloguer",
        ),
        'start_rent_date': fields.date('Data inicial lloguer')
    }

    _defaults = {
        'cini_auto': lambda *a: 1,
    }


GiscedataLecturesComptador()
