import logging

import pooler


def migrate(cursor, current_version):
    if not current_version:
        return
    logger = logging.getLogger('openerp.migration')
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    # Busquem tots els productes que estiguin a una categoria de lloguers
    imd_obj = pool.get('ir.model.data')
    categ_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'categ_alq_conta'
    )[1]
    uom_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio', 'uom_aql_elec_mes'
    )[1]
    product_obj = pool.get('product.product')
    p_ids = product_obj.search(cursor, uid, [
        ('categ_id', 'child_of', [categ_id]),
        ('uom_id', '!=', uom_id)
    ])
    logger.info("S'han trobat {0} productes a actualitzar".format(
        len(p_ids)
    ))
    for product in product_obj.read(cursor, uid, p_ids, ['name', 'uom_id']):
        logger.info('Actualitzant {0}:{1}({2}) -> {3}'.format(
            product['name'], product['uom_id'][1], product['uom_id'][0],
            uom_id
        ))
        product_obj.write(cursor, uid, [product['id']], {
            'uom_id': uom_id,
            'uom_po_id': uom_id
        })
