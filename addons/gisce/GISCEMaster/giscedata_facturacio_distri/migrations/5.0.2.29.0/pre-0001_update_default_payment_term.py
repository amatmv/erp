# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
import netsvc
# Crea el termini de pagament per defecte
def migrate(cursor, installed_version):
    # Es comprova si existeix 
    cursor.execute("""select ir.id as id_prop,
       term.id as id_term,
       lterm.id as id_lterm,
       term.name
from ir_property as ir
inner join account_payment_term as term on term.id = split_part(ir.value,',',2)::integer
inner join account_payment_term_line as lterm on lterm.payment_id = term.id
where fields_id in (
    select id 
    from ir_model_fields 
    where model = 'res.partner' and
    name = 'property_payment_term')
and split_part(ir.value,',',1) like 'account.payment.term'
and ir.res_id is Null""")

    # Si existeix cal vincular els ids a ir_model_data
    res = cursor.fetchall()
    for i in res:
        if '20' in i[3]:
            cursor.execute("""insert into ir_model_data 
            (name, module, model, res_id)
            values
            ('property_payment_term_distri', 'giscedata_facturacio_distri', 
             'ir_property', %s)""" % i[0])
    
            cursor.execute("""insert into ir_model_data 
            (name, module, model, res_id)
            values
            ('account_payment_term_distri', 'giscedata_facturacio_distri', 
             'account.payment.term', %s)""" % i[1])
    
            cursor.execute("""insert into ir_model_data 
            (name, module, model, res_id)
            values
            ('account_payment_term_line_distri', 'giscedata_facturacio_distri', 
             'account.payment.term.line', %s)""" % i[2])
