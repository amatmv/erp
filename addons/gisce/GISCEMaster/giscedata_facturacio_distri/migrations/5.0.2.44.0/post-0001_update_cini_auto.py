import netsvc
import pooler

UID = 1


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel("migration", level, msg)


def migrate(cursor, installed_version):
    pool = pooler.get_pool(cursor.dbname)
    compt_obj = pool.get('giscedata.lectures.comptador')
    ids = compt_obj.search(cursor, UID, [
        '|', ('cini', '=', False),
             ('cini', '=', '')
    ], 0, 0, False, {'active_test': False})
    compt_obj.write(cursor, UID, ids, {'cini_auto': 0})
    log("Recalculant CINIs...")
    compt_obj.write(cursor, UID, ids, {'cini_auto': 1})