# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Update module of wizard to giscedata_facturacio_distri')
    cursor.execute("update ir_model_data set module = 'giscedata_facturacio_distri' where name like '%fix_cch_fact_again%' and module = 'giscedata_facturacio_distri_tg';")


def down(cursor, installed_version):
    pass

migrate = up
