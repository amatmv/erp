# coding=utf-8
import logging
from datetime import datetime


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    # Esta fecha indicara a partir de que fecha los nuevos F1 usaran el
    # sistema de exportaciones

    # Todos los F1 anteriores a esta fecha se marcaran como exportados
    # Desde lote se podran exportar de igual manera

    fecha_inicio_exportaciones = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    update_facturas_set_f1_exported_true = (
        "UPDATE giscedata_facturacio_factura "
        "SET "
        "f1_exported = True, f1_export_date = %s "
        "WHERE NOT f1_exported"
        ""
    )

    cursor.execute(
        update_facturas_set_f1_exported_true,
        (fecha_inicio_exportaciones, )
    )

    logger.info(
        'Updated %s invoices, set as exported with export date %s',
        cursor.rowcount,
        fecha_inicio_exportaciones
    )


def down(cursor, installed_version):
    pass

migrate = up
