# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Baixa Tensió ACAD",
    "description": """Funcions per les eines CAD en Baixa Tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CAD",
    "depends":[
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
