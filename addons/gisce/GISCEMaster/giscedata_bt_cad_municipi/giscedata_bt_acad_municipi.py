# -*- coding: utf-8 -*-

from osv import osv,fields

class giscedata_bt_element(osv.osv):

  _name = 'giscedata.bt.element'
  _inherit = 'giscedata.bt.element'

  def read_distinct(self, cr, uid, values, id_municipi):
    cr.execute("""
      select name from giscedata_bt_element where municipi = %s
      and active = True
    """, (int(id_municipi), ))
    bt_in_bbdd = cr.dictfetchall()
    for v in values:
      try:
        bt_in_bbdd.remove({'name': v})
      except:
        pass
    return bt_in_bbdd

  def read_distinct2(self, cr, uid, values, id_municipi):
    in_array = []
    for value in values:
      cr.execute("""
        select name from giscedata_bt_element where name = %s and municipi = %s
        and active = True
      """, (value, int(id_municipi)))
      if not cr.fetchone():
        in_array.append(value)
    return in_array
  
  _columns = {
    'municipi': fields.many2one('res.municipi', 'Municipi', required=True),
  }

giscedata_bt_element()
