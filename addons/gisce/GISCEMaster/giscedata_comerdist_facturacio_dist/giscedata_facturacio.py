# -*- coding: utf-8 -*-
from osv import osv, fields
import pooler
from giscedata_comerdist import OOOPPool, Sync
from tools.translate import _
import netsvc
from libfacturacioatr.tarifes import repartir_energia, repartir_potencia, \
    get_dates_linia
from gestionatr.helpers.funcions import CODIS_REG_REFACT
from itertools import groupby
from datetime import datetime, timedelta

# Hack caching
_PRODUCTES_EXPORT = {}
_PR_PROXY = {}


class GiscedataFacturacioFactura(osv.osv):
    """Exportació a comercialitzadora.
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    _columns = {
        'remote_id': fields.integer('Comerdist RID', readonly=True),
    }

    _defaults = {
        'remote_id': lambda *a: 0,
    }

    def copy(self, cursor, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({'remote_id': 0})
        res_id = super(GiscedataFacturacioFactura,
                       self).copy(cursor, uid, id, default, context)
        return res_id

    _F_FIELDS = {}
    _L_FIELDS = {}

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for factura in self.browse(cursor, uid, ids, context):
            if (factura.partner_id.id in partner_ids and
                factura.get_config().user_local.id != uid):
                return True
            else:
                return False

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for factura in self.browse(cursor, uid, ids, context):
            partner_id = factura.partner_id.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            config = config_obj.browse(cursor, uid, config_ids[0])
            return config

    def invoice_open(self, cursor, uid, ids, context=None):
        """Obrim la factura. hem d'assignar l'origin a la remota.
        """
        # Cridem el mètode pare
        super(GiscedataFacturacioFactura,
              self).invoice_open(cursor, uid, ids, context)
        # Per cada una si hem de reassignar número a la remota ho fem
        for factura in self.browse(cursor, uid, ids, context):
            if factura.must_be_synched(context):
                config_id = factura.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(factura.id, active_test=False)
                if obj:
                    obj.write({'origin':'FPD:%s' % factura.number})
        return True

    def anullar(self, cursor, uid, ids, tipus='A', context=None):
        """Anul·la factures remotes (comerdist).
        """
        refund_ids = super(GiscedataFacturacioFactura,
                           self).anullar(cursor, uid, ids, tipus, context)
        for factura in self.browse(cursor, uid,  refund_ids, context):
            if factura.must_be_synched(context):
                # L'anul·lem remotament
                config_id = factura.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(factura.ref.id, active_test=False)
                if not obj:
                    continue
                remote_id = obj.anullar(tipus, context)
                if isinstance(remote_id, (list, tuple)):
                    remote_id = remote_id[0]
                factura.write({'remote_id': remote_id})
        return refund_ids

    def rectificar(self, cursor, uid, ids, tipus='R', context=None):
        """Rectifiquem la factura remota (comredist).

        Com que tenim el mètode anullar() sobreescrit per tal que ho faci en
        remot, en ppi ja l'hauria d'haver anul·lat en remot també, per tant
        ens queda exportar la factura rectificadora a comercial.
        """
        # Cridem el mètode pare per rectificar
        rect_ids = super(GiscedataFacturacioFactura,
                         self).rectificar(cursor, uid, ids, tipus, context)
        # rect_ids conté les anul·ladores amb substituient i les
        # rectificadores, per tant les hem de filtrar
        only_rect_ids = self.search(cursor, uid,
                                    [('id', 'in', rect_ids),
                                     ('tipo_rectificadora', 'in', ('R', 'RA'))])
        self.export_to_comer(cursor, uid, only_rect_ids, True, context)
        return rect_ids

    def _export_linia(self, cursor, uid, linia_dict, r_factura, r_polissa,
                      factura, prods_tarifa, proxy, context=None):
        """Exportem línia a línia.
        """
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        linia = linia_obj.browse(cursor, uid, linia_dict['id'])
        linia_proxy = proxy.GiscedataFacturacioFacturaLinia
        pricelist_proxy = proxy.ProductPricelist
        tname = factura.tarifa_acces_id.name
        product_code = linia.product_id.default_code
        r_linia = {}
        r_linia['factura_id'] = r_factura['id']
        r_linia['name'] = linia_dict['name']
        r_linia['data_desde'] = linia_dict['data_desde']
        r_linia['data_fins'] = linia_dict['data_fins']
        #Si ja ens passen el subtotal d'atr per que ve d'una reparticio
        #el fem servir. Si no, correspon al total de la linia
        if 'atrprice_subtotal' in linia_dict:
            r_linia['atrprice_subtotal'] = linia_dict['atrprice_subtotal']
        else:
            r_linia['atrprice_subtotal'] = linia_dict['price_subtotal']
        # El coeficient de la 2.0DHA no el tornem a enviar a
        # comercialització
        if tname == '2.0DHA' and linia.tipus == 'energia':
            r_linia['multi'] = 1
        else:
            r_linia['multi'] = linia_dict['multi']
        r_linia.update({
            'quantity': linia_dict['quantity'],
            'tipus': linia_dict['tipus'],
            'cosfi': linia_dict['cosfi'],
        })
        if linia.tipus in prods_tarifa:
            prod = prods_tarifa[linia.tipus][linia.product_id.name]
            r_linia['product_id'] = prod
        elif product_code == 'EXTRA':
            prod = _PRODUCTES_EXPORT['extra']
            r_linia['product_id'] = prod
        else:
            #Search for the product code first if it exists
            #it can be blank in lines without product
            #or in products without default_code
            if product_code:
                r_prod = (proxy.ProductProduct.
                          browse([('default_code', '=', product_code)]))
            else:
                r_prod = False
            if r_prod:
                prod = r_prod[0].id
            else:
                prod = _PRODUCTES_EXPORT['default']
            r_linia['product_id'] = prod
        #Check if we have to group product
        pricelist_proxy = False
        if (r_linia['product_id'] and
            r_factura['llista_preu']):
            pricelist = proxy.ProductPricelist.get(r_factura['llista_preu'])
        if (pricelist and pricelist.agrupacio):
            new_product_id = (pricelist.
                              get_product_group(r_linia['product_id']))
            if new_product_id:
                r_linia['product_id'] = new_product_id
                r_linia['name'] = proxy.ProductProduct.read(new_product_id,
                                                        ['name'])['name']
                prod = new_product_id
        if not self._L_FIELDS:
            self._L_FIELDS = linia_proxy.fields_get()
        # Obtenim valors per defecte
        #Si la linia es de potencia, la uos
        #ha de ser la mateixa que la de la linea local
        #i no la uom per defecte del producte
        if linia.tipus == 'potencia':
            search_params = [('name', '=', linia.uos_id.name),
                             ('factor', '=', linia.uos_id.factor)]
            uom_id = proxy.ProductUom.search(search_params)[0]
        else:
            uom_id = proxy.ProductProduct.get(prod).uom_id.id
        l_vals = linia_proxy.product_id_change([],
            r_factura['llista_preu'], linia_dict['data_desde'],
            prod, uom_id, r_polissa.id, linia_dict['quantity'],
            linia.name, 'out_invoice', r_factura['partner_id'],
            r_factura['fiscal_position'])
        r_linia.update(l_vals.get('value', {}))
        if product_code == 'EXTRA':
            #map taxes from this line into comer
            taxes = []
            for tax in linia.invoice_line_tax_id:
                r_tax = proxy.AccountTax.browse([('name', '=', tax.name)])
                taxes.append(r_tax[0].id)
            r_linia['invoice_line_tax_id'] = [(6, 0, taxes)]
        else:
            r_linia['invoice_line_tax_id'] = [
                (6, 0, l_vals.get('value', {}).get('invoice_line_tax_id', []))
            ]
        if linia.tipus not in prods_tarifa:
            # Reescrivim el preu
            r_linia['price_unit_multi'] = linia.price_unit_multi
        if linia.tipus == 'reactiva':
            # Calculem el cosfi
            pr_cosfi = _PRODUCTES_EXPORT['cosfi']
            l_vals = linia_proxy.product_id_change([],
            r_factura['llista_preu'], linia_dict['data_desde'],
            pr_cosfi, uom_id, r_polissa.id, linia.cosfi * 100,
            linia.name, 'out_invoice', r_factura['partner_id'])
            price = l_vals['value']['price_unit_multi']
            r_linia['price_unit_multi'] = price
        r_linia['id'] = linia_proxy.create(r_linia.copy())

    def export_to_comer_autoci(self, cursor, uid, ids, force=False,
                               context=None):
        """Exporta les factures commitant cada una.
        """
        if not context:
            context = {}
        context['autoci'] = False
        api = netsvc.LocalService('object_proxy')
        res = []
        for fid in ids:
            res += api.execute(cursor.dbname, uid, self._name,
                               'export_to_comer', [fid], force=force,
                               context=context)
        return res

    def export_to_comer(self, cursor, uid, ids, force=False, context=None):
        """Exportem les factures a la comercialitzadora.

        Aquest mètode a de ser la substitució de l'script aspirafact5.py.
        """
        if not context:
            context = {}
        if context.get('autoci', False):
            self.export_to_comer_autoci(cursor, uid, ids, force, context)
        logger = netsvc.Logger()
        factures_export_ids = []
        for factura in self.browse(cursor, uid, ids, context):
            # Comprovem si ja està exportada?
            if factura.remote_id:
                # comprovem que existeixi factura.remote_id en remot
                config_id = factura.get_config().id
                proxy = OOOPPool.get_ooop(cursor, uid, config_id)
                factura_proxy = proxy.GiscedataFacturacioFactura
                if factura_proxy.search([('id', '=', factura.remote_id)]):
                    logger.notifyChannel("objects", netsvc.LOG_INFO,
                                         "factura (id:%s) ja exportada (id:%s)"
                                         % (factura.id, factura.remote_id))
                    continue
            if not force and factura.state != 'open':
                continue
            if factura.must_be_synched(context):
                config_id = factura.get_config().id
                proxy = OOOPPool.get_ooop(cursor, uid, config_id)
                factura_proxy = proxy.GiscedataFacturacioFactura
                tname = factura.tarifa_acces_id.name
                # Cache de productes per les línies
                if tname not in _PRODUCTES_EXPORT:
                    _PRODUCTES_EXPORT[tname] = {}
                    productes = _PRODUCTES_EXPORT[tname]
                    r_tar = (proxy.GiscedataPolissaTarifa.
                             browse([('name', '=', tname)],
                                    context={'active_test': False}))
                    r_tar = r_tar[0]
                    productes['energia'] = r_tar.get_periodes_producte('te')
                    productes['potencia'] = r_tar.get_periodes_producte('tp')
                    productes['exces_potencia'] = r_tar.get_periodes_producte('ep')
                    productes['reactiva'] = r_tar.get_periodes_producte('tr')
                if not 'default' in _PRODUCTES_EXPORT:
                    # Agafem producte ALQ01 com per defecte
                    prod = (proxy.ProductProduct.
                            browse([('default_code', '=', 'ALQ01')]))
                    _PRODUCTES_EXPORT['default'] = prod[0].id
                if not 'extra' in _PRODUCTES_EXPORT:
                    # Agafem el producte amb codi extra
                    prod = (proxy.ProductProduct.
                            browse([('default_code', '=', 'EXTRA')]))
                    _PRODUCTES_EXPORT['extra'] = prod[0].id
                if not 'cosfi' in _PRODUCTES_EXPORT:
                    cosfi = (proxy.IrModelData.
                             browse([('name', '=', 'product_cosfi'),
                                     ('module', '=', 'giscedata_facturacio')]))
                    if not len(cosfi):
                        raise osv.except_osv('Error',
                               _(u'No existeix el producte cosfi en la base de '
                                 u'dades de comercialitzadora.\n'
                                 u'Actualitzar el mòdul giscedata_facturacio'))
                    cosfi = cosfi[0]
                    _PRODUCTES_EXPORT['cosfi'] = cosfi.res_id
                prods_tarifa = _PRODUCTES_EXPORT[tname]
                # Obtenim la pòlissa
                sync = Sync(cursor, uid, proxy, 'giscedata.polissa',
                            config=config_id)
                # Primer busquem la pòlissa activa
                r_polissa = sync.get_object(factura.polissa_id.id)
                if not r_polissa:
                    # Busquem amb l'active false per si està desactivat
                    r_polissa = sync.get_object(factura.polissa_id.id,
                                                active_test=False)
                # 1. Crear la factura
                #r_factura = factura_proxy.new()
                r_factura = {}
                if not self._F_FIELDS:
                    self._F_FIELDS = factura_proxy.fields_get()
                f_fields = self._F_FIELDS
                # Obtenim valors per defecte
                vals = factura_proxy.onchange_polissa([], r_polissa.id,
                                                      'out_invoice')
                # Aquí agafa els valors actuals, però per algunes coses ha
                # d'agafar els que tenia en la data_final de la factura
                vals['on_date'] = r_polissa.read(['facturacio', 'tarifa',
                                                  'potencia', 'llista_preu',
                                                  'property_unitat_potencia'],
                                                 {'date': factura.data_final})
                base = 'mes'
                factor = vals['on_date']['property_unitat_potencia'].factor
                if not factor % 365 or not factor % 366:
                    base = 'dia'
                vals['value'].update({
                    'facturacio': vals['on_date']['facturacio'],
                    'potencia': vals['on_date']['potencia'],
                    'tarifa_acces_id': vals['on_date']['tarifa'].id,
                    'llista_preu': vals['on_date']['llista_preu'].id
                })
                r_factura.update(vals.get('value', {}))
                r_factura.update({
                    'facturacio': factura.facturacio,
                    'tipo_facturacion': factura.tipo_facturacion,
                    'tipo_factura': factura.tipo_factura,
                    'tipo_rectificadora': factura.tipo_rectificadora,
                    'date_boe': factura.date_boe,
                    'payment_mode_id': r_polissa.payment_mode_id.id,
                    'date_invoice': factura.date_invoice,
                    'data_inici': factura.data_inici,
                    'data_final': factura.data_final,
                    'name': factura.name,
                    'polissa_id': r_polissa.id,
                    'type': factura.type
                })
                # Assignem la data límit de pagament
                if r_factura.get('payment_term', False):
                    vals = factura_proxy.onchange_payment_term_date_invoice([],
                        r_factura['payment_term'], r_factura['date_invoice'])
                    r_factura.update(vals.get('value', {}))
                # Assign fiscal position
                if factura.fiscal_position:
                    fp_proxy = proxy.AccountFiscalPosition
                    r_fp_id = (fp_proxy.
                               browse([('name', '=',
                                        factura.fiscal_position.name)]))
                    if r_fp_id:
                        r_factura['fiscal_position'] = r_fp_id[0].id
                # Si té referència ja que sigui una rectificadora la linkem
                if factura.ref:
                    sync = Sync(cursor, uid, proxy, self._name,
                                config=config_id)
                    r_ref = sync.get_object(factura.ref.id, active_test=False)
                    r_factura['ref'] = r_ref and r_ref.id or False
                    r_factura['fiscal_position'] = (r_ref and r_ref.fiscal_position
                                                    and r_ref.fiscal_position.id
                                                    or False)
                # Posem que l'origen de la factura a comercial és aquesta
                # FPD: Factura Peajes Distribución
                if factura.number:
                    r_factura['origin'] = 'FPD:%s' % factura.number
                # Standard nostre: posem a descripció el número de contracte
                r_factura['name'] = r_polissa.name
                #Les factures manuals no tenen lot
                if factura.lot_facturacio:
                    lot_name = factura.lot_facturacio.name
                    lot = proxy.GiscedataFacturacioLot.search([('name', '=',
                                                            lot_name)])[0]
                    r_factura['lot_facturacio'] = lot
                # Assignem el diari que toqui (igual que el codi local)
                journal_code = factura.journal_id.code
                journal = proxy.AccountJournal.search([('code', '=',
                                                        journal_code)])
                if len(journal):
                    r_factura['journal_id'] = journal[0]
                r_factura['id'] = factura_proxy.create(r_factura.copy()).id
                # 2. Crear les línies de factura
                r_facturador = proxy.GiscedataFacturacioFacturador
                versions = r_facturador.versions_de_preus(r_polissa.id,
                                                          factura.data_inici,
                                                          factura.data_final)
                facturador = self.pool.get('giscedata.facturacio.facturador')
                versions_dist = facturador.versions_de_preus(cursor, uid,
                                                             factura.polissa_id.id,
                                                             factura.data_inici,
                                                             factura.data_final)
                do_reparticio = False
                #Si a comer hi ha version que no son a distri, hem de repartir
                if list(set(versions.keys()) - set(versions_dist.keys())):
                    do_reparticio = True
                    if factura.lectures_energia_ids:
                        data_inici_lectura = '3000-01-01'
                        data_final_lectura = '1900-01-01'
                        for lectura in factura.lectures_energia_ids:
                            data_inici_lectura = min([lectura.data_anterior,
                                                      data_inici_lectura])
                            data_final_lectura = max([lectura.data_actual,
                                                      data_final_lectura])
                    else:
                        data_inici_lectura = False
                        data_final_lectura = False
                linia_export = []
                #Reparto real si es possible
                #Hem de tindre lectures de tancament per totes
                #les versions de preus de comercialitzadora
                tarifa = factura.tarifa_acces_id
                reparto_real = facturador.reparto_real(cursor, uid,
                                                        tarifa.name,
                                                        context=context)
                consums = {}
                if reparto_real and do_reparticio and len(versions) > 1:
                    version_dates = sorted(versions.keys())
                    consum_total = 0
                    for lectura in factura.lectures_energia_ids:
                        if lectura.tipus != 'activa':
                            continue
                        desde = datetime.strftime(datetime.strptime(lectura.data_anterior,
                                                                    '%Y-%m-%d')
                                                  + timedelta(days=1), '%Y-%m-%d')
                        hasta = lectura.data_actual
                        reparticio = repartir_energia(versions, lectura.consum,
                                                      desde, hasta)
                        periode = lectura.name.split('(')[1][:2]
                        for data_versio, consum in reparticio.items():
                            if not periode in consums:
                                consums[periode] = {}
                            if not data_versio in consums[periode]:
                                consums[periode][data_versio] = {'consum': 0,
                                                                 'data_desde': False,
                                                                 'data_fins': False}
                            vals = consums[periode][data_versio]
                            data_desde, data_fins = get_dates_linia(
                                    versions, data_versio, lectura.data_anterior,
                                    lectura.data_actual
                                )
                            consum_total += consum
                            vals['consum'] += consum
                            vals['data_desde'] = min((vals['data_desde']
                                                      or '3000-01-01'),
                                                     data_desde)
                            vals['data_fins'] = max((vals['data_fins']
                                                     or '1900-01-01'),
                                                    data_fins)
                    #Si hem de repartir, mirem les linies extra que no s'han de tocar
                    #per que no venen de consums de lectures i comprovem
                    #que hem pogut repartir tot el consum. Si no, reparto normal
                    if reparto_real:
                        extra_lines = factura.get_extra_factura_lines()
                        consum_distri = sum([linia.quantity for linia in factura.linies_energia
                                             if linia.id not in extra_lines])
                        if consum_distri != consum_total:
                            reparto_real = False
                            consums = {}
                add_consums = consums.copy()
                for linia in factura.linia_ids:
                    #Do not export regularizacion refacturacion to comer
                    if linia.product_id.default_code in CODIS_REG_REFACT:
                        continue
                    linia_dict = linia.read()[0]
                    if do_reparticio and len(versions) > 1:
                        reparticio = False
                        # Canvi de tarifa al costat de la comercialitzadora
                        # Mirem si podem fer reparto real de les linies d'energia
                        if (reparto_real
                            and linia.tipus == 'energia'
                            and linia.quantity != 0
                            and linia.id not in extra_lines):
                            periode = linia.product_id.name
                            if not periode in add_consums:
                                continue
                            atrtotal = linia.price_subtotal
                            numlectures = len(add_consums[periode].keys())
                            counter = 0
                            for lect_vals in add_consums[periode].values():
                                counter += 1
                                linia_dict['quantity'] = lect_vals['consum']
                                linia_dict['data_desde'] = lect_vals['data_desde']
                                linia_dict['data_fins'] = lect_vals['data_fins']
                                if counter == numlectures:
                                    atrprice = atrtotal
                                else:
                                    atrprice = round((lect_vals['consum']
                                            * linia.price_unit), 2)
                                    atrtotal -= atrprice
                                linia_dict['atrprice_subtotal'] = atrprice
                                linia_export.append(linia_dict.copy())
                            del add_consums[periode]
                            continue
                        # Si no, el que farem es repartir de forma normal
                        if (linia.tipus in ("energia", "reactiva")
                            and linia.quantity != 0):
                            if (reparto_real and linia.tipus == 'energia'
                                and linia.id not in extra_lines):
                                continue
                            reparto_desde = (data_inici_lectura or
                                             linia.data_desde)
                            # La llibreria té en compte el tipus de dada que
                            # li entra, si és un float retornarà amb decimals
                            # el repartiment. Per tant comprovarem si el
                            # valor és el mateix que passat a enter
                            consum_a_repartir = linia.quantity
                            if linia.quantity == int(linia.quantity):
                                consum_a_repartir = int(linia.quantity)
                            # Si des d'on hem de repartir és més petit que la
                            # data d'inici, vol dir que venim d'una lectura
                            # anterior i per tant li hem de sumar un dia.
                            if reparto_desde < factura.data_inici:
                                reparto_desde = datetime.strftime(
                                    datetime.strptime(reparto_desde, '%Y-%m-%d')
                                    + timedelta(days=1), '%Y-%m-%d'
                                )
                            reparticio = repartir_energia(
                                versions, consum_a_repartir, reparto_desde,
                                linia.data_fins
                            )
                            reparticio_atr = repartir_energia(
                                versions, linia.price_subtotal, reparto_desde,
                                linia.data_fins
                            )
                            for data_versio, consum in reparticio.items():
                                data_desde, data_fins = get_dates_linia(
                                    versions, data_versio, linia.data_desde,
                                    linia.data_fins
                                )
                                atr = reparticio_atr[data_versio]
                                linia_dict['atrprice_subtotal'] = atr
                                linia_dict['quantity'] = consum
                                linia_dict['data_desde'] = data_desde
                                linia_dict['data_fins'] = data_fins
                                linia_export.append(linia_dict.copy())
                        elif linia.tipus == "potencia":
                            reparticio = repartir_potencia(
                                versions, linia.data_desde, linia.data_fins,
                                base
                            )
                            num_repartos = len(reparticio)
                            reparto_atr = linia.price_subtotal
                            iterator = 0
                            for data_versio, multi in reparticio.items():
                                iterator += 1
                                data_desde, data_fins = get_dates_linia(
                                    versions, data_versio, linia.data_desde,
                                    linia.data_fins
                                )
                                if iterator == num_repartos:
                                    atr = reparto_atr
                                else:
                                    atr = round((linia.price_subtotal
                                             * ( multi / linia.multi)), 2)
                                    reparto_atr -= atr
                                linia_dict['atrprice_subtotal'] = atr
                                linia_dict['multi'] = multi
                                linia_dict['data_desde'] = data_desde
                                linia_dict['data_fins'] = data_fins
                                linia_export.append(linia_dict.copy())
                        else:
                            linia_export.append(linia_dict.copy())
                    else:
                        linia_export.append(linia_dict.copy())
                # Finalment exportem
                for linia in linia_export:
                    self._export_linia(cursor, uid, linia, r_factura,
                                       r_polissa, factura, prods_tarifa, proxy,
                                       context)
                # 3. Crear les lectures
                msg = _(u"No s'ha trobat el comptador %s en la "
                        u"comercialitzadora")
                # Energia
                sync = Sync(cursor, uid, proxy, 'giscedata.lectures.comptador',
                            config=config_id)
                comptador_c = {}
                #Agrupar les lectures si es necessari
                r_pricelist = proxy.ProductPricelist.get(r_factura['llista_preu'])
                agrupacio = r_pricelist.agrupacio
                lectures = []
                for lectura in factura.lectures_energia_ids:
                    r_lectura = {}
                    r_lectura.update({
                        'factura_id': r_factura['id'],
                        'name': lectura.name,
                        'tipus': lectura.tipus,
                        'magnitud': lectura.magnitud,
                        'data_actual': lectura.data_actual,
                        'lect_actual': lectura.lect_actual,
                        'data_anterior': lectura.data_anterior,
                        'lect_anterior': lectura.lect_anterior,
                        'consum': lectura.consum,
                        'comptador': lectura.comptador_id.name
                    })
                    if r_lectura['comptador'] not in comptador_c:
                        comptador = sync.get_object(lectura.comptador_id.id,
                                                active_test=False,
                                                double_test=True)
                        if not comptador:
                            err = msg % lectura.comptador_id.name
                            raise osv.except_osv('Error', err)
                        comptador_c[r_lectura['comptador']] = comptador.id
                    r_lectura['comptador_id'] = comptador_c[r_lectura['comptador']]
                    if agrupacio:
                        lectures.append(r_lectura)
                    else:
                        (proxy.GiscedataFacturacioLecturesEnergia.
                                                create(r_lectura.copy()))
                if agrupacio:
                    group_lectures = {}
                    keyfunc = lambda x: '%s %s %s %s %s' % (x['name'],
                                                  x['comptador'],
                                                  x['tipus'],
                                                  x['data_actual'],
                                                  x['data_anterior'])
                    #Group lectures by keyfunc key
                    for key, values in groupby(lectures, keyfunc):
                        group_lectures[key] = list(values)[0]
                    lectures_finals = group_lectures.copy()
                    #Once grouped sum and create lectures
                    for key, lectura in group_lectures.iteritems():
                        per_name = lectura['name']
                        newper = r_pricelist.get_periode_group(per_name)
                        if newper:
                            lectura['name'] = newper[1]
                            new_key = keyfunc(lectura)
                            vals = lectures_finals[new_key]
                            vals.update({
                            'lect_actual': (vals['lect_actual']
                                            + lectura['lect_actual']),
                            'lect_anterior': (vals['lect_anterior']
                                     + lectura['lect_anterior']),
                            'consum': (vals['consum']
                                       + lectura['consum']),
                            'data_actual': max(vals['data_actual'],
                                               lectura['data_actual']),
                            'data_anterior': min(vals['data_anterior'],
                                          lectura['data_anterior'])
                                })
                            del lectures_finals[key]
                    for lectura in lectures_finals.values():
                        proxy.GiscedataFacturacioLecturesEnergia.create(lectura.copy())
                # Potència
                for lectura in factura.lectures_potencia_ids:
                    r_lectura = {}
                    r_lectura.update({
                        'factura_id': r_factura['id'],
                        'name': lectura.name,
                        'pot_contract': lectura.pot_contract,
                        'pot_maximetre': lectura.pot_maximetre,
                        'exces': lectura.exces,
                        'comptador': lectura.comptador_id.name
                    })
                    if r_lectura['comptador'] not in comptador_c:
                        comptador = sync.get_object(lectura.comptador_id.id,
                                                active_test=False,
                                                double_test=True)
                        if not comptador:
                            err = msg % lectura.comptador_id.name
                            raise osv.except_osv('Error', err)
                        comptador_c[r_lectura['comptador']] = comptador.id
                    r_lectura['comptador_id'] = comptador_c[r_lectura['comptador']]
                    proxy.GiscedataFacturacioLecturesPotencia.create(r_lectura.copy())
                #Compute extra lines in comer
                extra_proxy = proxy.GiscedataFacturacioExtra
                extra_proxy.compute_extra_lines([r_factura['id']], {'export_to_comer': True})
                factura_proxy.button_reset_taxes([r_factura['id']])
                # Guardem l'id remot
                factura.write({'remote_id': r_factura['id']})
                factures_export_ids.append(r_factura['id'])
                logger.notifyChannel("objects", netsvc.LOG_INFO,
                                     "%s factures exportades..."
                                     % len(factures_export_ids))
        return factures_export_ids

    def export_data_to_comer_autoci(self, cursor, uid, ids, context=None):
        """Exporta les dades de les factures commitant cada una.
        """
        if context is None:
            context = {}
        context['autoci'] = False
        api = netsvc.LocalService('object_proxy')
        res = {'reads': 0, 'max': 0, 'extra': 0}
        for fid in ids:
            tmp_res = api.execute(cursor.dbname, uid, self._name,
                                  'export_data_to_comer', [fid],
                                  context=context)
            for key in res:
                res[key] += tmp_res[key]
        return res

    def export_data_to_comer(self, cursor, uid, ids, context=None):

        for id in ids:
            num_reads = self.export_reads_from_invoice(cursor, uid, id,
                                                       context=context)
            num_max = self.export_max_from_invoice(cursor, uid, id,
                                                   context=context)
            num_extra = self.export_extra_from_invoice(cursor, uid, id,
                                                       context=context)

        return {'reads': num_reads, 'max': num_max, 'extra': num_extra}

    def export_reads_from_invoice(self, cursor, uid, factura_id, context=None):

        fact_read_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        read_obj = self.pool.get('giscedata.lectures.lectura')

        if isinstance(factura_id, (list, tuple)):
            factura_id = factura_id[0]

        factura = self.browse(cursor, uid, factura_id)
        exported = 0

        if factura.must_be_synched(context):
            config_id = factura.get_config().id
            proxy = OOOPPool.get_ooop(cursor, uid, config_id)
            tarifa = factura.tarifa_acces_id
            period_ids = [periode.id for periode in tarifa.periodes
                          if periode.tipus == 'te']

            r_tar_model = proxy.GiscedataPolissaTarifa
            r_tar = r_tar_model.browse([('name', '=', tarifa.name)],
                                       context={'active_test': False})[0]
            r_periods = dict([(period.name, period.id)
                              for period in r_tar.periodes
                              if period.tipus == 'te'])

            sync = Sync(cursor, uid, proxy, 'giscedata.lectures.comptador',
                        config=config_id)
            keys = []

            r_comptadors = {}
            r_origins = {}

            reads_to_export = []

            for lectura in factura.lectures_energia_ids:
                meter = lectura.comptador_id
                # Search for original reads
                # As origin is not always filled in invoice reads,
                # look for the original read to get the origin
                search_params = [('comptador', '=', meter.id),
                                 ('name', 'in', (lectura.data_actual,
                                                 lectura.data_anterior)),
                                 ('periode', 'in', period_ids)]
                read_ids = read_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
                reads_to_export.extend(read_ids)

                # Search for meter
                if meter.name not in r_comptadors:
                    r_comptador = sync.get_object(meter.id,
                                                  active_test=False,
                                                  double_test=True)
                    if not r_comptador:
                        msg = _(u"No s'ha trobat el comptador %s en la "
                                u"comercialitzadora")
                        err = msg % meter.name
                        raise osv.except_osv('Error', err)
                    r_comptadors[meter.name] = r_comptador.id

            reads_to_export = list(set(reads_to_export))
            r_read_obj = proxy.GiscedataLecturesLectura
            for read_id in reads_to_export:
                read = read_obj.browse(cursor, uid, read_id)
                period = r_periods[read.periode.name]
                meter_name = read.comptador.name
                r_lectura_vals = {
                    'name': read.name,
                    'lectura': read.lectura,
                    'tipus': read.tipus,
                    'periode': period,
                    'comptador': r_comptadors[meter_name]
                }

                # Search for an existing read remote
                # If it already exists do not create
                search_params = [('comptador', '=', r_comptadors[meter_name]),
                                 ('name', '=', read.name),
                                 ('periode', '=', period),
                                 ('tipus', '=', read.tipus)]
                r_read_ids = r_read_obj.search(search_params,
                                               context={'active_test': False})
                if r_read_ids:
                    continue
                # Search for remote origins
                origin = read.origen_id
                if origin.id not in r_origins:
                    r_orig_model = proxy.GiscedataLecturesOrigen
                    search_params = [('codi', '=', origin.codi),
                                     ('subcodi', '=', origin.subcodi)]
                    r_origin = r_orig_model.browse(search_params,
                                                   context={'active_test': False})
                    r_origin = r_origin[0]
                    r_origins[origin.id] = r_origin.id
                r_lectura_vals['origen_id'] = r_origins[origin.id]
                r_read_obj.create(r_lectura_vals.copy())
                exported += 1

        return exported

    def export_max_from_invoice(self, cursor, uid, factura_id, context=None):

        fact_read_obj = self.pool.get('giscedata.facturacio.lectures.potencia')
        read_obj = self.pool.get('giscedata.lectures.potencia')

        if isinstance(factura_id, (list, tuple)):
            factura_id = factura_id[0]

        factura = self.browse(cursor, uid, factura_id)
        exported = 0

        if factura.must_be_synched(context):
            config_id = factura.get_config().id
            proxy = OOOPPool.get_ooop(cursor, uid, config_id)
            tarifa = factura.tarifa_acces_id
            period_ids = [periode.id for periode in tarifa.periodes
                          if periode.tipus == 'tp']

            r_tar_model = proxy.GiscedataPolissaTarifa
            r_tar = r_tar_model.browse([('name', '=', tarifa.name)],
                                       context={'active_test': False})[0]
            r_periods = dict([(period.name, period.id)
                              for period in r_tar.periodes
                              if period.tipus == 'tp'])

            sync = Sync(cursor, uid, proxy, 'giscedata.lectures.comptador',
                        config=config_id)
            keys = []

            r_comptadors = {}
            r_origins = {}

            reads_to_export = []

            for lectura in factura.lectures_potencia_ids:
                meter = lectura.comptador_id
                # Search for original reads between invoicing period
                search_params = [('comptador', '=', meter.id),
                                 ('name', '>=', factura.data_inici),
                                 ('name', '<=', factura.data_final),
                                 ('periode', 'in', period_ids)]
                read_ids = read_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
                reads_to_export.extend(read_ids)

                # Search for meter
                if meter.name not in r_comptadors:
                    r_comptador = sync.get_object(meter.id,
                                                  active_test=False,
                                                  double_test=True)
                    if not r_comptador:
                        msg = _(u"No s'ha trobat el comptador %s en la "
                                u"comercialitzadora")
                        err = msg % meter.name
                        raise osv.except_osv('Error', err)
                    r_comptadors[meter.name] = r_comptador.id

            reads_to_export = list(set(reads_to_export))
            r_read_obj = proxy.GiscedataLecturesPotencia
            for read_id in reads_to_export:
                read = read_obj.browse(cursor, uid, read_id)
                period = r_periods[read.periode.name]
                meter_name = read.comptador.name
                r_lectura_vals = {
                    'name': read.name,
                    'lectura': read.lectura,
                    'exces': read.exces,
                    'periode': period,
                    'comptador': r_comptadors[meter_name]
                }

                # Search for an existing read remote
                # If it already exists do not create
                search_params = [('comptador', '=', r_comptadors[meter_name]),
                                 ('name', '=', read.name),
                                 ('periode', '=', period)]
                r_read_ids = r_read_obj.search(search_params,
                                               context={'active_test': False})
                if r_read_ids:
                    continue
                # Search for remote origins
                r_read_obj.create(r_lectura_vals.copy())
                exported += 1

        return exported

    def export_extra_from_invoice(self, cursor, uid, factura_id, context=None):

        line_obj = self.pool.get('giscedata.facturacio.factura.linia')

        if isinstance(factura_id, (list, tuple)):
            factura_id = factura_id[0]

        factura = self.browse(cursor, uid, factura_id)
        exported = 0

        if factura.must_be_synched(context):
            config_id = factura.get_config().id
            proxy = OOOPPool.get_ooop(cursor, uid, config_id)
            # Get invoice lines coming from extra
            extra_line_ids = self.get_extra_factura_lines(cursor, uid,
                                                          [factura_id])
            if extra_line_ids:
                tarifa = factura.tarifa_acces_id
                r_tar_model = proxy.GiscedataPolissaTarifa
                r_tar = r_tar_model.browse([('name', '=', tarifa.name)],
                                            context={'active_test': False})[0]

                # Get journal_id
                journal_code = factura.journal_id.code
                journal_ids = proxy.AccountJournal.search([('code', '=',
                                                            journal_code)])

                # Get polissa_id
                sync = Sync(cursor, uid, proxy, 'giscedata.polissa',
                            config=config_id)
                r_polissa = sync.get_object(factura.polissa_id.id,
                                            active_test=False,
                                            double_test=True)

                # Get fiscal position
                if factura.fiscal_position:
                    fp_proxy = proxy.AccountFiscalPosition
                    search_params =[('name', '=',
                                     factura.fiscal_position.name)]
                    r_fp_id = (fp_proxy.browse(search_params))
                    if r_fp_id:
                        r_fiscal_position_id = r_fp_id[0].id
                    else:
                        r_fiscal_position_id = False

            r_products = {}
            for extra_line_id in extra_line_ids:
                line = line_obj.browse(cursor, uid, extra_line_id)
                r_extra_vals = {
                    'name': line.name,
                    'price_unit': line.price_unit,
                    'term': 1,
                    'tipus': line.tipus,
                    'date_to': factura.data_final,
                    'date_from': factura.data_inici,
                    'date_line_from': line.data_desde,
                    'date_line_to': line.data_fins,
                    'quantity': line.quantity,
                    'journal_ids': [(6, 0, journal_ids)],
                    'polissa_id': r_polissa.id,
                }

                if line.tipus in r_products:
                    pass
                elif line.tipus == 'energia':
                    r_products[line.tipus] = r_tar.get_periodes_producte('te')
                elif line.tipus == 'potencia':
                    r_products[line.tipus] = r_tar.get_periodes_producte('tp')
                elif line.tipus == 'reactiva':
                    r_products[line.tipus] = r_tar.get_periodes_producte('re')
                elif line.tipus == 'exces_potencia':
                    r_products[line.tipus] = r_tar.get_periodes_producte('ep')

                # Associate product
                default_product = False
                if line.tipus in r_products:
                    r_product_id = r_products[line.tipus][line.product_id.name]
                else:
                    r_product_obj = proxy.ProductProduct
                    search_params = [('default_code', '=', 'EXTRA')]
                    r_product_id = r_product_obj.search(search_params)[0]
                    default_product = True
                r_extra_vals['product_id'] = r_product_id

                #Associate uos_id
                search_params = [('name', '=', line.uos_id.name),
                                 ('factor', '=', line.uos_id.factor)]
                uom_id = proxy.ProductUom.search(search_params)[0]
                r_extra_vals['uos_id'] = uom_id

                # Associate account_id
                r_account_obj = proxy.AccountAccount
                search_params = [('code', '=', line.account_id.code)]
                r_account_ids = r_account_obj.search(search_params)
                r_extra_vals['account_id'] = r_account_ids[0]

                # Associate taxes
                taxes = []
                if default_product:
                    for tax in line.invoice_line_tax_id:
                        r_tax = proxy.AccountTax.browse([('name', '=',
                                                          tax.name)])
                        taxes.append(r_tax[0].id)
                else:
                    # Get remote taxes for product
                    r_polissa_vals = r_polissa.read(
                                                ['llista_preu', 'pagador'],
                                                {'date': factura.data_final})
                    r_line_obj = proxy.GiscedataFacturacioFacturaLinia
                    r_vals = r_line_obj.product_id_change([],
                                        r_polissa_vals['llista_preu'].id,
                                        line.data_desde,
                                        r_product_id,
                                        uom_id,
                                        r_polissa.id,
                                        line.quantity,
                                        line.name,
                                        'out_invoice',
                                        r_polissa_vals['pagador'].id,
                                        r_fiscal_position_id)['value']
                    r_extra_vals['price_unit'] = r_vals['price_unit_multi']
                    taxes = r_vals['invoice_line_tax_id']
                r_extra_vals['tax_ids'] = [(6, 0, taxes)]

                proxy.GiscedataFacturacioExtra.create(r_extra_vals.copy())
                exported += 1

        return exported

GiscedataFacturacioFactura()


class GiscedataFacturacioContractelotFactura(osv.osv):
    """Marcar si està exportada o no.
    """
    _name = 'giscedata.facturacio.contracte_lot.factura'
    _inherit = 'giscedata.facturacio.contracte_lot.factura'

    _columns = {
        'exportada': fields.boolean('Factura exportada'),
    }

    _defaults = {
        'exportada': lambda *a: 0,
    }

GiscedataFacturacioContractelotFactura()


class GiscedataFacturacioLot(osv.osv):
    """Afegim el sistema per exportar les factures.
    """
    _name = 'giscedata.facturacio.lot'
    _inherit = 'giscedata.facturacio.lot'

    def add_export_to_comer_callback(self, cursor, uid, ids, context=None):
        task_obj = self.pool.get('res.task')
        for lotid in ids:
            task_obj.add_background_call(cursor, uid,
                                         self.export_to_comer_callback, None,
                                         None, cursor.dbname, lotid)
        return True

    def export_to_comer_callback(self, uid, tid, dbname, lotid):
        """Exportació de factures del lot.
        """
        try:
            dba = pooler.get_db_only(dbname)
            cursor = dba.cursor()
            task_obj = self.pool.get('res.task')
            factura_obj = self.pool.get('giscedata.facturacio.factura')
            clf_obj = self.pool.get('giscedata.facturacio.contracte_lot.factura')
            # Busquem les pendents per exportar
            search_params = [
                ('contracte_lot_id.lot_id.id', '=', lotid),
                ('exportada', '=', 0)
            ]
            clf_ids = clf_obj.search(cursor, uid, search_params)
            # Contem les que ja s'han exportat
            search_params = [
                ('contracte_lot_id.lot_id.id', '=', lotid),
                ('exportada', '=', 1)
            ]
            done = clf_obj.search_count(cursor, uid, search_params)
            # El total són les que ja estan fetes més les que s'han de fer
            total = done + len(clf_ids)
            task = task_obj.browse(cursor, uid, tid)
            while task.state in ('waiting', 'running'):
                if not clf_ids:
                    task_obj.update_progress(cursor, uid, [task.id],
                                             progress=100)
                task = task_obj.browse(cursor, uid, tid)
                if task.state in ('waiting', 'running'):
                    for clf in clf_obj.browse(cursor, uid, clf_ids):
                        try:
                            fcr = dba.cursor()
                            fid = clf.factura_id.id
                            factura_obj.export_to_comer(fcr, uid, [fid])
                            clf_obj.write(fcr, uid, [clf.id], {'exportada': 1})
                            done += 1
                            progress = round(float(done)/float(total), 4)
                            progress *= 100
                            task_obj.update_progress(fcr, uid, [task.id],
                                                     progress=progress)
                            fcr.commit()
                            service_name = 'sync.%s' % id(fcr)
                            if netsvc.service_exist(service_name):
                                netsvc.SERVICES[service_name].commit()
                        except Exception, exc:
                            fcr.rollback()
                            service_name = 'sync.%s' % id(fcr)
                            if netsvc.service_exist(service_name):
                                netsvc.SERVICES[service_name].rollback()
                                raise exc
                        finally:
                            fcr.close()
                            service_name = 'sync.%s' % id(fcr)
                            if netsvc.service_exist(service_name):
                                netsvc.SERVICES[service_name].close()
                                del netsvc.SERVICES[service_name]
            # Commiting global cursor
            cursor.commit()
            service_name = 'sync.%s' % id(cursor)
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].commit()
        except Exception, exc:
            cursor.rollback()
            service_name = 'sync.%s' % id(cursor)
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].rollback()
            raise exc
        finally:
            if task.state != 'done':
                task_obj.add_background_call(cursor, uid,
                                             self.export_to_comer_callback,
                                             tid, None, cursor.dbname, lotid)
            cursor.close()
            service_name = 'sync.%s' % id(cursor)
            if netsvc.service_exist(service_name):
                netsvc.SERVICES[service_name].close()
                del netsvc.SERVICES[service_name]
        return True

GiscedataFacturacioLot()
