# -*- coding: utf-8 -*-
{
    "name": "Comerdist Facturació (distri)",
    "description": """
    This module provide :
      * Volcat de les factures de distribuidora a comercialitzadora
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended",
        "giscedata_facturacio_distri",
        "giscedata_comerdist"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/export_to_comer_view.xml",
        "giscedata_facturacio_view.xml",
        "ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
