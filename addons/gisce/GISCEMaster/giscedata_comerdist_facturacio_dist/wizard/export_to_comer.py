# -*- coding: utf-8 -*-
import traceback

from osv import osv, fields
import netsvc

class WizardExportToComer(osv.osv_memory):
    """Assistent per exportar les factures a comercialitzadora.
    """
    _name = 'giscedata.facturacio.export_to_comer'

    def get_factures(self, cursor, uid, ids, count=False, context=None):
        """Retorna els ids o el número de factures depenen del paràmetre count.
        """
        if not context:
            context = {}
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        wizard = self.browse(cursor, uid, ids[0], context)
        #Si la crida l'ha feta una factura
        #no cerquem per lot, si no per id en active_ids
        if context.get('from_invoice', False):
            search_params = [('id', 'in', context.get('active_ids', [])),]
        else:
            lot_id = context['active_id']
            search_params = [('lot_facturacio.id', '=', lot_id)]
        # Facturacio == 3 => Totes
        if wizard.facturacio != 3:
            search_params.append(('facturacio', '=', wizard.facturacio))
        if count:
            return factura_obj.search_count(cursor, uid, search_params)
        else:
            return factura_obj.search(cursor, uid, search_params)

    def action_continuar(self, cursor, uid, ids, context=None):
        """Seleccionem les factures a exportar.
        """
        wizard = self.browse(cursor, uid, ids[0], context)
        n_factures = wizard.get_factures(count=True, context=context)
        wizard.write({'state': 'confirm', 'n_factures': n_factures})

    def action_exportar(self, cursor, uid, ids, context=None):
        """Exportem les factures a comercialitzadora.
        """
        logger = netsvc.Logger()
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        wizard = self.browse(cursor, uid, ids[0], context)
        factures_ids = wizard.get_factures(context=context)
        n_export = 0
        res_data = {'reads': 0, 'max': 0, 'extra': 0}
        errors = []
        for fid in factures_ids:
            try:
                if wizard.exportacio == 'all':
                    res = factura_obj.export_to_comer_autoci(cursor, uid, [fid],
                                                             force=True)
                    n_export += len(res)
                else:
                    res = factura_obj.export_data_to_comer_autoci(cursor, uid,
                                                                  [fid])
                    for key in res:
                        res_data[key] += res[key]
            except:
                factura = factura_obj.browse(cursor, uid, fid)
                error = ("- Error exportant la factura %s (id:%s)\n"
                         "TRACEBACK:\n"
                         "%s" % (factura.name, factura.id,
                                 traceback.format_exc()))
                logger.notifyChannel("objects", netsvc.LOG_ERROR, error)
                errors.append(error)
        errors = '\n------------------\n'.join(errors)
        if wizard.exportacio == 'all':
            wizard.write({'state': 'result', 'n_export': n_export,
                          'errors': errors})
        else:
            wizard_vals = {
                'n_reads': res_data['reads'],
                'n_max': res_data['max'],
                'n_extra': res_data['extra'],
                'state': 'result',
                'errors': errors,
            }
            wizard.write(wizard_vals)

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    _columns = {
        'facturacio': fields.selection([(1, 'Mensuals'),
                                        (2, 'Bimestrals'),
                                        (3, 'Totes')], 'Facturació',
                                        required=True),
        'exportacio': fields.selection([('all', 'Completa'),
                                        ('data', 'Lectures i extres')],
                                       'Exportació',
                                       required=True),
        'state': fields.selection([('init', 'Init'),
                                   ('confirm', 'Confirmació'),
                                   ('result', 'Resultat')], 'State'),
        'n_factures': fields.integer('Factures a exportar'),
        'n_export': fields.integer('Factures exportades'),
        'n_reads': fields.integer('Lectures exportades', readonly=True),
        'n_max': fields.integer('Maxímetres exportats', readonly=True),
        'n_extra': fields.integer('Conceptes extra exportats', readonly=True),
        'errors': fields.text('Errors produïts'),
    }

    _defaults = {
        'facturacio': lambda *a: 3,
        'exportacio': lambda *a: 'all',
        'state': lambda *a: 'init',
        'n_factures': lambda *a: 0,
        'n_export': lambda *a: 0,
        'n_reads': lambda *a: 0,
        'n_max': lambda *a: 0,
        'n_extra': lambda *a: 0,
    }

WizardExportToComer()
