# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
import csv
from datetime import datetime
from tools.translate import _
from giscedata_measures.utils import last_sunday
from dateutil.relativedelta import relativedelta
import pandas as pd
from giscedata_measures_comer.headers import OBJEAGCL_HEADER
from giscedata_measures.headers import AGCLACUM_HEADER, AGCLACUM5_HEADER

HOUR_CHANGE_MONTHS = [10]

FILE_SERVICE = {
    'CLINMEOS_': {
        'active': True, 'loader': 'import_clinmeos'
    },
    'AGCLOS_': {
        'active': True, 'loader': 'import_agclos'
    },
    'AGCL5OS_': {
        'active': True, 'loader': 'import_agcl5os'
    },
    'AGCLACUM_': {
        'active': True, 'loader': 'import_agclacum', 'header': AGCLACUM_HEADER
    },
    'AGCLACUM5_': {
        'active': True, 'loader': 'import_agclacum5', 'header': AGCLACUM5_HEADER
    },
    'AOBJEAGCL_': {
        'active': True, 'loader': 'import_aobjeagcl', 'header': OBJEAGCL_HEADER
    },

}


class MyDialect(csv.Dialect):
    lineterminator = '\n'
    delimiter = ';'


class CsvStruct(object):
    @property
    def reader_struct(self):
        return {
            'sep': ';',
            'index_col': False,
            'lineterminator': '\n',
            'dtype': {
                'distribuidora': str,
                'comercialitzadora': str,
                'agree_tarifa': str,
                'agree_tipo': str,
                'provincia': str
            },
        }


class GiscedataMeasuresLoader(osv.osv):

    _name = 'giscedata.measures.loader'
    _inherit = 'giscedata.measures.loader'

    def load_file(self, cursor, uid, ids, filename, data, context=None):
        file_type = self.get_filetype_by_filename(filename)
        importer = getattr(self, FILE_SERVICE[file_type]['loader'])
        importer(cursor, uid, filename, data, context)

    def import_clinmeos(self, cursor, uid, filename, data, context=None):
        """
        Importa un fitxer CLINMEOS
        """
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        clinmeos_obj = self.pool.get('giscedata.objeccions.clinmeos')
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')
        version = filename.split('.')[1]
        creation_date = filename.rsplit('_', 1)[1].split('.')[0]
        date_start = filename.rsplit('_')[-2]
        name = date_start
        date_start = datetime.strptime(date_start, '%Y%m')
        date_end = date_start + relativedelta(months=1)
        creation_date = datetime.strptime(creation_date, '%Y%m%d')

        start = datetime.now()
        periode_id = periode_obj.search(cursor, uid, [('name', '=', name)])
        # If period does not exist, create it
        if not periode_id:
            periode_id.append(
                periode_obj.create(
                    cursor, uid, {'name': name, 'data_inici': date_start,
                                  'data_final': date_end})
            )

        if clinmeos_obj.search(cursor, uid, [
            ('name', '=', filename), ('versio', '=', version)
        ]):
            _msg = _(u"Ja existeix un fitxer amb aquest nom i versio")
            return {'state': 'done', 'info': _msg}
            # wizard.write(
            #     {'state': 'done', 'info': _msg})

        vals = {
            'periode': periode_id[0],
            'name': filename,
            'data_creacio': creation_date,
            'data_inici': date_start,
            'data_final': date_end,
            'versio': version,
        }
        # Create the new CLINEMOS file
        clinmeos_id = clinmeos_obj.create(cursor, uid, vals)

        line_ids = []
        # _file = StringIO.StringIO(base64.b64decode(wizard.file))
        _file = data
        reader = csv.reader(_file, delimiter=';')
        for row in reader:

            line = {
                'clinmeos_id': clinmeos_id,
                'cups': row[0],
                'distribuidora': row[1],
                'comercialitzadora': row[2],
                'agree_tensio': row[3],
                'agree_tarifa': row[4],
                'agree_dh': row[5],
                'agree_tipo': str(row[6]).zfill(2),
                'provincia': row[7],
                'mesura_ab': row[8],
                'data_inici': datetime.strptime(row[9], '%Y/%m/%d %H'),
                'data_final': datetime.strptime(row[10], '%Y/%m/%d %H'),
                'consum_ae': row[11],
                'consum_r1': row[12],
                'consum_r4': row[13],
                'n_estimats': row[14],
                'consum_ae_estimada': row[15],
            }

            # Create a new CLINMEOS_Line object for each row
            line_ids.append(
                clinmeos_line_obj.create(cursor, uid, line, context=context)
            )
        print('time: {}'.format(datetime.now() - start))

        return clinmeos_id

    def import_agclos(self, cursor, uid, agclos_id, data, context=None):
        """
        Importa un fitxer AGCLOS
        """
        agclos_line_obj = self.pool.get('giscedata.objeccions.agclos.line')
        agclos_obj = self.pool.get('giscedata.objeccions.agclos')
        agclos_id = context.get('agclos_id', False)
        if isinstance(agclos_id, (list, tuple)):
            agclos_id = agclos_id[0]
        period_name = agclos_obj.read(
            cursor, uid, agclos_id, ['periode'])['periode'][1]
        month_ = int(period_name[4:6])
        year_ = int(period_name[0:4])
        day_change = last_sunday(year_, month_)

        line_ids = []
        reader = csv.reader(data, delimiter=';')
        for row in reader:
            year = int(row[0])
            month = int(row[1])
            day = int(row[2])
            hour = int(row[3])
            if month in HOUR_CHANGE_MONTHS and day == day_change and hour > 2:
                hour -= 1
            if hour == 24:
                hour = 0
                day, month, year = self.get_next_day(
                    day, month, year
                )
            the_date = datetime(int(year), int(month), int(day), int(hour))
            line = {
                'agclos_id': agclos_id,
                'timestamp': the_date,
                'magnitud': row[4],
                'distribuidora': row[5],
                'comercialitzadora': row[6],
                'agree_tensio': row[7],
                'agree_tarifa': row[8],
                'agree_dh': row[9],
                'agree_tipo': str(row[10]).zfill(2),
                'provincia': row[11],
                'consum': int(row[12]),
                'n_punts': int(row[13]),
                'origen': int(row[14]),
                'indicador_firmeza': row[15],
            }
            line_ids.append(
                agclos_line_obj.create(cursor, uid, line, context=context)
            )

    def import_agcl5os(self, cursor, uid, filename, data, context=None):
        """
        Importa un fitxer AGCL5OS
        """
        agcl5os_line_obj = self.pool.get('giscedata.objeccions.agcl5os.line')
        agcl5os_id = context.get('agcl5os_id', False)
        if isinstance(agcl5os_id, (list, tuple)):
            agcl5os_id = agcl5os_id[0]
        line_ids = []
        reader = csv.reader(data, delimiter=';')
        for row in reader:
            line = {
                'agcl5os_id': agcl5os_id,
                'distribuidora': row[0],
                'comercialitzadora': row[1],
                'agree_tensio': row[2],
                'agree_tarifa': row[3],
                'agree_dh': row[4],
                'agree_tipo': str(row[5]).zfill(2),
                'provincia': row[6],
                'timestamp': row[7],
                'season': row[8],
                'consum_ae': int(row[9]),
                'n_punts': int(row[10]),
                'consum_ae_real': int(row[11]),
                'n_real': int(row[12]),
                'consum_ae_estimada': int(row[13]),
                'n_estimats': int(row[14]),
            }
            line_ids.append(
                agcl5os_line_obj.create(cursor, uid, line, context=context)
            )

    def import_agclacum(self, cursor, uid, filename, data, context=None):
        """
        Importa un fitxer AGCLACUM
        """
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        agclacum_obj = self.pool.get(
            'giscedata.objeccions.agclacum')
        agclacum_line_obj = self.pool.get(
            'giscedata.objeccions.agclacum.line')

        period_name = filename.split('.')[0][-6:]
        version = filename.split('.')[1]
        periode_id = periode_obj.search(cursor, uid, [('name', '=', period_name)])[0]
        if not periode_id:
            osv.except_osv('Error', _(u'No existeix aquest Perode'))
        agclacum_id = agclacum_obj.create(
            cursor, uid, {'name': filename, 'periode': periode_id, 'data_inici': datetime.now(), 'versio': version}
        )

        f = CsvStruct()
        df_agclacum = pd.read_csv(data, names=AGCLACUM_HEADER, **f.reader_struct)
        for line in df_agclacum.T.to_dict().values():
            line.update({'agclacum_id': agclacum_id})
            agclacum_line_obj.create(cursor, uid, line, context=context)

    def import_agclacum5(self, cursor, uid, filename, data, context=None):
        """
        Importa un fitxer AGCLACUM5
        """
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        agclacum_obj = self.pool.get(
            'giscedata.objeccions.agclacum')
        agclacum_line_obj = self.pool.get(
            'giscedata.objeccions.agclacum.line')

        period_name = filename.split('.')[0][-6:]
        version = filename.split('.')[1]
        periode_id = periode_obj.search(cursor, uid, [('name', '=', period_name)])[0]
        if not periode_id:
            osv.except_osv('Error', _(u'No existeix aquest Perode'))
        agclacum5_id = agclacum_obj.create(
            cursor, uid, {'name': filename, 'periode': periode_id, 'data_inici': datetime.now(), 'versio': version}
        )

        f = CsvStruct()
        df_agclacum = pd.read_csv(data, names=AGCLACUM5_HEADER, **f.reader_struct)
        for line in df_agclacum.T.to_dict().values():
            line.update({'agclacum_id': agclacum5_id})
            agclacum_line_obj.create(cursor, uid, line, context=context)

    def import_aobjeagcl(self, cursor, uid, filename, data, context=None):
        """
        Importa un fitxer AOBJEAGCL
        """
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        aobjeagcl_line_obj = self.pool.get(
            'giscedata.objeccions.aobjeagcl.line')

        date_start = filename.rsplit('_')[-2]
        name = date_start

        start = datetime.now()
        periode_id = periode_obj.search(cursor, uid, [('name', '=', name)])[0]
        if not periode_id:
            osv.except_osv('Error', _(u'No existeix aquest Perode'))

        reader = csv.reader(data, delimiter=';')
        for row in reader:
            print(row)
            line = {
                'periode_id': periode_id,
                'codi_objeccio': row[0],
                'distribuidora': row[1],
                'comercialitzadora': row[2],
                'agree_tensio': row[3],
                'agree_tarifa': row[4],
                'agree_dh': row[5],
                'agree_tipo': str(row[6]).zfill(2),
                'provincia': row[7],
                'data_inici': datetime.strptime(row[8], '%Y%m%d %H'),
                'data_final': datetime.strptime(row[9], '%Y%m%d %H'),
                'motiu': row[10],
                'consum_publicat': row[11],
                'consum_proposat': row[12],
                'descripcio': row[13],
                'objeccio_auto': True if row[15] == 'S' else False,
                'acceptacio': True if row[15] == 'S' else False,
                'no_acceptacio_motiu': row[16],
                'comentari_emisor_resposta': row[17],
            }
            aobjeagcl_line_obj.create(cursor, uid, line)
        periode_obj.write(
            cursor, uid, periode_id,
            {'ultima_carrega_respostes': start.strftime(
                '%d-%m-%Y %H:%M:%S')}
        )

    @staticmethod
    def get_next_day(day, month, year):
        the_date = datetime.strptime(
            str(year) + str(month) + str(day), '%Y%m%d')
        the_date = the_date + relativedelta(days=1)
        day = str(the_date.day).zfill(2)

        month = str(the_date.month).zfill(2)
        year = str(the_date.year)

        return day, month, year

    @staticmethod
    def get_filetype_by_filename(filename):
        for k in FILE_SERVICE.keys():
            if k == filename.split('_', 1)[0].upper() + '_':
                return k
        raise NameError()


GiscedataMeasuresLoader()
