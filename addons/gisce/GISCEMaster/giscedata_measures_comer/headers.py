# -*- coding: utf-8 -*-
OBJECTION_REASON = [
    ("999", "999 - Otros"),
    ("000", "000 - La agregación no pertenece a este comercializador en el mes "
            "que se está liquidando."),
    ("001", "001 - La agregación pertenece a este comercializador, y sin "
            "embargo no se ha publicado nada para el mes que se está liquidando"
     ),
    ("002", "002 - Fecha inicio vigencia de la agregación incorrecta"),
    ("003", "003 - Fecha fin vigencia de la agregación incorrecta"),
    ("004", "004 - No son correctos los puntos frontera participantes en "
            "esta agregación"),
    ("102", "102 - Medida energía R1 (kVArh) de la agregación incorrecta"),
    ("103", "103 - Medida energía R2 (kVArh) de la agregación incorrecta"),
    ("100", "100 - Medida energía AE (kWh) de la agregación incorrecta"),
    ("101", "101 - Medida energía AS (kWh) de la agregación incorrecta"),
    ("106", "106 - Perfil incorrecto"),
    ("107", "107 - No necesita perfil"),
    ("104", "104 - Medida energía R3 (kVArh) de la agregación incorrecta"),
    ("105", "105 - Medida energía R4 (kVArh) de la agregación incorrecta"),
    ("700", "700 - No se dispone de datos de esta agregación en el "
            "fichero CLINME"),
    ("801", "801 - Duplicidad de puntos en CLINME"),
    ("800", "800 - Incoherencia CLINME - Agregación"),
    ("any_reason", "Cualquier Motivo")
]

AGGREGATION = [
    'comercialitzadora', 'distribuidora', 'agree_tensio', 'agree_tarifa',
    'agree_dh', 'agree_tipo', 'provincia'
]

OBJEAGCL_HEADER = [
    'distribuidora', 'comercialitzadora', 'agree_tensio', 'agree_tarifa',
    'agree_dh', 'agree_tipo', 'provincia', 'data_inici', 'data_final', 'motiu',
    'consum_ae', 'consum_proposat', 'descripcio', 'autobjeccio'
]

REVAC_HEADER = [
    'codi_objeccio', 'consum_proposat', 'motiu_revisio', 'indicatiu_noves_dades'
]
