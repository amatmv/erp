# -*- coding: utf-8 -*-
{
  "name": "Objeccions",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Mesures REE a comercialitzadora (Objeccions)
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      "giscedata_measures",
      "giscedata_profiles_comer",
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
        "wizard/wizard_importar_fitxer_objeccions_view.xml",
        "wizard/wizard_open_objections_view.xml",
        "wizard/wizard_check_objections_view.xml",
        "wizard/wizard_objection_view_file_view.xml",
        "giscedata_measures_view.xml",
        "wizard/wizard_create_objections_view.xml",
        "wizard/wizard_objection_reason_selection_view.xml",
        "wizard/wizard_objection_integrity_view.xml",
        "giscedata_measures_data.xml",
        "security/ir.model.access.csv"
  ],
  "active": False,
  "installable": True
}
