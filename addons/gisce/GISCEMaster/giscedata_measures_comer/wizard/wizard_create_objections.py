# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from base64 import b64encode
from ..headers import OBJECTION_REASON, OBJEAGCL_HEADER
import pandas as pd
import tempfile

class CreateObjections(osv.osv_memory):

    _name = 'giscedata.create.objections'

    def _default_info(self, cursor, uid, context=None):
        """Retorna la infomració per defecte, mostrant els CUPS seleccionats
        a objectar
        """
        if context is None:
            context = {}

        obj_objeccions = self.pool.get('giscedata.objeccions.comparativa.linia')
        active_id = context.get('active_id', False)
        if not active_id:
            return ''

        cl_comparativa = obj_objeccions.read(
            cursor, uid, active_id, ['clinmeos_id_comparativa']
        )['clinmeos_id_comparativa'][0]
        objection_ids = obj_objeccions.search(cursor, uid, [
            ('motiu', '!=', False),
            ('clinmeos_id_comparativa', '=', cl_comparativa)
        ])

        cups = obj_objeccions.read(
            cursor,
            uid,
            objection_ids,
            ['cups'],
            context=context
        )
        info = _(u"S'objectarán els següents {0} CUPS: {1}").format(
            len(objection_ids),
            ', '.join([x['cups'] for x in cups])
        )

        return info

    def _default_result(self, cursor, uid, context=None):
        """Retorna la informació a mostrar al final del wizard
        """
        info = _(u"Fitxer OBJEINME generat correctament, revisar cas a la "
                 u"secció d'objeccions")

        return info

    def _get_file_mode(self, cursor, uid, context=None):
        from_model = context.get('from_model', [])
        if from_model == 'giscedata.objeccions.comparativa.linia':
            return 'OBJEINME'
        elif from_model == 'giscedata.objeccions.comparativa.linia.agclos':
            return 'OBJEAGCL'
        elif from_model == 'giscedata.objeccions.comparativa.linia.agcl5os':
            return 'OBJEAGCL5'
        else:
            return None

    def increment_version(self, cursor, uid, ids, filename, periode_id, context=None):
        if not context:
            context = {}

        attach_obj = self.pool.get('ir.attachment')
        check_filename = filename.split('.')[0]
        file_ids = attach_obj.search(cursor, uid, [
            ('name', '=like', check_filename + '%'),
            ('res_id', '=', periode_id),
            ('res_model', '=', 'giscedata.objeccions.periode')
        ])
        if file_ids:
            version = attach_obj.read(
                cursor, uid, file_ids, ['name']
            )
            version = [x['name'].split('.')[1] for x in version]
            version = max(version)
            file_version = str(int(version) + 1)
            filename = (filename.split('.')[0] + '.' + file_version)
        return filename

    def create_file(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        if self._get_file_mode(cursor, uid, context=context) == 'OBJEINME':
            self.gen_objeinme_all_types(cursor, uid, ids, context=context)
        elif self._get_file_mode(cursor, uid, context=context) == 'OBJEAGCL':
            self.gen_objeagcl_34_types(cursor, uid, ids, context=context)
        elif self._get_file_mode(cursor, uid, context=context) == 'OBJEAGCL5':
            self.gen_objeagcl_5_types(cursor, uid, ids, context=context)
        else:
            self.gen_files(cursor, uid, ids, context=context)

    def gen_files(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        objeagcl_obj = self.pool.get('giscedata.objeccions.objeagcl')
        objeinme_obj = self.pool.get('giscedata.objeccions.objeinme')
        crm_section_obj = self.pool.get('crm.case.section')
        attach_obj = self.pool.get('ir.attachment')
        crm_obj = self.pool.get('crm.case')
        wizard = self.browse(cursor, uid, ids[0], context)

        agclos_id = None
        agcl5os_id = None
        clinmeos_id = wizard.clinmeos_file.id
        periode_id = context.get('active_id', False)
        gen_files = []
        if wizard.agcl5os_file:
            agcl5os_id = wizard.agcl5os_file.id
        if wizard.agclos_file:
            agclos_id = wizard.agclos_file.id
        if not wizard.agclos_file and not wizard.agcl5os_file:
            args_ = []
        else:
            args_ = objeagcl_obj.gen_objeagcl(
                cursor, uid, ids, periode_id=periode_id, agclos_id=agclos_id, agcl5os_id=agcl5os_id, auto_obj=False,
                context=context
            )

        for arg_ in args_:
            with open(arg_['filename'], "rb") as file_:
                content = b64encode(file_.read())
            filename = 'OBJEAGCL_{comer}_{distri}_{periode}_{data}.{version}'.format(**arg_)
            filename = self.increment_version(cursor, uid, ids, filename, periode_id, context=context)
            vals = {
                'name': filename,
                'datas_fname': filename,
                'res_id': periode_id,
                'res_model': 'giscedata.objeccions.periode',
                'datas': content,
                'description': arg_['origin_name']
            }
            attach_obj.create(cursor, uid, vals)
            gen_files.append(filename)

        # OBJEINME
        args_ = objeinme_obj.gen_objeinme(
            cursor, uid, ids, periode_id=periode_id, clinmeos_id=clinmeos_id, auto_obj=False, context=context
        )
        for arg_ in args_:
            with open(arg_['filename'], "rb") as file_:
                content = b64encode(file_.read())
            filename = 'OBJEINME_{comer}_{distri}_{periode}_{data}.{version}'.format(**arg_)
            filename = self.increment_version(cursor, uid, ids, filename, periode_id, context=context)
            # objeinme_obj.create(cursor, uid, {
            #     'name': filename.split('.')[0],
            #     'versio': file_version,
            #     'periode': periode_id,
            # })

            # Create crm case
            section_id = crm_section_obj.search(
                cursor, uid, [('code', '=', 'OBJEC')]
            )[0]
            crm_case_vals = {
                'name': 'Fitxer objeccions {0}'.format(filename),
                'section_id': section_id,
                'description': filename,
            }
            case_id = crm_obj.create(cursor, uid, crm_case_vals)
            vals = {
                'name': filename,
                'datas_fname': filename,
                'res_id': case_id,
                'res_model': 'crm.case',
                'datas': content,
                'description': arg_['origin_name']
            }
            # Create attachment into period
            vals_period = {
                'name': filename,
                'datas_fname': filename,
                'res_id': periode_id,
                'res_model': 'giscedata.objeccions.periode',
                'datas': content,
                'description': arg_['origin_name']
            }
            attach_obj.create(cursor, uid, vals)
            attach_obj.create(cursor, uid, vals_period)
            gen_files.append(filename)
        msg_ = _(u'Fitxers generats: \n{}'.format(', '.join(gen_files)))
        wizard.write({'state': 'done', 'result': msg_})

    def set_description(self, cursor, uid, ids, vals, context=None):
        vals['description'] = 'file'#file_.name

    def gen_objeagcl_34_types(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia.agclos'
        )
        attach_obj = self.pool.get('ir.attachment')
        agclos_obj = self.pool.get('giscedata.objeccions.agclos')
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        wizard = self.browse(cursor, uid, ids[0], context)
        active_id = context.get('active_id', False)

        cl_comparativa = comparativa_linia_obj.read(
            cursor, uid, active_id, ['agclos_id_comparativa']
        )['agclos_id_comparativa'][0]
        objection_ids = comparativa_linia_obj.search(cursor, uid, [
            ('motiu', '!=', False),
            ('agclos_id_comparativa', '=', cl_comparativa)
        ])
        file_id = comparativa_linia_obj.read(
            cursor, uid, objection_ids, ['agclos_id_comparativa']
        )[0]['agclos_id_comparativa'][0]
        periode_id = agclos_obj.read(
            cursor, uid, file_id, ['periode']
        )['periode'][0]
        periode = periode_obj.read(cursor, uid, periode_id, ['name'])['name']

        objections_data = comparativa_linia_obj.read(
            cursor, uid, objection_ids, OBJEAGCL_HEADER
        )
        df_objeagcl = pd.DataFrame(data=objections_data)
        if not wizard.objeccio_auto:
            auto_objection = 'N'
        else:
            auto_objection = 'S'

        # Prepare df
        df_objeagcl['autobjeccio'] = auto_objection
        date_keys = ['data_inici', 'data_final']
        for key in date_keys:
            df_objeagcl[key] = df_objeagcl[key].replace(False, '')
            df_objeagcl[key] = df_objeagcl[key].fillna('')
            df_objeagcl[key] = df_objeagcl[key].apply(lambda x: x[:13].replace('-', '/'))
        comer = list(df_objeagcl['comercialitzadora'])[0]
        distris = list(set(df_objeagcl['distribuidora']))
        for distri in distris:
            df_by_distri = df_objeagcl[
                df_objeagcl['distribuidora'] == str(distri)]
            df_by_distri = df_by_distri.replace(False, '')
            _id, filename = tempfile.mkstemp(suffix='.csv')
            df_by_distri.to_csv(
                filename, sep=';', header=False, columns=OBJEAGCL_HEADER,
                index=False, line_terminator=';\n'
            )

            with open(filename, "rb") as _file:
                content = b64encode(_file.read())
            filename = 'OBJEAGCL_{comer}_{distri}_{periode}_{date}.{v}'.format(
                comer=comer, distri=distri, periode=periode,
                date=datetime.now().strftime('%Y%m%d'), v='1'
            )
            vals = {
                'name': filename,
                'datas_fname': filename,
                'res_id': periode_id,
                'res_model': 'giscedata.objeccions.periode',
                'datas': content,
            }
            attach_obj.create(cursor, uid, vals)

        wizard.write({'state': 'done'})

    def gen_objeagcl_5_types(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia.agcl5os'
        )
        attach_obj = self.pool.get('ir.attachment')
        agcl5os_obj = self.pool.get('giscedata.objeccions.agcl5os')
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        wizard = self.browse(cursor, uid, ids[0], context)
        active_id = context.get('active_id', False)

        cl_comparativa = comparativa_linia_obj.read(
            cursor, uid, active_id, ['agcl5os_id_comparativa']
        )['agcl5os_id_comparativa'][0]
        objection_ids = comparativa_linia_obj.search(cursor, uid, [
            ('motiu', '!=', False),
            ('agcl5os_id_comparativa', '=', cl_comparativa)
        ])
        file_id = comparativa_linia_obj.read(
            cursor, uid, objection_ids, ['agcl5os_id_comparativa']
        )[0]['agcl5os_id_comparativa'][0]
        periode_id = agcl5os_obj.read(
            cursor, uid, file_id, ['periode']
        )['periode'][0]
        periode = periode_obj.read(cursor, uid, periode_id, ['name'])['name']

        objections_data = comparativa_linia_obj.read(
            cursor, uid, objection_ids, OBJEAGCL_HEADER
        )
        df_objeagcl = pd.DataFrame(data=objections_data)
        if not wizard.objeccio_auto:
            auto_objection = 'N'
        else:
            auto_objection = 'S'

        # Prepare df
        df_objeagcl['autobjeccio'] = auto_objection
        date_keys = ['data_inici', 'data_final']
        for key in date_keys:
            df_objeagcl[key] = df_objeagcl[key].replace(False, '')
            df_objeagcl[key] = df_objeagcl[key].fillna('')
            df_objeagcl[key] = df_objeagcl[key].apply(lambda x: x[:13].replace('-', '/'))
        comer = list(df_objeagcl['comercialitzadora'])[0]
        distris = list(set(df_objeagcl['distribuidora']))
        for distri in distris:
            df_by_distri = df_objeagcl[
                df_objeagcl['distribuidora'] == str(distri)]
            df_by_distri = df_by_distri.replace(False, '')
            _id, filename = tempfile.mkstemp(suffix='.csv')
            df_by_distri.to_csv(
                filename, sep=';', header=False, columns=OBJEAGCL_HEADER,
                index=False, line_terminator=';\n'
            )

            with open(filename, "rb") as _file:
                content = b64encode(_file.read())
            filename = 'OBJEAGCL_{comer}_{distri}_{periode}_{date}.{v}'.format(
                comer=comer, distri=distri, periode=periode,
                date=datetime.now().strftime('%Y%m%d'), v='1'
            )
            vals = {
                'name': filename,
                'datas_fname': filename,
                'res_id': periode_id,
                'res_model': 'giscedata.objeccions.periode',
                'datas': content,
            }
            attach_obj.create(cursor, uid, vals)

        wizard.write({'state': 'done'})

    def gen_objeinme_all_types(self, cursor, uid, ids, context=None):
        """Crea un fitxer OBJEINME per distribuidora i motiu. Crea un cas
        CRM i un fitxer per cada distribuidora. n casos y n fitxers per n
        distribuidores.
        """
        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        clinmeos_obj = self.pool.get('giscedata.objeccions.clinmeos')
        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia'
        )
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')
        attach_obj = self.pool.get('ir.attachment')
        crm_obj = self.pool.get('crm.case')
        crm_section_obj = self.pool.get('crm.case.section')

        active_id = context.get('active_id', False)
        file_id = comparativa_linia_obj.read(
            cursor, uid, active_id, ['clinmeos_id_comparativa']
        )['clinmeos_id_comparativa'][0]
        objection_ids = comparativa_linia_obj.search(cursor, uid, [
            ('motiu', '!=', False),
            ('clinmeos_id_comparativa', '=', file_id)
        ])
        periode_id = clinmeos_obj.read(
            cursor, uid, file_id, ['periode']
        )['periode'][0]
        objections_data = comparativa_linia_obj.read(
            cursor, uid, objection_ids,
            ['cups', 'consum_ae', 'consum_calculat', 'distribuidora', 'motiu',
             'descripcio', 'clinmeos_id_comparativa', 'clinmeos_line_id']
        )

        objections_by_distri = {}
        for a_cups in objections_data:
            cups = a_cups['cups']
            distri = a_cups['distribuidora']

            if distri not in objections_by_distri:
                objections_by_distri[distri] = []
            objections_by_distri[distri].append(a_cups)

        clinmeos_id = objections_data[0]['clinmeos_id_comparativa'][0]
        # toDo fetch aggregated data (based on distri) and generate N files
        clinmeos_data = clinmeos_obj.read(
            cursor, uid, clinmeos_id, ['data_inici', 'data_final', 'periode']
        )
        date_objection_init = clinmeos_data['data_inici'].replace('-', '') + ' 01'
        date_objection_end = clinmeos_data['data_final'].replace('-', '') + ' 00'
        if not wizard.objeccio_auto:
            auto_objection = 'N'
        else:
            auto_objection = 'S'

        clinmeos_lines = clinmeos_obj.read(
            cursor, uid, clinmeos_id, ['lines']
        )['lines']

        for distri in objections_by_distri:
            a_distri = objections_by_distri[distri]
            objeinme_lines = []
            for line in a_distri:
                if not line['descripcio']:
                    comment = ""
                else:
                    comment = line['descripcio']
                objeinme_lines.append(';'.join(
                    [
                        line['cups'],
                        date_objection_init,
                        date_objection_end,
                        line['motiu'],
                        str(int(line['consum_ae'])),
                        str(int(line['consum_calculat'])),
                        comment,
                        auto_objection + ';',
                    ]
                ))
            file_target_date = clinmeos_data['data_inici'].replace('-', '')[:6]
            file_generation_date = datetime.today().strftime("%Y%m%d")
            file_version = "0"

            lines_data = clinmeos_line_obj.read(
                cursor, uid, clinmeos_lines, [
                    'distribuidora', 'comercialitzadora', 'data_inici'
                ]
            )[0]
            # toDo create file in memory and save as DB attachment
            objeinme_filename = 'OBJEINME_{0}_{1}_{2}_{3}.{4}'.format(
                lines_data['comercialitzadora'], distri,
                file_target_date, file_generation_date, file_version)

            file_content = ""
            for elem in objeinme_lines:
                file_content += elem + '\n'

            objeinme_filename = self.increment_version(cursor, uid, ids, objeinme_filename, periode_id, context=context)
            section_id = crm_section_obj.search(
                cursor, uid, [('code', '=', 'OBJEC')]
            )[0]
            crm_case_vals = {
                'name': 'Fitxer objeccions {0}'.format(objeinme_filename),
                'section_id': section_id,
                'description': objeinme_filename,
            }
            # Create crm case
            case_id = crm_obj.create(cursor, uid, crm_case_vals)

            vals = {
                'name': objeinme_filename,
                'datas_fname': objeinme_filename,
                'res_id': case_id,
                'res_model': 'crm.case',
                'datas': b64encode(file_content),
            }
            vals_period = {
                'name': objeinme_filename,
                'datas_fname': objeinme_filename,
                'res_id': periode_id,
                'res_model': 'giscedata.objeccions.periode',
                'datas': b64encode(file_content),
            }
            attach_obj.create(cursor, uid, vals)
            attach_obj.create(cursor, uid, vals_period)
        wizard.write({'state': 'done'})

    _columns = {
        'objeccio_motiu': fields.selection(OBJECTION_REASON, 'Motiu'),
        'comentari': fields.text('Comentari'),
        'objeccio_auto': fields.boolean(
            'Auto-objecció', help='Objecció a auto-objecció'
        ),
        'info': fields.text('CUPS seleccionats', readonly=True),
        'state': fields.char('State', size=4),
        'result': fields.text('Resultat', readonly=True),
        # 'agclos_files': fields.many2many(
        #     'giscedata.objeccions.agclos', 'create_objections_agclos_rel',
        #     'create_objections_id', 'agclos_id', 'AGCLOS'
        # ),
        # 'agcl5os_files': fields.many2many(
        #     'giscedata.objeccions.agcl5os', 'create_objections_agcl5os_rel',
        #     'create_objections_id', 'agcl5os_id', 'AGCL5OS'
        # ),
        # 'clinmeos_files': fields.many2many(
        #     'giscedata.objeccions.clinmeos', 'create_objections_clinmeos_rel',
        #     'create_objections_id', 'clinmeos_id', 'CLINMEOS',
        #     domain="[('state', '=', 'done'), ('periode', '=', active_ids[0)]"
        # ),
        'agclos_file': fields.many2one(
            'giscedata.objeccions.agclos', 'AGCLOS',
            domain=[('state', '=', 'done')]),
        'agcl5os_file': fields.many2one(
            'giscedata.objeccions.agcl5os', 'AGCL5OS',
            domain=[('state', '=', 'done')]
        ),
        'clinmeos_file': fields.many2one(
            'giscedata.objeccions.clinmeos', 'CLINMEOS', required=True,
            domain=[('state', '=', 'done')]
        )
    }

    _defaults = {
        #'info': _default_info,
        #'result': _default_result,
        'objeccio_motiu': lambda *a: '999',
        'state': lambda *a: 'init',
    }


CreateObjections()
