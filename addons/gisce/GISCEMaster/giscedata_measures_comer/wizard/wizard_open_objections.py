# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class OpenObjections(osv.osv_memory):

    def _default_file(self, cursor, uid, context=None):
        """Si existeix un sol fitxer per període el retorna per defecte,
        altrament retorna un fitxer no existent, per activar l'onchange de
        selection_file i així filtrar els fitxers del període. Això es fa per
        no confondre el fitxer pel qual es vol revisar dades
        :return: clinmeos_id o agcl5os_id o agclos_id o agclacum_id o agclacum5_id depen del model
        """
        period_obj = self.pool.get('giscedata.objeccions.periode')
        period_id = context.get('active_id', [])
        from_model = context.get('from_model', [])
        _file_ids = None
        file_key = None
        if from_model == 'giscedata.objeccions.clinmeos':
            file_key = 'clinmeos_ids'
        elif from_model == 'giscedata.objeccions.agcl5os':
            file_key = 'agcl5os_ids'
        elif from_model == 'giscedata.objeccions.agclos':
            file_key = 'agclos_ids'
        elif from_model == 'giscedata.objeccions.agclacum':
            file_key = 'agclacum_ids'

        if file_key:
            _file_ids = period_obj.read(
                cursor, uid, period_id, [file_key]
            )[file_key]

        if _file_ids:
            if len(_file_ids) > 1:
                return False
            else:
                return _file_ids[0]
        else:
            return False

    def _default_model_state(self, cursor, uid, context):
        if context is None:
            context = {}

        from_model = context.get('from_model', [])
        if from_model == 'giscedata.objeccions.clinmeos':
            return 'clinmeos_mode'
        elif from_model == 'giscedata.objeccions.agcl5os':
            return 'agcl5os_mode'
        elif from_model == 'giscedata.objeccions.agclos':
            return 'agclos_mode'
        elif from_model == 'giscedata.objeccions.agclacum':
            return 'agclacum_mode'
        else:
            return 'clinmeos_mode'

    def _default_info(self, cursor, uid, context):
        return _(u'Calculant en segon pla, revisar '
                 u'estat de processament del fitxer')

    def selection_file(self, cursor, uid, ids, context=None):
        """Retorna unicament els fitxers d'aquest periode
        :param context: dict with active_ids
        :return: dict like {
            'value': {},
            'domain': {'file': list_of_files},
            'warning': {}
            }
        """
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        periode = context.get('active_ids', [])
        from_model = context.get('from_model', [])
        file_column = 'file_clinmeos'
        if from_model == 'giscedata.objeccions.agcl5os':
            file_column = 'file_agcl5os'
        elif from_model == 'giscedata.objeccions.agclos':
            file_column = 'file_agclos'
        elif from_model == 'giscedata.objeccions.agclacum':
            file_column = 'file_agclacums'

        _file_obj = self.pool.get(from_model)
        if periode:
            periode = periode[0]
            # Get the clinmeos:ids or agcl5os:ids
            _file_ids = _file_obj.search(
                cursor, uid, [('periode', '=', periode)]
            )

            res['domain'].update(
                {file_column: [('id', 'in', _file_ids)]}
            )
        else:
            res['domain'].update({file_column: []})

        return res

    def filter_lines(self, cursor, uid, ids, context=None):
        """Obre una nova pestanya amb les linies a objectar
        """
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        from_model = context.get('from_model', [])
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        res_model = 'giscedata.objeccions.comparativa.linia'
        domain = [('clinmeos_id_comparativa', '=', wizard.file_clinmeos.id)]

        if not wizard.consult_objections:
            if from_model == 'giscedata.objeccions.clinmeos':
                _file = wizard.file_clinmeos
                res_model = 'giscedata.objeccions.comparativa.linia'
                if wizard.background:
                    periode_obj.contrast_consumption_async(cursor, uid, _file.id)
                    _file.write({'state': 'processing'})
                    wizard.write({'model_state': 'end'})
                    return True
                else:
                    periode_obj.contrast_consumption(cursor, uid, _file.id)

            elif from_model == 'giscedata.objeccions.agcl5os':
                _file = wizard.file_agcl5os
                domain = [
                    ('agcl5os_id_comparativa', '=', _file.id)
                ]
                res_model = 'giscedata.objeccions.comparativa.linia.agcl5os'
                if wizard.background:
                    periode_obj.contrast_agcl5os_async(cursor, uid, _file.id, context=context)
                    _file.write({'state': 'processing'})
                    wizard.write({'model_state': 'end'})
                    return True
                else:
                    periode_obj.contrast_agcl5os(cursor, uid, _file.id, context=context)
            elif from_model == 'giscedata.objeccions.agclos':
                _file = wizard.file_agclos
                domain = [
                    ('agclos_id_comparativa', '=', _file.id)
                ]
                res_model = 'giscedata.objeccions.comparativa.linia.agclos'
                if wizard.background:
                    periode_obj.contrast_agcl5os_async(cursor, uid, _file.id, context=context)
                    _file.write({'state': 'processing'})
                    wizard.write({'model_state': 'end'})
                    return True

                else:
                    periode_obj.contrast_agcl5os(cursor, uid, _file.id, context=context)
            elif from_model == 'giscedata.objeccions.agclacum':
                _file = wizard.file_agclacums
                agclacums_ids = [x.id for x in wizard.file_agclacums]
                domain = [
                    ('fitxer_id_comparativa', 'in', agclacums_ids)
                ]
                filename = ' + '.join([x.name for x in wizard.file_agclacums])
                _file.name = filename
                res_model = 'giscedata.objeccions.comparativa.linia.agregacions'
                if wizard.background:
                    periode_obj.contrast_aggregations_async(cursor, uid, agclacums_ids, 'agclacum', context=context)
                    self.pool.get('giscedata.objeccions.agclacum').write(
                        cursor, uid, agclacums_ids, {'state': 'processing'}
                    )
                    wizard.write({'model_state': 'end'})
                    return True

                else:
                    periode_obj.contrast_aggregations(cursor, uid, agclacums_ids, 'agclacum', context=context)

        vals_view = {
            'name': _(u"Línies d'objeccions {}").format(_file.name),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': res_model,
            'limit': 80,
            'type': 'ir.actions.act_window',
            'domain': domain,
        }

        return vals_view

    _name = 'giscedata.open.objections'

    _columns = {
        'file_clinmeos': fields.many2one(
            'giscedata.objeccions.clinmeos', 'CLINMEOS'
        ),
        'file_agcl5os': fields.many2one(
            'giscedata.objeccions.agcl5os', 'AGCL5OS'
        ),
        'file_agclos': fields.many2one(
            'giscedata.objeccions.agclos', 'AGCLOS'
        ),
        'file_agclacums': fields.many2many(
            'giscedata.objeccions.agclacum', 'giscedata_agclacum_rel', 'agclacum_id', 'id', 'AGCLACUMs'
        ),
        'consult_objections': fields.boolean(
            "Consultar dades ja contrastades"
        ),
        'model_state': fields.text('Model state'),
        'background': fields.boolean(
            "Segon pla", help='Contrastar consums en segon pla'
        ),
        'info': fields.text('Observacions', readonly=True),
    }

    _defaults = {
        'file_clinmeos': _default_file,
        'file_agcl5os': _default_file,
        'file_agclos': _default_file,
        'file_agclacums': _default_file,
        'model_state': _default_model_state,
        'info': _default_info,
        'consult_objections': lambda *x: False,
        'background': lambda *x: True,
    }


OpenObjections()
