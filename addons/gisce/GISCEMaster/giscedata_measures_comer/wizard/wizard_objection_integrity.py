# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
import pandas as pd
from tools.translate import _
from datetime import datetime
from ..headers import AGGREGATION
from tools.translate import _

class ObjectionIntegrity(osv.osv_memory):

    _name = 'objection.integrity'

    def _default_info(self, cursor, uid, context=None):
        info = _(
            u"SI ES MARCA ACTUALITZAR MOTIUS:\n"
            u"Aquesta contrastació es farà un cop hagin acabat les dels AGCLS i "
            u"CLINMEOS amb els següents inputs i comprovacions:\n\n- Comprovarà "
            u"que no hi hagi cap contrastació calculant-se en aquell període\n"
            u"- Comprovarà que hi hagi mínim una contrastació acabada en aquell "
            u"període\n- Si TOTS els CUPS tenen motius diferents, s'escriurà "
            u"motiu 999 a l'OBJEAGCL\n- Si TOTS els CUPS tenen un sol motiu, "
            u"s'escriurà aquest a l'OBJEAGCL\n- S'indicarà un diferencial "
            u"d'energia en valor absolut per poder marcar objecció "
            u"800 a totes les línies que siguin igual o superior a aquest\n- El "
            u"motiu 800 prevaldrà sobre els altres motius\n"
            u"- L'energia que es demana objectar dels CUPS es resta al total de l'agregació\n"
            u"- Agregarà per tant l'energia diferencial dels CUPS a Objectar als AGCLS\n"
            u"x = (consum imputat AGCL - (consum imputat CLINMEOS - consum proposat CLINMEOS))"
        )

        return info

    def _cnt_abs_diferencial(self, cursor, uid, ids):
        diff = self.read(cursor, uid, ids, ['diferencial'])[0]
        if diff['diferencial'] < 0:
            return False
        else:
            return True

    def _default_file(self, cursor, uid, context=None):
        """Retorna el fitxer CLINMEOS per defecte
        :return: clinmeos_id
        """
        period_obj = self.pool.get('giscedata.objeccions.periode')
        period_id = context.get('active_id', [])
        from_model = context.get('from_model', [])
        _file_ids = None
        if from_model == 'giscedata.objeccions.clinmeos':
            _file_ids = period_obj.read(
                cursor, uid, period_id, ['clinmeos_ids']
            )['clinmeos_ids']
        elif from_model == 'giscedata.objeccions.agcl5os':
            _file_ids = period_obj.read(
                cursor, uid, period_id, ['agcl5os_ids']
            )['agcl5os_ids']
        elif from_model == 'giscedata.objeccions.agclos':
            _file_ids = period_obj.read(
                cursor, uid, period_id, ['agclos_ids']
            )['agclos_ids']

        if _file_ids:
            return _file_ids[0]
        else:
            return False

    def _default_model_state(self, cursor, uid, context):
        if context is None:
            context = {}

        from_model = context.get('from_model', [])
        if from_model == 'giscedata.objeccions.clinmeos':
            return 'clinmeos_mode'
        elif from_model == 'giscedata.objeccions.agcl5os':
            return 'agcl5os_mode'
        elif from_model == 'giscedata.objeccions.agclos':
            return 'agclos_mode'
        else:
            return 'clinmeos_mode'

    def _default_periode(self, cursor, uid, context=None):
        """Lot de perfilació per defecte
        """
        if context is None:
            context = {}

        lot_id = context.get('active_ids', [])
        if lot_id:
            return lot_id[0]
        else:
            return False

    def on_change_periode(self, cursor, uid, ids, periode, context=None):
        """Filtra totes les pòlisses del contracte_lot_perfils
        """
        if context is None:
            context = {}

        file_models = [
            (self.pool.get('giscedata.objeccions.clinmeos'), 'file_clinmeos'),
            (self.pool.get('giscedata.objeccions.agclos'), 'file_agclos'),
            (self.pool.get('giscedata.objeccions.agcl5os'), 'file_agcl5os')
        ]

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if periode:
            # Get the clinmeos:ids/agclos:ids/agcl5os:ids
            for file_model in file_models:
                _file_ids = file_model[0].search(
                    cursor, uid, [('periode', '=', periode)]
                )
                res['domain'].update(
                    {file_model[1]: [('id', 'in', _file_ids)]}
                )

        return res

    def selection_file(self, cursor, uid, ids, context=None):
        """Retorna unicament els fitxers d'aquest periode
        :param context: dict with active_ids
        :return: dict like {
            'value': {},
            'domain': {'file': list_of_files},
            'warning': {}
            }
        """
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        periode = context.get('active_ids', [])
        from_model = context.get('from_model', [])
        file_column = 'file_clinmeos'
        if from_model == 'giscedata.objeccions.agcl5os':
            file_column = 'file_agcl5os'
        elif from_model == 'giscedata.objeccions.agclos':
            file_column = 'file_agclos'

        _file_obj = self.pool.get(from_model)
        if periode:
            periode = periode[0]
            # Get the clinmeos:ids or agcl5os:ids
            _file_ids = _file_obj.search(
                cursor, uid, [('periode', '=', periode)]
            )

            res['domain'].update(
                {file_column: [('id', 'in', _file_ids)]}
            )
        else:
            res['domain'].update({file_column: []})

        return res

    def update_reasons(self, cursor, uid, clinmeos_id, agclos_id, agcl5os_id, multiple_agg_reasons=False, context=None):
        if context is None:
            context = {}

        clinmeos_obj = self.pool.get('giscedata.objeccions.clinmeos')
        agclos_obj = self.pool.get('giscedata.objeccions.agclos')
        agcl5os_obj = self.pool.get('giscedata.objeccions.agcl5os')
        clinmeos_comp_line_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia')
        agclos_comp_line_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia.agclos')
        agcl5os_comp_line_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia.agcl5os')

        clinmeos_state = clinmeos_obj.read(
            cursor, uid, clinmeos_id, ['state'])['state']
        agclos_state = agclos_obj.read(
            cursor, uid, agclos_id, ['state'])['state']
        agcl5os_state = agcl5os_obj.read(
            cursor, uid, agcl5os_id, ['state'])['state']

        # Check states
        if (clinmeos_state != 'done' or agclos_state != 'done'
            or agcl5os_state != 'done'):
            msg = _(u"Les comparatives no están acabades")
            raise osv.except_osv('Error', msg)
        else:
            # Get aggregated CLINMEOS data with list of reasons
            base_aggregation = AGGREGATION[:]
            base_aggregation.remove('comercialitzadora')

            comp_clinmeos_line_ids = clinmeos_comp_line_obj.search(
                cursor, uid, [
                    ('motiu', '!=', False),
                    ('clinmeos_id_comparativa', '=', clinmeos_id)
                ]
            )
            read_params = base_aggregation + ['motiu', 'diferencia']
            comp_clinmeos_line_data = clinmeos_comp_line_obj.read(
                cursor, uid, comp_clinmeos_line_ids, read_params
            )
            # Construct a series LIKE (AGG): [reasons]
            df_check = pd.DataFrame(data=comp_clinmeos_line_data)
            df_check = df_check.groupby(base_aggregation).aggregate(
                {'diferencia': 'sum', 'motiu': lambda x: list(x)}).reset_index()
            for elem in df_check.T.to_dict().values():
                search_params = [
                    ('distribuidora', '=', elem['distribuidora']),
                    ('agree_tensio', '=', elem['agree_tensio']),
                    ('agree_tarifa', '=', elem['agree_tarifa']),
                    ('agree_dh', '=', elem['agree_dh']),
                    ('agree_tipo', '=', elem['agree_tipo']),
                    ('provincia', '=', elem['provincia']),
                ]
                v = list(set(elem['motiu']))
                diferencia = int(elem['diferencia'])

                if len(v) == 1:
                    # Only one reason
                    motiu = v[0]
                else:
                    # One line by reason
                    # or 999 reason
                    # Multiple reasons
                    if multiple_agg_reasons:
                        if elem['agree_tipo'] > '04':
                            type_ = 'agcl5os'
                            agcl_id = agcl5os_id
                        else:
                            type_ = 'agclos'
                            agcl_id = agclos_id
                        self.create_multiple_objection_lines(
                            cursor, uid, v, agcl_id, search_params, type_, context=context
                        )
                        continue
                    else:
                        # todo: utilizar solo la funcion de create_multiple_objection_lines
                        # todo: funciona ya también con un solo motivo
                        motiu = "999"

                # Search AGGS
                comp_agclos_id = agclos_comp_line_obj.search(
                    cursor, uid,
                    search_params + [('agclos_id_comparativa', '=', agclos_id)]
                )
                comp_agcl5os_id = agcl5os_comp_line_obj.search(
                    cursor, uid,
                    search_params +
                    [('agcl5os_id_comparativa', '=', agcl5os_id)]
                )
                if motiu == "001":
                    create_params = {}
                    for elem in search_params:
                        key = elem[0]
                        value = elem[2]
                        create_params[key] = value
                    create_params['motiu'] = motiu
                    if create_params['agree_tipo'] <= '04':
                        model_obj = agclos_comp_line_obj
                        # Preventive_delete
                        if comp_agclos_id:
                            model_obj.unlink(cursor, uid, comp_agclos_id)
                        create_params['agclos_id_comparativa'] = agclos_id
                    else:
                        model_obj = agcl5os_comp_line_obj
                        # Preventive_delete
                        if comp_agcl5os_id:
                            model_obj.unlink(cursor, uid, comp_agcl5os_id)
                        create_params['agcl5os_id_comparativa'] = agcl5os_id
                    # Create line
                    model_obj.create(cursor, uid, create_params)
                    continue
                # Update reasons in AGGS
                if comp_agclos_id:
                    comp_agclos_id = comp_agclos_id[0]
                    consum_imputat = agclos_comp_line_obj.read(
                        cursor, uid, comp_agclos_id, ['consum_ae']
                    )['consum_ae']
                    # proposed consumption x + (-y)
                    consum_proposat = consum_imputat + diferencia
                    agclos_comp_line_obj.write(
                        cursor, uid, comp_agclos_id, {
                            'motiu': motiu, 'consum_proposat': consum_proposat
                        }
                    )
                if comp_agcl5os_id:
                    comp_agcl5os_id = comp_agcl5os_id[0]
                    consum_imputat = agcl5os_comp_line_obj.read(
                        cursor, uid, comp_agcl5os_id, ['consum_ae']
                    )['consum_ae']
                    # proposed consumption x + (-y)
                    consum_proposat = consum_imputat + diferencia
                    agcl5os_comp_line_obj.write(
                        cursor, uid, comp_agcl5os_id, {
                            'motiu': motiu, 'consum_proposat': consum_proposat
                        }
                    )

    def update_800_reason(self, cursor, uid, integrity_ids, tolerance,
                          clinmeos_id, agclos_id, agcl5os_id):
        integrity_obj = self.pool.get('giscedata.objeccions.totals')
        agclos_comp_line_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia.agclos')
        agcl5os_comp_line_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia.agcl5os')

        base_aggregation = AGGREGATION[:]
        base_aggregation.remove('comercialitzadora')

        for idata in integrity_obj.read(
                cursor, uid, integrity_ids, base_aggregation + ['diferencia']):
            if tolerance != 0 and abs(idata['diferencia']) >= tolerance:
                search_params = [
                    ('distribuidora', '=', idata['distribuidora']),
                    ('agree_tensio', '=', idata['agree_tensio']),
                    ('agree_tarifa', '=', idata['agree_tarifa']),
                    ('agree_dh', '=', idata['agree_dh']),
                    ('agree_tipo', '=', idata['agree_tipo']),
                    ('provincia', '=', idata['provincia'])
                ]
                # Search AGGS
                comp_agclos_ids = agclos_comp_line_obj.search(
                    cursor, uid,
                    search_params + [('agclos_id_comparativa', '=', agclos_id)]
                )
                comp_agcl5os_ids = agcl5os_comp_line_obj.search(
                    cursor, uid,
                    search_params + [
                        ('agcl5os_id_comparativa', '=', agcl5os_id)]
                )
                # Update reasons in AGGS
                if comp_agclos_ids:
                    agclos_comp_line_obj.write(
                        cursor, uid, comp_agclos_ids, {'motiu': "800"}
                    )
                if comp_agcl5os_ids:
                    agcl5os_comp_line_obj.write(
                        cursor, uid, comp_agcl5os_ids, {'motiu': "800"}
                    )

    def create_multiple_objection_lines(self, cursor, uid, motius, agcl_id, search_params, type_, context=None):
        if context is None:
            context = {}

        if type_ == 'agclos':
            model_comp_line_obj = self.pool.get('giscedata.objeccions.comparativa.linia.agclos')
            field = 'agclos_id_comparativa'
        elif type_ == 'agcl5os':
            model_comp_line_obj = self.pool.get('giscedata.objeccions.comparativa.linia.agcl5os')
            field = 'agcl5os_id_comparativa'
        elif type_ == 'agclacum':
            model_comp_line_obj = self.pool.get('giscedata.objeccions.comparativa.linia.agclacum')
            field = 'agclacum_id_comparativa'
        elif type_ == 'agclacum5':
            model_comp_line_obj = self.pool.get('giscedata.objeccions.comparativa.linia.agclacum5')
            field = 'agclacum5_id_comparativa'

        for n_line, motiu in enumerate(motius, start=1):
            if n_line == 1:
                comp_model_id = model_comp_line_obj.search(cursor, uid, search_params + [(field, '=', agcl_id)])
                model_comp_line_obj.write(cursor, uid, comp_model_id[0], {'motiu': motiu})
            else:
                # create reason
                comp_model_id = model_comp_line_obj.search(cursor, uid, search_params + [(field, '=', agcl_id)])
                create_params = model_comp_line_obj.read(cursor, uid, comp_model_id[0], [])
                create_params.update({'motiu': motiu, field: create_params[field][0]})
                create_params.pop('id')
                model_comp_line_obj.create(cursor, uid, create_params)

    def filter_lines(self, cursor, uid, ids, context=None):
        """Crea una comparativa entre CLINMEOS i AGCL+AGCL5OS"""
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        periode_id = context.get('active_id', False)
        clinmeos = wizard.file_clinmeos
        agclos = wizard.file_agclos
        agcl5os = wizard.file_agcl5os
        multiple_aggs = wizard.multiple_agg_reasons
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')
        agclos_line_obj = self.pool.get('giscedata.objeccions.agclos.line')
        agcl5os_line_obj = self.pool.get('giscedata.objeccions.agcl5os.line')
        integrity_obj = self.pool.get('giscedata.objeccions.totals')

        # Preventive delete
        comparative_ids = integrity_obj.search(
            cursor, uid, [('periode_id', '=', periode_id)]
        )
        integrity_obj.unlink(cursor, uid, comparative_ids)

        # Set calc
        lines = [x.id for x in clinmeos.lines]
        df_clinmeos = pd.DataFrame(
            data=clinmeos_line_obj.read(cursor, uid, lines, [])
        )
        lines = [x.id for x in agclos.lines]
        df_agclos = pd.DataFrame(
            data=agclos_line_obj.read(cursor, uid, lines, [])
        )
        lines = [x.id for x in agcl5os.lines]
        df_agcl5os = pd.DataFrame(
            data=agcl5os_line_obj.read(cursor, uid, lines, [])
        )
        df_agclos.rename(
            index=str, columns={"consum": "consum_agcls"}, inplace=True
        )
        df_agcl5os.rename(
            index=str, columns={"consum_ae": "consum_agcls"}, inplace=True
        )
        df_clinmeos.rename(
            index=str, columns={"consum_ae": "consum_clinmeos"}, inplace=True
        )
        df_agclos = pd.concat([df_agclos, df_agcl5os])
        df_agclos = df_agclos.groupby(AGGREGATION).aggregate(
            {'consum_agcls': 'sum'}).reset_index()
        df_clinmeos = df_clinmeos.groupby(AGGREGATION).aggregate(
            {'consum_clinmeos': 'sum'}).reset_index()
        df_agclos['agree_tipo'] = df_agclos['agree_tipo'].astype('int')
        df_clinmeos['agree_tipo'] = df_clinmeos['agree_tipo'].astype('int')

        integrity_ids = []
        df_result = pd.merge(df_clinmeos, df_agclos, on=AGGREGATION)
        aggs = df_result.T.to_dict().values()
        for agg in aggs:
            agg['periode_id'] = periode_id
            agg['diferencia'] = agg['consum_clinmeos'] - agg['consum_agcls']
            integrity_ids.append(integrity_obj.create(cursor, uid, agg))
        agcl_names = str(agclos.name) + ' + ' + str(agcl5os.name)
        periode_obj.write(cursor, uid, periode_id,
            {'clinmeos_comparat': clinmeos.name, 'agcls_comparats': agcl_names,
             'ultima_comprovacio_consums': datetime.now().strftime(
                 '%d-%m-%Y %H:%M:%S')
             }
        )
        if wizard.update_reasons_check:
            self.update_reasons(
                cursor, uid, clinmeos.id, agclos.id, agcl5os.id, multiple_aggs, context=context
            )
            self.update_800_reason(
                cursor, uid, integrity_ids, wizard.diferencial, clinmeos.id, agclos.id, agcl5os.id
            )
        wizard.write({'state': 'done'})

    _columns = {
        'file_clinmeos': fields.many2one(
            'giscedata.objeccions.clinmeos', 'CLINMEOS', required=True
        ),
        'file_agcl5os': fields.many2one(
            'giscedata.objeccions.agcl5os', 'AGCL5OS', required=True
        ),
        'file_agclos': fields.many2one(
            'giscedata.objeccions.agclos', 'AGCLOS', required=True
        ),
        'periode': fields.many2one(
            'giscedata.objeccions.periode', 'Periode'
        ),
        'update_reasons_check': fields.boolean('Actualitzar motius'),
        'diferencial': fields.integer(
            "Diferencial d'energia", help="Objecció 800 a les línes amb igual "
                                          "o més diferencia a la indicada"),
        'info': fields.text('Informació', readonly=True),
        'state': fields.char('State', size=4),
        'multiple_agg_reasons': fields.boolean('Agregacions multiples motius', help="Crear varies linies segons CLINMEOS"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'periode': _default_periode,
        'update_reasons': lambda *a: True,
        'multiple_agg_reasons': lambda *a: True,
        'info': _default_info,
    }

    _constraints = [(
        _cnt_abs_diferencial,
        _(u"Error: El diferencial d'energia ha de ser positiu"),
        ['diferencial']
    )]

ObjectionIntegrity()
