# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

class CheckObjections(osv.osv_memory):

    def _default_file(self, cursor, uid, context=None):
        """Si existeix un sol fitxer per període el retorna per defecte,
        altrament retorna un fitxer no existent, per activar l'onchange de
        selection_file i així filtrar els fitxers del període. Això es fa per
        no confondre el fitxer pel qual es vol revisar dades
        :return: clinmeos_id o agcl5os_id o agclos_id o agclacum_id o agclacum5_id depen del model
        """
        period_obj = self.pool.get('giscedata.objeccions.periode')
        period_id = context.get('active_id', [])
        _file_ids = None
        file_key = None
        if self._get_file_mode(cursor, uid, context) == 'CLINMEOS':
            file_key = 'clinmeos_ids'
        if self._get_file_mode(cursor, uid, context) == 'AGCL5OS':
            file_key = 'agcl5os_ids'
        if self._get_file_mode(cursor, uid, context) == 'AGCLOS':
            file_key = 'agclos_ids'
        if self._get_file_mode(cursor, uid, context) == 'AGCLACUM':
            file_key = 'agclacum_ids'

        if file_key:
            _file_ids = period_obj.read(
                cursor, uid, period_id, [file_key]
            )[file_key]

        if _file_ids:
            if len(_file_ids) > 1:
                return False
            else:
                return _file_ids[0]
        else:
            return False

    def _get_file_mode(self, cursor, uid, context=None):
        from_model = context.get('from_model', [])
        if from_model == 'giscedata.objeccions.comparativa.linia':
            return 'CLINMEOS'
        elif from_model == 'giscedata.objeccions.comparativa.linia.agcl5os':
            return 'AGCL5OS'
        elif from_model == 'giscedata.objeccions.comparativa.linia.agclos':
            return 'AGCLOS'
        elif from_model == 'giscedata.objeccions.comparativa.linia.agregacions':
            return 'AGCLACUM'
        else:
            return None

    def selection_file(self, cursor, uid, ids, context=None):
        """Retorna unicament els fitxers CLINMEOS d'aquest periode
        :param context: dict with active_ids
        :return: dict like {
            'value': {},
            'domain': {'file': list_of_clinmeos},
            'warning': {}
            }
        """
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        periode = context.get('active_ids', [])
        from_model = context.get('from_model', [])
        file_obj = self.pool.get('giscedata.objeccions.clinmeos')
        file_column = 'file_clinmeos'
        if self._get_file_mode(cursor, uid, context) == 'AGCL5OS':
            file_column = 'file_agcl5os'
            file_obj = self.pool.get('giscedata.objeccions.agcl5os')
        elif self._get_file_mode(cursor, uid, context) == 'AGCLOS':
            file_column = 'file_agclos'
            file_obj = self.pool.get('giscedata.objeccions.agclos')
        elif self._get_file_mode(cursor, uid, context) == 'AGCLACUM':
            file_column = 'file_agclacums'
            file_obj = self.pool.get('giscedata.objeccions.agclacum')
        if periode:
            periode = periode[0]
            # Get the clinmeos:ids
            file_ids = file_obj.search(
                cursor, uid, [('periode', '=', periode)]
            )

            res['domain'].update(
                {file_column: [('id', 'in', file_ids)]}
            )
        else:
            res['domain'].update({file_column: []})

        return res

    def filter_lines(self, cursor, uid, ids, context=None):
        """Obre una nova pestanya amb les linies a objectar
        """
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        res_model = context.get('from_model', [])
        domain = [('clinmeos_id_comparativa', '=', wizard.file_clinmeos.id)]
        filename = wizard.file_clinmeos.name

        if self._get_file_mode(cursor, uid, context) == 'AGCL5OS':
            domain = [('agcl5os_id_comparativa', '=', wizard.file_agcl5os.id)]
            filename = wizard.file_agcl5os.name

        elif self._get_file_mode(cursor, uid, context) == 'AGCLOS':
            domain = [('agclos_id_comparativa', '=', wizard.file_agclos.id)]
            filename = wizard.file_agclos.name

        elif self._get_file_mode(cursor, uid, context) == 'AGCLACUM':
            agclacums_ids = [x.id for x in wizard.file_agclacums]
            domain = [('fitxer_id_comparativa', 'in', agclacums_ids)]
            filename = ' + '.join([x.name for x in wizard.file_agclacums])

        vals_view = {
            'name': _(u"Línies d'objeccions {}").format(filename),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': res_model,
            'limit': 80,
            'type': 'ir.actions.act_window',
            'domain': domain,
        }

        return vals_view

    _name = 'giscedata.check.objections'

    _columns = {
        'file_clinmeos': fields.many2one(
            'giscedata.objeccions.clinmeos', 'CLINMEOS',
        ),
        'file_agcl5os': fields.many2one(
            'giscedata.objeccions.agcl5os', 'AGCL5OS',
        ),
        'file_agclos': fields.many2one(
            'giscedata.objeccions.agclos', 'AGCLOS',
        ),
        'file_agclacums': fields.many2many(
            'giscedata.objeccions.agclacum', 'giscedata_agclacum_rel', 'agclacum_id', 'id', 'AGCLACUMs'
        ),
        'model_state': fields.text('Model state'),
    }

    _defaults = {
        'file_clinmeos': _default_file,
        'file_agcl5os': _default_file,
        'file_agclos': _default_file,
        'file_agclacums': _default_file,
        'model_state': _get_file_mode,
        'consult_objections': lambda *x: False,
    }


CheckObjections()
