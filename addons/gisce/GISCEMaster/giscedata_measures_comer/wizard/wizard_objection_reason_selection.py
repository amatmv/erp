# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from ..headers import OBJECTION_REASON


class ObjectionReasonSelection(osv.osv_memory):

    _name = 'objection.reason.selection'

    _OBJECTION_REASON = OBJECTION_REASON + [("DELETE", "Deshacer Motivo")]

    def _default_motiu(self, cursor, uid, context=None):
        return OBJECTION_REASON[0]

    def set_motiu(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        comparativa_ids = context.get('active_ids', [])
        from_model = context.get('from_model', [])
        comparativa_linia_obj = self.pool.get(from_model)

        if comparativa_ids:
            if wizard.motiu != 'DELETE':
                comparativa_linia_obj.write(
                    cursor, uid, comparativa_ids, {
                        'motiu': wizard.motiu, 'descripcio': wizard.descripcio
                    }
                )
            else:
                comparativa_linia_obj.write(
                    cursor, uid, comparativa_ids, {
                        'motiu': False, 'descripcio': wizard.descripcio
                    }
                )
        wizard.write({'state': 'end'})

    _columns = {
        'motiu': fields.selection(_OBJECTION_REASON, 'Motiu Objecció'),
        'descripcio': fields.text('Descripció Objecció'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'Estat')
    }

    _defaults = {
        'motiu': _default_motiu,
        'state': lambda *a: 'init',
    }


ObjectionReasonSelection()
