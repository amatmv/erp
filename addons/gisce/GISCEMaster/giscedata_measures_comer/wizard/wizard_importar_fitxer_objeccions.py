from osv import osv, fields
import csv
import base64
import StringIO
from datetime import datetime
from dateutil.relativedelta import relativedelta
from tools.translate import _
from zipfile import ZipFile, BadZipfile
from giscedata_measures.utils import last_sunday

HOUR_CHANGE_MONTHS = [10]


class ImportarFitxerObjeccions(osv.osv_memory):

    def check_cups(self, cursor, uid, cups, context=None):
        cups_ps_obj = self.pool.get('giscedata.cups.ps')
        cups_ps_ids = cups_ps_obj.search(
            cursor, uid, [('name', 'like', cups + '%')]
        )
        # cups found
        if cups_ps_ids:
            return True
        # cups not found
        else:
            return False

    @staticmethod
    def read_zip(_file):
        data = base64.b64decode(_file)
        fileHandle = StringIO.StringIO(data)
        try:
            return ZipFile(fileHandle, "r")
        except BadZipfile:
            raise osv.except_osv(_('Error'), _(u'El fitxer ha de ser un .zip'))

    @staticmethod
    def get_next_day(day, month, year):
        the_date = datetime.strptime(
            str(year) + str(month) + str(day), '%Y%m%d')
        the_date = the_date + relativedelta(days=1)
        day = str(the_date.day).zfill(2)

        month = str(the_date.month).zfill(2)
        year = str(the_date.year)

        return day, month, year

    @staticmethod
    def agcl_parse_filename(filename):
        version = filename.split('.')[1]
        date_start = filename.rsplit('_', 1)[1].split('.')[0]
        name = date_start[:6]
        try:
            date_start = datetime.strptime(date_start, '%Y%m')
        except Exception:
            date_start = datetime.strptime(date_start, '%Y%m%d')
        date_end = date_start + relativedelta(months=1)

        return {
            'name': filename.replace('.zip', ''), 'versio': version,
            'data_inici': date_start, 'data_final': date_end,
            'nom_periode': name
        }

    def action_import_file(self, cursor, uid, ids, context=None):
        """ Importa un fitxer d'Objeccions """
        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        content = base64.b64decode(wizard.file)
        if 'AGCL5OS' in wizard.filename.upper():
            context.update({'agcl5os_id': self.create_agcl5os(cursor, uid, ids, wizard.filename, context=context)})
        elif 'AGCLOS' in wizard.filename.upper():
            context.update({'agclos_id': self.create_agclos(cursor, uid, ids, wizard.filename, context=context)})
        else:
            fileHandle = StringIO.StringIO(content)
            try:
                zf = ZipFile(fileHandle, "r")
                lfiles = [x for x in zf.NameToInfo if "AGCLOS_" in x]
                if lfiles:
                    filename = lfiles[0]
                    context.update({'agclos_id': self.create_agclos(cursor, uid, ids, filename, context=context)})
                lfiles = [x for x in zf.NameToInfo if "AGCL5OS_" in x]
                if lfiles:
                    filename = lfiles[0]
                    context.update({'agcl5os_id': self.create_agcl5os(cursor, uid, ids, filename, context=context)})

            except BadZipfile:
                pass
        measures_loader_obj = self.pool.get('giscedata.measures.loader')

        res = measures_loader_obj.parse_file(cursor, uid, wizard.filename, content, context=context)
        if not res:
            res = {}
        res.update({'state': 'done', 'info': _(u"Fitxer/s importat/s correctament")})
        wizard.write(res)
        return True

    def create_agclos(self, cursor, uid, ids, filename, context=None):
        """ Crea un fitxer AGCLOS"""
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        agclos_obj = self.pool.get('giscedata.objeccions.agclos')

        wizard = self.browse(cursor, uid, ids[0], context)
        res = self.agcl_parse_filename(filename)

        periode_id = periode_obj.search(
            cursor, uid, [('name', '=', res['nom_periode'])]
        )
        if not periode_id:
            periode_id.append(
                periode_obj.create(
                    cursor, uid, {'name': res['nom_periode'],
                                  'data_inici': res['data_inici'],
                                  'data_final': res['data_final']})
            )
        if agclos_obj.search(cursor, uid, [
            ('name', '=', res['name']), ('versio', '=', res['versio'])
        ]):
            _msg = _(u"Ja existeix un fitxer amb aquest nom i versio")
            wizard.write(
                {'state': 'done', 'info': _msg})
            raise osv.except_osv(_(u'Error'), _msg)

        # Create the new AGCLOS file
        res['periode'] = periode_id[0]
        agclos_id = agclos_obj.create(cursor, uid, res)

        return agclos_id

    def create_agcl5os(self, cursor, uid, ids, filename, context=None):
        """ Crea un fitxer AGCL5OS i crea un obj AGCL5OS per
        filename - version"""
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        agcl5os_obj = self.pool.get('giscedata.objeccions.agcl5os')

        wizard = self.browse(cursor, uid, ids[0], context)
        res = self.agcl_parse_filename(filename)

        periode_id = periode_obj.search(
            cursor, uid, [('name', '=', res['nom_periode'])]
        )
        if not periode_id:
            periode_id.append(
                periode_obj.create(
                    cursor, uid, {'name': res['nom_periode'],
                                  'data_inici': res['data_inici'],
                                  'data_final': res['data_final']})
            )
        if agcl5os_obj.search(cursor, uid, [
            ('name', '=', res['name']), ('versio', '=', res['versio'])
        ]):
            _msg = _(u"Ja existeix un fitxer amb aquest nom i versio")
            wizard.write(
                {'state': 'done', 'info': _msg})
            raise osv.except_osv(_(u'Error'), _msg)

        # Create the new AGCLOS file
        res['periode'] = periode_id[0]
        agcl5os_id = agcl5os_obj.create(cursor, uid, res)

        return agcl5os_id

    def _default_file_format(self, cursor, uid, context=None):
        if not context:
            context = {}
        respostes = context.get('respostes', [])
        if respostes:
            return 'aobjeagcl'
        else:
            return 'clinmeos'

    _name = 'giscedata.importar.fitxer.objeccions'

    _format_selection = [
        ('clinmeos', 'CLINMEOS'),
        ('agclos', 'AGCLOS'),
        ('agcl5os', 'AGCL5OS'),
        ('agclacum', 'AGCLACUM'),
        ('agclacum5', 'AGCLACUM5'),
        ('aobjeagcl', 'AOBJEAGCL'),
    ]

    _columns = {
        'file': fields.binary('Fitxer'),
        'filename': fields.binary('Nom', size=32),
        'file_format': fields.selection(_format_selection, 'Format'),
        'state': fields.char('State', size=4),
        'info': fields.text('Info', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'file_format': _default_file_format,
    }


ImportarFitxerObjeccions()
