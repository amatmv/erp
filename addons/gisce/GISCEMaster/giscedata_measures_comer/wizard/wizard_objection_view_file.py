# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class ViewFile(osv.osv_memory):

    def _get_file_mode(self, cursor, uid, context=None):
        from_model = context.get('from_model', [])
        if from_model == 'giscedata.objeccions.clinmeos':
            return 'CLINMEOS'
        elif from_model == 'giscedata.objeccions.agcl5os':
            return 'AGCL5OS'
        elif from_model == 'giscedata.objeccions.agclos':
            return 'AGCLOS'
        elif from_model == 'giscedata.objeccions.agclacum':
            return 'AGCLACUM'
        elif from_model == 'giscedata.objeccions.agclacum5':
            return 'AGCLACUM5'
        else:
            return None

    def filter_lines(self, cursor, uid, ids, context=None):
        """Obre una nova pestanya amb les linies a objectar
        """
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        res_model = context.get('from_model', [])
        active_id = context.get('active_id', None)
        model_obj = self.pool.get(res_model + '.line')
        filename_id = wizard.model_state.lower() + '_id'
        domain = [(filename_id, '=', active_id)]
        filename = self.pool.get(res_model).read(cursor, uid, active_id, ['name'])['name']

        vals_view = {
            'name': _(u"Línies del fitxer {}").format(filename),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': model_obj._name,
            'limit': 80,
            'type': 'ir.actions.act_window',
            'domain': domain,
        }

        return vals_view

    _name = 'giscedata.objections.view.file'

    _columns = {
        'model_state': fields.text('Model state'),
        'filename': fields.text('Info', readonly=True),
    }

    _defaults = {
        'model_state': _get_file_mode,
        'filename': lambda *x: 'Veure contingut del fitxer'
    }


ViewFile()
