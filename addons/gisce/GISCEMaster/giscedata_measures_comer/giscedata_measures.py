# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from oorq.decorators import job
import logging
from enerdata.profiles import Dragger
import pandas as pd
import numpy as np
import itertools as it
from headers import OBJECTION_REASON, AGGREGATION, REVAC_HEADER, OBJEAGCL_HEADER
from tempfile import NamedTemporaryFile, mkstemp
from base64 import b64encode
from os import unlink
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES

CUPS_LIST_SPLIT = 10
CUPS_CONFIG = True

FIELDS_TG = {
    'cups': 'name',
    'timestamp': 'datetime',
    'consumption': 'ai',
    'magnitud': 1/1000.0,
}

FIELDS_PROFILE = {
    'cups': 'cups',
    'timestamp': 'timestamp',
    'consumption': 'lectura',
    'magnitud': 1.0,
}


class GiscedataObjeccionsPeriode(osv.osv):

    _name = 'giscedata.objeccions.periode'

    _order = "name asc"

    def get_files_info(self, cursor, uid, filetype, context=None):
        """
        :param cursor:
        :param uid:
        :param filetype:
        :param context:
        :return: {'file_obj': 'giscedata.objeccions.agclacum', 'comparativa_linia': }
        """
        if context is None:
            context = {}
        filetype = filetype.lower()
        model_name = 'giscedata.objeccions.{}'.format(filetype)
        model_obj = self.pool.get(model_name)
        model_name = str(model_obj._name)
        model_lines_obj = model_obj._columns['lines']._obj
        comparative_base = 'giscedata.objeccions.comparativa.linia'
        if filetype == 'agclacum':
            comparative_name = '{}.{}'.format(comparative_base, 'agregacions')
        elif filetype == 'clinmeos':
            comparative_name = comparative_base
        else:
            comparative_name = '{}.{}'.format(comparative_base, filetype.lower())
        comparative_obj = self.pool.get(comparative_name)

        return {
            'model_obj': model_obj, 'model_name': model_name,
            'lines_obj': model_lines_obj, 'linies_name': model_obj._columns['lines'],
            'comparative_obj': comparative_obj, 'comparative_name': comparative_name
        }

    def get_cups_info(self, cursor, uid, cups_name, data_inici, data_final):
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        cups_name += '%'
        modcons = modcon_obj.q(cursor, uid).read(
            ['cups.name'], only_active=False
        ).where(
            [
                ('data_inici', '<=', data_final),
                ('data_final', '>=', data_inici),
                ('cups.name', '=like', cups_name),
                ('polissa_id.state', 'not in', CONTRACT_IGNORED_STATES),
            ]
        )
        if not modcons:
            return {}
        else:
            return modcons[0]

    @staticmethod
    def overlapping_dates(di, df, di2, df2):
        if di <= df2 and df >= di2:
            return True

    @staticmethod
    def convert_periodhour_to_date(di, df):
        """
        Trim dates without hour, and parse last day 00h to -1day
        :param di: str date
        :param df: str date
        :return: di, df str dates
        """
        df = datetime.strptime(df[:10], '%Y-%m-%d')
        df -= timedelta(days=1)
        df = df.strftime('%Y-%m-%d')
        di = di[:10]

        return di, df

    def get_dates_by_agg(self, cursor, uid, cups_name, vals, context=None):
        """
        Get vigence dates by CUPS - aggregation level from modcons
        :param vals: dict with where_params
        :return: str vigence dates
        """
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        data_inici, data_final = self.convert_periodhour_to_date(
            vals['data_inici'], vals['data_final'])
        where_params = [
            ('cups.name', '=like', cups_name),
            ('distribuidora.ref', '=', vals['distribuidora']),
            ('agree_tensio', '=', vals['agree_tensio']),
            ('agree_tarifa', '=', vals['agree_tarifa']),
            ('agree_dh', '=', vals['agree_dh']),
            ('agree_tipus', '=', vals['agree_tipo']),
            ('cups.id_municipi.state.ree_code', '=', vals['provincia']),
            ('data_inici', '<=', data_final),
            ('data_final', '>=', data_inici),
            ('polissa_id.state', 'not in', CONTRACT_IGNORED_STATES),
        ]
        dates = modcon_obj.q(cursor, uid).read(
            ['data_inici', 'data_final'], only_active=False
        ).where(where_params)
        if not dates:
            return False, False
        # Check all possible modcons that does not mean a aggregation jump
        date_vigence_ini = min([x['data_inici'] for x in dates])
        date_vigence_end = max([x['data_final'] for x in dates])

        if date_vigence_ini > data_inici:
            data_inici = date_vigence_ini
        if date_vigence_end < data_final:
            data_final = date_vigence_end

        return self.patch_date_to_complete_period(data_inici, data_final)

    def check_duplicate_cups(self, cursor, uid, lines_ids, context=None):
        """
        Check CUPS by {'CUPS': [(di, df), (di, df)...]} and create comparativa
        linia if CUPS by dates is duplicate
        :param lines_ids: clinmeos_line_ids
        """
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')
        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia'
        )
        data = clinmeos_line_obj.read(cursor, uid, lines_ids)
        df_clinmeos = pd.DataFrame(data=data)
        cups_list = df_clinmeos.groupby(['cups']).aggregate(
            {'consum_ae': 'count'}
        ).reset_index()
        cups_list = list(cups_list[cups_list['consum_ae'] > 1]['cups'])
        df_cups = df_clinmeos[df_clinmeos.cups.isin(cups_list)]
        cups_dates = {}
        for x in df_cups.T.to_dict().values():
            if x['cups'] in cups_dates:
                cups_dates[x['cups']].append((x['data_inici'], x['data_final']))
            else:
                cups_dates[x['cups']] = [(x['data_inici'], x['data_final'])]
        duplicate_cups = []
        for cups in cups_dates.keys():
            for L in range(0, len(cups_dates[cups]) + 1):
                for subset in it.combinations(cups_dates[cups], L):
                    if len(subset) == 2:
                        if self.overlapping_dates(subset[0][0], subset[0][1],
                                                  subset[1][0], subset[1][1]):
                            duplicate_cups.append(cups)

        df_clinmeos = df_clinmeos[
            df_clinmeos.cups.isin(list(set(duplicate_cups)))
        ]
        for elem in df_clinmeos.T.to_dict().values():
            comparativa_linia_obj.create(cursor, uid, {
                'cups': elem['cups'],
                'consum_ae': elem['consum_ae_estimada'],
                'consum_calculat': 0,
                'consum_existeix': True,
                'motiu': "801",
                'distribuidora': elem['distribuidora'],
                'clinmeos_id_comparativa': elem['clinmeos_id'][0],
                'clinmeos_line_id': elem['id'],
            })

    def check_existence_cups(self, cursor, uid, lines_ids, data_inici,
                             data_final, context=None):
        """Check existence cups and:
        create objection line if cups is not our and discard this line.
        create objection line if our CUPS without energy
        :param lines_ids: clinmeos_line_ids
        :param data_inici: str di
        :param data_final: str df
        :return line_ids: line_ids to continue check
        """
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')
        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia'
        )

        # Prepare CUPS ERP DataFrame
        modcons = modcon_obj.q(cursor, uid).read(
            ['cups.name', 'cups.distribuidora_id.ref', 'agree_tensio',
             'agree_tarifa', 'agree_dh', 'agree_tipus',
             'cups.id_municipi.state.ree_code'], only_active=False
        ).where([
            ('data_inici', '<=', data_final), ('data_final', '>=', data_inici), ('agree_tipus', '>=', '03'),
            ('polissa_id.state', 'not in', CONTRACT_IGNORED_STATES),
        ])
        df_cups_erp = pd.DataFrame(data=modcons)
        df_cups_erp.rename(
            columns={
                "cups.name": "cups",
                "cups.distribuidora_id.ref": "distribuidora",
                "agree_tipus": "agree_tipo",
                "cups.id_municipi.state.ree_code": "provincia"
            }, index=str, inplace=True
        )
        # Drop possible modcons by not aggregation jump
        df_cups_erp.drop_duplicates(
            subset=['cups', 'distribuidora', 'agree_tensio', 'agree_tarifa',
                    'agree_dh', 'agree_tipo', 'provincia'],
            keep='first', inplace=True
        )

        # Prepare CUPS CLINMEOS DataFrame
        data = clinmeos_line_obj.read(
            cursor, uid, lines_ids, AGGREGATION + ['cups', 'clinmeos_id', 'consum_ae']
        )
        df_cups_clinmeos = pd.DataFrame(data=data)
        clinmeos_id = data[0]['clinmeos_id'][0]

        # Transform cups to 20 chars and save in sub_cups row
        df_cups_erp['sub_cups'] = df_cups_erp['cups'].apply(lambda x: x[:20])
        df_cups_clinmeos['sub_cups'] = df_cups_clinmeos['cups'].apply(
            lambda x: x[:20])

        # CUPS is not our or our CUPS without energy
        df = pd.merge(
            df_cups_erp, df_cups_clinmeos, how='outer',
            on=['sub_cups', 'distribuidora'], indicator=True
        )
        df = df[df['_merge'] != 'both']

        # Discard a existent lines on CLINMEOS when CUPS is not our detected
        # If have checked, no need to check more these CUPS
        for line_id in list(df[df['_merge'] == 'right_only']['id_y']):
            lines_ids.remove(line_id)

        data_df_000 = []
        data_df_001 = []
        sub_fields = [
            'cups', 'agree_tensio', 'agree_tarifa',
            'agree_dh', 'agree_tipo', 'provincia'
        ]
        fields = sub_fields + ['distribuidora']
        # Create objection lines
        for elem in df.T.to_dict().values():
            if elem['_merge'] == 'right_only':
                # CUPS is not our
                motiu = "000"
                # Get right info
                for key in sub_fields:
                    elem[key] = elem[key + '_y']
                # Include imputed consumption & save aggregation
                consum_ae = elem['consum_ae']
                tmp_dict = dict((k, elem[k]) for k in fields if k in elem)
                data_df_000.append(tmp_dict)
            else:
                # our CUPS without energy
                motiu = "001"
                # Get left info
                for key in sub_fields:
                    elem[key] = elem[key + '_x']
                consum_ae = 0
                # Save aggregation
                tmp_dict = dict((k, elem[k]) for k in fields if k in elem)
                data_df_001.append(tmp_dict)
            comparativa_linia_obj.create(cursor, uid, {
                'cups': elem['cups'],
                'consum_ae': consum_ae,
                'consum_calculat': 0,
                'consum_existeix': True,
                'motiu': motiu,
                'distribuidora': elem['distribuidora'],
                'agree_tensio': elem['agree_tensio'],
                'agree_tarifa': elem['agree_tarifa'],
                'agree_dh': elem['agree_dh'],
                'provincia': elem['provincia'],
                'agree_tipo': elem['agree_tipo'],
                'clinmeos_id_comparativa': clinmeos_id,
            })
        # Aggregate datasets
        agg = AGGREGATION[:]
        agg.remove('comercialitzadora')
        df_clinmeos = df_cups_clinmeos.groupby(agg).aggregate(
            {'cups': 'count'}).reset_index()
        df_erp = df_cups_erp.groupby(agg).aggregate(
            {'cups': 'count'}).reset_index()
        df_000 = pd.DataFrame(data=data_df_000)
        df_001 = pd.DataFrame(data=data_df_001)
        if not df_000.empty:
            df_000 = df_000.groupby(agg).aggregate(
                {'cups': 'count'}).reset_index()
        if not df_001.empty:
            df_001 = df_001.groupby(agg).aggregate(
                {'cups': 'count'}).reset_index()

        self.update_00s_reasons(
            cursor, uid, df_clinmeos, df_erp, df_000, df_001, clinmeos_id)

        return lines_ids

    def update_00s_reasons(self, cursor, uid, df_clinmeos, df_erp, df_000,
                           df_001, clinmeos_id):
        """
        Compare if the 00s reasons are complete, if not write a 999 reason
        CLINMEOS vs 000
        ERP vs 001
        :param df_clinmeos: df with clinmeos aggregated data and n_cups counted
        :param df_erp: df with clinmeos aggregated data and n_cups counted
        :param df_000: df with 000 reason aggregated data and n_cups counted
        :param df_001: df with 001 reason aggregated data and n_cups counted
        :param clinmeos_id: clinmeos file id
        """
        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia'
        )
        fields = [
            'distribuidora', 'agree_tensio', 'agree_tarifa', 'agree_dh',
            'agree_tipo', 'provincia'
        ]
        if not df_000.empty:
            # Check aggs by CLINMEOS FILE
            df_merged = pd.merge(
                df_clinmeos, df_000, how='right', indicator=True)
            df = df_merged[df_merged['_merge'] != 'both']
            for elem in df.T.to_dict().values():
                search_params = []
                for key in fields:
                    search_params += [(key, '=', elem[key])]
                search_params += [
                    ('motiu', '=', '000'),
                    ('clinmeos_id_comparativa', '=', clinmeos_id)
                ]
                comp_ids = comparativa_linia_obj.search(
                    cursor, uid, search_params)
                comparativa_linia_obj.write(
                    cursor, uid, comp_ids, {'motiu': '004'})

        if not df_001.empty:
            # Check aggs by ERP
            df_merged = pd.merge(df_erp, df_001, how='right', indicator=True)
            df = df_merged[df_merged['_merge'] != 'both']
            for elem in df.T.to_dict().values():
                search_params = []
                for key in fields:
                    search_params += [(key, '=', elem[key])]
                search_params += [
                    ('motiu', '=', '001'),
                    ('clinmeos_id_comparativa', '=', clinmeos_id)
                ]
                comp_ids = comparativa_linia_obj.search(
                    cursor, uid, search_params)
                comparativa_linia_obj.write(
                    cursor, uid, comp_ids, {'motiu': '004'})

    @job(queue='generate_objections', timeout=3600 * 2)
    def contrast_consumption_async(self, cursor, uid, clinmeos_id,
                                   context=None):
        return self.contrast_consumption(cursor, uid, clinmeos_id, context)

    def contrast_consumption(self, cursor, uid, clinmeos_id, context=None):
        """Visualitza consums imputats (CLINMEOS) vs consums calculats (comer)
        :param clinmeos_id: id del fitxer CLINMEOS associat
        :return: giscedata_objeccions_comparativa_linia creat
        """
        if context is None:
            context = {}
        logger = logging.getLogger('openerp.CLINMEOS.{}'.format(__name__))
        clinmeos_obj = self.pool.get('giscedata.objeccions.clinmeos')
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')
        comparativa_linia_obj = self.pool.get(
            'giscedata.objeccions.comparativa.linia'
        )
        cch_obj = self.pool.get('tg.cchfact')
        profiles_obj = self.pool.get('giscedata.profiles.profile')

        # Get clinmeos data
        clinmeos_data = clinmeos_obj.read(
            cursor, uid, clinmeos_id, [
                'lines', 'name', 'periode', 'data_inici', 'data_final'
            ]
        )

        # Extract dates from file 1 to 31
        data_inici = clinmeos_data['data_inici']
        data_final = clinmeos_data['data_final']
        data_final = (
            datetime.strptime(data_final, '%Y-%m-%d') - timedelta(days=1)
        ).strftime("%Y-%m-%d")

        # Preventive delete for possible existing comparativa_linia
        # for this file
        search_params = [('clinmeos_id_comparativa', '=', clinmeos_data['id'])]
        res_ids = comparativa_linia_obj.search(cursor, uid, search_params)
        comparativa_linia_obj.unlink(cursor, uid, res_ids)

        # Check duplicate_cups
        lines_ids = clinmeos_data['lines']
        self.check_duplicate_cups(cursor, uid, lines_ids, context=context)
        # Check existence cups
        lines_ids = self.check_existence_cups(
            cursor, uid, lines_ids, data_inici, data_final, context=context
        )

        # extract dict LIKE  {'cupsname': {'consumption': x...}}
        cups_data = {
        x['cups']: {
            'consumption': x['consum_ae'], 'line_id': x['id'],
            'distribuidora': x['distribuidora'], 'data_inici': x['data_inici'],
            'data_final': x['data_final'], 'agree_tensio': x['agree_tensio'],
            'agree_tipo': x['agree_tipo'], 'agree_tarifa': x['agree_tarifa'],
            'agree_dh': x['agree_dh'], 'provincia': x['provincia']
        } for x in clinmeos_line_obj.read(
            cursor, uid, lines_ids,
            [
                'cups', 'consum_ae', 'data_inici', 'data_final'
            ] + AGGREGATION
        )}

        cups_list = [x for x in cups_data]
        cups_not_finded = []
        total_num_cups = len(cups_list)
        for num_cups, CUPS in enumerate(np.array(cups_list), start=1):
            # Initialize consumptions, if do not return for this CUPS,
            # set it to 0
            # Try to process the comparation between consumption data and
            # CLINMEOS data
            trobat_cch = False
            trobat_prof = False
            data_inici_consum = False
            data_final_consum = False
            data_inici_vigencia = False
            data_final_vigencia = False
            try:
                _cups = CUPS[:20]
                consumption_cups_prof = 0
                consumption_cups_cch = 0
                motiu = False
                cups_related = self.get_cups_info(
                    cursor, uid, _cups, cups_data[CUPS]['data_inici'][:10],
                    cups_data[CUPS]['data_final'][:10]
                )
                cups_name = cups_related.get('cups.name', False)

                if cups_name:
                    clinmeos_consumption = cups_data[CUPS]['consumption']
                    consumption_cups_cch = 0
                    dates = []
                    c_ids = cch_obj.search(cursor, uid, [
                        ('name', '=', cups_name),
                        ('datetime', '>=', cups_data[CUPS]['data_inici']),
                        ('datetime', '<=', cups_data[CUPS]['data_final']),
                    ])
                    if c_ids:
                        trobat_cch = True
                        magnitud = 1
                        consumption_data = cch_obj.read(
                            cursor, uid, c_ids, ['ai', 'datetime'])
                        dragger = Dragger()
                        for consumption in consumption_data:
                            consumption_cups_cch += dragger.drag(
                                consumption['ai'] * magnitud / 1000.0
                            )
                            dates.append(consumption['datetime'])
                        consumption_cups_cch = int(round(consumption_cups_cch))
                        logger.info(
                            'Contrast {} tg consumption: {}kWh vs {}kWh - #{} '
                            'of #{}'.format(
                                cups_name, clinmeos_consumption,
                                consumption_cups_cch, num_cups, total_num_cups
                            )
                        )
                        data_inici_consum = min(dates)
                        data_final_consum = max(dates)
                    if consumption_cups_cch != clinmeos_consumption or not trobat_cch:
                        consumption_cups_prof = 0
                        dates = []
                        prof_ids = profiles_obj.search_multipurpose_profiles(
                            cursor, uid, cups_name, cups_data[CUPS]['data_inici'], cups_data[CUPS]['data_final'],
                            context=context
                        )
                        if prof_ids:
                            trobat_prof = True
                            for consumption in profiles_obj.read(cursor, uid, prof_ids, ['lectura', 'timestamp']):
                                consumption_cups_prof += consumption['lectura']
                                dates.append(consumption['timestamp'])
                            logger.info(
                                'Contrast {} profile consumption: {}kWh vs '
                                '{}kWh - #{} of #{}'.format(
                                    cups_name, clinmeos_consumption,
                                    consumption_cups_prof, num_cups,
                                    total_num_cups
                                )
                            )
                            data_inici_consum = min(dates)
                            data_final_consum = max(dates)
                    data_inici_vigencia, data_final_vigencia = (
                        self.get_dates_by_agg(
                            cursor, uid, cups_name, cups_data[CUPS]
                        )
                    )
                    if data_inici_vigencia and data_final_vigencia:
                        if data_inici_vigencia != cups_data[CUPS]['data_inici']:
                            motiu = "002"
                        if data_final_vigencia != cups_data[CUPS]['data_final']:
                            motiu = "003"
                else:
                    # CUPS not finded
                    motiu = "999"
                if not trobat_cch and not trobat_prof:
                    logger.info(str(cups_related))
                    cups_not_finded.append(cups_related)
                    consum_existeix = False
                else:
                    consum_existeix = True

                # Select the closer consumption patching consumptions if not exist
                # TODO to be deleted once the tg flag is consistent
                clinmeos_consumption = cups_data[CUPS]['consumption']
                if not trobat_cch:
                    consumption_cups_cch = consumption_cups_prof
                if not trobat_prof:
                    consumption_cups_prof = consumption_cups_cch
                consumption_cups = consumption_cups_prof if (abs(consumption_cups_prof - clinmeos_consumption) < abs(consumption_cups_cch - clinmeos_consumption)) else consumption_cups_cch

                #diff = consumption_cups - clinmeos_consumption
                #percentual = float((abs(diff) / float(max(consumption_cups,
                # clinmeos_consumption))) * 100)

                comparativa_linia_obj.create(cursor, uid, {
                    'cups': CUPS,
                    'consum_ae': clinmeos_consumption,
                    'consum_calculat': consumption_cups,
                    'consum_existeix': consum_existeix,
                    'data_inici_imputat': cups_data[CUPS]['data_inici'],
                    'data_final_imputat': cups_data[CUPS]['data_final'],
                    'data_inici_consum': data_inici_consum,
                    'data_final_consum': data_final_consum,
                    'data_inici_calculat': data_inici_vigencia,
                    'data_final_calculat': data_final_vigencia,
                    'motiu': motiu,
                    'distribuidora': cups_data[CUPS]['distribuidora'],
                    'clinmeos_id_comparativa': clinmeos_data['id'],
                    'clinmeos_line_id': cups_data[CUPS]['line_id'],
                    'agree_tensio': cups_data[CUPS]['agree_tensio'],
                    'agree_tarifa': cups_data[CUPS]['agree_tarifa'],
                    'agree_dh': cups_data[CUPS]['agree_dh'],
                    'agree_tipo': cups_data[CUPS]['agree_tipo'],
                    'provincia': cups_data[CUPS]['provincia']
                })
            except Exception:
                _msg = _(
                    u"Error processant comparativa per CUPS '{}'".format(CUPS)
                )
                logger.info(_msg)
                # toDo warn to user without raising an error, to follow the
                continue
        clinmeos_obj.write(cursor, uid, clinmeos_id, {
            'data_comparacio': datetime.now(), 'state': 'done'
        })

    def default_comercialitzadora(self, cursor, uid, context=None):
        """ Default comer
        :return: comer as str
        """
        users_obj = self.pool.get('res.users')
        user = users_obj.browse(cursor, uid, uid)
        return user.company_id.partner_id.ref

    @staticmethod
    def patch_date_to_complete_period(start_date, end_date):
        """
        Get first and last tg.profile slot timestamp from a period.

        :param start_date: first date of the period LIKE 'YYYY-MM-DD'
        :param end_date: last date of the period LIKE 'YYYY-MM-DD'
        :return: two str date_periods LIKE 'YYYY-MM-DD HH:mm:ss'
        """

        di = "{0} 01:00:00".format(start_date)
        df = "{0} 00:00:00".format(
            (datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
             ).strftime("%Y-%m-%d")
        )

        return di, df

    @staticmethod
    def get_start_end_from_file(di, df):
        data_inici = di[:10]
        data_final = (
                datetime.strptime(df, '%Y-%m-%d %H:%M:%S') - timedelta(days=1)
        ).strftime("%Y-%m-%d")

        return data_inici, data_final

    def get_objection_file_data(self, cursor, uid, mode, file_id, context=None):
        mode = mode.lower()
        if mode == 'agclos':
            file_obj = self.pool.get('giscedata.objeccions.agclos.line')
            filename_id = 'agclos_id'
        elif mode == 'agcl5os':
            file_obj = self.pool.get('giscedata.objeccions.agcl5os.line')
            filename_id = 'agcl5os_id'
        elif mode == 'agclacum':
            file_obj = self.pool.get('giscedata.objeccions.agclacum.line')
            filename_id = 'agclacum_id'
        lines = file_obj.search(
            cursor, uid, [(filename_id, '=', file_id)]
        )
        data = file_obj.read(cursor, uid, lines)
        return data

    @staticmethod
    def get_file_mode(context=None):
        if context is None:
            context = {}

        from_model = context.get('from_model', None)
        if from_model == 'giscedata.objeccions.clinmeos':
            return 'CLINMEOS'
        elif from_model == 'giscedata.objeccions.agcl5os':
            return 'AGCL5OS'
        elif from_model == 'giscedata.objeccions.agclos':
            return 'AGCLOS'
        elif from_model == 'giscedata.objeccions.agclacum':
            return 'AGCLACUM'
        else:
            return None

    @job(queue='generate_objections', timeout=3600 * 2)
    def contrast_agcl5os_async(self, cursor, uid, file_id, context=None):
        return self.contrast_agcl5os(cursor, uid, file_id, context)

    def contrast_agcl5os(self, cursor, uid, file_id, context=None):
        """
        Create a keyed dict
        {distribuidora}_{comercialitzadora}_{agree_tensio}_{agree_tarifa}_
        {agree_tensio}_{agree_dh}_{season}
        :param cursor:
        :param uid:
        :param ids:
        :param agcl5os_id:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        from tqdm import tqdm
        logger = logging.getLogger(__name__)
        from_model = context.get('from_model', 'giscedata.objeccions.agcl5os')
        file_obj = self.pool.get(from_model)

        if self.get_file_mode(context) == 'AGCLOS':
            comparativa_linia_obj = self.pool.get(
                'giscedata.objeccions.comparativa.linia.agclos'
            )
            # Preventive delete
            search_params = [('agclos_id_comparativa', '=', file_id)]
            res_ids = comparativa_linia_obj.search(cursor, uid, search_params)
            comparativa_linia_obj.unlink(cursor, uid, res_ids)

            # DataFrame
            data = self.get_objection_file_data(cursor, uid, 'agclos', file_id)
            df_obj_file = pd.DataFrame(data=data)
            df_obj_file.rename(
                index=str, columns={"consum": "consum_ae"}, inplace=True
            )
            data_inici, data_final = self.get_start_end_from_file(
                min(list(df_obj_file['timestamp'])),
                max(list(df_obj_file['timestamp']))
            )
            df_obj_file = df_obj_file.groupby(AGGREGATION).aggregate(
                {'consum_ae': 'sum'}).reset_index()

            # Check duplicates
            df_obj_file["is_duplicate"] = df_obj_file.duplicated()
            df_dups = df_obj_file[df_obj_file['is_duplicate']]
            for duplicated_agg in df_dups.T.to_dict().values():
                duplicated_agg['motiu'] = '004'
                duplicated_agg['agclos_id_comparativa'] = file_id
                comparativa_linia_obj.create(cursor, uid, duplicated_agg,
                                             context)
                comparativa_linia_obj.create(duplicated_agg)

            where_params = [
                ('agree_tipus', 'in', ('03', '04')),
                ('data_inici', '<=', data_final),
                ('data_final', '>=', data_inici),
                ('polissa_id.state', 'not in', CONTRACT_IGNORED_STATES),
            ]
            mode = 'AGCLOS'
        elif self.get_file_mode(context) == 'AGCL5OS':
            comparativa_linia_obj = self.pool.get(
                'giscedata.objeccions.comparativa.linia.agcl5os'
            )
            # Preventive delete
            search_params = [('agcl5os_id_comparativa', '=', file_id)]
            res_ids = comparativa_linia_obj.search(cursor, uid, search_params)
            comparativa_linia_obj.unlink(cursor, uid, res_ids)

            # DataFrame
            data = self.get_objection_file_data(cursor, uid, 'agcl5os', file_id)
            df_obj_file = pd.DataFrame(data=data)
            data_inici, data_final = self.get_start_end_from_file(
                min(list(df_obj_file['timestamp'])),
                max(list(df_obj_file['timestamp']))
            )

            df_obj_file = df_obj_file.groupby(AGGREGATION).aggregate(
                {'consum_ae': 'sum'}).reset_index()

            # Check duplicates
            df_obj_file["is_duplicate"] = df_obj_file.duplicated()
            df_dups = df_obj_file[df_obj_file['is_duplicate']]
            for duplicated_agg in df_dups.T.to_dict().values():
                duplicated_agg['motiu'] = '004'
                duplicated_agg['agcl5os_id_comparativa'] = file_id
                comparativa_linia_obj.create(cursor, uid, duplicated_agg, context)
                comparativa_linia_obj.create(duplicated_agg)

            where_params = [
                ('potencia', '<=', 15),
                ('agree_tarifa', 'in', ('21', '2A')),
                ('data_inici', '<=', data_final),
                ('data_final', '>=', data_inici),
                ('polissa_id.state', 'not in', CONTRACT_IGNORED_STATES),
            ]
            mode = 'AGCL5OS'
        else:
            return None

        df_obj_file.drop('is_duplicate', axis=1, inplace=True, errors='ignore')
        aggregates_dict = {}

        aggregate_base = {
            "consumption": 0,
            "n_punts": 0,
            "m_real": 0,
            "n_real": 0,
            "m_estimada": 0,
            "n_estimada": 0
        }

        # models
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        tg_profiles_obj = self.pool.get('tg.cchfact')
        profiles_obj = self.pool.get('giscedata.profiles.profile')

        comercialitzadora = self.default_comercialitzadora(cursor, uid)

        # AGCLxOS
        from osv.expression import OOQuery
        q = OOQuery(modcon_obj, cursor, uid)
        sql = q.select([
            'data_inici',
            'data_final',
            'agree_tarifa',
            'agree_tensio',
            'agree_dh',
            'agree_tipus',
            'tg',
            'cups.name',
            'distribuidora.ref',
            'cups.id_municipi.state.ree_code'
        ], only_active=False).where(where_params)
        cursor.execute(*sql)

        modcon_list = cursor.dictfetchall()
        try:
            # for all modcons
            for a_modcon in tqdm(modcon_list):
                a_modcon['comercialitzadora'] = comercialitzadora

                di = data_inici
                df = data_final
                if a_modcon['data_inici'] > data_inici:
                    di = a_modcon['data_inici']
                if a_modcon['data_final'] < data_final:
                    df = a_modcon['data_final']
                di_ts, df_ts = self.patch_date_to_complete_period(di, df)

                # using cch_fact
                consumptions_obj = tg_profiles_obj
                fields = FIELDS_TG
                cups_consumptions_ids = consumptions_obj.search(cursor, uid, [
                    (fields['cups'], '=', a_modcon['cups.name']),
                    (fields['timestamp'], '>=', di_ts),
                    (fields['timestamp'], '<=', df_ts)
                ])
                if not cups_consumptions_ids:
                    # using profiles
                    consumptions_obj = profiles_obj
                    fields = FIELDS_PROFILE
                    cups_consumptions_ids = consumptions_obj.search(
                        cursor, uid, [
                            (fields['cups'], '=', a_modcon['cups.name']),
                            (fields['timestamp'], '>=', di_ts),
                            (fields['timestamp'], '<=', df_ts)
                        ])

                if not cups_consumptions_ids:
                    _msg = (
                        "There are no consumptions available for this modcon "
                        "'{}' di:{} df:{}".format(
                            a_modcon['cups.name'], di_ts, df_ts
                        )
                    )
                    logger.info(_msg)
                    continue

                cups_consumptions = consumptions_obj.read(
                    cursor, uid, cups_consumptions_ids, [
                        fields['consumption'],
                        fields['timestamp']
                    ]
                )
                if consumptions_obj == profiles_obj:
                    modcon_consumption = sum([x[fields['consumption']] * fields['magnitud'] for x in cups_consumptions])
                else:
                    dragger = Dragger()
                    modcon_consumption = sum([dragger.drag(x[fields['consumption']] * fields['magnitud']) for x in cups_consumptions])

                # Set data in aggregated_key_dict
                a_modcon['distribuidora'] = a_modcon['distribuidora.ref']
                a_modcon['provincia'] = a_modcon['cups.id_municipi.state.ree_code']
                aggregate_key = "{distribuidora};{comercialitzadora};{agree_tensio};{agree_tarifa};{agree_dh};{agree_tipus};{provincia}".format(
                    **a_modcon
                )

                # Process consumption
                current_aggregate = aggregates_dict.get(aggregate_key, dict(
                    aggregate_base))
                current_aggregate['consumption'] += modcon_consumption
                # Update PS count
                current_aggregate['n_punts'] += 1

                aggregates_dict.update({aggregate_key: current_aggregate})
        except Exception as e:
            print (e)

        report = []
        head = [
            'distribuidora', 'comercialitzadora', 'agree_tensio',
            'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia',
            'consum_calculat', 'n_punts', 'm_real', 'n_real', 'm_estimada',
            'n_estimada'
        ]
        for k, v in aggregates_dict.iteritems():
            report.append(tuple(k.split(';') + [
                v['consumption'],
                v['n_punts'], v['m_real'],
                v['n_real'], v['m_estimada'],
                v['n_estimada']
            ]))

        df_data = pd.DataFrame(report, columns=head)
        # Prepare dfs
        df_data['agree_tarifa'] = df_data['agree_tarifa'].astype('str', errors='ignore')
        df_data['consum_calculat'] = df_data['consum_calculat'].astype('int', errors='ignore')
        df_obj_file['consum_ae'] = df_obj_file['consum_ae'].astype('int', errors='ignore')
        df_data.drop(
            ['m_real', 'n_real', 'n_estimada', 'm_estimada'], axis=1,
            inplace=True, errors='ignore'
        )

        # Merge & calc diffs
        df = pd.merge(
            df_obj_file, df_data, how='left', on=AGGREGATION, indicator=True
        )
        for key in ('consum_calculat', 'consum_ae', 'n_punts'):
            df[key] = df[key].fillna(0)
        try:
            df['motiu'] = np.where(df['_merge'] == 'left', '004', False)
        except Exception as e:
            _msg = 'Has failed the aggregation calculation not our {}'.format(e)
            logger.info(_msg)
        df['diferencia'] = df['consum_calculat'] - df['consum_ae']

        # Create aggregations diff (comparativa.linia)
        aggs = df.T.to_dict().values()
        for agg in aggs:
            if mode == 'AGCL5OS':
                agg['agcl5os_id_comparativa'] = file_id
            else:
                agg['agclos_id_comparativa'] = file_id
            try:
                percentual = float((abs(agg['diferencia']) / float(max(agg['consum_calculat'], agg['consum_ae']))) * 100)
            except ZeroDivisionError:
                percentual = 0.0
            agg['consum_existeix'] = True
            agg['data_inici'] = di_ts
            agg['data_final'] = df_ts
            agg['percentual'] = percentual
            if agg['motiu'] == 'False':
                agg.pop('motiu', None)
            comparativa_linia_obj.create(cursor, uid, agg, context)
        file_obj.write(cursor, uid, file_id, {
            'data_comparacio': datetime.now(), 'state': 'done'
        })

    @job(queue='generate_objections', timeout=3600 * 2)
    def contrast_aggregations_async(self, cursor, uid, agclacums_ids, file_type, context=None):
        self.contrast_aggregations(cursor, uid, agclacums_ids, file_type, context=context)

    def contrast_aggregations(self, cursor, uid, agclacums_ids, file_type, context=None):
        """
        Create a keyed dict
        {distribuidora}_{comercialitzadora}_{agree_tensio}_{agree_tarifa}_
        {agree_tensio}_{agree_dh}_{season}
        :param file_id: file identifier int
        :param context: {'from_model': str}
        :return:
        """
        if context is None:
            context = {}

        from tqdm import tqdm
        logger = logging.getLogger(__name__)
        tg_profiles_obj = self.pool.get('tg.cchfact')
        profiles_obj = self.pool.get('giscedata.profiles.profile')
        period_obj = self.pool.get('giscedata.objeccions.periode')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        if not file_type:
            file_type = context.get('type', 'agclacum')
        file_info = period_obj.get_files_info(cursor, uid, file_type, context=context)
        comparativa_linia_obj = file_info['comparative_obj']
        file_obj = file_info['model_obj']
        # Preventive delete
        search_params = [('fitxer_id_comparativa', 'in', agclacums_ids)]
        res_ids = comparativa_linia_obj.search(cursor, uid, search_params)
        comparativa_linia_obj.unlink(cursor, uid, res_ids)

        # DataFrame
        df_obj_file = pd.DataFrame({})
        for file_id in agclacums_ids:
            data = self.get_objection_file_data(cursor, uid, file_type.upper(), file_id)
            simple_df = pd.DataFrame(data=data)
            if df_obj_file.empty:
                df_obj_file = simple_df.copy()
            else:
                df_obj_file = df_obj_file.append(simple_df)
        df_obj_file.rename(
            index=str, columns={"consum": "consum_ae"}, inplace=True
        )
        df_obj_file = df_obj_file.groupby(AGGREGATION + ['agclacum_id']).aggregate(
            {'consum_ae': 'sum'}).reset_index()
        # df_obj_file = df_obj_file[df_obj_file['magnitud'] == 'AE']
        df_obj_file.drop(
            ['magnitud', 'n_hores'], axis=1,
            inplace=True, errors='ignore'
        )
        dates = period_obj.read(cursor, uid, context.get('active_id'), ['data_inici', 'data_final'])
        data_inici, data_final = self.convert_periodhour_to_date(dates['data_inici'], dates['data_final'])

        for agg in df_obj_file.T.to_dict().values():
            where_params = [
                ('distribuidora.ref', '=', agg['distribuidora']),
                ('agree_tensio', '=', agg['agree_tensio']),
                ('agree_tarifa', '=', agg['agree_tarifa']),
                ('agree_dh', '=', agg['agree_dh']),
                ('agree_tipus', '=', agg['agree_tipo'].zfill(2)),
                ('cups.id_municipi.state.ree_code', '=', agg['provincia']),
                ('data_inici', '<=', data_final),
                ('data_final', '>=', data_inici),
                ('polissa_id.state', '!=', 'cancelada'),
            ]
            from osv.expression import OOQuery
            q = OOQuery(modcon_obj, cursor, uid)
            sql = q.select([
                'data_inici',
                'data_final',
                'agree_tarifa',
                'agree_tensio',
                'agree_dh',
                'agree_tipus',
                'tg',
                'cups.name',
                'distribuidora.ref',
                'cups.id_municipi.state.ree_code'
            ], only_active=False).where(where_params)
            cursor.execute(*sql)
            consum_agg = 0
            for modcon in cursor.dictfetchall():
                di = data_inici
                df = data_final
                if modcon['data_inici'] > data_inici:
                    di = modcon['data_inici']
                if modcon['data_final'] < data_final:
                    df = modcon['data_final']
                di_ts, df_ts = self.patch_date_to_complete_period(di, df)

                # using cch_fact
                consumptions_obj = tg_profiles_obj
                fields = FIELDS_TG
                cups_consumptions_ids = consumptions_obj.search(cursor, uid, [
                    (fields['cups'], '=', modcon['cups.name']),
                    (fields['timestamp'], '>=', di_ts),
                    (fields['timestamp'], '<=', df_ts)
                ])
                if not cups_consumptions_ids:
                    # using profiles
                    consumptions_obj = profiles_obj
                    fields = FIELDS_PROFILE
                    cups_consumptions_ids = consumptions_obj.search(
                        cursor, uid, [
                            (fields['cups'], '=', modcon['cups.name']),
                            (fields['timestamp'], '>=', di_ts),
                            (fields['timestamp'], '<=', df_ts)
                        ])
                cups_consumptions = consumptions_obj.read(
                    cursor, uid, cups_consumptions_ids, [
                        fields['consumption'],
                        fields['timestamp']
                    ]
                )
                if consumptions_obj == profiles_obj:
                    modcon_consumption = sum([x[fields['consumption']] * fields['magnitud'] for x in cups_consumptions])
                else:
                    dragger = Dragger()
                    modcon_consumption = sum(
                        [dragger.drag(x[fields['consumption']] * fields['magnitud']) for x in cups_consumptions])
                consum_agg += modcon_consumption
            agclacum_id = agg.pop('agclacum_id', [])
            agg.update(
                {
                    'consum_calculat': consum_agg,
                    'consum_existeix': True,
                    'fitxer_id_comparativa': agclacum_id[0]
                }
            )
            comparativa_linia_obj.create(cursor, uid, agg)
            logger.info(
                'Contrasted agg {distribuidora}{agree_tensio}{agree_tarifa}{agree_dh}{agree_tipo}{provincia}'.format(
                    **agg
                )
            )
        file_obj.write(cursor, uid, agclacums_ids, {
            'data_comparacio': datetime.now(), 'state': 'done'
        })

    def make_revac(self, cursor, uid, ids, context=None):
        """
        Create a REVAC file
        """
        revac_obj = self.pool.get('giscedata.objeccions.revac')
        if isinstance(ids, int):
            ids = [ids]

        args_ = revac_obj.gen_revac(
            cursor, uid, ids, context=context
        )
        self.attach_measures_file_to_lot(
            cursor, uid, ids, args_, context=None)

        return True

    def attach_measures_file_to_lot(self, cursor, uid, ids, args, context=None):
        """
        Create an attachment related to lot with measure file
        """
        attach_obj = self.pool.get('ir.attachment')

        # Generate file
        file_name = '{type}_{comer}_{periode}_{data}.{version}'.format(**args)

        check_version = file_name.replace('.1', '')
        attach_ids = attach_obj.search(
            cursor, uid, [('datas_fname', '=like', check_version + '%')]
        )
        if attach_ids:
            versions = [att['datas_fname'] for att in attach_obj.read(
                cursor, uid, attach_ids, ['datas_fname']
            )]
            version = int(max(versions).split('.')[1])
            version += 1
            file_name = file_name.split('.')[0] + '.' + str(version)

        with open(args['filename'], "rb") as _file:
            content = b64encode(_file.read())

        if isinstance(ids, int or float):
            ids = [ids]

        vals = {
            'name': file_name,
            'datas_fname': file_name,
            'res_id': ids[0],
            'res_model': 'giscedata.objeccions.periode',
            'datas': content,
        }
        attach_obj.create(cursor, uid, vals)

        try:
            unlink(args['filename'])
        except:
            # Try del file from disk
            pass

        return True

    def _get_related_attachments(self, cursor, uid, ids, field_name, arg,
                                 context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        attach_obj = self.pool.get('ir.attachment')
        for periode in self.read(cursor, uid, ids, ['id'], context=context):
            attach_ids = attach_obj.search(
                cursor, uid, [
                    ('res_id', '=', periode['id']),
                    ('res_model', '=', 'giscedata.objeccions.periode')
                ]
            )
            res[periode['id']] = attach_ids

        return res

    _columns = {
        'name': fields.char('Període', size=16, required=True, readonly=True),
        'data_inici': fields.date('Data inici', readonly=True),
        'data_final': fields.date('Data final', readonly=True),
        'clinmeos_comparat': fields.char(
            'CLINMEOS Comparat', size=40, readonly=True
        ),
        'agcls_comparats': fields.char(
            'AGCLS Comparats', size=60, readonly=True
        ),
        'ultima_comprovacio_consums': fields.char(
            'Última comprovació consums', readonly=True, size=19
        ),
        'ultima_carrega_respostes': fields.char(
            'Última càrrega de respostes', readonly=True, size=19
        ),
        'agregacions_totals_ids': fields.one2many(
            'giscedata.objeccions.totals', 'periode_id',
            'Comprovació de consums'
        ),
        'respostes_objeccions_ids': fields.one2many(
            'giscedata.objeccions.aobjeagcl.line', 'periode_id',
            'Respostes Objeccions'
        ),
        'clinmeos_ids': fields.one2many(
            'giscedata.objeccions.clinmeos', 'periode', 'CLINMEOS'
        ),
        'agclos_ids': fields.one2many(
            'giscedata.objeccions.agclos', 'periode', 'AGCLOS'
        ),
        'agcl5os_ids': fields.one2many(
            'giscedata.objeccions.agcl5os', 'periode', 'AGCL5OS'
        ),
        'agclacum_ids': fields.one2many(
            'giscedata.objeccions.agclacum', 'periode', 'AGCLACUM'
        ),
        'objeinme_ids': fields.one2many(
            'giscedata.objeccions.objeinme', 'periode', 'OBJEINME'
        ),
        'related_attachments': fields.function(
            _get_related_attachments,
            method=True,
            string='Fitxers generats',
            type='one2many',
            relation='ir.attachment'
        ),
    }


GiscedataObjeccionsPeriode()


class GiscedataObjeccionsCLINMEOS(osv.osv):

    _name = 'giscedata.objeccions.clinmeos'

    _state_selection = [
        ('open', 'Open'),
        ('processing', 'Processing'),
        ('error', 'Error'),
        ('done', 'Done'),
    ]

    _columns = {
        'name': fields.char('Nom', size=34, required=True, readonly=True),
        'periode': fields.many2one(
            'giscedata.objeccions.periode', 'Període', ondelete='cascade'
        ),
        'data_creacio': fields.date('Data creació', readonly=True),
        'data_comparacio': fields.datetime('Data última comparació'),
        'data_inici': fields.date('Data inici', required=True, readonly=True),
        'data_final': fields.date('Data final', required=True, readonly=True),
        'versio': fields.char('Versió', size=2, required=True, readonly=True),
        'lines': fields.one2many(
            'giscedata.objeccions.clinmeos.line', 'clinmeos_id',
            'Línies CLINMEOS'
        ),
        'state': fields.selection(_state_selection, 'Status', required=True),
    }

    _defaults = {
        'state': lambda *x: 'open'
    }


GiscedataObjeccionsCLINMEOS()


class GiscedataObjeccionsCLINMEOSLine(osv.osv):

    _name = 'giscedata.objeccions.clinmeos.line'

    _rec_name = 'clinmeos_id'

    _columns = {
        'clinmeos_id': fields.many2one(
            'giscedata.objeccions.clinmeos', 'CLINMEOS', required=True,
            ondelete='cascade'
        ),
        'cups': fields.char('CUPS', size=22, required=True, readonly=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True, readonly=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True, readonly=True
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True, readonly=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True, readonly=True),
        'agree_dh': fields.char('DH', size=2, required=True, readonly=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True, readonly=True),
        'provincia': fields.char('Provincia', size=2, required=True, readonly=True),
        'mesura_ab': fields.char(
            'Indicador mesura ALTA/BAIXA', size=1, required=True, readonly=True
        ),
        'data_inici': fields.datetime('Data inici', required=True, readonly=True),
        'data_final': fields.datetime('Data final', required=True, readonly=True),
        'consum_ae': fields.integer('Consum AE', required=True, readonly=True),
        'consum_r1': fields.integer('Consum R1', required=True, readonly=True),
        'consum_r4': fields.integer('Consum R4', required=True, readonly=True),
        'n_estimats': fields.integer('Períodes estimats', required=True, readonly=True),
        'consum_ae_estimada': fields.integer(
            'Consum AE Estimada', required=True, readonly=True
        ),
    }


GiscedataObjeccionsCLINMEOSLine()


class GiscedataObjeccionsAGCLOS(osv.osv):

    _name = 'giscedata.objeccions.agclos'

    _state_selection = [
        ('open', 'Open'),
        ('processing', 'Processing'),
        ('error', 'Error'),
        ('done', 'Done'),
    ]

    _columns = {
        'name': fields.char('Nom', size=34, required=True, readonly=True),
        'periode': fields.many2one(
            'giscedata.objeccions.periode', 'Període', ondelete='cascade'
        ),
        'data_comparacio': fields.datetime('Data última comparació'),
        'data_inici': fields.date('Data inici', required=True, readonly=True),
        'data_final': fields.date('Data final', required=True, readonly=True),
        'versio': fields.char('Versió', size=3, required=True, readonly=True),
        'lines': fields.one2many(
            'giscedata.objeccions.agclos.line', 'agclos_id',
            'Línies AGCLOS'
        ),
        'state': fields.selection(_state_selection, 'Status', required=True),
    }

    _defaults = {
        'state': lambda *x: 'open'
    }


GiscedataObjeccionsAGCLOS()


class GiscedataObjeccionsAGCLOSLine(osv.osv):
    _name = 'giscedata.objeccions.agclos.line'

    _rec_name = 'agclos_id'

    _columns = {
        'agclos_id': fields.many2one(
            'giscedata.objeccions.agclos', 'AGCLOS', required=True,
            ondelete='cascade'
        ),
        'timestamp': fields.datetime('Data', required=True, readonly=True),
        'magnitud': fields.char('Magnitud', size=2, required=True, readonly=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True, readonly=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True, readonly=True
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True, readonly=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True, readonly=True),
        'agree_dh': fields.char('DH', size=2, required=True, readonly=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True, readonly=True),
        'provincia': fields.char('Provincia', size=2, required=True, readonly=True),
        'consum': fields.integer('Consum', required=True, readonly=True),
        'n_punts': fields.integer('Número Punts', required=True, readonly=True),
        'origen': fields.integer('Orígen', required=True, readonly=True),
        'indicador_firmeza': fields.char(
            'Indicador Firmesa', size=2, required=True, readonly=True
        ),
    }


GiscedataObjeccionsAGCLOSLine()


class GiscedataObjeccionsAGCL5OS(osv.osv):

    _name = 'giscedata.objeccions.agcl5os'

    _state_selection = [
        ('open', 'Open'),
        ('processing', 'Processing'),
        ('error', 'Error'),
        ('done', 'Done'),
    ]

    _columns = {
        'name': fields.char('Nom', size=34, required=True, readonly=True),
        'periode': fields.many2one(
            'giscedata.objeccions.periode', 'Període', ondelete='cascade'
        ),
        'data_comparacio': fields.datetime('Data última comparació'),
        'data_inici': fields.date('Data inici', required=True, readonly=True),
        'data_final': fields.date('Data final', required=True, readonly=True),
        'versio': fields.char('Versió', size=3, required=True, readonly=True),
        'lines': fields.one2many(
            'giscedata.objeccions.agcl5os.line', 'agcl5os_id',
            'Línies AGCL5OS'
        ),
        'state': fields.selection(_state_selection, 'Status', required=True),
    }

    _defaults = {
        'state': lambda *x: 'open'
    }


GiscedataObjeccionsAGCL5OS()


class GiscedataObjeccionsAGCL5OSLine(osv.osv):
    _name = 'giscedata.objeccions.agcl5os.line'

    _rec_name = 'agcl5os_id'

    _columns = {
        'agcl5os_id': fields.many2one(
            'giscedata.objeccions.agcl5os', 'AGCL5OS', required=True,
            ondelete='cascade'
        ),
        'distribuidora': fields.char('Distribuidora', size=4, required=True, readonly=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True, readonly=True
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True, readonly=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True, readonly=True),
        'agree_dh': fields.char('DH', size=2, required=True, readonly=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True, readonly=True),
        'provincia': fields.char('Provincia', size=2, required=True, readonly=True),
        'timestamp': fields.datetime('Data', required=True, readonly=True),
        'season': fields.integer('Estació', required=True, readonly=True),
        'consum_ae': fields.integer('Consum AE', required=True, readonly=True),
        'n_punts': fields.integer('Número Punts', required=True, readonly=True),
        'consum_ae_real': fields.integer('Consum Real', required=True, readonly=True),
        'n_real': fields.integer('Número Punts Real', required=True, readonly=True),
        'consum_ae_estimada': fields.integer('Consum Estimada', required=True, readonly=True),
        'n_estimats': fields.integer('Períodes estimats', required=True, readonly=True),
    }


GiscedataObjeccionsAGCL5OSLine()


class GiscedataObjeccionsAGCLACUM(osv.osv):

    _name = 'giscedata.objeccions.agclacum'

    _state_selection = [
        ('open', 'Open'),
        ('processing', 'Processing'),
        ('error', 'Error'),
        ('done', 'Done'),
    ]

    _columns = {
        'name': fields.char('Nom', size=34, required=True, readonly=True),
        'periode': fields.many2one(
            'giscedata.objeccions.periode', 'Període', ondelete='cascade'
        ),
        'data_comparacio': fields.datetime('Data última comparació'),
        'data_inici': fields.date('Data', required=True, readonly=True),
        'versio': fields.char('Versió', size=3, required=True, readonly=True),
        'lines': fields.one2many(
            'giscedata.objeccions.agclacum.line', 'agclacum_id',
            'Línies AGCLACUM'
        ),
        'state': fields.selection(_state_selection, 'Status', required=True),
    }

    _defaults = {
        'state': lambda *x: 'open'
    }


GiscedataObjeccionsAGCLACUM()


class GiscedataObjeccionsAGCLACUMLine(osv.osv):

    _name = 'giscedata.objeccions.agclacum.line'
    _rec_name = 'agclacum_id'
    _order = 'magnitud asc'

    _columns = {
        'agclacum_id': fields.many2one(
            'giscedata.objeccions.agclacum', 'AGCLACUM', required=True,
            ondelete='cascade'
        ),
        'distribuidora': fields.char('Distribuidora', size=4, required=True, readonly=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True, readonly=True
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True, readonly=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True, readonly=True),
        'agree_dh': fields.char('DH', size=2, required=True, readonly=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True, readonly=True),
        'provincia': fields.char('Provincia', size=2, required=True, readonly=True),
        'magnitud': fields.char('Magnitud', size=2, required=True, readonly=True),
        'consum': fields.integer('Consum Real', required=True, readonly=True),
        'consum_estimat': fields.integer('Consum Estimat', readonly=True),
        'n_hores': fields.integer('Número Hores', required=True, readonly=True),
    }
    # consum_estimat forma part de AGCLACUM5, per AGCLACUM utilitza el default a 0

    _defaults = {
        'consum_estimat': lambda *a: 0
    }


GiscedataObjeccionsAGCLACUMLine()


class GiscedataObjeccionsOBJEINME(osv.osv):

    _name = 'giscedata.objeccions.objeinme'

    def gen_objeinme(self, cursor, uid, ids, periode_id, clinmeos_id=None, auto_obj=False, context=None):
        """Crea un fitxer OBJEINME per distribuidora i motiu. Crea un cas
        CRM i un fitxer per cada distribuidora. n casos y n fitxers per n
        distribuidores.
        :param periode_id: periode objeccions integer id
        :param clinmeos_id:  CLINMEOS integer id
        :param auto_obj: True, False
        :param context: {'active_id': periode_objeccions_id}
        :return: Llista de diccionaris: {
            'type': 'OBJEINME', 'filename': fitxer temporal persistit a disc,
            'distri': distribuidora str, 'comer': comer str, 'periode': 'YYYYMM',
            'data': data actual 'origin_name': fitxers CLINMEOS origen, 'version': integer versio
        }
        """
        if not context:
            context = {}

        clinmeos_obj = self.pool.get('giscedata.objeccions.clinmeos')
        comparativa_linia_obj = self.pool.get('giscedata.objeccions.comparativa.linia')
        clinmeos_line_obj = self.pool.get('giscedata.objeccions.clinmeos.line')

        generated_files = []
        objection_ids = comparativa_linia_obj.search(cursor, uid, [
            ('motiu', '!=', False),
            ('clinmeos_id_comparativa', '=', clinmeos_id)
        ])
        if not objection_ids:
            return generated_files
        objections_data = comparativa_linia_obj.read(
            cursor, uid, objection_ids,
            ['cups', 'consum_ae', 'consum_calculat', 'distribuidora', 'motiu',
             'descripcio', 'clinmeos_id_comparativa', 'clinmeos_line_id']
        )

        objections_by_distri = {}
        for a_cups in objections_data:
            cups = a_cups['cups']
            distri = a_cups['distribuidora']

            if distri not in objections_by_distri:
                objections_by_distri[distri] = []
            objections_by_distri[distri].append(a_cups)

        clinmeos_data = clinmeos_obj.read(cursor, uid, clinmeos_id, ['data_inici', 'data_final'])
        date_objection_init = clinmeos_data['data_inici'].replace('-', '') + ' 01'
        date_objection_end = clinmeos_data['data_final'].replace('-', '') + ' 00'
        if auto_obj:
            auto_objection = 'S'
        else:
            auto_objection = 'N'

        clinmeos_lines = clinmeos_obj.read(
            cursor, uid, clinmeos_id, ['lines']
        )['lines']

        res = []
        for distri in objections_by_distri:
            a_distri = objections_by_distri[distri]
            objeinme_lines = []
            for line in a_distri:
                if not line['descripcio']:
                    comment = ""
                else:
                    comment = line['descripcio']
                if line['motiu'] == '800':
                    consum_imp = 0
                    consum_calc = 0
                else:
                    consum_imp = int(line['consum_ae'])
                    consum_calc = int(line['consum_calculat'])

                objeinme_lines.append(';'.join(
                    [
                        line['cups'],
                        date_objection_init,
                        date_objection_end,
                        line['motiu'],
                        str(consum_imp),
                        str(consum_calc),
                        comment,
                        auto_objection + ';',
                    ]
                ))
            file_target_date = clinmeos_data['data_inici'].replace('-', '')[:6]
            lines_data = clinmeos_line_obj.read(cursor, uid, clinmeos_lines, ['comercialitzadora'])[0]

            file_content = ""
            for elem in objeinme_lines:
                file_content += elem + '\n'
            _id, filename = mkstemp(suffix='.csv')
            with open(filename, "wb") as file_:
                file_.write(file_content)

            origin_name = clinmeos_obj.read(cursor, uid, clinmeos_id, ['name'])['name']
            # Prepare a list of dicts with file info
            res.append({
                'type': 'OBJEINME',
                'filename': filename,
                'distri': str(distri),
                'comer': str(lines_data['comercialitzadora']),
                'periode': file_target_date,
                'data': datetime.now().strftime('%Y%m%d'),
                'origin_name': origin_name,
                'version': 1
            })

        return res

    _columns = {
        'name': fields.char('Nom', size=55, required=True),
        'data_creacio': fields.char('Data creació', size=8),
        'versio': fields.char('Versió', size=2, required=True),
        'periode': fields.many2one(
            'giscedata.objeccions.periode', 'Període', ondelete='cascade'
        ),
        'linies_comparativa_id': fields.one2many(
            'giscedata.objeccions.comparativa.linia', 'objeinme_id',
            'Comparativa'
        )
    }


GiscedataObjeccionsOBJEINME()


class GiscedataObjeccionsOBJEAGCL(osv.osv):

    _name = 'giscedata.objeccions.objeagcl'

    def gen_objeagcl(self, cursor, uid, ids, periode_id, agclos_id=None, agcl5os_id=None, auto_obj=False, context=None):
        """Crea un fitxer OBJEAGCL per distribuidora i motiu. Crea un fitxer per
        cada distribuidora. n fitxers per n distribuidores.
        :param periode_id: periode objeccions integer id
        :param agclos_id:  AGCLOS integer id
        :param agcl5os_id: AGCL5OS integer id
        :param auto_obj: True, False
        :param context: {'active_id': periode_objeccions_id}
        :return: Llista de diccionaris: {
            'type': 'OBJEAGCL', 'filename': fitxer temporal persistit a disc,
            'distri': distribuidora str, 'comer': comer str, 'periode': 'YYYYMM',
            'data': data actual 'origin_name': fitxers AGCL origen, 'version': integer versio
        }
        """
        if context is None:
            context = {}
        if not agclos_id and not agcl5os_id:
            raise osv.except_osv('Error', 'Fitxers AGCLS no especificats')

        ca_obj = self.pool.get('giscedata.objeccions.comparativa.linia.agclos')
        ca5_obj = self.pool.get('giscedata.objeccions.comparativa.linia.agcl5os')
        period_obj = self.pool.get('giscedata.objeccions.periode')
        periode = period_obj.read(cursor, uid, periode_id, ['name'])['name']

        res = []
        generated_files = []
        objections_data = []
        if agclos_id:
            comparativa_obj = ca_obj
            objection_ids = comparativa_obj.search(cursor, uid, [
                ('motiu', '!=', False),
                ('agclos_id_comparativa', '=', agclos_id)
            ])
            if objection_ids:
                objections_data += comparativa_obj.read(
                    cursor, uid, objection_ids, OBJEAGCL_HEADER
                )
        if agcl5os_id:
            comparativa_obj = ca5_obj
            objection_ids = ca5_obj.search(cursor, uid, [
                ('motiu', '!=', False),
                ('agcl5os_id_comparativa', '=', agcl5os_id)
            ])
            if objection_ids:
                objections_data += comparativa_obj.read(
                    cursor, uid, objection_ids, OBJEAGCL_HEADER
                )
        df_objeagcl = pd.DataFrame(data=objections_data)
        if df_objeagcl.empty:
            return generated_files
        if auto_obj:
            auto_objection = 'S'
        else:
            auto_objection = 'N'

        # Prepare df
        df_objeagcl['autobjeccio'] = auto_objection
        date_keys = ['data_inici', 'data_final']
        for key in date_keys:
            df_objeagcl[key] = df_objeagcl[key].replace(False, '')
            df_objeagcl[key] = df_objeagcl[key].fillna('')
            df_objeagcl[key] = df_objeagcl[key].apply(lambda x: x[:13].replace('-', '/'))
        comer = list(df_objeagcl['comercialitzadora'])[0]
        distris = list(set(df_objeagcl['distribuidora']))
        for distri in distris:
            df_by_distri = df_objeagcl[
                df_objeagcl['distribuidora'] == str(distri)]
            df_by_distri = df_by_distri.replace(False, '')
            _id, filename = mkstemp(suffix='.csv')
            df_by_distri.to_csv(
                filename, sep=';', header=False, columns=OBJEAGCL_HEADER,
                index=False, line_terminator=';\n'
            )

            origin_name = []
            if agclos_id:
                a_obj = self.pool.get('giscedata.objeccions.agclos')
                agclos_name = a_obj.read(cursor, uid, agclos_id, ['name'])['name']
                origin_name.append(agclos_name)
            if agcl5os_id:
                a5_obj = self.pool.get('giscedata.objeccions.agcl5os')
                agcl5os_name = a5_obj.read(cursor, uid, agcl5os_id, ['name'])['name']
                origin_name.append(agcl5os_name)
            origin_name = ' + '.join(origin_name)

            # Prepare a list of dicts with file info
            res.append({
                'type': 'OBJEAGCL',
                'filename': filename,
                'distri': str(distri),
                'comer': str(comer),
                'periode': periode,
                'data': datetime.now().strftime('%Y%m%d'),
                'origin_name': origin_name,
                'version': 1
            })

        return res

    _columns = {

    }


GiscedataObjeccionsOBJEAGCL()


class GiscedataObjeccionsComparativaLinia(osv.osv):

    _name = 'giscedata.objeccions.comparativa.linia'

    def create(self, cursor, uid, vals, context=None):
        """ Calc default % diff and kWh diff """
        consumption_cups = vals.get('consum_calculat', 0)
        clinmeos_consumption = vals.get('consum_ae', 0)

        diff = consumption_cups - clinmeos_consumption
        try:
            percentual = float(
                (abs(diff) / float(max(consumption_cups, clinmeos_consumption)))
                * 100
            )
        except ZeroDivisionError:
            percentual = 0.0
        vals['diferencia'] = diff
        vals['percentual'] = percentual

        # Set agg
        agg = vals.copy()
        for key in AGGREGATION[1:]:
            if key not in agg:
                agg[key] = ""
        vals['agree_codi'] = (
            "{distribuidora};{agree_tensio};{agree_tarifa};{agree_dh};{agree_tipo};{provincia}".format(**agg)
        )

        return super(GiscedataObjeccionsComparativaLinia, self).create(
            cursor, uid, vals, context
        )

    def search(self, cursor, uid, args, offset=0, limit=0, order=None, context=None, count=False):
        new_args = []
        for arg in args:
            key = arg[0]
            operator = arg[1]
            res = arg[2]
            if key == 'agree_codi':
                new_args.append([key, operator, res.upper()])
            elif (key == 'motiu') and (res == 'any_reason'):
                new_args.append([key, '!=', 'null'])
            else:
                new_args.append(arg)
        return super(GiscedataObjeccionsComparativaLinia, self).search(
            cursor, uid, new_args, offset=offset, limit=limit, order=order, context=context, count=count
        )

    _columns = {
        'cups': fields.char('CUPS', size=22, required=True),
        'consum_ae': fields.integer('Consum imputat', required=True),
        'consum_calculat': fields.integer('Consum', required=True),
        'consum_existeix': fields.boolean(
            'Existeix okW', default=False
        ),
        'data_inici_imputat': fields.datetime('Data inici Imputat'),
        'data_final_imputat': fields.datetime('Data final Imputat'),
        'data_inici_calculat': fields.datetime('Data inici Calculat'),
        'data_final_calculat': fields.datetime('Data final Calculat'),
        'data_inici_consum': fields.datetime('Data inici Consum'),
        'data_final_consum': fields.datetime('Data final Consum'),
        'motiu': fields.selection(OBJECTION_REASON, 'Motiu Objecció'),
        'descripcio': fields.text('Descripció Objecció'),
        'diferencia': fields.float('Diff kWh'),
        'percentual': fields.float('Diff %'),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'agree_tensio': fields.char('Nivell Tensió', size=2),
        'agree_tarifa': fields.char('Tarifa', size=4),
        'agree_dh': fields.char('DH', size=2),
        'agree_tipo': fields.char('Tipo de Punt', size=2),
        'provincia': fields.char('Provincia', size=2),
        'agree_codi': fields.char('Codi agregació', size=200),
        'clinmeos_id_comparativa': fields.many2one(
            'giscedata.objeccions.clinmeos', 'CLINMEOS', ondelete='cascade'
        ),
        'clinmeos_line_id': fields.many2one(
            'giscedata.objeccions.clinmeos.line', 'Línia clinmeos',
            ondelete='cascade'
        ),
        'objeinme_id': fields.many2one(
            'giscedata.objeccions.objeinme', 'Fitxer objeccio',
            ondelete='cascade'
        ),
    }


GiscedataObjeccionsComparativaLinia()


class GiscedataObjeccionsComparativaLiniaAGCLOS(osv.osv):

    _name = 'giscedata.objeccions.comparativa.linia.agclos'

    def create(self, cursor, uid, vals, context=None):
        """ Set default required fields """
        period_obj = self.pool.get('giscedata.objeccions.periode')
        consumption_calc = vals.get('consum_calculat', 0)
        consumption_ae = vals.get('consum_ae', 0)
        agree_tipo = vals.get('agree_tipo', 0)
        default_comer = period_obj.default_comercialitzadora(cursor, uid)
        comer = vals.get('comercialitzadora', default_comer)
        vals['consum_calculat'] = consumption_calc
        vals['consum_ae'] = consumption_ae
        vals['comercialitzadora'] = comer
        vals['agree_tipo'] = agree_tipo.zfill(2)

        agg = vals.copy()
        for key in AGGREGATION[1:]:
            if key not in agg:
                agg[key] = ""
        vals['agree_codi'] = (
            "{distribuidora};{agree_tensio};{agree_tarifa};{agree_dh};{agree_tipo};{provincia}".format(**agg)
        )

        return super(GiscedataObjeccionsComparativaLiniaAGCLOS, self).create(
            cursor, uid, vals, context
        )

    def search(self, cursor, uid, args, offset=0, limit=0, order=None, context=None, count=False):
        new_args = []
        for arg in args:
            key = arg[0]
            operator = arg[1]
            res = arg[2]
            if key == 'agree_codi':
                new_args.append([key, operator, res.upper()])
            elif (key == 'motiu') and (res == 'any_reason'):
                new_args.append([key, '!=', 'null'])
            else:
                new_args.append(arg)
        return super(GiscedataObjeccionsComparativaLiniaAGCLOS, self).search(
            cursor, uid, new_args, offset=offset, limit=limit, order=order, context=context, count=count
        )

    _columns = {
        'consum_ae': fields.integer('Consum imputat', required=True),
        'consum_calculat': fields.integer('Consum calculat', required=True),
        'consum_proposat': fields.integer('Consum proposat'),
        'consum_existeix': fields.boolean(
            'Existeix Consum', default=False
        ),
        'diferencia': fields.float('Diff kWh'),
        'percentual': fields.float('Diff %'),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=4, required=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True),
        'provincia': fields.char('Provincia', size=2, required=True),
        'agree_codi': fields.char('Codi agregació', size=200),
        'motiu': fields.selection(OBJECTION_REASON, 'Motiu Objecció'),
        'descripcio': fields.text('Descripció Objecció'),
        'data_inici': fields.datetime('Data inici'),
        'data_final': fields.datetime('Data final'),
        'agclos_id_comparativa': fields.many2one(
            'giscedata.objeccions.agclos', 'AGCLOS', ondelete='cascade'
        ),
    }

    _defaults = {
        'consum_proposat': lambda *x: 0
    }


GiscedataObjeccionsComparativaLiniaAGCLOS()


class GiscedataObjeccionsComparativaLiniaAGCL5OS(osv.osv):

    _name = 'giscedata.objeccions.comparativa.linia.agcl5os'
    _rec_name = 'comercialitzadora'

    def create(self, cursor, uid, vals, context=None):
        """ Set default required fields """
        period_obj = self.pool.get('giscedata.objeccions.periode')
        consumption_calc = vals.get('consum_calculat', 0)
        consumption_ae = vals.get('consum_ae', 0)
        agree_tipo = vals.get('agree_tipo', '5')
        default_comer = period_obj.default_comercialitzadora(cursor, uid)
        comer = vals.get('comercialitzadora', default_comer)
        vals['consum_calculat'] = consumption_calc
        vals['consum_ae'] = consumption_ae
        vals['comercialitzadora'] = comer
        vals['agree_tipo'] = agree_tipo.zfill(2)

        # Set agg
        agg = vals.copy()
        for key in AGGREGATION[1:]:
            if key not in agg:
                agg[key] = ""
        vals['agree_codi'] = (
            "{distribuidora};{agree_tensio};{agree_tarifa};{agree_dh};{agree_tipo};{provincia}".format(**agg)
        )

        return super(GiscedataObjeccionsComparativaLiniaAGCL5OS, self).create(
            cursor, uid, vals, context
        )

    def search(self, cursor, uid, args, offset=0, limit=0, order=None, context=None, count=False):
        new_args = []
        for arg in args:
            key = arg[0]
            operator = arg[1]
            res = arg[2]
            if key == 'agree_codi':
                new_args.append([key, operator, res.upper()])
            elif (key == 'motiu') and (res == 'any_reason'):
                new_args.append([key, '!=', 'null'])
            else:
                new_args.append(arg)
        return super(GiscedataObjeccionsComparativaLiniaAGCL5OS, self).search(
            cursor, uid, new_args, offset=offset, limit=limit, order=order, context=context, count=count
        )

    _columns = {
        'consum_ae': fields.integer('Consum imputat', required=True),
        'consum_calculat': fields.integer('Consum calculat', required=True),
        'consum_proposat': fields.integer('Consum proposat'),
        'consum_existeix': fields.boolean(
            'Existeix Consum', default=False
        ),
        'diferencia': fields.float('Diff kWh'),
        'percentual': fields.float('Diff %'),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True),
        'provincia': fields.char('Provincia', size=2, required=True),
        'agree_codi': fields.char('Codi agregació', size=200),
        'motiu': fields.selection(OBJECTION_REASON, 'Motiu Objecció'),
        'descripcio': fields.text('Descripció Objecció'),
        'data_inici': fields.datetime('Data inici'),
        'data_final': fields.datetime('Data final'),
        'agcl5os_id_comparativa': fields.many2one(
            'giscedata.objeccions.agcl5os', 'AGCL5OS', ondelete='cascade'
        )
    }

    _defaults = {
        'consum_proposat': lambda *x: 0
    }


GiscedataObjeccionsComparativaLiniaAGCL5OS()


class GiscedataObjeccionsComparativaLiniaAgregacions(osv.osv):

    _name = 'giscedata.objeccions.comparativa.linia.agregacions'
    _rec_name = 'comercialitzadora'

    def create(self, cursor, uid, vals, context=None):
        """ Set default required fields """
        period_obj = self.pool.get('giscedata.objeccions.periode')
        consumption_calc = int(vals.get('consum_calculat', 0))
        consumption_ae = int(vals.get('consum_ae', 0))
        agree_tipo = vals.get('agree_tipo', 0)
        default_comer = period_obj.default_comercialitzadora(cursor, uid)
        comer = vals.get('comercialitzadora', default_comer)

        diff = consumption_calc - consumption_ae
        try:
            percentual = float(
                (abs(diff) / float(max(consumption_calc, consumption_ae)))
                * 100
            )
        except ZeroDivisionError:
            percentual = 0.0
        vals['diferencia'] = diff
        vals['percentual'] = percentual
        vals['consum_calculat'] = consumption_calc
        vals['consum_ae'] = consumption_ae
        vals['comercialitzadora'] = comer
        vals['agree_tipo'] = agree_tipo.zfill(2)

        agg = vals.copy()
        for key in AGGREGATION[1:]:
            if key not in agg:
                agg[key] = ""
        vals['agree_codi'] = (
            "{distribuidora};{agree_tensio};{agree_tarifa};{agree_dh};{agree_tipo};{provincia}".format(**agg)
        )

        return super(GiscedataObjeccionsComparativaLiniaAgregacions, self).create(
            cursor, uid, vals, context
        )

    def search(self, cursor, uid, args, offset=0, limit=0, order=None, context=None, count=False):
        new_args = []
        for arg in args:
            key = arg[0]
            operator = arg[1]
            res = arg[2]
            if key == 'agree_codi':
                new_args.append([key, operator, res.upper()])
            elif (key == 'motiu') and (res == 'any_reason'):
                new_args.append([key, '!=', 'null'])
            else:
                new_args.append(arg)
        return super(GiscedataObjeccionsComparativaLiniaAgregacions, self).search(
            cursor, uid, new_args, offset=offset, limit=limit, order=order, context=context, count=count
        )

    _columns = {
        'consum_ae': fields.integer('Consum imputat', required=True),
        'consum_calculat': fields.integer('Consum calculat', required=True),
        'consum_proposat': fields.integer('Consum proposat'),
        'consum_existeix': fields.boolean(
            'Existeix Consum', default=False
        ),
        'diferencia': fields.float('Diff kWh'),
        'percentual': fields.float('Diff %'),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=4, required=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True),
        'provincia': fields.char('Provincia', size=2, required=True),
        'agree_codi': fields.char('Codi agregació', size=200),
        'motiu': fields.selection(OBJECTION_REASON, 'Motiu Objecció'),
        'descripcio': fields.text('Descripció Objecció'),
        'data_inici': fields.datetime('Data inici'),
        'data_final': fields.datetime('Data final'),
        'fitxer_id_comparativa': fields.many2one(
            'giscedata.objeccions.agclacum', 'AGCLACUM', ondelete='cascade'
        ),
    }

    _defaults = {
        'consum_proposat': lambda *x: 0
    }


GiscedataObjeccionsComparativaLiniaAgregacions()


class GiscedataObjeccionsTotals(osv.osv):

    _name = 'giscedata.objeccions.totals'
    _rec_name = 'comercialitzadora'

    def create(self, cursor, uid, vals, context=None):
        """ Set zfilled agree tipo """
        agree_tipo = str(vals.get('agree_tipo', '0'))
        vals['agree_tipo'] = agree_tipo.zfill(2)

        return super(GiscedataObjeccionsTotals, self).create(
            cursor, uid, vals, context
        )

    _columns = {
        'periode_id': fields.many2one(
            'giscedata.objeccions.periode', 'Període', required=True
        ),
        'consum_clinmeos': fields.integer('Consum CLINMEOS', required=True),
        'consum_agcls': fields.integer('Consum AGCLS', required=True),
        'diferencia': fields.integer('Diff kWh (CLINMEOS - AGCLS)'),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True
        ),
        'agree_tensio': fields.char('Nivell Tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo de Punt', size=2, required=True),
        'provincia': fields.char('Provincia', size=2, required=True),
    }


GiscedataObjeccionsTotals()


class GiscedataObjeccionsAOBJEAGCLLine(osv.osv):

    _name = 'giscedata.objeccions.aobjeagcl.line'
    _rec_name = 'codi_objeccio'

    _columns = {
        'periode_id': fields.many2one(
            'giscedata.objeccions.periode', 'Període', required=True
        ),
        'codi_objeccio': fields.char('Codi Objecció', size=50, readonly=True),
        'distribuidora': fields.char(
            'Distribuidora', size=4, required=True, readonly=True
        ),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True, readonly=True
        ),
        'agree_tensio': fields.char(
            'Nivell Tensió', size=2, required=True, readonly=True
        ),
        'agree_tarifa': fields.char(
            'Tarifa', size=4, required=True, readonly=True
        ),
        'agree_dh': fields.char('DH', size=2, required=True, readonly=True),
        'agree_tipo': fields.char(
            'Tipo de Punt', size=2, required=True, readonly=True
        ),
        'provincia': fields.char(
            'Provincia', size=2, required=True, readonly=True
        ),
        'data_inici': fields.datetime('Data inici', required=True, readonly=True),
        'data_final': fields.datetime('Data final', required=True, readonly=True),
        'motiu': fields.selection(
            OBJECTION_REASON, 'Motiu Objecció', readonly=True
        ),
        'consum_publicat': fields.integer('Consum Publicat', readonly=True),
        'consum_proposat': fields.integer('Consum Proposat', readonly=True),
        'descripcio': fields.text('Descripció Objecció'),
        'objeccio_auto': fields.boolean(
            'Auto-objecció', help='Objecció a auto-objecció'
        ),
        'acceptacio': fields.boolean('Acceptació S/N', required=True),
        'no_acceptacio_motiu': fields.text('Motiu de no acceptació'),
        'comentari_emisor_resposta': fields.text(
            'Comentari emisor de la resposta'),
    }


GiscedataObjeccionsAOBJEAGCLLine()


class GiscedataObjeccionsREVAC(osv.osv):
    _name = 'giscedata.objeccions.revac'

    def gen_revac(self, cursor, uid, ids, context=None):
        response_obj = self.pool.get('giscedata.objeccions.aobjeagcl.line')
        periode_obj = self.pool.get('giscedata.objeccions.periode')
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        res_ids = response_obj.search(
            cursor, uid, [('periode_id', '=', ids), ('acceptacio', '!=', 'N')]
        )
        content = []
        fields_to_read = ['codi_objeccio', 'consum_proposat', 'descripcio']
        for line in response_obj.read(cursor, uid, res_ids, fields_to_read):
            line['motiu_revisio'] = line['descripcio']
            line['indicatiu_noves_dades'] = 'N'
            content.append(line)
        f = NamedTemporaryFile(delete=False)
        df = pd.DataFrame(data=content)
        df.to_csv(
            f.name, sep=';', header=False, columns=REVAC_HEADER,
            index=False, line_terminator=';\n'
        )
        comer = periode_obj.default_comercialitzadora(cursor, uid, context)
        periode = periode_obj.read(cursor, uid, ids, ['name'])
        if periode:
            periode = periode['name']
        res = {
            'type': 'REVAC',
            'filename': f.name,
            'comer': comer,
            'periode': periode,
            'data': datetime.now().strftime('%Y%m%d'),
            'version': 1
        }

        return res

    _columns = {

    }


GiscedataObjeccionsREVAC()
