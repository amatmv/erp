from osv import osv, fields


class StockProductionLot(osv.osv):

    _inherit = 'stock.production.lot'

    _columns = {
        'manufacturing_year': fields.integer('Manufacturing year',
                                             readonly=True)
    }


StockProductionLot()
