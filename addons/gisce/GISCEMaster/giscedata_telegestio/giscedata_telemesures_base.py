# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from tools import cache

from giscedata_telemesures_base.giscedata_telemesures_base import TECHNOLOGY_TYPE_METER_SEL

if 'telegestio' not in dict(TECHNOLOGY_TYPE_METER_SEL).keys():
   TECHNOLOGY_TYPE_METER_SEL += [('telegestion', 'Telegestión')]


TREE_TITLES = {
    'tg.billing': _('Billings'),
    'tg.profile': _('Profiles'),
}

class GiscedataRegistradorTG(osv.osv):

    _name = 'giscedata.registrador'
    _inherit = 'giscedata.registrador'

    def register_tg(self, cursor, uid, cnc_id, tg_name, context=None):
        technology = 'telegestion'
        rtype = 'unknown'
        exists = self.search(cursor, uid, [('name', '=', tg_name)])
        reg_data = {'name': tg_name, 'cnc_id': cnc_id, 'technology': technology}
        if not exists:
            reg_data.update({'type': rtype})
            return self.create(cursor, uid, reg_data)
        else:
            self.write(cursor, uid, exists[0], reg_data)
            return exists[0]

    _columns = {
        'cnc_id': fields.many2one('tg.concentrator', 'Concentrator'),
        'pc_act': fields.date('PCact ActDate'),
        'pc_act_tr1': fields.integer('TR1'),
        'pc_act_tr2': fields.integer('TR2'),
        'pc_act_tr3': fields.integer('TR3'),
        'pc_act_tr4': fields.integer('TR4'),
        'pc_act_tr5': fields.integer('TR5'),
        'pc_act_tr6': fields.integer('TR6'),
        'pc_latent': fields.date('PCLatent ActDate'),
        'pc_latent_tr1': fields.integer('TR1'),
        'pc_latent_tr2': fields.integer('TR2'),
        'pc_latent_tr3': fields.integer('TR3'),
        'pc_latent_tr4': fields.integer('TR4'),
        'pc_latent_tr5': fields.integer('TR5'),
        'pc_latent_tr6': fields.integer('TR6'),
    }


GiscedataRegistradorTG()
