import random
from datetime import datetime, timedelta
from dateutil import parser
import logging
try:
    from collections import Counter
except ImportError:
    from backport_collections import Counter
import time
import unittest
from tools import cache

from destral import testing
from destral.transaction import Transaction
from giscedata_telegestio.utils import *
from giscedata_telegestio.telegestio import (
    KIND_REAL, KIND_REAL_PROFILED, KIND_ADJUSTED, KIND_AUTOREAD_PROFILED,
    KIND_HISTORY_PROFILED, KIND_USE_FACTOR_PROFILED
)
from enerdata.datetime.timezone import TIMEZONE
from enerdata.profiles.profile import ProfileHour, Profile
from enerdata.profiles import Dragger
from enerdata.contracts.tariff import get_tariff_by_code
from expects import *

from switching.input.messages import TG, message
from addons import get_module_resource

from primestg.report import Report as primeReport

logger = logging.getLogger(__name__)


def get_random_meter_name():
    return 'TEST{0}'.format(int(time.time() * 10**6))


class BaseTests(unittest.TestCase):

    def test_get_season(self):
        winter_dt = TIMEZONE.localize(datetime(2015, 1, 1))
        expect(get_season(winter_dt)).to(be('W'))

        summer_dt = TIMEZONE.localize(datetime(2015, 8, 1))
        expect(get_season(summer_dt)).to(be('S'))

    def test_convert_to_mongo(self):
        p = ProfileHour(
            TIMEZONE.localize(datetime(2015, 1, 1, 1)),
            23,
            True,
            0.0
        )
        vals = convert_to_mongodb(p)
        expect(vals).to(have_keys(
            timestamp='2015-01-01 01:00:00',
            ai_fact=23,
            valid=True,
            cch_fact=True,
            season='W',
            magn=1
        ))

    def parse_esios_datetime(self):
        d = parser.parser('2015-10-25T02:00:00.000+02:00')
        self.assertEqual(localize_esios_dt(d).tzname(), 'CEST')
        d = parser.parser('2015-10-25T02:00:00.000+01:00')
        self.assertEqual(localize_esios_dt(d).tzname(), 'CET')


class FixProfileTests(testing.OOTestCase):

    def setUp(self):
        self.mongodb_name = 'test_{0}'.format(int(time.time()))
        self.meter_name = get_random_meter_name()
        self.create_profile()
        super(FixProfileTests, self).setUp()

    def tearDown(self):
        self.remove_profile()

    def create_profile(self):
        tg_profile_obj = self.openerp.pool.get('tg.profile')

        item_tmpl = {
            u'ae': u'0',
            u'ai': u'17',
            u'id': 9,
            u'magn': u'1',
            u'name': u'{}'.format(self.meter_name),
            u'r1': u'0',
            u'r2': u'0',
            u'r3': u'0',
            u'r4': u'24',
            u'season': u'S',
            u'timestamp': False,
            u'valid': False,
            u'valid_date': False
        }
        self.measures = []
        self.start = datetime(2015, 5, 1, 1)
        self.end = datetime(2015, 6, 1, 0)

        gap_start = datetime(2015, 5, 15)
        gap_end = datetime(2015, 5, 16)
        start_idx = self.start
        self.gaps = []
        self.number_invalid_hours = 0
        self.complete_profile = []
        while start_idx <= self.end:
            energy = random.randint(0, 1000)  # in Wh
            profile_hour = item_tmpl.copy()
            profile_hour['timestamp'] = start_idx.strftime('%Y-%m-%d %H:00:00')
            profile_hour['ai'] = energy

            self.complete_profile.append(profile_hour)
            if gap_start < start_idx < gap_end:
                self.gaps.append(start_idx)
                start_idx += timedelta(hours=1)
                continue
            if random.randint(0, 10) > 8:
                valid = False
                self.number_invalid_hours += 1
                self.gaps.append(start_idx)
            else:
                valid = True
            profile_hour['valid'] = valid
            with Transaction().start(self.database) as txn:
                p_id = tg_profile_obj.create(txn.cursor, txn.user, profile_hour)
                profile_hour['id'] = p_id
            self.measures.append(profile_hour)
            start_idx += timedelta(hours=1)
        logger.debug('{0} random measures created for meter {1}'.format(
            len(self.measures), self.meter_name
        ))

    def remove_profile(self):
        tg_profile_obj = self.openerp.pool.get('tg.profile')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            unlink_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name)
            ])
            logger.debug('Removing {0} measures'.format(len(unlink_ids)))
            tg_profile_obj.unlink(txn.cursor, txn.user, unlink_ids)
        self.number_invalid_hours = 0
        # Clear lists
        del self.measures[:]
        del self.gaps[:]
        del self.complete_profile[:]

    def test_fix_profile(self):
        tg_profile_obj = self.openerp.pool.get('tg.profile')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            profiles_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name),
                ('timestamp', '>=', self.start.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', self.end.strftime('%Y-%m-%d %H:%M:%S'))
            ])
            expect(len(profiles_ids)).to(equal(len(self.measures)))
            ph_measures = []
            for measure in tg_profile_obj.read(cursor, uid, profiles_ids):
                ph_measures.append(convert_to_profilehour(measure))
            start = TIMEZONE.localize(self.start)
            end = TIMEZONE.localize(self.end)
            profile = Profile(start, end, ph_measures)

            number_of_gaps = len(profile.gaps)
            expect(number_of_gaps).to(equal(len(self.gaps)))

            balance = Counter()
            tariff_code = '2.0DHA'
            tariff = get_tariff_by_code(tariff_code)()
            tariff.cof = 'A'

            drag = Dragger()
            for ph in self.complete_profile:
                period = tariff.get_period_by_date(TIMEZONE.localize(
                    datetime.strptime(ph['timestamp'], '%Y-%m-%d %H:%M:%S')
                ))
                value = ph['ai'] / 1000.0
                balance[period.code] += drag.drag(value, period.code)

            tg_profile_obj.fix(
                cursor,
                uid,
                self.meter_name,
                self.start.strftime('%Y-%m-%d %H:%M:%S'),
                self.end.strftime('%Y-%m-%d %H:%M:%S'),
                '2.0DHA',
                balance,
                '50-00'
            )

            profiles_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name),
                ('timestamp', '>=', self.start.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', self.end.strftime('%Y-%m-%d %H:%M:%S'))
            ])
            expect(len(profiles_ids)).to(equal(profile.n_hours))
            for p in tg_profile_obj.read(cursor, uid, profiles_ids):
                dt = localize_season(p['timestamp'], p['season'])
                if dt in profile.gaps:
                    expect(p['kind_fact']).to(equal(KIND_AUTOREAD_PROFILED))
                else:
                    expect(p['kind_fact']).to(equal(KIND_REAL))

    def test_adjust_profile(self):
        tg_profile_obj = self.openerp.pool.get('tg.profile')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            profiles_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name),
                ('timestamp', '>=', self.start.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', self.end.strftime('%Y-%m-%d %H:%M:%S'))
            ])
            expect(len(profiles_ids)).to(equal(len(self.measures)))
            ph_measures = []
            for measure in tg_profile_obj.read(cursor, uid, profiles_ids):
                ph_measures.append(convert_to_profilehour(measure))
            start = TIMEZONE.localize(self.start)
            end = TIMEZONE.localize(self.end)
            profile = Profile(start, end, ph_measures)

            number_of_gaps = len(profile.gaps)
            expect(number_of_gaps).to(equal(len(self.gaps)))

            balance = Counter()
            tariff_code = '2.0DHA'
            tariff = get_tariff_by_code(tariff_code)()
            tariff.cof = 'A'

            drag = Dragger()
            for ph in self.complete_profile:
                period = tariff.get_period_by_date(TIMEZONE.localize(
                    datetime.strptime(ph['timestamp'], '%Y-%m-%d %H:%M:%S')
                ))
                value = ph['ai'] * 0.0005  # Convert to kW and force to adjust
                balance[period.code] += drag.drag(value, period.code)

            tg_profile_obj.fix(
                cursor,
                uid,
                self.meter_name,
                self.start.strftime('%Y-%m-%d %H:%M:%S'),
                self.end.strftime('%Y-%m-%d %H:%M:%S'),
                '2.0DHA',
                balance,
                '20-00'
            )

            profiles_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name),
                ('timestamp', '>=', self.start.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', self.end.strftime('%Y-%m-%d %H:%M:%S'))
            ])
            expect(len(profiles_ids)).to(equal(profile.n_hours))
            expect(sum(balance.values())).to(be_below(
                sum(x['ai'] for x in self.complete_profile))
            )
            for p in tg_profile_obj.read(cursor, uid, profiles_ids):
                dt = localize_season(p['timestamp'], p['season'])
                if dt in profile.gaps:
                    expect(p['kind_fact']).to(equal(KIND_REAL_PROFILED))
                    expect(p['ai_fact']).to(equal(0))
                else:
                    if p['ai'] != p['ai_fact']:
                        expect(p['kind_fact']).to(equal(KIND_ADJUSTED))

    def test_estimated_consumtion_is_profiled(self):
        tg_profile_obj = self.openerp.pool.get('tg.profile')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            profiles_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name),
                ('timestamp', '>=', self.start.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', self.end.strftime('%Y-%m-%d %H:%M:%S'))
            ])
            expect(len(profiles_ids)).to(equal(len(self.measures)))
            ph_measures = []
            for measure in tg_profile_obj.read(cursor, uid, profiles_ids):
                ph_measures.append(convert_to_profilehour(measure))
            start = TIMEZONE.localize(self.start)
            end = TIMEZONE.localize(self.end)
            profile = Profile(start, end, ph_measures)

            number_of_gaps = len(profile.gaps)
            expect(number_of_gaps).to(equal(len(self.gaps)))

            balance = Counter()
            tariff_code = '2.0DHA'
            tariff = get_tariff_by_code(tariff_code)()
            tariff.cof = 'A'

            drag = Dragger()
            for ph in self.complete_profile:
                period = tariff.get_period_by_date(TIMEZONE.localize(
                    datetime.strptime(ph['timestamp'], '%Y-%m-%d %H:%M:%S')
                ))
                value = ph['ai'] / 1000.0
                balance[period.code] += drag.drag(value, period.code)

            tg_profile_obj.fix(
                cursor,
                uid,
                self.meter_name,
                self.start.strftime('%Y-%m-%d %H:%M:%S'),
                self.end.strftime('%Y-%m-%d %H:%M:%S'),
                '2.0DHA',
                balance,
                '40-01'
            )

            profiles_ids = tg_profile_obj.search(cursor, uid, [
                ('name', '=', self.meter_name),
                ('timestamp', '>=', self.start.strftime('%Y-%m-%d %H:%M:%S')),
                ('timestamp', '<=', self.end.strftime('%Y-%m-%d %H:%M:%S'))
            ])
            expect(len(profiles_ids)).to(equal(profile.n_hours))
            ai_fact = 0
            for p in tg_profile_obj.read(cursor, uid, profiles_ids):
                dt = localize_season(p['timestamp'], p['season'])
                expect(p['kind_fact']).to(equal(KIND_HISTORY_PROFILED))
                ai_fact += p['ai_fact']
            expect(ai_fact).to(equal(sum(balance.values() * 1000)))


class TG_Tests(testing.OOTestCase):

    def setUp(self):
        self.mongodb_name = 'test_{0}'.format(int(time.time()))
        s05_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S05.xml'
        )
        self.s05_xml = open(s05_path, 'r')

        s04_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S04.xml'
        )
        self.s04_xml = open(s04_path, 'r')

        s04_exc_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S04_exception.xml'
        )
        self.s04_exc_xml = open(s04_exc_path, 'r')

        s12_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S12.xml'
        )
        self.s12_xml = open(s12_path, 'r')

        s15_1_1_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S15_1_1.xml'
        )
        self.s15_1_1_xml = open(s15_1_1_path, 'r')

        s15_3_5_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S15_3_5.xml'
        )
        self.s15_3_5_xml = open(s15_3_5_path, 'r')

        s02_24_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S02.xml'
        )
        self.s02_24_xml = open(s02_24_path, 'r')

        s09_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S09.xml'
        )
        self.s09_xml = open(s09_path, 'r')

        s13_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S13.xml'
        )
        self.s13_xml = open(s13_path, 'r')

        s17_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S17.xml'
        )
        self.s17_xml = open(s17_path, 'r')

        s15_exc_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S15_exception.xml'
        )
        self.s15_exc_xml = open(s15_exc_path, 'r')

        s02_exc_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S02_exception.xml'
        )
        self.s02_exc_xml = open(s02_exc_path, 'r')

        s09_mul_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S09_multiple_meters.xml'
        )
        self.s09_mul_xml = open(s09_mul_path, 'r')

        s24_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S24.xml'
        )
        self.s24_xml = open(s24_path, 'r')

        s24_one_meter_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S24_one_meter.xml'
        )
        self.s24_one_meter_xml = open(s24_one_meter_path, 'r')

        s24_one_meter_failing_path = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S24_failing.xml'
        )
        self.s24_one_meter_failing_xml = open(s24_one_meter_failing_path, 'r')

        s27_xml = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S27.xml'
        )
        self.s27_xml = open(s27_xml, 'r')

        s27_no_values_xml = get_module_resource(
            'giscedata_telegestio', 'tests', 'data', 'S27_no_values.xml'
        )
        self.s27_no_values_xml = open(s27_no_values_xml, 'r')
        
    def tearDown(self):
        self.s05_xml.close()
        self.s04_xml.close()
        self.s04_exc_xml.close()
        self.s12_xml.close()
        self.s15_1_1_xml.close()
        self.s15_3_5_xml.close()
        self.s02_24_xml.close()
        self.s09_xml.close()
        self.s13_xml.close()
        self.s17_xml.close()
        self.s15_exc_xml.close()
        self.s02_exc_xml.close()
        self.s24_xml.close()
        self.s24_one_meter_xml.close()
        self.s24_one_meter_failing_xml.close()
        self.s27_xml.close()
        self.s27_no_values_xml.close()
        self.clean()
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.unlink_tg_models(cursor, uid)
            cache.clean_caches_for_db(cursor.dbname)

    def clean(self):
        tg_billing_obj = self.openerp.pool.get('tg.billing')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0036301516")])
            tg_billing_obj.unlink(cursor, uid, bill_ids)

    def has_registrator(self, cursor, uid, name, cnc_name):
        reg_obj = self.openerp.pool.get('giscedata.registrador')

        r_ids = reg_obj.search(cursor, uid, [('name', '=', name)])
        self.assertEqual(len(r_ids), 1)
        reg = reg_obj.browse(cursor, uid, r_ids[0], ['cnc_id', 'technology'])
        self.assertEqual(reg.technology, 'telegestion')
        self.assertEqual(reg.cnc_id.name, cnc_name)
        return True

    def unlink_tg_models(self, cursor, uid):
        for model_name in ['tg.event', 'tg.profile', 'tg.meter.availability', 'tg.billing']:

            cch_obj = self.openerp.pool.get(model_name)
            del_ids = cch_obj.search(cursor, uid, [])
            cch_obj.unlink(cursor, uid, del_ids)

    def test_load_S05(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')

        tg_xml = primeReport(self.s05_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIV0036301516', 'CT11')
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0036301516")])
            assert len(bill_ids) == 28

    def test_load_S05_contract1_only(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')
        res_config_obj = self.openerp.pool.get('res.config')

        tg_xml = primeReport(self.s05_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Sets only contract 1 data (Ctr=1)
            res_config_obj.set(cursor, uid, 'tg_contract_1_only', '1')
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIV0036301516', 'CT11')
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0036301516")])
            assert len(bill_ids) == 14

    def test_load_S04(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')

        tg_xml = primeReport(self.s04_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIV0036301516', 'CT11')
            self.has_registrator(cursor, uid, 'ZIV0038500293', 'CT11')
            self.has_registrator(cursor, uid, 'ZIV0038500292', 'CT11')
            self.has_registrator(cursor, uid, 'ZIV0038500291', 'CT11')
            self.has_registrator(cursor, uid, 'ZIV0038500290', 'CT11')
            self.has_registrator(cursor, uid, 'ZIV0038500289', 'CT11')
            self.has_registrator(cursor, uid, 'ZIV0036393087', 'CT11')
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0036301516")])
            assert len(bill_ids) == 14

    def test_load_S04_with_warnings(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')

        expected_warning = \
        "\nZIV0036301516:\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00001228230000W (Fhi) year is out of range\n"

        tg_xml = primeReport(self.s04_exc_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with self.assertRaises(ValueError) as vm:
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIV0036301516', 'CT11')
            self.assertEqual(expected_warning, vm.exception.message)
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0036301516")])
            assert len(bill_ids) == 14

    def test_load_S04_contract1_only(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')
        res_config_obj = self.openerp.pool.get('res.config')

        tg_xml = primeReport(self.s04_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Sets only contract 1 data (Ctr=1)
            res_config_obj.set(cursor, uid, 'tg_contract_1_only', '1')
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIV0036301516', 'CT11')
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0036301516")])
            assert len(bill_ids) == 14

    def test_load_S12(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_concentrator_obj = self.openerp.pool.get('tg.concentrator')

        tg_xml = primeReport(self.s12_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            ids = tg_concentrator_obj.search(cursor, uid, [])
            db_params = tg_concentrator_obj.read(cursor, uid, ids,
                                                 ['name',
                                                  'rpt_ftp_ip_address',
                                                  'rpt_ftp_user',
                                                  'fwdcup_ftp_ip_address',
                                                  'fwdcup_ftp_user',
                                                  'fwmtup_ftp_ip_address',
                                                  'fwmtup_ftp_user',
                                                  'cycle_ftp_ip_address',
                                                  'cycle_ftp_user'])
            for cnc in tg_xml.concentrators:
                db_cnc = (item for item in db_params if item["name"] ==
                          cnc.name).next()
                tg_concentrator_obj.get_cnc(cursor, uid, cnc.name)
                values = cnc.values
                for val in values:
                    expect(val['rpt_ftp_ip_address']).to(
                        equal(db_cnc['rpt_ftp_ip_address']))
                    expect(val['rpt_ftp_user']).to(
                        equal(db_cnc['rpt_ftp_user']))
                    expect(val['fwdcup_ftp_ip_address']).to(
                        equal(db_cnc['fwdcup_ftp_ip_address']))
                    expect(val['fwdcup_ftp_user']).to(
                        equal(db_cnc['fwdcup_ftp_user']))
                    expect(val['fwmtup_ftp_ip_address']).to(
                        equal(db_cnc['fwmtup_ftp_ip_address']))
                    expect(val['fwmtup_ftp_user']).to(
                        equal(db_cnc['fwmtup_ftp_user']))
                    expect(val['cycle_ftp_ip_address']).to(
                        equal(db_cnc['cycle_ftp_ip_address']))
                    expect(val['cycle_ftp_user']).to(
                        equal(db_cnc['cycle_ftp_user']))

    def test_load_S15(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')
        tg_xml_1_1 = primeReport(self.s15_1_1_xml)
        tg_xml_3_5 = primeReport(self.s15_3_5_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_1_1)
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_3_5)
            event_ids = tg_event_obj.search(cursor, uid, [])
            events = tg_event_obj.read(cursor, uid, event_ids, ['name',
                                                                'event_code',
                                                                'event_group'])
            if events[0]['event_code'] == 1:
                expect(events[0]['event_group']).to(equal(1))
                expect(events[1]['event_group']).to(equal(3))
                expect(events[1]['event_code']).to(equal(5))
            else:
                expect(events[1]['event_group']).to(equal(1))
                expect(events[1]['event_code']).to(equal(1))
                expect(events[0]['event_group']).to(equal(3))
                expect(events[0]['event_code']).to(equal(5))

    def test_load_S15_with_warnings(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')

        expected_warning = \
        "ZIV0000035545\n" \
        "ERROR: Reading a concentrator event. Thrown exception: Date out of range: 00001228230000W (Fh) year is out of range\n" \
        "ERROR: Reading a concentrator event. Thrown exception: Date out of range: 00001228230000W (Fh) year is out of range"

        tg_xml_exc = primeReport(self.s15_exc_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with self.assertRaises(ValueError) as vm:
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml_exc)
            self.assertEqual(expected_warning, vm.exception.message)

        event_ids = tg_event_obj.search(cursor, uid, [])
        events = tg_event_obj.read(cursor, uid, event_ids, ['name',
                                                            'event_code',
                                                            'event_group'])
        expect(len(events)).to(equal(1))
        expect(events[0]['event_group']).to(equal(2))
        expect(events[0]['event_code']).to(equal(1))

    def test_load_S02(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_profile_obj = self.openerp.pool.get('tg.profile')
        tg_xml_24 = primeReport(self.s02_24_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_24)
            self.has_registrator(cursor, uid, 'ZIV0040318130', 'ZIV0004394488')
            profile_ids = tg_profile_obj.search(cursor, uid, [('name', '=',
                                                              'ZIV0040318130')])
            self.assertEqual(len(profile_ids), 24)

    def test_load_S02_with_warnings(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_profile_obj = self.openerp.pool.get('tg.profile')

        expected_warning = \
        "\nZIV0040318130:\n" \
        "ERROR: Thrown exception: Date out of range: 00000201130000W (Fh) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00000201140000W (Fh) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00000201150000W (Fh) year is out of range\n" \
        "ERROR: Thrown exception: Date out of range: 00000201160000W (Fh) year is out of range\n"

        tg_xml_24_exc = primeReport(self.s02_exc_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with self.assertRaises(ValueError) as vm:
                tg_reader_obj.insert_primestg(cursor, uid, tg_xml_24_exc)
            self.assertEqual(expected_warning, vm.exception.message)
            self.has_registrator(cursor, uid, 'ZIV0040318130', 'ZIV0004394488')
            profile_ids = tg_profile_obj.search(cursor, uid, [('name', '=',
                                                              'ZIV0040318130')])
            self.assertEqual(len(profile_ids), 20)

    def test_load_S09(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')
        tg_xml_ = primeReport(self.s09_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            self.has_registrator(cursor, uid, 'ZIV0034631044', 'ZIV0000034180')
            event_ids = tg_event_obj.search(cursor, uid, [])
            events = tg_event_obj.read(cursor, uid, event_ids, ['name', 'data',
                                                                'event_code',
                                                                'event_group'])
            for event in events:
                if event['event_code'] == 3:
                    expect(event['data']).to(equal('D1: 0\nD2: 1'))
                    expect(event['event_group']).to(equal(2))
                else:
                    expect(event['event_group']).to(equal(6))
                    expect(event['event_code']).to(be_within(0, 3))

    def test_load_S13(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')
        tg_xml_ = primeReport(self.s13_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            self.has_registrator(cursor, uid, 'ZIV0034631044', 'ZIV0000034180')
            event_ids = tg_event_obj.search(cursor, uid, [])
            events = tg_event_obj.read(cursor, uid, event_ids, ['name', 'data',
                                                                'event_code',
                                                                'event_group'])
            for event in events:
                if event['event_code'] == 3:
                    expect(event['data']).to(equal('D1: 0\nD2: 1'))
                    expect(event['event_group']).to(equal(2))
                else:
                    expect(event['event_group']).to(equal(6))
                    expect(event['event_code']).to(be_within(0, 3))

    def test_load_S17(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')
        tg_xml_ = primeReport(self.s17_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            event_id = tg_event_obj.search(cursor, uid, [])[0]
            event = tg_event_obj.read(cursor, uid, event_id, ['name', 'data',
                                                              'event_code',
                                                              'event_group'])
            expect(event['event_code']).to(equal(9))
            expect(event['data']).to(equal('D1: ZIV0034647715\nD2: 81054'))
            expect(event['event_group']).to(equal(5))

    def test_load_S24(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_avail_obj = self.openerp.pool.get('tg.meter.availability')
        tg_xml_ = primeReport(self.s24_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            self.has_registrator(cursor, uid, 'CIR0141406756', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'CIR0141413501', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'CIR0501500336', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'SAG0155385243', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'ITE0131750207', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'ZIV0036303384', 'CIR4621511030')

            avail_id = tg_avail_obj.search(cursor, uid, [])[0]
            event = tg_avail_obj.read(cursor, uid, avail_id, ['name', 'status',
                                                              'cnc_name',
                                                              'managed'])
            expect(event['name']).to(equal('CIR0141406756'))
            expect(event['status']).to(equal(2))
            expect(event['cnc_name']).to(equal('CIR4621511030'))
            expect(event['managed']).to(equal(True))

    def test_obtain_meter_availability_from_modified_S24(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_avail_obj = self.openerp.pool.get('tg.meter.availability')
        tg_xml_ = primeReport(self.s24_one_meter_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            self.has_registrator(cursor, uid, 'CIR0501407341', 'CIR4621511030')

            date_from = datetime(2018, 4, 10, 0)
            date_to = datetime(2018, 5, 22, 0)
            aggr = tg_avail_obj.obtain_meter_availability_data(cursor, uid, 'CIR0501407341', date_from, date_to)
            total_value = 0
            for hour in aggr[0]:
                total_value += hour['value']
            expect(len(aggr)).to(equal(2))
            expect(len(aggr[0])).to(equal(94))
            expect(total_value).to(equal(8600))

    def test_obtain_meter_availability_for_weekday_S24(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_avail_obj = self.openerp.pool.get('tg.meter.availability')
        tg_xml_ = primeReport(self.s24_one_meter_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            self.has_registrator(cursor, uid, 'CIR0501407341', 'CIR4621511030')

            aggr = tg_avail_obj.obtain_meter_availability_for_weekday(cursor, uid, 'CIR0501407341', '2018-05-10', min_value=51)
            hour = tg_avail_obj.get_best_conn_hour_from_aggr(aggr)
            expect(hour).to(equal(1))

    def test_obtain_meter_availability_for_failing_meter_S24(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_avail_obj = self.openerp.pool.get('tg.meter.availability')
        tg_xml_ = primeReport(self.s24_one_meter_failing_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            self.has_registrator(cursor, uid, 'CIR0141406756', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'CIR0141413501', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'CIR0501221770', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'CIR0501222666', 'CIR4621511030')
            self.has_registrator(cursor, uid, 'CIR0501308446', 'CIR4621511030')

            aggr = tg_avail_obj.obtain_meter_availability_for_weekday(cursor, uid, 'CIR0141413501', '2018-05-01')
            hour = tg_avail_obj.get_best_conn_hour_from_aggr(aggr)
            expect(hour).to(equal(2))
            expect(aggr[0]['hour']).to(equal(2))
            expect(aggr[0]['value']).to(equal(100))
            expect(aggr[1]['hour']).to(equal(3))
            expect(aggr[1]['value']).to(equal(0))
            expect(aggr[2]['hour']).to(equal(4))
            expect(aggr[2]['value']).to(equal(0))
            expect(aggr[3]['hour']).to(equal(5))
            expect(aggr[3]['value']).to(equal(0))
            expect(aggr[4]['hour']).to(equal(6))
            expect(aggr[4]['value']).to(equal(0))
            expect(aggr[5]['hour']).to(equal(7))
            expect(aggr[5]['value']).to(equal(0))
            expect(aggr[6]['hour']).to(equal(8))
            expect(aggr[6]['value']).to(equal(100))

            aggr = tg_avail_obj.obtain_meter_availability_for_weekday(cursor, uid, 'CIR0501222666', '2018-05-01')
            expect(aggr[0]['hour']).to(equal(2))
            expect(aggr[0]['value']).to(equal(100))
            expect(aggr[1]['hour']).to(equal(3))
            expect(aggr[1]['value']).to(equal(100))
            expect(aggr[2]['hour']).to(equal(4))
            expect(aggr[2]['value']).to(equal(0))
            expect(aggr[3]['hour']).to(equal(5))
            expect(aggr[3]['value']).to(equal(0))
            expect(aggr[4]['hour']).to(equal(6))
            expect(aggr[4]['value']).to(equal(0))
            expect(aggr[5]['hour']).to(equal(7))
            expect(aggr[5]['value']).to(equal(100))
            expect(aggr[6]['hour']).to(equal(8))
            expect(aggr[6]['value']).to(equal(100))

            aggr = tg_avail_obj.obtain_meter_availability_for_weekday(cursor, uid, 'CIR0501221770', '2018-05-01')
            expect(aggr[0]['hour']).to(equal(2))
            expect(aggr[0]['value']).to(equal(100))
            expect(aggr[1]['hour']).to(equal(3))
            expect(aggr[1]['value']).to(equal(100))
            expect(aggr[2]['hour']).to(equal(4))
            expect(aggr[2]['value']).to(equal(100))
            expect(aggr[3]['hour']).to(equal(5))
            expect(aggr[3]['value']).to(equal(100))
            expect(aggr[4]['hour']).to(equal(6))
            expect(aggr[4]['value']).to(equal(100))
            expect(aggr[5]['hour']).to(equal(7))
            expect(aggr[5]['value']).to(equal(100))
            expect(aggr[6]['hour']).to(equal(8))
            expect(aggr[6]['value']).to(equal(0))

    def test_load_S27(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')

        tg_xml = primeReport(self.s27_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIV0044510398', 'ZIV0004395680')
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0044510398")])
            assert len(bill_ids) == 7
            bill_id = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIV0044510398"),
                              ('period', '=', 1)])
            billing = tg_billing_obj.read(cursor, uid, bill_id, ['ai', 'max',
                                                                 'date_begin',
                                                                 'date_end',
                                                                 'type'])[0]
            assert billing['ai'] == 237
            assert billing['max'] == 984
            assert billing['date_begin'] == '2018-10-31 16:44:12'
            assert billing['date_end'] == '2018-10-31 16:44:12'
            assert billing['type'] == 'manual'

    def test_load_S27_without_values(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_billing_obj = self.openerp.pool.get('tg.billing')

        tg_xml = primeReport(self.s27_no_values_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml)
            self.has_registrator(cursor, uid, 'ZIVS004394488', 'ZIV0004394488')
            bill_ids = tg_billing_obj.search(
                cursor, uid, [('name', '=', "ZIVS004394488")])
            assert len(bill_ids) == 7

    def test_load_events_and_deactivate_old(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')
        tg_xml_ = primeReport(self.s09_mul_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            event_ids = tg_event_obj.search(cursor, uid, [('active', '=', True)])
            expect(len(event_ids)).to(equal(21))

    def test_cleaning_of_duplicated_events(self):
        tg_reader_obj = self.openerp.pool.get('tg.reader')
        tg_event_obj = self.openerp.pool.get('tg.event')
        tg_event_val_obj = self.openerp.pool.get('tg.event.validate')
        tg_xml_ = primeReport(self.s09_mul_xml)
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            meters_on_file = ['ZIV0034785808', 'ZIV0034785848', 'ZIV0035000025',
                              'ZIV0037440626', 'ZIV0040318860', 'ZIV0034631030',
                              'ZIV0034631041', 'ZIV0034631044', 'ZIV0034631045',
                              'ZIV0034631049']
            # Insert the file twice to create duplicates
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            tg_reader_obj.insert_primestg(cursor, uid, tg_xml_)
            event_ids = tg_event_obj.search(cursor, uid, [])
            for meter_name in meters_on_file:
                tg_event_val_obj.clean_duplicates(cursor, uid, meter_name)

            clean_event_ids = tg_event_obj.search(cursor, uid, [])

            expect(len(event_ids)).to(equal(464))
            expect(len(clean_event_ids)).to(equal(232))
