# -*- coding: utf-8 -*-

from osv import osv, fields
import netsvc
import pooler
from tools.translate import _
from tools.misc import cache
from mongodb_backend import osv_mongodb
from ftplib import error_perm
from .utils import *
from enerdata.contracts.tariff import get_tariff_by_code
from enerdata.profiles.profile import Profile
from enerdata.datetime.timezone import TIMEZONE
import socket
import logging
import pandas as pd
from mongodb_backend.mongodb2 import mdbpool
from oorq.decorators import job, create_jobs_group
import re
from mongodb_backend.mongodb2 import mdbpool
from datetime import datetime, timedelta, date
from ast import literal_eval
from primestg.report import Report as primeReport
from primestg.report.reports import SUPPORTED_REPORTS
from primestg.message import is_gziped
import base64
from itertools import groupby, count
import altair as alt


KIND_REAL = '1'
KIND_REAL_PROFILED = '2'
KIND_ADJUSTED = '3'
KIND_AUTOREAD_PROFILED = '4'
KIND_HISTORY_PROFILED = '5'
KIND_USE_FACTOR_PROFILED = '6'


ORIGIN_KIND_PROFILED = {
    '10-00': KIND_REAL_PROFILED,
    '11-00': KIND_REAL_PROFILED,
    '20-00': KIND_REAL_PROFILED,
    '21-00': KIND_REAL_PROFILED,
    '30-00': KIND_REAL_PROFILED,
    '31-00': KIND_REAL_PROFILED,
    '40-01': KIND_HISTORY_PROFILED,
    '40-02': KIND_USE_FACTOR_PROFILED,
    '50-00': KIND_AUTOREAD_PROFILED,
    '60-00': KIND_REAL_PROFILED
}
"""Map of oriding code and subcode CODE-SUBCODE to Kind of profiled
"""

CNC_REPORTS = ['S12', 'S15', 'S17', 'S24']


def get_all_meter_names(model):
    tg_model_mdb = mdbpool.get_collection(model)
    return tg_model_mdb.distinct('name')


class TgBilling(osv_mongodb.osv_mongodb):

    _name = 'tg.billing'
    _order = 'date_end desc'

    _type_billing = [
        ('day', 'Daily'),
        ('month', 'Monthly'),
    ]

    _type_value = [
        ('i', 'Incremental'),
        ('a', 'Absolute'),
    ]

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned
        when the correct indexes are created'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            elif field == 'period_sel_from':
                value = int(value)
                field = 'period'
                operator = '>='
            elif field == 'period_sel_to':
                value = int(value)
                field = 'period'
                operator = '<='
            new_args.append((field, operator, value))
        return super(TgBilling,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    def get_billing(self, cursor, uid, meter, date,
                    type, num_periods, context=None):
        '''get billing values for date and meter.
        Go back in time until valid billing found or
        no more billing data'''
        #For one period fare, we have to return 0 period of billing
        #The rest of the fares, takes exactly the same period number
        if num_periods == 1:
            periods = [0]
        else:
            periods = range(1, num_periods + 1)
        search_params = [('date_end', '=', date),
                         ('name', '=', meter),
                         ('type', '=', type),
                         ('value', '=', 'a'),
                         ('valid', '=', True),
                         ('period', 'in', periods)]
        billing_ids = self.search(cursor, uid, search_params,
                                  context=context)
        res = self.read(cursor, uid, billing_ids,
                        [], context=context)
        if len(res) != num_periods:
            if type == 'month':
                type = 'day'
                res = self.get_billing(cursor, uid, meter,
                                       date, type, num_periods,
                                       context=context)
            if type == 'day' and not res:
                date = self.get_previous_date(cursor, uid, meter, date,
                                              type, context=context)
                if not date:
                    return []
                res = self.get_billing(cursor, uid, meter,
                                       date, type, num_periods,
                                       context=context)
        return res

    def get_previous_date(self, cursor, uid, meter,
                          date, type, context=None):
        search_params = [('date_end', '<', date),
                         ('name', '=', meter),
                         ('type', '=', type),
                         ('value', '=', 'a'),
                         ('valid', '=', True)]
        previous_id = self.search(cursor, uid, search_params,
                                  limit=1, order='date_end desc')
        if previous_id:
            previous = self.read(cursor, uid, previous_id,
                                 ['date_end'])[0]
            return previous.get('date_end', False)
        return False

    def get_last_billing_date(self, cursor, uid, meter,
                              date_limit, context=None):
        '''return date of last billing
        @date_limit: date. Limit date to search for billing info
        returns: dict with day and month values'''
        if not context:
            context = {}
        res = {'day': False, 'month': False}
        day_search = [('name', '=', meter),
                      ('value', '=', 'a'),
                      ('valid', '=', True),
                      ('type', '=', 'day'),
                      ('date_end', '<=', date_limit)]
        billing_ids = self.search(cursor, uid, day_search,
                                  limit=1)
        if billing_ids:
            res['day'] = self.read(cursor, uid,
                                   billing_ids,
                                   ['date_end'])[0]['date_end']
        month_search = [('name', '=', meter),
                        ('value', '=', 'a'),
                        ('valid', '=', True),
                        ('type', '=', 'month'),
                        ('date_end', '<=', date_limit)]
        billing_ids = self.search(cursor, uid, month_search,
                                  limit=1)
        if billing_ids:
            res['month'] = self.read(cursor, uid,
                                     billing_ids,
                                     ['date_end'])[0]['date_end']
        return res

    def validate(self, cursor, uid, ids, action='validate', context=None):
        '''mark the read as valid or not
        @action: string with values validate or unvalidate'''

        for id in ids:
            if action == 'validate':
                now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                self.write(cursor, uid, [id],
                           {'valid': True,
                            'valid_date': now})
            elif action == 'unvalidate':
                self.write(cursor, uid, [id],
                           {'valid': False,
                            'valid_date': None})

    def _period_sel(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)

        return res

    _columns = {
        'name': fields.char('Meter', size=50, required=True),
        'contract': fields.integer('Contract'),
        'type': fields.selection(_type_billing, 'Type', size=20,),
        'value': fields.selection(_type_value, 'Value', size=3,),
        'date_begin': fields.datetime('From'),
        'date_end': fields.datetime('To'),
        'period': fields.integer('Period'),
        'max': fields.integer('Max (W)'),
        'date_max': fields.datetime('Max register date'),
        'ai': fields.integer('In'),
        'ae': fields.integer('Out'),
        'r1': fields.integer('R1'),
        'r2': fields.integer('R2'),
        'r3': fields.integer('R3'),
        'r4': fields.integer('R4'),
        'valid': fields.boolean('Valid'),
        'valid_date': fields.datetime('Valid date'),
        'meter_id': fields.integer('MeterID'),
        'cnc_name': fields.char('CnC Name', size=20),
        'period_sel_from': fields.function(
            _period_sel,
            type='selection',
            selection=[(x, 'Period ' + x) for x in '0123456'],
            string='Periode from',
        ),
        'period_sel_to': fields.function(
            _period_sel,
            type='selection',
            selection=[(x, 'Period ' + x) for x in '0123456'],
            string='Periode to',
        )
    }

    _defaults = {
        'max': lambda *a: 0,
        'date_max': lambda *a: False,
        'valid': lambda *a: False,
        'valid_date': lambda *a: False,
        'meter_id': lambda *a: 0,
        'cnc_name': lambda *a: '',
    }

TgBilling()


class TgProfile(osv_mongodb.osv_mongodb):

    _name = 'tg.profile'
    _order = 'timestamp desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgProfile,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    def validate(self, cursor, uid, ids, action='validate', context=None):
        '''mark the read as valid or not
        @action: string with values validate or unvalidate'''

        for id in ids:
            if action == 'validate':
                now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                self.write(cursor, uid, [id],
                           {'valid': True,
                            'valid_date': now})
            elif action == 'unvalidate':
                self.write(cursor, uid, [id],
                           {'valid': False,
                            'valid_date': None})

    _kind_selection = [
        (KIND_REAL, '1 - Real'),
        (KIND_REAL_PROFILED, '2 - Real profiled'),
        (KIND_ADJUSTED, '3 - Adjusted'),
        (KIND_AUTOREAD_PROFILED, '4 - Autoread profiled'),
        (KIND_HISTORY_PROFILED, '5 - History profiled'),
        (KIND_USE_FACTOR_PROFILED, '6 - Use factor profiled')
    ]

    _columns = {
        'name': fields.char('Meter', size=50, required=True),
        'cnc_name': fields.char('CnC Name', size=20),
        'timestamp': fields.datetime('Timestamp'),
        'season': fields.selection([('W', 'Winter'),
                                    ('S', 'Summer')], 'Season', size=1),
        'magn': fields.selection([(1, 'Watts'),
                                  (1000, 'kWatts')], 'Magnitude'),
        'ai': fields.float('In'),
        'ae': fields.float('Out'),
        'r1': fields.float('R1'),
        'r2': fields.float('R2'),
        'r3': fields.float('R3'),
        'r4': fields.float('R4'),
        'bc': fields.char('Bc', size=2, help='Bit 0. Sense significat\n'
                                             'Bit 1. Període incomplet '
                                             'degut a fallada d\'energia\n'
                                             'Bit 2. Intrusió detectada\n'
                                             'Bit 3. Paràmetres modificats '
                                             'durant període\n'
                                             'Bit 4. Verificació de temps '
                                             'durant el període\n'
                                             'Bit 5. Desbordament\n'
                                             'Bit 6. Comptador sincronitzat '
                                             'durant el període\n'
                                             'Bit 7. Mesura invàlida'),
        'valid': fields.boolean('Valid'),
        'valid_date': fields.datetime('Valid date'),
        'ai_fact': fields.float('In fact'),
        'ae_fact': fields.float('Out fact'),
        'r1_fact': fields.float('R1 fact'),
        'r2_fact': fields.float('R2 fact'),
        'r3_fact': fields.float('R3 fact'),
        'r4_fact': fields.float('R4 fact'),
        'kind_fact': fields.selection(_kind_selection,
                                      'CCH obtaining method'),
        'firm_fact': fields.boolean('CCH Firm'),
        'cch_bruta': fields.boolean('CCH raw'),
        'cch_fact': fields.boolean('CCH invoicing'),
    }

    def fix(self, cursor, uid, meter, start, end, tariff, balance, origin):
        """Fix a complete profile using enerdata lib.

        :param cursor: Database cursor
        :param uid: User id
        :param meter: Meter name
        :param start: Start date (included)
        :param end:  End date (included)
        :param tariff: Tariff code
        :param balance: Balance in kW
        :param origin: The origin code in form of `CODE-SUBCODE`. Eg:: `20-00`
        :return: True
        """

        balance = balance.copy()

        logger = logging.getLogger('openerp')

        # Delete all cch_fact True and cch_bruta False because they were
        # gaps and if you want to recalculate it maybe is a refacturation
        del_profiles_ids = self.search(cursor, uid, [
            ('name', '=', meter),
            ('timestamp', '>=', start),
            ('timestamp', '<=', end),
            ('cch_fact', '=', True),
            ('cch_bruta', '=', False)
        ])
        if del_profiles_ids:
            logger.info(
                'Already found {0} gaps filled. Deleting them...'.format(
                    len(del_profiles_ids)
                )
            )
            self.unlink(cursor, uid, del_profiles_ids)

        # First get all the profiles hours from start to end of meter
        profiles_ids = self.search(cursor, uid, [
            ('name', '=', meter),
            ('timestamp', '>=', start),
            ('timestamp', '<=', end)
        ])

        # Transform balance from kW to W
        for period, consumption in balance.items():
            balance[period] = consumption * 1000

        ph_measures = []
        for profile in self.read(cursor, uid, profiles_ids):
            ph_measures.append(convert_to_profilehour(profile))

        start = TIMEZONE.localize(datetime.strptime(start,
                                                    '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(end,
                                                  '%Y-%m-%d %H:00:00'))
        t_obj = self.pool.get('giscedata.polissa.tarifa')
        t_id = t_obj.search(cursor, uid, [('name', '=', tariff)])
        if not t_id:
            raise Exception('{0} is not a valid tariff'.format(tariff))
        t_id = t_id[0]
        tariff = t_obj.read(cursor, uid, t_id, ['name', 'cof'])
        t = get_tariff_by_code(tariff['name'])()
        # Change cof_a, cof_b, cof_c.. to A, B, C
        # TODO: dependency between giscedata_perfils and giscedata_telegestio
        t.cof = tariff['cof'].split('_')[1].upper()
        ph_measures_pass = ph_measures
        if origin.startswith('40'):
            ph_measures_pass = []
        profile = Profile(start, end, ph_measures_pass)
        profile.profile_class = ESIOS_PROFILE_MAP[tariff['name']]
        fixed_profile = profile.fixit(t, balance, 1000)
        logger.info('Profile from meter: {0} ({1} - {2}) have {3} gaps'.format(
            meter, start, end, len(profile.gaps)
        ))
        invalid = dict((m.date, m) for m in ph_measures
                       if not m.valid or origin.startswith('40'))
        adjusted_periods = fixed_profile.adjusted_periods
        for measure in fixed_profile.measures:
            vals = convert_to_mongodb(measure)
            if isinstance(measure, ProfileHour):
                # Is a gap or a measure that is not valid
                logger.info('Meter {0} filling gap {1} with {2}'.format(
                    meter, measure.date, measure.measure
                ))
                # TODO: make gaps kind_fact depends on the origin
                if origin in ORIGIN_KIND_PROFILED:
                    kind = ORIGIN_KIND_PROFILED[origin]
                else:
                    logger.warning(
                        'Origin: {0} not in ORIGIN_KIND_PROFILED: {1}. '
                        'Setting KIND_REAL ({2}) as default'.format(
                            origin, ORIGIN_KIND_PROFILED.keys(), KIND_REAL
                        )
                    )
                    kind = KIND_REAL
                vals['kind_fact'] = kind
                vals['name'] = meter
                original = invalid.pop(measure.date, None)
                if original is None:
                    vals['cch_bruta'] = False
                    self.create(cursor, uid, vals)
                else:
                    # Is not a real gap, it was a invalid measure
                    vals['ae_fact'] = 0
                    vals['r1_fact'] = 0
                    vals['r2_fact'] = 0
                    vals['r3_fact'] = 0
                    vals['r4_fact'] = 0
                    self.write(cursor, uid, [original.meta['id']], vals)
            else:
                period = t.get_period_by_date(measure.date).code
                # Is adjusted if period is marked as adjusted
                if period in adjusted_periods:
                    logger.info('Meter {0} adjusting {1} with {2}'.format(
                        meter, measure.date, measure.measure
                    ))
                    vals['kind_fact'] = KIND_ADJUSTED
                else:
                    vals['kind_fact'] = KIND_REAL
                self.write(cursor, uid, [measure.meta['id']], vals)

        return True

    _defaults = {
        'valid': lambda *a: False,
        'valid_date': lambda *a: False,
        'cch_bruta': lambda *a: True,
        'cch_fact': lambda *a: False,
    }

    def _cron_upload_p5d(self, cursor, uid, ids, context=None):
        """
        Generate and upload CCH VAL (P5D)

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: passed to cronjob_generate_cch_val_curves (not used)
        :param context: dictionary with the context
        :return: None
        """

        if context is None:
            context = {}

        giscedata_polissa_modcontractual = \
            self.pool.get('giscedata.polissa.modcontractual')

        context.update({'set_last_cch': True})

        giscedata_polissa_modcontractual.cronjob_generate_cch_val_curves(
            cursor,
            uid,
            ids,
            context=context,
            upload=True
        )

TgProfile()


class TgReaderError(osv_mongodb.osv_mongodb):

    _name = 'tg.reader.error'
    _order = 'create_date desc'

    _columns = {
        'name': fields.char('Meter', size=50, required=True),
        'cnc_name': fields.char('CnC Name', size=20),
        'categ_code': fields.integer('Cat. Code'),
        'categ_desc': fields.char('Description', size=200),
        'error_code': fields.integer('Err. Code'),
        'error_desc': fields.char('Description', size=200),
        'create_date': fields.datetime('Date'),
    }


TgReaderError()


class TgReader(osv.osv):
    '''Implements reader'''

    _name = 'tg.reader'
    _auto = False

    def insert_values_S02(self, cursor, uid, meter, version, context=None):
        '''insert S02 values for meter or register error code'''

        profile_obj = self.pool.get('tg.profile')

        values = meter.values
        for value in values:
            profile_obj.create(cursor, uid, value)

        return True

    def insert_values_billing(self, cursor, uid, meter, tipus,
                              version, context=None):
        '''insert billing values for meter or register error code'''
        if context is None:
            context = {}

        billing_obj = self.pool.get('tg.billing')
        contract_1_only = context.get('contract_1_only', False)

        values = meter.values
        for value in values:
            if (tipus in ['S05', 'S04']
                    and contract_1_only and value['contract'] != 1):
                continue
            billing_obj.create(cursor, uid, value)

        return True

    def insert_values_S04(self, cursor, uid, meter,
                          version, context=None):
        self.insert_values_billing(cursor, uid, meter, 'S04',
                                   version, context=context)

    def insert_values_S05(self, cursor, uid, meter,
                          version, context=None):
        self.insert_values_billing(cursor, uid, meter, 'S05',
                                   version, context=context)

    def insert_values_S27(self, cursor, uid, meter,
                          version, context=None):
        self.insert_values_billing(cursor, uid, meter, 'S27',
                                   version, context=context)

    def insert_events(self, cursor, uid, meter, version,
                      report, context=None):
        '''insert events reports'''

        event_obj = self.pool.get('tg.event')
        event_val_obj = self.pool.get('tg.event.validate')
        events_spec = {}

        if report in ('S09', 'S13'):
            events_dict = meter_events
            event_type = 'M'
        else:
            events_dict = dc_events
            event_type = 'C'
        context['events_type'] = [event_type]
        context['events_spec'] = events_spec
        values = meter.values
        meter_name = values[0]['name']
        for value in values:
            # Update type of event depending on report
            value.update({'type': event_type,
                          'active': True})
            group = value['event_group']
            code = value['event_code']
            if events_spec.get(group, False):
                if not events_spec[group].get(code, False):
                    events_spec[group][code] = True
            else:
                events_spec.update({group: {code: True}})
            if group in events_dict and code in events_dict[group]:
                event_desc = events_dict[group][code]
                value.update({'event_code_desc': event_desc})
            else:
                value.update({'event_code_desc': ''})
            event_obj.create(cursor, uid, value)
        event_val_obj.deactivate_old_events(cursor, uid, meter_name,
                                            context=context)

        return True

    def insert_values_S09(self, cursor, uid, meter,
                          version, context=None):
        '''insert S09 values (meter events)'''

        return self.insert_events(cursor, uid, meter, version,
                                  'S09', context)

    def insert_values_S13(self, cursor, uid, meter,
                          version, context=None):
        '''insert S13 values (spontaneous meter events)'''

        return self.insert_events(cursor, uid, meter, version,
                                  'S13', context)

    def insert_values_S17(self, cursor, uid, meter,
                          version, context=None):
        '''insert S17 values (concentrator events)'''

        return self.insert_events(cursor, uid, meter, version,
                                  'S17', context)

    def insert_values_S15(self, cursor, uid, meter,
                          version, context=None):
        '''insert S15 values (spontaneous concentrator events)'''

        return self.insert_events(cursor, uid, meter, version,
                                  'S15', context)

    def insert_values_S12(self, cursor, uid, cnc,
                          version, context=None):
        '''S12: Concentrator values and config'''
        cnc_obj = self.pool.get('tg.concentrator')
        task_obj = self.pool.get('tg.concentrator.task')
        task_data_obj = self.pool.get('tg.concentrator.task.data')
        values = cnc.values
        for value in values:
            cnc_id = cnc_obj.get_cnc(cursor, uid, cnc.name,
                                     context=context)
            vals = value.copy()
            del vals['tasks']
            if not vals['r_password']:
                del vals['r_password']
            if not vals['w_password']:
                del vals['w_password']
            cnc_obj.write(cursor, uid, cnc_id, vals)
            # Delete all cnc task prior to insert
            search_params = [('cnc_id', '=', cnc_id)]
            task_ids = task_obj.search(cursor, uid, search_params)
            task_obj.unlink(cursor, uid, task_ids)
            # Insert new tasks
            for task in value['tasks']:
                task_val = task.copy()
                del task_val['task_data']
                task_val.update({'cnc_id': cnc_id})
                if task_val.get('periodicity', False):
                    periodicity = task_val.get('periodicity', False)
                    tr_periodicity = task_obj.translate_time(cursor, uid,
                                                             periodicity)
                    task_val.update({'periodicity': tr_periodicity})
                task_id = task_obj.create(cursor, uid, task_val)
                for task_data in task['task_data']:
                    data_val = task_data.copy()
                    data_val.update({'task_id': task_id})
                    task_data_obj.create(cursor, uid, data_val)
        return True

    def insert_values_S23(self, cursor, uid, meter, id, context=None):
        '''insert S23 values tg meter data'''
        registrator_obj = self.pool.get('giscedata.registrador')
        if meter.values[0].get('pc_act') != 'supervisor':
            data = {
                'pc_act': meter.values[0].get('pc_act').get('act_date'),
                'pc_act_tr1': meter.values[0].get('pc_act').get('contrato1').get('tr1'),
                'pc_act_tr2': meter.values[0].get('pc_act').get('contrato1').get('tr2'),
                'pc_act_tr3': meter.values[0].get('pc_act').get('contrato1').get('tr3'),
                'pc_act_tr4': meter.values[0].get('pc_act').get('contrato1').get('tr4'),
                'pc_act_tr5': meter.values[0].get('pc_act').get('contrato1').get('tr5'),
                'pc_act_tr6': meter.values[0].get('pc_act').get('contrato1').get('tr6'),
                'pc_latent': meter.values[0].get('pc_latent').get('act_date'),
                'pc_latent_tr1': meter.values[0].get('pc_latent').get('contrato1').get('tr1'),
                'pc_latent_tr2': meter.values[0].get('pc_latent').get('contrato1').get('tr2'),
                'pc_latent_tr3': meter.values[0].get('pc_latent').get('contrato1').get('tr3'),
                'pc_latent_tr4': meter.values[0].get('pc_latent').get('contrato1').get('tr4'),
                'pc_latent_tr5': meter.values[0].get('pc_latent').get('contrato1').get('tr5'),
                'pc_latent_tr6': meter.values[0].get('pc_latent').get('contrato1').get('tr6'),
            }
        else:
            data = {
                'type': 'supervisor'
            }
        registrator_obj.write(cursor, uid, context.get('meter_id'), data, context=context)

        return True

    def insert_values_S24(self, cursor, uid, cnc, version, context=None):
        '''insert S24 values tg meter availability data'''
        tg_avail_obj = self.pool.get('tg.meter.availability')
        cnc_obj = self.pool.get('tg.concentrator')
        registrator_obj = self.pool.get('giscedata.registrador')

        data = cnc.values[0]
        cnc_name = data['cnc_name']
        cnc_id = cnc_obj.get_cnc(cursor, uid, cnc_name)
        for meter in data['meters']:
            tg_name = meter['name']
            meter['timestamp'] = data['timestamp']
            meter['cnc_name'] = cnc_name
            meter['managed'] = meter.pop('active')
            meter['status'] = meter.pop('status')
            tg_avail_obj.create(cursor, uid, meter)
            meter_id = registrator_obj.register_tg(cursor, uid, cnc_id, tg_name)
            data = {
                'timestamp': meter['timestamp'],
                'managed': meter['managed'],
                'status': meter['status']
            }
            registrator_obj.write(cursor, uid, meter_id, data, context=context)

        return True

    def insert_primestg(self, cursor, uid, tg_xml, context=None):
        '''insert function for values from a primestg lib xml'''
        if not context:
            context = {}

        logger = netsvc.Logger()
        conf_obj = self.pool.get('res.config')
        cnc_obj = self.pool.get('tg.concentrator')
        registrator_obj = self.pool.get('giscedata.registrador')

        contract_1_only = (conf_obj.get(
            cursor, uid, 'tg_contract_1_only', '0'
        ) == '1')

        warnings = []

        for cnc in tg_xml.concentrators:
            # Autoregister new concentrators
            cnc_id = cnc_obj.get_cnc(cursor, uid, cnc.name,
                                     context=context)
            if tg_xml.report_type in CNC_REPORTS:
                getattr(self, 'insert_values_%s' % tg_xml.report_type)(
                    cursor, uid, cnc, tg_xml.report_version,
                    context=context
                )
                if cnc.warnings:
                    raise ValueError(cnc.name + '\n' + '\n'.join(cnc.warnings))
            else:
                if cnc.meters:
                    for meter in cnc.meters:
                        # creates registrator
                        meter_id = registrator_obj.register_tg(
                            cursor, uid, cnc_id, meter.name, context=context
                        )
                        if meter.errors:
                            if context.get('meter_errors', True):
                                self.create_file_reader_errors(
                                    cursor, uid, cnc.name, meter,
                                    context=context)
                            continue
                        context.update({'meter_id': meter_id})
                        context.update({'contract_1_only': contract_1_only})
                        getattr(self, 'insert_values_%s' % tg_xml.report_type)(
                            cursor, uid, meter, tg_xml.report_version,
                            context=context
                        )
                        try:
                            warnings.append(meter.warnings)
                        except AttributeError as e:
                            logger.notifyChannel("TG Reader", netsvc.LOG_WARNING,
                                                 "Report %s has no warnings "
                                                 "yet." % tg_xml.report_type)
            if warnings:
                w_meter = [(list(w.keys()), list(w.values())) for w in warnings]
                msg = ''
                for meter in w_meter:
                    if meter[0]:
                        msg = msg + '\n' + meter[0][0] + ':\n'
                        for warning in meter[1][0]:
                            msg = msg + warning + '\n'
                if msg:
                    raise ValueError(msg)

        return context.get('srr_id', False)

    def create_file_reader_errors(self, cursor, uid, cnc_name, meter,
                                  context=None):

        error_obj = self.pool.get('tg.error')
        reader_error_obj = self.pool.get('tg.reader.error')

        # Create case for this meter
        res = error_obj.get_error(cursor, uid,
                                  meter.errors['errcat'],
                                  meter.errors['errcode'],
                                  context=context)
        if not res:
            # Error code not found so no error raised
            return
        # register reader error
        reader_error_vals = {'name': meter.name,
                             'cnc_name': cnc_name,
                             'categ_code': res['categ']['code'],
                             'categ_desc': res['categ']['desc'],
                             'error_code': res['error']['code'],
                             'error_desc': res['error']['desc']}
        return reader_error_obj.create(cursor, uid, reader_error_vals)

    def register_read(self, cursor, uid, filename='', state='error', notes=None,
                      content=None):
        '''register readed file'''
        reg_obj = self.pool.get('tg.reader.register')
        att_obj = self.pool.get('ir.attachment')
        conf_obj = self.pool.get('res.config')
        file_types_to_save = literal_eval(
            conf_obj.get(cursor, uid, 'save_tg_files', '[]')
        )
        logger = netsvc.Logger()
        db = pooler.get_db_only(cursor.dbname)
        tmp_cr = db.cursor()
        try:
            vals = {'name': filename,
                    'state': state,
                    'notes': notes}
            res_id = reg_obj.create(tmp_cr, uid, vals)

            if state in file_types_to_save:
                if content and res_id:
                    att_obj.create(cursor, uid, {
                        'name': filename,
                        'datas': base64.b64encode(content),
                        'datas_fname': filename,
                        'res_model': 'tg.reader.register',
                        'res_id': res_id,
                    })

            tmp_cr.commit()
        except Exception as exc:
            res_id = False
            logger.notifyChannel(
                "TG Reader", netsvc.LOG_INFO,
                "Error registering %s read..." % filename)
            tmp_cr.rollback()
        finally:
            tmp_cr.close()
        return res_id

    def reader(self, cursor, uid, ftp_ids=None, one_file=None, f_to_read=None,
               context=None):
        '''Connect to the ftp server and read all the files'''

        ftp = self.pool.get('tg.ftp')
        logger = netsvc.Logger()
        db = pooler.get_db_only(cursor.dbname)
        state = False
        only_file_read = False
        result = {}
        if ftp_ids is None:
            ftp_ids = ftp.search(cursor, uid, [])
        for ftp_id in ftp_ids:
            ftpcon = ftp.login(cursor, uid, ftp_id)
            dirs_to_read = ftp.get_read_dirs(cursor, uid, ftp_id)
            for fdir in dirs_to_read:
                files_to_read = []
                # Only one file
                if len(ftp_ids) == 1 and f_to_read:
                    files_to_read = f_to_read
                else:
                    file_list = ftp.get_files(cursor, uid, ftpcon, fdir)
                    if one_file and only_file_read:
                        ftp.close(cursor, uid, ftpcon)
                        return None
                    if one_file and one_file in file_list:
                        files_to_read = [one_file]
                        only_file_read = True
                    elif one_file:
                        continue
                    else:
                        files_to_read = file_list
                files_to_read = [fdir + ft for ft in files_to_read]
                for filename in files_to_read:
                    file_content = False
                    srr_id = False
                    try:
                        tmp_cr = db.cursor()
                        read_success = False
                        supported = False
                        error_file = False  # To detect BAD/EMPTY files
                        logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                             "Reading %s..." % filename)
                        file_content = ftp.read_file(cursor, uid, ftpcon, filename)
                        if is_gziped(file_content):
                            file_content = file_content.replace('\x0d\x0a',
                                                                '\x0a')

                        if not file_content:
                            error_file = True
                            raise osv.except_osv(_(u'File is empty.'), (_(
                                u"File empty. Will be moved to error folder")))
                        supported = True
                        tg_xml = primeReport(file_content)
                        srr_id = self.insert_primestg(tmp_cr, uid, tg_xml)
                        tmp_cr.commit()
                    except error_perm:
                        tmp_cr.rollback()
                    except ValueError as exc:
                        logger.notifyChannel("TG Reader",
                                             netsvc.LOG_ERROR, exc)
                        tmp_cr.rollback()
                        # Register error reading
                        state = 'partial_read'
                    except Exception as exc:
                        logger.notifyChannel("TG Reader",
                                             netsvc.LOG_ERROR, exc)
                        tmp_cr.rollback()
                        # Register error reading
                        state = 'error'
                    else:
                        # Everything correct, move the file
                        ftpcon = ftp.login(cursor, uid, ftp_id)
                        dest_path = '%s%s/' % (ftp.get_root_dir(cursor, uid,
                                                                ftp_id),
                                               filename.split('_')[-1][:6])
                        ftp.move_file(cursor, uid, ftpcon, filename,
                                      fdir, dest_path)
                        # Register correct read
                        state = 'readed'
                        read_success = True
                    finally:
                        if state:
                            reg_id = self.register_read(
                                cursor, uid, filename=filename, state=state,
                                content=file_content
                            )
                            if srr_id:
                                if result.get(srr_id[0], False):
                                    result[srr_id[0]].append(reg_id)
                                else:
                                    result[srr_id[0]] = [reg_id]
                        if (not read_success and supported) or error_file:
                            dest_path = '%s%s/' % (ftp.get_root_dir(cursor, uid,
                                                   ftp_id), 'error/')
                            ftp.move_file(cursor, uid, ftpcon, filename,
                                          fdir, dest_path)
                        tmp_cr.close()
                logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                     "Finished reading files from: %s" % fdir)
            ftp.close(cursor, uid, ftpcon)
        return result


TgReader()


class TgReaderRegister(osv.osv):
    '''implements a register for each read file'''
    _name = 'tg.reader.register'
    _order = 'create_date desc'

    def status_chart(self, cursor, uid, from_date=None, to_date=None):
        if from_date is None:
            from_date = datetime.now().strftime('%Y-%m-%d')
        if to_date is None:
            to_date = datetime.now().strftime('%Y-%m-%d')
        if to_date < from_date:
            raise osv.except_osv(
                _('User error'),
                _("To date can't be lower than from date")
            )
        results = self.q(cursor, uid).read([
            'create_date', 'state', 'name'
        ]).where([
            ('create_date', '>=', from_date),
            ('create_date', '<=', to_date)
        ])

        regs_df = pd.DataFrame(results)
        regs_df['report_type'] = regs_df.apply(
            lambda x: x['name'].split('_')[2], axis='columns')
        regs_df['create_date'] = regs_df['create_date'].apply(
            lambda x: x.split(' ')[-1].split(':')[0])
        regs_hour = regs_df.groupby(
            ['create_date', 'report_type']).size().reset_index()
        regs_hour.rename({0: 'num_files'}, axis='columns', inplace=True)
        ftp_files_chart = alt.Chart(regs_hour).mark_bar().encode(
            alt.Y(u'num_files', title='Número fitxers'),
            alt.X(u'create_date', title='Data hora'),
            color=alt.Color('report_type', title='Informe')
        )
        return ftp_files_chart.to_dict()

    _columns = {
        'name': fields.char('File name', size=100, readonly=True),
        'create_date': fields.datetime('Date readed', readonly=True, select=True),
        'state': fields.selection([('readed', 'Readed'),
                                   ('error', 'Error'),
                                   ('partial_read', 'Partially')],
                                  'State', readonly=True),
        'notes': fields.text('Notes'),
        'file_date': fields.datetime('Date file received', readonly=True),
        'request_ids': fields.many2many('tg.reader.register',
                                        'stg_requests_tg_reader_register_rel',
                                        'tg_reader_id', 'request_id',
                                        'Reader registers'),
    }

    _defaults = {
        'state': lambda *a: 'readed',
    }

TgReaderRegister()


class TgConcentratorConnection(osv.osv):

    _name = 'tg.concentrator.connection'

    _columns = {
        'name': fields.char('Description', size=200, required=True),
        'code': fields.char('Code', size=15, required=True),
    }

TgConcentratorConnection()


class TgConcentrator(osv.osv):
    '''implements the basic TG concentrator model'''
    _name = 'tg.concentrator'

    def name_get(self, cursor, uid, ids, context=None):

        res = []
        for cnc in self.read(cursor, uid, ids,
                             ['name', 'description'],
                             context=context):
            res.append((cnc['id'], '%s (%s)' % (cnc['name'],
                                                cnc['description'] or '')))
        return res

    @cache()
    def get_cnc(self, cursor, uid, name, context=None):
        '''autoregister concentrator name if do not exists'''
        search_params = [('name', '=', name)]
        ids = self.search(cursor, uid, search_params, context=context)
        if not ids:
            cnc_id = self.create(cursor, uid, {'name': name})
        else:
            cnc_id = ids[0]
        return cnc_id

    def _ip_address_valid(self, cursor, uid, ids):
        '''validates an ipv4 address'''
        for cnc in self.browse(cursor, uid, ids):
            if not cnc.ip_address:
                continue
            try:
                socket.inet_aton(cnc.ip_address)
            except socket.error:
                return False
        return True

    def _get_connection(self, cr, uid, context=None):
        obj = self.pool.get('tg.concentrator.connection')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['code', 'name'], context)
        res = [(r['code'], r['name']) for r in res]
        return res

    def _get_address(self, cr, uid, ids, field_name, args, context=None):
        if not context:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        res = {}
        for dc_vals in self.read(cr, uid, ids, ['public_ip', 'dc_ws_port',
                                 'dc_web_port', 'dc_web_protocol', 'dc_ws_path_cnc'], context):
            values = {
                'dc_ws_address': False,
                'dc_web_address': False
            }
            if dc_vals['public_ip']:
                if dc_vals['dc_web_port'] and dc_vals['dc_web_protocol']:
                    values['dc_web_address'] = '{dc_web_protocol}://' \
                                               '{public_ip}:{dc_web_port}' \
                                               ''.format(**dc_vals)
                else:
                    values['dc_web_address'] = '{dc_web_protocol}://' \
                                               '{public_ip}'.format(**dc_vals)
                if dc_vals['dc_ws_port']:
                    values['dc_ws_address'] = '{dc_web_protocol}://' \
                                              '{public_ip}:{dc_ws_port}' \
                                              ''.format(**dc_vals)
                    if dc_vals['dc_ws_path_cnc']:
                        values['dc_ws_address'] = '{dc_web_protocol}://' \
                                                  '{public_ip}:{dc_ws_port}/{dc_ws_path_cnc}' \
                                                  ''.format(**dc_vals)
            res[dc_vals['id']] = values
        return res

    def read_tg_pending_files_for_cnc(self, cursor, uid, cnc_id, report=None,
                                      context=None):
        if not report:
            report = ['S02', 'S04', 'S05']
        ftp = self.pool.get('tg.ftp')
        reader_obj = self.pool.get('tg.reader')

        logger = netsvc.Logger()

        sub = self.read(cursor, uid, cnc_id, ['name'])
        reports = '|'.join(e for e in report)
        r = re.compile("{}.*({}).*".format(sub['name'], reports))

        ftp_ids = ftp.search(cursor, uid, [])
        for ftp_id in ftp_ids:
            ftp_con = ftp.login(cursor, uid, ftp_id)
            dirs_to_read = ftp.get_read_dirs(cursor, uid, ftp_id)
            for f_dir in dirs_to_read:
                file_list = ftp.get_files(cursor, uid, ftp_con, f_dir)
                to_read = list(filter(r.match, file_list))
                if len(to_read) == 1:
                    logger.notifyChannel("TG ReadsRetry", netsvc.LOG_INFO,
                                         "There is a file pending to read {}"
                                         "".format(to_read[0]))
                    reader_obj.reader(cursor, uid, ftp_ids=[ftp_id],
                                      one_file=to_read[0], context=context)
                elif len(to_read) > 1:
                    logger.notifyChannel("TG ReadsRetry", netsvc.LOG_INFO,
                                         "There are {} files pending to read"
                                         "".format(len(to_read)))
                    reader_obj.reader(cursor, uid, ftp_ids=[ftp_id],
                                      f_to_read=to_read, context=context)
                else:
                    logger.notifyChannel("TG ReadsRetry", netsvc.LOG_INFO,
                                         "No files pending to read for pattern:"
                                         " {}".format(r.pattern))
        return True

    _columns = {
        'name': fields.char('Name', size=20, required=True),
        'description': fields.char('Description', size=200),
        'active': fields.boolean('Active'),
        'ip_address': fields.char('IP address', size=20),
        'dc_web_address': fields.function(_get_address, string='WEB',
                                          method=True, type='char', size=50,
                                          multi='address'),
        'dc_web_port': fields.integer('WEB port'),
        'dc_web_protocol': fields.selection([('http', 'HTTP'),
                                            ('https', 'HTTPS')],
                                            'Web protocol'),
        'date': fields.datetime('Retrieval date'),
        'model': fields.char('Model', size=50),
        'mf_year': fields.char('Manufacturing year', size=4),
        'type': fields.char('Type of equipment', size=20),
        'w_password': fields.char('Admin password', size=20),
        'r_password': fields.char('Guest password', size=20),
        'fw_version': fields.char('Firmware', size=20),
        'fw_comm_version': fields.char('Firmware comm.', size=20),
        'protocol': fields.char('Protocol', size=20),
        'communication': fields.char('Comm. protocol', size=20),
        'battery_mon': fields.char('Battery monitor', size=10),
        'dc_ws_address': fields.function(_get_address, string='DC address',
                                         method=True, type='char', size=50,
                                         multi='address'),
        'dc_ws_port': fields.integer('DC port'),
        'dc_ws_path_cnc': fields.char('Ruta WS CNC', size=50),
        'ip_mask': fields.char('Mask', size=15),
        'ip_gtw': fields.char('Gateway', size=15),
        'dhcp': fields.boolean('DHCP'),
        'slave1': fields.char('Slave 1', size=10),
        'slave2': fields.char('Slave 2', size=10),
        'slave3': fields.char('Slave 3', size=10),
        'local_ip_address': fields.char('Local IP', size=15),
        'local_ip_mask': fields.char('Local Mask', size=15),
        'plc_mac': fields.char('PLC MAC', size=17),
        'serial_port_speed': fields.char('Serial port speed', size=15),
        'priority': fields.boolean('Priority'),
        'stg_ws_ip_address': fields.char('STG address', size=50),
        'stg_ws_port': fields.integer('STG port'),
        'stg_ws_password': fields.char('STG Password', size=20),
        'ntp_ip_address': fields.char('NTP IP', size=15),
        'rpt_ftp_ip_address': fields.char('FTP IP', size=64),
        'rpt_ftp_user': fields.char('FTP User', size=64),
        'rpt_ftp_password': fields.char('FTP Password', size=64),
        'fwdcup_ftp_ip_address': fields.char('FTP IP', size=64),
        'fwdcup_ftp_user': fields.char('FTP User', size=64),
        'fwdcup_ftp_password': fields.char('FTP Password', size=64),
        'fwmtup_ftp_ip_address': fields.char('FTP IP', size=64),
        'fwmtup_ftp_user': fields.char('FTP User', size=64),
        'fwmtup_ftp_password': fields.char('FTP Password', size=64),
        'retries': fields.integer('Retries'),
        'time_btw_retries': fields.integer('Time between retries'),
        'cycle_ftp_ip_address': fields.char('FTP IP', size=64),
        'cycle_ftp_user': fields.char('FTP User', size=64),
        'cycle_ftp_password': fields.char('FTP Password', size=64),
        'cycle_ftp_dir': fields.char('FTP Dir', size=64),
        'sync_meter': fields.boolean('Sync meters'),
        'fwmtup_timeout': fields.integer('FW Upgrade timeout'),
        'max_time_deviation': fields.integer('Max deviation'),
        'min_time_deviation': fields.integer('Min deviation'),
        'reset_msg': fields.boolean('Reset message'),
        'rpt_meter_limit': fields.integer('Rpt meter limit'),
        'rpt_time_limit': fields.integer('Rpt time limit'),
        'disconn_time': fields.integer('Disconnect time'),
        'disconn_retries': fields.integer('Retries'),
        'disconn_retry_interval': fields.integer('Time between retries'),
        'meter_reg_data': fields.char('Reg data', size=100),
        'report_format': fields.char('Report format', size=5),
        's26_content': fields.char('S26 content', size=200),
        'values_check_delay': fields.integer('Check delay'),
        'max_order_outdate': fields.integer('Max order outdate'),
        'restart_delay': fields.integer('Restart delay'),
        'ntp_max_deviation': fields.integer('NTP Max deviation'),
        'session_timeout': fields.integer('Session timeout'),
        'max_sessions': fields.integer('Max sessions'),
        'task_ids': fields.one2many('tg.concentrator.task', 'cnc_id',
                                    'Concentrator'),
        'connection': fields.selection(_get_connection, 'Connection'),
        'supervision': fields.boolean('Supervision'),
        'operator': fields.char('Operator', size=100),
        'telephone': fields.char('Telephone', size=20),
        'operator_ip_address': fields.char('Reg data', size=20),
        'apn': fields.char('APN', size=100),
        'apn_user': fields.char('APN user', size=100),
        'apn_password': fields.char('APN password', size=100),
        'modem_user': fields.char('Modem user', size=100),
        'modem_password': fields.char('Modem password', size=100),
        'public_ip': fields.char('Public IP', size=20),
        'total_meters_active': fields.integer('Active meters'),
        'total_meters_temporary_failure': fields.integer('Temporary failure meters'),
        'total_meters_permanent_failure': fields.integer('Permanent failure meters'),
        'status': fields.selection([('up', 'UP'), ('down', 'DOWN')], 'Status'),
        'last_heartbeat': fields.datetime('Last heartbeat'),

        'registrador_ids': fields.one2many('giscedata.registrador',
                                           'cnc_id', 'Registradors'),
    }

    _defaults = {
        'supervision': lambda *a: False,
        'dc_web_protocol': lambda *a: 'http',
        'active': lambda *a: True,
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         _('This concentrator name already exists'))]

    _constraints = [(_ip_address_valid,
                     _('Ip address not valid'),
                     ['ip_address'])]

TgConcentrator()


class TgConcentratorTask(osv.osv):

    _name = 'tg.concentrator.task'

    _time_positions = [
        {'short_name': 'Y', 'position': 3, 'name': 'Year'},
        {'short_name': 'M', 'position': 5, 'name': 'Month'},
        {'short_name': 'd', 'position': 7, 'name': 'Day'},
        {'short_name': 'h', 'position': 9, 'name': 'Hour'},
        {'short_name': 'm', 'position': 11, 'name': 'Minute'},
        {'short_name': 's', 'position': 13, 'name': 'Second'}
        ]

    def translate_time(self, cursor, uid, time, context=None):
        '''Translate relative PRIME time format in a human readable string
        @time: string with relative time
        Returns: human readable time string'''
        res = ''
        sorted_list = sorted(self._time_positions,
                             key=lambda item: item['position'])
        for item in sorted_list:
            if time[item['position']] != '0':
                res += '%s%s' % (time[item['position']],
                                 item['short_name'])
        return res

    def _get_requests(self, cr, uid, ids, prop, unknow_none, context=None):
        res = {}
        for task in self.browse(cr, uid, ids, context=context):
            res[task.id] = ' '.join(d['request'] for d in task.data_ids)
        return res

    _columns = {
        'cnc_id': fields.many2one('tg.concentrator',
                                  'Concentrator',
                                  required=True),
        'name': fields.char('Name', size=20),
        'priority': fields.integer('Priority'),
        'date_from': fields.datetime('From'),
        'periodicity': fields.char('Periodicity', size=20),
        'complete': fields.boolean('Completion'),
        'meters': fields.char('Meters', size=200),
        'data_ids': fields.one2many('tg.concentrator.task.data',
                                    'task_id', 'Data'),
        'requests': fields.function(_get_requests, method=True, string='Requests', type='char'),
    }

TgConcentratorTask()


class TgConcentratorTaskData(osv.osv):

    _name = 'tg.concentrator.task.data'
    _rec_name = 'request'

    _columns = {
        'task_id': fields.many2one('tg.concentrator.task',
                                   'Task', required=True,
                                   ondelete='cascade'),
        'request': fields.char('Request', size=3),
        'stg_send': fields.boolean('Send STG'),
        'store': fields.boolean('Store'),
        'attributes': fields.char('Attribute', size=10),
    }

TgConcentratorTaskData()


class TgErrorCategory(osv.osv):

    _name = 'tg.error.category'

    def name_get(self, cursor, uid, ids, context=None):

        res = []
        for categ in self.browse(cursor, uid, ids, context=context):
            res.append((categ.id, '(%s) %s' % (categ.code, categ.name)))
        return res

    @cache()
    def get_category(self, cursor, uid, errcat, context=None):
        '''returns a dict with id: description of categories with
        code in errcat'''
        search_params = [('code', '=', errcat)]
        res = self.search_reader(cursor, uid, search_params,
                                 ['name', 'id'],
                                 context=context)
        if res:
            return res[0]
        return False

    _columns = {
        'name': fields.char('Description', size=200, required=True),
        'code': fields.integer('Code', required=True),
    }

TgErrorCategory()


class TgError(osv.osv):

    _name = 'tg.error'

    @cache()
    def get_error(self, cursor, uid, errcat, errcode, context=None):
        '''returns a string with error category and error desc'''
        categ_obj = self.pool.get('tg.error.category')

        categ = categ_obj.get_category(cursor, uid, errcat,
                                       context=context)
        if not categ:
            return False
        search_params = [('code', '=', errcode),
                         ('category_id', '=', categ['id'])]

        error = self.search_reader(cursor, uid, search_params,
                                   ['name', 'id'],
                                   context=context)
        if not error:
            return False
        else:
            error = error[0]
        res = {'categ': {'code': int(errcat),
                         'desc': categ['name']},
               'error': {'code': int(errcode),
                         'desc': error['name']}}
        return res

    _columns = {
        'name': fields.char('Description', size=200, required=True),
        'code': fields.integer('Code', required=True),
        'category_id': fields.many2one('tg.error.category', 'Category',
                                       required=True),
    }

TgError()


class TgEvent(osv_mongodb.osv_mongodb):

    _name = 'tg.event'
    _order = 'timestamp desc'

    _columns = {
        'name': fields.char('Meter', size=50, required=True, select=True,
                            readonly=True),
        'timestamp': fields.datetime('Timestamp', readonly=True),
        'season': fields.selection([('W', 'Winter'),
                                    ('S', 'Summer')], 'Season', size=1,
                                   readonly=True),
        'cnc_name': fields.char('CnC Name', size=20, readonly=True),
        'event_group': fields.integer('Event group', required=True,
                                      readonly=True),
        'event_code': fields.integer('Event code', required=True,
                                     readonly=True),
        'event_code_desc': fields.char('Code desc', size=200,
                                       readonly=True),
        'data': fields.text('Event data', readonly=True),
        'type': fields.selection([('C', 'Concentrator'),
                                  ('M', 'Meter')], 'Event type', size=1,
                                 readonly=True),
        'active': fields.boolean('Active'),
    }

TgEvent()

class TgEventValidate(osv.osv):

    _name = 'tg.event.validate'
    _auto = False

    def get_duplicates(self, cursor, uid, meter_name):
        """
        Get all duplicated events of the meter with name = meter_name
        :param cursor: Database cursor
        :param uid: User ID
        :param meter_name: The name of the target meter
        :return: List of dictionaries with all fields on return_keys for all
        the duplicated ids
        """

        tg_event_obj = self.pool.get('tg.event')

        read_keys = ['name', 'timestamp', 'event_group', 'event_code', 'type']
        keywords = ['name', 'timestamp', 'event_group', 'event_code', 'type']
        return_keys = ['id']
        events = tg_event_obj.search(cursor, uid, [
            ('name', '=', meter_name)])
        read_events = tg_event_obj.read(cursor, uid, events, read_keys)

        df = pd.DataFrame(read_events)
        df.sort_values(read_keys, ascending=False, inplace=True)
        df_duplicated = df.duplicated(subset=keywords)

        return df[df_duplicated.values][return_keys].T.to_dict().values()

    def clean_duplicates(self, cursor, uid, meter_names, context=None):
        """ Check all the events from the meters in meter_name deleting
        " duplicates if there is a duplicate that has been validated is
        " the one that remains
        """
        if not meter_names:
            meter_names = get_all_meter_names('tg_event')
        if not isinstance(meter_names, (tuple, list)):
            meter_names = [meter_names]

        tg_event_obj = mdbpool.get_collection('tg_event')
        for meter_name in meter_names:
            meter_dups = self.get_duplicates(cursor, uid, meter_name)
            if meter_dups:
                dups_ids = [x['id'] for x in meter_dups]
                tg_event_obj.remove({'id': {'$in': dups_ids}})

    def deactivate_old_events(self, cursor, uid, meter_name, context=None):
        """
        Check for the active events of the same type for the meter and
        deactivates them all but the last.
        :param meter_name: Name of the meter to validate
        :return: True
        """
        types = ['C', 'M']
        if context.get('events_type', False):
            types = context['events_type']
        if 'M' in types:
            self.deactivate_old_meter_events(cursor, uid, meter_name,
                                             context=context)
        if 'C' in types:
            self.deactivate_old_dc_events(cursor, uid, meter_name,
                                          context=context)

    def deactivate_old_meter_events(self, cursor, uid, meter_name, context=None):
        tg_event_obj = self.pool.get('tg.event')
        types = meter_events
        if context.get('events_spec', False):
            types = context['events_spec']
        for group in types:
            for code in types[group]:
                search_params = {
                    ('name', '=', meter_name),
                    ('event_group', '=', group),
                    ('event_code', '=', code),
                    ('type', '=', 'M'),
                    ('active', '=', True),
                }
                event_ids = tg_event_obj.search(cursor, uid, search_params,
                                                order='timestamp desc')[1:]
                tg_event_obj.write(cursor, uid, event_ids, {'active': False})

    def deactivate_old_dc_events(self, cursor, uid, dc_name, context=None):
        tg_event_obj = self.pool.get('tg.event')
        types = meter_events
        if context.get('events_spec', False):
            types = context['events_spec']
        for group in types:
            for code in types[group]:
                search_params = {
                    ('name', '=', dc_name),
                    ('event_group', '=', group),
                    ('event_code', '=', code),
                    ('type', '=', 'C'),
                    ('active', '=', True),
                }
                event_ids = tg_event_obj.search(cursor, uid, search_params,
                                                order='timestamp desc')[1:]
                tg_event_obj.write(cursor, uid, event_ids, {'active': False})

    @job(queue='tg_validate_event', timeout=1200, result_ttl=24 * 3600)
    def validate_individual(self, cursor, uid, ids, meter_name, context=None):
        '''This function has to validate the events
        information that comes from smart meters
        '''

        if not context:
            context = {}

        self.clean_duplicates(cursor, uid, meter_name, context=context)

    def validate_event(self, cursor, uid, ids, meter_names=[], context=None):
        '''This function has to validate the event
        information that comes from smart meters
        '''

        if not meter_names:
            meter_names = get_all_meter_names('tg_event')
        if not isinstance(meter_names, (tuple, list)):
            meter_names = [meter_names]

        jobs_ids = []
        for meter_name in meter_names:
            j = self.validate_individual(cursor, uid, ids, meter_name,
                                         context=context)
            jobs_ids.append(j.id)

        if len(jobs_ids) > 1:
            create_jobs_group(
                cursor.dbname, uid, _('Validate event'),
                'tg.validate.event', jobs_ids
            )
        return True

TgEventValidate()

class TgMeterAvailability(osv_mongodb.osv_mongodb):

    _name = 'tg.meter.availability'
    _order = 'timestamp desc'

    def obtain_meter_availability_data(self, cursor, uid, meter_name, date_from,
                                       date_to, min_value=None, last_hour=24,
                                       context=None):
        if not isinstance(date_from, datetime) and not isinstance(date_to, datetime):
            try:
                date_from = datetime.strptime(date_from, '%Y-%m-%d %H:%M:%S')
                date_to = datetime.strptime(date_to, '%Y-%m-%d %H:%M:%S')
            except:
                raise ValueError('Dates must be datetime instances when calling'
                                 ' this function')
        tg_meter_avail = mdbpool.get_collection('tg_meter_availability')
        aggr = tg_meter_avail.aggregate(
            [
                {"$match": {
                            "name": meter_name,
                            "managed": True,
                            "timestamp": {
                                "$gte": date_from,
                                "$lte": date_to
                            },
                            }},
                {"$group":
                     {
                         "_id":
                             {
                                 "hour": {"$hour": "$timestamp"},
                                 "dayOfWeek": {"$dayOfWeek": "$timestamp"},
                             },
                         "status": {"$push": "$status"},
                         "day": {"$first": {"$dayOfWeek": "$timestamp"}},
                         "hour": {"$first": {"$hour": "$timestamp"}},
                     }
                }
            ]
        )["result"]
        data_amount = 0
        not_good = []
        for hour in aggr:
            positive = 0
            for stat in hour['status']:
                if stat == 2:
                    positive += 1
                data_amount += 1
            heat_value = 100 * positive / len(hour['status'])
            hour['value'] = heat_value
            if last_hour == 24:
                hour['hour'] += 1
            hour.pop('_id', None)
            hour.pop('status', None)
            if min_value and heat_value < min_value:
                not_good.append(hour)
        for bad_hour in not_good:
            aggr.remove(bad_hour)
        return aggr, data_amount

    def obtain_meter_availability_for_weekday(self, cursor, uid, meter_name,
                                              r_date, min_value=None,
                                              last_hour=24, context=None):
        if not isinstance(r_date, (datetime, date)):
            try:
                r_date = datetime.strptime(r_date, '%Y-%m-%d')
            except:
                raise ValueError('Dates must be datetime or datetime.date '
                                 'instances when calling this function')
        tg_meter_avail = mdbpool.get_collection('tg_meter_availability')

        # MongoDB dayOfWeek: 1 (Sunday) and 7 (Saturday).
        # Datetime weekday: 0 (Monday) and 6 (Sunday).
        mongo_weekday = ((r_date.weekday() + 1) % 7) + 1

        aggr = tg_meter_avail.aggregate(
            [
                {"$match": {
                    "name": meter_name,
                    "managed": True,
                }},
                {"$project": {
                    "dow": {"$dayOfWeek": "$timestamp"},
                    "status": 1,
                    "hour": {"$hour": "$timestamp"}
                }},
                {"$match": {"dow": mongo_weekday}},
                {"$group":
                    {
                        "_id": "$hour",
                        "hour": {"$first": "$hour"},
                        "status": {"$push": "$status"}
                    }
                },
                {"$sort": {"hour": 1}}
            ]
        )["result"]
        not_good = []
        if aggr:
            max_stats = len(max(aggr, key=lambda x: len(x['status']))['status'])
            for hour in aggr:
                positive = 0
                for stat in hour['status']:
                    if stat == 2:
                        positive += 1
                heat_value = 100 * positive / max_stats
                hour['value'] = heat_value
                if last_hour == 24:
                    hour['hour'] += 1
                hour.pop('_id', None)
                hour.pop('status', None)
                if min_value and heat_value < min_value:
                    not_good.append(hour)
        for bad_hour in not_good:
            aggr.remove(bad_hour)
        return aggr

    @staticmethod
    def get_best_conn_hour_from_aggr(aggr, context=None):
        best_conn = 0
        best_hour = 0
        for hour in aggr:
            if hour['value'] > best_conn:
                best_conn = hour['value']
                best_hour = hour['hour']
        return best_hour

    @staticmethod
    def get_best_conn_hour_with_continuity_from_aggr(aggr, context=None):
        best_option = {
            'best_hour': -1,
            'best_conn': 0,
            'best_length': 0,
        }
        r = groupby(aggr, lambda n, c=count(): n['hour'] - next(c))
        for k, g in r:
            g = list(g)
            if len(g) == 1:
                if g[0]['value'] > best_option['best_conn'] and \
                                best_option['best_length'] <= 1:
                    best_option.update({
                        'best_hour': g[0]['hour'],
                        'best_conn': g[0]['value'],
                        'best_length': 1,
                    })
            elif len(g) == 2:
                if g[0]['value'] > g[1]['value']:
                    if g[0]['value'] >= best_option['best_conn'] and \
                                    best_option['best_length'] < 2:
                        best_option.update({
                            'best_hour': g[0]['hour'],
                            'best_conn': g[0]['value'],
                            'best_length': 2,
                        })
                else:
                    if g[1]['value'] >= best_option['best_conn'] and \
                                    best_option['best_length'] < 2:
                        best_option.update({
                            'best_hour': g[1]['hour'],
                            'best_conn': g[1]['value'],
                            'best_length': 2,
                        })
            else:
                mid = len(g)/2
                if g[mid]['value'] >= g[mid+1]['value']:
                    if g[mid]['value'] >= best_option['best_conn'] and \
                                    best_option['best_length'] < 3:
                        best_option.update({
                            'best_hour': g[mid]['hour'],
                            'best_conn': g[mid]['value'],
                            'best_length': len(g),
                        })
                else:
                    if g[mid]['value'] >= best_option['best_conn'] and \
                                    best_option['best_length'] < 3:
                        best_option.update({
                            'best_hour': g[mid+1]['hour'],
                            'best_conn': g[mid+1]['value'],
                            'best_length': len(g),
                        })
        return best_option['best_hour']

    _columns = {
        'name': fields.char('Meter', size=50, required=True, select=True,
                            readonly=True),
        'timestamp': fields.datetime('Timestamp', readonly=True),
        'season': fields.selection([('W', 'Winter'),
                                    ('S', 'Summer')], 'Season', size=1,
                                   readonly=True, required=True),
        'cnc_name': fields.char('CnC Name', size=20, readonly=True),
        'status': fields.selection([(0, 'Permanent Failure'),
                                    (1, 'Temporal Failure'),
                                    (2, 'Registered')], 'Communication status',
                                   size=1, readonly=True),
        'managed': fields.boolean('Managed', help='If enabled, this meter was '
                                  'managed by the referenced concentrator on '
                                  'that specific time.')
    }

TgMeterAvailability()
