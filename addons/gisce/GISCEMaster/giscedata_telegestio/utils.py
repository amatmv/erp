# -*- coding: utf-8 -*-

from collections import namedtuple
from datetime import datetime
try:
    import cPickle as pickle
except ImportError:
    import pickle

from dateutil import parser
from enerdata.profiles.profile import ProfileHour, Coefficent, REEProfile
from enerdata.datetime.timezone import TIMEZONE
from esios import Esios

from tools import config
from oorq.oorq import setup_redis_connection

ESIOS_TOKEN = config.get('esios_token', '')


def localize_esios_dt(dt):
    assert isinstance(dt, datetime)
    d = datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second)
    if dt.utcoffset().seconds == 7200:
        dst = True
    elif dt.utcoffset().seconds == 3600:
        dst = False
    else:
        raise Exception('Not a valid offset')
    return TIMEZONE.localize(d, is_dst=dst)


def localize_season(dt, season):
    if isinstance(dt, basestring):
        dt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
    dst = bool(season == 'S')
    return TIMEZONE.normalize(TIMEZONE.localize(dt, is_dst=dst))


def get_season(dt):
    if dt.dst().seconds > 0:
        return 'S'
    else:
        return 'W'


def get_hours_amount(date_from, date_to):
    season_ini = TIMEZONE.localize(date_from)
    season_end = TIMEZONE.localize(date_to)
    delta = season_end - season_ini
    return delta.total_seconds()/3600


def convert_to_profilehour(measure):
    ph = TGProfileHour(
        localize_season(measure['timestamp'], measure['season']),
        measure['ai'],
        measure['valid'],
        0.0,
        meta=measure
    )
    return ph


def convert_to_mongodb(measure):
    assert isinstance(measure, (ProfileHour, TGProfileHour))
    return dict(
        timestamp=TIMEZONE.normalize(measure.date).strftime('%Y-%m-%d %H:00:00'),
        ai_fact=measure.measure,
        valid=True,
        valid_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        cch_fact=True,
        magn=1,
        season=get_season(measure.date)
    )


def get_hexadecimal_control_bits_combination(array_bits):
    """
    Get Hexadecimal combination bits from decimal bits to rule out the tg
    validation
    :param array_bits: list LIKE [1, 3, 7]. bits to rule out validation
    :return: array hexadecimal bits LIKE ['01', '2c', '8a']
    """
    from itertools import combinations
    array = dict.fromkeys([0, 1, 2, 3, 4, 5, 6, 7], 0)
    result = []
    for L in range(0, len(array_bits)+1):
        for subset in combinations(array_bits, L):
            subarray = array.copy()
            value = ''
            for x in subset:
                subarray[x] = 1
            for y in sorted(subarray.keys(), reverse=True):
                value += str(subarray[y])
            assert len(value) == 8, 'Value does not match the number of bits'
            v = int(str(value), 2)
            hex_value = hex(v).split('x')[-1]
            if len(hex_value) < 2:
                hex_value = hex_value.zfill(2)
            if hex_value != '00':
                result.append(hex_value)

    return result


class TGProfileHour(namedtuple('TGProfileHour',
                               ProfileHour._fields + ('meta', ))):
    __slots__ = ()

    def __lt__(self, other):
        return self.date < other.date

    def __le__(self, other):
        return self.date <= other.date

    def __gt__(self, other):
        return self.date > other.date

    def __ge__(self, other):
        return self.date >= other.date


class EsiosProfile(object):

    @classmethod
    def get_range(cls, start, end):
        e = Esios(ESIOS_TOKEN)
        method = getattr(e, cls.method)
        redis_conn = setup_redis_connection()
        key = '{}-{}-{}'.format(cls.method, start, end)
        cofs = redis_conn.get(key)
        if cofs:
            cofs = pickle.loads(cofs)
        else:
            cofs = []
            profs = method().get(start, end)
            for prof in profs['indicator']['values']:
                dt = localize_esios_dt(parser.parse(prof['datetime']))
                cofs.append(Coefficent(
                    TIMEZONE.normalize(dt), dict(
                        (k, float(prof['value'])) for i, k in enumerate('ABCD', 5)
                    ))
                )
            redis_conn.set(key, pickle.dumps(cofs), 3600)
        return cofs


class EsiosProfile20A(EsiosProfile):
    method = 'profile_pvpc_20A'


class EsiosProfile20DHA(EsiosProfile):
    method = 'profile_pvpc_20DHA'


class EsiosProfile20DHS(EsiosProfile):
    method = 'profile_pvpc_20DHS'


ESIOS_PROFILE_MAP = {
    '2.0A': EsiosProfile20A,
    '2.1A': EsiosProfile20A,
    '2.0DHA': EsiosProfile20DHA,
    '2.1DHA': EsiosProfile20DHA,
    '2.0DHS': EsiosProfile20DHS,
    '2.1DHS': EsiosProfile20DHS,
    '3.0A': REEProfile
}

meter_events = {1: {
    1: 'Reboot with loss of data',
    2: 'Reboot without loss of data',
    3: 'Power Fail',
    4: 'Power failure. Phase 1',
    5: 'Power failure. Phase 2',
    6: 'Power failure. Phase 3',
    7: 'Neutral loss',
    8: 'Low Battery',
    9: 'Critical Internal Error ',

    21: 'End of power failure. Phase 1',
    22: 'End of power failure. Phase 2',
    23: 'End of power failure. Phase 3',
    24: 'Official change local time Winter->Summer',
    25: 'Official change local time Summer-> Winter',
    30: 'Register parameters change',
    31: 'Communication ports parameters change',
    32: 'Reading password change',
    33: 'Parametrization password change',
    34: 'Firmware password change',
    35: 'Battery cleared',
    36: 'Automatic daylight-saving time change',
    37: 'Minimum time between billing end change',
    38: 'Load profile capture period change',
    39: 'Transform ratio changed',
    40: 'Clock synchronization mode changed',
    41: 'Program label changed',
    42: 'Manual closures activation status changed',
    43: 'Output magnitude changed',
    44: 'Closure command prompted contract 1',
    45: 'Parameters contract 1 changed',

    47: 'Special days table contract 1 passive change',
    48: 'Seasons table contract 1 passive change',
    49: 'Contract 1 passive cleared',
    50: 'Automatic billing end contract 1 passive change',
    51: 'Activation date contract 1 passive change',

    52: 'Closure command prompted contract 2',
    53: 'Parameters contract 2 changed',
    54: 'Special days table contract 2 passive change',
    55: 'Seasons table contract 2 passive change',
    56: 'Contract 2 passive cleared',
    57: 'Automatic billing end contract 2 passive change',
    58: 'Activation date contract 2 passive change',

    59: 'Closure command prompted contract 3',
    60: 'Parameters contract 3 changed',
    61: 'Special days table contract 3 passive change',
    62: 'Seasons table contract 3 passive change',
    63: 'Contract 3 passive cleared',
    64: 'Automatic billing end contract 3 passive change',
    65: 'Activation date contract 3 passive change',

    66: 'Contract 1, 2 and 3. Manual billing end (button)',

    90: 'Time threshold for voltage sags and swells changed',
    91: 'Time threshold for long power failures (T\') changed',
    92: 'Nominal voltage (Vn) changed',
    93: 'Max voltage level changed (+V)',
    94: 'Min voltage level changed (-V)',
    95: 'Difference between min voltage and no voltage changed',

    96: 'Contract Power changed',
    97: 'Firmware changed',
    98: 'Clock synchronization',
    99: 'Passwords Reset. Passwords take the manufacturing values',
    100: 'Parameters & registers Reset. Parameters take manufacturing values and billing and load profiles values come '
         'to zero.'
}, 2: {
    1: 'Manual Power control connection from meter push bottom.',
    2: 'Remote disconnection (command)',
    3: 'Remote connection (command)',
    4: 'Power contract control disconnection',
    5: 'Manual Power control connection from the household main interrupter.',
    6: 'no trip Current exceeded by blockade',
    7: 'Disconnect enabled',
    8: 'Disconnect disabled',
    9: 'Residual power control disconnection',
    10: 'Residual power deactivation control connection',
    11: 'Residual power control connection'
}, 3: {
    1: 'Quality non finished Events. Under limit voltage between phases average',
    2: 'Quality non finished Events. Under limit voltage L1',
    3: 'Quality non finished Events. Under limit voltage L2',
    4: 'Quality non finished Events. Under limit voltage L3',
    5: 'Quality non finished Events. Over limit voltage between phases average',
    6: 'Quality non finished Events. Over limit voltage L1',
    7: 'Quality non finished Events. Over limit voltage L2',
    8: 'Quality non finished Events. Over limit voltage L3',
    9: 'Quality non finished Events. Long power failure for all phases',
    10: 'Quality non finished Events. Long power failure L1',
    11: 'Quality non finished Events. Long power failure L2',
    12: 'Quality non finished Events. Long power failure L3',

    13: 'Quality finished Events. Under limit voltage between phases average',
    14: 'Quality finished Events. Under limit voltage L1',
    15: 'Quality finished Events. Under limit voltage L2',
    16: 'Quality finished Events. Under limit voltage L3',
    17: 'Quality finished Events. Over limit voltage between phases average',
    18: 'Quality finished Events. Over limit voltage L1',
    19: 'Quality finished Events. Over limit voltage L2',
    20: 'Quality finished Events. Over limit voltage L3',
    21: 'Quality finished Events. Long power failure for all phases',
    22: 'Quality finished Events. Long power failure L1',
    23: 'Quality finished Events. Long power failure L2',
    24: 'Quality finished Events. Long power failure L3'
}, 4: {
    1: 'Fraud, Cover opened',
    2: 'Fraud, Cover closed',
    3: 'Fraud, Strong DC field detected',
    4: 'Fraud, No strong DC field anymore',
    5: 'Fraud, Current without voltage',
    6: 'Fraud, Communication Fraud detection',
}, 5: {
    1: 'Reception order management critic residual demand',
    2: 'Reception order management critic % decrease demand',
    3: 'Reception order management demand absolute value critic demand',
    4: 'Reception order management no critic residual demand',
    5: 'Client acceptance management no critic residual demand',
    6: 'Client rejection management no critic residual demand',
    7: 'Reception order management no critic demand % decrease',
    8: 'Client acceptance management no critic demand % decrease',
    9: 'Rejection management no critic demand % decrease',
    10: 'Reception order management no critic absolute demand',
    11: 'Client acceptance management no critic absolute demand',
    12: 'Client Rejection management no critic absolute demand',
    13: 'subscribed residual demand changed',
    14: 'begin residual demand',
    15: 'End residual demand',
    16: 'begin decrease % subscribed demand',
    17: 'End decrease % subscribed demand',
    18: 'Begin reduction absolute power',
    19: 'End reduction absolute power',
    20: 'Demand close to Contract Power'
}, 6: {
    1: 'Frequent occurrence-common: Begin communication PLC Port',
    2: 'Frequent occurrence-common: End communication PLC Port',
    3: 'Frequent occurrence-common: Begin communication Optical Port',
    4: 'Frequent occurrence-common: End communication Optical Port',
    5: 'Frequent occurrence-common: Begin communication Serial Port',
    6: 'Frequent occurrence-common: End communications Serial Port'
}}

dc_events = {1: {
    1: 'Arranque alimentación alterna CD',
    2: 'Arranque alimentación contínua CD(si la tuviera)',
    5: 'Fallo alimentacion alterna CD',
    6: 'Fallo alimentación contínua CD(si la tuviese)',
    7: 'Batería baja',
    8: 'Cambio hora CD menor o igual de 30 segundos(NTPMaxDeviation)',
    9: 'Cambio hora CD mayor que 30 segundos (NTPMaxDeviation)',
    10: 'Error de memoria de programa',
    11: 'Error memoria RAM',
    12: 'Error memoria NV',
    13: 'Error de Watchdog',
    14: 'Error del sistema de archivos',
    15: 'Espacio libre de la memoria flash bajo (capacidad mínima libre superada)',
    16: 'Copia de seguridad realizada',
    17: 'Error de fallo del display (si lo tuviera)',
    18: 'Reserva para usos futuros. Error 01-16',
    19: 'Cambio configuración CD (parámetros)',
    20: 'Cambio configuracion puertos comunicaciones',
    21: 'Cambio Clave Consulta/Lectura, indicar usuario y método acceso',
    22: 'Cambio Clave Parametrización, indicar usuario y método acceso',
    23: 'Cambio Clave Firmware, indicar usuario y método acceso',
    24: 'Realizado Reset de Bateria, indicar usuario y método acceso',
    25: 'Realizado borrado manual de datos de medida',
    26: 'Realizado borrado automático de datos antiguos de la BD',
    27: 'Actualización de versión firmware CD',
    28: 'Actualización de versión firmware PLC PRIME (si afecta la actualización a la versión PRIME)',
    29: 'Actualización de versión firmware GPRS',
    30: 'Cambio Tipo Sincronismo Reloj',
    31: 'Cambio Asignacion de Salidas (cuando las tenga)',
    32: 'Fallo Descarga fichero firmware contador ftp desde el STG tras reintentos',
    33: 'Fallo Descarga fichero firmware concentrador ftp desde el STG tras reintentos',
    34: 'Fallo proceso actualizacion firmware contador',
    35: 'Fallo proceso actualización firmware concentrador',
    36: 'Petición errónea del STG (mensaje malformado, fechas inconsistentes, desconocido..)',
}, 2: {
    1: 'Inicio usuarios conectados al CD (usuario y contraseña)',
    2: 'Fin usuarios conectados al CD (usuario y contraseña)',
    3: 'Intento acceso con clave errónea (intrusión - usuario y contraseña)',
    4: 'Inicio usuarios conectados al CD (usuario y contraseña)',
    5: 'Fin usuarios conectados al CD (usuario y contraseña)',
    6: 'Intento acceso con clave errónea (intrusión - usuario y contraseña)',
    7: 'Apertura tapa bajo precinto fabricante',
    8: 'Cierre tapa bajo precinto fabricante',
    9: 'Apertura tapa cubrebornas/cubrehilos',
    10: 'Cierre tapa cubrebornas/cubrehilos'
}, 3: {
    1: 'Recepción orden gestión de la demanda potencia residual crítica',
    2: 'Recepción orden gestión de la demanda reducción % potencia crítica',
    3: 'Recepción orden gestión de la demanda valor abosluto de potencia crítica',
    4: 'Recepción orden gestión de la demanda potencia residual no critica',
    5: 'Aceptación gestión de la demanda por cliente potencia residual no critica',
    6: 'Rechazo gestión de la demanda por cliente potencia residual no critica',
    7: 'Recepción orden gestión de la demanda reducción % potencia no critica',
    8: 'Aceptación gestión de la demanda por cliente reducción % potencia no critica',
    9: 'Rechazo gestión de la demanda por cliente reducción % potencia no critica',
    10: 'Recepción orden gestión de la demanda potencia valor absoluto no critica',
    11: 'Aceptación gestión de la demanda por cliente potencia absoluto no critica',
    12: 'Rechazo gestión de la demanda por cliente potencia absoluto no critica',
    13: 'Cambio valor programación potencia residual contratada. Valor previo y actual',
    14: 'Activación/Inicio potencia residual. Valor previo y actual.',
    15: 'Fin potencia residual',
    16: 'Activación/Inicio reducción % potencia contratada',
    17: 'Fin reducción % potencia contratada',
    18: 'Activación/Inicio reducción potencia absoluta',
    19: 'Fin reducción potencia absoluta',
    20: 'Petición de datos desde dispositivo de cliente',
    21: 'Inicio ciclo petición de datos al contador por gestión de la demanda',
    22: 'Inicio ciclo envío automático de datos al dispositivo del cliente',
    23: 'Cambio parámetros de gestión de la demanda',
}, 4: {
    1: 'Inicio establecimiento de comunicaciones puerto PLC',
    2: 'Fin de comunicaciones puerto PLC',
    3: 'Inico establecimiento de comunicaciones puerto ETHERNET',
    4: 'Fin de comunicaciones puerto ETHERNET',
    5: 'Inico establecimiento de comunicaciones puerto GPRS',
    6: 'Fin de comunicaciones puerto GPRS',
    7: 'Inicio establecimiento de comunicaciones puerto serie RS-485',
    8: 'Fin de comunicaciones puerto serie RS-485',
    9: 'La señal de GPRS es muy débil',
    10: 'La conexión GSM/GPRS está caída.(cuando la tenga).',
    11: 'No hay tarjeta SIM instalada',
    12: 'La tarjeta SIM está pidiendo un código PIN',
    13: 'La conexión Ethernet está caída. (cuando la tenga).',
    14: 'El chip PRIME está funcionando incorrectamente o no responde',
    15: 'No existe MAC del módem PRIME definida.',
    16: 'Fallo envio WS al STG tras reintentos',
    17: 'Fallo envio fichero FTP al STG tras reintentos. Link lost while transfer',
    18: 'Fallo envio fichero FTP al STG tras reintentos. Wrong FTP username or password',
    19: 'Fallo envio fichero FTP al STG tras reintentos. Ftp service unavailable',
}, 5: {
    1: 'Alta física de un contador. Se añade un campo con el ID único del contador dado de alta en el sistema',
    2: 'Baja de un contador (física o a petición de STG). Se añade un campo con el ID único del contador dado de baja '
       'en el sistema',
}}
