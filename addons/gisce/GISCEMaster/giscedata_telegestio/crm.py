# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from tools.misc import cache


class CrmCase(osv.osv):

    _inherit = 'crm.case'
    _name = 'crm.case'

    @cache(timeout=3600)
    def _ff_cnc(self, cursor, uid, ids, field_name, arg, context=None):
        '''Takes cnc from meter in ref2'''
        meter_model = 'giscedata.lectures.comptador'
        meter_obj = self.pool.get(meter_model)

        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        res = dict.fromkeys(ids, '')

        for case in self.browse(cursor, uid, ids):
            if case.section_id.code == 'TG':
                if case.ref2:
                    ref_dict = case.ref2.split(',')
                    if len(ref_dict) > 1 and ref_dict[0] == meter_model:
                        meter = meter_obj.browse(cursor, uid, int(ref_dict[1]))
                        if meter and meter.tg:
                            cnc_name = meter.tg_cnc_id.name
                            res[case.id] = cnc_name
        return res

    def _ff_cnc_search(self, cursor, uid, obj, name, args, context=None):
        '''Search case's by cnc name'''
        if not context:
            context = {}

        cnc_obj = self.pool.get('tg.concentrator')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        search_dict = []
        for arg in args:
            search_dict.append(('name', arg[1], arg[2]))

        cnc_ids = cnc_obj.search(cursor, uid, search_dict)

        ref2_list = []
        if cnc_ids:
            meter_ids = meter_obj.search(cursor, uid,
                                         [('tg_cnc_id', 'in', cnc_ids)])

            ref2_list = ['giscedata.lectures.comptador,%s' % id
                         for id in meter_ids]

        return [('ref2', 'in', ref2_list), ('ref2', '!=', '')]

    def _ff_hour(self, cursor, uid, ids, field_name, arg, context=None):
        '''void function to implement hour search'''
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        res = dict.fromkeys(ids, False)

        return res

    def _ff_hour_search(self, cursor, uid, obj, name, args, context=None):
        '''function to implement hour search in case date'''
        if not context:
            context = {}

        section_obj = self.pool.get('crm.case.section')
        tg_section_id = section_obj.search(cursor, uid, [('code', '=', 'TG')])

        comparacio = (name == 'tg_hour_ini' and '>=' or '<')

        sql_tmpl = """SELECT id FROM crm_case WHERE section_id=%%s
                      AND date_part('%s', date) %s %%s"""
        sql = sql_tmpl % ('hour', comparacio)

        cursor.execute(sql, (tg_section_id[0], args[0][2]))
        case_ids = cursor.fetchall()

        return [('id', 'in', case_ids)]

    def _get_hours(self, cursor, uid, context=None):
        '''Returns hours tuple list'''
        return [(h, '%02d:00' % h) for h in range(0, 24)]

    def _get_ct(self, cursor, uid, ids, field_name, arg, context=None):
        """
        This method retrieve the CT of the cases formatted as a
        string containing its code and name
        @param cursor:
        @param uid:
        @param ids: ids of the cases.
        @param context:
        @return:
        """
        contract_model = 'giscedata.polissa'
        contract_obj = self.pool.get(contract_model)

        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        results = dict.fromkeys(ids, '')

        for case in self.browse(cursor, uid, ids, context):
            try:
                if case.section_id.code != 'TG':
                    continue
            except AttributeError:
                # browse object tried to load a ref field which points
                # to a model that it's still not loaded:
                continue

            ref_dict = case.ref and case.ref.split(',') or ''
            if ref_dict and ref_dict[0] == contract_model:
                contract = contract_obj.browse(
                    cursor, uid, int(ref_dict[1]), context
                )
                if contract.cups and contract.cups.et:
                    results[case.id] = contract.cups.et
        return results

    _columns = {
        'tg_cnc': fields.function(_ff_cnc, string='Concentrator', method=True,
                                  type="char", readonly=True,
                                  fnct_search=_ff_cnc_search,),
        'tg_hour_ini': fields.function(_ff_hour, string='Start time', method=True,
                                       type="selection", readonly=True,
                                       selection=_get_hours, size=10,
                                       fnct_search=_ff_hour_search,),
        'tg_hour_fi': fields.function(_ff_hour, string='End time', method=True,
                                      type="selection", readonly=True,
                                      selection=_get_hours, size=10,
                                      fnct_search=_ff_hour_search,),
        'tg_ct': fields.function(_get_ct, string='CT', method=True, type="char",
                                 readonly=True, store=True, size=60)
    }

CrmCase()
