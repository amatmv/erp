# coding=utf-8

import logging
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Change name of field modem_ip to public_ip')

    if oopgrade.column_exists(cursor, 'tg_concentrator', 'modem_ip'):

        oopgrade.rename_columns(cursor, {
            'tg_concentrator': [('modem_ip', 'public_ip')]
        })


def down(cursor, installed_version):
    pass

migrate = up
