# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        return False

    logger = logging.getLogger('openerp.migration')
    logger.info('Updating CCH_SERVER_FILES_STATUS')

    update_query = """
        UPDATE tg_cch_upload_status
        SET cch_server_status=subquery.status_upload_file, cch_server_datetime=subquery.dt
        FROM (
            SELECT id AS subq_id,
                CASE WHEN status LIKE 'uploaded' THEN 'OK'
                ELSE NULL
                END AS status_upload_file,
                CASE WHEN status LIKE 'uploaded' THEN datetime
                ELSE NULL
                END AS dt
            FROM tg_cch_upload_status
            ) AS subquery
        WHERE
        tg_cch_upload_status.status LIKE 'uploaded'
        AND tg_cch_upload_status.name IS NOT NULL
        AND tg_cch_upload_status.id=subquery.subq_id;
    """
    cursor.execute(update_query)
    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
