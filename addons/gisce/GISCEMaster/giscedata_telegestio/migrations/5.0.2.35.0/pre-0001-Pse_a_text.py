# -*- coding: utf-8 -*-
""" Convertim el camp serial_port_speed de la base de dades de int a char"""
import pooler
from datetime import datetime


def migrate(cursor, installed_version):
    uid = 1
    cursor.execute("""ALTER TABLE tg_concentrator
                      ALTER serial_port_speed TYPE CHAR(15)""")
