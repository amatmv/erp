<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<meta charset="utf-8">
<html>
<head>
    <style type="text/css">
	    ${css}
        rect.bordered {
            stroke: #E6E6E6;
            stroke-width: 2px;
        }

        text.mono {
            font-size: 9pt;
            font-family: Consolas, courier;
            fill: #000;
        }

        text.axis-workweek {
            fill: #000;
        }

        text.axis-worktime {
            fill: #000;
        }

        .text_capcalera{
            float:left;
            margin-top:20px;
        }
        .titol{
            float:left;
            width: 100%;
            text-align: center;
            font-weight: bold;
            padding: 3px;
            border-bottom: 1px solid #000000;
            border-top: 1px solid #000000;
            margin-top: 20px;
            margin-bottom: 15px;
            vertical-align: middle;
        }

        table{
            border-collapse: collapse;
            width: 100%;
            border-bottom: 1px solid #9F9F9F;
        }
        table td{
            text-align: left;
            font-size: 10px;
        }
    </style>
    <script src="http://d3js.org/d3.v3.js"></script>
</head>
<body>
    <div class="titol">
        ${_("Availability chart")}
    </div>
    <div>
        <table>
            <tr>
                <td style="width: 15%;">
                    <span class="label">${_("From date:")}</span>
                    <span class="field">${objects['date_from']}</span>
                </td>
                <td style="width: 80%;">
                    <span class="label">${_("To date:")}</span>
                    <span class="field">${objects['date_to']}</span>
                </td>
            </tr>
        </table>
    </div>
    %for meter in objects['meters']:
        <table>
            <tr>
                <td style="width: 15%;">
                    <span class="label">${_("For meter:")}</span>
                    <span class="field">${meter.keys()[0]}</span>
                </td>
            </tr>
        </table>
        <div id="${meter.keys()[0]}"></div>
        <script type="text/javascript">
            json_data = ${meter[meter.keys()[0]]['aggr']};
            var margin = {top: 50, right: 0, bottom: 100, left: 30},
                    width = 960 - margin.left - margin.right,
                    height = 430 - margin.top - margin.bottom,
                    gridSize = Math.floor(width / 24),
                    legendElementWidth = gridSize * 2,
                    buckets = 9,
                    colors = ["#ffffd9", "#edf8b1", "#c7e9b4", "#7fcdbb", "#41b6c4", "#1d91c0", "#225ea8", "#253494", "#081d58"], // alternatively colorbrewer.YlGnBu[9]
                    days = ["Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                    times = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "00"];

            var svg = d3.select("#${meter.keys()[0]}").append("svg")
                    .attr("width", width + margin.left + margin.right)
                    .attr("height", height + margin.top + margin.bottom)
                    .append("g")
                    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            var dayLabels = svg.selectAll(".dayLabel")
                    .data(days)
                    .enter().append("text")
                    .text(function (d) {
                        return d;
                    })
                    .attr("x", 0)
                    .attr("y", function (d, i) {
                        return i * gridSize;
                    })
                    .style("text-anchor", "end")
                    .attr("transform", "translate(-6," + gridSize / 1.5 + ")")
                    .attr("class", function (d, i) {
                        return ((i >= 0 && i <= 6) ? "dayLabel mono axis axis-workweek" : "dayLabel mono axis");
                    });

            var timeLabels = svg.selectAll(".timeLabel")
                    .data(times)
                    .enter().append("text")
                    .text(function (d) {
                        return d;
                    })
                    .attr("x", function (d, i) {
                        return i * gridSize;
                    })
                    .attr("y", 0)
                    .style("text-anchor", "middle")
                    .attr("transform", "translate(" + gridSize / 2 + ", -6)")
                    .attr("class", function (d, i) {
                        return ((i >= 0 && i <= 23) ? "timeLabel mono axis axis-worktime" : "timeLabel mono axis");
                    });

            var heatmapChart = function (data) {
                var colorScale = d3.scale.quantile()
                        .domain([0, d3.max(data, function (d) {
                            return d.value;
                        })])
                        .range(colors);

                var cards = svg.selectAll(".hour")
                        .data(data, function (d) {
                            return d.day + ':' + d.hour;
                        });

                cards.append("title");

                cards.enter().append("rect")
                        .attr("x", function (d) {
                            return (d.hour - 1) * gridSize;
                        })
                        .attr("y", function (d) {
                            return (d.day - 1) * gridSize;
                        })
                        .attr("rx", 4)
                        .attr("ry", 4)
                        .attr("class", "hour bordered")
                        .attr("width", gridSize)
                        .attr("height", gridSize)
                        .style("fill", colors[0]);

                cards.transition().duration(0)
                        .style("fill", function (d) {
                            return colorScale(d.value);
                        });

                cards.select("title").text(function (d) {
                    return d.value;
                });

                cards.exit().remove();

                var legend = svg.selectAll(".legend")
                        .data([0].concat(colorScale.quantiles()), function (d) {
                            return d;
                        });

                legend.enter().append("g")
                        .attr("class", "legend");

                legend.append("rect")
                        .attr("x", function (d, i) {
                            return legendElementWidth * i;
                        })
                        .attr("y", height)
                        .attr("width", legendElementWidth)
                        .attr("height", gridSize / 2)
                        .style("fill", function (d, i) {
                            return colors[i];
                        });

                legend.append("text")
                        .attr("class", "mono")
                        .text(function (d) {
                            return "≥ " + Math.round(d);
                        })
                        .attr("x", function (d, i) {
                            return legendElementWidth * i;
                        })
                        .attr("y", height + gridSize);

                legend.exit().remove();
            };

            heatmapChart(json_data);
        </script>
    %endfor
</body>
</html>
