from osv import osv, fields
from tools.translate import _
import StringIO
import paramiko
from datetime import datetime, timedelta
import base64
from random import randint
from time import sleep
import logging
from oorq.decorators import job
from os.path import exists
from os import listdir, mkdir, remove
import re


class TgCchServer(osv.osv):

    _name = 'tg.cch.server'

    @job(queue='low', timeout='14400')
    def upload(
            self,
            cursor,
            uid,
            server_id,
            attachment_id,
            context=None,
    ):
        """
        Upload an ir.attachment.

        :param cursor: database cursor
        :param uid: user identification
        :param server_id: tg.cch.server identification
        :param attachment_id: ir.attachment identification
        :param context: dictionary with context
        :param file_type: string with the type of attachment
        :return: None
        """
        tg_upload_status = osv.TransactionExecute(
            cursor.dbname, uid, 'tg.cch.upload.status'
        )
        tg_cch_marketer_server = self.pool.get('tg.cch.marketer.server')
        ir_attachment = self.pool.get('ir.attachment')
        crm_case = self.pool.get('crm.case')
        logger = logging.getLogger('openerp.{}'.format(__name__))

        if isinstance(server_id, (list, tuple)):
            server_id = server_id[0]
        server = self.browse(cursor, uid, server_id, context=context)

        while (tg_upload_status.search_count([('status', '=', 'uploading')])
                >= (server.max_connections or 1000)):
            sleep_seconds = randint(30, 120)
            logger.info(
                'Maximum simultaneous uploads reached, sleeping {} seconds '
                'before try to upload it again.'.format(sleep_seconds)
            )
            sleep(sleep_seconds)

        attachment = ir_attachment.browse(
            cursor,
            uid,
            attachment_id,
            context=context
        )

        status_id = tg_upload_status.search(
            [('attachment','=', attachment_id)],
            context=context
        )
        if status_id:
            tg_upload_status.write(
                status_id,
                {
                    'status': 'uploading',
                    'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                },
                context=context

            )
            status_id = status_id[0]
        else:
            status_id = tg_upload_status.create(
                {
                    'name': attachment.name,
                    'attachment': attachment_id,
                    'status': 'uploading',
                    'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                },
                context=context
            )

        ref = '{},{}'.format('tg.cch.upload.status', status_id)
        case_id = crm_case.search(
            cursor,
            uid,
            [('ref', '=', ref)],
            context=context
        )

        marketer_server_ids = tg_cch_marketer_server.search(
            cursor,
            uid,
            [
                ('server', '=', server_id),
                ('marketer', '=', attachment.marketer.id)
            ],
            context=context
        )
        marketer_server = tg_cch_marketer_server.browse(
            cursor,
            uid,
            marketer_server_ids,
            context=context
        )

        file = StringIO.StringIO(base64.b64decode(attachment.datas))

        if (
            marketer_server
        ) and (
            marketer_server[0].username
        ) and (
            marketer_server[0].password
        ):
            login = marketer_server[0]
        else:
            login = server

        comment = ''

        try:
            transport = paramiko.Transport((server.hostname, server.port))
            transport.connect(
                username=login.username,
                password=login.password
            )
            sftp = paramiko.SFTPClient.from_transport(transport)
            folder = server[attachment.type]
            sftp.putfo(file, '{}/{}'.format(folder, attachment.name))

            transport.close()

        except Exception as e:
            status = 'error'
            description = _('Can not upload the file {}').format(
                attachment.name
            )
            error = '{}: {}'.format(e.__class__,e)
            comment = '{}: {}: {}'.format(
                datetime.now(),
                description,
                error
            )
            extra_vals = {
                'description': comment
            }

            if context is None:
                context = {}
            context.update({'model': 'tg.cch.upload.status'})
            if case_id:
                case_description = crm_case.read(
                    cursor,
                    uid,
                    case_id,
                    ['description']
                )
                new_description = '{}\n{}'.format(
                    case_description[0]['description'],
                    comment
                )
                crm_case.write(
                    cursor,
                    uid,
                    case_id,
                    {'description': new_description}
                )
            else:
                case_id = crm_case.create_case_generic(
                    cursor,
                    uid,
                    [status_id],
                    context=context,
                    description=description,
                    section='TG',
                    extra_vals=extra_vals
                )
            crm_case.case_open(cursor, uid, case_id)

        else:
            status = 'uploaded'
            if case_id:
                crm_case.case_close(cursor, uid, case_id)
        finally:
            try:
                transport.close()
            except:
                pass

        tg_upload_status.write(
            status_id,
            {
                'status': status,
                'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                'comment': comment
            },
            context=context
        )

    def ftp_upload(self, cursor, uid, service, comer, filename, file_path, attachment_id, context=None):
        if context is None:
            context = {}

        provider_obj = self.pool.get('giscedata.ftp.provider')
        prov_id = provider_obj.get_provider(cursor, uid, service=service, context=context)
        provider = provider_obj.browse(cursor, uid, prov_id)
        if provider.f5d_enabled:
            path_ = getattr(provider, '{}_upload'.format(service), provider.f5d_upload)
            if '{comer}' in path_:
                path_ = path_.format(comer=comer)
            res = provider.upload(
                filename=filename, path=path_, file_obj=None, file_path=file_path
            )

            if res:
                status = 'uploaded'
            else:
                status = 'error'
            tg_cch_upload_status = self.pool.get('tg.cch.upload.status')
            tg_cch_upload_status.create(
                cursor, uid, {
                    'name': filename,
                    'attachment': attachment_id,
                    'status': status,
                    'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                }, context=context
            )

    _columns = {
        'name': fields.char("Name", size=256, required=True),
        'active': fields.boolean("Active"),
        'default': fields.boolean(
            "Default",
            help="Used if marketer doesn't have a specific one"
        ),
        'hostname': fields.char("Hostname", size=256, required=True),
        'port': fields.integer("Port"),
        'username': fields.char(
            "Default username",
            size=256,
            help="Used if marketer doesn't have a specific one"
        ),
        'password': fields.char(
            "Default password",
            size=256,
            help="Used if marketer doesn't have a specific one"
        ),
        'max_connections': fields.integer(
            "Maximum connections",
            help="Maxium connections simultaneous for this server",
            required=True
        ),
        'p5d': fields.char(
            "P5D (CCH VAL) folder",
            size=256,
            required=True
        ),
        'f5d': fields.char(
            "F5D (CCH FACT) folder",
            size=256,
            required=True
        )
    }

    _defaults = {
        'max_connections': lambda *a: 1000
    }

TgCchServer()


class TgCchUploadStatus(osv.osv):
    _name = 'tg.cch.upload.status'
    _order = 'datetime desc'

    def reupload(
            self,
            cursor,
            uid,
            upload_status_id,
            context=None,
            force=False
    ):
        """
        Upload the attachment to the server again.

        :param cursor: database cursor
        :param uid: user identidier
        :param upload_status_id: CCH upload status identifier
        :param context: dictionary with the context
        :param force: True: upload again. False: only upload if not uploaded.
        :return: None
        """
        ir_attachment = self.pool.get('ir.attachment')
        ftp_obj = self.pool.get('giscedata.ftp.connections')

        for upload_status in self.browse(
                cursor,
                uid,
                upload_status_id,
                context=context
        ):
            if force or upload_status.status != 'uploaded':
                ftp_server = ftp_obj.search(cursor, uid, [('category_type.code', '=', 'TGM')])
                if not ftp_server:
                    # SFTP Mode
                    ir_attachment.upload(
                        cursor,
                        uid,
                        upload_status.attachment.id,
                        context=context
                    )
                else:
                    # FTP Mode
                    self.reupload_on_ftp(cursor, uid, upload_status.id, context=context)

    def reupload_on_ftp(self, cursor, uid, upload_status_id, context=None):
        """
        Reupload file to FTP
        :param upload_status: tg_cch_upload_status id
        :return: None
        """
        ir_attachment = self.pool.get('ir.attachment')
        res_obj = self.pool.get('res.config')
        upload_status = self.browse(cursor, uid, upload_status_id)
        filedir = res_obj.get(
            cursor, uid, 'tg_profile_temp_filedir', '/tmp/curves'
        )
        att_id = upload_status.attachment.id
        filename = ir_attachment.read(cursor, uid, att_id, ['name'])['name']
        comer_name = filename[9:13]
        file_type = filename[:3].lower()
        # one dir per comer
        dest_dir = '{0}/{1}'.format(filedir, comer_name)
        if not exists(dest_dir):
            return False
        curve_filepath = '{}/{}'.format(dest_dir, filename)

        provider_obj = self.pool.get('giscedata.ftp.provider')
        prov_id = provider_obj.get_provider(cursor, uid, service=file_type, context=context)
        provider = provider_obj.browse(cursor, uid, prov_id)
        ftp_enabled = getattr(provider, '{}_enabled'.format(file_type), provider.f5d_enabled)
        if ftp_enabled:
            file_path = getattr(provider, '{}_upload'.format(file_type), provider.f5d_upload)
            ftp_path = file_path.format(comer=comer_name)

            res = provider.upload(
                filename=filename, path=ftp_path, file_obj=None, file_path=curve_filepath
            )
            if res:
                upload_status.write({'status': 'uploaded'})

    def get_status_cch(self, cursor, uid, ids, context=None):
        """
        Get logs from CCH server n days back
        Write status file: OK + comment + datetime or ERROR + comment + datetime
        Remove local downloaded file when used
        :return: None
        """
        if context is None:
            context = {}
        conf_obj = self.pool.get('res.config')
        tg_cch_server_obj = self.pool.get('tg.cch.server')
        server_id = tg_cch_server_obj.search(cursor, uid, [('hostname', '=', 'cch.asemeservicios.com')])
        if not server_id:
            return False
        server_id = server_id[0]
        days_back = int(conf_obj.get(cursor, uid, 'tg_cch_server_get_days', 5))
        log_prefix = 'log_{}'.format((datetime.now() - timedelta(days=days_back)).strftime('%y%m%d'))
        path_donwloaded = self.get_cs_logs(cursor, uid, server_id, log_prefix, context=context)
        self.parse_fp5d_logs(cursor, uid, path_donwloaded, context=context)

    def get_cs_logs(self, cursor, uid, server_id, log_prefix, context=None):
        tg_cch_server_obj = self.pool.get('tg.cch.server')
        remotepath = '/Logs/'
        localpath = '/tmp/cch_logs/'
        try:
            listdir(localpath)
        except Exception:
            mkdir(localpath)
        server = tg_cch_server_obj.browse(cursor, uid, server_id, context={})
        transport = paramiko.Transport((server.hostname, server.port))
        login = server
        transport.connect(username=login.username, password=login.password)
        sftp = paramiko.SFTPClient.from_transport(transport)
        for file_ in sftp.listdir(remotepath):
            if file_ > log_prefix:
                sftp.get(remotepath + file_, localpath + file_)
        return localpath

    def parse_fp5d_logs(self, cursor, uid, localpath, context=None):
        upload_status_ids = self.search(cursor, uid, [
            '|', ('cch_server_status', '=', False), ('cch_server_status', '!=', 'OK')
        ], context=context)
        for pf5d_file in self.read(cursor, uid, upload_status_ids, ['name']):
            if not pf5d_file['name']:
                continue
            for log in listdir(localpath):
                with open(localpath + log, "r") as file_:
                    for line in file_:
                        if re.search(pf5d_file['name'], line):
                            dt, status, fxfilename, message = line.split(';')
                            message = message.replace('\r', '')
                            message = message.replace('\n', '')
                            dt = datetime.strptime(dt, "%d/%m/%Y %H:%M:%S").strftime('%Y-%m-%d %H:%M:%S')
                            self.write(cursor, uid, pf5d_file['id'], {
                                'cch_server_status': status, 'cch_server_comment': message, 'cch_server_datetime': dt
                            })
        for file_ in listdir(localpath):
            remove(localpath + file_)

    def _cronjob_upload_failed_cch(self, cursor, uid, ids, context=None):
        """
        Upload failed CCH files again.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param context: dictionary with context
        :return: None
        """

        upload_status_ids = self.search(
            cursor,
            uid,
            [('status', '=', 'error')],
            context=context
        )
        self.reupload(cursor, uid, upload_status_ids, context=context)

    def _cronjob_get_status_cch(self, cursor, uid, ids, context=None):
        """
        Cronjob to Get logs from CCH server n days back
        Write status file: OK + comment + datetime or ERROR + comment + datetime
        Remove local downloaded ile when used
        :return: None
        """

        self.get_status_cch(cursor, uid, ids, context=context)

    _columns = {
        'name': fields.char("Name", size=256),
        'attachment': fields.many2one(
            'ir.attachment',
            "Attachment",
            required=True
        ),
        'status': fields.selection(
            [
                ('pending', "Pending"),
                ('uploading', "Uploading"),
                ('uploaded', "Uploaded"),
                ('error', "Error")
            ],
            'Status',
            required=True
        ),
        'datetime': fields.datetime("Date", required=True),
        'comment': fields.char("Comment", size=256),
        'cch_server_status': fields.selection([('OK', 'OK'), ("ERROR", "ERROR")], 'Status CCH Server'),
        'cch_server_datetime': fields.datetime("CCH Server Processed date"),
        'cch_server_comment': fields.char("Comment CCH Server", size=256),
    }

TgCchUploadStatus()


class TgCchMarketerServer(osv.osv):
    _name = 'tg.cch.marketer.server'

    def _default_marketer(self, cursor, uid, context=None):
        """
        Return de marketer from context.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dictionary of context
        :return: marketer of context
        """
        if context is None:
            context = {}
        return context.get('marketer', False)

    _defaults = {
      'marketer': _default_marketer
    }

    _columns = {
        'marketer': fields.many2one('res.partner', "Marketer", required=True),
        'server': fields.many2one('tg.cch.server', "Server", required=True),
        'username': fields.char("User", size=256),
        'password': fields.char("Password", size=256),
    }

TgCchMarketerServer()


class IrAttachment(osv.osv):
    _name = 'ir.attachment'
    _inherit = 'ir.attachment'

    def upload(self, cursor, uid, attachment_id, context=None):
        """
        Upload the attachment.

        :param cursor: database cursor
        :param uid: user identifier
        :param attachment_id: ir,attachment identifier
        :param context: dictionary with the context
        :return: None
        """

        tg_cch_marketer_server = self.pool.get('tg.cch.marketer.server')
        tg_cch_server = self.pool.get('tg.cch.server')

        attachment = self.browse(cursor, uid, attachment_id, context=None)

        marketer_server_ids = tg_cch_marketer_server.search(
            cursor,
            uid,
            [
                ('marketer', '=', attachment.marketer.id)
            ],
            context=context
        )
        marketer_servers = tg_cch_marketer_server.read(
            cursor,
            uid,
            marketer_server_ids,
            ['server'],
            context=context
        )
        server_ids = [a['server'][0] for a in marketer_servers]

        if not server_ids:
            server_ids = tg_cch_server.search(
                cursor,
                uid,
                [('default', '=', True)],
                context=context
            )

        for server in tg_cch_server.browse(
                cursor,
                uid,
                server_ids,
                context=context
        ):
            server.upload(
                attachment_id
            )

    _columns = {
        'marketer': fields.many2one('res.partner', "Marketer"),
        'type': fields.selection(
            [
                ('p5d', 'P5D (CCH VAL)'),
                ('f5d', 'F5D (CCH FACT)')
            ],
            'Type'
        )
    }

IrAttachment()

class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'server_connections': fields.one2many(
            'tg.cch.marketer.server',
            'marketer',
            'SFTP connections'
        )
    }

ResPartner()