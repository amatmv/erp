# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import time
from datetime import datetime


class WizardShowReadDataTG(osv.osv_memory):

    _name = 'wizard.show.tg.data'

    def _default_meter_name(self, cursor, uid, context=None):
        if context is None:
            context = {}

        registrador_obj = self.pool.get('giscedata.registrador')

        meter_id = context.get('active_id', False)

        name = registrador_obj.read(cursor, uid, meter_id, ['name'])['name']
        return name

    def action_show_read(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        tgmodel = context.get('tgmodel', 'tg.billing')

        wizard = self.browse(cursor, uid, ids[0], context=context)
        meter_name = wizard.meter_name

        domain = [('name', '=', meter_name)]

        if tgmodel == 'tg.billing':
            view_name = _('TG Billings {}').format(meter_name)
        elif tgmodel == 'tg.profile':
            view_name = _('TG Profiles {}').format(meter_name)

        vals_view = {
                'name': view_name,
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': tgmodel,
                'limit': 80,
                'type': 'ir.actions.act_window',
                'domain': domain,
                }

        return vals_view

    _columns = {

        'meter_name': fields.char('Meter Tg Name', size=32, required=True),
    }

    _defaults = {
        'meter_name': _default_meter_name
    }


WizardShowReadDataTG()
