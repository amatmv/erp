# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime
import json


class WizardAvailabilityReport(osv.osv_memory):

    _name = 'wizard.availability.report'

    def action_print_availability_report(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        avail_obj = self.pool.get('tg.meter.availability')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = context.get('active_ids', [])
        meter_names = meter_obj.read(cursor, uid, meter_ids, ['meter_tg_name'])
        meters = []
        for meter in meter_names:
            meter_name = meter['meter_tg_name']
            aggr = avail_obj.obtain_meter_availability_data(
                cursor, uid, meter_name, wizard.date_from, wizard.date_to)
            if aggr[1] == 0:
                continue
            meters.append(
                {
                    meter_name: {
                        'aggr': json.dumps(aggr[0]),
                        'data_amount': aggr[1],
                    }
                }
            )
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'tg.meter.heatmap',
            'datas': {
                'meters': meters,
                'date_from': wizard.date_from,
                'date_to': wizard.date_to
            }
        }

    _columns = {
        'date_from': fields.datetime('Date from', required=True),
        'date_to': fields.datetime('Date to', required=True),
        'info': fields.text('Info'),
    }

    _defaults = {
        'date_to': lambda *a: datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'info': lambda *a: _('Generate an availability report on the selected '
                             'period.')
    }

WizardAvailabilityReport()
