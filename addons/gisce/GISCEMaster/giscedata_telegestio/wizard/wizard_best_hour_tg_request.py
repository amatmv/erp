# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime


class WizardBestHourTgRequest(osv.osv_memory):

    _name = 'wizard.best.hour.tg.request'

    def action_get_best_hour(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        avail_obj = self.pool.get('tg.meter.availability')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        meter_name = meter_obj.read(cursor, uid, wizard.meter_id.id,
                                    ['meter_tg_name'])['meter_tg_name']
        aggr = avail_obj.obtain_meter_availability_for_weekday(cursor, uid,
                                                meter_name, wizard.request_date)
        if not aggr:
            wizard.write({'info': _("Couldn't find any connectivity data for "
                                    "this meter.")})
        else:
            hour = avail_obj.get_best_conn_hour_from_aggr(aggr, context=context)
            hour = str(hour).zfill(2)
            wizard.write({'info': _("The best hour to send a request to this "
                                "meter would be around: {}").format(str(hour))})

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador',
                                    'Meter', required=True),
        'request_date': fields.date('Request date', required=True),
        'info': fields.text('Info'),
    }

    _defaults = {
        'meter_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')[0],
        'request_date': lambda *a: datetime.today().date().strftime('%Y-%m-%d'),
        'info': lambda *a: _('Best hour for TG requests')
    }

WizardBestHourTgRequest()
