# -*- coding: utf-8 -*-

from osv import osv, fields


class WizardTgReadFiles(osv.osv_memory):

    _name = 'wizard.tg.read.files'

    def action_read(self, cursor, uid, ids, context=None):

        reader = self.pool.get('tg.reader')
        wizard = self.browse(cursor, uid, ids[0])

        reader.reader(cursor, uid, ftp_ids=[wizard.ftp_id.id],
                      context=context)

        return {}

    _columns = {
        'ftp_id': fields.many2one('tg.ftp', 'FTP Connection', required=True),
    }

WizardTgReadFiles()
