# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from giscedata_telegestio.utils import get_hexadecimal_control_bits_combination


class WizardAssignControlBitsNoValidate(osv.osv_memory):

    _name = 'wizard.assign.control.bits.no.validate'

    STATES = [('init', 'Init'), ('end', 'End')]

    def _get_default_info(self, cursor, uid, ids, context=None):
        conf_obj = self.pool.get('res.config')
        bc_exclude = eval(conf_obj.get(
            cursor, uid, 'tg_profile_bc_exclude', '[]')
        )
        if bc_exclude:
            _info = _("Bits actuales: {}. Marque los bits a asignar no "
                      "validación".format(bc_exclude))
        else:
            _info = _("Ningún bit a excepción. Marque los bits a asignar no "
                      "validación")

        return _info

    def action_set_hexadecimal_bits(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        wiz_obj = self.browse(cursor, uid, ids[0])

        # Get selected bits
        selected_bits = []
        for bit in range(1, 8):
            if getattr(wiz_obj, 'bit{}'.format(bit)):
                selected_bits.append(bit)

        result = get_hexadecimal_control_bits_combination(selected_bits)
        conf_obj = self.pool.get('res.config')
        bc_id = conf_obj.search(
            cursor, uid, [('name', '=', 'tg_profile_bc_exclude')]
        )
        if not bc_id:
            conf_obj.create(
                cursor, uid, {
                    'name': 'tg_profile_bc_exclude', 'value': result,
                    'description': False
                }
            )
        else:
            conf_obj.set(cursor, uid, 'tg_profile_bc_exclude', result)
        _msg = _("Bits actualizados correctamente: {}".format(str(result)))

        wiz_obj.write({'info': _msg, 'state': 'end'})

        return True

    _columns = {
        'bit1': fields.boolean("Bit 1. Incomplete period due to power failure"),
        'bit2': fields.boolean("Bit 2. Intrusion detected"),
        'bit3': fields.boolean("Bit 3. Modified parameters during period"),
        'bit4': fields.boolean("Bit 4. Time verification during period"),
        'bit5': fields.boolean("Bit 5. Overflow"),
        'bit6': fields.boolean("Bit 6. Synchronized meter during period"),
        'bit7': fields.boolean("Bit 7. Invalid measure"),
        'info': fields.text("Info", readonly=True),
        'state': fields.selection(STATES, 'states', select=True, required=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'bit1': lambda *a: False,
        'bit2': lambda *a: False,
        'bit3': lambda *a: False,
        'bit4': lambda *a: False,
        'bit5': lambda *a: False,
        'bit6': lambda *a: False,
        'bit7': lambda *a: False,
        'info': _get_default_info,
    }


WizardAssignControlBitsNoValidate()
