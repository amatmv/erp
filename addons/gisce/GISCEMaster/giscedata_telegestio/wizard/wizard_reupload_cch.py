from osv import osv, fields
from tools.translate import _

class TgCchReuploadWizard(osv.osv_memory):
    """
    Wizard to upload CCH.
    """

    _name = 'tg.cch.reupload.wizard'

    def reupload(
            self,
            cursor,
            uid,
            ids,
            context=None
    ):
        """
        Upload CCH

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: list with the wizard identifier
        :param context: dict with the context
        :return: None
        """

        tg_cch_upload_status = self.pool.get('tg.cch.upload.status')

        wizard = self.browse(cursor, uid, ids[0], context=context)

        tg_cch_upload_status.reupload(
            cursor,
            uid,
            context['active_ids'],
            force=wizard.force
        )

        info = _("Uploaded")

        wizard.write({'state': 'end', 'info': info})

    def _default_info(self, cursor, uid, context=None):
        """
        The notice about the action

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: string with the information
        """

        return _("Upload selected CCH")

    _columns = {
        'state': fields.selection(
            [
                ('init', 'Inicial'),
                ('fail', 'Fallit'),
                ('end', 'Final')
            ],
            'State'
        ),
        'info': fields.text('Info'),
        'force': fields.boolean(
            'Force',
            help=_("Upload althrough was uploaded")
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info
    }

TgCchReuploadWizard()