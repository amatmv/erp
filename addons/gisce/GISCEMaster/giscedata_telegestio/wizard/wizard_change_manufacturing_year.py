# -*- coding: utf-8 -*-
from osv import osv, fields


class WizardChangeManufacturingYear(osv.osv_memory):

    _name = 'wizard.change.manufacturing.year'

    _columns = {
        'manufacturing_year': fields.integer('New Manufacturing year')
    }

    def action_set_manufacturing_year(self, cursor, uid, ids, context=None):
        prodlot_o = self.pool.get('stock.production.lot')

        manufacturing_year = self.read(
            cursor, uid, ids, [], context=context
        )[0]['manufacturing_year']

        prodlot_o.write(
            cursor, uid, context['active_id'],
            {'manufacturing_year': manufacturing_year}, context=context
        )
        return {}


WizardChangeManufacturingYear()
