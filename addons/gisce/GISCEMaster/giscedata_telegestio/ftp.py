# -*- coding: utf-8 -*-

from ftplib import all_errors, FTP
from osv import osv, fields
from tools.translate import _
import socket
from StringIO import StringIO
import re


class TgFtp(osv.osv):

    _name = 'tg.ftp'

    def login(self, cursor, uid, server_id, context=None):
        '''login to a ftp server
        returns a connection to the server'''

        if isinstance(server_id, (list, tuple)):
            server_id = server_id[0]

        server = self.browse(cursor, uid, server_id)
        try:
            ftpcon = FTP(server.ip_address)
            if server.anonymous:
                ftpcon.login()
            else:
                ftpcon.connect(server.ip_address, int(server.port or '21'))
                ftpcon.login(server.user, server.password)
        except all_errors, e:
            raise osv.except_osv(_('Error!'),
                                 _('Connection error: %s') % (e))
        return ftpcon

    def close(self, cursor, uid, ftpcon, context=None):
        '''close connection to a ftp server'''
        return ftpcon.quit()

    def get_files(self, cursor, uid, ftpcon, path='/', context=None):
        '''return list of files to read'''
        try:
            res = ftpcon.nlst(path)
        except all_errors, e:
            raise osv.except_osv(_('Error!'),
                                 _('Directory listing error: %s') % (e))
        # some FTP servers returns full path, others only filename
        if res:
            res = [p.split('/')[-1] for p in res]
        return res

    def read_file(self, cursor, uid, ftpcon, filename, context=None):
        '''returns the content of a file as string'''
        output = StringIO()
        ftpcon.retrbinary('RETR %s' % filename, output.write)
        return output.getvalue()

    def move_file(self, cursor, uid, ftpcon, filename, origin_path,
                  dest_path, context=None):
        '''Moves a file from origin_path to dest_path
        inside the ftp server. If dest_path do not exists
        it will be created'''
        filename = filename.split('/')[-1]
        try:
            ftpcon.cwd(dest_path)
        except all_errors, e:
            ftpcon.mkd(dest_path)
        try:
            ftpcon.rename('%s%s' % (origin_path, filename),
                          '%s%s' % (dest_path, filename))
        except all_errors, e:
            if '550' in str(e):
                ftpcon.rename(
                        '%s%s' % (origin_path, filename),
                        '%s%s' % (dest_path, filename+'_dup')
                )
            else:
                raise osv.except_osv(_('Error!'),
                                 _('Move file error: %s') % (e))
        return True

    def get_root_dir(self, cursor, uid, server_id, context=None):
        server = self.read(cursor, uid, server_id, ['root_dir'])

        return server['root_dir'] or '/'

    def get_read_dirs(self, cursor, uid, server_id, context=None):
        server = self.read(cursor, uid, server_id, ['read_dir'])

        dir_list = server['read_dir'].split(';') or ['/']

        return dir_list

    def _ip_address_valid(self, cursor, uid, ids):
        '''validates an ipv4 address'''
        for server in self.browse(cursor, uid, ids):
            try:
                socket.inet_aton(server.ip_address)
            except socket.error:
                return False
        return True

    def _valid_dir_name(self, dirname):
        '''checks dirname'''
        not_allowed_chars = '[,*?<>:\'"|]'
        if re.search(not_allowed_chars, dirname):
            return False
        else:
            return True

    def _path_valid(self, cursor, uid, ids):
        '''validates a FTP directory path'''
        for server in self.browse(cursor, uid, ids):
            rdir = server.root_dir
            if not self._valid_dir_name(rdir):
                return False
            if not rdir or rdir[-1] != "/" or rdir[0] != "/":
                return False
            # semi-colon separated directories list
            r_dirs = server.read_dir.split(';')
            for rdir in r_dirs:
                if not self._valid_dir_name(rdir):
                    return False
                if not rdir or rdir[-1] != "/" or rdir[0] != "/":
                    return False
        return True

    _columns = {
        'name': fields.char('Description', size=150, required=True),
        'code': fields.char('Code', size=20),
        'ip_address': fields.char('IP address', size=15, required=True),
        'user': fields.char('User', size=100),
        'password': fields.char('Password', size=50),
        'port': fields.char('Port', size=50),
        'anonymous': fields.boolean('Anonymous login'),
        'root_dir': fields.char('Root dir', size=20),
        'read_dir': fields.char('Read dir(s)', size=255,
                                help='semi-colon(;) separated dir list'),
    }

    _defaults = {
        'anonymous': lambda *a: False,
        'root_dir': lambda *a: '/',
        'read_dir': lambda *a: '/',
        'port': lambda *a: '21',
    }

    _constraints = [(_ip_address_valid,
                     _(u'Ip address not valid'),
                     ['ip_address']),
                    (_path_valid,
                     _(u"FTP directory not valid. Invalid chars ',', '*', '?',"
                       u" '<' ,'>' ,':' ,'\"', '|' in dir name or FTP "
                       u'directories must be enclosed by slashes ("/"). i.e '
                       u'"/root/dir/"'),
                     ['root_dir', 'read_dir']),
                    ]

TgFtp()
