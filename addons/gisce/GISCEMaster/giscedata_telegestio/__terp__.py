# -*- coding: utf-8 -*-
{
    "name": "Telegestio",
    "description": """
    Interact with PRIME
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_cups_distri",
        "giscedata_telemesures_base"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_telegestio_demo.xml"
    ],
    "update_xml":[
        "security/telegestio_security.xml",
        "wizard/wizard_read_files_view.xml",
        "wizard/wizard_validate_billing_view.xml",
        "wizard/wizard_reupload_cch_view.xml",
        "telegestio_view.xml",
        "ftp_view.xml",
        "cch_server_view.xml",
        "telegestio_data.xml",
        "telegestio_scheduler.xml",
        "lot_view.xml",
        "security/ir.model.access.csv",
        "giscedata_telegestio_report.xml",
        "wizard/wizard_best_hour_tg_request_view.xml",
        "wizard/wizard_availability_report_view.xml",
        "wizard/wizard_change_manufacturing_year.xml",
        "wizard/wizard_assign_control_bits_no_validate_view.xml",
        "wizard/wizard_show_tg_data_view.xml",
        "giscedata_telemesures_base_view.xml",
    ],
    "active": False,
    "installable": True
}
