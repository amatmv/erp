from base_index.base_index import BaseIndex


class GiscedataCupsPs(BaseIndex):
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _index_fields = {
        'titular.name': True,
    }

GiscedataCupsPs()
