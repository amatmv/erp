# -*- coding: utf-8 -*-
{
    "name": "Index CUPS (titular)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index for CUPS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cups_index",
        "giscedata_cups_titular"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
