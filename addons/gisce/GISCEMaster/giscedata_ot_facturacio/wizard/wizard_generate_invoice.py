# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from tools.translate import _


class NewFacturaOt(osv.osv_memory):
    _name = "wizard.generate.invoice.ot"

    def create_factures(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        context.update({
            "active_ids": self.active_ids,
            "replace_factures": wizard.replace_factures
        })

        invoice_obj = self.pool.get("account.invoice")
        line_obj = self.pool.get("account.invoice.line")
        ot_obj = self.pool.get("giscedata.ot")

        ot_ids = self._get_ots_per_facturar(cursor, uid, context)

        try:
            vals = self.get_factura_vals(cursor, uid, ids, ot_ids, context=context)
            invoice_id = invoice_obj.create(cursor, uid, vals, context=context)
            for ot_id in ot_ids:
                vals = self.get_line_vals(cursor, uid, invoice_id, ot_id, context)
                line_obj.create(cursor, uid, vals)
            ot_obj.write(cursor, uid, ot_ids, {'factura_id': invoice_id})
            msg = _(u"Factura creada correctament")
            state = 'final'
        except osv.except_osv as e:
            msg = "Error: {0}".format(e.value)
            invoice_id = False
            state = 'error'

        self.write(cursor, uid, ids, {
            'state': state,
            'info': msg,
            'factura_id': invoice_id
        })

    def accio_mostrar_factura(self, cursor, uid, ids, context=None):
        factura = self.read(cursor, uid, ids[0], ['factura_id'])
        factura = factura[0]['factura_id']
        return {
            'domain': "[('id','in', {0})]".format(str([factura])),
            'name': _('Factura Generada'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window'
        }

    def get_factura_vals(self, cursor, uid, ids, ot_ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        contrata = wizard.contrata_id
        dmin = wizard.data_min[:-3]
        dmax = wizard.data_max[:-3]
        msg = ""
        if not len(contrata.address):
            msg += _(u"L'Empresa Encarregada no té cap adreça assignada.\n")
        if not contrata.property_account_payable:
            msg += _(u"L'Empresa Encarregada no té cap compte assignat per fer-hi pagaments.\n")
        if msg:
            raise osv.except_osv('Error', msg)
        imd_obj = self.pool.get('ir.model.data')
        ot_journal = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_ot_facturacio', 'journal_ot'
        )[1]
        vals = {
            'name': _(u"OTs {0} - {1}".format(dmin, dmax)),
            'comment': _(u"Factura de Ordres de Treball"),
            'date_invoice': wizard.data_factura,
            'partner_id': contrata.id,
            'address_invoice_id': contrata.address[0].id,
            'account_id': contrata.property_account_payable.id,
            'type': 'in_invoice',
            'partner_bank': len(contrata.bank_ids) and contrata.bank_ids[0].id or False,
            'journal_id': ot_journal,
        }

        # Check if we have SII modules installed. If they are, we mark invoices
        # with "don't send"
        acc_inv_obj = self.pool.get('account.invoice')
        if 'sii_to_send' in acc_inv_obj.fields_get(cursor, uid):
            vals.update({'sii_to_send': False})
        return vals

    def get_line_vals(self, cursor, uid, invoice_id, ot_id, context=None):
        ot_obj = self.pool.get("giscedata.ot")
        partner_obj = self.pool.get("res.partner")
        close_obj = self.pool.get("giscedata.ot.tancament")
        invoice_obj = self.pool.get("account.invoice")

        ot_info = ot_obj.read(cursor, uid, ot_id, ['name', 'close_id', 'units',
                                                   'operator_partner_id'])
        if not ot_info['close_id']:
            msg = _(u"La ordre de treball amb id {0} no té codi de "
                    u"tancament.").format(ot_info['id'])
            raise osv.except_osv('Error', msg)
        close_id = ot_info['close_id'][0]
        desc = ot_info['name']

        close_info = close_obj.read(cursor, uid, close_id, ['product_id'])
        if not close_info['product_id']:
            msg = _(u"El codi de tancament {0} no té cap producte "
                    u"assignat.").format(ot_info['close_id'][1])
            raise osv.except_osv('Error', msg)
        product_id = close_info['product_id'][0]

        if not ot_info['operator_partner_id']:
            msg = _(u"La ordre de treball amb id {0} no té empresa "
                    u"encarregada.").format(ot_info['id'])
            raise osv.except_osv('Error', msg)
        contrata_id = ot_info['operator_partner_id'][0]

        partner_info = partner_obj.read(cursor, uid, contrata_id,
                                        ['property_account_payable'])
        if not partner_info['property_account_payable']:
            msg = _(u"L'Empresa Encarregada no té cap compte assignat per "
                    u"fer-hi pagaments.")
            raise osv.except_osv('Error', msg)
        account = partner_info['property_account_payable'][0]

        inv_info = invoice_obj.read(cursor, uid, invoice_id, ['date_invoice'])
        fdate = inv_info['date_invoice']
        price = self.get_product_price(cursor, uid, fdate, product_id, context)

        vals = {
            'name': _(u"OT {0}: {1}").format(ot_id, desc),
            'invoice_id': invoice_id,
            'product_id': product_id,
            'quantity': ot_info['units'],
            'account_id': account,
            'price_unit': price
        }
        return vals

    def get_product_price(self, cursor, uid, fdate, product_id, context):
        pricelist_obj = self.pool.get('product.pricelist')
        imd_obj = self.pool.get('ir.model.data')
        tarif_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_ot_facturacio', 'tarif_ot')[1]
        context.update({'date': fdate})
        return pricelist_obj.price_get(cursor, uid, [tarif_id], product_id, 1.0, context=context)[tarif_id]

    def _fill_info(self, cursor, uid, context=None):
        if not context:
            return ""
        if context.get("active_ids", False):
            self.active_ids = context.get("active_ids")
            ot_ids = self.active_ids
        else:
            context.update({"active_ids": self.active_ids})
            ot_ids = self.active_ids

        info = _(u"* S'ignoraran les ordres de treball que no estan a "
                 u"facturar.\n")
        if context.get("replace_factures", False):
            info += _(u"* Es substituiran les factures en borrador de les "
                      u"ordres seleccionades per la nova factura.\n\n")
        else:
            info += _(u"* S'ignoraran les ordres de treball que ja tenen "
                      u"factures en borrador.\n\n")

        check_contratas = self._check_contratas(cursor, uid, context)
        if not check_contratas:
            msg = self._contratas_msg(cursor, uid, ot_ids, context)
            info = _(u"Error\nOrdres de Treball amb diferents Empreses:\n{0}\n"
                     ).format(msg)
            return info
        if len(self._get_ots_per_facturar(cursor, uid, context)) == 0:
            info = _(u"Error\nNo s'ha seleccionat cap Ordre de Treball a "
                     u"facturar.\n")
            return info
        if not self._check_a_facturar(cursor, uid, ot_ids, context):
            msg = self._a_facturar_msg(cursor, uid, ot_ids, context)
            info += _(u"Warning. S'ha seleccionat Ordres de Treball que no "
                      u"s'han de facturar:\n{0}\n").format(msg)
        if not self._check_factures(cursor, uid, ot_ids, context):
            msg = self._factures_msg(cursor, uid, ot_ids, context)
            info += _(u"Warning. S'ha seleccionat Ordres de Treball per facturar "
                      u"ja que tenen factures en borrador:\n{0}").format(msg)
        return info

    def _check_contratas(self, cursor, uid, context=None):
        """
        Returns True if all 'operator_partner_id' of ot_ids are the same.
        Returns False otherwise.
        """
        ot_obj = self.pool.get("giscedata.ot")
        ot_ids = context.get("active_ids", [])
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['operator_partner_id'])
        contratas = []
        for ot_info in ots_info:
            contrata = ot_info['operator_partner_id'][0]
            if contrata not in contratas:
                contratas.append(contrata)
            if len(contratas) > 1:
                return False
        return len(contratas) == 1

    def _contratas_msg(self, cursor, uid, ot_ids, context=None):
        """
        Returns a message with information about the number of ot_ids for
        operator_partner_id.
        """
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['operator_partner_id'])

        contratas_dict = {}
        for ot_info in ots_info:
            contrata = ot_info['operator_partner_id'][0]
            if not contratas_dict.get(contrata, False):
                contratas_dict[contrata] = 0
            contratas_dict[contrata] += 1

        partner_obj = self.pool.get("res.partner")
        msg = ""
        for contrata in contratas_dict.keys():
            name = partner_obj.read(cursor, uid, contrata, ['name'])['name']
            msg += "*{0}: {1}\n".format(name, contratas_dict[contrata])
        return msg

    def _check_a_facturar(self, cursor, uid, ot_ids, context=None):
        """
        Returns True if all 'a_facturar' of ot_ids are True.
        Returns False otherwise.
        """
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['a_facturar'])
        for ot_info in ots_info:
            if not ot_info['a_facturar']:
                return False
        return True

    def _a_facturar_msg(self, cursor, uid, ot_ids, context=None):
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['a_facturar'])
        a_facturar = 0

        for ot_info in ots_info:
            if ot_info['a_facturar']:
                a_facturar += 1
        no_facturar = len(ot_ids) - a_facturar
        msg = _(u"*A Facturar: {0}\n"
                u"*No Facturar: {1}\n").format(a_facturar, no_facturar)
        return msg

    def _check_factures(self, cursor, uid, ot_ids, context=None):
        """
        Returns True if all 'factura_id' of ot_ids are False.
        Returns False otherwise.
        """
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['factura_id', 'a_facturar'])
        for ot_info in ots_info:
            if ot_info['a_facturar'] and ot_info['factura_id']:
                return False
        return True

    def _factures_msg(self, cursor, uid, ot_ids, context=None):
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['factura_id', 'a_facturar'])
        amb_factura = 0
        sense_factura = 0
        for ot_info in ots_info:
            if ot_info['a_facturar']:
                if ot_info['factura_id']:
                    amb_factura += 1
                elif not ot_info['factura_id']:
                    sense_factura += 1
        msg = _(u"*Amb Factura: {0}\n"
                u"*Sense factura: {1}\n").format(amb_factura, sense_factura)
        return msg

    def _get_numero_ots(self, cursor, uid, context=None):
        if not context:
            return 0
        return len(context.get("active_ids", []))

    def _get_contrata(self, cursor, uid, context=None):
        if not self._check_contratas(cursor, uid, context):
            return False
        ot_id = context.get("active_ids", [])[0]
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_id, ['operator_partner_id'])
        return ots_info['operator_partner_id'][0]

    def _get_numero_per_facturar(self, cursor, uid, context=None):
        return len(self._get_ots_per_facturar(cursor, uid, context))

    def _get_data_min(self, cursor, uid, context=None):
        if not context:
            return False
        ot_ids = context.get("active_ids", False)
        if len(ot_ids) < 1:
            return False
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['date_closed'])
        datestr = "%Y-%m-%d %H:%M:%S"
        dates = [datetime.strptime(x['date_closed'], datestr) for x in ots_info if
                 x['date_closed']]
        if not len(dates):
            return False
        return min(dates).strftime('%Y-%m-%d')

    def _get_data_max(self, cursor, uid, context=None):
        if not context:
            return False
        ot_ids = context.get("active_ids", False)
        if len(ot_ids) < 1:
            return False
        ot_obj = self.pool.get("giscedata.ot")
        ots_info = ot_obj.read(cursor, uid, ot_ids, ['date_closed'])
        datestr = "%Y-%m-%d %H:%M:%S"
        dates = [datetime.strptime(x['date_closed'], datestr) for x in ots_info if
                 x['date_closed']]
        if not len(dates):
            return False
        return max(dates).strftime('%Y-%m-%d')

    def _get_data_factura(self, cursor, uid, context=None):
        return datetime.today().strftime('%Y-%m-%d')

    def _get_ots_per_facturar(self, cursor, uid, context=None):
        if not context:
            return 0
        ot_ids = context.get("active_ids", [])
        ot_obj = self.pool.get("giscedata.ot")
        replace_factures = context.get("replace_factures", False)
        if not replace_factures:
            ot_per_facturar = ot_obj.search(
                cursor, uid, [('id', 'in', ot_ids),
                              ('a_facturar', '=', True),
                              ('factura_id', '=', False)]
            )
        else:
            ot_per_facturar = ot_obj.search(
                cursor, uid, [('id', 'in', ot_ids),
                              ('a_facturar', '=', True)]
            )
        return ot_per_facturar

    def onchange_replace_factures(self, cursor, uid, ids, replace_factures,
                                 context=None):
        if not context:
            context = {}
        context.update({
            "replace_factures": bool(replace_factures),
        })
        res = {'value': {}}
        res['value'].update({
            'info': self._fill_info(cursor, uid, context),
            'total_per_facturar': self._get_numero_per_facturar(cursor, uid, context),
        })
        return res

    _columns = {
        'info': fields.text("Info", readonly=True),
        'state': fields.char('State', size=16),
        'factura_id': fields.many2one("account.invoice", "Factura"),
        'check_contratas': fields.boolean(),
        'total_treballs': fields.integer(_(u"Total treballs"), readonly=True),
        'total_per_facturar': fields.integer(_(u"Total per facturar"),
                                             readonly=True),
        'contrata_id': fields.many2one('res.partner', _(u"Empresa Encarregada"),
                                       readonly=True),
        'data_min': fields.date(_(u"Data Inici"), readonly=True),
        'data_max': fields.date(_(u"Data Final"), readonly=True),
        'data_factura': fields.date(_(u"Data Factura"), required=True),
        'replace_factures': fields.boolean(
            _(u"Forçar Factures"),
            help=_(u"Si hi ha Ordres de Treball que ja tenen factures peró "
                   u"aquestes estan en borrador, es substituïran per les "
                   u"noves creades. Altrament s'ignoraran."))
    }

    _defaults = {
        'info': _fill_info,
        'check_contratas': _check_contratas,
        'total_treballs': _get_numero_ots,
        'total_per_facturar': _get_numero_per_facturar,
        'contrata_id': _get_contrata,
        'data_min': _get_data_min,
        'data_max': _get_data_max,
        'data_factura': _get_data_factura,
        'state': lambda *x: 'init',
    }

NewFacturaOt()
