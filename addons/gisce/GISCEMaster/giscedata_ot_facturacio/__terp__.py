# -*- coding: utf-8 -*-
{
    "name": "GISCE Facturacio Ordres de Treball",
    "description": """Facturacio de les Ordres de Treball.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_ot_comptadors"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_ot_facturacio_demo.xml"
    ],
    "update_xml":[
        "giscedata_ot_facturacio_data.xml",
        "giscedata_ot_view.xml",
        "wizard/wizard_generate_invoice_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
