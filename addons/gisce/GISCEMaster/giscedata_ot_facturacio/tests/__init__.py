# -*- coding: utf-8 -*-
import unittest

from destral import testing
from destral.transaction import Transaction
from datetime import datetime, timedelta
from expects import expect, raise_error
from osv.orm import except_orm
from addons import get_module_resource
import time
from tools.misc import cache


class WizardTest(testing.OOTestCase):

    def test_cretion_invoice_from_ots(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool

            ot_obj = pool.get("giscedata.ot")
            imd_obj = pool.get('ir.model.data')

            # Get data to create OTs
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            partner_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            wdate = datetime.today().strftime('%Y-%m-%d')

            close_1 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_facturacio', 'tancament_ot_p1'
            )[1]
            close_2 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_facturacio', 'tancament_ot_p2'
            )[1]

            tarif_version = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_facturacio', 'tarif_ot_test'
            )[1]
            pool.get("product.pricelist.version").write(
                cursor, uid, [tarif_version], {
                    'date_end': (
                        datetime.today() + timedelta(days=10)
                    ).strftime("%Y-%m-%d")
                }
            )

            # Create OTs
            ot_vals = {
                'name': "OT1",
                'operator_partner_id': partner_id,
                'work_date': wdate,
                'date_closed': wdate,
                'cups_id': cups_id,
                'polissa_id': polissa_id,
                'close_id': close_1,
                'units': 1,
                'state': 'done',
            }
            ot1 = ot_obj.create(cursor, uid, ot_vals)

            ot_vals.update({
                'name': "OT2",
                'units': 2,
                'close_id': close_2
            })
            ot2 = ot_obj.create(cursor, uid, ot_vals)

            # Create wizard with ots as active_ids
            context = {'active_ids': [ot1, ot2]}
            wizard_obj = pool.get("wizard.generate.invoice.ot")
            wid = wizard_obj.create(cursor, uid, {}, context)

            # Call wizard to create the invoice fro OTs
            wizard_obj.create_factures(cursor, uid, [wid], context)

            # Get created invoice
            wizard = wizard_obj.browse(cursor, uid, wid)
            factura = wizard.factura_id
            partner = wizard.contrata_id

            # Check factura vals
            self.assertEqual(factura.date_invoice, wizard.data_factura)
            self.assertEqual(factura.partner_id.id, partner.id)
            self.assertEqual(factura.address_invoice_id.id,
                             partner.address[0].id)
            self.assertEqual(factura.account_id.id,
                             partner.property_account_payable.id)
            self.assertEqual(factura.partner_bank.id, partner.bank_ids[0].id)
            self.assertEqual(len(factura.invoice_line), 2)
            # All products have price 1.0 and we have 3 units
            self.assertEqual(factura.amount_total, 3)
