# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class GiscedataOtTancament(osv.osv):
    _name = 'giscedata.ot.tancament'
    _inherit = 'giscedata.ot.tancament'

    def _proveidors(self, cursor, uid, ids, field_name, ar, context=None):
        res = dict([(i, False) for i in ids])
        supliers_obj = self.pool.get('product.supplierinfo')
        for id in ids:
            prd_id = self.read(cursor, uid, id, ['product_id'])['product_id'][0]
            suplier_ids = supliers_obj.search(cursor, uid,
                                              [('product_id', '=', prd_id)])
            supliers_info = supliers_obj.read(cursor, uid,
                                              suplier_ids, ['name'])
            res[id] = [x['name'][0] for x in supliers_info]
        return res

    def _ff_sellers_search(self, cursor, uid, obj, name, args, context):
        supliers_obj = self.pool.get('product.supplierinfo')
        suplier_ids = supliers_obj.search(cursor, uid,
                                          [('name', '=', args[0][2])])
        supliers_info = supliers_obj.read(cursor, uid,
                                          suplier_ids, ['product_id'])
        prod_ids = [x['product_id'][0] for x in supliers_info]
        return [('product_id', 'in', prod_ids)]

    _columns = {
        'name': fields.related('product_id', 'default_code',
                               type='char', relation='product.product',
                               string="Codi Tancament"),
        'product_id': fields.many2one('product.product', 'Producte'),
        'product_description': fields.related('product_id', 'description',
                                              string="Desc.", readonly=True,
                                              type='char', size=256),
        'seller_ids': fields.function(
            _proveidors, method=True, type='one2many', relation='res.partner',
            string='Proveïdors', fnct_search=_ff_sellers_search),
    }

GiscedataOtTancament()


class GiscedataOt(osv.osv):
    _name = 'giscedata.ot'
    _inherit = 'giscedata.ot'

    def onchange_section_id(self, cursor, uid, ids, section_id, polissa_id,
                            cups_id, context=None):
        vals = super(GiscedataOt, self).onchange_section_id(
            cursor, uid, ids, section_id, polissa_id, cups_id)
        if 'close_id' not in vals['value']:
            return vals
        close_id = vals['value']['close_id']
        if not close_id:
            return vals
        close_obj = self.pool.get('giscedata.ot.tancament')
        partners = close_obj.read(cursor, uid, close_id, ['seller_ids'])
        if len(partners['seller_ids']) == 1:
            partner = partners['seller_ids'][0]
            vals['value'].update({'operator_partner_id': partner})
            extr_vals = self.onchange_operator_partner_id(
                cursor, uid, ids, partner
            )
            vals['value'].update(extr_vals['value'])
        return vals

    def create(self, cursor, uid, vals, context=None):
        if vals.get('section_id', False) and not vals.get('close_id', False):
            sid = vals['section_id']
            pol_id = vals.get('polissa_id')
            cups_id = vals.get('cups_id')
            extr_vals = self.onchange_section_id(cursor, uid, None, sid, pol_id,
                                                 cups_id)
            vals.update(extr_vals['value'])
        return super(GiscedataOt, self).create(cursor, uid, vals, context)

    def _ff_facturada(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict([(i, False) for i in ids])
        invoice_obj = self.pool.get("account.invoice")

        info = self.read(cursor, uid, ids, ['factura_id'])
        info = [(x['id'], x['factura_id'][0]) for x in info if x['factura_id']]
        for ot_id, factura in info:
            state = invoice_obj.read(cursor, uid, factura, ['state'])['state']
            res[ot_id] = state == 'proforma2'

        return res

    def _ff_facturada_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = None
        ids = self.search(cursor, uid, [('factura_id.state', '=', 'proforma2')])
        operacio = args[0][2] and 'in' or 'not in'
        return [('id', operacio, ids)]

    def _ff_a_facturar(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict([(i, False) for i in ids])
        info = self.read(cursor, uid, ids, ['state', 'facturada'])
        for ot_info in info:
            ot_id = ot_info['id']
            state = ot_info['state']
            facturada = ot_info['facturada']
            res[ot_id] = (not facturada) and (state == 'done')
        return res

    def _ff_a_facturar_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = None
        ids = self.search(cursor, uid, [('state', '=', 'done'),
                                        ('facturada', '=', False)])
        operacio = args[0][2] and 'in' or 'not in'
        return [('id', operacio, ids)]

    def _check_units(self, cursor, uid, ids):
        for case in self.browse(cursor, uid, ids):
            if case.units <= 0:
                return False
        return True

    _columns = {
        'units': fields.integer(string="Unitats"),
        'a_facturar': fields.function(_ff_a_facturar, method=True,
                                      type='boolean', string=u'A Facturar',
                                      fnct_search=_ff_a_facturar_search,
                                      readonly=True, select=2),
        'factura_id': fields.many2one('account.invoice', "Factura", select=2),
        'facturada': fields.function(_ff_facturada, method=True, type='boolean',
                                     string=u'Facturada',
                                     fnct_search=_ff_facturada_search,
                                     readonly=True,
                                     select=2),
    }

    _defaults = {
        'units': lambda *a: 1,
    }

    _constraints = [(_check_units,
                     _(u'Error: Les unitats han de ser superiors a 0.'),
                     ['units'])]

GiscedataOt()


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def action_move_create(self, cr, uid, ids, *args):
        imd_obj = self.pool.get('ir.model.data')
        journal_ot = imd_obj.get_object_reference(
            cr, uid, 'giscedata_ot_facturacio', 'journal_ot'
        )[1]
        for inv in self.read(cr, uid, ids, ['journal_id']):
            if inv['journal_id'][0] == journal_ot:
                raise osv.except_osv(
                    _(u'Atenció!'),
                    _(
                        u"La factura (id: {0}) està al diari de Ordres de "
                        u"Treball.\nLes factures d'aquest diari no es poden "
                        u"obrir."
                    ).format(inv['id'])
                )
        return super(AccountInvoice, self).action_move_create(cr, uid, ids, args)

AccountInvoice()
