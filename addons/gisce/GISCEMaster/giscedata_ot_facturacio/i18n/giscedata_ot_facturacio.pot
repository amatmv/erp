# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 12:51\n"
"PO-Revision-Date: 2018-05-16 10:10+0000\n"
"Last-Translator: Jaume  Florez Valenzuela <jflorez@gisce.net>\n"
"Language-Team: Catalan (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:159
#, python-format
msgid "* S'ignoraran les ordres de treball que no estan a facturar.\n"
msgstr "* S'ignoraran les ordres de treball que no estan a facturar.\n"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:123
#, python-format
msgid "L'Empresa Encarregada no té cap compte assignat per fer-hi pagaments."
msgstr "L'Empresa Encarregada no té cap compte assignat per fer-hi pagaments."

#. module: giscedata_ot_facturacio
#: model:account.journal,name:giscedata_ot_facturacio.journal_ot
msgid "Factures de Ordres de Treball"
msgstr "Factures de Ordres de Treball"

#. module: giscedata_ot_facturacio
#: model:product.template,description:giscedata_ot_facturacio.product_ot_p2_product_template
msgid ""
"Alta de cliente para suministro trifásico donde previamente no existe "
"contador trifásico directo o existe y es necesaria su sustitución"
msgstr "Alta de cliente para suministro trifásico donde previamente no existe contador trifásico directo o existe y es necesaria su sustitución"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/giscedata_ot_facturacio.py:167
#, python-format
msgid "Atenció!"
msgstr "Atenció!"

#. module: giscedata_ot_facturacio
#: constraint:product.template:0
msgid ""
"Error: The default UOM and the purchase UOM must be in the same category."
msgstr "Error: The default UOM and the purchase UOM must be in the same category."

#. module: giscedata_ot_facturacio
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:376
#: help:wizard.generate.invoice.ot,replace_factures:0
#, python-format
msgid ""
"Si hi ha Ordres de Treball que ja tenen factures peró aquestes estan en "
"borrador, es substituïran per les noves creades. Altrament s'ignoraran."
msgstr "Si hi ha Ordres de Treball que ja tenen factures peró aquestes estan en borrador, es substituïran per les noves creades. Altrament s'ignoraran."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:115
#, python-format
msgid "La ordre de treball amb id {0} no té empresa encarregada."
msgstr "La ordre de treball amb id {0} no té empresa encarregada."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:64
#, python-format
msgid "L'Empresa Encarregada no té cap adreça assignada.\n"
msgstr "L'Empresa Encarregada no té cap adreça assignada.\n"

#. module: giscedata_ot_facturacio
#: model:product.template,description:giscedata_ot_facturacio.product_ot_p3_product_template
msgid ""
"Alta de cliente para suministro trifásico donde previamente no existe "
"contador trifásico indirecto o existente y es necesaria su sustitución"
msgstr "Alta de cliente para suministro trifásico donde previamente no existe contador trifásico indirecto o existente y es necesaria su sustitución"

#. module: giscedata_ot_facturacio
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: giscedata_ot_facturacio
#: model:ir.actions.act_window,name:giscedata_ot_facturacio.action_wizard_generate_invoice_ot
#: view:wizard.generate.invoice.ot:0
msgid "Generar Factures"
msgstr "Generar Factures"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/giscedata_ot_facturacio.py:168
#, python-format
msgid ""
"La factura (id: {0}) està al diari de Ordres de Treball.\n"
"Les factures d'aquest diari no es poden obrir."
msgstr "La factura (id: {0}) està al diari de Ordres de Treball.\nLes factures d'aquest diari no es poden obrir."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:369
#: field:wizard.generate.invoice.ot,contrata_id:0
#, python-format
msgid "Empresa Encarregada"
msgstr "Empresa Encarregada"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:74
#, python-format
msgid "OTs {0} - {1}"
msgstr "OTs {0} - {1}"

#. module: giscedata_ot_facturacio
#: model:ir.actions.act_window,name:giscedata_ot_facturacio.action_invoice_ot_tree
#: model:ir.ui.menu,name:giscedata_ot_facturacio.menu_action_invoice_ot_tree
msgid "Factures OT"
msgstr "Factures OT"

#. module: giscedata_ot_facturacio
#: model:product.template,description:giscedata_ot_facturacio.product_ot_p1_product_template
msgid ""
"Alta de cliente para suministro monofásico donde previamente no existe "
"contador o existe y es necesaria su sustitución"
msgstr "Alta de cliente para suministro monofásico donde previamente no existe contador o existe y es necesaria su sustitución"

#. module: giscedata_ot_facturacio
#: model:product.category,name:giscedata_ot_facturacio.categ_OT
msgid "OT"
msgstr "OT"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot.tancament,product_id:0
msgid "Producte"
msgstr "Producte"

#. module: giscedata_ot_facturacio
#: view:wizard.generate.invoice.ot:0
msgid "Obrir Factures"
msgstr "Obrir Factures"

#. module: giscedata_ot_facturacio
#: view:wizard.generate.invoice.ot:0
msgid "Generar factures entre:"
msgstr "Generar factures entre:"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:371
#: field:wizard.generate.invoice.ot,data_min:0
#, python-format
msgid "Data Inici"
msgstr "Data Inici"

#. module: giscedata_ot_facturacio
#: field:wizard.generate.invoice.ot,check_contratas:0
msgid "unknown"
msgstr "unknown"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:372
#: field:wizard.generate.invoice.ot,data_max:0
#, python-format
msgid "Data Final"
msgstr "Data Final"

#. module: giscedata_ot_facturacio
#: view:wizard.generate.invoice.ot:0
msgid "-"
msgstr "-"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:180
#, python-format
msgid ""
"Warning. S'ha seleccionat Ordres de Treball que no s'han de facturar:\n"
"{0}\n"
msgstr "Warning. S'ha seleccionat Ordres de Treball que no s'han de facturar:\n{0}\n"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:175
#, python-format
msgid ""
"Error\n"
"No s'ha seleccionat cap Ordre de Treball a facturar.\n"
msgstr "Error\nNo s'ha seleccionat cap Ordre de Treball a facturar.\n"

#. module: giscedata_ot_facturacio
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/giscedata_ot_facturacio.py:148
#, python-format
msgid "Error: Les unitats han de ser superiors a 0."
msgstr "Error: Les unitats han de ser superiors a 0."

#. module: giscedata_ot_facturacio
#: model:ir.module.module,description:giscedata_ot_facturacio.module_meta_information
msgid "Facturacio de les Ordres de Treball."
msgstr "Facturacio de les Ordres de Treball."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:50
#, python-format
msgid "Factura Generada"
msgstr "Factura Generada"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:32
#, python-format
msgid "Factura creada correctament"
msgstr "Factura creada correctament"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:275
#, python-format
msgid ""
"*Amb Factura: {0}\n"
"*Sense factura: {1}\n"
msgstr "*Amb Factura: {0}\n*Sense factura: {1}\n"

#. module: giscedata_ot_facturacio
#: view:wizard.generate.invoice.ot:0 field:wizard.generate.invoice.ot,info:0
msgid "Info"
msgstr "Info"

#. module: giscedata_ot_facturacio
#: model:product.template,name:giscedata_ot_facturacio.product_ot_p2_product_template
msgid "p2"
msgstr "p2"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:165
#, python-format
msgid ""
"* S'ignoraran les ordres de treball que ja tenen factures en borrador.\n"
"\n"
msgstr "* S'ignoraran les ordres de treball que ja tenen factures en borrador.\n\n"

#. module: giscedata_ot_facturacio
#: model:product.category,name:giscedata_ot_facturacio.categ_OT_2
msgid "Contrato 2"
msgstr "Contrato 2"

#. module: giscedata_ot_facturacio
#: constraint:product.template:0
msgid "Error: UOS must be in a different category than the UOM"
msgstr "Error: UOS must be in a different category than the UOM"

#. module: giscedata_ot_facturacio
#: model:product.category,name:giscedata_ot_facturacio.categ_OT_1
msgid "Contrato 1"
msgstr "Contrato 1"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot.tancament,seller_ids:0
msgid "Proveïdors"
msgstr "Proveïdors"

#. module: giscedata_ot_facturacio
#: constraint:product.pricelist.version:0
msgid "You cannot have 2 pricelist versions that overlap!"
msgstr "You cannot have 2 pricelist versions that overlap!"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot,a_facturar:0
msgid "A Facturar"
msgstr "A Facturar"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:184
#, python-format
msgid ""
"Warning. S'ha seleccionat Ordres de Treball per facturar ja que tenen factures en borrador:\n"
"{0}"
msgstr "Warning. S'ha seleccionat Ordres de Treball per facturar ja que tenen factures en borrador:\n{0}"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:66
#, python-format
msgid "L'Empresa Encarregada no té cap compte assignat per fer-hi pagaments.\n"
msgstr "L'Empresa Encarregada no té cap compte assignat per fer-hi pagaments.\n"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot,units:0
msgid "Unitats"
msgstr "Unitats"

#. module: giscedata_ot_facturacio
#: model:product.template,name:giscedata_ot_facturacio.product_ot_p3_product_template
msgid "p3"
msgstr "p3"

#. module: giscedata_ot_facturacio
#: model:product.template,name:giscedata_ot_facturacio.product_ot_p1_product_template
msgid "p1"
msgstr "p1"

#. module: giscedata_ot_facturacio
#: view:giscedata.ot:0
msgid "Facturació"
msgstr "Facturació"

#. module: giscedata_ot_facturacio
#: constraint:product.pricelist.item:0
msgid ""
"Error ! You cannot assign the Main Pricelist as Other Pricelist in PriceList"
" Item!"
msgstr "Error ! You cannot assign the Main Pricelist as Other Pricelist in PriceList Item!"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot.tancament,product_description:0
msgid "Desc."
msgstr "Desc."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:366
#: field:wizard.generate.invoice.ot,total_treballs:0
#, python-format
msgid "Total treballs"
msgstr "Total treballs"

#. module: giscedata_ot_facturacio
#: model:product.template,description:giscedata_ot_facturacio.product_ot_p4_product_template
msgid "Alta de cliente donde previamente existe contador y éste es válido"
msgstr "Alta de cliente donde previamente existe contador y éste es válido"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:162
#, python-format
msgid ""
"* Es substituiran les factures en borrador de les ordres seleccionades per la nova factura.\n"
"\n"
msgstr "* Es substituiran les factures en borrador de les ordres seleccionades per la nova factura.\n\n"

#. module: giscedata_ot_facturacio
#: model:ir.module.module,shortdesc:giscedata_ot_facturacio.module_meta_information
msgid "GISCE Facturacio Ordres de Treball"
msgstr "GISCE Facturacio Ordres de Treball"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:101
#, python-format
msgid "La ordre de treball amb id {0} no té codi de tancament."
msgstr "La ordre de treball amb id {0} no té codi de tancament."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:375
#: field:wizard.generate.invoice.ot,replace_factures:0
#, python-format
msgid "Forçar Factures"
msgstr "Forçar Factures"

#. module: giscedata_ot_facturacio
#: model:product.template,name:giscedata_ot_facturacio.product_ot_p4_product_template
msgid "p4"
msgstr "p4"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:367
#: field:wizard.generate.invoice.ot,total_per_facturar:0
#, python-format
msgid "Total per facturar"
msgstr "Total per facturar"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:109
#, python-format
msgid "El codi de tancament {0} no té cap producte assignat."
msgstr "El codi de tancament {0} no té cap producte assignat."

#. module: giscedata_ot_facturacio
#: constraint:product.category:0
msgid "Error ! You can not create recursive categories."
msgstr "Error ! You can not create recursive categories."

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:373
#: field:wizard.generate.invoice.ot,data_factura:0
#, python-format
msgid "Data Factura"
msgstr "Data Factura"

#. module: giscedata_ot_facturacio
#: field:wizard.generate.invoice.ot,state:0
msgid "State"
msgstr "State"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:248
#, python-format
msgid ""
"*A Facturar: {0}\n"
"*No Facturar: {1}\n"
msgstr "*A Facturar: {0}\n*No Facturar: {1}\n"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot,factura_id:0
#: field:wizard.generate.invoice.ot,factura_id:0
msgid "Factura"
msgstr "Factura"

#. module: giscedata_ot_facturacio
#: view:wizard.generate.invoice.ot:0
msgid "Generar"
msgstr "Generar"

#. module: giscedata_ot_facturacio
#: constraint:product.product:0
msgid "Error: Invalid ean code"
msgstr "Error: Invalid ean code"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:171
#, python-format
msgid ""
"Error\n"
"Ordres de Treball amb diferents Empreses:\n"
"{0}\n"
msgstr "Error\nOrdres de Treball amb diferents Empreses:\n{0}\n"

#. module: giscedata_ot_facturacio
#: model:product.pricelist.version,name:giscedata_ot_facturacio.tarif_ot_test
msgid "OT - Test"
msgstr "OT - Test"

#. module: giscedata_ot_facturacio
#: view:giscedata.ot:0
msgid "Ordre de Treball"
msgstr "Ordre de Treball"

#. module: giscedata_ot_facturacio
#: view:wizard.generate.invoice.ot:0
msgid "Cancel"
msgstr "Cancel"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:133
#, python-format
msgid "OT {0}: {1}"
msgstr "OT {0}: {1}"

#. module: giscedata_ot_facturacio
#: model:ir.model,name:giscedata_ot_facturacio.model_wizard_generate_invoice_ot
msgid "wizard.generate.invoice.ot"
msgstr "wizard.generate.invoice.ot"

#. module: giscedata_ot_facturacio
#: code:addons/giscedata_ot_facturacio/wizard/wizard_generate_invoice.py:75
#, python-format
msgid "Factura de Ordres de Treball"
msgstr "Factura de Ordres de Treball"

#. module: giscedata_ot_facturacio
#: field:giscedata.ot,facturada:0
msgid "Facturada"
msgstr "Facturada"

#. module: giscedata_ot_facturacio
#: model:product.pricelist,name:giscedata_ot_facturacio.tarif_ot
msgid "Tarifas OT"
msgstr "Tarifas OT"
