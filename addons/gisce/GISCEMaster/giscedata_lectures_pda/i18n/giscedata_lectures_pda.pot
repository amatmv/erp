# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# * giscedata_lectures_pda
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-08-12 13:45+0000\n"
"PO-Revision-Date: 2019-08-12 13:45+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: \n"
"Generated-By: Babel 2.7.0\n"

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,check_polisses:0
msgid "unknown"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,zona:0
#: field:wizard.informe.ruta.lectura,zona:0
#: field:wizard.reordenar.zona,zona_ant:0
msgid "Zona"
msgstr ""

#. module: giscedata_lectures_pda
#: code:addons/giscedata_lectures_pda/giscedata_lectures.py:27
#, python-format
msgid ""
"Els següents comptadors ja estan carregats a la TPL:\n"
" %s"
msgstr ""

#. module: giscedata_lectures_pda
#: selection:wizard.reordenar.zona,state:0
msgid "End"
msgstr ""

#. module: giscedata_lectures_pda
#: selection:wizard.informe.ruta.lectura,informe:0
msgid "Observacions"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
msgid "Cerrar"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "- Manualment emplenar zona i ordre"
msgstr ""

#. module: giscedata_lectures_pda
#: selection:giscedata.lectures.pda.export,state:0
#: selection:wizard.reordenar.zona,state:0
msgid "Init"
msgstr ""

#. module: giscedata_lectures_pda
#: help:wizard.reordenar.zona,zona_nova:0
msgid ""
"En blanc per no més llevar de la zona actual\n"
"sense incloure el cups en una altra zona"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.ui.menu,name:giscedata_lectures_pda.menu_lectures_pda_export
msgid "Exportación fichero rutas"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.act_window,name:giscedata_lectures_pda.action_wizard_informe_ruta_lectura
#: view:wizard.informe.ruta.lectura:0
msgid "Dades"
msgstr ""

#. module: giscedata_lectures_pda
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.reordenar.zona,despres_de:0
msgid "Despŕes de"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.reordenar.zona,cups_id:0
msgid "CUPS"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.ui.menu,name:giscedata_lectures_pda.menu_obrir_cups_zona_tree
msgid "Consulta de zones"
msgstr ""

#. module: giscedata_lectures_pda
#: selection:wizard.informe.ruta.lectura,informe:0
msgid "Complet"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,name:0
msgid "Nom"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.report.xml,name:giscedata_lectures_pda.report_wizard_informe_ruta_lectura_resum
msgid "Informe resumen"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "Dades actuals"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "Endavant"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.informe.ruta.lectura,informe:0
msgid "Informe"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "Opcions:"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "Proposar"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.model,name:giscedata_lectures_pda.model_giscedata_lectures_pda_export
msgid "giscedata.lectures.pda.export"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,check_ignore:0
msgid "Ignorar comprovacions"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.model,name:giscedata_lectures_pda.model_wizard_informe_ruta_lectura
msgid "wizard.informe.ruta.lectura"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
#: view:wizard.informe.ruta.lectura:0
#: view:wizard.reordenar.zona:0
msgid "Cancel·lar"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.model,name:giscedata_lectures_pda.model_wizard_reordenar_zona
msgid "wizard.reordenar.zona"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,exportar_tall:0
msgid "Exportar comptadors en estat tall"
msgstr ""

#. module: giscedata_lectures_pda
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.comptador,conf_telelectura:0
msgid "Conf. Telelectura"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.act_window,name:giscedata_lectures_pda.action_wizard_lectures_pda_export
#: view:giscedata.lectures.pda.export:0
msgid "Exportar zona"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.comptador:0
msgid "Aquest comptador està carregat a una TPL en procés de lectura"
msgstr ""

#. module: giscedata_lectures_pda
#: help:giscedata.lectures.comptador,avis_comptador:0
msgid "Aquest avís es mostrarà en la TPL en el moment de pendre la lectura"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
msgid "Pòlisses"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
msgid "Otra zona"
msgstr ""

#. module: giscedata_lectures_pda
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "- Assignar després d'un CUPS existent"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.module.module,description:giscedata_lectures_pda.module_meta_information
msgid ""
"\n"
"Mòdul per exportar les lectures i treballar amb el programa de la PDA.\n"
"  * Exporta un fitxer per tal de carregar-lo a la PDA i poder fer les "
"lectures.\n"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,file:0
msgid "Fichero"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.informe.ruta.lectura,data_inici:0
msgid "Des de"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,check_polisses_ids:0
msgid "Pòlisses ids"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.reordenar.zona,zona_nova:0
msgid "Zona nova"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.informe.ruta.lectura:0
msgid "Imprimir"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.module.module,shortdesc:giscedata_lectures_pda.module_meta_information
msgid "Exportació de Lectures per PDA"
msgstr ""

#. module: giscedata_lectures_pda
#: code:addons/giscedata_lectures_pda/wizard/export.py:65
#, python-format
msgid ""
"Hi ha %s pòlisses que no tenen lot de facturació assignat. Si no se'ls hi"
" assigna, no estaran al fitxer de ruta."
msgstr ""

#. module: giscedata_lectures_pda
#: selection:giscedata.lectures.pda.export,state:0
msgid "END"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,state:0
#: field:wizard.reordenar.zona,state:0
msgid "Estat"
msgstr ""

#. module: giscedata_lectures_pda
#: code:addons/giscedata_lectures_pda/giscedata_lectures.py:26
msgid "Error"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.cups.ps:0
msgid "Zones de lectura"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,dia:0
msgid "Dia de lectura"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.pda.export,status:0
msgid "Message"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.wizard,name:giscedata_lectures_pda.wizard_lectures_pda_import
msgid "Importación lecturas TPL"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "Dades noves"
msgstr ""

#. module: giscedata_lectures_pda
#: code:addons/giscedata_lectures_pda/wizard/export.py:270
#, python-format
msgid "Pòlisses sense lot en %s.ruta"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.comptador,avis_comptador:0
msgid "Avís pel lecturer"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.ui.menu,name:giscedata_lectures_pda.menu_base_TPL
#: view:giscedata.lectures.comptador:0
msgid "TPL"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.ui.menu,name:giscedata_lectures_pda.menu_lectures_pda_import
msgid "Importación de lecturas"
msgstr ""

#. module: giscedata_lectures_pda
#: selection:wizard.informe.ruta.lectura,informe:0
msgid "Resum"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
msgid "Obrir pòlisses"
msgstr ""

#. module: giscedata_lectures_pda
#: field:giscedata.lectures.comptador,in_tpl:0
msgid "Carregat a la TPL"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.reordenar.zona,ordre_ant:0
msgid "Ordre"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
msgid ""
"El dia se utilizará para escoger el contador vigente en caso que una "
"póliza tenga mas de un contador"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "- Obtenir zona i ordre segons el carrer i número"
msgstr ""

#. module: giscedata_lectures_pda
#: view:wizard.reordenar.zona:0
msgid "Reordenar"
msgstr ""

#. module: giscedata_lectures_pda
#: view:giscedata.lectures.pda.export:0
msgid "Generar"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.informe.ruta.lectura,data_final:0
msgid "Fins"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.act_window,name:giscedata_lectures_pda.action_wizard_reordenar_zona
#: view:wizard.reordenar.zona:0
msgid "Recolocar cups"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.report.xml,name:giscedata_lectures_pda.report_wizard_informe_ruta_lectura
#: model:ir.ui.menu,name:giscedata_lectures_pda.menu_wizard_soller_ruta_lecturas
msgid "Informe ruta"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.act_window,name:giscedata_lectures_pda.action_cups_zona_tree
msgid "Cups"
msgstr ""

#. module: giscedata_lectures_pda
#: selection:giscedata.lectures.pda.export,state:0
msgid "Check"
msgstr ""

#. module: giscedata_lectures_pda
#: model:ir.actions.report.xml,name:giscedata_lectures_pda.report_wizard_informe_ruta_lectura_observacions
msgid "Informe observacions"
msgstr ""

#. module: giscedata_lectures_pda
#: field:wizard.reordenar.zona,ordre_nou:0
msgid "Ordre nou"
msgstr ""

