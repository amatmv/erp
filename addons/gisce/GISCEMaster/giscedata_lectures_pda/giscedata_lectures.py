# -*- coding: utf-8 -*-
from osv import osv, fields
from osv.expression import OOQuery
from tools.translate import _


class GiscedataLecturesComptador(osv.osv):
    """Afegim camps pel treball amb la PDA.
    """
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def unlink(self, cursor, uid, ids, context=None):
        comptador_query = self.q(cursor, uid).select(['name']).where([
            ('in_tpl', '=', True),
            ('id', 'in', ids)
        ])
        cursor.execute(*comptador_query)
        loaded_meters_tpl = cursor.dictfetchall()

        if loaded_meters_tpl:
            comptadors_str = "\n".join([
                "\t{}".format(meter['name']) for meter in loaded_meters_tpl
            ])
            raise osv.except_osv(
                _(u"Error"),
                _(u"Els següents comptadors ja estan carregats a la TPL:\n %s")
                % comptadors_str
            )
        res = super(GiscedataLecturesComptador, self).unlink(
            cursor, uid, ids, context=context
        )
        return res

    _columns = {
        'avis_comptador': fields.char('Avís pel lecturer', size=256,
                                      help=u"Aquest avís es mostrarà en la "
                                           u"TPL en el moment de pendre la "
                                           u"lectura"),
        'in_tpl': fields.boolean('Carregat a la TPL', required=False),
        'conf_telelectura': fields.char('Conf. Telelectura', size=255),
    }

    _defaults = {
        'in_tpl': lambda *a: False,
        'conf_telelectura': lambda *a: 'com1:9600:8:None:1:1:1:2:2000:1I',
    }

GiscedataLecturesComptador()
