# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestExport(testing.OOTestCase):
    def test_method_get_cups_from_zone_sorted_correctly(self):
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        wiz_obj = self.openerp.pool.get('giscedata.lectures.pda.export')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            cups_ids = cups_obj.search(cursor, uid, [])
            index = 1
            for cups_id in cups_ids:
                cups_obj.write(cursor, uid, cups_id, {'ordre': index})
                index += 1
            query = """SELECT id FROM giscedata_cups_ps ORDER BY ordre"""
            cursor.execute(query)
            expected_cups = [idd[0] for idd in cursor.fetchall()]
            cups_obj.write(cursor, uid, expected_cups, {'zona': 'test zone'})
            func_exe = wiz_obj.get_cups_ids(cursor, uid, None, 'test zone')
            self.assertEquals(expected_cups, func_exe)
