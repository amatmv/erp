# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction

from addons import get_module_resource
from giscedata_lectures_pda.wizard.import_file import _import_data
from osv import osv


class LoadTPLTest(testing.OOTestCase):

    def setUp(self):
        lectura_file_noi_path = get_module_resource(
            'giscedata_lectures_pda', 'tests', 'data',
            'ruta_noi.lectura'
        )
        with open(lectura_file_noi_path, 'r') as lnoi_file:
            self.lectura_noi_file = base64.encodestring(lnoi_file.read())

        lectura_file_inc_path = get_module_resource(
            'giscedata_lectures_pda', 'tests', 'data',
            'ruta_inc.lectura'
        )
        with open(lectura_file_inc_path, 'r') as linc_file:
            self.lectura_inc_file = base64.encodestring(linc_file.read())

    def get_origin_ids(self, cursor, uid):
        '''
        Gets origin id from code
        :param origin_code: origin code
        :return: {'TPL': id, 'Sin Lectura': id}
        '''
        origin_obj = self.openerp.pool.get('giscedata.lectures.origen')

        origins = {}

        origins['TPL'] = origin_obj.search(
            cursor, uid, [('codi', '=', '20')]
        )[0]

        origins['Sin Lectura'] = origin_obj.search(
                cursor, uid, [('codi', '=', '99')]
        )[0]

        return origins

    def get_meters(self, cursor, uid):
        """
        Get meters ids
        :return: {'comptador_0001': id, 'comptador_0002': id}
        """
        imd_obj = self.openerp.pool.get('ir.model.data')

        meters = {}

        for meter_name in ['comptador_0001', 'comptador_0002']:
            # gets meters
            meters[meter_name] = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', meter_name
            )[1]
        return meters

    def load_tpl_file(self, cursor, uid, lecturafile, prefix):
        """
        :param lecturafile: File descriptor to load
        :param prefix: Prefix of "Sin lectura" incidence
        :return:
        """
        cfg_obj = self.openerp.pool.get('res.config')

        cfg_obj.set(cursor, uid, 'pda_no_lect_origin_incidence_prefix', prefix)

        wiz_data = {
            'form': {
                'data': 'real',
                'file': lecturafile
            }
        }

        _import_data(None, cursor, uid, wiz_data, context={})

    def get_incidencies_dict(self, cursor, uid):
        """
        Returns incidencies dict
        :return: {'A100': id, 'I910': id}
        """
        incidencies_obj = self.openerp.pool.get(
            'giscedata.lectures.incidencies.incidencia'
        )

        incidencies_ids = incidencies_obj.search(
            cursor, uid, [('name', 'in', ['A100', 'I910'])]
        )
        incidencies = dict([
            (i['name'], i['id'])
            for i in incidencies_obj.read(
                cursor, uid, incidencies_ids, ['name']
            )
        ])

        return incidencies

    def get_loaded_measures(self, cursor, uid, meter_ids):
        """
        Gets loaded measures, always between '2016-10-01' and '2016-12-01'
        :param meter_ids: mater ids list
        :return: lect list as in OERP orm read function
        """
        lectures_obj = self.openerp.pool.get('giscedata.lectures.lectura')

        l_ids = lectures_obj.search(
            cursor, uid, [
                ('comptador', 'in', meter_ids),
                ('name', '>=', '2016-10-01'),
                ('name', '<=', '2016-12-01')
            ]
        )
        lects = lectures_obj.read(
            cursor, uid, l_ids, []
        )

        return lects

    def test_load_lectura_no_incidencies(self):
        """
        Loads a .lectura file
        Always TPL origin (No incidence in file)
        :return:
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.load_tpl_file(cursor, uid, self.lectura_noi_file, 'I')

            origins = self.get_origin_ids(cursor, uid)
            meter_ids = self.get_meters(cursor, uid).values()

            lects = self.get_loaded_measures(cursor, uid, meter_ids)
            for lect in lects:
                lect_origin = lect['origen_id'][0]
                lect_inc = lect['incidencia_id']
                # Always TPL
                assert lect_origin == origins['TPL']
                assert lect_inc is False

    def test_load_lectura_no_prefix(self):
        """
        Loads a .lectura file without prefix
        :return:
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.load_tpl_file(cursor, uid, self.lectura_inc_file, '')

            origins = self.get_origin_ids(cursor, uid)
            incidencies = self.get_incidencies_dict(cursor, uid)
            meter_ids = self.get_meters(cursor, uid).values()

            lects = self.get_loaded_measures(cursor, uid, meter_ids)
            for lect in lects:
                lect_origin = lect['origen_id'][0]
                lect_inc = lect['incidencia_id']
                # Always TPL
                if lect['name'] == '2016-10-01':
                    # TPL
                    assert lect_origin == origins['TPL']
                    assert lect_inc[0] == incidencies['A100']
                elif lect['name'] == '2016-11-01':
                    # No lect
                    assert lect_origin == origins['TPL']
                    assert lect_inc[0] == incidencies['I910']

    def test_load_lectura_incidencies_prefix_I(self):
        """
        Loads a .lectura file when prefix 'I'
        :return:
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.load_tpl_file(cursor, uid, self.lectura_inc_file, 'I')

            origins = self.get_origin_ids(cursor, uid)
            incidencies = self.get_incidencies_dict(cursor, uid)
            meter_ids = self.get_meters(cursor, uid).values()

            lects = self.get_loaded_measures(cursor, uid, meter_ids)
            for lect in lects:
                lect_origin = lect['origen_id'][0]
                lect_inc = lect['incidencia_id']
                if lect['name'] == '2016-10-01':
                    # TPL
                    assert lect_origin == origins['TPL']
                    assert lect_inc[0] == incidencies['A100']
                elif lect['name'] == '2016-11-01':
                    # No lect
                    assert lect_origin == origins['Sin Lectura']
                    assert lect_inc[0] == incidencies['I910']

    def test_load_lectura_incidencies_prefix_A(self):
        """
        Loads a .lectura file when prefix 'A'
        :return:
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.load_tpl_file(cursor, uid, self.lectura_inc_file, 'A')

            origins = self.get_origin_ids(cursor, uid)
            incidencies = self.get_incidencies_dict(cursor, uid)
            meter_ids = self.get_meters(cursor, uid).values()

            lects = self.get_loaded_measures(cursor, uid, meter_ids)
            for lect in lects:
                lect_origin = lect['origen_id'][0]
                lect_inc = lect['incidencia_id']
                if lect['name'] == '2016-10-01':
                    # Sin lectura
                    assert lect_origin == origins['Sin Lectura']
                    assert lect_inc[0] == incidencies['A100']
                elif lect['name'] == '2016-11-01':
                    # TPL
                    assert lect_origin == origins['TPL']
                    assert lect_inc[0] == incidencies['I910']

    def test_unlink_tpl(self):
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        lecture_obj = self.openerp.pool.get('giscedata.lectures.lectura')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            meters_ids = self.get_meters(cursor, uid)
            ids = meters_ids.values()
            meters = meter_obj.read(cursor, uid, ids, ['name', 'lectures'])
            id_meter_0001 = meters_ids['comptador_0001']

            meters_names = []
            lectures_ids = []
            meter_name_by_id = {}
            for meter in meters:
                meters_names.append(meter['name'])
                lectures_ids += meter['lectures']
                meter_name_by_id[meter['id']] = meter['name']

            lectures_ids = list(set(lectures_ids))

            meter_obj.write(cursor, uid, id_meter_0001, {'in_tpl': True})

            def unlink_meters():
                meter_obj.unlink(cursor, uid, ids)

            # if some meter couldn't be removed, then it should have thrown an
            # exception
            error = ".*Els següents comptadors ja estan carregats a la " \
                    "TPL:.*\n.*{}".format(meter_name_by_id[id_meter_0001])
            with self.assertRaisesRegexp(osv.except_osv, error):
                unlink_meters()

            # if some meter couldn't be removed, then all meters selected to be
            # removed must exist
            meters_ids = meter_obj.search(
                cursor, uid, [('name', 'in', meters_names)]
            )
            self.assertEqual(len(meters_ids), len(meters_names))

            # set meter values so it can be removed
            lecture_obj.unlink(cursor, uid, lectures_ids)
            meter_obj.write(
                cursor, uid, id_meter_0001, {'in_tpl': False}
            )
            unlink_meters()
            meters_ids = meter_obj.search(
                cursor, uid, [('name', 'in', meters_names)]
            )
            self.assertEqual(len(meters_ids), 0)
