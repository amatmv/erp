# -*- coding: utf-8 -*-

import wizard
import pooler
import base64
import csv
import StringIO
import time


def _init(self, cr, uid, data, context={}):
    return {
      'file': False,
    }


def _triar_data(self, cr, uid, data, context=None):
    """Triem quina data del fitxer de la TPL fem servir.
    """
    if not context:
        context = {}
    return [('planificada', 'Planificada (fitxer ruta)'),
            ('real', 'Real TPL')]

_select_file_form = """<?xml version="1.0"?>
<form string="Seleccionar fichero" col="2">
  <field name="data" colspan="4" required="True"/>
  <field name="file" colspan="4" required="True"/>
</form>"""

_select_file_fields = {
  'data': {'string': 'Data', 'type': 'selection', 'selection': _triar_data},
  'file': {'string': 'Fichero', 'type': 'binary'},
}


def _import_data(self, cr, uid, data, context={}):
    # Creem un diccionari amb totes les anomalies possibles per no haver de fer consultes
    anomalia_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.incidencies.incidencia')
    anomalies = {}
    for a in anomalia_obj.read(cr, uid, anomalia_obj.search(cr, uid, []), ['id', 'name']):
        anomalies[a['name']] = a['id']

    energia_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.lectura')
    potencia_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.potencia')
    comptador_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.comptador')

    origen_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.origen')
    # Origen TPL
    origen_tpl = origen_obj.search(cr, uid, [('codi', '=', '20')])
    if origen_tpl:
        origen_tpl_id = origen_tpl[0]
    # Origen Sin Lectura
    origen_nolect = origen_obj.search(cr, uid, [('codi', '=', '99')])
    if origen_nolect:
        origen_nolect_id = origen_nolect[0]

    cfg_obj = pooler.get_pool(cr.dbname).get('res.config')

    # Sin Lectura prefix
    inc_prefix = cfg_obj.get(
        cr, uid, 'pda_no_lect_origin_incidence_prefix'
    ) or False

    output = StringIO.StringIO(base64.b64decode(data['form']['file']))
    reader = csv.reader(output, delimiter='\t')
    vals = {}
    linies_procesades = 0
    e_updated = []
    e_not_updated = []
    p_updated = []
    p_not_updated = []
    init = time.strftime('%Y-%m-%d %H:%M:%S')

    if data['form']['data'] == 'real':
        data_col = 2
    elif data['form']['data'] == 'planificada':
        data_col = 1

    for row in reader:
        # row[0] es id_comptador
        vals[row[0]] = {}
        vals[row[0]]['energia'] = {}
        vals[row[0]]['potencia'] = {}
        linies_procesades += 1
        # row[1] es data_lectura, YYYYMMDDhhmmss
        data = '%s-%s-%s' % (row[data_col][0:4], row[data_col][4:6],
                             row[data_col][6:8])
        # row[2] es data_registre
        # row[3] es lectura_electronica
        # row[4] es codi_anomalia
        # row[5] es etiqueta_demanada
        cb_nou = row[5]
        # row[6] es observacions
        observacions = row[6]
        # les lectures comencen a row[7], quatre camps per cada lectura
        # row[n+0] es id_periode
        # row[n+1] es nom_periode
        # row[n+2] es lectura_anterior
        # row[n+3] es lectura_actual
        # tal com s'exporten, els nom_periode comencen per P, MAX o EXC
        # row[7] és la configuració (ja preparem el diccionari vals per escriure la configuració al camp)
        vals[row[0]]['conf'] = row[7]
        # Selects origin
        if row[4] and inc_prefix and row[4].startswith(inc_prefix):
            origen_id = origen_nolect_id
        else:
            origen_id = origen_tpl_id
        for i in range(8, len(row)):
            if not row[i]:
                continue
            if row[i].startswith('P'):  # Lectura d'energia
                if not vals[row[0]]['energia'].has_key(row[i]):
                    vals[row[0]]['energia'][row[i]] = {}

                vals[row[0]]['energia'][row[i]] = {
                  'periode': row[i - 1],
                  'name': data,
                  'lectura': row[i + 2],
                  'comptador': row[0],
                  'tipus': row[i][-1],
                  'incidencia_id': anomalies.get(row[4], False),
                  'observacions': observacions,
                  'origen_id': origen_id,
                }
            elif row[i].startswith('MAX'):  # Lectura potencia
                if not vals[row[0]]['potencia'].has_key(row[i]):
                    vals[row[0]]['potencia'][row[i]] = {}

                vals[row[0]]['potencia'][row[i]] = {
                  'periode': row[i - 1],
                  'name': data,
                  'lectura': row[i + 2],
                  'comptador': row[0],
                  'incidencia_id': anomalies.get(row[4], False),
                  'observacions': observacions,
                  'origen_id': origen_id,
                }
            elif row[i].startswith('EXC'):  # Lectura excesos
                vals[row[0]]['potencia']['MAX%s' % row[i][-1]]['exces'] = row[i + 2]

    # Processem el diccionari de lectures
    for c in vals.keys():
        # Actualitzem la config del comptador
        cid = int(c)
        # Comprovem si aquest comptador és el correcte
        comptador = comptador_obj.browse(cr, uid, cid)
        if not comptador.active and comptador.data_baixa < data:
            # El comptador aquest ja no és vàlid hem de buscar-lo per número
            # de sèrie.
            search_params = [('name', '=', comptador.name),
                             ('data_alta', '>=', comptador.data_baixa)]
            cids = comptador_obj.search(cr, uid, search_params)
            if cids and len(cids) == 1:
                cid = cids[0]
                comptador_nou = comptador_obj.browse(cr, uid, cid)
                #Si al trobar el comptador, esta assignat
                #a una polissa amb diferent tarifa (canvis de potencia)
                #no el podem actualitzar
                if comptador.polissa.tarifa != comptador_nou.polissa.tarifa:
                    if comptador.id not in e_not_updated:
                        e_not_updated.append(comptador.id)
                    continue
                else:
                    comptador = comptador_nou
        #Actualitzem la configuracio de telelectura
        #i desmarquem el comptador com carregat a la tpl
        comptador_obj.write(cr, uid, [cid],
                            {'conf_telelectura': vals[c]['conf'],
                             'in_tpl': False,},
                            context={'sync': False})
        #Treiem la data de lectura pel comptador
        data_lectura = max([vals[c]['energia'][x]['name']\
                                for x in vals[c]['energia'].keys()])
        #Si es mes gran que la data de baixa, no introduim la lectura
        if not comptador.active and comptador.data_baixa < data_lectura:
            if cid not in e_not_updated:
                    e_not_updated.append(cid)
            continue

        for p in vals[c]['energia'].keys():
            v = vals[c]['energia'][p]
            v['comptador'] = cid
            if not comptador.active and\
               comptador.data_baixa < v['name']:
                if v['comptador'] not in e_not_updated:
                    e_not_updated.append(v['comptador'])
                continue
            search_params = [
              ('comptador.id', '=', v['comptador']),
              ('periode.id', '=', v['periode']),
              ('tipus', '=', v['tipus']),
              ('name', '>=', v['name']),
              ('name', '<=', v['name'])
            ]
            e_ids = energia_obj.search(cr, uid, search_params)
            if len(e_ids):
                if v['comptador'] not in e_not_updated:
                    e_not_updated.append(v['comptador'])
            else:
                energia_obj.create(cr, uid, v)
                if v['comptador'] not in e_updated:
                    e_updated.append(v['comptador'])

        for p in vals[c]['potencia'].keys():
            v = vals[c]['potencia'][p]
            v['comptador'] = cid
            search_params = [
              ('comptador.id', '=', v['comptador']),
              ('periode.id', '=', v['periode']),
              ('name', '>=', v['name']),
              ('name', '<=', v['name'])
            ]
            p_ids = potencia_obj.search(cr, uid, search_params)
            if len(p_ids):
                if v['comptador'] not in p_not_updated:
                    p_not_updated.append(v['comptador'])
            else:
                potencia_obj.create(cr, uid, v)
                if v['comptador'] not in p_updated:
                    p_updated.append(v['comptador'])
    comptadors_not_updated_ener = [c['name'] for c in comptador_obj.read(cr, uid, e_not_updated, ['name'])]
    comptadors_not_updated_pot = [c['name'] for c in comptador_obj.read(cr, uid, p_not_updated, ['name'])]

    output.close()
    end = time.strftime('%Y-%m-%d %H:%M:%S')
    return {
      'n_e_updated': len(e_updated),
      'n_e_not_updated': len(e_not_updated),
      'n_p_updated': len(p_updated),
      'n_p_not_updated': len(p_not_updated),
      'comptadors_not_updated_e': ', '.join(comptadors_not_updated_ener),
      'comptadors_not_updated_p': ', '.join(comptadors_not_updated_pot),
      'lp': linies_procesades,
      'init': init,
      'end': end,
    }

_import_data_form = """<?xml version="1.0"?>
<form string="Resultado">
  <separator string="General" colspan="4"/>
  <field name="init" />
  <field name="end" />
  <field name="lp" />
  <separator string="Energía" colspan="4" />
  <field name="n_e_updated" string="# actualizados"/>
  <field name="n_e_not_updated" string="# no actualizados"/>
  <group colspan="4" string="Contadores no actualizados">
    <field name="comptadors_not_updated_e" colspan="4" nolabel="1" />
  </group>
  <separator string="Potencia" colspan="4" />
  <field name="n_p_updated" string="# actualizados"/>
  <field name="n_p_not_updated" string="# no actualizados"/>
  <group colspan="4" string="Contadores no actualizados">
    <field name="comptadors_not_updated_p" colspan="4" nolabel="1" />
  </group>
</form>
"""

_import_data_fields = {
  'n_e_updated': {'string': '# actualizados energía', 'type': 'integer', 'readonly': True},
  'n_e_not_updated': {'string': '# no actualizados energía', 'type': 'integer', 'readonly': True},
  'n_p_updated': {'string': '# actualizados potencia', 'type': 'integer', 'readonly': True},
  'n_p_not_updated': {'string': '# no actualizados potencia', 'type': 'integer', 'readonly': True},
  'comptadors_not_updated_e': {'string': 'Contadores no actualizados energía', 'type': 'text', 'readonly': True},
  'comptadors_not_updated_p': {'string': 'Contadores no actualizados potencia', 'type': 'text', 'readonly': True},
  'lp': {'string': '# lineas procesadas', 'type': 'integer', 'readonly': True},
  'init': {'string': 'Inicio', 'type': 'datetime', 'readonly': True},
  'end': {'string': 'Final', 'type': 'datetime', 'readonly': True},
}


class wizard_lectures_pda_import(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
          'result': {'type': 'state', 'state': 'select_file'},
      },
      'select_file': {
          'actions': [],
          'result': {'type': 'form', 'arch': _select_file_form, 'fields': _select_file_fields, 'state': [('import_data', 'Importar')]},
      },
      'import_data': {
          'actions': [_import_data],
          'result': {'type': 'form', 'arch': _import_data_form, 'fields': _import_data_fields, 'state': [('init', 'Importar otro fichero'), ('end', 'Cerrar', 'gtk-close')]}
      },
      'end': {
          'actions': [],
          'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_lectures_pda_import('giscedata.lectures.pda.import')
