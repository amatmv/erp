# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time
import calendar


class WizardInformeRutaLectura(osv.osv_memory):

    """Wizard para treure diferents 
    informes de rutes de lectura"""

    _name = "wizard.informe.ruta.lectura"
    
    _get_informe = [('report_wizard_informe_ruta_lectura','Complet'),
                    ('report_wizard_informe_ruta_lectura_observacions','Observacions'),
                    ('report_wizard_informe_ruta_lectura_resum','Resum'),]
    
    def _get_zones(self, cursor, uid, context=None):

        cursor.execute("""select distinct zona as zona 
            from giscedata_cups_ps 
            where zona is not null order by zona""")
        zones = [(a['zona'], a['zona']) for a in cursor.dictfetchall()]
        return zones
    
    def _get_inici(self, cursor, uid, context=None):
        '''retorna el primer dia del mes actual'''
        
        return time.strftime('%Y-%m-01')
    
    def _get_final(self, cursor, uid, context=None):
        '''retorna el darrer dia del mes actual'''
        
        now = time.strftime('%Y-%m-%d')
        month_days = calendar.monthrange(int(now[:4]), int(now[5:7]))[1]
        
        return '%s-%s-%s'%(now[:4], now[5:7], month_days)

    def imprimir(self, cursor, uid, ids, context=None):
        
        wizard = self.browse(cursor, uid, ids[0])

        datas = {'form':{'data_inici':wizard.data_inici,
                         'data_final':wizard.data_final,
                         'zona':wizard.zona,
                        }
                }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': wizard.informe,
            'datas': datas,
        }

    _columns = {
        'data_inici': fields.date('Des de', required=True),
        'data_final': fields.date('Fins', required=True),
        'zona':fields.selection(_get_zones, 'Zona', required=True),
        'informe': fields.selection(_get_informe, 'Informe', required=True),
                
    }

    _defaults = {
        'data_inici': _get_inici,
        'data_final': _get_final,
    }

WizardInformeRutaLectura()
