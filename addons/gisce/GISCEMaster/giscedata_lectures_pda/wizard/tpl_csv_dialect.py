import csv

class TplCsvDialect(csv.Dialect):
    delimiter = '\t'
    skipinitialspace = False
    quotechar = '\b'
    doublequote = False
    escapechar = '\b'
    lineterminator = '\n'
    quoting = csv.QUOTE_NONE

csv.register_dialect('tpl', TplCsvDialect)
