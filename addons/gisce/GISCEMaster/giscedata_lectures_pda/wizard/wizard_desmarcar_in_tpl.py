# -*- coding: utf-8 -*-

from osv import osv, fields


class WizardDesmarcarInTpl(osv.osv_memory):
    _name = "wizard.desmarcar.in.tpl"

    def action_unmark(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        meter_ids = context.get('active_ids', [])
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_obj.write(cursor, uid, meter_ids, {'in_tpl': False})
        self.write(cursor, uid, ids, {'state': 'end'})

    def default_info(self, cursor, uid, context=None):
        selected_meters = []
        if not context:
            context = {}
        meter_ids = context.get('active_ids', [])
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        for meter_id in meter_ids:
            meter = meter_obj.read(cursor, uid, meter_id, ['name'])
            selected_meters.append(meter.get('name'))
        return "\n".join(selected_meters)

    _columns = {
        'info': fields.text('Comptadors seleccionats'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')])
    }

    _defaults = {
        'info': default_info,
        'state': lambda *a: 'init'
    }


WizardDesmarcarInTpl()
