# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import base64
import time
import csv
import StringIO
from tpl_csv_dialect import TplCsvDialect
from osv.expression import OOQuery


class GiscedataLecturesPdaExport(osv.osv_memory):

    _name = 'giscedata.lectures.pda.export'

    def _zones(self, cursor, uid, context=None):
        query = '''SELECT distinct zona as zona
                FROM giscedata_cups_ps
                WHERE zona is not null
                ORDER by zona'''
        cursor.execute(query)
        zones = [(a['zona'], a['zona']) for a in cursor.dictfetchall()]
        return zones

    def _get_comptador_actiu(self, cursor, uid, wizard,
                             polissa_id, dia, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        comptadors = polissa_obj.comptadors_actius(cursor, uid,
                                                   polissa_id, dia, dia)
        if not len(comptadors):
            comptador = False
        else:
            comptador = comptador_obj.browse(cursor, uid,
                                             comptadors[0])
        return comptador

    def action_export_data(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])

        polissa_obj = self.pool.get('giscedata.polissa')

        #Search cups in zona
        cups_ids = self.get_cups_ids(
            cursor, uid, ids, wizard.zona, context=context
        )

        # Comprovem que totes les pòlisses tenen lot de facturació
        search_params = [
            ('cups', 'in', cups_ids),
            ('lot_facturacio', '=', False),
        ]
        pol_ids = polissa_obj.search(cursor, uid, search_params)
        if pol_ids and not wizard.check_ignore:
            vals = {
                'check_polisses_ids': ','.join([str(x) for x in pol_ids]),
                'status': _(u"Hi ha %s pòlisses que no tenen lot de "
                            u"facturació assignat. Si no se'ls hi "
                            u"assigna, no estaran al fitxer de ruta."
                            % len(pol_ids)),
                'state': 'check'
            }
            wizard.write(vals)
            return True

        values = wizard.generate_ruta(cups_ids, context=context)
        values.update({
            'name': '%s.ruta' % wizard.zona,
            'state': 'end'
        })
        wizard.write(values)

    def generate_ruta(self, cursor, uid, ids, cups_ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context=context)
        cups_obj = self.pool.get('giscedata.cups.ps')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        dia = wizard.dia
        mes = dia.split('-')[1]
        contas = []

        output = StringIO.StringIO()
        writer = csv.writer(output, dialect='tpl')

        # de YYYY-MM-DD a YYYYMMDDhhmmss
        writer.writerow([u'%s000000' % wizard.dia.replace('-', '')])
        comptador_ids = []

        for cups in cups_obj.browse(cursor, uid, cups_ids, context):
            polissa = cups.polissa_polissa
            if not polissa:
                continue
            if (not wizard.exportar_tall and
                polissa.state == 'tall'):
                continue
            if (len(polissa.comptadors) and
                polissa.proxima_facturacio and
                polissa.proxima_facturacio[0:7] == dia[0:7]):
                # Mirem si aquesta polissa l'hem d'afegir al
                # fitxer segons el camp pròxima facturació
                comptador = self._get_comptador_actiu(cursor, uid,
                                                      wizard,
                                                      polissa.id,
                                                      dia,
                                                      context=context)
                if not comptador:
                    continue
                comptador_ids.append(comptador.id)
                address = []
                for ai in [cups.nv, cups.pnp, cups.es, cups.pt, cups.pu]:
                    if ai:
                        address.append(u'%s' % ai)
                contas = []
                poblacio = cups.id_poblacio and cups.id_poblacio.name or ''
                aclarador = cups.aclarador or ''
                linia_aclarador = '{} {}'.format(aclarador, poblacio)
                contas.extend([
                  cups.zona,                # zona
                  cups.ordre,               # ordre
                  polissa.name,             # id_polissa
                  comptador.id,             # id_comptador
                  comptador.name,           # nom_comptador
                  polissa.tarifa.id,        # id_tarifa
                  polissa.tarifa.name,      # nom_tarifa
                  polissa.potencia,         # potencia
                  polissa.titular.name,     # nom_abonat
                  ' '.join(address),        # adreca
                  linia_aclarador,  # poblacio
                  '',                                 # situacio_comptador
                  comptador.avis_comptador or '',     # avis_comptador
                  comptador.conf_telelectura or ''    # conf_port_i_credencials
                ])

                # Busquem activa y reactiva anterior
                lect_act = []
                lect_rec = []
                for periode in polissa.tarifa.periodes:
                    if periode.tipus == 'te':
                        #Activa
                        lect_ener = [
                          periode.id,
                          u'%sA' % (periode.name)
                        ]
                        search_params = [('comptador.id', '=', comptador.id),
                                         ('periode.id', '=', periode.id),
                                         ('tipus', '=', 'A')]
                        lect_ids = lectures_obj.search(cursor, uid,
                                                       search_params,
                                                       limit=1,
                                                       order="name desc")
                        if len(lect_ids):
                            lectura = lectures_obj.browse(cursor, uid,
                                                          lect_ids[0])
                            lect_ener.append(lectura.lectura)
                        else:
                            lect_ener.append('')
                        lect_act += lect_ener
                        #Reactiva
                        lect_ener = [
                          periode.id,
                          '%sR' % (periode.name)
                        ]
                        search_params = [('comptador.id', '=', comptador.id),
                                         ('periode.id', '=', periode.id),
                                         ('tipus', '=', 'R')]
                        lect_ids = lectures_obj.search(cursor, uid,
                                                       search_params,
                                                       limit=1,
                                                       order="name desc")
                        if len(lect_ids):
                            lectura = lectures_obj.browse(cursor, uid,
                                                          lect_ids[0])
                            lect_ener.append(lectura.lectura)
                        else:
                            lect_ener.append('')
                        lect_rec += lect_ener
                contas.extend(lect_act + lect_rec)
                # Busquem les potencies
                if (polissa.facturacio_potencia == 'max' or
                            polissa.tarifa.name[:2] in ('3.', '6.')):
                    for periode in polissa.tarifa.periodes:
                        if periode.tipus == 'tp':
                            lect_ener = [
                              periode.id,
                              u'MAX%s' % (periode.name[-1]),
                              u''
                            ]
                            contas.extend(lect_ener)

                    # Mirem si li toca Excesos (totes les 6.XA)
                    if polissa.tarifa.name[0] == '6':
                        for periode in polissa.tarifa.periodes:
                            if periode.tipus == 'tp':
                                lect_ener = [
                                  periode.id,
                                  u'EXC%s' % (periode.name[-1]),
                                  u''
                                ]
                                contas.extend(lect_ener)

                # No podem permetre tabuladors als camps,
                # ja que estem fent un CSV sense 'quotes'
                csv_row = []
                for element in contas:
                    element = u'%s' % element
                    csv_row.append(element.replace(u'\t', u' '))
                try:
                    # Ho afegim al CSV
                    csv_row = [s.encode('utf-8') for s in csv_row]
                    writer.writerow(csv_row)
                except:
                    print u"""Hay algún caracter mal codificado en los
                      campos alfanumericos de un contador:
                      zona: %s
                      orden: %s
                      póliza: %s
                      contador: %s""" % (contas[0], contas[1],
                                         contas[2], contas[4])
                    print (u"La línea completa tiene estos campos, "
                           u"entre los que se encuentran los cuatro "
                           u"que se acaban de indicar:")
                    for value in contas:
                        print '  - "%s"' % (value)
                    print u"La excepción que ha lanzado writer.writerow():"
                    raise
        #Marquem els comptadors com a exportats
        #comptador_obj.write(cursor, uid, comptador_ids,
        #                    {'in_tpl': True},
        #                    context={'sync': False})
        if comptador_ids:
            cursor.execute("UPDATE giscedata_lectures_comptador SET "
                           "in_tpl=True WHERE id in %s", (tuple(comptador_ids), ))

        tpl_file = base64.b64encode(output.getvalue())
        status = (u"Fichero de rutas (%s.ruta) listo "
                  u"con %i contadores" % (wizard.zona,
                                          len(comptador_ids)))
        if not tpl_file:
            status = u"""No hay contadores para la ruta:
                     %s en el periodo %s/%s""" % (wizard.zona,
                                                  mes, any)
        output.close()
        return {'file': tpl_file, 'status': status}


    def action_begin(self, cursor, uid, ids, context=None):

        return {
            'name': 'Seleccionar zona y mes',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscedata.lectures.pda.export',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
            }

    def action_open_polisses_tab(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        pol_ids = [a.id for a in wiz.check_polisses]
        return {
            'domain': "[('id','in', %s)]" % str(pol_ids),
            'name': _(u'Pòlisses sense lot en %s.ruta' % wiz.zona),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.polissa',
            'type': 'ir.actions.act_window'
        }

    def _fnct_check_polisses(self, cursor, uid, ids, field_name, args,
                             context=None):
        res = {}
        for wiz in self.browse(cursor, uid, ids):
            if wiz.check_polisses_ids:
                pol_ids = [int(x) for x in wiz.check_polisses_ids.split(',')]
            else:
                pol_ids = []
            res[wiz.id] = pol_ids
        return res

    def get_cups_ids(self, cursor, uid, ids, zona, context=None):
        if zona:
            cup_obj = self.pool.get('giscedata.cups.ps')
            search_params = [('zona', '=', zona)]
            q = OOQuery(cup_obj, cursor, uid)
            sql = q.select(['id'], order_by=('ordre.asc', )).where(search_params)
            cursor.execute(*sql)
            return [cups['id'] for cups in cursor.dictfetchall()]
        else:
            return []

    _columns = {
        'zona': fields.selection(_zones, 'Zona'),
        'dia': fields.date('Dia de lectura', required=True),
        'exportar_tall': fields.boolean('Exportar comptadors en estat tall'),
        'name': fields.char('Nom', size=60),
        'file': fields.binary('Fichero'),
        'status': fields.text('Message', readonly=True),
        'check_ignore': fields.boolean('Ignorar comprovacions'),
        'check_polisses': fields.function(_fnct_check_polisses,
                                          type='one2many',
                                          obj='giscedata.polissa',
                                          method=True),
        'check_polisses_ids': fields.text('Pòlisses ids', readonly=True),
        'state': fields.selection([('init', 'Init'),
                                   ('check', 'Check'),
                                   ('end', 'END')], 'Estat'),
    }

    _defaults = {
        'dia': lambda *a: time.strftime('%Y-%m-%d'),
        'exportar_tall': lambda *a: True,
        'state': lambda *a: 'init',
        'check_ignore': lambda *a: 0,
    }

GiscedataLecturesPdaExport()
