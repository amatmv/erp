# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields

class WizardReordenarZona(osv.osv_memory):
    ''' Wizard per introduir o llevar un cups d'una zona'''

    _name = 'wizard.reordenar.zona'

    def inserta_cups(self, cursor, uid, ids,
                     zona, ordre, cups_origen, context=None):
        '''funcio que inserta un cups en una zona'''

        cups_obj = self.pool.get("giscedata.cups.ps")
        cups = cups_obj.browse(cursor, uid, cups_origen)
        #Movemos todos los cups posteriores
        #al orden que nos han dado
        search_params = [('zona','=',zona),
                         ('ordre','>=',ordre)]
        cups_ids = cups_obj.search(cursor, uid, search_params,
                                   order='ordre')
        for cups_id in cups_obj.browse(cursor, uid, cups_ids):
            cups_id.write({'ordre': '%04d'%(int(cups_id.ordre)+1)},
                            context={'sync': False})
        #Una vez movidos todos los cups posteriores, escribimos en el cups
        cups.write({'zona':zona, 'ordre': ordre},
                    context={'sync': False})
        return True

    def elimina_cups(self, cursor, uid, ids, cups_origen, context=None):
        '''funcio que elimina un cups de su zona de lectura'''

        cups_obj = self.pool.get("giscedata.cups.ps")
        cups = cups_obj.browse(cursor, uid, cups_origen)
        #miramos los valores actuales de este cups
        zona = cups.zona
        ordre = cups.ordre
        if not zona or zona == '0000':
            #Si la zona es nula o ceros,
            #solo lo marcamos como zona 0000
            cups.write({'zona': '0000', 'ordre': '0000'},
                        context={'sync': False})
            return True
        #buscamos todos los cups de la zona
        #posteriores al orden que nos han dado una posicion
        search_params = [('zona','=',zona),
                         ('ordre','>',ordre)]
        cups_ids = cups_obj.search(cursor, uid, search_params,
                                   order='ordre')
        #Los movemos una posicion
        for cups_id in cups_obj.browse(cursor, uid, cups_ids):
            cups_id.write({'ordre': '%04d'%(int(cups_id.ordre)-1)},
                            context={'sync': False})
        #Una vez movidos todos los cups posteriores, escribimos en el cups
        cups.write({'zona': '0000', 'ordre': '0000'},
                    context={'sync': False})
        return True

    def action_confirmar(self, cursor, uid, ids, context=None):

        self.browse(cursor, uid, ids[0]).write({'state':'end'})

    def action_mover(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        self.elimina_cups(cursor, uid, wizard.id, wizard.cups_id.id)
        if wizard.despres_de and not wizard.zona_nova:
            # Force to propose
            wizard.propose()
            wizard = self.browse(cursor, uid, ids[0])
        if wizard.zona_nova:
            self.inserta_cups(cursor, uid, wizard.id,
                              wizard.zona_nova.rjust(4,'0'),
                              wizard.ordre_nou.rjust(4,'0'),
                              wizard.cups_id.id)
        return {}

    def propose(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        cups_obj = self.pool.get('giscedata.cups.ps')
        if not wiz.despres_de:
            cups_ids = cups_obj.search(cursor, uid, [
                ('nv', '=', wiz.cups_id.nv),
                ('pnp', '>=', wiz.cups_id.pnp)
            ], order='pnp asc', limit=1)
            if cups_ids:
                cups_id = cups_ids[0]
                cups = cups_obj.read(cursor, uid, cups_id, ['zona', 'ordre'])
                wiz.write({
                    'zona_nova': cups['zona'],
                    'ordre_nou': '%04d' % (int(cups['ordre']) + 1)
                })
            return True
        else:
            wiz.write({
                'zona_nova': wiz.despres_de.zona,
                'ordre_nou': '%04d' % (int(wiz.despres_de.ordre) + 1)
            })
        return False

    _columns = {
        'cups_id':fields.many2one('giscedata.cups.ps', 'CUPS',
                                  required=True, readonly=False),
        'despres_de': fields.many2one('giscedata.cups.ps', 'Despŕes de'),
        'zona_ant':fields.related('cups_id', 'zona', type='char',
                                  relation='giscedata.cups.ps',
                                  string='Zona', readonly=True),
        'ordre_ant':fields.related('cups_id', 'ordre', type='char',
                                  relation='giscedata.cups.ps',
                                  string='Ordre', readonly=True),
        'zona_nova':fields.char('Zona nova', size=20,
                                required=False, readonly=False,
                                help=u"En blanc per no més llevar de la zona actual\n"
                                     u"sense incloure el cups en una altra zona"),
        'ordre_nou':fields.char('Ordre nou', size=20,
                                required=False, readonly=False),
        'state':fields.selection([('init','Init'), ('end','End')],
                                 'Estat', select=True, size=5)

    }

    _defaults = {
        'state': lambda *a: 'init',
    }
WizardReordenarZona()
