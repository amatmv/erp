# -*- coding: utf-8 -*-
{
    "name": "Exportació de Lectures per PDA",
    "description": """
Mòdul per exportar les lectures i treballar amb el programa de la PDA.
  * Exporta un fitxer per tal de carregar-lo a la PDA i poder fer les lectures.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_pda_data.xml",
        "giscedata_lectures_view.xml",
        "giscedata_lectures_pda_report.xml",
        "giscedata_lectures_pda_wizard.xml",
        "wizard/wizard_export_view.xml",
        "wizard/wizard_informe_ruta_lectura_view.xml",
        "wizard/wizard_reordenar_zona_view.xml",
        "wizard/wizard_desmarcar_in_tpl_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
