#!/bin/sh
#
#  packTPL.sh
#  
#  Descripció: Ajuntar les dades de tots els fitxers de corbes en un de sol
#

CURVA_DIR=/home/smb/TPLshare
SAVE_FILE=packTPL_`date +%y%m%d`.txt
ERROR_LOG_FILE=/home/smb/log/error.log
ACCESS_LOG_FILE=/home/smb/log/access.log
STDERR_LOG_FILE=/home/smb/log/empty.log
RESULT_LOG_FILE=resultado.log

rm $STDERR_LOG_FILE 2> /dev/null

if [ -n "$CURVA_DIR" ] ; then
  if [ ! -d "$CURVA_DIR" ] ; then
    echo [`date`] "Error: Directori inexistent" >> $ERROR_LOG_FILE
    exit 0
  fi
else
  echo [`date`] "Error: Directori desconegut" >> $ERROR_LOG_FILE
  exit 0
fi

cd $CURVA_DIR

if [ -f $SAVE_FILE ] ; then
  rm $SAVE_FILE
fi

if [ -f $RESULT_LOG_FILE ] ; then
  rm $RESULT_LOG_FILE
fi

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for file in `ls -d *.curva 2> $STDERR_LOG_FILE` ; do
  echo [`date`] "Procesando fichero: $file" >> $RESULT_LOG_FILE
  COMPTADOR=`head -n 1 $file |awk '{print $1}'`
  tail -n +3 $file | awk -v var=$COMPTADOR '{print var "\t" $0}' >> $SAVE_FILE
done
IFS=$SAVEIFS

if [ ! -s $STDERR_LOG_FILE ] ; then
  echo [`date`] "Fichero $CURVA_DIR/$SAVE_FILE creado correctamente" >> $RESULT_LOG_FILE
  cat $RESULT_LOG_FILE >> $ACCESS_LOG_FILE
fi
