# -*- coding: utf-8 -*-

import jasper_reports

def informe_ruta(cursor, uid, ids, data, context):
    
    return {
        'parameters': {'data_inici':data['form']['data_inici'],
                       'data_final':data['form']['data_final'],
                       'zona': data['form']['zona'], },
    }

jasper_reports.report_jasper(
   'report.report_wizard_informe_ruta_lectura',
   'wizard.soller.ruta.lecturas',
   parser=informe_ruta
)

jasper_reports.report_jasper(
   'report.report_wizard_informe_ruta_lectura_observacions',
   'wizard.soller.ruta.lecturas',
   parser=informe_ruta
)

jasper_reports.report_jasper(
   'report.report_wizard_informe_ruta_lectura_resum',
   'wizard.soller.ruta.lecturas',
   parser=informe_ruta
)
