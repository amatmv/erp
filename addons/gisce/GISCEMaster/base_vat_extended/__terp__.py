# -*- coding: utf-8 -*-
{
    "name": "VAT (extended)",
    "description": """
 * Enable the VAT Number for the partner. 
 * Check the validity of that VAT Number with or without country code before VAT number.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules/Base",
    "depends":[
        "base",
        "base_vat",
        "account"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
