# -*- encoding: utf-8 -*-
"""Overwriting check_vat from base_vat module."""
from osv import osv
import netsvc

class ResPartner(osv.osv):
    """Partner object."""
    _name = 'res.partner'
    _inherit = 'res.partner'

    def check_vat(self, cursor, uid, ids):
        """Check vat using country from address."""

        for partner in self.browse(cursor, uid, ids):
            if not partner.vat:
                continue
            if partner.country:
                vat_country = partner.country.code.lower()
            else:
                vat_country = partner.vat[:2].lower()
            vat_number = partner.vat
            if partner.vat[:2].lower() == vat_country:
                vat_number = partner.vat[2:].replace(' ', '')
            check = getattr(self, 'check_vat_' + vat_country)
            if not check(vat_number):
                return False

        return True

ResPartner()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

