# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Intensitats per Sortides BT",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE Master",
    "depends":[
        "base",
        "giscedata_trieni",
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_sortidesbt_intensitats_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
