# -*- coding: iso-8859-1 -*-
from osv import osv, fields

import math
import time

class giscedata_cts_sortidesbt(osv.osv):

    _name = 'giscedata.cts.sortidesbt'
    _inherit = 'giscedata.cts.sortidesbt'

    _columns = {
      'intensitats': fields.one2many('giscedata.cts.sortidesbt.intensitats', 'sortidabt', 'Intensitats'),
    }


giscedata_cts_sortidesbt()

class giscedata_cts_sortidesbt_intensitats(osv.osv):

    _name = 'giscedata.cts.sortidesbt.intensitats'

    _columns = {
      'name': fields.datetime('Data', required=True),
      'trimestre': fields.many2one('giscedata.trieni', 'Trimestre'),
      'sortidabt': fields.many2one('giscedata.cts.sortidesbt', 'Sortida BT', required=True),
      'r': fields.float('R'),
      's': fields.float('S'),
      't': fields.float('T'),
      'observacions': fields.text('Observacions'),
    }

    _order = 'name desc'

    _defaults = {
      'name': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
    }

giscedata_cts_sortidesbt_intensitats()
