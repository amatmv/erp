# -*- coding: utf-8 -*-
{
    "name": "Condicions generals",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
    * Condicions generals pòlisses
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_condicions_generals_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
