# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataPolissaCondicionsGenerals(osv.osv):
    _name = 'giscedata.polissa.condicions.generals'

    _columns = {
        'name': fields.char(u'Descripció', size=256, required=True),
        'attachment_id': fields.many2one(
            'ir.attachment', 'Document'
        )
    }

GiscedataPolissaCondicionsGenerals()
