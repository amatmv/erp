# -*- coding: utf-8 -*-
import base64

from giscedata_polissa.report.contracte import ContracteWebkitParser


class ReportContracteCondicionsGenerals(ContracteWebkitParser):

    def get_attachment_datas(self, pool, cursor, uid, model_id, datas, context=None):
        policy_o = pool.get('giscedata.polissa')
        res = []

        polissa_date = datas.get('form', {}).get('polissa_date')
        ctx = context.copy()

        if polissa_date:
            ctx['date'] = polissa_date

        policy = policy_o.browse(cursor, uid, model_id, context=ctx)

        if policy.condicions_generals_id:
            attachment = policy.condicions_generals_id.attachment_id
            if attachment and attachment.datas:
                res.append(base64.b64decode(attachment.datas))

        return res


ReportContracteCondicionsGenerals()
