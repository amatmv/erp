from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'condicions_generals_id': fields.many2one(
            'giscedata.polissa.condicions.generals', 'Condicions generals'
        )
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'condicions_generals_id': fields.many2one(
            'giscedata.polissa.condicions.generals', 'Condicions generals'
        )
    }

GiscedataPolissaModcontractual()
