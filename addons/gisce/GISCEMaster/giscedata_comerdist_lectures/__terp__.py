# -*- coding: utf-8 -*-
{
    "name": "Lectura de comptadors (Comerdist)",
    "description": """Lectures dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_lectures",
        "giscedata_comerdist"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
