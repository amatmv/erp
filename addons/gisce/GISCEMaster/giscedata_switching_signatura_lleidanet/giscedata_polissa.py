# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def start_signature_atr(self, cursor, uid, polissa_id, cas_id, addr_id, context=None):
        """
        Funtionality not implemented with this provider. No signature process.
        """
        if not context:
            context = {}
        context['enviament_pendent'] = True
        context['state'] = 'open'
        return super(GiscedataPolissa, self).start_signature_atr(
            cursor, uid, polissa_id, cas_id, addr_id, context=context
        )


GiscedataPolissa()
