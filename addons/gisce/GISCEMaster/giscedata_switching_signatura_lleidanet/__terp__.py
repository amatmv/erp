# -*- coding: utf-8 -*-
{
    "name": "Signatura digital processos ATR Lleida-net (Comercialitzadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_switching_signatura"
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[],
    "active": False,
    "installable": True
}
