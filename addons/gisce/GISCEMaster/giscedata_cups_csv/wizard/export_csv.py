# -*- coding: utf-8 -*-
import wizard
import base64
import csv
import StringIO
import time


def _init(self, cr, uid, data, context={}):
    return {}

def _csv(self, cr, uid, data, context={}):
    #crearem el fitxer CSV
    # SQL substitutiu:
    cr.execute("""
  select
          cups.zona as ZONA,
          cups.ordre as ORDRE,
          polissa.name as POLISSA,
          partner.name as NOM,
          cups.nv as ADRECA,
          cups.pnp as NUM,
          cups.pt as PIS,
          cups.pu as PORTA,
          polissa.potencia as POT,
          cups.et as ET,
          cups.linia as LINIA,
          escomesa.name as NODE,
          cups.name as CUPS,
          polissa.n_comptador as CONTADOR
  from
          giscedata_cups_ps cups,
          giscedata_polissa polissa,
          giscedata_cups_escomesa escomesa,
          res_partner as partner
  where
          polissa.cups = cups.id
          and cups.id_escomesa = escomesa.id
          and polissa.partner_id = partner.id
          and to_char(cups.write_date, 'YYYY-MM-DD') = to_char(now(), 'YYYY-MM-DD')""")
    output = StringIO.StringIO()
    writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_ALL)
    for line in cr.fetchall():
        writer.writerow(line)
    file = base64.b64encode(output.getvalue())
    output.close()
    return {'name': 'cups_modificats_%s.csv' % (time.strftime('%d-%m-%Y')), 'file': file}

_csv_form = """<?xml version="1.0"?>
<form string="Fitxer CUPS Modificats">
  <field name="name" colspan="4" width="500"/>
  <field name="file" colspan="4"/>
</form>
"""

_csv_fields = {
  'name': {'string': 'Nom', 'type': 'char', 'size': 60},
  'file': {'string': 'Fitxer', 'type': 'binary'},
}


class wizard_giscedata_cups_csv_export(wizard.interface):

    states = {
      'init': {
          'actions': [_init],
          'result': { 'type' : 'state', 'state' : 'csv' },
      },
      'csv': {
          'actions': [_csv],
          'result': {'type': 'form', 'arch': _csv_form, 'fields': _csv_fields, 'state': [('end', 'Tancar', 'gtk-close')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_giscedata_cups_csv_export('giscedata.cups.csv.export')
