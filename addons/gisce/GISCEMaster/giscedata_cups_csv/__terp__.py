# -*- coding: utf-8 -*-
{
    "name": "GISCE Data CUPS CSV",
    "description": """Extreu un CSV amb els CUPS modificats en el dia actual""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_cups",
        "giscedata_cups_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_csv_wizard.xml"
    ],
    "active": False,
    "installable": True
}
