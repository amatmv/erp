<%
    from datetime import datetime

    from babel.numbers import format_currency, format_number
    from babel.dates import format_date
    from addons import get_module_resource


    def get_results(cursor, start, end, series):
        sql_file = get_module_resource(
            'giscedata_informes_comptables', 'sql', 'libro_registro_iva.sql'
        )
        with open(sql_file, 'r') as f:
            sql = f.read()
        cursor.execute(sql, (start, end, tuple(series)))
        results = cursor.dictfetchall()
        return results

    def get_diarios(pool, cursor, uid, journals_ids):
        journal_obj = pool.get('account.journal')
        return journal_obj.read(cursor, uid, journals_ids)
%>
<!doctype html>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    	<style type="text/css">
body
{
    padding: 5px;
	line-height: 1.6em;
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
    font-size: 12px;
}

tr {
    page-break-inside: avoid;
}

table {
     border-collapse: collapse;
     width: auto;
     padding: 0px;
     margin: 0px;
}

.hor-minimalist-a
{
	background: #fff;
	margin: 45px;
	border-collapse: collapse;
	text-align: left;
}

.hor-minimalist-a thead {
    display: table-header-group;
}

.hor-minimalist-a tfoot {
    display: table-row-group
}

.hor-minimalist-a th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding-top: 25px;
	border-bottom: 2px solid #6678b1;
}

.hor-minimalist-a td
{
	color: #669;
	padding: 2px 8px 0px 8px;
    min-width: 100px;
}

.hor-minimalist-a tfoot td
{
    padding-top: 15px;
    font-weight: bold;
}

        </style>
</head>
${company.name}
<h1>Libro registro IVA</h1>
<dl>
    <dt>Fecha desde:</dt>
    <dd>${format_date(datetime.strptime(data['form']['data_inici'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')}</dd>
    <dt>Fecha hasta:</dt>
    <dd>${format_date(datetime.strptime(data['form']['data_final'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')}</dd>
    <dt>Diarios:</dt>
    <dd>
        </ul>
        %for d in get_diarios(user.pool, user._cr, user.id, data['form']['sequences']):
            <li>${d['name']}</li>
        %endfor
        </ul>
    </dd>
</dl>
<table class="hor-minimalist-a">
    <thead>
        <th>Factura</th>
        <th>Fecha</th>
        <th>Nombre</th>
        <th>NIF</th>
        <th>Impuesto</th>
        <th>Base</th>
        <th>Cantidad</th>
        <th>Total</th>
    </thead>
    <tbody>
    <%
        resum = {}
    %>
    %for result in get_results(user._cr, data['form']['data_inici'], data['form']['data_final'], data['form']['sequences']):
        <%
            resum.setdefault(result['nomimpuesto'], {'base': 0, 'impuesto': 0, 'total': 0})
            resum[result['nomimpuesto']]['base'] += result['base']
            resum[result['nomimpuesto']]['impuesto'] += result['impuesto']
            resum[result['nomimpuesto']]['total'] += result['total']
        %>
        <tr>
            <td>${result['numfactura']}</td>
            <td>${result['fecha_factura']}</td>
            <td>${result['nompagador']}</td>
            <td>${result['nif']}</td>
            <td>${result['nomimpuesto']}</td>
            <td>${format_number(result['base'], locale='es_ES')}</td>
            <td>${format_number(result['impuesto'], locale='es_ES')}</td>
            <td>${format_number(result['total'], locale='es_ES')}</td>
        </tr>
    %endfor
    </tbody>
</table>
<table class="hor-minimalist-a">
    <thead>
        <th>Nombre impuesto</th>
        <th>Base</th>
        <th>Cantidad</th>
        <th>Total</th>
    </thead>
    <tbody>
    %for impost, totals in resum.items():
        <tr>
            <td>${impost}</td>
            <td>${format_currency(totals['base'], 'EUR', locale='es_ES')}</td>
            <td>${format_currency(totals['impuesto'], 'EUR', locale='es_ES')}</td>
            <td>${format_currency(totals['total'], 'EUR', locale='es_ES')}</td>
        </tr>
    %endfor
    </tbody>
</table>
</html>
