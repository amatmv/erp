<%
    from datetime import datetime
    pool = objects[0].pool
    move_line_obj = pool.get('account.move.line')
    fact_obj = pool.get('giscedata.facturacio.factura')
    user_obj = pool.get('res.users')
    company_obj = pool.get('res.company')
    partner_obj = pool.get('res.partner')
    journal_obj = pool.get('account.journal')

    if data and 'move_line_ids' in data.keys():
        # if it comes from the wizard
        move_line_ids = data['move_line_ids']
        move_lines = move_line_obj.browse(cursor, uid, move_line_ids)
        date_from = data['date_from']
        date_to = data['date_to']
        journal_id = data['journal_id']
    else:
        # if it comes from the view opened by the wizard
        move_lines = objects
        date_from = context['date_from']
        date_to = context['date_to']
        journal_id = context['journal_id']

    journal_name = journal_obj.read(cursor, uid, journal_id, ['name'])['name']

    def get_factura(line):
        fact_ids = fact_obj.search(cursor, uid, [('number', '=', line.ref)])
        if fact_ids:
            fact_id = fact_ids[0]
            return fact_obj.read(
                cursor, uid, fact_id, ['number', 'date_invoice', 'date_due']
            )
        return False

    def get_user_name():
        user = user_obj.read(cursor, uid, [uid], ['name'])[0]['name']
        return user

    def get_company():
        company_id = user_obj.read(cursor, uid, [uid], ['company_id'])[0]['company_id'][0]
        company = company_obj.read(cursor, uid, company_id, ['name', 'partner_id'])
        partner = partner_obj.read(cursor, uid, company['partner_id'][0], ['vat'])
        return "{0}, {1}".format(company['name'], partner['vat'])
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_informes_comptables/report/llistat_cobraments.css"/>
    </head>
    <body>
        %if move_lines:
            <%def name="header_line()">
                <center>
                    <div>
                        <div class="header_title">
                            ${_(u"Listado de cobros realizados - Diario: ")}${journal_name}
                        </div>
                        <br>
                        <div class="partner">
                            ${get_company()}
                        </div>
                        <div class="page_number">
                            <b>Usuario:</b> ${get_user_name()}
                            <br>
                            <b>Fecha informe:</b> ${datetime.today().strftime('%d/%m/%Y %H:%M:%S')}
                            <br>
                            <b>Fecha desde:</b> ${formatLang(date_from, date=True)}
                            <br>
                            <b>Fecha hasta:</b> ${formatLang(date_to, date=True)}
                        </div>
                    </div>
                </center>
            </%def>
            <div class="continguts">
                <table>
                    <thead>
                        <tr>
                            <td colspan="13" class="header_line">${header_line()}</td>
                        </tr>
                        <tr>
                            <td colspan="8"></td>
                        </tr>
                        <tr>
                            <td class="titol">
                                ${_(u"F. Pago")}
                            </td>
                            <td class="titol">
                                ${_(u"Factura")}
                            </td>
                            <td class="titol">
                                ${_(u"Descripción")}
                            </td>
                            <td class="titol">
                                ${_(u"F. Factura")}
                            </td>
                            <td class="titol">
                                ${_(u"Cliente")}
                            </td>
                            <td class="titol">
                                ${_(u"Importe")}
                            </td>
                            <td class="titol">
                                ${_(u"Medio Pago")}
                            </td>
                            <td class="titol">
                                ${_(u"Entidad Bancaria")}
                            </td>
                            <td class="titol">
                                ${_(u"F.Vto.")}
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        %for line in move_lines:
                        <%
                            factura = get_factura(line)
                        %>
                            <tr>
                                <td>
                                    ${formatLang(line.date, date=True)}
                                </td>
                                <td>
                                    %if factura:
                                        ${factura['number'] or ''}
                                    %endif
                                </td>
                                <td>
                                    ${line.name}
                                </td>
                                <td>
                                    %if factura:
                                        ${formatLang(factura['date_invoice'], date=True)}
                                    %endif
                                </td>
                                <td>
                                    ${line.partner_id.name}
                                </td>
                                <td  style="text-align: right;">
                                    ${"{0:.2f}".format(line.credit - line.debit)}
                                </td>
                                <td style="text-align: center;">
                                    ${line.journal_id.name}
                                </td>
                                <td>
                                    %if line.partner_bank:
                                        ${line.partner_bank.name}
                                    %endif
                                </td>
                                <td>
                                    %if factura:
                                        ${formatLang(factura['date_due'], date=True)}
                                    %endif
                                </td>
                            </tr>
                        %endfor
                        <tr>
                            <td colspan="8"></td>
                        </tr>
                        <tr class="foot">
                            <td style="text-align: right;" colspan="4">${_(u"Totales")}</td>
                            <td><center>${len(move_lines)}</center></td>
                            <%
                                total = 0
                                for line in move_lines:
                                    total += line.credit
                                    total -= line.debit
                            %>

                            <td style="text-align: right;"> ${"{0:.2f}".format(total)}</td>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        %else:
            <br><br><br>
            <center>
                <b><font size="4">${_(u"No se han encontrado cobros en el diario ")}${journal_name}${_(u" entre las fechas ")}${formatLang(date_from, date=True)}${_(u" y ")}${formatLang(date_to, date=True)}.</font></b>
            </center>
        %endif
    </body>
</html>
