# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
        })


webkit_report.WebKitParser(
   'report.giscedata.informes.comptables.llistat.cobraments',
   'wizard.llistat.cobraments',
   'giscedata_informes_comptables/report/llistat_cobraments.mako',
   parser=report_webkit_html
)

webkit_report.WebKitParser(
   'report.account.move.line.llistat.cobraments',
   'account.move.line',
   'giscedata_informes_comptables/report/llistat_cobraments.mako',
   parser=report_webkit_html
)


class DummyHeader(object):
    margin_top = 0
    margin_bottom = 0
    margin_left = 0
    margin_right = 0
    orientation = 'Portrait'
    format = 'A4'
    html = '<html></html>'
    footer_html = ''
    css = ''


class NoXMLWebkitReport(webkit_report.WebKitParser):
    def create_single_pdf(self, cursor, uid, ids, data, report_xml=False,
                          context=None):
        webkit_header = getattr(report_xml, 'webkit_header', None)
        if not webkit_header:
            report_xml.webkit_header = DummyHeader()
        return super(NoXMLWebkitReport, self).create_single_pdf(
            cursor, uid, ids, data, report_xml, context
        )


NoXMLWebkitReport(
    'report.libro_registro_iva',
    'wizard.informes.comptabilitat',
    'giscedata_informes_comptables/report/libro_registro_iva.mako',
    parser=report_sxw.rml_parse
)
