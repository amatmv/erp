# -*- coding: utf-8 -*-
from datetime import datetime

from osv import osv, fields
from tools.translate import _
from addons import get_module_resource


class WizardLlistatCobraments(osv.osv_memory):
    _name = 'wizard.llistat.cobraments'

    _columns = {
        'date_from': fields.date('Data inici', required=True),
        'date_to': fields.date('Data final', required=True),
        'journal_id': fields.many2one('account.journal', 'Diari', required=True)
    }

    _defaults = {
        'date_from': lambda *a: datetime.now().strftime('%Y-%m-%d'),
        'date_to': lambda *a: datetime.now().strftime('%Y-%m-%d')
    }

    def get_line_ids(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        sql_path = get_module_resource(
            'giscedata_informes_comptables',
            'sql',
            'llistat_cobraments.sql'
        )
        with open(sql_path, 'r') as sql_file:
            sql = sql_file.read()
            sql_params = wiz.read()[0]
            sql_params.pop('id', None)
            cursor.execute(sql, sql_params)

            move_line_ids = [x[0] for x in cursor.fetchall()]
        return move_line_ids

    def imprimir(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        move_line_ids = self.get_line_ids(cursor, uid, ids, context)
        dades = {
            'move_line_ids': move_line_ids,
            'date_from': wiz.date_from,
            'date_to': wiz.date_to,
            'journal_id': wiz.journal_id.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'giscedata.informes.comptables.llistat.cobraments',
            'datas': dades
        }

    def run(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        move_line_ids = self.get_line_ids(cursor, uid, ids, context)

        return {
            'domain': [('id', 'in', move_line_ids)],
            'name': _('Cobraments {0}: {1} a {2}').format(
                wiz.journal_id.name, wiz.date_from, wiz.date_to
            ),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'type': 'ir.actions.act_window',
            'context': {
                'date_from': wiz.date_from,
                'date_to': wiz.date_to,
                'journal_id': wiz.journal_id.id
            }
        }

WizardLlistatCobraments()
