# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
import time


class WizardInformesComptabilitat(osv.osv_memory):

    _name = "wizard.informes.comptabilitat"
    
    _get_informe = [('report_facturacio_llibre_registre_iva_fechas',
                     'Llibre registre'),
                    ('report_facturacio_llibre_registre_general_fechas',
                     'Llibre registre general'),
                    ('report_facturacio_listado_fianzas_fechas',
                     'Llistat de fiances'),
                    ('libro_registro_iva',
                     'Llibre registra IVA (multiserie)')
                   ]

    def _get_sequence(self, cursor, uid, ids, context=None):
        '''retorna una llista amb totes les seqs associades
        a un diari de facturacio'''

        journal_obj = self.pool.get('account.journal')
        
        res = []

        search_params = [('invoice_sequence_id','!=', None),]
        journal_ids = journal_obj.search(cursor, uid, search_params)
        for journal in journal_obj.browse(cursor, uid, journal_ids):
            repetit = [x[0] for x in res] 
            if not journal.invoice_sequence_id.code in repetit:
                res.append((journal.invoice_sequence_id.code,
                        journal.invoice_sequence_id.name))

        return res

    def imprimir(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        datas = {'form': wizard.read()[0]}
        
        # TODO: Remove when all will multisequence
        sequence = datas['form'].pop('sequence')
        datas['form']['serie'] = sequence

        return {
            'type': 'ir.actions.report.xml',
            'report_name': wizard.informe,
            'datas': datas,
        }

    _columns = {
        'data_inici': fields.date('Des de', required=True),
        'data_final': fields.date('Fins', required=True),
        'informe': fields.selection(_get_informe, 'Informe', required=True),
        'sequence': fields.selection(_get_sequence, 'Serie'),
        'sequences': fields.many2many(
            'account.journal',
            'account_journal',
            'id',
            'id',
            'Serie', domain=[
                ('invoice_sequence_id', '!=', False)]
        )
    }


WizardInformesComptabilitat()
