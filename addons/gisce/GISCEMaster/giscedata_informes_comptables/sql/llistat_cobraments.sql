select
  DISTINCT ml.id, ml.date
from account_move_line ml
  inner join account_move m on ml.move_id = m.id
  inner join account_move_reconcile r on (ml.reconcile_id = r.id or ml.reconcile_partial_id = r.id)
where ml.date between %(date_from)s and %(date_to)s and ml.journal_id = %(journal_id)s
order by ml.date asc