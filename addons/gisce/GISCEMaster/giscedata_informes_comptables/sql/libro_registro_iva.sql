SELECT
    factura.id as idfactura,
    factura.number as numfactura,
    to_char(factura.date_invoice, 'dd/mm/YYYY') as fecha_factura,
    pagador.name as nompagador,
    pagador.vat as nif,
    serie.name as nomserie,
    coalesce(tax.name, '') as nomimpuesto,
    COALESCE(
        CASE
            WHEN factura.type = 'out_refund'
            THEN tax.base * -1
            ELSE tax.base
        END, 0
    ) as base,
    COALESCE(
        CASE
            WHEN factura.type = 'out_refund'
            THEN tax.amount * -1
            ELSE tax.amount
        END, 0
    ) as impuesto,
    COALESCE(
        CASE
            WHEN factura.type = 'out_refund'
            THEN (tax.base + tax.amount) * -1
            ELSE (tax.base + tax.amount)
        END, 0
    ) as total,
    empresa.name as empresa,
    substring(coalesce(partner_comp.vat,''),3) as cifempresa
FROM account_invoice factura
LEFT JOIN res_partner pagador ON pagador.id = factura.partner_id
LEFT JOIN res_partner_address direccion ON direccion.id = factura.address_invoice_id
INNER JOIN account_journal journal  ON journal.id = factura.journal_id
INNER JOIN ir_sequence serie  ON serie.id = journal.invoice_sequence_id
LEFT JOIN account_invoice_tax tax ON tax.invoice_id = factura.id
LEFT JOIN res_company empresa ON empresa.id = factura.company_id
LEFT JOIN res_partner partner_comp  ON partner_comp.id = empresa.partner_id
WHERE
    factura.date_invoice  between %s and %s
    and journal.id in %s
    and (tax.name ilike 'IVA%%' or coalesce(tax.name, '') = '')
    and factura.state in ('open','paid')
order by factura.date_invoice asc