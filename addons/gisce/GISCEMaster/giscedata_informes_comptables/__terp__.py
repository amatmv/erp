# -*- coding: utf-8 -*-
{
    "name": "Informes comptables",
    "description": """
Diferents informes comptables
    * Llibre registre de factures (No més IVA)
    * Llibre registre de factures (Tots els impostos)
    * Llistat de fiances (generades pel mòdul de contractació)
    * Llistats de cobros
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Account",
    "depends":[
        "giscedata_facturacio",
        "c2c_webkit_report",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "informes_comptabilitat_report.xml",
        "wizard/wizard_informes_comptabilitat_view.xml",
        "wizard/wizard_llistat_cobraments_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
