  select
          ct.name,
          ct.descripcio,
          t.name,
          m.name as data,
          COALESCE(m.intensitat_max_bt1, 0.00) as mesurada_b1,
          COALESCE(c.amp_nom_b1, 0.00) as max_b1,
          case
                  when c.amp_nom_b1 > 0 then
                          round(100*(m.intensitat_max_bt1/c.amp_nom_b1)::numeric, 2)
                  else
                          0
          end as saturacio_b1,
          coalesce(t.k,1) as k,
          COALESCE(m.intensitat_max_bt2, 0.00) as mesurada_b2,
          c.amp_nom_b2 as max_b2,
          case
                  when c.amp_nom_b2 > 0 then
                          round(100*(m.intensitat_max_bt2/c.amp_nom_b2)::numeric,2)
                  else
                          0
          end as saturacio_b2,
          case
                  when c.amp_nom_b1 > 0 and c.amp_nom_b2 > 0 then
                          round(100*(((m.intensitat_max_bt1/c.amp_nom_b1)*coalesce(t.k,1))+(m.intensitat_max_bt2/c.amp_nom_b2))::numeric,2)
                  when c.amp_nom_b1 > 0 and coalesce(c.amp_nom_b2,0) = 0 then
                          round(100*(m.intensitat_max_bt1/c.amp_nom_b1)::numeric,2)
                  when coalesce(c.amp_nom_b1,0) = 0 and c.amp_nom_b2 > 0 then
                          round(100*(m.intensitat_max_bt2/c.amp_nom_b2)::numeric,2)
                  else
                          0
          end as b1_b2
  from
          giscedata_transformador_mesures m,
          giscedata_transformador_trafo t,
          giscedata_transformador_connexio c,
          giscedata_transformador_estat e,
          giscedata_cts ct
  where
          m.id in (
                  select
                          id
                  from
                          giscedata_transformador_mesures m
                  where
                          m.trafo = t.id
                          and m.name is not null
                          and (m.intensitat_max_bt1 > 0 or m.intensitat_max_bt2 > 0)
                  order by name desc limit 1

          )
          and m.ct = t.ct
          and m.trafo = t.id
          and t.ct is not null
          and t.id_estat = e.id
          and e.codi = 1
          and c.trafo_id = t.id
          and c.conectada = True
          and ct.id = m.ct
  order by b1_b2 desc
