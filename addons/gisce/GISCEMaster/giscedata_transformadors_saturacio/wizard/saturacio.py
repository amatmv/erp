# -*- coding: utf-8 -*-

import wizard
import pooler
from tools import config

def _print(self, cr, uid, data, context={}):
    sql_path = '%s/giscedata_transformadors_saturacio/sql/saturacio.sql'
    with open(sql_path % config['addons_path']) as f:
        sql = f.read()
        cr.execute(sql)
    saturacio_obj = pooler.get_pool(cr.dbname).get('giscedata.transformadors.saturacio')
    ids = saturacio_obj.search(cr, uid, [])
    return {'ids': ids}

class wizard_giscedata_transformadors_saturacio(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'print'}
      },
      'print': {
        'actions': [_print],
        'result': {'type': 'print', 'report':
        'giscedata.transformadors.saturacio', 'get_id_from_action':True,
          'state':'end'}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      }
    }

wizard_giscedata_transformadors_saturacio('giscedata.transformadors.saturacio')
