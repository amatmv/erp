# -*- coding: utf-8 -*-
import logging

def up(cursor, installed_version):

    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting old giscedata_transformadors_saturacio view')

    sql = """DROP VIEW IF EXISTS giscedata_transformadors_saturacio"""

    cursor.execute(sql)

    logger.info('Succesfully deleted giscedata_transformadors_saturacio view')


def down(cursor, installed_version):
    pass


migrate = up
