# -*- coding: utf-8 -*-
{
    "name": "Saturació dels Transformadors",
    "description": """Informes de saturació dels transformadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "c2c_webkit_report",
        "giscedata_transformadors",
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_transformadors_saturacio_wizard.xml",
        "giscedata_transformadors_saturacio_report.xml",
        "giscedata_transformadors_saturacio_view.xml",
        "giscedata_cts.xml",
        "giscedata_transformadors.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
