# -*- coding: utf-8 -*-
from osv import osv, fields


class giscedata_transformadors_saturacio(osv.osv):

    _name = 'giscedata.transformadors.saturacio'

    _columns = {
        'ct': fields.many2one('giscedata.cts', 'CT'),
        'trafo': fields.many2one('giscedata.transformador.trafo', 'Nº. TRF.'),
        'data': fields.datetime('Data'),
        'mesurada_b1': fields.float('Mesurada B1', digits=(16, 8)),
        'max_b1': fields.float('Màxima B1', digits=(16, 8)),
        'saturacio_b1': fields.float('Saturació B1', digits=(16, 8)),
        'k': fields.float('K', digits=(16, 8)),
        'mesurada_b2': fields.float('Mesurada B2', digits=(16, 8)),
        'max_b2': fields.float('Màxima B2', digits=(16, 8)),
        'saturacio_b2': fields.float('Saturació B2', digits=(16, 8)),
        'b1_b2': fields.float('B1+B2', digits=(16, 8)),
    }

    _order = 'data desc, ct , trafo'

giscedata_transformadors_saturacio()


class GiscedataTransformadorTrafoSaturacio(osv.osv):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    _columns = {
        'saturacio_ids': fields.one2many(
            'giscedata.transformadors.saturacio', 'trafo', 'Saturacions'
        )
    }


GiscedataTransformadorTrafoSaturacio()


class GiscedataCtsSaturacio(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
        'saturacio_ids': fields.one2many(
            'giscedata.transformadors.saturacio', 'ct', 'Saturacions'
        )
    }


GiscedataCtsSaturacio()
