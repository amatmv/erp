# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients Autonumèric",
    "description": """Suggereix el següent número d'expedient que li pertoca.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
