# -*- coding: utf-8 -*-

from osv import osv,fields

class giscedata_expedients_expedient(osv.osv):

    _name = 'giscedata.expedients.expedient'
    _inherit = 'giscedata.expedients.expedient'

    def _suggerir_numero(self, cr, uid, context):
        cr.execute("select max(empresa) from giscedata_expedients_expedient where empresa ~ '^[0-9]+$';")
        n = cr.fetchone() or (0,)
        try:
            n = str(int(n[0])+1)
        except:
            n = ''
        return n

    _defaults = {
      'empresa': _suggerir_numero,
    }


giscedata_expedients_expedient()
