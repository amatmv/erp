# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Agosto 2013",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 185 - 03/08/2013.
""",
    "version": "0-dev",
    "author": "Bartomeu Miro Mateu",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20130801_data.xml"
    ],
    "active": False,
    "installable": True
}
