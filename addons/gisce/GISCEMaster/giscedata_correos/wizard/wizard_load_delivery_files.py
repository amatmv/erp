# -*- encoding: utf-8 -*-

from tools.translate import _
from osv import osv, fields
from giscedata_correos.sicer_correos import SicerProduct as sicer
from csv import DictWriter
from tempfile import TemporaryFile
from base64 import b64encode


class WizardLoadDeliveryFiles(osv.osv_memory):
    '''
    Asistente dedicado a la importación i interpretación de ficheros de
    envios y a la actualización de los estados de las líneas de las remesas
    especificadas en enl fichero cargado
    '''

    _name = 'wizard.load.delivery.files'
    _description = 'Allow\'s user to load delivery files and update orders'

    order_ids = []

    def _default_info(self, cursor, uid, context=None):
        return _('Assistent per a la importació de fitxers de resultats de '
                 'càrrega i actualització de les línies de les remeses')

    def ch_state(self, cursor, uid, ids, state, context=None):
        self.write(cursor, uid, ids, {'state': state}, context=context)

    def interpret_file(self, cursor, uid, ids, context=None):
        '''This method read and process file'''
        imp_file = self.read(
            cursor, uid, ids, ['file'], context=context)[0]['file']
        # Results is a list of dicts with sicer/order and result as keys
        # results = sicer.SicerProduct.interpret_delivery_file(imp_file)
        results = sicer.interpret_retorno_notificaciones_sin_reembolso_file(imp_file)

        return results

    def update_lines(self, cursor, uid, ids, context=None):
        '''This method update all lines with results in imported file'''
        # todo generate summary for info
        order_obj = self.pool.get('giscedata.correos.order')
        line_obj = self.pool.get('giscedata.correos.order.line')

        results = self.interpret_file(cursor, uid, ids, context=context)
        # For the moment wizard only works with one file, but method was
        # prepared to work with multiple files

        err_list = []
        err_info = ''
        for file_res in results:
            # order_dicts is like following dict
            # [{
            # 'order_header': Record,
            # 'details': [Record...],
            # 'modifications': [Record...],
            # 'order_footer': Record
            # },...]
            self.order_ids = []
            order_dicts = file_res['file_orders']
            for order_dict in order_dicts:
                order_code = order_dict['order_header'].get_for_file('order_code')
                order_state = order_dict['order_header'].get_for_file('order_state')

                s_params = [
                    ('name', 'ilike', '%{0}'.format(order_code)),
                    ('state', '!=', 'close')
                ]
                order_id = order_obj.search(
                    cursor, uid, s_params, context=context
                )
                # To avoid a half update of order we make a pre check of order
                # header and get them if order header doesn't has code or can't
                # be found we will skip these although lines has order code
                if order_id:
                    order_id = order_id[0]
                    self.order_ids.append(order_id)
                    # In case of line can't be located in order, the update
                    # of a full order should be canceled to avoid half order
                    # processed.
                    # For the described case, we will store lines info in next
                    # list to full process after interpret all lines in order
                    # domain.
                    update_list = []
                    can_update = True
                    for r_detail in order_dict['details'] + order_dict['modifications']:
                        # Todo: fix possible future bug, suppose an order
                        # CO18001 where 18 are last 2 digits of year, if in 2028
                        # a new order is created, name can be repeated and if we
                        # search for name we will obtain two results
                        # Get the current year to generate full code is not a
                        # solution because supose year 2020 if and order is
                        # created in 2019 but we process them in 2020 the code
                        # will not be generated correctly (expected CO19001 but
                        # obtains CO20001)
                        # order_code = u'CO' + unicode(datetime.now().year)[2]

                        # cod_situacion is a result of "entrega" see
                        # giscedata_correos/sicer_correos/DELIVERY_STATUS table
                        cod_situacion = r_detail.get_for_file('situation_code')
                        date_situation = r_detail.get_for_file('situation_date')

                        # Read sicer product + client + order + number
                        keys_line = ['product_code', 'client_code',
                                     'order_code', 'shipping_number'
                                     ]
                        sicerc = ''
                        for key in keys_line:
                            sicerc += r_detail.get_for_file(key)

                        # TODO a list with (sicer, cod situacion) and osv multi write?
                        # OR todo Or Construct a mapping keys to allow easy acces latter
                        # todo or group by cod_situacion to avoid multiple writes after
                        line_id = line_obj.search(
                            cursor, uid, [('sicer', '=', sicerc),
                                          ('order_id', '=', order_id)
                                          ], context=context
                        )

                        # If a line can't be found break for because full order
                        # can be updated and append in err list to generate
                        # summary later.
                        if not line_id:
                            can_update = False
                            err_list.append(r_detail)

                        # Append future write dict because can be update
                        # Stored format tuple (line_id, write_line_dict)
                        # TODO ADD REST KEYS TO UPDATE////to ask
                        else:
                            update_list.append(
                                (line_id, {'delivery_status': cod_situacion,
                                           'reception_date': date_situation})
                            )

                    for line_tuple in update_list:
                        line_id = line_tuple[0]
                        line_obj.write(cursor, uid, line_id,
                                       line_tuple[1], context=context
                                       )
                        update_result = line_obj.update_pending_states_invoice_state(
                            cursor, uid, line_tuple[0], context=context
                        )
                        if update_result == 'distinct_state':
                            line_data = line_obj.read(
                                cursor, uid, line_id,
                                ['sicer', 'invoice_id', 'invoice_state']
                            )[0]
                            sicer = line_data['sicer']
                            inv_number = line_data['invoice_id'][1]
                            line_inv_state = line_data['invoice_state'][1]
                            err_info += _(
                                'La línia amb codi sicer {sicer} de la '
                                'factura {fact} té un estat d\'impagament '
                                'diferent de {state}.\n'.format(
                                    sicer=sicer, fact=inv_number,
                                    state=line_inv_state
                                )
                            )
                            err_list.append(r_detail)
                    if not can_update:
                        err_info += _(
                            'Algunes linies de la remesa {0} no es poden '
                            'actualitzar\n'.format(order_code))

                    # Fecha cierre is stored f func, then we need to overwrite
                    # using a sql query

                    if order_state == 'C':
                        fecha_cierre = order_dict['order_header'].get_for_file(
                            'cierre_date')

                        query = """
                                UPDATE giscedata_correos_order 
                                                SET order_state = %(order_state)s,
                                                cierre_date = %(fecha_cierre)s
                                                    WHERE id = %(order_id)s
                                """
                        params = {'order_state': order_state,
                                  'fecha_cierre': fecha_cierre,
                                  'order_id': order_id
                                  }

                    else:
                        query = """
                                UPDATE giscedata_correos_order 
                                    SET order_state = %(order_state)s 
                                    WHERE id = %(order_id)s
                                """
                        params = {'order_state': order_state,
                                  'order_id': order_id
                                  }

                    cursor.execute(query, params)

                else:
                    # If no order any line in order can't be updated
                    err_info += _('Remesa {0} no trobada\n'.format(order_code))

                    err_list += (
                        order_dict['details'] + order_dict['modifications']
                    )

        self.ch_state(cursor, uid, ids, 'end', context=context)
        if err_list:
            self.generate_errors_csv(cursor, uid, ids, err_list, context=context)
        if err_info:
            self.write(cursor, uid, ids, {'info': err_info}, context=context)
        else:
            self.write(cursor, uid, ids, {'info': _('Línies processades correctement')}, context=context)

        # Create attatchment
        att_order_obj = self.pool.get('giscedata.correos.attatchment')

        wiz_reads = self.read(
            cursor, uid, ids, ['file', 'file_name', 'info', 'err_file', 'err_fname'], context=context
        )[0]

        attc_vals = {
            'order_ids': self.order_ids,
            'file': wiz_reads['file'],
            'file_name': wiz_reads['file_name'],
            'result': wiz_reads['info'],
        }

        if err_list:
            attc_vals.update(
                {'errors_file': wiz_reads['err_file'],
                 'errors_file_name': wiz_reads['err_fname']
                 })

        att_order_obj.create_attachment_for_orders(
            cursor, uid, attc_vals, context=context
        )

    def generate_errors_csv(self, cursor, uid, ids, errors_list, context=None):
        # I think is a good idea to allow user to download a csv with not
        # updated lines to check
        keys_line = ['product_code', 'client_code',
                     'order_code', 'shipping_number'
                     ]

        res = False
        with TemporaryFile() as csvfile:
            fieldnames = ['order_code', 'sicer_code',
                          'delivery_state', 'type_line(D/M)'
                          ]
            buff = DictWriter(csvfile, fieldnames=fieldnames)
            # Write file header with filenames list
            buff.writeheader()

            for rec_error in errors_list:
                sicerc = ''
                for key in keys_line:
                    sicerc += rec_error.get_for_file(key)

                buff.writerow({
                    'order_code': rec_error.get_for_file('order_code'),
                    'sicer_code': sicerc,
                    'delivery_state': rec_error.get_for_file(
                        'situation_code'),
                    'type_line(D/M)': rec_error.get_for_file('register_type'),
                })
            # seek change w to r
            csvfile.seek(0)
            res = csvfile.read()
            imp_fname = self.read(
                cursor, uid, ids, ['file_name'], context=context
            )[0]['file_name']

            self.write(cursor, uid, ids, {'err_file': b64encode(res),
                                          'err_fname': '{0}{1}'.format(
                                              (imp_fname or 'results'),
                                              '-errors.csv'
                                          ),
                                          'has_errors': True
                                          }
                       )
        return res

    def return_updated_orders_tree_view(self, cursor, uid, ids, context=None):
        return {
            'domain': [('id', 'in', self.order_ids)],
            'name': _('Remeses actualitzades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.correos.order',
            'type': 'ir.actions.act_window',
            'view_id': False,
        }

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('confirm', 'Confirm'), ('end', 'End')], 'State',
            readonly=True
        ),
        'file': fields.binary(
            'File', required=True, help='Fitxer de resultats de càrrega'
        ),
        'info': fields.text('Informació', readonly=True),
        'file_name': fields.char('File name', size=40),
        'err_file': fields.binary(
            'Fitxer d\'errors', readonly=True, help='Fitxer errors'
        ),
        'err_fname': fields.char('Error file name', size=50, readonly=True),
        'has_errors': fields.boolean('Errors'),

    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'has_errors': lambda *a: False
    }

WizardLoadDeliveryFiles()