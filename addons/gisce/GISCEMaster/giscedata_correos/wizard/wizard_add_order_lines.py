# -*- encoding: utf-8 -*-

from tools.translate import _
from osv import osv, fields
from collections import defaultdict


class WizardAddOrderLines(osv.osv_memory):
    """Wizard per afegir linies a les remeses de correus espanya"""

    _name = 'wizard.add.order.lines'
    _description = 'Wizard to add order lines into correos orders'

    def _default_model(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('model', '')

    def _default_info(self, cursor, uid, context=None):
        if not context:
            context = {}
        model = context.get('model', '')
        act_ids = context.get('active_ids', [])
        info = ''
        total_invoices = len(act_ids)
        if model:
            info = _(
                'S\'afegiran {0} factures a la remesa:\n'
            ).format(total_invoices)

            inv_obj = self.pool.get(model)

            inv_r = inv_obj.read(
                cursor, uid, act_ids, ['name', 'pending_state'], context=context
            )
            pending_counts = defaultdict(int)
            for inv in inv_r:
                pending_counts[inv['pending_state']] += 1
            for k in pending_counts.keys():
                state = k
                if state:
                    info += _(
                        'Estat {0}: {1} factures\n'
                    ).format(state[1], pending_counts[k])
                else:
                    info += _(
                        'Sense estat: {0} factures\n'
                    ).format(pending_counts[k])

            return info
        else:
            raise osv.except_osv('Error', _('Model invalid'))

    def confirm_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'confirm'}, context=context)

    def add_lines_to_order(self, cursor, uid, ids, context=None):
        ord_obj = self.pool.get('giscedata.correos.order')
        wiz_fields = self.read(
            cursor, uid, ids, ['order_id', 'model'], context=context
        )[0]
        invoice_ids = context.get('active_ids', [])
        order_id = wiz_fields['order_id']
        model = wiz_fields['model']
        msg = ''
        if invoice_ids:
            errors = []
            ordline_obj = self.pool.get('giscedata.correos.order.line')
            invoice_obj = self.pool.get(model)

            reads = ['pending_state', 'partner_id']
            if model == 'giscedata.facturacio.factura':
                reads.append('invoice_id')
            invoices_states = invoice_obj.read(
                cursor, uid, invoice_ids, reads, context=context
            )
            params = {
                'order_id': order_id
            }

            for inv in invoices_states:
                try:
                    if model == 'account.invoice':
                        params['invoice_id'] = inv['id']
                    else:
                        params['invoice_id'] = inv['invoice_id'][0]
                    if inv['pending_state']:
                        params['invoice_state'] = inv['pending_state'][0]
                        params['origin_invoice_state'] = inv['pending_state'][0]
                    else:
                        params['invoice_state'] = False
                    params['partner_id'] = inv['partner_id'][0]

                    can_be_added = ord_obj.check_duplicated_invoices(
                        cursor, uid, order_id, params['invoice_id'],
                        context=context
                    )[0]
                    if not can_be_added:
                        raise Exception(
                            _('La factura {0} ja ha sigut afegida anteriorment'
                              ).format(params['invoice_id'])
                        )
                    inv_obj = self.pool.get('account.invoice')
                    inv_state = inv_obj.read(
                        cursor, uid, params['invoice_id'], ['state'],
                        context=context
                    )
                    if inv_state['state'] == 'draft':
                        raise Exception(
                            _('La factura {0} està en borrador i no pot ser '
                              'afegida').format(params['invoice_id'])
                        )
                    if inv_state['state'] == 'paid':
                        raise Exception(
                            _('La factura {0} està pagada i no pot ser '
                              'afegida').format(params['invoice_id'])
                        )
                    line_id = ordline_obj.create(
                        cursor, uid, params, context=context
                    )
                    if line_id:
                        ordline_obj.set_sicer(
                            cursor, uid, line_id, context=context
                        )
                except Exception as e:
                    if model == 'account.invoice':
                        errors.append((inv['id'], str(e)))
                    else:
                        errors.append((inv['invoice_id'], str(e)))
            if errors:
                msg = _('Error al crear linies per les factures:\n')
                m_name = model[-7:]
                for err in errors:
                    msg += '{0}: {1} -> {2}\n'.format(
                        m_name, str(err[0]), err[1]
                    )
            else:
                msg = _('S\'han afegit correctament totes les línies')
            self.call_recalculate_lines_sequence(cursor, uid, order_id,
                                                 context=context)
            self.call_recalculate_lines_sicer(
                cursor, uid, order_id, context=context
            )

        else:
            msg = _('Error no s\'ha seleccionat cap factura\n')
        self.write(
            cursor, uid, ids, {'info': msg, 'state': 'end'}, context=context
        )

    def call_recalculate_lines_sequence(self, cursor, uid, ord_id, context=None):
        ord_obj = self.pool.get('giscedata.correos.order')
        ord_obj.recalculate_order_lines_sequence(
            cursor, uid, ord_id, context=context
        )

    def call_recalculate_lines_sicer(self, cursor, uid, ord_id, context=None):
        ord_obj = self.pool.get('giscedata.correos.order')
        ord_obj.recalculate_order_lines_sicer(
            cursor, uid, ord_id, context=context
        )

    def return_updated_order_view(self, cursor, uid, ids, context=None):
        order_id = self.read(
            cursor, uid, ids, ['order_id'], context=context
        )[0]['order_id']
        return {
            'domain': [('id', '=', order_id)],
            'name': _('Remeses actualitzada'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.correos.order',
            'type': 'ir.actions.act_window',
            'view_id': False,
        }

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('confirm', 'Confirm'), ('end', 'End')], 'State'
        ),
        'model': fields.char('Model', size=60),
        'info': fields.text('Information', readonly=True),
        'order_id': fields.many2one(
            'giscedata.correos.order', 'Order', required=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'model': _default_model,
    }
WizardAddOrderLines()
