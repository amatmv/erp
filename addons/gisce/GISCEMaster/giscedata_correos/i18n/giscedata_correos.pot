# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 13:17\n"
"PO-Revision-Date: 2018-11-02 14:20+0000\n"
"Last-Translator: gdalmau <gdalmau@gisce.net>\n"
"Language-Team: Catalan (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_correos
#: field:wizard.load.delivery.files,has_errors:0
msgid "Errors"
msgstr "Errors"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_load_delivery_files.py:24
#, python-format
msgid ""
"Assistent per a la importació de fitxers de resultats de càrrega i "
"actualització de les línies de les remeses"
msgstr "Assistent per a la importació de fitxers de resultats de càrrega i actualització de les línies de les remeses"

#. module: giscedata_correos
#: selection:giscedata.correos.order,order_state:0
msgid "Tancada"
msgstr "Tancada"

#. module: giscedata_correos
#: code:addons/giscedata_correos/sicer_correos.py:404
#, python-format
msgid "Line {0} should start with (C, c, D, M)"
msgstr "Line {0} should start with (C, c, D, M)"

#. module: giscedata_correos
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: giscedata_correos
#: field:giscedata.correos.order,name:0
msgid "Nom"
msgstr "Nom"

#. module: giscedata_correos
#: field:giscedata.correos.order,user_id:0
msgid "Usuari"
msgstr "Usuari"

#. module: giscedata_correos
#: view:giscedata.correos.order.line:0
msgid "Informació del destinatari"
msgstr "Informació del destinatari"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "Crear Línies"
msgstr "Crear Línies"

#. module: giscedata_correos
#: view:giscedata.correos.attatchment:0
msgid "Informació adjunt"
msgstr "Informació adjunt"

#. module: giscedata_correos
#: model:ir.model,name:giscedata_correos.model_giscedata_correos_order_line
msgid "This class represents line in correos order"
msgstr "This class represents line in correos order"

#. module: giscedata_correos
#: model:ir.actions.act_window,name:giscedata_correos.action_open_correo_orders_lines_tree_invoice
#: model:ir.actions.act_window,name:giscedata_correos.action_open_correos_lines_from_energy_invoice
msgid "Línies remeses correus"
msgstr "Línies remeses correus"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,partner_street:0
msgid "Adreça"
msgstr "Adreça"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:46
#, python-format
msgid "Sense estat: {0} factures\n"
msgstr "Sense estat: {0} factures\n"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_load_delivery_files.py:165
#, python-format
msgid "Algunes linies de la remesa {0} no es poden actualitzar\n"
msgstr "Algunes linies de la remesa {0} no es poden actualitzar\n"

#. module: giscedata_correos
#: model:ir.actions.act_window,name:giscedata_correos.action_wizard_add_order_lines_factura_form
#: model:ir.actions.act_window,name:giscedata_correos.action_wizard_add_order_lines_invoice_form
msgid "Afegir a remesa de correus"
msgstr "Afegir a remesa de correus"

#. module: giscedata_correos
#: field:wizard.load.delivery.files,file_name:0
msgid "File name"
msgstr "File name"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,order_id:0
msgid "Remesa"
msgstr "Remesa"

#. module: giscedata_correos
#: model:ir.actions.act_window,name:giscedata_correos.action_open_correos_attc_full_list_tree
#: model:ir.actions.act_window,name:giscedata_correos.action_view_correos_order_attc_from_order_tree
#: model:ir.ui.menu,name:giscedata_correos.correos_attc_menu
msgid "Adjunts de correos"
msgstr "Adjunts de correos"

#. module: giscedata_correos
#: field:giscedata.correos.attatchment,order_ids:0
#: field:giscedata.correos.order,attc_ids:0
msgid "Remeses"
msgstr "Remeses"

#. module: giscedata_correos
#: field:giscedata.correos.attatchment,attachment_retorno_id:0
msgid "Fitxer de retorn SICER"
msgstr "Fitxer de retorn SICER"

#. module: giscedata_correos
#: view:giscedata.correos.order:0
msgid "Enviar"
msgstr "Enviar"

#. module: giscedata_correos
#: view:giscedata.correos.attatchment:0
msgid "Fichero de errores"
msgstr "Fichero de errores"

#. module: giscedata_correos
#: view:giscedata.correos.order.line:0
msgid "Informació línia"
msgstr "Informació línia"

#. module: giscedata_correos
#: field:giscedata.correos.attatchment,comments:0
msgid "Comentarios usuario"
msgstr "Comentarios usuario"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "Resum"
msgstr "Resum"

#. module: giscedata_correos
#: code:addons/giscedata_correos/giscedata_correos.py:143
#, python-format
msgid "Mètode check_duplicated_invoices -> {0}"
msgstr "Mètode check_duplicated_invoices -> {0}"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,invoice_id:0
msgid "Factura"
msgstr "Factura"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "Accés a la remesa"
msgstr "Accés a la remesa"

#. module: giscedata_correos
#: selection:giscedata.correos.order,order_state:0
msgid "Oberta"
msgstr "Oberta"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,partner_zip:0
msgid "Codi Postal"
msgstr "Codi Postal"

#. module: giscedata_correos
#: selection:wizard.add.order.lines,state:0
#: selection:wizard.load.delivery.files,state:0
msgid "End"
msgstr "End"

#. module: giscedata_correos
#: selection:wizard.add.order.lines,state:0
#: selection:wizard.load.delivery.files,state:0
msgid "Confirm"
msgstr "Confirm"

#. module: giscedata_correos
#: view:giscedata.correos.order:0
msgid "Llistat de remeses de correus"
msgstr "Llistat de remeses de correus"

#. module: giscedata_correos
#: code:addons/giscedata_correos/sicer_correos.py:245
#, python-format
msgid "Invalid lenght!, (product code - 2 | client code - 8)"
msgstr "Invalid lenght!, (product code - 2 | client code - 8)"

#. module: giscedata_correos
#: field:wizard.add.order.lines,state:0
#: field:wizard.load.delivery.files,state:0
msgid "State"
msgstr "State"

#. module: giscedata_correos
#: selection:giscedata.correos.order,order_state:0
msgid "Bloquejada"
msgstr "Bloquejada"

#. module: giscedata_correos
#: field:giscedata.correos.attatchment,result_import:0
msgid "Resultado de importación"
msgstr "Resultado de importación"

#. module: giscedata_correos
#: model:ir.ui.menu,name:giscedata_correos.correos_menu
msgid "Correos"
msgstr "Correos"

#. module: giscedata_correos
#: field:giscedata.correos.order,descriptive_name:0
msgid "Nom descriptiu"
msgstr "Nom descriptiu"

#. module: giscedata_correos
#: field:giscedata.correos.order,cierre_date:0
msgid "Data de tancament"
msgstr "Data de tancament"

#. module: giscedata_correos
#: code:addons/giscedata_correos/sicer_correos.py:261
#, python-format
msgid "ERROR, incorrect sicer lenght!, withoit letter should be 22"
msgstr "ERROR, incorrect sicer lenght!, withoit letter should be 22"

#. module: giscedata_correos
#: view:giscedata.correos.order:0
msgid "Remesa de correus"
msgstr "Remesa de correus"

#. module: giscedata_correos
#: field:giscedata.correos.order,n_lines:0
msgid "Total línies"
msgstr "Total línies"

#. module: giscedata_correos
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:110
#, python-format
msgid "La factura {0} està en borrador i no pot ser afegida"
msgstr "La factura {0} està en borrador i no pot ser afegida"

#. module: giscedata_correos
#: view:giscedata.correos.order.line:0
msgid "Estat de recepció"
msgstr "Estat de recepció"

#. module: giscedata_correos
#: view:giscedata.correos.attatchment:0
msgid "Remeses relacionades"
msgstr "Remeses relacionades"

#. module: giscedata_correos
#: view:wizard.load.delivery.files:0
msgid "Importar i actualitzar"
msgstr "Importar i actualitzar"

#. module: giscedata_correos
#: help:giscedata.correos.order,state:0
msgid "Estat de la remesa a l'ERP"
msgstr "Estat de la remesa a l'ERP"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,partner_id:0
msgid "Partner"
msgstr "Partner"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,origin_invoice_state:0
msgid "Estat pendent original"
msgstr "Estat pendent original"

#. module: giscedata_correos
#: model:ir.module.module,description:giscedata_correos.module_meta_information
msgid ""
"\n"
"Modulo de correos españa:\n"
"    * Calcular sicers\n"
"    * Enviar correos\n"
"    * Recibir correos\n"
"    "
msgstr "\nModulo de correos españa:\n    * Calcular sicers\n    * Enviar correos\n    * Recibir correos\n    "

#. module: giscedata_correos
#: view:wizard.load.delivery.files:0
msgid "Fitxer d'errors (linies no actualitzades)"
msgstr "Fitxer d'errors (linies no actualitzades)"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:42
#, python-format
msgid "Estat {0}: {1} factures\n"
msgstr "Estat {0}: {1} factures\n"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:133
#, python-format
msgid "S'han afegit correctament totes les línies"
msgstr "S'han afegit correctament totes les línies"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "                                                      "
msgstr "                                                      "

#. module: giscedata_correos
#: code:addons/giscedata_correos/giscedata_correos.py:88
#, python-format
msgid "Workfow Error!"
msgstr "Workfow Error!"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_load_delivery_files.py:156
#, python-format
msgid ""
"La línia amb codi sicer {sicer} de la factura {fact} té un estat "
"d'impagament diferent de {state}.\n"
msgstr "La línia amb codi sicer {sicer} de la factura {fact} té un estat d'impagament diferent de {state}.\n"

#. module: giscedata_correos
#: help:giscedata.correos.order,descriptive_name:0
msgid "Nom indicatiu per l'usuari (No se'n fara cap ús)"
msgstr "Nom indicatiu per l'usuari (No se'n fara cap ús)"

#. module: giscedata_correos
#: field:wizard.load.delivery.files,err_fname:0
msgid "Error file name"
msgstr "Error file name"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "Selecció de remesa"
msgstr "Selecció de remesa"

#. module: giscedata_correos
#: code:addons/giscedata_correos/sicer_correos.py:272
#, python-format
msgid "ERROR, Client sequence code lenght error!, should be 8"
msgstr "ERROR, Client sequence code lenght error!, should be 8"

#. module: giscedata_correos
#: field:giscedata.correos.order,order_lines:0
#: model:ir.actions.act_window,name:giscedata_correos.action_open_correo_orders_lines_tree
msgid "Línies de la remesa"
msgstr "Línies de la remesa"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:126
#, python-format
msgid "Error al crear linies per les factures:\n"
msgstr "Error al crear linies per les factures:\n"

#. module: giscedata_correos
#: code:addons/giscedata_correos/giscedata_correos.py:440
#, python-format
msgid "Cal configurar la direccio de notificació per a la factura {0}"
msgstr "Cal configurar la direccio de notificació per a la factura {0}"

#. module: giscedata_correos
#: help:giscedata.correos.order,order_state:0
msgid "Estat de la remesa segons correus"
msgstr "Estat de la remesa segons correus"

#. module: giscedata_correos
#: view:wizard.load.delivery.files:0
msgid "Selecció de fitxer d'importació"
msgstr "Selecció de fitxer d'importació"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:141
#, python-format
msgid "Error no s'ha seleccionat cap factura\n"
msgstr "Error no s'ha seleccionat cap factura\n"

#. module: giscedata_correos
#: selection:giscedata.correos.order,state:0
msgid "Esborrany"
msgstr "Esborrany"

#. module: giscedata_correos
#: model:ir.model,name:giscedata_correos.model_wizard_add_order_lines
msgid "Wizard to add order lines into correos orders"
msgstr "Wizard to add order lines into correos orders"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:52
#, python-format
msgid "Model invalid"
msgstr "Model invalid"

#. module: giscedata_correos
#: view:giscedata.correos.order:0
msgid "Finalitzar"
msgstr "Finalitzar"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0 view:wizard.load.delivery.files:0
msgid "Cancel·lar"
msgstr "Cancel·lar"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,partner_city:0
msgid "Ciutat"
msgstr "Ciutat"

#. module: giscedata_correos
#: view:giscedata.correos.attatchment:0
msgid "Adjunts relacionats remesa (Correos)"
msgstr "Adjunts relacionats remesa (Correos)"

#. module: giscedata_correos
#: model:ir.module.module,shortdesc:giscedata_correos.module_meta_information
msgid "Giscedata correos"
msgstr "Giscedata correos"

#. module: giscedata_correos
#: view:wizard.load.delivery.files:0
msgid "Importació i tractament de fitxers de resultats"
msgstr "Importació i tractament de fitxers de resultats"

#. module: giscedata_correos
#: selection:giscedata.correos.order,state:0
msgid "Enviament"
msgstr "Enviament"

#. module: giscedata_correos
#: view:giscedata.correos.order.line:0
msgid "Línia de remesa de correus"
msgstr "Línia de remesa de correus"

#. module: giscedata_correos
#: model:ir.model,name:giscedata_correos.model_giscedata_correos_attatchment
msgid "Adjunts de sicer"
msgstr "Adjunts de sicer"

#. module: giscedata_correos
#: view:giscedata.correos.order.line:0
msgid "Llista de les línies de la remesa"
msgstr "Llista de les línies de la remesa"

#. module: giscedata_correos
#: field:wizard.load.delivery.files,err_file:0
msgid "Fitxer d'errors"
msgstr "Fitxer d'errors"

#. module: giscedata_correos
#: code:addons/giscedata_correos/giscedata_correos.py:142
#, python-format
msgid "Error"
msgstr "Error"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:164
#, python-format
msgid "Remeses actualitzada"
msgstr "Remeses actualitzada"

#. module: giscedata_correos
#: field:giscedata.correos.order,state:0
msgid "Estat de la remesa"
msgstr "Estat de la remesa"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,invoice_state:0
msgid "Estat pendent actual"
msgstr "Estat pendent actual"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,delivery_status:0
msgid "Estat de l'enviament"
msgstr "Estat de l'enviament"

#. module: giscedata_correos
#: field:wizard.add.order.lines,info:0
msgid "Information"
msgstr "Information"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,reception_date:0
msgid "Data de recepció"
msgstr "Data de recepció"

#. module: giscedata_correos
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscedata_correos
#: help:giscedata.correos.order.line,origin_invoice_state:0
msgid "Estat original en que es va afegir la factura"
msgstr "Estat original en que es va afegir la factura"

#. module: giscedata_correos
#: selection:wizard.add.order.lines,state:0
#: selection:wizard.load.delivery.files,state:0
msgid "Init"
msgstr "Init"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "Afegir línies a la remesa de correus"
msgstr "Afegir línies a la remesa de correus"

#. module: giscedata_correos
#: field:wizard.load.delivery.files,info:0
msgid "Informació"
msgstr "Informació"

#. module: giscedata_correos
#: selection:giscedata.correos.order,state:0
msgid "Tancat"
msgstr "Tancat"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:27
#, python-format
msgid "S'afegiran {0} factures a la remesa:\n"
msgstr "S'afegiran {0} factures a la remesa:\n"

#. module: giscedata_correos
#: view:giscedata.correos.order:0 view:wizard.add.order.lines:0
#: view:wizard.load.delivery.files:0
msgid "Tancar"
msgstr "Tancar"

#. module: giscedata_correos
#: code:addons/giscedata_correos/sicer_correos.py:264
#, python-format
msgid "ERROR, incorrect format!, should be str"
msgstr "ERROR, incorrect format!, should be str"

#. module: giscedata_correos
#: code:addons/giscedata_correos/sicer_correos.py:269
#, python-format
msgid "ERROR, Remittance code lenght error!, should be 4"
msgstr "ERROR, Remittance code lenght error!, should be 4"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_load_delivery_files.py:211
#, python-format
msgid "Línies processades correctement"
msgstr "Línies processades correctement"

#. module: giscedata_correos
#: field:giscedata.correos.attatchment,attachment_errors_id:0
msgid "Fitxer d'errors d'importació"
msgstr "Fitxer d'errors d'importació"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,delivered:0
msgid "Entregat?"
msgstr "Entregat?"

#. module: giscedata_correos
#: model:ir.actions.act_window,name:giscedata_correos.action_wizard_load_delivery_files_form
#: model:ir.ui.menu,name:giscedata_correos.wiz_import_loads_results
msgid "Importació de fitxers de resultats"
msgstr "Importació de fitxers de resultats"

#. module: giscedata_correos
#: field:giscedata.correos.order,order_state:0
msgid "Estat de la remesa correos"
msgstr "Estat de la remesa correos"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_load_delivery_files.py:285
#: view:wizard.load.delivery.files:0
#, python-format
msgid "Remeses actualitzades"
msgstr "Remeses actualitzades"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,sicer:0
msgid "Codi Sicer"
msgstr "Codi Sicer"

#. module: giscedata_correos
#: model:ir.model,name:giscedata_correos.model_wizard_load_delivery_files
msgid "Allow's user to load delivery files and update orders"
msgstr "Allow's user to load delivery files and update orders"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_add_order_lines.py:100
#, python-format
msgid "La factura {0} ja ha sigut afegida anteriorment"
msgstr "La factura {0} ja ha sigut afegida anteriorment"

#. module: giscedata_correos
#: code:addons/giscedata_correos/giscedata_correos.py:89
#, python-format
msgid "Can't change state from {0} to {1}"
msgstr "Can't change state from {0} to {1}"

#. module: giscedata_correos
#: view:wizard.add.order.lines:0
msgid "Següent"
msgstr "Següent"

#. module: giscedata_correos
#: field:wizard.add.order.lines,model:0
msgid "Model"
msgstr "Model"

#. module: giscedata_correos
#: field:giscedata.correos.attatchment,attachment_name:0
msgid "Nombre adjunto"
msgstr "Nombre adjunto"

#. module: giscedata_correos
#: field:wizard.add.order.lines,order_id:0
msgid "Order"
msgstr "Order"

#. module: giscedata_correos
#: field:giscedata.correos.order,sending_date:0
msgid "Data d'enviament"
msgstr "Data d'enviament"

#. module: giscedata_correos
#: field:giscedata.correos.order.line,sequence:0
msgid "Sequence"
msgstr "Sequence"

#. module: giscedata_correos
#: code:addons/giscedata_correos/wizard/wizard_load_delivery_files.py:199
#, python-format
msgid "Remesa {0} no trobada\n"
msgstr "Remesa {0} no trobada\n"

#. module: giscedata_correos
#: model:ir.model,name:giscedata_correos.model_giscedata_correos_order
msgid "This class represents an order of invoices to be sent by correos"
msgstr "This class represents an order of invoices to be sent by correos"

#. module: giscedata_correos
#: view:giscedata.correos.order:0
msgid "Informació de la remesa"
msgstr "Informació de la remesa"

#. module: giscedata_correos
#: code:addons/giscedata_correos/giscedata_correos.py:446
#, python-format
msgid ""
"Cal configurar la direccio del client {0} amb vat {1} associat a la factura "
"{2}"
msgstr "Cal configurar la direccio del client {0} amb vat {1} associat a la factura {2}"

#. module: giscedata_correos
#: model:ir.actions.act_window,name:giscedata_correos.action_open_correo_orders_tree
#: model:ir.ui.menu,name:giscedata_correos.menu_correos_order
msgid "Remeses de correus"
msgstr "Remeses de correus"

#. module: giscedata_correos
#: help:wizard.load.delivery.files,file:0
msgid "Fitxer de resultats de càrrega"
msgstr "Fitxer de resultats de càrrega"

#. module: giscedata_correos
#: field:wizard.load.delivery.files,file:0
msgid "File"
msgstr "File"

#. module: giscedata_correos
#: selection:giscedata.correos.order,state:0
msgid "Finalitzada"
msgstr "Finalitzada"

#. module: giscedata_correos
#: help:wizard.load.delivery.files,err_file:0
msgid "Fitxer errors"
msgstr "Fitxer errors"

#. module: giscedata_correos
#: view:giscedata.correos.attatchment:0
msgid "Comentaris adicionals"
msgstr "Comentaris adicionals"
