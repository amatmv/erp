# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from os import getpid, remove, path
from tempfile import TemporaryFile
from datetime import datetime
from base64 import b64encode, b64decode


class TestsWizardLoadDeliveryFile(testing.OOTestCase):
    def get_poliza_ready(self, cursor, uid, polissa_id=False):
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1] if not polissa_id else polissa_id

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        partner = polissa_obj.read(
            cursor, uid, polissa_id, ['titular']
        )['titular'][0]

        # Set vat as no enterprise
        res_obj = self.openerp.pool.get('res.partner')
        res_obj.write(cursor, uid, partner, {'vat': 'ES71895522Y'})

        cnae_9820 = imd_obj.get_object_reference(
            cursor, uid, 'giscemisc_cnae', 'cnae_9820'
        )[1]

        # Here current owner vat is enterprise and power is under 10 kW
        polissa_obj.write(
            cursor, uid, polissa_id,
            {
                'cnae': cnae_9820, 'tipus_vivenda': 'habitual',

            }
        )

    def create_demo_lines_in_demo_orders(self, cursor, uid):
        imd_obj = self.openerp.pool.get('ir.model.data')
        w_add_lines = self.openerp.pool.get('wizard.add.order.lines')
        order_obj = self.openerp.pool.get('giscedata.correos.order')

        # Demo oreder
        order_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0001'
        )[1]


        # Demo facturas (0001...0007)

        fact_ids = [
            imd_obj.get_object_reference(cursor, uid, 'giscedata_facturacio',
                                         'factura_000{0}'.format(idx))[1]
            for idx in range(1, 8)
        ]

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        # p_id = imd_obj.get_object_reference(
        #     cursor, uid,
        #     'giscedata_facturacio_comer_bono_social',
        #     'carta_1_pendent_pending_state'
        # )[1]

        fact_obj.write(cursor, uid, fact_ids, {'state': 'open'})

        # fact_obj.invoice_open(cursor, uid, fact_ids)

        context = {
            'active_ids': fact_ids,
            'model': 'giscedata.facturacio.factura'
        }
        wiz_ord_id = w_add_lines.create(
            cursor, uid, {'order_id': order_id}, context=context
        )

        wiz = w_add_lines.browse(cursor, uid, wiz_ord_id, context=context)
        wiz.confirm_state(context=context)
        wiz.add_lines_to_order(context=context)
        lines = order_obj.read(
            cursor, uid, order_id, ['order_lines', 'name'], context=context
        )
        self.order_code = lines['name'][-4:]
        return order_id, lines['order_lines']

    def set_client_code_config(self, cursor, uid):
        res_obj = self.openerp.pool.get('res.config')
        # Init client code config var str(size=8) ex. 99999999
        cli_cod = '12345678'
        conf_id = res_obj.search(cursor, uid,
                                 [('name', '=', 'sicer_partner_code')]
                                 )
        res_obj.write(cursor, uid, conf_id, {'value': cli_cod})
        self.client_code = cli_cod

    def get_invoice_ids_from_facturas(self, cursor, uid, fact_ids):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        return [
            record['invoice_id']
            for record in fact_obj.read(cursor, uid, fact_ids, ['invoice_id'])
        ]

    def set_pending_states_in_demo_facturas(self, cursor, uid, fact_ids, invoice=False):
        pen_obj = self.openerp.pool.get('account.invoice.pending.state')
        # Get some pending state id, for the moment doesn't care the type
        # TODO change in future when states implemented
        pend_id = pen_obj.search(cursor, uid, [])[0]
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pend_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio_comer_bono_social',
            'carta_1_pendent_pending_state'
        )[1]
        if not invoice:
            inv_ids = self.get_invoice_ids_from_facturas(cursor, uid, fact_ids)
        else:
            inv_ids = fact_ids
        inv_obj.set_pending(cursor, uid, inv_ids, pend_id)

    def create_tmp_file_to_import_and_check_changes(self, cursor, uid, cheat=False):
        # cheat var as True generate a wrong file to test errors change
        # fill order headers with worng order code then any line in order will
        # be processed

        # This method create a tmp file to be imported in wizard load delivery
        # file to test if lines are correctly updated with file info
        line_obj = self.openerp.pool.get('giscedata.correos.order.line')
        filename = '/tmp/CD12345678FC20180123.8003.%s' % getpid()
        self.file_name = filename.replace('/tmp/', '')
        res = ''
        temp = open(filename, 'w+b')
        try:
            order_id = self.order_id
            order_lines = self.line_ids
            client_code = self.client_code
            codigo_oficina = '7777777'
            fecha_situacion = fecha_emision = fecha_deposito = send_date = datetime.now().strftime('%Y%m%d%H:%M')
            n_orders = '1'.zfill(3)
            n_details = str(len(order_lines)).zfill(9)
            if cheat:
                # Fake order code, it does't exist
                order_code = '800X'
            else:
                order_code = self.order_code

            # ex FCD1234567877777772018021301:24
            file_header = 'FCD{0}{1}{2}'.format(
                client_code, codigo_oficina, send_date
            ).ljust(92)
            # ex.fCD12345678001000000007000000000000
            file_footer = 'fcd{0}{1}{2}'.format(
                client_code, n_orders, n_details
            ).ljust(92)

            # ex. CCD1234567880012018020715:32        A20180206
            order_header = 'CCD{0}{1}{2}{3}A{4}'.format(
                client_code, order_code, fecha_deposito,
                ''.ljust(8), fecha_emision[:-5]
            ).ljust(92)

            # ex. cCD123456788001000000006
            order_footer = 'cCD{0}{1}{2}'.format(
                client_code, order_code, n_details
            ).ljust(92)

            temp.write(file_header)
            temp.write('\n')
            temp.write(order_header)
            temp.write('\n')

            line_reads = ['sicer']
            codigo_situacion = '18'

            for line in line_obj.read(cursor, uid, order_lines, line_reads):
                l_detail = 'DF{0}{1}{2}{3}{4}'.format(
                    line['sicer'], codigo_situacion, fecha_situacion[:-5],
                    codigo_oficina, line['sicer']
                ).ljust(92)
                temp.write(l_detail)
                temp.write('\n')
            temp.write(order_footer)
            temp.write('\n')
            temp.write(file_footer)
            temp.write('\n')

            # Allows read after write file
            temp.seek(0)

            res = temp.read()

        finally:
            temp.close()
            # Clean up the temporary file yourself
            remove(filename)
        return res

    def setUp(self):
        self.txn = Transaction().start(self.database)
        cursor = self.txn.cursor
        uid = self.txn.user
        # Sets config var (client code)
        self.set_client_code_config(cursor, uid)
        # Create order with lines from factures
        # Store order_id and line_ids in self to avoid multiple readings in
        # tests
        self.get_poliza_ready(cursor, uid)
        self.order_id, self.line_ids = self.create_demo_lines_in_demo_orders(
            cursor, uid
        )

    def tearDown(self):
        self.txn.stop()

    def test_update_lines_on_return_correos_file(self):
        line_obj = self.openerp.pool.get('giscedata.correos.order.line')
        wiz_load_obj = self.openerp.pool.get('wizard.load.delivery.files')
        uid = self.txn.user
        cursor = self.txn.cursor
        pool = self.openerp.pool

        order_id = self.order_id
        lines_ids = self.line_ids

        r_fields = ['sicer', 'delivery_status', 'reception_date', 'delivered', 'invoice_id']
        lines_to_check_fields = line_obj.read(cursor, uid, lines_ids, r_fields)

        invoice_ids = [r['invoice_id'][0] for r in lines_to_check_fields]

        self.set_pending_states_in_demo_facturas(
            cursor, uid, invoice_ids, invoice=True
        )

        # Check if initial values are false, to check after if it change
        # If next line fails is because fore something values are set
        self.assertTrue(
            all(not rec['delivery_status'] and not rec['reception_date']
                for rec in lines_to_check_fields)
        )

        str_file = self.create_tmp_file_to_import_and_check_changes(cursor, uid)

        wiz_id = wiz_load_obj.create(cursor, uid, {'file': b64encode(str_file), 'file_name': self.file_name, 'err_fname': '{}{}'.format('errors-', self.file_name)})
        # print wiz_load_obj.read(cursor, uid , wiz_id, ['file'])
        wiz = wiz_load_obj.browse(cursor, uid, wiz_id)

        # Update order lines
        wiz.update_lines()
        # TODO solve problems if a date value is void in file

        lines_to_check_fields = line_obj.read(cursor, uid, lines_ids, r_fields)

        self.assertTrue(
            all([record['delivery_status'] == '18' and record['delivered'] and record['reception_date']
                 for record in lines_to_check_fields])
        )

        # For the moment force brake test
        #self.assertTrue(False)

    def test_generate_csv_if_some_order_can_be_processed(self):
        line_obj = self.openerp.pool.get('giscedata.correos.order.line')
        wiz_load_obj = self.openerp.pool.get('wizard.load.delivery.files')
        uid = self.txn.user
        cursor = self.txn.cursor

        # Generate wrong file
        str_file = self.create_tmp_file_to_import_and_check_changes(
            cursor, uid, True
        )

        wiz_id = wiz_load_obj.create(
            cursor, uid,
            {
                'file': b64encode(str_file),
                'file_name': self.file_name,
                'err_fname': '{}{}'.format('errors-', self.file_name)
            })
        # print wiz_load_obj.read(cursor, uid , wiz_id, ['file'])
        wiz = wiz_load_obj.browse(cursor, uid, wiz_id)

        # Update order lines
        wiz.update_lines()

        sicers = line_obj.read(cursor, uid, self.line_ids, ['sicer'])

        sicers = [l['sicer'] for l in reversed(sicers)]

        t_fname = 'errors_csv_return_file_process.csv'
        pathh = path.dirname(path.realpath(__file__)) + '/fixtures/{0}'.format(
            t_fname)
        with open(pathh, "r") as myfile:
            data = myfile.read().format(
                order_code=self.order_code,
                sicer_1=sicers[0],
                sicer_2=sicers[1],
                sicer_3=sicers[2],
                sicer_4=sicers[3],
                sicer_5=sicers[4],
                sicer_6=sicers[5],
                sicer_7=sicers[6],
            ).replace('\n', '\r\n')
            # Convert test file to windows format end lines
        self.assertEqual(b64decode(wiz.err_file), data)
