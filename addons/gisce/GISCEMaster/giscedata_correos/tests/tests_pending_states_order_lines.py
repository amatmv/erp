# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import netsvc
from addons import get_module_resource
from base64 import b64encode
from datetime import datetime
from json import loads


class CommonMethodsTest(testing.OOTestCase):
    def create_fiscal_year(self, cursor, uid, fiscal_year, context=None):
        fyear_obj = self.openerp.pool.get('account.fiscalyear')
        fy_id = fyear_obj.create(cursor, uid, {
            'name': fiscal_year,
            'code': fiscal_year,
            'date_start': '%s-01-01' % fiscal_year,
            'date_stop': '%s-12-31' % fiscal_year
        })
        fyear_obj.create_period(cursor, uid, [fy_id])

    def activar_polissa(self, cursor, uid, polissa_id, context=None):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol = pol_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['validar', 'contracte'])

    def create_modcon_with_nocutoff_pending_process(
            self, cursor, uid, polissa_id=None, date_from=None, date_to=None, context=None):

        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        if not date_from:
            date_from = datetime(year=2017, month=2, day=1).strftime('%Y-%m-%d')

        if not date_to:
            date_to = datetime.today().strftime('%Y-%m-%d')

        if not polissa_id:
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

        nocutoff_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'd8_nocutoff'
        )[1]

        nocutoff_process_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'nocutoff_pending_state_process'
        )[1]

        pol = pol_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])

        pol.write(
            {'nocutoff': nocutoff_id}
        )

        wz_crear_mc_obj = self.openerp.pool.get(
            'giscedata.polissa.crear.contracte'
        )

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )

        mod_create = {
            'data_inici': date_from,
            'data_final': date_to
        }

        mod_create.update(res.get('value', {}))

        wiz_mod.write(mod_create)

        wiz_mod.action_crear_contracte(ctx)

        res = pol_obj.read(
            cursor, uid, polissa_id, ['process_id', 'nocutoff']
        )
        self.assertEqual(res['nocutoff'][0], nocutoff_id)
        self.assertEqual(res['process_id'][0], nocutoff_process_id)

    def generate_manual_invoice(self, cursor, uid, polissa_id, date_start, date_end, journal_id):
        imd_obj = self.openerp.pool.get('ir.model.data')
        wiz_fact_manual_obj = self.openerp.pool.get('wizard.manual.invoice')
        wiz_fact_manual_id = wiz_fact_manual_obj.create(cursor, uid, vals={
            'polissa_id': polissa_id,
            'date_start': date_start,
            'date_end': date_end,
            'date_invoice': datetime.today().strftime('%Y-%m-%d'),
            'journal_id': journal_id
        })

        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        context = {'llista_preu': pricelist_id}

        #   -   Create regular invoice within ExtraLine's dates

        wiz_fact_manual_obj.action_manual_invoice(
            cursor, uid, [wiz_fact_manual_id], context)

        fact_id = wiz_fact_manual_obj.read(
            cursor, uid, wiz_fact_manual_id, ['invoice_ids']
        )[0]['invoice_ids']
        fact_id = loads(fact_id)[0]

        return fact_id

    def set_client_code_config(self, cursor, uid):
        res_obj = self.openerp.pool.get('res.config')
        # Init client code config var str(size=8) ex. 99999999
        cli_cod = '12345678'
        conf_id = res_obj.search(cursor, uid,
                                 [('name', '=', 'sicer_partner_code')]
                                 )
        res_obj.write(cursor, uid, conf_id, {'value': cli_cod})
        self.client_code = cli_cod

    def get_invoice_ids_from_facturas(self, cursor, uid, fact_ids):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        if isinstance(fact_ids, (int, long)):
            fact_ids = [fact_ids]
        return [
            record['invoice_id'][0]
            for record in fact_obj.read(cursor, uid, fact_ids, ['invoice_id'])
        ]

    def get_poliza_ready_fror_bono_social_disponible(self, cursor, uid, polissa_id):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner = polissa_obj.read(
            cursor, uid, polissa_id, ['titular']
        )['titular'][0]

        # Set vat as no enterprise
        res_obj = self.openerp.pool.get('res.partner')
        res_obj.write(cursor, uid, partner, {'vat': 'ES71895522Y'})

        cnae_9820 = imd_obj.get_object_reference(
            cursor, uid, 'giscemisc_cnae', 'cnae_9820'
        )[1]

        # Here current owner vat is enterprise and power is under 10 kW
        polissa_obj.write(
            cursor, uid, polissa_id,
            {'cnae': cnae_9820, 'tipus_vivenda': 'habitual', 'state': 'activa'}
        )

        bono_value = polissa_obj.read(
            cursor, uid, polissa_id, ['bono_social_disponible']
        )['bono_social_disponible']

        self.assertTrue(bono_value)


class TestsPendingStatesBonoSocial(CommonMethodsTest):
    def create_demo_lines_in_demo_orders(self, cursor, uid):
        imd_obj = self.openerp.pool.get('ir.model.data')
        w_add_lines = self.openerp.pool.get('wizard.add.order.lines')
        order_obj = self.openerp.pool.get('giscedata.correos.order')

        # Demo oreder
        order_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0001'
        )[1]

        demo_pay_type_id = imd_obj.get_object_reference(
            cursor, uid, 'account_payment', 'payment_mode_demo'
        )[1]

        # Demo facturas (0001...0008)

        rang = range(1, 9)
        rang.remove(3)

        fact_ids = [
            imd_obj.get_object_reference(cursor, uid, 'giscedata_facturacio',
                                         'factura_000{0}'.format(idx))[1]
            for idx in rang
        ]

        self.factures = fact_ids

        inv_ids = self.get_invoice_ids_from_facturas(cursor, uid, fact_ids)

        wf_service = netsvc.LocalService("workflow")

        for inv_id in inv_ids:
            wf_service.trg_validate(
                uid, 'account.invoice', inv_id[0],
                'invoice_open', cursor
            )

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_obj.write(cursor, uid, fact_ids, {'state': 'open', 'payment_mode_id': demo_pay_type_id})

        context = {
            'active_ids': fact_ids,
            'model': 'giscedata.facturacio.factura'
        }
        wiz_ord_id = w_add_lines.create(
            cursor, uid, {'order_id': order_id}, context=context
        )

        wiz = w_add_lines.browse(cursor, uid, wiz_ord_id, context=context)
        wiz.confirm_state(context=context)
        wiz.add_lines_to_order(context=context)
        lines = order_obj.read(
            cursor, uid, order_id, ['order_lines', 'name'], context=context
        )
        self.order_code = lines['name'][-4:]
        return order_id, lines['order_lines']

    def set_pending_states_in_demo_facturas(self, cursor, uid, fact_ids):
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')

        # 0001 -> correcte
        # 0002 -> Pendent carta 1 enviada
        # 0003 -> Carta 1
        # 0004 -> Pendiente carta 2
        # 0005 -> Carta 2
        # 0006 -> Aviso
        # 0007 -> Pendiente carta aviso de corte
        # 0008 -> Carta aviso de corte

        # Don't modify ids_ref order!!!!!!

        ids_ref = [
                   'pendent_carta_1_enviada_pending_state',
                   'carta_1_pending_state',
                   'carta_2_pendent_pending_state',
                   'carta_2_pending_state',
                   'avis_tall_pending_state',
                   'pendent_carta_avis_tall_pending_state',
                   'carta_avis_tall_pending_state'
                   ]

        inv_ids = self.get_invoice_ids_from_facturas(cursor, uid, fact_ids)

        for inv_id, id_ref in zip(inv_ids, ids_ref):

            module = 'giscedata_facturacio_comer_bono_social'

            if id_ref == 'pendent_carta_1_enviada_pending_state':
                module = 'giscedata_correos_bono_social'

            pending_state = imd_obj.get_object_reference(
                cursor, uid, module, id_ref
            )[1]

            inv_obj.set_pending(cursor, uid, inv_id[0], pending_state)

        self.invoices = inv_ids

    def setUp(self):
        self.txn = Transaction().start(self.database)
        cursor = self.txn.cursor
        uid = self.txn.user
        # Sets config var (client code)
        self.set_client_code_config(cursor, uid)
        self.order_id, self.line_ids = self.create_demo_lines_in_demo_orders(
            cursor, uid
        )
        self.set_pending_states_in_demo_facturas(cursor, uid, self.factures)

    def tearDown(self):
        self.txn.stop()


class TestsPendingStatesNocutoff(CommonMethodsTest):

    def set_pending_states_in_demo_facturas(self, cursor, uid, fact_ids):
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')

        pendiente_carta_5 = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_suministros_nocutoff',
            'carta_5_pendiente_envio_nocutoff_pending_state'
        )

        pendiente_carta_6 = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_suministros_nocutoff',
            'carta_6_pendiente_envio_nocutoff_pending_state'
        )

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.openerp.install_module('giscedata_correos_suministros_nocutoff')

        cursor = self.txn.cursor
        uid = self.txn.user

        self.set_client_code_config(cursor, uid)
        self.order_id, self.line_ids = self.create_demo_lines_in_demo_orders(
            cursor, uid
        )
        self.set_pending_states_in_demo_facturas(cursor, uid, self.factures)

    def tearDown(self):
        self.txn.stop()


class TestsValidationWizardLoadDeliveryFile(CommonMethodsTest):
    """
    Classe test para comprovar que se hagan correctamente las validaciones
    durante la importación del fichero de resultados y que solo actualize los
    estados pendientes cuando toque.
    """

    def add_one_line_to_order_with_pending_state(self, robinson_cursor, uid, pending_state):
        """
        Comprueba que dada una linea a Carta 1 enviada al sicer, si llega un
        codigo de perdida para ser actualizada, vuelve atrás.
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        w_add_lines = self.openerp.pool.get('wizard.add.order.lines')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        order_obj = self.openerp.pool.get('giscedata.correos.order')
        line_obj = self.openerp.pool.get('giscedata.correos.order.line')
        inv_obj = self.openerp.pool.get('account.invoice')

        cursor = robinson_cursor
        order_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0001'
        )[1]

        fact_1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]

        fact_obj.write(cursor, uid, fact_1_id, {'state': 'open'})

        invoice_id = fact_obj.read(cursor, uid, fact_1_id, ['invoice_id'])['invoice_id'][0]

        wf_service = netsvc.LocalService("workflow")
        wf_service.trg_validate(
            uid, 'account.invoice', invoice_id, 'invoice_open', cursor
        )
        inv_obj.set_pending(cursor, uid, invoice_id, pending_state)

        context = {
            'active_ids': [fact_1_id],
            'model': 'giscedata.facturacio.factura'
        }
        wiz_ord_id = w_add_lines.create(
            cursor, uid, {'order_id': order_id}, context=context
        )

        wiz = w_add_lines.browse(cursor, uid, wiz_ord_id, context=context)
        wiz.confirm_state(context=context)
        wiz.add_lines_to_order(context=context)

        lines = order_obj.read(
            cursor, uid, order_id, ['order_lines', 'name'], context=context
        )
        self.order_code = lines['name'][-4:]

        self.sicer = line_obj.read(
            cursor, uid, lines['order_lines'][0], ['sicer']
        )['sicer']

        invoice_ids = self.get_invoice_ids_from_facturas(
            cursor, uid, [fact_1_id]
        )

        pending_invoice = inv_obj.read(
            cursor, uid, invoice_ids[0], ['pending_state']
        )['pending_state'][0]

        self.assertEqual(pending_state, pending_invoice)

        self.invoice_id = invoice_ids[0]

    def setUp(self):
        self.txn = Transaction().start(self.database)
        cursor = self.txn.cursor
        uid = self.txn.user
        # Sets config var (client code)
        self.set_client_code_config(cursor, uid)

    def tearDown(self):
        self.txn.stop()

    def test_carta_perdida_carta_1_enviada_sicer(self):
        uid = self.txn.user
        cursor = self.txn.cursor

        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')
        wiz_load_obj = self.openerp.pool.get('wizard.load.delivery.files')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        self.get_poliza_ready_fror_bono_social_disponible(
            cursor, uid, polissa_id
        )

        # Carta 1 enviada SICER
        pending_state = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos_bono_social',
            'pendent_carta_1_enviada_pending_state'
        )[1]

        self.add_one_line_to_order_with_pending_state(
            cursor, uid, pending_state
        )

        invoice_id = self.invoice_id

        file_name = 'delivery_file_carta_perdida.04'

        file_path = get_module_resource(
            'giscedata_correos', 'tests', 'fixtures',
            file_name
        )
        with open(file_path, 'r') as f:
            str_file = f.read()

        wiz_id = wiz_load_obj.create(
            cursor, uid,
            {
                'file': b64encode(
                    str_file.format(
                        sicer_line=self.sicer, comp_code=12345678,
                        order_code=int(self.order_code)
                    )
                ),
                'file_name': file_name

            }
        )
        wiz = wiz_load_obj.browse(cursor, uid, wiz_id)

        # Update order lines
        wiz.update_lines()

        expected_pending_state_to = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'carta_1_pendent_pending_state'
        )[1]

        pending_invoice = inv_obj.read(
            cursor, uid, invoice_id, ['pending_state']
        )['pending_state'][0]

        self.assertEqual(expected_pending_state_to, pending_invoice)
