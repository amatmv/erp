# -*- coding: utf-8 -*-
from giscedata_correos.sicer_correos import SicerProduct, NOTIFICATION_RESULTS_END_FILE, NOTIFICATION_RESULTS_FILE_HEADER, NOTIFICATION_RESULTS_ORDER_END, NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS, NOTIFICATION_RESULTS_ORDER_HEADER
from unittest import TestCase
from datetime import date, datetime
from os import path
from base64 import b64encode
from retrofix import record


class TestsSicerProduct(TestCase):

    def test_correct_letter_generation(self):

        sicer_no_letter = 'PE08000536165000755542'
        expected_sicer = 'PE08000536165000755542K'
        expected_letter = 'K'
        codi_producte = 'PE'
        codi_client = '08000536'
        remesa = '1650'
        seq_cli = '00755542'

        # Crear producte cifer amb codi prod i codi cli

        sicer_prod = SicerProduct(codi_producte, codi_client)

        # Calculate letter
        letter = sicer_prod.get_national_checksum(sicer_no_letter)
        assert (letter == expected_letter)

        # Get all sicer
        complet_sicer = sicer_prod.get_sicer(remesa, seq_cli)
        assert (complet_sicer == expected_sicer)


class TestsFilesInterpretation(TestCase):

    def test_interprete_correctly_fichero_de_resultados(self):
        # IF TEST FAILS SET self.maxDiff AS None AT START OF THIS TEST TO VIEW
        # FULL DIFFS WITH DICTS

        # Ejemplo de fichero de fin de remesa
        t_fname = 'CD12345678FC20180123.8003'

        expected_file_header = u'FCN1234567877777772018021112:25C'

        expected_file_header = record.Record.extract(
            expected_file_header, NOTIFICATION_RESULTS_FILE_HEADER
        )

        expected_file_footer = u'fCD12345678001000000005'

        expected_file_footer = record.Record.extract(
            expected_file_footer, NOTIFICATION_RESULTS_END_FILE
        )

        expected_order_header = (
            u'CCD1234567880032018012213:1420180215C20180219' + u''.ljust(47)
        )
        expected_order_header = record.Record.extract(
            expected_order_header, NOTIFICATION_RESULTS_ORDER_HEADER
        )


        expected_details = [
            u'DFCD12345678800300000002V18201802177777777EJEMPLODEINFORMACIONINTERNADELCLIENTEPEPE' + u''.ljust(9),
            u'DFCD12345678800300000003H18201802177777777EJEMPLODEINFORMACIONINTERNADELCLIENTEPACO' + u''.ljust(9),
            u'DFCD12345678800300000004L18201802177777777EJEMPLODEINFORMACIONINTERNADELCLIENTELOOP' + u''.ljust(9)
        ]
        expected_details = [
            record.Record.extract(
                l, NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS
            ) for l in expected_details
        ]

        expected_mods = [
            u'MFCD12345678800300000005C18201802177777777EJEMPLODEINFORMACIONINTERNADELCLIENTESAMI',
            u'MFCD12345678800300000006K18201802177777777EJEMPLODEINFORMACIONINTERNADELCLIENTELAIK'
        ]

        expected_mods = [
            record.Record.extract(
                l, NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS
            ) for l in expected_mods
        ]

        expected_order_footer = u'cCD123456788003000000005'
        expected_order_footer = record.Record.extract(
            expected_order_footer, NOTIFICATION_RESULTS_ORDER_END
        )

        expected_dict = [
            {'order_header': expected_order_header,
             'details': expected_details,
             'modifications': expected_mods,
             'order_footer': expected_order_footer
             },
        ]

        expected_res = [
            {
                'file_header': expected_file_header,
                'file_orders': expected_dict,
                'file_footer': expected_file_footer,
            },
        ]

        pathh = path.dirname(path.realpath(__file__)) + '/fixtures/{0}'.format(t_fname)
        with open(pathh, "r") as myfile:
            data = myfile.read()
        data = b64encode(data)
        res = SicerProduct.interpret_retorno_notificaciones_sin_reembolso_file(data)
        self.assertDictEqual(res[0], expected_res[0])


