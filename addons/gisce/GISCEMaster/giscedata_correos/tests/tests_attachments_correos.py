# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from base64 import b64encode, b64decode
from StringIO import StringIO
from addons import get_module_resource


class TestsSicerProduct(testing.OOTestCase):

    def set_client_code_config(self, cursor, uid):
        res_obj = self.openerp.pool.get('res.config')
        # Init client code config var str(size=8) ex. 99999999
        cli_cod = '12345678'
        conf_id = res_obj.search(cursor, uid,
                                 [('name', '=', 'sicer_partner_code')]
                                 )
        res_obj.write(cursor, uid, conf_id, {'value': cli_cod})
        self.client_code = cli_cod

    def get_poliza_ready(self, cursor, uid, polissa_id=False):
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1] if not polissa_id else polissa_id

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        partner = polissa_obj.read(
            cursor, uid, polissa_id, ['titular']
        )['titular'][0]

        # Set vat as no enterprise
        res_obj = self.openerp.pool.get('res.partner')
        res_obj.write(cursor, uid, partner, {'vat': 'ES71895522Y'})

        cnae_9820 = imd_obj.get_object_reference(
            cursor, uid, 'giscemisc_cnae', 'cnae_9820'
        )[1]

        # Here current owner vat is enterprise and power is under 10 kW
        polissa_obj.write(
            cursor, uid, polissa_id,
            {
                'cnae': cnae_9820, 'tipus_vivenda': 'habitual',

            }
        )

    def get_order_0000x_ready_for_action(self, cursor, uid, order_id=False):
        """
        Añade una linea a la remesa indicada con la factura demo 0001
        - Busca la remesa con el id indicado en order_id
        - Activa la factura demo 0001
        - Añade una linea a la remesa indicada con la factura 0001
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        order_obj = self.openerp.pool.get('giscedata.correos.order')
        lin_obj = self.openerp.pool.get('giscedata.correos.order.line')
        wiz_obj = self.openerp.pool.get('wizard.add.order.lines')
        res_obj = self.openerp.pool.get('res.config')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        inv_obj = self.openerp.pool.get('account.invoice')

        if not order_id:
            remesa_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_correos', 'co_0001'
            )[1]
            order_id = remesa_0001

        factura_0001 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]

        fact_obj.write(cursor, uid, factura_0001, {'state': 'open'})

        pend_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio_comer_bono_social',
            'carta_1_pendent_pending_state'
        )[1]

        inv_id = fact_obj.read(
            cursor, uid, factura_0001, ['invoice_id']
        )['invoice_id'][0]

        inv_obj.set_pending(cursor, uid, inv_id, pend_id)

        moved_pend_id = inv_obj.read(
            cursor, uid, inv_id, ['pending_state']
        )['pending_state'][0]

        self.assertEqual(moved_pend_id, pend_id)

        context = {
            'active_ids': [factura_0001],
            'model': 'giscedata.facturacio.factura'
        }

        wiz_ord_id = wiz_obj.create(
            cursor, uid, {'order_id': order_id}, context=context
        )

        wiz = wiz_obj.browse(cursor, uid, wiz_ord_id, context=context)
        wiz.confirm_state(context=context)
        wiz.add_lines_to_order(context=context)
        lines = order_obj.read(
            cursor, uid, order_id, ['order_lines'], context=context
        )['order_lines']

        self.assertEqual(len(lines), 1)
        self.assertEqual(
            wiz.info, 'S\'han afegit correctament totes les línies'
        )

        return order_id, lines

    def setUp(self):
        self.openerp.install_module(
            'giscedata_correos_bono_social'
        )
        self.txn = Transaction().start(self.database)
        cursor = self.txn.cursor
        uid = self.txn.user

        # Sets config var (client code)
        self.set_client_code_config(cursor, uid)

        self.get_poliza_ready(cursor, uid, polissa_id=False)

        # Create order with lines from factures
        # Store order_id and line_ids in self to avoid multiple readings in
        # tests

        # self.order_id, self.line_ids = self.create_demo_lines_in_demo_orders(
        #     cursor, uid
        # )

    def tearDown(self):
        self.txn.stop()

    def test_create_att_orders(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        order_obj = self.openerp.pool.get('giscedata.correos.order')
        att_order_obj = self.openerp.pool.get('giscedata.correos.attatchment')
        attach_obj = self.openerp.pool.get('ir.attachment')

        uid = self.txn.user
        cursor = self.txn.cursor

        # Demo oreder
        order1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0001'
        )[1]

        order2_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0002'
        )[1]

        file_64_enc = """
        ICAgICAgICAgIF9fX18KICAgICAgICAgIFxfXy8gICAgICAgICAjICMjCiA
        gICAgICAgIGAoICBgXj1fIHAgXyMjI18KICAgICAgICAgIGMgICAvICApICB8ICA
        gLwogICBfX19fXy0gLy9eLS0tfiAgX2MgIDMKIC8gIC0tLS1eXCAvXl9cICAgLy
        AtLSwtCiggICB8ICB8ICBPX3wgXF8vICAsLwp8ICAgfCAgfCAvIFx8ICBgLS0gLwooKC
        hHICAgfC0tLS0tfAogICAgICAvLy0tLS0tXAogICAgIC8vICAgICAgIFwKICAg
        LyAgIHwgICAgIHwgIF58CiAgIHwgICB8ICAgICB8ICAgfAogICB8X19fX3wgICAgfF9fX198
        """

        vals = {
            'name': u'test_file.txt',
            'datas': file_64_enc,
            'datas_fname': u'test_file.txt',
        }
        att_id = attach_obj.create(cursor, uid, vals)

        model_vals = {
            'attachment_retorno_id': att_id,
            'order_ids': [(6, 0, [order1_id, order2_id])],
            'result_import': u'Todo correcto...',
            'comments': u'Esto es un comentario de prueba',
        }

        res_id = att_order_obj.create(cursor, uid, model_vals)

        res_vals = att_order_obj.read(
            cursor, uid, res_id,
            ['attachment_retorno_id', 'order_ids',
             'result_import', 'comments', 'attachment_name'
             ]
        )

        expected_vals = {
            'attachment_retorno_id': att_id,
            'order_ids': [order1_id, order2_id],
            'result_import': u'Todo correcto...',
            'comments': u'Esto es un comentario de prueba'
        }

        self.assertEqual(res_vals['attachment_retorno_id'][0], expected_vals['attachment_retorno_id'])
        self.assertEqual(res_vals['order_ids'], expected_vals['order_ids'])
        self.assertEqual(res_vals['result_import'], expected_vals['result_import'])
        self.assertEqual(res_vals['comments'], expected_vals['comments'])

        attached_file = attach_obj.read(cursor, uid, att_id, ['datas'])['datas']

        self.assertEqual(file_64_enc, attached_file)

    def test_wizard_importacio_create_attachments_and_errors_too(self):
        ord_obj = self.openerp.pool.get('giscedata.correos.order')
        line_obj = self.openerp.pool.get('giscedata.correos.order.line')
        attc_ord_obj = self.openerp.pool.get('giscedata.correos.attatchment')
        wiz_load_obj = self.openerp.pool.get('wizard.load.delivery.files')
        conf_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')
        ir_atc_obj = self.openerp.pool.get('ir.attachment')
        uid = self.txn.user
        cursor = self.txn.cursor

        # Generate wrong file
        # str_file = self.create_tmp_file_to_import_and_check_changes(
        #     cursor, uid, True
        # )

        order_id, lines = self.get_order_0000x_ready_for_action(
            cursor, uid, order_id=False
        )

        remesa_0002 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0001'
        )[1]

        order_code2 = ord_obj.get_order_code(cursor, uid, remesa_0002)

        sicer_line = line_obj.read(cursor, uid, lines[0], ['sicer'])['sicer']
        order_code = ord_obj.get_order_code(cursor, uid, order_id)

        comp_code = conf_obj.get(cursor, uid, 'sicer_partner_code', False)

        fichero_retorno_path = get_module_resource(
            'giscedata_correos', 'tests', 'fixtures', 'fichero_retorno_sicer_1_1.8004')
        with open(fichero_retorno_path, 'r') as f:
            fichero_retorno_path = f.read()

        # NOS INVENTAMOS UN SICER PARA GENERAR FICHERO DE ERRORES
        sicer_line2 = 'CD12345678{0}00000002V'.format(order_code2)
        formated_file_str = fichero_retorno_path.format(
            comp_code=comp_code, order_code=order_code, sicer_line=sicer_line,
            order_code2=order_code2, sicer_line2=sicer_line2
        )
        f_name = 'fichero_retorno_sicer_1_1.8004'
        err_f_name = 'errors-fichero_retorno_sicer_1_1.8004'
        wiz_id = wiz_load_obj.create(
            cursor, uid,
            {
                'file': b64encode(formated_file_str),
                'file_name': f_name,
                'err_fname': err_f_name
            })

        wiz = wiz_load_obj.browse(cursor, uid, wiz_id)
        # Update order lines

        wiz.update_lines()

        query_search_attc = """
        SELECT att.id, att.attachment_name, att.attachment_retorno_id, 
        att.result_import, att.attachment_errors_id
        FROM giscedata_correos_attatchment AS att
        RIGHT JOIN giscedata_correos_order_rel AS rel ON (rel.att_order_id = att.id)
        RIGHT JOIN giscedata_correos_order AS o ON (rel.order_id = o.id)
        WHERE o.id = %s
        """

        cursor.execute(query_search_attc, (order_id,))

        att_info = cursor.dictfetchall()[0]

        att_id = att_info['id']

        attachment_name = att_info['attachment_name']

        result_import = att_info['result_import']

        attachment_retorno_id = att_info['attachment_retorno_id']

        attachment_errors_id = att_info['attachment_errors_id']

        self.assertEqual(attachment_name, f_name)

        self.assertEqual(result_import, wiz.info)

        self.assertTrue(attachment_errors_id)

        fichero_adjunto_nuevo_modelo = ir_atc_obj.read(
            cursor, uid, attachment_retorno_id, ['datas']
        )['datas']

        self.assertEqual(
            b64decode(fichero_adjunto_nuevo_modelo), formated_file_str
        )
