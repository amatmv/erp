# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestsWizardOrderAddLines(testing.OOTestCase):

    def test_add_invoices_to_order(self):
        order_obj = self.openerp.pool.get('giscedata.correos.order')
        wiz_obj = self.openerp.pool.get('wizard.add.order.lines')
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')
        pen_obj = self.openerp.pool.get('account.invoice.pending.state')
        res_obj = self.openerp.pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Set config var (8 fixed code)
            conf_id = res_obj.search(cursor, uid,
                                     [('name', '=', 'sicer_partner_code')])
            res_obj.write(cursor, uid, conf_id, {'value': '99999999'})

            # Create order with sending_date, not required but it's necessary to
            # create file, name depends of this
            order_id = order_obj.create(
                cursor, uid, {'sending_date': '2018-02-01'})

            # Get demo invoices 1 and 2
            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]
            invoice_id_2 = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0002'
            )[1]
            demo_invoice_keys = [invoice_id, invoice_id_2]

            inv_obj = self.openerp.pool.get('account.invoice')
            inv_obj.write(cursor, uid, demo_invoice_keys, {'state': 'open'})

            pend_id = imd_obj.get_object_reference(
                cursor, uid,
                'giscedata_facturacio_comer_bono_social',
                'carta_1_pendent_pending_state'
            )[1]

            # Pending state is stored field function, for this one we need to
            # execute a query to set this
            query_pendings = '''UPDATE account_invoice SET pending_state = {0} 
            WHERE id in {1}'''.format(str(pend_id), str((invoice_id, invoice_id_2)))
            cursor.execute(query_pendings)

            context = {
                'active_ids': demo_invoice_keys,
                'model': 'account.invoice'
            }

            wiz_ord_id = wiz_obj.create(
                cursor, uid, {'order_id': order_id}, context=context
            )

            wiz = wiz_obj.browse(cursor, uid, wiz_ord_id, context=context)
            wiz.confirm_state(context=context)
            wiz.add_lines_to_order(context=context)
            self.assertEqual(
                wiz.info, 'S\'han afegit correctament totes les línies'
            )
            lines = order_obj.read(cursor, uid, order_id, ['order_lines'],
                                   context=context)['order_lines']

            self.assertEqual(len(demo_invoice_keys), len(lines))

    def test_add_factures_to_order(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        order_obj = self.openerp.pool.get('giscedata.correos.order')
        lin_obj = self.openerp.pool.get('giscedata.correos.order.line')
        wiz_obj = self.openerp.pool.get('wizard.add.order.lines')
        res_obj = self.openerp.pool.get('res.config')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        addr_obj = self.openerp.pool.get('res.partner.address')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Set config var
            conf_id = res_obj.search(cursor, uid,
                                     [('name', '=', 'sicer_partner_code')])
            res_obj.write(cursor, uid, conf_id, {'value': '99999999'})
            order_id = order_obj.create(cursor, uid,
                                        {'sending_date': '2018-02-01'})

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            invoice_id_2 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0002'
            )[1]
            demo_invoice_keys = [invoice_id, invoice_id_2]

            new_address_not_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_tang'
            )[1]

            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol_add_id = pol_obj.read(
                cursor, uid, pol_id, ['direccio_notificacio']
            )['direccio_notificacio'][0]

            self.assertNotEqual(new_address_not_id, pol_add_id)

            pol_obj.write(cursor, uid, pol_id, {'direccio_notificacio': new_address_not_id})

            fact_obj.write(cursor, uid, demo_invoice_keys, {'state': 'open'})

            context = {
                'active_ids': demo_invoice_keys,
                'model': 'giscedata.facturacio.factura'
            }

            wiz_ord_id = wiz_obj.create(
                cursor, uid, {'order_id': order_id}, context=context
            )

            wiz = wiz_obj.browse(cursor, uid, wiz_ord_id, context=context)
            wiz.confirm_state(context=context)
            wiz.add_lines_to_order(context=context)
            lines = order_obj.read(cursor, uid, order_id, ['order_lines'], context=context)['order_lines']
            self.assertEqual(
                wiz.info, 'S\'han afegit correctament totes les línies'
            )
            self.assertEqual(len(demo_invoice_keys), len(lines))

            res_lines = lin_obj.read(
                cursor, uid, lines,
                ['partner_street', 'partner_city', 'partner_zip']
            )

            for res in res_lines:
                self.assertEqual(res['partner_street'], '31 Hong Kong street')
                self.assertEqual(res['partner_city'], 'Taiwan')
                self.assertEqual(res['partner_zip'], '23410')

    def test_dont_allow_add_draft_and_duplicated(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        order_obj = self.openerp.pool.get('giscedata.correos.order')
        wiz_obj = self.openerp.pool.get('wizard.add.order.lines')
        res_obj = self.openerp.pool.get('res.config')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Set config var
            conf_id = res_obj.search(cursor, uid,
                                     [('name', '=', 'sicer_partner_code')])
            res_obj.write(cursor, uid, conf_id, {'value': '99999999'})
            order_id = order_obj.create(cursor, uid,
                                        {'sending_date': '2018-02-01'})

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            invoice_id_2 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0002'
            )[1]

            demo_invoice_keys = [invoice_id, invoice_id_2]

            # Get info to check wiz info
            inv_names = fact_obj.read(cursor, uid, demo_invoice_keys, ['invoice_id'])
            inv_names = [k['invoice_id'] for k in inv_names]
            # END get info

            context = {
                'active_ids': demo_invoice_keys,
                'model': 'giscedata.facturacio.factura'
            }

            wiz_ord_id = wiz_obj.create(
                cursor, uid, {'order_id': order_id}, context=context
            )

            wiz = wiz_obj.browse(cursor, uid, wiz_ord_id, context=context)
            wiz.confirm_state(context=context)
            wiz.add_lines_to_order(context=context)
            lines = order_obj.read(cursor, uid, order_id, ['order_lines'],
                                   context=context)['order_lines']
            self.assertEqual(
                wiz.info, 'Error al crear linies per les factures:\nfactura: '
                          '{0} -> La factura {1} est\xc3\xa0 en '
                          'borrador i no pot ser afegida\nfactura: '
                          '{2} -> La factura {3} est\xc3\xa0 '
                          'en borrador i no pot ser afegida\n'.format(
                           inv_names[0], inv_names[0][0],
                           inv_names[1], inv_names[1][0]
                           )
            )
            # No se deberian haver añadido ninguna factura ya que estan en
            # borrador
            self.assertEqual(0, len(lines))

            # Abrimos las facturas ahora si que se deberian añadir
            fact_obj.write(cursor, uid, demo_invoice_keys, {'state': 'open'})
            wiz.add_lines_to_order(context=context)
            lines = order_obj.read(cursor, uid, order_id, ['order_lines'],
                                   context=context)['order_lines']
            self.assertEqual(len(demo_invoice_keys), len(lines))

            wiz.add_lines_to_order(context=context)
            lines = order_obj.read(cursor, uid, order_id, ['order_lines'],
                                   context=context)['order_lines']
            self.assertEqual(len(demo_invoice_keys), len(lines))

            # Intentamos añadir por segunda vez las mismas facturas, no
            # deberia dejar
            wiz = wiz_obj.browse(cursor, uid, wiz_ord_id, context=context)
            self.assertEqual('Error al crear linies per les factures:\n'
                             'factura: '
                             '{0} -> La factura {1} ja ha '
                             'sigut afegida anteriorment\nfactura: '
                             '{2} -> '
                             'La factura {3} ja ha sigut afegida '
                             'anteriorment\n'.format(
                              inv_names[0], inv_names[0][0],
                              inv_names[1], inv_names[1][0]
                             ),
                             wiz.info
            )
