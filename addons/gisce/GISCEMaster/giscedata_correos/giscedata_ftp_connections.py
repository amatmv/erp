from __future__ import absolute_import
from giscedata_ftp_connections.giscedata_ftp_connections import _CATEGORIES

if ('correos', 'Correos') not in _CATEGORIES:
    _CATEGORIES.append(('correos', 'Correos'))
