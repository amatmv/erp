# -*- coding: utf-8 -*-
{
    "name": "Giscedata correos",
    "description": """
Modulo de correos españa:
    * Calcular sicers
    * Enviar correos
    * Recibir correos
    """,
    "version": "0-dev",
    "author": "Gisce",
    "category": "Polissa",
    "depends":[
        "giscedata_correos_base_flux",
        "giscedata_ftp_connections"
    ],
    "init_xml": [],
    "demo_xml": ["giscedata_correos_demo.xml"],
    "update_xml":[
        "giscedata_correos_data.xml",
        "giscedata_correos_view.xml",
        "wizard/wizard_add_order_lines_view.xml",
        "wizard/wizard_load_delivery_files_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
