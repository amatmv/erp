# -*- encoding: utf-8 -*-
from tools.translate import _
from base64 import b64decode
from retrofix import record, exception
from retrofix.fields import *
from retrofix.fields import __all__
from datetime import datetime
from copy import deepcopy

if 'DateNone' not in __all__:
    __all__.append('DateNone')


class DateNone(Date):
    def __init__(self, pattern):
        Date.__init__(self, pattern)

    def set_from_file(self, value):
        if value == '0' * len(value) or value == ' ' * len(value):
            return
        try:
            return datetime.strptime(value, self._pattern)
        except ValueError:
            raise exception.RetrofixException(
                'Invalid date value "%s" does not '
                'match pattern "%s" in field "%s"' % (
                    value, self._pattern, self._name)
            )

    def get_for_file(self, value):
        if value is None:
            return False
        else:
            res = datetime.strftime(value, self._pattern)
        return super(Date, self).get_for_file(res)


SICER_LETTERS = 'TRWAGMYFPDXBNJZSQVHLCKE'

DELIVERY_STATUS = {
    "01": ("Entregado en domicilio", True),
    "02": ("Dirección Incorrecta. En proceso de devolución", False),
    '03': ('Ausente (Seguimiento. Implica aviso)', False),
    '04': ('Desconocido. En proceso de devolución (Final)', False),
    '05': ('Fallecido. En proceso de devolución (Final)', False),
    '06': ('Rehusado. En proceso de devolución (Final)', False),
    '07': ('Nadie se hace cargo (Final)', False),
    '08': ('Entregado en oficina (Final)', True),
    '09': ('Caducado. No retirado en oficina. En proceso de devolución (Final)', False),
    '10': ('Entregado turno tarde (Final)', True),
    '11': ('Devuelto turno tarde (Final)', False),
    '12': ('Llegada a oficina (Seguimiento)', False),
    '13': ('Depositado en Buzón (Final)', True),
    '14': ('Llegada a Oficina. Emitido aviso a destinatario (Seguimiento)', False),
    '15': ('Emitido 2o aviso a destinatario (Seguimiento)', False),
    '18': ('Entregado en destino (Final)', True),
    '27': ('Entregado al remitente por devolución (Final)', False),
    'EA': ('Extraviado (no se puede justificar la entrega, no aparece) (Final)', False),
    'EB': ('No ha sido depositado en Correos y Telégrafos. (Final)', False),
    'EC': ('Sustracción, robo. (Final)', False),
    'ED': ('Envío fuera de ámbito SICER. (Final)', False),
    'EZ': ('Sin Información. Será automático al cierre de la remesa (Final)', True),
}


# FIXED STRUCTURED OF ORDER DELIVERY RESULTS FILES

RESULT_HEADER = (
    (1,  1,  'register_type', Char),
    (2,  2,  'product_code', Char),
    (4,  8,  'client_code', Char),
    (12, 2,  'result', Char),
    (14, 3,  'total_orders', Integer),
    (17, 15, 'blank_spaces', Char),
)

RESULT_LINE = (
    (1,   1,  'register_type', Char),
    (2,   2,  'product_code', Char),
    (4,   8,  'client_code', Char),
    (12,  4,  'order_code', Char),
    (16,  8,  'total_shipments', Integer),
    (24,  8,  'accepted_shipments', Integer),
)

RESULT_END = (
    (1,   1,   'register_type', Char),
    (2,   8,   'load_date', Date('%Y%m%d')),
    (10,  5,   'load_time', Char),
    (15,  17,  'blank_spaces', Char),
)

# END FIXED STRUCTURE

# FIXED STRUCTURES FOR CERTIFICATES AND NOTIFICATIONS WITH RECEIPT NOTICE
# Cliente - > correos pg 20 documentation
# RECEIPT_NOTICE_RESULTS_HEADER =(
#     (1,  1, 'register_type', Char),
#     (2,  1, 'file_type', Char),
#     (3,  2, 'product_code', Char),
#     (5,  8, 'client_code', Char),
#     (13, 7, 'office_code', Char),
#     (20, 8, 'file_date', Date('%Y%m%d')),
#     (28, 5, 'file_time', Char),
#     (33, 315, 'blank_spaces', Char)
# )
#
# RECEIPT_NOTICE_ORDER_HEADER = (
#     (1,  1, 'register_type', Char),
#     (2,  2, 'product_code', Char),
#     (4,  8, 'client_code', Char),
#     (12, 4, 'order_code', Char),
#     (16, 8, 'emission_date'),
#     (),
#     (),
# )
# END FIXED STRUCTURES

# FIXED STRUCTURED LINES OF NOTIFICATIONS RESULTS FILES
# Flow correos -> Cliente
# Resultados de Entregas para Notificaciones y otros productos sin reembolso

NOTIFICATION_RESULTS_FILE_HEADER = (
    (1,  1, 'register_type', Char),
    (2,  2, 'product_code', Char),
    (4,  8, 'client_code', Char),
    (12, 7, 'office_code', Char),
    (19, 8, 'return_date', DateNone('%Y%m%d')),
    (27, 5, 'return_time', Char),
    (32, 61, 'blank_cierre', Char) # Si el fichero es de cierre formato Cbbbb...
)

NOTIFICATION_RESULTS_ORDER_HEADER = (
    (1,  1, 'register_type', Char),
    (2,  2, 'product_code', Char),
    (4,  8, 'client_code', Char),
    (12, 4, 'order_code', Char),
    (16, 8, 'deposit_date', DateNone('%Y%m%d')),
    (24, 5, 'deposit_time', Char),
    (29, 8, 'cierre_date', DateNone('%Y%m%d')),
    (37, 1, 'order_state', Char),
    (38, 8, 'emission_date', DateNone('%Y%m%d')),
    (46, 47, 'blank_spaces', Char)
)

# THIS FIXED STRUCTURE IS THE SAME FOR DETAILS AND MODIFICATIONS


NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS = (
    (1,  1,  'register_type', Char),
    (2,  1,  'incidence_type', Char),
    # Codigos del envio (Sicer)
    (3,  2,  'product_code', Char),
    (5,  8,  'client_code', Char),
    (13, 4,  'order_code', Char),
    (17, 9,  'shipping_number', Char),
    (26, 2,  'situation_code', Char),
    (28, 8,  'situation_date', DateNone('%Y%m%d')),
    (36, 7,  'situation_office_code', Char),
    (43, 41, 'client_info', Char),
    (84, 9,  'situation_code_info', Char)
)

NOTIFICATION_RESULTS_ORDER_END = (
    (1,  1,  'register_type', Char),
    (2,  2,  'product_code', Char),
    (4,  8,  'client_code', Char),
    (12, 4,  'order_code', Char),
    (16, 9,  'detailed_registers', Char),
    (25, 68, 'blank_spaces', Char)
)
NOTIFICATION_RESULTS_END_FILE = (
    (1,  1,  'register_type', Char),
    (2,  2,  'product_code', Char),
    (4,  8,  'client_code', Char),
    (12, 3,  'n_orders', Char),
    (15, 9,  'n_registers', Char),
    (24, 69, 'blank_spaces', Char)
)

# END FIXED STRUCTURE

# TABLA DE ERRORES SITUACIONES DE ENTREGA

TABLA_SITUACIONES_ENTREGA = [
    ('01', 'Entregado en Domicilio (Final)'),
    ('02', 'Dirección Incorrecta. En proceso de devolución (Final)'),
    ('03', 'Ausente (Seguimiento. Implica aviso)'),
    ('04', 'Desconocido. En proceso de devolución (Final)'),
    ('05', 'Fallecido. En proceso de devolución (Final)'),
    ('06', 'Rehusado. En proceso de devolución (Final)'),
    ('07', 'Nadie se hace cargo (Final)'),
    ('08', 'Entregado en oficina (Final)'),
    ('09', 'Caducado. No retirado en oficina. En proceso de devolución (Final)'),
    ('10', 'Entregado turno tarde (Final)'),
    ('11', 'Devuelto turno tarde (Final)'),
    ('12', 'Llegada a oficina (Seguimiento)'),
    ('13', 'Depositado en Buzón (Final)'),
    ('14', 'Llegada a Oficina. Emitido aviso a destinatario (Seguimiento)'),
    ('15', 'Emitido 2o aviso a destinatario (Seguimiento)'),
    ('18', 'Entregado en destino (Final)'),
    ('27', 'Entregado al remitente por devolución (Final)'),
    ('EA', 'Extraviado (no se puede justificar la entrega, no aparece) (Final)'),
    ('EB', 'No ha sido depositado en Correos y Telégrafos. (Final)'),
    ('EC', 'Sustracción, robo. (Final)'),
    ('ED', 'Envío fuera de ámbito SICER. (Final)'),
    ('EZ', 'Sin Información. Será automático al cierre de la remesa (Final)'),
]

RECEPTION_STATE_TO_NOTIFICATION_STATE = {
    '01': 'entregado',
    '02': 'perdida',
    '03': 'enviat',
    '04': 'perdida',
    '05': 'no_entrega',
    '06': 'no_entrega',
    '07': 'no_entrega',
    '08': 'entregado',
    '09': 'no_entrega',
    '10': 'entregado',
    '11': 'no_entrega',
    '12': 'enviat',
    '13': 'entregado',
    '14': 'enviat',
    '15': 'enviat',
    '18': 'entregado',
    '27': 'no_entrega',
    'EA': 'perdida',
    'EB': 'perdida',
    'EC': 'perdida',
    'ED': 'no_entrega',
    'EZ': 'entregado',
}


class SicerProduct:

    _name = 'sicer.product'

    def __init__(self, product_c, client_c):
        if len(product_c) == 2 and len(client_c) == 8:
            self.product_code = product_c  # size 2
            self.client_code = client_c  # size 8
        else:
            raise ValueError(_("Invalid lenght!, "
                             "(product code - 2 | client code - 8)"))

    @staticmethod
    def get_national_checksum(str_sicer):
        # Input: str 22 size returns letter of result:
        # Convert all chars into int ascii code value
        # Sum all results
        # return Div sum 23

        if isinstance(str_sicer, basestring):
            if len(str_sicer) == 22:
                # List of ascii code equivalence
                list_converted = [ord(ch) for ch in str_sicer]
                return SICER_LETTERS[sum(list_converted) % 23]
            else:
                raise ValueError(_("ERROR, incorrect sicer lenght!, "
                                 "withoit letter should be 22"))
        else:
            raise ValueError(_("ERROR, incorrect format!, should be str"))

    def get_sicer(self, remesa, seq_client):

        if len(remesa) != 4:
            raise ValueError(_("ERROR, Remittance code lenght error!, "
                             "should be 4"))
        if len(seq_client) != 8:
            raise ValueError(_("ERROR, Client sequence code lenght error!, "
                             "should be 8"))
        sicer_str = self.product_code + self.client_code + remesa + seq_client
        letter = self.get_national_checksum(sicer_str)
        return sicer_str + letter

    # Next methods parse files dialogs with correos and cliente
    # Dialogs legend:
    # F1 (client -> correos) Recepción de Datos de remesas - Detalles
    # F2/F3(results/errors) (correos -> client) resultado de carga y
    # relación de errores del fichero anterior
    # F4 (correos/cliente) Retorno de Resultados de entregas y reembolsos
    # -------------------------------------------------------------------
    # F1 ->
    # F2 -> interpret_delivery_file(files)
    # F3 ->
    # F4 -> interpret_retorno_notificaciones_sin_reembolso_file(files)
    @staticmethod
    def interpret_retorno_notificaciones_sin_reembolso_file(res_files):
        """
        This method allows user to pass by parameter a list of files.
        Returned structure are (list of dictionaries where value in
                                key 'file_orders is another list of dictionaries
                                with all orders in these file):
            [
                {
                    'file_header': retrofix Record F
                    'file_orders': [
                        {'order_header: retrofix Record x,
                         'details': [record_a1, ...,  record_an-1],
                         'modifications': [retrofix Record_b1, ..., Record_bn-1],
                         'order_footer': retrofix Record y
                        },...
                    ]
                    'file_footer': retrofix Record f
                },...
            ]
        Used STRUCTURES to read files (on giscedata_correos/sicer_correos):
            NOTIFICATION_RESULTS_FILE_HEADER (1 line)
            NOTIFICATION_RESULTS_ORDER_HEADER (n orders in file)
            NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS (n lines in order)
            NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS (n lines in order mod)
            NOTIFICATION_RESULTS_ORDER_END (n orders)
            NOTIFICATION_RESULTS_END_FILE (1 line)

        """
        res = []

        RES_TEMPLATE = {
            'order_header': False,
            'details': [],
            'modifications': [],
            'order_footer': False
        }

        RETURN_TEMPLATE = {
            'file_header': False,
            'file_orders': [],
            'file_footer': False,
        }

        if not isinstance(res_files, list):
            res_files = [res_files]
        if all(isinstance(f, basestring) for f in res_files):
            order_results = []
            for res_file in res_files:
                # File formated in utf-8 then need to decode
                res_file = b64decode(res_file).decode('utf-8')
                file_lines = res_file.splitlines()

                # First and last line in file define header and footer
                header_file = record.Record.extract(
                    file_lines[0], NOTIFICATION_RESULTS_FILE_HEADER
                )
                end_file = record.Record.extract(
                    file_lines[-1], NOTIFICATION_RESULTS_END_FILE
                )

                # Remove header and footer to iterate only over orders
                file_lines = file_lines[1:-1]

                # Register codes parsing
                # F | f -> header | footer file but these lines are off of
                # iterated list
                # C -> Header order line
                # D -> Details line in order
                # M -> Modification line in order
                # c -> Order footer
                # list should be something like this F [C D M c C D M c] f
                # List of dicts with order details and mods

                # This list will contains dicts with order content
                # {sicer, order, ...}

                captured_dict = RES_TEMPLATE.copy()
                tmp_res = RETURN_TEMPLATE.copy()
                for line in file_lines:
                    # If line is Order header
                    if line.startswith('C'):
                        if captured_dict:
                            # captured_dict = RES_TEMPLATE.copy() (DONT USE)
                            # USE DEEP COPY TO AVOID LIST REF COPIED,
                            # PYTHON MINDFUCKS ATACK AGAIN
                            captured_dict = deepcopy(RES_TEMPLATE)
                        # Header order
                        rec_ord_h = record.Record.extract(
                            line, NOTIFICATION_RESULTS_ORDER_HEADER
                        )
                        captured_dict['order_header'] = rec_ord_h
                    elif line.startswith('D'):
                        rec_detail = record.Record.extract(
                            line,
                            NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS
                        )
                        captured_dict['details'].append(rec_detail)
                    elif line.startswith('M'):
                        rec_mod = record.Record.extract(
                            line,
                            NOTIFICATION_RESULTS_ORDER_DETAILS_MODIFICATIONS
                        )
                        captured_dict['modifications'].append(rec_mod)
                    # End order
                    elif line.startswith('c'):
                        rec_ord_f = record.Record.extract(
                            line, NOTIFICATION_RESULTS_ORDER_END
                        )
                        captured_dict['order_footer'] = rec_ord_f
                        # captured_dict.order_footer = rec_ord_f
                        tmp_res['file_orders'].append(captured_dict)

                    else:
                        raise ValueError(
                            _('Line {0} should start with (C, c, D, M)').format(line))
                tmp_res['file_header'] = header_file
                tmp_res['file_footer'] = end_file
                order_results.append(tmp_res)
                return order_results
        return res

    @staticmethod
    def interpret_delivery_file(res_files):
        """
        Description: read order results file (correos -> cliente)
        This method allows user to pass by parameter a list of files.
        Returned structure are (list of dictionaries where value in
                                key 'details' is another list of records):
            [
                {
                    'file_header': retrofix Record F
                    'details': [
                        record1, ...
                    ]
                    'file_footer': retrofix Record f
                },...
            ]
        Used STRUCTURES to read files (on giscedata_correos/sicer_correos):
            RESULT_HEADER (1 line)
            RESULT_LINE (n orders in file)
            RESULT_END (1 line)

        """
        res = []
        DICT_TEMPLATE = {
            'file_header': False,
            'details': [],
            'file_footer': False,
        }
        if res_files:
            if not isinstance(res_files, list):
                res_files = [res_files]
            if all(isinstance(f, basestring) for f in res_files):
                for res_file in res_files:
                    res_file = b64decode(res_file).decode('utf-8')
                    # TODO check type file? errors or not?
                    file_lines = res_file.splitlines()
                    # START bloc parse lines in file
                    #   header = first line in file
                    #   end = last line in file
                    #   file_lines = lines in file less header and end
                    header = file_lines[0]
                    end = file_lines[-1]
                    file_lines = file_lines[1:-1]
                    # END Bloc

                    # records = file_lines in RESULT_LINE format
                    record_header = record.Record.extract(header, RESULT_HEADER)
                    record_end = record.Record.extract(end, RESULT_END)
                    records_orders = []
                    for line in file_lines:
                        records_orders.append(
                            record.Record.extract(line, RESULT_LINE)
                        )
                    # Store tmp file records in a res list
                    dict_tmp = DICT_TEMPLATE.copy()
                    dict_tmp['file_header'] = record_header
                    dict_tmp['details'] = records_orders
                    dict_tmp['file_footer'] = record_end
                    res.append(dict_tmp)

            else:
                raise ValueError(
                    "Parameter file is not a str is {0}".format(
                        type(res_files))
                )
        else:
            raise ValueError("Parameter file is none")
