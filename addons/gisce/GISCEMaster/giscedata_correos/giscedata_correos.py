# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields
from sicer_correos import SicerProduct, DELIVERY_STATUS, \
    RECEPTION_STATE_TO_NOTIFICATION_STATE
from tools.translate import _
from datetime import datetime
import collections
from osv.expression import OOQuery
from base64 import b64decode, b64encode


class GiscedataCorreosOrder(osv.osv):
    _name = 'giscedata.correos.order'
    _description = 'This class represents an order of invoices to be ' \
                   'sent by correos'

    # Added send to send state workflow to avoid problems on generating
    # client file on wizards because it change state to sent
    CONSTRAINTS_TABLE_STATES = {
        'draft': [],
        'send': ['draft', 'send'],
        'end': ['send'],
        'close': ['draft', 'send', 'end'],
    }

    def _total_lines(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        if not context:
            context = {}
        for line in self.read(cursor, uid, ids, ['order_lines'], context=context):
            res[line['id']] = len(line['order_lines'])

        return res

    def get_order_code(self, cursor, uid, ids, context=None):
        order_name = self.read(
            cursor, uid, ids, ['name'], context=context
        )['name']

        return order_name[-4:]

    def name_get(self, cursor, uid, ids, context=None):
        if not len(ids):
            return []
        reads = self.read(
                cursor, uid, ids, ['name', 'descriptive_name'], context=context)
        res = [(r['id'], '{name} [{descriptive_name}]'.format(**r))
               for r in reads]

        return res

    def recalculate_order_lines_sequence(self, cursor, uid, ids, context=None):
        ord_line_obj = self.pool.get('giscedata.correos.order.line')
        if isinstance(ids, (int, long)):
            ids = [ids]
        for order_id in ids:
            seq = 1
            ord_lines = self.read(
                cursor, uid, order_id, ['order_lines'], context=context
            )['order_lines']
            for line_id in ord_lines:
                ord_line_obj.write(cursor, uid, [line_id],
                                   {'sequence': seq}, context=context
                                   )
                seq += 1

    def recalculate_order_lines_sicer(self, cursor, uid, ids, context=None):
        ord_line_obj = self.pool.get('giscedata.correos.order.line')
        if isinstance(ids, (int, long)):
            ids = [ids]
        for order in self.read(cursor, uid, ids, ['order_lines'], context=context):
            ord_line_obj.set_sicer(
                cursor, uid, order['order_lines'], context=context
            )

    def change_state(self, cursor, uid, ids, state=None, context=None):

        if isinstance(ids, (int, long)):
            ids = [ids]
        if state:
            current_state = self.read(
                cursor, uid, ids, ['state'], context=context
            )[0]['state']

            if current_state not in self.CONSTRAINTS_TABLE_STATES[state]:
                raise osv.except_osv(_('Workfow Error!'),
                                     _('Can\'t change state from {0} to {1}'
                                       ).format(current_state, state)
                                     )
            else:
                self.write(cursor, uid, ids, {'state': state}, context=context)

    def check_order_has_draft_invoices(self, cursor, uid, ids, context=None):
        # Check if order has someone invoice in draft state.
        line_obj = self.pool.get('giscedata.correos.order.line')

        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        lines = self.read(
            cursor, uid, ids, ['order_lines'], context=context
        )['order_lines']

        draft_inv = line_obj.search(
            cursor, uid, [
                ('order_id', '=', ids),
                ('invoice_id.state', '=', 'draft')
            ], context=context)

        return draft_inv

    def enviar(self, cursor, uid, ids, context=None):
        self.change_state(cursor, uid, ids, 'send', context=context)

    def tancar(self, cursor, uid, ids, context=None):
        self.change_state(cursor, uid, ids, 'close', context=context)

    def finalitzar(self, cursor, uid, ids, context=None):
        self.change_state(cursor, uid, ids, 'end', context=context)

    def _ff_cierre_date(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        for order in self.read(
                cursor, uid, ids, ['order_state'], context=context):

            if order['order_state'] == 'C':
                res[order['id']] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        return res

    def check_duplicated_invoices(self, cursor, uid, ord_id, inv_ids, context=None):
        if context is None:
            context = {}
        msg_err = ''
        if not ord_id:
            msg_err = msg_err + 'Falta la remesa '
        if not inv_ids:
            msg_err += 'No s\'ha passat cap factura per parametre'
        if msg_err:
            raise osv.except_osv(
                _('Error'),
                _('Mètode check_duplicated_invoices -> {0}').format(msg_err)
            )
        if not isinstance(inv_ids, (list, tuple)):
            inv_ids = [inv_ids]

        if isinstance(ord_id, (list, tuple)):
            ord_id = ord_id[0]

        order_lines_ids = self.read(
            cursor, uid, ord_id, ['order_lines'], context=context
        )['order_lines']

        line_obj = self.pool.get('giscedata.correos.order.line')

        line_ord_invoice = line_obj.read(
            cursor, uid, order_lines_ids, ['invoice_id'], context=context
        )

        inv_line_ids = map(lambda d: d['invoice_id'][0], line_ord_invoice)

        len_inv_will_be_added = len(inv_ids)
        inv_can_be_added = list(set(inv_ids) - set(inv_line_ids))
        all_can_be_added = len_inv_will_be_added == len(inv_can_be_added)

        # If someone invoice can't be added return false + list of inv can be
        # added else True + list inv can be added

        return all_can_be_added, inv_can_be_added

    def go_next_pendings(self, cursor, uid, order_ids, force_update=False, o_line_ids=None, context=None):
        if context is None:
            context = {}

        if o_line_ids and isinstance(order_ids, (list, tuple)) and len(order_ids) > 1:
            raise osv.except_osv(
                'Error',
                'Si se pasan lineas por parametro solo puede referirse a una orden!'
            )

        if isinstance(order_ids, (int, long)):
            order_ids = [order_ids]

        errors = {}

        error = collections.namedtuple(
            'Error', 'p_state_inv p_state_line p_state_dest inv_num sicer'
        )

        succes = {}

        correct = collections.namedtuple(
            'Correct', 'p_state_inv p_state_line p_state_dest inv_num sicer'
        )

        ord_line_obj = self.pool.get('giscedata.correos.order.line')
        pstate_obj = self.pool.get('account.invoice.pending.state')
        inv_obj = self.pool.get('account.invoice')
        imd_obj = self.pool.get('ir.model.data')
        # [{'id': 1,'order_lines': [1,2,3]},...]
        order_line_ids = self.read(
            cursor, uid, order_ids, ['order_lines'], context=context
        )

        correct_state = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'correct_bono_social_pending_state'
        )[1]

        for lines in order_line_ids:
            if o_line_ids:
                lines_ids = o_line_ids
            else:
                lines_ids = lines['order_lines']

            order_id = lines['id']

            q = OOQuery(ord_line_obj, cursor, uid)
            sql = q.select(
                ['id', 'invoice_state', 'invoice_id', 'invoice_id.pending_state',
                 'invoice_state.name',
                 'invoice_id.pending_state.weight',
                 'invoice_id.state',
                 'invoice_id.number',
                 'invoice_id.pending_state.name',
                 'invoice_id.pending_state.process_id', 'sicer', 'order_id.name']
            ).where([
                ('id', 'in', lines_ids)
            ])
            cursor.execute(*sql)
            for line in cursor.dictfetchall():
                weight = line['invoice_id.pending_state.weight']
                process_id = line['invoice_id.pending_state.process_id']
                pstate_id = pstate_obj.get_next(
                    cursor, uid, weight, process_id
                )
                order_name = line['order_id.name']
                # Si la factura ya esta pagada y la linea no esta actualizada
                # la actualizamos
                if line['invoice_id.pending_state'] == correct_state and line['invoice_id.state'] == 'paid':
                    if line['invoice_state'] != correct_state:
                        if force_update:
                            ord_line_obj.write(
                                cursor, uid, [line['id']],
                                {'invoice_state': correct_state}
                            )
                    found = succes.get(order_name, False)
                    dest_state = pstate_obj.read(
                        cursor, uid, correct_state, ['name']
                    )['name']

                    correct_info = correct(
                        p_state_inv=line['invoice_id.pending_state.name'],
                        p_state_line=line['invoice_state.name'],
                        p_state_dest=dest_state,
                        inv_num=line['invoice_id.number'],
                        sicer=line['sicer']
                    )
                    if found:
                        succes[order_name].append(correct_info)
                    else:
                        succes[order_name] = [correct_info]

                    continue

                elif line['invoice_state'] == line['invoice_id.pending_state']:

                    if pstate_id != line['invoice_id.pending_state']:
                        inv_obj.set_pending(
                            cursor, uid, [line['invoice_id']], pstate_id
                        )
                        if force_update:
                            ord_line_obj.write(
                                cursor, uid, [line['id']],
                                {'invoice_state': pstate_id}
                            )
                        found = succes.get(order_name, False)
                        dest_state = pstate_obj.read(
                            cursor, uid, pstate_id, ['name']
                        )['name']
                        correct_info = correct(
                            p_state_inv=line['invoice_id.pending_state.name'],
                            p_state_line=line['invoice_state.name'],
                            p_state_dest=dest_state,
                            inv_num=line['invoice_id.number'],
                            sicer=line['sicer']
                        )
                        if found:
                            succes[order_name].append(correct_info)
                        else:
                            succes[order_name] = [correct_info]

                        continue
                order_found = errors.get(order_name, False)
                dest_state = pstate_obj.read(cursor, uid, pstate_id, ['name'])['name']
                fail_info = error(
                    p_state_inv=line['invoice_id.pending_state.name'],
                    p_state_line=line['invoice_state.name'],
                    p_state_dest=dest_state,
                    inv_num=line['invoice_id.number'],
                    sicer=line['sicer']
                )
                if order_found:
                    errors[order_name].append(fail_info)
                else:
                    errors[order_name] = [fail_info]
        return succes, errors

    def _get_total(self, cursor, uid, ids, field_name, arg, context=None):
        result = dict.fromkeys(ids, {
                'pendiente_facturar': 0,
                'total_facturado': 0,
                'total': 0

            })
        # Cuidao si se hacen operaciones en el default dict, es la misma
        # instancia para todos, pero como remplazamos con una referencia nueva
        # va bien i es mas optimo asi.

        query = (
            'SELECT '
            'ol.order_id AS o_id, '
            'SUM(i.amount_total) AS o_total, '
            'SUM(i.residual) AS o_restant '
            'FROM giscedata_correos_order_line AS ol '
            'INNER JOIN account_invoice AS i ON (ol.invoice_id = i.id) '
            'WHERE ol.order_id in %s '
            'GROUP BY ol.order_id'
        )

        cursor.execute(query, (tuple(ids),))

        res = cursor.dictfetchall()

        for rec in res:
            result[rec['o_id']] = {
                'pendiente_facturar': rec['o_restant'],
                'total_facturado': rec['o_total'] - rec['o_restant'],
                'total': rec['o_total']

            }

        return result

    _store_cierre = {'giscedata.correos.order':
                     (lambda self, cr, uid, ids, c=None: ids,
                      ['order_state'], 20)
                     }

    _columns = {
        'name': fields.char(
            'Nom', size=10, required=True, select=1, readonly=True),
        'descriptive_name': fields.char(
            'Nom descriptiu', size=64, select=1,
            help=u'Nom indicatiu per l\'usuari (No se\'n fara cap ús)'),
        'user_id': fields.many2one(
            'res.users', 'Usuari', select=1, required=True),
        'sending_date': fields.datetime('Data d\'enviament', select=1),
        'order_lines': fields.one2many(
            'giscedata.correos.order.line', 'order_id', 'Línies de la remesa'),
        'state': fields.selection(
            [('draft', 'Esborrany'), ('send', 'Enviament'), ('end', 'Finalitzada'), ('close', 'Tancat')],
            'Estat de la remesa', readonly=True, select=True,
            help='Estat de la remesa a l\'ERP'
        ),
        'n_lines': fields.function(
            _total_lines, string='Total línies', type='integer', readonly=True,
            method=True
        ),
        'order_state': fields.selection(
            [('B', 'Bloquejada'), ('A', 'Oberta'), ('C', 'Tancada')],
            'Estat de la remesa correos', select=1,
            help='Estat de la remesa segons correus'
        ),
        'cierre_date': fields.function(_ff_cierre_date, method=True,
                                       string='Data de tancament',
                                       type='datetime',
                                       select=1, readonly=True,
                                       store=_store_cierre),
        'pendiente_facturar': fields.function(
            _get_total, method=True, type='float',
            digits=(16, 2), readonly=True,
            string='Pendiente a Facturar', multi='totals_calc'
        ),
        'total_facturado': fields.function(
            _get_total, method=True, type='float',
            digits=(16, 2), readonly=True,
            string='Total Facturat', multi='totals_calc'
        ),
        'total': fields.function(
            _get_total, method=True, type='float',
            digits=(16, 2), readonly=True,
            string='Total', multi='totals_calc'
        )
    }

    _defaults = {
        'name': lambda self, cr, uid, context: self.pool.get(
            'ir.sequence').get(cr, uid, 'giscedata.correos.order'),
        'user_id': lambda self, cr, uid, context: uid,
        'state': lambda *a: 'draft',
        'order_state': lambda *a: 'B',
    }

GiscedataCorreosOrder()


class GiscedataCorreosOrderLine(osv.osv):
    _name = 'giscedata.correos.order.line'
    _description = 'This class represents line in correos order'

    def _get_partner_address(self, cursor, uid, ids, field_name, arg, context=None):
        result = {}
        addr_obj = self.pool.get('res.partner.address')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_obj = self.pool.get('account.invoice')

        for record in self.read(cursor, uid, ids, ['partner_id', 'invoice_id'], context=context):
            invoice_id = record['invoice_id'][0]

            partner_name_info = ''
            partner_vat_info = ''
            missing_info = no_partner_found = False

            la_kueri = fact_obj.q(cursor, uid)
            la_kueri = la_kueri.select(
                ['polissa_id.direccio_notificacio.street',
                 'polissa_id.direccio_notificacio.city',
                 'polissa_id.direccio_notificacio.zip',
                 'polissa_id.direccio_notificacio.partner_id.name',
                 'polissa_id.direccio_notificacio.partner_id.vat'
                 ]
            ).where([('invoice_id', '=', invoice_id)])
            cursor.execute(*la_kueri)
            address_info = cursor.dictfetchall()

            if address_info:
                address_info = address_info[0]
                result[record['id']] = {
                    'partner_street': address_info['polissa_id.direccio_notificacio.street'],
                    'partner_city': address_info['polissa_id.direccio_notificacio.city'],
                    'partner_zip': str(address_info['polissa_id.direccio_notificacio.zip'])

                }
                short_result = result[record['id']]
                if not short_result['partner_street'] or not ['partner_city'] or not ['partner_zip']:
                    missing_info = True
                    partner_name_info = address_info['polissa_id.direccio_notificacio.partner_id.name'] or ''
                    partner_vat_info = address_info['polissa_id.direccio_notificacio.partner_id.vat'] or ''
            else:
                la_kueri = inv_obj.q(cursor, uid)
                la_kueri = la_kueri.select(
                    ['address_contact_id.street',
                     'address_contact_id.city',
                     'address_contact_id.zip',
                     'address_contact_id.partner_id.name',
                     'address_contact_id.partner_id.vat'
                     ]
                ).where([('id', '=', invoice_id)])
                cursor.execute(*la_kueri)
                address_info = cursor.dictfetchall()

                if address_info:
                    address_info = address_info[0]
                    result[record['id']] = {
                        'partner_street': address_info[
                            'address_contact_id.street'],
                        'partner_city': address_info[
                            'address_contact_id.city'],
                        'partner_zip': str(
                            address_info['address_contact_id.zip'])
                    }
                    short_result = result[record['id']]
                    if not short_result['partner_street'] or not ['partner_city'] or not ['partner_zip']:
                        missing_info = True
                        partner_name_info = address_info[
                            'address_contact_id.partner_id.name'] or ''
                        partner_vat_info = address_info[
                            'address_contact_id.partner_id.vat'] or ''
                else:
                    missing_info = True
                    no_partner_found = True

            if missing_info:
                name_factura = inv_obj.read(
                    cursor, uid, invoice_id, ['number'], context=context
                )['number']
                if no_partner_found:
                    raise Exception(
                        _('Cal configurar la direccio de '
                          'notificació per a la factura {0}'
                          ).format(name_factura)
                    )
                else:
                    raise Exception(
                        _('Cal configurar la direccio del client {0} amb '
                          'vat {1} associat a la factura {2}'
                          ).format(
                            partner_name_info, partner_vat_info, name_factura
                        )
                    )
        return result

    def set_sicer(self, cursor, uid, ids, context=None):
        conf_obj = self.pool.get('res.config')
        if isinstance(ids, (int, long)):
            ids = [ids]
        product_code = 'CD'
        client_code = conf_obj.get(cursor, uid, 'sicer_partner_code', False)
        if not client_code:
            raise osv.except_osv("Error",
                                 "Cal configurar la variable "
                                 "de configuració sicer_partner_code"
                                 )
        sicer = SicerProduct(product_code, client_code)
        ord_obj = self.pool.get('giscedata.correos.order')
        if client_code:
            for line in self.read(
                    cursor, uid, ids, ['order_id', 'sequence', 'name'], context=context):
                order_code = ord_obj.get_order_code(cursor, uid, line['order_id'][0], context=context)
                sequence = str(line['sequence']).zfill(8)
                sicer_str = sicer.get_sicer(order_code, sequence)
                if sicer_str:
                    self.write(cursor, uid, line['id'], {'sicer': sicer_str},
                               context=context)

    def _ff_delivered(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        for line_f in self.read(
                cursor, uid, ids, ['delivery_status'], context=context):
            delivery_key = DELIVERY_STATUS.get(line_f['delivery_status'], False)

            if delivery_key:
                delivery_key = delivery_key[1]

            res[line_f['id']] = delivery_key
        return res

    def get_selection_delivery(self, cursor, uid, context=None):
        '''Method to get selection'''
        return [(k, DELIVERY_STATUS[k][0]) for k in DELIVERY_STATUS.keys()]

    # def _invoice_state(self, cursor, uid, ids, name, args, context=None):
    #     pend_obj = self.pool.get('account.invoice.pending.state')
    #     inv_obj = self.pool.get('account.invoice')
    #     imd_obj = self.pool.get('ir.model.data')
    #     res = {}
    #     params = ['invoice_id', 'invoice_state', 'delivery_status']
    #     line_reads = self.read(cursor, uid, ids, params, context=context)
    #
    #     correct_state = imd_obj.get_object_reference(
    #         cursor, uid, 'giscedata_facturacio_comer_bono_social',
    #         'correct_bono_social_pending_state'
    #     )[1]
    #
    #     for line in line_reads:
    #         delivery_status = line['delivery_status']
    #         invoice_id = line['invoice_id'][0]
    #         p_state = line['invoice_state'][0]
    #         idd = line['id']
    #
    #         inv_state = inv_obj.read(
    #             cursor, uid, invoice_id, ['state'], context=context
    #         )['state']
    #
    #         if inv_state == 'paid':
    #             p_id = correct_state
    #         else:
    #             p_id = False
    #             if delivery_status:
    #                 noti_type = RECEPTION_STATE_TO_NOTIFICATION_STATE[delivery_status]
    #
    #                 if noti_type == 'entregado':
    #                     p_id = pend_obj.notification_delivered(
    #                         cursor, uid, invoice_id, p_state, context=context)
    #                 elif noti_type == 'enviat':
    #                     p_id = pend_obj.notification_sent(
    #                         cursor, uid, invoice_id, p_state, context=context)
    #                 elif noti_type == 'perdida':
    #                     p_id = pend_obj.notification_lost(
    #                         cursor, uid, invoice_id, p_state, context=context)
    #                 elif noti_type == 'no_entrega':
    #                     p_id = pend_obj.notification_not_delivered(
    #                         cursor, uid, invoice_id, p_state, context=context)
    #
    #         if p_id:
    #             res[idd] = p_id
    #
    #     return res

    def update_pending_states_invoice_state(self, cursor, uid, line_ids, context=None):
        pend_obj = self.pool.get('account.invoice.pending.state')
        inv_obj = self.pool.get('account.invoice')
        imd_obj = self.pool.get('ir.model.data')
        res = {}
        params = ['invoice_id', 'invoice_state', 'delivery_status']
        line_reads = self.read(cursor, uid, line_ids, params, context=context)

        correct_state = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'correct_bono_social_pending_state'
        )[1]

        for line in line_reads:
            delivery_status = line['delivery_status']
            invoice_id = line['invoice_id'][0]
            p_state = line['invoice_state'][0]

            inv_reads = inv_obj.read(
                cursor, uid, invoice_id, ['state', 'pending_state'], context=context
            )
            inv_state = inv_reads['state']
            inv_pending_state = inv_reads['pending_state'][0]

            if inv_state != 'paid':
                if inv_pending_state != p_state:
                    return 'distinct_state'
                else:
                    if delivery_status:
                        noti_type = RECEPTION_STATE_TO_NOTIFICATION_STATE[delivery_status]

                        if noti_type == 'entregado':
                            p_id = pend_obj.notification_delivered(
                                cursor, uid, invoice_id, p_state, context=context)
                        elif noti_type == 'enviat':
                            p_id = pend_obj.notification_sent(
                                cursor, uid, invoice_id, p_state, context=context)
                        elif noti_type == 'perdida':
                            p_id = pend_obj.notification_lost(
                                cursor, uid, invoice_id, p_state, context=context)
                        elif noti_type == 'no_entrega':
                            p_id = pend_obj.notification_not_delivered(
                                cursor, uid, invoice_id, p_state, context=context)
                    return 'processed'
            else:
                return 'paid'

    def _get_invoice_state_from_line(self, cursor, uid, ids, context=None):
        # Use line_obj instated of self because functions under store
        # doesn't has module reference

        line_obj = self.pool.get('giscedata.correos.order.line')

        q = OOQuery(line_obj, cursor, uid)
        sql = q.select(['id',]).where([
            ('invoice_id', 'in', ids),
        ])

        cursor.execute(*sql)
        return [result[0] for result in cursor.fetchall()]

    _store_delivered = {'giscedata.correos.order.line':
                        (lambda self, cr, uid, ids, c=None:
                         ids, ['delivery_status'], 20)
                        }

    _store_delivered_or_invoice_paid = {
        'giscedata.correos.order.line':
            (lambda self, cr, uid, ids, c=None:
             ids, ['delivery_status'], 20),

        'account.invoice': (
            _get_invoice_state_from_line, ['state'], 20)
    }

    _columns = {
        'order_id': fields.many2one(
            'giscedata.correos.order', 'Remesa', required=True,
            ondelete='cascade', readonly=True
        ),
        'invoice_id': fields.many2one(
            'account.invoice', 'Factura', required=True, readonly=True),
        'invoice_state': fields.many2one(
            'account.invoice.pending.state',
            string='Estat pendent actual',
        ),
        # 'invoice_state': fields.function(
        #     _invoice_state, method=True, type='many2one',
        #     relation='account.invoice.pending.state', string='Estat pendent',
        #     store=_store_delivered_or_invoice_paid),
        'partner_id': fields.many2one(
            'res.partner', 'Partner', required=True, readonly=True),
        'partner_city': fields.function(_get_partner_address, type='char',
                                        size=60,
                                        method=True, string='Ciutat',
                                        multi='adreça'),
        'partner_street': fields.function(_get_partner_address, type='char',
                                          size=254,
                                          method=True, string='Adreça',
                                          multi='adreça'
                                          ),
        'partner_zip': fields.function(_get_partner_address, type='char',
                                       size=10, method=True,
                                       string='Codi Postal',
                                       multi='adreça'
                                       ),
        'sequence': fields.integer('Sequence', readonly=True),
        'sicer': fields.char('Codi Sicer', size=50, readonly=True),
        'reception_date': fields.datetime('Data de recepció', select=True),
        'delivery_status': fields.selection(
            get_selection_delivery, 'Estat de l\'enviament', select=True
        ),
        'delivered': fields.function(_ff_delivered, method=True,
                                     string='Entregat?',
                                     type='boolean',
                                     store=_store_delivered,
                                     select=True),
        'origin_invoice_state': fields.many2one(
            'account.invoice.pending.state',
            string='Estat pendent original',
            help='Estat original en que es va afegir la factura'
        ),



    }

    _defaults = {}

GiscedataCorreosOrderLine()


class GiscedataCorreosAttachment(osv.osv):
    _name = 'giscedata.correos.attatchment'
    _description = 'Adjunts de sicer'

    def create_attachment_for_orders(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}

        attc_obj = self.pool.get('ir.attachment')

        order_ids = vals.get('order_ids', [])
        file_datas = vals.get('file', '')
        file_name = vals.get('file_name', '')
        result = vals.get('result', '')
        comments = vals.get('comments', '')

        errors_file = vals.get('errors_file', False)
        errors_fname = vals.get('errors_file_name', False)

        attc_vals = {
            'name': file_name,
            'datas_fname': file_name,
            'datas': file_datas
        }

        attc_id = attc_obj.create(cursor, uid, attc_vals, context=context)

        correos_attc_vals = {
            'attachment_retorno_id': attc_id,
            'order_ids': [(6, 0, order_ids)],
            'attachment_name': file_name,
            'result_import': result,
            'comments': comments
        }

        if errors_file:
            err_attc_vals = {
                'name': errors_fname,
                'datas_fname': errors_fname,
                'datas': errors_file
            }
            attc_err_id = attc_obj.create(
                cursor, uid, err_attc_vals, context=context
            )

            correos_attc_vals.update({
                'attachment_errors_id': attc_err_id
            })

        return self.create(cursor, uid, correos_attc_vals, context=context)

    _columns = {
        'attachment_retorno_id': fields.many2one(
            'ir.attachment', 'Fitxer de retorn SICER'
        ),

        'attachment_name': fields.char('Nombre adjunto', size=30),  # max 23

        'result_import': fields.text('Resultado de importación'),

        'comments': fields.text('Comentarios usuario'),  # Comenarios usuario
        'attachment_errors_id': fields.many2one(
            'ir.attachment', 'Fitxer d\'errors d\'importació'
        ),

    }

    _defaults = {
        'result_import': lambda *a: '',
        'comments': lambda *a: '',
        'attachment_errors_id': lambda *a: False,
    }

GiscedataCorreosAttachment()
