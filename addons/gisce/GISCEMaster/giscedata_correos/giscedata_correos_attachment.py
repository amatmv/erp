# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields


class GiscedataCorreosOrder(osv.osv):
    _name = 'giscedata.correos.order'
    _inherit = 'giscedata.correos.order'

    _columns = {
        'attc_ids': fields.many2many(
            'giscedata.correos.attatchment',
            'giscedata_correos_order_rel',
            'order_id', 'att_order_id',
            'Remeses'
        ),
    }
GiscedataCorreosOrder()


class GiscedataCorreosAttachment(osv.osv):
    _name = 'giscedata.correos.attatchment'
    _inherit = 'giscedata.correos.attatchment'

    _columns = {
        'order_ids': fields.many2many(
            'giscedata.correos.order',
            'giscedata_correos_order_rel',
            'att_order_id', 'order_id',
            'Remeses'
        ),
    }
GiscedataCorreosAttachment()
