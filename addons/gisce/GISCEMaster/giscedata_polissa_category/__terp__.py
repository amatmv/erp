# -*- coding: utf-8 -*-
{
    "name": "Categories per contractes",
    "description": """
	Categories per contractes
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
