## -*- coding: utf-8 -*-
<%
    from operator import attrgetter, itemgetter
    from datetime import datetime, timedelta
    from giscedata_facturacio_comer.giscedata_facturacio_report import origen_energia_espanya_2017
    from base_iban.base_iban import _printed_iban_format
    import json, re

    report_o = pool.get('giscedata.facturacio.factura.report')
    banner_obj = pool.get('report.banner')

    median_consumption_mssg = _(
        u"El seu consum mitjà diari en el període facturat ha sigut de {} € "
        u"que corresponent a {} kWh/dia ({} dies)"
    )

    def get_total_invoice_line_formatted(invoice_line):
        discount = invoice_line.get('discount', '')
        if discount:
            discount = _(u"{}% Dto: ").format(discount)

        return "{}{}".format(discount, formatLang(invoice_line['total']))

    # demo data
    comer_energy_origin = {
        'Renovable': 12.5,      'Cogeneración de Alta Eficiencia': 12.5,
        'Cogeneración': 12.5,   'CC Gas Natural': 12.5,
        'Carbón': 12.5,         'Fuel/Gas': 12.5,
        'Nuclear': 12.5,        'Otras': 12.5,
    }

%>
<%def name="header(factura, comer, mail_format)">
    <%
        from giscedata_facturacio_comer.report.utils import clean_nif
        contact = factura['contact']
    %>
    <table id="header">
        <colgroup>
            <col width="55%"/>
            <col>
        </colgroup>
        <tbody>
            <tr>
                <%
                    rowspan_logo = 'rowspan="4"'
                    if mail_format:
                        rowspan_logo = 'rowspan="5"'
                %>
                <td ${rowspan_logo}><img src="data:image/jpeg;base64,${comer['logo']}"/></td>
                <td>${comer['name']}</td>
            </tr>
            <tr>
                <td>${clean_nif(comer['vat'])}</td>
            </tr>
            <tr>
                <td>${comer['street'].replace('..', '.')}</td>
            </tr>
            <tr>
                <td>${comer['zip']}, ${comer['poblacio']} (${comer['state']})</td>
            </tr>
            %if mail_format:
                <tr>
                    <td>
                        <!-- FINESTRETA -->
                        <table id="finestreta">
                            <tbody>
                                <tr>
                                    <td>${contact['name']}</td>
                                </tr>
                                <tr>
                                    <td>${contact['street'].replace('..', '.')}</td>
                                </tr>
                                <tr>
                                    <td>${contact['zip']} ${contact['city']}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- END FINESTRETA -->
                    </td>
                </tr>
            %endif
        </tbody>
    </table>
</%def>
<%def name="historic_chart(factura)">
    <%
        historic = factura['historic']
        historical = historic['historic']

        accumulated_consumption_mssg = _(
            u"El seu consum acumulat el darrer any ha sigut de {} kWh"
        )
        accumulated_consumption_full_mssg = accumulated_consumption_mssg.format(
            formatLang(historical['year_consumption'], digits=0)
        )

        historic_average_consumption_mssg = _(
            u"El seu consum mitjà diari els últims {} mesos ({} dies) ha estat de "
            u"{} € que corresponen a {} kWh/dia "
        )
        historic_average_consumption_full_mssg = historic_average_consumption_mssg.format(
            historical['months'], historical['days'],
            formatLang(historical['average_cost']),
            formatLang(historical['average_consumption'])
        )
    %>
    <table class="styled-table">
        <thead>
            <tr>
                <th>${_(u"HISTORIAL ÚLTIMS 14 MESOS")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="background-color">
                    <div class="chart_consum" id="chart_consum_${factura['id']}"></div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="italic">
                    ${historic_average_consumption_full_mssg}<br/>
                    ${accumulated_consumption_full_mssg}
                </td>
            </tr>
        </tfoot>
    </table>
    <script>
        // BAR PLOT VARIABLES
        var barplot_height = 200;
        var barplot_width = 695;

        var grad_colors_1 = [['#badcff', '#4D77A4']];

        var grad_colors_3 = [
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4'],
            ['#badcff', '#4D77A4']
        ];

        var factura_id = ${factura['id']};
        var data_consum = ${json.dumps(sorted(historic['historic_js'], key=lambda h: h['mes']))};
    </script>
    <script src="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.js"></script>
</%def>
<%def name="breakdowns_and_urgencies(factura, comer)">
    <%
        supplier = factura['supplier']
        access_contract = supplier['ref'] or ''
    %>
    <table id="avaries-urgencies-atencio-client">
        <colgroup>
            <col width="50%"/>
            <col width="50%"/>
        </colgroup>
        <tbody>
            <tr>
                <td valign="top">
                    <table class="styled-table-with-caption">
                        <caption>${_(u"AVARIES I URGÈNCIES")}</caption>
                        <colgroup>
                            <col width="40%"/>
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>${_(u"Empresa distribuïdora")}</th>
                                <td>${supplier['name']}</td>
                            </tr>
                            <tr>
                                <th>${_(u"Telèfon gratuït")}</th>
                                <td>${supplier['phone']}</td>
                            </tr>
                            <tr>
                                <th>${_(u"Contracte d'accés")}</th>
                                <td>${access_contract}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td valign="top">
                    <table class="styled-table-with-caption">
                        <caption>${_(u"ATENCIÓ AL CLIENT")}</caption>
                        <colgroup>
                            <col width="40%"/>
                        </colgroup>
                        <tbody>
                            <tr>
                                <th>${_(u"Empresa comercialitzadora")}</th>
                                <td>${comer['name']}</td>
                            </tr>
                            <tr>
                                <th>${_(u"Telèfon gratuït")}</th>
                                <td>${comer['phone']}</td>
                            </tr>
                            <tr>
                                <th>${_(u"Correu electrònic")}</th>
                                <td>${comer['email']}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="invoice(factura)">
    <% payer = factura['payer'] %>
    <table class="styled-table" id="factura">
        <thead>
            <tr>
                <th colspan="2">${_(u"FACTURA")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">${_(u"Número de factura")}</td>
                <td>${factura.get('name', '')}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Data Emissió")}</td>
                <td>${formatLang(factura['date'], date=True)}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Periode de facturació")}</td>
                <td>${factura['init_date']} - ${factura['end_date']} (${_(u"Dies")}: ${factura['days']})</td>
            </tr>
            %if not domiciliado:
                <tr>
                    <td class="bold">${_(u"Venciment")}</td>
                    <td>${formatLang(factura['due_date'], date=True)}</td>
                </tr>
            %endif
            <tr>
                <td class="bold">${_(u"Pagador")}</td>
                <td>${payer['name']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"NIF")}</td>
                <td colspan="3">${payer['vat']}</td>
            </tr>
            %if domiciliado:
                <tr>
                    <td class="bold">${_(u"Entitat")}</td>
                    <td colspan="3">${factura['bank']['name']}</td>
                </tr>
                <tr>
                    <td class="bold">${_(u"IBAN")}</td>
                    <td colspan="3">${_printed_iban_format(factura['bank']['iban'])[:-4]} ****</td>
                </tr>
            %endif
        </tbody>
    </table>
</%def>
<%def name="invoice_summary(factura)">
    <%
        detailed_lines = factura['detailed_lines']
        taxes = factura['taxes']
        power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
        other_lines = power_excess_and_other_detailed_info['other_lines']
    %>
    <table id="resum-factura" class="styled-table">
        <thead>
            <tr>
                <th colspan="2">${_(u"RESUM FACTURA")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">${_(u"Per energia")}</td>
                <td class="right">${formatLang(detailed_lines['energy']['total'])}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Per potència")}</td>
                <td class="right">${formatLang(detailed_lines['power']['total'])}</td>
            </tr>
            % if detailed_lines['reactive']['total'] > 0:
                <tr>
                    <td class="bold">${_(u"Penalització per energia reactiva")}</td>
                    <td class="right">${formatLang(detailed_lines['reactive']['total'])}</td>
                </tr>
            % endif
            <tr class="row-separator">
                <td class="bold">${_(u"Impost electricitat")}</td>
                <td class="right">${formatLang(taxes['iese']['total'])}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Lloguer comptador")}</td>
                <td class="right">${factura['rent']}</td>

            </tr>
            % if other_lines['total'] != 0:
                <tr>
                    <td class="bold">${_(u"Altres conceptes")}</td>
                    <td class="right">${formatLang(other_lines['total'])}</td>
                </tr>
            % endif
            % for tax in taxes['iva']['lines']:
                <tr>
                    <td class="bold">${tax['name']}</td>
                    <td class="right">${formatLang(tax['amount'])}</td>
                </tr>
            % endfor
            <tr class="row-separator">
                <td class="bold">${_(u"TOTAL")}</td>
                <td class="bold right">${"{} €".format(formatLang(factura['amount']))}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="amount_destination(factura)">
    <%
        pie = factura['pie']
        pie_regulats = pie['regulats']
        pie_impostos = pie['impostos']
        pie_costos = pie['costos']
        dades_reparto = pie['dades_reparto']
        for repartiment in dades_reparto:
            repartiment[3] = formatLang(repartiment[3])
    %>
    <table id="desti-factura" class="styled-table">
        <thead>
            <tr>
                <th class="seccio">${_(u"DESTÍ DE LA FACTURA")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>${_(u"El destí de l'import de la seva factura ({} €) és el següent:").format(formatLang(factura['amount']))}</td>
            </tr>
            <tr>
                <td>
                    <div class="chart_desti" id="chart_desti_${factura['id']}"></div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td class="italic">${_(u"Als imports indicats en el diagrama se'ls ha d'afegir, en el seu cas, el lloguer dels equips de mesura i control")}</td>
            </tr>
        </tfoot>
    </table>
    <script>
        // PIE CHART VARIABLES
        var piechart_height = 177;
        var piechart_width = 700;

        //pie start_position
        var pie_left = 130;

        var pie_total = ${pie['total']};
        var pie_data = [
            {val: ${pie_regulats}, perc: 30, code: "REG"},
            {val: ${pie_costos}, perc: 55, code: "PROD"},
            {val: ${pie_impostos},  perc: 15 ,code: "IMP"}
        ];

        var pie_etiquetes = {
            'REG': {t: ['${formatLang(float(pie_regulats))} €','${_(u"Costos regulats")}'], w:100},
            'IMP': {t: ['${formatLang(float(pie_impostos))} €','${_(u"Impostos aplicats")}'], w:100},
            'PROD': {t: ['${formatLang(float(pie_costos))} €','${_(u"Costos de producció electricitat")}'], w:150}
        };

        var reparto = ${json.dumps(pie['reparto'])};
        var dades_reparto = ${json.dumps(dades_reparto)};
    </script>
    <script src="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.js"></script>
</%def>
<%def name="energy_origin(comer_energy_origin, spain_energy_origin)">
    <table width="94%" align="center" class="styled-table">
        <thead>
            <tr>
                <th colspan="3">${_(u"Origen de l'electricitat")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div id="energy-origin-comer"></div>
                </td>
                <td>
                    <table id="energy-origin">
                        <colgroup>
                            <col width="5%"/>
                            <col width="43%"/>
                            <col width="26%"/>
                            <col width="26%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th colspan="2">${_(u"Origen")}</th>
                                <th>${comer['name']}</th>
                                ## TODO any dinamic
                                <th>${_(u"Mitjana sistema elèctric espanyol (2017)")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><div class="square color-renovable"></div></td>
                                <td>${_(u"Renovable")}</td>
                                <td class="center">${comer_energy_origin['Renovable']}%</td>
                                <td class="center">${spain_energy_origin['Renovable']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-cogeneracio-alta-eficiencia"></div></td>
                                <td>${_(u"Cogeneració d'Alta Eficiència")}</td>
                                <td class="center">${comer_energy_origin['Cogeneración de Alta Eficiencia']}%</td>
                                <td class="center">${spain_energy_origin['Cogeneración de Alta Eficiencia']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-cogeneracio"></div></td>
                                <td>${_(u"Cogeneració")}</td>
                                <td class="center">${comer_energy_origin['Cogeneración']}%</td>
                                <td class="center">${spain_energy_origin['Cogeneración']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-cc-gas-natural"></div></td>
                                <td>${_(u"CC Gas Natural")}</td>
                                <td class="center">${comer_energy_origin['CC Gas Natural']}%</td>
                                <td class="center">${spain_energy_origin['CC Gas Natural']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-carbo"></div></td>
                                <td>${_(u"Carbó")}</td>
                                <td class="center">${comer_energy_origin['Carbón']}%</td>
                                <td class="center">${spain_energy_origin['Carbón']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-fuel-gas"></div></td>
                                <td>${_(u"Fuel/Gas")}</td>
                                <td class="center">${comer_energy_origin['Fuel/Gas']}%</td>
                                <td class="center">${spain_energy_origin['Fuel/Gas']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-nuclear"></div></td>
                                <td>${_(u"Nuclear")}</td>
                                <td class="center">${comer_energy_origin['Nuclear']}%</td>
                                <td class="center">${spain_energy_origin['Nuclear']}%</td>
                            </tr>
                            <tr>
                                <td><div class="square color-altres"></div></td>
                                <td>${_(u"Altres")}</td>
                                <td class="center">${comer_energy_origin['Otras']}%</td>
                                <td class="center">${spain_energy_origin['Otras']}%</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <div id="energy-origin-country"></div>
                </td>
            </tr>
        </tbody>
    </table>
    <script>
        var donut_height = 170;
        var colors = [
            '#579834', '#579834',
            '#89c563', '#d7ef81',
            '#d9d82e', '#eea620',
            '#fc6230', '#ee3e26'
        ];
        cols = [
            ['Renovable', ${comer_energy_origin['Renovable']}],
            ['Cogeneración de Alta Eficiencia', ${comer_energy_origin['Cogeneración de Alta Eficiencia']}],
            ['Cogeneración', ${comer_energy_origin['Cogeneración']}],
            ['CC Gas Natural', ${comer_energy_origin['CC Gas Natural']}],
            ['Carbón', ${comer_energy_origin['Carbón']}],
            ['Fuel/Gas', ${comer_energy_origin['Fuel/Gas']}],
            ['Nuclear', ${comer_energy_origin['Nuclear']}],
            ['Otras', ${comer_energy_origin['Otras']}],
        ];
        donut_chart(donut_height, donut_height, cols, colors, '#energy-origin-comer', false, 'Panticosa');
        cols = [
            ['Renovable', ${spain_energy_origin['Renovable']}],
            ['Cogeneración de Alta Eficiencia', ${spain_energy_origin['Cogeneración de Alta Eficiencia']}],
            ['Cogeneración', ${spain_energy_origin['Cogeneración']}],
            ['CC Gas Natural', ${spain_energy_origin['CC Gas Natural']}],
            ['Carbón', ${spain_energy_origin['Carbón']}],
            ['Fuel/Gas', ${spain_energy_origin['Fuel/Gas']}],
            ['Nuclear', ${spain_energy_origin['Nuclear']}],
            ['Otras', ${spain_energy_origin['Otras']}],
        ];
        donut_chart(donut_height, donut_height, cols, colors, '#energy-origin-country', false, 'España')
    </script>
</%def>
<%def name="environmental_impact(cursor, uid, factura_date)">
    <%
        environmental_impact_co2 = banner_obj.get_banner(
            cursor, uid, 'giscedata.facturacio.factura', factura_date,
            code='environmental_impact_co2'
        )
        environmental_impact_radioactive = banner_obj.get_banner(
            cursor, uid, 'giscedata.facturacio.factura', factura_date,
            code='environmental_impact_radioactive'
        )

        if environmental_impact_co2:
            environmental_impact_co2 = '<img src="data:image/jpeg;base64,{}">'.format(environmental_impact_co2)
        else:
            environmental_impact_co2 = '<img src="{}/giscedata_facturacio_comer/report/assets/images/environmental_impact_co2.jpg">'.format(addons_path)

        if environmental_impact_radioactive:
            environmental_impact_radioactive = '<img src="data:image/jpeg;base64,{}">'.format(environmental_impact_radioactive)
        else:
            environmental_impact_radioactive = '<img src="{}/giscedata_facturacio_comer/report/assets/images/environmental_impact_radioactive.jpg">'.format(addons_path)
    %>
    <table id="environmental-impact" class="styled-table">
        <colgroup>
            <col width="40%"/>
            <col width="20%"/>
            <col width="40%"/>
        </colgroup>
        <thead>
            <tr>
                <th colspan="3">${_(u"Impacte medioambiental")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div id="environmental-impact-co2">${environmental_impact_co2}</div>
                </td>
                <td id="environmental-impact-descr" class="center">
                    ${_(u"L'impacte medioambiental de la seva electricitat consumida depèn de les fonts d'energía utilitzades per a la seva generació.")}
                </td>
                <td>
                    <div id="environmental-impact-radioactive">${environmental_impact_radioactive}</div>
                </td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="contract(factura)">
    <%
        holder = factura['holder']
        policy = factura['policy']
        supplier = factura['supplier']
        access_contract = supplier['ref'] or ''
    %>
    <table class="styled-table">
        <thead>
            <tr>
                <th colspan="4">${_(u"CONTRACTE")}</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="bold">${_(u"Titular")}</td>
                <td>${holder['name']}</td>
                <td class="bold">${_(u"NIF")}</td>
                <td>${holder['vat']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Contracte del suministrament")}</td>
                <td>${policy['name']}</td>
                <td class="bold">${_(u"Contracte d'accés")}</td>
                <td>${access_contract}</td>
            </tr>
            <tr>
                <td rowspan="2" class="bold">${_(u"Data alta")}</td>
                <td rowspan="2">${formatLang(policy['start_date'], date=True)}</td>
                <td class="bold">${_(u"Data final")}</td>
                <td>${formatLang(policy['end_date'], date=True)}</td>
            </tr>
            <tr>
                <td colspan="2" class="little">${_(u"(renovació anual automàtica)")}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Tarifa")}</td>
                <td>${factura['tariff']}</td>
                <td class="bold">${_(u"CNAE")}</td>
                <td>${policy['cnae']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Producte Comer")}</td>
                <td colspan="3">${policy['pricelist']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"CUPS")}</td>
                <td colspan="3">${factura['cups']['name']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Distribuïdora")}</td>
                <td colspan="3">${supplier['name']}</td>
            </tr>
            <tr>
                <td class="bold">${_(u"Adreça de subministrament")}</td>
                <td colspan="3">${factura['cups']['address']}</td>
            </tr>
        </tbody>
    </table>
</%def>
<%def name="invoice_detail_energy(factura)">
    <table class="styled-inner-table">
        <colgroup>
            <col class="detall-factura-inner-table-col-1"/>
            <col class="detall-factura-inner-table-col-2"/>
            <col class="detall-factura-inner-table-col-3"/>
            <col class="detall-factura-inner-table-col-4"/>
            <col class="detall-factura-inner-table-col-5"/>
        </colgroup>
        <thead>
            <tr>
                <th>${_(u"Terme d'energia")}</th>
                <th>${_(u"Consum")} (kWh)</th>
                <th>${_(u"Preu")} (€/kWh)</th>
                <th colspan="2">${_(u"Total")} (€)</th>
             </tr>
        </thead>
        <tbody>
            % for energy_line in factura['detailed_lines']['energy']['lines']:
                <tr>
                    <td class="bold center">${energy_line['name']}</td>
                    <td class="right">${int(energy_line['consumption'])}</td>
                    <td class="right">${formatLang(energy_line['price'], digits=6)}</td>
                    <td class="right">${get_total_invoice_line_formatted(energy_line)}</td>
                    <td class="left white toll italic">(${_(u"dels quals peatges {}").format(formatLang(energy_line['toll']))})</td>
                </tr>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="invoice_detail_power(factura)">
    <table class="styled-inner-table">
        <colgroup>
            <col width="20%"/>
            <col width="20%"/>
            <col width="20%"/>
            <col/>
            <col/>
            <col width="20%"/>
        </colgroup>
        <thead>
            <tr>
                <th>${_(u"Terme de potència")}</th>
                <th>${_(u"Consum")} (kW)</th>
                <th>${_(u"Preu")} (€/kW-${_(u"dia")})</th>
                % if monthly_invoice:
                    <th>${_(u"Mesos")}</th>
                %else:
                    <th>${_(u"Dies")}</th>
                %endif
                <th colspan="2">${_(u"Total")} (€)</th>
            </tr>
        </thead>
        <tbody>
            % for power_line in factura['detailed_lines']['power']['lines']:
                <tr>
                    <td class="bold center white">${power_line['name']}</td>
                    <td class="right white">${formatLang(power_line['consumption'], digits=3)}</td>
                    <td class="right white">${formatLang(power_line['price'], digits=6)}</td>
                    <td class="center white">${int(power_line['time'])}</td>
                    <td class="right white">${get_total_invoice_line_formatted(power_line)}</td>
                    <td class="left white italic toll">(${_(u"dels quals peatges {}").format(formatLang(power_line['toll']))})</td>
                </tr>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="invoice_detail_reactive(factura)">
    <table class="styled-inner-table">
        <colgroup>
            <col class="detall-factura-inner-table-col-1"/>
            <col class="detall-factura-inner-table-col-2"/>
            <col class="detall-factura-inner-table-col-3"/>
            <col class="detall-factura-inner-table-col-4"/>
            <col class="detall-factura-inner-table-col-5"/>
        </colgroup>
        <thead>
            <tr>
                <th>${_(u"Energia reactiva")}</th>
                <th>${_(u"Excés")} (kVArh)</th>
                <th>${_(u"Preu")} (€/kVArh)</th>
                <th colspan="2">${_(u"Total")} (€)</th>
            </tr>
        </thead>
        <tbody>
            % for reactive_line in factura['detailed_lines']['reactive']['lines']:
                <tr>
                    <td class="bold center">${reactive_line['name']}</td>
                    <td class="right">${formatLang(reactive_line['consumption'])}</td>
                    <td class="right">${formatLang(reactive_line['price'], digits=6)}</td>
                    <td class="right">${get_total_invoice_line_formatted(reactive_line)}</td>
                    <td class="left white toll italic">(${_(u"dels quals peatges {}").format(formatLang(reactive_line['toll']))})</td>
                </tr>
            % endfor
        </tbody>
    </table>
</%def>
<%def name="power_excess(factura)">
    <%
        power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
        power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
    %>
    <table class="styled-inner-table">
        <colgroup>
            <col class="detall-factura-inner-table-col-1"/>
            <col class="detall-factura-inner-table-col-2"/>
            <col class="detall-factura-inner-table-col-3"/>
            <col class="detall-factura-inner-table-col-4"/>
            <col class="detall-factura-inner-table-col-5"/>
        </colgroup>
        <thead>
            <tr>
                <th>${_(u"Excés potència")}</th>
                <th>${_(u"Excés")} (kVArh)</th>
                <th>${_(u"Preu")} (€/kVArh)</th>
                <th colspan="2">${_(u"Total")} (€)</th>
            </tr>
        </thead>
        <tbody>
            <% total_exces = 0.0 %>
            % for power_line_with_excess in power_lines_with_excess:
                <tr>
                    <td class="bold center">${power_line_with_excess['name']}</td>
                    <td class="right">${formatLang(power_line_with_excess['excess'])}</td>
                    <td class="right">${formatLang(power_line_with_excess['price'], digits=6)}</td>
                    <td class="center" colspan="2">${power_line_with_excess['total']}</td>
                </tr>
                <% total_exces += power_line_with_excess['total'] %>
            % endfor
        </tbody>
    </table>
</%def>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/factura.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/assets/pie-chart.css"/>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/assets/consumption-chart.css"/>
        <script src="${assets_path}/js/d3.min.js"></script>
        <script src="${assets_path}/js/c3.min.js"></script>
        <script src="${addons_path}/giscedata_facturacio_comer/report/assets/environmental_impact.js"></script>
        <style>
            @font-face {
                font-family: "Roboto";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
            @font-face {
                font-family: "Roboto";
                src: url("${assets_path}/fonts/Roboto/Roboto-Bold.ttf") format('truetype');
                font-weight: bold;
            }
        </style>
    </head>
    <%
        # Company information
        comer = report_o.get_company_info(cursor, uid, company.id)[company.id]
        lateral_info =  comer['lateral_info'] or ''
    %>
    <body>
        %for factura in factura_data:
            <%
                contact = factura['contact']
                detailed_lines = factura['detailed_lines']
                invoiced_power = factura['invoiced_power']
                periods_and_readings = factura['periods_and_readings']
                periods = periods_and_readings['periods']
                periods_a = periods['active']
                periods_r = periods['reactive']
                periods_m = periods['maximeter']
                readings = periods_and_readings['readings']
                readings_a = readings['active']
                readings_r = readings['reactive']
                readings_m = readings['maximeter']
                policy = factura['policy']
                power_excess_and_other_detailed_info = factura['power_excess_and_other_detailed_info']
                other_lines = power_excess_and_other_detailed_info['other_lines']
                meters_rent = power_excess_and_other_detailed_info['meters_rent']
                power_lines_with_excess = power_excess_and_other_detailed_info['lines_with_power_excess']
                taxes = factura['taxes']

                median_consumption_full_mssg = median_consumption_mssg.format(
                    formatLang(detailed_lines['energy']['total']/(factura['days'] or 1.0)),
                    formatLang(factura['diari_factura_actual_kwh']),
                    factura['days'] or 1
                )
## _____________________________________________________________________________
                # Banner images
                factura_date = datetime.strptime(factura['date'], '%d/%m/%Y').strftime('%Y-%m-%d')
                banner_img = banner_obj.get_banner(
                    cursor, uid, 'giscedata.facturacio.factura',
                    factura_date, code='main_banner'
                )

                setLang(factura['lang_partner'])
                domiciliado = factura['payment_type_code'] == 'RECIBO_CSB'
                mail_format = True
            %>


            <div id="lateral-info" class="center">${lateral_info}</div>
            <div id="first-page">
                ${header(factura, comer, mail_format)}
                <!-- BANNER + (TITULAR + RESUM FACTURA)-->
                <table id="banner-holder-summary">
                    <colgroup>
                        <col width="60%"/>
                        <col width="40%"/>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td id="banner-cell" valign="top">
##                                 <!-- BANNER -->
##                                 <div class="banner">
##                                     <%
##                                         banner_img = banner_obj.get_banner(
##                                             cursor, uid, 'giscedata.facturacio.factura',
##                                             factura_date
##                                         )
##                                     %>
##                                     %if banner_img:
##                                         <img src="data:image/jpeg;base64,${banner_img}">
## ##                                  If necessary, uncomment and will show the demo banner
## ##                                     %else:
## ##                                         <img src="${addons_path}/giscedata_facturacio_comer/report/assets/banner-sample.png"/>
##                                     %endif
##                                 </div>
##                                 <!-- END BANNER -->
                                    ${invoice(factura)}
                                </td>
                            <td valign="top">
                                %if not mail_format:
                                    <!-- DESTINATARI -->
                                    <table class="styled-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">${_(u"DESTINATARI")}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bold">${_(u"Nom")}</td>
                                                <td>${contact['name']}</td>
                                            </tr>
                                            <tr>
                                                <td class="bold">${_(u"Direcció")}</td>
                                                <td colspan="3">${contact['street'].replace('..', '.')} ${contact['zip']} ${contact['city']}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- END DESTINATARI -->
                                %endif
                                ${invoice_summary(factura)}
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                        </tr>
                    </tbody>
                </table>
                ${contract(factura)}
                ${historic_chart(factura)}
                ${breakdowns_and_urgencies(factura, comer)}
            </div>
            <!-- END FIRST PAGE -->
            <p style="page-break-after:always; clear:both"></p>
            <!-- SECOND PAGE -->
            <div id="second-page">
                <!-- DETALLE FACTURA -->
                <table id="detall-factura" class="styled-table center-th">
                    <colgroup>
                        <col width="32%" class="background-color"/>
                        <col width="20%"/>
                        <col/>
                        <col width="10%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th colspan="3">${_(u"DETALL FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3">${invoice_detail_energy(factura)}</td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="4" class="bold right white">${formatLang(detailed_lines['energy']['total'])} €</td>
                        </tr>
                        <tr>
                            <td colspan="3">${invoice_detail_power(factura)}</td>
                        </tr>
                        <tr class="row-separator">
                            <td colspan="5" class="bold right white">${formatLang(detailed_lines['power']['total'])} €</td>
                        </tr>
                        %if detailed_lines['reactive']['lines']:
                            <tr>
                                <td colspan="3">${invoice_detail_reactive(factura)}</td>
                            </tr>
                            <tr class="row-separator">
                                <td colspan="4" class="bold right white">${formatLang(detailed_lines['reactive']['total'])} €</td>
                            </tr>
                        %endif
                        %if power_lines_with_excess:
                            <tr>
                                <td colspan="3">${power_excess(factura)}</td>
                            </tr>
                            <tr class="row-separator">
                                <td colspan="4" class="bold right white">${formatLang(total_exces)} €</td>
                            </tr>
                        %endif
                        % for iese_line in taxes['iese']['lines']:
                            <tr class="row-separator">
                                <th>${_(u"Impost de l'electricitat")}</th>
                                <td class="right">${formatLang(iese_line['base_amount'])} €</td>
                                <td class="right">${u"5,11269632%"}</td>
                                <td class="bold right">${formatLang(iese_line['tax_amount'])} €</td>
                            </tr>
                        % endfor
                        % for meter_rent in meters_rent:
                            <tr class="row-separator">
                                <th>${_(u"Lloguer de comptador")}</th>
                                <td></td>
                                <td></td>
                                <td class="bold right">${formatLang(meter_rent['amount'])} €</td>
                            </tr>
                        % endfor
                        %if other_lines['lines']:
                            <tr class="row-separator">
                                <% rowspan = len(other_lines['lines']) + 1 %>
                                <th rowspan="${rowspan}">${_(u"Altres")}</th>
                                <td colspan="3"></td>
                            </tr>
                            % for other_line in other_lines['lines']:
                                <tr>
                                    <td colspan="2" class="bold">${other_line['name']}</td>
                                    <td class="bold right">${formatLang(other_line['price'])} €</td>
                                </tr>
                            % endfor
                        %endif
                        % for iva_line in taxes['iva']['lines']:
                            <tr class="row-separator">
                                <th>${iva_line['name']}</th>
                                <td class="right">${formatLang(iva_line['base'])} €</td>
                                <td class="right">${iva_line['percentage']}</td>
                                <td class="bold right">${formatLang(iva_line['amount'])} €</td>
                            </tr>
                        % endfor
                        <tr class="row-separator">
                            <th>${_(u"TOTAL FACTURA")}</th>
                            <td></td>
                            <td></td>
                            <td class="bold right">${formatLang(factura['amount'])} €</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4" class="white">${_(u"Els preus dels termes de peatge d’accés són els publicats a ORDEN ETU/1976/2016")}</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="white">${_(u"Els preus del lloguer dels comptadors són els establerts a ORDEN IET/1491/2013")}</td>
                        </tr>
                    </tfoot>
                </table>
                <!-- END DETALLE FACTURA -->
                <!-- LECTURES -->
                <table id="consum-electric" class="styled-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th>${_(u"CONSUM ELÈCTRIC")}</th>
                            % for period_name in periods_a:
                                <th>${period_name}</th>
                            % endfor
                        </tr>
                    </thead>
                    <tbody>
                        <!-- ACTIVA -->
                        %if readings_a['readings']:
                            <tr>
                                <% rowspan_vertical_text = len(readings_a['readings']) * 3 + 2 %>
                                <th rowspan="${rowspan_vertical_text}" class="rotate h65px"><div>${_(u"ENERGIA")}</div></th>
                            </tr>
                            % for meter in sorted(readings_a['readings']):
                                <%
                                    meter_readings =  readings_a['readings'][meter]
                                    previous_reading_title = _(u"Lectura anterior {} ({})").format(
                                        formatLang(meter_readings['P1']['previous_date'], date=True),
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura actual {} ({})").format(
                                        formatLang(meter_readings['P1']['current_date'], date=True),
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr>
                                    <td class="bold light-blue">${_(u"Número de comptador")}</td>
                                    % for period_name in periods_a:
                                        <td class="center light-blue">${meter}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${previous_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${current_reading_title}</td>
                                    % for period in periods_a:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold">${_(u"Total període")}</td>
                                % for period in periods_a:
                                    <td class="right">${formatLang(readings_a['total_consumption'][period], digits=0)} kWh</td>
                                %endfor
                            </tr>
                        %endif
                        <!-- END ACTIVA -->
                        <!-- REACTIVA -->
                        % if readings_r['readings'] or te_maximetre:
                            <tr>
                                <% rowspan_vertical_text = len(readings_r['readings']) * 3 + 2 %>
                                <th rowspan="${rowspan_vertical_text}" class="rotate h65px"><div>${_(u"REACTIVA")}</div></th>
                            </tr>
                            % for meter in sorted(readings_r['readings']):
                                <%
                                    meter_readings =  readings_r['readings'][meter]
                                    previous_reading_title = _(u"Lectura anterior {} ({})").format(
                                        formatLang(meter_readings['P1']['previous_date'], date=True),
                                        meter_readings['P1']['previous_origin']
                                    )
                                    current_reading_title = _(u"Lectura actual {} ({})").format(
                                        formatLang(meter_readings['P1']['current_date'], date=True),
                                        meter_readings['P1']['current_origin']
                                    )
                                %>
                                <tr class="row-separator">
                                    <td class="bold light-blue">${_(u"Número de comptador")}</td>
                                    % for period_name in periods_r:
                                            <td class="center light-blue">${meter}</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${previous_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['previous_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                                <tr>
                                    <td class="bold">${current_reading_title}</td>
                                    % for period in periods_r:
                                        <td class="right">${formatLang(meter_readings[period]['current_reading'], digits=0)} kWh</td>
                                    % endfor
                                </tr>
                            % endfor
                            <tr>
                                <td class="bold">${_(u"Total període")}</td>
                                % for period_name in periods_r:
                                    <td class="right">${formatLang(readings_r['total_consumption'][period], digits=0)} kWh</td>
                                % endfor
                            </tr>
                        %endif
                        <!-- END REACTIVA -->
                        <!-- MAXIMETRE -->
                        % if policy['has_maximeter']:
                            <tr class="header">
                                <th rowspan="4" class="rotate h65px"> <div>${_(u"MAXÍMETRE")}</div></th>
                            </tr>
                            <tr class="row-separator">
                                <td class="bold">${_(u"Potència contractada")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(readings_m[period]['power'], digits=3)} kW</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Lectura maxímetre")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(readings_m[period]['maximeter'], digits=3)} kW</td>
                                % endfor
                            </tr>
                            <tr>
                                <td class="bold">${_(u"Potència facturada període")}</td>
                                % for period in periods_m:
                                    <td class="right">${formatLang(factura['invoiced_power'][period], digits=3)} kW</td>
                                % endfor
                            </tr>
                        %endif
                        <!-- MAXIMETRE -->
                    </tbody>
                    <tfoot>
                        <tr>
                            <% colspan =  len(periods_a) + 2 %>
                            <td colspan="${colspan}" class="center">
                                ${median_consumption_full_mssg}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <p style="page-break-after:always; clear:both"></p>
            <div id="third-page">
                ${amount_destination(factura)}
                ${energy_origin(comer_energy_origin, origen_energia_espanya_2017)}
                ${environmental_impact(cursor, uid, factura_date)}
            </div>
            % if factura != factura_data[-1]:
                <p style="page-break-after:always; clear:both"></p>
            % endif
        %endfor
    </body>
</html>

<!--
https://www.boe.es/diario_boe/txt.php?id=BOE-A-2014-5655
Cuarto. Contenido mínimo de la factura para consumidores con derecho al precio
voluntario para el pequeño consumidor acogidos a la oferta a precio fijo anual
de la comercializadora de referencia y para consumidores en mercado libre cuyo
suministro se realice en baja tensión de hasta 15 kW de potencia contratada.

En todo caso, el contenido mínimo a incluir en las facturas que deberán remitir
los comercializadores de referencia a los consumidores con derecho a precio
voluntario para el pequeño consumidor acogidos a la oferta anual a precio fijo,
y los comercializadores a los consumidores en condiciones de libre mercado cuyo
suministro se realice en baja tensión de hasta 15 kW de potencia será el
siguiente:

a) Identidad y domicilio social de la empresa comercializadora.

b) Número de factura y fecha de cargo o fecha límite de pago, según corresponda.

c) Periodo de facturación.

d) Nombre, apellidos y NIF del titular del contrato.

e) Peaje de acceso de aplicación al suministro.

f) Potencia contratada.

g) Código unificado de punto de suministro.

h) Referencia del contrato de suministro y nombre de la empresa comercializadora.

i) Referencia del contrato de acceso, en su caso, y nombre de la empresa
distribuidora a la que esté conectado el punto de suministro.

j) Fecha final del contrato de suministro.

k) Fecha de emisión de la factura.

l) Dirección del suministro.

m) Lecturas del contador en el periodo de facturación, indicando si son reales
o estimadas, y consumo de electricidad en dicho periodo desagregado por
periodos tarifarios, en su caso.

n) Historial de consumo de los últimos catorce meses.

o) Importe total de la factura, desglosado del siguiente modo:

    – Precios aplicados por el suministrador a la potencia contratada y a la
    energía consumida, y cuantías obtenidas por la aplicación de cada uno de
    estos conceptos.

    Adicionalmente, deberá indicarse qué parte de las cuantías obtenidas según
    lo anterior corresponde a la aplicación de los precios de los peajes de
    acceso a las redes o de cualquier otro precio en vigor. Deberá hacerse
    constar igualmente la disposición oficial donde se fijen dichos peajes de
    acceso a las redes y otros precios.

    – Impuestos y gravámenes aplicados.

    – Precio del alquiler de los equipos de medida y control, de acuerdo con la
    normativa vigente.

    – Precios de otros servicios prestados, incluidos en su caso los precios de
    los servicios de valor añadido y de mantenimiento que se propongan, de
    acuerdo con la normativa vigente.

    – Otros conceptos, entre los que se incluye el detalle de las
    regularizaciones u otros conceptos que se lleven a cabo.

p) Destino del importe total de la factura, diferenciando:

    – Costes asociados a la regulación eléctrica, entre los que figurarán:

        a. los incentivos a las energías renovables, cogeneración y residuos,

        b. los costes asociados a las redes de transporte y distribución,

        c. otros costes regulados.

    – Coste de producción de electricidad.

    – Impuestos aplicados.

q) Información sobre la contribución de cada fuente energética primaria en la
mezcla global de energías primarias utilizadas para producir la electricidad en
el conjunto de la energía vendida por la comercializadora y en el conjunto del
sistema eléctrico español durante el año anterior (o el previo al anterior, en
las facturas emitidas durante los meses de enero a marzo), que deberá ajustarse
a lo publicado en la Circular 1/2008, de 7 de febrero, de la Comisión Nacional
de Energía, de información al consumidor sobre el origen de la electricidad
consumida y su impacto sobre el medio ambiente.

r) Referencia a las fuentes en las que se encuentre publicada la información
sobre el impacto en el medio ambiente, al menos en cuanto a las emisiones
totales de CO2 y los residuos radiactivos habidos en el sector eléctrico
durante el año anterior (o el previo al anterior, en las facturas emitidas
durante los meses de enero a marzo), señalando la contribución equivalente que
hubiera tenido en dichos impactos la electricidad vendida por la empresa
durante el año anterior (o el previo al anterior, en las facturas emitidas
durante los meses de enero a marzo), según lo dispuesto en la Circular 1/2008,
de 7 de febrero, de la Comisión Nacional de Energía, de información al
consumidor sobre el origen de la electricidad consumida y su impacto sobre el
medio ambiente.

s) Información sobre sus derechos respecto de las vías de resolución de
reclamaciones.

t) Dirección postal, número de teléfono gratuito, número de fax o dirección de
correo electrónico, a los que los consumidores puedan dirigir sus quejas,
reclamaciones e incidencias en relación al servicio contratado u ofertado, así
como solicitudes de información sobre los aspectos relativos a la contratación
y suministro o comunicaciones.

u) Número de teléfono del distribuidor al que esté conectado el punto de
suministro para averías.

Octavo. Información sobre el destino del importe de la factura.

1. Para la determinación de la cuantía correspondiente a los costes regulados
que aparecen en el área g) de la factura, relativa al destino del importe de la
misma, se aplicará el siguiente criterio:

    Se considerarán incluidos en el mismo:

    – Los peajes de acceso a las redes de transporte y distribución y cargos
    que correspondan.

    – Los pagos al operador del sistema y al operador del mercado en concepto
    de retribución de estos operadores.

    – Los pagos por capacidad que corresponda en función de los periodos
    tarifarios.

    – Se tendrán en cuenta las cuantías que resulten de aplicar las pérdidas
    publicadas por el Operador del Sistema.

2. El resto de costes que compongan el importe total de la factura, salvo los
relativos a los impuestos aplicados y al alquiler de los equipos de medida y
control, formarán parte de la cuantía denominada «coste de producción de
electricidad y margen de comercialización» en los modelos I, II y III de los
anexos y «coste de producción de electricidad» en los modelos IV y V de los
anexos.

3. Hasta la aprobación y efectiva aplicación de la orden ministerial que, en
su caso, revise los peajes de acceso contenidos en la vigente Orden
IET/107/2014, de 31 de enero, por la que se revisan los peajes de acceso de
energía eléctrica para 2014 y, en todo caso, para el año 2014:

    a) el coste del servicio de gestión de la demanda de interrumpibilidad
    será igualmente incluido entre los costes regulados que aparecen en el
    gráfico del área g) de la factura.

    b) Los porcentajes a aplicar sobre el importe de los costes regulados
    calculados según el presente apartado a efectos del reparto que se llevará
    a cabo en el gráfico del área g) de la factura, serán los siguientes:

    Incentivos a las energías renovables, cogeneración y residuos: 36,28%.

    Coste de redes de distribución y transporte: 32,12%.

    Otros costes regulados: 31,60%.
-->