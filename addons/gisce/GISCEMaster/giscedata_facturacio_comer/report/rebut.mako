<%
    from datetime import datetime
    from numbertoletters import number_to_letters

    pool = objects[0].pool
    users_obj = pool.get('res.users')
    fact_obj = pool.get('giscedata.facturacio.factura')
    def clean(text):
        return text or ''

    def localize_period(period, locale):
        if period:
            import babel
            from datetime import datetime
            dtf = datetime.strptime(period, '%Y-%m-%d')
            dtf = dtf.strftime("%y%m%d")
            dt = datetime.strptime(dtf, '%y%m%d')
            return babel.dates.format_datetime(dt, 'd LLLL Y', locale=locale)
        else:
            return ''

    def get_localitat():
        usuari = users_obj.browse(cursor, uid, uid)
        return usuari.address_id.id_municipi.name
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            body{
                font-family: "Liberation Sans";
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
            }
            table{
                font-size: 10px;
                width: 98%;
                float: right;
                border-collapse: collapse;
                border: 0px;
            }
            table td{
                padding-left: 5px;
                padding-right: 5px;
            }
            table td.titol{
                font-size: 7px;
                border-top: 1px solid black;
                border-left: 1px solid black;
                border-right: 1px solid black;
            }
            table td.field{
                border-bottom: 1px solid black;
                border-left: 1px solid black;
                border-right: 1px solid black;
                font-weight: bold;
                font-size: 12px;
            }
            table td.field_blank{
                font-weight: bold;
                font-size: 12px;
            }
            table td.field_bottom{
                border-bottom: 1px solid black;
                font-weight: bold;
                font-size: 12px;
            }
            table td.bold_clear{
                font-weight: bold;
                font-size: 12px;
            }
            #logo{
                position: relative;
                float: left;
                width: 10%;
                height: 400px;
            }
            #img_logo{
                position: relative;
                -ms-transform: rotate(-90deg); /* IE 9 */
                -webkit-transform: rotate(-90deg); /* Chrome, Safari, Opera */
                transform: rotate(90deg);
                left: -110px;
                top: 105px;
                height: 100px;
            }
            #rebut{
                position: relative;
                float: left;
                height: 400px;
                width: 90%;
            }
            .bank_table{
                border-bottom: 1px solid black;
                border-left: 1px solid black;
                border-right: 1px solid black;
            }
            #adreca{
                position: relative;
                top: 20px;
                float: left;
                width: 380px;
                left: 5px;
            }
            #signatura{
                position: relative;
                top: 20px;
                float: left;
                left: 15px;
                width: 220px;
            }
            .space{
                margin-top: 25px;
            }
            ul {
                list-style: none;
                margin-left:10px;
                padding:0px;
            }
            ul li:before {
                color: #000000;
                content: '» ';
                font-size: 1.2em;
                font-weight: bold;
            }
            ${css}
        </style>
    </head>
    <body>
        <%
            i = 1
        %>
        %for fact in objects:
            <div id="logo">
                <img id="img_logo" src="data:image/jpeg;base64,${company.logo}" >
            </div>
            <div id="rebut">
                <table class="space">
                    <tr>
                        <td class="titol" style="width: 90px;">${_(u"REBUT")}</td>
                        <td class="titol">${_(u"LOCALITAT D'EXPEDICIÓ")}</td>
                        <td class="titol" style="width: 120px;">${_(u"IMPORT TOTAL €uros")}</td>
                    </tr>
                    <tr>
                        % if fact.group_move_id:
                            <td class="field">${fact.group_move_id.ref}</td>
                        % elif fact.number:
                            <td class="field">${fact.number}</td>
                        % else:
                            <td class="field"></td>
                        % endif
                        <td class="field">${get_localitat()}</td>
                        <td class="field" style="text-align: right;">${formatLang(amount_grouped(fact, 'amount_total'), monetary=True)}</td>
                    </tr>
                </table>
                <table style="margin-top: -1px;">
                    <tr>
                        <td class="titol" style="width: 90px;">${_(u"DATA EMISSIÓ")}</td>
                        <td class="titol">${_(u"DATA VENCIMENT")}</td>
                    </tr>
                    <tr>
                        <td class="field" style="width: 50%;">${localize_period(datetime.now().strftime("%Y-%m-%d"), 'es_ES')}</td>
                        <td class="field">${localize_period(fact.date_due or False, 'es_ES')}</td>
                    </tr>
                </table>
                <table class="space">
                    <tr>
                        <td style="width: 65px;"></td>
                        <td class="field_bottom" style="width: 150px;">${_(u"Factura Nº:")}
                        % if fact.group_move_id:
                            ${fact.group_move_id.ref}
                        % else:
                             ${fact.number or ''}
                        % endif
                        </td>
                        <td class="field_bottom">${_(u"Data Factura:")}&emsp;${fact.date_invoice}</td>
                        <td class="field_bottom">${_(u"Client:")}&emsp;${fact.polissa_id.name}</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="field_bottom" style="width: 65px;">${_(u"€UROS")}</td>
                        <td class="field_bottom">
                            % if fact.lang_partner == 'es_ES':
                                ${number_to_letters(amount_grouped(fact, 'amount_total')).upper()}
                            % else:
                                ${amount_grouped(fact, 'amount_total')}
                            % endif
                        </td>
                    </tr>
                </table>
                <table style="border: 1px solid black; margin-top: 5px;">
                    <tr style="border: 1px solid black;">
                        <td style="border: 1px solid black;"><i>${_(u"Domicili de pagament")}</i></td>
                        <td><i>${_(u"NÚM. COMPTE")}</i></td>
                    </tr>
                    <tr>
                        <td style="border-right: 1px solid black;">${_(u"PERSONA O ENTITAT")} &nbsp;&nbsp;<font size="2px"><b>${fact.partner_id.name}</b></font></td>
                        <td style="text-align: center;"><font size="2px"><b>${fact.partner_bank.iban or ''}</b></font></td>
                    </tr>
                    <tr>
                        <td class="bank_table">${_(u"DIRECCIÓ")} &nbsp;&nbsp;<font size="2px"><b>${fact.address_invoice_id.street}</b></font></td></td>
                        <td class="bank_table"></td>
                    </tr>
                </table>
                <div style="clear:both"></div>
                <div id="adreca">
                    <table style="border: 1px solid black;">
                        <tr>
                            <td>${_(u"NOM I DOMICILI DEL PAGADOR")}</td>
                        </tr>
                        <tr>
                            <td class="field_blank">
                            %if fact.partner_bank.owner_id:
                                ${fact.partner_bank.owner_id.name}
                            %else:
                                <!-- Obtenir nom i domicili del pagador des del contracte -->
                                ${fact.polissa_id.pagador.name}
                            %endif
                            </td>
                        </tr>
                        <tr>
                            <td class="field_blank">
                            % if fact.partner_bank.owner_address_id:
                                ${fact.partner_bank.owner_address_id.street}
                            % else:
                                ${fact.polissa_id.pagador.address.street[0]}
                            % endif
                            </td>
                        </tr>
                        <tr>
                            <td class="field_blank">
                            % if fact.partner_bank.owner_address_id:
                                ${fact.partner_bank.owner_address_id.zip}&emsp;${fact.partner_bank.owner_address_id.state_id.name}
                            % else:
                                ${fact.polissa_id.pagador.address.zip[0]}&emsp;${fact.polissa_id.pagador.address.state_id.name[0]}
                            % endif
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="signatura">
                    <i>
                        ${fact.polissa_id.comercialitzadora.name}
                        <br>
                    </i>
                </div>
            </div>
            % if fact.group_move_id:
                % if fact.group_move_id:
                    <h3>${_(u"Resum factures agrupades")}:</h3>
                    <ul>
                        <%
                            inv_ids = [l.invoice.id for l in
                                            fact.group_move_id.line_id if l.invoice]
                            fact_ids = fact_obj.search(
                                cursor, uid, [('invoice_id', 'in', inv_ids)])
                        %>
                        % for l in fact_obj.browse(cursor, uid, fact_ids):
                            <li>
                                <%
                                    sentit = 1.0
                                    if l.type in['out_refund', 'in_refund']:
                                        sentit = -1.0
                                    if l.tipo_rectificadora in ['A', 'B', 'BRA']:
                                        text = ' (Factura abonadora de la factura {0})'.format(
                                            l.ref.number or '')
                                    elif l.tipo_rectificadora in ['R', 'RA']:
                                        text = ' (Factura rectificadora de la factura {0})'.format(
                                            l.ref.number or '')
                                    else:
                                        text = ''
                                %>
                                ${l.number} total: ${formatLang(sentit * l.amount_total, monetary=True)} &euro; ${text}
                            </li>
                        % endfor
                    </ul>
                % endif
                <h3><strong>Total: ${formatLang(amount_grouped(fact, 'amount_total'), monetary=True)} &euro;</strong></h3>
            % endif
            % if i < len(objects):
                <div style="clear: both;"></div>
                <p style="page-break-after:always"></p>
            % endif
            <%
                i += 1
            %>
        %endfor
    </body>
</html>
