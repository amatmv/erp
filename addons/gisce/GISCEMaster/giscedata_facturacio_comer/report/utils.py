# -*- coding: utf-8 -*-
from dateutil import parser


def has_setu35_line(cursor, uid, pool, inv):
    imd_obj = pool.get('ir.model.data')
    con48_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_comer', 'concepte_48'
    )[1]
    altres_lines = []
    for l in inv.linia_ids:
        if l.tipus == 'altres' and l.product_id.id == con48_id:
            altres_lines.append(l)
    sups_obj = pool.get('giscedata.suplements.territorials.2013.comer')
    search_params = [
        ('cups', '=', inv.cups_id.name)
    ]
    sups_ids = sups_obj.search(
        cursor, uid, search_params
    )
    return altres_lines and sups_ids


def pretty_date(data, type='normal'):
    any, mes, dia = data.split('-')
    if type == 'normal':
        return '%s/%s/%s' % (dia, mes, any)
    else:
        return ''


def get_giscedata_fact(cursor, invoice_id):
    sql = """SELECT * FROM giscedata_facturacio_factura WHERE id=%s""" % invoice_id.id
    cursor.execute(sql)
    return cursor.dictfetchall()


def get_discriminador(cursor, factura_id):
    sql = """SELECT count(*) as total FROM giscedata_facturacio_factura_linia
    WHERE tipus='energia' and factura_id = %s
    """ % factura_id
    cursor.execute(sql)
    return cursor.dictfetchall()[0]['total']


def get_distri_phone(cursor, uid, polissa):
    """ Per telèfons de ENDESA segons CUPS, aprofitant funció de switching"""
    sw_obj = polissa.pool.get('giscedata.switching')
    pa_obj = polissa.pool.get('res.partner.address')

    partner_id = sw_obj.partner_map(cursor, uid,
                                    polissa.cups, polissa.distribuidora.id)
    if partner_id:
        pa_ids = pa_obj.search(cursor, uid, [('partner_id', '=', partner_id)])
        return (pa_obj.read(cursor, uid, [pa_ids[0]], ['phone'])[0]['phone'] or
                polissa.distribuidora.address[0].phone)
    else:
        return polissa.distribuidora.address[0].phone


def get_other_info(cursor, factura_id):
    sql = """SELECT
             giscedata_cups_ps.name AS giscedata_cups_ps_name,
             coalesce(giscedata_cups_ps.nv, '') AS giscedata_cups_ps_nv,
             coalesce(giscedata_cups_ps.pnp, '') AS giscedata_cups_ps_pnp,
             coalesce(giscedata_cups_ps.es, '') AS giscedata_cups_ps_es,
             coalesce(giscedata_cups_ps.pt, '') AS giscedata_cups_ps_pt,
             coalesce(giscedata_cups_ps.pu, '') AS giscedata_cups_ps_pu,
             coalesce(giscedata_cups_ps.cpo, '') AS giscedata_cups_ps_cpo,
             coalesce(giscedata_cups_ps.cpa, '') AS giscedata_cups_ps_cpa,
             coalesce(giscedata_cups_ps.ref_catastral,'') as giscedata_cups_ps_ref_cat,
             res_municipi_cups.name AS res_municipi_cups_name,
             account_invoice.number AS account_invoice_number,
             account_invoice.id AS account_invoice_id,
             account_invoice.date_invoice AS account_invoice_date_invoice,
             giscedata_facturacio_factura.data_inici AS giscedata_facturacio_factura_data_inici,
             giscedata_facturacio_factura.data_final AS giscedata_facturacio_factura_data_final,
             giscedata_facturacio_factura.potencia AS giscedata_facturacio_factura_potencia,
             account_invoice.amount_total AS account_invoice_amount_total,
             giscedata_facturacio_factura.id AS giscedata_facturacio_factura_id,
             product_pricelist.name AS product_pricelist_name,
             giscedata_polissa.id AS giscedata_polissa_id,
             giscedata_polissa.name AS giscedata_polissa_name,
             giscedata_polissa.ref_dist AS giscedata_polissa_ref_dist,
             giscedata_polissa.data_alta as giscedata_polissa_data_alta,
             giscedata_polissa.data_baixa as giscedata_polissa_data_baixa,
             giscedata_polissa.potencia as giscedata_polissa_potencia,
             giscedata_tensions_tensio.name as giscedata_polissa_tensio,
             giscemisc_cnae.name as giscedata_polissa_cnae,
             giscedata_polissa_tarifa.name as giscedata_polissa_tarifa,
             res_partner.name AS res_partner_name,
             res_partner.ref AS res_partner_ref,
             res_partner.vat AS res_partner_vat,
             res_bank.name AS res_bank_name,
             payment_type.name AS payment_type_name,
             res_partner_bank.iban AS res_partner_bank_acc_number,
             account_invoice.date_due AS account_invoice_date_due,
             invoice_address.name AS invoice_address_name,
             invoice_address.street AS invoice_address_street,
             invoice_address.street2 AS invoice_address_street2,
             invoice_address.city AS invoice_address_city,
             invoice_address.zip AS invoice_address_zip,
             invoice_state.name AS invoice_state_name,
             invoice_country.name AS invoice_country_name,
             account_invoice.company_id AS account_invoice_company_id,
             giscedata_polissa.distribuidora AS giscedata_polissa_distribuidora,
             giscedata_polissa.id AS giscedata_polissa_id
        FROM
         giscedata_facturacio_factura giscedata_facturacio_factura
         LEFT JOIN giscedata_cups_ps giscedata_cups_ps ON  giscedata_cups_ps.id = giscedata_facturacio_factura.cups_id
         LEFT JOIN res_municipi res_municipi_cups ON giscedata_cups_ps.id_municipi = res_municipi_cups.id
             LEFT JOIN account_invoice account_invoice ON giscedata_facturacio_factura.invoice_id = account_invoice.id
             LEFT JOIN product_pricelist product_pricelist ON giscedata_facturacio_factura.llista_preu = product_pricelist.id
             LEFT JOIN giscedata_polissa giscedata_polissa ON giscedata_facturacio_factura.polissa_id = giscedata_polissa.id
             LEFT JOIN res_partner res_partner ON giscedata_polissa.pagador = res_partner.id
             LEFT JOIN res_partner_bank ON account_invoice.partner_bank = res_partner_bank.id
             LEFT JOIN res_bank ON res_partner_bank.bank = res_bank.id
             LEFT JOIN res_partner_address invoice_address ON account_invoice.address_invoice_id = invoice_address.id
             LEFT JOIN res_country_state invoice_state ON invoice_address.state_id = invoice_state.id
             LEFT JOIN res_country invoice_country ON invoice_address.country_id = invoice_country.id
             LEFT JOIN payment_type ON account_invoice.payment_type = payment_type.id
             LEFT JOIN giscemisc_cnae giscemisc_cnae ON giscemisc_cnae.id =  giscedata_polissa.cnae
             LEFT JOIN giscedata_tensions_tensio giscedata_tensions_tensio ON giscedata_tensions_tensio.id =  giscedata_polissa.tensio_normalitzada
             LEFT JOIN giscedata_polissa_tarifa giscedata_polissa_tarifa ON giscedata_polissa_tarifa.id =  giscedata_polissa.tarifa

         WHERE giscedata_facturacio_factura.id = %s """ % (factura_id)
    cursor.execute(sql)
    return cursor.dictfetchall()


def get_historics(cursor, polissa_id, data_inici, data_final):
    sql = """SELECT * FROM (
                SELECT mes AS mes,
                periode AS periode,
                sum(suma_fact) AS facturat,
                sum(suma_consum) AS consum,
                min(data_ini) AS data_ini,
                max(data_fin) AS data_fin
                FROM (
                SELECT f.polissa_id AS polissa_id,
                       to_char(f.data_inici, 'YYYY/MM') AS mes,
                       pt.name AS periode,
                       COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
                       COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
                       MIN(f.data_inici) data_ini,
                       MAX(f.data_final) data_fin
                       FROM
                            giscedata_facturacio_factura f
                            LEFT JOIN account_invoice i on f.invoice_id = i.id
                            LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
                            LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
                            LEFT JOIN product_product pp on il.product_id=pp.id
                            LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
                       WHERE
                            fl.tipus = 'energia' AND
                            f.polissa_id = %(p_id)s AND
                            f.data_inici <= %(data_inicial)s AND
                            f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
                            AND (fl.isdiscount IS NULL OR NOT fl.isdiscount)
                            AND i.type in('out_invoice', 'out_refund')
                       GROUP BY
                            f.polissa_id, pt.name, f.data_inici
                       ORDER BY f.data_inici DESC ) AS consums
                GROUP BY polissa_id, periode, mes
                ORDER BY mes DESC, periode ASC
                ) consums_ordenats
                ORDER BY mes ASC
        """
    cursor.execute(
        sql,
        {'p_id': polissa_id,
         'data_inicial': data_inici,
         'data_final': data_final,
         })
    return cursor.dictfetchall()


def get_linies_tipus(cursor, factura_id, tipus):
    sql = """SELECT
             giscedata_facturacio_factura_linia."price_unit_multi" AS giscedata_facturacio_factura_linia_price_unit_multi,
             giscedata_facturacio_factura_linia."factura_id" AS giscedata_facturacio_factura_linia_factura_id,
             giscedata_facturacio_factura_linia.tipus AS giscedata_facturacio_factura_linia_tipus,
             giscedata_facturacio_factura_linia.multi AS giscedata_facturacio_factura_linia_multi,
             account_invoice_line."name" AS account_invoice_line_name,
             account_invoice_line."discount" AS account_invoice_line_discount,
             account_invoice_line."quantity" AS account_invoice_line_quantity,
             coalesce(product_uom."name",'') AS product_uom_name,
             account_invoice_line."price_subtotal" AS account_invoice_line_price_subtotal
        FROM
             "public"."account_invoice_line" account_invoice_line INNER JOIN "public"."giscedata_facturacio_factura_linia" giscedata_facturacio_factura_linia ON account_invoice_line."id" = giscedata_facturacio_factura_linia."invoice_line_id"
             LEFT JOIN "public"."product_uom" product_uom ON account_invoice_line."uos_id" = product_uom."id"
        WHERE giscedata_facturacio_factura_linia.factura_id  = %s
              and giscedata_facturacio_factura_linia.tipus in (%s)
        ORDER BY giscedata_facturacio_factura_linia.tipus,account_invoice_line.name

        """ % (factura_id, "'" + "','".join(tipus) + "'")
    cursor.execute(sql)
    return cursor.dictfetchall()


def get_discounts(cursor, factura, tipus='energia'):
    if tipus == 'energia':
        linies = factura.linies_energia
    elif tipus == 'potencia':
        linies = factura.linies_potencia

    discount_lines = [x for x in linies if x.isdiscount]
    discounts = {}
    if discount_lines:
        for discount in discount_lines:
            product_id = discount.product_id.id
            real = sum(
                [x.price_unit for x in linies
                 if x.product_id.id == product_id
                 and not x.isdiscount]
            )
            discount_price = sum(
                [x.price_unit for x in linies
                 if x.product_id.id == product_id and x.isdiscount]
            )
            discounts[product_id] = abs(round(
                (discount_price / real) * 100.0))
    return discounts


def get_with_line_dicount(line, positive=True):
        linia_key = '{}_{}'.format(line.name, line.quantity)
        factura = line.factura_id
        energy_lines = {}
        for linia in factura.linies_energia:
            key = '{}_{}'.format(linia.name, linia.quantity)
            if key == linia_key:
                if not linia.isdiscount and not linia.discount != 0.0:
                    energy_lines.setdefault(key, [])
                    energy_lines[key].append(linia)
        if energy_lines[linia_key]:
            orig_linia = energy_lines[linia_key][0]
            orig_price = orig_linia.price_unit_multi
            discount_price = line.price_unit_multi
            if positive:
                discount_price = abs(discount_price)
            discount_percent = round((discount_price/orig_price)*100.0, 2)
            return discount_percent, orig_price

        else:
            return 0.0, 0.0


def get_line_discount(line, positive=True):
    '''

    :param line: Invoice line
    :param positive: If true return discount in absolute value
    :return: Tuple with discount with float and original price
    '''
    if line.isdiscount:
        note = line.note
        if note and '%' in note:
            discount, original_price = note.split('%')
            discount = float(discount.strip().replace(',', '.'))
            original_price = float(
                original_price.replace('sobre', '').strip().replace(',', '.')
            )
            if positive:
                discount = abs(discount)
            return discount, original_price
        else:
            return get_with_line_dicount(line, positive)
    else:
        return 0.0, 0.0


def get_distribution_percentage(cursor, uid, pool, data):
    """
    For a date I return the i c o of that date
    :param cursor:
    :param uid:
    :param pool:
    :param data: date in YYYY-mm-dd
    :return: Dictionari as: dict['i'] = percentage
    """

    repart_obj = pool.get(
        'giscedata.facturacio.factura.repartiment.linia')
    sql = repart_obj.q(cursor, uid).read(['percentatge', 'codi']).where([
        ('repartiment_id.data_inici', '<=', data),
        '|',
        ('repartiment_id.data_fi', '>=', data),
        ('repartiment_id.data_fi', '=', None)
    ])

    res = {}

    for value in sql:
        res[value['codi']] = value['percentatge']

    return res


def clean_nif(nif):
    if nif:
        return nif.replace('ES', '')
    else:
        return ''


def setstr(string):
    return string if string else ""


def erp_date_to_dmy(ymd):
    """
    Parses date in ERP format (%Y-%m-%d) to standard format (%d-%m-%Y).
    :param ymd: date ERP formated.
    :type: str
    :return: date ERP formated.
    :rtype: str
    """
    return parser.parse(ymd).strftime('%d/%m/%Y')
