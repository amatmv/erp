function donut_chart(height, width, cols, colors, div_id, show_legend, title, thickness) {
    thickness = typeof thickness !== 'undefined' ? thickness : 40;
    return c3.generate({
        bindto: div_id,
        size: {
            height: height,
            width: width,
        },
        data: {
            columns: cols,
            type: 'donut',
            order: null
        },
        color: {
            pattern: colors
        },
        donut: {
            width: thickness,
            title: title,
            label: {
              format: function (value) { return ''; }
            }
        },
        legend: {
            show: show_legend,
            position: 'right'
        },
    });
}