# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config
import pooler
import tempfile
import pypdftk

OPTIMIZED_INVOICES = ['giscedata_facturacio_comer/report/factura.mako']


class FacturaReportWebkitHTML(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(FacturaReportWebkitHTML, self).__init__(
            cursor, uid, name, context=context
        )

        company_address_ids = self.localcontext['company'].partner_id.address_get(
            ['contact', 'default']
        )
        company_address_id = company_address_ids.get('contact', False)
        if not company_address_id:
            company_address_id = company_address_ids['default']

        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
            'pool': self.pool,
            'company_address': self.pool.get('res.partner.address').browse(
                cursor, uid, company_address_id
            )
        })


class FacturaReportWebkitParserHTML(webkit_report.WebKitParser):

    def __init__(
            self, name='report.giscedata.facturacio.factura',
            table='giscedata.facturacio.factura', rml=None,
            parser=FacturaReportWebkitHTML, header=True, store=False,
            factura_conceptes_path=None
    ):
        self.tmpl2 = factura_conceptes_path
        if rml is None:
            rml = False
        super(FacturaReportWebkitParserHTML, self).__init__(
            name, table, rml, parser, header, store
        )

    def create(self, cursor, uid, ids, data, context=None):
        """
        If tmpl2, then will check which invoice has to be printed: normal (01)
        or conceptos (other than 01). Else will always print the normal one.
        :param cursor:
        :param uid:
        :param ids:
        :param data:
        :param context:
        :return:
        """
        if self.tmpl2:
            if context is None:
                context = {}
            pool = pooler.get_pool(cursor.dbname)
            factura_o = pool.get('giscedata.facturacio.factura')
            to_join = []

            factura_f = ['tipo_factura']
            dmn = [('id', 'in', ids)]
            factura_vs = factura_o.q(cursor, uid).read(factura_f).where(dmn)

            ir_obj = pool.get('ir.actions.report.xml')
            report_xml_ids = ir_obj.search(
                cursor, uid, [('report_name', '=', self.name[7:])], context=context
            )
            file_type = 'pdf'

            for factura_v in factura_vs:
                report_path = self.tmpl
                if factura_v['tipo_factura'] != '01':
                    report_path = self.tmpl2

                ir_obj.write(
                    cursor, uid, report_xml_ids, {'report_webkit': report_path},
                    context=context
                )

                res = super(FacturaReportWebkitParserHTML, self).create(
                    cursor, uid, [factura_v['id']], data, context=context
                )
                res_path, file_type = webkit_report.write_file_and_get_path(res)
                to_join.append(res_path)

            content = webkit_report.concat_files(to_join, file_type)

            res = content, file_type
        else:
            res = super(FacturaReportWebkitParserHTML, self).create(
                cursor, uid, ids, data, context=context
            )

        return res

    def getObjects(self, cursor, uid, ids, context):
        """
        Obtains the browse record list of the invoices if the report is not
        optimized. If the report is optimized, then on the mako report there
        will be a new variable (factura_data) that contains all the necessary
        data to print the invoices.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids.
        :param context:
        :return: <Browse.Record> list if the report is not optimized.
        """
        pool = pooler.get_pool(cursor.dbname)
        ir_obj = pool.get('ir.actions.report.xml')
        report_xml_ids = ir_obj.search(
            cursor, uid, [('report_name', '=', self.name[7:])], context=context
        )
        report_xml_vs = ir_obj.read(cursor, uid, report_xml_ids, ['report_webkit'])
        report_path = report_xml_vs[0]['report_webkit']

        is_factura_conceptes = report_path == self.tmpl2

        # The optimized reports are those who no longer need the objects
        # attribute and iterate over factura_data.
        optimized_report = report_path in OPTIMIZED_INVOICES

        objects = super(FacturaReportWebkitParserHTML, self).getObjects(
            cursor, uid, ids, context
        )

        if is_factura_conceptes or not optimized_report:
            return objects

        report_o = self.pool.get('giscedata.facturacio.factura.report')

        factura_data = report_o.get_invoice_info(
            cursor, uid, ids, context=context
        ).values()

        factura_ids = [f['id'] for f in factura_data]

        power_excess_and_other_detailed_info = report_o.get_power_exces_and_other_detailed_info(
            cursor, uid, factura_ids, context=context
        )
        detailed_lines = report_o.get_lines_detailed_info(
            cursor, uid, factura_ids, context=context
        )

        other_lines_total = dict.fromkeys(factura_ids, 0)
        for factura_id, info in power_excess_and_other_detailed_info.items():
            other_lines_total[factura_id] = info['other_lines']['total']

        pie = report_o.get_pie_data(
            cursor, uid, factura_ids, other_lines_total, context=context
        )
        historic = report_o.get_historics(
            cursor, uid, factura_ids, context=context
        )
        taxes = report_o.get_taxes(cursor, uid, factura_ids, context=context)
        periods_and_readings = report_o.get_periods_and_readings(
            cursor, uid, factura_ids, context=context
        )

        periods_m = dict.fromkeys(factura_ids, None)
        for factura_id, info in periods_and_readings.items():
            periods_m[factura_id] = info['periods']['maximeter']

        payer = report_o.get_payer_info(
            cursor, uid, factura_ids, context=context
        )
        holder = report_o.get_holder_info(
            cursor, uid, factura_ids, context=context
        )
        contact = report_o.get_contact_info(
            cursor, uid, factura_ids, context=context
        )
        policy = report_o.get_policy_info(
            cursor, uid, factura_ids, context=context
        )
        supplier = report_o.get_supplier_info(
            cursor, uid, factura_ids, context=context
        )
        invoiced_power = report_o.get_invoiced_power(
            cursor, uid, factura_ids, periods_m, context=context
        )

        for factura_index in range(0, len(factura_ids)):
            factura_id = factura_ids[factura_index]
            factura_data[factura_index].update({
                'contact': contact[factura_id],
                'detailed_lines': detailed_lines[factura_id],
                'historic': historic[factura_id],
                'holder': holder[factura_id],
                'invoiced_power': invoiced_power[factura_id],
                'payer': payer[factura_id],
                'periods_and_readings': periods_and_readings[factura_id],
                'pie': pie[factura_id],
                'policy': policy[factura_id],
                'power_excess_and_other_detailed_info': power_excess_and_other_detailed_info[factura_id],
                'supplier': supplier[factura_id],
                'taxes': taxes[factura_id],
            })

        self.parser_instance.localcontext.update({
            'factura_data': factura_data,
        })

        return objects


FacturaReportWebkitParserHTML(
    rml='giscedata_facturacio_comer/report/factura.mako',
    factura_conceptes_path='giscedata_facturacio_comer/report/factura_conceptes.mako',
)
