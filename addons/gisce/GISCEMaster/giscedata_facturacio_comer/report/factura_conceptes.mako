## -*- coding: utf-8 -*-
<%
    from giscedata_facturacio_comer.report.utils import clean_nif
    from base_iban.base_iban import _printed_iban_format

    def clean(text):
        return text or ''

    company_town = company_address.id_municipi
    company_town = company_town.name if company_town else company_address.city
%>
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_comer/report/factura_conceptes.css"/>
        <style type="text/css">
            ${css}
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
    </head>
    <body>
        %for invoice in objects :
            <%
                total_altres = 0
                total_altres = sum([l.price_subtotal for l in invoice.linia_ids])
                polissa = invoice.polissa_id
                contact_address = invoice.address_contact_id or invoice.polissa_id.direccio_notificacio
                contact_town = contact_address.id_municipi
                contact_town = contact_town.name if contact_town else contact_address.city
                domiciliat = invoice.payment_type.code == 'RECIBO_CSB'
            %>
            <div id="lateral-info" class="center">
                ${clean(company.rml_footer2)}
            </div>
            <!-- PAGE -->
            <div id="page">
                <!-- HEADER -->
                <table id="header">
                    <colgroup>
                        <col width="60%"/>
                        <col>
                    </colgroup>
                    <tbody>
                        <tr>
                            <td rowspan="4"><img src="data:image/jpeg;base64,${company.logo}"/></td>
                            <td>${invoice.company_id.partner_id.name}</td>
                        </tr>
                        <tr>
                            <td>${clean_nif(invoice.company_id.partner_id.vat)}</td>
                        </tr>
                        <tr>
                            <td>${company_address.street.replace('..', '.')}</td>
                        </tr>
                        <tr>
                            <td>${company_address.zip}, ${company_town} (${company_address.state_id.name})</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END HEADER -->
                <!-- FINESTRETA -->
                <div id="finestreta" class="right-50">
                    <table>
                        <tr>
                            <td>${contact_address.name}</td>
                        </tr>
                        <tr>
                            <td>${contact_address.street.replace('..', '.')}</td>
                        </tr>
                        <tr>
                            <td>${contact_address.zip} ${contact_town}</td>
                        </tr>
                        <tr>
                            <td>${contact_address.state_id.name}</td>
                        </tr>
                    </table>
                </div>
                <!-- END FINESTRETA -->
                <!-- INFO -->
                <table id="info-base" class="styled-table round-top-borders">
                    <caption>
                        ${_(u"Factura")}
                        %if invoice.tipo_rectificadora == "A":
                            ${_(u"Abonadora")}
                        %endif
                    </caption>
                    <colgroup>
                        <col />
                        %if invoice.tipo_rectificadora == "A":
                            <col />
                        %endif
                        <col width="16%"/>
                        <col width="16%"/>
                        <col width="16%"/>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>${_(u"Factura Nº")}</th>
                            %if invoice.tipo_rectificadora == "A":
                                <th>${_(u"Factura Abonada Nº")}</th>
                            %endif
                            <th>${_(u"Data")}</th>
                            <th>${_(u"Nº Client")}</th>
                            <th>${_(u"DNI/CIF")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            %if invoice.tipo_rectificadora == "A":
                                <td class="center">${'{} {}'.format(clean(invoice.number), _(u"(ABONADORA)"))}</td>
                                <td class="center">${clean(invoice.rectifying_id.number)}</td>
                            %else:
                                <td class="center">${clean(invoice.number)}</td>
                            %endif
                            <td class="center">${invoice.date_invoice}</td>
                            <td class="center">${clean(polissa.name)}</td>
                            <td class="center">${clean(invoice.partner_id.vat).replace('ES','')}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END INFO -->
                <!-- SUPPLY POINT DATA -->
                <table id="supply-data" class="styled-table">
                    <caption>${_(u"Dades del punt de subministrament")}</caption>
                    <thead>
                        <tr>
                            <th>${_(u"Nom del titular")}</th>
                            <th>${_(u"Població")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="center">${clean(polissa.titular.name)}</td>
                            <td class="center">${clean(polissa.cups.id_poblacio.name)}</td>
                        </tr>
                        <tr>
                            <th>${_(u"Domicili")}</th>
                            <th>${_(u"CUPS")}</th>
                        </tr>
                        <tr>
                            <td class="center">${clean(polissa.cups.direccio)}</td>
                            <td class="center">${clean(polissa.cups.name)}</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END SUPPLY POINT DATA -->
                <!-- LINES -->
                <div class="content-concepts">
                    <table class="styled-table">
                        <thead>
                            <tr>
                                <th class="concept">${_(u"Descripció del concepte")}</th>
                                <th class="iva">${_(u"IVA (%)")}</th>
                                <th class="import">${_(u"Import (€)")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                iva_set = set()
                            %>
                            %for l in invoice.linia_ids:
                                <tr>
                                    <td>${l.name}</td>
                                    <td class="right">
                                        %for tax in l.invoice_line_tax_id:
                                            %if 'IVA' in tax.name:
                                                ${formatLang(tax.amount * 100.0)}
                                                <%
                                                    iva_set.add(tax.amount * 100.0)
                                                %>
                                            %endif
                                        %endfor
                                    </td>
                                    <td class="right">${formatLang(l.price_subtotal)}</td>
                                </tr>
                            %endfor
                        </tbody>
                    </table>
                </div>
                <!-- END LINES -->
                <!-- SUMMARY -->
                <table class="summary">
                    <thead>
                        <tr>
                            <th>${_(u"Suma imports")}</th>
                            <th>${_(u"Base imponible")}</th>
                            <th>${_(u"% IVA")}</th>
                            <th>${_(u"Import IVA")}</th>
                            <th class="bold">${_(u"TOTAL FACTURA")}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="right">${formatLang(invoice.amount_untaxed)} €</td>
                            <td class="right">${formatLang(sum([tl.base_amount for tl in invoice.tax_line if 'IVA' in tl.name] or [0]))} €</td>
                            <td class="right">${len(list(iva_set)) and formatLang(list(iva_set)[0]) or ''} €</td>
                            <td class="right">${formatLang(invoice.amount_tax)} €</td>
                            <td class="right bold">${formatLang(invoice.amount_total)} €</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END SUMMARY -->
                <!-- PAYMENT DATA -->
                <table id="payment-data" class="styled-table">
                    <tbody>
                        <tr>
                            <th>${_(u"Forma de pagament")}</th>
                            <td>${clean(invoice.payment_type.name)}</td>
                        </tr>
                        %if domiciliat:
                            <tr>
                                <th>${_(u"Domiciliació")}</th>
                                <td>${clean(invoice.partner_bank.name)}</td>
                            </tr>
                            <tr>
                                <th>${_(u"IBAN")}</th>
                                <td>${_printed_iban_format(invoice.partner_bank.iban)[:-4]} ****</td>
                            </tr>
                        %endif
                    </tbody>
                </table>
                <!-- END PAYMENT DATA -->
                % if invoice != objects[-1]:
                    <p style="page-break-after:always;"></p>
                % endif
            </div>
            <!-- END PAGE -->
        %endfor
    </body>
</html>
