# -*- coding: utf-8 -*-

import jasper_reports
from datetime import datetime
from time import mktime, strptime


def _f_avis_impagament(cr, uid, ids, data, context):
    params = data['form']['parameters']
    params['data_tall'] = datetime.fromtimestamp(
                                        mktime(strptime(params['data_tall'],
                                        '%Y-%m-%d')))
    params['data_limit'] = datetime.fromtimestamp(
                                        mktime(strptime(params['data_limit'],
                                         '%Y-%m-%d')))

    return {
        'ids': ids,
        'parameters': data['form']['parameters'],
    }

jasper_reports.report_jasper(
   'report.giscedata.facturacio.factura.avis_impagat',
   'giscedata.facturacio.factura',
   parser=_f_avis_impagament
)

