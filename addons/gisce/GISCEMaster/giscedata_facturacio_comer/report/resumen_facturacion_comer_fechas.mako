<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        ${css}
        </style>
        <style type="text/css">
            body {
                font-family: sans-serif;
                font-size: 12px;
                margin-right: 10px;
                margin-left: 10px;
                margin-top: 20px;
            }

            thead {
            	display: table-header-group;
            }
			tr {
				page-break-inside: avoid;
			}
            td, th {
                text-align: center;
                vertical-align: top;
            }
            table{
                font-size: 10px;
                border-collapse: collapse;
                width: 100%;
            }
            .text_capcalera table td,
            .text_capcalera table th {
                text-align: left;
                font-size: 10px;
            }
            .foot {
                border-top: double 1px #9F9F9F;
                background-color: #dddddd;
            }
            .split{
                border-left: 1px solid #9F9F9F;
            }
            thead.header{
                border-bottom: 1px solid #9F9F9F;
            }
            #hor-minimalist-a {
                background: #fff;
                margin: 45px;
                border-collapse: collapse;
                text-align: left;
            }

            #hor-minimalist-a th {
                font-size: 10px;
                font-weight: normal;
                color: #039;
                padding: 10px 8px;
                border-bottom: 2px solid #6678b1;
            }

            #hor-minimalist-a td {
                color: #669;
                padding: 9px 8px 0px 8px;
                min-width: 100px;
            }

            #hor-minimalist-a .foot td {
                padding-top: 15px;
                font-weight: bold;
            }

            #header {
                margin-top: 5px;
                margin-bottom: 15px;
                position: relative;
            }

            .missatge_error {
                position: relative;
                top: 15px;
                font-size: 18px;
                font-weight: bold;
            }

            #header .company {
                font-size: 12px;
                text-transform: uppercase;
                font-weight: bold;
            }
            .total-column {
                background-color: #dddddd;
                font-weight: bold;
            }
            #header .text_capcalera {
                margin-top: 20px;
            }

            #header .text_vigencia {
                margin-top: 5px;
                font-weight: bold;
            }
            #logo img {
                margin-top: 10px;
                height: 60px;
            }

        </style>
        <%block name="custom_css" />

    </head>
<%def name="get_fields_to_sum_fact()">
    <%
        fields_to_sum_fact = [
             'lp_euros',
             'le_euros',
             'lr_euros',
             'excesos_euros',
             'otros',
             'lloguers_euros',
             'ie_amount',
             'amount_untaxed',
             'amount_tax',
             'amount_total'
        ]
        return fields_to_sum_fact
    %>
</%def>
<%def name="get_facturacion_cols()">
    <th>${_(u"Tarifa")}</th>
    <th>${_(u"Clients")}</th>
    <th>${_(u"Pot. Fact")}</th>
    <th class="split">${_(u"E")}</th>
    <th>${_(u"P1")}</th>
    <th>${_(u"P2")}</th>
    <th>${_(u"P3")}</th>
    <th>${_(u"P4")}</th>
    <th>${_(u"P5")}</th>
    <th>${_(u"P6")}</th>
    <th>${_(u"Total")}</th>
    <th class="split">${_(u"Potencia")}</th>
    <th>${_(u"Activa")}</th>
    <th>${_(u"Reactiva")}</th>
    <th>${_(u"Exces.")}</th>
    <th>${_(u"Altres")}</th>
    <th>${_(u"Lloguers")}</th>
    <th>${_(u"IE")}</th>
    <th>${_(u"Base imp.")}</th>
    <th>${_(u"IVA")}</th>
    <th>${_(u"Total")}</th>
</%def>

    <%
        from addons import get_module_resource
        from ast import literal_eval
        import pandas as pd
        from osv.osv import except_osv
        from babel.numbers import format_currency, format_number,format_decimal

        fields_to_sum_activa = [
             'p1_consumo',
             'p2_consumo',
             'p3_consumo',
             'p4_consumo',
             'p5_consumo',
             'p6_consumo'
        ]

        fields_to_sum_fact = self.get_fields_to_sum_fact()

        fields_to_sum_reactiva = [
             'p1_consumor',
             'p2_consumor',
             'p3_consumor',
             'p4_consumor',
             'p5_consumor',
             'p6_consumor'
        ]

        def get_results():
            sql_file_path = get_module_resource(
                'giscedata_facturacio_comer', 'sql',
                'resumen_facturacion_comer_por_tarifas.sql'
            )
            query = open(sql_file_path, 'r').read()
            if len(data['form']['states'].split(',')) == 1:
            	query_states = tuple(data['form']['states'].split(','))
            else:
            	query_states = eval(data['form']['states'])

            fact = literal_eval(data['form']['facturacio'])
            if isinstance(fact, int):
                query_fact = list()
                query_fact.append(fact)
            else:
                query_fact = fact
            fact = tuple(query_fact)

            cursor.execute(query,
                (
                    data['form']['data_inici'],data['form']['data_final'],
                    fact,
                    query_states,
                    tuple(data['form']['multi_serie'])
                )
            )
            df = pd.DataFrame(cursor.dictfetchall())
            df = df.fillna(0.0)
            return df
    %>

    <% df = get_results() %>
	<body>
        <div id="header">
            <div style="float:left; width: 25%;">
                <div id="logo">
                    <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                </div>
            </div>
            <div style="float:left; width: 45%; margin-top: 5px;">
                <div class="company">
                    ${company.name}
                    <p>${_(u'Peatges per tarifes detallat (comercialitzadora)')}</p>
                </div>
                <div>
                    <table class="hor-minimalist-a">
                        <tr>
                            <td class="text_vigencia">${_(u"Dades")}</td>
                            <td>${_(u"Data inici:")} ${formatLang(data['form']['data_inici'], date=True)}</td>
                            <td>${_(u"Data final")} ${formatLang(data['form']['data_final'], date=True)}</td>
                        </tr>
                    </table>
                </div>
            </div>
            % if 'nom_serie' in df:
            <div style="float:right; width: 25%; ">
                <div class="text_capcalera">
                    <span class="text_vigencia">${_(u"Seqüències")}</span>
                    <table class="hor-minimalist-a">
                        <tbody>
                            % for name, group in df.groupby('nom_serie'):
                                <tr>
                                    <td colspan="1">${name}</td>
                                </tr>
                            %endfor
                        </tbody>
                    </table>
                </div>
            </div>
            %endif
        </div>
        % if df.empty:
        <div class="missatge_error">
            <table>
                <tr>
                    <td>
                        <p><b>${_(u"No s'ha trobat cap factura amb els paràmetres de cerca.")}</b></p>
                    </td>
                </tr>
            </table>
        </div>
        %else:
        <div class="titol">
            <table>
                <thead class="header">
                    <tr>
                        <th colspan="3"></th>
                        <th colspan="8" class="split">${_(u"Consums")}</th>
                        <th colspan="10" class="split">${_(u"Facturació")}</th>
                    </tr>
                    <tr>
                        ${self.get_facturacion_cols()}
                    </tr>
                </thead>
                % for name, group in df.groupby('tarifa'):
                <tr class="activa">
                    <td>${name}</td>
                    <td>${format_number(group['number'].count(), locale='es_ES')}</td>
                    <td>${format_decimal(group['lp_kw'].sum(), locale='es_ES')}</td>
                    <td class="split" style="padding-left:5px;">A</td>
                    % for field in fields_to_sum_activa:
                        <td>${format_decimal(group[field].sum(), locale='es_ES')}</td>
                    % endfor
                    <td>
                        ${format_decimal(
                           sum(((group[fields_to_sum_activa]).sum(axis=1)))
                          , locale='es_ES')}
                    </td>
                    % for index,field in enumerate(fields_to_sum_fact):
                        %if field == "amount_total":
                            <% total = 'class="total-column"' %>
                        %else:
                            <% total = '' %>
                        %endif
                        %if index == 0:
                           <td class="split" ${total}>
                        %else:
                            <td ${total}>
                        %endif
                        ${format_currency(
                            group[field].sum(),'EUR', locale='es_ES'
                        )}
                        </td>
                    % endfor
                </tr>
                <tr class="reactiva">
                    <td colspan="3"></td>
                    <td class="split" style="padding-left:5px;">R</td>
                    % for field in fields_to_sum_reactiva:
                        <td>${format_decimal(group[field].sum(), locale='es_ES')}</td>
                    % endfor
                    <td>
                        ${format_decimal(
                           sum(((group[fields_to_sum_reactiva]).sum(axis=1)))
                          , locale='es_ES')}
                    </td>
                    <td class="split" colspan="10"></td>
                </tr>
                % endfor
                <tr class="activa foot">
                  <td><strong>TOTAL</strong></td>
                    <td>${format_number(df['number'].count(), locale='es_ES')}</td>
                    <td>${format_decimal(df['lp_kw'].sum(), locale='es_ES')}</td>
                    <td class="split">A</td>
                    % for field in fields_to_sum_activa:
                        <td>${format_decimal(df[field].sum(), locale='es_ES')}</td>
                    % endfor
                    <td>
                        ${format_decimal(
                           sum(((df[fields_to_sum_activa]).sum(axis=1)))
                          , locale='es_ES')}
                    </td>
                    % for index, field in enumerate(fields_to_sum_fact):
                        %if index == 0:
                           <td class="split">
                        %else:
                            <td>
                        %endif
                        ${format_currency(
                            df[field].sum(),'EUR', locale='es_ES'
                        )}</td>
                    % endfor
                </tr>
                <tr class="reactiva foot">
                    <td colspan="3"></td>
                    <td class="split">R</td>
                    % for field in fields_to_sum_reactiva:
                       <td>
                        ${format_decimal(df[field].sum(), locale='es_ES')}</td>
                    % endfor
                    <td>
                        ${format_decimal(sum(((df[fields_to_sum_reactiva]).sum(axis=1))), locale='es_ES')}
                    </td>
                    <td class="split" colspan="10"></td>
                </tr>
            </table>
        </div>
        %endif
	</body>
</html>
