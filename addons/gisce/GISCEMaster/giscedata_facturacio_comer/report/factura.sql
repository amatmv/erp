SELECT
     giscedata_cups_ps.name AS giscedata_cups_ps_name,
     coalesce(giscedata_cups_ps.nv, '') AS giscedata_cups_ps_nv,
     coalesce(giscedata_cups_ps.pnp, '') AS giscedata_cups_ps_pnp,
     coalesce(giscedata_cups_ps.es, '') AS giscedata_cups_ps_es,
     coalesce(giscedata_cups_ps.pt, '') AS giscedata_cups_ps_pt,
     coalesce(giscedata_cups_ps.pu, '') AS giscedata_cups_ps_pu,
     coalesce(giscedata_cups_ps.cpo, '') AS giscedata_cups_ps_cpo,
     coalesce(giscedata_cups_ps.cpa, '') AS giscedata_cups_ps_cpa,
     res_municipi_cups.name AS res_municipi_cups_name,
     account_invoice.number AS account_invoice_number,
     account_invoice.id AS account_invoice_id,
     account_invoice.date_invoice AS account_invoice_date_invoice,
     giscedata_facturacio_factura.data_inici AS giscedata_facturacio_factura_data_inici,
     giscedata_facturacio_factura.data_final AS giscedata_facturacio_factura_data_final,
     giscedata_facturacio_factura.potencia AS giscedata_facturacio_factura_potencia,
     account_invoice.amount_total AS account_invoice_amount_total,
     giscedata_facturacio_factura.id AS giscedata_facturacio_factura_id,
     product_pricelist.name AS product_pricelist_name,
     giscedata_polissa.name AS giscedata_polissa_name,
     res_partner.name AS res_partner_name,
     res_partner.vat AS res_partner_vat,
     res_partner_bank.acc_number AS res_partner_bank_acc_number,
     account_invoice.date_due AS account_invoice_date_due,
     invoice_address.name AS invoice_address_name,
     invoice_address.street AS invoice_address_street,
     invoice_address.street2 AS invoice_address_street2,
     invoice_address.city AS invoice_address_city,
     invoice_address.zip AS invoice_address_zip,
     invoice_state.name AS invoice_state_name,
     invoice_country.name AS invoice_country_name

FROM
	 giscedata_facturacio_factura giscedata_facturacio_factura
	 LEFT JOIN giscedata_cups_ps giscedata_cups_ps ON  giscedata_cups_ps.id = giscedata_facturacio_factura.cups_id
	 LEFT JOIN res_municipi res_municipi_cups ON giscedata_cups_ps.id_municipi = res_municipi_cups.id
     LEFT JOIN account_invoice account_invoice ON giscedata_facturacio_factura.invoice_id = account_invoice.id
     LEFT JOIN product_pricelist product_pricelist ON giscedata_facturacio_factura.llista_preu = product_pricelist.id
     LEFT JOIN giscedata_polissa giscedata_polissa ON giscedata_facturacio_factura.polissa_id = giscedata_polissa.id
     LEFT JOIN res_partner res_partner ON giscedata_polissa.titular = res_partner.id
     LEFT JOIN res_partner_bank ON account_invoice.partner_bank = res_partner_bank.id
     LEFT JOIN res_partner_address invoice_address ON account_invoice.address_invoice_id = invoice_address.id
     LEFT JOIN res_country_state invoice_state ON invoice_address.state_id = invoice_state.id
     LEFT JOIN res_country invoice_country ON invoice_address.country_id = invoice_country.id
