# -*- coding: utf-8 -*-

import jasper_reports


def informes(cr, uid, ids, data, context):

    return {
        'parameters': {'data_inici': data['form']['data_inici'],
                       'data_final': data['form']['data_final'],
                       'serie': data['form']['serie'],
                       'states': data['form']['states'],
                       'facturacio': data['form']['facturacio'], },
    }

jasper_reports.report_jasper(
   'report.report_resumen_facturacion_comer_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.report_resumen_facturacion_comer_grupcobratori_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.report_resumen_facturacion_comer_distribuidora_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.report_resumen_facturacion_comer_municipio_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.report_resumen_facturacion_comer_municipio_peaje_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.report_resumen_facturacion_comer_cne_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.resumen_facturacion_comer_impuestos_distri_fecha',
   'wizard.informes.facturacio.comer',
   parser=informes)

jasper_reports.report_jasper(
   'report.detalle_facturacion_comer_otros_distri_fechas',
   'wizard.informes.facturacio.comer',
   parser=informes)

