# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioValidationValidator(osv.osv):
    _inherit = 'giscedata.facturacio.validation.validator'
    _name = 'giscedata.facturacio.validation.validator'

    def check_iese(self, cursor, uid, fact, parameters):
        fact_obj = self.pool.get('giscedata.facturacio.facturador')
        fact_rm_iese_ids = fact_obj.get_invoices_to_remove_iese(
            cursor, uid, fact.id
        )
        if fact_rm_iese_ids:
            return {
                'num_factura': fact.number
            }
        return None

    def check_invoice_has_partner_bank(self, cursor, uid, fact, parameters):
        if fact.polissa_id.payment_mode_id and fact.polissa_id.payment_mode_id.require_bank_account and not fact.partner_bank:
            return {'error':''}
        return None

GiscedataFacturacioValidationValidator()
