# -*- coding: utf-8 -*-
import logging


def up(cursor, installed_version):
    """Assignem per defecte la uom 'alq' a tots els comptadors.
    """
    logger = logging.getLogger('migration')

    search = """
    SELECT id FROM ir_values
    WHERE NAME = 'Factura'
      AND key2 ILIKE 'client_print_multi'
      AND model = 'giscedata.facturacio.factura'
    """
    cursor.execute(search)
    del_id = cursor.fetchall()
    if not del_id:
        logger.info(
            'IR.VALUES for FACTURA JASPER'
            ' has already been deleted or has not been found')
        return
    delete = """
    DELETE FROM ir_values WHERE id=(
      SELECT id FROM ir_values
      WHERE NAME = 'Factura'
        AND key2 ILIKE 'client_print_multi'
        AND model = 'giscedata.facturacio.factura'
    )
    """
    cursor.execute(delete)
    logger.info('IR.VALUES for FACTURA JASPER has been DELETED')


def down():
    pass


migrate = up
