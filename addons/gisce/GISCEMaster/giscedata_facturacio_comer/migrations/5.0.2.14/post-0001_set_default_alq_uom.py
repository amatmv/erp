# -*- coding: utf-8 -*-
import pooler

def migrate(cursor, installed_version):
    """Assignem per defecte la uom 'alq' a tots els comptadors.
    """
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    compt_obj = pool.get('giscedata.lectures.comptador')
    cids = compt_obj.search(cursor, uid, [('uom_id', '=', False)],
                            context={'active_test': False})
    if not cids:
        return
    imd_id = imd_obj._get_id(cursor, uid, 'giscedata_lectures', 'uom_alq_elec')
    res = imd_obj.read(cursor, uid, imd_id, ['res_id'])
    compt_obj.write(cursor, uid, cids, {'uom_id': res['res_id']})
