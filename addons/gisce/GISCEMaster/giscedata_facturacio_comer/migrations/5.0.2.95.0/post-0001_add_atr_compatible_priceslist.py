# coding=utf-8
import pooler


def migrate(cursor, installed_version):
    uid = 1
    mode_code = 'atr'

    sql = """
select distinct mode_facturacio, llista_preu from (
    select distinct mode_facturacio, llista_preu from giscedata_polissa where mode_facturacio is not null and llista_preu is not null
    union
    select distinct mode_facturacio, llista_preu from giscedata_polissa_modcontractual where mode_facturacio is not null and llista_preu is not null
) as foo
where mode_facturacio = %s
    """
    cursor.execute(sql, (mode_code, ))

    pl_ids = [x[1] for x in cursor.fetchall()]

    pool = pooler.get_pool(cursor.dbname)
    mf_obj = pool.get('giscedata.polissa.mode.facturacio')
    mode_id = mf_obj.search(cursor, uid, [('code', '=', mode_code)], limit=1)
    for pl_id in pl_ids:
        mf_obj.write(cursor, uid, mode_id, {
            'compatible_price_lists': [(4, pl_id)]
        })


up = migrate
