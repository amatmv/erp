# -*- coding: utf-8 -*-

"""
Models d'informes de preus del ministeri d'indústria, turisme i comerç

"""

from osv import fields, osv

class GiscedataFacturacioInformePreusConsumidors(osv.osv):
    """Model d'informe de preus de consumidors
    """

    def get_data_domestico_anual(self, cursor, uid, ids, context=None):
        return self._get_data_anual(
            cursor, uid, ids, "domestic", context=context
        )

    def get_data_industrial_anual(self, cursor, uid, ids, context=None):
        return self._get_data_anual(
            cursor, uid, ids, "industrial", context=context
        )

    def _get_data_anual(self, cursor, uid, ids, type, context=None):
        """
        :param type: Function name that retrieves the requiered
        anual data from a line.
        :type: str
        :return:
        """

        if not isinstance(ids, list):
            ids = [ids]

        if type == 'industrial':
            query = """
                SELECT franja,
                    sum("energia_A" * 1000) as energia,
                    sum("fact_total_CF") / sum("energia_A" * 1000) as energia_total_sin_impuestos,
                    sum("fact_tarifa_F") / sum("energia_A" * 1000) as energiaysuministro,
                    sum("fact_equips" + "fact_tarifa_C") / sum("energia_A" * 1000) as tarifa_acceso,
                    sum("fact_equips") / sum("energia_A" * 1000) as alquiler_equipos,
                    sum("fact_total_CF" - "fact_total_BE") / sum("energia_A" * 1000) as impuestos_no_recuperables
                FROM giscedata_facturacio_informe_preus_consumidors_linia
                WHERE consumidor = 'industrial' AND informe_id in %s
                GROUP BY franja
            """
        else:
            query = """
                SELECT franja,
                    sum("energia_A"*1000) as energia,
                    sum("fact_total_DG")/sum("energia_A"*1000) as energia_total_con_impuestos,
                    sum("fact_tarifa_G")/sum("energia_A"*1000) as energiaysuministro,
                    sum("fact_equips" + "fact_tarifa_D")/sum("energia_A"*1000) as tarifa_acceso,
                    sum("fact_equips")/sum("energia_A"*1000) as alquiler_equipos,
                    sum("fact_total_DG"-"fact_total_BE")/sum("energia_A"*1000) as impuestos_e_iva
                FROM giscedata_facturacio_informe_preus_consumidors_linia
                WHERE consumidor = 'domestic' AND informe_id in %s
                GROUP BY franja
            """

        cursor.execute(query, (tuple(ids),))
        data = cursor.dictfetchall()
        return data

    _name = 'giscedata.facturacio.informe_preus_consumidors'
    _description = __doc__

    _columns = { 
       'name': fields.char('Data càlcul', size=32, required=True),
       'linies': fields.one2many('giscedata.facturacio.'
                                 'informe_preus_consumidors.linia',
                                 'informe_id', 'Línies')
       }   

GiscedataFacturacioInformePreusConsumidors()

class GiscedataFacturacioInformePreusConsumidorsLinia(osv.osv):
    """Model de lína d'informe de preus de comercialitzadora per consumidors 
       industrials i domèstics
    """

    _name = 'giscedata.facturacio.informe_preus_consumidors.linia'
    _columns = {
        'num_clients': fields.integer('Número clientes'),
        'energia_A': fields.float('Energía (MWh)', digits=(16,3)),
        'potencia': fields.float('Potencia (MW)', digits=(16,3),
                                 help='Potencia facturada (MWh)'),
        'fact_tarifa_B': fields.float('Facturación B (€)', digits=(16,4),
                                      help='Fact. tarifa de acceso sin \
                                      impuestos'),
        'fact_tarifa_C': fields.float('Facturación C (€)', digits=(16,4),
                                      help='Fact. tarifa de acceso sin IVA ni \
                                      otros impuestos recuperables'),
        'fact_tarifa_D': fields.float('Facturación D (€)', digits=(16,4),
                                      help='Fact. tarifa de acceso con todos \
                                      impuestos e IVA'),
        'fact_tarifa_E': fields.float('Facturación E (€)', digits=(16,4),
                                      help='Fact. energía a mercado libre sin \
                                      impuestos'),
        'fact_tarifa_F': fields.float('Facturación F (€)', digits=(16,4),
                                      help='Fact. energía a mercado libre sin \
                                      IVA ni otros impuestos recuperables'),
        'fact_tarifa_G': fields.float('Facturación G (€)', digits=(16,4),
                                      help='Fact. energía a mercado libre con \
                                      todos los impuestos e IVA'),
        'fact_total_BE': fields.float('Total B+E (€)', digits=(16,4),
                                      help='Fact. total sin impuestos'),
        'fact_total_CF': fields.float('Total C+F (€)', digits=(16,4),
                                      help='Fact. total sin IVA ni otros \
                                      impuestos recuperables'),
        'fact_total_DG': fields.float('Total D+G (€)', digits=(16,4),
                                      help='Fact. total con todos los \
                                      impuestos e IVA'),
        'fact_equips': fields.float('Facturación equipos (€)', digits=(16,4),
                                     help='Fact. alquiler equipos de medida' 
                                     'y control'),
        'v_max': fields.float('Tensión máxima (kV)', digits=(16,3)),
        'v_min': fields.float('Tensión mínima (kV)', digits=(16,3)),
        'preu_BEA': fields.float('Precio (B+E)/A', digits=(16,4),
                                 help='Precio total sin impuestos(€/kWh)'),
        'preu_CFA': fields.float('Precio (C+F)/A (€/kWh)', digits=(16, 4),
                                 help='Precio total sin IVA ni otros impuestos '
                                 'recuperables'),
        'preu_DGA': fields.float('Precio (D+G)/A (€/kWh)', digits=(16, 4),
                                 help='Precio total con todos los impuestos e '
                                 'IVA (€/kWh'),
        'franja': fields.char('Franja', size=9, required=True),
        'consumidor': fields.selection([('industrial', 'industrial'), 
                                        ('domestic', 'domèstic')],
                                        'Consumidor', required=True),
        'informe_id': fields.many2one('giscedata.facturacio.informe_preus'
                                      '_consumidors', 'Informe', 
                                      required=True, ondelete='cascade')
    }

    _defaults = {
           'num_clients': lambda *a: 0,
           'energia_A': lambda *a: 0.0,
           'potencia': lambda *a: 0.0,
           'fact_tarifa_B': lambda *a: 0.0,
           'fact_tarifa_C': lambda *a: 0.0,
           'fact_tarifa_D': lambda *a: 0.0,
           'fact_tarifa_E': lambda *a: 0.0,
           'fact_tarifa_F': lambda *a: 0.0,
           'fact_tarifa_G': lambda *a: 0.0,
           'fact_total_BE': lambda *a: 0.0,
           'fact_total_CF': lambda *a: 0.0,
           'fact_total_DG': lambda *a: 0.0,
           'fact_equips': lambda *a: 0.0,
           'v_max': lambda *a: 0.0,
           'v_min': lambda *a: 0.0,
           'preu_BEA': lambda *a: 0.0,
    }

    _order = "consumidor, franja"

GiscedataFacturacioInformePreusConsumidorsLinia()
