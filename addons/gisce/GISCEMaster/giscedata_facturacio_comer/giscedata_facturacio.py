# -*- coding: utf-8 -*-
"""Modificacions del model giscedata_facturacio per la comercialitzadora.
"""
# pylint: disable-msg=E1101,W0223
import re
import time

import StringIO
import base64

from osv import osv, fields
from libfacturacioatr.tarifes import calc_cosfi, aggr_consums
from tools.translate import _
from tools import config, float_round
from tools.misc import cache, timeit
from gestionatr.input.messages.F1 import CODIS_AUTOCONSUM

from base_extended.excel import Sheet


class GiscedataFacturacioFacturador(osv.osv):
    """Metaclasse per generar les factures (no té taula SQL).
    """
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def _get_atrprice_subtotal(self, cursor, uid, vals,
                               linia, context=None):
        '''returns atr price depending on product line'''

        model_obj = self.pool.get('ir.model.data')
        pricelist_obj = self.pool.get('product.pricelist')
        tarifa_elec = model_obj._get_obj(cursor, uid, 'giscedata_facturacio',
                                         'pricelist_tarifas_electricidad')
        date = (linia.data_desde or
                linia.factura_id.date_invoice or
                time.strftime('%Y-%m-%d'))
        #Do not compute atr price in altres
        if linia.tipus in ('altres', 'generacio'):
            return 0
        #lloguer lines already have atr price
        elif linia.tipus == 'lloguer':
            return linia.price_subtotal
        #If reactiva, we have to check cosfi price
        elif linia.tipus == 'reactiva':
            product_id = model_obj._get_obj(cursor, uid,
                                         'giscedata_facturacio',
                                         'product_cosfi').id
            quantity = linia.cosfi * 100
        else:
            if isinstance(vals['product_id'], (list, tuple)):
                product_id = vals['product_id'][0]
            else:
                product_id = vals['product_id']
            #Cas energia negativa (FENOSA)
            # Busquem el preu ATR com si fos positiu
            quantity = abs(vals['quantity'])

        if not product_id:
            return 0
        elif "force_price" in vals and vals['force_price'] == 0.0 and linia.tipus == u'subtotal_xml':
            return 0
        ctx = context.copy()
        ctx.update({'date': date, 'uom': linia.uos_id.id})
        atrprice = pricelist_obj.price_get(cursor, uid,
                         [tarifa_elec.id],
                         product_id, quantity,
                         context=ctx)[tarifa_elec.id]
        if atrprice:
            return atrprice * vals['quantity'] * vals['multi']
        return res

    def crear_linia(self, cursor, uid, factura_id, vals, context=None):

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        product_obj = self.pool.get('product.product')

        factura = factura_obj.browse(cursor, uid, factura_id)
        orig_vals = vals.copy()
        #Check if we have to group product
        if (vals['product_id'] and
            factura.llista_preu and
            factura.llista_preu.agrupacio):
            new_product_id = (factura.llista_preu.
                              get_product_group(vals['product_id']))
            if new_product_id:
                vals['product_id'] = new_product_id
                vals['name'] = product_obj.read(cursor, uid,
                                                new_product_id,
                                                ['name'])['name']
        linia_id = super(GiscedataFacturacioFacturador,
                     self).crear_linia(cursor, uid, factura_id,
                                       vals, context=context)
        #Compute atrprice_subtotal for the new line
        #We use original product and original quantity
        #before grouping
        linia = linia_obj.browse(cursor, uid, linia_id)
        atrprice = self._get_atrprice_subtotal(cursor, uid,
                                               orig_vals,
                                               linia,
                                               context=context)
        linia.write({'atrprice_subtotal': atrprice + linia.atrprice_subtotal})
        return linia_id

    def crear_lectures_energia(self, cursor, uid, factura_id, vals,
                               context=None):
        """Agrupacio de les lectures d'energia segons els periodes
        """
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura = factura_obj.browse(cursor, uid, factura_id)
        ids = []
        new_vals = vals.copy()
        if factura.llista_preu and factura.llista_preu.agrupacio:
            for periode, lectura in vals.iteritems():
                if (not lectura.get('anterior', {})
                    or not lectura.get('actual', {})):
                    continue
                per_name = lectura['actual']['periode'][1]
                newper = (factura.llista_preu.
                          get_periode_group(lectura['actual']['periode'][1]))
                if not newper:
                    continue
                #We have to group this period
                actual_new = new_vals[newper[0]]['actual']
                actual_old = new_vals[periode]['actual']
                actual_new.update({'lectura': (actual_new.get('lectura', 0)
                                          + actual_old.get('lectura', 0)),
                                   'consum': (actual_new.get('consum', 0)
                                           + actual_old.get('consum', 0)),
                                   'data_actual': max(actual_new['name'],
                                                      actual_old['name']),
                                   'data_anterior': min(actual_new['name'],
                                                    actual_old['name'])})
                ant_new = new_vals[newper[0]]['anterior']
                ant_old = new_vals[periode]['anterior']
                ant_new.update({'lectura': (ant_new.get('lectura', 0) +
                                            ant_old.get('lectura', 0)),
                                'consum': (ant_new.get('consum', 0) +
                                           ant_old.get('consum', 0)),
                                'data_actual': max(ant_new['name'],
                                                   ant_old['name']),
                                'data_anterior': min(ant_new['name'],
                                                     ant_old['name'])})
                del new_vals[periode]

        return super(GiscedataFacturacioFacturador,
                     self).crear_lectures_energia(cursor, uid, factura_id,
                                                  new_vals, context=context)

    def get_invoices_to_remove_iese(self, cursor, uid, fact_ids, context=None):
        '''
        Donat un seguit d'ids de factures en retorna les que sel's hi ha
        d'eliminar l'impost IESE segons les següents condicions:
            - energia_kwh 0 or False
            and
            - te lectures reals
            and
            - no te lectures estimades

        :param fact_ids: int or list of giscedata_facturacio_factura id
        :return: list of ids where need to remove IESE tax
        '''

        if context is None:
            context = {}

        if not fact_ids:
            return []

        fact_ids_to_remove_iese = []

        if not isinstance(fact_ids, (list, tuple)):
            fact_ids = [fact_ids]

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        # First filter (check for none energia_kwh)
        search_params = [
            ('id', 'in', fact_ids),
            '|',
            ('energia_kwh', '=', 0),
            ('energia_kwh', '=', False),
        ]

        fact_to_check = fact_obj.search(
            cursor, uid, search_params, context=context
        )

        ctx = context.copy()
        ctx['avoid_cast'] = True
        for fact in fact_obj.browse(cursor, uid, fact_to_check, context=context):
            # El camp energia_kwh que es fa servir per saber si tenen consum 0 es un enter, llavors les factures que
            # tinguiun consum 0.X tambe les haurem trobat pero a aquestes no sels hi ha de treure el iese.
            # Per aixo ho tornem a calcular pero passant per context que no volem que ens transformi el resultat a enter
            energia_kw = fact._ff_total_tipus(None, None, context=ctx).get(fact.id, {}).get('energia_kwh', 0.0)
            if energia_kw:
                continue
            if fact.lectures_energia_ids:
                lectures_reals = True
                for lectura_e in fact.lectures_energia_ids:
                    type_name = lectura_e.origen_id.name.lower()
                    if 'estimada' in type_name or 'sense lectura' in type_name:
                        lectures_reals = False
                        break
                has_iese = False
                for line in fact.linia_ids:
                    for tax in line.invoice_line_tax_id:
                        if 'especial' in tax.name.lower():
                            has_iese = True
                if lectures_reals and has_iese:
                    fact_ids_to_remove_iese.append(fact.id)

        return fact_ids_to_remove_iese

    def remove_iese_tax(self, cursor, uid, fact_ids, context=None):
        if context is None:
            context = {}

        if fact_ids:
            if not isinstance(fact_ids, (list, tuple)):
                fact_ids = [fact_ids]
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            for fact in fact_obj.browse(cursor, uid, fact_ids, context=context):
                reset_taxes = False
                for line in fact.linia_ids:
                    for tax in line.invoice_line_tax_id:
                        if 'especial' in tax.name.lower():
                            reset_taxes = True
                            line.write({'invoice_line_tax_id': [(3, tax.id)]})
                if reset_taxes:
                    fact.button_reset_taxes()

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        if context is None:
            context = {}
        fact_ids = super(GiscedataFacturacioFacturador, self).fact_via_lectures(
            cursor, uid, polissa_id, lot_id, context=context
        )

        facts_to_remove_iese = self.get_invoices_to_remove_iese(
            cursor, uid, fact_ids, context=context
        )

        if facts_to_remove_iese:
            self.remove_iese_tax(
                cursor, uid, facts_to_remove_iese, context=context
            )

        return fact_ids

    def fact_via_lectures_pre_taxes_post_hook(self, cursor, uid, factura_id, context=None):
        res = super(GiscedataFacturacioFacturador, self).fact_via_lectures_pre_taxes_post_hook(cursor, uid, factura_id, context=context)
        self.ajustar_saldo_excedents_autoconsum(cursor, uid, factura_id, context=context)
        return res

    def ajustar_saldo_excedents_autoconsum(self, cursor, uid, ids, context=None):
        """
        Crea una linia de tipus "generacio" per tal que el total € de les linies "generacio" no superi el total € de
        les linies "energia"
        """
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        factura_o = self.pool.get("giscedata.facturacio.factura")
        flinia_o = self.pool.get("giscedata.facturacio.factura.linia")
        conf_o = self.pool.get("res.config")
        preus_sense_peatges = int(conf_o.get(cursor, 1, "saldo_excedents_autoconsum_sense_peatges", "0"))

        for factura_id in ids:
            if not factura_o.te_autoconsum(cursor, uid, factura_id, context=context):
                continue

            l_activa = flinia_o.search(cursor, uid, [('tipus', '=', 'energia'), ('factura_id', '=', factura_id)])
            saldo_amb_excedents = 0.0
            for info in flinia_o.read(cursor, uid, l_activa, ['price_subtotal']):
                saldo_amb_excedents += info['price_subtotal']
            if preus_sense_peatges:
                saldo_amb_excedents -= factura_o.read(cursor, uid, factura_id, ['total_energia_atr'])['total_energia_atr']

            l_generacio = flinia_o.search(cursor, uid, [('tipus', '=', 'generacio'), ('factura_id', '=', factura_id)])
            for info in flinia_o.read(cursor, uid, l_generacio, ['price_subtotal']):
                saldo_amb_excedents += info['price_subtotal']

            if saldo_amb_excedents < 0:
                imd_o = self.pool.get("ir.model.data")
                product_id = imd_o.get_object_reference(
                    cursor, uid, "giscedata_facturacio_comer", 'saldo_excedents_autoconsum'
                )[1]
                vals = {
                    'product_id': product_id,
                    'force_price': saldo_amb_excedents * -1,
                    'quantity': 1,
                    'tipus': 'generacio',
                    'name': _(u"Saldo excedents d'autoconsum")
                }
                ctx = context.copy()
                ctx['pricelist_base_price'] = 0
                self.crear_linia(cursor, uid, factura_id, vals, context=ctx)

        return True

    def fact_via_proveidor(self, cursor, uid, polissa_id, lot_id,
                           context=None):
        """Facturem la pòlissa a través de les factures de proveïdor.
        """
        factures_creades = []
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]
        if not context:
            context = {}
        # Pools
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        product_obj = self.pool.get('product.product')
        # Busquem la factura d'aquesta pòlissa
        lot = lot_obj.browse(cursor, uid, lot_id)
        search_params = [
                ('polissa_id.id', '=', polissa_id),
                ('data_final', '>', lot.data_inici),
                ('data_final', '<=', lot.data_final),
                ('type', '=', 'in_invoice'),
                ('state', '=', 'open')
        ]
        fids = factura_obj.search(cursor, uid, search_params,
                                  context={'active_test': False})
        for factura_p in factura_obj.browse(cursor, uid, fids):
            defaults = {'date_invoice': factura_p.date_invoice,
                        'type': 'out_invoice',
                        'number': False,
                        'lot_facturacio': lot_id,
                        'tax_line': []}
            # Cridem l'onchange_polissa per tal que ens empleni tots els camps
            vals = factura_obj.onchange_polissa(cursor, uid, [],
                factura_p.polissa_id.id, 'out_invoice',
                context={'date': factura_p.data_final})
            defaults.update(vals['value'])
            factura_c_id = factura_obj.copy(cursor, uid, factura_p.id,
                                            default=defaults)
            factura_c = factura_obj.browse(cursor, uid, factura_c_id)
            consums = {'activa': {},
                       'reactiva': {}}
            for lectura in factura_c.lectures_energia_ids:
                periode = re.findall('P[1-6]', lectura.name)[0]
                consums[lectura.tipus][periode] = lectura.consum
            consums['activa'] = aggr_consums(consums['activa'])
            consums['reactiva'] = aggr_consums(consums['reactiva'])

            for linia in factura_c.linia_ids:
                # Només recalculem les que tinguin un producte asssociat
                if linia.tipus not in ('energia', 'potencia', 'reactiva',
                                       'exces_potencia', 'lloguer', 'altres'):
                    continue
                only_tax = False
                if not linia.product_id:
                    p_search_params = [('default_code', '=', 'ALQ01')]
                    def_p = product_obj.search(cursor, uid, p_search_params)[0]
                    linia.product_id = product_obj.browse(cursor, uid, def_p)
                    only_tax = True
                vals = linia_obj.product_id_change(cursor, uid, [],
                                                   factura_c.llista_preu.id,
                                                   linia.data_fins,
                                                   linia.product_id.id,
                                                   linia.product_id.uom_id.id,
                                                   polissa_id, linia.quantity,
                                                   linia.name, 'out_invoice',
                                                   factura_c.partner_id.id)
                vals = vals['value']
                if only_tax:
                    l_vals = {}
                    l_vals['account_id'] = vals['account_id']
                else:
                    l_vals = vals.copy()
                l_vals['invoice_line_tax_id'] = [(6, 0,
                                        vals.get('invoice_line_tax_id', []))]
                if linia.tipus == 'reactiva':
                    # Multipliquem cosfi * 100 perquè a les llistes de preus
                    # la quantitat mínima ha de ser un enter i ho tenim entrat
                    # multiplicat per 100
                    if not linia.cosfi:
                        con_activa = consums['activa'][linia.name]
                        con_reactiva = consums['reactiva'][linia.name]
                        linia.cosfi = calc_cosfi(con_activa, con_reactiva)
                        l_vals['cosfi'] = linia.cosfi
                    cosfi_cent = linia.cosfi * 100
                    price = self.calc_cosfi_price(cursor, uid, cosfi_cent,
                                                  factura_c.llista_preu.id,
                                    context={'date': linia.data_fins})
                    l_vals['price_unit_multi'] = price
                linia.write(l_vals)
            factura_c.button_reset_taxes()
            factures_creades.append(factura_c_id)
        # Finalment returnem les factures creades
        return factures_creades

    def crear_linies_lloguer(self, cursor, uid, factura_id, vals,
                             context=None):
        """Creem les línies de lloguer per la factura segons els termes.
        """
        if not context:
            context = {}
        product_obj = self.pool.get('product.product')
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        # El preu ens el passaran com a product_id
        comptador_id = vals.get('product_id', False)
        if comptador_id:
            comptador = compt_obj.browse(cursor, uid, comptador_id)
            vals['name'] = comptador.descripcio_lloguer
            vals['uos_id'] = comptador.uom_id.id
            price = comptador.preu_lloguer
            # Fem forçar el preu del lloguer a X
            vals['force_price'] = price
        product_id = product_obj.search(cursor, uid,
                                        [('default_code', '=', 'ALQ01')])[0]
        vals['product_id'] = product_id
        ctx = context.copy()
        ctx.update({'lloguer_comer': True})
        linia_id = self.crear_linia(cursor, uid, factura_id, vals, ctx)
        # Eliminem el producte
        linia_obj.write(cursor, uid, [linia_id], {'product_id': False})
        return linia_id

    def fact_lloguer(self, facturador, comptador, data_inici_periode_f,
                     data_final_periode_f, context=None):
        """Facturem el lloguer.
        """
        lloguer_name = comptador.descripcio_lloguer
        facturador.conf['lloguers'] = [lloguer_name]
        data_alta_c = max(comptador.data_alta, data_inici_periode_f)
        data_baixa_c = min(comptador.data_baixa or '3000-01-01',
                           data_final_periode_f)
        base = 'mes'
        factor = comptador.uom_id.factor
        if not factor % 365 or not factor % 366:
            base = 'dia'
        facturador.factura_lloguer(data_alta_c, data_baixa_c, base=base)
        return {comptador.descripcio_lloguer: comptador.id}

    def get_productes_autoconsum(self, cursor, uid, tipus="excedent", context=None):
        """
        Tipus esperats: ['autoconsum', 'generacio', 'excedent']
        """
        res = {}
        imd_o = self.pool.get("ir.model.data")
        for codi_producte, tipus_producte in CODIS_AUTOCONSUM.iteritems():
            if tipus_producte == tipus:
               res['P'+codi_producte[1]] = imd_o.get_object_reference(
                   cursor, uid, "giscedata_facturacio_comer", "concepte_{}".format(codi_producte)
               )[1]
        return res

GiscedataFacturacioFacturador()


class GiscedataFacturacioContracteLot(osv.osv):
    """Contracte Lot per comercialitzadora.
    """
    _name = 'giscedata.facturacio.contracte_lot'
    _inherit = 'giscedata.facturacio.contracte_lot'

    def __init__(self, pool, cursor):
        """Init to add new states."""
        super(GiscedataFacturacioContracteLot, self).__init__(pool, cursor)
        for sel in [('proveidor', 'Proveidor')]:
            if sel not in self._columns['tipus_facturacio'].selection:
                self._columns['tipus_facturacio'].selection.append(sel)
            if sel not in self._columns['state'].selection:
                self._columns['state'].selection.append(sel)

    def cnd_proveidor(self, cursor, uid, ids, context=None):
        """Condició si té factures de proveïdor.
        """
        cfg_obj = self.pool.get('res.config')
        permit_provider = int(cfg_obj.get(cursor, uid, 'permit_provider', '0'))
        if not permit_provider:
            return False
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        for clot in self.browse(cursor, uid, ids):
            # Busquem si té factures de proveïdor per aquesta pòlissa en
            # aquest lot.
            search_params = [
                ('polissa_id.id', '=', clot.polissa_id.id),
                ('data_final', '>', clot.lot_id.data_inici),
                ('data_final', '<=', clot.lot_id.data_final),
                ('type', '=', 'in_invoice'),
                ('state', '=', 'open')
            ]
            if not factura_obj.search_count(cursor, uid, search_params,
                                            context={'active_test': False}):
                return False
        return True

    def wkf_lectures(self, cursor, uid, ids, context=None):
        """Si es compleix la condicio per facturar per proveidor
        aquest estat cridara a wkf_proveidor encara que es pugui
        facturar per lectures. Si tenim factura de proveidor, a per ella"""

        for clot_id in ids:
            if self.cnd_proveidor(cursor, uid, [clot_id], context):
                self.wkf_proveidor(cursor, uid, [clot_id], context)
            else:
                super(GiscedataFacturacioContracteLot,
                      self).wkf_lectures(cursor, uid, [clot_id], context)

        return True

    def wkf_proveidor(self, cursor, uid, ids, context=None):
        """Estat proveïdor.
        """
        self.write(cursor, uid, ids, {'state': 'proveidor',
                                      'tipus_facturacio': 'proveidor',
                                      'status': False})
        #Transicio a facturar
        self.wkf_facturar(cursor, uid, ids, context)
        return True

    def cnd_facturat(self, cursor, uid, ids, context=None):
        """Condició per saber si ja té la factura feta (Exportada).
        """
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        for clot in self.browse(cursor, uid, ids):
            # Busquem si ja té una factura creada
            search_params = [
                ('polissa_id.id', '=', clot.polissa_id.id),
                ('lot_facturacio.id', '=', clot.lot_id.id),
                ('type', '=', 'out_invoice')
            ]
            context = {'active_test': False}
            if not factura_obj.search_count(cursor, uid, search_params,
                                            context=context):
                return False

        return True

    def validate_invoice_mandate(self, cursor, uid, id, context=None):

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        config_obj = self.pool.get('res.config')
        fact_check_mandato = int(config_obj.get(cursor, uid,
                                                'fact_check_mandato', '0'))
        if not fact_check_mandato:
            return True
        clot = self.browse(cursor, uid, id)
        search_params = [
            ('polissa_id.id', '=', clot.polissa_id.id),
            ('lot_facturacio.id', '=', clot.lot_id.id),
            ('type', '=', 'out_invoice')
        ]
        context = {'active_test': False}
        factura_ids = factura_obj.search(cursor, uid, search_params,
                                        context=context)
        mandate_missing = False
        for factura in factura_obj.browse(cursor, uid, factura_ids):
            if factura.partner_bank and not factura.mandate_id:
                mandate_missing = True
        if mandate_missing:
            msg = _(u"* Hi han factures domiciliades sense mandato associat")
            clot.write({'status': msg})
            return False
        return True

    def wkf_obert(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        for clot_id in ids:
            if (not context.get('from_lot', False) and
                self.cnd_facturat(cursor, uid, [clot_id], context=context)):
                if self.validate_invoice_mandate(cursor, uid, clot_id):
                    self.wkf_facturat(cursor, uid, [clot_id], context=context)
            else:
                super(GiscedataFacturacioContracteLot,
                      self).wkf_obert(cursor, uid, [clot_id], context=context)

        return True

GiscedataFacturacioContracteLot()


class GiscedataFacturacioFactura(osv.osv):
    """Classe per la factura de comercialitzadora."""
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    enviat_carpeta_selection = [
        ('inbox', 'Entrada'),
        ('drafts', 'Esborranys'),
        ('outbox', 'Sortida'),
        ('trash', 'Paperera'),
        ('followup', 'Amunt'),
        ('sent', 'Enviats'),
    ]

    def _trg_totals(self, cursor, uid, ids, context=None):
        """Funció per especificar els IDs a recalcular
        """
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_ids = []
        for fact in self.read(cursor, uid, ids, ['invoice_id']):
            if fact['invoice_id']:
                inv_ids.append(fact['invoice_id'][0])
        return fact_obj.search(cursor, uid, [('invoice_id', 'in', inv_ids)])

    def _trg_dies(self, cursor, uid, ids, context=None):
        """Funció to specify ids to recalcultae `dies` field
        """
        return ids

    def _ff_total_energia_atr(self, cursor, uid, ids, field, arg, context=None):
        """Funció que retorna el total d'€ facturats en concepte de peatges.
        """
        query = '''
            SELECT
              fl.factura_id,
              sum(coalesce(fl.atrprice_subtotal, 0))
            FROM giscedata_facturacio_factura_linia fl
            INNER JOIN account_invoice_line il
              ON il.id = fl.invoice_line_id
            INNER JOIN account_invoice i
            ON il.invoice_id = i.id
            WHERE fl.tipus = 'energia'
            AND fl.factura_id in %s
            AND i.date_invoice >= %s
            GROUP BY fl.factura_id
        '''
        cursor.execute(query, (tuple(ids), '2013-01-01'))
        return dict([(x[0], round(x[1], 2)) for x in cursor.fetchall()])

    def _ff_total_atr(self, cursor, uid, ids, field, arg, context=None):
        """Funció que retorna el total d'€ facturats en concepte de peatges.
        """
        query = '''
            SELECT
              fl.factura_id,
              sum(coalesce(fl.atrprice_subtotal, 0))
            FROM giscedata_facturacio_factura_linia fl
            INNER JOIN account_invoice_line il
              ON il.id = fl.invoice_line_id
            INNER JOIN account_invoice i
            ON il.invoice_id = i.id
            WHERE fl.tipus in ('potencia', 'energia',
                               'reactiva', 'exces_potencia')
            AND fl.factura_id in %s
            AND i.date_invoice >= %s
            GROUP BY fl.factura_id
        '''
        cursor.execute(query, (tuple(ids), '2013-01-01'))
        return dict([(x[0], round(x[1], 2)) for x in cursor.fetchall()])

    def _ff_total_energia(self, cursor, uid, ids, field, arg, context=None):

        res = {}
        for factura in self.browse(cursor, uid, ids):
            res[factura.id] = 0
            for linia in factura.linies_energia:
                if linia.isdiscount:
                    continue
                res[factura.id] += linia.quantity
            res[factura.id] = int(res[factura.id])
        return res

    @cache(timeout=10)
    def _fnc_per_enviar(self, cursor, uid, ids, field_name, args,
                        context=None):
        """Retorna si una factura està marcada per enviar o no.
        """
        res = dict([(x, 'postal') for x in ids])
        cursor.execute("""
            select
              f.id,
              m.enviament
            from
              giscedata_facturacio_factura f
              left join giscedata_polissa_modcontractual m
                on (m.polissa_id = f.polissa_id)
              left join account_invoice i on (f.invoice_id = i.id)
            where
              f.id in %s
              and i.date_invoice between m.data_inici and m.data_final
        """, (tuple(ids), ))
        res.update(dict([(x[0], x[1]) for x in cursor.fetchall()]))
        return res

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
               context=None, count=False):

        #Always search with like when per_enviar field
        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, match = arg
            if field == 'per_enviar':
                operator = 'like'
            new_args.append((field, operator, match))

        return super(GiscedataFacturacioFactura,
                     self).search(cursor, uid, new_args, offset=offset,
                                  limit=limit, order=order,
                                  context=context, count=count)

    def copy(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        default['per_enviar'] = ''
        return super(GiscedataFacturacioFactura,
                     self).copy(cursor, uid, id, default, context)

    _STORE_TOTALS = {'account.invoice.line': (_trg_totals, ['quantity'], 20)}

    _columns = {
        'payment_mode_id': fields.many2one('payment.mode', 'Grup de pagament',
                                           required=True, readonly=False),
        'total_energia_atr': fields.function(
            _ff_total_energia_atr, type='float',
            method=True, string='Total € energia ATR'
        ),
        'total_atr': fields.function(_ff_total_atr, type='float',
                                     method=True,
                                     string='Total Peatges',
                                     store={
                                         'account.invoice.line':
                                         (_trg_totals, ['quantity'], 0)}),
        'enviat': fields.boolean('Enviada per E-mail', readonly=True),
        'enviat_data': fields.datetime('Data enviament', readonly=True),
        'enviat_carpeta': fields.selection(enviat_carpeta_selection,
                                           'Carpeta', readonly=True),
        'per_enviar': fields.function(
            _fnc_per_enviar,
            method=True,
            type='selection',
            selection=[('email', 'E-mail'), ('postal', 'Correu postal')],
            string='Tipus d\'enviament',
            store=True,
            select=2
        ),
    }

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        """Sobreescrivim el mètode per afegir els camps que hem afegit.

        Fem que retorni payment_mode_id
        """
        res = super(GiscedataFacturacioFactura,
                    self).onchange_polissa(cursor, uid, ids, polissa_id, type_,
                                           context)
        if polissa_id:
            polissa = self.pool.get('giscedata.polissa').browse(cursor, uid,
                                                                polissa_id,
                                                                context)
            res['value'].update({
                'payment_mode_id': polissa.payment_mode_id.id
            })
        return res

    def __init__(self, pool, cursor):
        """Canviem el nom de Pòlissa -> Contracte.
        De moment ho canviem des de l'init per si es volen tocar altres
        propietats només s'hauran de tocar al base.
        """
        super(GiscedataFacturacioFactura, self).__init__(pool, cursor)
        self._columns['polissa_id'].string = 'Contracte'

    _defaults = {
        'enviat': lambda *a: 0,
        'enviat_data': lambda *a: False
    }

    def _cnt_llista_preu_compatible(self, cursor, uid, ids):
        """Comprovem que la llista de preu és compatible amb la tarifa.
        """
        for factura in self.browse(cursor, uid, ids):
            if not factura.llista_preu or not factura.tarifa_acces_id:
                continue
            if (factura.llista_preu not in
                factura.tarifa_acces_id.llistes_preus_comptatibles):
                return False
        return True

    # Poweremails hooks
    def poweremail_create_callback(self, cursor, uid, ids, vals,
                                   context=None):
        """Hook que cridarà el poweremail quan es creei un email
        a partir d'una factura.
        """
        folder = vals.get('folder', False)
        self.write(cursor, uid, ids, {'enviat': 1, 'enviat_carpeta': folder})
        return True

    def poweremail_unlink_callback(self, cursor, uid, ids, context=None):
        """Hook que cridarà el poweremail quan s'elimini un email.
        """
        self.write(cursor, uid, ids, {'enviat': 0})
        return True

    def poweremail_write_callback(self, cursor, uid, ids, vals,
                                  context=None):
        """Hook que cridarà el poweremail quan es modifiqui un email.
        """
        vals_w = {}
        key_map = (
            ('enviat_data', 'date_mail'),
            ('enviat_carpeta', 'folder')
        )
        for f_key, key in key_map:
            if key in vals:
                vals_w[f_key] = vals[key]
        if vals_w:
            self.write(cursor, uid, ids, vals_w)
        return True

    def generar_resum_factures_xls(self, cursor, uid, ids, header, context=None):
        line_obj = self.pool.get('giscedata.facturacio.factura.linia')
        polissa_obj = self.pool.get('giscedata.polissa')

        output = StringIO.StringIO()
        sheet = Sheet(_('Resum factures'))
        sheet.add_row([_(h).encode('utf8') for h in header])
        for fact_id in ids:
            read_fields = ['date_invoice', 'polissa_id', 'facturacio',
                           'tarifa_acces_id', 'llista_preu',
                           'linies_potencia', 'linies_energia', 'name',
                           ]

            fact = self.read(cursor, uid, fact_id, read_fields)
            ctx = context.copy()
            ctx.update({'date': fact['date_invoice']})

            polissa = polissa_obj.browse(cursor, uid, fact['polissa_id'][0],
                                         context=ctx)

            if fact['facturacio'] == 1:
                facturacio = _('Mensual')
            else:
                facturacio = _('Bimensual')
            row = [
                   fact['name'],
                   polissa.cups.dp,
                   facturacio,
                   polissa.distribuidora.name,
                   fact['tarifa_acces_id'][1],
                   fact['llista_preu'][1].replace(' (EUR)', '')
            ]

            #            P1 P2 P3 P4 P5 P6
            potencies = [0, 0, 0, 0, 0, 0]
            for line in line_obj.read(cursor, uid, fact['linies_potencia']):
                if (len(line['product_id'][1]) >= 2 and
                    line['product_id'][1][-2] == 'P'):
                    pos = int(line['product_id'][1][-1]) - 1
                    if not line['isdiscount'] and not potencies[pos]:
                        # name Px where x in 1..6
                        potencies[pos] += line['quantity']

            #           P1 P1 P2 P2 P3 P3 P4 P4 P5 P5 P6 P6
            #           €  kw €  kw €  kw €  kw €  kw €  kw
            energies = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            for line in line_obj.read(cursor, uid, fact['linies_energia']):
                if (len(line['product_id'][1]) >= 2 and
                    line['product_id'][1][-2] == 'P'):
                    # €
                    #     |---           Px          ---|          |-- € are even
                    pos = (int(line['product_id'][1][-1]) - 1) * 2
                    energies[pos] += line['price_subtotal']
                    # kW
                    if not line['isdiscount']:  #                      |-- € are even
                        pos = (int(line['product_id'][1][-1]) - 1) * 2 + 1
                        energies[pos] += line['quantity']

            row.extend(potencies)
            row.extend(energies)
            sheet.add_row(row)
        sheet.save(output)
        return {'csv': base64.b64encode(output.getvalue())}

    _constraints = [(_cnt_llista_preu_compatible,
                     _(u"La llista de preu no és compatible amb la tarifa "
                       u"d'accés."),
                     ['tarifa', 'llista_preu']), ]

GiscedataFacturacioFactura()


class GiscedataFacturacioFacturaLinia(osv.osv):

    _name = 'giscedata.facturacio.factura.linia'
    _inherit = 'giscedata.facturacio.factura.linia'

    def product_id_change(self, cursor, uid, ids, pricelist, date_invoice,
                          product, uom, polissa_id=None, qty=0, name='',
                          type_='out_invoice', partner_id=False,
                          fposition_id=False, price_unit=False,
                          address_invoice_id=False, context=None):
        '''make the discount visible'''
        pricelist_obj = self.pool.get('product.pricelist')
        pricetype_obj = self.pool.get('product.price.type')
        product_obj = self.pool.get('product.product')

        if not context:
            context = {}

        res = super(GiscedataFacturacioFacturaLinia,
                    self).product_id_change(cursor, uid, ids, pricelist,
                          date_invoice, product, uom, polissa_id=polissa_id,
                          qty=qty, name=name, type_=type_,
                          partner_id=partner_id,
                          fposition_id=fposition_id, price_unit=price_unit,
                          address_invoice_id=address_invoice_id,
                          context=context)

        list = pricelist_obj.browse(cursor, uid, pricelist)
        if (type_ in ['out_invoice'] and
            product and list.visible_discount):
            #Get pricelist version
            date = date_invoice or time.strftime('%Y-%m-%d')
            cursor.execute(
                '''SELECT id
                   FROM product_pricelist_version
                   WHERE pricelist_id = %s AND active=True
                   AND (date_start IS NULL OR date_start <= %s)
                   AND (date_end IS NULL OR date_end >= %s)
                   ORDER BY id LIMIT 1''', (pricelist, date, date))
            version_id = cursor.fetchone()[0]
            warning_msg = res.get('warning', False)
            if warning_msg:
                raise osv.except_osv(
                    warning_msg.get('title', _(u'Error')),
                    warning_msg.get('message')
                )
            price = res['value']['price_unit_multi']
            #Get parent price
            prod = product_obj.browse(cursor, uid, product)
            category_ids = prod.get_categories()
            #Search for then item that give us the price for the product
            cursor.execute(
                '''SELECT i.*, pl.currency_id
                   FROM product_pricelist_item i
                   INNER JOIN product_pricelist_version v
                     ON i.price_version_id = v.id
                   INNER JOIN product_pricelist pl
                     ON v.pricelist_id = pl.id
                   WHERE
                     (coalesce(product_tmpl_id, 0) = 0 OR product_tmpl_id = %s)
                     AND (coalesce(product_id, 0) = 0 OR product_id = %s)
                     AND (coalesce(categ_id, 0) = 0 OR categ_id IN %s)
                     AND price_version_id = %s
                     AND (min_quantity IS NULL OR min_quantity <= %s)
                   ORDER BY sequence LIMIT 1''',
                   (prod.product_tmpl_id.id, prod.id, tuple(category_ids),
                    version_id, abs(qty) or 1.0))
            item_vals = cursor.dictfetchone()
            pricetype_id = item_vals['base']
            #if price is based on a product field
            if pricetype_id > 0:
                # Only allow visible discount for energy products
                # based on other pricelist. We cannot read directly from
                # product because of uom's and historic prices
                if prod.categ_id.name in ('Potencia', 'Energia',
                                          'Reactiva', 'Exc. Potencia'):
                    parent_price = price
                else:
                    field_name = pricetype_obj.read(cursor, uid, pricetype_id,
                                                ['field'])['field']
                    parent_price = product_obj.read(cursor, uid, product,
                                                    [field_name])[field_name]
            #if price is based on another pricelist
            elif pricetype_id == -1:
                ctx = context.copy()
                ctx['uom'] = uom
                ctx['date'] = date
                parent_pricelist_id = item_vals['base_pricelist_id']
                parent_price = pricelist_obj.price_get(cursor, uid,
                               [parent_pricelist_id], product,
                               qty or 1.0, partner_id,
                               context=ctx)[parent_pricelist_id]
            elif pricetype_id == -3:
                parent_price = item_vals['base_price']
                product_uom = prod.uos_id or prod.uom_id
                parent_price = self.pool.get('product.uom')._compute_price(
                        cursor, uid, product_uom.id, parent_price, uom
                )
            elif pricetype_id == -4:
                parent_price = context['pricelist_base_price']
            else:
                return res
            #See if we have any changes between parent_price and price
            #If so, compute discount based on item fields
            if parent_price > price:
                discount = (parent_price - price) / parent_price * 100
                res['value']['price_unit_multi'] = parent_price
                res['value']['discount'] = discount

        return res

    def create(self, cursor, uid, values, context=None):
        '''if discount present, create new line with discount quantity'''

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_obj = self.pool.get('account.invoice')
        lang_obj = self.pool.get('res.lang')
        config_obj = self.pool.get('res.config')
        show_discount_separate = int(
                config_obj.get(cursor, uid, 'show_discount_separate', '1')
        )

        if not context:
            context = {}

        is_in_invoice = None

        if context.get('type_invoice', None):
            is_in_invoice = context['type_invoice'].startswith('in')
        else:
            inv_id = values.get('invoice_id', None)

            if not inv_id:
                if values.get('factura_id', None):
                    inv_id = fact_obj.read(
                        cursor, uid, values['factura_id'], ['invoice_id']
                    )['invoice_id'][0]

            if inv_id:
                invoice_type = inv_obj.read(
                    cursor, uid, inv_id, ['type']
                )['type']

                is_in_invoice = invoice_type.startswith('in')

        if values.get('discount', 0) and show_discount_separate and not is_in_invoice:
            #Search for lang
            lang_code = context.get('lang', 'ca_ES')
            lang_id = lang_obj.search(cursor, uid, [('code', '=', lang_code)])
            ctx = context.copy()
            discount_vals = values.copy()
            discount = discount_vals['discount']
            price = discount_vals['price_unit_multi']
            discount_price = ((-1 * price) +
                              price *
                               (1 - (values['discount'] or 0.0) / 100.0))
            #update discount_vals
            discount_vals['price_unit_multi'] = float_round(discount_price,
                                                int(config['price_accuracy']))
            discount_vals['discount'] = 0
            discount_vals['atrprice_subtotal'] = 0
            discount_vals['isdiscount'] = True
            if lang_id:
                format_discount = lang_obj.format(cursor, uid, lang_id,
                                              '%.2f', discount)
                format_price = lang_obj.format(cursor, uid, lang_id,
                                              '%.6f', price)
                discount_vals['note'] = _(u"-%s %% sobre %s" %
                                      (format_discount, format_price))
            else:
                discount_vals['note'] = _(u"-{:.2f} % sobre {:6f}").format(
                                            discount, price)
            #Remove discount from values
            values['discount'] = 0
            self.create(cursor, uid, discount_vals, context=context)
        #lloguer products are removed when the invoice
        #is created directly from comer. If not removed from
        #values when creating, lloguer lines will not group
        if (values['tipus'] == 'lloguer'
            and context.get('lloguer_comer', False)):
            values['product_id'] = 0
        return super(GiscedataFacturacioFacturaLinia,
                     self).create(cursor, uid, values, context=context)

    def _get_line_from_iline(self, cursor, uid, ids, context=None):

        invoice_line_obj = self.pool.get('account.invoice.line')
        factura_linia_obj = self.pool.get('giscedata.facturacio.factura.linia')

        search_params = [('invoice_line_id', 'in', ids)]
        return factura_linia_obj.search(cursor, uid, search_params)

    _columns = {
        'atrprice_subtotal': fields.float("Subtotal Peatges",
                                          digits=(16, 2), readonly=True),
        'isdiscount': fields.boolean("Línia de descompte")
    }

    _defaults = {
        'atrprice_subtotal': lambda *a: 0,
        'isdiscount': lambda *a: False,
    }

GiscedataFacturacioFacturaLinia()


class GiscedataFacturacioFacturaLecturesEnergia(osv.osv):
    """Classe per les lectures d'energia de les factures.
    """

    _name = 'giscedata.facturacio.lectures.energia'
    _inherit = 'giscedata.facturacio.lectures.energia'

    def create(self, cursor, uid, vals, context=None):
        if vals.get('comptador_id'):
            comptador_obj = self.pool.get('giscedata.lectures.comptador')
            vals['comptador'] = comptador_obj.browse(cursor, uid,
                                                     vals['comptador_id']).name
        res_id = super(GiscedataFacturacioFacturaLecturesEnergia,
                       self).create(cursor, uid, vals, context)
        return res_id

    _columns = {
         'comptador': fields.char('Comptador', size=64, required=True,
                                  readonly=True),
         'comptador_id': fields.many2one('giscedata.lectures.comptador',
                                         'Comptador', required=True,
                                         readonly=True),
    }

GiscedataFacturacioFacturaLecturesEnergia()


class GiscedataFacturacioFacturaLecturesPotencia(osv.osv):
    """Classe per les lectures de potència de les factures.
    """

    _name = 'giscedata.facturacio.lectures.potencia'
    _inherit = 'giscedata.facturacio.lectures.potencia'

    def create(self, cursor, uid, vals, context=None):
        if vals.get('comptador_id'):
            comptador_obj = self.pool.get('giscedata.lectures.comptador')
            vals['comptador'] = comptador_obj.browse(cursor, uid,
                                                     vals['comptador_id']).name
        res_id = super(GiscedataFacturacioFacturaLecturesPotencia,
                       self).create(cursor, uid, vals, context)
        return res_id

    _columns = {
        'comptador': fields.char('Comptador', size=64, required=True,
                                readonly=True),
        'comptador_id': fields.many2one('giscedata.lectures.comptador',
                                        'Comptador', required=True,
                                        readonly=True),
    }

GiscedataFacturacioFacturaLecturesPotencia()
