# -*- coding: utf-8 -*-
"""Modififiquem coses del package giscedata_polissa.
"""
from osv import osv, fields
from tools.translate import _
from osv.orm import OnlyFieldsConstraint

TIPUS_ENVIAMENT = [("postal", "Correu postal"),
                   ("email", "E-mail"),
                   ("postal+email", "Correu postal i e-mail")]


def _modes_facturacio(obj, cursor, uid, context=None):
    mode_fact_o = obj.pool.get('giscedata.polissa.mode.facturacio')
    if not mode_fact_o:
        return []
    sql = mode_fact_o.q(cursor, uid).select(['name', 'code']).where([])
    cursor.execute(*sql)
    modes_fact = cursor.dictfetchall()
    selection = []
    for mode_fact in modes_fact:
        selection.append((mode_fact['code'], mode_fact['name']))

    return selection


class GiscedataPolissa(osv.osv):
    """Afegim una constraint per tal de comprovar si la tarifa és vàlida.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def onchange_tipo_pago(self, cursor, uid, ids, tipo_pago, pagador, context=None):
        """ This function  automatize the completation of some fields
            if payment type is Recibo_CBS (Recibo domiciliado)
              if only exist one bank account automatic add it in field, 
              the same for payment group (Sepa 19)
        """
        res = super(GiscedataPolissa, self).onchange_tipo_pago(
            cursor, uid, ids, tipo_pago, pagador, context
        )
        if tipo_pago:
            tpg = self.pool.get('payment.type').browse(cursor, uid, tipo_pago)
            if tpg.code == 'RECIBO_CSB':
                pm_obj = self.pool.get('payment.mode')
                pm_ids = pm_obj.search(cursor, uid, [
                    ('type', '=', tpg.id),
                    ('tipo', '=', 'sepa19')
                ])
                if len(pm_ids) == 1:
                    res['value'].update({'payment_mode_id': pm_ids[0]})

        return res

    def onchange_modo_pago(self, cursor, uid, ids, modo_pago, pagador, context=None):
        if not modo_pago:
            return {}
        pm_obj = self.pool.get('payment.mode')
        pminfo = pm_obj.read(cursor, uid, modo_pago, ['type'], context=context)
        if not pminfo['type']:
            return {}
        tipo_pago = pminfo['type']
        if tipo_pago and isinstance(tipo_pago, tuple):
            tipo_pago = tipo_pago[0]
        res = self.onchange_tipo_pago(
            cursor, uid, ids, tipo_pago, pagador, context
        )
        res['value']['tipo_pago'] = tipo_pago
        return res

    def _default_enviament(self, cursor, uid, context=None):
        """Obté el valor per defecte de l'enviament segons configuració
        """
        cfg_obj = self.pool.get('res.config')
        cfg_id = cfg_obj.search(cursor, uid,
                                [('name', '=', 'polissa_enviament_default')])
        if not cfg_id:
            valor = 'postal'
        else:
            valor = cfg_obj.read(cursor, uid, cfg_id[0], ['value'])['value']
        return valor

    def _ff_emails(self, cursor, uid, ids, field_name, arg,
                          context=None):
        if not context:
            context = {}
        res = {}
        for polissa in self.browse(cursor, uid, ids):
            res[polissa.id] = {
                'pagador_email': polissa.direccio_pagament.email,
                'notificacio_email': polissa.direccio_notificacio.email
            }
        return res

    _columns = {
        'payment_mode_id': fields.many2one('payment.mode', 'Grup de pagament',
                                           readonly=True,
                                           states={'modcontractual': [('readonly', False),
                                                                      ('required', True)],
                                                    'esborrany': [('readonly', False)],
                                                    'validar': [('readonly', False),
                                                                ('required', True)]}),
        'enviament': fields.selection(
            TIPUS_ENVIAMENT,
            "Enviar factura via",
            readonly=True,
            states={'modcontractual': [('readonly', False), ('required', True)],
                    'esborrany': [('readonly', False)],
                    'validar': [('readonly', False)]}
        ),
        'pagador_email': fields.function(_ff_emails, method=True,
                                         type='char', readonly=True,
                                         string=u'Email dir. fiscal', size=240,
                                         multi='emails'),
        'notificacio_email': fields.function(_ff_emails, method=True,
                                             type='char', readonly=True,
                                             string=u'Email dir. notif.',
                                             size=240, multi='emails'),
        'refacturacio_pendent': fields.boolean('Refacturació Pendent'),

        'mode_facturacio': fields.selection(
            _modes_facturacio,
            u'Mode facturació',
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False), ('required', True)],
                'modcontractual': [('readonly', False), ('required', True)]
            }
        )
    }

    _defaults = {
      'enviament': _default_enviament,
      'refacturacio_pendent': lambda *a: 0,
      'mode_facturacio': lambda *a: 'atr',
    }

    def _cnt_llista_preu_compatible(self, cursor, uid, ids):
        """Comprovem que la llista de preu és compatible amb la tarifa.
        """
        llista_preu_in_preus_compatibles = """
            SELECT COUNT(*) FROM giscedata_polissa_tarifa_pricelist AS tpl
            INNER JOIN giscedata_polissa p ON p.id = %s
            WHERE  p.tarifa = tpl.tarifa_id AND tpl.pricelist_id = p.llista_preu
        """

        mode_fact_in_modes_fact_compatibles = """
            SELECT COUNT(*) FROM giscedata_polissa_invoicing_mode_pricelist AS impl
            INNER JOIN giscedata_polissa p ON p.id = %s
            INNER JOIN giscedata_polissa_mode_facturacio mf ON mf.code = %s
            WHERE mf.id = impl.invoicing_mode_id AND impl.pricelist_id = p.llista_preu
        """

        cols = ['id', 'llista_preu', 'tarifa', 'mode_facturacio']
        dmn = [('id', 'in', ids)]
        q = self.q(cursor, uid).select(cols).where(dmn)
        cursor.execute(*q)
        pols = cursor.dictfetchall()
        for pol in pols:
            tarifa_id = pol['tarifa']
            if pol['llista_preu'] and tarifa_id:
                cursor.execute(llista_preu_in_preus_compatibles, (pol['id'],))
                val = cursor.fetchall()
                if not val[0][0]:
                    return False

                cursor.execute(
                    mode_fact_in_modes_fact_compatibles,
                    (pol['id'], pol['mode_facturacio'])
                )
                val = cursor.fetchall()
                if not val[0][0]:
                    return False
        return True

    _constraints = [
        OnlyFieldsConstraint(
            _cnt_llista_preu_compatible,
            _(u"La llista de preu no és compatible amb la tarifa d'accés o el "
              u"mode de facturació"),
            ['tarifa', 'mode_facturacio', 'llista_preu']
        ),
    ]

    def update_mandate(self, cursor, uid, polissa_id, context=None):
        """Afegim el codi de creditor segons el grup de pagament.
        """
        res = super(GiscedataPolissa,
                    self).update_mandate(cursor, uid, polissa_id, context)
        if not context:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id)
        if polissa.payment_mode_id:
            res['creditor_code'] = polissa.payment_mode_id.sepa_creditor_code
        return res


GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'payment_mode_id': fields.many2one('payment.mode', 'Grup de pagament'),
        'enviament': fields.selection(TIPUS_ENVIAMENT, "Enviar factura via"),
        'pagador_email': fields.char('Email dir. fiscal', size=240, readonly=True),
        'notificacio_email': fields.char('Email dir. notif.',
                                           size=240, readonly=True),
        'mode_facturacio': fields.selection(
            _modes_facturacio, u'Mode facturació'
        ),
    }

    _defaults = {
        'mode_facturacio': lambda *a: 'atr',
    }


GiscedataPolissaModcontractual()


class GiscedataPolissaTarifa(osv.osv):
    """Modifiquem la classe per ficar amb quines llistes de preus és compatible.
    """
    _name = 'giscedata.polissa.tarifa'
    _inherit = 'giscedata.polissa.tarifa'

    _columns = {
        'llistes_preus_comptatibles': fields.many2many('product.pricelist',
                                           'giscedata_polissa_tarifa_pricelist',
                                           'tarifa_id', 'pricelist_id',
                                           'Llistes de preus compatibles')
    }


GiscedataPolissaTarifa()


class GiscedataPolissaModeFacturacio(osv.osv):

    _name = "giscedata.polissa.mode.facturacio"
    _columns = {
        'name': fields.char('Nom', size=16, required=True),
        'code': fields.char('Codi', size=8, required=True),
    }


GiscedataPolissaModeFacturacio()


class GiscedataPolissaModeFacturacio(osv.osv):

    _name = "giscedata.polissa.mode.facturacio"
    _inherit = "giscedata.polissa.mode.facturacio"

    _columns = {
        'compatible_price_lists': fields.many2many(
            'product.pricelist', 'giscedata_polissa_invoicing_mode_pricelist',
            'invoicing_mode_id', 'pricelist_id', 'Llista de preus compatibles'
        )
    }


GiscedataPolissaModeFacturacio()
