# -*- coding: utf-8 -*-
from libcomxml.core import XmlField, XmlModel


# -- Nodes comuns

class Notificacion(XmlModel):

    _sort_order = ('pais', 'domicilio', 'direccion', 'codigo_postal',
                   'provincia', 'municipio', 'telefono', 'fax', 'movil',
                   'e_mail')

    def __init__(self):
        self.notificacion = XmlField('NOTIFICACION', attributes={'xmlns': ""})
        self.pais = XmlField('PAIS')
        self.domicilio = XmlField('DOMICILIO')
        self.direccion = XmlField('DIRECCION')
        self.codigo_postal = XmlField('CODIGO_POSTAL')
        self.provincia = XmlField('PROVINCIA')
        self.municipio = XmlField('MUNICIPIO')
        self.telefono = XmlField('TELEFONO')
        self.fax = XmlField('FAX')
        self.movil = XmlField('MOVIL')
        self.e_mail = XmlField('E_MAIL')
        super(Notificacion, self).__init__(
            'NOTIFICACION', 'notificacion', drop_empty=False
        )


class Empresa(XmlModel):

    _sort_order = ('pais', 'razon_social', 'cif', 'vat', 'domicilio',
                   'direccion', 'codigo_postal', 'provincia', 'municipio',
                   'telefono', 'fax', 'movil', 'e_mail')

    def __init__(self):
        self.empresa = XmlField('EMPRESA', attributes={'xmlns': ""})
        self.pais = XmlField('PAIS')
        self.razon_social = XmlField('RAZON_SOCIAL')
        self.cif = XmlField('CIF')
        self.vat = XmlField('VAT')
        self.domicilio = XmlField('DOMICILIO')
        self.direccion = XmlField('DIRECCION')
        self.codigo_postal = XmlField('CODIGO_POSTAL')
        self.provincia = XmlField('PROVINCIA')
        self.municipio = XmlField('MUNICIPIO')
        self.telefono = XmlField('TELEFONO')
        self.fax = XmlField('FAX')
        self.movil = XmlField('MOVIL')
        self.e_mail = XmlField('E_MAIL')
        super(Empresa, self).__init__('EMPRESA', 'empresa', drop_empty=False)

# ---------------


class XML606(XmlModel):
    __NSMAP = {
        'xsd': "http://www.w3.org/2001/XMLSchema",
        'xsi': "http://www.w3.org/2001/XMLSchema-instance"
    }
    _sort_order = ['informacion', 'empresa', 'bandas', 'notificacion']
    pretty_print = True

    @property
    def _nsmap(self):
        return self.__NSMAP.copy()

    def __init__(self):
        self._attrs = getattr(self, '_attrs', {})
        self._attrs.update({'nsmap': self._nsmap})
        self._root = getattr(self, '_root', None)
        self.root = XmlField(self._root, attributes=self._attrs)

        self.empresa = Empresa()
        self.notificacion = Notificacion()

        super(XML606, self).__init__('Informacion', 'root', drop_empty=False)

    def get_banda(self, franja):
        return getattr(self.bandas, franja.replace('-', '_'))

    def set(self, companies_vals, informes_vals):
        self.__set_company_vals(companies_vals)
        self._set_informes_vals(informes_vals)

    def _set_informes_vals(self, informes_vals):
        for informe_name, linies_vals in informes_vals.items():
            for linia_vals in linies_vals:
                self._set_linia_vals(linia_vals)

    def _set_linia_vals(self, linia_vals):
        pass

    def __set_company_vals(self, companies_vals):
        for company_id, company_vals in companies_vals.items():
            self.empresa.feed({
                'pais': company_vals['country'],
                'razon_social': company_vals['name'],
                'cif': company_vals['cifnif'],
                'vat': company_vals['vat'],
                'domicilio': company_vals['street'],
                'direccion': company_vals['street'],
                'codigo_postal': company_vals['zip'],
                'provincia': company_vals['state_id'],
                'municipio': company_vals['id_municipi'],
                'telefono': company_vals['phone'],
                'fax': company_vals['fax'],
                'movil': company_vals['mobile'],
                'e_mail': company_vals['email']
            })
            self.notificacion.feed({
                'pais': company_vals['country'],
                'domicilio': company_vals['street'],
                'direccion': company_vals['street'],
                'codigo_postal': company_vals['zip'],
                'provincia': company_vals['state_id'],
                'municipio': company_vals['id_municipi'],
                'telefono': company_vals['phone'],
                'fax': company_vals['fax'],
                'movil': company_vals['mobile'],
                'e_mail': company_vals['email']
            })


class XML606ML(XML606):

    def __init__(self):
        self.bandas = BandasML(self._leafs)
        super(XML606ML, self).__init__()

    def _set_linia_vals(self, linia_vals):
        banda = self.get_banda(linia_vals['franja'])
        banda.feed({
            'nclientes': linia_vals['num_clients'],
            'energiaml': linia_vals['energia_A'],
            'potenciaf': linia_vals['potencia'],
            'ftarifa_sin_impuestos': linia_vals['fact_tarifa_B'],
            'ftarifa_sin_iva': linia_vals['fact_tarifa_C'],
            'fenergia_ml_sin_impuestos': linia_vals['fact_tarifa_E'],
            'ftarifa_con_impuestos': linia_vals['fact_tarifa_D'],
            'fenergia_ml_sin_iva': linia_vals['fact_tarifa_F'],
            'fenergia_ml_con_impuestos': linia_vals['fact_tarifa_G'],
            'f_sin_impuestos': linia_vals['fact_total_BE'],
            'f_con_impuestos': linia_vals['fact_total_DG'],
            'f_alquiler': linia_vals['fact_equips'],
            'tensionmax': linia_vals['v_max'],
            'tensionmin': linia_vals['v_min'],
            'p_total_sin_impuestos': linia_vals['preu_BEA'],
            'f_sin_iva': linia_vals['fact_total_CF'],
            'p_sin_iva': linia_vals['preu_CFA'],
            'p_con_impuestos': linia_vals['preu_DGA']
        })


class XML606IndustrialAnual(XML606):

    def __init__(self):
        self.bandas = BandasAnualesIndustrial(self._leafs)
        super(XML606IndustrialAnual, self).__init__()


class XML606DomesticoAnual(XML606):

    def __init__(self):
        self.bandas = BandasAnualesDomestico(self._leafs)
        super(XML606DomesticoAnual, self).__init__()

# -- Bandas


class BandasAnualesIndustrial(XmlModel):

    def __init__(self, bandes):
        self._sort_order = bandes
        self.bandas = XmlField('BANDASINDUSTRIALANUAL', attributes={'xmlns': ""})
        for attr in self._sort_order:
            setattr(self, attr, BandaAnualIndustrial(attr))
        super(BandasAnualesIndustrial, self).__init__(
            'BANDASINDUSTRIALANUAL', 'bandas', drop_empty=False
        )


class BandasAnualesDomestico(XmlModel):

    def __init__(self, bandes):
        self._sort_order = bandes
        self.bandas = XmlField('BANDASDOMESTICOANUAL', attributes={'xmlns': ""})
        for attr in self._sort_order:
            setattr(self, attr, BandaAnualDomestico(attr))
        super(BandasAnualesDomestico, self).__init__(
            'BANDASDOMESTICOANUAL', 'bandas', drop_empty=False
        )


class BandaAnual(XmlModel):

    def __init__(self, root):
        self._sort_order = ['energia', 'energiaysuministro', 'tarifa_acceso',
                            'alquiler_equipos']
        self.root = XmlField(root)
        self.energia = XmlField('ENERGIA')
        self.energiaysuministro = XmlField('ENERGIAYSUMINISTRO')
        self.tarifa_acceso = XmlField('TARIFA_ACCESO')
        self.alquiler_equipos = XmlField('ALQUILER_EQUIPOS')
        super(BandaAnual, self).__init__(root, 'root', drop_empty=False)


class BandaAnualIndustrial(BandaAnual):

    def __init__(self, root):
        self.energia_total_sin_impuestos = XmlField('ENERGIA_TOTAL_SIN_IMPUESTOS')
        self.impuestos_no_recuperables = XmlField('IMPUESTOS_NO_RECUPERABLES')
        super(BandaAnualIndustrial, self).__init__(root)
        self._sort_order.insert(1, 'energia_total_sin_impuestos')
        self._sort_order.insert(len(self._sort_order), 'impuestos_no_recuperables')


class BandaAnualDomestico(BandaAnual):
    def __init__(self, root):
        self.energia_total_con_impuestos = XmlField('ENERGIA_TOTAL_CON_IMPUESTOS')
        self.impuestos_e_iva = XmlField('IMPUESTOS_E_IVA')
        super(BandaAnualDomestico, self).__init__(root)
        self._sort_order.insert(1, 'energia_total_con_impuestos')
        self._sort_order.insert(len(self._sort_order), 'impuestos_e_iva')


class BandasML(XmlModel):

    def __init__(self, bandes):
        self._sort_order = bandes
        self.bandas = XmlField('BANDASML', attributes={'xmlns': ""})
        for attr in self._sort_order:
            setattr(self, attr, BandaML(attr))
        super(BandasML, self).__init__('BANDASML', 'bandas', drop_empty=False)


class BandaML(XmlModel):

    _sort_order = ['nclientes', 'energiaml', 'potenciaf',
                   'ftarifa_sin_impuestos', 'ftarifa_sin_iva',
                   'fenergia_ml_sin_impuestos', 'ftarifa_con_impuestos',
                   'fenergia_ml_sin_iva', 'fenergia_ml_con_impuestos',
                   'f_sin_impuestos', 'f_con_impuestos', 'f_alquiler',
                   'tensionmax', 'tensionmin', 'p_total_sin_impuestos',
                   'f_sin_iva', 'p_sin_iva', 'p_con_impuestos']

    def __init__(self, root):
        self.root = XmlField(root)
        self.nclientes = XmlField('NCLIENTES')
        self.energiaml = XmlField('ENERGIAML')
        self.potenciaf = XmlField('POTENCIAF')
        self.ftarifa_sin_impuestos = XmlField('FTARIFA_SIN_IMPUESTOS')
        self.ftarifa_sin_iva = XmlField('FTARIFA_SIN_IVA')
        self.fenergia_ml_sin_impuestos = XmlField('FENERGIA_ML_SIN_IMPUESTOS')
        self.ftarifa_con_impuestos = XmlField('FTARIFA_CON_IMPUESTOS')
        self.fenergia_ml_sin_iva = XmlField('FENERGIA_ML_SIN_IVA')
        self.fenergia_ml_con_impuestos = XmlField('FENERGIA_ML_CON_IMPUESTOS')
        self.f_sin_impuestos = XmlField('F_SIN_IMPUESTOS')
        self.f_con_impuestos = XmlField('F_CON_IMPUESTOS')
        self.f_alquiler = XmlField('F_ALQUILER')
        self.tensionmax = XmlField('TENSIONMAX')
        self.tensionmin = XmlField('TENSIONMIN')
        self.p_total_sin_impuestos = XmlField('P_TOTAL_SIN_IMPUESTOS')
        self.f_sin_iva = XmlField('F_SIN_IVA')
        self.p_sin_iva = XmlField('P_SIN_IVA')
        self.p_con_impuestos = XmlField('P_CON_IMPUESTOS')
        super(BandaML, self).__init__(root, 'root', drop_empty=False)

# -- classes que generen els XML


class MITC606IndustrialAnual(XML606IndustrialAnual):

    def __init__(self):
        self._attrs = {'xmlns': "http://tempuri.org/XMLIndustrialAnual.xsd"}
        self._root = 'InformacionIndustrialA'
        self._leafs = ['Banda_IA', 'Banda_IB', 'Banda_IC', 'Banda_ID',
                       'Banda_IE', 'Banda_IF', 'Banda_IG']
        super(MITC606IndustrialAnual, self).__init__()
        self._sort_order.insert(2, 'bandasindustrialanual')

    def _set_linia_vals(self, linia_vals):
        banda = self.get_banda(linia_vals['franja'])
        banda.feed({
            'energia': linia_vals['energia'],
            'energia_total_sin_impuestos': linia_vals['energia_total_sin_impuestos'],
            'energiaysuministro': linia_vals['energiaysuministro'],
            'tarifa_acceso': linia_vals['tarifa_acceso'],
            'alquiler_equipos': linia_vals['alquiler_equipos'],
            'impuestos_no_recuperables': linia_vals['impuestos_no_recuperables']
        })


class MITC606DomesticoAnual(XML606DomesticoAnual):

    def __init__(self):
        self._attrs = {'xmlns': "http://tempuri.org/XMLDomesticoAnual.xsd"}
        self._root = 'InformacionDomesticoA'
        self._leafs = ['Banda_DA', 'Banda_DB', 'Banda_DC', 'Banda_DD1',
                       'Banda_DD2', 'Banda_DE']
        super(MITC606DomesticoAnual, self).__init__()
        self._sort_order.insert(2, 'bandasdomesticoanual')

    def _set_linia_vals(self, linia_vals):
        banda = self.get_banda(linia_vals['franja'])
        banda.feed({
            'energia': linia_vals['energia'],
            'energia_total_con_impuestos': linia_vals['energia_total_con_impuestos'],
            'energiaysuministro': linia_vals['energiaysuministro'],
            'tarifa_acceso': linia_vals['tarifa_acceso'],
            'alquiler_equipos': linia_vals['alquiler_equipos'],
            'impuestos_e_iva': linia_vals['impuestos_e_iva']
        })


class MITC606DomesticoML(XML606ML):

    def __init__(self):
        self._attrs = {'xmlns': "http://tempuri.org/XMLDomesticoML.xsd"}
        self._root = 'InformacionD'
        self._leafs = ['Banda_DA', 'Banda_DB', 'Banda_DC', 'Banda_DD1',
                       'Banda_DD2', 'Banda_DE']
        super(MITC606DomesticoML, self).__init__()


class MITC606IndustrialML(XML606ML):

    def __init__(self):
        self._attrs = {'xmlns': "http://tempuri.org/XMLDomesticoIndustrial.xsd"}
        self._root = 'Informacion'
        self._leafs = ['Banda_IA', 'Banda_IB', 'Banda_IC', 'Banda_ID',
                       'Banda_IE', 'Banda_IF', 'Banda_IG']
        super(MITC606IndustrialML, self).__init__()
