# -*- coding: utf-8 -*-

import wizard
import pooler

_get_params_form = """<?xml version="1.0"?>
<form title="Dates límit">
  <field name="data_limit" required="True"/>
  <newline/>
  <field name="data_tall" required="True"/>
</form>
"""

_get_params_fields = {
    'data_limit': {'string': 'Data límit de pagament', 'type': 'date'},
    'data_tall': {'string': 'Data de tall', 'type': 'date'},

}
class WizardAvisImpagament(wizard.interface):
    """Wizard per llençar el report de l'avís d'impagament.

    Es fa servir per passar paràmetres al Jasper.
    """

    def _get_params(self, cr, uid, data, context=None):
        """Funció per demanar i retornar els paràmetres.
        """
        res = {
            'parameters': {
                'data_tall': data['form']['data_tall'],
                'data_limit': data['form']['data_limit'],
            }
        }
        return res

    states = {
        'init': {
            'actions': [],
            'result': {
                'type': 'form',
                'arch': _get_params_form,
                'fields': _get_params_fields,
                'state': [('end', 'Cancel'), ('get_params', 'Següent')],
            }
        },
        'get_params': {
            'actions': [_get_params],
            'result': {
                'type': 'print',
                'name': 'Carta avís d\'impagament',
                'report': 'giscedata.facturacio.factura.avis_impagat',
                'xml': 'giscedata_facturacio_comer/report/avis_imapgament.jrxml',
                'state': 'end',
            }
        }
    }

WizardAvisImpagament('wizard_avis_impagament')
