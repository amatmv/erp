# -*- coding: utf-8 -*-

"""Wizards d'informe de preus de comercialitzadora del mitc, model 606.

   Quan no hi ha l'històric d'un any facturat, es pot realitzar
   l'informe extrapolant-ne el consum. Per això cal sel·leccionar
   'Extrapolar el consum anual' i configurar el factor de correcció.
   Per exemple, si s'han facturat els primers 6 mesos, de 
   l'any, s'especifiquen la data_inici (1-gener) i data_final 
   (30-juny), es selecciona l'extrpol·lació i f_corr (2).

   Especificacions: https://www.boe.es/eli/es/o/2011/03/16/itc606/con
"""
import sys
from datetime import datetime, timedelta
import time
import pandas as pd
import netsvc
from osv import fields, osv
from tools.misc import cache
from tools import config
from tools.translate import _
from libfacturacioatr import tarifes
from progressbar import ProgressBar, ETA, Percentage

from giscedata_facturacio.giscedata_facturacio import REFUND_RECTIFICATIVE_INVOICE_TYPES


IESE_NAME = 'Impuesto especial sobre la electricidad'
IVA_NAME = 'IVA 18%'


def restar_any(data):
    """Resta un any a la data.
       La data de retorn es considera part d'un interval tancat
       i serà inclusiva
    """
    dt = datetime.strptime(data, "%Y-%m-%d")
    dia = dt.day
    for i in range(12):
        dt = dt.replace(day=1)
        dt -= timedelta(days=1)
    while dt.day > dia:
        dt -= timedelta(days=1)
    # ajustar a l'interval tancat
    dt += timedelta(days=1)
    return dt.strftime("%Y-%m-%d")


class WizardInformePreusMitc(osv.osv_memory):
    """Wizard per llençar l'informe de preus del mitc
    """
    _name = "wizard.informe.preus_mitc"
    _description = __doc__

    RANGS_VALS_DOM = {
        'Banda-DA': ('<', 1000),
        'Banda-DB': ('<', 2500),
        'Banda-DC': ('<', 5000),
        'Banda-DD1': ('<', 8000),
        'Banda-DD2': ('<', 15000),
        'Banda-DE': ('>=', 15000),
    }

    RANGS_VALS_IND = {
        'Banda-IA': ('<', 20),
        'Banda-IB': ('<', 500),
        'Banda-IC': ('<', 2000),
        'Banda-ID': ('<', 20000),
        'Banda-IE': ('<', 70000),
        'Banda-IF': ('<=', 150000),
        'Banda-IG': ('>', 150000),
    }

    OPER_RANG = {
        'industrial': 0.001,
        'domestic': 1
    }

    logger = netsvc.Logger()

    def get_info_polisses(self, cursor, uid, ids, llista_factures, info_polissa_dom,
                          info_polissa_ind, context=None):
        """
        Retorna un diccionari amb les polisses i la seva informació. Agrupa la
        informació de les factures per cada pòlissa.
        :param cursor:
        :param uid:
        :param ids:
        :param llista_factures:
        :param info_polissa_dom:
        :param info_polissa_ind:
        :param context:
        :return:
        """
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids, context)
        dt_wiz_inici = datetime.strptime(wiz.data_inici, "%Y-%m-%d")
        # Calculem les dates del periode anterior
        # Si extrapolem, no mes calculem energia del periode
        # de l'informe, si no calculem de tot un any
        if wiz.ext_anual:
            pre_data_inici = wiz.data_inici
        else:
            pre_data_inici = restar_any(wiz.data_final)

        pre_data_final = wiz.data_final
        self.logger.notifyChannel(
            "objects", netsvc.LOG_INFO, u"Calculant energia entre %s i %s" % (pre_data_inici, pre_data_final)
        )
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        self.logger.notifyChannel(
            "objects", netsvc.LOG_INFO, u"Informe de preus segons %s factures" % len(llista_factures)
        )
        pb = ProgressBar(
            maxval=len(llista_factures), widgets=[Percentage(), ' ', ETA()]
        ).start()

        query_energia_file = u"%s/giscedata_facturacio_comer/sql/query_energia_itc606.sql" % config['addons_path']
        query_energia = open(query_energia_file).read()
        # Compute energia for polisses
        cursor.execute(query_energia, (pre_data_inici, pre_data_final))
        # Diccionari k: polissa id, v: energia
        energia_polissa = dict([(x[0], x[1]) for x in cursor.fetchall()])
        done = 0
        for factura_id in llista_factures:
            factura = factura_obj.browse(cursor, uid, factura_id)
            skip = False
            for l in factura.linia_ids:
                if l.tipus in ('energia', 'potencia') and not l.product_id.id:
                    skip = True
                    break
            if skip:
                self.logger.notifyChannel(
                    "objects", netsvc.LOG_WARNING,
                    u"La factura {} s'ha ignorat ja que te alguna linia sense "
                    u"producte".format(factura.number)
                )
                continue

            self.logger.notifyChannel(
                "objects", netsvc.LOG_DEBUG, u"Calculant factura %s" % factura.number
            )
            pb.update(done)
            subtotal = 0
            potencia_total = 0
            linies_potencia = 0
            energia_factura = 0

            if factura.partner_id.cifnif == 'NI':
                info_polissa = info_polissa_dom
                self.logger.notifyChannel(
                    "objects", netsvc.LOG_DEBUG, u"Domèstic"
                )
            else:
                info_polissa = info_polissa_ind
                self.logger.notifyChannel(
                    "objects", netsvc.LOG_DEBUG, u"Industrial"
                )

            vals = info_polissa.get(factura.polissa_id.name, {})
            vals.setdefault('lloguer', 0)
            vals.setdefault('valid',  0)
            vals.setdefault('A_anual', 0)
            vals.setdefault('A', 0)
            vals.setdefault('potencia', 0)
            vals.setdefault('B', 0)
            vals.setdefault('C', 0)
            vals.setdefault('D', 0)
            vals.setdefault('E', 0)
            vals.setdefault('F', 0)
            vals.setdefault('G', 0)
            vals.setdefault('BE', 0)
            vals.setdefault('CF', 0)
            vals.setdefault('DG', 0)

            iva_factor = 0

            for l in factura.linia_ids:
                vals['valid'] = 1
                self.logger.notifyChannel(
                    "objects", netsvc.LOG_DEBUG, u"línia %s" % l.tipus
                )
                if l.tipus == 'altres':
                    continue

                if not l.isdiscount:
                    # Recalculem per distri
                    if l.tipus == 'energia':
                        # Energía mercado libre
                        vals['A'] += l.quantity

                    if l.tipus == 'potencia':
                        potencia_total += l.quantity
                        linies_potencia += 1

                    if l.tipus == 'lloguer':
                        vals['lloguer'] += l.price_subtotal

                    # Es suposa que el % d'IVA per les linies != altres sempre
                    # es el mateix
                    if iva_factor == 0:
                        for tax in l.invoice_line_tax_id:
                            if tax.name.startswith('IVA'):
                                iva_factor = tax.amount

                subtotal_line = round(l.quantity * l.multi * l.price_unit_multi, 4)

                if l.tipus == 'energia':
                    energia_factura += subtotal_line

                # Aixo es B + E
                subtotal += subtotal_line

            # En el cas de linies de potencia, si hi ha mes de una
            # no podem sumar-les, i hem de calcular la mitjana
            if linies_potencia:
                vals['potencia'] += potencia_total / linies_potencia

            dt_f_final = datetime.strptime(factura.data_final, "%Y-%m-%d")

            if dt_f_final >= dt_wiz_inici:
                iese_amount = 0
                iese_factor = 0
                for invoice_tax in factura.tax_line:
                    if IESE_NAME in invoice_tax.name:
                        iese_amount = invoice_tax.amount
                        if invoice_tax.factor_base_amount > 0:
                            iese_factor = iese_amount / invoice_tax.factor_base_amount

                # Facturación energía a mercado libre sin impuestos
                vals['E'] += energia_factura

                # Facturación energía a mercado libre sin IVA ni otros impuestos
                # recuperables
                F = energia_factura + energia_factura * iese_factor
                vals['F'] += F

                # Facturación energía a mercado libre con todos los impuestos e
                # IVA
                vals['G'] += F + F * iva_factor

                # Facturación total sin impuestos
                vals['BE'] += subtotal

                # Facturación total sin IVA ni otros impuestos
                # recuperables. (IESE es un impuesto no recuperable)
                CF = subtotal + iese_amount
                vals['CF'] += CF

                # Facturación total con todos los impuestos e IVA
                vals['DG'] += CF + CF * iva_factor

                vals['tensio'] = factura.polissa_id.tensio
                self.logger.notifyChannel(
                    "objects", netsvc.LOG_DEBUG, u"VALS: %s" % vals
                )
            
            if factura.polissa_id.name not in info_polissa:
                # If not extrapolate, calc energia of the previous period
                vals['A_anual'] += energia_polissa.get(factura.polissa_id.id, 0)
                info_polissa[factura.polissa_id.name] = vals

            done += 1

        pb.finish()

    def get_range_name(self, consum_anual, tipus_consumidor):
        """ Retorna el nom clau del rang de classificació
        """
        consum_anual *= self.OPER_RANG[tipus_consumidor]

        if tipus_consumidor == 'domestic':
            rangs_vals = self.RANGS_VALS_DOM
        else:
            rangs_vals = self.RANGS_VALS_IND

        name = ""

        for key, value in sorted(rangs_vals.items()):
            if eval('%d %s %d' % (consum_anual, value[0], value[1])):
                name = key
                break

        return name

    def initvals(self, vals, range_name, tipus_consumidor):
        vals.setdefault('num_clients', 0)
        vals.setdefault('energia_A', 0)
        vals.setdefault('potencia', 0)
        vals.setdefault('fact_tarifa_B', 0)
        vals.setdefault('fact_tarifa_C', 0)
        vals.setdefault('fact_tarifa_D', 0)
        vals.setdefault('fact_tarifa_E', 0)
        vals.setdefault('fact_tarifa_F', 0)
        vals.setdefault('fact_tarifa_G', 0)
        vals.setdefault('fact_total_BE', 0)
        vals.setdefault('fact_total_CF', 0)
        vals.setdefault('fact_total_DG', 0)
        vals.setdefault('fact_equips', 0)
        vals.setdefault('v_max', 0)
        vals.setdefault('v_min', sys.maxint)
        vals.setdefault('preu_BEA', 0)
        vals.setdefault('preu_CFA', 0)
        vals.setdefault('preu_DGA', 0)
        vals.setdefault('franja', range_name)
        vals.setdefault('consumidor', tipus_consumidor)
        vals.setdefault('informe_id', 0)

    def classificar_polisses(self, cursor, uid, ids, info_polissa, rangs_res, 
                             f_corr, tipus_consumidor):
        """ Classifica la informació de les polisses en la taula de resultats 
            rangs_res segons rangs de classificació
        """
        wiz = self.browse(cursor, uid, ids[0])

        rows = []
        for key, values in sorted(info_polissa.items()):
            row = {'polissa': key}
            row.update(values)
            rows.append(row)

        df = pd.DataFrame(rows)
        df.to_csv(
            '/tmp/606_%s_%s.csv'.format(int(time.time()), cursor.dbname),
            index=None,
            sep=';',
        )
        # TODO add this csv in wizard to validate generation

        for key in sorted(info_polissa.keys()):
            value = info_polissa[key]

            if wiz.ext_anual:
                consum_anual = value['A_anual'] * f_corr
            else:
                consum_anual = value['A_anual']

            if not value['valid']:
                continue

            range_name = self.get_range_name(consum_anual, tipus_consumidor)

            if range_name not in rangs_res:
                rangs_res[range_name] = {}
                self.initvals(
                    rangs_res[range_name], range_name, tipus_consumidor
                )

            rang = rangs_res[range_name]
            rang['informe_id'] = wiz.informe_id.id
            rang['num_clients'] += 1
            rang['energia_A'] += value['A']
            rang['potencia'] += value['potencia']
            rang['fact_tarifa_B'] += value['B']
            rang['fact_tarifa_C'] += value['C']
            rang['fact_tarifa_D'] += value['D']
            rang['fact_total_BE'] += value['BE']
            rang['fact_total_CF'] += value['CF']
            rang['fact_total_DG'] += value['DG']
            rang['fact_tarifa_E'] += value['E']
            rang['fact_tarifa_F'] += value['F'] 
            rang['fact_tarifa_G'] += value['G']
            rang['fact_equips'] += value['lloguer']
            rang['v_max'] = max(value.get('tensio', 0), rang['v_max'])
            rang['v_min'] = min(value.get('tensio', sys.maxint), rang['v_min'])
    
    def action_generar_dades(self, cursor, uid, ids, context=None):
        """ Omple les taules amb les dades generades
        """
        if not context:
            context = {}

        if isinstance(ids, list):
            ids = ids[0]

        self.write(cursor, uid, [ids], {'state': 'create'}, context)
        wiz = self.browse(cursor, uid, ids, context)
        informe_obj = self.pool.get(
            'giscedata.facturacio.informe_preus_consumidors'
        )
        linia_informe_obj = self.pool.get(
            'giscedata.facturacio.informe_preus_consumidors.linia'
        )
    
        informe_name = '%s - %s' % (wiz.data_inici, wiz.data_final)
        self.logger.notifyChannel(
            "Informe de preus mitc", netsvc.LOG_INFO, u"Període: %s" % informe_name
        )
        search_params = [('name', '=', informe_name)]
        informe = informe_obj.search(cursor, uid, search_params)

        if informe:
            informe_obj.unlink(cursor, uid, informe)

        vals = {
            'name': informe_name
        }
        informe = informe_obj.create(cursor, uid, vals)
        wiz.write({'informe_id': informe})
        req_inici = wiz.data_inici
        # 'B': ANULCSUST, 'A': ANULADORA
        # Primer busquem totes les referències de factures anul·ladores (A) i 
        # rectificadores amb substituient (B)
        o_factures = self.pool.get('giscedata.facturacio.factura')
        search_params = [('tipo_rectificadora', 'in', REFUND_RECTIFICATIVE_INVOICE_TYPES),
                         ('invoice_id.journal_id.code', '=like', 'ENERGIA%'),
                         ('date_invoice', '>=', req_inici)]
        f_ids = o_factures.search(cursor, uid, search_params)
        refs = [f['ref'] and f['ref'][0] or 0 
                for f in o_factures.read(cursor, uid, f_ids, ['ref'])]
        # Ara busquem totes les factures Normals (N) i Rectificadores (R) les
        # quals els seus ids no estiguin a refs
        search_params = [('tipo_rectificadora', 'in', ('N', 'R', 'RA')),
                         ('id', 'not in', refs),
                         ('invoice_id.journal_id.code', '=like', 'ENERGIA%'),
                         ('date_invoice', '>=', req_inici),
                         ('date_invoice', '<=', wiz.data_final),
                         ('tipo_factura', '=', '01')]
        factures = o_factures.search(cursor, uid, search_params)
        info_polissa_ind = {}
        info_polissa_dom = {}
        # llista_factures = o_factures.browse(cursor, uid, factures, context)
        # Agrupar la informació de les factures segons pòlisses
        if factures:
            self.get_info_polisses(
                cursor, uid, ids, factures, info_polissa_dom, info_polissa_ind
            )
        rangs_res = {}
        # Classificar la informació de les pòlisses segons rangs
        wiz.classificar_polisses(
            info_polissa_dom, rangs_res, wiz.f_corr, 'domestic'
        )
        wiz.classificar_polisses(
            info_polissa_ind, rangs_res, wiz.f_corr, 'industrial'
        )
        for value in rangs_res.values():
            value['fact_tarifa_B'] = value['fact_total_BE'] - value['fact_tarifa_E']
            value['fact_tarifa_C'] = value['fact_total_CF'] - value['fact_tarifa_F']
            value['fact_tarifa_D'] = value['fact_total_DG'] - value['fact_tarifa_G']
            value['preu_BEA'] = round(value['fact_total_BE'] / value['energia_A'], 4)
            value['preu_CFA'] = round(value['fact_total_CF'] / value['energia_A'], 4)
            value['preu_DGA'] = round(value['fact_total_DG'] / value['energia_A'], 4)
            # Passem de kW a MW
            value['energia_A'] *= 0.001
            value['potencia'] *= 0.001
            # Paseem de V a kV
            value['v_min'] *= 0.001
            value['v_max'] *= 0.001
            linia_informe_obj.create(cursor, uid, value, context)

        self.logger.notifyChannel(
            "objects", netsvc.LOG_INFO, u"Informa de preus finalitzat"
        )

    def action_obrir_informe(self, cursor, uid, ids, context=None):
        
        """Retorna el diccionari adient per obrir la fitxa de l'informe
        """
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context)

        return {
            "domain": "[('id', '=', %i)]" % wiz.informe_id.id,
            "name": 'Informe generat',
            "view_type": "form",
            "view_mode": "tree,form",
            "res_model": "giscedata.facturacio.informe_preus_consumidors",
            "type": "ir.actions.act_window"
        }

    _columns = {
        'state': fields.char('Estat', size=16, required=True),
        'informe_id': fields.many2one('giscedata.facturacio.informe_preus'
                                      '_consumidors', 'Informe'),
        'data_inici': fields.date('Data d\'inici'),
        'data_final': fields.date('Data final'),
        'f_corr': fields.float('Factor de correcció', digits=(3, 2)),
        'ext_anual': fields.boolean('Extrapolar el consum anual')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'f_corr': lambda *a: 1,
        'ext_anual': lambda *a: False,
    }


WizardInformePreusMitc()
