# -*- coding: utf-8 -*-
from __future__ import absolute_import
from tools.translate import _
from osv import osv, fields
from l10n_ES_remesas.mandato import MANDATE_SCHEME_SELECTION

from datetime import datetime, date


class WizardBankAccountChange(osv.osv_memory):

    def _get_default_pagador(self, cursor, uid, context=None):
        polissa = self.pool.get('giscedata.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        return polissa.pagador.id

    def _get_account_owner(self, cursor, uid, context=None):
        polissa = self.pool.get('giscedata.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        bank_obj = self.pool.get('res.partner.bank')

        model_extes = bank_obj.fields_get(
            cursor, uid, ['owner_id'], context=context
        )
        if model_extes and polissa.payment_mode_id.require_bank_account:
            account_owner_id = (
                (polissa.bank and polissa.bank.owner_id.id) or
                self._get_default_pagador(cursor, uid, context=context)
            )
        else:
            account_owner_id = self._get_default_pagador(
                cursor, uid, context=context
            )
        return account_owner_id

    def _get_default_pagador_address(self, cursor, uid, context=None):
        polissa = self.pool.get('giscedata.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        return polissa.direccio_pagament.id

    def _get_default_payment_mode(self, cursor, uid, context=None):
        polissa = self.pool.get('giscedata.polissa').browse(
            cursor, uid, context['active_id'], context=context
        )
        payment_mode = polissa.payment_mode_id
        return payment_mode.id if payment_mode.require_bank_account else False

    def action_bank_account_change(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        conf_obj = self.pool.get('res.config')
        polissa_obj = self.pool.get('giscedata.polissa')

        wiz = self.browse(cursor, uid, ids, context=context)[0]

        contract_modify_iban_modcon_type = conf_obj.get(
            cursor, uid, 'contract_modify_iban_modcon_type', 'new'
        )
        pol_state = polissa_obj.read(
            cursor, uid, context['active_id'], ['state'])
        esborrany = pol_state['state'] == 'esborrany'
        if contract_modify_iban_modcon_type == 'new' and not esborrany:
            polissa_id = context.get('active_id', False)
            igual = polissa_obj.check_data_inici_igual_modcon_activa(
                cursor, uid, polissa_id, wiz.change_date, context=context
            )[polissa_id]
            if igual:
                wiz.write({
                    'state': 'conf',
                    'info': _(u"Ja existeix una modificació contractual "
                              u"amb data {}. "
                              u"Vols actualitzar-la?".format(wiz.change_date))
                })
                res = True
            else:
                res = wiz.action_bank_account_change_confirm(context=context)
        else:
            res = wiz.action_bank_account_change_confirm(context=context)
        return res

    def action_bank_account_change_confirm(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids, context=context)[0]
        account_iban = wiz.account_iban.replace(" ", "")
        modcon_type = context.get('modcon_type', None)
        mandate_new_id = self.pool.get('giscedata.polissa').change_bank_account(
            cursor, uid, [context.get('active_id', False)], account_iban,
            wiz.change_date, wiz.pagador.id, wiz.mandate_scheme,
            wiz.account_owner, wiz.owner_address, wiz.payment_mode,
            modcon_type=modcon_type, context=context
        )
        context.update({
            'print': ['client', 'bank', 'company'],
            'address': wiz.owner_address.id
        })
        if wiz.print_mandate:
            return self.print_mandate(
                cursor, uid, [mandate_new_id], context=context
            )
        else:
            return {}

    def print_mandate(self, cursor, uid, ids, context=None):
        mandate_obj = self.pool.get('payment.mandate')
        return mandate_obj.print_mandate(
            cursor, uid, ids, context=context
        )

    def to_init(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'init'}, context=context)
        return True

    _name = 'wizard.bank.account.change'

    _columns = {
        'account_iban': fields.char(
            'Compte bancari', size=126, required=True
        ),
        'account_owner': fields.many2one(
            'res.partner', 'Propietari', required=True
        ),
        'owner_address': fields.many2one(
            'res.partner.address', 'Adreça', required=True
        ),
        'change_date': fields.date('Data de canvi', required=True),
        'pagador': fields.many2one('res.partner', 'Raó fiscal', readonly=True),
        'payment_mode': fields.many2one('payment.mode', 'Grup de pagament'),
        'print_mandate': fields.boolean('Imprimir mandat'),
        'mandate_scheme': fields.selection(
            MANDATE_SCHEME_SELECTION, 'Mandate schema', required=True,
        ),
        'state': fields.selection(
            [
                ('init', 'Init'), ('end', 'Final'), ('conf', 'Confirmar')
            ], 'Estat'
        ),
        'info': fields.text('Informació', readonly=True)
    }

    _defaults = {
        'pagador': _get_default_pagador,
        'mandate_scheme': lambda *a: 'core',
        'account_owner': _get_account_owner,
        'owner_address': _get_default_pagador_address,
        'change_date': lambda *a: date.today().strftime('%Y-%m-%d'),
        'payment_mode': _get_default_payment_mode,
        'state': 'init',
        'print_mandate': True
    }


WizardBankAccountChange()
