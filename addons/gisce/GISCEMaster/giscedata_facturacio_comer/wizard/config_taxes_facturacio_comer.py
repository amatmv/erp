# -*- coding: utf-8 -*-
from osv import fields,osv
import pooler

class config_taxes_facturacio_comer(osv.osv_memory):
    _name = 'config.taxes.facturacio.comer'
    
    # Definim les columnes, ho farem bastant 'cutre', només es podran escollir 3 impostos:
    # IVA venda, IESE i IVA Compra. Després nosaltres ja sabrem a quins productes s'han 
    # d'aplicar aquests impostos 
    _columns = {
        'iva_venda': fields.many2one('account.tax', 'IVA (venta)', domain=[('type_tax_use', '=', 'sale')]),
        'iese_venda': fields.many2one('account.tax', 'IESE', domain=[('type_tax_use', '=', 'sale')]),
        'iva_compra': fields.many2one('account.tax', 'IVA (compra)', domain=[('type_tax_use', '=', 'purchase')])
    }
    
    def action_set(self, cr, uid, ids, context=None):
        
        config = self.browse(cr, uid, ids[0], context)
        # Per cada periode de les tarifes hi apliquem aquests impostos
        tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
        product_obj = self.pool.get('product.product')
        tarifa_ids = tarifa_obj.search(cr, uid, [])
        tarifes_xml_ids = ['categ_t20A_new', 'categ_t20DHA_new', 'categ_t20DHS',
                           'categ_t20A', 'categ_t20DHA', 'categ_t21DHS',
                           'categ_t30A', 'categ_t31A', 'categ_t31A_LB',
                           'categ_t61', 'categ_t61a', 'categ_t61b', 'categ_t62',
                           'categ_t63', 'categ_t64', 'categ_t65']
        categ_ids = []
        for xml_id in tarifes_xml_ids:
            md_obj = pooler.get_pool(cr.dbname).get('ir.model.data')
            md_id = md_obj.search(cr, uid, [('module', '=', 'giscedata_polissa'),
                                            ('name', '=', xml_id)])
            categ_ids.append(md_obj.browse(cr, uid, md_id[0]).res_id)
        vals = {
            'taxes_id': [(6, 0, [config.iva_venda.id, config.iese_venda.id])],
            'supplier_taxes_id': [(6, 0, [config.iva_compra.id])]
        }
        search_params = [('categ_id', 'child_of', categ_ids)]
        productes_ids = product_obj.search(cr, uid, search_params)
        product_obj.write(cr, uid, productes_ids, vals)
                
        # Ara falta tots els lloguers de comptadors
        vals = {
            'taxes_id': [(6, 0, [config.iva_venda.id])],
            'supplier_taxes_id': [(6, 0, [config.iva_compra.id])]
        }
        # busquem l'id de (categ_alq_conta) child_of
        md_obj = pooler.get_pool(cr.dbname).get('ir.model.data')
        md_id = md_obj.search(cr, uid, [('module', '=', 'giscedata_lectures'), ('name', '=', 'categ_alq_conta')])
        categ_id = md_obj.browse(cr, uid, md_id[0]).res_id
        product_obj = pooler.get_pool(cr.dbname).get('product.product')
        pids = product_obj.search(cr, uid, [('categ_id', 'child_of', [categ_id])])
        # Escrivim a tots aquests productes 
        product_obj.write(cr, uid, pids, vals)
        return {
                'view_type': 'form',
                "view_mode": 'form',
                'res_model': 'ir.actions.configuration.wizard',
                'type': 'ir.actions.act_window',
                'target':'new',
         }
        
config_taxes_facturacio_comer()