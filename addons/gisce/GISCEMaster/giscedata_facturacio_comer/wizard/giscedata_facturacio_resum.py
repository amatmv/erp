# -*- coding: utf-8 -*-

from osv import osv

class WizardGiscedataFacturacioResum(osv.osv_memory):
    _name = 'wizard.giscedata.facturacio.resum'
    _inherit = 'wizard.giscedata.facturacio.resum'

    def db_origin(self):
        return 'C'

WizardGiscedataFacturacioResum()


