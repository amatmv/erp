# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools import config
from tools.translate import _
from giscedata_facturacio_comer.sql.sql_column_titles import *
from giscedata_facturacio.wizard.wizard_informes_facturacio import (
    MULTI_SEQUENCE, INFORMES_DESC, INFORMES_EXPORTABLES
)
import pandas as pd
from StringIO import StringIO
from base64 import b64encode

INFORMES_DESC.update(
    {
        'report_resumen_facturacion_comer_fecha':
        _(u"Resum de facturació per tarifes amb el desglosament dels consums "
          u"segons els periodes i energia.\nL'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_resumen_facturacion_comer_distribuidora_fecha':
        _(u"Resum de facturació per distribuïdora i tarifa amb el "
          u"desglosament dels consums segons els periodes i energia.\n"
          u"L'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_resumen_facturacion_comer_grupcobratori_fecha':
        _(u"Resum per grup de cobrament. Inclou el nombre de factures "
          u"i el total facturat"),

        'report_resumen_facturacion_comer_municipio_fecha':
        _(u"Resum de facturació per municipi i tarifa. Inclou el desglosament "
          u"dels consums segons els periodes i energia.\nL'informe inclou el "
          u"resum en kWh, potència total, euros facturats i nombre de clients "
          u"afectats"),

        'report_resumen_facturacion_comer_municipio_peaje_fecha':
        _(u"Resum de facturació per tarifes d'accés amb el desglosament dels "
          u"consums "
          u"segons els periodes i energia.\nL'informe inclou el resum en kWh, "
          u"potència total, euros facturats i nombre de clients afectats"),

        'report_resumen_facturacion_comer_cne_fecha':
        _(u"Resum de facturació per tarifes que inclou el detall de l'energia "
          u"facturada, potència facturada i potència contractada per periode"),

        'resumen_facturacion_comer_impuestos_distri_fecha':
        _(u"Resum de facturació per distribuïdora. Detalla base, impostos "
          u"i total en euros facturats." ),

        'detalle_facturacion_comer_otros_distri_fechas':
        _(u"Resum de facturació per distribuïdora que inclou únicament els "
          u"productes de tipus 'Altres'. El resum mostra els totals facturats."
          ),

        'export_factures_desglossat':
        _(u"Resum de les factures en CSV."),

        'export_factures_desglossat_excel':
        _(u"Resum de les factures en Excel."),

        'informe_facturacion_contabilidad_comer':
            _(
                u"Informe que contiene el resumen detallado de la "
                u"contabilidad de la comercializadora agrupado por listas "
                u"de precios"
            )
    }
)

INFORMES_CSV = {
    'export_factures_desglossat': {
         'header': QUERY_RESUM_COMER_TITLES,
         'query': ("%s/giscedata_facturacio_comer/sql/query_resum_comer.sql"
                    % config['addons_path'])
    },
    'export_factures_desglossat_excel': {
        'header': [
            _(u"Contracte"),
            _(u"CP"),
            _(u"Tipo Facturació"),
            _(u"Distribuïdora"),
            _(u"Tarifa peatge"),
            _(u"Tarifa comercial"),
            _(u"P P1"),
            _(u"P P2"),
            _(u"P P3"),
            _(u"P P4"),
            _(u"P P5"),
            _(u"P P6"),
            _(u"E P1 €"),
            _(u"E P1 kW"),
            _(u"E P2 €"),
            _(u"E P2 kW"),
            _(u"E P3 €"),
            _(u"E P3 kW"),
            _(u"E P4 €"),
            _(u"E P4 kW"),
            _(u"E P5 €"),
            _(u"E P5 kW"),
            _(u"E P6 €"),
            _(u"E P6 kW"),
        ],
        'query': ''
    }
}
MULTI_SEQUENCE.update({
    'report_resumen_facturacion_comer_fecha': True,
    'informe_facturacion_contabilidad_comer': True
})

INFORMES_EXPORTABLES.append('informe_facturacion_contabilidad_comer')


class WizardInformesFacturacioComer(osv.osv_memory):

    _name = "wizard.informes.facturacio"

    _inherit = "wizard.informes.facturacio"

    _informes_csv = INFORMES_CSV

    def _get_informe(self, cursor, uid, context=None):
        """ Com a funció perquè si no no tradueix"""
        _opcions = super(WizardInformesFacturacioComer, self)._get_informe(
            cursor, uid, context=context)

        _opcions += [
            ('report_resumen_facturacion_comer_fecha', _('Resum per tarifes')),
            ('report_resumen_facturacion_comer_distribuidora_fecha', _('Resum per distribuïdora i tarifa')),
            ('report_resumen_facturacion_comer_grupcobratori_fecha', _('Resum per grup cobratori')),
            ('report_resumen_facturacion_comer_municipio_fecha', _('Resum per municipi i tarifa')),
            ('report_resumen_facturacion_comer_municipio_peaje_fecha', _('Resum per municipi i tarifa d\'accés')),
            ('report_resumen_facturacion_comer_cne_fecha', _('Detall per període i tarifa de peatge')),
            ('resumen_facturacion_comer_impuestos_distri_fecha', _('Resum per impostos i distribuïdora')),
            ('detalle_facturacion_comer_otros_distri_fechas', _('Detall d\'altres conceptes')),
            ('export_factures_desglossat', _('Resum Factures (CSV)')),
            ('export_factures_desglossat_excel', _('Resum factures Excel')),
            ('informe_facturacion_contabilidad_comer',
             _('Informe de facturación de contabilidad agrupado por '
               'lista de precios')
             )
         ]

        return _opcions

    def exportar(self, cursor, uid, ids, context=None):
        """ Retorna el CSV sol·licitat """

        if not context:
            context = {}
        is_exported = super(WizardInformesFacturacioComer, self).exportar(
            cursor, uid, ids, context=context
        )

        if not is_exported:
            wizard = self.browse(cursor, uid, ids[0])
            vals = {}
            if wizard.informe == 'export_factures_desglossat' or \
               wizard.informe == 'export_factures_desglossat_excel':
                res, info = self.exportar_resum_factures(cursor, uid, ids,
                                                         context)
                vals = {
                    'file': res['csv'],
                    'estat': 'done',
                    'info': info,
                }

            elif wizard.informe == 'export_factures_desglossat_excel':
                vals.update({'name': _('ResumFactures.xls')})

            elif wizard.informe in ('informe_facturacion_contabilidad_comer',):
                df = self.get_df_generic_informe_contabilidad(
                    cursor, uid, ids, context=context
                )
                res = self.exportar_informe_contabilidad(
                    cursor, uid, ids, df, context=context
                )
                d_ini = wizard.data_inici
                d_fi = wizard.data_final

                f_name = 'Resumen_agrupado_por_precios_contabilidad_facturacion_{0}_{1}.csv'.format(d_ini, d_fi)
                vals = {
                    'file': res,
                    'estat': 'done',
                    'name': f_name
                }
            if vals:
                wizard.write(vals)

            return bool(vals)

        return is_exported

    def parse_sql_contabilidad_depends_of_whereim(self, cursor, uid, ids, query_str, context=None):

        select_desc_energia = """
        COALESCE(descuentos_energia.desc_ener, 0) * (
            CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END
        ) AS "Descuento energía",
        """

        select_desc_potencia = """
        COALESCE(descuentos_potencia.descuento, 0) * (
            CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END
        ) AS "Descuento potencia",
        """

        join_energia = """
        LEFT JOIN (
          SELECT lf.factura_id AS factura_id,SUM(li.quantity) AS energia
          FROM giscedata_facturacio_factura_linia lf
          LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
          WHERE lf.tipus='energia' AND lf.isdiscount = False
          GROUP BY lf.factura_id
        ) AS ene ON (ene.factura_id=f.id)
        """

        join_descuentos = """
        LEFT JOIN (
            SELECT lf.factura_id AS factura_id, SUM(li.price_subtotal) AS desc_ener
                FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE
                lf.tipus ='energia'
                AND lf.isdiscount
            GROUP BY lf.factura_id
        ) AS descuentos_energia ON (descuentos_energia.factura_id=f.id)

        LEFT JOIN (
            SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS descuento
                FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE
                lf.tipus ='potencia'
                AND lf.isdiscount
            GROUP BY lf.factura_id
        ) AS descuentos_potencia ON (descuentos_potencia.factura_id=f.id)
        """

        query_str = query_str.replace(
            '%(select_descuento_energia)s', select_desc_energia
        )
        query_str = query_str.replace(
            '%(select_descuento_potencia)s', select_desc_potencia
        )
        query_str = query_str.replace(
            '%(join_energia)s', join_energia
        )
        query_str = query_str.replace(
            '%(joins_descuentos)s', join_descuentos
        )

        return query_str

    def exportar_informe_contabilidad(self, cursor, uid, ids, df, context=None):
        res = super(
            WizardInformesFacturacioComer, self
        ).exportar_informe_contabilidad(
            cursor, uid, ids, df, context=context
        )

        if not res:
            informe = self.read(
                cursor, uid, ids[0], ['informe'], context=context
            )[0]['informe']

            if informe in ('informe_facturacion_contabilidad_comer',):

                df_count_clients = df[
                    ['Lista de precios', 'id_client', 'Tarifa de acceso']
                ].groupby(['Lista de precios', 'Tarifa de acceso'])['id_client'].apply(
                    lambda x: len(x.unique())
                ).reset_index().rename(index=str, columns={"id_client": "Numero de clientes"})

                df_count_facturas = df[
                    ['Lista de precios', 'id_invoice', 'Tarifa de acceso']
                ].groupby(['Lista de precios', 'Tarifa de acceso'])['id_invoice'].apply(
                    lambda x: len(x.unique())
                ).reset_index().rename(index=str, columns={"id_invoice": "Numero de facturas"})

                df_price_list_group = df.groupby(
                    ['Lista de precios', 'Tarifa de acceso']
                ).sum().reset_index()

                df_price_list_group = pd.merge(
                    df_price_list_group, df_count_clients,
                    how="inner", on=["Lista de precios", "Tarifa de acceso"]
                )
                df_price_list_group = pd.merge(
                    df_price_list_group, df_count_facturas,
                    how="inner", on=["Lista de precios", "Tarifa de acceso"]
                )

                cols = df_price_list_group.columns.tolist()

                col_n_clients = "Numero de clientes"
                col_n_factures = "Numero de facturas"

                # Get position after group
                new_position = df_price_list_group.columns.get_loc("Tarifa de acceso") + 1

                cols.insert(new_position, cols.pop(cols.index(col_n_factures)))
                cols.insert(new_position, cols.pop(cols.index(col_n_clients)))
                cols.pop(cols.index("id_client"))
                cols.pop(cols.index("id_invoice"))

                df_price_list_group = df_price_list_group[cols]

                gen_file = StringIO()

                df_price_list_group.to_csv(
                    gen_file, header=True, sep=';', encoding='utf-8-sig',
                    index=None, decimal=',', float_format='%.2f'
                )

                return b64encode(gen_file.getvalue())

        return res

    _columns = {
        'informe': fields.selection(_get_informe, 'Informe', required=True),
    }

WizardInformesFacturacioComer()
