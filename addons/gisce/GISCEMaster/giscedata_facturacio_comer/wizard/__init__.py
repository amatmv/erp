# -*- coding: utf-8 -*-
"""Wizards
"""
import config_taxes_facturacio_comer
import wizard_avis_impagament
import informe_preus_mitc_wizard
import giscedata_facturacio_resum
import informe_model159_wizard
import wizard_informes_facturacio_comer
import wizard_factures_per_email
import wizard_pricelist_report
import wizard_invoice_open_and_send
import wizard_payer_change
import wizard_avancar_facturacio
import pricelist_from_file
import wizard_bank_account_change
import wizard_generar_xml_606
