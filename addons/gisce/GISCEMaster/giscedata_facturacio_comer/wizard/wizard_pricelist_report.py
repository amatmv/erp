# -*- coding: utf-8 -*-

from osv import osv, fields
import time
from datetime import datetime


class WizardPricelistReportComer(osv.osv_memory):

    _name = 'wizard.pricelist.report'
    _inherit = 'wizard.pricelist.report'

    def action_print(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0])
        context.update({
            'tarifa': wizard.tarifa_id.id,
            'pricelist': wizard.pricelist_id.id,
            'date': wizard.price_date
        })
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.polissa',
            'report_name': 'report_preus_annex',
            'report_webkit': "'giscedata_facturacio/report/preus.mako'",
            'webkit_header': 'report_header_preus_annex',
            'groups_id': ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'ids': ids,
                'context': context
            }
        }

    _columns = {
        'pricelist_id': fields.many2one('product.pricelist',
                                        'Pricelist', required=False),
        'tarifa_id': fields.many2one('giscedata.polissa.tarifa',
                                     'Tarifa', required=False),
        'price_date': fields.date('Date', required=True),
    }

    _defaults = {
        'price_date': lambda *a: datetime.now().strftime('%Y-%m-%d')
    }

WizardPricelistReportComer()
