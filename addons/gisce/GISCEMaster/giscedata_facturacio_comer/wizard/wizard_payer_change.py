# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime, timedelta
import vatnumber
from tools.translate import _
import json


class WizardPayerChange(osv.osv_memory):

    _name = 'wizard.payer.change'

    def test_only_one_meter(self, cursor, uid, contract_id, date, context=None):
        """
        :param contract_id:
        :return: Returns True if only one meter on date or False otherwise
        """
        if context is None:
            context = {}

        if isinstance(contract_id, (tuple, list)):
            contract_id = contract_id[0]

        contract_obj = self.pool.get('giscedata.polissa')

        meter_id = contract_obj.get_comptador_data(
            cursor, uid,
            [contract_id], date,
            context=dict(context, multi=True)
        )
        if isinstance(meter_id, (tuple, list)) and len(meter_id) > 1:
            return False
        return True

    def get_lectures(self, cursor, uid, context=None):
        if context is None:
            context = {}

        contract_obj = self.pool.get('giscedata.polissa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        contract_id = context.get('active_id', False)

        if not contract_id:
            return None

        contract_fields = [
            'name', 'cups', 'direccio_pagament', 'data_ultima_lectura',
            'data_alta'
        ]
        contract_vals = contract_obj.read(
            cursor, uid, contract_id, contract_fields, context=context
        )
        last_invoiced_measure_date = contract_vals['data_ultima_lectura']
        contract_start_date = contract_vals['data_alta']
        ref_measure_date = last_invoiced_measure_date or contract_start_date

        only_one_meter = self.test_only_one_meter(
            cursor, uid, contract_id, ref_measure_date
        )

        if not only_one_meter:
            info = _(u"S'han trobat 2 comptadors o més pel contracte {0} "
                     u" el dia {1}."
                     u"").format(
                contract_vals['name'], ref_measure_date
            )
            raise osv.except_osv(_(u'ERROR'), info)

        # meter in last measure
        meter_id = contract_obj.get_comptador_data(
            cursor, uid,
            [contract_id], ref_measure_date,
            context=context
        )
        if not meter_id:
            info = _(u"No s'ha trobat cap comptador pel contracte {0} "
                     u" el dia {1}."
                     u"").format(
                contract_vals['name'], ref_measure_date
            )
            raise osv.except_osv(_(u'ERROR'), info)

        # tariff in last measure
        ctx = context.copy()
        ctx.update({'date': ref_measure_date})
        c_vals_date = contract_obj.read(
            cursor, uid, contract_id, ['tarifa'], context=ctx
        )
        tariff_id = c_vals_date['tarifa'][0]

        lectures = meter_obj.get_lectures(
            cursor, uid,
            meter_id, tariff_id, ref_measure_date, tipus='A',
            context=context
        )

        # Measures validation
        num_measures_found = len([l for l in lectures.values()
                                 if l['actual'].get('lectura', False)]
                                 )
        if num_measures_found < len(lectures.keys()):
            info = _(u"No s'ha trobat totes les lectures pel contracte {0} "
                     u" el dia {1}."
                     u"").format(contract_vals['name'], ref_measure_date)
            raise osv.except_osv(_(u'ERROR'), info)

        lectures_dict = dict([(p, l['actual']['lectura'])
                              for p, l in lectures.items()])

        return lectures_dict

    def get_contract_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        cups_obj = self.pool.get('giscedata.cups.ps')
        contract_obj = self.pool.get('giscedata.polissa')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')

        res = {}
        contract_id = context.get('active_id', False)

        if not contract_id:
            return None

        contract_fields = [
            'cups', 'pagador', 'data_ultima_lectura', 'tarifa', 'titular',
            'facturacio_potencia', 'name', 'bank', 'data_alta'
        ]
        contract_vals = contract_obj.read(
            cursor, uid, contract_id, contract_fields, context=context
        )
        last_invoiced_measure_date = contract_vals['data_ultima_lectura']

        cups_id = contract_vals['cups'][0]
        cups_vals = cups_obj.read(cursor, uid, cups_id, ['direccio'])

        tariff_id = contract_vals['tarifa'][0]
        num_periods = tariff_obj.get_num_periodes(
            cursor, uid, [tariff_id], context=context
        )

        res.update({
            'contract_id': contract_id,
            'contract_name': contract_vals['name'],
            'cups_address': cups_vals['direccio'],
            'current_payer_id': contract_vals['pagador'][0],
            'current_payer_name': contract_vals['pagador'][1],
            'bank_id': contract_vals['bank'][0],
            'bank_number': contract_vals['bank'][1],
            'last_read_date': last_invoiced_measure_date,
            'start_date': contract_vals['data_alta'],
            'num_periods': num_periods,
            'owner_id': contract_vals['titular'][0],
            'mcp': contract_vals['facturacio_potencia'],
            'tariff': tariff_id,
        })

        return res

    def _get_default_change_date(self, cursor, uid, context=None):
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        # if not last_read_date, takes contract start date
        date = c_vals['last_read_date'] or c_vals['start_date']
        last_date = datetime.strptime(date, '%Y-%m-%d')
        tomorrow = last_date + timedelta(days=1)

        return tomorrow.strftime('%Y-%m-%d')

    def _get_default_contract_id(self, cursor, uid, context=None):
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['contract_id']

    def _get_default_cups_address(self, cursor, uid, context=None):
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['cups_address']

    def _get_default_current_payer_id(self, cursor, uid, context=None):
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['current_payer_id']

    def _get_default_last_read_date(self, cursor, uid, context=None):
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)
        # if not last_read_date, takes contract start date
        date = c_vals['last_read_date'] or c_vals['start_date']

        return date

    def _get_default_num_periods(self, cursor, uid, context=None):
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['num_periods']

    def _get_default_p1_measure(self, cursor, uid, context=None):
        if context is None:
            context = {}

        l_vals = self.get_lectures(cursor, uid, context=context)

        return l_vals.get('P1', 0)

    def _get_default_p2_measure(self, cursor, uid, context=None):
        if context is None:
            context = {}

        l_vals = self.get_lectures(cursor, uid, context=context)

        return l_vals.get('P2', 0)

    def _get_default_p3_measure(self, cursor, uid, context=None):
        if context is None:
            context = {}

        l_vals = self.get_lectures(cursor, uid, context=context)

        return l_vals.get('P3', 0)

    def _get_default_mcp(self, cursor, uid, context=None):
        """get mcp from contract"""
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['mcp']

    def _get_default_payer(self, cursor, uid, context=None):
        """Default payer is contract owner"""
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['owner_id']

    def _get_default_bank(self, cursor, uid, context=None):
        """Default payer is contract owner"""
        if context is None:
            context = {}

        c_vals = self.get_contract_info(cursor, uid, context=context)

        return c_vals['bank_id']

    def _get_bank_data(self, cursor, uid, payer, context=None):
        """ get bank data of payer"""
        res = {'value': {}, 'domain': {}}

        partner_obj = self.pool.get('res.partner')

        if payer:
            partner_vals = partner_obj.read(
                cursor, uid, payer, ['bank_ids'], context=context
            )

            res['domain'] = {'bank_account_id': [('partner_id', '=', payer)]}

            if len(partner_vals['bank_ids']) == 1:
                bank_value = partner_vals['bank_ids'][0]
            else:
                bank_value = ''

            res['value'].update({'bank_account_id': bank_value, })

        return res

    def onchange_payer(self, cursor, uid, ids, payer, context=None):
        """ Search vat field on payer change"""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        partner_obj = self.pool.get('res.partner')

        if payer:
            partner_vals = partner_obj.read(
                cursor, uid, payer, ['vat'], context=context
            )

            res_bank = self._get_bank_data(cursor, uid, payer, context=context)
            res['domain'].update(res_bank.get('domain'))
            res['value'].update(res_bank.get('value'))

            res['value'].update({'vat': partner_vals['vat']})

        return res

    def onchange_vat(self, cursor, uid, ids, vat, context=None):
        """ Search the partner with the new VAT, if exists, and fills
            payer field and banck_account value/domain"""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        partner_obj = self.pool.get('res.partner')

        if not vat:
            return res

        if vat and not vat.startswith('ES'):
            vat = 'ES{0}'.format(vat.upper().zfill(9))

        if not vatnumber.check_vat(vat):
            res.update({
                'warning': {
                    'title': _(u'NIF/CIF incorrecte'),
                    'message': _(u'El document introduït no és vàlid'),
                }
            })
            return res

        partner_ids = partner_obj.search(cursor, uid, [('vat', '=', vat)])

        if partner_ids:
            partner_id = partner_ids[0]

            res_bank = self._get_bank_data(cursor, uid, partner_id, context)
            res['value'].update(res_bank.get('value', {}))
            res['domain'].update(res_bank.get('domain', {}))
            res['value'].update({'payer_id': partner_id, })
        else:
            res['value'].update(dict([('payer_id', '')]))

        return res

    def get_mandate(self, cursor, uid, ids, context=None):
        if context is None:
            return False
        cfg_obj = self.pool.get('res.config')
        mandate_obj = self.pool.get('payment.mandate')
        partner_obj = self.pool.get('res.partner')
        contract_obj = self.pool.get('giscedata.polissa')

        nc_vals = self.get_contract_info(cursor, uid, context=context)
        payer_id = nc_vals['current_payer_id']
        payer_vals = partner_obj.read(cursor, uid, payer_id, ['vat'])
        iban = nc_vals['bank_number'].replace(' ', '')

        # Mirar la variable de configuració que ens digui si hem de
        # tancar el(s) mandato(s) anterior(s) o si el deixem obert
        close_mandato = int(cfg_obj.get(cursor, uid,
                                        'payer_change_close_prev_mandate', '0'))
        if close_mandato:
            change_date = self.read(cursor, uid, ids[0], ['change_date'],
                                    context=context)[0]['change_date']
            search_params = [('reference', '=', 'giscedata.polissa,{0}'.format(
                     nc_vals['contract_id'])
                  ),
                 ('date_end', '=', False),
                 ]
            mandates = mandate_obj.search(cursor, uid, search_params)

            data_canvi = datetime.strptime(change_date, '%Y-%m-%d')
            dia_abans = data_canvi - timedelta(days=1)
            dia_abans_str = datetime.strftime(dia_abans, '%Y-%m-%d')

            mandate_obj.write(cursor, uid, mandates,
                              {'date_end': dia_abans_str})

        search_params = [('debtor_iban', 'ilike', iban),
                         ('debtor_vat', 'ilike', payer_vals['vat']),
                         ('date_end', '=', False),
                         ('reference', '=', 'giscedata.polissa,{0}'.format(
                             nc_vals['contract_id'])
                          ),
                         ]

        mandates = mandate_obj.search(cursor, uid, search_params)
        if not mandates:
            mandate_id = mandate_obj.create(
                cursor, uid, contract_obj.update_mandate(
                    cursor, uid, nc_vals['contract_id']
                )
            )
            if close_mandato:
                # posar com a data inicial del mandato la data del canvi
                mandate_obj.write(cursor, uid, mandate_id,
                                  {'date': change_date})
        else:
            mandate_id = mandates[0]

        return mandate_id

    def action_payer_change(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        cfg_obj = self.pool.get('res.config')
        contract_obj = self.pool.get('giscedata.polissa')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meas_origin_obj = self.pool.get('giscedata.lectures.origen')
        meas_retail_origin_obj = self.pool.get(
            'giscedata.lectures.origen_comer'
        )
        imd_obj = self.pool.get('ir.model.data')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        wiz_fields = ['num_periods', 'change_date', 'payer_id',
                      'measure_p1', 'measure_p2', 'measure_p3',
                      'insert_measures', 'bank_account_id', 'comments']
        wiz_vals = self.read(
            cursor, uid, ids[0], wiz_fields, context=context
        )[0]
        c_vals = self.get_contract_info(cursor, uid, context=context)
        # measures origins
        # self-measure (50)
        origin_id = meas_origin_obj.search(
            cursor, uid, [('codi', '=', '50')]
        )[0]
        origin = (origin_id, 'autolectura')
        # self-measure (AL)
        retail_origin_id = meas_retail_origin_obj.search(
            cursor, uid, [('codi', '=', 'AL')]
        )[0]
        retail_origin = (retail_origin_id, 'autolectura')

        change_date = wiz_vals['change_date']
        contract_id = c_vals['contract_id']
        payer_id = wiz_vals['payer_id']
        infos = []
        factura_ids = []

        # measure is in date before change
        measure_date = (datetime.strptime(change_date, '%Y-%m-%d')
                        - timedelta(days=1)).strftime('%Y-%m-%d')

        # only insert measures/invoice when:
        # * there's a measure inserted ;)
        # * icp
        # * measure_date is the last read invoiced
        insert_measures = (
            c_vals['last_read_date'] != measure_date
            and c_vals['mcp'] != 'max'
            and wiz_vals['insert_measures']
        )

        if c_vals['last_read_date'] == measure_date:
            info_tmpl = _(u"* ATENCIÓ! NO HEM CREAT LA FACTURA perquè la "
                          u"última lectura facturada ({0}) és el dia abans de "
                          u"la data de canvi ({1}) ")
            info = info_tmpl.format(c_vals['last_read_date'], measure_date)
            infos.append(info)

        if insert_measures:
            # insert measures on meter
            measures = []
            meter_id = contract_obj.get_comptador_data(
                cursor, uid, [contract_id], change_date, context=context
            )
            meter = (meter_id, 'meter')
            if not meter_id:
                info = _(u"No s'ha trobat cap comptador pel contracte {0} "
                         u" el dia {1}."
                         u"").format(c_vals['contract_name'], change_date)
                raise osv.except_osv(_(u'ERROR'), info)

            periods_product = tariff_obj.get_periodes(
                cursor, uid, c_vals['tariff'], 'te', context=context
            )

            comments = _(u'Canvi de raó fiscal')
            for p, v in periods_product.items():
                measure_val = wiz_vals['measure_{0}'.format(p.lower())]
                periode = (v, p)

                measure = {
                    'name': measure_date,
                    'comptador': meter,
                    'tipus': 'A',
                    'periode': periode,
                    'lectura': measure_val,
                    'origen_id': origin,
                    'origen_comer_id': retail_origin,
                    'observacions': comments,
                    'incidencia_id': False,
                }
                measures.append(measure)
            for in_pool in [False, True]:
                meter_obj.inserta_lectures(
                    cursor, uid, measures, [], pool=in_pool,
                    context=context
                )
            # Draft Invoice
            ctx = context.copy()
            journal_id = imd_obj.get_object_reference(
                cursor, uid,
                'giscedata_facturacio', 'facturacio_journal_energia'
            )[1]

            invoice_start_date = (
                datetime.strptime(c_vals['last_read_date'], '%Y-%m-%d')
                + timedelta(days=1)
            ).strftime('%Y-%m-%d')

            ctx.update({
                'sync': False,
                'factura_manual': True,
                'data_inici_factura': invoice_start_date,
                'data_final_factura': measure_date,
                'journal_id': journal_id,
            })

            factura_ids = facturador_obj.fact_via_lectures(
                cursor, uid, contract_id, False, context=ctx
            )
            if not factura_ids:
                info_tmpl = _(u"* No hem pogut crear la factura entre les dates"
                              u" {0} i {1} ")
                info = info_tmpl.format(
                    ctx['data_inici_factura'], ctx['data_final_factura']
                )
            else:
                # Sets date invoice as current date
                factura_obj.write(
                    cursor, uid, factura_ids,
                    {'date_invoice': datetime.today().strftime('%Y-%m-%d')}
                )
                info_tmpl = _(u"* S'ha creat la factura en esborrany (id = {0})"
                              u" des de {1} fins a {2}")
                info = info_tmpl.format(
                    factura_ids,
                    ctx['data_inici_factura'],
                    ctx['data_final_factura']
                )
            infos.append(info)

        # Contract Modification
        vals = {
            'pagador': payer_id,
            'bank': wiz_vals['bank_account_id'],
        }

        # Mirar la variable de configuració que ens digui si hem de
        # canviar la persona de notificació o no
        update_notif = int(cfg_obj.get(cursor, uid,
                                      'payer_change_update_notificacio', '1'))
        if update_notif:
            vals.update({
                'notificacio': 'pagador',
            })

        if c_vals['current_payer_id'] != payer_id:
            if c_vals['owner_id'] == payer_id:
                vals.update({'pagador_sel': 'titular'})
            else:
                vals.update({'pagador_sel': 'altre_p'})

            if update_notif:
                res = contract_obj.onchange_altre_pagador(
                    cursor, uid, ids, payer_id, 'pagador', context=context
                )
            else:
                res = contract_obj.onchange_altre_pagador(
                    cursor, uid, ids, payer_id, None, context=context
                )
            # modifies direccio_pagador and direccio_notificacio
            vals.update(res['value'])

        contract_obj.send_signal(cursor, uid, [contract_id], 'modcontractual')
        contract_obj.write(cursor, uid, contract_id, vals, context=context)
        # comments
        comments = [
            _(u"* Antic pagador '{0}' ({1}) amb compte '{2}'").format(
                c_vals['current_payer_name'],
                c_vals['current_payer_id'],
                c_vals['bank_number'],
            )
        ]
        infos.append(comments[0])

        if wiz_vals['comments']:
            comments.append(wiz_vals['comments'])
        comments.append('\n')

        contract_obj.update_observacions(
            cursor, uid, contract_id,
            '\n'.join(comments),
            _(u'Canvi de Pagador'),
        )

        # activem la modificacio contractual
        wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
        ctx = {'active_id': contract_id}
        params = {'duracio': 'actual', 'accio': 'nou'}

        wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mc = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
        res = wz_crear_mc_obj.onchange_duracio(cursor, uid,
                                               [wz_id], change_date,
                                               wiz_mc.duracio, ctx)
        if res.get('warning', False):
            info = _(u"Hem trobat un error amb les dates al crear la"
                     u" modificació contractual de la pólissa '%s' "
                     u". Data activació '%s'. %s: %s. "
                     u"AQUEST CANVI JA HA ESTAT ACTIVAT?"
                     u"" % (c_vals['contract_name'], change_date,
                            res['warning']['title'],
                            res['warning']['message'])
                     )
            contract_obj.send_signal(
                cursor, uid, [contract_id], 'undo_modcontractual'
            )

            raise osv.except_osv(_(u'ERROR'), info)

        wiz_mc.write({'data_final': res['value']['data_final'] or '',
                      'data_inici': change_date})
        wiz_mc.action_crear_contracte(ctx)

        # info
        nc_vals = self.get_contract_info(cursor, uid, context=context)
        infos.append(_(u"* Nou pagador '{0}' ({1}) amb compte '{2}'").format(
            nc_vals['current_payer_name'],
            nc_vals['current_payer_id'],
            nc_vals['bank_number'],
        ))

        # Mandate
        mandate_id = self.get_mandate(cursor, uid, ids, context=context)
        infos.append(_(u"* S'ha creat el mandato: {0}").format(mandate_id))

        res_vals = {'state': 'done', 'info': '\n'.join(infos),
                    'invoice_ids': json.dumps(factura_ids)}
        self.write(cursor, uid, [wiz_vals['id']], res_vals)

    def show_invoice(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context=context)
        factura_ids = json.loads(wizard.invoice_ids)
        return {
            'domain': [('id', 'in', factura_ids)],
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        # info
        'contract_id': fields.many2one('giscedata.polissa', string='Contracte',
                                       readonly=True),
        'cups_address': fields.char(string='Adreça CUPS', size=256,
                                    readonly=True),
        'current_payer_id': fields.many2one('res.partner', 'Raó fiscal actual',
                                            readonly=True),
        'last_read_date': fields.date('Data última lectura', readonly=True),
        # measures
        'change_date': fields.date('Data Canvi', required=True),
        'last_measure_p1': fields.integer('Lectura P1 Anterior', readonly=True),
        'last_measure_p2': fields.integer('Lectura P2 Anterior', readonly=True),
        'last_measure_p3': fields.integer('Lectura P3 Anterior', readonly=True),
        'insert_measures': fields.boolean('Introdueix les lectures de tall'),
        'measure_p1': fields.integer('Lectura P1'),
        'measure_p2': fields.integer('Lectura P2'),
        'measure_p3': fields.integer('Lectura P3'),
        'mcp': fields.char('Mode Control Potència', size=3),
        # payer change
        'vat': fields.char('Número de Document', size=11),
        'payer_id': fields.many2one(
            'res.partner', 'Nou Pagador/Persona Fiscal', required=True
        ),
        'bank_account_id': fields.many2one(
            'res.partner.bank', 'Compte Bancari', required=True,
            domain="[('partner_id', '=', payer_id)]"
        ),
        'invoice_ids': fields.text('Invoices'),
        # comments
        'comments': fields.text('Comentaris'),
        # support data
        'num_periods': fields.integer('Número de periodes', readonly=True),
        'state': fields.char('Estat', size=16, required=True),
        'info': fields.text('Info', readonly=True),
    }

    _defaults = {
        'contract_id': _get_default_contract_id,
        'cups_address': _get_default_cups_address,
        'current_payer_id': _get_default_current_payer_id,
        'last_read_date': _get_default_last_read_date,
        'change_date': _get_default_change_date,
        'insert_measures': lambda *a: False,
        'last_measure_p1': _get_default_p1_measure,
        'last_measure_p2': _get_default_p2_measure,
        'last_measure_p3': _get_default_p3_measure,
        'mcp': _get_default_mcp,
        'payer_id': _get_default_payer,
        'bank_account_id': _get_default_bank,

        'num_periods': _get_default_num_periods,
        'state': lambda *a: 'init',
    }

WizardPayerChange()
