# -*- coding: utf-8 -*-

"""
    Wizard model 159
"""

import datetime
import base64
import unicodedata

import pooler
from osv import fields, osv
from tools import config
from tools.translate import _

import informe_preus_mitc_wizard

# [Navarra, Viskaia, Àlaba, Guipúscoa]
INE_ESPECIAL = ['31', '48', '01', '20']

def strip_accents(s):
    """Retorna la cadena sense accents i manté les ç i ñ"""
    protegit = [u'ç', u'Ç', u'ñ', u'Ñ']
    _s = []
    for c in s:
        if c not in protegit:
            _s += [val for val in unicodedata.normalize('NFD', c)
                           if unicodedata.category(val) != 'Mn']
        else:
            _s.append(c)
    return ''.join(_s)

def calc_digit_control_ine(ine):
    """Retorna el dígit de control del codi ine
       font: http://www.xnoccio.com/es/341-codigos-ine-de-municipios/
    """
    A = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    B = [0, 3, 8, 2, 7, 4, 1, 5, 9, 6]
    C = [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
    
    s = C[int(ine[0])] + B[int(ine[1])] + A[int(ine[2])] \
        + C[int(ine[3])] + B[int(ine[4])]
    res =  0 if not s % 10 else 10 - s % 10
    return str(res)

def calc_iban(b_acc):
    """Retorna el codi IBAN
       b_acc: codi bancari + dues lletres de país + '00'
    """
    vals = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
            'Q','R','S','T','U','V','W','X','Y','Z');
    subs = ('10','11','12','13','14','15','16','17','18','19','20','21','22',
            '23','24','25',' 26','27','28','29','30','31','32','33','34','35');
    for i, v in enumerate(vals):
        b_acc = b_acc.replace(v, subs[i])
    return '%02i' % (98 - long(b_acc) % 97)

def select_unitat_mesura(quantitat):
    """ retorna el quocient i unitat a aplicar 
    """
    #kW
    if quantitat < 10000: 
        unitat = 'K'
        quocient = 1
    #MW
    elif quantitat < 10000000:
        unitat = 'M'
        quocient = 1000 
    #GW
    elif quantitat < 10000000000:
        unitat = 'G'
        quocient = 1000000
    #TW
    else:
        unitat = 'T'
        quocient = 1000000000
    return quocient, unitat

_fields_header = (
    # Camps que composen la primera línia de l'informe
    ('tipus', 1),
    ('d_model', 3),
    ('d_exercici', 4),
    ('d_nif', 9),
    ('rao', 40),
    ('suport', 1),
     # Contacte
    ('telf', 9),
    ('nom', 40),
    ('num', 13 ),
    ('compl', 1),
    ('subst', 1),
    ('num_ant', 13),
    ('num_tit', 9),
     # Import total
    ('signe', 1),
    ('entera', 15),
    ('decimal', 2),
     # Padding
    ('padding', 338),
)

_fields = (
    ('tipus', 1),
    ('d_model', 3),
    ('d_exercici', 4),
    ('d_nif', 9),
    ('t_nif', 9),
    ('r_nif', 9), 
    ('nom', 40), 
    # habitatge
    ('h_tipus', 5), 
    ('h_nom', 50),
    ('h_t_num', 3),
    ('h_num', 5),
    ('h_cal', 3),
    ('h_bloc', 3),
    ('h_port', 3),
    ('h_esc', 3),
    ('h_pis', 3),
    ('h_porta', 3), 
    ('h_compl', 40),
    ('h_loc', 30),
    ('h_mun', 30),
    ('h_ine_mun', 5),
    ('h_ine_prov', 2),
    ('h_cp', 5),
    # Banc
    ('b_tipus', 1),
    ('b_iban_pais', 2),
    ('b_iban_dc', 2),
    ('b_entitat', 4),
    ('b_suc', 4),
    ('b_dc', 2),
    ('b_num', 10),
    ('b_pais', 2),
    ('b_id', 15),
    ('polissa', 12), 
    # CUPS
    ('c_pais', 2),
    ('c_distri', 4), 
    ('c_num', 12),
    ('c_ch', 2),
    ('c_mes', 1),
    ('c_sub', 1),
    ('tipus_be', 1),
    ('situacio', 1),
    ('ref_cat', 20),
    # alta
    ('a_any', 4),
    ('a_mes', 2),
    ('a_dia', 2),
    # baixa
    ('b_any', 4),
    ('b_mes', 2),
    ('b_dia', 2),
    # consum facturat
    ('c_unitat', 1), 
    ('c_01', 4),
    ('c_t_01', 1), 
    ('c_02', 4),
    ('c_t_02', 1),
    ('c_03', 4),
    ('c_t_03', 1), 
    ('c_04', 4), 
    ('c_t_04', 1),
    ('c_05', 4),
    ('c_t_05', 1), 
    ('c_06', 4),
    ('c_t_06', 1),
    ('c_07', 4), 
    ('c_t_07', 1), 
    ('c_08', 4), 
    ('c_t_08', 1),
    ('c_09', 4), 
    ('c_t_09', 1), 
    ('c_10', 4), 
    ('c_t_10', 1), 
    ('c_11', 4),
    ('c_t_11', 1),
    ('c_12', 4), 
    ('c_t_12', 1), 
    # import total facturat
    ('i_signe', 1), 
    ('i_enter', 9), 
    ('i_decim', 2), 
    # potencia
    ('p_unitat', 1), 
    ('p_entera', 4),
    ('p_decimal', 2),
    # padding
    ('padding', 38)
)
    
_fields_header_keys = [x[0] for x in _fields_header]
_fields_keys = [x[0] for x in _fields]

_sqlfile = '%s/giscedata_facturacio_comer/sql/informe_model159_consums.sql'\
                                               %  config['addons_path']
_query = open(_sqlfile).read()


def _fields_header_size(name):
    """ Retorna el nombre de lletres del camp name en _fields_header
    """
    return _fields_header[_fields_header_keys.index(name)][1]


def _fields_size(name):
    """ Retorna el nombre de lletres del camp name en _fields
    """
    return _fields[_fields_keys.index(name)][1]


def get_consums_polissa(cursor, params_query):
    """ Retorna el la llista consum els consums de cada
        mes i el total facturat
    """
    cursor.execute(_query, params_query)
    res = cursor.fetchall()
    val = {}
    for i in res:
        val.update({i[0]: ((i[1:13]), i[13])})
    return val


class WizardInformeModel159(osv.osv_memory):
    """Wizard per llençar l'informe del model 159
    """
    _name = 'wizard.informe.model159'

    def action_generar_dades(self, cursor, uid, ids, context=None):
        """ Funció per generar l'informe
            sortida: crea un nou informe del mòdul giscedata.facturacio.
                     model159 i les seves línies en giscedata.facturacio.
                     model159.linia
            
            Per tal de passar el validador, en el cas de no existir la
            informació a la bd, s'assignen els següents camps per defecte:
            registre 2:
                NIF del titular (18): '12345678Z'
                codi postal (264): '12321'
                tipus de via (76): 'CL'
                tipus de numeració (131): 'NUM' o 'S/N'
                Data d'alta de contracte (367): 01-07-2009 en el cas de 
                                                ser anterior al 1930
        """
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        inf = self.pool.get('giscedata.facturacio.model159')
        lin = self.pool.get('giscedata.facturacio.model159.linia')
        head = self.pool.get('giscedata.facturacio.model159.header')
        user = self.pool.get('res.users')
        poli = self.pool.get('giscedata.polissa')
        db = pooler.get_db_only(cursor.dbname)
        wiz = self.browse(cursor, uid, ids, context)
        vals = {}
        data = wiz.data_final
        limit_dt = datetime.datetime.strptime(data, "%Y-%m-%d")
        data_inici = informe_preus_mitc_wizard.restar_any(data)
        inf_name = '%s - %s' % (data_inici, data)
        cr = db.cursor()
        try:
            informe = inf.create(cr, uid, {'name': inf_name})
            cr.commit()
        except:
            cr.rollback()
        finally:
            cr.close()
        vals.update({'informe_id': informe})
        for key in _fields:
            vals.update({key[0]: ''})
        vals['tipus'] = '2'
        vals['d_model'] = '159'
        vals['d_exercici'] = data.partition('-')[0]
        vals['d_nif'] = user.browse(cursor, uid, uid, context).\
                    company_id.partner_id.vat.strip()[-_fields_size('d_nif'):]
        err = []
        n_p_id = 0
        fact_total_anual = 0
        query = """
            select id from giscedata_polissa where (
                ((data_alta >= %(data_inici)s and data_alta <= %(data_fi)s)
                or
                (data_baixa > %(data_inici)s and data_baixa <= %(data_fi)s)
                or
                (data_alta < %(data_inici)s and (active is True or data_baixa > %(data_fi)s)))
                and state not in ('esborrany', 'validar', 'cancelada')
            )
        """
        cursor.execute(query, {'data_inici': data_inici, 'data_fi': data})
        _poli = cursor.fetchall()
        poli_id = [x[0] for x in _poli]
        _any = data.partition('-')[0]
        params_query = ()
        for i in range(12):
            params_query += ('%02d/%s' % (i+1, _any), data_inici, 
                                data)
        params_query += (_any, data_inici, data, tuple(poli_id))
        res = get_consums_polissa(cursor, params_query)
        for p_id in poli.browse(cursor, uid, poli_id, context):
            cr = db.cursor()
            try:
                p_nif = 'titular'
                if not p_id.titular:
                    raise osv.except_osv('Error',
                                     _("Polissa %d sense titular" % p_id.id))
                nif_t = p_id.titular.vat.strip()[-_fields_size('t_nif'):]\
                        if p_id.titular.vat else ''
                nif_p = p_id.pagador.vat.strip()[-_fields_size('t_nif'):]\
                        if p_id.pagador.vat else ''
                if nif_t != nif_p and nif_p:
                    p_nif = 'pagador'
                vals['t_nif'] = getattr(p_id, p_nif).vat.strip()\
                                [-_fields_size('t_nif'):] \
                                if getattr(p_id, p_nif).vat else '12345678Z'
                vals['r_nif'] = ''
                vals['nom'] = strip_accents(getattr(p_id, p_nif).name).upper()
                # Habitatge
                if not p_id.cups:
                    raise osv.except_osv('Error',
                                        _("Polissa %d sense CUPS" % p_id.id))
                vals['h_tipus'] = p_id.cups.catas_tv[:_fields_size('h_tipus')]\
                                              if p_id.cups.catas_tv else 'CL'
                vals['h_nom'] = strip_accents(p_id.cups.nv).upper() \
                                                    if p_id.cups.nv else ''
                try:
                    i_num = '%05d' % (int(p_id.cups.pnp) 
                                                if p_id.cups.pnp else 0)
                except ValueError:
                    i_num = '%05d' % 0
                vals['h_num'] = i_num
                vals['h_t_num'] = 'NUM' if int(i_num) > 0 else 'S/N'
                vals['h_cal'] = ''
                vals['h_bloc'] = ''
                vals['h_port'] = ''
                vals['h_esc'] = p_id.cups.es if p_id.cups.es else ''
                vals['h_pis'] = p_id.cups.pt if p_id.cups.pt else ''
                vals['h_porta'] = p_id.cups.pu if p_id.cups.pu else ''
                vals['h_compl'] = ''
                h_loc = strip_accents(p_id.cups.id_municipi.name).upper()
                vals['h_loc'] = h_loc
                vals['h_mun'] = h_loc
                ine = p_id.cups.id_municipi.ine
                #ine += calc_digit_control_ine(ine)
                vals['h_ine_mun'] = ine[:5]
                vals['h_ine_prov'] = ine[:2]
                vals['h_cp'] = p_id.cups.dp if p_id.cups.dp else '12321'
                # Banc
                b_compte = ''
                if p_id.bank.state == 'iban':
                    iban = p_id.bank.iban
                    # Solo 'ES' es aceptado como código país
                    if iban.startswith('ES'):
                        b_compte = iban[4:]
                        iban_pais = iban[0:2] if b_compte else ''
                        iban_dc = iban[2:4] if b_compte else ''
                else:
                    b_compte = p_id.bank.acc_number.replace(' ', '') \
                           if p_id.bank.acc_number else ''
                    iban_dc = (
                        calc_iban(b_compte+'ES'+'00')
                        if b_compte else ''
                    )
                    iban_pais = 'ES' if b_compte else ''

                if not b_compte:
                    iban_pais = ''
                    iban_dc = ''

                vals['b_tipus'] = 'A' if b_compte else 'O'
                vals['b_iban_pais'] = iban_pais
                vals['b_iban_dc'] = iban_dc
                vals['b_entitat'] = b_compte[:4]
                vals['b_suc'] = b_compte[4:8]
                vals['b_dc'] = b_compte[8:10]
                vals['b_num'] = b_compte[10:20]
                vals['b_pais'] = ''
                vals['b_id'] = ''
                if not p_id.name:
                    raise osv.except_osv('Error',
                                        _("Polissa %d sense nom" % p_id.id))
                vals['polissa'] = p_id.name
                # CUPS
                cups = p_id.cups.name.strip()
                vals['c_pais'] = cups[:2]
                vals['c_distri'] = cups[2:6]
                vals['c_num'] = cups[6:18]
                vals['c_ch'] = cups[18:20]
                vals['c_mes'] = cups[20:21]
                vals['c_sub'] = cups[21:22]
                vals['tipus_be'] = '2'
                vals['ref_cat'] = p_id.cups.ref_catastral \
                                         if p_id.cups.ref_catastral else ''
                if not vals['ref_cat']: 
                    situacio = '3'
                elif vals['h_ine_prov'] not in INE_ESPECIAL:
                    situacio = '1'
                else:
                    situacio = '2'
                vals['situacio'] = situacio
                # alta
                d = datetime.datetime.strptime(p_id.data_alta, "%Y-%m-%d")
                if d.year >= 1930:
                    vals['a_any'] = str(d.year)
                    vals['a_mes'] = '%02d' % d.month
                    vals['a_dia'] = '%02d' % d.day
                else:
                    # data d'inici del règim de lliure competència
                    vals['a_any'] = '2009'
                    vals['a_mes'] = '07'
                    vals['a_dia'] = '01'
                # baixa
                d = ''
                if p_id.data_baixa:
                    d = datetime.datetime.strptime(p_id.data_baixa, "%Y-%m-%d")
                    if d > limit_dt:
                        d = ''
                if d:
                    vals['b_any'] = str(d.year)
                    vals['b_mes'] = '%02d' % d.month
                    vals['b_dia'] = '%02d' % d.day
                else:
                    vals['b_any'] = '0' * 4
                    vals['b_mes'] = '0' * 2
                    vals['b_dia'] = '0' * 2
                # consum facturat
                consum = res[p_id.id][0]
                f_any = res[p_id.id][1]
                promig = sum(consum) / 12
                quocient, vals['c_unitat'] = select_unitat_mesura(promig)
                for i in range(len(consum)):
                    vals['c_%02d' % (i+1)] = '%04d' % (consum[i] / quocient)
                    vals['c_t_%02d' % (i+1)] = 'R'
                # import total facturat
                fact_total_anual += f_any
                v = str(round(float(f_any), 2)).split('.')
                vals['i_signe'] = 'N' if int(v[0]) < 0 else ''
                vals['i_enter'] = '%09d' % abs(int(v[0]))
                vals['i_decim'] = '%s' % v[1][:2] if len(v[1]) >= 2 \
                                                   else v[1] + '0'
                # potencia
                pot = p_id.potencia
                quocient, vals['p_unitat'] = select_unitat_mesura(pot)
                v = str(round(float(pot), 2)).split('.')
                vals['p_entera'] = '%04d' % (float(v[0]) / quocient)
                vals['p_decimal'] = '%s' % v[1][:2] if len(v[1]) >= 2\
                                                    else v[1] + '0'
                vals['padding'] = ''
                lin.create(cr, uid, vals)
                cr.commit()
            except osv.except_osv as e:
                err.append('> %s' % e.value)
                cr.rollback()
            except Exception as e:
                import traceback
                traceback.print_exc()
                err.append('> Polissa %d: %s' % (p_id.id, e))
                cr.rollback()
            finally:
                cr.close()
                n_p_id += 1
        # Header
        vals = {}
        vals['informe_id'] = informe
        company = user.browse(cursor, uid, uid, context).company_id.partner_id
        vals['d_nif'] = company.vat.strip()[-_fields_header_size('d_nif'):]
        vals['tipus'] = '1'
        vals['d_model'] = '159'
        vals['d_exercici'] = data.partition('-')[0]
        vals['num_tit'] = '%09d' % n_p_id
        vals['rao'] = strip_accents(company.name).upper()
        # Import total
        v = str(round(float(fact_total_anual), 2)).split('.')
        vals['signe'] = 'N' if int(v[0]) < 0 else ''
        vals['entera'] = '%015d' % abs(int(v[0]))
        vals['decimal'] = '%s' % v[1][:2] if len(v[1]) >= 2 else v[1] + '0'
        # Padding
        vals['padding'] = ''
        head.create(cursor, uid, vals)
        wiz.write({'state': 'done', 'informe_id': informe, 'err': '\n'.join(err)})

    def action_obrir_informe(self, cursor, uid, ids, context=None):
        """Retorna el diccionari adient per obrir la fitxa de l'informe
        """
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context)

        return {
            "domain": "[('id', '=', %i)]" % wiz.informe_id.id,
            "name": 'Informe generat',
            "view_type": "form",
            "view_mode": "tree,form",
            "res_model": "giscedata.facturacio.model159",
            "type": "ir.actions.act_window"
        }
    
    _columns = {
        'state': fields.char('Estat', size=16, required=True),
        'data_final': fields.date('Data final'),
        'informe_id': fields.many2one('giscedata.facturacio.model159.linia',
                                      'Informe'),
        'err': fields.text('LListat d\'errors'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardInformeModel159()


class WizardInformeModel159GenerarDocument(osv.osv_memory):
    """Wizard per generar el document a partir de les línies del model 159
    """
    _name = 'wizard.informe.model159.generar.document'

    def action_generar_document(self, cursor, uid, ids, context=None):
        """ Funció per retornar el document amb les línies de l'informe
        """
        inf = self.pool.get('giscedata.facturacio.model159')
        lin = self.pool.get('giscedata.facturacio.model159.linia')
        head = self.pool.get('giscedata.facturacio.model159.header')
        informe_id = context['active_id']
        wiz = self.browse(cursor, uid, ids[0], context)
        head_id = inf.read(cursor, uid, informe_id, ['header'], context)
        i_info = head.read(cursor, uid, head_id['header'][0], 
                                            ['d_exercici', 'd_nif'], context)
        # Header
        vals = {}
        vals['suport'] = wiz.suport
        # Contacte
        vals['telf'] = wiz.telf.replace(' ', '')
        w = WizardInformeModel159()
        vals['nom'] = strip_accents((u'%s %s' \
                                % (wiz.cognoms.strip(), wiz.nom.strip()))\
                                 [:_fields_header_size('nom')]).upper()
        vals['num'] = '%s%s%s%03d' % ('159', i_info['d_exercici'][-1:], 
                                          i_info['d_nif'][2:8], wiz.seq)

        head.write(cursor, uid, head_id['header'][0], vals)
        # Generar el fitxer 
        i_info = head.read(cursor, uid, head_id['header'][0],
                           _fields_header_keys, context)
        # Preparem dades de complementaries/suplementaries
        fill_l = dict(_fields_header)['num_ant']
        i_info['num_ant'] = (i_info['num_ant'] or '').ljust(fill_l, '0')
        l_txt = []
        for field in _fields_header:
            l_txt.append((i_info[field[0]] or '').upper().ljust(field[1]))
        l_txt.append('\n')
        lin_id = inf.read(cursor, uid, informe_id, ['linies', 'name'], context)
        l_info = lin.read(cursor, uid, lin_id['linies'], _fields_keys, context)
        for linia in l_info:
            for field in _fields:
                l_txt.append((linia[field[0]] or u'').ljust(field[1]))
            l_txt.append('\n')
        txt = ''.join(l_txt)
        filename = 'informe_model159_%s.txt' % lin_id['name'].partition('-')[0]
        _txt = txt.encode('iso-8859-1', 'replace')
        report = base64.b64encode(_txt)
        wiz.write({'name': filename, 'file': report, 'state': 'done'})

    _suport = [('C', 'DVD'), ('T', 'Transmissió telemàtica')]

    def _seq(self, cursor, uid, context=None):
        """ Retornar el número de seqüència segons l'històric d'informes
            generats
        """
        inf = self.pool.get('giscedata.facturacio.model159')
        informe_id = context['active_id']
        i_info = inf.read(cursor, uid, informe_id, ['name', 'num'])
        i_id = inf.search(cursor, uid, [('name', '!=', i_info['name'])], 
                          order='name')
        i_rec = inf.browse(cursor, uid, i_id, context)
        seq = 0
        for i in i_rec:
            try:
                num_inf = int(i.header[0].num[-3:] or 0)
            except:
                num_inf = 0
            seq = num_inf if num_inf > seq else seq
        seq += 1
        return seq

    _columns = {
        'file': fields.binary('Fitxer'),
        'state': fields.char('State', size=16),
        'name': fields.char('File name', size=64),
        'suport': fields.selection(_suport, 'Suport', required=True),
        'cognoms': fields.char('Cognoms', size=35, required=True),
        'nom': fields.char('Nom', size=15, required=True),
        'telf': fields.char('Telèfon', size=9, required=True),
        'seq': fields.integer('Número d\'informe', required=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'suport': lambda *a: 'T',
        'seq': _seq,
    }


WizardInformeModel159GenerarDocument()
