# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
import base64
from addons.giscedata_facturacio_comer.xml import MITC606


TIPUS = [('industrial', 'Industrial'), ('domestic', 'Doméstico')]
INTERVAL = [('semestral', 'Semestral'), ('anual', 'Anual')]


class WizardGenerarXML606(osv.osv_memory):

    def fetch_data_ml(self, cursor, uid, ids, context=None):
        informe_obj = self.pool.get(
            'giscedata.facturacio.informe_preus_consumidors'
        )
        informes_vals = informe_obj.read(
            cursor, uid, context['active_ids'], context=context
        )
        linia_obj = self.pool.get(
            'giscedata.facturacio.informe_preus_consumidors.linia'
        )
        res = {}
        for informe_vals in informes_vals:
            linies_vals = linia_obj.read(
                cursor, uid, informe_vals['linies'], context=context
            )
            domestics = []
            industrials = []
            for linia_vals in linies_vals:
                if linia_vals['consumidor'] == 'domestic':
                    domestics.append(linia_vals)
                elif linia_vals['consumidor'] == 'industrial':
                    industrials.append(linia_vals)
            res[informe_vals['name']] = {
                'domestics': domestics,
                'industrials': industrials
            }
        return res

    def fetch_data_domestico_anual(self, cursor, uid, ids, context=None):
        informe_obj = self.pool.get(
            'giscedata.facturacio.informe_preus_consumidors'
        )
        return informe_obj.get_data_domestico_anual(
            cursor, uid, context['active_ids'], context=context
        )

    def fetch_data_industrial_anual(self, cursor, uid, ids, context=None):
        informe_obj = self.pool.get(
            'giscedata.facturacio.informe_preus_consumidors'
        )
        return informe_obj.get_data_industrial_anual(
            cursor, uid, context['active_ids'], context=context
        )

    def fetch_data_empresa(self, cursor, uid, ids, context=None):
        users_obj = self.pool.get('res.users')
        address_obj = self.pool.get('res.partner.address')

        uids = uid
        if not isinstance(uids, list):
            uids = [uids]
        users_insts = users_obj.browse(cursor, uid, uids, context=context)

        res = {}
        for user_inst in users_insts:
            partner_inst = user_inst.company_id.partner_id
            address_id = partner_inst.address_get()['default']
            address_inst = address_obj.browse(
                cursor, uid, [address_id], context=context
            )[0]
            municipi = address_inst.id_municipi
            res[partner_inst.id] = {
                'country': partner_inst.country.code,
                'name': partner_inst.name,
                'cifnif': partner_inst.cifnif,
                'vat': partner_inst.vat,
                'street': address_inst.street,
                'zip': address_inst.zip,
                'state_id': address_inst.state_id.code,
                'id_municipi': "{}{}".format(municipi.ine, municipi.dc)[2:],
                'phone': address_inst.phone,
                'fax': address_inst.fax,
                'mobile': address_inst.mobile,
                'email': address_inst.email
            }
        return res

    def action_generar_xml(self, cursor, uid, ids, context=None):

        linies_domestiques_vals = {}
        linies_industrials_vals = {}
        wiz_inst = self.browse(cursor, uid, ids, context=context)[0]

        if wiz_inst.interval == 'semestral':
            ml_data = self.fetch_data_ml(cursor, uid, ids, context=context)
            for informe_id, value in ml_data.items():
                linies_domestiques_vals.update({informe_id: value['domestics']})
                linies_industrials_vals.update({informe_id: value['industrials']})

            if wiz_inst.tipus == 'domestic':
                linies_vals = linies_domestiques_vals
                xml = MITC606.MITC606DomesticoML()
            else:
                linies_vals = linies_industrials_vals
                xml = MITC606.MITC606IndustrialML()
        else:
            if wiz_inst.tipus == 'domestic':
                linies_vals = {'tmp': self.fetch_data_domestico_anual(
                    cursor, uid, ids, context=context
                )}
                xml = MITC606.MITC606DomesticoAnual()
            else:
                linies_vals = {'tmp': self.fetch_data_industrial_anual(
                    cursor, uid, ids, context=context
                )}
                xml = MITC606.MITC606IndustrialAnual()

        for tmp, bandas in linies_vals.items():
            for banda in bandas:
                for field, value in banda.items():
                    if isinstance(value, float):
                        value_str = '{0:.4f}'.format(value)
                        banda[field] = value_str.replace('.', ',')

        xml.set(
            self.fetch_data_empresa(cursor, uid, ids, context=context),
            linies_vals
        )
        xml.build_tree()

        fitxer = base64.b64encode(str(xml))

        params = {
            'fitxer': fitxer
        }
        self.write(cursor, uid, ids, params, context=context)
        return True

    _name = 'wizard.generar.xml.606'

    _columns = {
        'fitxer': fields.binary('Fitxer'),
        'tipus': fields.selection(TIPUS, 'Tipus', required=True),
        'interval': fields.selection(INTERVAL, 'Interval', required=True)
    }


WizardGenerarXML606()
