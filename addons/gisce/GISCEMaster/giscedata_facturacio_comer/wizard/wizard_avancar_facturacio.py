# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from oorq.oorq import AsyncMode


class WizardAvancarFacturacio(osv.osv_memory):

    _name = 'wizard.avancar.facturacio'

    def get_default_polissa_id(self, cursor, uid, context=None):
        """Llegim la pólissa"""
        if context is None:
            return False
        else:
            return context.get('active_id', False)

    def get_default_data_inici(self, cursor, uid, polissa_id, context=None):
        """Retorna la data d'última lectura de la pòlissa i en cas de no
        tenir-ne la data d'alta"""
        if context is None:
            context = {}

        polisses_obj = self.pool.get('giscedata.polissa')
        polissa_data = polisses_obj.read(
            cursor, uid, polissa_id, ['data_ultima_lectura', 'data_alta'])
        data_inici_facturacio = (
            polissa_data['data_ultima_lectura'] or polissa_data['data_alta']
        )

        data_inici_obj = datetime.strptime(
            data_inici_facturacio, '%Y-%m-%d'
        ).date() + relativedelta(days=1)
        data_inici = data_inici_obj.strftime('%Y-%m-%d')

        return data_inici

    def get_default_data_ultima_lectura(self, cursor, uid, polissa_id,
                                        context=None):
        """Retorna la data d'última lectura de la pòlissa"""
        if context is None:
            context = {}

        polisses_obj = self.pool.get('giscedata.polissa')
        polissa_data = polisses_obj.read(
            cursor, uid, polissa_id, ['data_ultima_lectura'])
        data_ultima_lectura = polissa_data['data_ultima_lectura']

        return data_ultima_lectura

    def get_default_journal_id(self, cursor, uid, context=None):
        """Retorna el journal d'energia"""
        if context is None:
            context = {}

        journal_obj = self.pool.get('account.journal')
        journal_ids = journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )

        if journal_ids:
            return journal_ids[0]
        return False

    def get_data_final_ultima_factura_client(self, cursor, uid, polissa_id,
                                             context=None):
        """Obté la data de l'última factura client de la pòlissa"""
        if context is None:
            context = {}

        factures_obj = self.pool.get('giscedata.facturacio.factura')
        factura_id = factures_obj.search(
            cursor, uid,
            [('polissa_id', '=', polissa_id), ('type', '=', 'out_invoice')],
            order='data_final desc', limit=1
        )[0]
        factura_data = factures_obj.read(
            cursor, uid, factura_id, ['data_final'])
        data_final = factura_data['data_final']

        return data_final

    def default_get(self, cursor, uid, fields_list, context=None):
        if context is None:
            context = {}
        res = super(WizardAvancarFacturacio, self).default_get(
            cursor, uid, fields_list, context
        )

        polissa_id = self.get_default_polissa_id(cursor, uid, context)
        data_inici = self.get_default_data_inici(
            cursor, uid, polissa_id, context)
        data_ultima_lectura = self.get_default_data_ultima_lectura(
            cursor, uid, polissa_id, context)
        journal_id = self.get_default_journal_id(cursor, uid, context)
        data_ultima_factura = self.get_data_final_ultima_factura_client(
            cursor, uid, polissa_id, context)

        warning_text = ''
        if data_ultima_factura > data_inici:
            warning_text = _('\n\nWARNING: Existeix una factura amb data final '
                             'posterior a la data de l\'última lectura '
                             'facturada')

        info = _('Aquest assistent avança la facturació de la pòlissa al lot '
                 'actual si aquesta està endarrerida{0}').format(warning_text)

        res.update({
            'polissa_id': polissa_id,
            'data_inici': data_inici,
            'data_ultima_lectura_original': data_ultima_lectura,
            'state': 'init',
            'journal_id': journal_id,
            'info': info
        })

        return res

    def facturar(self, cursor, uid, polissa_id, data_factura, context=None):
        """Genera factura de la pòlissa"""
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_ids = facturador_obj.fact_via_lectures(
            cursor, uid, polissa_id, lot_id=False, context=context)
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        if data_factura:
            vals = {
                'date_invoice': data_factura
            }
            payment_term = factura_obj.read(
                cursor, uid, factura_ids, ['partner_id', 'payment_term']
            )[0]['payment_term']
            if payment_term:
                payment_term_id = payment_term[0]
                date_due_res = factura_obj.onchange_payment_term_date_invoice(
                    cursor, uid, [], payment_term_id, data_factura
                )
                date_due = date_due_res.get('value', {})

                if date_due:
                    vals.update({
                        'date_due': date_due['date_due']
                    })

            factura_obj.write(cursor, uid, factura_ids, vals)

        return factura_ids

    def validar_facturacio(self, cursor, uid, polissa_id, data_inici,
                           context=None):
        """Valida si es pot facturar
        Si la lectura actual és posterior a la data_inici
        """
        if context is None:
            context = {}
        res = {'facturacio_valida': False}
        polisses_obj = self.pool.get('giscedata.polissa')
        polissa = polisses_obj.browse(cursor, uid, polissa_id)
        compt = [
            c for c in polissa.comptadors if c.active
        ][0]
        lectures = compt.get_lectures_per_facturar(
            polissa.tarifa.id, context=context)
        if lectures and lectures.values():
            valors = lectures.values()[0]['actual']
            if 'name' in valors.keys():
                data_lectura_actual = valors['name']
                # Si la lectura és posterior o igual a la data d'inici de
                # facturació es pot facturar
                if data_inici < data_lectura_actual:
                    res.update({
                        'data_fi': data_lectura_actual,
                        'facturacio_valida': True
                    })

        return res

    def actualitzar_polissa_lot_actual(self, cursor, uid, polissa_id,
                                       context=None):
        """Actualitza el lot de facturació de la pòlissa al lot actual"""
        res = {}
        lot_fact = datetime.now().strftime('%m/%Y')
        lot_fact_obj = self.pool.get('giscedata.facturacio.lot')
        lot_facturacio_id = lot_fact_obj.search(
            cursor, uid, [('name', '=', lot_fact)])
        if not lot_facturacio_id:
            raise osv.except_osv(
                _('Atenció'),
                _(u'El lot de facturació {} no existeix.').format(lot_fact)
            )

        polisses_obj = self.pool.get('giscedata.polissa')
        polisses_obj.write(
            cursor, uid, polissa_id, {
                'lot_facturacio': lot_facturacio_id[0]
            }
        )

        res['info'] = _('La pòlissa {0} s\'ha actualitzat al lot de '
                        'facturació actual \'{1}\'').format(
            polissa_id, lot_fact
        )

        return res

    def action_generar_factura(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])
        polissa_id = wizard.polissa_id.id

        data_inici = wizard.data_inici

        # Utilitzem de data ultima lectura temporal la data d'inici de la
        # factura menys un dia
        data_ultima_lectura_provisional = (datetime.strptime(
            data_inici, '%Y-%m-%d'
        ).date() - relativedelta(days=1)).strftime('%Y-%m-%d')

        # Carreguem les lectures
        validator_obj = self.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        warn_obj = self.pool.get('giscedata.facturacio.validation.warning')
        conf_obj = self.pool.get('res.config')
        polisses_obj = self.pool.get('giscedata.polissa')
        comptador_ids = polisses_obj.get_comptadors(
            cursor, uid, wizard.polissa_id.id, context)
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        # Obtenim la data límit per carregar la següent lectura
        max_dies_seg_lect = int(conf_obj.get(
            cursor, uid,
            'pool_llindar_carrega_lectures_avancar_facturacio', '1000'))

        data_limit_lectura = (datetime.strptime(
            data_ultima_lectura_provisional, '%Y-%m-%d'
        ) + relativedelta(days=max_dies_seg_lect)).strftime('%Y-%m-%d')
        with AsyncMode('sync') as asmode:
            # Carreguem lectures de pool del comptador a facturació fins la
            # data límit de la següent lectura
            ctx = context.copy()
            ctx.update({
                'allow_new_measures': True,
                'data_ultima_lectura': data_ultima_lectura_provisional,
                'llindar_estimacio': max_dies_seg_lect
            })
            carrega_lectures = comptador_obj.get_lectures_from_pool(
                cursor, uid, comptador_ids, data_limit_lectura, context=ctx
            )

        vals = {}
        ctx = context.copy()
        ctx.update({'ult_lectura_fact': data_ultima_lectura_provisional})
        # Validem si es pot facturar
        facturacio_valida = self.validar_facturacio(
            cursor, uid, polissa_id, data_inici, context=ctx)

        if facturacio_valida['facturacio_valida']:
            data_fi = facturacio_valida['data_fi']
            ctx.update({'sync': False,
                        'factura_manual': True,
                        'data_inici_factura': data_inici,
                        'data_final_factura': data_fi,
                        'journal_id': wizard.journal_id.id})
            factura_id = self.facturar(
                cursor, uid, polissa_id, wizard.data_factura, context=ctx
            )[0]
            warning_ids = validator_obj.validate_invoice(
                cursor, uid, factura_id, context=ctx
            )
            warning_vals = []
            if warning_ids:
                vals.update({'with_warnings': True})
                warning_vals = warn_obj.read(
                    cursor, uid, warning_ids,
                    ['name', 'message']
                )

            factures_obj = self.pool.get('giscedata.facturacio.factura')
            data_final_factura = factures_obj.read(
                cursor, uid, factura_id, ['data_final'])['data_final']

            # La data d'inici de la següent factura serà la data final de la
            # factura més un dia
            nova_data_inici = (datetime.strptime(
                data_final_factura, '%Y-%m-%d'
            ).date() + relativedelta(days=1)).strftime('%Y-%m-%d')
            info = _(u"Factura entre {} i {} generada correctament.\n"
                     u"Warnings:\n").format(data_inici, data_fi)
            for warning in warning_vals:
                info += '\t{}: {}\n'.format(warning['name'], warning['message'])
            vals.update({
                'data_inici': nova_data_inici,
                'info': info
            })
            nova_data_fi = datetime.strptime(
                nova_data_inici, '%Y-%m-%d'
            ).date() + relativedelta(months=1) - relativedelta(days=1)
            # Si la data de fi de la nova factura és superior a la data actual
            # actualitzem la pòlissa al lot de facturació actual
            if nova_data_fi > date.today():
                actualitzar_polissa = self.actualitzar_polissa_lot_actual(
                    cursor, uid, polissa_id, context)
                nova_info = _('{0} \n\n {1}').format(
                    vals['info'], actualitzar_polissa['info']
                )
                vals.update({
                    'state': 'end',
                    'info': nova_info
                })
        else:
            info_aux = ''
            for comptador_id in comptador_ids:
                info_aux += carrega_lectures['comptadors'][comptador_id] + '\n'
            vals.update({
                'info': _('No s\'ha pogut facturar\n') + info_aux,
                'state': 'error'
            })

        wizard.write(vals)

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Pòlissa', required=True),
        'data_inici': fields.date(
            'Data d\'inici', required=True),
        'data_factura': fields.date('Data de la factura', required=True),
        'data_ultima_lectura_original': fields.date(
            'Data de l\'última lectura facturada original'),
        'state': fields.selection([('init', 'Inici'),
                                   ('end', 'Final'),
                                   ('error', 'Error')],
                                  'Estat'),
        'journal_id': fields.many2one(
            'account.journal', 'Diari', required=True),
        'info': fields.text('Informació'),
        'with_warnings': fields.boolean('Warnings', readonly=True)
    }

    _defaults = {
        'data_factura': lambda *a: date.today().strftime('%Y-%m-%d')
    }


WizardAvancarFacturacio()
