# -*- coding: utf-8 -*-

import csv
import base64
import StringIO
import json
from datetime import datetime
from datetime import timedelta

from osv import osv
from osv import fields
from tools.translate import _

class WizardPricelistFromFile(osv.osv_memory):
    '''Pujar un fitxer csv per crear una llista de preus.
    Cada línia ha d'acabar en '\n' i els camps han d'estar separats per
    punt i coma.

    Cada línia ha de tenir el format següent:

    tarifes compatibles separades per coma;
    nom de la llista de preus; preu P1 energia;...;[Preu P1 potencia; ...]

    tarifa_compatible1[,tarifa_compatible2,tarifa_compatible3...];
    nom de la llista de preus;
    preu P1[;preu P2; preu P3]...,

    Exemple:

    Línia amb preus de potència:
    2.0A;1601 20A 0550 ME;0,126162;42.043426

    Una sense preus de potència:
    3.1A,3.1A LB;1608 31A 1500 CM;0.123248;0.104112;0.705466

    La mateixa però amb preus de potència
    3.1A,3.1A LB;1608 31A 1500 CM;0.123248;0.104112;0.705466;50;45;35

    '''

    _name = 'wizard.pricelist.from.file'

    def _pricelist_type_get(self, cr, uid, context={}):
        pricelist_type_obj = self.pool.get('product.pricelist.type')
        pricelist_type_ids = pricelist_type_obj.search(cr, uid, [], order='name')
        pricelist_types = pricelist_type_obj.read(cr, uid, pricelist_type_ids, ['key','name'], context=context)

        res = []

        for type in pricelist_types:
            res.append((type['key'],type['name']))

        return res

    def get_base_pricelist(self, cr, uid, ids, context=None):
        imd_obj = self.pool.get('ir.model.data')
        peatge_pl = imd_obj.get_object_reference(cr, uid,
            'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]
        return peatge_pl

    def create_item(self, cr, uid, vals):
        prod_list_item_obj = self.pool.get('product.pricelist.item')
        prod_list_item_obj.create(cr, uid, vals)
        return True

    def item_exists(self, cr, uid, product_id, version_id):
        prod_list_item_obj = self.pool.get('product.pricelist.item')
        it_ids = prod_list_item_obj.search(cr, uid, [
            ('product_id', '=', product_id), ('price_version_id', '=', version_id)
        ])
        return len(it_ids) != 0

    def get_max_sequence(self, cr, uid, version_id):
        """
        Return the maximum sequence of the elements inside this version
        ignoring secuences 99, 999 and 999 that are usually related to the
        item 'others'
        """
        prod_list_item_obj = self.pool.get('product.pricelist.item')
        it_ids = prod_list_item_obj.search(
            cr, uid, [
                ('price_version_id', '=', version_id),
                ('sequence', 'not in', [99, 999, 9999])
            ], order="sequence DESC"
        )
        if not len(it_ids):
            return 0
        return prod_list_item_obj.read(cr, uid, it_ids[0], ['sequence'])['sequence']

    def action_upload_pricelist(self, cr, uid, ids, context=None):

        prod_list_obj = self.pool.get('product.pricelist')
        prod_list_v_obj = self.pool.get('product.pricelist.version')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        period_obj = self.pool.get('giscedata.polissa.tarifa.periodes')

        wizard = self.browse(cr, uid, ids[0])
        based_on_pricelist_id = False
        fitxer = StringIO.StringIO(base64.b64decode(wizard.pricelist_file))
        reader = csv.reader(fitxer, delimiter=';')
        sequences = {}
        atrid = self.pool.get("ir.model.data").get_object_reference(
            cr, uid, "giscedata_facturacio_comer", "mode_facturacio_atr"
        )[1]
        mode_atr = self.pool.get("giscedata.polissa.mode.facturacio").browse(cr, uid, atrid)

        ptype = wizard.pricelist_type
        list_ids = []

        for row in reader:

            if not row:
                self.write(cr, uid, ids,
                           {'state': 'end',
                            })
                return True

            resum = self.read(cr, uid, ids[0])[0]['resum']

            # mirem que tinguem les tarifes d'accés indicades
            tarifes_acces_compatibles = row[0]
            tarifes_compatibles = []
            for tarifa_name in tarifes_acces_compatibles.split(','):
                param = [('name', '=', tarifa_name)]
                tarifa_id = tarifa_obj.search(cr, uid, param)
                if len(tarifa_id) == 1:
                    tarifa = tarifa_obj.browse(cr, uid, tarifa_id[0])
                    tarifes_compatibles.append(tarifa)
                else:
                    self.write(cr, uid, ids,
                               {'error': _(u'Tariff not found: {0}\n'
                                           u'No pricelist was created.').format(
                                   tarifa_name
                               ),
                                'state': 'error',
                                })
                    return False

            # mirem si la llista de preus existeix
            list_name = row[1]
            search_params = [('type', '=', ptype),
                             ('name', '=', list_name)]
            list_id = prod_list_obj.search(cr, uid, search_params)

            if list_id:
                list_id = list_id[0]
                resum += _('\nTariff: {0}, pricelist: {1} exists').\
                    format(tarifa_name,
                           list_name
                           )

            # si no existeix la creem
            else:
                list_id = prod_list_obj.create(cr, uid, {
                    'name': list_name,
                    'type': ptype,
                    'visible_discount': True
                })
                resum += _('\nTariff: {0}, pricelist: {1} created').\
                    format(tarifa_name,
                           list_name
                           )

            list_ids.append(list_id)
            if ptype == 'sale':
                # i la fem compatible amb les tarifes indicades
                for tar in tarifes_compatibles:
                    if list_id not in [x.id for x in tar.llistes_preus_comptatibles]:
                        # 4 és l'id de la tarifa de preus d'electricitat
                        tar.write({'llistes_preus_comptatibles': [(4, list_id), ]})
                # les fem compatibles mab la facturacio atr
                if list_id not in [x.id for x in mode_atr.compatible_price_lists]:
                    mode_atr.write({'compatible_price_lists': [(4, list_id), ]})

            # Intentem obtenir la llista de preus en la que ens basem si n'hi ha
            based_on_pricelist_id = False
            if row[2]:
                based_on_pricelist_id = prod_list_obj.search(cr, uid, [('name', '=', row[2])])
                if len(based_on_pricelist_id):
                    based_on_pricelist_id = based_on_pricelist_id[0]
                else:
                    self.write(cr, uid, ids,
                               {'error': _(u'Base Tariff not found: {0}\n'
                                           u'No pricelist was created.').format(
                                   row[2]
                               ),
                                   'state': 'error',
                               })
                    return False

            # sempre intentem crear una nova versió de preus
            version_date_start = wizard.data_inici
            version_date_end = wizard.data_final
            v_name = '{0} {1}'.format(list_name, version_date_start)
            search_params = [('pricelist_id', '=', list_id),
                             ('name', '=', v_name),
                             ('date_start', '=', version_date_start)]

            version_id = prod_list_v_obj.search(cr, uid, search_params)

            if version_id:
                if isinstance(version_id, (list, tuple)):
                    version_id = version_id[0]
                # ja existeix una versió de preus que comença en la mateixa
                # data, no la creem
                resum += _('\nA pricelist version already exists for '
                           'pricelist {0} with start date {1}. '
                           'Pricelist version was not created.').\
                    format(list_name, version_date_start)
                self.write(cr, uid, ids, {'resum': resum})
                sequences[version_id] = self.get_max_sequence(cr, uid, version_id) + 1
            else:

                # hem de tancar la versió anterior, si n'hi ha,
                # amb data 1 dia abans de la data inici de la nova
                params = [('pricelist_id', '=', list_id),
                                 ('date_start', '<=', wizard.data_inici),
                                 ('date_end' ,'>=', wizard.data_inici)]
                params_o = [('pricelist_id', '=', list_id),
                            ('date_start', '<=', wizard.data_inici),
                            ('date_end', '=', False)]

                version_existent = prod_list_v_obj.search(cr, uid, params)
                if not version_existent:
                    version_existent = prod_list_v_obj.search(cr, uid, params_o)

                if version_existent:

                    name_existent = prod_list_v_obj.read(
                        cr, uid, version_existent, ['name']
                    )[0]['name']

                    inici = datetime.strptime(wizard.data_inici, '%Y-%m-%d')
                    dia_abans = inici - timedelta(days=1)
                    dia_abans_str = dia_abans.strftime('%Y-%m-%d')

                    prod_list_v_obj.write(cr, uid, version_existent,
                                          {'date_end': dia_abans_str})
                    resum += '\nCerrada la versión de precios {0} a fecha {1}'.\
                        format(
                        name_existent,
                        dia_abans_str
                    )

                # un cop tancada l'anterior (si calia), creem la nova
                version_id = prod_list_v_obj.create(cr, uid,
                    {'name': v_name,
                     'pricelist_id': list_id,
                     'date_start': version_date_start,
                     'date_end': version_date_end,
                     }
                )
                if isinstance(version_id, (list, tuple)):
                    version_id = version_id[0]
                vals = {
                    'name': 'Otros',
                    'sequence': 9999,
                    'price_version_id': version_id,
                    'base_pricelist_id': self.get_base_pricelist(cr, uid, ids)
                }

                # creem la regla de tarifa 'otros'
                self.create_item(cr, uid, vals)

            # busquem el num de periodes d'energia de la tarifa compatible
            params = [('tarifa','=',tarifes_compatibles[0].id),
                      ('agrupat_amb', '=', False),
                      ('tipus', '=', 'te'),
                      ]
            periodes = period_obj.search(cr, uid, params)

            # define -3 on base indicates that item is based on pricelist price
            vals = {'price_version_id': version_id,
                    'sequence': 5,
                    'base': -3
                    }
            if based_on_pricelist_id:
                vals.update({
                    'base_pricelist_id': based_on_pricelist_id,
                    'base': -1
                })

            new_pos = self.compute_extra_columns(cr, uid, wizard, list_id, row, 3, context=context)

            # creem els preus d'energia
            pos = new_pos
            for i in range(len(periodes)):
                if version_id not in sequences:
                    sequences[version_id] = self.get_max_sequence(cr, uid, version_id) + 1
                price = float(row[pos+i].replace(',', '.'))
                nom_periode = 'P{0}'.format(i+1)
                params = [('tarifa', '=', tarifes_compatibles[0].id),
                          ('product_id.name', '=', nom_periode),
                          ('tipus', '=', 'te')]
                periode_id = period_obj.search(cr, uid, params)
                # En els periodes amb els que estigui agrupat posem els mateixos preus
                argupats_ids = period_obj.search(cr, uid, [
                    ('agrupat_amb', '=', periode_id[0]),
                ])
                periodes_to_update = periode_id + argupats_ids
                for periode_id in periodes_to_update:
                    periode = period_obj.browse(cr, uid, periode_id)
                    if self.item_exists(cr, uid, periode.product_id.id, version_id):
                        continue
                    vals_per = vals.copy()
                    name = '{} {} ENERGIA {}'.format(tarifa_name, periode.name, v_name)
                    vals_per.update({
                        'name': name,
                        'product_id': periode.product_id.id,
                        'sequence': sequences[version_id]
                    })
                    if not based_on_pricelist_id:
                        vals_per.update({
                            # 'price_discount': discount,
                            'base_price': price,

                        })
                    else:
                        vals_per.update({
                            # 'price_discount': discount,
                            'price_surcharge': price,

                        })
                    sequences[version_id] += 1
                    self.create_item(cr, uid, vals_per)

            # mirem si tenim preus de potència a continuació i els creem
            pos = pos + len(periodes)
            if len(row) - 1 >= pos:
                if version_id not in sequences:
                    sequences[version_id] = self.get_max_sequence(cr, uid, version_id) + 1
                # busquem el num de periodes de potencia de la tarifa compatible
                params = [('tarifa','=',tarifes_compatibles[0].id),
                          ('agrupat_amb', '=', False),
                          ('tipus', '=', 'tp')]
                periodes = period_obj.search(cr, uid, params)

                for i in range(len(periodes)):
                    price = float(row[pos+i].replace(',', '.'))
                    nom_periode = 'P{0}'.format(i+1)
                    params = [('tarifa', '=', tarifes_compatibles[0].id),
                              ('product_id.name', '=', nom_periode),
                              ('tipus', '=', 'tp')]
                    periode_id = period_obj.search(cr, uid, params)
                    # En els periodes amb els que estigui agrupat posem els mateixos preus
                    argupats_ids = period_obj.search(cr, uid, [
                        ('agrupat_amb', '=', periode_id[0]),
                    ])
                    periodes_to_update = periode_id + argupats_ids
                    for periode_id in periodes_to_update:
                        periode = period_obj.browse(cr, uid, periode_id)
                        if self.item_exists(cr, uid, periode.product_id.id, version_id):
                            continue

                        vals_per = vals.copy()
                        name = '{} {} POTENCIA {}'.format(tarifa_name, periode.name, v_name)
                        vals_per.update({
                            'name': name,
                            'product_id': periode.product_id.id,
                            'sequence': sequences[version_id]
                        })
                        if not based_on_pricelist_id:
                            vals_per.update({
                                # 'price_discount': discount,
                                'base_price': price,

                            })
                        else:
                            vals_per.update({
                                # 'price_discount': discount,
                                'price_surcharge': price,

                            })
                        sequences[version_id] += 1
                        self.create_item(cr, uid, vals_per)

            resum += _('\nPricelist version created {0}').format(v_name)

            self.write(
                cr, uid, ids, {'resum': resum,
                               'pricelist_updated': json.dumps(list_ids)}
            )

        return True

    def compute_extra_columns(self, cursor, uid, wizard, list_id, row, current_pos, context=None):
        return current_pos

    _columns = {
        'pricelist_file':fields.binary('Fitxer de tarifa',
            required=True,
            filters=None,
            help="Crea una nova llista de preus o si ja existeix una amb el "
                 "mateix nom l'actualitza creant una versió de preus nova.\n\n"
                 "IMPORTANT Cada línia ha d'acabar en \\n (salt de línia o "
                 "intro), LA ULTIMA LÍNIA TAMBÉ.\n\n"
                 "Els camps han d'estar separats per punt i coma.\n"
                 "Cada línia ha de tenir el format següent:\n"
                 "tarifes compatibles separades per coma; nom de la "
                 "llista de preus; preu P1 energia;...;[Preu P1 potencia; ...]\n"
                 "Exemple línia amb preus de potència:\n"
                 "2.0A;1601 20A 0550 ME;0,126162;42.043426\n"
                 "Exemple línia sense preus de potència:\n"
                 "3.1A,3.1A LB;1608 31A 1500 CM;0.123248;0.104112;0.705466"
                                       ),
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'state': fields.selection([('init', 'init'),
                                   ('end', 'end'),
                                   ('error', 'error'),],
                                  'Estat', readonly=True),
        'error': fields.text('Errors'),
        'resum': fields.text('Resum'),
        'pricelist_type': fields.selection(_pricelist_type_get, 'Pricelist Type', required=True),
        'pricelist_updated': fields.text("Llistes actualitzades"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'resum': lambda *a: '\n',
        'pricelist_type': lambda *a: 'sale'
    }

WizardPricelistFromFile()
