# -*- coding: utf-8 -*-
from osv import osv
from operator import itemgetter
from datetime import datetime, timedelta
from tools.translate import _
from giscedata_facturacio_comer.report import utils
from dateutil import parser
import re
import math

# Repartiment segons BOE
repartiment_BOE = {'i': 36.28, 'c': 32.12, 'o': 31.60}

origen_energia_espanya_2017 = {
    'Renovable': 32.0,
    'Cogeneración de Alta Eficiencia': 0.7,
    'Cogeneración': 10.2,
    'CC Gas Natural': 14.4,
    'Carbón': 17.5,
    'Fuel/Gas': 2.7,
    'Nuclear': 21.5,
    'Otras': 1.0,
}


class GiscedataFacturacioFacturaReport(osv.osv):

    _auto = False
    _name = 'giscedata.facturacio.factura.report'

    @staticmethod
    def _cmp_lines_detailed_info(line_a, line_b):
        order = (line_a['name'] > line_b['name']) - (line_a['name'] < line_b['name'])
        if order == 0:
            order = (line_a['total'] < line_b['total']) - (line_a['total'] > line_b['total'])

        return order

    @staticmethod
    def _sort_lines_detailed_info(mycmp):
        class K:
            def __init__(self, obj, *args):
                self.obj = obj

            def __lt__(self, other):
                return mycmp(self.obj, other.obj) < 0

            def __gt__(self, other):
                return mycmp(self.obj, other.obj) > 0

            def __eq__(self, other):
                return mycmp(self.obj, other.obj) == 0

            def __le__(self, other):
                return mycmp(self.obj, other.obj) <= 0

            def __ge__(self, other):
                return mycmp(self.obj, other.obj) >= 0

            def __ne__(self, other):
                return mycmp(self.obj, other.obj) != 0

        return K

    def _get_origen_lectura(self, cursor, uid, lectura, context=None):
        """Busquem l'origen de la lectura cercant-la a les lectures de facturació"""
        res = {
            lectura['data_actual']: _(u"real"),
            lectura['data_anterior']: _(u"real")
        }

        lectura_obj = self.pool.get('giscedata.lectures.lectura')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        origen_obj = self.pool.get('giscedata.lectures.origen')

        # Recuperar l'estimada
        estimada_id = origen_obj.search(cursor, uid, [('codi', '=', '40')])[0]

        # Busquem la tarifa
        if lectura and lectura['name']:
            tarifa_id = tarifa_obj.search(
                cursor, uid, [('name', '=', lectura['name'][:-5])]
            )

            if tarifa_id:
                tipus = lectura['tipus'] == 'activa' and 'A' or 'R'

                search_vals = [
                    ('comptador', '=', lectura['comptador']),
                    ('periode.name', '=', lectura['name'][-3:-1]),
                    ('periode.tarifa', '=', tarifa_id[0]),
                    ('tipus', '=', tipus),
                    ('name', 'in', [lectura['data_actual'], lectura['data_anterior']])
                ]
                lect_ids = lectura_obj.search(cursor, uid, search_vals)
                lect_vals = lectura_obj.read(
                    cursor, uid, lect_ids, ['name', 'origen_comer_id', 'origen_id']
                )

                for lect in lect_vals:
                    # En funció dels origens, escrivim el text
                    # Si Estimada (40)
                    # La resta: Real
                    origen_txt = _(u"real")
                    if lect['origen_id'][0] == estimada_id:
                        origen_txt = _(u"estimada")

                    res[lect['name']] = origen_txt

        return res

    def get_taxes(self, cursor, uid, ids, context=None):
        """
        Obtains the invoice taxes to be shown at an invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the keys are the invoice ids and the value is
            a dictionary with the following structure:
                - taxes: dictionary where the key is the tax name and the value
                    is the tax amount.
                - iese: dictionary with the following structure:
                    - lines: list of dictionaries. Each dictionary has the
                        following structure:
                            - name: tax name
                            - tax_amount: tax amount
                            - base_amount: tax base amount
                            - amount: tax amount
                    - total: sum of tax amount of all iese lines
                - iva: dictionary with the following structure:
                    - lines: list of dictionaries. Each dictionary has the
                        following structure:
                            - name: tax name
                            - base: tax base
                            - amount: tax amount
                            - percentage: tax percentage
                    - total: sum of tax amount of all iva lines
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('giscedata.facturacio.factura')

        invoice_f = ['id', 'invoice_id', 'invoice_id.type']
        dmn = [('id', 'in', ids)]
        invoice_vs = factura_o.q(cursor, uid).read(invoice_f).where(dmn)

        invoice_ids = []
        map_invoice_id = {}
        invoices_sign = {}
        for invoice_v in invoice_vs:
            invoice_id = invoice_v['invoice_id']
            invoice_ids.append(invoice_id)
            map_invoice_id[invoice_id] = invoice_v['id']
            invoices_sign[invoice_id] = -1 if invoice_v['invoice_id.type'] in ['out_refund', 'in_refund'] else 1

        tax_o = self.pool.get('account.invoice.tax')
        tax_f = ['name', 'amount', 'base', 'amount', 'base_amount',
                 'tax_amount', 'invoice_id']
        dmn = [('invoice_id', 'in', invoice_ids)]

        res = {}
        for factura_id in ids:
            res[factura_id] = {
                'taxes': {},
                'iese': {'lines': [], 'total': 0},
                'iva': {'lines': [], 'total': 0}
            }

        for tax_v in tax_o.q(cursor, uid).read(tax_f).where(dmn):
            invoice_id = tax_v['invoice_id']
            factura_id = map_invoice_id[invoice_id]
            sign = invoices_sign[invoice_id]

            tax_name = tax_v['name']
            tax_amount = sign * tax_v['amount']
            res[factura_id]['taxes'].update({
                tax_name: tax_amount
            })
            if 'IVA' in tax_name:
                res[factura_id]['iva']['lines'].append({
                    'name': tax_name,
                    'base': sign * tax_v['base'],
                    'amount': tax_amount,
                    'percentage': tax_name.replace('IVA', '').strip(),
                })
            else:
                res[factura_id]['iese']['total'] += tax_amount
                res[factura_id]['iese']['lines'].append({
                    'name': tax_name,
                    'base_amount': tax_v['base_amount'],
                    'tax_amount': tax_v['tax_amount'],
                })

        return res

    def get_power_exces_and_other_detailed_info(self, cursor, uid, ids, context=None):
        """
        Obtains the lines with power excess and other detailed information
        about the invoice. The other info is: the cost of the meters rental and
        other lines that are invoiced.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - meters_rent: list of dictionaries. Each dictionary has the
                    following structure:
                        - amount: price of the meter rental
                        - days: ...
                        - price: rental price
                - other_lines: dictionary with the following structure:
                    - lines: list of dictionaries. Each dictionary has the
                        following structure:
                            - name: line name
                            - price: line price
                - lines_with_power_excess: list of dictionaries. Each dictionary
                    has the following structure:
                        - name: line name
                        - excess: power excess (kVArh)
                        - price: line price
                        - total: line cost
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_line_o = self.pool.get('giscedata.facturacio.factura.linia')
        product_o = self.pool.get('product.product')

        dmn = [('factura_id', 'in', ids)]

        res = {}
        for factura_id in ids:
            res[factura_id] = {
                'meters_rent': [],
                'other_lines': {'lines': [], 'total': 0},
                'lines_with_power_excess': []
            }

        price_subtotal = 'invoice_line_id.price_subtotal'
        quantity = 'invoice_line_id.quantity'
        product = 'invoice_line_id.product_id.id'
        name = 'invoice_line_id.name'
        invoice_type = 'factura_id.invoice_id.type'
        factura_line_f = ['tipus', 'price_unit_multi', name, price_subtotal,
                          'factura_id', 'data_desde', quantity, product,
                          invoice_type]

        q = factura_line_o.q(cursor, uid).select(factura_line_f).where(dmn)

        # As OOQuery only supports INNER JOINS, 'q' will be replaced by a raw
        # query (the same generated by OOQuery but using LEFT JOINS)
        q = """
            SELECT
                "a"."tipus" AS "tipus",
                "a"."price_unit_multi" AS "price_unit_multi",
                "b"."name" AS "invoice_line_id.name",
                "b"."price_subtotal" AS "invoice_line_id.price_subtotal",
                "a"."factura_id" AS "factura_id",
                "a"."data_desde" AS "data_desde",
                "b"."quantity" AS "invoice_line_id.quantity",
                "c"."id" AS "invoice_line_id.product_id.id",
                "e"."type" AS "factura_id.invoice_id.type"
            FROM "giscedata_facturacio_factura_linia" AS "a"
            LEFT JOIN "account_invoice_line" AS "b" ON ("a"."invoice_line_id" = "b"."id")
            LEFT JOIN "product_product" AS "c" ON (("b"."product_id" = "c"."id") AND ("c"."active" = True))
            LEFT JOIN "giscedata_facturacio_factura" AS "d" ON ("a"."factura_id" = "d"."id")
            LEFT JOIN "account_invoice" AS "e" ON ("d"."invoice_id" = "e"."id")
            WHERE (("a"."factura_id" IN %s)) ORDER BY "a"."id"
        """

        cursor.execute(q, (tuple(ids),))

        for factura_line_v in cursor.dictfetchall():
            factura_id = factura_line_v['factura_id']
            sign = -1 if factura_line_v[invoice_type] in ['out_refund', 'in_refund'] else 1
            invoice_line_type = factura_line_v['tipus']

            if invoice_line_type in 'lloguer':
                res[factura_id]['meters_rent'].append({
                    'amount': sign * factura_line_v[price_subtotal],
                    'days': factura_line_v[quantity],
                    'price': sign * factura_line_v['price_unit_multi']
                })
            else:
                product_id = factura_line_v[product]
                product_code = False
                if product_id:
                    product_code = product_o.read(
                        cursor, uid, product_id, ['code'], context=context
                    )['code']
                donation_line = product_code == 'DN01'
                if invoice_line_type in 'altres' and not donation_line:
                    price = sign * factura_line_v[price_subtotal]
                    res[factura_id]['other_lines']['lines'].append({
                        'name': factura_line_v[name],
                        'price': price
                    })
                    res[factura_id]['other_lines']['total'] += price
                elif invoice_line_type == 'exces_potencia':
                    res[factura_id]['lines_with_power_excess'].append({
                        'name': factura_line_v[name],
                        'data_desde': factura_line_v['data_desde'],
                        'excess': factura_line_v[quantity],
                        'price': sign * factura_line_v['price_unit_multi'],
                        'total': sign * factura_line_v[price_subtotal],
                    })

        for factura_id in ids:
            res[factura_id]['lines_with_power_excess'] = sorted(
                sorted(
                    res[factura_id]['lines_with_power_excess'], key=itemgetter('data_desde')
                ), key=itemgetter('name')
            )

        return res

    def get_lines_detailed_info(self, cursor, uid, ids, context=None):
        """
        Obtains the details of the energy, power and reactive lines of the
        invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the keys: energy, power and reactive. The value of
            each key is a Dictionary with the following structure:
                - total: total price of the energy, power or reactive lines.
                - lines: List of Dictionaries with the detailed line
                    information:
                        - consumption: line consumption (quantity)
                        - discount (optional): discount percentage
                        - isdiscount: indicates if the line is a discount one
                        - name: line name.
                        - note: line notes.
                        - price: line price (price_unit_multi)
                        - time: (multi)
                        - toll: 'peajes'
                        - total: line cost (price_subtotal)
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_line_o = self.pool.get('giscedata.facturacio.factura.linia')

        name = 'invoice_line_id.name'
        quantity = 'invoice_line_id.quantity'
        price_subtotal = 'invoice_line_id.price_subtotal'
        invoice_type = 'factura_id.invoice_id.type'
        note = 'invoice_line_id.note'
        factura_line_f = ['price_unit_multi', name, quantity, 'multi', 'tipus',
                          price_subtotal, 'factura_id.id', 'data_desde', note,
                          'atrprice_subtotal', invoice_type, 'product_id',
                          'isdiscount']
        line_types = ['energia', 'potencia', 'reactiva']
        dmn = [('factura_id', 'in', ids), ('tipus', 'in', line_types)]

        factura_line_vs = factura_line_o.q(cursor, uid).read(factura_line_f).where(dmn)

        for factura_line_v in factura_line_vs:
            sign = -1 if factura_line_v[invoice_type] in ['out_refund', 'in_refund'] else 1

            factura_line_v['name'] = factura_line_v.pop(name)
            factura_line_v['consumption'] = factura_line_v.pop(quantity)
            factura_line_v['price'] = sign * factura_line_v.pop('price_unit_multi')
            factura_line_v['time'] = factura_line_v.pop('multi')
            factura_line_v['toll'] = sign * factura_line_v.pop('atrprice_subtotal')
            factura_line_v['total'] = sign * factura_line_v.pop(price_subtotal)
            factura_line_v['note'] = factura_line_v.pop(note)

        factura_line_vs = sorted(
            sorted(
                factura_line_vs, key=itemgetter('data_desde')
            ), key=itemgetter('name')
        )

        res = {}
        for factura_id in ids:
            res[factura_id] = {
                'energy': {
                    'lines': [],
                    'total': 0
                },
                'power': {
                    'lines': [],
                    'total': 0
                },
                'reactive': {
                    'lines': [],
                    'total': 0
                },
            }

        for factura_line_v in factura_line_vs:
            factura_id = factura_line_v['factura_id.id']
            if factura_line_v['tipus'] in ['energia', 'potencia', 'reactiva']:
                root_dict = res[factura_id]['reactive']
                if factura_line_v['tipus'] == 'energia':
                    root_dict = res[factura_id]['energy']
                elif factura_line_v['tipus'] == 'potencia':
                    root_dict = res[factura_id]['power']

                root_dict['lines'].append(factura_line_v)
                root_dict['total'] += factura_line_v['total']

        comparer = GiscedataFacturacioFacturaReport._sort_lines_detailed_info(
            GiscedataFacturacioFacturaReport._cmp_lines_detailed_info
        )

        for factura_id, vals in res.items():
            for t in ['energy', 'power', 'reactive']:
                lines = vals[t]['lines']

                vals[t]['lines'] = sorted(lines, key=comparer)
                discount_lines = {}
                total = {}
                for line in vals[t]['lines']:
                    product_id = line['product_id']
                    if line['isdiscount']:
                        if product_id not in discount_lines:
                            discount_lines[product_id] = []
                        discount_lines[product_id].append(line)
                    else:
                        total[product_id] = total.get(product_id, 0) + line['total']

                for product_id in total.keys():
                    for discount_line in discount_lines.get(product_id, []):
                        line_total = total[product_id]
                        if line_total > 0:
                            discount_line['discount'] = abs(math.trunc(
                                discount_line['total'] / line_total * 100
                            ))

        return res

    def get_policy_info(self, cursor, uid, ids, context=None):
        """
        Obtains the policy info related to the invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - id: <giscedata.polissa> id
                - name: policy number
                - start_date: "fecha de alta" (str: %Y-%m-%d)
                - end_date: end date of the current modcon (str: %Y-%m-%d)
                - cnae: CNAE number
                - pricelist: pricelist name
                - periods_power: Dictionary where the key is the period name
                    (P1, P2, P3, ....) and the value is the hired power.
                - has_maximeter: boolean that indicates if it is invoiced
                    using maximeter.
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('giscedata.facturacio.factura')
        policy_o = self.pool.get('giscedata.polissa')
        power_by_periods_o = self.pool.get(
            'giscedata.polissa.potencia.contractada.periode'
        )
        period_o = self.pool.get('giscedata.polissa.tarifa.periodes')
        cnae_o = self.pool.get('giscemisc.cnae')
        pricelist_o = self.pool.get('product.pricelist')
        modcon_o = self.pool.get('giscedata.polissa.modcontractual')

        res = {}
        for factura_id in ids:
            res[factura_id] = {
                'id': -1,               'name': '',
                'start_date': '',       'end_date': '',
                'cnae': '',             'pricelist': '',
                'periods_power': {},    'has_maximeter': False,
            }

        factura_vs = factura_o.read(
            cursor, uid, ids, ['polissa_id', 'data_inici']
        )

        policy_f = ['data_alta', 'cnae', 'name', 'llista_preu',
                    'potencies_periode', 'modcontractual_activa',
                    'facturacio_potencia']

        for factura_v in factura_vs:
            ctx = {'date': factura_v['data_inici']}

            factura_id = factura_v['id']
            policy_id = factura_v['polissa_id'][0]

            policy_v = policy_o.read(
                cursor, uid, policy_id, policy_f, context=ctx
            )

            cnae_id = policy_v['cnae'][0]
            cnae_v = cnae_o.read(cursor, uid, cnae_id, ['name'])

            pricelist_id = policy_v['llista_preu'][0]
            pricelist_v = pricelist_o.read(cursor, uid, pricelist_id, ['name'])

            modcon_id = policy_v['modcontractual_activa'][0]
            modcon_v = modcon_o.read(cursor, uid, modcon_id, ['data_final'])

            periods_power = policy_o.get_potencies_dict(
                cursor, uid, policy_id, context=ctx
            )

            res[factura_id].update({
                'id': policy_id,
                'name': policy_v['name'],
                'start_date': policy_v['data_alta'],
                'end_date': modcon_v['data_final'],
                'cnae': cnae_v['name'],
                'pricelist': pricelist_v['name'],
                'has_maximeter': policy_v['facturacio_potencia'] == 'max',
                'periods_power': periods_power
            })

        return res

    def get_supplier_info(self, cursor, uid, ids, context=None):
        """
        Obtains the supplier ("distribuidora") information related to the
        invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - ref: "referencia de la distribuidora"
                - name: "nom de la distribuidora"
                - phone: "numero de telefon de la distribuidora" (from the
                    default address).
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('giscedata.facturacio.factura')
        res_partner_o = self.pool.get('res.partner')
        res_partner_address_o = self.pool.get('res.partner.address')

        supplier_id = 'polissa_id.distribuidora.id'
        supplier_ref = 'polissa_id.ref_dist'
        supplier_name = 'polissa_id.distribuidora.name'

        factura_f = [supplier_id, supplier_name, supplier_ref, 'id']
        dmn = [('id', 'in', ids)]

        q = factura_o.q(cursor, uid).select(factura_f).where(dmn)
        cursor.execute(*q)

        res = {}

        for factura_v in cursor.dictfetchall():
            factura_id = factura_v['id']

            supplier_address_id = res_partner_o.address_get(
                cursor, uid, [factura_v[supplier_id]]
            )['default']

            supplier_phone = res_partner_address_o.read(
                cursor, uid, supplier_address_id, ['phone'], context=context
            )['phone']

            res[factura_id] = {
                'ref': factura_v[supplier_ref],
                'name': factura_v[supplier_name],
                'phone': supplier_phone,
            }

        return res

    def get_holder_info(self, cursor, uid, ids, context=None):
        """
        Obtains the policy's holder information related to the invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - name: holder's name
                - vat: holder's VAT ("NIF"/"CIF")
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('giscedata.facturacio.factura')

        name = 'polissa_id.titular.name'
        vat = 'polissa_id.titular.vat'

        factura_f = [name, vat, 'id']
        dmn = [('id', 'in', ids)]

        factura_vs = factura_o.q(cursor, uid).read(factura_f).where(dmn)

        res = {}

        for factura_v in factura_vs:
            factura_id = factura_v['id']

            res[factura_id] = {
                'name': utils.setstr(factura_v[name]),
                'vat': utils.setstr(factura_v[vat])
            }

        return res

    def get_historics(self, cursor, uid, ids, context=None):
        """
        Obtains the necessary data to generate the historic chart.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value a
            dictionary with the following structure:
                - historic: dictionary with the following structure:
                    - init_date: historic start date
                    - end_date: historic end date
                    - days: historic's number of days
                    - months: historic's number of days
                    - consumption: historic's total consumption
                    - average_cost: historic's average cost
                    - year_consumption: consumption of the current year
                    - average_consumption: historic's average consumption
                - historic_js: won't be described. It has the necessary
                    data to construct the 14-month consumption chart. Shouldn't
                    be modified.
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('giscedata.facturacio.factura')
        factura_f = ['polissa_id.id', 'data_inici', 'data_final', 'id']
        dmn = [('id', 'in', ids)]
        q = factura_o.q(cursor, uid).select(factura_f).where(dmn)
        cursor.execute(*q)

        q_historic = """SELECT * FROM (
                        SELECT mes AS mes,
                        periode AS periode,
                        sum(suma_fact) AS facturat,
                        sum(suma_consum) AS consum,
                        min(data_ini) AS data_ini,
                        max(data_fin) AS data_fin
                        FROM (
                        SELECT f.polissa_id AS polissa_id,
                               to_char(f.data_inici, 'YYYY/MM') AS mes,
                               pt.name AS periode,
                               COALESCE(SUM(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_consum,
                               COALESCE(SUM(il.price_subtotal*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END)),0.0) as suma_fact,
                               MIN(f.data_inici) data_ini,
                               MAX(f.data_final) data_fin
                               FROM
                                    giscedata_facturacio_factura f
                                    LEFT JOIN account_invoice i on f.invoice_id = i.id
                                    LEFT JOIN giscedata_facturacio_factura_linia fl on f.id=fl.factura_id
                                    LEFT JOIN account_invoice_line il on il.id=fl.invoice_line_id
                                    LEFT JOIN product_product pp on il.product_id=pp.id
                                    LEFT JOIN product_template pt on pp.product_tmpl_id=pt.id
                               WHERE
                                    fl.tipus = 'energia' AND
                                    f.polissa_id = %(p_id)s AND
                                    f.data_inici <= %(data_inicial)s AND
                                    f.data_inici >= date_trunc('month', date %(data_final)s) - interval '14 month'
                                    AND (fl.isdiscount IS NULL OR NOT fl.isdiscount)
                                    AND i.type in('out_invoice', 'out_refund')
                               GROUP BY
                                    f.polissa_id, pt.name, f.data_inici
                               ORDER BY f.data_inici DESC ) AS consums
                        GROUP BY polissa_id, periode, mes
                        ORDER BY mes DESC, periode ASC
                        ) consums_ordenats
                        ORDER BY mes ASC
                """

        res = {}
        for factura_id in ids:
            res[factura_id] = {
                'historic': {
                    'init_date': datetime.max.strftime("%Y-%m-%d"),
                    'end_date': '1900-01-01',
                    'days': 0,
                    'months': 0,
                    'invoiced': 0,
                    'consumption': 0,
                    'average_cost': 0,
                    'year_consumption': 0,
                    'average_consumption': 0,
                },
                'historic_js': []
            }

        for factura_v in cursor.dictfetchall():
            factura_id = factura_v['id']
            q_params = {
                'p_id': factura_v['polissa_id.id'],
                'data_inicial': factura_v['data_inici'],
                'data_final': factura_v['data_final'],
            }
            cursor.execute(q_historic, q_params)

            historic_graf = {}
            periodes_graf = []

            invoice_init_date = datetime.strptime(factura_v['data_inici'], '%Y-%m-%d')

            mes_any_inicial = (invoice_init_date - timedelta(days=365)).strftime("%Y/%m")

            historic = res[factura_id]['historic']

            dates_changed = False
            for row in cursor.dictfetchall():
                month = row['mes']
                period = row['periode']
                consumption = row['consum']
                init_date = row['data_ini']
                end_date = row['data_fin']

                historic_graf.setdefault(month, {})
                historic_graf[month].update({period: consumption})
                periodes_graf.append(period)
                if month > mes_any_inicial:
                    historic['year_consumption'] += consumption
                if init_date < historic['init_date']:
                    historic['init_date'] = init_date
                if end_date > historic['end_date']:
                    historic['end_date'] = end_date
                historic['invoiced'] += row['facturat']
                historic['consumption'] += row['consum']
                dates_changed = True
            else:
                if not dates_changed:
                    historic['init_date'] = factura_v['data_inici']
                    historic['end_date'] = factura_v['data_final']

            historic_js = res[factura_id]['historic_js']
            for month, consumptions in historic_graf.items():
                p_js = {'mes': month}

                for p in periodes_graf:
                    period_consumption = consumptions.get(p, 0.0)
                    p_js.update({p: period_consumption})

                historic_js.append(p_js)

            historic['init_date'] = datetime.strptime(
                historic['init_date'], '%Y-%m-%d'
            )
            historic['end_date'] = datetime.strptime(
                historic['end_date'], '%Y-%m-%d'
            )
            date_diff = historic['end_date'] - historic['init_date']
            historic['days'] = 1 + date_diff.days

            historic['months'] = int(historic['days'] / 30)
            historic['average_cost'] = historic['invoiced'] / historic['days']
            historic['average_consumption'] = historic['consumption'] / historic['days']

        return res

    def get_periods_and_readings(self, cursor, uid, ids, context=None):
        """
        Obtains the periods and the readings of the invoice
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - periods: dictionary with the following structure:
                    - active: list(str) of active periods names (P1, P2, P3, ...)
                    - reactive: list(str) reactive periods names
                    - maximeter: list(str) maximeter periods names
                - readings: dictionary with the following structure:
                    - active: dictionary with the following structure:
                        - total_consumption: sum of the active readings for all
                            the meters.
                        - readings: dictionary where the key is the meter name
                            (SN) and the value is a dictionary with the
                            following structure:
                                - previous_reading: value of the previous
                                    reading
                                - current_reading: value of the current reading
                                - consumption: reading consumption
                                - previous_date: reading start date.
                                - current_date: reading end date.
                                - previous_origin:
                                - current_origin:
                    - reactive: same as active but with reactive readings.
                    - maximeter: dictionary where the key is the period and the
                        value is a dictionary with the following structure:
                            - power: hired power
                            - maximeter: maximeter reading
        """
        if not isinstance(ids, list):
            ids = [ids]

        energy_readings_o = self.pool.get('giscedata.facturacio.lectures.energia')
        power_readings_o = self.pool.get('giscedata.facturacio.lectures.potencia')

        res = {}
        factura_has_energy_lines = {}

        for factura_id in ids:
            res[factura_id] = {
                'periods': {
                    'active': [],
                    'reactive': [],
                    'maximeter': []
                },
                'readings': {
                    'active': {
                        'total_consumption': {},
                        'readings': {}
                    },
                    'reactive': {
                        'total_consumption': {},
                        'readings': {}
                    },
                    'maximeter': {},
                },
            }
            factura_has_energy_lines[factura_id] = False

        energy_readings_f = [
            'tipus', 'name', 'factura_id', 'comptador', 'lect_anterior',
            'lect_actual', 'consum', 'data_anterior', 'data_actual'
        ]
        dmn = [('factura_id', 'in', ids)]

        # TODO this could be improved using an ordered list
        energy_reading_vs = energy_readings_o.q(cursor, uid).read(energy_readings_f).where(dmn)
        for energy_reading_v in energy_reading_vs:
            factura_id = energy_reading_v['factura_id']
            factura_has_energy_lines[factura_id] = True

            energy_reading_type = energy_reading_v['tipus']
            period = re.findall('(P[1-9])', energy_reading_v['name'])[0]
            meter = energy_reading_v['comptador']

            factura_periods = res[factura_id]['periods']
            factura_readings = res[factura_id]['readings']

            origin = self._get_origen_lectura(
                cursor, uid, energy_reading_v, context=context
            )

            energy_reading_values = {
                'previous_reading': energy_reading_v['lect_anterior'],
                'current_reading': energy_reading_v['lect_actual'],
                'consumption': energy_reading_v['consum'],
                'previous_date': energy_reading_v['data_anterior'],
                'current_date': energy_reading_v['data_actual'],
                'previous_origin': origin[energy_reading_v['data_anterior']],
                'current_origin': origin[energy_reading_v['data_actual']],
            }

            if energy_reading_type == 'activa':
                factura_periods['active'].append(period)

                factura_readings['active']['readings'].setdefault(meter, {})
                factura_readings['active']['readings'][meter].update({
                    period: energy_reading_values
                })
                partial_cosumption = factura_readings['active']['total_consumption'].get(period, 0)
                factura_readings['active']['total_consumption'][period] = partial_cosumption + energy_reading_values['consumption']

            elif energy_reading_type == 'reactiva':
                factura_periods['reactive'].append(period)

                factura_readings['reactive']['readings'].setdefault(meter, {})
                factura_readings['reactive']['readings'][meter].update({
                    period: energy_reading_values
                })
                partial_cosumption = factura_readings['reactive']['total_consumption'].get(period, 0)
                factura_readings['reactive']['total_consumption'][period] = partial_cosumption + energy_reading_values['consumption']

        power_readings_f = ['pot_maximetre', 'name', 'pot_contract', 'factura_id']
        dmn = [('factura_id', 'in', ids)]

        power_reading_vs = power_readings_o.q(cursor, uid).read(power_readings_f).where(dmn)
        for power_reading_v in power_reading_vs:
            factura_id = power_reading_v['factura_id']

            period = power_reading_v['name']
            maximeter_reading = power_reading_v['pot_maximetre']

            res[factura_id]['readings']['maximeter'][period] = {
                'power': power_reading_v['pot_contract'],
                'maximeter': maximeter_reading
            }

            factura_periods = res[factura_id]['periods']
            factura_periods['maximeter'].append(period)

        for factura_id in ids:
            factura_periods = res[factura_id]['periods']

            active_periods = factura_periods['active']
            reactive_periods = factura_periods['reactive']
            maximeter_periods = factura_periods['maximeter']

            res[factura_id]['periods']['active'] = sorted(list(set(active_periods)))
            res[factura_id]['periods']['reactive'] = sorted(list(set(reactive_periods)))
            res[factura_id]['periods']['maximeter'] = sorted(list(set(maximeter_periods)))

        return res

    def get_invoiced_power(self, cursor, uid, ids, periods_maximeter, context=None):
        """
        Obtains the invoiced power of the invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param periods_maximeter: Dictionary where the key is the
            invoice id and the value is the list value.
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary where each key is the period name of the invoiced power
            (value).
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_line_o = self.pool.get('giscedata.facturacio.factura.linia')
        period_f = 'invoice_line_id.product_id.product_tmpl_id.name'
        quantity = 'invoice_line_id.quantity'
        factura_line_f = [period_f, quantity, 'factura_id']
        dmn = [('factura_id', 'in', ids), ('tipus', '=', 'potencia')]

        res = {}
        for factura_id in ids:
            res[factura_id] = dict.fromkeys(periods_maximeter[factura_id], 0)

        power_line_vs = factura_line_o.q(cursor, uid).read(factura_line_f).where(dmn)
        for power_line_v in power_line_vs:
            factura_id = power_line_v['factura_id']
            period = power_line_v[period_f]
            period_power = res[factura_id].get(period, 0)
            res[factura_id].update({
                period: max(period_power, power_line_v['invoice_line_id.quantity'])
            })

        return res

    def get_pie_data(self, cursor, uid, ids, other_lines_total, context=None):
        """
        Obtains the necessary data to fill the pie chart.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param other_lines_total: Dictionary where the key is the invoice id and
            the value is the sum of the other lines.
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
        dictionary with the following keys.
            - total
            - regulats
            - impostos
            - costos
            - reparto
            - dades_reparto
        The values (and the keys) shouldn't
        be modified if not necessary.
        """
        if not isinstance(ids, list):
            ids = [ids]

        res = {}

        factura_f = ['invoice_id.amount_total', 'total_lloguers', 'total_atr',
                     'invoice_id.amount_tax', 'id']
        dmn = [('id', 'in', ids)]
        factura_o = self.pool.get('giscedata.facturacio.factura')
        factura_vs = factura_o.q(cursor, uid).read(factura_f).where(dmn)

        for factura_v in factura_vs:
            factura_id = factura_v['id']

            pie_total = factura_v['invoice_id.amount_total'] - factura_v['total_lloguers']

            total_atr = 0
            if factura_v['total_atr']:
                total_atr = factura_v['total_atr']

            pie_regulats = total_atr + other_lines_total[factura_id]
            pie_impostos = float(factura_v['invoice_id.amount_tax'])

            reparto = {
                'i': pie_regulats * repartiment_BOE['i'] / 100,
                'c': pie_regulats * repartiment_BOE['c'] / 100,
                'o': pie_regulats * repartiment_BOE['o'] / 100
            }

            res[factura_id] = {
                'total': pie_total,
                'regulats': pie_regulats,
                'impostos': pie_impostos,
                'costos': pie_total - pie_regulats - pie_impostos,
                'reparto': reparto,
                'dades_reparto': [
                    [
                        [0, repartiment_BOE['i']],
                        'i',
                        _(u"Incentius a les energies renovables, cogeneració i "
                          u"residus"),
                        reparto['i']
                    ],
                    [
                        [repartiment_BOE['i'], repartiment_BOE['i'] + repartiment_BOE['c']],
                        'c',
                        _(u"Cost de xarxes de distribució i transport"),
                        reparto['c']
                    ],
                    [
                        [repartiment_BOE['i'] + repartiment_BOE['c'], 100.00],
                        'o',
                        _(u"Altres Costos regulats (inclosa anualitat del "
                          u"dèficit)"),
                        reparto['o']
                    ]
                ]
            }

        return res

    def get_company_info(self, cursor, uid, ids, context=None):
        """
        Obtains the necessary data to show of company ("comercialitzadora")on a
        invoice.
        :param cursor:
        :param uid:
        :param ids: <res.company> ids
        :param context:
        :return: Dictionary where the key is the company id and the value is a
        dictionary with the following structure:
            - name: "nom de la comercialitzadora"
            - email: "correu electronic de la comercialitzadora"
            - lateral_info: "informació legal de la comercialitzadora.
                Tipicament es posa al lateral de la factura."
            - phone: "numero de telefon de la comercialitzadora"
            - logo: "imatge amb el logo de la comercializadora"
            - vat: "CIF de la comercialitzadora"
            - street: "direccio de la comercialitzadora"
            - zip: ...
            - poblacio: ...
            - state: ...
            - entity: entity to show if bar code.
        """
        if not isinstance(ids, list):
            ids = [ids]

        partner_o = self.pool.get('res.partner')
        res_partner_address_o = self.pool.get('res.partner.address')
        company_o = self.pool.get('res.company')

        address_f = ['street', 'zip', 'id_poblacio.name', 'email', 'phone',
                     'state_id.name']
        address_select = res_partner_address_o.q(cursor, uid).select(address_f)

        company_f = ['partner_id.id', 'partner_id.name', 'partner_id.vat',
                     'id', 'rml_footer2', 'logo']
        dmn = [('id', 'in', ids)]

        q = company_o.q(cursor, uid).select(company_f).where(dmn)
        cursor.execute(*q)

        res = {}

        for company_v in cursor.dictfetchall():
            company_id = company_v['id']

            company_address_id = partner_o.address_get(
                cursor, uid, [company_v['partner_id.id']]
            )['default']

            q = address_select.where([('id', '=', company_address_id)])
            cursor.execute(*q)
            address_v = cursor.dictfetchall()[0]

            vat = company_v['partner_id.vat']
            entity = ''.join([c for c in vat if c.isdigit()])

            res[company_id] = {
                'name': company_v['partner_id.name'],
                'vat': vat,
                'logo': company_v['logo'],
                'lateral_info': company_v['rml_footer2'],
                'street': address_v['street'],
                'zip': address_v['zip'],
                'poblacio': address_v['id_poblacio.name'],
                'state': address_v['state_id.name'],
                'phone': address_v['phone'],
                'email': address_v['email'],
                'entity': entity,
            }

        return res

    def get_contact_info(self, cursor, uid, ids, context=None):
        """
        Obtains the contact information related to the invoice.
        :param cursor:
        :param uid:
        :param ids: <giscedata.facturacio.factura> ids
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            dictionary with the following structure:
                - name: contact name
                - street: contact street address
                - zip: contact zip address
                - city: contact city (id_municipi)
                - aclarador: ...
        """
        if not isinstance(ids, list):
            ids = [ids]

        factura_o = self.pool.get('giscedata.facturacio.factura')

        name_f = 'invoice_id.address_contact_id.name'
        street_f = 'invoice_id.address_contact_id.street'
        zip_f = 'invoice_id.address_contact_id.zip'
        city_f = 'invoice_id.address_contact_id.id_municipi.name'
        aclarador_f = 'invoice_id.address_contact_id.aclarador'

        factura_f = [name_f, street_f, 'id', zip_f, city_f, aclarador_f]
        dmn = [('id', 'in', ids)]

        res = {}

        # Use this query instead the OOQuery because it uses LEFT joins instead
        # of INNER ones.
        q = """
            SELECT
                "c"."name" AS "invoice_id.address_contact_id.name",
                "c"."street" AS "invoice_id.address_contact_id.street",
                "a"."id" AS "id",
                "c"."zip" AS "invoice_id.address_contact_id.zip",
                "d"."name" AS "invoice_id.address_contact_id.id_municipi.name",
                "c"."aclarador" AS "invoice_id.address_contact_id.aclarador"
            FROM "giscedata_facturacio_factura" AS "a"
            LEFT JOIN "account_invoice" AS "b" ON ("a"."invoice_id" = "b"."id")
            LEFT JOIN "res_partner_address" AS "c" ON ("b"."address_contact_id" = "c"."id")
            LEFT JOIN "res_municipi" AS "d" ON ("c"."id_municipi" = "d"."id")
            WHERE (("a"."id" IN %s))
            ORDER BY "a"."id" DESC
        """
        cursor.execute(q, (tuple(ids),))
        factura_vs = cursor.dictfetchall()

        for factura_v in factura_vs:
            factura_id = factura_v['id']

            res[factura_id] = {
                'name': utils.setstr(factura_v[name_f]),
                'street': utils.setstr(factura_v[street_f]),
                'zip': utils.setstr(factura_v[zip_f]),
                'city': utils.setstr(factura_v[city_f]),
                'aclarador':  utils.setstr(factura_v[aclarador_f]),
            }

        return res

    def get_payer_info(self, cursor, uid, ids, context=None):
        """
        Obtains the payer information.
        :param cursor:
        :param uid:
        :param ids: List of <giscedata.facturacio.factura> ids.
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            Dictionary with the following structure:
                - vat: payer vat.
                - name: payer name.
        """
        factura_o = self.pool.get('giscedata.facturacio.factura')

        res = {}

        factura_f = ['id', 'invoice_id.partner_id.vat', 'invoice_id.partner_id.name']
        dmn = [('id', 'in', ids)]

        for factura_v in factura_o.q(cursor, uid).read(factura_f).where(dmn):
            factura_id = factura_v['id']
            res[factura_id] = {
                'vat': factura_v['invoice_id.partner_id.vat'],
                'name': factura_v['invoice_id.partner_id.name']
            }

        return res

    def get_invoice_info(self, cursor, uid, ids, context=None):
        """
        Obtains the invoice information.
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return: Dictionary where the key is the invoice id and the value is a
            Dictionary with the following structure:
                - address: Dictionary with the following structre:
                    - street: ...
                    - zip: ...
                    - municipi: ...
                - amount: invoice's total amount.
                - bank: Dictionary with the following structure:
                    - name: bank's name.
                    - iban: IBAN
                - cups: Dictionary with the following structure:
                    - id: ...
                    - name: CUPS' name.
                    - address: CUPS'S address.
                - date: invoice's date.
                - days: day's difference between initial date and end date.
                - due_date: invoice's due date.
                - end_date: invoice's end date.
                - id: invoice id.
                - init_date: invoice's initial date.
                - hired_power: hired power.
                - name (optional): invoice's name.
                - payment_type: payment type name. If not set, None.
                - payment_type_code: payment type code. If not set, None.
                - pricelist: price list name.
                - rent: meters rent's total cost.
                - tariff: tariff name.
                - diari_factura_actual_kwh
                - suffix_bar_code: suffix to show if bar code.
                - ref_bar_code: reference to show if bar code.
                - lang_partner: Language of the partner
                - invoice_type: Invoice type
        """
        if not isinstance(ids, list):
            ids = [ids]

        payment_type_o = self.pool.get('payment.type')
        payment_type_f = ['name', 'code']

        q = """
            SELECT
                address.street AS street,
                address.zip AS zip,
                municipi.name AS municipi_name,
                invoice.amount_total AS amount,
                bank.iban AS bank_iban,
                bank.name AS bank_name,
                cups.id AS cups_id,
                cups.direccio AS cups_address,
                cups.name AS cups_name,
                invoice.date_invoice AS invoice_date,
                invoice.date_due AS due_date,
                factura.data_final AS end_date,
                factura.id AS id,
                factura.data_inici AS init_date,
                factura.potencia AS hired_power,
                invoice.number AS invoice_name,
                invoice.payment_type AS payment_type_id,
                pricelist.name AS pricelist_name,
                factura.total_lloguers AS rent,
                tariff.name AS tariff_name,
                factura.energia_kwh AS energy_kwh,
                invoice.type AS invoice_type,
                partner.lang AS lang_partner
            FROM giscedata_facturacio_factura AS factura
            LEFT JOIN account_invoice AS invoice ON invoice.id = factura.invoice_id
            LEFT JOIN giscedata_cups_ps AS cups ON cups.id = factura.cups_id
            LEFT JOIN res_partner_bank AS bank ON bank.id = invoice.partner_bank
            LEFT JOIN product_pricelist AS pricelist ON pricelist.id = factura.llista_preu
            LEFT JOIN giscedata_polissa_tarifa AS tariff ON tariff.id = factura.tarifa_acces_id
            LEFT JOIN res_partner_address AS address ON address.id = invoice.address_invoice_id
            LEFT JOIN res_municipi AS municipi ON municipi.id = address.id_municipi
            LEFT JOIN res_partner AS partner ON partner.id = invoice.partner_id
            WHERE factura.id IN %s
        """

        cursor.execute(q, (tuple(ids),))

        res = {}
        for factura_v in cursor.dictfetchall():
            factura_id = factura_v['id']

            address = {
                'street': factura_v['street'],
                'zip': factura_v['zip'],
                'municipi': factura_v['municipi_name'],
            }
            bank = {
                'name': factura_v['bank_name'],
                'iban': factura_v['bank_iban'],
            }
            cups = {
                'id': factura_v['cups_id'],
                'name': factura_v['cups_name'],
                'address': factura_v['cups_address'],
            }

            erp_init_date = factura_v['init_date']
            erp_end_date = factura_v['end_date']

            init_date = parser.parse(erp_init_date)
            end_date = parser.parse(erp_end_date)

            days = 1 + (end_date - init_date).days

            energy_kwh = factura_v['energy_kwh']
            if not energy_kwh:
                energy_kwh = 0

            sign = -1 if factura_v['invoice_type'] in ['out_refund', 'in_refund'] else 1

            due_date = factura_v['due_date']
            suffix_bar_code = '101'
            if due_date:
                due_date = utils.erp_date_to_dmy(due_date)
                suffix_bar_code = '501'

            ref_bar_code = '{}'.format(factura_id).zfill(11)

            payment_type_id = factura_v['payment_type_id']
            payment_type_v = payment_type_o.read(
                cursor, uid, payment_type_id, payment_type_f, context=context
            )

            res[factura_id] = {
                'address': address,
                'amount': sign * factura_v['amount'],
                'bank': bank,
                'cups': cups,
                'date': utils.erp_date_to_dmy(factura_v['invoice_date']),
                'days': days,
                'due_date': due_date,
                'end_date': end_date.strftime('%d/%m/%Y'),
                'id': factura_id,
                'init_date': init_date.strftime('%d/%m/%Y'),
                'hired_power': factura_v['hired_power'],
                'lang_partner': factura_v['lang_partner'],
                'payment_type': payment_type_v['name'] or None,
                'payment_type_code': payment_type_v['code'] or None,
                'pricelist': factura_v['pricelist_name'],
                'rent': sign * factura_v['rent'],
                'tariff': factura_v['tariff_name'],
                'diari_factura_actual_kwh': (energy_kwh * 1.0) / (days or 1.0),
                'suffix_bar_code': suffix_bar_code,
                'ref_bar_code': ref_bar_code,
                'invoice_type': factura_v['invoice_type'],
            }

            name = factura_v['invoice_name']
            if name:
                res[factura_id].update({'name': name})

        return res

    @staticmethod
    def historic_js_c3_formatted(historic_js, accumulated):
        """

        :param historic_js:
        :param accumulated:

        :return:
        """
        bar_data = {}
        months = []

        for month_data in sorted(historic_js, key=lambda x: x['mes']):
            for k in sorted(month_data.keys()):
                v = month_data[k]
                if type(v) is float or type(v) is int:
                    if k not in bar_data:
                        bar_data[k] = []
                    bar_data[k].append(v)
                else:
                    months.append(v)

        res = []

        if accumulated:
            res.append(['accumulated'])
            for i, period in enumerate(sorted(bar_data.keys())):
                for j, period_consumption in enumerate(bar_data[period]):
                    if i == 0:
                        res[0].append(period_consumption)
                    else:
                        res[0][j + 1] += period_consumption
        else:
            for period in sorted(bar_data.keys()):
                res.append([period] + bar_data[period])

        return res, sorted(months)


GiscedataFacturacioFacturaReport()


class GiscedataFacturacioCartaRequerimentReport(osv.osv):
    _auto = False
    _name = 'giscedata.facturacio.carta.requeriment.report'

    def get_start_day(self, cursor, uid, ids, modo_carta, context=None):
        """
        This function returns the start day of the Payment Notice.
        :param cursor:
        :param uid:
        :param ids:
        :param context: Maybe we will add a force feature in context
        :return: A datetime with the day on which the Payment Notice starts.
        """
        imd_obj = self.pool.get('ir.model.data')
        account_invoice_obj = self.pool.get('account.invoice')

        if not isinstance(ids, list):
            ids = [ids]

        res = {}
        for carta_id in ids:
            carta_requerimiento_id = ''
            if modo_carta == 'req1':
                carta_requerimiento_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_facturacio_comer_bono_social',
                    'carta_1_pending_state')[1]
            elif modo_carta == 'req2':
                carta_requerimiento_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_facturacio_comer_bono_social',
                    'carta_2_pending_state')[1]
            elif modo_carta == 'corte_bs':
                carta_requerimiento_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_facturacio_comer_bono_social',
                    'carta_avis_tall_pending_state')[1]
            data = account_invoice_obj.get_last_change_date(
                cursor, uid, carta_id,
                carta_requerimiento_id, context=context
            )
            if data:
                data_inici = datetime.strptime(data, '%Y-%m-%d')
            else:
                data_inici = datetime.now()
            res[carta_id] = data_inici
        return res


GiscedataFacturacioCartaRequerimentReport()
