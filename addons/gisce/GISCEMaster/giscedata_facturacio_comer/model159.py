# -*- coding: utf-8 -*-

"""
Model 159

"""

from osv import fields, osv


class GiscedataFacturacioModel159(osv.osv):
    """ Model 159
    """
    _name = 'giscedata.facturacio.model159'
    _columns = {
        'name': fields.char('Data càlcul', size=32, required=True),
        'linies': fields.one2many('giscedata.facturacio.model159.linia',
                                  'informe_id', 'Línies'),
        'header': fields.one2many('giscedata.facturacio.model159.header',
                                  'informe_id', 'Capçalera'),
    }


GiscedataFacturacioModel159()

    
class GiscedataFacturacioModel159Header(osv.osv):
    """ Capçalera del model 159
    """
    _name = 'giscedata.facturacio.model159.header'
    _columns = {
        'informe_id': fields.many2one('giscedata.facturacio.model159',
                                      'Informe', ondelete='cascade'),
        # Camps que composen la primera línia de l'informe
        'tipus': fields.char('Tipus', size=1),
        'd_model': fields.char('Model', size=3),
        'd_exercici': fields.char('Exercici', size=4),
        'd_nif': fields.char('NIF declarant', size=9),
        'rao': fields.char('Raó social', size=40),
        'suport': fields.char('Suport', size=1),
        # Contacte
        'telf': fields.char('Telèfon', size=9),
        'nom': fields.char('Nom', size=40),
        'num': fields.char('Número declaració', size=13),
        'compl': fields.char('Decl. compl.', size=1),
        'subst': fields.char('Decl. subs.', size=1),
        'num_ant': fields.char('Núm. decl. anterior', size=13),
        'num_tit': fields.char('Número titulars', size=9),
        # Import total
        'signe': fields.char('Signe', size=1),
        'entera': fields.char('Part entera', size=15),
        'decimal': fields.char('Part decimal', size=2),
        # Padding
        'padding': fields.char('padding', size=1),
    }


GiscedataFacturacioModel159Header()


class GiscedataFacturacioModel159Linia(osv.osv):
    """ Línies del model 159
    """
    _name = 'giscedata.facturacio.model159.linia'
    _columns = {
        'informe_id': fields.many2one('giscedata.facturacio.model159',
                                      'Informe', ondelete='cascade'),
        'tipus': fields.char('Tipus', size=1),
        'd_model': fields.char('Model', size=3),
        'd_exercici': fields.char('Exercici', size=4),
        'd_nif': fields.char('NIF declarant', size=9),
        't_nif': fields.char('NIF titular', size=9),
        'r_nif': fields.char('NIF representant legal', size=9),
        'nom': fields.char('Nom o raó social', size=40),
        # habitatge
        'h_tipus': fields.char('Tipus de via', size=5),
        'h_nom': fields.char('Nom de la via', size=50),
        'h_t_num': fields.char('Tipus de numeració', size=3),
        'h_num': fields.char('Número de casa', size=5),
        'h_cal': fields.char('Cal·lificador del número', size=3),
        'h_bloc': fields.char('Bloc', size=3),
        'h_port': fields.char('Portal', size=3),
        'h_esc': fields.char('Escala', size=3),
        'h_pis': fields.char('Planta o pis', size=3),
        'h_porta': fields.char('Porta', size=3),
        'h_compl': fields.char('Complement', size=40),
        'h_loc': fields.char('Localitad', size=30),
        'h_mun': fields.char('Municipi', size=30),
        'h_ine_mun': fields.char('Codi municipi', size=5),
        'h_ine_prov': fields.char('Codi província', size=2),
        'h_cp': fields.char('Codi postal', size=5),
        # Banc
        'b_tipus': fields.char('Tipus de compte', size=1),
        'b_iban_pais': fields.char('País ISO', size=2),
        'b_iban_dc': fields.char('IBAN D.C.', size=2),
        'b_entitat': fields.char('Entitat', size=4),
        'b_suc': fields.char('Sucursal', size=4),
        'b_dc': fields.char('D.C.', size=2),
        'b_num': fields.char('Número de compte', size=10),
        'b_pais': fields.char('Codi de país', size=2),
        'b_id': fields.char('Codi d\'identificació', size=15),
        'polissa': fields.char('Contracte de subministre', size=12),
        # CUPS
        'c_pais': fields.char('Codi país', size=2),
        'c_distri': fields.char('Distribuidora', size=4),
        'c_num': fields.char('Número', size=12),
        'c_ch': fields.char('Control', size=2),
        'c_mes': fields.char('Punts de mesura', size=1),
        'c_sub': fields.char('Punts de subministre', size=1),
        'tipus_be': fields.char('Tipus de bé', size=1),
        'situacio': fields.char('Situació', size=1),
        'ref_cat': fields.char('Referència catastral', size=20),
        # alta
        'a_any': fields.char('Any alta', size=4),
        'a_mes': fields.char('Mes alta', size=2),
        'a_dia': fields.char('Dia alta', size=2),
        # baixa
        'b_any': fields.char('Any baixa', size=4),
        'b_mes': fields.char('Mes baixa', size=2),
        'b_dia': fields.char('Dia baixa', size=2),
        # consum facturat
        'c_unitat': fields.char('Unitat de mesura', size=1),
        'c_01': fields.char('Consum gener', size=4),
        'c_t_01': fields.char('Tipus lectura gener', size=1),
        'c_02': fields.char('Consum febrer', size=4),
        'c_t_02': fields.char('Tipus lectura febrer', size=1),
        'c_03': fields.char('Consum març', size=4),
        'c_t_03': fields.char('Tipus lectura març', size=1),
        'c_04': fields.char('Consum abril', size=4),
        'c_t_04': fields.char('Tipus lectura abril', size=1),
        'c_05': fields.char('Consum maig', size=4),
        'c_t_05': fields.char('Tipus lectura maig', size=1),
        'c_06': fields.char('Consum juny', size=4),
        'c_t_06': fields.char('Tipus lectura juny', size=1),
        'c_07': fields.char('Consum juliol', size=4),
        'c_t_07': fields.char('Tipus lectura juliol', size=1),
        'c_08': fields.char('Consum agost', size=4),
        'c_t_08': fields.char('Tipus lectura agost', size=1),
        'c_09': fields.char('Consum setembre', size=4),
        'c_t_09': fields.char('Tipus lectura setembre', size=1),
        'c_10': fields.char('Consum octubre', size=4),
        'c_t_10': fields.char('Tipus lectura octubre', size=1),
        'c_11': fields.char('Consum novembre', size=4),
        'c_t_11': fields.char('Tipus lectura novembre', size=1),
        'c_12': fields.char('Consum desembre', size=4),
        'c_t_12': fields.char('Tipus lectura desembre', size=1),
        # import total facturat
        'i_signe': fields.char('Signe', size=1),
        'i_enter': fields.char('Import part entera', size=9),
        'i_decim': fields.char('Import part decimal', size=2),
        # potencia
        'p_unitat': fields.char('Unitat mesura', size=1),
        'p_entera': fields.char('Part entera', size=4),
        'p_decimal': fields.char('Part decimal', size=2),
        # padding
        'padding': fields.char('padding', size=1),
    }


GiscedataFacturacioModel159Linia()
