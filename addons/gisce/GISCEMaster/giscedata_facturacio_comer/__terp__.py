# -*- coding: utf-8 -*-
{
    "name": "Facturació (Comercialitzadora)",
    "description": """Facturació (Comercialitzadora)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_facturacio",
        "giscedata_polissa_comer",
        "purchase",
        "jasper_reports",
        "giscedata_lectures_comer",
        "poweremail",
        "poweremail_references",
        "report_banner",
        "giscedata_administracio_publica"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_polissa_demo.xml",
        "giscedata_facturacio_comer_demo.xml"
    ],
    "update_xml":[
        "wizard/wizard_generar_xml_606_view.xml",
        "giscedata_facturacio_view.xml",
        "giscedata_facturacio_data.xml",
        "giscedata_polissa_data.xml",
        "giscedata_polissa_view.xml",
        "giscedata_lectures_view.xml",
        "giscedata_facturacio_comer_report.xml",
        "wizard/config_taxes_facturacio_comer_view.xml",
        "product_view.xml",
        "pricelist_view.xml",
        "giscedata_facturacio_comer_wizard.xml",
        "informe_preus_mitc_view.xml",
        "wizard/informe_preus_mitc_wizard_view.xml",
        "wizard/informe_model159_wizard_view.xml",
        "wizard/wizard_factures_per_email_view.xml",
        "wizard/wizard_payer_change.xml",
        "wizard/wizard_avancar_facturacio_view.xml",
        "informe_model159_view.xml",
        "wizard/wizard_invoice_open_and_send.xml",
        "wizard/pricelist_from_file_view.xml",
        "wizard/wizard_bank_account_change.xml",
        "wizard/wizard_ranas_view.xml",
        "ir.model.access.csv",
        "giscedata_facturacio_validation_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
