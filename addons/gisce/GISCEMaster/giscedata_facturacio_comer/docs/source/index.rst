.. giscedata_facturacio_comer documentation master file, created by
   sphinx-quickstart on Thu Feb  9 11:50:36 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació del mòdul de facturació per comercialitzadores
===========================================================

Documentació d'usuari del mòdul per generar l'informe de preus de consumidors (model 606)
=========================================================================================

En la ubicació que es mostra en la `Figura 1`_ es troba el menú per a la 
realizació de l'informe de preus de consumidors, model 606 que es detalla 
en el BOE n. 69  Sec. I. Pàg. 30271.

El wizard de realització de l'informe es mostra en la `Figura 2`_.
Tal i com s'especifica en l'annex I del BOE esmentat anteriorment 
l'informe, que s'entregarà dos cops l'any,  ha de recollir la informació 
dels darrers 6 mesos. El període en qüestió s'especificarà en els apartats 
'Data d'inici' i 'Data final'. La opció 'Extrapolar el consum anual', es 
sel·leccionarà únicament en el cas de no existir històric d'un any (12 mesos
faturats complets), cas
en que caldrà especificar el factor de correcció a aplicar al resultat.
Per exemple, en el cas de tenir històric dels darrers 6 mesos, s'indicarà 
a l'ERP que extrapoli el conum 
i en el factor de correcció es sel·leccionarà 2. Si diposéssim de 
12 mesos facturats únicament caldria especificar 1 al factor de correcció.
Trobeu altres exemples en la secció `Exemples de configuració`_.

En finalitzar l'informe, es mostrarà el missatge de la `Figura 3`_, que
en acceptar-se ens llistarà l'informe (veure `Figura 4`_). Únicament
cal prémer fent doble-click al damunt de l'informe per veure'n el resultat.
La `Figura 5`_ mostra el resultat d'un informe.

Finalment podem mostrar tots els informes generats anant al sub-menú
que s'indica en la `Figura 6`_ que resulta en el llistat de la 
`Figura 7`_. Únicament caldria fer doble-click al damunt de l'informe escollit
per veure'n la seva informació.

Exemples de configuració:
-------------------------

.. _`Exemples de configuració`:

Cas 1: Hem facturat des de 01/01/2012 a 30/06/2012 (6 mesos)

    * Extrapolar consum: Sí
    * Factor de correcció: 2 (**2** x 6 mesos = 12 mesos)


Cas 2: Tenim factures de 01/04/2012 a 30/06/2012 (3 mesos)

    * Extrapolar el consum: Sí
    * Factor de correcció: 4 (**4** x 3 mesos = 12 mesos)


Cas 3: Tenim 12 mesos d'històric de facturació:

    * Extrapolar el consum: No
    * Factor de correcció: 1



Figures
-------

.. _`Figura 1`:
.. figure::  ./img/model_606/menu_wiz.png
   :align:  center
   
   Figura 1: Ubicació del formulari de generació de l'informe.

.. _`Figura 2`:
.. figure::  ./img/model_606/wizard.png
   :align:  center
   
   Figura 2: Wizard

.. _`Figura 3`:
.. figure::  ./img/model_606/accept.png
   :align:  center
   
   Figura 3: Missatge de finalització de l'informe de preus.

.. _`Figura 4`:
.. figure::  ./img/model_606/inf_llist.png
   :align:  center
   
   Figura 4: Darrer informe realitzat.

.. _`Figura 5`:
.. figure::  ./img/model_606/linies_informe.png
   :align:  center
   
   Figura 5: Informació associada a l'informe.

.. _`Figura 6`:
.. figure::  ./img/model_606/menu_llist.png
   :align:  center
   
   Figura 6: Formulari per llistar tots els informes realitzats.

.. _`Figura 7`:
.. figure::  ./img/model_606/tot_llist.png
   :align:  center
   
   Figura 7: Llistat d'informes realitzats.


Documentació d'usuari del mòdul per generar l'informe del model 159
===================================================================

En la ubicació que es mostra en la `Figura 8`_ es troba el menú per a la 
generació de l'informe del model 159, tal i com es detalla en el BOE n. 182
Sec. I. Pàg. 64838.

La `Figura 9`_ mostra el wizard de generació de l'informe. Es generarà un nou
informe amb la data final indicada en prémer 'Generar informe'.  En finalitzar
el procés de realització de l'informe es mostren els errors ocurreguts 
(`Figura 10`_). Prement 'Veure Informe' es llista l'informe generat 
(`Figura 11`_). Finalment, fent doble click en l'informe generat, veurem les 
línies de l'informe (`Figura 12`_) i tindrem la possibilitat d'exportar-lo
prement 'Exportar document'.

En exportar el document, es sol·licita informació relativa a la 
persona de contacte, informació relativa al suport que s'utilitzarà
per la transmissió del document i el número d'informe que es correspon. El
número d'informe és un número seqüencial relatiu a cada informe entregat.

Valors per defecte
------------------

Per tal que el resultat de la validació sigui correcte, hi ha una sèrie de 
camps als quals se'ls assigna un valor per defecte en cas de no tenir valor
a l'ERP. La següent taula en detalla el nom del camp, la posició
en la línia del registre de tipus 2 i el valor assignat per defecte.

==================   ========== ============
Camp                 Posició    Valor
==================   ========== ============
NIF del titular      18         12345678Z
Tipus de via         76         CL
Tipus de numeració   131        NUM o S/N (en funció de si hi ha número)
Codi postal          264        12321
Data d'alta          367        01/07/2009 (data d'inici del règim de lliure competència) 
                                en el cas de ser anterior a l'any 1930
==================   ========== ============

Errors de validació
-------------------


El programa de validació del model 159, en la versió v1.0 de 2011 [1], genera una sèrie d'errors pels quals no hi ha sol·lució. Es llisten a continuació:

==================   =================================================== ============
Codi d'error         Camp                                                Motiu
==================   =================================================== ============
2 0801               Tipus de via (Direcció del subministre)             Certs tipus de via proporcionats pel catastre.
2 0701               Cognoms i nom, raó social o denominació del titular Noms que contenen nombres. També noms amb una única paraula.
==================   =================================================== ============



[1] http://www.agenciatributaria.es/static_files/AEAT/Contenidos_Comunes/La_Agencia_Tributaria/Descarga_Programas/Descarga/prevalid/2011/CobolWindows/Prevalidacion_159_2011_v10.exe

Figures
-------

.. _`Figura 8`:
.. figure:: ./img/model_159/menu.png
   :align:  center

   Figura 8: Ubicació del menú per a la generació de l'informe del model 159.

.. _`Figura 9`:
.. figure:: ./img/model_159/wiz_generar.png
   :align: center

   Figura 9: Wizard de generació de l'informe.

.. _`Figura 10`:
.. figure:: ./img/model_159/wiz_mostrar.png
   :align:  center
    
   Figura 10: Wizard que es mostra en finalitzar l'informe i que mostra els errors apareguts.

.. _`Figura 11`:
.. figure:: ./img/model_159/imp_inf.png
   :align:  center

   Figura 11: Informe generat.

.. _`Figura 12`:
.. figure:: ./img/model_159/linia_tree.png
   :align:  center

   Figura 12: Línies de l'informe generat.


.. _`Figura 13`:
.. figure:: ./img/model_159/wiz_exportar.png
   :align:  center
   
   Figura 13: Wizard per exportar l'informe.

.. _`Figura 14`:
.. figure:: ./img/model_159/imp_tree.png
   :align:  center
   
   Figura 14: Llistat de tots els informes generats.

Enviament de factures a través de correu electrònic
===================================================

Hi ha la possibilitat d'enviar les factures d'energia a través del correu electrònic. Per defecte
ve configurat amb dos idiomes, el català i el castellà. Això vol dir que abans s'haurà de configurar
l'idioma dels clients al desitjat. També es pot modificar el text de la plantilla del correu
electrònic per tal d'adaptar-lo a les necessitats de cadascú.

Configuració del client
-----------------------

Així doncs en un contracte ara disposarem d'uns camps nous per tal de decidir com es vol enviar la
factura.

.. figure:: ./img/email/contracte_enviament_email.png
    :align: center

Tenim tres opcions a l'hora de decidir com volem enviar una factura:

  * Correu postal
  * Correu postal i e-mail
  * E-mail

Per defecte el contracte ve amb Correu postal activat, i per tant s'hauran de marcar tots aquells que
es vulgui fer la comunicació a través de correu electrònic. Per dur a terme això farem una **nova**
modificació contractual i actualitzem el camp **Enviar factura via** amb el tipus d'enviament que
es desitgi i actualitzem l'**Adreça de pagament** amb l'e-mail correcte. També ens podem assegurar
que l'idioma de la **Persona pagadora** sigui el correcta.

Aquestes opcions ens serviran a l'hora de fer l'enviament massiu. Els que tinguin seleccionada alguna
de les dues opcions que inclouen l'e-mail es seleccionarà per enviar.

Enviament massiu de factures a través del lot
---------------------------------------------

Es pot realitzar un enviament massiu de les factures a través del lot de facturació. Per tal de realitzar-lo
obrim el lot i en el marge dret hi tindrem l'assistent disponible.

Els requisits per tal que l'assistent seleccioni una factura per enviar són els següents:

* La factura ha d'estar en estat **Obert**.
* La factura no ha d'haver estat enviada.
* La pòlissa ha de tenir una modificació contractual amb enviament per email i que comprengui la data de la
  factura.

.. figure:: ./img/email/lot_assistent_massiu.png
    :align: center

Escollim l'idioma pel qual volem enviar les factures i apretem **Continuar**.
Un cop l'executem se'ns obrirà una finestra on se'ns mostren les factures per idioma d'aquest lot que tenim
pendents d'enviar.

.. figure:: ./img/email/assistent_1.png
    :align: center

Es mostrarà la plantilla per aquell idioma per si la volem modificar per aquesta ocasió i haurem d'escollir des
del compte que ho volem enviar.

.. figure:: ./img/email/assistent_2.png
    :align: center

Finalment apretem el botó **Envia tots els correus** i ens esperem a que faci la feina.

.. note::
    Pot ser que se'ns quedi el client de l'ERP bloquejat, si ens passa el deixem continuar ja que està preparant
    la sortida dels correus electrònics. Podem obrir un altre client i continuar treballant.

Haurem de realitzar l'enviament per cada idioma.

Enviament d'una sola factura (manual)
-------------------------------------

Quan volguem re-enviar una sola factura, o enviar-ne alguna que no estigui marcada per enviar també podrem fer-ho.
Obrim la factura i al marge dret veurem l'assistent **Enviar factura per e-mail Mail Form**.

.. figure:: ./img/email/poweremail_wizard.png

Després ens sortirà la plantilla igual que en l'enviament massiu.

.. note::
    Aquest assistent no contempla si la factura ja s'ha enviat, això vol dir que no ens avisarà si un correu ja ha
    sigut enviat.

Enviament múltiple de factures (manual)
---------------------------------------

Ens hem de situar al llistat de factures i fer el filtre que volguem. És molt important fer el filtre de l'**Idioma Client**
ja que si no s'utiltiza aquest filtre s'enviarà amb l'idioma del client de la primera factura de la selecció.

Un cop tenim la selecció feta (Si no en seleccionem cap el programa entén que són tots els de la llista) apretem el botó
d'**Acció** del menú principal, ens apareixeran el llistat d'assistents disponibles i escollim **Enviar factura per e-mail
Mail Form**.

.. figure:: ./img/email/poweremail_wizard_list.png

La resta és com l'enviament massiu.

.. note::
    Aquest assistent no contempla si la factura ja s'ha enviat, això vol dir que no ens avisarà si un correu ja ha
    sigut enviat.

Sistema d'enviament
-------------------

.. blockdiag::

    blockdiag {
        lot -> enviament -> enviar -> sortida -> email -> enviats;
        sortida -> sortida [label = "Cada 5m"]

        lot [shape = "note",
             label = "Lot"];
        enviament [label = "Enviament\nmassiu",
                   shape = "diamond"];
        email [shape = "mail"];
        enviar[label =  "Enviar",
               shape = "diamond"];
        sortida -> email [folded];
    }

Sempre que fem un enviament el correu es queda a la carpeta de **Sortida**, i el sistema cada 5 minuts
comprova si hi ha correus per enviar. En cas que n'hi hagi s'envien i es mouen a la carpeta d'**Enviats**.

Camps addicionals a una factura d'energia
-----------------------------------------

Si una factura està marcada per enviar per E-mail (una modificació contractual que comprèn la
data de la factura i que té seleccionat l'enviament per e-mail) tindrà 4 camps nous a la pestanya **Other info**.

.. figure:: ./img/email/camps_factura.png
    :align: center

* **Enviada per E-mail**: estarà marcat quan aquesta factura ja hagi sigut enviada.
* **Carpeta**: ens mostra a quina carpeta del sistema de correu es troba.

  * **Entrada**: S'utilitza per la recepció d'emails, en aquest cas no s'utilitzarà.
  * **Esborranys**: Quan fem un enviament i marquem que ho deixem a Esborrany, aquests correus electrònics mai
    s'enviaran fins que no els marquem per enviar.
  * **Sortida**: Està a punt de ser enviat.
  * **Papelera**: Per ser eliminat
  * **Enviats**: Ja ha sortit del nostre sistema i està entregat al sistema de correu destí.

* **Data enviament**: Data quan el correu electrònic va sortir del sistema.

En el cas que no estigui marcada per enviar per e-mail veuríem el següent text.

.. figure:: ./img/email/camps_factura_no_email.png
    :align: center

Comprovació dels enviaments d'un lot
------------------------------------

A partir del lot accedim a la drecera **Factures generades** i s'obrirà el llistat de les factures generades a través
d'aquell lot. Despleguem els filtres addicionals i veurem que hi ha 4 filtres nous.

.. figure:: ./img/email/filtres_factures.png
    :align: center

* **Enviada per E-mail**: Ens permet seleccionar les que ja s'han enviat i les que no.
* **Carpeta**: A quina carpeta es troba.
* **Data enviament**: Busquem per data d'enviament.
* **Marcada enviar per E-mail**: Està marcada que s'ha d'enviar a través de correu electrònic.

Per tant utilitzant el filtre de **Marcada enviar per E-mail**: **Sí** i **Enviada per E-mail**: **No** tindríem
totes les que encara no s'han enviat.

Comprovació d'enviaments pendents
---------------------------------

Tenim un menú disponible a través de **Facturació > Mercat lliure > Factures Client > Factures pendents d'enviar per email**.
Aquest és un llistat de factures amb alguns filtres per defecte fets:

* La factura estigui marcada per enviar
* No s'hagi enviat

.. note::
    Si en comptes de fer una modificació contractual nova, hem sobreescrit l'existent pot ser que en aquest llistat ens surtin
    factures antigues pendents d'enviar. Això és degut a que és la modificació contractual qui marca a partir de quina data
    una factura ha de ser enviada per email.

