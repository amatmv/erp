# coding=utf-8
from destral import testing
from osv.orm import ValidateException


class TestModeFacturacio(testing.OOTestCaseWithCursor):

    def test_polissa_mode_facturacio_selection(self):

        cursor = self.cursor
        uid = self.uid

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        f = pol_obj.fields_get(cursor, uid, ['mode_facturacio'])
        self.assertListEqual(
            f['mode_facturacio']['selection'],
            [('atr', 'ATR')]
        )

        mc_obj = self.openerp.pool.get('giscedata.polissa.modcontractual')
        f = mc_obj.fields_get(cursor, uid, ['mode_facturacio'])
        self.assertListEqual(
            f['mode_facturacio']['selection'],
            [('atr', 'ATR')]
        )

    def test_polsisa_mode_facturacio_llista_constraint(self):
        cursor = self.cursor
        uid = self.uid

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        mf_obj = self.openerp.pool.get('giscedata.polissa.mode.facturacio')

        pol_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        pl_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'pricelist_tarifas_electricidad'
        )[1]

        # Remove all compatible priceslist from ATR mode
        mode_id = mf_obj.search(cursor, uid, [('code', '=', 'atr')], limit=1)[0]
        mf_obj.write(cursor, uid, [mode_id], {
            'compatible_price_lists': [(3, pl_id)]
        })

        with self.assertRaises(ValidateException):
            pol_obj.write(cursor, uid, [pol_id], {'llista_preu': pl_id})