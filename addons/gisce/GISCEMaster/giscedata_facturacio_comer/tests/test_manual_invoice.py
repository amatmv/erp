# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
import mock
import json


class TestsManualInvoiceWizard(testing.OOTestCase):

    def test_manual_invoice_with_auotconsum(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            procelist = self.openerp.pool.get("product.pricelist").browse(cursor, uid, imd_obj.get_object_reference(cursor, uid, "giscedata_facturacio", "pricelist_tarifas_electricidad")[1])
            for version in procelist.version_id:
                self.openerp.pool.get("product.pricelist.item").create(cursor, uid, {
                    'product_id': imd_obj.get_object_reference(cursor, uid, "giscedata_facturacio_comer", "concepte_71")[1],
                    'base': -3,
                    'base_price': 0.5,
                    'name': "excedents 1",
                    'price_version_id': version.id
                })
                version.write({'date_end': None})

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            lot_id = lot_obj.search(cursor, uid, [
                ('name', '=', '11/2017'),

            ], limit=1)[0]

            vals = {
                'data_alta': '2017-10-01',
                'data_baixa': False,
                'potencia': 8.050,
                'data_ultima_lectura': '2017-10-28',
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '41',
                'contract_type': '01',
                'lot_facturacio': lot_id,
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                # Delete lloguer because it will fail in manual invoice because
                # it's implemented in comer/dist facturacio modules
                comptador.write({'lloguer': False})

            comptador = contract.comptadors[0]
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            vals = {
                'name': '2017-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'lectura_exporta': 100,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            vals = {
                'name': '2017-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'lectura_exporta': 210,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2017-11-01',
                'date_end': '2017-11-30',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2017-11-01')
            # Check lines
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 1000)
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.price_subtotal, 44.03)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 8.05)
            self.assertEqual(len(invoice.linies_generacio), 2)
            self.assertEqual(invoice.linies_generacio[0].tipus, 'generacio')
            self.assertEqual(invoice.linies_generacio[0].invoice_line_id.quantity, -110)
            self.assertEqual(invoice.linies_generacio[0].invoice_line_id.price_subtotal, -55.0)
            self.assertEqual(invoice.linies_generacio[1].tipus, 'generacio')
            self.assertEqual(invoice.linies_generacio[1].invoice_line_id.quantity, 1)
            self.assertEqual(invoice.linies_generacio[1].invoice_line_id.price_subtotal, 10.97)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 2)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].magnitud, 'AE')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 9000)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 8000)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2017-10-28')
            self.assertEqual(invoice.lectures_energia_ids[1].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[1].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[1].magnitud, 'AS')
            self.assertEqual(invoice.lectures_energia_ids[1].lect_actual, 210)
            self.assertEqual(invoice.lectures_energia_ids[1].lect_anterior, 100)
            self.assertEqual(invoice.lectures_energia_ids[1].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[1].data_anterior, '2017-10-28')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 8.05)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)
            self.assertFalse(invoice.lot_facturacio)

    def test_manual_invoice_with_auotconsum_visible_discount(self):
        self.openerp.install_module(
            'giscedata_tarifas_pagos_capacidad_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            procelist = self.openerp.pool.get("product.pricelist").browse(cursor, uid, imd_obj.get_object_reference(cursor, uid, "giscedata_facturacio", "pricelist_tarifas_electricidad")[1])
            procelist.write({'visible_discount': False})
            for version in procelist.version_id:
                self.openerp.pool.get("product.pricelist.item").create(cursor, uid, {
                    'product_id': imd_obj.get_object_reference(cursor, uid, "giscedata_facturacio_comer", "concepte_71")[1],
                    'base': -3,
                    'base_price': 0.5,
                    'name': "excedents 1",
                    'price_version_id': version.id
                })
                version.write({'date_end': None})

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            lot_id = lot_obj.search(cursor, uid, [
                ('name', '=', '11/2017'),

            ], limit=1)[0]

            vals = {
                'data_alta': '2017-10-01',
                'data_baixa': False,
                'potencia': 8.050,
                'data_ultima_lectura': '2017-10-28',
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '41',
                'contract_type': '01',
                'lot_facturacio': lot_id,
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                # Delete lloguer because it will fail in manual invoice because
                # it's implemented in comer/dist facturacio modules
                comptador.write({'lloguer': False})

            comptador = contract.comptadors[0]
            # Create lectures 1 day before activation
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            vals = {
                'name': '2017-10-28',
                'periode': periode_id,
                'lectura': 8000,
                'lectura_exporta': 100,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            vals = {
                'name': '2017-11-28',
                'periode': periode_id,
                'lectura': 9000,
                'lectura_exporta': 210,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            journal_obj = self.openerp.pool.get('account.journal')
            journal_id = journal_obj.search(
                cursor, uid, [('code', '=', 'ENERGIA')]
            )[0]
            wz_fact_id = wz_mi_obj.create(cursor, uid, {})
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            wz_fact.write({
                'polissa_id': contract_id,
                'date_start': '2017-11-01',
                'date_end': '2017-11-30',
                'journal_id': journal_id
            })
            wz_fact.action_manual_invoice()
            wz_fact = wz_mi_obj.browse(cursor, uid, wz_fact_id)
            invoice_id = json.loads(wz_fact.invoice_ids)[0]
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.data_inici, '2017-11-01')
            # Check lines
            self.assertEqual(len(invoice.linies_energia), 1)
            self.assertEqual(invoice.linies_energia[0].tipus, 'energia')
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.quantity, 1000)
            self.assertEqual(invoice.linies_energia[0].invoice_line_id.price_subtotal, 44.03)
            self.assertEqual(len(invoice.linies_potencia), 1)
            self.assertEqual(invoice.linies_potencia[0].tipus, 'potencia')
            self.assertEqual(invoice.linies_potencia[0].invoice_line_id.quantity, 8.05)
            self.assertEqual(len(invoice.linies_generacio), 2)
            self.assertEqual(invoice.linies_generacio[0].tipus, 'generacio')
            self.assertEqual(invoice.linies_generacio[0].invoice_line_id.quantity, -110)
            self.assertEqual(invoice.linies_generacio[0].invoice_line_id.price_subtotal, -55.0)
            self.assertEqual(invoice.linies_generacio[1].tipus, 'generacio')
            self.assertEqual(invoice.linies_generacio[1].invoice_line_id.quantity, 1)
            self.assertEqual(invoice.linies_generacio[1].invoice_line_id.price_subtotal, 10.97)
            # Check lectures
            self.assertEqual(len(invoice.lectures_energia_ids), 2)
            self.assertEqual(invoice.lectures_energia_ids[0].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[0].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[0].magnitud, 'AE')
            self.assertEqual(invoice.lectures_energia_ids[0].lect_actual, 9000)
            self.assertEqual(invoice.lectures_energia_ids[0].lect_anterior, 8000)
            self.assertEqual(invoice.lectures_energia_ids[0].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[0].data_anterior, '2017-10-28')
            self.assertEqual(invoice.lectures_energia_ids[1].name, '2.0A (P1)')
            self.assertEqual(invoice.lectures_energia_ids[1].tipus, 'activa')
            self.assertEqual(invoice.lectures_energia_ids[1].magnitud, 'AS')
            self.assertEqual(invoice.lectures_energia_ids[1].lect_actual, 210)
            self.assertEqual(invoice.lectures_energia_ids[1].lect_anterior, 100)
            self.assertEqual(invoice.lectures_energia_ids[1].data_actual, '2017-11-28')
            self.assertEqual(invoice.lectures_energia_ids[1].data_anterior, '2017-10-28')
            self.assertEqual(len(invoice.lectures_potencia_ids), 1)
            self.assertEqual(invoice.lectures_potencia_ids[0].name, 'P1')
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_contract, 8.05)
            self.assertEqual(invoice.lectures_potencia_ids[0].pot_maximetre, 0)
            self.assertFalse(invoice.lectures_potencia_ids[0].data_actual)
            self.assertFalse(invoice.lot_facturacio)
