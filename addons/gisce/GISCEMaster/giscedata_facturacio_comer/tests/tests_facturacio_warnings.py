# -*- coding: utf-8 -*-
from expects import expect
from expects import contain
from destral import testing
from destral.transaction import Transaction
from expects import expect
from expects import raise_error
from expects import equal
from expects import contain
from expects import contain_exactly

class TestsFacturesValidation(testing.OOTestCase):
    def setUp(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        line_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        warn_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.warning.template'
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        ctx = {'active_test': False}

        # We make sure that all warnings are active
        warn_ids = warn_obj.search(
            cursor, uid, [], context=ctx
        )
        warn_obj.write(cursor, uid, warn_ids, {'active': True})

    def tearDown(self):
        self.txn.stop()

    def test_check_iese(self):
        vali_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        tax_obj = self.openerp.pool.get('account.tax')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        facturador_obj = self.openerp.pool.get('giscedata.facturacio.facturador')
        imd_obj = self.openerp.pool.get('ir.model.data')
        lect_obj = self.openerp.pool.get('giscedata.facturacio.lectures.energia')
        cursor = self.txn.cursor
        uid = self.txn.user

        # get telemesura origin id
        origen_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]

        tax_id = tax_obj.search(cursor, uid, [('name', 'like', '%special%')])[0]

        # Crear factura amb IESE, consum 0 i una lectura real
        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0008'
        )[1]
        fact = fact_obj.browse(cursor, uid, fact_id)
        fact.energia_kwh = 0

        lect_ids = [b_record.id for b_record in fact.lectures_energia_ids]
        lect_obj.write(cursor, uid, lect_ids, {'origen_id': origen_id})

        for line in fact.linia_ids:
            line.write({'invoice_line_tax_id': [(4, tax_id)],
                        'quantity': 0})

        # Comprovar que el check valida i la factura té IESE
        result = vali_obj.check_iese(
            cursor, uid, fact, {}
        )
        self.assertIsNotNone(result)

        # Eliminar IESE de la factura
        facturador_obj.remove_iese_tax(cursor, uid, fact_id)

        # Comprovar que el check de facturació ja no salta
        result = vali_obj.check_iese(
            cursor, uid, fact, {}
        )

        self.assertIsNone(result)

    def model(self, model_name):
        return self.openerp.pool.get(model_name)

    def get_fixture(self, model, reference):
        imd_obj = self.model('ir.model.data')
        return imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user,
            model,
            reference
        )[1]

    def validation_warnings(self, factura_id):
        vali_obj = self.model('giscedata.facturacio.validation.validator')
        warn_obj = self.model('giscedata.facturacio.validation.warning')
        warning_ids = vali_obj.validate_invoice(
            self.txn.cursor, self.txn.user,
            factura_id
        )
        warning_vals = warn_obj.read(
            self.txn.cursor, self.txn.user,
            warning_ids,
            ['name']
        )
        return [warn['name'] for warn in warning_vals]


    def test_check_invoice_has_partner_bank__paymentmode_require_bank_account_and_has_no_bank__no_ok(self):
        factura_id = self.get_fixture('giscedata_facturacio', 'factura_0001')

        fact_obj = self.model('giscedata.facturacio.factura')
        fact_obj.write(self.txn.cursor,self.txn.user,factura_id,{'partner_bank':None})

        polissa_id = fact_obj.read(self.txn.cursor,self.txn.user,factura_id,['polissa_id'])['polissa_id'][0]
        pol_obj = self.model('giscedata.polissa')

        payment_mode_id = pol_obj.read(self.txn.cursor,self.txn.user,polissa_id,['payment_mode_id'])['payment_mode_id'][0]

        paym_obj = self.model('payment.mode')
        paym_obj.write(self.txn.cursor,self.txn.user,payment_mode_id,{'require_bank_account':True})

        warnings = self.validation_warnings(factura_id)
        expect(warnings).to(contain('F025'))

    def test_check_invoice_has_partner_bank__paymentmode_require_bank_account_and_has_bank__ok(self):
        factura_id = self.get_fixture('giscedata_facturacio', 'factura_0001')

        rpbk_obj = self.model('res.partner.bank')
        rpbk_id = rpbk_obj.search(self.txn.cursor,self.txn.user,[])[0]

        fact_obj = self.model('giscedata.facturacio.factura')
        fact_obj.write(self.txn.cursor,self.txn.user,factura_id,{'partner_bank':rpbk_id})

        polissa_id = fact_obj.read(self.txn.cursor,self.txn.user,factura_id,['polissa_id'])['polissa_id'][0]
        pol_obj = self.model('giscedata.polissa')

        payment_mode_id = pol_obj.read(self.txn.cursor,self.txn.user,polissa_id,['payment_mode_id'])['payment_mode_id'][0]

        paym_obj = self.model('payment.mode')
        paym_obj.write(self.txn.cursor,self.txn.user,payment_mode_id,{'require_bank_account':True})

        warnings = self.validation_warnings(factura_id)
        expect(warnings).not_to(contain('F025'))

    def test_check_invoice_has_partner_bank__paymentmode_not_require_bank_and_has_no_bank__ok(self):
        factura_id = self.get_fixture('giscedata_facturacio', 'factura_0001')

        fact_obj = self.model('giscedata.facturacio.factura')
        fact_obj.write(self.txn.cursor,self.txn.user,factura_id,{'partner_bank':None})

        polissa_id = fact_obj.read(self.txn.cursor,self.txn.user,factura_id,['polissa_id'])['polissa_id'][0]
        pol_obj = self.model('giscedata.polissa')

        payment_mode_id = pol_obj.read(self.txn.cursor,self.txn.user,polissa_id,['payment_mode_id'])['payment_mode_id'][0]

        paym_obj = self.model('payment.mode')
        paym_obj.write(self.txn.cursor,self.txn.user,payment_mode_id,{'require_bank_account':False})

        warnings = self.validation_warnings(factura_id)
        expect(warnings).not_to(contain('F025'))

    def test_check_invoice_has_partner_bank__paymentmode_not_require_bank_and_has_bank__ok(self):
        factura_id = self.get_fixture('giscedata_facturacio', 'factura_0001')

        rpbk_obj = self.model('res.partner.bank')
        rpbk_id = rpbk_obj.search(self.txn.cursor,self.txn.user,[])[0]

        fact_obj = self.model('giscedata.facturacio.factura')
        fact_obj.write(self.txn.cursor,self.txn.user,factura_id,{'partner_bank':rpbk_id})

        polissa_id = fact_obj.read(self.txn.cursor,self.txn.user,factura_id,['polissa_id'])['polissa_id'][0]
        pol_obj = self.model('giscedata.polissa')

        payment_mode_id = pol_obj.read(self.txn.cursor,self.txn.user,polissa_id,['payment_mode_id'])['payment_mode_id'][0]

        paym_obj = self.model('payment.mode')
        paym_obj.write(self.txn.cursor,self.txn.user,payment_mode_id,{'require_bank_account':False})

        warnings = self.validation_warnings(factura_id)
        expect(warnings).not_to(contain('F025'))