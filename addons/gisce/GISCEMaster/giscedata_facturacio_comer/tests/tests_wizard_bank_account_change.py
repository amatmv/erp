# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import datetime


class TestWizardAccountChange(testing.OOTestCase):

    def activar_polissa(self, txn):
        # per poder canviar de compte, la pòlissa ha d'estar activa.
        cursor = txn.cursor
        uid = txn.user

        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        # per poder canviar de compte, abans ha de tenir un compte bancari vàlid
        bank_id = imd_obj.get_object_reference(
            cursor, uid, 'base_iban', 'res_partner_bank_iban_0002'
        )[1]
        polissa_obj.write(cursor, uid, [polissa_id], {'bank': bank_id})

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        return polissa_id

    def crear_mandato(self, txn, polissa_id):
        # per poder canviar de compte, ha d'haver com a minim un mandat.
        mandate_obj = self.openerp.pool.get('payment.mandate')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cursor = txn.cursor
        uid = txn.user

        mandate_vals = polissa_obj.update_mandate(cursor, uid, polissa_id)
        mandate_id = mandate_obj.create(cursor, uid, mandate_vals)

        return mandate_id

    def test_account_change_correctly(self):
        wizard_obj = self.openerp.pool.get('wizard.bank.account.change')
        modcontractual_obj = self.openerp.pool.get(
            'giscedata.polissa.modcontractual'
        )
        mandate_obj = self.openerp.pool.get('payment.mandate')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        conf_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # obtenim l'id de la variable de configuració per crear o modificar
            # la modificacio contractual.
            var_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer',
                'contract_modify_iban_modcon_type'
            )[1]

            # ens assegurem que ha de crear una modificació contractual
            conf_obj.write(cursor, uid, [var_id], {'value': 'new'})

            polissa_id = self.activar_polissa(txn)
            mandate_old_id = self.crear_mandato(txn, polissa_id)

            polissa_inst = polissa_obj.browse(cursor, uid, polissa_id)

            context = {'active_id': polissa_id}

            data_selected_str = "2016-06-06"
            data_selected = datetime.datetime.strptime(
                data_selected_str, "%Y-%m-%d"
            ).date()

            # Compte bancari actual
            bank_old = polissa_inst.bank
            bank_old_id = bank_old.id

            # per fer el canvi es necessari tenir un compte associat
            self.assertTrue(bank_old_id)
            propietari_id = partner_obj.search(
                cursor, uid, [], limit=1, context=context
            )[0]

            # també necessitem l'adreça del titular
            owner_address = address_obj.read(
                cursor, uid, [propietari_id], context=context
            )[0]['id']

            params = {
                'change_date': data_selected_str,
                'account_iban': 'ES9121000418450200051332',
                'mandate_scheme': 'core',
                'payment_mode': polissa_inst.payment_mode_id.id,
                'owner_address': owner_address,
                'account_owner': propietari_id
            }
            wiz_id = wizard_obj.create(
                cursor, uid, params, context=context
            )

            modcontractual_old_id = None
            for modcontractual in polissa_inst.modcontractuals_ids:
                if modcontractual.state == 'actiu':
                    modcontractual_old_id = modcontractual.id

            # llista ids de totes les modificacions contractuals
            modcontractual_ids_before = modcontractual_obj.search(
                cursor, uid, [('polissa_id', '=', polissa_id)], context=context
            )
            # Hi ha d'haver com a minim una modificacio contractual
            self.assertTrue(modcontractual_ids_before)

            # ----- Es fa el canvi de compte bancari -----
            wizard = wizard_obj.browse(cursor, uid, wiz_id, context=context)
            wizard.action_bank_account_change(context=context)

            context.update({'active_test': False})
            modcontractual_ids_after = modcontractual_obj.search(
                cursor, uid, [('polissa_id', '=', polissa_id)], context=context
            )

            # S'ha creat una modificacio contractual
            self.assertEqual(
                len(modcontractual_ids_after), len(modcontractual_ids_before) + 1
            )

            modcontractual_new_id = (
                    set(modcontractual_ids_after) - set(modcontractual_ids_before)
            ).pop()

            bank_new_id = polissa_obj.read(
                cursor, uid, [polissa_id], ['bank']
            )[0]['bank'][0]

            # el canvi de compte ha sigut efectiu:
            self.assertNotEqual(bank_old_id, bank_new_id)

            data_selected_minus_one_day = data_selected - datetime.timedelta(days=1)

            data_final_contracte_old_should_be = str(
                data_selected_minus_one_day
            )
            data_final_contracte_old_is = modcontractual_obj.read(
                cursor, uid, [modcontractual_old_id], ['data_final'],
                context=context
            )[0]['data_final']
            # - La modificacio contractual anterior, té data de fi a data
            #   del canvi -1 dia
            self.assertEqual(
                data_final_contracte_old_is, data_final_contracte_old_should_be
            )

            data_inici_contracte_new_should_be = data_selected_str
            data_inici_contracte_new_is = modcontractual_obj.read(
                cursor, uid, [modcontractual_new_id], ['data_inici'],
                context=context
            )[0]['data_inici']

            # - La modificació contractual actual té data d'inici, a data
            #   del canvi
            self.assertEqual(
                data_inici_contracte_new_is, data_inici_contracte_new_should_be
            )

            data_final_mandato_old_should_be = str(
                data_selected_minus_one_day
            )
            data_final_mandato_old_is = mandate_obj.read(
                cursor, uid, [mandate_old_id], ['date_end'], context=context
            )[0]['date_end']

            # - Mandato vell, té data de fi a data del canvi -1 dia
            self.assertEqual(
                data_final_mandato_old_is, data_final_mandato_old_should_be
            )

            reference = "giscedata.polissa," + str(polissa_id)
            mandate_new_id = mandate_obj.search(
                cursor, uid,
                [('reference', '=', reference), ('date_end', '=', None)]
            )[0]

            data_inici_mandato_new_should_be = data_selected_str
            data_inici_mandato_new_is = mandate_obj.read(
                cursor, uid, [mandate_new_id], ['date'], context=context
            )[0]['date']

            # - Mandato nou creat sense data de fi, i amb data d'inici a
            #   data del canvi
            self.assertEqual(
                data_inici_mandato_new_is, data_inici_mandato_new_should_be
            )

            # - Opcional: La modificació contractual actual té data de fi,
            # a data de fi del contracte

    def test_account_change_update_modcontractual(self):
        wizard_obj = self.openerp.pool.get('wizard.bank.account.change')
        modcontractual_obj = self.openerp.pool.get(
            'giscedata.polissa.modcontractual'
        )
        mandate_obj = self.openerp.pool.get('payment.mandate')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        conf_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # obtenim l'id de la variable de configuració per crear o modificar
            # la modificacio contractual.
            var_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer',
                'contract_modify_iban_modcon_type'
            )[1]

            # ens assegurem que ha d'actualitzar la modificacio contractual
            conf_obj.write(cursor, uid, [var_id], {'value': 'current'})

            polissa_id = self.activar_polissa(txn)
            mandate_old_id = self.crear_mandato(txn, polissa_id)

            polissa_inst = polissa_obj.browse(cursor, uid, polissa_id)

            context = {'active_id': polissa_id}

            data_selected_str = "2016-06-06"
            data_selected = datetime.datetime.strptime(
                data_selected_str, "%Y-%m-%d"
            ).date()

            # Compte bancari actual
            bank_old = polissa_inst.bank
            bank_old_id = bank_old.id

            # per fer el canvi es necessari tenir un compte associat
            self.assertTrue(bank_old_id)
            propietari_id = partner_obj.search(
                cursor, uid, [], limit=1, context=context
            )[0]

            # també necessitem l'adreça del titular
            owner_address = address_obj.read(
                cursor, uid, [propietari_id], context=context
            )[0]['id']

            params = {
                'change_date': data_selected_str,
                'account_iban': 'ES9121000418450200051332',
                'mandate_scheme': 'core',
                'payment_mode': polissa_inst.payment_mode_id.id,
                'owner_address': owner_address,
                'account_owner': propietari_id
            }
            wiz_id = wizard_obj.create(
                cursor, uid, params, context=context
            )

            # llista ids de totes les modificacions contractuals
            modcontractual_ids_before = modcontractual_obj.search(
                cursor, uid, [('polissa_id', '=', polissa_id)], context=context
            )
            # Hi ha d'haver com a minim una modificacio contractual
            self.assertTrue(modcontractual_ids_before)

            # ----- Es fa el canvi de compte bancari -----
            wizard = wizard_obj.browse(cursor, uid, wiz_id, context=context)
            wizard.action_bank_account_change(context=context)

            context.update({'active_test': False})
            modcontractual_ids_after = modcontractual_obj.search(
                cursor, uid, [('polissa_id', '=', polissa_id)], context=context
            )

            # no s'ha d'haver creat una nova modificacio contracttual
            self.assertEqual(
                len(modcontractual_ids_after), len(modcontractual_ids_before)
            )

            bank_new_id = polissa_obj.read(
                cursor, uid, [polissa_id], ['bank']
            )[0]['bank'][0]

            # el canvi de compte ha sigut efectiu:
            self.assertNotEqual(bank_old_id, bank_new_id)

            data_selected_minus_one_day = data_selected - datetime.timedelta(
                days=1)

            data_final_mandato_old_should_be = str(
                data_selected_minus_one_day
            )
            data_final_mandato_old_is = mandate_obj.read(
                cursor, uid, [mandate_old_id], ['date_end'], context=context
            )[0]['date_end']

            # - Mandato vell, té data de fi a data del canvi -1 dia
            self.assertEqual(
                data_final_mandato_old_is, data_final_mandato_old_should_be
            )

            reference = "giscedata.polissa," + str(polissa_id)
            mandate_new_id = mandate_obj.search(
                cursor, uid,
                [('reference', '=', reference), ('date_end', '=', None)]
            )[0]

            data_inici_mandato_new_should_be = data_selected_str
            data_inici_mandato_new_is = mandate_obj.read(
                cursor, uid, [mandate_new_id], ['date'], context=context
            )[0]['date']

            # - Mandato nou creat sense data de fi, i amb data d'inici a
            #   data del canvi
            self.assertEqual(
                data_inici_mandato_new_is, data_inici_mandato_new_should_be
            )
