SELECT f.polissa_id, sum(il.quantity)
FROM giscedata_facturacio_factura_linia fl
INNER JOIN account_invoice_line il
  ON il.id = fl.invoice_line_id
INNER JOIN giscedata_facturacio_factura f
  ON f.id = fl.factura_id
INNER JOIN account_invoice i
ON i.id = f.invoice_id
INNER JOIN account_journal journal
ON journal.id = i.journal_id
WHERE i.date_invoice between %s and %s
AND fl.tipus = 'energia' AND fl.isdiscount = False
AND journal.code like 'ENERGIA%%'
AND i.state in ('open', 'paid')
AND f.tipo_rectificadora in ('N', 'R', 'RA')
AND i.type = 'out_invoice'
AND i.refund_by_id IS NULL
GROUP BY f.polissa_id
