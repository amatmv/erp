SELECT f.polissa_id, sum(il.price_subtotal)
FROM giscedata_facturacio_factura_linia fl
INNER JOIN account_invoice_line il
  ON il.id = fl.invoice_line_id
INNER JOIN giscedata_facturacio_factura f
  ON f.id = fl.factura_id
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN product_product product
  ON product.id = il.product_id
INNER JOIN account_journal journal
  ON journal.id = i.journal_id
WHERE i.date_invoice between '2012-07-01' and '2013-02-28'
AND product.default_code in %s
AND journal.code like 'ENERGIA%%'
AND i.state in ('open', 'paid')
AND f.tipo_rectificadora in ('N', 'R', 'RA')
AND i.type = 'out_invoice'
AND f.id not in
(SELECT ref
  FROM giscedata_facturacio_factura f
  INNER JOIN account_invoice i
  ON i.id = f.invoice_id
  INNER JOIN account_journal journal
  ON journal.id = i.journal_id
  WHERE f.tipo_rectificadora in ('A', 'B', 'BRA')
  AND journal.code like 'ENERGIA%%'
  AND coalesce(ref, 0) <> 0)
group by f.polissa_id