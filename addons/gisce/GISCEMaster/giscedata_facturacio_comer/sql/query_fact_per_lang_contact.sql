SELECT
  count(f.id),
  f.enviat,
  coalesce(lang.name, '*'),
  CASE
    WHEN coalesce(address.email, '') = '' THEN false
    ELSE true
  END
FROM giscedata_facturacio_factura f
INNER JOIN account_invoice i
  ON i.id = f.invoice_id
INNER JOIN res_partner p
  ON p.id = i.partner_id
INNER JOIN res_partner_address address
  ON address.id = i.address_contact_id
LEFT JOIN res_lang lang
  ON lang.code = p.lang
WHERE
  lot_facturacio = %s
  AND per_enviar like '%%email%%'
GROUP BY
  coalesce(lang.name, '*'),
  f.enviat,
  CASE
    WHEN coalesce(address.email, '') = '' THEN false
    ELSE true
  END
ORDER BY
  coalesce(lang.name, '*'),
  f.enviat