# -*- coding: utf-8 -*-
from tools.translate import _

QUERY_RESUM_COMER_TITLES = [
    _(u"Distribuidora"),
    _(u"Ref. Distribuidora"),
    _(u"Comercialitzadora"),
    _(u"Cod. Comer."),
    _(u'Contracte'),
    _(u'Tipo pago'),
    _(u'Nom client'),
    _(u"CUPS"),
    _(u"Direcció"),
    _(u"Tarifa d'acces"),
    _(u"Tarifa de comercialitzadora"),
    _(u"factura"),
    _(u"Ref. factura"),
    _(u"Data factura"),
    _(u"tipus factura"),
    _(u"Data inicial"),
    _(u"Data final"),
    _(u"núm. díes"),
    _(u"vat"),
    _(u"energia_kw"),
    _(u"potencia"),
    _(u"descuentos_potencia"),
    _(u"energia"),
    _(u"descuentos_energia"),
    _(u"reactiva"),
    _(u"excesos"),
    _(u"altres conceptes"),
    _(u"altres sense refacturacio"),
    _(u"refacturacio"),
    _(u"refacturacio_t4"),
    _(u"refacturacio_t1"),
    _(u"lloguer"),
    _(u"fiança"),
    _(u"conceptes sense impostos"),
    _(u"total conceptes"),
    _(u"iva1"),
    _(u"Base IVA1"),
    _(u"total iva1"),
    _(u"iva2"),
    _(u"Base IVA2"),
    _(u"total iva2"),
    _(u"Base IESE IVA1"),
    _(u"total IESE IVA1"),
    _(u"Base IESE IVA2"),
    _(u"total IESE IVA2"),
    _(u"Base IESE"),
    _(u"total IESE"),
    _(u"base"),
    _(u"iva"),
    _(u"total"),
]


def add_fields_query_resum(field, fields_to_add):
    """
    Insert the list of fields: *fields_to_add* after the field *field*
    in the columns.
    :type field: str | unicode
    :type fields_to_add: list[str]
    """
    global QUERY_RESUM_COMER_TITLES
    pos = QUERY_RESUM_COMER_TITLES.index(field) + 1
    for field_name in reversed(fields_to_add):
        QUERY_RESUM_COMER_TITLES.insert(pos, field_name)
