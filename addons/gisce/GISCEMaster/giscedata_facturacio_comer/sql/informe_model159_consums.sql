/* Retorna de totes les pòlisses passades per paràmetre
   els consums facturats de cada mes i el total de 
   l'any indicat.
*/
select 
    polissa.id,
    round(coalesce(enero.consumo,0)) as enero,
    round(coalesce(febrero.consumo,0)) as febrero,
    round(coalesce(marzo.consumo,0)) as marzo,
    round(coalesce(abril.consumo,0)) as abril,
    round(coalesce(mayo.consumo,0)) as mayo,
    round(coalesce(junio.consumo,0)) as junio,
    round(coalesce(julio.consumo,0)) as julio,
    round(coalesce(agosto.consumo,0)) as agosto,
    round(coalesce(septiembre.consumo,0)) as septiembre,
    round(coalesce(octubre.consumo,0)) as octubre,
    round(coalesce(noviembre.consumo,0)) as noviembre,
    round(coalesce(diciembre.consumo,0)) as diciembre,
    round(coalesce(total.facturado,0),2) as facturado
FROM giscedata_polissa polissa
LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)enero
ON enero.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s 
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)febrero
ON febrero.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s 
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)marzo
ON marzo.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)abril
ON abril.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)mayo
ON mayo.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)junio
ON junio.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)julio
ON julio.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)agosto
ON agosto.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)septiembre
ON septiembre.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)octubre
ON octubre.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)noviembre
ON noviembre.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
inner join giscedata_facturacio_factura_linia fl
on fl.factura_id = f.id
inner join account_invoice_line il
ON fl.invoice_line_id = il.id
where to_char(i.date_invoice, 'MM/YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and fl.tipus = 'energia' and fl.isdiscount = False
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)diciembre
ON diciembre.polissa_id  = polissa.id

LEFT JOIN (SELECT polissa_id,sum(i.amount_total) as facturado from
giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id
where to_char(i.date_invoice, 'YYYY') = %s
and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
and f.id not in (select ref from giscedata_facturacio_factura f 
inner join account_invoice i on i.id = f.invoice_id 
inner join account_journal journal on journal.id = i.journal_id 
where i.date_invoice between %s and %s
and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
and coalesce(ref,0)<>0)
group by polissa_id)total
ON total.polissa_id  = polissa.id

where polissa.id in %s
