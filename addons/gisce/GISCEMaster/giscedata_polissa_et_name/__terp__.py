# -*- coding: utf-8 -*-
{
    "name": "Nom de la ET a les pòlisses",
    "description": """Afegeix la descripció de la ET que ha fet el GIS per topologia a les pòlisses""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_et_name_view.xml"
    ],
    "active": False,
    "installable": True
}
