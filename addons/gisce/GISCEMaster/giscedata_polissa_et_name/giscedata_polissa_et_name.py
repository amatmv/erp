# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_polissa(osv.osv):
  
  _name = 'giscedata.polissa'
  _inherit = 'giscedata.polissa'

  def _et_name(self, cr, uid, ids, field_name, arg, context):
    res = {}
    ids_sql = ','.join(map(str, map(int, ids)))
    cr.execute("""
select 
  p.id,
  coalesce(ct.descripcio,'') 
from 
  giscedata_polissa p 
left join giscedata_cups_ps c 
  on (p.cups = c.id) 
left join giscedata_cts ct 
  on (c.et = ct.name) 
where 
  p.id in (%s)""" % ids_sql)

    for r in cr.fetchall():
      res[r[0]] = r[1]
    return res

  _columns = {
    'et_name': fields.function(_et_name, method=True, type='char', string='Descripció ET', size=255),
  }

giscedata_polissa()
