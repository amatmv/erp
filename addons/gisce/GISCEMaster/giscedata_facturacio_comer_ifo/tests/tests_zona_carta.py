# coding=utf-8
from destral import testing
from destral.transaction import Transaction


class TestsZonaCarta(testing.OOTestCase):
    def test_set_back_values_zona_carta(self):
        zona_obj = self.openerp.pool.get('giscedata.polissa.carta.zona')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            params = {'name': 'AAAAAAAAAAAAAA'}
            zona_id = zona_obj.create(cursor, uid, params)

            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            zona_in_pol = polissa_obj.read(
                cursor, uid, pol_id,
                ['zona_carta', 'ordre_carta', 'zona_carta_id']
            )
            zona_in_pol.pop('id', None)
            keys_false = all(value is False for value in zona_in_pol.values())
            self.assertTrue(keys_false)

            polissa_obj.write(cursor, uid, pol_id, {'zona_carta_id': zona_id})

            zona_in_pol = polissa_obj.read(
                cursor, uid, pol_id,
                ['zona_carta', 'zona_carta_id']
            )

            zona_in_zone = zona_obj.read(cursor, uid, zona_id, ['name'])

            self.assertEqual(zona_in_pol['zona_carta'], zona_in_zone['name'])
            self.assertEqual(zona_in_pol['zona_carta_id'][0], zona_id)