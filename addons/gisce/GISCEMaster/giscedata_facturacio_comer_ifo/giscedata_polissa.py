# -*- coding: utf-8 -*-
"""Mòdul giscedata_polissa (comercialitzadora)."""
from osv import osv, fields


class GiscedataPolissaCartaZona(osv.osv):
    _name = "giscedata.polissa.carta.zona"
    _description = "Zona carta d'una polissa"

    _columns = {
        'name': fields.char('Zona carta', size=60, select=True),
    }

GiscedataPolissaCartaZona()


class GiscedataPolissa(osv.osv):
    """Pòlissa (Comercialitzadora)."""
    
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'
    _description = "Polissa d'un Client"

    def _get_pol_zona_ids(self, cursor, uid, ids, context=None):
        pol_obj = self.pool.get('giscedata.polissa')
        if not isinstance(ids, list):
            ids = [ids]
        return pol_obj.search(
            cursor, uid, [('zona_carta_id', 'in', ids)], context=context)

    def _zona_get(self, cursor, uid, ids, field_name, args, context=None):
        zona_obj = self.pool.get('giscedata.polissa.carta.zona')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = dict.fromkeys(ids, False)
        for zona in self.read(cursor, uid, ids, ['zona_carta_id']):
            z_id = zona.get('zona_carta_id', False)
            if z_id:
                res[zona['id']] = z_id[1]
        return res

    _STORE_PROC_ZONA_CARTA_FIELD = {
        'giscedata.polissa': (
            lambda self, cursor, uid, ids, context=None: ids,
            ['zona_carta_id'], 60),
        'giscedata.polissa.carta.zona': (_get_pol_zona_ids, ['name'], 60),
    }

    _columns = {
        'zona_carta': fields.function(
            _zona_get,
            size=60,
            method=True,
            string='Zona carta',
            type='char',
            store=_STORE_PROC_ZONA_CARTA_FIELD,
            select=True
        ),
        'zona_carta_id': fields.many2one('giscedata.polissa.carta.zona',
                                         'Zona carta', ),
        'ordre_carta': fields.char('Ordre carta', size=60, select=True),

    }

    def wkf_contracte(self, cursor, uid, ids):
        res = super(GiscedataPolissa, self).wkf_contracte(cursor, uid, ids)
        for polissa in self.browse(cursor, uid, ids):
            md_actual = polissa.modcontractual_activa
            md_antiga = md_actual.modcontractual_ant
            if not md_antiga:
                continue
            postal = md_actual.enviament.find('postal') >= 0
            if not postal:
                continue
            enrutable = md_actual.direccio_notificacio.id_municipi.enrutable
            if not enrutable:
                continue
            addr_actual = md_actual.direccio_notificacio.street
            addr_antiga = md_antiga.direccio_notificacio.street
            canvi_addr = addr_actual != addr_antiga
            if canvi_addr:
                polissa.write({'zona_carta': False, 'ordre_carta': False})
        return res

GiscedataPolissa()

