# -*- coding: utf-8 -*-
{
    "name": "GISCE Facturació Comer",
    "description": """Wizard per imprimir les factures segons zona i ordre.""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Facturació",
    "depends":[
        "base",
        "oorq",
        "giscedata_facturacio_comer",
        "giscedata_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_imprimir_factures_view.xml",
        "wizard/wizard_reordenar_zona_view.xml",
        "giscedata_polissa_view.xml",
        "res_municipi_view.xml",
        "giscedata_facturacio_comer_ifo_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
