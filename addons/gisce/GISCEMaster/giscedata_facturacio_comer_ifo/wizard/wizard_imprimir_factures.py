# -*- coding: utf-8 -*-
"""
Wizard per imprimir les factures ordenades
==========================================

LERSA demana que les factures de comercialitzadora s'imprimeixin segons la
ruta i l'ordre de lectura de distribuidora.

"""

import base64
# cStringIO is faster :)
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO
import logging
import os
import shutil
import tempfile
import threading
import zipfile
import netsvc

try:
    from ast import literal_eval
except ImportError:
    literal_eval = eval

import pooler
from osv import fields, osv
from tools.translate import _
from tools import config
from oorq.decorators import job
from oorq.oorq import JobsPool
from giscedata_facturacio.defs import TIPO_RECTIFICADORA_SELECTION

import pypdftk
from time import strftime

ESTATS_IMP_FACTURES = [
    ('zona', 'zona'),
    ('resum', 'resum'),
    ('working', 'Working'),
    ('fitxer', 'fitxer')
]

INVOICES_STATES = [
    ('draft', _('Esborrany')),
    ('open', _('Obertes')),
    ('paid', _('Realitzades')),
    ('cancel', _(u'Cancel·lades')),
    ('all', _('Totes'))
]

TIPUS_FACTURES = TIPO_RECTIFICADORA_SELECTION + [('all', _('Totes'))]

class ProgressJobsPool(JobsPool):

    def __init__(self, wizard):
        self.wizard = wizard
        super(ProgressJobsPool, self).__init__()
    
    @property
    def all_done(self):
        logger = logging.getLogger('openerp.ifo.progress')
        logger.info('Progress: {0}/{1} {2}%'.format(
            self._num_jobs_done, self._num_jobs, self.progress
        ))
        self.wizard.write({'zone_progress': self.progress})
        return super(ProgressJobsPool, self).all_done


class WizardImprimirFactures(osv.osv_memory):
    """Wizard per imprimir les factures ordenades
    """
    _name = "imprimir.factures"

    def _get_zones(self, cursor, uid, context=None):
        """Retorna les diferents zones i l'especial 'Totes'.

        Això ens valdrà per decidir quines factures trobar.
        """
        cursor.execute("""SELECT distinct zona_carta
                          FROM giscedata_polissa
                          WHERE zona_carta IS NOT NULL
                          ORDER BY zona_carta ASC""")
        return [('zona_' + zona[0], zona[0]) for zona in cursor.fetchall()]

    def _get_lots(self, cursor, uid, context=None):
        """Retorna els lots pels què hi ha factures
        """
        cursor.execute("""SELECT distinct lot_facturacio 
                          FROM giscedata_facturacio_factura
                          WHERE lot_facturacio IS NOT null""")
        lots_ids = [per[0] for per in cursor.fetchall()]
        if not lots_ids:
            return []
        cursor.execute("""SELECT id, name
                          FROM giscedata_facturacio_lot
                          WHERE id IN %s 
                          ORDER BY data_final DESC""", (tuple(lots_ids),))
        return cursor.fetchall()

    def get_selected_zones(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        vals = self.read(cursor, uid, ids, context=context)[0]
        if vals['zona'] == 'select':
            zones = tuple(
                k.lstrip('zona_') for k, v in vals.items()
                if k.startswith('zona_') and v
            )
        else:
            zones = tuple(
                k.lstrip('zona_') for k, v in vals.items()
                if k.startswith('zona_')
            )
        return zones

    def go_start(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        self.write(cursor, uid, ids, {'state': ESTATS_IMP_FACTURES[0]})
        return True

    def get_invoices(self, cursor, uid, ids, context=None):
        """Cerca les factures de comercial segons lot de facturació i període
        i en guarda els IDs ordenats segons la zona i l'ordre dels CUPS
        corresponents de distribuidora a context['factures_imprimir']
        """
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids, context)[0]

        factures_ordenades = {}
        # Agafem les factures ordenades per l'ordre i zona del contracte

        zones = wiz.get_selected_zones()
        select_header = '''SELECT p.zona_carta, f.id
                         FROM giscedata_facturacio_factura f
                         LEFT JOIN giscedata_polissa p on (f.polissa_id=p.id)
                         LEFT JOIN account_invoice i on (f.invoice_id = i.id)
                        '''
        order_by = """ ORDER BY p.zona_carta ASC, p.ordre_carta ASC"""
        where_factures = ' '
        if wiz.estat_factures != 'all':
            where_factures += " AND i.state = '{state}'".format(
                state=wiz.estat_factures
            )
        if wiz.diari_factures != 'all':
            where_factures += " AND i.journal_id = {journal}".format(
                journal=wiz.diari_factures
            )
        if wiz.buscar_per == 'lot':
            sql_date = " "
            if wiz.data_inici:
                sql_date += " AND i.date_invoice >= '{0}' ".format(
                    wiz.data_inici
                )
            if wiz.data_final:
                sql_date += " AND i.date_invoice <= '{0}' ".format(
                    wiz.data_final
                )
            if wiz.zona == 'sz':
                sql = select_header + """
                         WHERE f.lot_facturacio = %s AND p.zona_carta IS NULL
                         AND p.enviament != 'email'
                         AND i.type in ('out_invoice', 'out_refund') """
                sql += sql_date
                sql += where_factures + order_by
                cursor.execute(sql, (wiz.lot,))
            else:
                sql = select_header + """
                         WHERE f.lot_facturacio = %s AND p.zona_carta IN %s
                         AND p.enviament != 'email'
                         AND i.type in ('out_invoice', 'out_refund')"""
                sql += sql_date
                sql += where_factures + order_by
                cursor.execute(sql, (wiz.lot, zones))
        elif wiz.buscar_per == 'active_ids':
            sql = select_header + """
                WHERE f.id in %s
                AND p.enviament != 'email'
                AND i.type in ('out_invoice', 'out_refund')
            """
            active_ids = context.get('active_ids', [])
            if wiz.zona == 'sz':
                sql += """
                AND p.zona_carta IS NULL
                """
                sql += where_factures + order_by
                cursor.execute(sql, (tuple(active_ids), ))
            else:
                sql += """
                   AND p.zona_carta IN %s
                   """
                sql += where_factures + order_by
                cursor.execute(sql, (tuple(active_ids), zones))

        else:
            if wiz.zona == 'sz':
                sql = select_header + """
                         WHERE i.date_invoice >= %s AND i.date_invoice <= %s
                         AND p.zona_carta IS NULL
                         AND p.enviament != 'email'
                         AND i.type in ('out_invoice', 'out_refund')
                         """
                sql += where_factures
                if wiz.start_invoice_number:
                    sql += """ AND i.number >= '{0}' """.format(
                        wiz.start_invoice_number)
                if wiz.end_invoice_number:
                    sql += """ AND i.number <= '{0}' """.format(
                        wiz.end_invoice_number)
                sql += order_by
                cursor.execute(sql, (wiz.data_inici, wiz.data_final))
            else:
                sql = select_header + """
                         WHERE i.date_invoice >= %s AND i.date_invoice <= %s
                         AND p.zona_carta IN %s
                         AND p.enviament != 'email'
                         AND i.type in ('out_invoice', 'out_refund')"""
                sql += where_factures
                if wiz.start_invoice_number:
                    sql += """ AND i.number >= '{0}' """.format(
                        wiz.start_invoice_number)
                if wiz.end_invoice_number:
                    sql += """ AND i.number <= '{0}' """.format(
                        wiz.end_invoice_number)
                sql += order_by
                cursor.execute(sql, (wiz.data_inici, wiz.data_final, zones))
        n_factures = 0
        for row in cursor.fetchall():
            zona = row[0] or 'sz'
            factures_ordenades.setdefault(zona, [])
            factures_ordenades[zona].append(row[1])
            n_factures += 1
        wiz.write({'state': 'resum', 'n_factures': n_factures,
                   'factures': str(factures_ordenades)})
        return True

    def print_invoices(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        print_thread = threading.Thread(
            target=self.print_invoices_threaded,
            args=(cursor.dbname, uid, ids, context)
        )
        print_thread.start()
        self.write(cursor, uid, ids, {'state': 'working'})
        return True

    def print_invoices_threaded(self, dbname, uid, ids, context=None):
        """Crida el report per cadascuna dels IDs de factures a 
        wiz.factures i els ajunta en un sol PDF.
        """
        logger = logging.getLogger('openerp.ifo.print_invoices')
        if not context:
            context = {}
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]

        cr = pooler.get_db(dbname).cursor()
        try:
            wiz = self.browse(cr, uid, ids)

            factures = literal_eval(wiz.factures)
            generated_files = {}
            failed_invoice = []
            report = 'report.{0}'.format(wiz.report.report_name)
            # iterem sobre les factures, creant els PDF al disc per després
            # fer el fitxer ZIP
            zone_done = 1.0
            for zona in factures:
                j_pool = ProgressJobsPool(wiz)
                dirname = '/tmp/pdfs_zona_%s' % (zona,)
                # si existeix el directori, l'eliminem, així segur que comencem
                # de zero
                if os.path.exists(dirname):
                    if os.path.isdir(dirname):
                        shutil.rmtree(dirname)
                    else:
                        os.remove(dirname)
                os.mkdir(dirname)
                ordre = 0
                generated_files.setdefault(zona, [])
                for fid in factures[zona]:
                    j_pool.add_job(self.render_to_file(
                        cr, uid, [fid], report, dirname, zona, ordre, context
                    ))
                    ordre += 1
                j_pool.join()
                for status, result in j_pool.results.values():
                    if not status:
                        generated_files[zona].append(result)
                    else:
                        failed_invoice.extend(result)
                generated_files[zona] = sorted(generated_files[zona])
                logger.info('Zone: {0} -> {1} pdfs rendered'.format(
                    zona, len(generated_files[zona]))
                )
                wiz.write({'progress': (zone_done / len(factures)) * 100})
                zone_done += 1
            zip_path = tempfile.mkstemp()[1]
            zip_file_obj = open(zip_path, 'w')
            factures_arc = zipfile.ZipFile(zip_file_obj, 'w', allowZip64=True)
            zones_len = {}
            current_date = strftime('%Y-%m-%d')
            for zona in factures:
                idx = 1
                zones_len[zona] = len(generated_files[zona])
                while generated_files[zona]:
                    z_files = generated_files[zona][:wiz.factures_per_pdf]
                    del generated_files[zona][:wiz.factures_per_pdf]
                    # fitxer on ajuntar tots els PDFs
                    ffile_name = _('/tmp/factures_zona_{0}_{1}_{2}.pdf').format(
                        zona, current_date, ('%05d' % idx)
                    )
                    ffile_name = ffile_name.decode('utf-8').encode('ascii',
                                                                   'ignore')
                    pypdftk.concat(z_files, out_file=ffile_name)
                    factures_arc.write(ffile_name)
                    idx += 1
            factures_arc.close()
            if wiz.zona == 'all':
                zones = _('Totes').lower()
            else:
                zones = '_'.join(wiz.get_selected_zones())
            # limit filename length
            zip_name = _('factures_{}_{}.zip').format(zones[:200], current_date)
            values = {'name': zip_name, 'state': 'fitxer'}
            cfg_obj = self.pool.get('res.config')
            download_path = cfg_obj.get(
                cr, uid, 'comer_ifo_download_path', False)
            download_url = cfg_obj.get(
                cr, uid, 'comer_ifo_download_url', False)
            if download_path and download_url:
                new_path = os.path.join(download_path, zip_name)
                shutil.move(zip_path, new_path)
                os.chmod(new_path, 0o777)
                values['download_url'] = '{}/{}'.format(download_url, zip_name)
            else:
                zipb64 = base64.b64encode(open(zip_file_obj.name).read())
                values['fitxer'] = zipb64
                values['download_url'] = False

            zonas_info = u''
            for z in zones_len:
                zonas_info += _(u'{} Total imprés:{}\n').format(z, zones_len[z])

            info = zonas_info
            if failed_invoice:
                info += _(u"Les següents factures no s'han pogut generar\n")
                fact_obj = self.pool.get('giscedata.facturacio.factura')
                for fact in fact_obj.browse(cr, uid, failed_invoice):
                    info += _(u'Factura Núm:{0} (id:{1}) \n').format(
                        fact.invoice_id.number, fact.id
                    )

            values['info'] = info
            wiz.write(values)
        finally:
            cr.close()
        return True

    @job(queue=config.get('invoice_render_queue', 'invoice_render'),
         result_ttl=24 * 3600)
    def render_to_file(self, cursor, uid, fids, report, dirname, zona, ordre,
                       context=None):
        """Return a tuple of status (0: OK, 1: Failed) and the invoice path.
        """
        if context is None:
            context = {}
        try:
            output_file_name = '%s_%s.pdf' % (zona, str(ordre).zfill(4))
            report = netsvc.service_exist(report)
            values = {
                'model': 'giscedata.facturacio.factura',
                'id': fids,
                'report_type': 'pdf'
            }
            content = report.create(cursor, uid, fids, values, context)[0]
            # Escriure report a "fitxer"
            fitxer_name = '%s/%s' % (dirname, output_file_name)
            with open(fitxer_name, 'wb') as f:
                f.write(content)
            return 0, fitxer_name
        except Exception:
            import traceback
            traceback.print_exc()
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()
            return 1, fids

    def _get_journals(self, cursor, uid, context=None):
        journal_obj = self.pool.get('account.journal')
        jid = journal_obj.search(
            cursor, uid, ['|', ('code', '=like', 'ENERGIA%'),
                            ('code', '=', 'CONTRATACION')]
        )
        journals = journal_obj.read(
            cursor, uid, jid, ['name'], context=context)
        result = [(j['id'], j['name']) for j in journals]
        result += [('all', _('Tots'))]
        return result


    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        from lxml import etree
        zones = self._get_zones(cursor, uid, context)
        self._columns.update(
            dict((z[0], fields.boolean(z[1])) for z in zones)
        )
        res = super(WizardImprimirFactures, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        res['fields'].update(
            dict((z[0], {'type': 'boolean', 'string': z[1]}) for z in zones)
        )
        xml = etree.fromstring(res['arch'])
        group = xml.xpath("//group[@name='select_zones']")
        for z in zones:
            group[0].append(etree.fromstring(
                '<field name="{0}"/>'.format(z[0])
            ))
        res['arch'] = etree.tostring(xml)
        return res

    _columns = {
        'report': fields.many2one(
            'ir.actions.report.xml',
            'Report',
            domain=[('model', '=', 'giscedata.facturacio.factura')]
        ),
        'name': fields.char('Nom', size=300),
        'state': fields.selection(ESTATS_IMP_FACTURES, 'Estat'),
        'zona': fields.selection([
            ('select', 'Seleccionar'), ('all', 'Totes'), ('sz', 'Sense zona')
        ], 'Zona', required=True),
        'lot': fields.selection(_get_lots, 'Lot'),
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'start_invoice_number': fields.char('Factura inicial', size=128),
        'end_invoice_number': fields.char('Factura final', size=128),
        'buscar_per': fields.selection([
            ('data', 'Data'),
            ('lot', 'Lot'),
            ('active_ids', 'Filtre actual')
        ], 'Filtrar factures per'
        ),
        'estat_factures': fields.selection(
            INVOICES_STATES, 'Estat'
        ),
        'diari_factures': fields.selection(
            _get_journals, 'Diari'
        ),
        'n_factures': fields.integer('Factures a imprimir'),
        'factures': fields.text('Factures'),
        'fitxer': fields.binary('Factures generades'),
        'info': fields.text('Informació'),
        'progress': fields.float(u'Progrés general'),
        'zone_progress': fields.float(u'Progrés zona'),
        'factures_per_pdf': fields.integer(
            u'Número factures per PDF',
            help=u"Número màxim de factures per cada PDF. "
                 u"Per evitar generar PDFs amb moltes pàgines"
        ),
        'download_url': fields.char('Download', size=1000)
    }

    def _default_report(self, cursor, uid, context=None):
        if context is None:
            context = {}
        report_obj = self.pool.get('ir.actions.report.xml')
        return report_obj.search(cursor, uid, [
            ('report_name', '=', 'giscedata.facturacio.factura'),
            ('model', '=', 'giscedata.facturacio.factura')
        ])[0]
    _defaults = {
        'report': _default_report,
        'zona': lambda *a: 'select',
        'state': lambda *a: ESTATS_IMP_FACTURES[0],
        'estat_factures': lambda *a: INVOICES_STATES[-1],
        'diari_factures': lambda *a: 'all',
        'n_factures': lambda *a: 0,
        'factures_per_pdf': 1000,
    }

WizardImprimirFactures()
