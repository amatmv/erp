# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class WizardReordenarZona(osv.osv_memory):
    ''' Wizard per introduir o llevar un cups d'una zona'''

    _name = 'wizard.reordenar.zona'

    def inserta(self, cursor, uid, ids, zona, ordre, src_pol_id, context=None):
        '''funcio que inserta un cups en una zona'''

        pol_obj = self.pool.get("giscedata.polissa")
        search_params = [
            ('zona_carta_id', '=', zona),
            ('ordre_carta', '>=', ordre)]

        pol_ids = pol_obj.search(
            cursor, uid, search_params, order='ordre_carta asc'
        )
        for polissa in pol_obj.browse(cursor, uid, pol_ids):
            polissa.write({
                'ordre_carta': '%04d' % (int(polissa.ordre_carta) + 1)
            }, context={'sync': False})
        # Una vez movidos todos los cups posteriores, escribimos en el cups
        polissa = pol_obj.browse(cursor, uid, src_pol_id)
        polissa.write({
            'zona_carta_id': zona,
            'ordre_carta': ordre
        }, context={'sync': False})
        return True

    def confirmar(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'end'}, context=context)

    def moure(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])

        if wizard.despres_de and not wizard.zona_nova:
            # Force to propose
            wizard.propose()
            wizard = self.browse(cursor, uid, ids[0])
        if wizard.zona_nova:
            self.inserta(
                cursor, uid, wizard.id,
                wizard.zona_nova.id,
                wizard.ordre_nou.rjust(4, '0'),
                wizard.polissa_id.id
            )
        return {}

    def propose(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        pol_obj = self.pool.get('giscedata.polissa')
        if not wiz.despres_de:
            pol = wiz.polissa_id
            pol_ids = pol_obj.search(cursor, uid, [
                ('direccio_notificacio.nv', '=', pol.direccio_notificacio.nv),
                ('direccio_notificacio.pnp', '>=', pol.direccio_notificacio.pnp)
            ])
            if pol_ids:
                polisses = pol_obj.browse(cursor, uid, pol_ids)
                polissa = sorted(
                    polisses,
                    key=lambda a: getattr(a.direccio_notificacio, 'pnp')
                )[0]
                wiz.write({
                    'zona_nova': polissa.zona_carta_id.id,
                    'ordre_nou': '%04d' % (int(polissa.ordre_carta) + 1)
                })
            return True
        else:
            wiz.write({
                'zona_nova': wiz.despres_de.zona_carta_id.id,
                'ordre_nou': '%04d' % (int(wiz.despres_de.ordre_carta) + 1)
            })
        return False

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa',
            'Contracte',
            required=True,
            readonly=False
        ),
        'despres_de': fields.many2one(
            'giscedata.polissa',
            'Despŕes de'
        ),
        'zona_ant': fields.related(
            'polissa_id',
            'zona_carta_id',
            type='many2one',
            relation='giscedata.polissa',
            string='Zona',
            readonly=True
        ),
        'ordre_ant': fields.related(
            'polissa_id',
            'ordre_carta',
            type='char',
            relation='giscedata.polissa',
            string='Ordre',
            readonly=True
        ),
        'zona_nova': fields.many2one(
            'giscedata.polissa.carta.zona',
            'Zona nova',
            help=u"En blanc per no més llevar de la zona actual\n"
                 u"sense incloure el cups en una altra zona"
        ),
        'ordre_nou': fields.char(
            'Ordre nou',
            size=20,
            required=False,
            readonly=False
        ),
        'state': fields.selection([
            ('init', 'Init'), ('end', 'End')
            ], 'Estat', size=5
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
    }
WizardReordenarZona()
