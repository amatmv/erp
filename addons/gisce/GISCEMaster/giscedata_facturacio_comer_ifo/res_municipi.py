from osv import osv, fields


class ResMunicipi(osv.osv):
    _name = 'res.municipi'
    _inherit = 'res.municipi'

    _columns = {
        'enrutable': fields.boolean('Enrutable')
    }

    _defaults = {
        'enrutable': lambda *a: 0,
    }

ResMunicipi()
