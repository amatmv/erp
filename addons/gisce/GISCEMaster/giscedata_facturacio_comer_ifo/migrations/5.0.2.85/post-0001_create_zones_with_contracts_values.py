# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Create new carta zona on giscedata_polissa_carta_zona table '
                'for any distict zona_carta on contracts')

    query = '''
            INSERT INTO giscedata_polissa_carta_zona (name)
            SELECT DISTINCT zona_carta from giscedata_polissa
            WHERE zona_carta is not null
    '''

    cursor.execute(query)

    query = '''
            UPDATE giscedata_polissa
                SET zona_carta_id = z.id
                FROM giscedata_polissa_carta_zona AS z
                    WHERE giscedata_polissa.zona_carta = z.name
        '''

    cursor.execute(query)

    logger.info('Created zones successfully!!')


def down(cursor, installed_version):
    pass


migrate = up