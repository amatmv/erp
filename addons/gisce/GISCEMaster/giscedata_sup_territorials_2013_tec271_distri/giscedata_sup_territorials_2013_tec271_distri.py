# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from datetime import date as datetime_date
from tools.translate import _
from osv.osv import except_osv
import pandas as pd
from StringIO import StringIO
from zipfile import ZIP_DEFLATED, ZipFile
from base64 import b64encode
from defs_suplementos_territoriales_2013_tec_271 import *
from gestionatr.defs import TABLA_107
from copy import deepcopy
from comentarios_suplementos_retrofix import SuplementosTerritorialesComments
from numpy import isnan, sum
from tqdm import tqdm
import numpy as np


class GiscedataSuplementsTerritorials2013Tec271Distri(object):
    # Codigos referentes a las comunidades afectadas
    codi_comunitats = [
        '01',  # Andalucía
        '02',  # Aragón
        '03',  # Asturais
        '06',  # Cantabria
        '07',  # Castilla y león
        '09',  # Cataluña
        '11',  # Extremadura
        '12',  # Galicia
        '13',  # Madrid
        '14',  # Murcia
        '15',  # Navarra
    ]

    # Periodos para precios de los términos de potencia y energía activa
    periodes_eval = [
        ('2013-01-01', '2013-07-31'),
        ('2013-08-01', '2013-12-31'),
    ]

    descr_calcul = (
        "0000"  # Información Texto libre
        "{potencia_p1}"  # 6 Campos 14 Caracteres (P/Periodo W) 1-8
        "{potencia_p2}"  # 6 Campos 14 Caracteres (P/Periodo W) 8-12
        "{potencia}"  # 1 Campo 14 Caracteres (Potencia 2013 W)
        "{importe_potencia}"  # 1 Campo 13 Caracteres (Importe 10:2)
        "{energia_p1}"  # 6 Campos 14 Caracteres (E/Periodo kWh 11:2) 1-8
        "{energia_p2}"  # 6 Campos 14 Caracteres (E/Periodo kWh 11:2) 8-12
        "{energia}"  # 1 Campo 14 Caracteres (Energia 2013 kWh 11:2)
        "{importe_energia}"  # 1 Campo 13 Caracteres (Importe 10:2)
        # "{}"                  2 Caracteres (Num Termes)
        # "{}"                  2 Caracters (Num Termes pendents)
    )

    consum_buit = {
        'periode': (False, False),
        'tarifa': False,
        'potencia': {
            'consum': {
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            'notes': [
                {
                    'p1': 0,
                    'p2': 0,
                    'p3': 0,
                    'p4': 0,
                    'p5': 0,
                    'p6': 0,
                },
                {
                    'p1': 0,
                    'p2': 0,
                    'p3': 0,
                    'p4': 0,
                    'p5': 0,
                    'p6': 0,
                },
                {
                    'p1': 0,
                    'p2': 0,
                    'p3': 0,
                    'p4': 0,
                    'p5': 0,
                    'p6': 0,
                }
            ],
            'total': 0
        },
        'energia': {
            'consum': {
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            'notes': {
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            'total': 0
        },
    }

    suplementos = {
        '01': {  # Andalucía
            'comunidad': '01',
            'potencia': [
                potencia_andalucia_period_1, potencia_andalucia_period_2
            ],
            'energia': [energia_andalucia_period_1, energia_andalucia_period_2],
        },
        '02': {  # Aragón
            'comunidad': '02',
            'potencia': [potencia_aragon_period_1, potencia_aragon_period_2],
            'energia': [energia_aragon_period_1, energia_aragon_period_2],
        },
        '03': {  # Asturais
            'comunidad': '03',
            'potencia': [potencia_ast_period_1, potencia_ast_period_2],
            'energia': [energia_ast_period_1, energia_ast_period_2],
        },
        '06': {  # Cantabria
            'comunidad': '06',
            'potencia': [
                potencia_cantabria_period_1, potencia_cantabria_period_2
            ],
            'energia': [energia_cantabria_period_1, energia_cantabria_period_2],
        },
        '07': {  # Castilla y león
            'comunidad': '07',
            'potencia': [potencia_cale_period_1, potencia_cale_period_2],
            'energia': [energia_cale_period_1, energia_cale_period_2],
        },
        '09': {  # Cataluña
            'comunidad': '09',
            'potencia': [potencia_cat_period_1, potencia_cat_1_period_2],
            'energia': [energia_cat_period_1, energia_cat_period_2],
        },
        '11': {  # Extremadura
            'comunidad': '11',
            'potencia': [potencia_ext_period_1, potencia_ext_period_2],
            'energia': [energia_ext_period_1, energia_ext_period_2],
        },
        '12': {  # Galicia
            'comunidad': '12',
            'potencia': [potencia_gal_period_1, potencia_gal_period_2],
            'energia': [energia_gal_period_1, energia_gal_period_2],
        },
        '13': {  # Madrid
            'comunidad': '13',
            'potencia': [potencia_mad_period_1, potencia_mad_period_2],
            'energia': [energia_mad_period_1, energia_mad_period_2],
        },
        '14': {  # Murcia
            'comunidad': '14',
            'potencia': [potencia_mur_period_1, potencia_mur_period_2],
            'energia': [energia_mur_period_1, energia_mur_period_2],
        },
        '15': {  # Navarra
            'comunidad': '15',
            'potencia': [potencia_nav_period_1, potencia_nav_period_2],
            'energia': [energia_nav_period_1, energia_nav_period_2],
        },

    }

    export_valid_header = [
        u'CUPS',
        u'TarifaATRFact',
        u'FechaDesde',
        u'FechaHasta',
        u'NumeroDias',
        u'MediaPondPotenciaAFacturar_P1 (W)',
        u'MediaPondPotenciaAFacturar_P2 (W)',
        u'MediaPondPotenciaAFacturar_P3 (W)',
        u'MediaPondPotenciaAFacturar_P4 (W)',
        u'MediaPondPotenciaAFacturar_P5 (W)',
        u'MediaPondPotenciaAFacturar_P6 (W)',
        u'SuplementoPotencia_P1 (€/W)',
        u'SuplementoPotencia_P2 (€/W)',
        u'SuplementoPotencia_P3 (€/W)',
        u'SuplementoPotencia_P4 (€/W)',
        u'SuplementoPotencia_P5 (€/W)',
        u'SuplementoPotencia_P6 (€/W)',
        u'ImporteSuplementoTerminoPotencia_P1 (€)',
        u'ImporteSuplementoTerminoPotencia_P2 (€)',
        u'ImporteSuplementoTerminoPotencia_P3 (€)',
        u'ImporteSuplementoTerminoPotencia_P4 (€)',
        u'ImporteSuplementoTerminoPotencia_P5 (€)',
        u'ImporteSuplementoTerminoPotencia_P6 (€)',
        u'ImporteTotalSuplementoTerminoPotencia (€)',
        u'ValorEnergiaActiva_P1 (kWh)',
        u'ValorEnergiaActiva_P2 (kWh)',
        u'ValorEnergiaActiva_P3 (kWh)',
        u'ValorEnergiaActiva_P4 (kWh)',
        u'ValorEnergiaActiva_P5 (kWh)',
        u'ValorEnergiaActiva_P6 (kWh)',
        u'SuplementoEnergia_P1 (€/kWh)',
        u'SuplementoEnergia_P2 (€/kWh)',
        u'SuplementoEnergia_P3 (€/kWh)',
        u'SuplementoEnergia_P4 (€/kWh)',
        u'SuplementoEnergia_P5 (€/kWh)',
        u'SuplementoEnergia_P6 (€/kWh)',
        u'ImporteSuplementoTerminoEnergia_P1 (€)',
        u'ImporteSuplementoTerminoEnergia_P2 (€)',
        u'ImporteSuplementoTerminoEnergia_P3 (€)',
        u'ImporteSuplementoTerminoEnergia_P4 (€)',
        u'ImporteSuplementoTerminoEnergia_P5 (€)',
        u'ImporteSuplementoTerminoEnergia_P6 (€)',
        u'ImporteTotalSuplementoTerminoEnergia (€)',
        u'ImporteTotalARegularizar (€)'
    ]

    valid_header_conv_dict = {
        u'CUPS': u'CUPS',
        u'TarifaATRFact': u'TarifaATRFact',
        u'FechaDesde': u'FechaDesde',
        u'FechaHasta': u'FechaHasta',
        u'MediaPondPotenciaAFacturar_P1(W)': u'MediaPondPotenciaAFacturar_P1 (W)',
        u'MediaPondPotenciaAFacturar_P2(W)': u'MediaPondPotenciaAFacturar_P2 (W)',
        u'MediaPondPotenciaAFacturar_P3(W)': u'MediaPondPotenciaAFacturar_P3 (W)',
        u'MediaPondPotenciaAFacturar_P4(W)': u'MediaPondPotenciaAFacturar_P4 (W)',
        u'MediaPondPotenciaAFacturar_P5(W)': u'MediaPondPotenciaAFacturar_P5 (W)',
        u'MediaPondPotenciaAFacturar_P6(W)': u'MediaPondPotenciaAFacturar_P6 (W)',
        u'SuplementoPotencia_P1(€/W)': u'SuplementoPotencia_P1 (€/W)',
        u'SuplementoPotencia_P2(€/W)': u'SuplementoPotencia_P2 (€/W)',
        u'SuplementoPotencia_P3(€/W)': u'SuplementoPotencia_P3 (€/W)',
        u'SuplementoPotencia_P4(€/W)': u'SuplementoPotencia_P4 (€/W)',
        u'SuplementoPotencia_P5(€/W)': u'SuplementoPotencia_P5 (€/W)',
        u'SuplementoPotencia_P6(€/W)': u'SuplementoPotencia_P6 (€/W)',
        u'ValorEnergiaActiva_P1(kWh)': u'ValorEnergiaActiva_P1 (kWh)',
        u'ValorEnergiaActiva_P2(kWh)': u'ValorEnergiaActiva_P2 (kWh)',
        u'ValorEnergiaActiva_P3(kWh)': u'ValorEnergiaActiva_P3 (kWh)',
        u'ValorEnergiaActiva_P4(kWh)': u'ValorEnergiaActiva_P4 (kWh)',
        u'ValorEnergiaActiva_P5(kWh)': u'ValorEnergiaActiva_P5 (kWh)',
        u'ValorEnergiaActiva_P6(kWh)': u'ValorEnergiaActiva_P6 (kWh)',
        u'SuplementoEnergia_P1(€/kWh)': u'SuplementoEnergia_P1 (€/kWh)',
        u'SuplementoEnergia_P2(€/kWh)': u'SuplementoEnergia_P2 (€/kWh)',
        u'SuplementoEnergia_P3(€/kWh)': u'SuplementoEnergia_P3 (€/kWh)',
        u'SuplementoEnergia_P4(€/kWh)': u'SuplementoEnergia_P4 (€/kWh)',
        u'SuplementoEnergia_P5(€/kWh)': u'SuplementoEnergia_P5 (€/kWh)',
        u'SuplementoEnergia_P6(€/kWh)': u'SuplementoEnergia_P6(€/kWh)',
    }

    def __init__(self, pool, cursor, uid, context=None):
        self.pool = pool
        self.cursor = cursor
        self.uid = uid
        self.context = {} if context is None else context

    def group_consums_cups_tarifa(self, df_consum):
        df_consum = df_consum.groupby([u'CUPS', u'TarifaATRFact', u'Notas'])[
            [
                u'FechaDesde', u'FechaHasta',
                u'ImporteTotalSuplementoTerminoPotencia(€)',
                u'ImporteTotalSuplementoTerminoEnergia(€)',
                u'ImporteTotalARegularizar(€)',
                u'ValorEnergiaActiva_P1(kWh)',
                u'ValorEnergiaActiva_P2(kWh)',
                u'ValorEnergiaActiva_P3(kWh)',
                u'ValorEnergiaActiva_P4(kWh)',
                u'ValorEnergiaActiva_P5(kWh)',
                u'ValorEnergiaActiva_P6(kWh)'
            ]
        ].agg(
            {
                u'ImporteTotalSuplementoTerminoPotencia(€)': np.sum,
                u'ImporteTotalSuplementoTerminoEnergia(€)': np.sum,
                u'ImporteTotalARegularizar(€)': np.sum,
                u'FechaDesde': np.min,
                u'FechaHasta': np.max,
                u'ValorEnergiaActiva_P1(kWh)': np.sum,
                u'ValorEnergiaActiva_P2(kWh)': np.sum,
                u'ValorEnergiaActiva_P3(kWh)': np.sum,
                u'ValorEnergiaActiva_P4(kWh)': np.sum,
                u'ValorEnergiaActiva_P5(kWh)': np.sum,
                u'ValorEnergiaActiva_P6(kWh)': np.sum

            }
        )

        return df_consum

    def create_liquidacions(self, row_group_by_tarifa, grouped_extra, cups_id):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        liquidacio_obj = self.pool.get(
            'giscedata.liquidacio.suplement.territorial.tec271.data'
        )
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        # TODO revisar fecha
        data_cobrament = liquidacio_obj.get_data_cobrament_linies(
            cursor, uid, context=context
        )

        TABLA_107_dict = dict(TABLA_107)
        comunitat_id = False
        polissa_id = False
        cups_name = ''
        tipus = False

        if cups_id:
            cups_id = cups_id[0]
            comunitat_id = cups_obj.browse(
                cursor, uid, cups_id, context=context
            ).id_municipi.state.comunitat_autonoma.id

            cups_reads = cups_obj.read(
                cursor, uid, cups_id, ['polissa_polissa', 'name'],
                context=context
            )

            cups_name = cups_reads['name']

            polissa_id = cups_reads['polissa_polissa']

            if polissa_id:
                polissa_id = polissa_id[0]
                polissa = polissa_obj.read(
                    cursor, uid, polissa_id,
                    ['data_alta', 'data_baixa'],
                    context=context
                )

                data_alta = datetime.strptime(polissa['data_alta'], '%Y-%m-%d')
                data_baixa = polissa['data_baixa']

                if not data_alta <= data_cobrament:
                    tipus = '2'

                if data_baixa:
                    data_baixa = datetime.strptime(
                        polissa['data_baixa'], '%Y-%m-%d')
                    if data_baixa >= data_cobrament:
                        tipus = '2'
                    else:
                        tipus = '1'
                else:
                    tipus = '0'

        else:
            tipus = '1'

        tarifa_code = str(row_group_by_tarifa[u'TarifaATRFact']).zfill(3)

        tarifa_name = TABLA_107_dict[tarifa_code].replace(
            '.A', 'A').replace('.B', 'B').replace('.D', 'D').replace(' ', '')

        tarifa_id = tarifa_obj.search(
            cursor, uid,
            [
                ('name', '=', tarifa_name),
                ('codi_ocsum', '=', tarifa_code)
            ], context=context
        )

        if tarifa_id:
            tarifa_id = tarifa_id[0]

            vals = {
                'cups_id': cups_id,
                'polissa_id': polissa_id,
                'tarifa_id': tarifa_id,
                'import_total': float(row_group_by_tarifa[u'ImporteTotalARegularizar(€)']),
                'tipus': tipus,
                'ccaa_id': comunitat_id,
                'energia_total': row_group_by_tarifa[u'ImporteTotalSuplementoTerminoEnergia(€)'],
                'cups': cups_name

            }
            liquidacio_obj.create(cursor, uid, vals, context=context)
        else:
            print 'ERROR: no tarifa %s' % row_group_by_tarifa[u'CUPS']
        # TODO aviam que fem aqui
        grouped_extra.append(row_group_by_tarifa.to_dict())

    def create_extra_line(self, grouped_extra_info):
        if grouped_extra_info:
            grouped_df = pd.DataFrame(data=grouped_extra_info)[
                [u'CUPS', u'FechaDesde', u'FechaHasta',
                 u'ValorEnergiaActiva_P1(kWh)',
                 u'ValorEnergiaActiva_P2(kWh)',
                 u'ValorEnergiaActiva_P3(kWh)',
                 u'ValorEnergiaActiva_P4(kWh)',
                 u'ValorEnergiaActiva_P5(kWh)',
                 u'ValorEnergiaActiva_P6(kWh)',
                 u'ImporteTotalSuplementoTerminoPotencia(€)',
                 u'ImporteTotalSuplementoTerminoEnergia(€)',
                 u'ImporteTotalARegularizar(€)', u'Notas']
            ]

            grouped_df = grouped_df.groupby([u'CUPS', u'Notas']).agg(
                {
                    u'ImporteTotalSuplementoTerminoPotencia(€)': np.sum,
                    u'ImporteTotalSuplementoTerminoEnergia(€)': np.sum,
                    u'ImporteTotalARegularizar(€)': np.sum,
                    u'ValorEnergiaActiva_P1(kWh)': np.sum,
                    u'ValorEnergiaActiva_P2(kWh)': np.sum,
                    u'ValorEnergiaActiva_P3(kWh)': np.sum,
                    u'ValorEnergiaActiva_P4(kWh)': np.sum,
                    u'ValorEnergiaActiva_P5(kWh)': np.sum,
                    u'ValorEnergiaActiva_P6(kWh)': np.sum,
                    u'FechaDesde': np.min,
                    u'FechaHasta': np.max

                }
            ).reset_index().iloc[0]

            cups_name = grouped_extra_info[0][u'CUPS']
            cursor = self.cursor
            uid = self.uid
            context = self.context

            product_obj = self.pool.get('product.product')
            product_tec271 = product_obj.search(
                cursor, uid, [('default_code', '=', 'TEC271')], context=context
            )[0]

            extraline_obj = self.pool.get('giscedata.facturacio.extra')
            cups_obj = self.pool.get('giscedata.cups.ps')
            cups_id = cups_obj.search(
                cursor, uid,
                [('name', 'ilike', '{}%%'.format(cups_name[:20]))],
                context=context
            )

            if not cups_id:
                return False
            else:
                cups_id = cups_id[0]

            polissa_id = cups_obj.read(
                cursor, uid, cups_id, ['polissa_polissa'], context=context
            )['polissa_polissa']

            if not polissa_id:
                return False
            else:
                polissa_id = polissa_id[0]

            extraline_id = extraline_obj.search(
                cursor, uid,
                [
                    ('polissa_id', '=', polissa_id),
                    ('name', 'ilike', 'Suplemento Territorial%'),
                    ('product_id', '=', product_tec271)
                ], context=context
            )
            if not extraline_id:
                product = product_obj.browse(cursor, uid, product_tec271,
                                             context=context)
                if product.product_tmpl_id.property_account_income:
                    account = product.product_tmpl_id.property_account_income.id
                else:
                    account = product.categ_id.property_account_income_categ.id

                diari_obj = self.pool.get('account.journal')
                diari_id = diari_obj.search(
                    cursor, uid, [('code', '=', 'ENERGIA')], context=context
                )[0]

                def calcular_terminos_segun_importe(importe_total):
                    n_termes = 1
                    if 2.0 < importe_total <= 12.12:
                        n_termes = int(importe_total)
                    elif importe_total > 12.12:
                        n_termes = 12

                    return n_termes

                termes = calcular_terminos_segun_importe(
                    grouped_df[u'ImporteTotalARegularizar(€)']
                )
                nota_comentari = SuplementosTerritorialesComments.\
                    generate_json_from_imported_note(
                        grouped_df[u'Notas'],
                        grouped_df[u'ImporteTotalARegularizar(€)'],
                        termes, grouped_df
                    )

                extraline_vals = {
                    'account_id': account,
                    'journal_ids': [(6, 0, [diari_id])],
                    'product_id': product_tec271,
                    'price_unit': round(
                        grouped_df[u'ImporteTotalARegularizar(€)'], 2
                    ),
                    # 'notes': '{}{}'.format(
                    #     dades_linia['notes'],
                    #     termes
                    # ),
                    'notes': nota_comentari,
                    'name': product.description,
                    'date_line_from': grouped_df[u'FechaDesde'],
                    'date_line_to': grouped_df[u'FechaHasta'],
                    'date_from': datetime(2019, 6, 1).strftime('%Y-%m-%d'),
                    'date_to': datetime(2020, 9, 1).strftime('%Y-%m-%d'),
                    'polissa_id': polissa_id,
                    'term': termes
                }

                return extraline_obj.create(
                    cursor, uid, extraline_vals, context=context
                )
            else:
                # TODO no hauria d'entrar mai aqui ja que ja fem l'agrupacio
                # Faig un raise aqui perque vol dir que han reimportat i no
                # s'han netejat les dades
                raise except_osv(_('Warning'),
                    'Fichero reimportado o con los mismos cups ya importados, '
                    'limpiar datos de liquidaciones y lineas extras '
                    'ex. %s' % cups_name
                )
                #extraline_obj.write(
                #    cursor, uid, extraline_id,
                #    {'notes': grouped_df[u'Notas']},
                #    context=context
                #)
            #return extraline_id

        return False

    def import_procesed_consums(self, df_consum):
        cursor = self.cursor
        uid = self.uid
        context = self.context
        cups_obj = self.pool.get('giscedata.cups.ps')

        for i, new_df in tqdm(df_consum.groupby(level=0), desc='Creando lineas extra y liqudaciones por cups'):
            cups_df = new_df.reset_index()
            grouped_info_for_extra_line = []
            cups_id = cups_obj.search(
                cursor, uid,
                [('name', 'ilike', '{}%%'.format(i[:20]))],
                context=context
            )

            cups_df.apply(
                lambda x: self.create_liquidacions(
                    x, grouped_info_for_extra_line, cups_id
                ), axis=1
            )
            self.create_extra_line(grouped_info_for_extra_line)

    def import_from_excel(self, str_file_xls):
        consums_file = StringIO(str_file_xls)
        consums = []
        consums_df = pd.read_excel(consums_file, dtype={u'TarifaATRFact': str, u'FechaDesde': datetime, u'FechaHasta': datetime, u'Notas': str})
        consums_file.close()

        # Lambda expression to remove \n and ' ' from column names
        clean_header_of_unworthy_chars = (
            lambda s: s.replace('\n', '').replace(' ', '').replace("\"", "")
        )
        consums_df = consums_df.rename(
            columns=lambda x: clean_header_of_unworthy_chars(x)
        )
        consums_df[u'FechaDesde'] = pd.to_datetime(consums_df[u'FechaDesde'])
        consums_df[u'FechaHasta'] = pd.to_datetime(consums_df[u'FechaHasta'])

        # Validate file header column names
        self.validate_header(consums_df)

        # Remove fake lines prom dataframe (last line with totals)
        consums_df = consums_df[consums_df.CUPS.notnull()]

        self.calculate_calculated_values(consums_df)

        consums_df = self.group_consums_cups_tarifa(consums_df)

        self.import_procesed_consums(consums_df)

        return True

    def importar_consum(self, row, consums):
        """
        Dada una linea del datafame de suplementos añade un diccionario con la
        informacion en la lista "consums".
        :param row: row of dataframe (pandas)
        :param consums: list of dicts to store consums info
        :return:
        """

        def conv_date(date_to_conv):
            if not isinstance(date_to_conv, (basestring, str, unicode)) and isinstance(date_to_conv, datetime_date):
                date_to_conv = str(date_to_conv)
            try:
                assert (datetime.strptime(date_to_conv, '%Y-%m-%d'))
            except:
                for format_date in ('%d/%m/%Y', '%Y/%m/%d', '%d-%m-%Y'):
                    try:
                        return datetime.strptime(
                            date_to_conv, format_date
                        ).strftime('%Y-%m-%d')
                    except:
                        pass
                info = _("La data: {} té un format desconegut.".format(date_to_conv))
                raise except_osv(_(u'Error!'), info)
            return date_to_conv

        cups = row.CUPS
        tarifa_code = str(int(row.TarifaATRFact)).zfill(3)  # ex. 001
        fecha_desde = conv_date(row.FechaDesde)
        fecha_hasta = conv_date(row.FechaHasta)
        periode = (fecha_desde, fecha_hasta)
        dies = int(row.NumeroDias)

        medias_potencia = {
            u'p1': float(row[u"MediaPondPotenciaAFacturar_P1(W)"] or 0.0),
            u'p2': float(row[u"MediaPondPotenciaAFacturar_P2(W)"] or 0.0),
            u'p3': float(row[u"MediaPondPotenciaAFacturar_P3(W)"] or 0.0),
            u'p4': float(row[u"MediaPondPotenciaAFacturar_P4(W)"] or 0.0),
            u'p5': float(row[u"MediaPondPotenciaAFacturar_P5(W)"] or 0.0),
            u'p6': float(row[u"MediaPondPotenciaAFacturar_P6(W)"] or 0.0)
        }

        suplementos_potencia = {
            u'p1': float(row[u"SuplementoPotencia_P1(€/W)"] or 0.0),
            u'p2': float(row[u"SuplementoPotencia_P2(€/W)"] or 0.0),
            u'p3': float(row[u"SuplementoPotencia_P3(€/W)"] or 0.0),
            u'p4': float(row[u"SuplementoPotencia_P4(€/W)"] or 0.0),
            u'p5': float(row[u"SuplementoPotencia_P5(€/W)"] or 0.0),
            u'p6': float(row[u"SuplementoPotencia_P6(€/W)"] or 0.0)
        }

        importes_potencia = {
            u'p1': float(row[u"ImporteSuplementoTerminoPotencia_P1(€)"] or 0.0),
            u'p2': float(row[u"ImporteSuplementoTerminoPotencia_P2(€)"] or 0.0),
            u'p3': float(row[u"ImporteSuplementoTerminoPotencia_P3(€)"] or 0.0),
            u'p4': float(row[u"ImporteSuplementoTerminoPotencia_P4(€)"] or 0.0),
            u'p5': float(row[u"ImporteSuplementoTerminoPotencia_P5(€)"] or 0.0),
            u'p6': float(row[u"ImporteSuplementoTerminoPotencia_P6(€)"] or 0.0)
        }

        total_potencia = float(
            row[u'ImporteTotalSuplementoTerminoPotencia(€)'] or 0.0
        )

        valores_energia_activa = {
            u'p1': float(row[u"ValorEnergiaActiva_P1(kWh)"] or 0.0),
            u'p2': float(row[u"ValorEnergiaActiva_P2(kWh)"] or 0.0),
            u'p3': float(row[u"ValorEnergiaActiva_P3(kWh)"] or 0.0),
            u'p4': float(row[u"ValorEnergiaActiva_P4(kWh)"] or 0.0),
            u'p5': float(row[u"ValorEnergiaActiva_P5(kWh)"] or 0.0),
            u'p6': float(row[u"ValorEnergiaActiva_P6(kWh)"] or 0.0)
        }

        suplementos_energia = {
            u'p1': float(row[u"SuplementoEnergia_P1(€/kWh)"] or 0.0),
            u'p2': float(row[u"SuplementoEnergia_P2(€/kWh)"] or 0.0),
            u'p3': float(row[u"SuplementoEnergia_P3(€/kWh)"] or 0.0),
            u'p4': float(row[u"SuplementoEnergia_P4(€/kWh)"] or 0.0),
            u'p5': float(row[u"SuplementoEnergia_P5(€/kWh)"] or 0.0),
            u'p6': float(row[u"SuplementoEnergia_P6(€/kWh)"] or 0.0)
        }

        importes_energia = {
            u'p1': float(row[u"ImporteSuplementoTerminoEnergia_P1(€)"] or 0.0),
            u'p2': float(row[u"ImporteSuplementoTerminoEnergia_P2(€)"] or 0.0),
            u'p3': float(row[u"ImporteSuplementoTerminoEnergia_P3(€)"] or 0.0),
            u'p4': float(row[u"ImporteSuplementoTerminoEnergia_P4(€)"] or 0.0),
            u'p5': float(row[u"ImporteSuplementoTerminoEnergia_P5(€)"] or 0.0),
            u'p6': float(row[u"ImporteSuplementoTerminoEnergia_P6(€)"] or 0.0)
        }

        total_energia = float(
            row[u"ImporteTotalSuplementoTerminoEnergia(€)"] or 0.0
        )

        total_a_regularizar = float(
            row[u"ImporteTotalARegularizar(€)"] or 0.0
        )

        calculated_potencia_total = 0.0
        calculated_energia_total = 0.0
        for num_periode in range(1, 7):
            nom_periode = u'p{}'.format(num_periode)
            p_potencia = medias_potencia[nom_periode]
            p_supl_potencia = suplementos_potencia[nom_periode]
            p_import_potencia = p_potencia * p_supl_potencia
            calculated_potencia_total += p_import_potencia * dies

            p_energia = valores_energia_activa[nom_periode]
            p_import_energia = p_energia * suplementos_energia[nom_periode]
            calculated_energia_total += p_import_energia

        calculated_total = calculated_potencia_total + calculated_energia_total

        # Nos aseguramos de que el valor a regularizar sea igual a la suma de
        # los campos sino usamos el valor que calculamos nosotros
        if round(calculated_total, 2) != total_a_regularizar:
            total_a_regularizar = calculated_total

        # Usamos los valore calculados
        total_energia = calculated_energia_total
        total_potencia = calculated_potencia_total

        notes_potencia = [
            medias_potencia,
            {
                'data': False,
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            {
                'data': False,
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            }
        ]
        consums.append({
            'cups': cups,
            'tarifa': tarifa_code,
            'periode': periode,
            'dies': dies,
            'potencia': {
                'consum': medias_potencia,
                'notes': notes_potencia,
                'suplements': suplementos_potencia,
                'imports': importes_potencia,
                'total': float(total_potencia)
            },
            'energia': {
                'consum': valores_energia_activa,
                'notes': valores_energia_activa,
                'suplements': suplementos_energia,
                'imports': importes_energia,
                'total': float(total_energia)
            },
            'total': total_a_regularizar
        })

    def calculate_calculated_values(self, consums_df):
        '''
        Calcula los campos calculables para el ficheros si no lo estan
        :param consums_df: DataFrame consumemas
        :return:
        '''

        def mul_nan(b, a, c=1):
            if isnan(b):
                b = 0
            if isnan(a):
                a = 0
            if isnan(c):
                c = 0
            return b * a * c

        def sum_array_nan(l):
            return sum([0.0 if (isnan(elem) or False) else elem for elem in l])

        period_names = [u'P1', u'P2', u'P3', u'P4', u'P5', u'P6']

        # Remove hours, minutes and seconds
        consums_df[u'FechaDesde'] = consums_df[u'FechaDesde'].dt.date
        consums_df[u'FechaHasta'] = consums_df[u'FechaHasta'].dt.date

        # Calculate number of days and create column if don't exist
        consums_df[u"NumeroDias"] = consums_df.apply(
            lambda row: (row.FechaHasta - row.FechaDesde).days, axis=1
        )

        # Calculo del total potencia

        have_importes_periodes_potencia_calulated = {
            u'ImporteSuplementoTerminoPotencia_P1(€)',
        }.issubset(consums_df.columns)

        empty_importes_periodes_potencia_calulated = consums_df[
            u'ImporteSuplementoTerminoPotencia_P1(€)'
        ].isnull().values.all()

        if not have_importes_periodes_potencia_calulated or empty_importes_periodes_potencia_calulated:
            for p in period_names:
                media_p_pot = (
                    u'MediaPondPotenciaAFacturar_{}(W)'.format(p)
                )
                suplemento_p_pot = (
                    u'SuplementoPotencia_{}(€/W)'.format(p)
                )
                total_p_pot = (
                    u'ImporteSuplementoTerminoPotencia_{}(€)'.format(p)
                )

                consums_df[total_p_pot] = consums_df.apply(
                    lambda x: mul_nan(x[media_p_pot], x[suplemento_p_pot]), axis=1
                )

        column_importe_total_potencia_exist = {
            u'ImporteTotalSuplementoTerminoPotencia(€)',
        }.issubset(consums_df.columns)

        column_importe_total_potencia_empty = consums_df[
            u'ImporteTotalSuplementoTerminoPotencia(€)'
        ].isnull().values.all()

        if not column_importe_total_potencia_exist or column_importe_total_potencia_empty:
            # sumar parcials
            partial_import = u'ImporteSuplementoTerminoPotencia_{}(€)'
            consums_df[u'ImporteTotalSuplementoTerminoPotencia(€)'] = consums_df.apply(
                lambda x: sum_array_nan(
                    [x[partial_import.format(v)] for v in period_names]
                ), axis=1
            )

        # Calculo del total energia

        have_importes_periodes_energia_calulated = {
            u'ImporteSuplementoTerminoEnergia_P1(€)',
        }.issubset(consums_df.columns)

        empty_importes_periodes_energia_calulated = consums_df[
            u'ImporteSuplementoTerminoEnergia_P1(€)'
        ].isnull().values.all()

        if not have_importes_periodes_energia_calulated or empty_importes_periodes_energia_calulated:
            for p in period_names:
                valor_p_energy = (
                    u'ValorEnergiaActiva_{}(kWh)'.format(p)
                )
                suplemento_p_energy = (
                    u'SuplementoEnergia_P1(€/kWh)'.format(p)
                )

                total_p_energy = (
                    u'ImporteSuplementoTerminoEnergia_{}(€)'.format(p)
                )

                consums_df[total_p_energy] = consums_df.apply(
                    lambda x: mul_nan(x[valor_p_energy], x[suplemento_p_energy]),
                    axis=1
                )

        column_importe_total_energia_exist = {
            u'ImporteTotalSuplementoTerminoEnergia(€)',
        }.issubset(consums_df.columns)

        column_importe_total_energia_empty = consums_df[
            u'ImporteTotalSuplementoTerminoEnergia(€)'
        ].isnull().values.all()

        if not column_importe_total_energia_exist or column_importe_total_energia_empty:
            # sumar parcials
            partial_import = u'ImporteSuplementoTerminoEnergia_{}(€)'
            consums_df[
                u'ImporteTotalSuplementoTerminoEnergia(€)'] = consums_df.apply(
                lambda x: sum_array_nan(
                    [x[partial_import.format(v)] for v in period_names]
                ), axis=1
            )
            # Mirar si hi ha els imports sino calcular

        # Calulo del total regularizado

        column_importe_total_regularizado_exist = {
            u'ImporteTotalARegularizar(€)',
        }.issubset(consums_df.columns)

        column_importe_total_regularizado_empty = consums_df[
            u'ImporteTotalARegularizar(€)'
        ].isnull().values.all()

        if not column_importe_total_regularizado_exist or column_importe_total_regularizado_empty:
            consums_df[
                u'ImporteTotalARegularizar(€)'] = consums_df.apply(
                lambda x: sum(
                    [
                        x[u'ImporteTotalSuplementoTerminoPotencia(€)'],
                        x[u'ImporteTotalSuplementoTerminoEnergia(€)']
                    ]
                ), axis=1
            )

        consums_df.fillna(value=0, inplace=True)

    def validate_header(self, file_df):
        '''
        Function to validate if file contains column names included
        in self.valid_header
        :param file_df: Pandas data frame
        :return:
        '''
        file_header = list(file_df.columns.values)

        for column_name in self.valid_header_conv_dict.keys():
            if column_name not in file_header:
                raise except_osv(
                    _('Error!'),
                    _('El fitxer ha de tenir la columna %s') % column_name
                )

    def export_to_excel(self):
        """

        :return: binary file with ...
        """
        cursor = self.cursor
        uid = self.uid
        context = self.context

        cups_obj = self.pool.get('giscedata.cups.ps')

        cups_comunidades_afectadas_ids = self.get_affected_cups()
        n_afected_cups = len(cups_comunidades_afectadas_ids)
        consum_total = []

        for cups in tqdm(cups_obj.read(cursor, uid, cups_comunidades_afectadas_ids, ['name'], context=context), desc="Procesant cups"):
            cups_name = cups['name']
            cups_id = cups['id']
            for periode in self.periodes_eval:
                consums = self.get_consum_cups(cups_id, periode)
                consum_total.append({
                    'cups': cups_name,
                    'periode': periode,
                    'consums': consums,
                    'suplements': self.get_suplements_consum(
                        cups_id, consums, periode
                    ),
                })

        # Create dict of dicts from consums list
        consum_total = self.sort_by_comer(consum_total)
        exported_files = self.export_consums_to_file(consum_total)

        return exported_files

    def export_consums_to_file(self, consums):
        # Fichero con todas las lineas de consumos
        file_xls = StringIO()

        # Zip to store comer files
        file_zip = StringIO()
        zip_fp = ZipFile(
            file_zip, 'w', compression=ZIP_DEFLATED
        )

        # Create dataframe with sorted valid header
        df_consums_complet = pd.DataFrame(columns=self.export_valid_header)
        for comer_code in consums.keys():
            # comer_consums contains info to generate file
            comer_consums = consums.get(comer_code)
            # individual file for any comer
            file_comer = StringIO()
            writer_comer = pd.ExcelWriter(file_comer)

            df_comer = pd.DataFrame(columns=self.export_valid_header)

            for line_consum in comer_consums:
                cups = line_consum['cups']
                periode = line_consum['periode']
                suplements = line_consum['suplements']
                ndies = (
                    datetime.strptime(periode[1], '%Y-%m-%d') -
                    datetime.strptime(periode[0], '%Y-%m-%d')
                ).days +1
                periodes_potencia = line_consum['consums_potencia']
                periodes_energia = line_consum['consums_energia']

                suplements_potencia = (suplements['suplements']['potencia'])

                imports_potencia = suplements['imports']['potencia']
                total_potencia = sum(imports_potencia)
                suplements_energia = suplements['suplements']['energia']
                imports_energia = suplements['imports']['energia']
                total_energia = sum(imports_energia)

                line_dict = {
                    u'CUPS': cups,
                    u'TarifaATRFact': line_consum['tarifa'],
                    u'FechaDesde': (datetime.strptime(periode[0], '%Y-%m-%d') - timedelta(days=1)).strftime('%Y-%m-%d'),
                    u'FechaHasta': periode[1],
                    u'NumeroDias': ndies,
                    u'MediaPondPotenciaAFacturar_P1 (W)': u'{}'.format(
                        long(round(periodes_potencia['p1'] * 1000, 0))
                    ).zfill(14),
                    u'MediaPondPotenciaAFacturar_P2 (W)': u'{}'.format(
                        long(round(periodes_potencia['p2'] * 1000, 0))
                    ).zfill(14),
                    u'MediaPondPotenciaAFacturar_P3 (W)': u'{}'.format(
                        long(round(periodes_potencia['p3'] * 1000, 0))
                    ).zfill(14),
                    u'MediaPondPotenciaAFacturar_P4 (W)': u'{}'.format(
                        long(round(periodes_potencia['p4'] * 1000, 0))
                    ).zfill(14),
                    u'MediaPondPotenciaAFacturar_P5 (W)': u'{}'.format(
                        long(round(periodes_potencia['p5'] * 1000, 0))
                    ).zfill(14),
                    u'MediaPondPotenciaAFacturar_P6 (W)': u'{}'.format(
                        long(round(periodes_potencia['p6'] * 1000, 0))
                    ).zfill(14),
                    u'SuplementoPotencia_P1 (€/W)': u'{:015.9f}'.format(round(suplements_potencia[0] / 365 / 1000 * ndies, 9)),
                    u'SuplementoPotencia_P2 (€/W)': u'{:015.9f}'.format(round(suplements_potencia[1] / 365 / 1000 * ndies, 9)),
                    u'SuplementoPotencia_P3 (€/W)': u'{:015.9f}'.format(round(suplements_potencia[2] / 365 / 1000 * ndies, 9)),
                    u'SuplementoPotencia_P4 (€/W)': u'{:015.9f}'.format(round(suplements_potencia[3] / 365 / 1000 * ndies, 9)),
                    u'SuplementoPotencia_P5 (€/W)': u'{:015.9f}'.format(round(suplements_potencia[4] / 365 / 1000 * ndies, 9)),
                    u'SuplementoPotencia_P6 (€/W)': u'{:015.9f}'.format(round(suplements_potencia[5] / 365 / 1000 * ndies, 9)),
                    u'ImporteSuplementoTerminoPotencia_P1 (€)': u'{:+013.2f}'.format(round(imports_potencia[0], 2)),
                    u'ImporteSuplementoTerminoPotencia_P2 (€)': u'{:+013.2f}'.format(round(imports_potencia[1], 2)),
                    u'ImporteSuplementoTerminoPotencia_P3 (€)': u'{:+013.2f}'.format(round(imports_potencia[2], 2)),
                    u'ImporteSuplementoTerminoPotencia_P4 (€)': u'{:+013.2f}'.format(round(imports_potencia[3], 2)),
                    u'ImporteSuplementoTerminoPotencia_P5 (€)': u'{:+013.2f}'.format(round(imports_potencia[4], 2)),
                    u'ImporteSuplementoTerminoPotencia_P6 (€)': u'{:+013.2f}'.format(round(imports_potencia[5], 2)),
                    u'ImporteTotalSuplementoTerminoPotencia (€)': u'{:+013.2f}'.format(round(total_potencia, 2)),
                    u'ValorEnergiaActiva_P1 (kWh)': u'{:+013.2f}'.format(round(periodes_energia['p1'], 2)),
                    u'ValorEnergiaActiva_P2 (kWh)': u'{:+013.2f}'.format(round(periodes_energia['p2'], 2)),
                    u'ValorEnergiaActiva_P3 (kWh)': u'{:+013.2f}'.format(round(periodes_energia['p3'], 2)),
                    u'ValorEnergiaActiva_P4 (kWh)': u'{:+013.2f}'.format(round(periodes_energia['p4'], 2)),
                    u'ValorEnergiaActiva_P5 (kWh)': u'{:+013.2f}'.format(round(periodes_energia['p5'], 2)),
                    u'ValorEnergiaActiva_P6 (kWh)': u'{:+013.2f}'.format(round(periodes_energia['p6'], 2)),
                    u'SuplementoEnergia_P1 (€/kWh)': u'{:015.9f}'.format(round(suplements_energia[0], 9)),
                    u'SuplementoEnergia_P2 (€/kWh)': u'{:015.9f}'.format(round(suplements_energia[1], 9)),
                    u'SuplementoEnergia_P3 (€/kWh)': u'{:015.9f}'.format(round(suplements_energia[2], 9)),
                    u'SuplementoEnergia_P4 (€/kWh)': u'{:015.9f}'.format(round(suplements_energia[3], 9)),
                    u'SuplementoEnergia_P5 (€/kWh)': u'{:015.9f}'.format(round(suplements_energia[4], 9)),
                    u'SuplementoEnergia_P6 (€/kWh)': u'{:015.9f}'.format(round(suplements_energia[5], 9)),
                    u'ImporteSuplementoTerminoEnergia_P1 (€)': u'{:+013.2f}'.format(round(imports_energia[0], 2)),
                    u'ImporteSuplementoTerminoEnergia_P2 (€)': u'{:+013.2f}'.format(round(imports_energia[1], 2)),
                    u'ImporteSuplementoTerminoEnergia_P3 (€)': u'{:+013.2f}'.format(round(imports_energia[2], 2)),
                    u'ImporteSuplementoTerminoEnergia_P4 (€)': u'{:+013.2f}'.format(round(imports_energia[3], 2)),
                    u'ImporteSuplementoTerminoEnergia_P5 (€)': u'{:+013.2f}'.format(round(imports_energia[4], 2)),
                    u'ImporteSuplementoTerminoEnergia_P6 (€)': u'{:+013.2f}'.format(round(imports_energia[5], 2)),
                    u'ImporteTotalSuplementoTerminoEnergia (€)': u'{:+013.2f}'.format(round(total_energia, 2)),
                    u'ImporteTotalARegularizar (€)': u'{:+013.2f}'.format(round((total_potencia + total_energia), 2)),
                }

                df_comer_line = pd.DataFrame(
                    data=[line_dict], columns=self.export_valid_header
                )

                df_comer = df_comer.append(df_comer_line)
                df_consums_complet = df_consums_complet.append(df_comer_line)

            df_comer.fillna(value="", inplace=True)
            df_comer[u'TarifaATRFact'] = df_comer.TarifaATRFact.astype(str)
            df_comer = df_comer.sort_values(by=['CUPS', 'FechaDesde'], ascending=True)
            df_comer.to_excel(writer_comer, index=None, header=True, encoding='utf-8-sig')

            writer_comer.save()

            individual_comer = file_comer.getvalue()

            file_comer.close()

            file_in_zip_name = 'ordre_TEC_271_2019_%s.xlsx' % comer_code
            zip_fp.writestr(file_in_zip_name, individual_comer)

        writer_complet = pd.ExcelWriter(file_xls)

        df_consums_complet.fillna(value="", inplace=True)
        df_consums_complet[u'TarifaATRFact'] = df_consums_complet.TarifaATRFact.astype(str)
        df_consums_complet = df_consums_complet.sort_values(by=['CUPS', 'FechaDesde'],
                                        ascending=True)
        comments_obj = SuplementosTerritorialesComments(
            self.pool, self.cursor, self.uid, self.context
        )

        comments_obj.generate_comments_from_complete_dataframe(
            df_consums_complet,
            self.periodes_eval[0][0],
            self.periodes_eval[1][1],
            self.remove_factures_abonades  # is a filter funct to apply
        )

        df_consums_complet.to_excel(
            writer_complet, index=None, header=True, encoding='utf-8-sig'
        )

        writer_complet.save()

        consums_totals_xls_str_file = b64encode(file_xls.getvalue())

        file_xls.close()

        zip_fp.close()

        zip_str_file = b64encode(file_zip.getvalue())

        file_zip.close()

        return {
            'zip_comers': zip_str_file,
            'xlsx_complet': consums_totals_xls_str_file,
            'errors': comments_obj.errores
        }

    def get_affected_cups(self):
        """
        Busqueda de los cups de las comunidades afectadas por la orden
        TEC/271/2019.
        :return: ids de los cups afectados por el suplemento TEC 271
        """

        cursor = self.cursor
        uid = self.uid
        context = self.context

        cups_obj = self.pool.get('giscedata.cups.ps')
        comunitat_obj = self.pool.get('res.comunitat_autonoma')
        provincia_obj = self.pool.get('res.country.state')
        pol_obj = self.pool.get('giscedata.polissa')
        mod_obj = self.pool.get('giscedata.polissa.modcontractual')

        # Busqueda de las comunidades afectadas
        id_comunitats = comunitat_obj.search(
            cursor, uid,
            [('codi', 'in', self.codi_comunitats)],
            context=context
        )

        # Busqueda de las provincias de las comunidades afectadas
        id_provincies = provincia_obj.search(
            cursor, uid,
            [('comunitat_autonoma', 'in', id_comunitats)],
            context=context
        )

        cups_ids = cups_obj.search(
            cursor, uid,
            [('id_provincia', 'in', id_provincies), ('active', '=', True),
             ('polissa_polissa', '!=', False)],
            context=context
        )

        cups_polisses = cups_obj.read(
            cursor, uid, cups_ids, ['polisses', 'polissa_polissa'], context=context
        )

        ctx = context.copy()
        ctx.update({'active_test': False})

        res_cups = []

        # Only get cups with titular in titulars of 2013
        for polisses_in_single_cups in cups_polisses:
            titular_actual = pol_obj.read(
                cursor, uid,
                polisses_in_single_cups['polissa_polissa'][0],
                ['titular'],
                context=context
            )['titular'][0]

            polisses_under_range = mod_obj.search(
                cursor, uid,
                [
                    ('id', 'in', polisses_in_single_cups['polisses']),
                    ('data_inici', '<', self.periodes_eval[1][1]),
                    '|', ('data_final', '>', self.periodes_eval[0][0]),
                    ('data_final', '=', False)
                ], context=ctx, order='data_inici asc')

            if polisses_under_range:
                titulars_2013 = mod_obj.read(
                    cursor, uid, polisses_under_range,
                    ['titular'],
                    context=context
                )

                titulars_2013 = [pol['titular'][0] for pol in titulars_2013 if pol['titular']]
                if titular_actual in titulars_2013:
                    res_cups.append(polisses_in_single_cups['id'])

        return list(set(res_cups))

    def remove_factures_abonades(self, factures):
        if not factures:
            return factures
        cursor = self.cursor
        uid = self.uid
        context = self.context

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        facturas_n_rectificadas = fact_obj.search(
            cursor, uid,
            [
                ('id', 'in', factures),
                ('rectificative_type', '=', 'N'),
                ('refund_by_id', '!=', False)
            ],
            context=context
        )

        if facturas_n_rectificadas:

            invoice_ids_n_rectificades = [
                f['invoice_id'][0]
                for f in fact_obj.read(
                    cursor, uid, facturas_n_rectificadas, ['invoice_id'], context=context)
            ]

            b_to_remove = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', factures),
                    ('rectificative_type', '=', 'B'),
                ],
                context=context
            )

            rectified_invoice_ids_from_b = fact_obj.read(
                cursor, uid, b_to_remove, ['rectifying_id'], context=context
            )
            rectified_invoice_ids_from_b = [
                f_b['rectifying_id'][0] for f_b in rectified_invoice_ids_from_b
            ]

            n_to_be_destroyed = fact_obj.search(
                cursor, uid,
                [('invoice_id', 'in', rectified_invoice_ids_from_b)],
                context=context
            )

            r_in_2013 = fact_obj.search(
                cursor, uid,
                [('rectifying_id', 'in', invoice_ids_n_rectificades),
                 ('data_inici', '>=', self.periodes_eval[0][0]),
                 ('data_inici', '<=', self.periodes_eval[1][1]),
                 ('rectificative_type', '=', 'R')
                 ]
            )

            factures = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', list(set(list(set(factures) - set(n_to_be_destroyed) - set(b_to_remove) - set(facturas_n_rectificadas)) + r_in_2013)))
                ], context=context, order='data_inici asc'
            )
        else:
            b_to_remove = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', factures),
                    ('rectificative_type', '=', 'B'),
                ],
                context=context
            )

            rectified_invoice_ids_from_b = fact_obj.read(
                cursor, uid, b_to_remove, ['rectifying_id'], context=context
            )
            rectified_invoice_ids_from_b = [
                f_b['rectifying_id'][0] for f_b in rectified_invoice_ids_from_b
            ]

            n_to_be_destroyed = fact_obj.search(
                cursor, uid,
                [('invoice_id', 'in', rectified_invoice_ids_from_b)],
                context=context
            )

            factures = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', list(set(list(set(factures) - set(n_to_be_destroyed) - set(b_to_remove)))))
                ], context=context, order='data_inici asc'
            )

        return factures

    def get_consum_cups(self, cups_id, periode_consum):

        cursor = self.cursor
        uid = self.uid
        context = self.context

        data_inici, data_final = periode_consum
        consum_factures = []
        polissa_obj = self.pool.get('giscedata.polissa')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        lect_energia_obj = self.pool.get(
            'giscedata.facturacio.lectures.energia'
        )
        lect_potencia_obj = self.pool.get(
            'giscedata.facturacio.factura.linia'
        )
        tarifa_obj = self.pool.get(
            'giscedata.polissa.tarifa'
        )

        ctx = context.copy()
        ctx.update({'active_test': False})
        # TODO revisar fechas de la busqueda
        polisses = polissa_obj.search(
            cursor, uid,
            [
                ('cups', '=', cups_id), ('data_alta', '<', periode_consum[1]),
                '|', ('data_baixa', '>', periode_consum[0]),
                ('data_baixa', '=', False)
            ], context=ctx, order='cups asc, data_alta asc')

        tarifa_anterior = 0
        magic_pot_before_mitjana = {
            'data_inici': False,
            'data_final': False,
            'potencies': []  # list of dicts {'p1':..., 'p2':...., 'ndies':...}
        }

        for polissa in polisses:
            factures = factura_obj.search(
                cursor, uid,
                [
                    ('polissa_id', '=', polissa),
                    ('data_inici', '<=', data_final),
                    ('data_final', '>=', data_inici),
                ], context=context, order='data_inici asc'
            )
            factures = self.remove_factures_abonades(factures)

            for factura_id in factures:
                multiple_periods = False
                factura_vals = factura_obj.read(
                    cursor, uid,
                    factura_id, [
                        'lectures_energia_ids', 'linies_potencia',
                        'tarifa_acces_id', 'data_inici', 'data_final',
                        'rectificative_type', 'tipo_rectificadora',
                    ], context=context
                )
                i_fecha_datetime = datetime.strptime(
                    factura_vals['data_inici'], '%Y-%m-%d'
                )
                f_fecha_datetime = datetime.strptime(
                    factura_vals['data_final'], '%Y-%m-%d'
                )

                tariff_id = factura_vals['tarifa_acces_id'][0]
                tarifa_name = factura_vals['tarifa_acces_id'][1]

                tarifa_name = tarifa_name.replace(' LB', '')

                energia_ids = factura_vals['lectures_energia_ids']
                potencia_ids = factura_vals['linies_potencia']
                lect_energia = lect_energia_obj.read(
                    cursor, uid, energia_ids, ['consum', 'name', 'tipus'],
                    context=context
                )

                periodes_energia = {}
                consum_energia = 0
                lect_potencia = lect_potencia_obj.read(
                    cursor, uid,
                    potencia_ids, ['quantity', 'name', 'data_desde', 'data_fins'],
                    context=context
                )

                if i_fecha_datetime < datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d') and f_fecha_datetime > datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d'):
                    multiple_periods = True
                    new_lect_potencia = []
                    if periode_consum == self.periodes_eval[0]: # Estem al primer periode

                        for lp in lect_potencia:
                            fecha_inicio_lectura_in_first = (
                                datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                <=
                                datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
                            )
                            fecha_fin_lectura_in_second = (
                                datetime.strptime(lp['data_fins'], '%Y-%m-%d')
                                >
                                datetime.strptime(self.periodes_eval[0][1],
                                                  '%Y-%m-%d')
                            )
                            if fecha_inicio_lectura_in_first and fecha_fin_lectura_in_second:
                                t_days = (datetime.strptime(lp['data_fins'], '%Y-%m-%d') - datetime.strptime(lp['data_desde'], '%Y-%m-%d')).days + 1
                                to_fact_days = (
                                                   datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
                                                   -
                                                   datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                               ).days + 1
                                # real_quantity_period = to_fact_days * lp['quantity'] / t_days
                                # lp['quantity'] = real_quantity_period
                                lp['data_fins'] = self.periodes_eval[0][1]

                                new_lect_potencia.append(lp)
                            elif fecha_inicio_lectura_in_first and not fecha_fin_lectura_in_second:
                                new_lect_potencia.append(lp)
                            # else:
                            #     print "descartada periode 1", lp

                    else:  # Segon periode
                        for lp in lect_potencia:
                            fecha_inicio_lectura_in_first = (
                                datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                <=
                                datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
                            )
                            fecha_fin_lectura_in_second = (
                                datetime.strptime(lp['data_fins'], '%Y-%m-%d')
                                >
                                datetime.strptime(self.periodes_eval[0][1],
                                                  '%Y-%m-%d')
                            )
                            if fecha_inicio_lectura_in_first and fecha_fin_lectura_in_second:
                                t_days = (
                                             datetime.strptime(lp['data_fins'], '%Y-%m-%d')
                                             -
                                             datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                         ).days + 1

                                to_fact_days = (
                                                   datetime.strptime(
                                                       lp['data_fins'],
                                                       '%Y-%m-%d'
                                                   )
                                                   -
                                                   datetime.strptime(
                                                       self.periodes_eval[1][0],
                                                       '%Y-%m-%d')
                                               ).days + 1



                                # real_quantity_period = to_fact_days * lp[
                                #     'quantity'] / t_days
                                # lp['quantity'] = real_quantity_period
                                lp['data_desde'] = self.periodes_eval[1][0]

                                new_lect_potencia.append(lp)
                            elif not fecha_inicio_lectura_in_first and fecha_fin_lectura_in_second:
                                new_lect_potencia.append(lp)
                            # else:
                            #     print "descartada periode 2", lp
                    lect_potencia = new_lect_potencia


                periodes_potencia = {}
                consum_potencia = 0
                for num_periode in range(1, 7):
                    consum_energia_periode = sum([
                        x['consum'] for x in lect_energia
                        if (x['name'] == '{} (P{})'.format(tarifa_name, num_periode)
                            ) and x['tipus'] == 'activa'
                    ])

                    if (factura_vals['rectificative_type'] and factura_vals['rectificative_type'] == 'B'):
                        consum_energia_periode *= -1
                    if (factura_vals['data_inici'] < data_inici
                        or factura_vals['data_final'] > data_final):
                        extra_days = 0
                        if factura_vals['data_inici'] < data_inici:
                            extra_days = (datetime.strptime(
                                data_inici, '%Y-%m-%d'
                            ) - datetime.strptime(
                                factura_vals['data_inici'], '%Y-%m-%d'
                            )).days
                        elif factura_vals['data_final'] > data_final:
                            extra_days = (datetime.strptime(
                                factura_vals['data_final'], '%Y-%m-%d'
                            ) - datetime.strptime(
                                data_final, '%Y-%m-%d'
                            )).days
                        factura_days = (datetime.strptime(
                            factura_vals['data_final'], '%Y-%m-%d'
                        ) - datetime.strptime(
                            factura_vals['data_inici'], '%Y-%m-%d'
                        )).days
                        consum_energia_periode = (
                            consum_energia_periode -
                            extra_days * consum_energia_periode / factura_days)
                    periodes_energia.update({
                        'p{}'.format(num_periode): consum_energia_periode
                    })

                    if not factura_vals['tipo_rectificadora'] in ['B']:
                        upper_fraction_value = 0
                        down_fraction_value = 0

                        for l_pot in lect_potencia:
                            if l_pot['name'] == 'P{}'.format(num_periode):
                                days_lect_pot = (
                                    datetime.strptime(l_pot['data_fins'], '%Y-%m-%d')
                                    - datetime.strptime(l_pot['data_desde'], '%Y-%m-%d')
                                ).days + 1
                                upper_fraction_value += (days_lect_pot * l_pot['quantity'])
                                down_fraction_value += days_lect_pot

                        if upper_fraction_value:
                            periodes_potencia.update({'p{}'.format(num_periode): upper_fraction_value / down_fraction_value})
                        else:
                            periodes_potencia.update({'p{}'.format(num_periode): 0})

                    consum_energia += periodes_energia[
                        'p{}'.format(num_periode)]
                    consum_potencia += periodes_potencia[
                        'p{}'.format(num_periode)]

                if tarifa_name in ['3.0A', '3.1A']:
                    consum_energia = 0
                    consum_potencia = 0
                    for num_periode in range(1, 4):
                        nom_periode = 'p{}'.format(num_periode)
                        nom_periode2 = 'p{}'.format(num_periode + 3)
                        periodes_energia.update({
                            nom_periode: (periodes_energia[nom_periode] +
                                          periodes_energia[nom_periode2]),
                            nom_periode2: 0
                        })
                        # periodes_potencia.update({nom_periode2: 0})
                        consum_energia += periodes_energia[nom_periode]
                        # consum_potencia += periodes_potencia[nom_periode]
                factura_vals.pop('lectures_energia_ids')
                factura_vals.pop('linies_potencia')
                factura_vals.update({
                    'consum_energia': (consum_energia, periodes_energia),
                    'consum_potencia': (consum_potencia, periodes_potencia),
                    'tarifa_name': tarifa_name,
                    'tarifa': tarifa_obj.read(
                        cursor, uid,
                        factura_vals['tarifa_acces_id'][0],
                        ['codi_ocsum'],
                        context=context
                    )['codi_ocsum'][1:],
                    'comer': polissa_obj.browse(
                        cursor, uid, polissa, context=context
                    ).cups.polissa_polissa.comercialitzadora.ref
                })
                tarifa_actual = factura_vals['tarifa']

                # TODO POSAR IF O NO?
                #if periode_consum == self.periodes_eval[0]:  # Estem al primer periode
                if datetime.strptime(factura_vals['data_final'], '%Y-%m-%d') > datetime.strptime(periode_consum[1], '%Y-%m-%d'):
                    factura_vals['data_final'] = periode_consum[1]
                #else:
                if datetime.strptime(factura_vals['data_inici'], '%Y-%m-%d') < datetime.strptime(periode_consum[0], '%Y-%m-%d'):
                    factura_vals['data_inici'] = periode_consum[0]

                if tarifa_anterior == tarifa_actual:
                    current_period_pots = deepcopy(periodes_potencia)
                    real_start_date = False
                    real_end_date = False

                    if datetime.strptime(factura_vals['data_inici'], '%Y-%m-%d') < datetime.strptime(periode_consum[0], '%Y-%m-%d'):
                        real_start_date = periode_consum[0]
                    else:
                        real_start_date = factura_vals['data_inici']

                    if datetime.strptime(factura_vals['data_final'], '%Y-%m-%d') > datetime.strptime(periode_consum[1], '%Y-%m-%d'):
                        real_end_date = periode_consum[1]
                    else:
                        real_end_date = factura_vals['data_final']

                    if datetime.strptime(real_end_date, '%Y-%m-%d') > datetime.strptime(magic_pot_before_mitjana['data_final'], '%Y-%m-%d'):
                        magic_pot_before_mitjana['data_final'] = real_end_date

                    if datetime.strptime(real_start_date, '%Y-%m-%d') < datetime.strptime(magic_pot_before_mitjana['data_inici'], '%Y-%m-%d'):
                        magic_pot_before_mitjana['data_inici'] = real_start_date

                    current_period_pots.update(
                        {'ndies':
                            (
                                 datetime.strptime(
                                     real_end_date, '%Y-%m-%d'
                                 )
                                 - datetime.strptime(
                                     real_start_date, '%Y-%m-%d'
                                 )
                            ).days+1,
                         'ff_inici': real_start_date,
                         'ff_final': real_end_date
                         }
                    )
                    magic_pot_before_mitjana['potencies'].append(
                        current_period_pots
                    )
                    pondered_dict = self.get_pondered_median(magic_pot_before_mitjana)

                    factura_vals2 = consum_factures[-1]
                    energia_anterior = factura_vals2['consum_energia']
                    potencia_anterior = factura_vals2['consum_potencia']
                    periodes_energia2 = {}
                    periodes_potencia2 = {}
                    for periode in periodes_energia.keys():
                        periodes_energia2.update({
                            periode: periodes_energia[periode] +
                                     energia_anterior[1][periode]
                        })
                        periodes_potencia2.update({
                            periode: pondered_dict[periode]
                        })
                    energia_agrupada = (
                        energia_anterior[0] + factura_vals['consum_energia'][0],
                        periodes_energia2
                    )

                    potencia_agrupada = (
                        potencia_anterior[0] + factura_vals['consum_potencia'][
                            0],
                        periodes_potencia2
                    )
                    consum_factures.remove(factura_vals2)
                    factura_vals.update({
                        'data_inici': magic_pot_before_mitjana['data_inici'],
                        'data_final': magic_pot_before_mitjana['data_final'],
                        'consum_energia': energia_agrupada,
                        'consum_potencia': potencia_agrupada,
                    })
                else:
                    magic_pot_before_mitjana['data_inici'] = factura_vals['data_inici']
                    magic_pot_before_mitjana['data_final'] = factura_vals['data_final']
                    current_period_pots = deepcopy(periodes_potencia)

                    real_start_date = False
                    real_end_date = False

                    if datetime.strptime(factura_vals['data_inici'],
                                         '%Y-%m-%d') < datetime.strptime(
                            periode_consum[0], '%Y-%m-%d'):
                        real_start_date = periode_consum[0]
                    else:
                        real_start_date = factura_vals['data_inici']

                    if datetime.strptime(factura_vals['data_final'],
                                         '%Y-%m-%d') > datetime.strptime(
                            periode_consum[1], '%Y-%m-%d'):
                        real_end_date = periode_consum[1]
                    else:
                        real_end_date = factura_vals['data_final']

                    current_period_pots.update(
                        {'ndies':
                             (
                                 datetime.strptime(
                                     real_end_date, '%Y-%m-%d'
                                 )
                                 - datetime.strptime(
                                     real_start_date, '%Y-%m-%d'
                                 )
                             ).days+1,
                         'ff_inici': real_start_date,
                         'ff_final': real_end_date
                         }
                    )

                    magic_pot_before_mitjana['potencies'].append(
                        current_period_pots
                    )
                    factura_vals.update(
                        {
                            'data_inici': magic_pot_before_mitjana['data_inici'],
                            'data_final': magic_pot_before_mitjana['data_final']
                        }
                    )

                consum_factures.append(factura_vals)
                tarifa_anterior = tarifa_actual

        return consum_factures

    def get_pondered_median(self, magic_dict):
        # magic_pot_before_mitjana = {
        #     'data_inici': False,
        #     'data_final': False,
        #     'potencies': []  # list of dicts {'p1':..., 'p2':...., 'ndies':...}
        # }
        res = {}
        for period in range(1, 7):
            period_i = 'p{}'.format(period)
            res_p = 0.0
            if magic_dict['potencies'] and magic_dict['potencies'][0].get(period_i, False):
                fraction_sup_pon = sum([pots_p[period_i] * pots_p['ndies'] for pots_p in magic_dict['potencies']])
                fraction_inf_pon = sum([pots_p['ndies'] for pots_p in magic_dict['potencies']])
                res_p = fraction_sup_pon / fraction_inf_pon
            res[period_i] = res_p
        return res

    def get_suplements_consum(self, cups_id, consum, periode):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        data_inici = datetime.strptime(periode[0], '%Y-%m-%d')
        data_inici_periode = datetime.strptime(self.periodes_eval[1][0], '%Y-%m-%d')
        data_final = datetime.strptime(periode[1], '%Y-%m-%d')
        data_final_periode = datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
        ndays = (data_final - data_inici).days + 1
        periode = (
            1 if data_inici >= data_inici_periode else 0
            if data_final <= data_final_periode else -1
        )
        if periode == -1:
            raise except_osv(_('Warning'),
                "Fecha de Inicio o Fecha final de la agrupación no "
                "corresponden a un solo"
                "periodo de suplementos: [({})-({})] // [({})-({})]".format(
                    self.periodes_eval[0][0], self.periodes_eval[0][1],
                    self.periodes_eval[1][0], self.periodes_eval[1][1],
                )
            )
        cups_obj = self.pool.get('giscedata.cups.ps')
        ca_codi = cups_obj.browse(
            cursor, uid, cups_id, context=context
        ).id_provincia.comunitat_autonoma.codi

        suplements_consum = []
        for elements in consum:
            tarifa_name = elements['tarifa_name']
            supl_energia = (
                self.suplementos[ca_codi]['energia'][periode][tarifa_name]
            )
            periodes_energia = elements['consum_energia'][1]
            supl_potencia = (
                self.suplementos[ca_codi]['potencia'][periode][tarifa_name]
            )
            periodes_potencia = elements['consum_potencia'][1]
            imports_energia = []
            suplements_energia = []
            imports_potencia = []
            suplements_potencia = []
            for p in range(1, 7):
                if len(supl_energia) >= p:
                    suplements_energia.append(supl_energia[p - 1])
                    imports_energia.append(
                        periodes_energia['p{}'.format(p)] * supl_energia[p - 1]
                    )
                else:
                    suplements_energia.append(0)
                    imports_energia.append(0)
                if len(supl_potencia) >= p:
                    suplements_potencia.append(supl_potencia[p - 1])
                    imports_potencia.append(
                        (periodes_potencia['p{}'.format(p)] * 1000) * (
                            supl_potencia[p - 1]/365/1000
                        ) * ndays
                    )
                else:
                    suplements_potencia.append(0)
                    imports_potencia.append(0)
            imports = {
                'energia': imports_energia,
                'potencia': imports_potencia
            }
            suplements = {
                'energia': suplements_energia,
                'potencia': suplements_potencia
            }
            suplement_element = {
                'imports': imports,
                'suplements': suplements
            }
            suplements_consum.append(suplement_element)
        return suplements_consum

    def sort_by_comer(self, dict_consums):
        new_consums = {}
        for elem in dict_consums:
            cups_name = elem['cups']
            periode = elem['periode']
            consums = elem['consums']
            suplements = elem['suplements']
            for polissa in consums:
                new_consum = {
                    'cups': cups_name,
                    'tarifa': str(int(polissa['tarifa'])).zfill(2),
                    'periode': (polissa['data_inici'], polissa['data_final']),
                    'consum_energia_total': polissa['consum_energia'][0],
                    'consums_energia': polissa['consum_energia'][1],
                    'consum_potencia_total': polissa['consum_potencia'][0],
                    'consums_potencia': polissa['consum_potencia'][1],
                    'suplements': suplements[consums.index(polissa)],
                }
                if polissa['comer'] in new_consums.keys():
                    new_consums[polissa['comer']].append(new_consum)
                else:
                    new_consums.update({
                        polissa['comer']: [new_consum]
                    })

        return new_consums
