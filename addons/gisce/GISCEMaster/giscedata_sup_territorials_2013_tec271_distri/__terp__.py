# -*- coding: utf-8 -*-
{
    "name": "",
    "description": """
    Adaptacions per a la refacturació de suplements 
    territorials per a les distris, 2019 amb revisio TEC/271/2019.
    https://www.boe.es/diario_boe/txt.php?id=BOE-A-2019-3486
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Wizard Master",
    "depends":[
        "giscedata_contractacio_distri",
        "giscedata_liquidacions_etu35_2017",
        "giscedata_liquidacions_tec271_2019",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "giscedata_sup_territorials_2013_tec271_distri_view.xml",
        "wizard/wizard_exportar_suplementos_tec271_view.xml",
        "wizard/wizard_importar_suplementos_tec271_view.xml",
        "wizard/wizard_export_files_from_external_files_view.xml",
        "wizard/wizard_liquidar_linia_extra_tec_271_view.xml",
        "wizard/wizard_cargar_liquidaciones_baja_cambio_view.xml",
        "giscedata_facturacio_extra_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}