# -*- coding: utf-8 -*-

from osv import osv, fields
from comentarios_suplementos_retrofix import \
    SuplementosTerritorialesComments as SC


class FacturacioExtra(osv.osv):

    _name = 'giscedata.facturacio.extra'
    _inherit = 'giscedata.facturacio.extra'

    def facturar_pendiente_con_nueva_linea_extra(self, cursor, uid, extra_ids, tuple_format=None, context=None):
        '''
        Genera una nueva linea extra a partir de la linia extra con el
        pendiente a liquidar y un solo termino, cancelando al mismo tiempo la
        linea anterior.
        :param extra_ids: list of extra lines ids to be liquidated
        :param tuple_format: if true
                                :return [
                                    (original_extra_id, liquitation_extra_id),
                                    ...
                                    ]
                             else
                                :return [liquitation_extra_id, ....]
        '''
        if context is None:
            context = {}

        if not isinstance(extra_ids, (tuple, list)):
            extra_ids = [extra_ids]

        res = []

        for extra_id in extra_ids:
            info_original_extra = self.read(
                cursor, uid, extra_id,
                [
                    'total_amount_pending',
                    'total_amount_invoiced',
                    'price_subtotal', 'notes',
                    'product_id'
                ],
                context=context
            )

            # last_send_note = SC.get_last_sended_note_from_extraline_json_note(
            #     info_original_extra['notes']
            # )
            #
            # last_term = SC.get_last_term_from_extraline_json_note(
            #     last_send_note
            # )
            last_term = self.get_next_term(
                cursor, uid, id_extraline=extra_id, date=False, fact_id=False
            ) - 1

            if last_term:
                # S'ha facturat alguna cosa

                total_pendent_liquidar = info_original_extra[
                    'total_amount_pending']

                overwrite_liquidate_old_extra = {
                    'price_unit': info_original_extra['total_amount_invoiced'],
                    'term': last_term
                }

                # Defaults for new extra_line, we don't want a literal copy
                default_values_new_extra = {
                    'price_unit': total_pendent_liquidar,
                    'term': 1,
                    'notes': SC.set_liquidacio_on_json_of_notes(
                        info_original_extra['notes'],
                        ultim_terme_facturat=last_term
                    )
                    # total_amount_pending already copied correctly in theory
                }

                new_extra_id = self.copy(
                    cursor, uid,
                    extra_id,
                    context=context
                )

                self.write(
                    cursor, uid,
                    new_extra_id, default_values_new_extra,
                    context=context
                )

                if tuple_format:
                    res.append((extra_id, new_extra_id))
                else:
                    res.append(new_extra_id)

            else:
                # No s'ha facturat res encara de la extra
                overwrite_liquidate_old_extra = {
                    'term': 1,
                    'notes': SC.set_liquidacio_on_json_of_notes(
                        info_original_extra['notes']
                    )
                }

                if tuple_format:
                    res.append((False, extra_id))
                else:
                    res.append(extra_id)

            # Override Old line to cancel it
            self.write(
                cursor, uid,
                extra_id, overwrite_liquidate_old_extra,
                context=context
            )

        return res

FacturacioExtra()
