# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from datetime import date as datetime_date
from tools.translate import _
from osv.osv import except_osv
import pandas as pd
from StringIO import StringIO
from zipfile import ZIP_DEFLATED, ZipFile
from base64 import b64encode
from defs_suplementos_territoriales_2013_tec_271 import *
from gestionatr.defs import TABLA_107
from copy import deepcopy
from comentarios_suplementos_retrofix import SuplementosTerritorialesComments
from numpy import isnan, sum
from tqdm import tqdm
import numpy as np


class GiscedataSuplementsTerritorialsLoadLiquidacions(object):
    codi_comunitats = [
        '01',  # Andalucía
        '02',  # Aragón
        '03',  # Asturais
        '06',  # Cantabria
        '07',  # Castilla y león
        '09',  # Cataluña
        '11',  # Extremadura
        '12',  # Galicia
        '13',  # Madrid
        '14',  # Murcia
        '15',  # Navarra
    ]

    # Periodos para precios de los términos de potencia y energía activa
    periodes_eval = [
        ('2013-01-01', '2013-07-31'),
        ('2013-08-01', '2013-12-31'),
    ]

    suplementos = {
        '01': {  # Andalucía
            'comunidad': '01',
            'potencia': [
                potencia_andalucia_period_1, potencia_andalucia_period_2
            ],
            'energia': [energia_andalucia_period_1, energia_andalucia_period_2],
        },
        '02': {  # Aragón
            'comunidad': '02',
            'potencia': [potencia_aragon_period_1, potencia_aragon_period_2],
            'energia': [energia_aragon_period_1, energia_aragon_period_2],
        },
        '03': {  # Asturais
            'comunidad': '03',
            'potencia': [potencia_ast_period_1, potencia_ast_period_2],
            'energia': [energia_ast_period_1, energia_ast_period_2],
        },
        '06': {  # Cantabria
            'comunidad': '06',
            'potencia': [
                potencia_cantabria_period_1, potencia_cantabria_period_2
            ],
            'energia': [energia_cantabria_period_1, energia_cantabria_period_2],
        },
        '07': {  # Castilla y león
            'comunidad': '07',
            'potencia': [potencia_cale_period_1, potencia_cale_period_2],
            'energia': [energia_cale_period_1, energia_cale_period_2],
        },
        '09': {  # Cataluña
            'comunidad': '09',
            'potencia': [potencia_cat_period_1, potencia_cat_1_period_2],
            'energia': [energia_cat_period_1, energia_cat_period_2],
        },
        '11': {  # Extremadura
            'comunidad': '11',
            'potencia': [potencia_ext_period_1, potencia_ext_period_2],
            'energia': [energia_ext_period_1, energia_ext_period_2],
        },
        '12': {  # Galicia
            'comunidad': '12',
            'potencia': [potencia_gal_period_1, potencia_gal_period_2],
            'energia': [energia_gal_period_1, energia_gal_period_2],
        },
        '13': {  # Madrid
            'comunidad': '13',
            'potencia': [potencia_mad_period_1, potencia_mad_period_2],
            'energia': [energia_mad_period_1, energia_mad_period_2],
        },
        '14': {  # Murcia
            'comunidad': '14',
            'potencia': [potencia_mur_period_1, potencia_mur_period_2],
            'energia': [energia_mur_period_1, energia_mur_period_2],
        },
        '15': {  # Navarra
            'comunidad': '15',
            'potencia': [potencia_nav_period_1, potencia_nav_period_2],
            'energia': [energia_nav_period_1, energia_nav_period_2],
        },

    }

    export_valid_header = [
        u'CUPS',
        u'TarifaATRFact',
        u'FechaDesde',
        u'FechaHasta',
        u'NumeroDias',
        u'MediaPondPotenciaAFacturar_P1 (W)',
        u'MediaPondPotenciaAFacturar_P2 (W)',
        u'MediaPondPotenciaAFacturar_P3 (W)',
        u'MediaPondPotenciaAFacturar_P4 (W)',
        u'MediaPondPotenciaAFacturar_P5 (W)',
        u'MediaPondPotenciaAFacturar_P6 (W)',
        u'SuplementoPotencia_P1 (€/W)',
        u'SuplementoPotencia_P2 (€/W)',
        u'SuplementoPotencia_P3 (€/W)',
        u'SuplementoPotencia_P4 (€/W)',
        u'SuplementoPotencia_P5 (€/W)',
        u'SuplementoPotencia_P6 (€/W)',
        u'ImporteSuplementoTerminoPotencia_P1 (€)',
        u'ImporteSuplementoTerminoPotencia_P2 (€)',
        u'ImporteSuplementoTerminoPotencia_P3 (€)',
        u'ImporteSuplementoTerminoPotencia_P4 (€)',
        u'ImporteSuplementoTerminoPotencia_P5 (€)',
        u'ImporteSuplementoTerminoPotencia_P6 (€)',
        u'ImporteTotalSuplementoTerminoPotencia (€)',
        u'ValorEnergiaActiva_P1 (kWh)',
        u'ValorEnergiaActiva_P2 (kWh)',
        u'ValorEnergiaActiva_P3 (kWh)',
        u'ValorEnergiaActiva_P4 (kWh)',
        u'ValorEnergiaActiva_P5 (kWh)',
        u'ValorEnergiaActiva_P6 (kWh)',
        u'SuplementoEnergia_P1 (€/kWh)',
        u'SuplementoEnergia_P2 (€/kWh)',
        u'SuplementoEnergia_P3 (€/kWh)',
        u'SuplementoEnergia_P4 (€/kWh)',
        u'SuplementoEnergia_P5 (€/kWh)',
        u'SuplementoEnergia_P6 (€/kWh)',
        u'ImporteSuplementoTerminoEnergia_P1 (€)',
        u'ImporteSuplementoTerminoEnergia_P2 (€)',
        u'ImporteSuplementoTerminoEnergia_P3 (€)',
        u'ImporteSuplementoTerminoEnergia_P4 (€)',
        u'ImporteSuplementoTerminoEnergia_P5 (€)',
        u'ImporteSuplementoTerminoEnergia_P6 (€)',
        u'ImporteTotalSuplementoTerminoEnergia (€)',
        u'ImporteTotalARegularizar (€)'
    ]

    valid_header_conv_dict = {
        u'CUPS': u'CUPS',
        u'TarifaATRFact': u'TarifaATRFact',
        u'FechaDesde': u'FechaDesde',
        u'FechaHasta': u'FechaHasta',
        u'MediaPondPotenciaAFacturar_P1(W)': u'MediaPondPotenciaAFacturar_P1 (W)',
        u'MediaPondPotenciaAFacturar_P2(W)': u'MediaPondPotenciaAFacturar_P2 (W)',
        u'MediaPondPotenciaAFacturar_P3(W)': u'MediaPondPotenciaAFacturar_P3 (W)',
        u'MediaPondPotenciaAFacturar_P4(W)': u'MediaPondPotenciaAFacturar_P4 (W)',
        u'MediaPondPotenciaAFacturar_P5(W)': u'MediaPondPotenciaAFacturar_P5 (W)',
        u'MediaPondPotenciaAFacturar_P6(W)': u'MediaPondPotenciaAFacturar_P6 (W)',
        u'SuplementoPotencia_P1(€/W)': u'SuplementoPotencia_P1 (€/W)',
        u'SuplementoPotencia_P2(€/W)': u'SuplementoPotencia_P2 (€/W)',
        u'SuplementoPotencia_P3(€/W)': u'SuplementoPotencia_P3 (€/W)',
        u'SuplementoPotencia_P4(€/W)': u'SuplementoPotencia_P4 (€/W)',
        u'SuplementoPotencia_P5(€/W)': u'SuplementoPotencia_P5 (€/W)',
        u'SuplementoPotencia_P6(€/W)': u'SuplementoPotencia_P6 (€/W)',
        u'ValorEnergiaActiva_P1(kWh)': u'ValorEnergiaActiva_P1 (kWh)',
        u'ValorEnergiaActiva_P2(kWh)': u'ValorEnergiaActiva_P2 (kWh)',
        u'ValorEnergiaActiva_P3(kWh)': u'ValorEnergiaActiva_P3 (kWh)',
        u'ValorEnergiaActiva_P4(kWh)': u'ValorEnergiaActiva_P4 (kWh)',
        u'ValorEnergiaActiva_P5(kWh)': u'ValorEnergiaActiva_P5 (kWh)',
        u'ValorEnergiaActiva_P6(kWh)': u'ValorEnergiaActiva_P6 (kWh)',
        u'SuplementoEnergia_P1(€/kWh)': u'SuplementoEnergia_P1 (€/kWh)',
        u'SuplementoEnergia_P2(€/kWh)': u'SuplementoEnergia_P2 (€/kWh)',
        u'SuplementoEnergia_P3(€/kWh)': u'SuplementoEnergia_P3 (€/kWh)',
        u'SuplementoEnergia_P4(€/kWh)': u'SuplementoEnergia_P4 (€/kWh)',
        u'SuplementoEnergia_P5(€/kWh)': u'SuplementoEnergia_P5 (€/kWh)',
        u'SuplementoEnergia_P6(€/kWh)': u'SuplementoEnergia_P6(€/kWh)',
    }

    def __init__(self, pool, cursor, uid, context=None):
        self.pool = pool
        self.cursor = cursor
        self.uid = uid
        self.context = {} if context is None else context
        self.mapping_fucking_liquidacions = {}  # Maping liquidacions tipo

    def get_pondered_median(self, magic_dict):
        # magic_pot_before_mitjana = {
        #     'data_inici': False,
        #     'data_final': False,
        #     'potencies': []  # list of dicts {'p1':..., 'p2':...., 'ndies':...}
        # }
        res = {}
        for period in range(1, 7):
            period_i = 'p{}'.format(period)
            res_p = 0.0
            if magic_dict['potencies'] and magic_dict['potencies'][0].get(period_i, False):
                fraction_sup_pon = sum([pots_p[period_i] * pots_p['ndies'] for pots_p in magic_dict['potencies']])
                fraction_inf_pon = sum([pots_p['ndies'] for pots_p in magic_dict['potencies']])
                res_p = fraction_sup_pon / fraction_inf_pon
            res[period_i] = res_p
        return res

    def remove_factures_abonades(self, factures):
        if not factures:
            return factures
        cursor = self.cursor
        uid = self.uid
        context = self.context

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        facturas_n_rectificadas = fact_obj.search(
            cursor, uid,
            [
                ('id', 'in', factures),
                ('rectificative_type', '=', 'N'),
                ('refund_by_id', '!=', False)
            ],
            context=context
        )

        if facturas_n_rectificadas:

            invoice_ids_n_rectificades = [
                f['invoice_id'][0]
                for f in fact_obj.read(
                    cursor, uid, facturas_n_rectificadas, ['invoice_id'], context=context)
            ]

            b_to_remove = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', factures),
                    ('rectificative_type', '=', 'B'),
                ],
                context=context
            )

            rectified_invoice_ids_from_b = fact_obj.read(
                cursor, uid, b_to_remove, ['rectifying_id'], context=context
            )
            rectified_invoice_ids_from_b = [
                f_b['rectifying_id'][0] for f_b in rectified_invoice_ids_from_b
            ]

            n_to_be_destroyed = fact_obj.search(
                cursor, uid,
                [('invoice_id', 'in', rectified_invoice_ids_from_b)],
                context=context
            )

            r_in_2013 = fact_obj.search(
                cursor, uid,
                [('rectifying_id', 'in', invoice_ids_n_rectificades),
                 ('data_inici', '>=', self.periodes_eval[0][0]),
                 ('data_inici', '<=', self.periodes_eval[1][1]),
                 ('rectificative_type', '=', 'R')
                 ]
            )

            factures = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', list(set(list(set(factures) - set(n_to_be_destroyed) - set(b_to_remove) - set(facturas_n_rectificadas)) + r_in_2013)))
                ], context=context, order='data_inici asc'
            )
        else:
            b_to_remove = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', factures),
                    ('rectificative_type', '=', 'B'),
                ],
                context=context
            )

            rectified_invoice_ids_from_b = fact_obj.read(
                cursor, uid, b_to_remove, ['rectifying_id'], context=context
            )
            rectified_invoice_ids_from_b = [
                f_b['rectifying_id'][0] for f_b in rectified_invoice_ids_from_b
            ]

            n_to_be_destroyed = fact_obj.search(
                cursor, uid,
                [('invoice_id', 'in', rectified_invoice_ids_from_b)],
                context=context
            )

            factures = fact_obj.search(
                cursor, uid,
                [
                    ('id', 'in', list(set(list(set(factures) - set(n_to_be_destroyed) - set(b_to_remove)))))
                ], context=context, order='data_inici asc'
            )

        return factures

    def get_affected_cups_baixa_and_canvi_titular(self):
        """
        Devuelve los cups afectados por suplementos
        de baja y cambio de titular
        """

        cursor = self.cursor
        uid = self.uid
        context = self.context

        cups_obj = self.pool.get('giscedata.cups.ps')
        comunitat_obj = self.pool.get('res.comunitat_autonoma')
        provincia_obj = self.pool.get('res.country.state')
        pol_obj = self.pool.get('giscedata.polissa')
        mod_obj = self.pool.get('giscedata.polissa.modcontractual')

        # Busqueda de las comunidades afectadas
        id_comunitats = comunitat_obj.search(
            cursor, uid,
            [('codi', 'in', self.codi_comunitats)],
            context=context
        )

        # Busqueda de las provincias de las comunidades afectadas
        id_provincies = provincia_obj.search(
            cursor, uid,
            [('comunitat_autonoma', 'in', id_comunitats)],
            context=context
        )

        ctx = context.copy()
        ctx.update({'active_test': False})

        cups_ids = cups_obj.search(
            cursor, uid,
            [('id_provincia', 'in', id_provincies)],
            context=ctx
        )

        cups_polisses = cups_obj.read(
            cursor, uid, cups_ids, ['polisses', 'polissa_polissa'],
            context=context
        )

        res_cups = []

        # Only get cups with titular in titulars of 2013
        for polisses_in_single_cups in cups_polisses:

            titular_actual = False
            if polisses_in_single_cups['polissa_polissa']:
                titular_actual = pol_obj.read(
                    cursor, uid,
                    polisses_in_single_cups['polissa_polissa'][0],
                    ['titular'],
                    context=context
                )['titular'][0]

            polisses_under_range = mod_obj.search(
                cursor, uid,
                [
                    ('id', 'in', polisses_in_single_cups['polisses']),
                    ('data_inici', '<', self.periodes_eval[1][1]),
                    '|', ('data_final', '>', self.periodes_eval[0][0]),
                    ('data_final', '=', False)
                ], context=ctx, order='data_inici asc')

            if polisses_under_range:

                if titular_actual:

                    poliza_baja = pol_obj.read(
                        cursor, uid,
                        polisses_in_single_cups['polissa_polissa'][0],
                        ['state'],
                        context=ctx
                    )['state'] in ('baixa',)

                    if poliza_baja:
                        res_cups.append(polisses_in_single_cups['id'])
                        self.mapping_fucking_liquidacions[polisses_in_single_cups['id']] = '1'
                    else:
                        titulars_2013 = mod_obj.read(
                            cursor, uid, polisses_under_range,
                            ['titular'],
                            context=context
                        )

                        titulars_2013 = [pol['titular'][0] for pol in titulars_2013 if
                                         pol['titular']]

                        if titular_actual not in titulars_2013:
                            res_cups.append(polisses_in_single_cups['id'])
                            self.mapping_fucking_liquidacions[polisses_in_single_cups['id']] = '2'
                else:
                    # BAJA
                    res_cups.append(polisses_in_single_cups['id'])
                    self.mapping_fucking_liquidacions[
                        polisses_in_single_cups['id']] = '1'

        return list(set(res_cups))

    def get_consum_cups(self, cups_id, periode_consum):

        cursor = self.cursor
        uid = self.uid
        context = self.context

        data_inici, data_final = periode_consum
        consum_factures = []
        polissa_obj = self.pool.get('giscedata.polissa')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        lect_energia_obj = self.pool.get(
            'giscedata.facturacio.lectures.energia'
        )
        lect_potencia_obj = self.pool.get(
            'giscedata.facturacio.factura.linia'
        )
        tarifa_obj = self.pool.get(
            'giscedata.polissa.tarifa'
        )

        ctx = context.copy()
        ctx.update({'active_test': False})
        # TODO revisar fechas de la busqueda
        polisses = polissa_obj.search(
            cursor, uid,
            [
                ('cups', '=', cups_id), ('data_alta', '<', periode_consum[1]),
                '|', ('data_baixa', '>', periode_consum[0]),
                ('data_baixa', '=', False)
            ], context=ctx, order='cups asc, data_alta asc')

        tarifa_anterior = 0
        magic_pot_before_mitjana = {
            'data_inici': False,
            'data_final': False,
            'potencies': []  # list of dicts {'p1':..., 'p2':...., 'ndies':...}
        }

        for polissa in polisses:
            factures = factura_obj.search(
                cursor, uid,
                [
                    ('polissa_id', '=', polissa),
                    ('data_inici', '<=', data_final),
                    ('data_final', '>=', data_inici),
                ], context=context, order='data_inici asc'
            )
            factures = self.remove_factures_abonades(factures)

            for factura_id in factures:
                multiple_periods = False
                factura_vals = factura_obj.read(
                    cursor, uid,
                    factura_id, [
                        'lectures_energia_ids', 'linies_potencia',
                        'tarifa_acces_id', 'data_inici', 'data_final',
                        'rectificative_type', 'tipo_rectificadora',
                    ], context=context
                )
                i_fecha_datetime = datetime.strptime(
                    factura_vals['data_inici'], '%Y-%m-%d'
                )
                f_fecha_datetime = datetime.strptime(
                    factura_vals['data_final'], '%Y-%m-%d'
                )

                tariff_id = factura_vals['tarifa_acces_id'][0]
                tarifa_name = factura_vals['tarifa_acces_id'][1]

                tarifa_name = tarifa_name.replace(' LB', '')

                energia_ids = factura_vals['lectures_energia_ids']
                potencia_ids = factura_vals['linies_potencia']
                lect_energia = lect_energia_obj.read(
                    cursor, uid, energia_ids, ['consum', 'name', 'tipus'],
                    context=context
                )

                periodes_energia = {}
                consum_energia = 0
                lect_potencia = lect_potencia_obj.read(
                    cursor, uid,
                    potencia_ids, ['quantity', 'name', 'data_desde', 'data_fins'],
                    context=context
                )

                if i_fecha_datetime < datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d') and f_fecha_datetime > datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d'):
                    multiple_periods = True
                    new_lect_potencia = []
                    if periode_consum == self.periodes_eval[0]: # Estem al primer periode

                        for lp in lect_potencia:
                            fecha_inicio_lectura_in_first = (
                                datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                <=
                                datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
                            )
                            fecha_fin_lectura_in_second = (
                                datetime.strptime(lp['data_fins'], '%Y-%m-%d')
                                >
                                datetime.strptime(self.periodes_eval[0][1],
                                                  '%Y-%m-%d')
                            )
                            if fecha_inicio_lectura_in_first and fecha_fin_lectura_in_second:
                                t_days = (datetime.strptime(lp['data_fins'], '%Y-%m-%d') - datetime.strptime(lp['data_desde'], '%Y-%m-%d')).days + 1
                                to_fact_days = (
                                                   datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
                                                   -
                                                   datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                               ).days + 1
                                # real_quantity_period = to_fact_days * lp['quantity'] / t_days
                                # lp['quantity'] = real_quantity_period
                                lp['data_fins'] = self.periodes_eval[0][1]

                                new_lect_potencia.append(lp)
                            elif fecha_inicio_lectura_in_first and not fecha_fin_lectura_in_second:
                                new_lect_potencia.append(lp)
                            # else:
                            #     print "descartada periode 1", lp

                    else:  # Segon periode
                        for lp in lect_potencia:
                            fecha_inicio_lectura_in_first = (
                                datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                <=
                                datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
                            )
                            fecha_fin_lectura_in_second = (
                                datetime.strptime(lp['data_fins'], '%Y-%m-%d')
                                >
                                datetime.strptime(self.periodes_eval[0][1],
                                                  '%Y-%m-%d')
                            )
                            if fecha_inicio_lectura_in_first and fecha_fin_lectura_in_second:
                                t_days = (
                                             datetime.strptime(lp['data_fins'], '%Y-%m-%d')
                                             -
                                             datetime.strptime(lp['data_desde'], '%Y-%m-%d')
                                         ).days + 1

                                to_fact_days = (
                                                   datetime.strptime(
                                                       lp['data_fins'],
                                                       '%Y-%m-%d'
                                                   )
                                                   -
                                                   datetime.strptime(
                                                       self.periodes_eval[1][0],
                                                       '%Y-%m-%d')
                                               ).days + 1



                                # real_quantity_period = to_fact_days * lp[
                                #     'quantity'] / t_days
                                # lp['quantity'] = real_quantity_period
                                lp['data_desde'] = self.periodes_eval[1][0]

                                new_lect_potencia.append(lp)
                            elif not fecha_inicio_lectura_in_first and fecha_fin_lectura_in_second:
                                new_lect_potencia.append(lp)
                            # else:
                            #     print "descartada periode 2", lp
                    lect_potencia = new_lect_potencia

                periodes_potencia = {}
                consum_potencia = 0
                for num_periode in range(1, 7):
                    consum_energia_periode = sum([
                        x['consum'] for x in lect_energia
                        if (x['name'] == '{} (P{})'.format(tarifa_name, num_periode)
                            ) and x['tipus'] == 'activa'
                    ])

                    if (factura_vals['rectificative_type'] and factura_vals['rectificative_type'] == 'B'):
                        consum_energia_periode *= -1
                    if (factura_vals['data_inici'] < data_inici
                        or factura_vals['data_final'] > data_final):
                        extra_days = 0
                        if factura_vals['data_inici'] < data_inici:
                            extra_days = (datetime.strptime(
                                data_inici, '%Y-%m-%d'
                            ) - datetime.strptime(
                                factura_vals['data_inici'], '%Y-%m-%d'
                            )).days
                        elif factura_vals['data_final'] > data_final:
                            extra_days = (datetime.strptime(
                                factura_vals['data_final'], '%Y-%m-%d'
                            ) - datetime.strptime(
                                data_final, '%Y-%m-%d'
                            )).days
                        factura_days = (datetime.strptime(
                            factura_vals['data_final'], '%Y-%m-%d'
                        ) - datetime.strptime(
                            factura_vals['data_inici'], '%Y-%m-%d'
                        )).days
                        consum_energia_periode = (
                            consum_energia_periode -
                            extra_days * consum_energia_periode / factura_days)
                    periodes_energia.update({
                        'p{}'.format(num_periode): consum_energia_periode
                    })

                    if not factura_vals['tipo_rectificadora'] in ['B']:
                        upper_fraction_value = 0
                        down_fraction_value = 0

                        for l_pot in lect_potencia:
                            if l_pot['name'] == 'P{}'.format(num_periode):
                                days_lect_pot = (
                                    datetime.strptime(l_pot['data_fins'], '%Y-%m-%d')
                                    - datetime.strptime(l_pot['data_desde'], '%Y-%m-%d')
                                ).days + 1
                                upper_fraction_value += (days_lect_pot * l_pot['quantity'])
                                down_fraction_value += days_lect_pot

                        if upper_fraction_value:
                            periodes_potencia.update({'p{}'.format(num_periode): upper_fraction_value / down_fraction_value})
                        else:
                            periodes_potencia.update({'p{}'.format(num_periode): 0})

                    consum_energia += periodes_energia[
                        'p{}'.format(num_periode)]
                    consum_potencia += periodes_potencia[
                        'p{}'.format(num_periode)]

                if tarifa_name in ['3.0A', '3.1A']:
                    consum_energia = 0
                    consum_potencia = 0
                    for num_periode in range(1, 4):
                        nom_periode = 'p{}'.format(num_periode)
                        nom_periode2 = 'p{}'.format(num_periode + 3)
                        periodes_energia.update({
                            nom_periode: (periodes_energia[nom_periode] +
                                          periodes_energia[nom_periode2]),
                            nom_periode2: 0
                        })
                        # periodes_potencia.update({nom_periode2: 0})
                        consum_energia += periodes_energia[nom_periode]
                        # consum_potencia += periodes_potencia[nom_periode]
                factura_vals.pop('lectures_energia_ids')
                factura_vals.pop('linies_potencia')

                polissa_pol = polissa_obj.browse(
                    cursor, uid, polissa, context=context
                )
                comer = polissa_pol.cups.polissa_polissa.comercialitzadora.ref if polissa_pol.cups.polissa_polissa.comercialitzadora else False

                factura_vals.update({
                    'consum_energia': (consum_energia, periodes_energia),
                    'consum_potencia': (consum_potencia, periodes_potencia),
                    'tarifa_name': tarifa_name,
                    'tarifa': tarifa_obj.read(
                        cursor, uid,
                        factura_vals['tarifa_acces_id'][0],
                        ['codi_ocsum'],
                        context=context
                    )['codi_ocsum'][1:],
                    'comer': comer
                })
                tarifa_actual = factura_vals['tarifa']

                # TODO POSAR IF O NO?
                #if periode_consum == self.periodes_eval[0]:  # Estem al primer periode
                if datetime.strptime(factura_vals['data_final'], '%Y-%m-%d') > datetime.strptime(periode_consum[1], '%Y-%m-%d'):
                    factura_vals['data_final'] = periode_consum[1]
                #else:
                if datetime.strptime(factura_vals['data_inici'], '%Y-%m-%d') < datetime.strptime(periode_consum[0], '%Y-%m-%d'):
                    factura_vals['data_inici'] = periode_consum[0]

                if tarifa_anterior == tarifa_actual:
                    current_period_pots = deepcopy(periodes_potencia)
                    real_start_date = False
                    real_end_date = False

                    if datetime.strptime(factura_vals['data_inici'], '%Y-%m-%d') < datetime.strptime(periode_consum[0], '%Y-%m-%d'):
                        real_start_date = periode_consum[0]
                    else:
                        real_start_date = factura_vals['data_inici']

                    if datetime.strptime(factura_vals['data_final'], '%Y-%m-%d') > datetime.strptime(periode_consum[1], '%Y-%m-%d'):
                        real_end_date = periode_consum[1]
                    else:
                        real_end_date = factura_vals['data_final']

                    if datetime.strptime(real_end_date, '%Y-%m-%d') > datetime.strptime(magic_pot_before_mitjana['data_final'], '%Y-%m-%d'):
                        magic_pot_before_mitjana['data_final'] = real_end_date

                    if datetime.strptime(real_start_date, '%Y-%m-%d') < datetime.strptime(magic_pot_before_mitjana['data_inici'], '%Y-%m-%d'):
                        magic_pot_before_mitjana['data_inici'] = real_start_date

                    current_period_pots.update(
                        {'ndies':
                            (
                                 datetime.strptime(
                                     real_end_date, '%Y-%m-%d'
                                 )
                                 - datetime.strptime(
                                     real_start_date, '%Y-%m-%d'
                                 )
                            ).days+1,
                         'ff_inici': real_start_date,
                         'ff_final': real_end_date
                         }
                    )
                    magic_pot_before_mitjana['potencies'].append(
                        current_period_pots
                    )
                    pondered_dict = self.get_pondered_median(magic_pot_before_mitjana)

                    factura_vals2 = consum_factures[-1]
                    energia_anterior = factura_vals2['consum_energia']
                    potencia_anterior = factura_vals2['consum_potencia']
                    periodes_energia2 = {}
                    periodes_potencia2 = {}
                    for periode in periodes_energia.keys():
                        periodes_energia2.update({
                            periode: periodes_energia[periode] +
                                     energia_anterior[1][periode]
                        })
                        periodes_potencia2.update({
                            periode: pondered_dict[periode]
                        })
                    energia_agrupada = (
                        energia_anterior[0] + factura_vals['consum_energia'][0],
                        periodes_energia2
                    )

                    potencia_agrupada = (
                        potencia_anterior[0] + factura_vals['consum_potencia'][
                            0],
                        periodes_potencia2
                    )
                    consum_factures.remove(factura_vals2)
                    factura_vals.update({
                        'data_inici': magic_pot_before_mitjana['data_inici'],
                        'data_final': magic_pot_before_mitjana['data_final'],
                        'consum_energia': energia_agrupada,
                        'consum_potencia': potencia_agrupada,
                    })
                else:
                    magic_pot_before_mitjana['data_inici'] = factura_vals['data_inici']
                    magic_pot_before_mitjana['data_final'] = factura_vals['data_final']
                    current_period_pots = deepcopy(periodes_potencia)

                    real_start_date = False
                    real_end_date = False

                    if datetime.strptime(factura_vals['data_inici'],
                                         '%Y-%m-%d') < datetime.strptime(
                            periode_consum[0], '%Y-%m-%d'):
                        real_start_date = periode_consum[0]
                    else:
                        real_start_date = factura_vals['data_inici']

                    if datetime.strptime(factura_vals['data_final'],
                                         '%Y-%m-%d') > datetime.strptime(
                            periode_consum[1], '%Y-%m-%d'):
                        real_end_date = periode_consum[1]
                    else:
                        real_end_date = factura_vals['data_final']

                    current_period_pots.update(
                        {'ndies':
                             (
                                 datetime.strptime(
                                     real_end_date, '%Y-%m-%d'
                                 )
                                 - datetime.strptime(
                                     real_start_date, '%Y-%m-%d'
                                 )
                             ).days+1,
                         'ff_inici': real_start_date,
                         'ff_final': real_end_date
                         }
                    )

                    magic_pot_before_mitjana['potencies'].append(
                        current_period_pots
                    )
                    factura_vals.update(
                        {
                            'data_inici': magic_pot_before_mitjana['data_inici'],
                            'data_final': magic_pot_before_mitjana['data_final']
                        }
                    )

                consum_factures.append(factura_vals)
                tarifa_anterior = tarifa_actual

        return consum_factures

    def get_suplements_consum(self, cups_id, consum, periode):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        data_inici = datetime.strptime(periode[0], '%Y-%m-%d')
        data_inici_periode = datetime.strptime(self.periodes_eval[1][0], '%Y-%m-%d')
        data_final = datetime.strptime(periode[1], '%Y-%m-%d')
        data_final_periode = datetime.strptime(self.periodes_eval[0][1], '%Y-%m-%d')
        ndays = (data_final - data_inici).days + 1
        periode = (
            1 if data_inici >= data_inici_periode else 0
            if data_final <= data_final_periode else -1
        )
        if periode == -1:
            raise except_osv(_('Warning'),
                "Fecha de Inicio o Fecha final de la agrupación no "
                "corresponden a un solo"
                "periodo de suplementos: [({})-({})] // [({})-({})]".format(
                    self.periodes_eval[0][0], self.periodes_eval[0][1],
                    self.periodes_eval[1][0], self.periodes_eval[1][1],
                )
            )
        cups_obj = self.pool.get('giscedata.cups.ps')
        ca_codi = cups_obj.browse(
            cursor, uid, cups_id, context=context
        ).id_provincia.comunitat_autonoma.codi

        suplements_consum = []
        for elements in consum:
            tarifa_name = elements['tarifa_name']
            supl_energia = (
                self.suplementos[ca_codi]['energia'][periode][tarifa_name]
            )
            periodes_energia = elements['consum_energia'][1]
            supl_potencia = (
                self.suplementos[ca_codi]['potencia'][periode][tarifa_name]
            )
            periodes_potencia = elements['consum_potencia'][1]
            imports_energia = []
            suplements_energia = []
            imports_potencia = []
            suplements_potencia = []
            for p in range(1, 7):
                if len(supl_energia) >= p:
                    suplements_energia.append(supl_energia[p - 1])
                    imports_energia.append(
                        periodes_energia['p{}'.format(p)] * supl_energia[p - 1]
                    )
                else:
                    suplements_energia.append(0)
                    imports_energia.append(0)
                if len(supl_potencia) >= p:
                    suplements_potencia.append(supl_potencia[p - 1])
                    imports_potencia.append(
                        (periodes_potencia['p{}'.format(p)] * 1000) * (
                            supl_potencia[p - 1]/365/1000
                        ) * ndays
                    )
                else:
                    suplements_potencia.append(0)
                    imports_potencia.append(0)
            imports = {
                'energia': imports_energia,
                'potencia': imports_potencia
            }
            suplements = {
                'energia': suplements_energia,
                'potencia': suplements_potencia
            }
            suplement_element = {
                'imports': imports,
                'suplements': suplements
            }
            suplements_consum.append(suplement_element)
        return suplements_consum

    def sort_by_comer(self, dict_consums):
        new_consums = {}
        for elem in dict_consums:
            cups_name = elem['cups']
            periode = elem['periode']
            consums = elem['consums']
            suplements = elem['suplements']
            for polissa in consums:
                new_consum = {
                    'cups': cups_name,
                    'tarifa': str(int(polissa['tarifa'])).zfill(2),
                    'periode': (polissa['data_inici'], polissa['data_final']),
                    'consum_energia_total': polissa['consum_energia'][0],
                    'consums_energia': polissa['consum_energia'][1],
                    'consum_potencia_total': polissa['consum_potencia'][0],
                    'consums_potencia': polissa['consum_potencia'][1],
                    'suplements': suplements[consums.index(polissa)],
                }
                if polissa['comer'] in new_consums.keys():
                    new_consums[polissa['comer']].append(new_consum)
                else:
                    new_consums.update({
                        polissa['comer']: [new_consum]
                    })

        return new_consums

    def export_consums_to_file(self, consums):

        # Create dataframe with sorted valid header
        df_consums_complet = pd.DataFrame(columns=self.export_valid_header)
        for comer_code in consums.keys():
            # comer_consums contains info to generate file
            comer_consums = consums.get(comer_code)
            # individual file for any comer

            df_comer = pd.DataFrame(columns=self.export_valid_header)

            for line_consum in comer_consums:
                cups = line_consum['cups']
                periode = line_consum['periode']
                suplements = line_consum['suplements']
                ndies = (
                    datetime.strptime(periode[1], '%Y-%m-%d') -
                    datetime.strptime(periode[0], '%Y-%m-%d')
                ).days +1
                periodes_potencia = line_consum['consums_potencia']
                periodes_energia = line_consum['consums_energia']

                suplements_potencia = (suplements['suplements']['potencia'])

                imports_potencia = suplements['imports']['potencia']
                total_potencia = sum(imports_potencia)
                suplements_energia = suplements['suplements']['energia']
                imports_energia = suplements['imports']['energia']
                total_energia = sum(imports_energia)

                line_dict = {
                    u'CUPS': cups,
                    u'TarifaATRFact': line_consum['tarifa'],
                    u'FechaDesde': (datetime.strptime(periode[0], '%Y-%m-%d') - timedelta(days=1)).strftime('%Y-%m-%d'),
                    u'FechaHasta': periode[1],
                    u'NumeroDias': ndies,
                    u'MediaPondPotenciaAFacturar_P1 (W)': long(round(periodes_potencia['p1'] * 1000, 0)),
                    u'MediaPondPotenciaAFacturar_P2 (W)': long(round(periodes_potencia['p2'] * 1000, 0)),
                    u'MediaPondPotenciaAFacturar_P3 (W)': long(round(periodes_potencia['p3'] * 1000, 0)),
                    u'MediaPondPotenciaAFacturar_P4 (W)': long(round(periodes_potencia['p4'] * 1000, 0)),
                    u'MediaPondPotenciaAFacturar_P5 (W)': long(round(periodes_potencia['p5'] * 1000, 0)),
                    u'MediaPondPotenciaAFacturar_P6 (W)': long(round(periodes_potencia['p6'] * 1000, 0)),
                    u'SuplementoPotencia_P1 (€/W)': round(suplements_potencia[0] / 365 / 1000 * ndies, 9),
                    u'SuplementoPotencia_P2 (€/W)': round(suplements_potencia[1] / 365 / 1000 * ndies, 9),
                    u'SuplementoPotencia_P3 (€/W)': round(suplements_potencia[2] / 365 / 1000 * ndies, 9),
                    u'SuplementoPotencia_P4 (€/W)': round(suplements_potencia[3] / 365 / 1000 * ndies, 9),
                    u'SuplementoPotencia_P5 (€/W)': round(suplements_potencia[4] / 365 / 1000 * ndies, 9),
                    u'SuplementoPotencia_P6 (€/W)': round(suplements_potencia[5] / 365 / 1000 * ndies, 9),
                    u'ImporteSuplementoTerminoPotencia_P1 (€)': round(imports_potencia[0], 2),
                    u'ImporteSuplementoTerminoPotencia_P2 (€)': round(imports_potencia[1], 2),
                    u'ImporteSuplementoTerminoPotencia_P3 (€)': round(imports_potencia[2], 2),
                    u'ImporteSuplementoTerminoPotencia_P4 (€)': round(imports_potencia[3], 2),
                    u'ImporteSuplementoTerminoPotencia_P5 (€)': round(imports_potencia[4], 2),
                    u'ImporteSuplementoTerminoPotencia_P6 (€)': round(imports_potencia[5], 2),
                    u'ImporteTotalSuplementoTerminoPotencia (€)': round(total_potencia, 2),
                    u'ValorEnergiaActiva_P1 (kWh)': round(periodes_energia['p1'], 2),
                    u'ValorEnergiaActiva_P2 (kWh)': round(periodes_energia['p2'], 2),
                    u'ValorEnergiaActiva_P3 (kWh)': round(periodes_energia['p3'], 2),
                    u'ValorEnergiaActiva_P4 (kWh)': round(periodes_energia['p4'], 2),
                    u'ValorEnergiaActiva_P5 (kWh)': round(periodes_energia['p5'], 2),
                    u'ValorEnergiaActiva_P6 (kWh)': round(periodes_energia['p6'], 2),
                    u'SuplementoEnergia_P1 (€/kWh)': round(suplements_energia[0], 9),
                    u'SuplementoEnergia_P2 (€/kWh)': round(suplements_energia[1], 9),
                    u'SuplementoEnergia_P3 (€/kWh)': round(suplements_energia[2], 9),
                    u'SuplementoEnergia_P4 (€/kWh)': round(suplements_energia[3], 9),
                    u'SuplementoEnergia_P5 (€/kWh)': round(suplements_energia[4], 9),
                    u'SuplementoEnergia_P6 (€/kWh)': round(suplements_energia[5], 9),
                    u'ImporteSuplementoTerminoEnergia_P1 (€)': round(imports_energia[0], 2),
                    u'ImporteSuplementoTerminoEnergia_P2 (€)': round(imports_energia[1], 2),
                    u'ImporteSuplementoTerminoEnergia_P3 (€)': round(imports_energia[2], 2),
                    u'ImporteSuplementoTerminoEnergia_P4 (€)': round(imports_energia[3], 2),
                    u'ImporteSuplementoTerminoEnergia_P5 (€)': round(imports_energia[4], 2),
                    u'ImporteSuplementoTerminoEnergia_P6 (€)': round(imports_energia[5], 2),
                    u'ImporteTotalSuplementoTerminoEnergia (€)': round(total_energia, 2),
                    u'ImporteTotalARegularizar (€)': round((total_potencia + total_energia), 2),
                }

                df_comer_line = pd.DataFrame(
                    data=[line_dict], columns=self.export_valid_header
                )

                df_comer = df_comer.append(df_comer_line)
                df_consums_complet = df_consums_complet.append(df_comer_line)

        df_consums_complet.fillna(value="", inplace=True)
        df_consums_complet[u'TarifaATRFact'] = df_consums_complet.TarifaATRFact.astype(str)
        df_consums_complet = df_consums_complet.sort_values(by=['CUPS', 'FechaDesde'],
                                        ascending=True)

        return df_consums_complet

    def export_to_df(self):
        """

        :return: binary file with ...
        """
        cursor = self.cursor
        uid = self.uid
        context = self.context

        cups_obj = self.pool.get('giscedata.cups.ps')

        cups_comunidades_afectadas_ids = self.get_affected_cups_baixa_and_canvi_titular()
        n_afected_cups = len(cups_comunidades_afectadas_ids)
        consum_total = []

        for cups in tqdm(cups_obj.read(cursor, uid, cups_comunidades_afectadas_ids, ['name'], context=context), desc="Procesant cups"):
            cups_name = cups['name']
            cups_id = cups['id']
            for periode in self.periodes_eval:
                consums = self.get_consum_cups(cups_id, periode)
                consum_total.append({
                    'cups': cups_name,
                    'periode': periode,
                    'consums': consums,
                    'suplements': self.get_suplements_consum(
                        cups_id, consums, periode
                    ),
                })

        # Create dict of dicts from consums list
        consum_total = self.sort_by_comer(consum_total)
        exported_df = self.export_consums_to_file(consum_total)

        return exported_df


########################################### IMPORT

    def group_consums_cups_tarifa(self, df_consum):
        df_consum = df_consum.groupby([u'CUPS', u'TarifaATRFact'])[
            [
                u'FechaDesde', u'FechaHasta',
                u'ImporteTotalSuplementoTerminoPotencia(€)',
                u'ImporteTotalSuplementoTerminoEnergia(€)',
                u'ImporteTotalARegularizar(€)',
                u'ValorEnergiaActiva_P1(kWh)',
                u'ValorEnergiaActiva_P2(kWh)',
                u'ValorEnergiaActiva_P3(kWh)',
                u'ValorEnergiaActiva_P4(kWh)',
                u'ValorEnergiaActiva_P5(kWh)',
                u'ValorEnergiaActiva_P6(kWh)'
            ]
        ].agg(
            {
                u'ImporteTotalSuplementoTerminoPotencia(€)': np.sum,
                u'ImporteTotalSuplementoTerminoEnergia(€)': np.sum,
                u'ImporteTotalARegularizar(€)': np.sum,
                u'FechaDesde': np.min,
                u'FechaHasta': np.max,
                u'ValorEnergiaActiva_P1(kWh)': np.sum,
                u'ValorEnergiaActiva_P2(kWh)': np.sum,
                u'ValorEnergiaActiva_P3(kWh)': np.sum,
                u'ValorEnergiaActiva_P4(kWh)': np.sum,
                u'ValorEnergiaActiva_P5(kWh)': np.sum,
                u'ValorEnergiaActiva_P6(kWh)': np.sum

            }
        )

        return df_consum

    def import_procesed_consums(self, df_consum):
        cursor = self.cursor
        uid = self.uid
        context = self.context
        cups_obj = self.pool.get('giscedata.cups.ps')

        for i, new_df in tqdm(df_consum.groupby(level=0), desc='Creando lineas extra y liqudaciones por cups'):
            cups_df = new_df.reset_index()
            grouped_info_for_extra_line = []

            ctx = deepcopy(context)
            ctx.update({'active_test': False})

            cups_id = cups_obj.search(
                cursor, uid,
                [('name', 'ilike', '{}%%'.format(i[:20]))],
                context=ctx
            )

            cups_df.apply(
                lambda x: self.create_liquidacions(
                    x, grouped_info_for_extra_line, cups_id
                ), axis=1
            )

    def create_liquidacions(self, row_group_by_tarifa, grouped_extra, cups_id):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        liquidacio_obj = self.pool.get(
            'giscedata.liquidacio.suplement.territorial.tec271.data'
        )
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        # TODO revisar fecha
        data_cobrament = liquidacio_obj.get_data_cobrament_linies(
            cursor, uid, context=context
        )

        TABLA_107_dict = dict(TABLA_107)
        comunitat_id = False
        polissa_id = False
        cups_name = ''
        tipus = False

        if cups_id:
            cups_id = cups_id[0]
            tipus = self.mapping_fucking_liquidacions[cups_id]

            comunitat_id = cups_obj.browse(
                cursor, uid, cups_id, context=context
            ).id_municipi.state.comunitat_autonoma.id

            cups_reads = cups_obj.read(
                cursor, uid, cups_id, ['polissa_polissa', 'name'],
                context=context
            )

            cups_name = cups_reads['name']

            polissa_id = cups_reads['polissa_polissa']

            if polissa_id:
                polissa_id = polissa_id[0]

        tarifa_code = str(row_group_by_tarifa[u'TarifaATRFact']).zfill(3)

        tarifa_name = TABLA_107_dict[tarifa_code].replace(
            '.A', 'A').replace('.B', 'B').replace('.D', 'D').replace(' ', '')

        tarifa_id = tarifa_obj.search(
            cursor, uid,
            [
                ('name', '=', tarifa_name),
                ('codi_ocsum', '=', tarifa_code)
            ], context=context
        )

        if tarifa_id:
            tarifa_id = tarifa_id[0]
            total_energia = sum(
                [row_group_by_tarifa[u'ValorEnergiaActiva_P{}(kWh)'.format(xx)] or 0.0
                 for xx in range(1, 7)]
            )
            vals = {
                'cups_id': cups_id,
                'polissa_id': polissa_id,
                'tarifa_id': tarifa_id,
                'import_total': float(row_group_by_tarifa[u'ImporteTotalARegularizar(€)']),
                'tipus': tipus,
                'ccaa_id': comunitat_id,
                'energia_total': total_energia,
                'cups': cups_name

            }
            exist_liq = liquidacio_obj.search(cursor, uid, [('cups_id', '=', cups_id)])
            if not exist_liq:
                liquidacio_obj.create(cursor, uid, vals, context=context)
        else:
            print 'ERROR: no tarifa %s' % row_group_by_tarifa[u'CUPS']
        # TODO aviam que fem aqui
        grouped_extra.append(row_group_by_tarifa.to_dict())

    def import_from_excel(self, consums_df):

        # Lambda expression to remove \n and ' ' from column names
        clean_header_of_unworthy_chars = (
            lambda s: s.replace('\n', '').replace(' ', '').replace("\"", "")
        )
        consums_df = consums_df.rename(
            columns=lambda x: clean_header_of_unworthy_chars(x)
        )
        consums_df[u'FechaDesde'] = pd.to_datetime(consums_df[u'FechaDesde'])
        consums_df[u'FechaHasta'] = pd.to_datetime(consums_df[u'FechaHasta'])

        # Remove fake lines prom dataframe (last line with totals)
        consums_df = consums_df[consums_df.CUPS.notnull()]

        consums_df = self.group_consums_cups_tarifa(consums_df)

        self.import_procesed_consums(consums_df)

        return True