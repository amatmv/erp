# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataFacturacioFacturador(osv.osv):
    """Implementació d'alguns aspectes específics de distribució.
    """
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def linia_aplicable_iese(self, cursor, uid, pagador_sel, linia):
        imd_obj = self.pool.get('ir.model.data')

        res = super(
            GiscedataFacturacioFacturador, self
        ).linia_aplicable_iese(cursor, uid, pagador_sel, linia)

        if not res:
            product_tec_271_id = imd_obj.get_object_reference(
                cursor, uid,
                'giscedata_contractacio_distri',
                'energia_ordre_tec_271_2019'
            )[1]

            if linia.product_id and linia.product_id.id == product_tec_271_id:
                res = True

        return res


GiscedataFacturacioFacturador()