# -*- coding: utf-8 -*-

from osv import osv, fields
from base64 import b64decode
import pandas as pd
from tools.translate import _
from tqdm import tqdm
from copy import deepcopy
from gestionatr.defs import TABLA_107
from giscedata_sup_territorials_2013_tec271_distri.\
    cargar_liquidaciones_bajas import \
    GiscedataSuplementsTerritorialsLoadLiquidacions as sup_distri_obj


class WizardCargarLiquidacionesBajaCambio(osv.osv_memory):
    """ Wizard per importar CSV dels Suplements Territorials"""
    _name = 'wizard.cargar.liquidaciones.baja.cambio'

    def fix_external_data_from_dataframe_aux(self, cursor, uid, wiz_ids, consums_df, context=None):
        if context is None:
            context = {}

        fix_periods = self.read(
            cursor, uid, wiz_ids, [u'fix_periods'], context=context
        )[0][u'fix_periods']

        partner_obj = self.pool.get('res.partner')
        cups_obj = self.pool.get('giscedata.cups.ps')

        # Remove lines cups nan
        consums_df = consums_df[consums_df.CUPS.notnull()]

        # Dataframe with two columns unique CUPS | list of NIF non repeated
        nifs_cups_df = consums_df[
            [u'CUPS', u'NIF']
        ].groupby(u'CUPS')[u'NIF'].apply(lambda x: list(set(x))).reset_index()

        # TODO not todo Remove nan nif values from lists
        nifs_cups_df[u'NIF'] = nifs_cups_df[u'NIF'].apply(
            lambda v: pd.Series(v).dropna().values
        )

        # var: cups_to_remove - Tendra los cups que tengan actualmente un
        # titular distinto al que tenia en 2013
        cups_to_remove = []
        mapping_liquidacions = {}
        mapping_comunitats = {}

        for index, cup_nifs in nifs_cups_df.iterrows():
            # cup_nifs -> CUPS (str) | NIF (list)
            current_cups_proceced = cup_nifs[u'CUPS']
            nifs_of_current_cups = cup_nifs[u'NIF']

            ori_line_ccaa_code = consums_df[
                (consums_df[u'CUPS'] == current_cups_proceced)
            ].head(1)[u'Comunidad_Autonoma'].item()

            ori_line_ccaa_id = self.get_comunidad_from_code(
                cursor, uid, [], ori_line_ccaa_code, context=consums_df
            )

            mapping_comunitats.update({current_cups_proceced: ori_line_ccaa_id})

            search_cups_info = u'''
            SELECT 
                cups.id AS cups_id, 
                pol.id AS pol_id,
                pol.state AS pol_state, 
                part.vat AS part_vat 
            FROM giscedata_cups_ps AS cups
                LEFT JOIN giscedata_polissa AS pol ON (pol.id = cups.polissa_polissa)
                LEFT JOIN res_partner AS part ON (part.id = pol.titular)
            WHERE cups.name ilike %s
            '''

            cursor.execute(
                search_cups_info,
                (current_cups_proceced, )
            )

            cups_info = cursor.dictfetchall()
            cups_info = cups_info[0] if cups_info else cups_info

            if cups_info and cups_info[u'part_vat']:
                cups_info[u'part_vat'] = cups_info[u'part_vat'].replace(
                    u'ES', u''
                )

            if nifs_of_current_cups.size > 0:

                if not cups_info:
                    mapping_liquidacions.update({current_cups_proceced: '1'})
                elif not cups_info[u'pol_id']:
                    mapping_liquidacions.update({current_cups_proceced: '1'})
                elif cups_info[u'pol_id'] and cups_info[u'pol_state'] == u'baixa':
                    mapping_liquidacions.update({current_cups_proceced: '1'})
                else:
                    if cups_info[u'part_vat'] not in tuple(nifs_of_current_cups):
                        mapping_liquidacions.update(
                            {current_cups_proceced: '2'}
                        )
                    else:
                        cups_to_remove.append(current_cups_proceced)
            else:
                if not cups_info:
                    mapping_liquidacions.update({current_cups_proceced: '1'})
                elif not cups_info[u'pol_id']:
                    mapping_liquidacions.update({current_cups_proceced: '1'})
                elif cups_info[u'pol_id'] and cups_info[u'pol_state'] == u'baixa':
                    mapping_liquidacions.update({current_cups_proceced: '1'})
                else:
                    mapping_liquidacions.update({current_cups_proceced: '2'})

        # Remove CUPS without nif and without same titular of 2013 if case
        if cups_to_remove:
            consums_df = consums_df[~consums_df[u'CUPS'].isin(cups_to_remove)]

        # Now we have var:"consums_df" only with cups to be regularized

        if consums_df.empty:
            raise osv.except_osv(
                _(u'ERROR'),
                _(u'No se han encontrado cups a regularizar')
            )

        # Potencia en W no en kW
        for period in range(1, 7):
            key_p = u'PotenciaFacturadaP{}'.format(period)
            key_p_c = u'PotenciaContratadaP{}'.format(period)

            consums_df[key_p] = consums_df[key_p].apply(lambda v: v * 1000)
            consums_df[key_p_c] = consums_df[key_p_c].apply(lambda v: v * 1000)

        # Som db data do rare things with periods, we fix it
        if fix_periods:
            external_sups_wiz_obj = self.pool.get(
                u'wizard.export.sup.territorials.tec271.external.files'
            )
            consums_df = external_sups_wiz_obj.fix_external_periods(consums_df)

        return consums_df, mapping_liquidacions, mapping_comunitats

    def create_liquidacions(self, cursor, uid, ids, row_group_by_tarifa, grouped_extra, cups_id, cups_name, mapping_liquidacions, mapping_comunitats, context=None):

        liquidacio_obj = self.pool.get(
            'giscedata.liquidacio.suplement.territorial.tec271.data'
        )
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        # TODO revisar fecha
        data_cobrament = liquidacio_obj.get_data_cobrament_linies(
            cursor, uid, context=context
        )

        TABLA_107_dict = dict(TABLA_107)
        comunitat_id = False
        polissa_id = False
        # cups_name = ''
        tipus = mapping_liquidacions[cups_name]
        comunitat_id = False
        if cups_id:
            cups_id = cups_id[0]
            if not cups_name:
                pass

            comunitat_id = cups_obj.browse(
                cursor, uid, cups_id, context=context
            ).id_municipi.state.comunitat_autonoma.id

            cups_reads = cups_obj.read(
                cursor, uid, cups_id, ['polissa_polissa', 'name'],
                context=context
            )

            # cups_name = cups_reads['name']

            polissa_id = cups_reads['polissa_polissa']

            if polissa_id:
                polissa_id = polissa_id[0]
        else:
            cups_id = False
            comunitat_id = mapping_comunitats[cups_name]

        tarifa_code = str(row_group_by_tarifa[u'TarifaATRFact']).zfill(3)

        tarifa_name = TABLA_107_dict[tarifa_code].replace(
            '.A', 'A').replace('.B', 'B').replace('.D', 'D').replace(' ', '')

        tarifa_id = tarifa_obj.search(
            cursor, uid,
            [
                ('name', '=', tarifa_name),
                ('codi_ocsum', '=', tarifa_code)
            ], context=context
        )

        if tarifa_id:
            tarifa_id = tarifa_id[0]
            total_energia = sum(
                [row_group_by_tarifa[u'ValorEnergiaActiva_P{}(kWh)'.format(xx)] or 0.0
                 for xx in range(1, 7)]
            )
            vals = {
                'cups_id': cups_id,
                'polissa_id': polissa_id,
                'tarifa_id': tarifa_id,
                'import_total': float(row_group_by_tarifa[u'ImporteTotalARegularizar(€)']),
                'tipus': tipus,
                'ccaa_id': comunitat_id,
                'energia_total': total_energia,
                'cups': cups_name

            }
            exist_liq = liquidacio_obj.search(
                cursor, uid, [('cups', 'ilike', '{}%%'.format(cups_name[:20]))]
            )
            if not exist_liq:
                liquidacio_obj.create(cursor, uid, vals, context=context)
        else:
            print 'ERROR: no tarifa %s' % row_group_by_tarifa[u'CUPS']
        # TODO aviam que fem aqui
        grouped_extra.append(row_group_by_tarifa.to_dict())

    def import_procesed_consums(self, cursor, uid, ids, df_consum, mapping_liquidacions, mapping_comunitats, context=None):
        cups_obj = self.pool.get('giscedata.cups.ps')

        for i, new_df in tqdm(df_consum.groupby(level=0), desc='Creando lineas extra y liqudaciones por cups'):
            cups_df = new_df.reset_index()
            grouped_info_for_extra_line = []

            ctx = deepcopy(context)
            ctx.update({'active_test': False})

            cups_id = cups_obj.search(
                cursor, uid,
                [('name', 'ilike', '{}%%'.format(i[:20]))],
                context=ctx
            )

            cups_df.apply(
                lambda x: self.create_liquidacions(
                    cursor, uid, ids, x, grouped_info_for_extra_line, cups_id, i,
                    mapping_liquidacions, mapping_comunitats, context=context
                ), axis=1
            )

    def _cargar_liquidaciones_desde_fichero(self, cursor, uid, ids, context=None):
        self_reads = self.read(
            cursor, uid, ids,
            [u'file', u'file_name', u'fix_periods'],
            context=context
        )[0]

        suplemento = sup_distri_obj(
            self.pool, cursor, uid, context=context
        )
        external_sups_wiz_obj = self.pool.get(
            u'wizard.export.sup.territorials.tec271.external.files'
        )

        wiz_create_vals = {
            u'imported_file': self_reads[u'file'],
            u'imported_file_name': self_reads[u'file_name'],
            u'fix_periods': self_reads[u'fix_periods']

        }

        ext_wiz_id = external_sups_wiz_obj.create(
            cursor, uid, wiz_create_vals, context=context
        )

        non_fixed_consums_df = external_sups_wiz_obj.load_file(
            cursor, uid, [ext_wiz_id], context=context
        )

        fixed_consums_df, mapping_liquidacions, mapping_comunitats = self.fix_external_data_from_dataframe_aux(
            cursor, uid, [ext_wiz_id], non_fixed_consums_df, context=context
        )

        fixed_consums_df = external_sups_wiz_obj.clean_anuladoras_from_dataframe(
            fixed_consums_df
        )

        complete_dataframe = external_sups_wiz_obj.export_files_from_fixed_dataframe(
            fixed_consums_df
        )

        # res = suplemento.import_from_excel(complete_dataframe)
        clean_header_of_unworthy_chars = (
            lambda s: s.replace('\n', '').replace(' ', '').replace("\"", "")
        )
        complete_dataframe = complete_dataframe.rename(
            columns=lambda x: clean_header_of_unworthy_chars(x)
        )

        complete_dataframe = complete_dataframe[complete_dataframe.CUPS.notnull()]

        consums_df = suplemento.group_consums_cups_tarifa(complete_dataframe)

        self.import_procesed_consums(
            cursor, uid, ids, consums_df,
            mapping_liquidacions, mapping_comunitats,
            context=context
        )

        return True

    def _cargar_liquidaciones_desde_erp(self, cursor, uid, ids, context=None):
        suplemento = sup_distri_obj(
            self.pool, cursor, uid, context=context
        )
        consums_df = suplemento.export_to_df()
        res = suplemento.import_from_excel(consums_df)
        return True

    def cargar_liquidaciones_baja_cambio(self, cursor, uid, ids, context=None):
        load_from_file = self.read(
            cursor, uid, ids, ['load_from_file'], context=context
        )[0]['load_from_file']

        if load_from_file:
            res = self._cargar_liquidaciones_desde_fichero(
                cursor, uid, ids, context=context
            )
        else:
            res = self._cargar_liquidaciones_desde_erp(
                cursor, uid, ids, context=context
            )

        self.write(cursor, uid, ids,
                   {
                       'state': 'end',
                       'info': 'No se han encontrado consumos en 2013'
                                if not res else 'Todo correcto'
                   }, context=context)

    def get_comunidad_from_code(self, cursor, uid, ids, codi, context=None):
        res = False
        comunitat_obj = self.pool.get('res.comunitat_autonoma')
        found = comunitat_obj.search(cursor, uid, [('codi', '=', codi)])

        res = found[0] if found else res

        return res

    _columns = {
        'state': fields.selection([('init', 'Inici'), ('end', 'Fi')], 'Estat'),
        'file': fields.binary('File'),
        'file_name': fields.char('Nombre del fichero', size=256),
        'info': fields.text('Información'),
        'load_from_file': fields.boolean('Cargar desde fichero'),
        u'fix_periods': fields.boolean(
            'Fix periodos',
            help=u'Los periodos de las bases de datos de otros clientes '
                 u'estan girados, si el fichero a importar no esta corregido '
                 u'marcar esta opción ej. 2.0 P1<-P2  | 3.0 P2<-P3'
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'load_from_file': lambda *a: False,
        u'fix_periods': lambda *a: True,
    }


WizardCargarLiquidacionesBajaCambio()