# -*- coding: utf-8 -*-

from osv import osv, fields
from base64 import b64decode
import pandas as pd
import datetime
from tools.translate import _
from wizard import except_wizard
from StringIO import StringIO
from giscedata_sup_territorials_2013_tec271_distri.\
    giscedata_sup_territorials_2013_tec271_distri import \
    GiscedataSuplementsTerritorials2013Tec271Distri as sup_distri_obj


class WizardImportSupTerritorialsTec271Distri(osv.osv_memory):
    """ Wizard per importar CSV dels Suplements Territorials"""
    _name = 'wizard.import.sup.territorials.tec271.distri'

    def import_xls(self, cursor, uid, ids, context=None):
        # Suplemento is a python obj
        suplemento = sup_distri_obj(
            self.pool, cursor, uid, context=context
        )

        wiz_reads = self.read(
            cursor, uid, ids, ['file', 'file_name'], context=context
        )[0]

        file_decoded = b64decode(wiz_reads['file'])

        file_name = wiz_reads['file_name']

        res = suplemento.import_from_excel(file_decoded)

        if res:
            self.write(cursor, uid, ids,
                       {
                           'state': 'end',
                           'info': 'Fitxer importat correctament'
                       }, context=context)
        else:
            self.write(cursor, uid, ids,
                       {
                           'state': 'end',
                           'info': 'No s\'ha pogut procesar'
                       }, context=context)

    _columns = {
        'state': fields.selection([('init', 'Inici'), ('end', 'Fi')], 'Estat'),
        'file': fields.binary('File', required=True),
        'file_name': fields.char('Nombre del fichero', size=256),
        'info': fields.text('Informació')
    }

    _defaults = {
        'state': lambda *a: 'init'
    }

WizardImportSupTerritorialsTec271Distri()
