# -*- coding: utf-8 -*-

from osv import osv, fields
from base64 import b64decode, b64encode
import pandas as pd
import numpy as np
import datetime
from tools.translate import _
from wizard import except_wizard
from StringIO import StringIO
from ..defs_suplementos_territoriales_2013_tec_271 import *
from tqdm import tqdm
from giscedata_sup_territorials_2013_tec271_distri.\
    giscedata_sup_territorials_2013_tec271_distri import \
    GiscedataSuplementsTerritorials2013Tec271Distri as sup_distri_obj
from gestionatr.defs import TABLA_107
from zipfile import ZIP_DEFLATED, ZipFile


class WizardExportSuplementosFromExternalFile(osv.osv_memory):

    """
    Wizard para generar ficheros de comercializadoras y
    fichero para la reimportación para las liquidaciones iy lineas extra


    """
    _name = 'wizard.export.sup.territorials.tec271.external.files'

    suplementos = {
        '01': {  # Andalucía
            'comunidad': '01',
            'potencia': [
                potencia_andalucia_period_1, potencia_andalucia_period_2
            ],
            'energia': [energia_andalucia_period_1, energia_andalucia_period_2],
        },
        '02': {  # Aragón
            'comunidad': '02',
            'potencia': [potencia_aragon_period_1, potencia_aragon_period_2],
            'energia': [energia_aragon_period_1, energia_aragon_period_2],
        },
        '03': {  # Asturais
            'comunidad': '03',
            'potencia': [potencia_ast_period_1, potencia_ast_period_2],
            'energia': [energia_ast_period_1, energia_ast_period_2],
        },
        '06': {  # Cantabria
            'comunidad': '06',
            'potencia': [
                potencia_cantabria_period_1, potencia_cantabria_period_2
            ],
            'energia': [energia_cantabria_period_1, energia_cantabria_period_2],
        },
        '07': {  # Castilla y león
            'comunidad': '07',
            'potencia': [potencia_cale_period_1, potencia_cale_period_2],
            'energia': [energia_cale_period_1, energia_cale_period_2],
        },
        '09': {  # Cataluña
            'comunidad': '09',
            'potencia': [potencia_cat_period_1, potencia_cat_1_period_2],
            'energia': [energia_cat_period_1, energia_cat_period_2],
        },
        '11': {  # Extremadura
            'comunidad': '11',
            'potencia': [potencia_ext_period_1, potencia_ext_period_2],
            'energia': [energia_ext_period_1, energia_ext_period_2],
        },
        '12': {  # Galicia
            'comunidad': '12',
            'potencia': [potencia_gal_period_1, potencia_gal_period_2],
            'energia': [energia_gal_period_1, energia_gal_period_2],
        },
        '13': {  # Madrid
            'comunidad': '13',
            'potencia': [potencia_mad_period_1, potencia_mad_period_2],
            'energia': [energia_mad_period_1, energia_mad_period_2],
        },
        '14': {  # Murcia
            'comunidad': '14',
            'potencia': [potencia_mur_period_1, potencia_mur_period_2],
            'energia': [energia_mur_period_1, energia_mur_period_2],
        },
        '15': {  # Navarra
            'comunidad': '15',
            'potencia': [potencia_nav_period_1, potencia_nav_period_2],
            'energia': [energia_nav_period_1, energia_nav_period_2],
        },

    }

    def export_consums_from_external_file(self, cursor, uid, ids, context=None):
        non_fixed_consums_df = self.load_file(cursor, uid, ids, context=context)

        fixed_consums_df = self.fix_external_data_from_dataframe(
            cursor, uid, ids, non_fixed_consums_df, context=context
        )

        fixed_consums_df = self.clean_anuladoras_from_dataframe(
            fixed_consums_df
        )

        complete_dataframe = self.export_files_from_fixed_dataframe(
            fixed_consums_df
        )

        encoded_file_complet = self.dataframe_to_excel(
            complete_dataframe
        )

        encoded_zip = self.export_comer_files_from_complete_dataframe(
            cursor, uid, ids, complete_dataframe, context=context
        )

        self_write_dict = {
            u'comers_zip_file': encoded_zip,
            u'reimportacion_file': encoded_file_complet,
            u'state': u'end'
        }

        self.write(
            cursor, uid, ids,
            self_write_dict,
            context=context
        )

    def load_file(self, cursor, uid, ids, context=None):
        wiz_reads = self.read(
            cursor, uid, ids, [u'imported_file'], context=context
        )[0]

        file_decoded = b64decode(wiz_reads[u'imported_file'])

        imp_file = StringIO(file_decoded)

        consums_2013_df = pd.read_excel(
            imp_file,
            dtype={
                u'Comunidad_Autonoma': str
            }
        )  # dtype={u'FechaDesde': datetime, u'FechaHasta': datetime}

        imp_file.close()

        return consums_2013_df

    @staticmethod
    def dataframe_to_excel(df, encode=True, sort_columns=None):
        file_xls = StringIO()
        writer_complet = pd.ExcelWriter(file_xls)

        df.to_excel(
            writer_complet,
            columns=sort_columns,
            index=None, header=True, encoding='utf-8-sig'
        )

        writer_complet.save()

        if encode:
            xls_str_file = b64encode(file_xls.getvalue())
        else:
            xls_str_file = file_xls.getvalue()

        file_xls.close()

        return xls_str_file

    def fix_external_data_from_dataframe(self, cursor, uid, ids, consums_df, context=None):
        if context is None:
            context = {}

        fix_periods = self.read(
            cursor, uid, ids, [u'fix_periods'], context=context
        )[0][u'fix_periods']

        partner_obj = self.pool.get('res.partner')
        cups_obj = self.pool.get('giscedata.cups.ps')

        # Remove lines cups nan
        consums_df = consums_df[consums_df.CUPS.notnull()]

        # Dataframe with two columns unique CUPS | list of NIF non repeated
        nifs_cups_df = consums_df[
            [u'CUPS', u'NIF']
        ].groupby(u'CUPS')[u'NIF'].apply(lambda x: list(set(x))).reset_index()

        # Remove nan nif values from lists
        nifs_cups_df[u'NIF'] = nifs_cups_df[u'NIF'].apply(
            lambda v: pd.Series(v).dropna().values
        )
        # nifs_cups_df[np.array(list(map(len, nifs_cups_df.NIF.values)))>1]  -- get cups with canvis de titular

        # var: cups_to_remove - Tendra los cups que tengan actualmente un
        # titular distinto al que tenia en 2013
        cups_to_remove = []

        for index, cup_nifs in nifs_cups_df.iterrows():
            # cup_nifs -> CUPS (str) | NIF (list)
            current_cups_proceced = cup_nifs[u'CUPS']
            nifs_of_current_cups = cup_nifs[u'NIF']

            if nifs_of_current_cups.size > 0:

                search_if_titular_of_cups_is_same = '''
                SELECT cups.id FROM giscedata_cups_ps AS cups
                INNER JOIN giscedata_polissa AS pol ON (pol.id = cups.polissa_polissa)
                INNER JOIN res_partner AS part ON (part.id = pol.titular)
                WHERE cups.polissa_polissa IS NOT NULL
                      AND cups.name ilike %s
                      AND REPLACE(part.vat, 'ES', '') in %s
                '''

                cursor.execute(
                    search_if_titular_of_cups_is_same,
                    (current_cups_proceced, tuple(nifs_of_current_cups))
                )

                same_titular_of_2013 = cursor.dictfetchall()

                if not same_titular_of_2013:
                    cups_to_remove.append(current_cups_proceced)

            else:
                cups_to_remove.append(current_cups_proceced)

        # Remove CUPS without nif and without same titular of 2013 if case
        if cups_to_remove:
            consums_df = consums_df[~consums_df[u'CUPS'].isin(cups_to_remove)]

        # Now we have var:"consums_df" only with cups to be regularized

        if consums_df.empty:
            raise osv.except_osv(
                _(u'ERROR'),
                _(u'No se han encontrado cups a regularizar')
            )

        # Potencia en W no en kW
        for period in range(1, 7):
            key_p = u'PotenciaFacturadaP{}'.format(period)
            key_p_c = u'PotenciaContratadaP{}'.format(period)

            consums_df[key_p] = consums_df[key_p].apply(lambda v: v * 1000)
            consums_df[key_p_c] = consums_df[key_p_c].apply(lambda v: v * 1000)

        # Som db data do rare things with periods, we fix it
        if fix_periods:
            consums_df = self.fix_external_periods(consums_df)

        return consums_df

    @staticmethod
    def fix_external_periods(consums_df_to_fix_periods):
        for index, row in consums_df_to_fix_periods.iterrows():

            if row[u'Tarifa'] in (u'2.0A', u'2.1A'):
                # P1 <- P2 & P2 = 0
                consums_df_to_fix_periods.loc[index, u'PotenciaContratadaP1'] = (
                    row[u'PotenciaContratadaP2']
                )

                consums_df_to_fix_periods.loc[index, u'PotenciaFacturadaP1'] = (
                    row[u'PotenciaFacturadaP2']
                )

                consums_df_to_fix_periods.loc[index, u'ActivaP1'] = (
                    row[u'ActivaP2']
                )

                consums_df_to_fix_periods.loc[index, u'PotenciaContratadaP2'] = 0
                consums_df_to_fix_periods.loc[index, u'PotenciaFacturadaP2'] = 0
                consums_df_to_fix_periods.loc[index, u'ActivaP2'] = 0

            elif row[u'Tarifa'] in (u'2.0DHA', u'2.1DHA'):
                # P2 <- P3 & P3 = 0
                consums_df_to_fix_periods.loc[index, u'PotenciaContratadaP2'] = (
                    row[u'PotenciaContratadaP3']
                )

                consums_df_to_fix_periods.loc[index, u'PotenciaFacturadaP2'] = (
                    row[u'PotenciaFacturadaP3']
                )

                consums_df_to_fix_periods.loc[index, u'ActivaP2'] = (
                    row[u'ActivaP3']
                )

                consums_df_to_fix_periods.loc[index, u'PotenciaContratadaP3'] = 0
                consums_df_to_fix_periods.loc[index, u'PotenciaFacturadaP3'] = 0
                consums_df_to_fix_periods.loc[index, u'ActivaP3'] = 0
            elif row[u'Tarifa'] in (u'3.0A', u'3.1'):  # TODO 3.1 too? no examples
                # Las 3.0 tienen 6 periodos de energia
                # sumamos las energias entre P1+P4 P2+P5 i P3+P6
                consums_df_to_fix_periods.loc[index, u'ActivaP1'] = (
                    row[u'ActivaP1'] + row[u'ActivaP4']
                )
                consums_df_to_fix_periods.loc[index, u'ActivaP2'] = (
                    row[u'ActivaP2'] + row[u'ActivaP5']
                )
                consums_df_to_fix_periods.loc[index, u'ActivaP3'] = (
                    row[u'ActivaP3'] + row[u'ActivaP6']
                )
                consums_df_to_fix_periods.loc[index, u'ActivaP4'] = 0
                consums_df_to_fix_periods.loc[index, u'ActivaP5'] = 0
                consums_df_to_fix_periods.loc[index, u'ActivaP6'] = 0

        return consums_df_to_fix_periods

    @staticmethod
    def clean_anuladoras_from_dataframe(complet_df):
        cups_in_dataframe = complet_df[u'CUPS'].unique().tolist()

        # Cuando hay anuladoras, cumple que hay 3 lineas con la misma
        # Fecha_Lectura_Actual 1 factura mal hecha, 1 factura anuladora y
        # la rectificadora, solamente nos interesa quedarnos con la
        # rectificadora

        # La linea buena entre comillas sera la que tiene la misma fecha en
        # Fecha_Lectura_Actual y Fecha_Lectura_Anterior, pero hara falta
        # rectificar las fechas con las correctas

        index_list_to_remove = []

        for cups in cups_in_dataframe:
            facts_cups = complet_df.loc[
                (complet_df[u'CUPS'] == cups)
            ]
            rectificaciones = facts_cups[
                facts_cups.duplicated([u'Fecha_Lectura_Actual'])
            ]
            if not rectificaciones.empty:
                # La funcion duplicated() solamente devuelve 2 lineas aun que
                # haya mas
                lista_fechas = rectificaciones[
                    u'Fecha_Lectura_Actual'
                ].unique().tolist()

                for fecha_lectura_actual in lista_fechas:
                    fecha_lectura_actual = pd.to_datetime(fecha_lectura_actual)

                    # Find facturas con la misma fecha
                    anul_and_rects = facts_cups.loc[
                        (
                            facts_cups[u'Fecha_Lectura_Actual']
                            ==
                            fecha_lectura_actual
                        )
                    ]

                    # Cojemos la linea negativa como referencia
                    anuladora = anul_and_rects.loc[
                        (anul_and_rects[u'Total_Activa'] < 0)
                        |
                        (anul_and_rects[u'PotenciaContratadaP1'] < 0)
                    ].iloc[0]

                    bad_fact = anul_and_rects.loc[
                        (
                            anul_and_rects[u'Total_Activa']
                            ==
                            abs(anuladora[u'Total_Activa'])
                        )
                        &
                        (
                            anul_and_rects[u'PotenciaContratadaP1']
                            ==
                            abs(anuladora[u'PotenciaContratadaP1'])
                        )
                        &
                        (
                            anul_and_rects[u'Tarifa']
                            ==
                            anuladora[u'Tarifa']
                        )
                        &
                        (
                            anul_and_rects[u'PotenciaFacturadaP1']
                            ==
                            abs(anuladora[u'PotenciaFacturadaP1'])
                        )
                    ].iloc[0]

                    index_list_to_remove.append(anuladora.head().name)
                    index_list_to_remove.append(bad_fact.head().name)

                    # TODO this if check case with rare things use or not?
                    # if anul_and_rects.shape[0] > 3 or anul_and_rects.shape[0] < 2:
                    #     print anul_and_rects
                    #

        if index_list_to_remove:
            complet_df = complet_df.drop(index_list_to_remove)

        return complet_df

    @staticmethod
    def grouped_row_to_formated_dict(row, mapping_107, first_period, code_comun, cups):
        # "period" will be used to find price in table
        period = 1 if not first_period else 0
        preus_energia = WizardExportSuplementosFromExternalFile.suplementos[
            code_comun
        ][u'energia'][period][row[u'Tarifa']]

        periodos_tarifa = len(preus_energia)

        preus_potencia = WizardExportSuplementosFromExternalFile.suplementos[
            code_comun
        ][u'potencia'][period][row[u'Tarifa']]

        periodos_tarifa_potencia = len(preus_potencia)

        # Con las agrupaciones, es posible que haya agujeros en la facturación
        # El numero de dias de la ROW, representa el numero de dias de consumo
        # real pero si se da el caso en que haya agueros en la facturacion,
        # estos no coincidiran con el periodo de las fechas, hara falta realizar
        # una regla de 3 inversa para conseguir el consumo dee potencia
        # repartida entre el periodo completo
        # Ej. Es lo mismo facturar 4000 W en 30 dias que 2000 en 60

        period_days_not_real_days = (
            row[u'Fecha_Lectura_Actual'] - row[u'Fecha_Lectura_Anterior']
        ).days

        dias_cosnumo_real = row[u'NumeroDias']

        potencias_ponderadas = [
            row[u'MulPotP{}Days'.format(p)] / row[u'NumeroDias']
            for p in range(1, 7)
        ]

        if dias_cosnumo_real != period_days_not_real_days:
            potencias_ponderadas = [
                np.average(
                    [pot, 0],
                    weights=[
                        dias_cosnumo_real,
                        (period_days_not_real_days - dias_cosnumo_real)
                    ]
                )
                for pot in potencias_ponderadas
            ]

            print 'AGUJERO', cups, 'Real: {}'.format(dias_cosnumo_real), 'Comp: {}'.format(period_days_not_real_days)

        res = {
            u'CUPS': cups,
            u'TarifaATRFact': mapping_107[row[u'Tarifa']],
            u'FechaDesde': row[u'Fecha_Lectura_Anterior'],
            u'FechaHasta': row[u'Fecha_Lectura_Actual'],
            u'NumeroDias': period_days_not_real_days,
            u'MediaPondPotenciaAFacturar_P1 (W)': long(
                round(potencias_ponderadas[0], 0)
            ),
            u'MediaPondPotenciaAFacturar_P2 (W)': long(
                round(potencias_ponderadas[1], 0)
            ),
            u'MediaPondPotenciaAFacturar_P3 (W)': long(
                round(potencias_ponderadas[2], 0)
            ),
            u'MediaPondPotenciaAFacturar_P4 (W)': long(
                round(potencias_ponderadas[3], 0)
            ),
            u'MediaPondPotenciaAFacturar_P5 (W)': long(
                round(potencias_ponderadas[4], 0)
            ),
            u'MediaPondPotenciaAFacturar_P6 (W)': long(
                round(potencias_ponderadas[5], 0)
            ),
            u'SuplementoPotencia_P1 (€/W)': round(
                (
                    (preus_potencia[0] / 365 / 1000 * row[u'NumeroDias'])
                    if periodos_tarifa_potencia else 0.0
                ), 9
            ),
            u'SuplementoPotencia_P2 (€/W)': round(
                (
                    (preus_potencia[1] / 365 / 1000 * row[u'NumeroDias'])
                    if not periodos_tarifa_potencia < 2 else 0.0
                ), 9
            ),
            u'SuplementoPotencia_P3 (€/W)': round(
                (
                    (preus_potencia[2] / 365 / 1000 * row[u'NumeroDias'])
                    if not periodos_tarifa_potencia < 3 else 0.0
                ), 9
            ),
            u'SuplementoPotencia_P4 (€/W)': round(
                (
                    (preus_potencia[3] / 365 / 1000 * row[u'NumeroDias'])
                    if not periodos_tarifa_potencia < 4 else 0.0
                ), 9
            ),
            u'SuplementoPotencia_P5 (€/W)': round(
                (
                    (preus_potencia[4] / 365 / 1000 * row[u'NumeroDias'])
                    if not periodos_tarifa_potencia < 5 else 0.0
                ), 9
            ),
            u'SuplementoPotencia_P6 (€/W)': round(
                (
                    (preus_potencia[5] / 365 / 1000 * row[u'NumeroDias'])
                    if not periodos_tarifa_potencia < 6 else 0.0
                ), 9
            ),
            u'ValorEnergiaActiva_P1 (kWh)': round(row[u'ActivaP1'], 2),
            u'ValorEnergiaActiva_P2 (kWh)': round(row[u'ActivaP2'], 2),
            u'ValorEnergiaActiva_P3 (kWh)': round(row[u'ActivaP3'], 2),
            u'ValorEnergiaActiva_P4 (kWh)': round(row[u'ActivaP4'], 2),
            u'ValorEnergiaActiva_P5 (kWh)': round(row[u'ActivaP5'], 2),
            u'ValorEnergiaActiva_P6 (kWh)': round(row[u'ActivaP6'], 2),
            u'SuplementoEnergia_P1 (€/kWh)': round(
                (preus_energia[0] if periodos_tarifa else 0.0), 9
            ),
            u'SuplementoEnergia_P2 (€/kWh)': round(
                (preus_energia[1] if not periodos_tarifa < 2 else 0.0), 9
            ),
            u'SuplementoEnergia_P3 (€/kWh)': round(
                (preus_energia[2] if not periodos_tarifa < 3 else 0.0), 9
            ),
            u'SuplementoEnergia_P4 (€/kWh)': round(
                (preus_energia[3] if not periodos_tarifa < 4 else 0.0), 9
            ),
            u'SuplementoEnergia_P5 (€/kWh)': round(
                (preus_energia[4] if not periodos_tarifa < 5 else 0.0), 9
            ),
            u'SuplementoEnergia_P6 (€/kWh)': round(
                (preus_energia[5] if not periodos_tarifa < 6 else 0.0), 9
            )
        }
        # PARTIAL IMPORTS
        res.update(
            {
                u'ImporteSuplementoTerminoPotencia_P1 (€)': round(
                    (
                        res[u'SuplementoPotencia_P1 (€/W)']
                        *
                        res[u'MediaPondPotenciaAFacturar_P1 (W)']
                    ), 2
                ),
                u'ImporteSuplementoTerminoPotencia_P2 (€)': round(
                    (
                        res[u'SuplementoPotencia_P2 (€/W)']
                        *
                        res[u'MediaPondPotenciaAFacturar_P2 (W)']
                     ), 2
                ),
                u'ImporteSuplementoTerminoPotencia_P3 (€)': round(
                    (
                        res[u'SuplementoPotencia_P3 (€/W)']
                        *
                        res[u'MediaPondPotenciaAFacturar_P3 (W)']
                     ), 2
                ),
                u'ImporteSuplementoTerminoPotencia_P4 (€)': round(
                    (
                        res[u'SuplementoPotencia_P4 (€/W)']
                        *
                        res[u'MediaPondPotenciaAFacturar_P4 (W)']
                     ), 2
                ),
                u'ImporteSuplementoTerminoPotencia_P5 (€)': round(
                    (
                        res[u'SuplementoPotencia_P5 (€/W)']
                        *
                        res[u'MediaPondPotenciaAFacturar_P5 (W)']
                     ), 2
                ),
                u'ImporteSuplementoTerminoPotencia_P6 (€)': round(
                    (
                        res[u'SuplementoPotencia_P6 (€/W)']
                        *
                        res[u'MediaPondPotenciaAFacturar_P6 (W)']
                     ), 2
                ),
                u'ImporteSuplementoTerminoEnergia_P1 (€)': round(
                    (
                        res[u'ValorEnergiaActiva_P1 (kWh)']
                        *
                        res[u'SuplementoEnergia_P1 (€/kWh)']
                    ), 2
                ),
                u'ImporteSuplementoTerminoEnergia_P2 (€)': round(
                    (
                        res[u'ValorEnergiaActiva_P2 (kWh)']
                        *
                        res[u'SuplementoEnergia_P2 (€/kWh)']
                     ), 2
                ),
                u'ImporteSuplementoTerminoEnergia_P3 (€)': round(
                    (
                        res[u'ValorEnergiaActiva_P3 (kWh)']
                        *
                        res[u'SuplementoEnergia_P3 (€/kWh)']
                    ), 2
                ),
                u'ImporteSuplementoTerminoEnergia_P4 (€)': round(
                    (
                        res[u'ValorEnergiaActiva_P4 (kWh)']
                        *
                        res[u'SuplementoEnergia_P4 (€/kWh)']
                    ), 2
                ),
                u'ImporteSuplementoTerminoEnergia_P5 (€)': round(
                    (
                        res[u'ValorEnergiaActiva_P5 (kWh)']
                        *
                        res[u'SuplementoEnergia_P5 (€/kWh)']
                    ), 2
                ),
                u'ImporteSuplementoTerminoEnergia_P6 (€)': round(
                    (
                        res[u'ValorEnergiaActiva_P6 (kWh)']
                        *
                        res[u'SuplementoEnergia_P6 (€/kWh)']
                    ), 2
                )
            }
        )

        # TOTAL IMPORTS

        pot_consums = [
            res[u'ImporteSuplementoTerminoPotencia_P{} (€)'.format(v)]
            for v in range(1, 7)
        ]

        energia_consums = [
            res[u'ImporteSuplementoTerminoEnergia_P{} (€)'.format(v)]
            for v in range(1, 7)
        ]
        res.update(
            {
                u'ImporteTotalSuplementoTerminoPotencia (€)': round(
                    np.sum(pot_consums), 2
                ),
                u'ImporteTotalSuplementoTerminoEnergia (€)': round(
                    np.sum(energia_consums), 2
                )
            }
        )

        res.update(
            {
                u'ImporteTotalARegularizar (€)': round(
                    np.sum(
                        [
                            res[u'ImporteTotalSuplementoTerminoPotencia (€)']
                            ,
                            res[u'ImporteTotalSuplementoTerminoEnergia (€)']
                        ]
                    ), 2
                )

            }
        )

        return res

    @staticmethod
    def generate_notas(grouped_cambios_pot):

        grouped_cambios_pot = grouped_cambios_pot.sort_values(
            by=[u'Fecha_Lectura_Anterior'], ascending=True
        )

        n_cambios_pot = grouped_cambios_pot.shape[0]
        periodes_potencia = ['{}{}{}{}{}{}', '{}{}{}{}{}{}', '{}{}{}{}{}{}']
        fechas_cambios = [u' ' * 10] * 2

        for p in range(0, n_cambios_pot):
            row = grouped_cambios_pot.iloc[p]

            if p > 0:  # Si cambio de potencia añadimos las fechas
                fechas_cambios[p-1] = row[
                    u'Fecha_Lectura_Anterior'
                ].date().strftime(u'%Y-%m-%d')

            periodes_potencia[p] = periodes_potencia[p].format(
                u'{:14.0f}'.format(row[u'PotenciaContratadaP1']).replace(
                    u' ', u'0'
                ),
                u'{:14.0f}'.format(
                    row[u'PotenciaContratadaP2']
                ).replace(
                    u' ', u'0'
                ) if row[u'PotenciaContratadaP2'] else u' ' * 14,

                u'{:14.0f}'.format(
                    row[u'PotenciaContratadaP3']
                ).replace(
                    u' ', u'0'
                ) if row[u'PotenciaContratadaP3'] else u' ' * 14,

                u'{:14.0f}'.format(
                    row[u'PotenciaContratadaP4']
                ).replace(
                    u' ', u'0'
                ) if row[u'PotenciaContratadaP4'] else u' ' * 14,
                u'{:14.0f}'.format(
                    row[u'PotenciaContratadaP5']
                ).replace(
                    u' ', u'0'
                ) if row[u'PotenciaContratadaP5'] else u' ' * 14,
                u'{:14.0f}'.format(
                    row[u'PotenciaContratadaP6']
                ).replace(
                    u' ', u'0'
                ) if row[u'PotenciaContratadaP6'] else u' ' * 14,
            )

        periodes_potencia_nota = u''.join(
            [
                pp if pp != u'{}{}{}{}{}{}' else (u' ' * 14 * 6)
                for pp in periodes_potencia
            ]
        )

        fechas_nota = u''.join(fechas_cambios)

        return u'{}{}'.format(periodes_potencia_nota, fechas_nota)

    def export_comer_files_from_complete_dataframe(self, cursor, uid, ids, complet_df, context=None):
        '''
        will return an encoded zip file with comerfiles
        Care! this will be called after generate reimportation file,
        because dataframe will be altered, if you want to call before
        uncomment copy line
        :param cursor:
        :param uid:
        :param ids:
        :param complet_df:
        :param context:
        :return:
        '''
        # Agrupar o procesar iterativamente por las distintas comers
        # Sacar Ficheros comers

        # complet_df = complet_df.copy()

        # We use lower because some cups are writen with 0f and others with 0F
        cups_in_dataframe = [c_name.upper() for c_name in complet_df[u'CUPS'].unique().tolist()]

        search_comer_cups = '''
        SELECT cups.name AS name, comer.ref AS ref 
        FROM giscedata_cups_ps AS cups
        INNER JOIN giscedata_polissa AS pol ON (pol.id = cups.polissa_polissa)
        INNER JOIN res_partner AS comer ON (comer.id = pol.comercialitzadora)
        WHERE cups.polissa_polissa IS NOT NULL
              AND upper(cups.name) in %s
        '''

        cursor.execute(search_comer_cups, (tuple(cups_in_dataframe), ))

        res_cups = cursor.dictfetchall()

        cups_comer_df = pd.DataFrame(res_cups)

        complet_df[u'comercializadora'] = complet_df.apply(
            lambda x: cups_comer_df.loc[
                (cups_comer_df[u"name"].str.upper() == x.CUPS.upper())
            ].iloc[0][u'ref'],
            axis=1
        )

        comer_list = complet_df[u'comercializadora'].unique().tolist()

        file_zip = StringIO()
        zip_fp = ZipFile(
            file_zip, 'w', compression=ZIP_DEFLATED
        )

        for comer in comer_list:
            comer_df = complet_df.loc[
                (complet_df[u'comercializadora'] == comer)
            ].drop(
                [u'comercializadora', u'Notas'], axis=1
            ).sort_values(by=[u'CUPS', u'FechaDesde'], ascending=True)

            comer_df[u'TarifaATRFact'] = comer_df.TarifaATRFact.astype(str)

            individual_comer = self.dataframe_to_excel(comer_df, encode=False)

            file_in_zip_name = 'ordre_TEC_271_2019_%s.xlsx' % comer

            zip_fp.writestr(file_in_zip_name, individual_comer)

        zip_fp.close()

        zip_str_file = b64encode(file_zip.getvalue())

        file_zip.close()

        return zip_str_file


    @staticmethod
    def export_files_from_fixed_dataframe(fixed_consums_df):
        cups_index_group = fixed_consums_df[
            [
                u'CUPS', u'Tarifa', u'Comunidad_Autonoma',
                u'Fecha_Lectura_Actual', u'Fecha_Lectura_Anterior',
                u'PotenciaContratadaP1', u'PotenciaContratadaP2',
                u'PotenciaContratadaP3', u'PotenciaContratadaP4',
                u'PotenciaContratadaP5', u'PotenciaContratadaP6',
                u'PotenciaFacturadaP1', u'PotenciaFacturadaP2',
                u'PotenciaFacturadaP3', u'PotenciaFacturadaP4',
                u'PotenciaFacturadaP5', u'PotenciaFacturadaP6',
                u'ActivaP1', u'ActivaP2', u'ActivaP3',
                u'ActivaP4', u'ActivaP5', u'ActivaP6',
                u'Total_Activa'
            ]
        ].sort_values(
            by=[u'CUPS', u'Fecha_Lectura_Actual'], ascending=True
        )

        cups_in_dataframe = cups_index_group[u'CUPS'].unique().tolist()

        partition_date = datetime.datetime(2013, 7, 31)

        # "result_dictrows_list" will contain dicts representing rows
        # for output file, it's more efficiently generate Dataframe from list
        # than appending rows to a Dataframe any time

        result_dictrows_list = []
        mapping_107 = dict(TABLA_107)

        def inver_and_cut(x):
            x = tuple(reversed(x))
            replace_func = (
                lambda y: y.replace(
                    u' ', u''
                ).replace(
                    u'.A', u'A'
                ).replace(
                    u'.D', u'D'
                ).replace(
                    u'.N', u'N'
                )
            )
            return replace_func(x[0]), x[1][-2:]
        # Mapping for tariff table
        mapping_107 = mapping_107.__class__(
            map(inver_and_cut, mapping_107.items())
        )

        for cups in cups_in_dataframe:

            facts_from_cups = cups_index_group.loc[
                cups_index_group[u'CUPS'] == cups].sort_values(
                by=[u'Fecha_Lectura_Actual'], ascending=True
            )

            comunidad_autonoma = facts_from_cups.iloc[0][u'Comunidad_Autonoma']

            # TODO SUMAR 1 a la fecha?
            cambios_potencia_contratada = facts_from_cups[
                [
                    u'Fecha_Lectura_Anterior',
                    u'PotenciaContratadaP1',
                    u'PotenciaContratadaP2',
                    u'PotenciaContratadaP3',
                    u'PotenciaContratadaP4',
                    u'PotenciaContratadaP5',
                    u'PotenciaContratadaP6'
                ]
            ].groupby(
                [
                    u'PotenciaContratadaP1',
                    u'PotenciaContratadaP2',
                    u'PotenciaContratadaP3',
                    u'PotenciaContratadaP4',
                    u'PotenciaContratadaP5',
                    u'PotenciaContratadaP6'
                ]
            ).agg(
                {
                    u'Fecha_Lectura_Anterior': np.min
                }
            ).reset_index()
            # Fecha en la que se produjo el cambio es la minima
            # "Fecha_Lectura_Anterior" de la agrupacion

            facts_from_cups = facts_from_cups.drop(
                [
                 u'PotenciaContratadaP1',
                 u'PotenciaContratadaP2',
                 u'PotenciaContratadaP3',
                 u'PotenciaContratadaP4',
                 u'PotenciaContratadaP5',
                 u'PotenciaContratadaP6'
                ], axis=1
            )

            # Add Column days to calculate average (weights)
            facts_from_cups[u'NumeroDias'] = (
                (
                    facts_from_cups[u'Fecha_Lectura_Actual']
                    -
                    facts_from_cups[u'Fecha_Lectura_Anterior']
                 ).astype(u'timedelta64[D]')
            )

            # Create columns with pots x days to after calculate pondered

            facts_from_cups[u'MulPotP1Days'] = (
                facts_from_cups[u'PotenciaFacturadaP1']
                *
                facts_from_cups[u'NumeroDias']
            )

            facts_from_cups[u'MulPotP2Days'] = (
                facts_from_cups[u'PotenciaFacturadaP2']
                *
                facts_from_cups[u'NumeroDias']
            )

            facts_from_cups[u'MulPotP3Days'] = (
                facts_from_cups[u'PotenciaFacturadaP3']
                *
                facts_from_cups[u'NumeroDias']
            )

            facts_from_cups[u'MulPotP4Days'] = (
                facts_from_cups[u'PotenciaFacturadaP4']
                *
                facts_from_cups[u'NumeroDias']
            )

            facts_from_cups[u'MulPotP5Days'] = (
                facts_from_cups[u'PotenciaFacturadaP5']
                *
                facts_from_cups[u'NumeroDias']
            )

            facts_from_cups[u'MulPotP6Days'] = (
                facts_from_cups[u'PotenciaFacturadaP6']
                *
                facts_from_cups[u'NumeroDias']
            )

            facts_from_cups = facts_from_cups.drop(
                [
                    u'PotenciaFacturadaP1',
                    u'PotenciaFacturadaP2',
                    u'PotenciaFacturadaP3',
                    u'PotenciaFacturadaP4',
                    u'PotenciaFacturadaP5',
                    u'PotenciaFacturadaP6'
                ], axis=1
            )

            #  ###############Facturas primer periodo ####################

            facts_primer_periode = facts_from_cups.loc[
                facts_from_cups[u'Fecha_Lectura_Actual'] < u'2013-08-01'
            ]

            #  ###############Facturas segundo periodo ####################

            facts_second_period = facts_from_cups.loc[
                (facts_from_cups[u'Fecha_Lectura_Actual'] >= u'2013-08-01')
                &
                (facts_from_cups[u'Fecha_Lectura_Anterior'] >= u'2013-08-01')
                &
                (facts_from_cups[u'Fecha_Lectura_Anterior'] < u'2013-12-31')
                ]

            #  ########Facturas en los dos periodos periodo #########

            # En la teoria solo puede haber como maximo una factura que se
            # cruce a menos que esta se haya rectificado, en cuyo caso pueden
            # aparecer 2 o mas
            facts_both_periods = facts_from_cups.loc[
                (facts_from_cups[u'Fecha_Lectura_Actual'] >= u'2013-08-01')
                &
                (facts_from_cups[u'Fecha_Lectura_Anterior'] < u'2013-08-01')

            ]

            if not facts_both_periods.empty:
                # We group por si las moscas hay mas de una
                # factura aun que no deberia

                both_periods_aggregation = facts_both_periods.groupby([u'Tarifa']).agg(
                    {
                        # u'PotenciaFacturadaP1': lambda x: np.average(
                        #     x[u'PotenciaFacturadaP1'],
                        #     weights=x[u'NumeroDias']
                        # ),
                        u'MulPotP1Days': np.sum,
                        u'MulPotP2Days': np.sum,
                        u'MulPotP3Days': np.sum,
                        u'MulPotP4Days': np.sum,
                        u'MulPotP5Days': np.sum,
                        u'MulPotP6Days': np.sum,
                        u'NumeroDias': np.sum,
                        u'ActivaP1': np.sum,
                        u'ActivaP2': np.sum,
                        u'ActivaP3': np.sum,
                        u'ActivaP4': np.sum,
                        u'ActivaP5': np.sum,
                        u'ActivaP6': np.sum,
                        u'Fecha_Lectura_Anterior': np.min,
                        u'Fecha_Lectura_Actual': np.max,

                    }
                ).reset_index()

                # Llegados a este punto repartiremos equitativamente las lineas
                # Entre los dos periodos
                for indx, row in both_periods_aggregation.iterrows():
                    days_first_period = (
                        partition_date
                        -
                        row[u'Fecha_Lectura_Anterior']
                    ).days

                    days_second_period = (
                        row[u'Fecha_Lectura_Actual']
                        -
                        partition_date
                    ).days

                    # Now we have days corresponding to any period
                    # and we cant separate energy and pot
                    # MulPotP1Days is sum grouped days x group pot(g by tariff)
                    # To obtain the proportional part we div
                    # MulPotP1Days for full days of invoices of same tariff
                    first_period_row = {
                        u'Tarifa': row[u'Tarifa'],
                        u'MulPotP1Days': row[u'MulPotP1Days'] / row[u'NumeroDias'] * days_first_period,
                        u'MulPotP2Days': row[u'MulPotP2Days'] / row[u'NumeroDias'] * days_first_period,
                        u'MulPotP3Days': row[u'MulPotP3Days'] / row[u'NumeroDias'] * days_first_period,
                        u'MulPotP4Days': row[u'MulPotP4Days'] / row[u'NumeroDias'] * days_first_period,
                        u'MulPotP5Days': row[u'MulPotP5Days'] / row[u'NumeroDias'] * days_first_period,
                        u'MulPotP6Days': row[u'MulPotP6Days'] / row[u'NumeroDias'] * days_first_period,
                        u'NumeroDias': days_first_period,
                        u'ActivaP1': row[u'ActivaP1'] / row[u'NumeroDias'] * days_first_period,
                        u'ActivaP2': row[u'ActivaP2'] / row[u'NumeroDias'] * days_first_period,
                        u'ActivaP3': row[u'ActivaP3'] / row[u'NumeroDias'] * days_first_period,
                        u'ActivaP4': row[u'ActivaP4'] / row[u'NumeroDias'] * days_first_period,
                        u'ActivaP5': row[u'ActivaP5'] / row[u'NumeroDias'] * days_first_period,
                        u'ActivaP6': row[u'ActivaP6'] / row[u'NumeroDias'] * days_first_period,
                        u'Fecha_Lectura_Anterior': row[u'Fecha_Lectura_Anterior'],
                        u'Fecha_Lectura_Actual':  partition_date
                    }

                    second_period_row = {
                        u'Tarifa': row[u'Tarifa'],
                        u'MulPotP1Days': row[u'MulPotP1Days'] / row[u'NumeroDias'] * days_second_period,
                        u'MulPotP2Days': row[u'MulPotP2Days'] / row[u'NumeroDias'] * days_second_period,
                        u'MulPotP3Days': row[u'MulPotP3Days'] / row[u'NumeroDias'] * days_second_period,
                        u'MulPotP4Days': row[u'MulPotP4Days'] / row[u'NumeroDias'] * days_second_period,
                        u'MulPotP5Days': row[u'MulPotP5Days'] / row[u'NumeroDias'] * days_second_period,
                        u'MulPotP6Days': row[u'MulPotP6Days'] / row[u'NumeroDias'] * days_second_period,
                        u'NumeroDias': days_second_period,
                        u'ActivaP1': row[u'ActivaP1'] / row[u'NumeroDias'] * days_second_period,
                        u'ActivaP2': row[u'ActivaP2'] / row[u'NumeroDias'] * days_second_period,
                        u'ActivaP3': row[u'ActivaP3'] / row[u'NumeroDias'] * days_second_period,
                        u'ActivaP4': row[u'ActivaP4'] / row[u'NumeroDias'] * days_second_period,
                        u'ActivaP5': row[u'ActivaP5'] / row[u'NumeroDias'] * days_second_period,
                        u'ActivaP6': row[u'ActivaP6'] / row[u'NumeroDias'] * days_second_period,
                        u'Fecha_Lectura_Anterior': partition_date,
                        u'Fecha_Lectura_Actual':  row[u'Fecha_Lectura_Actual']
                    }

                    facts_primer_periode = facts_primer_periode.append(
                        first_period_row, ignore_index=True
                    )
                    facts_second_period = facts_second_period.append(
                        second_period_row, ignore_index=True
                    )

            # Ahora que ya tenemos repartida la potencia i energia en cada
            # periodo incluyendo la separacion de las facturas que
            # cruzavan periodos empezamos a procesar i a agrupar otra vez por
            # tarifas para cada periodo

            # IMPORTANTE: al hacer una suma directa de los dias,
            # conseguimos los dias reales, por si hay algun mes no facturado
            # o alguna cosa extraña de estas, aunque las fechas pueden no
            # corresponder con los dias, pero nos servira para realizar
            # correctamente los calculos, nuestro lema és ni un watio
            # mas ni uno menos.

            first_period_aggregation = facts_primer_periode.groupby(
                [u'Tarifa']
            ).agg(
                {
                    # u'PotenciaFacturadaP1': lambda x: np.average(
                    #     x[u'PotenciaFacturadaP1'], weights=x[u'NumeroDias']
                    # ),
                    u'MulPotP1Days': np.sum,
                    u'MulPotP2Days': np.sum,
                    u'MulPotP3Days': np.sum,
                    u'MulPotP4Days': np.sum,
                    u'MulPotP5Days': np.sum,
                    u'MulPotP6Days': np.sum,
                    u'NumeroDias': np.sum,
                    u'ActivaP1': np.sum,
                    u'ActivaP2': np.sum,
                    u'ActivaP3': np.sum,
                    u'ActivaP4': np.sum,
                    u'ActivaP5': np.sum,
                    u'ActivaP6': np.sum,
                    u'Fecha_Lectura_Anterior': np.min,
                    u'Fecha_Lectura_Actual': np.max,

                }
            ).reset_index()

            second_period_aggregation = facts_second_period.groupby(
                [u'Tarifa']
            ).agg(
                {

                    u'MulPotP1Days': np.sum,
                    u'MulPotP2Days': np.sum,
                    u'MulPotP3Days': np.sum,
                    u'MulPotP4Days': np.sum,
                    u'MulPotP5Days': np.sum,
                    u'MulPotP6Days': np.sum,
                    u'NumeroDias': np.sum,
                    u'ActivaP1': np.sum,
                    u'ActivaP2': np.sum,
                    u'ActivaP3': np.sum,
                    u'ActivaP4': np.sum,
                    u'ActivaP5': np.sum,
                    u'ActivaP6': np.sum,
                    u'Fecha_Lectura_Anterior': np.min,
                    u'Fecha_Lectura_Actual': np.max,

                }
            ).reset_index()

            # Alguno de los dataframes puede estar vacio, ya que no tiene
            # por que haber consumos en uno de los periodos

            notas = WizardExportSuplementosFromExternalFile.generate_notas(
                cambios_potencia_contratada
            )

            if not first_period_aggregation.empty:
                for i_f_agg, row_f_p in first_period_aggregation.iterrows():
                    l_r_dict = WizardExportSuplementosFromExternalFile. \
                        grouped_row_to_formated_dict(
                            row_f_p, mapping_107, True, comunidad_autonoma,
                            cups
                        )
                    l_r_dict.update(
                        {
                            'Notas': notas
                        }
                    )
                    result_dictrows_list.append(l_r_dict)

            if not second_period_aggregation.empty:
                for i_s_agg, row_s_p in second_period_aggregation.iterrows():
                    l_r_dict = WizardExportSuplementosFromExternalFile. \
                        grouped_row_to_formated_dict(
                            row_s_p, mapping_107, False,
                            comunidad_autonoma, cups
                        )
                    l_r_dict.update(
                        {
                            'Notas': notas
                        }
                    )
                    result_dictrows_list.append(l_r_dict)

        if not result_dictrows_list:
            raise except_wizard(
                u'Error',
                u'No se han encontrado lineas de consumos para el 2013'
            )

        sorted_header = sup_distri_obj.export_valid_header[:]
        sorted_header.append(u'Notas')

        result_complete_dataframe = pd.DataFrame(
            result_dictrows_list,
            columns=sorted_header
        ).sort_values(by=[u'CUPS', u'FechaDesde'], ascending=True)

        result_complete_dataframe[u'FechaDesde'] = result_complete_dataframe[
            u'FechaDesde'
        ].dt.strftime(u'%Y-%m-%d')

        result_complete_dataframe[u'FechaHasta'] = result_complete_dataframe[
            u'FechaHasta'
        ].dt.strftime(u'%Y-%m-%d')

        return result_complete_dataframe

    _columns = {
        u'state': fields.selection(
            [(u'init', u'Init'), (u'end', u'End')], 'Estado'
        ),
        u'imported_file': fields.binary('Fichero Externo', required=True),
        u'imported_file_name': fields.char(
            'Nombre fichero importado', size=40
        ),

        u'comers_zip_file': fields.binary('Ficheros comercializadora'),
        u'comers_zip_file_name': fields.char('Nombre Zip comercializadoras', size=40),

        u'reimportacion_file': fields.binary('Fichero Liquidaciones'),
        u'reimportacion_file_name': fields.char(
            'Nombre fichero Liquidaciones', size=40
        ),

        u'info': fields.text('Información'),

        u'fix_periods': fields.boolean(
            'Fix periodos',
            help=u'Los periodos de las bases de datos de otros clientes '
                 u'estan girados, si el fichero a importar no esta corregido '
                 u'marcar esta opción ej. 2.0 P1<-P2  | 3.0 P2<-P3'
        )
    }

    _defaults = {
        u'reimportacion_file_name': lambda *a: u'ordre_TEC_271_2019.xlsx',
        u'comers_zip_file_name': lambda *a: u'ordre_TEC_271_2019_comers.zip',
        u'state': lambda *a: u'init',
        u'fix_periods': lambda *a: True,
        u'info': _(u'Pasos:\n'
                   u'1: Importar fichero externo generado\n'
                   u'2: Exportar desde fichero\n\n'
                   u'*Fix periodos, sirve para corregir problemas con los '
                   u'datos de los periodos ej. P2->P1'
                   u'Una vez finalizado el proceso se generaran dos ficheros,'
                   u'un ZIP con los ficheros XLSX para enviar a las '
                   u'comercializadoras y un fichero XLSX para ser '
                   u'reimportado en el ERP y generar las lineas extra')
    }

WizardExportSuplementosFromExternalFile()
