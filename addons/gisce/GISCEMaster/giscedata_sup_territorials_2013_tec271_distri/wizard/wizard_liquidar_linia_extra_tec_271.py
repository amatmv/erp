# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from wizard import except_wizard


class WizardLiquidarLiniaExtraTEC271(osv.osv_memory):

    _name = u'wizard.liquidar.linia.extra.tec271'

    def liquidar_linia_extra(self, cursor, uid, ids, context=None):
        extra_ids = context.get(u'active_ids', False)
        if extra_ids and len(extra_ids) > 1:
            raise except_wizard(u'Error', u'Por seguridad solamente se '
                                          u'permite liquidar una linea por '
                                          u'proceso'
                                )
        extra_id = context.get(u'active_id')

        extra_obj = self.pool.get('giscedata.facturacio.extra')
        product_obj = self.pool.get('product.product')
        imd_obj = self.pool.get('ir.model.data')

        original_extra_info = extra_obj.read(
            cursor, uid, extra_id,
            [u'product_id', u'term'],
            context=context
        )

        product_id = original_extra_info[u'product_id']

        if not product_id:
            raise except_wizard(u'Error', u'La linea extra que intentas '
                                          u'liquidar no tiene producto'
                                )
        product_id = product_id[0]

        product_tec271_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_contractacio_distri', 'energia_ordre_tec_271_2019'
            )[1]

        if product_id != product_tec271_id:
            raise except_wizard(u'Error', u'La linea extra que intentas '
                                          u'liquidar no tiene el '
                                          u'producto TEC271'
                                )

        created_updated_ex = extra_obj.facturar_pendiente_con_nueva_linea_extra(
            cursor, uid, extra_id,
            tuple_format=True,
            context=context
        )

        created_updated_ex_id = created_updated_ex[0][1]

        # If only_update no se habia facturado ningun termino solamente
        # actualizamos la linea
        only_update = not created_updated_ex[0][0]

        new_updated_extra_info = extra_obj.read(
            cursor, uid, created_updated_ex_id,
            [u'price_unit', u'term', u'total_amount_pending']
        )

        if only_update:
            result_message = _(
                u'Se ha actualizado la linea actual de {} terminos a {} termino'
                u'\n'
                u'Pendiente a facturar: {} €'
            ).format(
                original_extra_info[u'term'],
                new_updated_extra_info[u'term'],
                new_updated_extra_info[u'total_amount_pending']
            )
        else:
            original_updated_extra_info = extra_obj.read(
                cursor, uid, extra_id,
                [u'term', u'total_amount_invoiced'],
                context=context
            )
            result_message = _(
                u'Nueva linea creada con {} termino con '
                u'pendiente a facturar {} \n'
                u'Linea original liquidada ha quedado con {} terminos y '
                u'facturada con {}'
            ).format(
                new_updated_extra_info[u'term'],
                new_updated_extra_info[u'total_amount_pending'],
                original_updated_extra_info[u'term'],
                original_updated_extra_info[u'total_amount_invoiced']
            )
        self.write(
            cursor, uid, ids,
            {
                u'state': u'end',
                u'info': result_message
             },
            context=context
        )

    _columns = {
        u'state': fields.selection(
            [(u'init', u'Init'), (u'end', u'End')], 'Estado'
        ),
        u'info': fields.text('Información'),
    }

    _defaults = {
        u'state': lambda *a: u'init',
        u'info': lambda *a: u'Se procedera a generar una linea extra de 1 '
                            u'termino para liquidar el pendiente a facturar '
                            u'de la linea extra actual, si aun no se habia '
                            u'liquidado nada se actualizara la linea actual a '
                            u'1 termino\n'
                            u'Solamente realizar esta acción quando se vaia a '
                            u'hacer un cambio de titular o dar de baja al '
                            u'cliente'
    }
WizardLiquidarLiniaExtraTEC271()
