# -*- coding: utf-8 -*-

from osv import osv, fields
from base64 import b64decode
import pandas as pd
import datetime
from tools.translate import _
from wizard import except_wizard
from StringIO import StringIO
from giscedata_sup_territorials_2013_tec271_distri.\
    giscedata_sup_territorials_2013_tec271_distri import \
    GiscedataSuplementsTerritorials2013Tec271Distri as sup_distri_obj


class WizardExportSupTerritorialsTec271Distri(osv.osv_memory):
    """ Wizard per importar CSV dels Suplements Territorials"""
    _name = 'wizard.export.sup.territorials.tec271.distri'

    def export_to_xls(self, cursor, uid, ids, context=None):
        # Suplemento is a python obj
        suplemento = sup_distri_obj(
            self.pool, cursor, uid, context=context
        )

        res = suplemento.export_to_excel()

        if res:
            cosas_a_revisar = ''
            distintas_potencias_contratadas_mismo_periodo_i_factura = res['errors']['to_revise']['multiple_pot_same_period']
            mas_de_tres_cambios_de_potencia = res['errors']['to_revise']['multiple_pot_change']
            cups_con_cambios_de_potencia = res['errors']['to_revise']['pot_changed']

            if distintas_potencias_contratadas_mismo_periodo_i_factura:
                for e in distintas_potencias_contratadas_mismo_periodo_i_factura:
                    cosas_a_revisar = '{}{}\n'.format(cosas_a_revisar, e)

            if mas_de_tres_cambios_de_potencia:
                for e in mas_de_tres_cambios_de_potencia:
                    cosas_a_revisar = '{}{}\n'.format(cosas_a_revisar, e)

            if cups_con_cambios_de_potencia:
                cosas_a_revisar = '{}{}\n'.format(cosas_a_revisar, 'Cups con cambios de potencia:')
                for c in list(set(cups_con_cambios_de_potencia)):
                    cosas_a_revisar = '{}{}\n'.format(cosas_a_revisar, c)

            info = 'Ficheros generados correctamente'
            if cosas_a_revisar:
                info = '{} pero estaria bien revisar los comentarios en estos casos:\n{}'.format(info, cosas_a_revisar)
            self.write(cursor, uid, ids,
                       {
                           'state': 'end',
                           'file': res['xlsx_complet'],
                           'zip_file': res['zip_comers'],
                           'info': info
                       }, context=context)
        else:
            self.write(cursor, uid, ids,
                       {
                           'state': 'end',
                           'info': 'No se han encontrado consumos en 2013'
                       }, context=context)
    _columns = {
        'state': fields.selection([('init', 'Inici'), ('end', 'Fi')], 'Estat'),
        'file': fields.binary('File'),
        'file_name': fields.char('Nombre del fichero', size=256),
        'info': fields.text('Información'),
        'zip_file': fields.binary('Ficheros separados por comercializadora'),
        'zip_name': fields.char('Nom del zip', size=256),
    }

    _defaults = {
        'file_name': lambda *a: 'ordre_TEC_271_2019.xlsx',
        'zip_name': lambda *a: 'ordre_TEC_271_2019_comers.zip',
        'state': lambda *a: 'init'
    }


WizardExportSupTerritorialsTec271Distri()