# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from utilidades import TestingUtils
from datetime import datetime
from dateutil.relativedelta import relativedelta
from base64 import b64decode
from giscedata_sup_territorials_2013_tec271_distri.defs_suplementos_territoriales_2013_tec_271 import *
from StringIO import StringIO
import pandas as pd
import unittest


class TestsExportConsums(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.utils = TestingUtils(
            self.openerp.pool, self.cursor, self.uid
        )

    def tearDown(self):
        self.txn.stop()

    @unittest.skip('To be redone')
    def test_export_consums(self):
        cursor = self.cursor
        uid = self.uid

        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        cnae_9820 = imd_obj.get_object_reference(
            cursor, uid, 'giscemisc_cnae', 'cnae_9820'
        )[1]

        pol_obj.write(
            cursor, uid, polissa_id,
            {
                'data_alta': '2013-01-01',
                'cnae': cnae_9820,
                'data_baixa': (datetime.today()+relativedelta(months=1)).strftime('%Y-%m-%d')
             }
        )

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        cups_of_polissa_id = pol_obj.read(
            cursor, uid, polissa_id, ['cups']
        )['cups'][0]

        date = datetime.today()

        for y in range(2013, (date.year + 1)):
            self.utils.create_fiscal_year(y)

        self.utils.change_comunidad_cups(cups_of_polissa_id, '01')

        self.utils.activar_polissa(polissa_id)

        # Crear version de precios
        self.utils.create_pricelist_versions('2013-01-01')

        journal_id = self.utils.get_journal_id_from_name('ENERGIA')[0]

        fact_1_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]

        fact_obj.write(
            cursor, uid, fact_1_id,
            {
                'data_inici': '2013-01-01',
                'data_final': '2013-03-31'
            }
        )

        comptador_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'comptador_0001'
        )[1]

        self.utils.crear_lectures(comptador_id, '2013-02-28', fact_1_id)

        self.utils.open_factura(fact_1_id)

        wiz_export_consums_obj = self.openerp.pool.get(
            'wizard.export.sup.territorials.tec271.distri'
        )

        wiz_id = wiz_export_consums_obj.create(cursor, uid, {})

        wiz_export_consums_obj.export_to_xls(cursor, uid, [wiz_id])

        ff = b64decode(wiz_export_consums_obj.read(cursor, uid, wiz_id, ['file'])[0]['file'])

        f = StringIO(ff)

        df = pd.read_excel(f, dtype={u'TarifaATRFact': str})
        df.fillna(value=0, inplace=True)

        n_rows = df.shape[0]
        self.assertEqual(n_rows, 1)

        row = df.iloc[0]

        # 2.0A 1 p
        self.assertEqual(
            round(
                (
                    row[u'MediaPondPotenciaAFacturar_P1 (W)'] *
                    row[u'SuplementoPotencia_P1 (€/W)']
                    / pow(1000, 2)
                ), 2
            ),
            row[u'ImporteTotalSuplementoTerminoPotencia (€)']

        )
        # 4.6 Kw is hardcoded value in utils.crear_lectures()
        self.assertEqual(
            row[u'ImporteTotalSuplementoTerminoPotencia (€)'],
            round((4.6 * potencia_andalucia_period_1['2.0A'][0]), 2)

        )

        self.assertEqual(
            round(
                (
                    row[u'ValorEnergiaActiva_P1 (kWh)'] *
                    row[u'SuplementoEnergia_P1 (€/kWh)']
                ), 2
            ),
            row[u'ImporteTotalSuplementoTerminoEnergia (€)']

        )
        # 317 Kwh consum ene activa is hardcoded value in utils.crear_lectures()
        self.assertEqual(
            row[u'ImporteTotalSuplementoTerminoEnergia (€)'],
            round((317 * energia_andalucia_period_1['2.0A'][0]), 2)

        )

        self.assertEqual(
            row[u'ImporteTotalARegularizar (€)'],
            (
                round(
                    (
                        (317 * energia_andalucia_period_1['2.0A'][0]) +
                        (4.6 * potencia_andalucia_period_1['2.0A'][0])
                    ),
                    2
                )
            )
        )
