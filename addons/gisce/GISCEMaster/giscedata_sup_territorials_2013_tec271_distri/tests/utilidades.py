# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta


class TestingUtils(object):
    codi_comunitats = [
        '01',  # Andalucía
        '02',  # Aragón
        '03',  # Asturais
        '06',  # Cantabria
        '07',  # Castilla y león
        '09',  # Cataluña
        '11',  # Extremadura
        '12',  # Galicia
        '13',  # Madrid
        '14',  # Murcia
        '15',  # Navarra
    ]

    def __init__(self, pool, cursor, uid, context=None):
        self.pool = pool
        self.cursor = cursor
        self.uid = uid
        self.context = {} if context is None else context

    def change_comunidad_cups(self, cups, codi_comunitat):
        '''
        Hace que la direccion del cups cam,bie de comunidad autonoma
        :param cups: str with cups or id in datebase
        :return:
        '''
        cursor = self.cursor
        uid = self.uid
        context = self.context

        if codi_comunitat not in self.codi_comunitats:
            raise Exception(
                (
                    u'Codigo de comunidad %s no valido, '
                    u'el planeta marte no és una comunidad autonoma valida'
                ) % codi_comunitat
            )

        cups_obj = self.pool.get('giscedata.cups.ps')

        cups_id = False

        if isinstance(cups, (int, long)):
            cups_id = cups
        elif isinstance(cups, (basestring, str, unicode)):
            cups_list = cups_obj.search(
                cursor, uid, [('name', 'ilike', '{}%'.format(cups[:20]))],
                context=context
            )
            if cups_list:
                cups_id = cups_list[0]

        if not cups_id:
            raise Exception(
                u'Este cups ya no existe %s' % cups
            )

        comunidad_obj = self.pool.get('res.comunitat_autonoma')

        comunidad_id = comunidad_obj.search(
            cursor, uid, [('codi', '=', codi_comunitat)], context=context
        )[0]

        provincia_obj = self.pool.get('res.country.state')

        provincias_de_la_comunidad_ids = provincia_obj.search(
            cursor, uid, [('comunitat_autonoma', '=', comunidad_id)],
            context=context
        )

        if not provincias_de_la_comunidad_ids:
            raise Exception(
                u'No existen provincias para esta comunidad'
            )

        municipi_obj = self.pool.get('res.municipi')

        municipis_de_la_provincia_ids = municipi_obj.search(
            cursor, uid, [('state', 'in', provincias_de_la_comunidad_ids)],
            context=context
        )

        if not municipis_de_la_provincia_ids:
            raise Exception(
                u'No existen municipios para las provincias de '
                u'esta comunidad %s'
            )

        municipi_de_la_provincia_id = municipis_de_la_provincia_ids[0]

        cups_obj.write(
            cursor, uid, cups_id,
            {'id_municipi': municipi_de_la_provincia_id},
            context=context
        )

    def activar_polissa(self, polissa_id):
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        cursor = self.cursor
        uid = self.uid
        context = self.context

        polissa_obj = self.pool.get('giscedata.polissa')

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        state = polissa_obj.read(
            cursor, uid, polissa_id, ['state'], context=context
        )['state']

        if state != 'activa':
            raise Exception(
                u'No se ha podido activar la polissa, una patrulla de '
                u'policia se dirige hacia su localización'
            )

    def create_pricelist_versions(self, start_from_these_date):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        price_list = self.pool.get('product.pricelist')
        pl_vers_obj = self.pool.get('product.pricelist.version')

        tarifas_electricidad_id = price_list.search(
            cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
        )[0]

        create_version_params = {
            'name': 'Version - %s' % start_from_these_date.replace('-', '/'),
            'active': True,
            'pricelist_id': tarifas_electricidad_id,
            'date_start': start_from_these_date
        }

        pr_ver_id = pl_vers_obj.create(
            cursor, uid, create_version_params,
            context=context
        )

        return tarifas_electricidad_id, pr_ver_id

    def get_journal_id_from_name(self, journal_name):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        journal_obj = self.pool.get('account.journal')

        journals = journal_obj.search(
            cursor, uid, [('code', '=', journal_name)], context=context
        )

        return journals

    def unlink_extra_lines(self):
        extra_obj = self.pool.get('giscedata.facturacio.extra')

        cursor = self.cursor
        uid = self.uid
        context = self.context

        extra_ids = extra_obj.search(cursor, uid, [], context=context)

        extra_obj.unlink(cursor, uid, extra_ids, context=context)

        extra_ids = extra_obj.search(cursor, uid, [], context=context)

        if extra_ids:
            raise Exception(u'No se han borrado todas las lineas extra')

    def create_modcon(self, polissa_id, date_from, date_to=False):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        polissa_obj = self.pool.get('giscedata.polissa')
        imd_obj = self.pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        # polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )

        mod_create = {
            'data_inici': date_from,
            'data_final': date_to
        }

        mod_create.update(res.get('value', {}))

        wiz_mod.write(mod_create)

        wiz_mod.action_crear_contracte(ctx)

    def create_fiscal_year(self, year):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        fiscal_obj = self.pool.get('account.fiscalyear')

        fiscals = fiscal_obj.search(
            cursor, uid,
            [
                '|',
                ('name', 'ilike', '%{}%'.format(str(year))),
                ('code', 'ilike', '%{}%'.format(str(year)))
            ],
            context=context
        )

        if fiscals:
            return fiscals

        f_id = fiscal_obj.create(
            cursor, uid,
            {
                'date_start': '%s-01-01' % year,
                'date_stop': '%s-12-01' % year,
                'code': 'FY%s' % year,
                'name': 'Fiscal Year %s' % year
             },
            context=context
        )

        fiscal_obj.create_period(cursor, uid, [f_id])

        return f_id

    def open_factura(self, factura_id, force_date=False):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        if force_date:
            fact_obj.write(
                cursor, uid, factura_id, {'date_invoice': force_date}
            )

        fact_obj.button_reset_taxes(cursor, uid, [factura_id])

        fact_obj.invoice_open(cursor, uid, [factura_id], context=context)

    @staticmethod
    def assertXmlEqual(got, want):
        from lxml.doctestcompare import LXMLOutputChecker
        from doctest import Example

        checker = LXMLOutputChecker()
        if checker.check_output(want, got, 0):
            return
        message = checker.output_difference(Example("", want), got, 0)
        raise AssertionError(message)

    def crear_lectures(self, comptador_id, data_lectures, fact_id):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        facturador_obj = self.pool.get(
            'giscedata.facturacio.facturador'
        )

        fact_obj = self.pool.get(
            'giscedata.facturacio.factura'
        )

        fact = fact_obj.browse(cursor, uid, fact_id, context=context)

        if fact.lectures_energia_ids:
            for l in fact.lectures_energia_ids:
                l.unlink()

        if fact.lectures_potencia_ids:
            for l in fact.lectures_potencia_ids:
                l.unlink()

        fact.write({
            "lectures_energia_ids": [(6, 0, [])],
            "lectures_potencia_ids": [(6, 0, [])]
        })

        comp_name = comptador_obj.read(
            cursor, uid, comptador_id, ['name'], context=context
        )['name']

        l1 = {
            'periode': (1, 'P1'),
            'lectura': 12247,
            'consum': 317,
            'ajust': 317,
            'name': data_lectures,
            'comptador': (comptador_id, comp_name),
            'tipus': 'A'
        }

        l2 = l1.copy()
        l2.update({'periode': (2, 'P2')})
        l3 = l1.copy()
        l3.update({'periode': (3, 'P3')})
        l4 = l1.copy()
        l4.update({'periode': (4, 'P4')})
        l5 = l1.copy()
        l5.update({'periode': (5, 'P5')})
        l6 = l1.copy()
        l6.update({'periode': (6, 'P6')})

        lect_ant = {
            'lectura': 5,
            'name': (
                datetime.strptime(data_lectures, '%Y-%m-%d')-relativedelta(months=1)+relativedelta(day=31)
            ).strftime('%Y-%m-%d'),
        }

        facturador_obj.crear_lectures_energia(
            cursor, uid, fact_id, {
                1: {'actual': l1, 'anterior': lect_ant},
                2: {'actual': l2, 'anterior': lect_ant},
                3: {'actual': l3, 'anterior': lect_ant},
                4: {'actual': l4, 'anterior': lect_ant},
                5: {'actual': l5, 'anterior': lect_ant},
                6: {'actual': l6, 'anterior': lect_ant}
            }
        )

        p1 = {
            'name': 'P1',
            'pot_contract': 4.6,
            'pot_maximetre': 0,
            'exces': 0,
            'comptador_id': comptador_id,
            'factura_id': fact_id,
            'data_actual': data_lectures,
            'data_anterior': (
                datetime.strptime(data_lectures, '%Y-%m-%d')-relativedelta(months=1)+relativedelta(day=31)
            ).strftime('%Y-%m-%d')
        }
        p2 = p1.copy()
        p2.update({'name': 'P2'})
        p3 = p1.copy()
        p3.update({'name': 'P3'})
        p4 = p1.copy()
        p4.update({'name': 'P4'})
        p5 = p1.copy()
        p5.update({'name': 'P5'})
        p6 = p1.copy()
        p6.update({'name': 'P6'})

        facturador_obj.crear_lectures_potencia(
            cursor, uid, fact_id, {
                'P1': p1, 'P2': p2, 'P3': p3, 'P4': p4, 'P5': p5, 'P6': p6
            }
        )

    def test_invoice(self, fact_ids):
        fact_ids = sorted(fact_ids)

