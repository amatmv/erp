# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from utilidades import TestingUtils
from datetime import datetime
from base64 import b64encode, b64decode
from addons import get_module_resource
from json import loads
from StringIO import StringIO
import pandas as pd
import unittest


class TestsImportConsums(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.utils = TestingUtils(
            self.openerp.pool, self.cursor, self.uid
        )

    def tearDown(self):
        self.txn.stop()

    def test_import_one_line_consum_cups(self):
        pass

    @unittest.skip('To be redone')
    def test_import_two_lines_consum_cups_different_periods(self):
        cursor = self.cursor
        uid = self.uid

        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        cups_of_polissa_id = pol_obj.read(
            cursor, uid, polissa_id, ['cups']
        )['cups'][0]

        date = datetime.today()

        for y in range(2013, (date.year + 1)):
            self.utils.create_fiscal_year(y)

        self.utils.unlink_extra_lines()

        self.utils.change_comunidad_cups(cups_of_polissa_id, '01')

        self.utils.activar_polissa(polissa_id)

        date = datetime(date.year, date.month, 1).strftime('%Y-%m-%d')

        self.utils.create_modcon(polissa_id, date)

        # WIZARD IMPORT START
        import_consums_file_path = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_distri',
            'tests', 'fixtures',
            'consumos_cups_andalucia_dos_periodos_sin_calculos_echos.xlsx'
        )

        import_consums_file = False

        with open(import_consums_file_path, 'r') as f:
            import_consums_file = f.read()
            f.close()

        io_file = StringIO(import_consums_file)

        df_imported_file = pd.read_excel(io_file, dtype={u'TarifaATRFact': str})

        df_imported_file.fillna(value=0, inplace=True)

        # Lambda expression to remove \n and ' ' from column names
        clean_header_of_unworthy_chars = (
            lambda s: s.replace('\n', '').replace(' ', '').replace("\"", "")
        )
        df_imported_file = df_imported_file.rename(
            columns=lambda x: clean_header_of_unworthy_chars(x)
        )

        df_imported_file[u'FechaDesde'] = df_imported_file[u'FechaDesde'].dt.date
        df_imported_file[u'FechaHasta'] = df_imported_file[u'FechaHasta'].dt.date

        wiz_import_consums_obj = self.openerp.pool.get(
            'wizard.import.sup.territorials.tec271.distri'
        )

        wiz_import_id = wiz_import_consums_obj.create(
            cursor, uid,
            {
                'file': b64encode(import_consums_file),
                'file_name': 'consumos_cups_andalucia_dos_periodos.xlsx'
            }
        )
        wiz_import_consums_obj.import_xls(cursor, uid, [wiz_import_id])

        info = wiz_import_consums_obj.read(
            cursor, uid, wiz_import_id, ['info']
        )[0]['info']

        self.assertEqual(info, 'Fitxer importat correctament')

        # WIZARD IMPORT END

        # CHECK CREATED EXTRA LINES START
        extraline_obj = self.openerp.pool.get('giscedata.facturacio.extra')
        product_obj = self.openerp.pool.get('product.product')

        product_tec271 = product_obj.search(
            cursor, uid, [('default_code', '=', 'TEC271')]
        )[0]

        extra_ids = extraline_obj.search(
            cursor, uid, [('product_id', '=', product_tec271)]
        )

        # Solo se deberia crear una linea extra
        self.assertEqual(len(extra_ids), 1)

        extra_id = extra_ids[0]

        extra_info = extraline_obj.read(cursor, uid, extra_id, [])

        # TODO asserts with excel
        # print extra_info

        first_imported_row = df_imported_file.iloc[0]
        second_imported_row = df_imported_file.iloc[1]
        # print first_imported_row
        # print second_imported_row
        # ASSERTS EXTRA LINE

        self.assertEqual(extra_info['product_id'][0], product_tec271)

        self.assertEqual(extra_info['quantity'], 1.0)

        importe_potencia_1 = (
            (first_imported_row[u'MediaPondPotenciaAFacturar_P1(W)'] / 1000)
            *
            (first_imported_row[u'SuplementoPotencia_P1(€/W)'] / 1000)
            *
            (
                first_imported_row[u'FechaHasta']
                -
                first_imported_row[u'FechaDesde']
            ).days
            / 365
        )

        importe_enrgia_1 = (
            first_imported_row[u'ValorEnergiaActiva_P1(kWh)']
            *
            first_imported_row[u'SuplementoEnergia_P1(€/kWh)']
        )

        importe_potencia_2 = (
            (second_imported_row[u'MediaPondPotenciaAFacturar_P1(W)'] / 1000)
            *
            (second_imported_row[u'SuplementoPotencia_P1(€/W)'] / 1000)
            *
            (
                second_imported_row[u'FechaHasta']
                -
                second_imported_row[u'FechaDesde']
            ).days
            / 365
        )
        importe_enrgia_2 = (
            second_imported_row[u'ValorEnergiaActiva_P1(kWh)']
            *
            second_imported_row[u'SuplementoEnergia_P1(€/kWh)']
        )

        # print importe_potencia_1 + importe_potencia_2
        # print importe_enrgia_1 + importe_enrgia_2
        # print (importe_potencia_1 + importe_potencia_2) + importe_enrgia_1 + importe_enrgia_2


        self.assertEqual(extra_info['price_term'], )

        # CHECK CREATED EXTRA LINES END

        # CHECK CREATED LIQUIDACIONS START
        liquidacio_obj = self.openerp.pool.get(
            'giscedata.liquidacio.suplement.territorial.tec271.data'
        )
        liquidacions = liquidacio_obj.search(
            cursor, uid, [('cups_id', '=', cups_of_polissa_id)]
        )
        # Se deberian crear dos liquidaciones
        self.assertEqual(len(liquidacions), 2)

        # TODO asserts with excel
        for l in liquidacio_obj.read(cursor, uid, liquidacions, []):
            pass# print l

        # CHECK CREATED LIQUIDACIONS END

    @unittest.skip('To be redone')
    def test_import_two_lines_consum_cups_different_periods_export_f1(self):
        cursor = self.cursor
        uid = self.uid

        # Call this test to create extralines and liquidacions
        self.test_import_two_lines_consum_cups_different_periods()

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        cups_of_polissa_id = pol_obj.read(
            cursor, uid, polissa_id, ['cups']
        )['cups'][0]

        # Generate Factura
        wz_mi_obj = self.openerp.pool.get("wizard.manual.invoice")

        # ctx = {
        #     'active_id': polissa_id,
        #     'active_ids': [polissa_id]
        # }

        wiz_create_dict = {
            'polissa_id': polissa_id,
            'extra_lines_invoice': True
        }

        wiz_manual_inv_id = wz_mi_obj.create(cursor, uid, wiz_create_dict)

        update_onchange = wz_mi_obj.onchange_polissa_id(
            cursor, uid, [wiz_manual_inv_id], polissa_id
        ).get('value', {})

        update_onchange.update(
            wz_mi_obj.onchange_concepts_invoice_check(
                cursor, uid, [wiz_manual_inv_id],
                extra_lines_invoice=True,
                polissa_id=polissa_id
            )['value']
        )

        price_list_from_date = update_onchange['date_end']

        # Crear version de precios
        self.utils.create_pricelist_versions(price_list_from_date)

        journal_id = self.utils.get_journal_id_from_name('ENERGIA')[0]

        update_onchange.update({'journal_id': journal_id})

        wz_mi_obj.write(
            cursor, uid, [wiz_manual_inv_id], update_onchange
        )

        wz_mi_obj.action_manual_invoice(
            cursor, uid, [wiz_manual_inv_id]
        )

        generated_invoices = wz_mi_obj.read(
            cursor, uid, wiz_manual_inv_id, ['invoice_ids']
        )[0]['invoice_ids']

        factura_ids = loads(generated_invoices)

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        date = datetime.today()
        date = datetime(date.year, date.month, 1).strftime('%Y-%m-01')
        self.utils.open_factura(factura_ids[0])  # todo , date

        # TODO asserts
        # print fact_obj.read(cursor, uid, factura_ids[0], [])

        wiz_export_f1_obj = self.openerp.pool.get('wizard.export.xmls')

        ctx = {
            'active_id': factura_ids[0],
            'active_ids': [factura_ids],
            'factures': '1'
        }

        wiz_f1_id = wiz_export_f1_obj.create(
            cursor, uid,
            {'mark_as_exported': True, 'force_mark_exported': False},
            context=ctx
        )

        wiz_f1 = wiz_export_f1_obj.browse(cursor, uid, wiz_f1_id, context=ctx)

        wiz_f1.generar_xmls(context=ctx)

        exported = fact_obj.read(
            cursor, uid, factura_ids[0], ['f1_exported']
        )['f1_exported']

        self.assertTrue(exported)

        generated_f1 = b64decode(wiz_f1.read(['zipcontent'])[0]['zipcontent'])

        # print generated_f1
        # TODO assert F1

        expected_f1 = get_module_resource(
            'giscedata_sup_territorials_2013_tec271_distri',
            'tests', 'fixtures',
            'expected_f1_dos.xml'
        )

        with open(expected_f1, 'r') as f:
            expected_f1 = f.read()
            f.close()

        self.utils.assertXmlEqual(generated_f1, expected_f1)

    def test_import_two_lines_consum_cups_same_periods(self):
        pass