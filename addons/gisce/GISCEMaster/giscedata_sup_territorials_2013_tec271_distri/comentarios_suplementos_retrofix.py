# -*- encoding: utf-8 -*-
from tools.translate import _
from retrofix import record, exception
from retrofix.fields import *
import pandas as pd
import numpy as np
from copy import deepcopy
from tqdm import tqdm
from json import dumps, loads
from datetime import datetime


class DateNone(Date):
    def __init__(self, pattern):
        Date.__init__(self, pattern)

    def set_from_file(self, value):
        if value == '0' * len(value) or value == ' ' * len(value):
            return
        try:
            return datetime.strptime(value, self._pattern)
        except ValueError:
            raise exception.RetrofixException(
                'Invalid date value "%s" does not '
                'match pattern "%s" in field "%s"' % (
                    value, self._pattern, self._name)
            )


class SuplementosTerritorialesComments(object):
    template_json_comentari = {
        'energia_comentari': False,
        'comentari_liquidacio': False,
        'liquidacio': False,
        'termes_totals': False,
        'import_total': False
    }
    descr_calcul2 = (
        # Información Texto libre
        # Número de Fracció (2) 1-12
        "{periodes_potencia}"  # 6 Campos 14 Caracteres (P W 14)
        "{potencia_canvi1}"  # 6 Campos 14 Caracteres (P W 14)
        "{potencia_canvi2}"  # 6 Campos 14 Caracteres (P W 14)
        "{data_canvi1}"  # 1 Campo 10 Caracteres (YYYY-MM-DD)
        "{data_canvi2}"  # 1 Campo 10 Caracteres (YYYY-MM-DD)
        "{periodes_energia}"  # 6 Campos 14 Caracteres (E kWh +11:2)
        #    Energia TOTAL (sumada) de cada periode
    )

    consum_buit = {
        'periode': (False, False),
        'tarifa': False,
        'potencia': {
            'consum': {
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            'notes': [
                {
                    'p1': 0,
                    'p2': 0,
                    'p3': 0,
                    'p4': 0,
                    'p5': 0,
                    'p6': 0,
                },
                {
                    'p1': 0,
                    'p2': 0,
                    'p3': 0,
                    'p4': 0,
                    'p5': 0,
                    'p6': 0,
                },
                {
                    'p1': 0,
                    'p2': 0,
                    'p3': 0,
                    'p4': 0,
                    'p5': 0,
                    'p6': 0,
                }
            ],
            'total': 0
        },
        'energia': {
            'consum': {
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            'notes': {
                'p1': 0,
                'p2': 0,
                'p3': 0,
                'p4': 0,
                'p5': 0,
                'p6': 0,
            },
            'total': 0
        },
    }

    COMMENTS = {
        (1, 4, 'extension_refacturacion', Char),  # Siempre 0000
        (5, 2, 'numero_faccion', Char),  # Fraccion terminos
        # Potencias contratadas P1, P2, P3, P4, P5, P6 en W
        # comienzo del periodo del año 2013
        (7, 14, 'potencia_contratada_1', Char),
        (21, 14, 'potencia_contratada_2', Char),
        (35, 14, 'potencia_contratada_3', Char),
        (49, 14, 'potencia_contratada_4', Char),
        (63, 14, 'potencia_contratada_5', Char),
        (77, 14, 'potencia_contratada_6', Char),
        # Potencias contratadas P1, P2, P3, P4, P5, P6 en W
        # primer cambio de potencia si se ha realizado
        (91, 14, 'potencia_contratada_primer_cambio_1', Char),
        (105, 14, 'potencia_contratada_primer_cambio_2', Char),
        (119, 14, 'potencia_contratada_primer_cambio_3', Char),
        (133, 14, 'potencia_contratada_primer_cambio_4', Char),
        (147, 14, 'potencia_contratada_primer_cambio_5', Char),
        (161, 14, 'potencia_contratada_primer_cambio_6', Char),
        # Potencias contratadas P1, P2, P3, P4, P5, P6 en W
        # segundo cambio de potencia si se ha realizado
        (175, 14, 'potencia_contratada_segundo_cambio_1', Char),
        (189, 14, 'potencia_contratada_segundo_cambio_2', Char),
        (203, 14, 'potencia_contratada_segundo_cambio_3', Char),
        (217, 14, 'potencia_contratada_segundo_cambio_4', Char),
        (231, 14, 'potencia_contratada_segundo_cambio_5', Char),
        (245, 14, 'potencia_contratada_segundo_cambio_6', Char),

        (259, 10, 'fecha_primer_cambio', DateNone('%Y-%m-%d')),
        (269, 10, 'fecha_segundo_cambio', DateNone('%Y-%m-%d')),

        # Energia activa 6 periodos 11 enteros 2 decimales en kWh
        (279, 14, 'energia_activa_1', Char),
        (293, 14, 'energia_activa_2', Char),
        (307, 14, 'energia_activa_3', Char),
        (321, 14, 'energia_activa_4', Char),
        (335, 14, 'energia_activa_5', Char),
        (349, 14, 'energia_activa_6', Char),
    }

    def __init__(self, pool, cursor, uid, context=None):
        self.pool = pool
        self.cursor = cursor
        self.uid = uid
        self.errores = {
            'to_revise': {
                'multiple_pot_change': [],  # List for more than 3 pot change
                'multiple_pot_same_period': [],  # List with different pots for same Px and Invoice
                'pot_changed': []  # List of cups with pot changes
            },
            'errors': []
        }
        self.context = {} if context is None else context

    def generate_comments_from_complete_dataframe(self, complete_df, fecha_inicio_periodo, fecha_final_periodo, filter_funct=None):
        cursor = self.cursor
        uid = self.uid
        context = self.context

        cups_obj = self.pool.get('giscedata.cups.ps')
        pol_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        lect_potencia_obj = self.pool.get(
            'giscedata.facturacio.lectures.potencia'
        )

        complete_df[u'Notas'] = ''

        cups_names_unique = complete_df[u'CUPS'].unique().tolist()

        cups_ids = cups_obj.search(
            cursor, uid,
            [('name', 'in', cups_names_unique)],
            context=context
        )

        ctx = context.copy()
        ctx.update({'active_test': False})

        facts = fact_obj.search(
            cursor, uid,
            [
                ('cups_id', 'in', cups_ids),
                ('data_inici', '<=', fecha_final_periodo),
                ('data_final', '>=', fecha_inicio_periodo),
            ],
            context=ctx, order='cups_id asc, data_inici asc'

        )

        if filter_funct:
            facts = filter_funct(facts)

        fact_data = fact_obj.read(cursor, uid, facts,
                                  ['cups_id', 'data_inici', 'data_final',
                                   'lectures_potencia_ids', 'tarifa_acces_id'], context=ctx)

        facts_df = pd.DataFrame(data=fact_data)

        for cup in tqdm(facts_df[u'cups_id'].unique().tolist(), desc="Generant notas per cups"):
            list_dicts_periodes = {
                'P1': [],
                'P2': [],
                'P3': [],
                'P4': [],
                'P5': [],
                'P6': [],
            }
            list_facts_pots = []
            facts_from_cups = facts_df.loc[facts_df[u'cups_id'] == cup].sort_values(by=['data_inici'], ascending=True)

            for index, fact in facts_from_cups.iterrows():

                lects_info = lect_potencia_obj.read(
                    cursor, uid,
                    fact[u'lectures_potencia_ids'],
                    ['name', 'pot_contract'],
                    context=context
                )
                aux_dict = deepcopy(list_dicts_periodes)
                aux_dict['data_inici'] = fact['data_inici']
                for lect in lects_info:
                    if not aux_dict[lect['name'].upper()] or (aux_dict[lect['name'].upper()] and aux_dict[lect['name'].upper()][-1] != lect['pot_contract']):
                        aux_dict[lect['name'].upper()].append(lect['pot_contract'])
                        if len(aux_dict[lect['name'].upper()]) > 1:
                            # ERROR: Esto es un caso extraño, en el que
                            # hay distintas potencias contratadas para el
                            # mismo periodo y factura
                            self.errores['to_revise']['multiple_pot_same_period'].append(
                                'Revisar potencia contratada '
                                'del periodo {} de la '
                                'factura id: {} '
                                'Fecha inicio: {} CUPS: {}'.format(
                                    lect['name'].upper(),
                                    fact['id'],
                                    fact['data_inici'],
                                    fact['cups_id'][1]
                                )
                            )
                if fact[u'tarifa_acces_id'][1] in ('3.0A', '3.1A'):
                    if not aux_dict['P4']:
                        aux_dict['P4'] = [0]
                    if not aux_dict['P5']:
                        aux_dict['P5'] = [0]
                    if not aux_dict['P6']:
                        aux_dict['P6'] = [0]
                    aux_dict['P1'] = [max(aux_dict['P1'][0], aux_dict['P4'][0])]
                    aux_dict['P2'] = [max(aux_dict['P2'][0], aux_dict['P5'][0])]
                    aux_dict['P3'] = [max(aux_dict['P3'][0], aux_dict['P6'][0])]
                    aux_dict['P4'] = []
                    aux_dict['P5'] = []
                    aux_dict['P6'] = []
                list_facts_pots.append(aux_dict)

            lec_ant = False
            potencias = []
            for pot_fact in list_facts_pots:
                if lec_ant:
                    lect_ant_copy = deepcopy(lec_ant)
                    current_lect_copy = deepcopy(pot_fact)
                    lect_ant_copy.pop('data_inici')
                    current_lect_copy.pop('data_inici')
                    if lect_ant_copy != current_lect_copy:
                        self.errores['to_revise']['pot_changed'].append(cup[1])
                        potencias.append(pot_fact)
                else:
                    potencias.append(pot_fact)
                lec_ant = pot_fact

            if len(potencias) > 3:
                self.errores['to_revise']['multiple_pot_change'].append(
                    'Más de 3 cambios de '
                    'potencia para el cups {}'.format(cup[1])
                )

            # Cambios de potencia
            n_cambios_pot = len(potencias)
            periodes_potencia = ['{}{}{}{}{}{}', '{}{}{}{}{}{}', '{}{}{}{}{}{}']

            for canvi_i in range(0, n_cambios_pot):
                periodes_potencia[canvi_i] = periodes_potencia[canvi_i].format(
                    '{:14.0f}'.format(potencias[canvi_i]['P1'][0]*1000).replace(' ', '0'),
                    (
                        '{:14.0f}'.format(potencias[canvi_i]['P2'][0]*1000).replace(' ', '0')
                    ) if potencias[canvi_i]['P2'] else (
                        ''.join([' ' for i in range(0, 14)])
                    ),
                    (
                        '{:14.0f}'.format(potencias[canvi_i]['P3'][0]*1000).replace(' ', '0')
                    ) if potencias[canvi_i]['P3'] else (
                        ''.join([' ' for i in range(0, 14)])
                    ),
                    (
                        '{:14.0f}'.format(potencias[canvi_i]['P4'][0]*1000).replace(' ', '0')
                    ) if potencias[canvi_i]['P4'] else (
                        ''.join([' ' for i in range(0, 14)])
                    ),
                    (
                        '{:14.0f}'.format(potencias[canvi_i]['P5'][0]*1000).replace(' ', '0')
                    ) if potencias[canvi_i]['P5'] else (
                        ''.join([' ' for i in range(0, 14)])
                    ),
                    (
                        '{:14.0f}'.format(potencias[canvi_i]['P6'][0]*1000).replace(' ', '0')
                    ) if potencias[canvi_i]['P6'] else (
                        ''.join([' ' for i in range(0, 14)])
                    ),
                )
            # Ahora ya tenemos los comentarios para los cambios de potencia
            periodes_potencia = [pp if pp != '{}{}{}{}{}{}' else (' ' * 14 * 6) for pp in periodes_potencia]

            fechas_cambios = [' ' * 10] * 2
            if n_cambios_pot > 1:
                fechas_cambios[0] = potencias[1]['data_inici']
                if n_cambios_pot == 3:
                    fechas_cambios[1] = potencias[2]['data_inici']

            energias_activas = [' ' * 14] * 6

            current_cups_name = cup[1]

            # Get all lines in excel file referent to current cups to
            # calculate energy
            lines_cups_complete_df = complete_df.loc[complete_df[u'CUPS'] == current_cups_name]

            energia_activa_cups_sum = lines_cups_complete_df[
                [
                    u'ValorEnergiaActiva_P1 (kWh)',
                    u'ValorEnergiaActiva_P2 (kWh)',
                    u'ValorEnergiaActiva_P3 (kWh)',
                    u'ValorEnergiaActiva_P4 (kWh)',
                    u'ValorEnergiaActiva_P5 (kWh)',
                    u'ValorEnergiaActiva_P6 (kWh)',
                ]
            ].astype('float', copy=False).agg(np.sum)

            for period in range(1, 7):
                key_word = u'ValorEnergiaActiva_P%s (kWh)' % period
                if energia_activa_cups_sum[key_word]:
                    str_periode = '{:+15.2f}'.format(energia_activa_cups_sum[key_word])
                    n_zeros = str_periode.count(' ')
                    str_periode = str_periode.replace(' ', '')
                    str_periode = str_periode.replace('+', '+{}'.format(
                        '0' * n_zeros))
                    str_periode = str_periode.replace('-', '-{}'.format(
                        '0' * n_zeros))
                    str_periode = str_periode.replace('.', '')

                    energias_activas[period-1] = str_periode

            full_comment_cups = '{}{}{}'.format(
                ''.join(periodes_potencia),  # Cambios de potencia
                ''.join(fechas_cambios),  # Fechas de cambio de potencia
                ''.join(energias_activas)  # Suma de las energias activas
            )

            # Write comment in column Notas
            complete_df.loc[
                complete_df.CUPS == current_cups_name, u'Notas'
            ] = full_comment_cups

        return complete_df

    def comments_to_dict(self, comments):
        recor = record.Record.extract(comments, self.COMMENTS)

        return recor

    @staticmethod
    def generate_json_from_imported_note(nota, total_agrupat, termes, grouped_cups_df):
        # 362 is the full_size - 6 (without 6 first chars)
        # 272 is the full_size - 6 (without 6 first chars) - (6*14 energias)
        # grouped_cups_df is grouped values for cups and notes to
        # generate energia
        if len(nota) == 272:
            nota = SuplementosTerritorialesComments(None, None, None, None).merge_energia_in_comment(
                nota, grouped_cups_df
            )

        if len(nota) == 356:
            record_nota = SuplementosTerritorialesComments(
                None, None, None, None
            ).comments_to_dict('999999' + nota)

            potencies = ''
            potencies_primer_canvi = ''
            potencies_segon_canvi = ''
            energies = ''
            for p in range(1, 7):
                potencies = '{}{}'.format(
                    potencies,
                    record_nota.get_for_file('potencia_contratada_%s' % p)
                )
                potencies_primer_canvi = '{}{}'.format(
                    potencies_primer_canvi,
                    record_nota.get_for_file('potencia_contratada_primer_cambio_%s' % p)
                )
                potencies_segon_canvi = '{}{}'.format(
                    potencies_segon_canvi,
                    record_nota.get_for_file('potencia_contratada_segundo_cambio_%s' % p)
                )

                energies = '{}{}'.format(
                    energies,
                    record_nota.get_for_file('energia_activa_%s' % p)
                )

            nota_json = '<JSON>%s</JSON>'
            json_structure_dict_notas = {
                'comentari_original': nota,
                'comentari_liquidacio': '',
                'energia_comentari': energies,
                'potencia_comentari': potencies,
                'potencies_primer_canvi': potencies_primer_canvi,
                'potencies_segon_canvi': potencies_segon_canvi,
                'liquidacio': False,
                'termes_totals': termes,
                'import_total': round(total_agrupat, 2)
            }
            str_json = dumps(json_structure_dict_notas)

            nota_json = nota_json % str_json
        else:
            nota_json = 'Tamaño nota incorrecto {} != {}'. format(len(nota), 356)

        return nota_json

    @staticmethod
    def get_last_sended_note_from_extraline_json_note(full_note):
        # Expect full_note to be any string but only will parse and return
        # something if contains JSON brackets

        start = '<JSON>'
        end = '</JSON>'
        # This finds substring between JSON brackets
        json_parsed_from_note_str = (
            full_note[full_note.find(start) + len(start):full_note.rfind(end)]
        )

        if not json_parsed_from_note_str:
            return False

        # Convert string json format to dict
        json_parsed_from_note = loads(
            json_parsed_from_note_str
        )

        return json_parsed_from_note[u'comentari_liquidacio']

    @staticmethod
    def get_full_json_from_extraline_json_note(full_note):
        '''
        # Expect full_note to be any string but only will parse and return
        # something if contains JSON brackets
        :param full_note: str
        :return: dict with JSON brackets contenent
        '''

        start = '<JSON>'
        end = '</JSON>'
        # This finds substring between JSON brackets
        json_parsed_from_note_str = (
            full_note[full_note.find(start) + len(start):full_note.rfind(end)]
        )

        if not json_parsed_from_note_str:
            return False

        # Convert string json format to dict
        json_parsed_from_note = loads(
            json_parsed_from_note_str
        )

        return json_parsed_from_note

    @staticmethod
    def get_last_term_from_extraline_json_note(last_note):
        # Last note is full size comment 362 from json on extra lines
        # need to pas comentari_liquidacio
        if not isinstance(last_note, (basestring, )):
            return False

        if not last_note or len(last_note) != 362:
            return False

        record_nota = SuplementosTerritorialesComments(
            None, None, None, None
        ).comments_to_dict(last_note)

        last_term_invoiced = record_nota.get_for_file('numero_faccion')

        return int(last_term_invoiced)

    @staticmethod
    def set_liquidacio_on_json_of_notes(full_note, last_term=False, ultim_terme_facturat=False):
        dict_json_note = SuplementosTerritorialesComments(None, None, None, None).\
            get_full_json_from_extraline_json_note(full_note)

        dict_json_note['liquidacio'] = True

        term_comment = last_term if last_term else dict_json_note['termes_totals']

        dict_json_note['comentari_liquidacio'] = '0000{}{}'.format(
            term_comment, dict_json_note['comentari_original']
        )

        dict_json_note['terme_a_facturar'] = 1 \
            if not ultim_terme_facturat \
            else ultim_terme_facturat + 1

        new_str_json = dumps(dict_json_note)

        start = '<JSON>'
        end = '</JSON>'
        # This finds substring between JSON brackets
        json_parsed_from_note_str = (
            full_note[full_note.find(start) + len(start):full_note.rfind(end)]
        )

        updated_nota = full_note.replace(json_parsed_from_note_str, new_str_json)

        return updated_nota

    @staticmethod
    def merge_energia_in_comment(nota, grouped_cups_df):
        energias_activas = [' ' * 14] * 6
        energia_activa_cups_sum = grouped_cups_df[
            [
                u'ValorEnergiaActiva_P1(kWh)',
                u'ValorEnergiaActiva_P2(kWh)',
                u'ValorEnergiaActiva_P3(kWh)',
                u'ValorEnergiaActiva_P4(kWh)',
                u'ValorEnergiaActiva_P5(kWh)',
                u'ValorEnergiaActiva_P6(kWh)',
            ]
        ].astype('float', copy=False)

        for period in range(1, 7):
            key_word = u'ValorEnergiaActiva_P%s(kWh)' % period
            if energia_activa_cups_sum[key_word]:
                str_periode = '{:+15.2f}'.format(
                    energia_activa_cups_sum[key_word])
                n_zeros = str_periode.count(' ')
                str_periode = str_periode.replace(' ', '')
                str_periode = str_periode.replace('+', '+{}'.format(
                    '0' * n_zeros))
                str_periode = str_periode.replace('-', '-{}'.format(
                    '0' * n_zeros))
                str_periode = str_periode.replace('.', '')

                energias_activas[period - 1] = str_periode

        energias_comment = ''.join(energias_activas)

        return '{}{}'.format(nota, energias_comment)
