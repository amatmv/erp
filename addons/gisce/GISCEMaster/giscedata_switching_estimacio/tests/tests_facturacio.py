# -*- coding: utf-8 -*-
from expects import expect
from expects import contain

from destral.transaction import Transaction

from giscedata_switching.tests.common_tests import TestSwitchingImport
from giscedata_polissa.tests import utils as utils_polissa


class TestsInvoiceValidation(TestSwitchingImport):
    def setUp(self):
        warn_obj = self.model(
            'giscedata.facturacio.validation.warning.template'
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        # We make sure that all warnings are active
        warn_ids = warn_obj.search(
            cursor, uid, [], context={'active_test': False}
        )
        warn_obj.write(cursor, uid, warn_ids, {'active': True})

    def tearDown(self):
        self.txn.stop()

    def model(self, model_name):
        return self.openerp.pool.get(model_name)

    def get_fixture(self, model, reference):
        imd_obj = self.model('ir.model.data')
        return imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user,
            model,
            reference
        )[1]

    def validation_warnings_message(self, factura_id):
        vali_obj = self.model('giscedata.facturacio.validation.validator')
        warn_obj = self.model('giscedata.facturacio.validation.warning')
        warning_ids = vali_obj.validate_invoice(
            self.txn.cursor, self.txn.user,
            factura_id
        )
        warning_vals = warn_obj.read(
            self.txn.cursor, self.txn.user,
            warning_ids,
            ['name','message']
        )
        return dict([(warn['name'], warn['message']) for warn in warning_vals])

    def validation_warnings(self, factura_id):
        return self.validation_warnings_message(factura_id).keys()

    def create_complete_case(self, polissa_id, case, step, status, substep=None):
        sw_obj = self.model('giscedata.switching')
        step_obj = self.model(
            'giscedata.switching.{}.{}'.format(case.lower(), step))

        cursor = self.txn.cursor
        uid = self.txn.user

        step_id = self.create_case_and_step(
            cursor, uid, polissa_id, case, step)
        step_d = step_obj.browse(cursor, uid, step_id)

        case_id = step_d.sw_id.id

        sw_obj.write(cursor, uid, step_d.sw_id.id, {'state': status})

        if substep:
            substep_obj = self.model('giscedata.subtipus.reclamacio')
            substep_ids = substep_obj.search(
                cursor, uid, [('name','=',substep)])
            if substep_ids:
                step_obj.write(cursor, uid, step_id, {
                    'subtipus_id': substep_ids[0],
                    })

        # refresh after modifications
        step_d = step_obj.browse(cursor, uid, step_id)
        case_d = sw_obj.browse(cursor, uid, case_id)
        return (case_d,step_d)

    def test_create_complete_case__C101open(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C1', '01', 'open')

        self.assertEqual(r1.proces_id.name, 'C1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__C101closed(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C1', '01', 'done')

        self.assertEqual(r1.proces_id.name, 'C1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'done')

    def test_create_complete_case__C201open(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C2', '01', 'open')

        self.assertEqual(r1.proces_id.name, 'C2')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__C201closed(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C2', '01', 'done')

        self.assertEqual(r1.proces_id.name, 'C2')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'done')

    def test_create_complete_case__M101open(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'M1', '01', 'open')

        self.assertEqual(r1.proces_id.name, 'M1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__M101closed(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'M1', '01', 'done')

        self.assertEqual(r1.proces_id.name, 'M1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'done')

    def test_create_complete_case__R101open009(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '009')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '009')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101open036(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '036')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '036')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101open002(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '002')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '002')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101done009(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'done', '009')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '009')
        self.assertEqual(r1.state, 'done')

    # scenario build up helpers
    def create_lect(self,pol_id,
        date,period,lect_type,value,observations,origen_comer_code,origin_code,pool):
        # avaliable codes for origin_comer_code
        # ['Q1','F1','MA','OV','AL','ES','AT','CC']

        # available codes for origin_code
        # ['10','20','30','40','50','99','40','40','60']

        pol_obj = self.model('giscedata.polissa')
        pol_val = pol_obj.read(
            self.txn.cursor, self.txn.user, pol_id, ['comptador', 'tarifa'])
        if not pol_val:
            return None

        meter_search = [
            ('polissa.id', '=', pol_id),
            ('name', '=', pol_val['comptador'])]
        meter_obj = self.model('giscedata.lectures.comptador')
        meter_ids = meter_obj.search(
            self.txn.cursor, self.txn.user, meter_search)
        if not meter_ids:
            return None

        period_search = [
            ('tarifa.id', '=', pol_val['tarifa'][0]),
            ('tipus', '=', 'te'),
            ('agrupat_amb', '=', False),
            ('name', '=', period)]
        period_obj = self.model('giscedata.polissa.tarifa.periodes')
        period_ids = period_obj.search(
            self.txn.cursor, self.txn.user, period_search)
        if not period_ids:
            return None

        origin_comer_obj = self.model('giscedata.lectures.origen_comer')
        origin_comer_ids = origin_comer_obj.search(
            self.txn.cursor, self.txn.user,
            [('codi', '=', origen_comer_code)])
        if not origin_comer_ids:
            return None

        origin_obj = self.model('giscedata.lectures.origen')
        origin_id = origin_obj.search(
            self.txn.cursor, self.txn.user,
            [('codi', '=', origin_code)])[0]

        vals = {'comptador': meter_ids[0],
            'name': date,
            'periode': period_ids[0],
            'tipus': lect_type,
            'lectura': value,
            'observacions': observations,
            'origen_comer_id': origin_comer_ids[0],
            'origen_id': origin_id,
            }
        if pool:
            pool_lect_obj = self.model('giscedata.lectures.lectura.pool')
            return pool_lect_obj.create(self.txn.cursor, self.txn.user, vals)
        else:
            fact_lect_obj = self.model('giscedata.lectures.lectura')
            return fact_lect_obj.create(self.txn.cursor, self.txn.user, vals)

    def create_pool_lecture(self, pol_id, period, date, value):
        return self.create_lect(
            pol_id,
            date,
            period,
            'A',
            value,
            'distri lecture',
            'F1',
            '10',
            True)

    def set_invoice_dates(self,invoice_name,start,end):
        fact_obj = self.model('giscedata.facturacio.factura')

        fact_id = self.get_fixture('giscedata_facturacio', invoice_name)
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]

        fact_obj.write(
            self.txn.cursor, self.txn.user, fact_id, {
                'data_inici': start,
                'data_final': end
            }
        )
        return polissa_id, fact_id

    def set_invoice_dates_wpool(self, invoice_name, start, end):
        polissa_id, fact_id = self.set_invoice_dates(invoice_name, start, end)
        self.create_pool_lecture(polissa_id,'P1',start,100)
        self.create_pool_lecture(polissa_id,'P1',end,200)
        return polissa_id, fact_id

    def set_invoiced_date(self, invoice_id, invoiced_date):
        fact_obj = self.model('giscedata.facturacio.factura')
        fact_obj.write(
            self.txn.cursor, self.txn.user, invoice_id, {
                'date_invoice': invoiced_date
            }
        )

    def set_polissa_values(self, polissa_id, fact_period, fact_mode, not_estimable, end_date, start_date=None):
        polissa_obj = self.model('giscedata.polissa')
        values = {}
        if fact_period is not None:
            values['facturacio'] = fact_period
        if fact_mode is not None:
            values['facturacio_potencia'] = fact_mode
        if not_estimable is not None:
            values['no_estimable'] = not_estimable
        if end_date is not None:
            values['data_baixa'] = end_date
        if start_date is not None:
            values['data_alta'] = start_date

        polissa_obj.write(self.txn.cursor, self.txn.user, polissa_id, values)

    def validate_contract(self, polissa_id):
        polissa_obj = self.model('giscedata.polissa')
        polissa_obj.send_signal(
            self.txn.cursor,
            self.txn.user,
            [polissa_id],
            ['validar', 'contracte']
        )

    def create_modcontr(self,polissa_id,pot,start,end):
        utils_polissa.crear_modcon(
            self.openerp.pool,
            self.txn.cursor,
            self.txn.user,
            polissa_id,
            {'potencia': pot},
            start,
            end)

    # the tests
    def test_check_missing_days_with_exceptions__dates_ok(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-26')
        self.set_polissa_values(
            polissa_id, 1, 'icp',False,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ok_two_months(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-02-19')
        self.set_polissa_values(
            polissa_id, 2, 'icp',False,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'icp',False,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_two_months(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-02-18')
        self.set_polissa_values(
            polissa_id, 2, 'icp',False,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_maximeter(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'max',False,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_not_estimable(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'icp',True,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_data_baixa(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'icp',False,'2016-01-12')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_open_m1(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'icp',False,False)
        self.create_complete_case(polissa_id, 'M1', '01', 'open')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_closed_m1(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'icp',False,False)
        self.create_complete_case(polissa_id, 'M1', '01', 'done')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_open_r1(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(
            polissa_id, 1, 'icp',False,False)
        self.create_complete_case(polissa_id, 'R1', '01', 'open')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_february(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_inside(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-05','2017-01-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_before(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-01-05','2017-01-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_at_start_ok(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-01','2017-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_at_start_manual_nok(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-01-31','2017-01-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_start(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-02','2017-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_at_end_ok(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-24','2017-01-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_later(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-25','2017-01-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_end(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-23','2017-01-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_pool_lecture(polissa_id,'P1','2015-05-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_two_months_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-02-18')
        self.set_polissa_values(polissa_id, 2, 'icp',False,False)
        self.create_pool_lecture(polissa_id,'P1','2015-12-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_closed_m1_CT(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_complete_case(polissa_id, 'M1', '01', 'done')
        self.create_pool_lecture(polissa_id,'P1','2015-12-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_open_r1_CT(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-01-01','2016-01-25')
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_complete_case(polissa_id, 'R1', '01', 'open')
        self.create_pool_lecture(polissa_id,'P1','2015-12-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_february_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_pool_lecture(polissa_id,'P1','2016-01-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_inside_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-05','2017-01-05')
        self.create_pool_lecture(polissa_id,'P1','2016-01-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_before_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-01-05','2017-01-05')
        self.create_pool_lecture(polissa_id,'P1','2015-12-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_at_start_manual_nok_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-01-31','2017-01-01')
        self.create_pool_lecture(polissa_id,'P1','2015-12-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_start_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-02','2017-02-01')
        self.create_pool_lecture(polissa_id,'P1','2016-01-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_later_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-25','2017-01-05')
        self.create_pool_lecture(polissa_id,'P1','2016-01-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_check_missing_days_with_exceptions__dates_ko_mod_contr_wrong_end_CT(self):
        polissa_id, fact_id = self.set_invoice_dates_wpool(
            'factura_0001','2016-02-01','2016-02-23')
        self.validate_contract(polissa_id)
        self.set_polissa_values(polissa_id, 1, 'icp',False,False)
        self.create_modcontr(polissa_id,6.0,'2016-02-23','2017-01-05')
        self.create_pool_lecture(polissa_id,'P1','2016-01-01',50)
        self.set_invoiced_date(fact_id,'2016-02-01')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F018'))

    def test_exceeding_days_validation_on_mensual(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-06')
        self.set_polissa_values(polissa_id, 1, 'icp', False, False, '2015-05-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F009'))

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F009'))

    def test_exceeding_days_validation_on_bimensual(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-11')
        self.set_polissa_values(polissa_id, 2, 'icp', False, False, '2015-05-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F009'))

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-10')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F009'))

    def test_exceeding_days_validation_maximeter_on_mensual(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-06')
        self.set_polissa_values(polissa_id, 1, 'max', False, False, '2015-05-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F009'))

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F009'))

    def test_exceeding_days_validation_maximeter_on_bimensual(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-11')
        self.set_polissa_values(polissa_id, 2, 'max', False, False, '2015-05-05')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).to(contain('F009'))

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-10')

        warnings = self.validation_warnings(fact_id)
        expect(warnings).not_to(contain('F009'))

    def validation_warning_exceding_days_extended(self, fact_id, inc_days_maximeter=0):
        validator_obj = self.model('giscedata.facturacio.validation.validator')
        fact_obj = self.model('giscedata.facturacio.factura')
        cursor = self.txn.cursor
        uid = self.txn.user
        return validator_obj.check_exceding_days(
            cursor, uid,
            fact_obj.browse(cursor, uid, fact_id),
            {
                "n_days_mensual": 5,
                "n_days_bimensual": 10,
                "inc_days_maximeter": inc_days_maximeter,
                "n_days_first_invoice": 12,
                "n_days_not_estimable": 14,
            }
        )

    def test_exceeding_days_validation_maximeter_on_mensual_inc5(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-11')
        self.set_polissa_values(polissa_id, 1, 'max',False, False, '2015-05-05')

        validation = self.validation_warning_exceding_days_extended(fact_id, 5)
        self.assertEqual(validation, {'expected_days': 30, 'margin': 10, 'actual_days': 41})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-10')

        validation = self.validation_warning_exceding_days_extended(fact_id, 5)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_maximeter_on_bimensual_inc5(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-16')
        self.set_polissa_values(polissa_id, 2, 'max', False, False, '2015-05-05')

        validation = self.validation_warning_exceding_days_extended(fact_id, 5)
        self.assertEqual(validation, {'expected_days': 59, 'margin': 15, 'actual_days': 75})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-15')

        validation = self.validation_warning_exceding_days_extended(fact_id, 5)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_mensual_first_invoice(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-13')
        self.set_polissa_values(polissa_id, 1, 'icp', False, False, '2016-01-01')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 30, 'margin': 12, 'actual_days': 43})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-12')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_bimensual_first_invoice(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-13')
        self.set_polissa_values(polissa_id, 2, 'icp', False, False, '2016-01-01')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 59, 'margin': 12, 'actual_days': 72})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-12')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_mensual_max_first_invoice(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-13')
        self.set_polissa_values(polissa_id, 1, 'max', False, False, '2016-01-01')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 30, 'margin': 12, 'actual_days': 43})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-12')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_bimensual_max_first_invoice(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-13')
        self.set_polissa_values(polissa_id, 2, 'max', False, False, '2016-01-01')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 59, 'margin': 12, 'actual_days': 72})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-12')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_mensual_not_estimable(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-15')
        self.set_polissa_values(polissa_id, 1, 'icp', True, False, '2015-06-06')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 30, 'margin': 14, 'actual_days': 45})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-14')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_bimensual_not_estimable(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-15')
        self.set_polissa_values(polissa_id, 2, 'icp', True, False, '2015-06-06')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 59, 'margin': 14, 'actual_days': 74})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-14')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_mensual_max_not_estimable(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-15')
        self.set_polissa_values(polissa_id, 1, 'max', True, False, '2015-06-06')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 30, 'margin': 14, 'actual_days': 45})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-02-14')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)

    def test_exceeding_days_validation_on_bimensual_max_not_estimable(self):
        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-15')
        self.set_polissa_values(polissa_id, 2, 'max', True, False, '2015-06-06')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, {'expected_days': 59, 'margin': 14, 'actual_days': 74})

        polissa_id, fact_id = self.set_invoice_dates(
            'factura_0001','2016-01-01','2016-03-14')

        validation = self.validation_warning_exceding_days_extended(fact_id)
        self.assertEqual(validation, None)
