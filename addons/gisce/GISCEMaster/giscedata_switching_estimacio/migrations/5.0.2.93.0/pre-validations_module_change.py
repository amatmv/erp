# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Updating module\'s validation models that are defined 
        in giscedata_swithing to giscedata_switching_estimacio.
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module = 'giscedata_switching_estimacio'
        WHERE module = 'giscedata_switching'
        AND name IN (
            'warning_F018'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up
