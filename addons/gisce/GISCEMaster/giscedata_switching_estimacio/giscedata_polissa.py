# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from tools.translate import _
from tools import config
from tools.misc import cache


class GiscedataPolissa(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def es_estimable(self, cursor, uid, polissa_id, data=None, context=None):
        """ Decidim si una pólissa es pot estimar en funció de diferents
            criteris, p.e. tarifa, facturació potència, etc..."""
        res = super(GiscedataPolissa, self).es_estimable(cursor, uid,
                                                         polissa_id, data,
                                                         context)
        if res:
            # Mirem si té un cas obert
            res = not self.te_cas_obert(cursor, uid, polissa_id)

        return res

    def te_cn06_obert(self, cursor, uid, polissa_id):
        """ Mira si tenim un cas amb un pas cn06 relacionat amb aquesta
            pòlissa """
        res = False
        sw_obj = self.pool.get('giscedata.switching')
        step_obj = self.pool.get('giscedata.switching.step')

        sw_ids = sw_obj.search(cursor, uid,
                               [('cups_polissa_id', '=', polissa_id)])

        for sw_vals in sw_obj.read(cursor, uid, sw_ids, ['step_id', 'state']):
            if not sw_vals['step_id']:
                continue
            model = step_obj.get_step_model(cursor, uid, sw_vals['step_id'][0])
            cn_06s = ['giscedata.switching.c1.06', 'giscedata.switching.c2.06']
            #Tots els casos no tancats que tenen un Cn-06
            if model in cn_06s and sw_vals['state'] not in ['done']:
                res = True
                break

        return res

    def te_cas_obert(self, cursor, uid, polissa_id):
        """ Mira si tenim un cas relacionat amb aquesta pòlissa obert"""
        res = False
        sw_obj = self.pool.get('giscedata.switching')

        sw_ids = sw_obj.search(cursor, uid,
                               [('cups_polissa_id', '=', polissa_id)])

        for sw_vals in sw_obj.read(cursor, uid, sw_ids, ['state']):
            if sw_vals['state'] not in ['done', 'cancel']:
                res = True
                break

        return res

GiscedataPolissa()
