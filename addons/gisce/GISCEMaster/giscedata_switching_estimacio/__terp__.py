# -*- coding: utf-8 -*-
{
    "name": "Switching per estimació de lectures",
    "description": """
    Mòdul que afegeix funcionalitats a la estimació de lectures relacionades amb 
    el switching (o gestió ATR):
     * Marca com a no estimables les pòlisses que tenen un Cn-06 actiu
    """,
    "version": "0-dev",
    "author": "Gisce-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_switching_comer",
        "giscedata_lectures_estimacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": ['giscedata_facturacio_validation_data.xml'],
    "active": False,
    "installable": True
}
