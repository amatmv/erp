# -*- coding: utf-8 -*-
from osv import osv
from datetime import datetime, timedelta


class GiscedataFacturacioValidationValidator(osv.osv):
    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'
    _desc = 'Validador de factures amb casos ATR'

    def check_missing_days_with_exceptions(self, cursor, uid, fact, parameters):
        result = self.check_missing_days(cursor, uid, fact, parameters)
        if not result:
            return None

        if fact.polissa_id.facturacio_potencia != 'icp':
            return None

        if fact.polissa_id.no_estimable is True:
            return None

        if fact.polissa_id.data_baixa is not False:
            return None

        exception_cases = eval(parameters['exception_cases'])
        sw_obj = self.pool.get('giscedata.switching')
        sw_ids = sw_obj.search(
            cursor, uid, [
                ('cups_polissa_id', '=', fact.polissa_id.id),
                ('proces_id', 'in', exception_cases),
                ('state', '=', 'open'),
            ]
        )
        if sw_ids:
            return None

        def date_plus(string, ndays):
            return str(datetime.strptime(string, "%Y-%m-%d").date() + timedelta(days=ndays))

        data_inici_mod = fact.polissa_id.modcontractual_activa.data_inici
        if data_inici_mod:
            if data_inici_mod == fact.data_inici:
                return None
            if data_inici_mod == date_plus(fact.data_final, 1):
                return None

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fact_ids = fact_obj.search(
            cursor, uid, [
                ('polissa_id', '=', fact.polissa_id.id),
                ('type', 'in', ('out_refund', 'out_invoice')),
            ],order='date_invoice DESC')

        first_invoice = fact_ids and fact_ids[-1] == fact.id
        if first_invoice:
            pool_obj = self.pool.get('giscedata.lectures.lectura.pool')
            meters = [meter.name for meter in fact.polissa_id.comptadors]
            pool_ids = pool_obj.search(
                cursor, uid, [
                    ('comptador', 'in', meters),
                    ('name', '<', fact.data_inici),
                ])
            has_prev_pool_readings = len(pool_ids) > 0
            if has_prev_pool_readings:
                return None

        return result

    def check_exceding_days(self, cursor, uid, fact, parameters):
        def evaluate(param_mesual, param_bimensual, param_inc_maximeter):
            params = {
                "n_days_mensual": parameters.get(param_mesual, 5),
                "n_days_bimensual": parameters.get(param_bimensual, 10),
                "inc_days_maximeter": parameters.get(param_inc_maximeter, 0),
            }
            return super(
                    GiscedataFacturacioValidationValidator,
                    self
                ).check_exceding_days(cursor, uid, fact, params)

        result = evaluate("n_days_mensual", "n_days_bimensual", "inc_days_maximeter")
        if not result:
            return None

        if fact.data_inici == fact.polissa_id.data_alta:
            result = evaluate("n_days_first_invoice", "n_days_first_invoice", None)
            if not result:
                return None

        if fact.polissa_id.no_estimable is True:
            result = evaluate("n_days_not_estimable", "n_days_not_estimable", None)
            if not result:
                return None

        return result


GiscedataFacturacioValidationValidator()
