# -*- coding: utf-8 -*-
from osv import osv, fields, orm
import netsvc

class GiscedataSwitchingHelpers(osv.osv):

    _name = 'giscedata.switching.helpers'
    _inherit = 'giscedata.switching.helpers'

    def activa_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        ''' Calculem el consum anual al activar la pólissa '''
        res = super(GiscedataSwitchingHelpers,
                    self).activa_polissa_from_cn(cursor, uid, sw_id, context)

        sw_obj = self.pool.get('giscedata.switching')
        polissa_obj = self.pool.get('giscedata.polissa')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)

        polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

        # Consum anual
        polissa.cups.omple_consum_anual()

        return res

GiscedataSwitchingHelpers()