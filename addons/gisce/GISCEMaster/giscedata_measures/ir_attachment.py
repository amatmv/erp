# -*- coding: utf-8 -*-
from osv import osv


class IrAttachment(osv.osv):
    _name = 'ir.attachment'
    _inherit = 'ir.attachment'
    _order = 'create_date desc'


IrAttachment()
