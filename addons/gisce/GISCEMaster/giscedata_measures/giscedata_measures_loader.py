# -*- coding: utf-8 -*-
from osv import osv, fields
from base64 import b64encode, b64decode
import StringIO
from tools.translate import _
from zipfile import ZipFile, BadZipfile
import bz2

class GiscedataMeasuresLoader(osv.osv):

    _name = 'giscedata.measures.loader'

    _columns = {}

    @staticmethod
    def decompress(file_, type_):
        if type_ == 'zip':
            data = file_
            file_handle = StringIO.StringIO(data)
            try:
                return ZipFile(file_handle, "r")
            except BadZipfile:
                raise osv.except_osv(_('Error'), _(u'El fitxer ha de ser un .zip'))
        elif type_ == 'bz2':
            return bz2.decompress(file_)
        else:
            return file_

    def parse_file(self, cursor, uid, filename, content, context=None):
        compression_type = self.compression_type(filename)
        content = self.decompress(content, compression_type)
        if isinstance(content, ZipFile):
            zip_content = content
            for _file in zip_content.infolist():
                content = StringIO.StringIO(zip_content.read(_file.filename))
                self.load_file(cursor, uid, [], _file.filename, content, context=context)
        else:
            content = StringIO.StringIO(content)
            self.load_file(cursor, uid, [], filename, content, context=context)

    def load_file(self, cursor, uid, ids, file_type, data, context=None):
        raise NotImplementedError()

    @staticmethod
    def compression_type(filename):
        magic_dict = {
            "\x1f\x8b\x08": 'gz',
            "\x42\x5a\x68": 'bz2',
            "\x50\x4b\x03\x04": 'zip'
        }
        max_len = max(len(x) for x in magic_dict)
        try:
            with open(filename) as f:
                file_start = f.read(max_len)
            for magic, filetype in magic_dict.items():
                if file_start.startswith(magic):
                    return filetype
            return 'raw'
        except:
            if '.bz2' in filename:
                return 'bz2'
            elif '.zip' in filename:
                return 'zip'
            elif '.gz' in filename:
                return 'gz'
            else:
                return 'raw'


GiscedataMeasuresLoader()
