# -*- coding: utf-8 -*-
from osv import osv, fields


MAGNITUDS = {'AE': 'A', 'R1': 'R'}

TIPOS = [('1', 'Tipo 1'),
         ('2', 'Tipo 2'),
         ('3', 'Tipo 3'),
         ('4', 'Tipo 4'),
         ('5', 'Tipo 5')]

MEASURE_TYPE_FROM_PROFILE_TO_REPORT = {
    'AE': 'active_input',
    'R1': 'reactive_quadrant1',
    'R4': 'reactive_quadrant4'
}
MEASURE_TYPE_FROM_TG_PROFILE_TO_REPORT = {
    'ai': 'active_input',
    'r1': 'reactive_quadrant1',
    'r4': 'reactive_quadrant4'
}

INDICADOR_MESURA = {
    '31': 'A',
    '30': 'B',
    '21': 'B',
    '2A': 'B'
}


class GiscedataProfilesAggregations(osv.osv):

    _name = 'giscedata.profiles.aggregations'
    _rec_name = 'comercialitzadora'

    def create(self, cursor, uid, vals, context=None):
        """ Preventive delete so as not to duplicate aggregations,
        always update aggregations"""
        search_params = [
            ('distribuidora', '=', vals['distribuidora']),
            ('comercialitzadora', '=', vals['comercialitzadora']),
            ('agree_tensio', '=', vals['agree_tensio']),
            ('agree_tarifa', '=', vals['agree_tarifa']),
            ('agree_dh', '=', vals['agree_dh']),
            ('agree_tipo', '=', vals['agree_tipo']),
            ('provincia', '=', vals['provincia']),
            ('magnitud', '=', vals['magnitud']),
            ('lot_id', '=', vals['lot_id']),
        ]
        # Preventive delete, save old data
        # Update different and not False fields
        exist_id = self.search(cursor, uid, search_params)
        if exist_id:
            if isinstance(exist_id, list):
                exist_id = exist_id[0]
            agg = self.read(cursor, uid, exist_id)
            to_update = {}
            for k, v in vals.items():
                if vals[k] and vals[k] != agg[k]:
                    to_update[k] = vals[k]
            self.write(cursor, uid, exist_id, to_update)

        else:
            vals['consum'] = vals.get('consum', 0)
            return super(GiscedataProfilesAggregations, self).create(
                cursor, uid, vals, context
            )

    def name_get(self, cursor, uid, ids, context=None):
        """
        Parse name_get aggregations
        :param ids: aggregation_ids
        :return: list LIKE [(id: vals)]
        """
        if context is None:
            context = {}

        if not len(ids):
            return []

        res = []
        fields_to_read = [
            'distribuidora', 'comercialitzadora', 'agree_tensio',
            'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia'
        ]
        for aggr in self.read(cursor, uid, ids, fields_to_read):
            vals = ';'.join([aggr[k] for k in fields_to_read])
            res.append((aggr['id'], vals))
        return res

    _columns = {
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora', size=4, required=True
        ),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'magnitud': fields.char('Magnitud', size=2, required=True),
        'consum': fields.integer('Consum', required=True),
        'data_inici': fields.date('Data inici (min)'),
        'data_final': fields.date('Data final (max)'),
        'data_inici_ag': fields.date('Data inici (aggr)'),
        'data_final_ag': fields.date('Data final (aggr)')
    }

    _defaults = {
        'consum': lambda *a: 0,
    }


GiscedataProfilesAggregations()
