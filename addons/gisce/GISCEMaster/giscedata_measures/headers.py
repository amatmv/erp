AGCLACUM_HEADER = [
    'distribuidora', 'comercialitzadora', 'agree_tensio',
    'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia', 'magnitud',
    'consum', 'n_hores'
]

AGCLACUM5_HEADER = [
    'distribuidora', 'comercialitzadora', 'agree_tensio',
    'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia', 'magnitud',
    'consum', 'consum_estimat', 'n_hores'
]
