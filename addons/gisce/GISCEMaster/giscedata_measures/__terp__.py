# -*- coding: utf-8 -*-
{
  "name": "",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Mesures REE
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      "crm",
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      "giscedata_measures_view.xml",
      "security/giscedata_measures_security.xml",
      "security/ir.model.access.csv",
  ],
  "active": False,
  "installable": True
}