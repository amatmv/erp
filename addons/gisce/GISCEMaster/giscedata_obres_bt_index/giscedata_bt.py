from base_index.base_index import BaseIndex


class GiscedataBtElement(BaseIndex):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'
    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataBtElement()
