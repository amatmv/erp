# -*- coding: utf-8 -*-
from osv import osv
from mako.lookup import TemplateLookup
from tools.config import config
from mako.template import Template


class PolissaReport(osv.osv):
    _auto = False
    _name = 'giscedata.polissa.report'

    def render_mako_code(self, path, variables):
        """
        Obtains the HTML code generated from a mako file.
        :param path: path to the mako file starting from the module. i.e:
        module_name/report/my.mako
        :type path: str
        :param variables: dictionary with the variables used in the mako file.
        :type variables: dict[str: V]
        :return: HTML code
        :rtype: str
        """
        addons_path = config['addons_path']
        mako_path = "{}/{}".format(addons_path, path)
        addons_lookup = TemplateLookup(
            directories=[addons_path], input_encoding='utf-8'
        )
        with open(mako_path, 'r') as content_file:
            content = content_file.read()

        body_mako_tpl = Template(
            content, input_encoding='utf-8', lookup=addons_lookup
        )
        html = body_mako_tpl.render(**variables)
        return html

    def representante(self, cursor, uid, pol_id, translate_call):
        return ''

    def forma_pagament(self, cursor, uid, polissa, translate_call):
        return ''


PolissaReport()
