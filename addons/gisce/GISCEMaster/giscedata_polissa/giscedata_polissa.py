# -*- coding: utf-8 -*-
"""Modul giscedata_polissa (Base)."""
from enerdata.contracts import get_tariff_by_code

from osv import osv, fields
from tools.translate import _
from tools import email_send, cache
from datetime import date, datetime, timedelta
import calendar
import netsvc
import time
import pooler
import agree
from osv.expression import OOQuery
from enerdata.contracts.normalized_power import NormalizedPower
from gestionatr.defs import TABLA_9, TABLA_113, TENEN_AUTOCONSUM

# Pylint Stuff
# pylint: disable-msg=E1101,W0613,R0913,R0201,W0141
# Same values of TABLA 109 from CNMC specs
TG_OPERATIVA = [('1', 'Operativa amb CCH'),
                ('2', 'No operativa'),
                ('3', 'Operativa Sense CCH')]

# Same values of TABLA 113 from CNMC specs
TIPO_AUTOCONSUMO = TABLA_113

CONTRACT_STATES = [
        ('esborrany', 'Esborrany'),
        ('validar', 'Validar'),
        ('pendent', 'Pendent'),
        ('activa', 'Activa'),
        ('cancelada', 'Cancel·lada'),
        ('contracte', 'Activació Contracte'),
        ('novapolissa', 'Creació nova pòlissa'),
        ('modcontractual', 'Modificació Contractual'),
        ('impagament', 'Impagament'),
        ('tall', 'Tall'),
        ('baixa', 'Baixa')
]

CONTRACT_LOGGABLE_STATES = [
    'esborrany',
    'validar',
    'pendent',
    'activa',
    'impagament',
    'modcontractual',
    'cancelada',
    'tall',
    'baixa'
]

DEFAULT_STATES_MODCON = {
    'esborrany': [
        ('readonly', False)
    ],
    'validar': [
        ('readonly', False),
        ('required', True)
    ],
    'modcontractual': [
        ('readonly', False),
        ('required', True)
    ]
}

CONTRACT_IGNORED_STATES = [
    'validar',
    'esborrany',
    'cancelada'
]

CONTRACT_TYPES = TABLA_9

SIPS_CESSION_SEL = [
    ('unactive', 'Desactivat'),
    ('requested', 'Sol·licitat'),
    ('active', 'Activat')
]


def get_nivell_agregacio(cursor, table, ids, field_names, arg, context=None):
    """Calcula el nivell d'agregació.
    """
    # Fucking OpenERP WTF uid is not passed?
    UID = 1
    _obj = pooler.get_pool(cursor.dbname).get(table.replace('_', '.'))
    res = {}
    for obj in _obj.browse(cursor, UID, ids):
        potencia = obj.potencia
        if table == 'giscedata_polissa_modcontractual' and obj.potencies_periode:
            pot_dict = _obj.get_potencies_dict(cursor, UID, obj.id)
            potencia = max(pot_dict.values())
        if table == 'giscedata_polissa' and len(obj.potencies_periode) > 0:
            potencia = max([x.potencia for x in obj.potencies_periode])
        tensio = obj.tensio
        tarifa_tipus = obj.tarifa and obj.tarifa.tipus or False
        num_periodes = obj.tarifa and obj.tarifa.get_num_periodes() or False
        res[obj.id] = {
            'agree_tipus': agree.agree_tipus(potencia),
            'agree_tensio': agree.agree_tensio(
                tarifa_tipus, tensio, obj.tarifa.name
            ),
            'agree_dh': agree.agree_dh(num_periodes),
            'agree_tarifa': agree.agree_tarifa(tarifa_tipus, num_periodes,
                                               potencia, tensio)
        }
    return res

class GiscedataPolissaTarifa(osv.osv):
    """Tarifa d'accés de les pòlisses."""
    _name = 'giscedata.polissa.tarifa'
    _description = "Tarifa d'acces del client"

    def get_tarifa_from_ocsum(self, cursor, uid, codi_ocsum, tarifa_lb=False,
                              context=None):
        """Retorna la tarifa a partir del codi ocsum"""
        search_vals = [('codi_ocsum', '=', codi_ocsum)]
        if codi_ocsum == '011':  # In tarif 3.1A we check if it is LB
            if tarifa_lb:
                search_vals.append(('mesura_ab', '=', 'B'))
            else:
                search_vals.append(('mesura_ab', '=', 'A'))
        tarifa_id = self.search(cursor, uid, search_vals)

        if not tarifa_id:
            return False

        if tarifa_id:
            tarifa_id = tarifa_id[0]

        return tarifa_id

    def get_num_periodes(self, cursor, uid, ids, tipus='te', context=None):
        """Retorna el número de periodes reals (no agrupats de la tarifa).
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        for tid in ids:
            search_params = [('tarifa.id', '=', tid),
                             ('tipus', '=', tipus)]
            if context.get('agrupar_periodes', True):
                search_params.extend([('agrupat_amb', '=', False)])
            return periodes_obj.search_count(cursor, uid, search_params,
                                             context=ctx)

    def get_periodes(self, cursor, uid, tarifa_id, tipus='te', context=None):
        """Retorna un diccionari amb Periode -> Id.
        """
        if not context:
            context = {}
        tipus_valids = ('tp', 'te', 'tr', 'ep')
        if tipus not in tipus_valids:
            txt = _(u'El tipus només pot ser: {0}').format(
                ','.join(tipus_valids)
            )
            raise osv.except_osv(_(u'Tipus no vàlid'), txt)
        if context is None:
            context = {}
        periodes = {}
        if isinstance(tarifa_id, list) or isinstance(tarifa_id, tuple):
            tarifa_id = tarifa_id[0]
        tarifa = self.browse(cursor, uid, tarifa_id, context)
        for periode in tarifa.periodes:
            if context.get('sense_agrupar') and periode.agrupat_amb:
                continue
            if periode.tipus != tipus:
                continue
            periodes[periode.name] = periode.id
        return periodes

    def get_periodes_producte(self, cursor, uid, tarifa_id, tipus,
                              context=None):
        """Retorna un diccionari amb Periode -> Id producte.
        """
        tipus_valids = ('tp', 'te', 'tr', 'ep')
        if tipus not in tipus_valids:
            raise osv.except_osv(_(u'Tipus no vàlid'), _(u'El tipus només pot '
                                            'ser: %s') % ','.join(tipus_valids))
        productes = {}
        if isinstance(tarifa_id, list) or isinstance(tarifa_id, tuple):
            tarifa_id = tarifa_id[0]
        tarifa = self.browse(cursor, uid, tarifa_id, context)
        for periode in tarifa.periodes:
            if periode.tipus == tipus:
                productes[periode.name] = periode.product_id.id
            elif tipus == 'tr' and periode.product_reactiva_id:
                productes[periode.name] = periode.product_reactiva_id.id
            elif tipus == 'ep' and periode.product_exces_pot_id:
                productes[periode.name] = periode.product_exces_pot_id.id
        return productes

    def get_cofs_20DHA(self, cursor, uid, tarifa_id, context=None):
        """Retorna els coficients que es faran servir per la 2.0DHA.
        """
        cofs = {}
        if not context and context.get('listprice_id', False):
            raise osv.except_osv('Error', _('El context ha de tenir la '
                                            'clau listprice_id indicant la'
                                            'llista de preu.'))
        if not context.get('date', False):
            context['date'] = time.strftime('%Y-%m-%d')
        model_data_obj = self.pool.get('ir.model.data')
        pricelist_obj = self.pool.get('product.pricelist')
        cofs_ids = {'P1': 'cof_a_20DHA_new', 'P2': 'cof_b_20DHA_new'}
        list_id = context.get('listprice_id')
        for periode, xml_id in cofs_ids.items():
            search_params = [
                ('module', '=', 'giscedata_polissa'),
                ('name', '=', xml_id)
            ]
            p_cof_ids = model_data_obj.search(cursor, uid, search_params,
                                              context=context)
            p_cof_id = model_data_obj.read(cursor, uid, p_cof_ids,
                                           ['res_id'])[0]
            cofs[periode] = pricelist_obj.price_get(cursor, uid, [list_id],
                                                    p_cof_id['res_id'], 1,
                                                    context=context)[list_id]
        return cofs

    def get_grouped_periods(self, cursor, uid, tarifa_id, context=None):
        if isinstance(tarifa_id, list) or isinstance(tarifa_id, tuple):
            tarifa_id = tarifa_id[0]
        tarifa = self.browse(cursor, uid, tarifa_id, context)
        grouped = {}
        for periode in tarifa.periodes:
            if periode.agrupat_amb:
                grouped[periode.name] = periode.agrupat_amb.name
        return grouped

    _columns = {
      'name': fields.char('Codi', size=10, required=True),
      'descripcio': fields.text('Descripcio'),
      'periodes': fields.one2many('giscedata.polissa.tarifa.periodes',
                                  'tarifa', 'Periodes'),
      'codi_ocsum': fields.char('Codi OCSUM', size=3, readonly=True),
      'pot_min': fields.float('Potència min.'),
      'pot_max': fields.float('Potència màx.'),
      'tipus': fields.selection([('AT', 'Alta tensió'),
                                 ('BT', 'Baixa Tensió')], 'Tipus de tarifa'),
      'autogen_periodes_pot': fields.boolean('Autogenerar períodes'),
      'cof': fields.selection([('cof_a', 'A'),
                               ('cof_b', 'B'),
                               ('cof_c', 'C'),
                               ('cof_d', 'D')], 'Coficient Perfilació'),
      'mesura_ab': fields.selection([('A', 'En alta'),
                                    ('B', 'En baixa')], 'Tipus de mesura',
                                   size=1),
      'active': fields.boolean('Activa'),
    }

    _defaults = {
        'autogen_periodes_pot': lambda *a: True,
        'active': lambda *a: 1,
    }
    
    _order = "name asc"

    _sql_constraints = [('name_uniq', 'unique (name)',
                         'Aquest codi de tarifa ja existeix.'),]

GiscedataPolissaTarifa()


class GiscedataPolissaTarifaPeriodes(osv.osv):
    """Periodes de les Tarifes."""
    _name = 'giscedata.polissa.tarifa.periodes'
    _inherits = {'product.product': 'product_id'}

    def name_get(self, cursor, uid, ids, context=None):
        """Retornem una combinació Periode (Nom Tarifa)."""
        if not len(ids):
            return []
        res = []
        for periode in self.browse(cursor, uid, ids):
            res.append((periode.id, '%s (%s)' % (periode.tarifa.name,
                                                 periode.name)))
        return res

    _columns = {
      'tipus': fields.selection([('te', 'Termes Energia'),
                                 ('tp', 'Termes Potència')], 'Tipus',
                                 required=True),
      'efecte_reactiva': fields.boolean('Efecte en la reactiva'),
      'tarifa': fields.many2one('giscedata.polissa.tarifa',
                                'Tarifa a la que pertany'),
      'product_id': fields.many2one('product.product', 'Producte',
                                    ondelete='restrict'),
      'product_reactiva_id': fields.many2one('product.product', 'Reactiva',
                                             ondelete='restrict'),
      'product_exces_pot_id': fields.many2one('product.product', 'Excés 6.X',
                                              ondelete='restrict'),
      'agrupat_amb': fields.many2one('giscedata.polissa.tarifa.periodes',
                                     'Agrupat amb'),
      'agrupats': fields.one2many('giscedata.polissa.tarifa.periodes',
                                  'agrupat_amb', 'Periodes'),
    }

    _defaults = {
      'type': lambda *a: 'service',
      'procure_method': lambda *a: 'make_to_stock',
      'efecte_reactiva': lambda *a: 1,
      'standard_price': lambda *a: 0.000000,
    }

GiscedataPolissaTarifaPeriodes()

class GiscedataPolissaNocutoff(osv.osv):
    """Subministre no tallable."""
    _name = 'giscedata.polissa.nocutoff'
    _description = "Opció subministrament no tallable per comer i distri."
    _rec_name = 'motiu'

    _columns = {
        'motiu': fields.text('Motiu'),
        'descripcio': fields.text('Descripció del Motiu'),
        'boe_check': fields.boolean('BOE', readonly=True),
    }
GiscedataPolissaNocutoff()

class GiscedataPolissa(osv.osv):
    """Pòlissa."""
    _name = "giscedata.polissa"
    _description = "Polissa d'un Client"
    
    _polissa_states_selection = CONTRACT_STATES

    def get_related_attachments_fields(self, cursor, uid):
        return ['titular', 'cups']

    @cache(timeout=5 * 60)
    def exact_search(self, cursor, uid, context=None):
        if context is None:
            context = {}
        exact = int(self.pool.get('res.config').get(
                cursor, uid, 'polissa_cerca_exacte', '1')
        )
        return exact

    def search(self, cr, user, args, offset=0, limit=None, order=None,
            context=None, count=False):
        """Funció per fer cerques per name exacte, enlloc d'amb 'ilike'.
        """
        exact = self.exact_search(cr, user, context=context)
        for idx, arg in enumerate(args):
            if len(arg) == 3:
                field, operator, match = arg
                if field == 'name' and isinstance(match,(unicode,str)):
                    if exact and not '%' in match:
                        operator = '='
                    args[idx] = (field, operator, match)
        return super(GiscedataPolissa, self).search(cr, user, args, offset,
                                                   limit, order, context, count)

    def write(self, cursor, user, ids, vals, context=None):
        """Actualitzem els valors a la base de dades.
        """
        if vals.get('data_baixa', False):
            new_vals = self.onchange_data_baixa(cursor, user, ids,
                                                vals['data_baixa'], context)
            vals.update(new_vals['value'])
        if vals.get('renovacio_auto', False):
            new_vals = self.onchange_renovacio_auto(cursor, user, ids,
                                                    vals['renovacio_auto'],
                                                    context)
            vals.update(new_vals['value'])
        if vals.get('state', False):
            self.create_log(cursor, user, ids, vals['state'])
        res = super(GiscedataPolissa, self).write(cursor, user, ids,
                                                  vals, context)
        return res

    def create(self, cursor, user, vals, context=None):
        """Creem els valors a la base de dades.
        """
        if vals.get('data_baixa', False):
            new_vals = self.onchange_data_baixa(cursor, user, [],
                                                vals['data_baixa'], context)
            vals.update(new_vals['value'])
        if vals.get('renovacio_auto', False):
            new_vals = self.onchange_renovacio_auto(cursor, user, [],
                                                    vals['renovacio_auto'],
                                                    context)
            vals.update(new_vals['value'])
        res_id = super(GiscedataPolissa, self).create(cursor, user, vals,
                                                      context)
        return res_id

    def unlink(self, cursor, uid, ids, context=None):
        """Comprovem que es pot eliminar la Pòlissa.
        """
        msg_error = ''
        for polissa in self.browse(cursor, uid, ids, context):
            if polissa.state not in CONTRACT_IGNORED_STATES:
                msg_error += _(u'La pòlissa %s no es pot eliminar.\n'
                               u'Està en estat %s.\n' % (polissa.name or \
                                                         'id#'+str(polissa.id),
                                                         polissa.state))
        if msg_error:
            raise osv.except_osv('Error', msg_error)
        res = super(GiscedataPolissa, self).unlink(cursor, uid, ids, context)
        return res

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'name': False,
            'potencies_periode': [],
            'modcontractuals_ids': [],
            'modcontractual_activa': False,
            'log_ids': [],
            'data_alta': False,
            'data_baixa': False,
            'data_firma_contracte': False,
            'related_attachments': [],
        }
        default.update(default_values)
        res_id = super(
            GiscedataPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def copy(self, cursor, user, id, default=None, context=None):
        if context is None:
            context = {}
        res_id = super(GiscedataPolissa, self).copy(cursor, user, id, default, context)
        if context.get("from_atr", False) and res_id:
            # Mantenim les mateixes potencies periode.
            # S'utilitza al duplicar contractes quan es genera un M1.
            new_pots = []
            pot_pre_obj = self.pool.get('giscedata.polissa.potencia.contractada.periode')
            for pot in self.read(cursor, user, id, ['potencies_periode'])['potencies_periode']:
                info = pot_pre_obj.read(cursor, user, pot, context=context)
                new_id = pot_pre_obj.create(cursor, user, {
                    'periode_id': info['periode_id'][0],
                    'polissa_id': res_id,
                    'potencia': info['potencia'],
                    'active': info['active'],
                })
                new_pots.append(new_id)
            self.write(cursor, user, res_id, {'potencies_periode': [(6, 0, new_pots)]})
        return res_id

    def browse(self, cursor, uid, ids, context=None, list_class=None, fields_process=None):
        """Sobreescrivm el browse per fer-lo compatible segons el temps.
        """
        if not context:
            context = {}
        res = super(GiscedataPolissa, self).browse(cursor, uid, ids, context)
        if 'date' not in context:
            return res
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        fields_to_read = contracte_obj.get_fields_for_polissa(cursor, uid)
        # Busquem la modificació contractual que toqui segons la data
        date_mod = context['date']
        if isinstance(ids, (int, long)):
            res = [res]
        for pol in res:
            contracte_id = self._get_modcon_date(cursor, uid, pol['id'],
                                                 date_mod)
            contracte = contracte_obj.browse(cursor, uid, contracte_id)
            for field_apply in fields_to_read:
                value = getattr(contracte, field_apply)
                setattr(pol, field_apply, value)
            pot_dict = contracte.get_potencies_dict()
            for periode in pol.potencies_periode:
                potencia = pot_dict.get(periode.periode_id.name)
                if potencia:
                    periode.potencia = potencia
        if isinstance(ids, (int, long)):
            res = res[0]
        return res

    def read(self, cursor, uid, ids, fields=None, context=None,
             load='_classic_read'):
        if not context:
            context = {}
        res = super(GiscedataPolissa, self).read(cursor, uid, ids, fields,
                                                 context, load)
        if 'date' not in context:
            return res
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        fields_to_read = contracte_obj.get_fields_for_polissa(cursor, uid)
        if fields:
            fields_to_read = list(set(fields) & set(fields_to_read))

        # Si han pasado campos para leer pero no hay nada para sustituir. Se
        # puede devolver el resultado del read normal
        if fields and not fields_to_read:
            return res

        # Busquem la modificació contractual que toqui segons la data
        date_mod = context['date']
        if isinstance(ids, (int, long)):
            res = [res]
        for pol in res:
            contracte_id = self._get_modcon_date(cursor, uid, pol['id'],
                                                 date_mod)
            contracte = contracte_obj.read(cursor, uid, contracte_id,
                                           fields_to_read)
            for field_apply in fields_to_read:
                pol[field_apply] = contracte[field_apply]
        if isinstance(ids, (int, long)):
            res = res[0]
        return res

    def crear_modcon(self, cursor, uid, polissa_id, values, ini, fi, context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, values)
        wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')

        ctx = context.copy()
        ctx.update({'active_id': polissa_id})
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def change_dates(self, cursor, uid, ids, context=None):
        # TODO
        pass

    def _get_modcon_date(self, cursor, uid, polissa_id, date_mod):
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        polissa = self.browse(cursor, uid, polissa_id)
        if polissa.state == 'baixa' and date_mod > polissa.data_baixa:
            date_mod = polissa.data_baixa
        search_params = [('data_inici', '<=', date_mod),
                         ('data_final', '>=', date_mod),
                         ('polissa_id.id', '=', polissa_id)]
        c_ids = contracte_obj.search(cursor, uid, search_params, limit=1,
                                     context={'active_test': False})
        if not c_ids:
            raise osv.except_osv('Error', u'Error no existeix cap '
                u'modificació contractual per la pòlissa (id:%s) en la data %s'
                % (polissa_id, date_mod))
        return c_ids[0]

    def get_changes(self, cursor, uid, polissa_id, context=None):
        """Comprova les diferències entre el que hem modificat i el contracte 
        actual o el que li passem pel context via modcon_id."""
        if not context:
            context = {}
        if isinstance(polissa_id, tuple) or isinstance(polissa_id, list):
            polissa_id = polissa_id[0]
        polissa = self.browse(cursor, uid, polissa_id)
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        fields_to_read = contracte_obj.get_fields_for_polissa(cursor, uid)
        polissa_vals = self.read(cursor, uid, [polissa_id], fields_to_read,
                                 context)[0]
        modcon_id = context.get('modcon_id', polissa.modcontractual_activa.id)
        contracte_vals = contracte_obj.read(cursor, uid,  [modcon_id],
                                            fields_to_read, context)[0]
        del polissa_vals['id']
        del contracte_vals['id']
        res = {}
        for field in fields_to_read:
            if polissa_vals.get(field) != contracte_vals.get(field):
                res.setdefault(field, {'old': contracte_vals.get(field),
                                       'new': polissa_vals.get(field)})
        return res

    def generar_periodes_potencia(self, cursor, uid, ids, context=None):
        """Genera les potencies contractades per periode."""
        pot_periode = self.pool.get(
                            'giscedata.polissa.potencia.contractada.periode')
        if not context:
            context = {}
        for polissa in self.browse(cursor, uid, ids):
            #Si forcem la generació no hem de mirar res mes
            if not context.get('force_genpot', False)\
                and polissa.potencies_periode\
                and all([polissa.tarifa.id == p.periode_id.tarifa.id
                        for p in polissa.potencies_periode]):
                pots = list(set([p.potencia
                                 for p in polissa.potencies_periode]))
                # If autogen, but not all pots are equal do not touch
                if (polissa.tarifa.autogen_periodes_pot
                    and len(pots) != 1):
                    continue 
                if not polissa.tarifa.autogen_periodes_pot:
                    continue
            # Eliminem els periodes de potencia actuals
            p_ids = pot_periode.search(cursor, uid,
                                       [('polissa_id.id', '=', polissa.id)],
                                       context={'active_test': False})
            pot_periode.unlink(cursor, uid, p_ids)
            # Per cada periode de la tarifa del tipus potència cream.
            if not polissa.tarifa:
                raise osv.except_osv(
                    'Error',
                    _(u"Per poder generar els períodes de potència, primer "
                      u"s'ha d'assignar una tarifa d'accés.")
                )
            for periode in polissa.tarifa.periodes:
                if periode.tipus == 'tp' and not periode.agrupat_amb:
                    vals = {
                        'polissa_id': polissa.id,
                        'periode_id': periode.id,
                        'potencia': polissa.potencia,
                        'active': 1
                    }
                    pot_periode.create(cursor, uid, vals)
        return True

    def get_modcontractual_intervals(self, cursor, uid, polissa_id, data_inici,
                                     data_final, context=None):
        """Obté tots els intervals de la pòlissa amb la data de tall i les
        modificacions.
        """
        if isinstance(polissa_id, list) or isinstance(polissa_id, tuple):
            polissa_id = polissa_id[0]
        if not context:
            context = {}
        context.update({'active_test': False})
        modcontractual_obj = self.pool.get('giscedata.polissa.modcontractual')
        dates_de_tall = {}
        search_params = [
            ('polissa_id', '=', polissa_id),
            '|',
            '|',
            '&',
            ('data_inici', '<=', data_inici),
            ('data_final', '>=', data_final),
            '&',
            ('data_inici', '>=', data_inici),
            ('data_inici', '<=', data_final),
            '&',
            ('data_final', '>=', data_inici),
            ('data_final', '<=', data_final),
        ]
        modc_ids = modcontractual_obj.search(cursor, uid, search_params,
                                             context=context,
                                             order="data_inici asc")
        modsf = []
        changes = {}
        dates = {}
        if len(modc_ids) > 1:
            # Per defecte qualsevol canvi és una nou interval
            ffields = context.get('ffields',
                                  self.fields_get(cursor, uid).keys())
            diffs = {}
            fields_to_read = ffields + ['data_inici', 'data_final']
            for modc in sorted(modcontractual_obj.read(cursor, uid, modc_ids,
                                                fields_to_read),
                               key=lambda k: k['data_inici']):
                # Si és la primera l'afegim
                if not diffs:
                    modsf.append(modc['id'])
                    dates.setdefault(modc['id'],
                                     (modc['data_inici'], modc['data_final']))
                    data_inici = modc['data_inici']
                for ffield in ffields:
                    # Comprovem si tenim inicialitzat:
                    if diffs.get(ffield, None) is not None:
                        if diffs.get(ffield, False) != modc.get(ffield, False):
                            if modc['id'] not in modsf:
                                modsf.append(modc['id'])
                            # Posem el camp a les modificacions
                            changes.setdefault(modc['id'], [])
                            changes[modc['id']].append(ffield)
                    diffs[ffield] = modc.get(ffield, False)
                if modc['id'] in changes:
                    # Si hi ha canvis afegim la nova modificació a les dates
                    dates[modc['id']] = (modc['data_inici'], modc['data_final'])
                else:
                    # Si no hi ha canvis agafem les dates de l'última
                    data_inici1, data_final1 = dates[modsf[-1]]
                    dates[modsf[-1]] = (min(data_inici1, modc['data_inici']),
                                         max(data_final1, modc['data_final']))
            modc_ids = modsf[:]
        for modc in sorted(modcontractual_obj.read(cursor, uid, modc_ids,
                                                   ['data_inici', 'data_final'],
                                                   context),
                           key=lambda k: k['data_inici']):
            data_tall = max(data_inici, modc['data_inici'])
            dates_de_tall[data_tall] = {'id': modc['id'],
                                        'changes': changes.get(modc['id'], []),
                                        'dates': dates.get(modc['id'],
                                                           (modc['data_inici'],
                                                            modc['data_final']))
                                        }
        return dates_de_tall
    
    def onchange_potencia(self, cursor, uid, ids, potencia, tarifa_id,
                          context=None):
        """Comprovem les restriccions al canviar de potència."""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not potencia or not tarifa_id:
            return res
        if not isinstance(ids, (list,tuple)):
            ids = [ids]
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        tarifa_name = tarifa_obj.read(cursor, uid, tarifa_id, ['name'])['name']

        tensio_obj = self.pool.get('giscedata.tensions.tensio')


        tarifa_enerdata = get_tariff_by_code(tarifa_name)
        if tarifa_enerdata:
            tarifa_enerdata = tarifa_enerdata()

            np = NormalizedPower()
            potencia_w = int(potencia * 1000)
            if not tarifa_enerdata.is_maximum_power_correct(potencia):
                warning_msg = _(u'La tarifa seleccionada {0}: {1:.3f} > X >= '
                                u'{2:.3f} no es correspon amb la potència '
                                u'introduïda: {3:.3f}')
                res['warning'].update({
                        'title': _(u'Avís'),
                        'message': warning_msg.format(
                            tarifa_name, tarifa_enerdata.get_min_power(),
                            tarifa_enerdata.get_max_power(), potencia
                        )
                })
            if np.is_normalized(potencia_w):
                #Obtenim el nom de la tensió ex. 3x400/230
                volt_name = np.get_volt_int(potencia_w)[0]
                #Obtenim el codi de la tensio a partir del nom
                tensio_ids = []
                if volt_name:
                    volt_code = tensio_obj.get_cnmc_code(cursor, uid, volt_name)

                    #Cerquem la tensió corresponent al codi cnmc_code
                    tensio_ids = tensio_obj.search(cursor, uid, [
                        ('cnmc_code', '=', volt_code)
                    ])

                if tensio_ids:
                    #Assignem la tensio trobada
                    res['value']['tensio_normalitzada'] = tensio_ids[0]
            else:
                res['warning'].update({
                    'title': _(u'Avís'),
                    'message': _(u'La potència no és normalitzada')
                })
        if potencia <= 0:
            old_warning = res['warning'].get('message', '')
            if old_warning:
                old_warning += '\n'
            new_warning = old_warning + _(u'La potència és negativa')
            res['warning'].update({
                'title': _(u'Avís'),
                'message': new_warning
            })
        if tarifa_name and not tarifa_name.startswith("2."):
            for inf in self.read(cursor, uid, ids, ['facturacio_potencia']):
                if inf['facturacio_potencia'] and inf['facturacio_potencia'] != 'max':
                    res['warning'].update({
                        'title': _(u'Error'),
                        'message': _(u"En tarifes superiors a les 2.x s'ha de facturar per maxímetre")
                    })
        elif tarifa_name and tarifa_name.startswith("2."):
            for inf in self.read(cursor, uid, ids, ['facturacio_potencia']):
                if inf['facturacio_potencia'] and inf['facturacio_potencia'] == 'max':
                    res['warning'].update({
                        'title': _(u'Avís'),
                        'message': _(u"S'ha seleccionat facturació per maximetre a una tarifa 2.x. Revisa si és correcte.")
                    })

        return res
    
    def onchange_tensio(self, cursor, uid, ids, tensio, tarifa_id,
                        context=None):
        """Comprovacions quan es canvia de tensi."""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        # Si no ens posen tensió no fem res
        if not tensio:
            return res
        else:
            tensio_obj = self.pool.get('giscedata.tensions.tensio')
            tensio = tensio_obj.browse(cursor, uid, tensio, context)
        if tarifa_id:
            tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
            tarifa = tarifa_obj.browse(cursor, uid, tarifa_id, context)
        else:
            return res
        # Si la tensió > 1000 només es podrà escollir tarifes AT
        if tensio.tipus == 'AT':
            res['domain'].update({'tarifa_acces': [('tipus', '=', 'AT')]})
            if tarifa and tarifa.tipus != 'AT':
                # Si la tarifa que té actualment és diferent a AT l'esborrem i
                # donem un missatge d'avís
                warning_msg = "Atenció la tarifa no és AT i s'ha introduït"
                warning_msg += " una tensió de AT"
                res['value'].update({'tarifa_acces': False})
                res['warning'].update({
                    'title': _('Error'), 
                    'message': _(warning_msg)})
        else:
            res['domain'].update({'tarifa_acces': [('tipus', '=', 'BT')]})
            if tarifa and tarifa.tipus != 'BT':
                # Si la tarifa que té actualment és diferent a BT l'esborrem i
                # donem un missatge d'avís
                warning_msg = "Atenció la tarifa no és BT i s'ha introduït"
                warning_msg += " una tensió de BT"
                res['value'].update({'tarifa_acces': False})
                res['warning'].update({
                    'title': _('Error'), 
                    'message': _(warning_msg)})
        return res
    
    def onchange_data_baixa(self, cursor, user, ids, data_baixa, context=None):
        """Canvis a realitzar quan es posa data de baixa a una pòlissa."""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not data_baixa:
            res['value'].update({'renovacio_auto': 1})
        else:
            res['value'].update({'renovacio_auto': 0})
        return res

    def onchange_renovacio_auto(self, cursor, user, ids, renovacio_auto,
                                context=None):
        """Canvis a realitzar quan es posa renovació automàtica."""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if renovacio_auto:
            res['value'].update({'data_baixa': False})
        return res

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        """Undo last modcontractual and reactivate the one that remains"""

        if context is None:
            context = {}

        ctx = context.copy()
        ctx.update({'sync': False})

        modcon_model = 'giscedata.polissa.modcontractual'
        modcon_obj = self.pool.get(modcon_model)
        wkf_instance = self.pool.get('workflow.instance')
        wkf_item = self.pool.get('workflow.workitem')
        wkf_activity = self.pool.get('workflow.activity')

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for polissa in self.browse(cursor, uid, ids, context=context):
            # If only one modcon or no modcon, do not do nothing
            if len(polissa.modcontractuals_ids) <= 1:
                continue
            # If not actiu, cannot make the change
            if polissa.state != 'activa':
                raise osv.except_osv('Error',
                                     _(u"No es pot desfer la darrera "
                                       u"modificació si la pòlissa no "
                                       u"està en estat actiu"))
            # Locate modcons implicated
            mod_act = polissa.modcontractual_activa
            mod_ant = polissa.modcontractual_activa.modcontractual_ant
            mod_act_date = mod_act.data_final
            mod_act_state = mod_act.state
            # Write the correct state for unlinking modcon
            mod_act.write({'state': 'pendent'}, context=ctx)
            # Assign mod_ant as the active one
            polissa.write({'modcontractual_activa': mod_ant.id},
                          context=ctx)
            modcon_obj.unlink(cursor, uid, [mod_act.id],
                              context=ctx)
            # Lets reactivate mod_ant
            # Search for workflow instance
            search_params = [('res_type', '=', modcon_model),
                             ('res_id', '=', mod_ant.id)]
            instance_id = wkf_instance.search(cursor, uid, search_params)[0]
            # Search for workflow item
            search_params = [('inst_id', '=', instance_id)]
            item_id = wkf_item.search(cursor, uid, search_params)
            # Search for workflow activity id
            search_params = [('wkf_id.osv', '=', modcon_model),
                             ('name', '=', mod_act_state)]
            activity_id = wkf_activity.search(cursor, uid, search_params)[0]
            # Reactivate workflow for mod_ant
            wkf_item.write(cursor, uid, item_id, {'act_id': activity_id,
                                                  'state': 'complete'})
            # Write new values for mod_ant
            mod_ant.write({'state': mod_act_state,
                           'active': True,
                           'data_final': mod_act_date},
                          context=ctx)
            # Adapt the contract to the new activated modcon
            signals = ['modcontractual', 'undo_modcontractual']
            self.send_signal(cursor, uid, [polissa.id], signals)

        return True

    # Workflow Stuff
    def activar_xmlrpc(self, cursor, uid, ids):
        """Activa un pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id, 'validar',
                                    cursor)
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id, 'activar',
                                    cursor)
        return True

    def send_signal(self, cursor, uid, ids, signals):
        """Enviem el signal al workflow de la pòlissa.
        """
        wf_service = netsvc.LocalService('workflow')
        if not isinstance(signals, list) and not isinstance(signals, tuple):
            signals = [signals]
        for p_id in ids:
            for signal in signals:
                wf_service.trg_validate(uid, 'giscedata.polissa', p_id, signal,
                                        cursor)
        return True

    def esborrany_xmlrpc(self, cursor, uid, ids):
        """Passa a esborrany una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id,
                                    'esborrany', cursor)
        return True

    def wkf_esborrany(self, cursor, uid, ids):
        """Passem a esborrany una pòlissa."""
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        # Eliminem tots els contractes (si n'hi ha)
        for polissa in self.read(cursor, uid, ids, ['modcontractuals_ids']):
            unlink_ids = polissa['modcontractuals_ids']
            if unlink_ids:
                modcont_obj.unlink(cursor, uid, unlink_ids)
        # Escrivim esborrany a l'estat
        self.write(cursor, uid, ids,
                   {'state': 'esborrany', 'active': True},
                   {'sync': False})
        return True
        
    def wkf_validar(self, cursor, uid, ids):
        """Acció dins l'estat validar.
        """
        self.write(cursor, uid, ids,
                   {'state': 'validar'},
                   {'sync': False})
        return True

    def wkf_cancelar(self, cursor, uid, ids):
        """ Acció per a cancel·lar una pòlissa. """
        self.write(cursor, uid, ids,
                   {'state': 'cancelada', 'active': False,
                    'data_baixa': datetime.today().strftime("%Y-%m-%d")},
                   {'sync': False})
        return True

    def wkf_contracte(self, cursor, uid, ids):
        """Acció quan es passa a l'estat contracte.
        
        En aquest estat del workflow generem el contracte segons els valors
        que tingui la pòlissa. Ja sigui perquè és una pòlissa nova o perquè
        venim d'una modificació contractual.
        """
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        
        for polissa in self.browse(cursor, uid, ids):
            # Hem de crear un conctracte amb tots els valors que té la
            # pòlissa en l'actualitat
            vals = contracte_obj.get_vals_from_polissa(cursor, uid,
                                                       polissa.id)
            contracte_obj.create(cursor, uid, vals)
        self.write(cursor, uid, ids,
                   {'state': 'contracte'},
                   {'sync': False})
        return True
    
    def cnd_novapolissa(self, cursor, uid, ids):
        """Comprovem si s'ha de crear una nova pòlissa o no."""
        for polissa_id in ids:
            changes = self.get_changes(cursor, uid, polissa_id)
            # TODO: Poder configurar quins canvis impliquen nova pòlissa
            return changes.has_key('titular')
    
    def wkf_novapolissa(self, cursor, uid, ids):
        """Acció quan es passa a l'estat contracte.
        
        Coses a fer:
        * Crear una nova pòlissa amb els valors de l'actual copy() + defaults
        * Aplicar els valors de la modificació contractual actual per deixar
        la pòlissa tal i com estava
        """
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        for polissa in self.browse(cursor, uid, ids):
            modcontractual_activa = polissa.modcontractual_activa
            
            contract_vals = contracte_obj.get_vals_from_polissa(cursor, uid,
                                                                polissa.id)
            polissa_vals = contract_vals.copy()
            polissa_vals['data_alta'] = polissa_vals['data_inici']
            del polissa_vals['data_inici']
            del polissa_vals['data_final']
            del polissa_vals['tipus']
            fields_list = self.fields_get_keys(cursor, uid)
            polissa_vals.update(self.default_get(cursor, uid, fields_list))
            novapolissa_id = self.copy(cursor, uid, polissa.id, polissa_vals)
            # Enviem el signal per activar la nova pòlissa (contracte)
            wf_service = netsvc.LocalService('workflow')
            wf_service.trg_validate(uid, 'giscedata.polissa', novapolissa_id,
                                    'activar', cursor)
            contracte_obj.aplicar_modificacio(cursor, uid,
                                              modcontractual_activa.id,
                                              polissa.id)
            polissa.write(
                        {'data_baixa': modcontractual_activa.data_final,
                         'renovacio_auto': 0})
            # Enllacem modificacions contractuals
            novapolissa = self.browse(cursor, uid, novapolissa_id)
            novapolissa.modcontractuals_ids[-1].write(
                {'modcontractual_ant': modcontractual_activa.id}
            )
            modcontractual_activa.write(
                {'modcontractual_seg': novapolissa.modcontractuals_ids[-1].id}
            )
            
        self.write(cursor, uid, ids, {'state': 'novapolissa'})
        return True
    
    def cnd_contracte_actiu(self, cursor, uid, ids):
        """Comprovem la condició de contracte actiu."""
        avui = time.strftime('%Y-%m-%d')
        for polissa in self.browse(cursor, uid, ids):
            # Si la data d'inici <= que avui ho podem activar
            if polissa.modcontractual_activa and \
               polissa.modcontractual_activa.data_inici <= avui:
                return True
            else:
                return False

    def create_log(self, cursor, uid, ids, state, context=None):
        """ Crea un registre per a un canvi en la polissa. """
        if context is None: 
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        if state in CONTRACT_LOGGABLE_STATES:
            log_obj = self.pool.get('giscedata.polissa.log')
            for polissa_id in ids:
                log_obj.create(cursor, uid, {
                    'polissa_id': polissa_id,
                    'state': state,
                    'change_date': datetime.now(),
                    'user_id': uid
                })
        return True

    def wkf_pendent(self, cursor, uid, ids):
        """Estat pendent d'una pòlissa."""
        self.write(cursor, uid, ids,
                   {'state': 'pendent'},
                   {'sync': False})
        return True
    
    def wkf_activa(self, cursor, uid, ids):
        """Activem una pòlissa."""
        modcontractual_obj = self.pool.get('giscedata.polissa.modcontractual')
        for polissa in self.browse(cursor, uid, ids):
            # Creem un diccionari amb els valors que posarem a la pólissa
            vals = {'state': 'activa', 'active': 1}

            # If contract type is eventual (02 or 09) should keep low date and
            # deactivate auto renewal.
            # list_dat_fi store dicts with id's and contract types
            if polissa.contract_type not in (u'02', u'09'):
                vals.update({'data_baixa': False, 'renovacio_auto': 1})

            # Hem de numerar la pòlissa si tenim activat el gen_num_auto i
            # la pòlisa no té cap número assignat
            if polissa.name_auto and not polissa.name:
                vals.update({'name': 
                             self.pool.get('ir.sequence').get(cursor, uid, 
                                                    'giscedata.polissa')})
            if polissa.modcontractual_activa:
                if not polissa.modcontractual_activa.active:
                    # Això és que venim d'una reactivació enviem el signal
                    # de reactivar a la modificació contractual
                    dfpb = polissa.modcontractual_activa.data_final_pre_baixa
                    polissa.modcontractual_activa.write({'data_final': dfpb})
                    wf_service = netsvc.LocalService('workflow')
                    wf_service.trg_validate(uid,
                                            'giscedata.polissa.modcontractual',
                                            polissa.modcontractual_activa.id,
                                            'reactivar', cursor)
                modcontractual_obj.aplicar_modificacio(cursor, uid,
                                            polissa.modcontractual_activa.id,
                                            polissa.id)
            # Actualitzem la pòlissa
            self.write(cursor, uid, [polissa.id],
                       vals, {'sync': False})
        return True
      
    def wkf_modcontractual(self, cursor, uid, ids):
        """Fem una modificació contractual."""
        self.write(cursor, uid, ids,
                   {'state': 'modcontractual'},
                   {'sync': False})
        return True
    
    def wkf_undo_modcontractual(self, cursor, uid, ids):
        """Desfem una modificació contractual."""
        # Amb la transició automatica a actiu, ja s'aplica
        # la modificacio contractual activa

        return True

    def wkf_overwrite_modcontractual(self, cursor, uid, ids):
        """Apliquem directament la modificació contractual.
        """
        mc_obj = self.pool.get('giscedata.polissa.modcontractual')
        for polissa in self.browse(cursor, uid, ids):
            vals = mc_obj.get_vals_from_polissa(cursor, uid, polissa.id)
            mca = polissa.modcontractual_activa
            vals.update({'data_inici': mca.data_inici,
                         'data_final': mca.data_final,
                         'data_firma_contracte': mca.data_firma_contracte,
                         'tipus': mca.tipus,
                         'name': mca.name})
            mca.write(vals)
            mca = polissa.modcontractual_activa
            mca.write(vals)
        return True

    def cnd_activa_cancelar(self, cursor, uid, ids):
        """ Condició per comprovar que es pot cancel·lar una pòlissa activa. """
        # Implemented on giscedata_facturacio
        return True

    def cnd_impagament(self, cursor, uid, ids):
        """Condició per comprovar la transició a impagament.

        Aquesta haurà de ser sobreescrita pels mòduls de facturació.
        """
        return True
    
    def wkf_impagament(self, cursor, uid, ids):
        """Estat d'impagament."""
        self.write(cursor, uid, ids,
                   {'state': 'impagament'},
                   {'sync': False})
        return True

    def impagament_xmlrpc(self, cursor, uid, ids):
        """Passa a impagament una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id,
                                    'impagament', cursor)
        return True

    def wkf_tall(self, cursor, uid, ids):
        """Estat de tall."""
        self.write(cursor, uid, ids,
                   {'state': 'tall'},
                   {'sync': False})
        return True

    def tall_xmlrpc(self, cursor, uid, ids):
        """Passa a tall una pòlissa a través d'XML-RPC."""
        wf_service = netsvc.LocalService('workflow')
        for p_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', p_id,
                                    'tallar', cursor)
        return True

    def cnd_baixa(self, cursor, uid, ids):
        """Condició per passar a baixa.
        
        Que cada mòdul que exingui polissa faci les comprovacions que s'hagin
        de fer.
        """
        for polissa in self.browse(cursor, uid, ids):
            today = time.strftime('%Y-%m-%d')
            if polissa.renovacio_auto:
                raise osv.except_osv(_(u"Error"),
                                     _(u"No es poden donar de baixa"
                                       u" pòlisses amb renovació automàtica"))
                return False
            elif not polissa.data_baixa:
                raise osv.except_osv(_(u"Error"),
                                     _(u"No es poden donar de baixa"
                                       u" pòlisses sense una data de baixa"))
                return False
            elif polissa.data_baixa and polissa.data_baixa > today:
                raise osv.except_osv(_(u"Error"),
                                     _(u"No es poden donar de baixa pòlisses"
                                       u" amb data de baixa posterior a avui"))
                return False
            elif (polissa.data_baixa and
                  polissa.data_baixa < (polissa.modcontractual_activa.
                                        data_inici)):
                raise osv.except_osv(_(u"Error"),
                                     _(u"No es poden donar de baixa pòlisses "
                                       u"amb una data anterior a la data "
                                       u"d'inici de la darrera modificació "
                                       u"contractual"))
        return True
    
    def wkf_baixa(self, cursor, uid, ids):
        """Passem a l'estat de baixa."""
        for polissa in self.browse(cursor, uid, ids):
            data_final = polissa.data_baixa or time.strftime('%Y-%m-%d')
            if polissa.modcontractual_activa:
                data_final_pre_baixa = polissa.modcontractual_activa.data_final
                polissa.modcontractual_activa.write(
                    {'data_final': data_final,
                     'data_final_pre_baixa': data_final_pre_baixa})
                mod_id = polissa.modcontractual_activa.id
                wf_service = netsvc.LocalService('workflow')
                wf_service.trg_validate(uid, 'giscedata.polissa.modcontractual',
                                        mod_id, 'baixa', cursor)
        self.write(cursor, uid, ids, {'state': 'baixa', 
                                      'data_baixa': data_final,
                                      'renovacio_auto': False,
                                      'active': 0},
                                      {'sync': False})
        return True

    def cnd_validar_to_contracte(self, cursor, uid, ids):
        return True

    def _cnt_un_cups_per_polissa_activa(self, cursor, uid, ids):
        """Comprova que només hi ha una pòlissa 'activa' per CUPS.
        """
        states_ok = tuple(CONTRACT_IGNORED_STATES + ['pendent', 'baixa'])
        for polissa in self.browse(cursor, uid, ids):
            if polissa.state in states_ok:
                continue
            search_params = [('cups.id', '=', polissa.cups.id),
                             ('id', '!=', polissa.id),
                             ('state', 'not in', states_ok)]
            if self.search_count(cursor, uid, search_params):
                return False
        return True

    def _cnt_positive_power(self, cursor, uid, ids):
        """Comprova que la potencia sigui estrictament positiva
        """
        for polissa_vals in self.read(cursor, uid, ids, ['potencia', 'state']):
            if polissa_vals['potencia'] <= 0 and polissa_vals['state'] not in CONTRACT_IGNORED_STATES:
                return False
        return True

    def _get_tensio(self, cursor, uid, ids, name, arg, context=None):
        '''returns tensio associated to tensio normalitzada'''
        res = {}
        for polissa in self.browse(cursor, uid, ids, context=context):
            res[polissa.id] = polissa.tensio_normalitzada \
                              and polissa.tensio_normalitzada.tensio \
                              or 0
        return res

    def _cups_cp(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    def _cups_cp_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            return [('cups.dp', '=', args[0][2])]

    def _cups_poblacio(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)

        return res

    def _cups_poblacio_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        cups_obj = self.pool.get('giscedata.cups.ps')
        cups_ids = cups_obj.search(cursor, uid, [
            ('id_poblacio.name', 'ilike', args[0][2])
        ])
        cups_ids.append(-1)
        return [('cups.id', 'in', cups_ids)]

    def _fnc_firmat(self, cursor, uid, ids, name, arg, context=None):
        """Ens retorna si un contracte està firmat o no.
        """
        res = {}
        for pol in self.read(cursor, uid, ids, ['data_firma_contracte']):
            res[pol['id']] = bool(pol['data_firma_contracte'])
        return res

    def _fnc_firmat_search(self, cursor, uid, obj, name, args, context=None):
        if not args:
            return []
        else:
            firmat = bool(args[0][2])
            if firmat:
                domain = [('data_firma_contracte', '>', '0001-01-01')]
            else:
                domain = [('data_firma_contracte', '=', False)]
            return domain

    def update_observacions(self, cursor, uid, ids,  text, title='',
                            context=None):
        """Adds a new comment on 'observacions' field. If titol is passed, it
        generate's a header with date and title """
        if context is None:
            context = {}

        if isinstance(ids, (tuple, list)):
            ids = ids[0]

        contract_vals = self.read(cursor, uid, ids, ['observacions'])

        comments = []

        if title:
            cur_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            user_vals = self.pool.get('res.users').read(
                cursor, uid, uid, ['name', 'login']
            )
            title_txt = u'**** {0} [{1} - {2} ({3})] ****'.format(
                title, cur_date, user_vals['name'], user_vals['login']
            )
            comments.append(title_txt)

        comments.append(text)
        new_comment = "\n".join(comments)

        if contract_vals['observacions']:
            comment = "{0}\n{1}".format(
                new_comment,
                contract_vals['observacions']
            )
        else:
            comment = new_comment

        return self.write(cursor, uid, ids, {'observacions': comment})

    def _get_polissa_ids(self, cursor, uid, ids, context=None):
        """Retorna els ids que s'han de recalcular.

        Les pòlisses que s'han de recalcular són les que s'hagin modificat les
        modificacions contractuals actives, sino no cal recalcular la pòlissa.
        """
        polissa_obj = self.pool.get('giscedata.polissa')
        return polissa_obj.search(cursor, uid,
                                  [('modcontractual_activa', 'in', ids)])

    def _correct_cnae(self, cursor, uid, ids):
        """Comprova que el cnae es de 4 digits
        """
        res_config = self.pool.get('res.config')
        check_cnae = int(res_config.get(cursor, uid, 'check_cnae', 0))
        if check_cnae:
            cnae_obj = self.pool.get('giscemisc.cnae')
            for polissa_vals in self.read(cursor, uid, ids, ['cnae']):
                if polissa_vals['cnae']:
                    cnae_id = polissa_vals['cnae'][0]
                    vals = cnae_obj.read(cursor, uid, cnae_id, ['name'])
                    return len(vals['name']) == 4
        return True

    def _auto_init(self, cr, context={}):
        if context is None:
            context = {}
        res = super(GiscedataPolissa, self)._auto_init(cr, context)
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscedata_polissa_dadbc'")
        if not cr.fetchone():
            cr.execute('CREATE INDEX idx_giscedata_polissa_dadbc '
                       'ON giscedata_polissa (data_alta,data_baixa,cups)')
        return res

    def create_contracte(self, cursor, uid, ids, data_inici=None, pol_vals=None, context=None):
        '''
        Crea un contracte amb la corresponent modificació contractual.
        :param data_inici: Data inici del nou contracte. Si no hi ha data inici s'agafa la ultima lectura facturada +1
        :param pol_vals: ha de ser un diccionari. Si es proporciona es fa un write al contracte amb aquests valors
        '''
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]

        for polissa in self.browse(cursor, uid, ids, context=context):

            if polissa.state == "esborrany":
                raise Exception("No es poden fer modificacions contractuals a contractes en esborrany")
            elif polissa.state == "activa":
                polissa.send_signal(['modcontractual'])

            if pol_vals and isinstance(pol_vals, dict):
                polissa.write(pol_vals)

            if not data_inici and polissa.data_ultima_lectura:
                data_inici = datetime.strptime(polissa.data_ultima_lectura, "%Y-%m-%d") + timedelta(days=1)
                data_inici = data_inici.strftime("%Y-%m-%d")

            wz_crear_contracte_obj = self.pool.get(
                'giscedata.polissa.crear.contracte'
            )
            params = {'duracio': 'actual', 'accio': 'nou'}
            context.update({'active_id': polissa.id})

            try:
                wz_id = wz_crear_contracte_obj.create(
                    cursor, uid, params, context=context
                )
            except Exception as e:
                raise osv.except_osv(
                    _(u"ERROR"),
                    _(u"Verifica que la pòlissa %s estigui activada.") %
                    (polissa.name,)
                 )

            wz_inst = wz_crear_contracte_obj.browse(
                cursor, uid, wz_id, context=context
            )
            res = wz_crear_contracte_obj.onchange_duracio(
                cursor, uid, [wz_id], data_inici, wz_inst.duracio,
                context=context
            )

            if res.get('warning', False):
                info = _(u"Hem trobat un error amb les dates al crear la "
                         u"modificació contractual de la pólissa '%s'. %s: %s."
                         u"") % (polissa.name, res['warning']['title'],
                                 res['warning']['message'])
                polissa.send_signal('undo_modcontractual')
                raise osv.except_osv(_(u"ERROR"), info)

            params = {
                'data_final': res['value']['data_final'] or '',
                'data_inici': data_inici
            }
            wz_inst.write(params, context=context)
            wz_inst = wz_crear_contracte_obj.browse(
                cursor, uid, wz_id, context=context
            )
            wz_inst.action_crear_contracte(context=context)

    def modify_contracte(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        for pol_id in ids:
            wz_crear_contracte_obj = self.pool.get(
                'giscedata.polissa.crear.contracte'
            )
            params = {'duracio': 'actual', 'accio': 'modificar'}
            context.update({'active_id': pol_id})
            try:
                wz_id = wz_crear_contracte_obj.create(
                    cursor, uid, params, context=context
                )
            except Exception as e:
                raise osv.except_osv(_(u"ERROR"), _(
                    u"Verifica que la pòlissa estigui activada"))

            wz_inst = wz_crear_contracte_obj.browse(
                cursor, uid, wz_id, context=context
            )
            wz_inst.action_crear_contracte(context=context)

    def change_bank_account(self, cursor, uid, ids, iban, data, pagador_id,
                            mandate_scheme, account_owner_inst,
                            owner_address_inst, payment_mode_inst,
                            modcon_type=None, context=None):
        def _check_data(polissa_inst, account_iban):

            if polissa_inst.payment_mode_id.require_bank_account:
                bank = polissa_inst.bank
                if bank:
                    bank_old_iban = bank.iban.replace(" ", "")

                    if account_iban == bank_old_iban:
                        raise osv.except_osv(_(u"ERROR"), _(
                            u"El compte bancari a canviar ha de ser diferent a "
                            u"l'actual"
                        ))

            if polissa_inst.state != 'activa':
                raise osv.except_osv(_(u"ERROR"), _(
                    u"La pòlissa ha d'estar activa."))

        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        mandate_obj = self.pool.get('payment.mandate')
        bank_obj = self.pool.get('res.partner.bank')

        data_str = datetime.strptime(data, '%Y-%m-%d')

        if data_str > datetime.today():
            raise osv.except_osv(
                "Error",
                "No es pot fer aquest canvi a futur."
            )

        polissa = polissa_obj.browse(cursor, uid, ids, context=context)[0]

        # -- Verificar que les dades del wizard son coherents.
        _check_data(polissa, iban)

        mandate_reference = "giscedata.polissa," + str(polissa.id)
        mandate_new_id = context.get('use_mandate_id')
        mandate_ids = mandate_obj.search(
            cursor, uid,
            [
                ('reference', '=', mandate_reference),
                ('date_end', '=', None),
                ('signed', '=', True),
                ('id', '!=', mandate_new_id)
            ],
            context=context
        )

        if mandate_ids:
            # -- Finalitzar el mandato
            mandate_obj._close_mandate(
                cursor, uid, mandate_ids, data_str - timedelta(days=1),
                context=context
            )

        # -- Crear modificació contractual
        polissa.send_signal('modcontractual')

        # ---- Guardar les noves dades bancaries
        bank_new_id = bank_obj._find_or_create_bank(
            cursor, uid, pagador_id, iban, account_owner_inst,
            owner_address_inst, context=context
        )
        params = {
            'bank': bank_new_id,
        }

        if payment_mode_inst:
            params.update({
                'payment_mode_id': payment_mode_inst.id,
                'tipo_pago': payment_mode_inst.type.id
            })

        polissa.write(params, context=context)

        if modcon_type:
            contract_modify_iban_modcon_type = modcon_type
        else:
            conf_obj = self.pool.get('res.config')
            contract_modify_iban_modcon_type = conf_obj.get(
                cursor, uid, 'contract_modify_iban_modcon_type', 'new'
            )

        # -- Cear o modificar el contracte
        if contract_modify_iban_modcon_type == 'new':
            polissa.create_contracte(data, context=context)
        elif contract_modify_iban_modcon_type == 'current':
            polissa.modify_contracte(context=context)

        # -- Crear un nou mandat
        if not mandate_new_id:
            mandate_new_id = mandate_obj._create_mandate(
                cursor, uid, data_str, mandate_reference, mandate_scheme,
                context=context
            )
        return mandate_new_id

    def check_data_inici_anterior_modcon_activa(self, cursor, uid, ids,
                                                data_inici, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        polissa_querier = OOQuery(self.pool.get(self._name), cursor, uid)
        polissa_query = polissa_querier.select(
            ['id', 'modcontractual_activa.data_inici']
        ).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*polissa_query)
        vals = cursor.dictfetchall()
        res = {}
        single_res = {
            'title': _(u"Error"),
            'message': _(u"La data inicial no pot ser inferior a la "
                         u"inicial del contracte actual.")
        }
        for polissa_vals in vals:
            polissa_id = polissa_vals['id']
            if data_inici < polissa_vals['modcontractual_activa.data_inici']:
                res[polissa_id] = single_res
            else:
                res[polissa_id] = False

        return res

    def check_data_inici_igual_modcon_activa(self, cursor, uid, ids, data_inici,
                                             context=None):
        if not isinstance(ids, list):
            ids = [ids]
        polissa_querier = OOQuery(self.pool.get(self._name), cursor, uid)
        polissa_query = polissa_querier.select(
            ['id', 'modcontractual_activa.data_inici']
        ).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*polissa_query)
        vals = cursor.dictfetchall()
        res = {}
        single_res = {
            'title': _(u"Atenció"),
            'message': _(u"Ja existeix una modificació contractual activa amb "
                         u"aquesta data. Es modificarà la modificació actual.")
        }
        for polissa_vals in vals:
            polissa_id = polissa_vals['id']
            if data_inici == polissa_vals['modcontractual_activa.data_inici']:
                res[polissa_id] = single_res
            else:
                res[polissa_id] = False

        return res

    _STORE_PROC_AGREE_FIELDS = {
        'giscedata.polissa': (lambda self, cursor, uid, ids, context=None: ids,
                              ['tensio_normalitzada', 'potencia',
                               'tarifa', 'potencies_periode'], 20),
        'giscedata.polissa.modcontractual': (_get_polissa_ids,
                                             ['tensio', 'potencia', 'tarifa',
                                              'potencies_periode'],
                                             20)
    }

    def _related_attachments(self, cursor, uid, ids, field_name, arg,
                             context=None):
        return self.internal_related_attachments(
            cursor, uid, ids, field_name, arg, context=context)

    def internal_related_attachments(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        attach_obj = self.pool.get('ir.attachment')
        fields = list(set(self.get_related_attachments_fields(cursor, uid)))
        def_fields = self.fields_get(cursor, uid, fields)
        for polissa in self.read(cursor, uid, ids, fields, context=context):
            attach_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'giscedata.polissa'),
                ('res_id', '=', polissa['id'])
            ])
            for field in fields:
                model = def_fields[field].get('relation')
                if model and polissa[field]:
                    attach_ids += attach_obj.search(cursor, uid, [
                        ('res_model', '=', model),
                        ('res_id', '=', polissa[field][0])
                    ])

            res[polissa['id']] = list(set(attach_ids))

        return res

    def get_potencies_dict(self, cursor, uid, contract_id, context=None):
        """
        Gets potencies dict from modcon. Uses context['date'] to get values in
        such date
        :param contract_id: Contract to get dict
        :param context: key: date -> Date to finpower values
        :return: {P1: power1, P2: power2, ...., Pn: powern}
        """
        if context is None:
            context = {}

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        if context.get('date'):
            modcon_date = context.get('date')
            modcon_id = self._get_modcon_date(
                cursor, uid, contract_id, modcon_date
            )
        else:
            modcon_id = self.read(
                cursor, uid, contract_id, ['modcontractual_activa']
            )['modcontractual_activa'][0]

        return modcon_obj.get_potencies_dict(cursor, uid, modcon_id)

    def te_autoconsum(self, cursor, uid, polissa_id, context=None):
        if context is None:
            context = {}
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]
        tipus_autoconsum = self.read(cursor, uid, polissa_id, ['autoconsumo'], context=context)['autoconsumo']
        return tipus_autoconsum in TENEN_AUTOCONSUM

    def crear_modcon(self, cursor, uid, polissa_id, values, ini, fi, context=None):
        if context is None:
            context = {}
        pol = self.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        self.write(cursor, uid, polissa_id, values)
        wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')

        ctx = context.copy()
        ctx.update({'active_id': polissa_id})
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    _columns = {
        'name': fields.char('Pòlissa', size=60, readonly=True, select=True,
                            states={'esborrany':[('readonly', False)],
                                    'validar': [('readonly', False)]}),
        'name_auto': fields.boolean('Auto'),
        'active': fields.boolean('Activa', required=True, readonly=True),
        'renovacio_auto': fields.boolean('Renovació Automàtica'),
        'cups': fields.many2one('giscedata.cups.ps', 'CUPS', readonly=True,
                                states={'esborrany':[('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)]}),
        'cups_cp': fields.function(_cups_cp, type='char', string='Codi Postal',
                                   fnct_search=_cups_cp_search),
        # TODO: Canviar això per un camp funció 'trigger'
        'cups_direccio': fields.related('cups', 'direccio',type='char',
                                       relation='giscedata.cups.ps',
                                       string="Direcció CUPS", store=False,
                                       readonly=True, size=256),
        'cups_poblacio': fields.function(
            _cups_poblacio, string=u'Població', type='char',
            fnct_search=_cups_poblacio_search
        ),
        'observacions': fields.text('Observacions'),
        'print_observations': fields.text('Observacions impresses'),
        'potencia': fields.float('Potència contractada (kW)', digits=(16,3),
                                 readonly=True,
                                 states={
                                     'esborrany':[('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                }),
        'potencies_periode': fields.one2many(
                            'giscedata.polissa.potencia.contractada.periode',
                            'polissa_id', 'Potencies contractades per periode',
                            readonly=True,
                            states={
                                     'esborrany':[('readonly', False)],
                                     'validar': [('readonly', False)],
                                     'modcontractual': [('readonly', False)]
                                  }),
        'tarifa': fields.many2one('giscedata.polissa.tarifa', "Tarifa d'accés",
                                  readonly=True,
                                  states={
                                     'esborrany':[('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                     'modcontractual': [('readonly', False),
                                                        ('required', True)]
                                  }),
        'titular': fields.many2one('res.partner', 'Titular', readonly=True,
                                   states={
                                     'esborrany':[('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)],
                                   }, size=40),
        'titular_nif': fields.related('titular','vat', type='char',
                                    string='NIF titular', readonly=True),
        'tensio_normalitzada': fields.many2one('giscedata.tensions.tensio',
                                    'Tensió normalitzada', readonly=True,
                                    states={
                                        'esborrany':[('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                    }),
        'tensio': fields.function(_get_tensio, method=True,
                        type='integer', string='Tensió (v)',
                        store={'giscedata.polissa':
                               (lambda self, cursor,
                                uid, ids, c=None: ids,
                                ['tensio_normalitzada'], 20),
                               }),
        'state': fields.selection(_polissa_states_selection, 'Estat',
                                  required=True, readonly=True),
        'data_alta': fields.date('Data alta', readonly=True,
                                 states={
                                     'esborrany':[('readonly', False)],
                                     'validar': [('readonly', False),
                                                 ('required', True)]
                                }),
        'data_baixa': fields.date('Data baixa'),
        'data_firma_contracte': fields.datetime('Data firma contracte',
                                            readonly=True,
                                            states={
                                            'esborrany':[('readonly', False)],
                                            'validar': [('readonly', False),
                                                        ('required', True)]
                                            }),
        'modcontractuals_ids': fields.one2many(
                                        'giscedata.polissa.modcontractual',
                                        'polissa_id', 
                                        'Modificacions contractuals',
                                        context={'active_test': False},
                                        readonly=True),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE', ondelete='restrict',
                                readonly=True,
                                states={
                                    'esborrany':[('readonly', False)],
                                    'validar':[('readonly', False),
                                               ('required', True)],
                                    'modcontractual':[('readonly', False),
                                                      ('required', True)],
                                }),

        'comercialitzadora': fields.many2one('res.partner',
                                             'Comercialitzadora'),
        'distribuidora': fields.many2one('res.partner', 'Distribuidora'),
        'firmat': fields.function(_fnc_firmat, type='boolean',
                                  string="Contracte Firmat", method=True,
                                  fnct_search=_fnc_firmat_search),
        'tipus_vivenda': fields.selection(
            [('habitual', 'Habitual'),
             ('no_habitual', 'No habitual')],
            string='Tipus vivenda',
            readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)],
            }),
        'persona_fisica': fields.related('titular', 'cifnif',
                                        type='char', size=2),
        'tg': fields.selection(TG_OPERATIVA, u"Telegestió",
                               readonly=True,
                               states={'esborrany': [('readonly', False)],
                                       'validar': [('readonly', False)],
                                       'modcontractual': [('readonly', False)],
                                       },
                               help=u'Telegestió operativa amb o sense corba de'
                                    u' càrrega (CCH)'),
        'autoconsumo': fields.selection(
            TIPO_AUTOCONSUMO, u"Autoconsum", readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)],
            },
            help=u'Tipus de autoconsum del contracte segons RD. 900/2015'
        ),
        'agree_tensio': fields.function(get_nivell_agregacio, type='char',
                                        fnct_search=None, string="Codi Tensió",
                                        method=False, multi='agree', size=2,
                                        store=_STORE_PROC_AGREE_FIELDS,
                                        readonly=True),
        'agree_tarifa': fields.function(get_nivell_agregacio, type='char',
                                        fnct_search=None, string="Codi Tarifa",
                                        method=False, multi='agree', size=2,
                                        store=_STORE_PROC_AGREE_FIELDS,
                                        readonly=True),
        'agree_dh': fields.function(get_nivell_agregacio, type='char',
                                    fnct_search=None, string="Codi DH",
                                    method=False, multi='agree', size=2,
                                    store=_STORE_PROC_AGREE_FIELDS,
                                    readonly=True),
        'agree_tipus': fields.function(get_nivell_agregacio, type='char',
                                       fnct_search=None, string="Tipus Punt",
                                       method=False, multi='agree', size=2,
                                       store=_STORE_PROC_AGREE_FIELDS,
                                       readonly=True),
        'nocutoff': fields.many2one('giscedata.polissa.nocutoff',
                                    'Subministrament no tallable',
                                    readonly=True,
                                    states= {
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False)],
                                        'modcontractual': [('readonly', False)]},
                                    write=[
                                        'giscedata_polissa.group_giscedata_polissa_cutoff'
                                    ]),
        'log_ids': fields.one2many(
            'giscedata.polissa.log', 'polissa_id', u'Històric', readonly=True
        ),
        'contract_type': fields.selection(
            CONTRACT_TYPES, 'Tipus de contracte', readonly=True,
            help="Distintos tipos de contratos. Ver REAL DECRETO 1164/2001",
            states=DEFAULT_STATES_MODCON
        ),
        'no_cessio_sips': fields.selection(
            SIPS_CESSION_SEL,
            'No cessió de SIPS', readonly=True, states={
            'esborrany': [('readonly', False)],
            'validar': [('readonly', False)],
            'modcontractual': [('readonly', False)]
        }),
        'no_cessio_sips_data': fields.date(
            'Data sol·licitud', readonly=True, states={
            'esborrany': [('readonly', False)],
            'validar': [('readonly', False)],
            'modcontractual': [('readonly', False)]
        }),
        'related_attachments': fields.function(
            _related_attachments,
            method=True,
            string='Adjunts relacionats',
            type='one2many',
            relation='ir.attachment'
        ),
    }

    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'esborrany',
        'name_auto': lambda *a: 1,
        'renovacio_auto': lambda *a: 1,
        'tipus_vivenda': lambda *a: 'habitual',
        'tg': lambda *a: '2',
        'autoconsumo': lambda *a: '00',
        'contract_type': lambda *a: '01',
        'no_cessio_sips': lambda *a: 'unactive'
    }

    _sql_constraints = [
        ('name_unic', 'unique (name)', 
        _('Aquest número de pòlissa ja existeix.')),
    ]

    _constraints = [
        (
            _cnt_un_cups_per_polissa_activa,
            'Error: Ja existeix una pòlissa amb aquest CUPS.',
            ['cups']
        ),
        (
            _cnt_positive_power,
            'Error: La potencia ha de ser estrictament positiva',
            ['potencia']
        ),
        (
            _correct_cnae,
            u'Error: El CNAE escollit no es valid. Només es permeten CNAE de 4 digits',
            ['cnae']
        )
    ]

GiscedataPolissa()

class GiscedataPolissaLog(osv.osv):
    """ Log dels estats de la Pòlissa """
    _name = 'giscedata.polissa.log'

    _polissa_states_selection = CONTRACT_STATES

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Polissa', required=True, ondelete='cascade'
        ),
        'state': fields.selection(
            _polissa_states_selection, 'Estat', required=True
        ),
        'change_date': fields.datetime(
            'Data de canvi'
        ),
        'user_id': fields.many2one(
            'res.users', 'Usuari', required=True
        )
    }

GiscedataPolissaLog()


class GiscedataPolissaModcontractual(osv.osv):
    """Modificació Contractual d'una Pòlissa."""
    _name = 'giscedata.polissa.modcontractual'

    _modcontractual_states_selection = [
        ('esborrany', "Esborrany"),
        ('actiu', "Actiu"),
        ('pendent', "Pendent d'activació"),
        ('baixa', "Baixa"),
        ('baixa2', "Baixa per modificació"),
        ('baixa3', "Baixa per renovació"),
        ('baixa4', "Baixa per nova pòlissa")
    ]
    
    _tipus_modificacio_contractual = [
        ('alta', 'Alta'),
        ('baixa', 'Baixa'),
        ('mod', 'Modificació'),
        ('reno', 'Renovació')
    ]

    def unlink(self, cursor, uid, ids, context=None):
        """Comprovem que es pot eliminar la modificació contractual.
        """
        msg_error = ''
        for modcon in self.browse(cursor, uid, ids, context):
            if modcon.state != 'pendent':
                msg_error += _(u'- La modificació contractual %s de la pòlissa '
                               u'%s no es pot eliminar. Està en estat %s.\n' %
                               (modcon.name, modcon.polissa_id.name,
                                modcon.state))
        if msg_error:
            raise osv.except_osv('Error', msg_error, 'error')
        res = super(GiscedataPolissaModcontractual, self).unlink(cursor, uid,
                                                                 ids, context)
        return res

    def get_fields_for_polissa(self, cursor, uid, context=None):
        """Retorna una llista de camps per llegir a la pòlissa."""
        res_fields = self.fields_get_keys(cursor, uid)
        fields_to_delete = ['id',
                            'name',
                            'polissa_id',
                            'modcontractual_ant',
                            'modcontractual_seg',
                            'active',
                            'state',
                            'potencies_periode',
                            'tipus',
                            'observacions']
        
        for field in fields_to_delete:
            if field in res_fields:
                res_fields.remove(field)
        
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_fields = polissa_obj.fields_get_keys(cursor, uid)
        
        # Es converteix a llista perquè esborrem items de la llista que
        # estem iterant. D'aquesta forma tenim una copia
        for field in list(res_fields):
            if field not in polissa_fields:
                res_fields.remove(field)
        
        return res_fields

    def get_data_final(self, cursor, uid, data_inici, context=None):
        '''Obte la data final d'una modificacio contractual'''

        # Hi sumem 364 o 365 dies segons lo acordat amb Sóller 
        # (per mostrar intèrvals tancats)
        dies_durada_contracte = int(self.pool.get('res.config').get(cursor,
                        uid, 'giscedata_polissa_contracte_durada', 364))
        year, month, day = map(int, data_inici.split('-'))
        data_final = (date(year, month, day)
                      + timedelta(days=dies_durada_contracte))
        days = (calendar.isleap(year + 1)
                    and data_final >= date(year + 1, 2, 28)
                    and dies_durada_contracte + 1
                    or dies_durada_contracte)
        data_final = (date(year, month, day)
                      + timedelta(days=days)).strftime('%Y-%m-%d')
        return data_final
    
    def get_vals_from_polissa(self, cursor, uid, polissa_id, context=None):
        """Obté els valors per emplenar una modificació contractual.
        
        Valors generals que comparteixen tant distribuidora com comercialitzado-
        ra. Després cada mòdul especific pot modificar aquesta funció per esta-
        blir com s'han de copiar els camps de la modificació contractual.
        """
        fields_to_read = self.get_fields_for_polissa(cursor, uid, context)
        polissa_obj = self.pool.get('giscedata.polissa')
        vals = polissa_obj.read(cursor, uid, [polissa_id], fields_to_read,
                                context)[0]
        # Transformem els many2one (id, name) a id
        for field, value in vals.items():
            if isinstance(value, type(())):
                vals[field] = value[0]
            if (isinstance(value, list) and
                self.fields_get(cursor, uid, [field])[field]['type'] == 'many2many'):
                vals[field] = [(6,0, value)]
        # Assignem 'id' a 'polissa_id' i eliminem la clau 'id'
        vals['polissa_id'] = vals['id']
        del vals['id']
        
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        # Hem de generar els periodes per potencia abans generar l'string per
        # la modificació contractual si la pòlissa no en té
        polissa.generar_periodes_potencia()
        # Actualitzem l'objecte polissa ja que l'active record d'openerp
        # no ho fa
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        # Eliminem 'potencies_peride' ja que són una llista d'ids i nosaltres
        # volem un text amb P1: X P2: X ...
        potencies_periode = ["%s: %s" % (p.periode_id.name,
                                p.potencia) for p in polissa.potencies_periode]
        vals['potencies_periode'] = ' '.join(potencies_periode)
        
        # Assignem data_inici a la modificació contractual
        if (polissa.modcontractual_activa 
            and polissa.modcontractual_activa.data_final):
            year, month, day = map(int, 
                            polissa.modcontractual_activa.data_final.split('-'))
            data_inici = (date(year, month, day) 
                          + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            data_inici = polissa.data_alta
        
        # Si té data de baixa ja li assignem (p. exemple contractes de fires)
        if polissa.data_baixa:
            data_final = polissa.data_baixa
        else:
            data_final = self.get_data_final(cursor, uid, data_inici, context)
                          
        vals['data_inici'] = data_inici
        vals['data_final'] = data_final
        
        # Mirem si és del tipus 'alta' o 'modificacio'
        if polissa.modcontractual_activa and len(polissa.modcontractuals_ids):
            vals['tipus'] = 'mod'
        else:
            vals['tipus'] = 'alta'
            
        # Hem de posar la seqüencia que serà el número de modificacions que
        # tinguem fetes
        vals['name'] = len(polissa.modcontractuals_ids) + 1
        
        return vals
    
    def get_potencies_dict(self, cursor, uid, mod_id, context=None):
        """Retorna un diccionari amb les potències contractades.

        El periode com a clau i la potència com a valor. On estan guardades les
        potències és un string amb el següent format:
        PN: X.XXX PN+1: X.XXX PN+2: X.XXX
        """
        if isinstance(mod_id, tuple) or isinstance(mod_id, list):
            mod_id = mod_id[0]
        potencies = {}
        modcon = self.browse(cursor, uid, mod_id, context)
        pots = [p for p in modcon.potencies_periode.split(' ') if p]
        stop = len(pots)
        for i in xrange(0, stop, 2):
            key = pots[i].replace(':', '').strip()
            value = pots[i+1].strip()
            potencies[key] = float(value)
        return potencies

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        ctx = context.copy()

        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = False
        polissa_model = 'giscedata.polissa'
        polissa_obj = self.pool.get(polissa_model)

        # If not last modcon, cannot make the change

        fields_to_read = ['modcontractual_seg', 'polissa_id']
        modcon = self.read(cursor, uid, ids, fields_to_read)[0]

        if modcon['modcontractual_seg']:
            raise osv.except_osv('Error',
                                 _(u"No es pot esborrar aquesta "
                                   u"modificació contractual, ja que no és"
                                   u"la última."))
        else:
            id_pol = modcon['polissa_id'][0]
            res = polissa_obj.undo_last_modcontractual(
                cursor, uid, [id_pol], context
            )

        return res

    def aplicar_modificacio(self, cursor, uid, mod_id, polissa_id=None):
        """Aplica els canvis d'una modificació contractual a una pòlissa."""
        polissa_obj = self.pool.get('giscedata.polissa')
        fields_to_read = self.get_fields_for_polissa(cursor, uid)
        
        changes = self.read(cursor, uid, [mod_id], fields_to_read)[0]
        # Eliminem la clau 'id' que sempre la retorna al fer un read
        changes['modcontractual_activa'] = changes['id']
        del changes['id']
        # Hem de tenir en compte que els many2one retornen sempre 
        # tuple(id, name)
        for field, value in changes.items():
            if isinstance(value, type(())):
                changes[field] = value[0]
            if (isinstance(value, list) and
                self.fields_get(cursor, uid, [field])[field]['type'] == 'many2many'):
                changes[field] = [(6,0, value)]
                
        if not polissa_id:
            polissa_id = self.browse(cursor, uid, mod_id).polissa_id.id
            
        # Apliquem aquests canvis
        polissa_obj.write(cursor, uid, [polissa_id], changes)
        
        # Generem les potències
        polissa_obj.generar_periodes_potencia(cursor, uid, [polissa_id],
                                              context={'force_genpot': True})

        # Assignem les potencies de la modificació contractual
        potencies = self.get_potencies_dict(cursor, uid, mod_id)
        for periode in polissa_obj.browse(cursor, uid,
                                          polissa_id).potencies_periode:
            periode.write({'potencia': potencies.get(periode.periode_id.name,
                                                     periode.potencia)})
        return True
    
    def append_modificacio(self, cursor, uid, mod_id):
        """Afegeix una modificació contractual a la llista i actualitza els
        camps anterior i següent."""
        modcontractual = self.browse(cursor, uid, mod_id)
        if modcontractual.polissa_id.modcontractual_activa:
            mod_activa = modcontractual.polissa_id.modcontractual_activa
            mod_activa.write({'modcontractual_seg': mod_id})
            modcontractual.write({'modcontractual_ant': mod_activa.id})
        return True

    def renovar(self, cursor, uid, ids, context=None):
        """Renovem el contracte amb els valors actuals de la pòlissa.
        """
        fields_read = ['polissa_id', 'observacions', 'data_final']
        for modcontract in self.read(cursor, uid, ids, fields_read, context):
            vals = self.get_vals_from_polissa(cursor, uid,
                                              modcontract['polissa_id'][0],
                                              context)
            if modcontract['observacions']:
                modcontract['observacions'] += '\n'
            else:
                modcontract['observacions'] = ''
            modcontract['observacions'] += _(
                u'* Renovació automàtica a %s. Data final anterior %s. '
                u'Nova data final %s' % (
                '/'.join(reversed(vals['data_inici'].split('-'))),
                '/'.join(reversed(modcontract['data_final'].split('-'))),
                '/'.join(reversed(vals['data_final'].split('-'))),
                )
            )
            update = {
                'data_final': vals['data_final'],
                'observacions': modcontract['observacions']
            }
            self.write(cursor, uid, modcontract['id'],
                       update, {'sync': False})
        return True

    # Workflow Stuff
    def wkf_esborrany(self, cursor, uid, ids):
        """Estat esborrany."""
        for modcontractual in self.browse(cursor, uid, ids):
            self.append_modificacio(cursor, uid, modcontractual.id)
        self.write(cursor, uid, ids,
                   {'state': 'esborrany'},
                   {'sync': False})
        return True
        
    def wkf_pendent(self, cursor, uid, ids):
        """Estat pendent."""
        self.write(cursor, uid, ids,
                   {'state': 'pendent'},
                   {'sync': False})
        return True
        
    def wkf_actiu(self, cursor, uid, ids):
        """Activem la modificació contractual."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_id = modcontractual.id
            modcontractual.polissa_id.write({'modcontractual_activa': mod_id},
                                            {'sync': False})
            # Escrivim l'estat per tal que si hi ha modcontractual_ant vegi
            # que hem activat la següent
            modcontractual.write({'state': 'actiu', 'active': 1},
                                 {'sync': False})
            # Enviem un signal de guardar a l'anterior si n'hi ha per tal
            # que comprovi les condicions del workflow
            if modcontractual.modcontractual_ant:
                modcontractual.modcontractual_ant.write({})

        return True
    
    def cnd_activar_contracte(self, cursor, uid, ids):
        """Comprovem la condició de contracte actiu."""
        for mod_contractual in self.browse(cursor, uid, ids):
            if mod_contractual.data_inici <= time.strftime('%Y-%m-%d'):
                return True
            else:
                return False
    
    def cnd_baixa(self, cursor, uid, ids):
        """Comprovem que la pòlissa es pot donar de baixa.
        
        La baixa normal és quan no hi ha cap modificació contractual
        següent."""
        for modcontractual in self.browse(cursor, uid, ids):
            if not modcontractual.modcontractual_seg:
                return True
            else:
                return False
    
    def wkf_baixa(self, cursor, uid, ids):
        """Estat baixa."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa', 'active': 0},
                   {'sync': False})
        return True
    
    def cnd_baixa2(self, cursor, uid, ids):
        """Comprovació baixa2 (Baixa per modificació).
        
        Si la modificació té una següent és del tipus 'modificació' vol dir
        que aquesta és baixa per modificació
        """
        for modcontractual in self.browse(cursor, uid, ids):
            mod_seg = modcontractual.modcontractual_seg
            if mod_seg and mod_seg.state == 'actiu' and mod_seg.tipus == 'mod':
                return True
            else:
                return False
    
    def wkf_baixa2(self, cursor, uid, ids):
        """Estat baixa2  (Baixa per modificació)."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa2', 'active': 0},
                   {'sync': False})
        return True
    
    
    def cnd_baixa3(self, cursor, uid, ids):
        """Comprovem que és baixa per renovació.
        
        La pòlissa ha de tenir una modificació següent i aquesta ha de ser del
        tipus Renovació (reno)."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_seg = modcontractual.modcontractual_seg
            if mod_seg and mod_seg.state == 'actiu' and mod_seg.tipus == 'reno':
                return True
            else:
                return False
            
    def wkf_baixa3(self, cursor, uid, ids):
        """Estat baixa3 ((Baixa per renovació)."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa3', 'active': 0},
                   {'sync': False})
        return True
    
    def cnd_baixa4(self, cursor, uid, ids):
        """Comprovació baixa per Nova pòlissa.
        
        La pòlissa ha de tenir una modificació següent que apunti a una altre
        pòlissa i que sigui del tipus alta."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_seg = modcontractual.modcontractual_seg
            if mod_seg and mod_seg.tipus == 'alta' and \
            mod_seg.state == 'actiu' and \
            mod_seg.polissa_id.id != modcontractual.polissa_id.id:
                return True
            else:
                return False
    
    def wkf_baixa4(self, cursor, uid, ids):
        """Estat baixa4 ((Baixa per Pòlissa nova)."""
        self.write(cursor, uid, ids,
                   {'state': 'baixa4', 'active': 0},
                   {'sync': False})
        return True
        
    def wkf_cancel(self, cursor, uid, ids):
        """Estat cancel·lat."""
        self.write(cursor, uid, ids,
                   {'state': 'cancel'},
                   {'sync': False})
        return True

    def get_modcon_strings(self, cursor, uid, ids):
        """Plantilla pel missatge d'accions a les modificacions contractuals.
        """
        msg = []
        for mod in self.browse(cursor, uid, ids):
            msg += [_(u"* Pòlissa %s - %s" % (mod.polissa_id.name,
                                             mod.polissa_id.titular.name))]
            msg += [_(u"    Modificació contractual %s. Inici: %s Final: %s "
                      % (mod.name, mod.data_inici, mod.data_final))]
        return msg

    def send_action_message(self, cursor, uid, emails_to, requests_to, subject,
                            message):
        """Envia el missatge per correu o request.
        """
        user_obj = self.pool.get('res.users')
        req_obj = self.pool.get('res.request')
        if emails_to:
            email_from = user_obj.browse(cursor, uid, uid).address_id.email
            email_send(email_from, emails_to, subject, message)
        if requests_to:
            vals = {
                'name': subject,
                'body': message,
                'act_from': uid,
            }
            for login in requests_to:
                act_to = user_obj.search(cursor, uid, [('login', '=', login)])
                if act_to:
                    vals.update({'act_to': act_to[0]})
                    req_obj.create(cursor, uid, vals)

    def get_contractes_action_message(self, cursor, uid, context=None):
        """Escrivim el missatge per les accions que volem fer.

        El missatge retornarà diferents resultats segons les accions que es
        vulguin fer. Per això s'haurà de passar via context un clau 'actions'
        amb un string amb les inicials de les accions que es volen dur a terme.
        'A': Altes, 'B': Baixes, 'R': Renovació. (Ex. {'actions': 'RAB'} per
        fer-les totes)
        """
        if not context:
            context = {}
        actions = context.get('actions', '')
        if not actions:
            return ''
        msg = []
        if 'B' in actions:
            # Missatge per les baixes que es faran
            ids = self.get_contractes_baixa(cursor, uid, context)
            if ids:
                if msg:
                    msg += ['', '']
                msg += [_(u'**** BAIXES (%s) ****' % len(ids))]
                msg += self.get_modcon_strings(cursor, uid, ids)
        if 'A' in actions:
            # Missatge per les altes que es faran
            ids = self.get_contractes_activar(cursor, uid, context)
            if ids:
                if msg:
                    msg += ['', '']
                msg += [_(u'**** ALTES (%s) ****' % len(ids))]
                msg += self.get_modcon_strings(cursor, uid, ids)
        if 'R' in actions:
            # Missatge per les renovacions que es faran
            ids = self.get_contractes_renovar(cursor, uid, context)
            if ids:
                if msg:
                    msg += ['', '']
                msg += [_(u'**** RENOVACIONS (%s) ****' % len(ids))]
                msg += self.get_modcon_strings(cursor, uid, ids)
        return '\n'.join(msg)

    def _cronjob_message(self, cursor, uid, data=None, context=None):
        """CronJob per enviar un correu amb les accions que es portaran a terme.
        """
        if not data:
            data = {}
        if not context:
            context = {}
        emails_to = filter(lambda a: bool(a),
                           map(str.strip, data.get('emails_to', '').split(',')))
        requests_to = filter(lambda a: bool(a),
                             map(str.strip,
                                 data.get('requests_to', '').split(',')))
        execute_date = (datetime.now()
                        + timedelta(days=context.get('delay_days', 0)))
        subject = _(u"Accions planificades ERP")
        message = _(u"Aquestes accions es produiran a partir de la data %s\n\n"
                    % execute_date.strftime('%d/%m/%Y'))
        message += self.get_contractes_action_message(cursor, uid, context)
        self.send_action_message(cursor, uid, emails_to, requests_to, subject,
                                 message)
        return True

    def _cronjob_batch_actions(self, cursor, uid, data=None, context=None):
        """CronJob per fer totes les accions batch sobre les pòlisses.

        Ho fem amb un sol cronjob per evitar problemes en les transaccions a
        la base de dades. Amb una sola funció i un sol cron es farà tot amb la
        mateixa transacció."""
        if not data:
            data = {}
        if not context:
            context = {}
        emails_to = filter(lambda a: bool(a),
                           map(str.strip, data.get('emails_to', '').split(',')))
        requests_to = filter(lambda a: bool(a),
                             map(str.strip,
                                 data.get('requests_to', '').split(',')))
        subject = _(u"Resultat accions batch en modificacions contractuals")
        msg = []
        # 1. Donem de baixa els contractes que s'han configurat
        msg += [_(u'**** BAIXES ****')]
        msg += self._cronjob_baixa_contractes(cursor, uid, data, context)
        # 2. Activem els nous
        msg += ['', '']
        msg += [_(u'**** ALTES ****')]
        msg += self._cronjob_activar_contractes(cursor, uid, data, context)
        # 3. Renovem els actius que caduquin
        msg += ['', '']
        msg += [_(u'**** RENOVACIONS ****')]
        msg += self._cronjob_renovar_contractes(cursor, uid, data, context)
        message = u'\n'.join(msg)
        self.send_action_message(cursor, uid, emails_to, requests_to, subject,
                                 message)
        return True

    def get_contractes_activar(self, cursor, uid, context=None):
        """Busquem quins contractes compleixen la condició que s'activaran.

        """
        if not context:
            context = {}
        delay = context.get('delay_days', 1)
        act_date = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'pendent'),
                         ('data_inici', '<=', act_date),
                         ('polissa_id.active', '=', 1)]
        ids = self.search(cursor, uid, search_params, order="data_inici asc",
                          context={'active_test': False})
        return ids

    # Cada dia a les 00:00 es cridarà aquesta funció per comprovar si hi ha 
    # contractes pendents d'aplicar, en cas que n'hi hagi desactivarà el vell
    # i aplicarà el nou.
    def _cronjob_activar_contractes(self, cursor, uid, data=None, context=None):
        """Activem totes les modificacions contractuals pendents."""
        if not data:
            data = {}
        if not context:
            context = {}
        ids = self.get_contractes_activar(cursor, uid,
                                          context={'delay_days': 0})
        # Obrim un cursor per cada un i així podem avancem
        msg = []
        for mc_id in ids:
            cursor_tmp = pooler.get_db_only(cursor.dbname).cursor()
            try:
                self.write(cursor_tmp, uid, [mc_id], {'active': 1},
                           context={'sync': False})
                self.aplicar_modificacio(cursor_tmp, uid, mc_id)
                cursor_tmp.commit()
            except Exception, e:
                msg += self.get_modcon_strings(cursor, uid, [mc_id])
                msg += [u'    ERROR: %s' % e]
                cursor_tmp.rollback()
            finally:
                cursor_tmp.close()
        if not msg:
            msg = [_(u"Acció feta correctament.")]
        return msg

    def get_contractes_renovar(self, cursor, uid, context=None):
        """Retorna els ids dels contractes que es renovaran.
        """
        if not context:
            context = {}
        # Això ens dirà que s'han de renovar en aquests moments
        delay = context.get('delay_days', 0)
        day = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'actiu'),
                         ('data_final', '<=', day),
                         ('polissa_id.active', '=', 1),
                         ('polissa_id.renovacio_auto', '=', 1),
                         ('modcontractual_seg', '=', False)]
        ids = self.search(cursor, uid, search_params, order="data_inici asc",
                          context={'active_test': False})
        return ids

    # Cada dia a les 00:00 es cridarà aquesta funció per comrpvoar si hi ha
    # contractes que caduquin, en aquest cas es crearà un contracte nou de
    # renovació
    def _cronjob_renovar_contractes(self, cursor, uid, data=None, context=None):
        """Busquetem totes les modificacions contractuals que caduguin avui per
        renovar-les."""
        # Busquem totes els modificacions contractuals que estiguin actives i
        # que data_final sigui ahir i que tinguin activada la renovació auto-
        # màtica. Com que el cronjob s'executa a les 00:00 esteriem perdent
        # un dia de contracte
        if not data:
            data = {}
        if not context:
            context = {}
        ids = self.get_contractes_renovar(cursor, uid,
                                          context={'delay_days': -1})
        msg = []
        for mc_id in ids:
            cursor_tmp = pooler.get_db_only(cursor.dbname).cursor()
            try:
                self.renovar(cursor_tmp, uid, [mc_id], context={'sync': False})
                cursor_tmp.commit()
            except Exception, e:
                msg += self.get_modcon_strings(cursor, uid, [mc_id])
                msg += [u'    ERROR: %s' % e]
                cursor_tmp.rollback()
            finally:
                cursor_tmp.close()
        if not msg:
            msg = [_(u"Acció feta correctament.")]
        return msg

    def get_contractes_baixa(self, cursor, uid, context=None):
        """Retorna els contractes que s'han de donar de baixa.
        """
        if not context:
            context = {}
        delay = context.get('delay_days', 0)
        day = (datetime.now() + timedelta(days=delay)).strftime('%Y-%m-%d')
        search_params = [('state', '=', 'actiu'),
                         ('data_final', '<=', day),
                         ('polissa_id.active', '=', 1),
                         ('polissa_id.renovacio_auto', '=', 0)]
        ids = self.search(cursor, uid, search_params, order="data_final asc",
                          context={'active_test': False})
        return ids

    def _cronjob_baixa_contractes(self, cursor, uid, data=None, context=None):
        """Busquem totes les modificacions contractuals que s'han de donar
        de baixa automàticament."""
        if not data:
            data = {}
        if not context:
            context = {}
        ids = self.get_contractes_baixa(cursor, uid, context={'delay_days': -1})
        msg = []
        for modcontractual in self.read(cursor, uid, ids, ['polissa_id']):
            cursor_tmp = pooler.get_db_only(cursor.dbname).cursor()
            try:
                polissa_id = modcontractual['polissa_id'][0]
                wf_service = netsvc.LocalService('workflow')
                wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                        'baixa', cursor_tmp)
                cursor_tmp.commit()
            except Exception, e:
                msg += self.get_modcon_strings(cursor, uid,
                                               [modcontractual['id']])
                msg += [u'    ERROR: %s' % e]
                cursor_tmp.rollback()
            finally:
                cursor_tmp.close()
        if not msg:
            msg = [_(u"Acció feta correctament.")]
        return msg

    # Constraints
    def _cnt_check_date(self, cursor, uid, ids):
        """Comprova que no hi hagi dates superposades."""
        for mod_contrac in self.browse(cursor, uid, ids):
            # Busquem si ja hi ha un contracte solapat 
            # (Fem servir la funció OVERLAPS de PostgreSQL)
            for contracte in mod_contrac.polissa_id.modcontractuals_ids:
                if contracte.id != mod_contrac.id:
                    cursor.execute("""select (date %s, date %s) 
                    overlaps (date %s, date %s)""", (mod_contrac.data_inici, 
                                                     mod_contrac.data_final, 
                                                     contracte.data_inici, 
                                                     contracte.data_final))
                    res = cursor.fetchone()
                    if res and res[0]:
                        return False
        return True

    def _cnt_check_gaps(self, cursor, uid, ids):
        """Comprova que no hi hagi forats entre contractes."""
        for mod_contract in self.browse(cursor, uid, ids):
            # Només ho comprovem si la modificació contractual
            # té contracte anterior
            if mod_contract.modcontractual_ant:
                # Restem la data_final de l'anterior de la data_inici 
                # de l'actual si és més gran que 1 vol dir que hi ha 
                # més d'un dia de diferència
                cursor.execute("select date %s - date %s", 
                               (mod_contract.data_inici, 
                                mod_contract.modcontractual_ant.data_final))
                dies = cursor.fetchone()[0]
                if dies > 1:
                    return False
        return True
    
    # TODO: Fer tests       
    def _cnt_check_cups(self, cursor, uid, ids):
        for mod_contract in self.browse(cursor, uid, ids):
            if mod_contract.state == 'active':
                for contracte in mod_contract.cups.modcontractuals_ids:
                    if (mod_contract.id != contracte.id 
                        and contracte.state in ('actiu', 'baixa', 
                                                'baixa2', 'baixa3')):
                        year, month, day = map(int,
                                             contracte.data_final.split('-'))
                        data_final = (date(year, month, day) 
                                      + timedelta(days=1)).strftime('%Y-%m-%d')
                        cursor.execute("""select (date %s, date %s) 
                    overlaps (date %s, date %s)""", (mod_contract.data_inici, 
                                                     mod_contract.data_final, 
                                                     contracte.data_inici, 
                                                     data_final))
                        res = cursor.fetchone()
                        if res and res[0]:
                            return False
        return True

    _STORE_PROC_AGREE_FIELDS = {
        'giscedata.polissa.modcontractual': (lambda self, cursor, uid, ids,
                                             context=None: ids,
                                             ['tensio', 'potencia', 'tarifa',
                                              'potencies_periode'],
                                             20),
    }

    _columns = {
        'name': fields.char('Codi modificació', size=64, readonly=True,
                            required=True),
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True, ondelete='cascade',
                                      select=True),
        'modcontractual_ant': fields.many2one(
                                    'giscedata.polissa.modcontractual',
                                    'Modificació anterior'),
        'modcontractual_seg': fields.many2one(
                                    'giscedata.polissa.modcontractual',
                                    'Modificació següent', ondelete="set null"),
        'active': fields.boolean('Activa', readonly=True, required=True),
        'state': fields.selection(_modcontractual_states_selection, 'Estat',
                                  required=True, readonly=True),
        'tipus': fields.selection(_tipus_modificacio_contractual, 'Tipus',
                                  required=True),
        # Camps comuns entre comercialitzadora i distribuidora
        'cups': fields.many2one('giscedata.cups.ps', 'CUPS', required=True),
        'titular': fields.many2one('res.partner', 'Titular'),
        'tarifa': fields.many2one('giscedata.polissa.tarifa', 'Tarifa',
                                  required=True),
        'potencia': fields.float('Potència contractada (kW)', digits=(16,3),
                                 required=True),
        'tensio_normalitzada': fields.many2one('giscedata.tensions.tensio',
                                    'Tensió normalitzada', required=True),
        'tensio': fields.integer('Tensió (V)', required=True),
        'potencies_periode': fields.char('Potències contractades', size=256,
                                         required=False, readonly=True),
        'observacions': fields.text('Observacions'),
        'data_inici': fields.date('Data inici', required=True, select=True),
        'data_final': fields.date('Data final', select=True),
        'data_final_pre_baixa': fields.date('Data final (pre-baixa)'),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE', ondelete='restrict'),
        'data_firma_contracte': fields.datetime('Data firma contracte'),
        'tg': fields.selection(TG_OPERATIVA, u"Telegestió",
                               help=u'Telegestió operativa amb o sense corba '
                                    u'de càrrega (CCH)'),
        'autoconsumo': fields.selection(
            TIPO_AUTOCONSUMO, u"Autoconsum", readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)],
            },
            help=u'Tipus de autoconsum del contracte segons RD. 900/2015'
        ),
        'agree_tensio': fields.function(get_nivell_agregacio, type='char',
                                        fnct_search=None, string="Codi Tensió",
                                        method=False, multi='agree', size=2,
                                        store=_STORE_PROC_AGREE_FIELDS,
                                        readonly=True),
        'agree_tarifa': fields.function(get_nivell_agregacio, type='char',
                                        fnct_search=None, string="Codi Tarifa",
                                        method=False, multi='agree', size=2,
                                        store=_STORE_PROC_AGREE_FIELDS,
                                        readonly=True),
        'agree_dh': fields.function(get_nivell_agregacio, type='char',
                                    fnct_search=None, string="Codi DH",
                                    method=False, multi='agree', size=2,
                                    store=_STORE_PROC_AGREE_FIELDS,
                                    readonly=True),
        'agree_tipus': fields.function(get_nivell_agregacio, type='char',
                                       fnct_search=None, string="Tipus Punt",
                                       method=False, multi='agree', size=2,
                                       store=_STORE_PROC_AGREE_FIELDS,
                                       readonly=True),
        'nocutoff': fields.many2one('giscedata.polissa.nocutoff',
                                    'Subministrament no tallable'),
        'contract_type': fields.selection(
                        CONTRACT_TYPES, 'Tipus de contracte', required=True,
                        help = "Distintos tipos de contratos. Ver REAL DECRETO 1164/2001"
                                        ),
        'tipus_vivenda': fields.selection(
            [('habitual', 'Habitual'), ('no_habitual', 'No habitual')],
            string='Tipus vivenda', readonly=True,
        )
    }

    _constraints = [
        (_cnt_check_date, 'Ja existeix un contracte en aquestes dates',
         ['data_inici', 'data_final']),
        (_cnt_check_gaps, 'Hi ha dies de separació entre contractes',
         ['data_inici']),
        (_cnt_check_cups, 
         'Ja hi ha un CUPS en aquestes dates a un altre contracte', ['cups']),
    ]
    
    _sql_constraints = [
        ('date_coherence',
         "CHECK (data_inici <= coalesce(data_final, '2038-01-01'))",
         'data_inici ha de ser anterior a data_final'),
    ]
    
    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'esborrany',
        'tg': lambda *a: '2',
        'autoconsumo': lambda *a: '00',
        'contract_type': lambda *a: '01',
    }
    
    _order = 'data_final desc'
    
GiscedataPolissaModcontractual()

# Un cop creada la taula modificació contractual podem posar l'activa a la
# pòlissa, sino per dependències no ens deixa crear les taules
class GiscedataPolissaExtend(osv.osv):
    """Extensió per afegir el la modificació contractual activa."""
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'
    
    _columns = {
        'modcontractual_activa': fields.many2one(
                                        'giscedata.polissa.modcontractual',
                                        'Modificació contractual actual',
                                        readonly=True),
    } 

GiscedataPolissaExtend()

class GiscedataPolissaPotenciaContractadaPeriode(osv.osv):
    """Potències contractades per periode."""
    _name = 'giscedata.polissa.potencia.contractada.periode'
    _rec_name = 'periode_id'

    _columns = {
      'periode_id': fields.many2one('giscedata.polissa.tarifa.periodes',
                                    'Periode', required=True, readonly=True),
      'polissa_id': fields.many2one('giscedata.polissa', 'Polissa',
                                    required=True, ondelete='cascade'),
      'potencia': fields.float('Potencia contractada', digits=(15,3),
                               required=True),
      'active': fields.boolean('Activa'),
    }

    _defaults = {
      'active': lambda *a: 1,
    }

GiscedataPolissaPotenciaContractadaPeriode()
