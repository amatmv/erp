# -*- coding: utf-8 -*-
from datetime import timedelta, datetime

from osv import osv, fields


class WizardAltaBaixaAutoconsum(osv.osv_memory):

    _name = 'wizard.alta.baixa.autoconsum'

    def action_donar_alta_autoconsum(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        cups_autoconsum_obj = self.pool.get('giscedata.autoconsum.cups.autoconsum')
        wiz_vals = self.read(cursor, uid, ids, ['data', 'autoconsum_id'])[0]
        cups_id = context['active_id']

        cups_autoconsum_obj.alta_cups_autoconsum(cursor, uid, ids, cups_id, wiz_vals['autoconsum_id'],
                                                 wiz_vals['data'], context)
        return {}

    def action_donar_baixa_autoconsum(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        cups_autoconsum_obj = self.pool.get('giscedata.autoconsum.cups.autoconsum')
        wiz_vals = self.read(cursor, uid, ids, ['data', 'autoconsum_id'])[0]
        cups_id = context['active_id']

        cups_autoconsum_obj.baixa_cups_autoconsum(cursor, uid, ids, cups_id, wiz_vals['autoconsum_id'],
                                                  wiz_vals['data'], context)
        return {}

    def onchange_autoconsum_id(self, cursor, uid, ids, autoconsum_id, context={}):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        found = False
        cups_autoconsum_obj = self.pool.get('giscedata.autoconsum.cups.autoconsum')
        cups_autoconsum_ids = cups_autoconsum_obj.search(cursor, uid, [('autoconsum_id', '=', autoconsum_id)])
        for ca_id in cups_autoconsum_ids:
            vals = cups_autoconsum_obj.read(cursor, uid, ca_id, ['cups_id', 'data_final'])
            if vals['cups_id'][0] == context['active_id'] and not vals['data_final']:
                found = True
        res.update({'value': {'is_donat_alta': found}})
        return res

    _columns = {
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum', size=64, required=True),

        'is_donat_alta': fields.boolean('Està donat d\'alta'),

        'data': fields.date("Data", required=True),
    }

    _defaults = {
        'is_donat_alta': lambda *a: False,
    }


WizardAltaBaixaAutoconsum()
