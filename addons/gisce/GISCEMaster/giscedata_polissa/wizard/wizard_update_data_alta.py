# -*- encoding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import calendar
import time
from datetime import date, datetime, timedelta


class WizardUpdateDataAlta(osv.osv_memory):

    _name = 'wizard.update.data.alta'

    def check_we_can(self, cursor, uid, ids, context=None):
        '''do some checks prior to update the date'''

        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        wizard = self.browse(cursor, uid, ids[0])

        #As much as one modcontractual
        search_params = [('polissa_id', '=', wizard.polissa_id.id)]
        modcontractual_number = modcont_obj.search_count(cursor, uid,
                                                         search_params,
                                                         context=context)
        if modcontractual_number > 1:
            raise osv.except_osv(_('Error'),
                                 _(u"No es pot actualitzar una pòlissa "
                                   u"amb més d'una modificació contractual"))

        search_params.append(('invoice_id.journal_id.code', '=', 'ENERGIA'))

        #No invoices allowed
        factura_number = factura_obj.search_count(cursor, uid,
                                                  search_params,
                                                  context=context)
        if factura_number > 0 and wizard.check_invoices:
            raise osv.except_osv(_('Error'),
                                 _(u"No es pot actualitzar una pòlissa "
                                   u"amb factures generades"))
        return True

    def action_update_data(self, cursor, uid, ids, context=None):
        '''update data_alta in polissa
        and data_inici in modcontractual'''

        polissa_obj = self.pool.get('giscedata.polissa')
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        wizard = self.browse(cursor, uid, ids[0])
        polissa = wizard.polissa_id

        if not context:
            context = {}
        context.update({'sync': False,
                        'active_test': False})
        #Do some checks
        self.check_we_can(cursor, uid, ids, context=context)

        polissa.write({'data_alta': wizard.data_alta},
                      context=context)
        #Update data_inici and data_final in modcontractual
        if polissa.modcontractual_activa:
            modcontractual = polissa.modcontractual_activa
            data_inici = wizard.data_alta
            data_final = modcont_obj.get_data_final(cursor, uid,
                                                    data_inici, context)
            modcontractual.write({'data_inici': data_inici,
                                  'data_final': data_final},
                                  context=context)

        wizard.write({'state': 'end'})

    _columns = {
        'data_alta': fields.date('Data alta'),
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'State'),
        'check_invoices': fields.boolean('Comprovar factures',
                            help=u"Desmarqui aquesta casella si no vol "
                                 u"comprovar si la pòlissa te "
                                 u"factures emeses"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'check_invoices': lambda *a: True,
    }

WizardUpdateDataAlta()
