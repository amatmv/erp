# coding=utf-8
from __future__ import division

from osv import osv, fields
from enerdata.contracts import normalized_power


UNIT = {
    'W': 1,
    'kW': 1000
}

TENSIONS_MAP = {
    '2X220': '1X220',
    '2X230': '1X230',
    '3X220': '3X220/127',
    '3X230': '3X230/133',
    '3X380': '3X380/220',
    '3X400': '3X400/230',
}


class NormalizedPowerTension(normalized_power.NormalizedPower):

    def voltage_name_converter(self, voltage):
        # If is a key in TENSIONS_MAP
        voltage_name = TENSIONS_MAP.get(voltage, voltage)
        return voltage_name.lower()


    def get_norm_power_by_voltage(self, voltage, pot_min, pot_max):
        # CNMC -> REE Voltage map
        voltage_name = self.voltage_name_converter(voltage)
        #voltage_name = TENSIONS_MAP.get(voltage.lower(), voltage.lower())
        normalized = sorted([
            k for k, v in normalized_power.NORMALIZED_POWERS.items()
                if v[0] == voltage_name
        ])
        res = []
        for norm_pow in normalized:
            if pot_min < norm_pow <= pot_max:
                res.append(norm_pow)
            elif norm_pow > pot_max:
                break
        return res


class WizardSetNormalizedPower(osv.osv_memory):
    _name = 'wizard.set.normalized.power'

    def get_normalized(self, cursor, uid, context=None):
        if context is None:
            context = {}
        normalized = []
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        tensio_obj = self.pool.get('giscedata.tensions.tensio')
        tarifa_id = context.get('tarifa_id')
        if tarifa_id is None:
            tcode = context.get('tarifa_code')
            tarifa_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid, tcode)
        tensio_id = context.get('tensio_id')
        if tensio_id is None:
            pol_id = context.get('pol_id')
            if pol_id:
                pol = polissa_obj.browse(cursor, uid, pol_id, context)
                tensio_id = pol.tensio_normalitzada.id
        if tensio_id and tarifa_id:
            tensio = tensio_obj.browse(cursor, uid, tensio_id)
            tarifa = tarifa_obj.browse(cursor, uid, tarifa_id)
            nt = NormalizedPowerTension()

            voltage = tensio_obj.get_cnmc_name(cursor, uid, tensio.cnmc_code)
            pot_min = tarifa.pot_min * UNIT['kW']
            pot_max = tarifa.pot_max * UNIT['kW']
            normalized = nt.get_norm_power_by_voltage(voltage, pot_min, pot_max)
        return normalized

    def _normalized_power(self, cursor, uid, context=None):
        if context is None:
            context = {}
        normalized = self.get_normalized(cursor, uid, context)
        return [
            (p / UNIT[context.get('unit', 'kW')], p / UNIT[context.get('unit', 'kW')])
            for p in sorted(normalized)
        ]

    def _default_normalized_power(self, cursor, uid, context=None):
        if context is None:
            context = {}
        power = context.get('power')
        if power:
            normalized = self.get_normalized(cursor, uid, context)
            if normalized:
                power_w = min(normalized, key=lambda x: abs(x - power * UNIT[context.get('unit', 'kW')]))
                return power_w / UNIT[context.get('unit', 'kW')]
        return False

    def assign(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        model = self.pool.get(context.get('model'))
        field = context.get('field')
        active_id = context.get('active_id')
        if model and field and active_id:
            wiz = self.browse(cursor, uid, ids[0], context=None)
            obj = model.browse(cursor, uid, active_id)
            if obj:
                obj.write({field: wiz.normalized_power})
        return {}


    _columns = {
        'normalized_power': fields.selection(
            _normalized_power, u'Potència normalitzada'
        )
    }

    _defaults = {
        'normalized_power': _default_normalized_power
    }

WizardSetNormalizedPower()
