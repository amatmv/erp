# -*- coding: utf-8 -*-
from datetime import date, timedelta, datetime
from dateutil.relativedelta import relativedelta
import time
import netsvc
from tools.translate import _
from osv import osv, fields
import wizard


class WizardCanviarDates(osv.osv_memory):
    """Wizard per modificar la data d'una modificació contractual
    """
    _name = 'wizard.canviar.dates'

    def _default_dates(self, cursor, uid, context=None):
        if context is None:
            context = {}
        mod_id = context.get('active_id')
        if mod_id:
            modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
            modcon = modcon_obj.browse(cursor, uid, mod_id)
            res = [
                modcon.data_inici,
                modcon.data_final,
                modcon.data_firma_contracte[:19]
            ]
            return res
        else:
            return False, False

    def _default_data_inici(self, *args):
        return self._default_dates(*args)[0]

    def _default_data_final(self, *args):
        return self._default_dates(*args)[1]

    def _default_data_firma(self, *args):
        return self._default_dates(*args)[2]

    def _back(self, cursor, uid, ids, context=None):
        vals = {
            'state': 'init'
        }
        self.write(cursor, uid, ids, vals, context)

    def sum_days(self, data, days):
        date_d = datetime.strptime(data, '%Y-%m-%d')
        return date_d + relativedelta(days=days)

    def check_modcons(self, cursor, uid, ids, mod_id, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        polissa_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        modcon = modcon_obj.browse(cursor, uid, mod_id, context)
        d_ini = wiz.data_inici
        d_fi = wiz.data_final
        d_ini_ant = None
        d_fi_seg = None
        error_dini_ant = _("La nova data d'inici no pot ser anterior o igual "
                           "a la data d'inici de la modificació "
                           "contractual anterior.")
        error_dfi_post = _("La nova data final no pot ser posterior o igual a "
                           "la data final de la modificació contractual "
                           "següent.")
        new_data_fi = self.sum_days(wiz.data_inici, -1)
        new_data_ini = self.sum_days(wiz.data_final, 1)
        new_alta = datetime.strptime(
            wiz.data_inici, '%Y-%m-%d').strftime('%d/%m/%Y')
        new_final = datetime.strptime(
            wiz.data_final, '%Y-%m-%d').strftime('%d/%m/%Y')
        new_firma = datetime.strptime(
            wiz.data_firma, '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y %H:%M:%S')
        modcon_ant = modcon.modcontractual_ant.id
        modcon_seg = modcon.modcontractual_seg.id
        info = _('Altres modificacions:\n\n')
        info_ant = (_('Data final modificació contractual anterior: %s \n\n') %
                    new_data_fi.strftime('%d/%m/%Y'))
        info_seg = (_('Data inici modificació contractual següent: %s \n\n') %
                    new_data_ini.strftime('%d/%m/%Y'))
        info_alta = (_('Nova data d\'alta de la pòlissa %s: %s \n\n') %
                     (modcon.polissa_id.name, new_alta))
        info_baixa = (_('Nova data de baixa de la pòlissa %s: %s \n\n') %
                      (modcon.polissa_id.name, new_final))
        info_firma = (_('Nova data de firma de la pòlissa %s: %s \n\n') %
                      (modcon.polissa_id.name, new_firma))
        pol_id = modcon.polissa_id.id

        ant_ok = False

        error = _("ERROR: \n\n")
        hi_ha_errors = False

        if modcon.modcontractual_ant:
            d_ini_ant = modcon.modcontractual_ant.data_inici
            if d_ini <= d_ini_ant:
                error += error_dini_ant + '\n\n'
                hi_ha_errors = True
            else:
                ant_ok = True
                info += info_ant

        if modcon.modcontractual_seg:
            d_fi_seg = modcon.modcontractual_seg.data_final
            if d_fi >= d_fi_seg:
                error += error_dfi_post + '\n\n'
                hi_ha_errors = True
            else:
                cursor.execute("""UPDATE giscedata_polissa_modcontractual
                SET data_inici = %s where id = %s""", (new_data_ini, modcon_seg))
                info += info_seg

        if hi_ha_errors:
            return error, ''

        if ant_ok:
            cursor.execute("""UPDATE giscedata_polissa_modcontractual
            SET data_final = %s where id = %s""", (new_data_fi, modcon_ant))

        if modcon.polissa_id.modcontractual_activa.id == modcon.id:
            # Si la modificació contractual està activa, modifiquem
            # la data de firma de la pòlissa
            polissa_obj.write(cursor, uid, pol_id, {
                'data_firma_contracte': wiz.data_firma
            })
            info += info_firma

        if not modcon.modcontractual_ant:
            polissa_obj.write(cursor, uid, pol_id, {
                'data_alta': wiz.data_inici
            })
            info += info_alta

        if not modcon.modcontractual_seg:
            if modcon.polissa_id.data_baixa:
                polissa_obj.write(cursor, uid, pol_id, {
                    'data_baixa': wiz.data_final
                })
                info += info_baixa

        cursor.execute("""UPDATE giscedata_polissa_modcontractual
        SET data_inici = %s, data_final = %s,
        data_firma_contracte = %s
        WHERE id = %s""", (
            wiz.data_inici, wiz.data_final, wiz.data_firma, mod_id
        ))

        return '', info

    def change_date(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        mod_id = context.get('active_id')
        wiz = self.browse(cursor, uid, ids[0], context)
        resum = _("Dates modificades correctament.\n\n"
                  "Data inicial nova: {0}\n"
                  "Data final nova: {1}\n"
                  "Data firma contracte nova: {2}\n\n").format(
            datetime.strptime(wiz.data_inici, '%Y-%m-%d').strftime('%d/%m/%Y'),
            datetime.strptime(wiz.data_final, '%Y-%m-%d').strftime('%d/%m/%Y'),
            datetime.strptime(wiz.data_firma, '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y'),
        )
        error, altres = self.check_modcons(cursor, uid, ids, mod_id, context)
        informacio = resum + altres
        state = 'end'
        if error:
            informacio = error
            state = 'error'
        vals = {
            'state': state,
            'info': informacio
        }
        self.write(cursor, uid, ids, vals, context)

    _columns = {
        'data_inici': fields.date(_("Data inicial"), required=True),
        'data_final': fields.date(_('Data final'), required=True),
        'data_firma': fields.datetime(_('Data firma contracte'), required=True),
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End')], 'Estat'),
        'info': fields.text(_('Informació'), readonly=True),
    }

    _defaults = {
        'data_inici': _default_data_inici,
        'data_final': _default_data_final,
        'data_firma': _default_data_firma,
        'state': lambda *a: 'init'
    }


WizardCanviarDates()
