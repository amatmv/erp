# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import date, timedelta
import time
import netsvc
from tools.translate import _
from osv import osv, fields
import calendar


class GiscedataPolissaCrearContracte(osv.osv_memory):
    """Wizard per crear les modificacions contractuals.
    """
    _name = 'giscedata.polissa.crear.contracte'

    _duracio_selection = [
        ('actual', 'Final del contracte actual'),
        ('nou', 'Nou periode complet de contracte')
    ]

    _accio_selection = [
        ('modificar', 'Modificar el contracte existent'),
        ('nou', 'Crear una modificació contractual nova')
    ]

    def default_observacions(self, cursor, uid, context=None):
        """Obtenim les observacions d'aqesta modificació contractual.
        """
        polissa_id = context.get('active_id', False)
        polissa_obj = self.pool.get('giscedata.polissa')
        contracte_obj = self.pool.get('giscedata.polissa.modcontractual')
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        changes = polissa_obj.get_changes(cursor, uid, polissa.id)
        changes_string = _(u'Canvis realitzats detectats automàticament:\n')
        polissa_fields = polissa_obj.fields_get(cursor, uid,
                            changes.keys(), context)
        for field, change in changes.items():
            if field == 'observacions':
                continue
            for key in change.keys():
                if isinstance(change[key], tuple):
                    change[key] = change[key][1]
            field_name = polissa_fields[field].get('string', field)
            if polissa_fields[field].get('type', False) == 'selection':
                for item in polissa_fields[field]['selection']:
                    if item[0] == change['old']:
                        change['old'] = item[1]
                    if item[0] == change['new']:
                        change['new'] = item[1]
            changes_string += u'• %s: %s → %s\n' % (field_name, change['old'],
                                                  change['new'])
        potencies_periode = ["%s: %s" % (p.periode_id.name,
                                p.potencia) for p in polissa.potencies_periode]
        potencies_periode = ' '.join(potencies_periode)
        modcon_id = context.get('modcon_id', polissa.modcontractual_activa.id)
        potencies_periode_mod_con = contracte_obj.read(
            cursor, uid, modcon_id, ['potencies_periode'])['potencies_periode']

        if potencies_periode != potencies_periode_mod_con:
            label = polissa_obj.fields_get(
                cursor, uid, ['potencies_periode'], context=context
            )['potencies_periode']['string']
            changes_string += u'• %s: %s → %s\n' % (
                label,
                potencies_periode_mod_con,
                potencies_periode
            )
        # si el cridem amb modificacions previament, que les afegeixi al final
        observacions = context.get('observacions', False)
        if observacions:
            changes_string += '\n{0}'.format(observacions)
        return changes_string

    def default_data_inici(self, cursor, uid, context=None):
        """Obtenim la data d'inici.
        """
        config = self.pool.get('res.config')
        if int(config.get(cursor, uid, 'default_crear_contracte_data_final',
                          '0')):
            polissa_id = context.get('active_id', False)
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
            data_final = polissa.modcontractual_activa.data_final
            year, month, day = data_final.split('-')
            data_inici = (date(int(year), int(month), int(day))
                          + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            data_inici = time.strftime('%Y-%m-%d')
        return data_inici

    def default_data_firma(self, cursor, uid, context=None):
        """Obtenim la data d'inici.
        """
        polissa_id = context.get('active_id', False)
        polissa_obj = self.pool.get('giscedata.polissa')
        data_firma = polissa_obj.read(cursor, uid, polissa_id,
                        ['data_firma_contracte'])['data_firma_contracte']
        return data_firma

    def default_polissa_id(self, cursor, uid, context=None):
        """Obtenim la pòlissa que estem modificant.
        """
        return context.get('active_id', False)
    
    def default_state(self, cursor, uid, context=None):
        """Comprovem amb quin estat s'ha d'inicialitzar l'assistent.
        """
        polissa_id = context.get('active_id', False)
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        search_params = [
            ('polissa_id.id', '=', polissa_id),
            ('state', '=', 'pendent'),
        ]
        count = modcont_obj.search(cursor, uid, search_params)
        if count:
            return 'fail'
        else:
            return 'ok'

    def action_crear_contracte(self, cursor, uid, ids, context=None):
        """Creem la modificació contractual.
        """
        if context is None:
            context = {}
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        wizard = self.browse(cursor, uid, ids[0], context)
        polissa = wizard.polissa_id
        wf_service = netsvc.LocalService('workflow')
        vals = {}
        if wizard.accio == 'modificar' or wizard.data_inici == polissa.modcontractual_activa.data_inici:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa.id,
                                    'overwrite_modcontractual', cursor)
            modcontractual_creada = polissa.modcontractual_activa
        else:
            # Mirem si la data_inici és <= data_inici contracte actiu
            # Si: modifiquem la data_final contracte actiu a data_inici -1 dia
            if wizard.data_inici <= polissa.modcontractual_activa.data_final:
                year, month, day = wizard.data_inici.split('-')
                data_final = (date(int(year), int(month), int(day))
                              + timedelta(days=-1)).strftime('%Y-%m-%d')
                polissa.modcontractual_activa.write({'data_final': data_final})
            # Fem la transició del wizard:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa.id,
                                    'contracte', cursor)
            # Agafem l'últim contracte (serà el que hem creat) i hi posem les
            # observacions i la data final
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, uid, wizard.polissa_id.id)
            modcontractual_creada = polissa.modcontractuals_ids[0]
            vals = {'data_final': wizard.data_final,
                    'data_firma_contracte': wizard.data_firma_contracte}
        observacions = ''
        log_line_long = 82  # 80 + 2 \n
        log_line_text = '\n----- %s - %s (%s) %%s\n' % (
            time.strftime('%d/%m/%Y %H:%M'), user.name, user.login
        )
        if modcontractual_creada.observacions:
            observacions += modcontractual_creada.observacions
        observacions += log_line_text % ('-' * (log_line_long
                                                - len(log_line_text)))
        observacions += wizard.observacions
        observacions += context.get("extra_observacions", "")
        vals['observacions'] = observacions
        modcontractual_creada.write(vals)
        # Actualitzem la data de firma del contracte de la pòlissa
        polissa.write({
            'data_firma_contracte': wizard.data_firma_contracte
        })
        return {
            'type': 'ir.actions.act_window_close',
        }

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    def onchange_duracio(self, cursor, uid, ids, data_inici, duracio,
                         context=None):
        """Funció quan canviem la duració.
        """
        polissa_id = context.get('active_id', False)
        polissa_obj = self.pool.get('giscedata.polissa')
        modcont_obj = self.pool.get('giscedata.polissa.modcontractual')
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context)
        res = {'value': {}, 'warning': {}, 'domain': {}}
        if not duracio or not data_inici:
            return res
        # Canviem els valors
        if duracio == 'actual':
            data_final = polissa.modcontractual_activa.data_final
            res['value'].update({'data_final': data_final})
        else:
            data_final = modcont_obj.get_data_final(cursor, uid,
                                                    data_inici, context)
            res['value'].update({'data_final': data_final})
        if data_final <= data_inici:
            res['warning'].update({'title': _('Error'),
                                   'message': _('La data final no pot ser '
                                                'inferior a la inicial.')})
            res['value'].update({'data_final': False})
            return res

        anterior = polissa.check_data_inici_anterior_modcon_activa(
            data_inici, context=context
        )[polissa.id]
        if anterior:
            res['warning'].update(anterior)
            res['value'].update({'data_inici': False, 'data_final': False})
            return res

        igual = polissa.check_data_inici_igual_modcon_activa(
            data_inici, context=context
        )[polissa.id]
        if igual:
            res['warning'].update(igual)
            res['value'].update({
                'data_inici': False,
                'data_final': False,
                'accio': 'modificar'
            })
            return res
        return res

    _columns = {
        'data_inici': fields.date("Data d'activació"),
        'data_final': fields.date('Data final'),
        'data_firma_contracte': fields.datetime('Data firma'),
        'observacions': fields.text("Observacions", required=True),
        'duracio': fields.selection(_duracio_selection, 'Duració'),
        'accio': fields.selection(_accio_selection, 'Acció', required=True),
        'state': fields.selection([('ok', 'OK'), ('fail', 'Fail')], 'State'),
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa',
                                      required=True)
    }

    _defaults = {
        'observacions': default_observacions,
        'data_inici': default_data_inici,
        'data_firma_contracte': default_data_firma,
        'polissa_id': default_polissa_id,
        'state': default_state,
        'accio': lambda *a: 'nou',
    }

GiscedataPolissaCrearContracte()
