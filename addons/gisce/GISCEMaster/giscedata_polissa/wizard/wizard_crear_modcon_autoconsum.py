# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import date, timedelta
import time
import netsvc
from tools.translate import _
from osv import osv, fields


class GiscedataAutoconsumCrearModcon(osv.osv_memory):
    """ Wizard per crear les modificacions contractuals. """
    _name = 'giscedata.autoconsum.crear.modcon'

    _accio_selection = [
        ('modificar', 'Modificar el contracte existent'),
        ('nou', 'Crear una modificació contractual nova')
    ]

    def default_observacions(self, cursor, uid, context=None):
        """Obtenim les observacions d'aqesta modificació contractual.
        """
        autoconsum_id = context.get('active_id', False)
        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        autoconsum = autoconsum_obj.browse(cursor, uid, autoconsum_id, context)
        changes = autoconsum_obj.get_changes(cursor, uid, autoconsum.id)
        changes_string = _(u'Canvis realitzats detectats automàticament:\n')
        autoconsum_fields = autoconsum_obj.fields_get(cursor, uid, changes.keys(), context)
        for field, change in changes.items():
            if field == 'observacions':
                continue
            for key in change.keys():
                if isinstance(change[key], tuple):
                    change[key] = change[key][1]
            field_name = autoconsum_fields[field].get('string', field)
            if autoconsum_fields[field].get('type', False) == 'selection':
                for item in autoconsum_fields[field]['selection']:
                    if item[0] == change['old']:
                        change['old'] = item[1]
                    if item[0] == change['new']:
                        change['new'] = item[1]
            changes_string += u'• %s: %s → %s\n' % (field_name, change['old'], change['new'])

        # si el cridem amb modificacions previament, que les afegeixi al final
        observacions = context.get('observacions', False)
        if observacions:
            changes_string += '\n{0}'.format(observacions)
        return changes_string

    def default_data_inici(self, cursor, uid, context=None):
        """ Obtenim la data d'inici. """
        config = self.pool.get('res.config')
        if int(config.get(cursor, uid, 'default_crear_contracte_data_final', '0')):
            autoconsum_id = context.get('active_id', False)
            autoconsum_obj = self.pool.get('giscedata.autoconsum')
            autoconsum = autoconsum_obj.browse(cursor, uid, autoconsum_id, context)
            data_final = autoconsum.modcontractual_activa.data_final
            year, month, day = data_final.split('-')
            data_inici = (date(int(year), int(month), int(day))
                          + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            data_inici = time.strftime('%Y-%m-%d')
        return data_inici

    def default_autoconsum_id(self, cursor, uid, context=None):
        """Obtenim l'autoconsum que estem modificant.
        """
        return context.get('active_id', False)
    
    def default_state(self, cursor, uid, context=None):
        """Comprovem amb quin estat s'ha d'inicialitzar l'assistent.
        """
        autoconsum_id = context.get('active_id', False)
        modcon_obj = self.pool.get('giscedata.autoconsum.modcontractual')
        search_params = [
            ('autoconsum_id.id', '=', autoconsum_id),
            ('state', '=', 'pendent'),
        ]
        count = modcon_obj.search(cursor, uid, search_params)
        if count:
            return 'fail'
        else:
            return 'ok'

    def action_crear_modcon(self, cursor, uid, ids, context=None):
        """Creem la modificació contractual.
        """
        if context is None:
            context = {}
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        wizard = self.browse(cursor, uid, ids[0], context)
        autoconsum = wizard.autoconsum_id
        wf_service = netsvc.LocalService('workflow')
        vals = {}
        if wizard.accio == 'modificar' or wizard.data_inici == autoconsum.modcontractual_activa.data_inici:
            wf_service.trg_validate(uid, 'giscedata.autoconsum', autoconsum.id,
                                    'overwrite_modcontractual', cursor)
            modcontractual_creada = autoconsum.modcontractual_activa
        else:
            # Mirem si la data_inici és <= data_inici contracte actiu
            # Si: modifiquem la data_final contracte actiu a data_inici -1 dia
            if autoconsum.modcontractual_activa.data_final \
                    and wizard.data_inici <= autoconsum.modcontractual_activa.data_final:
                year, month, day = wizard.data_inici.split('-')
                data_final = (date(int(year), int(month), int(day))
                              + timedelta(days=-1)).strftime('%Y-%m-%d')
                autoconsum.modcontractual_activa.write({'data_final': data_final})
            # Fem la transició del wizard:
            wf_service.trg_validate(uid, 'giscedata.autoconsum', autoconsum.id,
                                    'crear', cursor)
            # Agafem l'últim contracte (serà el que hem creat) i hi posem les
            # observacions i la data final
            autoconsum_obj = self.pool.get('giscedata.autoconsum')
            autoconsum = autoconsum_obj.browse(cursor, uid, wizard.autoconsum_id.id)
            modcontractual_creada = autoconsum.modcontractuals_ids[0]
            if wizard.data_final:
                vals.update({'data_final': wizard.data_final})

        observacions = ''
        log_line_long = 82  # 80 + 2 \n
        log_line_text = '\n----- %s - %s (%s) %%s\n' % (
            time.strftime('%d/%m/%Y %H:%M'), user.name, user.login
        )
        if modcontractual_creada.observacions:
            observacions += modcontractual_creada.observacions
        observacions += log_line_text % ('-' * (log_line_long
                                                - len(log_line_text)))
        observacions += wizard.observacions
        observacions += context.get("extra_observacions", "")
        vals['observacions'] = observacions
        modcontractual_creada.write(vals)
        return {
            'type': 'ir.actions.act_window_close',
        }

    def action_cancel(self, cursor, uid, ids, context=None):
        """ Cancel·lem. """
        return {
            'type': 'ir.actions.act_window_close',
        }

    _columns = {
        'data_inici': fields.date("Data d'activació"),
        'data_final': fields.date('Data final'),
        'observacions': fields.text("Observacions", readonly=True),
        'accio': fields.selection(_accio_selection, 'Acció', required=True),
        'state': fields.selection([('ok', 'OK'), ('fail', 'Fail')], 'State'),
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum', required=True)
    }

    _defaults = {
        'observacions': default_observacions,
        'data_inici': default_data_inici,
        'autoconsum_id': default_autoconsum_id,
        'state': default_state,
        'accio': lambda *a: 'nou',
    }


GiscedataAutoconsumCrearModcon()
