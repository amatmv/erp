# -*- encoding: utf-8 -*-

from osv import osv, fields


class WizardUpdatePotPeriode(osv.osv_memory):

    _name = 'wizard.update.pot.periode'

    def fields_get(self, cursor, uid, fields=None,
                   context=None, read_access=True):
        if context is None:
            context = {}
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        modcon_id = context.get('active_ids', [])
        if modcon_id:
            modcon_id = modcon_id[0]
            modcon = modcon_obj.browse(cursor, uid, modcon_id)

            if modcon.tarifa.autogen_periodes_pot:
                fields = {}

        return super(WizardUpdatePotPeriode,
                   self).fields_get(cursor, uid,
                                    fields=fields,
                                    context=context,
                                    read_access=read_access)

    def fields_view_get(self, cursor, uid, view_id=None,
                        view_type='form', context=None, toolbar=False):
        '''dynamically add fields'''
        if context is None:
            context = {}
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        modcon_id = context.get('active_ids', [])
        if not modcon_id:
            return super(WizardUpdatePotPeriode, self).fields_view_get(
                cursor, uid, view_id, view_type, context, toolbar
            )
        else:
            modcon_id = modcon_id[0]
        modcon = modcon_obj.browse(cursor, uid, modcon_id)
        result = {}

        xml = '''<?xml version="1.0"?>\n'''\
              '''<%s string="Dades">\n\t''' % (view_type,)
        if not modcon.tarifa.autogen_periodes_pot:
            xml += '''<field name="p1_pot" colspan="4"/>\n'''
            xml += '''<field name="p2_pot" colspan="4"/>\n'''
            xml += '''<field name="p3_pot" colspan="4"/>\n'''
            xml += '''<field name="p4_pot" colspan="4"/>\n'''
            xml += '''<field name="p5_pot" colspan="4"/>\n'''
            xml += '''<field name="p6_pot" colspan="4"/>\n'''

        xml += '''<label string="Endavant per continuar"
               colspan="4" rowspan="4"/>'''

        xml += '''<group colspan="4" col="2">'''
        xml += '''<button special="cancel"
        string="Cancel·lar" icon="gtk-cancel"/>'''
        xml += '''<button name="action_update_pot" string="Endavant"
        type="object" icon="gtk-go-forward"/>'''
        xml += '''</group>'''

        xml += '''</%s>''' % (view_type,)

        result['arch'] = xml
        result['fields'] = self.fields_get(cursor, uid, {}, context=context)
        return result

    def action_update_pot(self, cursor, uid, ids, context=None):

        wizard = self.browse(cursor, uid, ids[0])
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        modcon_id = context.get('active_ids', [])[0]
        modcon = modcon_obj.browse(cursor, uid, modcon_id)

        pot_periode = []
        if not modcon.tarifa.autogen_periodes_pot:
            vals = wizard.read(['p1_pot', 'p2_pot', 'p3_pot',
                                'p4_pot', 'p5_pot', 'p6_pot'])[0]
            for periode in modcon.tarifa.periodes:
                if periode.tipus == 'tp' and not periode.agrupat_amb:
                    pot = vals['p%s_pot' % periode.name[-1]] or 0
                    pot_periode.append("%s: %s" % (periode.name, pot))
        else:
            for periode in modcon.tarifa.periodes:
                if periode.tipus == 'tp' and not periode.agrupat_amb:
                    pot_periode.append("%s: %s" % (periode.name,
                                                   modcon.potencia))
        modcon.write({'potencies_periode': ' '.join(pot_periode)})
        return {}

    _columns = {
        'p1_pot': fields.float('P1', digits=(16, 3)),
        'p2_pot': fields.float('P2', digits=(16, 3)),
        'p3_pot': fields.float('P3', digits=(16, 3)),
        'p4_pot': fields.float('P4', digits=(16, 3)),
        'p5_pot': fields.float('P5', digits=(16, 3)),
        'p6_pot': fields.float('P6', digits=(16, 3)),
    }

WizardUpdatePotPeriode()
