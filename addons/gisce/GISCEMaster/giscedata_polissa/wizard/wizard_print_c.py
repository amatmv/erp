# -*- coding: utf-8 -*-
"""Wizard per seleccionar el contracte amb les dades
de la modificació contractual segons data inici.
"""

# pylint: disable=E1101,W0223

import pooler
import wizard


def _c_id(self, cursor, uid, data, context=None):
    """Retorna l'ID del contracte actiu.
    """
    if not context:
        context = {}

    res = {'ids': [], 'parameters': {}}
    model = data.get('model')
    if model == 'giscedata.polissa.modcontractual':
        pool = pooler.get_pool(cursor.dbname)
        mod_obj = pool.get('giscedata.polissa.modcontractual')
        mod = mod_obj.browse(cursor, uid, data['id'], context)
        context['date'] = mod.data_inici
        res['polissa_date'] = context['date']
        res['ids'].append(mod.polissa_id.id)
    else:
        res['ids'].append(data['id'])

    return res


class WizardPrintC(wizard.interface):
    """Definició del wizard
    """
    states = {
        'init': {
            'actions': [],
            'result': {
                'type': 'state',
                'state': 'print_c'
            }
        },
        'print_c': {
            'actions': [_c_id],
            'result': {
                'type': 'print',
                'report': 'giscedata.polissa',
                'get_id_from_action': True, 'state': 'end'
            }
        },
        'end': {
            'actions': [],
            'result': {
                'type': 'state',
                'state': 'end'
            }
        }
    }

WizardPrintC('giscedata.polissa.print_c')
