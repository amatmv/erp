# -*- coding: utf-8 -*-
{
    "name": "Polisses dels clients (Base)",
    "description": """Afegeix les polisses pels clients""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "base",
        "c2c_webkit_report",
        "giscedata_cups",
        "product",
        "giscedata_tensions",
        "jasper_reports",
        "giscemisc_cnae"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_polissa_demo.xml"
    ],
    "update_xml": [
        "giscedata_polissa_workflow.xml",
        "giscedata_autoconsum_workflow.xml",
        "wizard/wizard_canviar_dates_view.xml",
        "wizard/wizard_canviar_dates_autoconsum_view.xml",
        "wizard/wizard_set_normalized_power_view.xml",
        "giscedata_polissa_wizard.xml",
        "wizard/wizard_crear_contracte_view.xml",
        "wizard/wizard_crear_modcon_autoconsum_view.xml",
        "wizard/wizard_update_pot_periode_view.xml",
        "giscedata_polissa_view.xml",
        "partner_view.xml",
        "giscedata_polissa_data.xml",
        "giscedata_polissa_sequence.xml",
        "giscedata_polissa_cronjobs.xml",
        "wizard/wizard_update_data_alta_view.xml",
        "wizard/wizard_update_data_firma_contracte_view.xml",
        "giscedata_polissa_report.xml",
        "giscedata_cups_view.xml",
        "security/giscedata_polissa_security.xml",
        "security/ir.model.access.csv",
        "giscedata_autoconsum_view.xml",
        "wizard/wizard_alta_baixa_autoconsum_view.xml",
    ],
    "active": False,
    "installable": True
}
