# -*- coding: utf-8 -*-
"""Funcions bàsiques per agregacions de pòlisses.
"""
from datetime import datetime


def check_potencia(potencia):
    """Fa que que la potència 0 sigui vàlida.

    >>> check_potencia(1)
    True
    >>> check_potencia(0)
    True
    >>> check_potencia(False)
    False
    >>> check_potencia(True)
    False
    >>> check_potencia(None)
    False
    """
    if potencia is None or isinstance(potencia, bool):
        return False
    return True


def agree_tipus(potencia):
    """Retorna el nivell d'agregació segons el tipus de mesura.

    >>> agree_tipus(False)
    False
    >>> agree_tipus(None)
    False
    >>> agree_tipus(0)
    '05'
    >>> agree_tipus(15)
    '05'
    >>> agree_tipus(50)
    '04'
    >>> agree_tipus(51)
    '03'
    >>> agree_tipus(450)
    '03'
    >>> agree_tipus(451)
    '02'
    >>> agree_tipus(10000)
    '01'
    """
    if not check_potencia(potencia):
        return False
    if potencia >= 10000:
        return '01'
    elif potencia > 450:
        return '02'
    elif 15 < potencia <= 50:
        return '04'
    elif potencia <= 15:
        return '05'
    else:
        return '03'


def agree_tensio(tarifa_tipus, tensio, tarifa=None):
    """Retorna el nivell d'agregació segons la tensió.

    >>> agree_tensio('BT', 220)
    'E0'
    >>> agree_tensio('AT', 1000)
    'E1'
    >>> agree_tensio('AT', 14000)
    'E2'
    >>> agree_tensio('AT', 1000, '6.1A')
    'E1'
    >>> agree_tensio('AT', 14000, '6.1A')
    'E2'
    >>> agree_tensio('AT', 15000, '6.1A')
    'E2'
    >>> agree_tensio('AT', 30000, '6.1B')
    'E2'
    >>> agree_tensio('AT', 36000)
    'E3'
    >>> agree_tensio('AT', 72500)
    'E4'
    >>> agree_tensio('AT', 145000)
    'E5'
    >>> agree_tensio('AT', 220000)
    'E6'
    """
    if not tarifa_tipus:
        return False
    if tarifa_tipus == 'BT':
        return 'E0'
    if not tensio:
        return False
    if 1000 <= tensio < 14000:
        return 'E1'
    elif 14000 <= tensio < 36000:
        return 'E2'
    elif 36000 <= tensio < 72500:
        return 'E3'
    elif 72500 <= tensio < 145000:
        return 'E4'
    elif 145000 <= tensio < 220000:
        return 'E5'
    elif tensio >= 220000:
        return 'E6'


def agree_tarifa(tipus, num_periodes, potencia, tensio, data=None):
    """Retorna el nivell d'agreació de la tarifa.

    >>> agree_tarifa('BT', 1, 10, 220, '2009-06-30')
    '20'
    >>> agree_tarifa('BT', 1, 10, 220)
    '2A'
    >>> agree_tarifa('BT', 2, 10, 220)
    '2A'
    >>> agree_tarifa('BT', 3, 10, 220)
    '2A'
    >>> agree_tarifa('BT', 2, 15, 220)
    '21'
    >>> agree_tarifa('BT', 3, 15, 220)
    '21'
    >>> agree_tarifa('BT', 3, 30, 220)
    '30'
    >>> agree_tarifa('AT', 3, 400, 1000)
    '31'
    >>> agree_tarifa('AT', 6, 400, 1000, '2014-12-31')
    '61'
    >>> agree_tarifa('AT', 6, 400, 20000)
    '6A'
    >>> agree_tarifa('AT', 6, 400, 30000)
    '6B'
    >>> agree_tarifa('AT', 6, 400, 36000)
    '62'
    """
    if not tipus or not num_periodes or not check_potencia(potencia):
        return False
    if not data:
        data = datetime.now().strftime('%Y-%m-%d')
    if tipus == 'BT':
        if potencia <= 10:
            if data < '2009-07-01':
                return '20'
            else:
                return '2A'
        elif 10 < potencia <= 15:
            return '21'
        else:
            return '30'
    else:
        if not tensio:
            return False
        if tensio < 36000:
            if num_periodes == 3:
                return '31'
            else:
                if data < '2015-01-01':
                    return '61'
                elif 1000 <= tensio < 30000:
                    return '6A'
                else:
                    return '6B'
        elif 36000 <= tensio < 72500:
            return '62'
        elif 72500 <= tensio < 145000:
            return '63'
        elif tensio >= 145000:
            return '64'
        else:
            return '65'


def agree_dh(num_periodes):
    """Es passa el número de períodes que té una tarifa i retorna el nivell
    d'agregació per aquesta.

    >>> agree_dh(-1)
    False

    >>> agree_dh(0)
    False

    agree_dh(1)
    'E1'

    agree_dh(2)
    'E2'

    agree_dh(3)
    'E3'

    agree_dh(6)
    'G0'
    """
    if not num_periodes or num_periodes <= 0:
        return False
    if num_periodes <= 3:
        return 'E%s' % num_periodes
    else:
        return 'G0'


if __name__ == "__main__":
    import doctest
    doctest.testmod()