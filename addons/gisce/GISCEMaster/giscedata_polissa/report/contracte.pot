# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-12-21 03:35+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Tarifa"
msgstr ""

msgid "Mòbil"
msgstr ""

msgid "Municipi"
msgstr ""

msgid "Escala"
msgstr ""

msgid "Codi"
msgstr ""

msgid "El Distribuïdor"
msgstr ""

msgid "DADES DEL PUNT DE SUBMINISTRE"
msgstr ""

msgid "Nom/Raó Social"
msgstr ""

msgid "Adreça"
msgstr ""

msgid "Nº"
msgstr ""

msgid "Fax"
msgstr ""

msgid "CONDICIONS TÉCNICO-ECONÒMIQUES"
msgstr ""

msgid "Provincia"
msgstr ""

msgid "DNI/NIF"
msgstr ""

msgid "Tensió nominal"
msgstr ""

msgid "EQUIP DE MESURA"
msgstr ""

msgid "Tipus de via"
msgstr ""

msgid "CNAE"
msgstr ""

msgid "Correu electrònic"
msgstr ""

msgid "Producte"
msgstr ""

msgid "CIF"
msgstr ""

msgid "Telèfon"
msgstr ""

msgid "Codi Parcel·la"
msgstr ""

msgid "Activitat principal"
msgstr ""

msgid "Codi Postal"
msgstr ""

msgid "Planta"
msgstr ""

msgid "CONTRACTE D''ACCÉS A LA XARXA DE DISTRIBUCIÓ DE {0}"
msgstr ""

msgid "COMERCIALITZADOR"
msgstr ""

msgid "Data final:"
msgstr ""

msgid "Segons regulació vigent"
msgstr ""

msgid "El Comercialitzador"
msgstr ""

msgid "Potencia contractrada (kW)"
msgstr ""

msgid "Referencia contracte"
msgstr ""

msgid "Referència Catastral"
msgstr ""

msgid "Data inici:"
msgstr ""

msgid "Modificació contractual"
msgstr ""

msgid "Codi Universal del Punt de Subministre (CUPS)"
msgstr ""

msgid "N. Sèrie equip de mesura"
msgstr ""

msgid "TARIFA D'ACCÉS"
msgstr ""

msgid "Empresa Distribuidora"
msgstr ""

msgid "DADES DEL CLIENT (TITULAR DE LA PÒLISSA DE SUBMINISTRAMENT)"
msgstr ""

msgid "Via"
msgstr ""

msgid "DISTRIBUÏDORA"
msgstr ""

msgid "Porta"
msgstr ""

msgid "Codi Polígon"
msgstr ""

msgid "Lloguer equip de mesura"
msgstr ""

msgid "Localitat"
msgstr ""

msgid "El Client"
msgstr ""

msgid "Província"
msgstr ""

