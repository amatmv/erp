<%
    from datetime import datetime
    setLang(user.context_lang)
    pool = objects[0].pool
    modcon_obj = pool.get('giscedata.polissa.modcontractual')
    tipusBubble=['default','lorem','ipsum','dolor','sit']
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_polissa/report/estils.css"/>
    </head>
    <body>
        %for polissa in objects:
            <%
                modcons = polissa.modcontractuals_ids
                modconsTimeline = modcons[0:5]
            %>

            <div class="dataAct">
                <th>${_(u'Data de l\'informe')}</th>
                <td>${datetime.now().strftime('%d/%m/%Y')}</td>
            </div>

            <h1>${_(u'Modificacions contractuals')}</h1>
            <div class="client">
                <div><h2>${_(u'Informació del client')}</h2></div>
                <table>
                    <tr>
                        <th class="nowrap">${_(u'POLISSA')}</th>
                        <td class="nowrap">${polissa.name}</td>
                        <th class="nowrap">${_(u'CUPS')}</th>
                        <td class="nowrap">${polissa.cups.name}</td>
                    </tr>
                    <tr>
                        <th class="nowrap">${_(u'TITULAR')}</th>
                        <td class="nowrap">${polissa.titular.name}</td>
                        <th class="nowrap" rowspan="2" style="vertical-align: top">${_(u'DIRECCIÓ CUPS')}</th>
                        <td rowspan="2" style="vertical-align: top">${polissa.cups.direccio}</td>
                    </tr>
                    <tr>
                        <th class="nowrap">${_(u'DATA D\'ALTA')}</th>
                        <td class="nowrap">${'{0[2]}/{0[1]}/{0[0]}'.format(polissa.data_alta.split('-'))}</td>
                    </tr>
                </table>
            </div>

            <h2>${_(u'Línia de temps')}</h2>
            <div id="timesheet">
                <!-- JavaScript -->
                <script src="${addons_path}/giscedata_polissa/report/javascripts/timesheet.js" type="text/javascript"></script>
                <script>
                    new Timesheet('timesheet', ${modconsTimeline[-1].data_inici.split('-')[0]}, ${modconsTimeline[0].data_final.split('-')[0]}+1, 643, 2, '-', [
                        %for i in modconsTimeline[::-1]:
                            ['${i.data_inici}', '${i.data_final}', '${i.name}', '${tipusBubble[int(i.name)%len(tipusBubble)-1]}'],
                        %endfor
                    ]);
                </script>
            </div>

            <h2>${_(u'Descripció de les modificacions')}</h2>
            %for i in modcons:
                <div class="observ">
                    <div class="${tipusBubble[int(i.name)%len(tipusBubble)-1]}"></div><h3>${_(u'Modificació')} ${i.name}</h3>

                    %if i.observacions:
                        %for mod in i.observacions[7:].split('* ')[0].split('\n----- '):
                            %if '•' in mod:
                                <p>${_(u'Modificació realitzada a data')} <b>${mod[0:16]}</b>:</p>

                                <table>
                                    <tr>
                                        <th>${_(u'Atribut modificat')}</th>
                                        <th>${_(u'Valor original')}</th>
                                        <th>${_(u'Valor nou')}</th>
                                    </tr>
                                    %for canvi in mod.split('• ')[1:]:
                                        <tr>
                                            <td>${canvi.split(': ')[0]}</td>
                                            <td>${canvi.split(': ')[1].split(' → ')[0]}</td>
                                            <td>${canvi.split(' → ')[1]}</td>
                                        </tr>
                                    %endfor
                                </table>
                                <br>
                            %endif
                        %endfor

                        %if len(i.observacions.split('* '))>1:
                            <table>
                                <tr>
                                    <th width="33%">${_(u'Renovació automàtica')}</th>
                                    <th width="33%">${_(u'Data final anterior')}</th>
                                    <th width="33%">${_(u'Nova data final')}</th>
                                </tr>
                                %for aux in range(1,len(i.observacions.split('* '))):
                                    <tr style="text-align: center">
                                        <td>${i.observacions.split('* ')[aux].split('.')[0][-10:]}</td>
                                        <td>${i.observacions.split('* ')[aux].split('.')[1][-10:]}</td>
                                        <td>${i.observacions.split('* ')[aux].split('.')[2][-10:]}</td>
                                    </tr>
                                %endfor
                            </table>
                            <br>
                        %elif '•' not in i.observacions:
                            <p style="margin-bottom: 10px">${_(u'No hi ha canvis en aquesta modificació.')}</p>
                        %endif
                    %else:
                        <p>${_(u'No hi ha observacions en aquesta modificació')}</p>
                    %endif
                </div>
            %endfor
        %endfor
    </body>
</html>