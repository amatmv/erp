# -*- coding: utf-8 -*-
import pooler
from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class ContracteRmlParser(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(ContracteRmlParser, self).__init__(
            cursor, uid, name, context=context
        )
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path'],
        })

    def set_context(self, objects, data, ids, report_type=None):
        polissa_date = data.get('form', {}).get('polissa_date')
        if polissa_date:
            ctx = {'date': polissa_date}
            polissa_obj = self.pool.get('giscedata.polissa')
            objects = polissa_obj.browse(self.cr, self.uid, ids, context=ctx)
        super(ContracteRmlParser, self).set_context(
            objects, data, ids, report_type
        )


class ContracteWebkitParser(webkit_report.WebKitParserAppendPDF):
    def __init__(
            self, name='report.giscedata.polissa', table='giscedata.polissa',
            rml='giscedata_polissa/report/contracte.mako',
            parser=ContracteRmlParser, header=True, store=False
    ):
        if name != 'report.giscedata.polissa':
            raise Exception

        super(ContracteWebkitParser, self).__init__(
            'report.giscedata.polissa', table, rml=rml, parser=parser,
            header=header, store=store
        )


ContracteWebkitParser()


class report_webkit_html_proj(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html_proj, self).__init__(
            cursor, uid, name, context=context
        )
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'company_id': self.localcontext['company'].id,
            'addons_path': config['addons_path'],
        })

webkit_report.WebKitParser(
    'report.modcon',
    'giscedata.polissa',
    'giscedata_polissa/report/modcon_report.mako',
    parser=report_webkit_html_proj
)


def _contracte_print(cursor, uid, ids, data, context=None):
    if not context:
        context = {}

    pool = pooler.get_pool(cursor.dbname)
    contractes = pool.get('giscedata.polissa')
    modcons = pool.get('giscedata.polissa.modcontractual')
    lang = pool.get('res.lang')

    #Search for lang
    lang_code = context.get('lang', False)
    lang_id = lang.search(cursor, uid, [('code', '=', lang_code)])

    modcontractual = False
    modcon = False
    if data['model'] == 'giscedata.polissa.modcontractual':
        modcontractual = data['id']
        modcon = modcons.browse(cursor, uid, data['id'])
        contracte_id = modcon.polissa_id.id
    else:
        # per defecte és contracte sempre
        contracte_id = data['id']
        modcon_act = contractes.read(cursor, uid,
            contracte_id, ['modcontractual_activa'])['modcontractual_activa']
        modcon = modcon_act and modcons.browse(cursor, uid, modcon_act[0])

    modcontractual_id = modcon and modcon.id or False

    modcon_name = modcon and modcon.name or ''
    modcon_inici = modcon and modcon.data_inici or ''
    modcon_final = modcon and modcon.data_final or ''
    modcon_firma = modcon and modcon.data_firma_contracte or ''

    ctx = context.copy()
    ctx.update(modcontractual and {'date': modcon_inici} or {})

    contracte = contractes.browse(cursor, uid,
        contracte_id,
        ctx)

    contracte_id = contracte.id
    # Agafem l'string de potències
    potencies_str = ""
    if modcon:
        potencies_dict = modcon.get_potencies_dict()
        for pp in sorted(potencies_dict):
            pot = potencies_dict[pp]
            if lang_id:
                pot = lang.format(cursor, uid, lang_id, '%.3f', pot)
            potencies_str = "%s %s: %s" % (potencies_str, pp, pot)
    else:
        #Update periodes potencia if esborrany
        if contracte.state == 'esborrany':
            contracte.generar_periodes_potencia(context={'sync': False})
        for pp in contracte.potencies_periode:
            pot = pp.potencia
            if lang_id:
                pot = lang.format(cursor, uid, lang_id, '%.3f', pp.potencia)
            potencies_str = "%s %s: %s" % (potencies_str,
                                           pp.periode_id.name,
                                           pot)

    data_inici = modcon_inici or contracte.data_alta or ''
    data_final = modcon_final or contracte.data_baixa or \
                    modcons.get_data_final(cursor, uid, data_inici) or ''
    data_firma = modcon_firma or contracte.data_firma_contracte or data_inici

    context.update(ctx)

    producte = None
    comptador_obj = pool.get('giscedata.lectures.comptador')

    cmpt = comptador_obj.search(cursor, uid, [('polissa', '=', contracte_id)])
    producte = ''
    prod_marca_mod = ''
    if cmpt:
        prod = comptador_obj.browse(cursor, uid, cmpt[0]).product_lloguer_id
        prod_marca = comptador_obj.browse(cursor, uid, cmpt[0]).product_id
        if prod:
            producte = '%s' % prod.name
        if prod_marca:
            prod_marca_mod = '%s' % prod_marca.name

    return {'ids': [contracte_id],
            'parameters': {
                'potencies': potencies_str,
                'data_inici': data_inici,
                'data_final': data_final,
                'data_firma': data_firma,
                'modcon': modcon_name,
                'producte_comptador': producte,
                'marca_modelo': prod_marca_mod,
                },
            }

#report_jasper('report.giscedata.polissa',
#              'giscedata.polissa', _contracte_print)
