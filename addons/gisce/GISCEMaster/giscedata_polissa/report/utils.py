# -*- coding: utf-8 -*-
def localize_period(period, locale):
    '''
    This function converts a numeric date to a traditional writing style.
    Example (in Spanish):
    2016/01/01 -> Viernes, 1 de Enero de 2016
    :param period: Date to be formatted to writing style
    :param locale: Language to be written
    :return: String with date in writing style
    '''
    try:
        import babel.dates
        from datetime import datetime
        # dtf = datetime.strptime(period, '%Y-%m-%d')
        # dtf = dtf.strftime("%y%m%d")
        # dt = datetime.strptime(dtf, '%y%m%d')
        return babel.dates.format_datetime(period, "d 'de' LLLL 'de' yyyy",
                                           locale=locale)
    except Exception:
        return ''


def datetime_to_date(date):
    """
    :param date: date(str) in format: '%Y-%m-%d %H:%M:%S'
    :return: date converted to format (str): '%Y-%m-%d'
    """
    from datetime import datetime
    return datetime.strftime(datetime.strptime(date, '%Y-%m-%d %H:%M:%S'),
                             '%Y-%m-%d')
