# -*- coding: utf-8 -*-
from osv import osv, fields
from osv.expression import OOQuery
from giscedata_polissa import CONTRACT_IGNORED_STATES


class GiscedataCupsPs(osv.osv):
    """Model de Punt de servei (CUPS)."""

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _polissa_not_in_states = tuple(CONTRACT_IGNORED_STATES + ['baixa'])

    def _get_polissa(self, cursor, uid, cups_id):
        """Retorna la pòlissa activa del CUPS."""
        po_ids = self.pool.get('giscedata.polissa').search(cursor, uid,
                                [('cups', '=', cups_id),
                                 ('state', 'not in',
                                  self._polissa_not_in_states)])
        if po_ids:
            return self.pool.get('giscedata.polissa').browse(cursor, uid,
                                                             po_ids[0])
        else:
            return False

    def _polissa_client(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna l'abonat actual del CUPS."""
        res = {}
        cursor.execute("""\
select
  c.id,
  coalesce(pa.name, '')
from
  giscedata_cups_ps c
  left join giscedata_polissa p on (p.cups = c.id and p.active = True
                                    and p.state not in ('%s'))
  left join res_partner pa on (p.titular = pa.id)
where
  c.id in (%s)""" % ("','".join(self._polissa_not_in_states),
                     ','.join(map(str, map(int, ids)))))
        for cups in cursor.fetchall():
            res[cups[0]] = cups[1]
        return res

    def _polissa_client_search(self, cursor, uid, obj, name, args, context=None):
        """Permet buscar un CUPS segons abonat."""
        if not len(args):
            return []
        else:
            pol_ids = self.pool.get('giscedata.polissa').search(cursor, uid,
                [('titular', args[0][1], args[0][2]), ('cups', '!=', False)])
            if not len(pol_ids):
                return [('id', '=', '0')]
            else:
                ids = []
                for polissa in self.pool.get('giscedata.polissa').browse(cursor,
                                                                uid, pol_ids):
                    ids.append(polissa.cups.id)
                return [('id', 'in', ids)]

    def _polissa_polissa(self, cursor, uid, ids, field_name, arg, context):
        """Número de pòlissa actual del CUPS."""
        res = {}
        cursor.execute("""\
            select
              c.id,
              p.id,
              p.name
            from
              giscedata_cups_ps c
              left join giscedata_polissa p on (
                p.cups = c.id and p.active = True and p.state not in ('%s'))
            where c.id in (%s)""" % ("','".join(self._polissa_not_in_states),
                                     ','.join(map(str, map(int, ids)))))
        for cups in cursor.fetchall():
            if cups[1] and cups[2]:
                res[cups[0]] = (cups[1], cups[2])
            elif cups[1]:
                res[cups[0]] = (cups[1], 'ID: %i' % cups[1])
            else:
                res[cups[0]] = False
        return res

    def _get_active_polissa_id(self, cr, uid, ids, context=None):
        if not context:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_vals = polissa_obj.read(cr, uid, ids, ['cups'])
        return [x['cups'][0] for x in polissa_vals if x.get('cups', False)]

    def find_most_recent_polissa(self, cursor, uid, ids, data, context=None):
        """
        Obté el identificador de la pòlissa més recent associat al cups segons
        la data passada per paràmetre.
        :param cursor:
        :param uid:
        :param ids:
        :param data: Buscarà la pòlissa que tingui la data d'alta més propera a
        a aquest paràmetre. La data ha d'estar en format ERP (YYYY-MM-DD)
        :type: str
        :param context:
        :return:
        """
        if not isinstance(ids, list):
            ids = [ids]

        pol_querier = OOQuery(self.pool.get('giscedata.polissa'), cursor, uid)
        pol_select = pol_querier.select(
            ['id'], order_by=['data_alta.desc'], limit=1, only_active=False
        )
        res = {}
        for cups_id in ids:
            pol_query = pol_select.where(
                [('data_alta', '<=', data), ('cups', '=', cups_id)]
            )
            cursor.execute(*pol_query)
            vals = cursor.dictfetchall()
            if vals:
                vals = vals[0]['id']
            else:
                vals = False
            res[cups_id] = vals
        return res

    _STORE_POLISSA = {
        'giscedata.polissa': (
            _get_active_polissa_id, ['cups', 'state'], 10
        ),
        'giscedata.cups.ps': (
            lambda self, cursor, uid, ids, c=None: ids, [], 20
        )
    }

    _columns = {
        'polissa_polissa': fields.function(
            _polissa_polissa, method=True, type='many2one',
            relation='giscedata.polissa', store=_STORE_POLISSA, string='Polissa'
        ),
        'polissa_potencia': fields.related(
            'polissa_polissa', 'potencia',
            string="Potència contractada (kW)",
            type='float', readonly=True,
            digits=(16, 3)
        ),
        'polisses': fields.one2many('giscedata.polissa.modcontractual', 'cups',
                                    'Històric Pòlisses',
                                    context={'active_test': False},
                                    readonly=True),
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum', size=64),
    }


GiscedataCupsPs()
