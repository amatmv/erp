# -*- coding: utf-8 -*-
"""Actualitzem el camp data_firma_contracte de modcon amb el valor actual
de la pólissa. En el cas que hi hagi valor, el deixem com està
"""

import netsvc
import pooler

def migrate(cursor, installed_version):
    uid = 1
    logger = netsvc.Logger()
    
    pool = pooler.get_pool(cursor.dbname)
    polissa_obj = pool.get('giscedata.polissa')
    mod_obj = pool.get('giscedata.polissa.modcontractual')
    search_params = []
    data_ids = polissa_obj.search(cursor,uid,search_params)
    migrades = []
    for p_id in data_ids:
        polissa = polissa_obj.browse(cursor,uid,p_id)
        if polissa.data_firma_contracte:
            for mod in polissa.modcontractuals_ids:
                if not mod.data_firma_contracte:
                    mod_obj.write(cursor,uid,[mod.id],
                        {'data_firma_contracte': polissa.data_firma_contracte},
                        {'sync': False})
                    migrades.append(polissa.id)

    logger.notifyChannel('migration', netsvc.LOG_INFO,
        "Polisses data_firma_contracte a modcons: %s" % ",".join([str(x) for x in migrades]))
