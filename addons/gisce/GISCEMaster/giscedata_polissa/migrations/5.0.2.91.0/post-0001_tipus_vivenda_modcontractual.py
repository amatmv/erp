# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Set tipus_vivenda from giscedata_polissa to tipus_vivenda '
                'from giscedata_polissa_modcontractual')

    cursor.execute("""
        UPDATE giscedata_polissa_modcontractual
        SET tipus_vivenda = p.tipus_vivenda
        FROM giscedata_polissa AS p 
        WHERE p.id = polissa_id
    """)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
