# coding=utf-8
from oopgrade import oopgrade
import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()
    if not installed_version:
        return

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Deleting action of giscedata_polissa from old jasper report.'
        'The contract report was replaced by a new mako contract and we need '
        'to delete the old button).'
    )

    query = "SELECT * FROM ir_values where name='Contracte' and key='action' " \
            "and key2='client_print_multi' and model='giscedata.polissa'"
    query_del = "DELETE FROM ir_values where name='Contracte' and key='action' and key2='client_print_multi' and model='giscedata.polissa'"
    cursor.execute(query)
    if len(cursor.fetchall()) == 1:
        cursor.execute(query_del)
        logger.notifyChannel('migration', netsvc.LOG_INFO, 'Succesfully deleted'
                                                           ' the old button!')
    else:
        logger.notifyChannel('migration', netsvc.LOG_INFO, 'Could not delete '
                                                           'the button. '
                                                           'Found more than '
                                                           'one candidate!')


def down(cursor, installed_version):
    pass

migrate = up
