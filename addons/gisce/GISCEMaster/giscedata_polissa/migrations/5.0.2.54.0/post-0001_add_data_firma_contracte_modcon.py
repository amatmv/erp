# -*- coding: utf-8 -*-
"""Update modcon without data_firma_contracte to the value in
the associated polissa
"""

import netsvc
import pooler

def migrate(cursor, installed_version):

    logger = netsvc.Logger()

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Copying data_firma_contracte from polissa to modcon')
    query = """
        UPDATE giscedata_polissa_modcontractual
            set data_firma_contracte = giscedata_polissa.data_firma_contracte
        FROM giscedata_polissa
        WHERE
            giscedata_polissa_modcontractual.polissa_id = giscedata_polissa.id
            AND coalesce(giscedata_polissa_modcontractual.data_firma_contracte,
                         '1900-01-01') = '1900-01-01'
    """
    cursor.execute(query)
    
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration finished')
