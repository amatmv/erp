# -*- coding: utf-8 -*-
"""Modifiquem les opcions per defecte del crontab inicial
"""

import netsvc
import pooler
from datetime import datetime, timedelta

def migrate(cursor, installed_version):
    uid = 1
    logger = netsvc.Logger()
    cursor.execute("SELECT id from res_users where id = %s" % (uid,))
    if not cursor.fetchall():
        logger.notifyChannel('migration', netsvc.LOG_ERROR,
                             'No existeix uid:%s (superusuari).' % uid)
        raise Exception("No existeix uid:%s" % uid)
    pool = pooler.get_pool(cursor.dbname)
    model_data_obj = pool.get('ir.model.data')
    search_params = [('name', '=', 'ir_cron_activar_contractes'),
                     ('module', '=', 'giscedata_polissa')]
    data_ids = model_data_obj.search(cursor, uid, search_params)
    if data_ids:
        cron_id = model_data_obj.browse(cursor, uid, data_ids[0]).res_id
        cron_obj = pool.get('ir.cron')
        vals = {
            'doall': 0,
            'args': "({'emails_to': '', 'requests_to': ''},)",
            'nextcall': (datetime.now()
                         + timedelta(days=1)).strftime('%Y-%m-%d 00:00:00')
        }
        cron_obj.write(cursor, uid, [cron_id], vals)
