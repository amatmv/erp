# -*- coding: utf-8 -*-

import netsvc


def migrate(cr, installed_version):
    logger = netsvc.Logger()

    # Rename (if it exists) the fields with . instead of _
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Renaming polissa_contracte_durada variable')
    query = "UPDATE res_config" \
            " SET name='giscedata_polissa_contracte_durada'" \
            " WHERE name='giscedata.polissa.contracte.durada'"
    cr.execute(query)

    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        'Checking existance of new variables of res.config on data.xml'
    )
    variables_changed = [
        'giscedata_polissa_contracte_durada',
        'default_crear_contracte_data_final'
    ]
    # Get resource from res.config
    query = "SELECT id,name FROM res_config WHERE name in %s"
    cr.execute(query, (tuple(variables_changed),))
    keep_variables = [
        # 0 noupdate / 1 module / 2 model / 3 res_id / 4 name
        (True, 'giscedata_polissa', 'res.config', elem[0], elem[1])
        for elem in cr.fetchall()
        ]
    # Get data from ir.model.data (data.xml)
    query = "SELECT res_id,name FROM ir_model_data" \
            " WHERE res_id=%s and name=%s and module=%s and model=%s"
    # Find collision within the two queries (already on data.xml)
    for elem in keep_variables:
        cr.execute(query, (elem[3], elem[4], elem[1], elem[2]))
        found = cr.fetchone()
        if found and elem[3] == found[0] and elem[4] == found[1]:
            keep_variables.remove(elem)
    # Insert to data.xml if not found
    query = "INSERT INTO ir_model_data" \
            " (noupdate, module, model, res_id, name)" \
            " VALUES %s"
    for elem in keep_variables:
        logger.notifyChannel(
            'migration', netsvc.LOG_INFO,
            'Creating: {} on ir_model_data from res_config'.format(elem[-1])
        )
        cr.execute(query, (tuple(elem),))
