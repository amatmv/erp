# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from progressbar import ProgressBar, ETA, Percentage, Bar

import pooler
import netsvc


TARIFES = ('2.0A', '2.1A', '2.0DHA', '2.1DHA', '2.0DHS', '2.1DHS',
           '3.0A', '3.1A', '3.1A LB', '6.1', '6.2', '6.3', '6.4', '6.5')

def omplir_vals_inicials(pol_sql, t_idx):
    vals = {
        'titular': pol_sql['partner_id'],
        'data_firma_contracte': pol_sql['data_alta'],
        'name_auto': 0,
        'renovacio_auto': 1,
        'state': 'esborrany',
        'tensio_normalitzada': t_idx.get(pol_sql['tensio'], t_idx['230']),
        'active': 1,
    }
    return vals

def baixa_comptadors(cursor, uid, pol, data_baixa):
    comptador_obj = pooler.get_pool(cursor.dbname).get('giscedata.lectures.comptador')
    param_search = [('polissa', '=', pol['id'])]
    comptadors = comptador_obj.search(cursor, uid, param_search)
    for comptador in comptadors:
        params = {'data_baixa': data_baixa, 'active': 0}
        comptador_obj.write(cursor, uid, [comptador], params)


def baixa_polissa(cursor, uid, pol_sql, pol, t_idx):
    logger = netsvc.Logger()
    pol_obj = pooler.get_pool(cursor.dbname).get('giscedata.polissa')
    vals = omplir_vals_inicials(pol_sql, t_idx)
    if not pol_sql['data_alta']:
        if pol_sql['data_baixa']:
            vals['data_alta'] = pol_sql['data_baixa']
            data_baixa = pol_sql['data_baixa']
        else:
            vals['data_alta'] = '1900-01-01'
            data_baixa = '1900-01-01'
    else:
        vals['data_alta'] = pol_sql['data_alta']
        if not pol_sql['data_baixa']:
            data_baixa = pol_sql['data_alta']
        else:
            data_baixa = pol_sql['data_baixa']
            if datetime.strptime(pol_sql['data_alta'], '%Y-%m-%d') >\
                    datetime.strptime(pol_sql['data_baixa'], '%Y-%m-%d'):
                data_baixa = pol_sql['data_alta']

    pol_obj.write(cursor, uid, [pol['id']], vals)
    try:
        if pol_sql['tarifa'] in TARIFES:
            pol_obj.generar_periodes_potencia(cursor, uid, [pol['id']])
    except:
        logger.notifyChannel('migration',
                             netsvc.LOG_INFO,
                             'Tarifa #%s# de la pòlissa %s ha fallat'%
                             (pol_sql['tarifa'], pol_sql['id']))
    if pol_sql['tarifa']:
        pol_obj.send_signal(cursor, uid, [pol['id']], ['validar', 'contracte'])
        vals = {'renovacio_auto': 0, 'data_baixa': data_baixa}
        pol_obj.write(cursor, uid, [pol['id']], vals)
        pol_obj.send_signal(cursor, uid, [pol['id']], ['baixa'])

def activar_polissa(cursor, uid, pol_sql, pol, t_idx):
    logger = netsvc.Logger()
    pol_obj = pooler.get_pool(cursor.dbname).get('giscedata.polissa')
    vals = omplir_vals_inicials(pol_sql, t_idx)
    if not pol['data_alta']:
        vals['data_alta'] = '1900-01-01'
    if pol['data_alta'] > datetime.now().strftime('%Y-%m-%d'):
        vals['data_alta'] = '1900-01-01'

    pol_obj.write(cursor, uid, [pol['id']], vals)

    if not pol['name'] or not pol['cups']:
    # Si no té número la deixem en esborrany
        return True
    try:
        if pol_sql['tarifa'] in TARIFES:
            pol_obj.generar_periodes_potencia(cursor, uid, [pol['id']])
    except:
        logger.notifyChannel('migration',
                             netsvc.LOG_INFO,
                             'Tarifa #%s# de la pòlissa %s ha fallat'%
                             (pol_sql['tarifa'], pol_sql['id']))


    if pol_sql['tarifa']:
        pol_obj.send_signal(cursor, uid, [pol['id']], ['validar', 'contracte'])




def migrate(cursor, installed_version):
    uid = 1
    logger = netsvc.Logger()
    pol_obj = pooler.get_pool(cursor.dbname).get('giscedata.polissa')
    cursor.execute("select id,name from giscedata_tensions_tensio")
    t_idx = {}
    for t in cursor.dictfetchall():
        t_idx[t['name']] = t['id']
    cursor.execute("update giscedata_polissa set state = 'esborrany' "
                   "where id not in (select polissa_id from giscedata_polissa_modcontractual v)")
    cursor.execute("select * from giscedata_polissa_v4 where name is not null "
                   " and id not in (select polissa_id from giscedata_polissa_modcontractual v) "
                   " order by active asc, data_baixa asc, id asc")
    pols_sql = cursor.dictfetchall()
    total = len(pols_sql)
    if total > 0:
        widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
        pbar = ProgressBar(widgets=widgets, maxval=total).start()
        done = 0
        print "Processar les pòlisses"
        for pol_sql in pols_sql:
            done += 1
            select_fields = ['name', 'cups', 'data_alta']
            pol = pol_obj.read(cursor, uid, pol_sql['id'], select_fields)
            # Creem el workflow si no està creat
            wf_service = netsvc.LocalService("workflow")
            cursor.execute("select id from wkf_instance where "
                           "res_type = 'giscedata.polissa' and res_id = %s",
                           (pol['id'],))
            res = cursor.fetchall()
            if not res:
                wf_service.trg_create(uid, 'giscedata.polissa', pol['id'], cursor)
            logger.notifyChannel('migration',
                                 netsvc.LOG_DEBUG,
                                 "Polissa: %s " % pol['name'])
            if pol_sql['active']:
                print "Activa"
                activar_polissa(cursor, uid, pol_sql, pol,  t_idx)
            else:
                print "Baixa"
                baixa_polissa(cursor, uid, pol_sql, pol, t_idx)
            pbar.update(done)
            cursor.commit()
        pbar.finish()