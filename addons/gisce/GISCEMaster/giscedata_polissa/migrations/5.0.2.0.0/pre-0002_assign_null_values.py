# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
import netsvc

def migrate(cursor, installed_version):
    cursor.execute("select exists(select * "
                "from information_schema.tables where table_name=%s)",
                ('giscedata_lectures_comptador',))
    if cursor.fetchone()[0]:
        cursor.execute("""SELECT p.id as id, min(c.data_alta) as data_alta
        from giscedata_lectures_comptador c
        left join giscedata_polissa p on c.polissa = p.id
        where p.data_alta is null group by p.id""")
        if cursor.rowcount:
            log("Assignant data_alta a %s pòlisses" % cursor.rowcount)
        dates_alta = {}
        for data in cursor.dictfetchall():
            cursor.execute("""update giscedata_polissa set data_alta = %s
            where id = %s""", (data['data_alta'], data['id']))
        cursor.execute("ALTER TABLE giscedata_polissa DROP COLUMN tensio CASCADE")
        # Pòlisses de baixa que no tenen data de baixa
        cursor.execute("""SELECT p.id as id, max(c.data_baixa) as data_baixa from
        giscedata_polissa p inner join giscedata_lectures_comptador c
        on (c.polissa = p.id) where p.active in (null, False)
        and p.data_baixa is null group by p.id""")
        if cursor.rowcount:
            log("Assignant data_baixa a %s pòlisses." % cursor.rowcount)
        for data in cursor.dictfetchall():
            cursor.execute("""update giscedata_polissa set data_baixa = %s where
            id = %s""", (data['data_baixa'], data['id']))
