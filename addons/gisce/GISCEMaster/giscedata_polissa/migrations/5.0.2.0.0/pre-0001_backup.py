# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor
import netsvc

def migrate(cursor, installed_version):
    """Canvis a executar relatius al mòdul base
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # backup
    mig = GisceUpgradeMigratoor('giscedata_polissa', cursor)
    mig.backup(suffix='v4')
    cursor.execute("""UPDATE ir_model_data set module = 'giscedata_polissa'
                      WHERE module = 'giscedata_lectures'
                            and model ilike 'product_%'
                            and name not ilike 'boe_%'
                            and name not ilike 'alq_%'
                            and name not ilike 'categ_alq'
                            and name != 'categ_cosfi'""")
    cursor.execute("""update ir_model_data set module = 'giscedata_polissa'
                      where module = 'giscedata_lectures'
                            and model ilike 'giscedata.polissa.%'""")
    mig.pre_xml({'giscedata.polissa.tarifa': ['name']})
