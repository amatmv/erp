.. Polissa documentation master file, created by
   sphinx-quickstart on Wed Apr  3 14:21:33 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació del mòdul de pólissa
*********************************

Canvi data firma contracte
==========================

En un contracte podem actualitzar la data de firma de contracte sense la
necessitat de fer una modificació contractual mitjançant un assistent creat
expresament

Des de una pólissa o des del botó acció del llistat de pólisses podem prèmer
sobre el botó **Actualitzar data firma contracte**. Ens apareixerà el formulari
:ref:`wizdatafirma` on podrem veure la pólissa seleccionada i la data de firma
de contracte actual (si en té). Prement en el botó continuar, actualitzarà la
data de firma de contracte de la **pólissa** i la **modificació contractual
activa**.
   
.. warning::

   No es farà cap tipus de validació sobre la data introduïda. Si és una data
   vàlida es modificarà la data actual sense tenir en compte cap altra
   consideració com la data d'alta i de baixa de la pòlissa o la data actual

.. _wizdatafirma:
.. figure:: /_static/WizardDataFirmaContracte.png

   Assistent per canviar la data de firma de contracte