# -*- coding: utf-8 -*-
import calendar
from datetime import date, timedelta, datetime

import netsvc
from gestionatr.defs import TABLA_126, TABLA_127, TABLA_128, TABLA_129, TABLA_130, TABLA_113
from giscedata_polissa import CONTRACT_IGNORED_STATES
from osv import osv, fields
from tools.translate import _

AUTOCONSUM_STATES = [
    ('esborrany', 'Esborrany'),
    ('validar', 'Validar'),
    ('actiu', 'Actiu'),
    ('creat', 'Creat'),
    ('cancellat', 'Cancel·lat'),
    ('modcontractual', 'Modificació Contractual'),
    ('baixa', 'Baixa')
]


class GiscedataAutoconsum(osv.osv):
    """Model d'Autoconsum."""

    _name = 'giscedata.autoconsum'
    _descritption = "Model d'autoconsum"
    _rec_name = 'cau'

    _polissa_not_in_states = tuple(CONTRACT_IGNORED_STATES + ['baixa'])

    def write(self, cursor, uid, ids, vals, context={}):
        if vals.get('seccio_registre', False) == '2':
            generador_obj = self.pool.get('giscedata.autoconsum.generador')
            generador_ids = self.read(cursor, uid, ids[0], ['generador_id'])['generador_id']
            partner_data = generador_obj.read(cursor, uid, generador_ids, ['partner_id'])
            wrong_generators = []
            for partner in partner_data:
                if not partner['partner_id']:
                    wrong_generators.append(partner['id'])
            if wrong_generators:
                raise osv.except_osv(_('Error de dades'),
                                     _(u"No es pot modificar l'autoconsum a 'Amb excendents' perque hi han "
                                       u"generadors que no tenen informat el titular quan aquest és requerit "
                                       u"per aquest tipus de secció de registre. Si us plau informi primer els "
                                       u"titulars als generadors i després canvïi la secció de registre a "
                                       u"'Amb excedents'. Ids de generadors afectats: {}".format(str(wrong_generators))))
        return super(GiscedataAutoconsum, self).write(cursor, uid, ids, vals, context)

    def get_changes(self, cursor, uid, autoconsum_id, context=None):
        """Comprova les diferències entre el que hem modificat i la modcon
        actual o el que li passem pel context via modcon_id."""
        if not context:
            context = {}
        if isinstance(autoconsum_id, tuple) or isinstance(autoconsum_id, list):
            autoconsum_id = autoconsum_id[0]
        autoconsum = self.browse(cursor, uid, autoconsum_id)
        modcon_obj = self.pool.get('giscedata.autoconsum.modcontractual')
        fields_to_read = modcon_obj.get_fields_for_autoconsum(cursor, uid)
        autoconsum_vals = self.read(cursor, uid, [autoconsum_id], fields_to_read, context)[0]
        modcon_id = context.get('modcon_id', autoconsum.modcontractual_activa.id)
        modcon_vals = modcon_obj.read(cursor, uid, [modcon_id], fields_to_read, context)[0]
        del autoconsum_vals['id']
        del modcon_vals['id']
        res = {}
        for field in fields_to_read:
            if autoconsum_vals.get(field) != modcon_vals.get(field):
                res.setdefault(field, {'old': modcon_vals.get(field),
                                       'new': autoconsum_vals.get(field)})
        return res

    def get_autoconsum_type_from_autoconsum(self, cursor, uid, autoconsum_id, context=None):
        if isinstance(autoconsum_id, (list, tuple)):
            autoconsum_id = autoconsum_id[0]

        generador_obj = self.pool.get('giscedata.autoconsum.generador')
        autoconsum_vals = self.read(cursor, uid, autoconsum_id, [])
        generador_ids = generador_obj.search(cursor, uid, [('autoconsum_id', '=', autoconsum_id)])
        generador_vals = generador_obj.read(cursor, uid, generador_ids, [])

        seccio_registre = autoconsum_vals['seccio_registre']
        subseccio = autoconsum_vals['subseccio']
        if not subseccio:
            subseccio = ''

        if not subseccio and seccio_registre == '2':
            subseccio = 'a0'

        collectiu = 1 if autoconsum_vals['collectiu'] else 0
        tipus_cups = 1
        temp_tipus_inst = []
        for generador in generador_vals:
            if generador['ssaa']:
                tipus_cups = 2
            temp_tipus_inst.append(int(generador['tipus_installacio']))

        if 3 in temp_tipus_inst:
            if 2 in temp_tipus_inst:
                tipus_installacio = 5
            elif 1 in temp_tipus_inst:
                tipus_installacio = 4
            else:
                tipus_installacio = 3
        elif 2 in temp_tipus_inst:
            tipus_installacio = 2
        else:
            tipus_installacio = 1

        choice = (
            seccio_registre,
            subseccio,
            collectiu,
            tipus_cups,
            tipus_installacio,
        )

        if choice == ('1', '', 0, 1, 1):  # Sin Excedentes Individual – Consumo
            return '31'
        elif choice == ('1', '', 1, 1, 2):  # Sin Excedentes Colectivo – Consumo
            return '32'
        elif choice == ('1', '', 1, 1, 2):  # Sin Excedentes Colectivo – Consumo
            return '33'
        elif choice == ('2', 'a0', 0, 1, 1):  # Con excedentes y compensación Individual - Consumo 
            return '41'
        elif choice == ('2', 'a0', 1, 1, 2):  # Con excedentes y compensación Colectivo– Consumo
            return '42'
        elif choice == ('2', 'b1', 0, 1, 1):  # Con excedentes sin compensación Individual sin cto de SSAA en Red Interior– Consumo 
            return '51'
        elif choice == ('2', 'b1', 1, 1, 2):  # Con excedentes sin compensación Colectivo sin cto de SSAA en Red Interior– Consumo 
            return '52'
        elif choice == ('2', 'b2', 0, 1, 1):  # Con excedentes sin compensación Individual con cto de  SSAA en Red Interior– Consumo 
            return '53'
        elif choice == ('2', 'b2', 0, 2, 1):  # Con excedentes sin compensación individual con cto de SSAA en Red Interior– SSAA 
            return '54'
        elif choice == ('2', 'b2', 1, 1, 2):  # Con excedentes sin compensación Colectivo con cto de SSAA en Red Interior– Consumo
            return '55'
        elif choice == ('2', 'b2', 1, 2, 2):  # Con excedentes sin compensación Colectivo con cto de SSAA en Red Interior– SSAA 
            return '56'
        elif choice == ('2', 'b2', 0, 1, 3):  # Con excedentes sin compensación Individual con cto de SSAA a través de red – Consumo
            return '61'
        elif choice == ('2', 'b2', 0, 2, 3):  # Con excedentes sin compensación individual con cto de SSAA a través de red – SSAA
            return '62'
        elif choice == ('2', 'b2', 1, 1, 3):  # Con excedentes sin compensación Colectivo con cto de SSAA a través de red – Consumo
            return '63'
        elif choice == ('2', 'b2', 1, 2, 3):  # Con excedentes sin compensación Colectivo con cto de SSAA a través de red – Consumo
            return '64'
        elif choice == ('2', 'b2', 0, 1, 4):  # Con excedentes sin compensación Individual con cto de SSAA a través de red y red interior – Consumo
            return '71'
        elif choice == ('2', 'b2', 0, 2, 4):  # Con excedentes sin compensación individual con cto de SSAA a través de red y red interior – SSAA
            return '72'
        elif choice == ('2', 'b2', 1, 1, 5):  # Con excedentes sin compensación Colectivo con cto de SSAA  a través de red y red interior – Consumo
            return '73'
        elif choice == ('2', 'b2', 1, 2, 5):  # Con excedentes sin compensación Colectivo con cto de SSAA a través de red y red interior - SSAA
            return '74'
        else:
            return False

    def get_autoconsum_vals_from_type(self, cursor, uid, autoconsum_type, context=None):
        if autoconsum_type not in dict(TABLA_113).keys():
            raise osv.except_osv('Error', _("Tipus d'autoconsum erroni o no definit."))

        if autoconsum_type in ['71', '72', '73', '74']:
            raise osv.except_osv(_('Procés manual requerit'), _("No es pot realitzar aquest procés automàticament "
                                                                "degut a que es requereix intervenció manual per a "
                                                                "modificar els generadors associats a l'autoconsum "
                                                                "segons siguin a través de xarxa o de xarxa interior."))
        # Amb o Sense excedents
        if autoconsum_type in ['31', '32', '33']:
            excedents = '1'
        else:
            excedents = '2'

        # Subseccio
        if autoconsum_type in ['31', '32', '33']:
            subseccio = False
        elif autoconsum_type in ['41', '42']:
            subseccio = 'a0'
        elif autoconsum_type in ['51', '52']:
            subseccio = 'b1'
        else:
            subseccio = 'b2'

        # Col.lectiu
        if autoconsum_type in ['32', '33', '42', '52', '55', '56', '63', '64', '73', '74']:
            collectiu = True
        else:
            collectiu = False

        # Tipus CUPS
        if autoconsum_type in ['54', '56', '62', '64', '72', '74']:
            tipus_cups = '02'
        else:
            tipus_cups = '01'

        # Tipus instal.lacio
        if autoconsum_type in ['31', '41', '51', '53', '54']:
            tipus_installacio = '01'
        elif autoconsum_type in ['32', '33', '42', '52', '55', '56']:
            tipus_installacio = '02'
        else:
            tipus_installacio = '03'

        vals = {
            'seccio_registre': excedents,
            'subseccio': subseccio,
            'collectiu': collectiu,
            'tipus_cups': tipus_cups,
            'tipus_installacio': tipus_installacio,
        }

        return vals

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        """Undo last modcontractual and reactivate the one that remains"""

        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        modcon_model = 'giscedata.autoconsum.modcontractual'
        modcon_obj = self.pool.get(modcon_model)
        wkf_instance = self.pool.get('workflow.instance')
        wkf_item = self.pool.get('workflow.workitem')
        wkf_activity = self.pool.get('workflow.activity')

        for autoconsum in self.browse(cursor, uid, ids, context=context):
            # If there's only one modcon or no modcon's, do nothing at all
            if len(autoconsum.modcontractuals_ids) <= 1:
                continue
            # If not actiu, changes are not allowed
            if autoconsum.state != 'actiu':
                raise osv.except_osv('Error',
                                     _(u"No es pot desfer la darrera modificació si "
                                       u"l'autoconsum no està en estat actiu"))
            # Get related modcon's
            modcon_act = autoconsum.modcontractual_activa
            modcon_ant = autoconsum.modcontractual_activa.modcontractual_ant
            modcon_act_date = modcon_act.data_final
            modcon_act_state = modcon_act.state

            # Make modcon_ant the current active one
            autoconsum.write({'modcontractual_activa': modcon_ant.id}, context=context)
            modcon_obj.unlink(cursor, uid, [modcon_act.id], context=context)
            # Let's reactivate modcon_ant
            # Search workflow instance
            search_params = [('res_type', '=', modcon_model),
                             ('res_id', '=', modcon_ant.id)]
            instance_id = wkf_instance.search(cursor, uid, search_params)[0]
            # Search for workflow item
            search_params = [('inst_id', '=', instance_id)]
            item_id = wkf_item.search(cursor, uid, search_params)
            # Search for workflow activity id
            search_params = [('wkf_id.osv', '=', modcon_model),
                             ('name', '=', modcon_act_state)]
            activity_id = wkf_activity.search(cursor, uid, search_params)[0]
            # Reactivate workflow for modcon_ant
            wkf_item.write(cursor, uid, item_id, {'act_id': activity_id,
                                                  'state': 'complete'})
            # Write new values for mod_ant
            modcon_ant.write({'state': modcon_act_state,
                              'active': True,
                              'data_final': modcon_act_date},
                             context=context)
            # Adapt the selfconsumption to the new activated modcon
            signals = ['modcontractual', 'undo_modcontractual']
            self.send_signal(cursor, uid, [autoconsum.id], signals)

        return True

    def send_signal(self, cursor, uid, ids, signals):
        """Enviem el signal al workflow de la pòlissa.
        """
        wf_service = netsvc.LocalService('workflow')
        if not isinstance(signals, list) and not isinstance(signals, tuple):
            signals = [signals]
        for p_id in ids:
            for signal in signals:
                wf_service.trg_validate(uid, 'giscedata.autoconsum', p_id, signal, cursor)
        return True

    def wkf_esborrany(self, cursor, uid, ids):
        """ Passem a esborrany un autoconsum. """
        modcont_obj = self.pool.get('giscedata.autoconsum.modcontractual')
        # Eliminem tots els autoconsums (si n'hi ha)
        for autoconsum_vals in self.read(cursor, uid, ids, ['modcontractuals_ids']):
            unlink_ids = autoconsum_vals['modcontractuals_ids']
            if unlink_ids:
                modcont_obj.unlink(cursor, uid, unlink_ids)
        # Escrivim esborrany a l'estat
        self.write(cursor, uid, ids, {'state': 'esborrany', 'active': True})
        return True

    def wkf_validar(self, cursor, uid, ids):
        """ Acció dins l'estat validar. """
        self.write(cursor, uid, ids, {'state': 'validar'})
        return True

    def wkf_cancellar(self, cursor, uid, ids):
        """ Acció per a cancel·lar un autoconsum. """
        self.write(cursor, uid, ids,
                   {'state': 'cancellat', 'active': False,
                    'data_baixa': datetime.today().strftime("%Y-%m-%d")})
        return True

    def wkf_crear(self, cursor, uid, ids):
        """ Acció quan es passa a l'estat crear.

        En aquest estat del workflow generem la modificació contractiual segons
        els valors que tingui l'autoconsum. Ja sigui perquè és un autoconsum nou o perquè
        venim d'una modificació contractual.
        """
        modcon_obj = self.pool.get('giscedata.autoconsum.modcontractual')

        for autoconsum in self.browse(cursor, uid, ids):
            # Hem de crear una modcon amb tots els valors que té
            # l'autoconsum en l'actualitat
            vals = modcon_obj.get_vals_from_autoconsum(cursor, uid, autoconsum.id)
            modcon_obj.create(cursor, uid, vals)
        self.write(cursor, uid, ids, {'state': 'creat'})
        return True

    def wkf_activar(self, cursor, uid, ids):
        """Activem un autoconsum."""
        modcontractual_obj = self.pool.get('giscedata.autoconsum.modcontractual')
        for autoconsum in self.browse(cursor, uid, ids):
            # Creem un diccionari amb els valors que posarem a la pólissa
            vals = {'state': 'actiu', 'active': 1, 'data_baixa': False}

            if autoconsum.modcontractual_activa:
                modcontractual_obj.aplicar_modificacio(cursor, uid, autoconsum.modcontractual_activa.id, autoconsum.id)

            # Actualitzem l'autoconsum
            self.write(cursor, uid, [autoconsum.id],
                       vals, {'sync': False})
        return True

    def wkf_baixa(self, cursor, uid, ids):
        """Donem de baixa un autoconsum."""
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        wiz_obj = self.pool.get('wizard.alta.baixa.autoconsum')
        for autoconsum in self.browse(cursor, uid, ids):
            for cups_id in cups_obj.search(cursor, uid, [('autoconsum_id', '=', autoconsum.id)]):
                ctx = {'active_id': cups_id, 'active_ids': [cups_id]}
                wiz_id = wiz_obj.create(cursor, uid, {
                    'autoconsum_id': autoconsum.id,
                    'data': '2019-10-10'}, context=ctx)
                wiz_obj.action_donar_baixa_autoconsum(cursor, uid, [wiz_id], context=ctx)
                polissa_id = polissa_obj.search(cursor, uid, [('cups', '=', cups_id)])
                ac_params = {'autoconsumo': '00', 'autoconsum_id': False}
                data_inici = datetime.now()
                data_fi = data_inici + timedelta(days=365)
                polissa_obj.crear_modcon(cursor, uid, polissa_id[0], ac_params,
                                         data_inici.strftime('%Y-%m-%d'),
                                         data_fi.strftime('%Y-%m-%d'),
                                         context=None)

            # Creem un diccionari amb els valors que posarem a la pólissa
            vals = {'state': 'baixa', 'active': 0, 'data_baixa': datetime.now().strftime('%Y-%m-%d')}

            # Actualitzem l'autoconsum
            self.write(cursor, uid, [autoconsum.id],
                       vals, {'sync': False})
        return True

    def wkf_modcontractual(self, cursor, uid, ids):
        """Fem una modificació contractual."""
        self.write(cursor, uid, ids,
                   {'state': 'modcontractual'},
                   {'sync': False})
        return True

    def wkf_undo_modcontractual(self, cursor, uid, ids):
        """Desfem una modificació contractual."""
        # Amb la transició automatica a actiu, ja s'aplica
        # la modificacio contractual activa

        return True

    def wkf_overwrite_modcontractual(self, cursor, uid, ids):
        """Apliquem directament la modificació contractual.
        """
        modcon_obj = self.pool.get('giscedata.autoconsum.modcontractual')
        for autoconsum in self.browse(cursor, uid, ids):
            vals = modcon_obj.get_vals_from_autoconsum(cursor, uid, autoconsum.id)
            modcon_activa = autoconsum.modcontractual_activa
            vals.update({'data_inici': modcon_activa.data_inici,
                         'data_final': modcon_activa.data_final,
                         'tipus': modcon_activa.tipus,
                         'name': modcon_activa.name})
            modcon_activa.write(vals)
        return True

    def activar_modificar_autoconsum(self, cursor, uid, pol_id, autoconsum_type, context=None):
        if context is None:
            context = {}
        if not pol_id:
            raise osv.except_osv('Error', _("L'identificador de pòlissa no és correcte."))
        res = {'status': 'OK', 'message': ''}

        pol_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        ac_obj = self.pool.get('giscedata.autoconsum')

        cups_id = pol_obj.read(cursor, uid, pol_id, ['cups'])['cups'][0]
        autoconsum_id = cups_obj.read(cursor, uid, cups_id, ['autoconsum_id'])['autoconsum_id']

        if autoconsum_id:
            autoconsum_id = autoconsum_id[0]
            state = ac_obj.read(cursor, uid, autoconsum_id, ['state'])['state']
            if state == 'esborrany':
                self.send_signal(cursor, uid, [autoconsum_id], ['validar', 'crear'])
            elif state == 'validar':
                self.send_signal(cursor, uid, [autoconsum_id], ['crear'])
            elif state == 'modcontractual':
                self.send_signal(cursor, uid, [autoconsum_id], ['undo_modcontractual'])
            else:
                raise osv.except_osv('Error',
                                     _(u"L'autoconsum es troba en un estat de baixa o cancel.lació. "
                                       u"Si us plau revisi l'autoconsum amb id: {}.").format(autoconsum_id))

            current_ac_type = self.get_autoconsum_type_from_autoconsum(cursor, uid, autoconsum_id, context=context)
            state = ac_obj.read(cursor, uid, autoconsum_id, ['state'])['state']
            if autoconsum_type == '00':
                if state == 'actiu':
                    self.send_signal(cursor, uid, [autoconsum_id], ['baixa'])
                    wiz_obj = self.pool.get('wizard.alta.baixa.autoconsum')
                    wiz_id = wiz_obj.create(cursor, uid, {
                        'autoconsum_id': autoconsum_id,
                        'data': datetime.now().strftime('%Y-%m-%d')}, context=context)
                    wiz_obj.action_donar_baixa_autoconsum(cursor, uid, [wiz_id], context=context)

            if current_ac_type != autoconsum_type:
                res = {'status': 'WARNING', 'message': _(u"L'autoconsum informat al cas i l'existent a sistema son "
                                                         u"diferents. S'ha actualitzat el segon amb les dades "
                                                         u"aportades al cas.")}

                vals = self.get_autoconsum_vals_from_type(cursor, uid, autoconsum_type, context=context)
                self.send_signal(cursor, uid, [autoconsum_id], ['modcontractual'])
                autoconsum_vals = {
                    'seccio_registre': vals.get('seccio_registre', False),
                    'subseccio': vals.get('subseccio', False),
                    'collectiu': vals.get('collectiu', False),
                }
                ac_obj.write(cursor, uid, [autoconsum_id], autoconsum_vals, context=context)
                wiz_obj = self.pool.get('giscedata.autoconsum.crear.modcon')
                wiz_vals = {
                    'accio': 'nou',
                    'data_inici': (datetime.now() + timedelta(days=2)).strftime('%Y-%m-%d'),
                }
                ctx = context.copy()
                ctx.update({'active_id': autoconsum_id, 'active_ids': [autoconsum_id]})
                wiz_id = wiz_obj.create(cursor, uid, wiz_vals, context=ctx)
                wiz = wiz_obj.browse(cursor, uid, wiz_id, context=ctx)
                wiz.action_crear_modcon(context=ctx)
        else:
            raise osv.except_osv('Error',
                                 _(u"No s'ha trobat cap autoconsum associat a la pòlissa"))
        return res

    _columns = {

        'cau': fields.char('CAU', size=26, readonly=True,
                           states={'esborrany': [('readonly', False)],
                                   'validar': [('readonly', False),
                                               ('required', True)]}),

        'seccio_registre': fields.selection(TABLA_127, 'Secció registre', readonly=True,
                                            states={'esborrany': [('readonly', False)],
                                                    'validar': [('readonly', False),
                                                                ('required', True)],
                                                    'modcontractual': [('readonly', False),
                                                                       ('required', True)]}),

        'subseccio': fields.selection(TABLA_128, 'Subsecció', readonly=True,
                                      states={'esborrany': [('readonly', False)],
                                              'validar': [('readonly', False)],
                                              'modcontractual': [('readonly', False)]}),

        'collectiu': fields.boolean('Col·lectiu', readonly=True,
                                    states={'esborrany': [('readonly', False)],
                                            'validar': [('readonly', False)],
                                            'modcontractual': [('readonly', False)]}),

        'polissa_id': fields.one2many('giscedata.polissa', 'autoconsum_id', 'Pòlissa', readonly=True),

        'cups_id': fields.one2many('giscedata.cups.ps', 'autoconsum_id', 'CUPS', readonly=True),

        'generador_id': fields.one2many('giscedata.autoconsum.generador', 'autoconsum_id', 'Generador', readonly=True),

        'data_alta': fields.date('Data Alta', readonly=True,
                                 states={'esborrany': [('readonly', False)],
                                         'validar': [('readonly', False),
                                                     ('required', True)]}),

        'data_baixa': fields.date('Data Baixa', readonly=True,
                                  states={'esborrany': [('readonly', False)],
                                          'validar': [('readonly', False)]}),

        'active': fields.boolean('Actiu', readonly=True),

        'state': fields.selection(AUTOCONSUM_STATES, 'Estat',
                                  readonly=True, required=True),

        'modcontractuals_ids': fields.one2many('giscedata.autoconsum.modcontractual',
                                               'autoconsum_id', 'Modificacions contractuals',
                                               context={'active_test': False}, readonly=True),

        'comentaris': fields.text('Comentaris', size=4000, readonly=True,
                                  states={'esborrany': [('readonly', False)],
                                          'validar': [('readonly', False)]}),
    }

    _defaults = {
        'collectiu': lambda *a: False,
        'active': lambda *a: True,
        'state': lambda *a: 'esborrany',
    }


GiscedataAutoconsum()


class GiscedataAutoconsumGenerador(osv.osv):
    """Model de generador d'autoconsum."""

    _name = 'giscedata.autoconsum.generador'
    _descritption = "Model de generador d'autoconsum"

    _rec_name = 'cil'

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        if not vals['partner_id']:
            autoconsum_obj = self.pool.get('giscedata.autoconsum')
            autoconsum_id = vals['autoconsum_id']
            seccio_registre = autoconsum_obj.read(cursor, uid, autoconsum_id, ['seccio_registre'])['seccio_registre']

            if seccio_registre == '2':
                raise osv.except_osv(_("No s'ha informat titular"),
                                     _(u"L'autoconsum que intenta associar al generador es amb "
                                       u"excedents i per tant el titular s'ha d'informar al generador. "
                                       u"Si us plau, informi un titular per a poder continuar."))
        res = super(GiscedataAutoconsumGenerador, self).create(
            cursor, uid, vals, context=context
        )
        return res

    def write(self, cursor, uid, ids, vals, context={}):
        partner_id = self.read(cursor, uid, ids[0], ['partner_id'])['partner_id']
        if not partner_id and not vals.get('partner_id', False):
            autoconsum_obj = self.pool.get('giscedata.autoconsum')
            autoconsum_id = self.read(cursor, uid, ids[0], ['autoconsum_id'])['autoconsum_id']
            seccio_registre = autoconsum_obj.read(cursor, uid, autoconsum_id[0], ['seccio_registre'])['seccio_registre']

            if seccio_registre == '2':
                raise osv.except_osv(_("No s'ha informat titular"),
                                     _(u"L'autoconsum que intenta associar al generador es amb "
                                       u"excedents i per tant el titular s'ha d'informar al generador. "
                                       u"Si us plau, informi un titular per a poder continuar."))
        return super(GiscedataAutoconsumGenerador, self).write(cursor, uid, ids, vals, context)

    _columns = {
        'cil': fields.char('CIL', size=25),
        'tec_generador': fields.selection(TABLA_126, 'Tipus generació', required=True),
        'combustible': fields.char('Combustible', size=25),
        'pot_instalada_gen': fields.float('Potencia generació', digits=(10, 3), required=True),
        'tipus_installacio': fields.selection(TABLA_129, 'Tipus instal·lació', required=True),
        'esquema_mesura': fields.selection(TABLA_130, 'Esquema mesura'),
        'ssaa': fields.boolean('SSAA', required=True),
        'ref_cadastre': fields.char('Ref. Cadastral', size=20),
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum', required=True),

        # UTM
        'utm_x': fields.char('Coord. X (m)', size=8),
        'utm_y': fields.char('Coord. Y (m)', size=8),
        'utm_fus': fields.char('Fus', size=2, help="Entre 1 i 60"),
        'utm_banda': fields.char('Banda', help="Des de la C a la X (excloent I i O)", size=60),

        'partner_id': fields.many2one('res.partner', 'Titular'),
        'data_alta': fields.date('Data Alta', required=True),
        'data_baixa': fields.date('Data Baixa'),
        'active': fields.boolean('Actiu'),
    }

    _defaults = {
        'ssaa': lambda *a: False,
        'active': lambda *a: True,
    }


GiscedataAutoconsumGenerador()


class GiscedataAutoconsumModcontractual(osv.osv):
    """Modificació Contractual d'un Autoconsum."""
    _name = 'giscedata.autoconsum.modcontractual'

    _modcontractual_states_selection = [
        ('esborrany', "Esborrany"),
        ('actiu', "Actiu"),
        ('baixa', "Baixa"),
    ]

    _tipus_modificacio_contractual = [
        ('alta', 'Alta'),
        ('baixa', 'Baixa'),
        ('mod', 'Modificació'),
    ]

    def unlink(self, cursor, uid, ids, context=None):
        """Comprovem que es pot eliminar la modificació contractual.
        """
        # msg_error = ''
        # for modcon in self.browse(cursor, uid, ids, context):
        #     if modcon.state != 'pendent':
        #         msg_error += _(u'- La modificació contractual %s de l\'autoconsum '
        #                        u'%s no es pot eliminar. Està en estat %s.\n' %
        #                        (modcon.name, modcon.autoconsum_id.cau,
        #                         modcon.state))
        # if msg_error:
        #     raise osv.except_osv('Error', msg_error, 'error')
        res = super(GiscedataAutoconsumModcontractual, self).unlink(cursor, uid, ids, context)
        return res

    def wkf_esborrany(self, cursor, uid, ids):
        """Estat esborrany."""
        for modcontractual in self.browse(cursor, uid, ids):
            self.append_modificacio(cursor, uid, modcontractual.id)
        self.write(cursor, uid, ids, {'state': 'esborrany'})
        return True

    def wkf_actiu(self, cursor, uid, ids):
        """Activem la modificació contractual."""
        for modcontractual in self.browse(cursor, uid, ids):
            mod_id = modcontractual.id
            modcontractual.autoconsum_id.write({'modcontractual_activa': mod_id})
            # Escrivim l'estat per tal que si hi ha modcontractual_ant vegi
            # que hem activat la següent
            modcontractual.write({'state': 'actiu', 'active': 1})
            # Enviem un signal de guardar a l'anterior si n'hi ha per tal
            # que comprovi les condicions del workflow
            if modcontractual.modcontractual_ant:
                modcontractual.modcontractual_ant.write({})

        return True

    def wkf_baixa(self, cursor, uid, ids):
        """Estat baixa."""
        self.write(cursor, uid, ids, {'state': 'baixa', 'active': 0})
        return True

    def get_fields_for_autoconsum(self, cursor, uid, context=None):
        """Retorna una llista de camps per llegir a l'autoconsum."""
        res_fields = self.fields_get_keys(cursor, uid)
        fields_to_delete = ['id',
                            'name',
                            'autoconsum_id',
                            'modcontractual_ant',
                            'modcontractual_seg',
                            'active',
                            'state',
                            'tipus',
                            'observacions']

        for field in fields_to_delete:
            if field in res_fields:
                res_fields.remove(field)

        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        autoconsum_fields = autoconsum_obj.fields_get_keys(cursor, uid)

        # Es converteix a llista perquè esborrem items de la llista que
        # estem iterant. D'aquesta forma tenim una copia
        for field in list(res_fields):
            if field not in autoconsum_fields:
                res_fields.remove(field)

        return res_fields

    def get_data_final(self, cursor, uid, data_inici, context=None):
        """Obte la data final d'una modificacio contractual"""

        dies_durada_autoconsum = int(self.pool.get('res.config').get(cursor,
                                                                     uid, 'giscedata_autoconsum_autoconsum_durada', 364))
        year, month, day = map(int, data_inici.split('-'))
        data_final = (date(year, month, day)
                      + timedelta(days=dies_durada_autoconsum))
        days = (calendar.isleap(year + 1)
                and data_final >= date(year + 1, 2, 28)
                and dies_durada_autoconsum + 1
                or dies_durada_autoconsum)
        data_final = (date(year, month, day)
                      + timedelta(days=days)).strftime('%Y-%m-%d')
        return data_final

    def get_vals_from_autoconsum(self, cursor, uid, autoconsum_id, context=None):
        """Obté els valors per emplenar una modificació contractual."""
        fields_to_read = self.get_fields_for_autoconsum(cursor, uid, context)
        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        vals = autoconsum_obj.read(cursor, uid, [autoconsum_id], fields_to_read, context)[0]

        # Transformem els many2one (id, name) a id
        for field, value in vals.items():
            if isinstance(value, type(())):
                vals[field] = value[0]
            if (isinstance(value, list) and
                    self.fields_get(cursor, uid, [field])[field]['type'] == 'many2many'):
                vals[field] = [(6, 0, value)]

        # Assignem 'id' a 'autoconsum_id' i eliminem la clau 'id'
        vals['autoconsum_id'] = vals['id']
        del vals['id']

        autoconsum = autoconsum_obj.browse(cursor, uid, autoconsum_id, context)

        if autoconsum.modcontractual_activa and autoconsum.modcontractual_activa.data_final:
            year, month, day = map(int, autoconsum.modcontractual_activa.data_final.split('-'))
            data_inici = (date(year, month, day) + timedelta(days=1)).strftime('%Y-%m-%d')
        else:
            data_inici = autoconsum.data_alta

        # Si té data de baixa ja li assignem
        if autoconsum.data_baixa:
            data_final = autoconsum.data_baixa
        else:
            data_final = self.get_data_final(cursor, uid, data_inici, context)

        vals['data_inici'] = data_inici
        vals['data_final'] = data_final

        # Mirem si és del tipus 'alta' o 'modificacio'
        if autoconsum.modcontractual_activa and len(autoconsum.modcontractuals_ids):
            vals['tipus'] = 'mod'
        else:
            vals['tipus'] = 'alta'

        # Hem de posar la seqüencia que serà el número de modificacions que
        # tinguem fetes
        vals['name'] = len(autoconsum.modcontractuals_ids) + 1

        return vals

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):
        """ Si es possible desfà la darrera modificació contractual """
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res = False
        autoconsum_obj = self.pool.get('giscedata.autoconsum')

        # If not last modcon, cannot make the change
        modcon_vals = self.read(cursor, uid, ids, ['modcontractual_seg', 'autoconsum_id'])[0]

        if modcon_vals['modcontractual_seg']:
            raise osv.except_osv('Error', _(u"No es pot esborrar aquesta modificació contractual, "
                                            u"ja que no és la última."))
        else:
            autoconsum_id = modcon_vals['autoconsum_id'][0]
            res = autoconsum_obj.undo_last_modcontractual(cursor, uid, [autoconsum_id], context)

        return res

    def aplicar_modificacio(self, cursor, uid, mod_id, autoconsum_id=None):
        """Aplica els canvis d'una modificació contractual a un autoconsum."""
        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        autoconsum_fields = self.get_fields_for_autoconsum(cursor, uid)
        changes = self.read(cursor, uid, [mod_id], autoconsum_fields)[0]

        # Eliminem la clau 'id' que sempre la retorna al fer un read
        changes['modcontractual_activa'] = changes['id']
        del changes['id']

        # Hem de tenir en compte que els many2one retornen sempre
        # tuple(id, name)
        for field, value in changes.items():
            if isinstance(value, type(())):
                changes[field] = value[0]
            if (isinstance(value, list) and
                    self.fields_get(cursor, uid, [field])[field]['type'] == 'many2many'):
                changes[field] = [(6, 0, value)]

        if not autoconsum_id:
            autoconsum_id = self.browse(cursor, uid, mod_id).autoconsum_id.id

        # Apliquem aquests canvis
        autoconsum_obj.write(cursor, uid, [autoconsum_id], changes)

        return True

    def append_modificacio(self, cursor, uid, mod_id):
        """Afegeix una modificació contractual a la llista i actualitza els
        camps anterior i següent."""
        modcontractual = self.browse(cursor, uid, mod_id)
        if modcontractual.autoconsum_id.modcontractual_activa:
            mod_activa = modcontractual.autoconsum_id.modcontractual_activa
            mod_activa.write({'modcontractual_seg': mod_id, 'state': 'baixa', 'active': False})
            modcontractual.write({'modcontractual_ant': mod_activa.id})
        return True

    # Constraints
    def _cnt_check_date(self, cursor, uid, ids):
        """Comprova que no hi hagi dates superposades."""
        for mod_contrac in self.browse(cursor, uid, ids):
            # Busquem si ja hi ha un autoconsum solapat
            # (Fem servir la funció OVERLAPS de PostgreSQL)
            for autoconsum in mod_contrac.autoconsum_id.modcontractuals_ids:
                if autoconsum.id != mod_contrac.id:
                    cursor.execute("""SELECT (date %s, date %s) OVERLAPS (date %s, date %s)""",
                                   (mod_contrac.data_inici, mod_contrac.data_final,
                                    autoconsum.data_inici, autoconsum.data_final))
                    res = cursor.fetchone()
                    if res and res[0]:
                        return False
        return True

    def _cnt_check_gaps(self, cursor, uid, ids):
        """Comprova que no hi hagi forats entre autoconsums."""
        for mod_contract in self.browse(cursor, uid, ids):
            # Només ho comprovem si la modificació contractual
            # té autoconsum anterior
            if mod_contract.modcontractual_ant:
                # Restem la data_final de l'anterior de la data_inici
                # de l'actual si és més gran que 1 vol dir que hi ha
                # més d'un dia de diferència
                cursor.execute("SELECT date %s - date %s",
                               (mod_contract.data_inici,
                                mod_contract.modcontractual_ant.data_final))
                dies = cursor.fetchone()[0]
                if dies > 1:
                    return False
        return True

    _columns = {
        'name': fields.char('Codi modificació', size=64, readonly=True,
                            required=True),
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum',
                                         required=True, ondelete='cascade',
                                         select=True),
        'modcontractual_ant': fields.many2one('giscedata.autoconsum.modcontractual',
                                              'Modificació anterior'),
        'modcontractual_seg': fields.many2one('giscedata.autoconsum.modcontractual',
                                              'Modificació següent', ondelete="set null"),
        'active': fields.boolean('Activa', readonly=True, required=True),
        'state': fields.selection(_modcontractual_states_selection, 'Estat',
                                  required=True, readonly=True),
        'tipus': fields.selection(_tipus_modificacio_contractual, 'Tipus',
                                  required=True),
        'observacions': fields.text('Observacions'),
        'data_inici': fields.date('Data inici', required=True, select=True),
        'data_final': fields.date('Data final', select=True),

        # Dades de l'autoconsum
        'cau': fields.char('CAU', size=26, required=True),
        'seccio_registre': fields.selection(TABLA_127, 'Secció registre', required=True),
        'subseccio': fields.selection(TABLA_128, 'Subsecció'),
        'collectiu': fields.boolean('Colectiu'),
        'data_alta': fields.date('Data Alta'),
        'data_baixa': fields.date('Data Baixa'),
    }

    _constraints = [
        (_cnt_check_date, 'Ja existeix un autoconsum en aquestes dates',
         ['data_inici', 'data_final']),
        (_cnt_check_gaps, 'Hi ha dies de separació entre autoconsums',
         ['data_inici']),
    ]

    _sql_constraints = [
        ('date_coherence',
         "CHECK (data_inici <= coalesce(data_final, '2038-01-01'))",
         'data_inici ha de ser anterior a data_final'),
    ]

    _defaults = {
        'active': lambda *a: 1,
        'state': lambda *a: 'actiu',
    }

    _order = 'data_final desc'


GiscedataAutoconsumModcontractual()


class GiscedataPolissaExtend(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def onchange_autoconsum_id(self, cursor, uid, ids, autoconsum_id, context={}):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        val = autoconsum_obj.read(cursor, uid, autoconsum_id, ['collectiu'])['collectiu']
        res.update({'value': {'is_autoconsum_collectiu': val}})
        return res

    _columns = {
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum (CAU)', readonly=True, states={
            'esborrany': [('readonly', False)],
            'validar': [('readonly', False)],
            'modcontractual': [('readonly', False)]}),
        'is_autoconsum_collectiu': fields.boolean("Es colectiu"),
        'coef_repartiment': fields.char('Coeficient de repartiment', readonly=True, size=10, states={
            'esborrany': [('readonly', False)],
            'validar': [('readonly', False)],
            'modcontractual': [('readonly', False)]}),
    }

    _defaults = {
        'coef_repartiment': lambda *a: '1.0',
        'is_autoconsum_collectiu': lambda *a: False,
    }


GiscedataPolissaExtend()


class GiscedataPolissaModcontractualExtend(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum (CAU)', readonly=True),
        'coef_repartiment': fields.char('Coeficient de repartiment', readonly=True, size=10),
    }

    _defaults = {
        'coef_repartiment': lambda *a: '1.0',
    }


GiscedataPolissaModcontractualExtend()


class GiscedataAutoconsumCupsAutoconsum(osv.osv):

    _name = 'giscedata.autoconsum.cups.autoconsum'

    def alta_cups_autoconsum(self, cursor, uid, ids, cups_id, autoconsum_id, data_alta, context=None):

        cups_autoconsum_obj = self.pool.get('giscedata.autoconsum.cups.autoconsum')
        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        cups_obj = self.pool.get('giscedata.cups.ps')

        autoconsum_state = autoconsum_obj.read(cursor, uid, autoconsum_id, ['state'])['state']

        # autoconsum must be in draft or active state
        if autoconsum_state not in ['esborrany', 'actiu']:
            raise osv.except_osv(_(u"Estat incompatible"),
                                 _(u"L'autoconsum ha d'estar en estat esborrany o actiu per "
                                   u"poderdonar-lo d'alta per aquest CUPS."))

        autoconsum_per_baixa = False

        for record in cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)]):
            vals = cups_autoconsum_obj.read(cursor, uid, record, ['data_inici', 'data_final'])
            # if data_final is False this record has to be updated with data_inici - 1 day
            if not vals['data_final']:
                autoconsum_per_baixa = record
            if vals['data_inici'] >= data_alta:
                raise osv.except_osv(_(u"Conflicte de dates"),
                                     _(u"La data d'alta del nou autoconsum no pot ser anterior o igual "
                                       u"a cap dels autoconsums associats anteriorment"))

        if autoconsum_per_baixa:
            data_baixa = datetime.strptime(data_alta, '%Y-%m-%d')
            data_baixa = data_baixa - timedelta(days=1)
            data_baixa = data_baixa.strftime("%Y-%m-%d")
            cups_autoconsum_obj.write(cursor, uid, autoconsum_per_baixa, {'data_final': data_baixa})

        values = {
            'cups_id': cups_id,
            'autoconsum_id': autoconsum_id,
            'data_inici': data_alta,
        }

        cups_autoconsum_obj.create(cursor, uid, values)
        cups_obj.write(cursor, uid, cups_id, {'autoconsum_id': autoconsum_id})

    def baixa_cups_autoconsum(self, cursor, uid, ids, cups_id, autoconsum_id, data_baixa, context=None):

        cups_autoconsum_obj = self.pool.get('giscedata.autoconsum.cups.autoconsum')
        cups_obj = self.pool.get('giscedata.cups.ps')

        cups_autoconsum_id = cups_autoconsum_obj.search(cursor, uid, [
            ('cups_id', '=', cups_id),
            ('autoconsum_id', '=', autoconsum_id)
        ], order='data_inici desc')

        # if there's no ids you cannot unsubscribe relation
        if not cups_autoconsum_id:
            raise osv.except_osv(_(u"Conflicte de dades"),
                                 _(u"Hi ha un problema amb aquesta relació CUPS - Autoconsum. "
                                   u"No existeix la relació i per tant no es pot donar de baixa."))

        vals = cups_autoconsum_obj.read(cursor, uid, cups_autoconsum_id[0], ['data_inici', 'data_final'])

        # End date must be grater or equal than init date
        if vals['data_inici'] > data_baixa:
            raise osv.except_osv(_(u"Conflicte de dates"),
                                 _(u"La data de baixa indicada no pot ser anterior a la data d'alta de "
                                   u"la relació CUPS-autoconsum, fixada a {}".format(vals['data_inici'])))

        # If end date has value the it's already unsubscribed
        if vals['data_final']:
            raise osv.except_osv(_(u"Donat de baixa prèviament"),
                                 _(u"El registre que intenta donar de baixa ja va ser donat de "
                                   u"baixa amb anterioritat a data {}".format(vals['data_final'])))
        else:
            cups_autoconsum_obj.write(cursor, uid, cups_autoconsum_id[0], {'data_final': data_baixa})
            cups_obj.write(cursor, uid, cups_id, {'autoconsum_id': False})

    _columns = {
        'autoconsum_id': fields.many2one('giscedata.autoconsum', 'Autoconsum'),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS'),
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
    }


GiscedataAutoconsumCupsAutoconsum()

# Un cop creada la taula modificació contractual podem posar l'activa a
# l'autoconsum, sino per dependències no ens deixa crear les taules
class GiscedataAutoconsumExtend(osv.osv):

    _name = 'giscedata.autoconsum'
    _inherit = 'giscedata.autoconsum'

    _columns = {
        'modcontractual_activa': fields.many2one(
            'giscedata.autoconsum.modcontractual',
            'Modificació contractual actual',
            readonly=True),

        'polissa_id': fields.one2many('giscedata.polissa',
                                      'autoconsum_id', 'Pòlisses',
                                      readonly=True),
    }


GiscedataAutoconsumExtend()


class GiscedataCupsExtend(osv.osv):

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _columns = {
        'hist_autoconsum': fields.one2many('giscedata.autoconsum.cups.autoconsum',
                                           'cups_id', 'Històric autoconsums'),
    }


GiscedataCupsExtend()
