# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction

class TestGiscedataLectures(testing.OOTestCase):

    def create_cups_autoconsum(self, cursor, uid, cups_id, cups_name, date):
        autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum')
        wiz_obj = self.openerp.pool.get('wizard.alta.baixa.autoconsum')

        autoconsum_vals = {
            'cau': cups_name + 'A001',
            'data_alta': date,
            'seccio_registre': '1',
        }
        autoconsum_id = autoconsum_obj.create(cursor, uid, autoconsum_vals)
        context = {'active_id': cups_id}

        wiz_id = wiz_obj.create(cursor, uid, {
            'autoconsum_id': autoconsum_id,
            'data': date}, context=context)

        wiz_obj.action_donar_alta_autoconsum(cursor, uid, [wiz_id], context=context)

        return autoconsum_id

    def unsubscribe_cups_autoconsum(self, cursor, uid, cups_id, autoconsum_id):
        wiz_obj = self.openerp.pool.get('wizard.alta.baixa.autoconsum')
        context = {'active_id': cups_id}

        wiz_id = wiz_obj.create(cursor, uid, {
            'autoconsum_id': autoconsum_id,
            'data': '2019-10-10'}, context=context)

        wiz_obj.action_donar_baixa_autoconsum(cursor, uid, [wiz_id], context=context)

    def test_alta_cups_autoconsum_relation(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum.cups.autoconsum')
            imd_obj = self.openerp.pool.get('ir.model.data')
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol = pol_obj.browse(cursor, uid, polissa_id)
            cups_id = pol.cups.id
            cups_name = pol.cups.name

            old_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])

            autoconsum_id = self.create_cups_autoconsum(cursor, uid, cups_id, cups_name, '2019-10-10')

            new_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])

            last_value = list(set(new_values) - set(old_values))

            self.assertIsNot(last_value, False)
            vals = cups_autoconsum_obj.read(cursor, uid, last_value[0], [])
            self.assertEqual(vals['autoconsum_id'][0], autoconsum_id)
            self.assertEqual(vals['autoconsum_id'][1], pol.cups.name + 'A001')
            self.assertEqual(vals['cups_id'][0], cups_id)
            self.assertEqual(vals['cups_id'][1], cups_name)
            self.assertEqual(vals['data_inici'], '2019-10-10')
            self.assertFalse(vals['data_final'])

            cups_vals = cups_obj.read(cursor, uid, cups_id, ['autoconsum_id'])['autoconsum_id']

            self.assertEqual(cups_vals[0], autoconsum_id)

    def test_baixa_cups_autoconsum_relation(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum.cups.autoconsum')
            imd_obj = self.openerp.pool.get('ir.model.data')
            pol_obj = self.openerp.pool.get('giscedata.polissa')

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol = pol_obj.browse(cursor, uid, polissa_id)
            cups_id = pol.cups.id
            cups_name = pol.cups.name

            old_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])

            autoconsum_id = self.create_cups_autoconsum(cursor, uid, cups_id, cups_name, '2019-10-10')

            new_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])

            self.unsubscribe_cups_autoconsum(cursor, uid, cups_id, autoconsum_id)

            last_value = list(set(new_values) - set(old_values))

            self.assertIsNot(last_value, False)
            vals = cups_autoconsum_obj.read(cursor, uid, last_value[0], [])
            self.assertEqual(vals['autoconsum_id'][0], autoconsum_id)
            self.assertEqual(vals['autoconsum_id'][1], pol.cups.name + 'A001')
            self.assertEqual(vals['cups_id'][0], cups_id)
            self.assertEqual(vals['cups_id'][1], cups_name)
            self.assertEqual(vals['data_inici'], '2019-10-10')
            self.assertEqual(vals['data_final'], '2019-10-10')

    def test_alta_second_cups_autoconsum_relation(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum.cups.autoconsum')
            imd_obj = self.openerp.pool.get('ir.model.data')
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol = pol_obj.browse(cursor, uid, polissa_id)
            cups_id = pol.cups.id
            cups_name = pol.cups.name

            old_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])

            autoconsum_id = self.create_cups_autoconsum(cursor, uid, cups_id, cups_name, '2019-10-10')
            new_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])
            first_rel_id = list(set(new_values) - set(old_values))

            autoconsum_id2 = self.create_cups_autoconsum(cursor, uid, cups_id, cups_name, '2019-10-12')
            last_values = cups_autoconsum_obj.search(cursor, uid, [('cups_id', '=', cups_id)])
            second_rel_id = list(set(last_values) - set(new_values))

            self.assertIsNot(first_rel_id, False)
            first_vals = cups_autoconsum_obj.read(cursor, uid, first_rel_id[0], [])
            self.assertEqual(first_vals['autoconsum_id'][0], autoconsum_id)
            self.assertEqual(first_vals['autoconsum_id'][1], pol.cups.name + 'A001')
            self.assertEqual(first_vals['cups_id'][0], cups_id)
            self.assertEqual(first_vals['cups_id'][1], cups_name)
            self.assertEqual(first_vals['data_inici'], '2019-10-10')
            self.assertEqual(first_vals['data_final'], '2019-10-11')

            self.assertIsNot(second_rel_id, False)
            second_vals = cups_autoconsum_obj.read(cursor, uid, second_rel_id[0], [])
            self.assertEqual(second_vals['autoconsum_id'][0], autoconsum_id2)
            self.assertEqual(second_vals['autoconsum_id'][1], pol.cups.name + 'A001')
            self.assertEqual(second_vals['cups_id'][0], cups_id)
            self.assertEqual(second_vals['cups_id'][1], cups_name)
            self.assertEqual(second_vals['data_inici'], '2019-10-12')
            self.assertFalse(second_vals['data_final'])

            cups_vals = cups_obj.read(cursor, uid, cups_id, ['autoconsum_id'])['autoconsum_id']

            self.assertEqual(cups_vals[0], autoconsum_id2)


