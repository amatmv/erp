# -*- coding: utf-8 -*-

import unittest

from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from dateutil.relativedelta import relativedelta

from .test_autoconsum import *
from expects import *
from utils import *


def crear_polissa(pool, cursor, uid):
    polissa_obj = pool.get('giscedata.polissa')
    modcon_obj = pool.get('giscedata.polissa.modcontractual')
    imd_obj = pool.get('ir.model.data')

    polissa_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_polissa', 'polissa_0001'
    )[1]
    pol = polissa_obj.browse(cursor, uid, polissa_id)
    modcon_id = pol.modcontractuals_ids[0].id
    modcon_obj.write(cursor, uid, modcon_id, {'data_final': '2016-03-01'})

    # Creem la segona modificació contractual fent un canvi de potencia

    crear_modcon(cursor, uid, 7.000, '2016-03-02', '2016-05-02')

    # Creem la tercera modificació contractual fent un canvi de potencia

    crear_modcon(cursor, uid, 7.250, '2017-05-03', '2017-01-01')

    return polissa_obj.browse(cursor, uid, polissa_id)


class TestWizardChangeDates(testing.OOTestCase):

    def setUp(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def tearDown(self):
        self.txn.stop()

    def crear_modcon(self, cursor, uid, potencia, ini, fi):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def crear_polissa(self, cursor, uid):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        modcon_obj = pool.get('giscedata.polissa.modcontractual')
        imd_obj = pool.get('ir.model.data')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon_id = pol.modcontractuals_ids[0].id
        modcon_obj.write(cursor, uid, modcon_id, {'data_final': '2016-03-01'})

        # Creem la segona modificació contractual fent un canvi de potencia

        self.crear_modcon(cursor, uid, 7.000, '2016-03-02', '2016-05-02')

        # Creem la tercera modificació contractual fent un canvi de potencia

        self.crear_modcon(cursor, uid, 7.250, '2017-05-03', '2017-01-01')

        return polissa_obj.browse(cursor, uid, polissa_id)

    def to_date(self, date):
        """
        :param date: datetime in format: '%Y-%m-%d %H:%M:%S'
        :return: '%Y-%m-%d' formatted date
        """
        try:
            assert type(date) is str
            return datetime.strptime(date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        except Exception as e:
            raise Exception(
                u"El tipus de la data ha de ser str amb format '%Y-%m-%d "
                u"%H:%M:%S'"
            )

    def test_data_baixa_modcon_es_modifica(self):
        """Aquest test comprova que la data de baixa de la modificació
            contractual és modificada per la nova data de baixa
            de l'assistent. També comprova que la data de baixa de la
            pòlissa (si en te) també es modifica (si es modifica la ultima
            modificació contractual)."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = self.crear_polissa(cursor, uid)
        wz_canviar_dates = pool.get('wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[0]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_final': '2017-02-01',
            'data_inici': '2016-04-03',
        })
        polissa_obj.write(cursor, uid, polissa_id, {
            'renovacio_auto': False,
            'data_baixa': '2017-01-01'
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[0]

        # Comprovació de resultats

        self.assertEqual(
            modcon.data_final,
            wiz.data_final
        )
        self.assertEqual(
            wiz.data_final,
            pol.data_baixa
        )

    def test_data_alta_modcon_es_modifica(self):
        """Aquest test comprova que la data d'alta de la modificació
        contractual és modificada per la nova data d'alta de l'assistent.
        També comprova que la data d'alta de la pòlissa és modificada si
        la modificació contractual modificaca és la primera."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = self.crear_polissa(cursor, uid)
        wz_canviar_dates = pool.get('wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[2]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_inici': '2016-02-01',
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[2]

        # Comprovació de resultats

        self.assertEqual(
            modcon.data_inici,
            wiz.data_inici
        )
        self.assertEqual(
            pol.data_alta,
            wiz.data_inici
        )

    def test_altres_modcons_no_es_solapen(self):
        """Aquest test comprova que les noves dates d'alta i baixa
        d'una modificació contractual no es solapen amb les respectives
        modificacions contractuals anterior i seguent. També comprova que
        la modificació contractual ha sigut modificada amb les dades
        entrades a l'assistent."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = self.crear_polissa(cursor, uid)
        wz_canviar_dates = pool.get('wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[1]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_final': '2016-07-02',
            'data_inici': '2016-04-01',
            'data_firma': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon = pol.modcontractuals_ids[1]

        data_modcon_ant = wiz.data_inici
        data_modcon_ant = datetime.strptime(data_modcon_ant, '%Y-%m-%d')
        data_modcon_ant += relativedelta(days=-1)
        data_modcon_ant = data_modcon_ant.strftime('%Y-%m-%d')

        data_modcon_seg = wiz.data_final
        data_modcon_seg = datetime.strptime(data_modcon_seg, '%Y-%m-%d')
        data_modcon_seg += relativedelta(days=1)
        data_modcon_seg = data_modcon_seg.strftime('%Y-%m-%d')

        # Comprovació de resultats

        self.assertEqual(
            modcon.data_final,
            wiz.data_final
        )
        self.assertEqual(
            modcon.data_inici,
            wiz.data_inici
        )
        self.assertEqual(
            data_modcon_ant,
            modcon.modcontractual_ant.data_final
        )
        self.assertEqual(
            data_modcon_seg,
            modcon.modcontractual_seg.data_inici
        )

    def test_data_firma_modcon_es_modifica(self):
        """Aquest test comprova que la data de firma de la modificació
        contractual és igual a la de l'assistent. També comprova
        que la data de firma del contracte es modifica (només si és la primera
        modificació contractual)."""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        # Canviem la data de firma de la primera modificació contractual.
        # Com que no està activa, no ha de modificar la data de firma de la
        # pòlissa
        pol = self.crear_polissa(cursor, uid)
        wz_canviar_dates = pool.get('wizard.canviar.dates')
        modcon = pol.modcontractuals_ids[2]
        ctx = {'active_id': modcon.id}

        wz_id = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz = wz_canviar_dates.browse(cursor, uid, wz_id, ctx)
        wiz.write({
            'data_firma': '2016-03-21 00:00:00',
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon_primera = pol.modcontractuals_ids[2]

        # Canviem la data de firma de la ultima modificació contractual.
        # Com que és la modificació contractual activa, la data de
        # firma del contracte s'hauria de modificar a la pòlissa i
        # també a la modificació contractual.
        modcon_ultima = pol.modcontractuals_ids[0]
        ctx = {'active_id': modcon_ultima.id}

        wz_id_ultima = wz_canviar_dates.create(cursor, uid, {}, ctx)
        wiz_ultima = wz_canviar_dates.browse(cursor, uid, wz_id_ultima, ctx)
        wiz_ultima.write({
            'data_firma': datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        })
        res = wz_canviar_dates.change_date(cursor, uid, [wz_id_ultima], ctx)
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        modcon_ultima = pol.modcontractuals_ids[0]

        # Comprovació de resultats

        self.assertEqual(
            self.to_date(modcon_primera.data_firma_contracte),
            self.to_date(wiz.data_firma)
        )
        self.assertEqual(
            self.to_date(pol.data_firma_contracte),
            self.to_date(wiz_ultima.data_firma)
        )
        self.assertEqual(
            self.to_date(modcon_ultima.data_firma_contracte),
            self.to_date(wiz_ultima.data_firma)
        )


class TestGiscedataPolissa(testing.OOTestCase):

    def test_afegir_log_polissa(self):
        """ Aquest test comprova que s'ha creat correctament el log per al
         canvi d'estat de la pòlissa"""

        pool = self.openerp.pool

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            model_obj = pool.get('ir.model.data')
            log_obj = pool.get('giscedata.polissa.log')

            polissa_id = model_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_actual = polissa_obj.read(cursor, uid, polissa_id, ['state'])

            polissa_obj.create_log(
                cursor, uid, polissa_actual['id'], polissa_actual['state']
            )

            self.assertTrue(log_obj.search(cursor, uid, [
                ('polissa_id', '=', polissa_actual['id']),
                ('state', '=', polissa_actual['state']),
            ]))

    def test_duplicate_polissa(self):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.generar_periodes_potencia(cursor, uid, [polissa_id])

            default_values = {
                'name': False,
                'potencies_periode': [],
                'modcontractuals_ids': [],
                'modcontractual_activa': False,
                'log_ids': [],
                'data_alta': False,
                'data_baixa': False,
                'data_firma_contracte': False,
            }

            copy_res_id = polissa_obj.copy(cursor, uid, polissa_id)
            polissa_data = polissa_obj.read(
                cursor, uid, copy_res_id, default_values.keys()
            )
            #  Check for the log_ids out of the default.
            #  Ensure only the ID of the creation log is there
            self.assertTrue(len(polissa_data['log_ids']) == 1)

            polissa_data.pop('id')
            polissa_data.pop('log_ids')
            default_values.pop('log_ids')
            self.assertDictEqual(default_values, polissa_data)

    def test_on_change_potencia_warning_if_not_normalized(self):
        """Si quan assignem una potencia aquesta no es normalitzada ens ha de
        sortir un warning avisant"""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tarifa = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20A_new'
            )[1]
            res = polissa_obj.onchange_potencia(
                cursor, uid, [], 5.61, tarifa
            )
            self.assertIn('warning', res.keys())
            warning = res['warning']
            expected_warning = {
                'title': u'Avís',
                'message': u'La potència no és normalitzada'
            }
            self.assertDictEqual(warning, expected_warning)

    def test_on_change_potencia_assign_tensio_on_normalized_power(self):
        """Si la potencia que ens passen és normalitzada assignem la tensió
        de forma automàtica"""
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tarifa = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20A_new'
            )[1]
            tensio = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tensions', 'tensio_3x230_400'
            )[1]
            res = polissa_obj.onchange_potencia(
                cursor, uid, [], 6.928, tarifa
            )
            self.assertIn('value', res.keys())
            value = res['value']
            expected_value = {
                'tensio_normalitzada': tensio,
            }
            self.assertDictEqual(value, expected_value)

    def test_get_num_periodes(self):
        pool = self.openerp.pool

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tarif_obj = pool.get('giscedata.polissa.tarifa')
            tarif_ids = tarif_obj.search(cursor, uid, [])
            tarifs = tarif_obj.read(cursor, uid, tarif_ids, ['name'])
            res = {}
            for t_id in tarifs:
                nar = tarif_obj.get_num_periodes(cursor, uid, [t_id['id']])
                np = tarif_obj.get_num_periodes(cursor, uid, [t_id['id']],
                                                tipus='tp')
                res.update({t_id['name']: (int(nar), int(np))})

            expected_values = {
                '2.0A': (1, 1),
                '2.0DHA': (2, 1),
                '2.0DHS': (3, 1),
                '2.1A': (1, 1),
                '2.1DHA': (2, 1),
                '2.1DHS': (3, 1),
                '3.0A': (3, 3),
                '3.1A': (3, 3),
                '3.1A LB': (3, 3),
                '6.1A': (6, 6),
                '6.1B': (6, 6),
                '6.2': (6, 6),
                '6.3': (6, 6),
                '6.4': (6, 6),
                '6.5': (6, 6),
            }
            self.assertDictEqual(res, expected_values)

    def test_get_tarif_from_ocsum_31A(self):
        pool = self.openerp.pool
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            tarif_obj = pool.get('giscedata.polissa.tarifa')
            # Get the tarif 3.1A with codi ocsum 011
            tarif_31_id = tarif_obj.get_tarifa_from_ocsum(cursor, uid, '011')
            tarif_31_name = tarif_obj.read(cursor, uid, tarif_31_id, ['name'])
            self.assertEqual('3.1A', tarif_31_name['name'])
            # Get the tarif 3.1A LB with the same codi ocsum (011) but with LB
            tarif_lb_id = tarif_obj.get_tarifa_from_ocsum(
                cursor, uid, '011', tarifa_lb=True
            )
            tarif_lb_name = tarif_obj.read(cursor, uid, tarif_lb_id, ['name'])
            self.assertEqual('3.1A LB', tarif_lb_name['name'])

    def test_get_potencies_dict(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pol_obj = pool.get('giscedata.polissa')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            # creates_modcons
            activar_polissa(pool, cursor, uid, contract_id)
            crear_modcon(
                pool, cursor, uid, contract_id,
                {'potencia': 6.600}, '2016-03-01', '2016-04-30'
            )
            crear_modcon(
                pool, cursor, uid, contract_id,
                {'potencia': 9.900}, '2016-05-01', '2016-06-30'
            )
            # gets current potencies_dict (9.9)
            p_dict = pol_obj.get_potencies_dict(
                cursor, uid, contract_id
            )
            expect(len(p_dict.keys())).to(equal(1))
            expect(p_dict).to(have_key('P1'))
            expect(p_dict.get('P1')).to(equal(9.9))

            # gets potencies_dict from modcontractual's
            #
            tests = {
                '2016-02-01': 6.0,
                '2016-04-01': 6.6,
                '2016-06-01': 9.9
            }
            for test_date, power in tests.items():
                ctx = {'date': test_date}
                p_dict = pol_obj.get_potencies_dict(
                    cursor, uid, contract_id, ctx
                )
                expect(len(p_dict.keys())).to(equal(1))
                expect(p_dict).to(have_key('P1'))
                expect(p_dict.get('P1')).to(equal(power))


class TestGiscedataPolissaWizzard(testing.OOTestCase):

    def test_get_norm_power_by_voltage(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            #pol_obj = pool.get('giscedata.polissa')
            #tarifa_obj = pool.get('giscedata.polissa.tarifa')
            #tensio_obj = pool.get('giscedata.tensions.tensio')

            tarifa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20A_new'
            )[1]

            tensio_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tensions', 'tensio_3x230_400'
            )[1]

            tensio_tr_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tensio_3x400'
            )[1]


            wz_normalized_power = pool.get('wizard.set.normalized.power')

            context_expected = {
                'tarifa_id': tarifa_id,
                'tensio_id': tensio_id,
            }

            context_to_translate = {
                'tarifa_id': tarifa_id,
                'tensio_id': tensio_tr_id,
            }

            expected_normalized_pow = wz_normalized_power.get_normalized(cursor,
                                            uid, context=context_expected)
            test_normalized_pow = wz_normalized_power.get_normalized(cursor,
                                             uid, context=context_to_translate)

            self.assertEqual(test_normalized_pow, expected_normalized_pow)
