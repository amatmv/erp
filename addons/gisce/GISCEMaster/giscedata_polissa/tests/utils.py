# -*- coding: utf-8 -*-


def crear_modcon(pool, cursor, uid, polissa_id, values, ini, fi):
    polissa_obj = pool.get('giscedata.polissa')
    pol = polissa_obj.browse(cursor, uid, polissa_id)
    pol.send_signal(['modcontractual'])
    polissa_obj.write(cursor, uid, polissa_id, values)
    wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

    ctx = {'active_id': polissa_id}
    params = {'duracio': 'nou'}

    wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
    wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
    res = wz_crear_mc_obj.onchange_duracio(
        cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
    )
    wiz_mod.write({
        'data_inici': ini,
        'data_final': fi
    })
    wiz_mod.action_crear_contracte(ctx)

def activar_polissa(pool, cursor, uid, polissa_id):
    polissa_obj = pool.get('giscedata.polissa')

    polissa_obj.send_signal(cursor, uid, [polissa_id], [
        'validar', 'contracte'
    ])
