# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_transformador_trafo(osv.osv):

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    _columns = {
      'name': fields.integer('Numero de transformador', required=True),
    }

    _order = "name asc"

giscedata_transformador_trafo()


# Steps SQL:
"""
ALTER TABLE giscedata_transformador_trafo ADD column name_int_tmp integer;
UPDATE giscedata_transformador_trafo SET name_int_tmp = name::int
ALTER TABLE giscedata_transformador_trafo DROP COLUMN name CASCADE;
ALTER TABLE giscedata_transformador_trafo RENAME name_int_tmp TO name;
"""
