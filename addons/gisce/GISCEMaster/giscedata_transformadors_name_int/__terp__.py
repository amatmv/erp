# -*- coding: utf-8 -*-
{
    "name": "Canviar a de char a int el nom del Tranformador",
    "description": """Canviar el camp name: varchar(50) a name:integer el camp del giscedata_transformadors_trafo.

****** ATENCIO ******

  * Abans d'instal·lar aquest mòdul, executar aquestes comandes a la base de dades:

ALTER TABLE giscedata_transformador_trafo ADD column name_int_tmp integer;
UPDATE giscedata_transformador_trafo SET name_int_tmp = name::int;
ALTER TABLE giscedata_transformador_trafo DROP COLUMN name CASCADE;
ALTER TABLE giscedata_transformador_trafo RENAME name_int_tmp TO name;

  * Després actualitzar el mòdul giscedatra_transformadors per tal que torni a generar les vistes del GIS.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
