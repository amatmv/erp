# -*- coding: utf-8 -*-
"""
Mòdul `partner_address_tipovia_trg`
===================================

Aquest mòdul fa del camp city un camp funció ja que molts reports fan servir
aquest camp i quan s'instal·la el mòdul `partner_address_tipovia` aquest deixa
de fer-se servir i mostrar-se. Amb aquest mòdul el què fem és escriure-hi el
valor de la població o del municipi, segons quin emplenin.

"""
from osv import fields, osv

class ResPartnerAddress(osv.osv):
    """Inherit de partner.address per afegir-hi un trigger pel camp city
    """
    _name = "res.partner.address"
    _inherit = "res.partner.address"

    def _trg_city(self, cursor, uid, ids, context=None):
        """Trigger que es cridarà per tal de tornar a calcular el camp city.
        """
        return ids

    def _trg_city_pob(self, cursor, uid, ids, context=None):
        pa_obj = self.pool.get('res.partner.address')
        return pa_obj.search(cursor, uid, [('id_poblacio', 'in', ids)])

    def _trg_city_mun(self, cursor, uid, ids, context=None):
        pa_obj = self.pool.get('res.partner.address')
        return pa_obj.search(cursor, uid, [('id_municipi', 'in', ids)])

    def _city(self, cursor, uid, ids, field_name, arg, context=None):
        """Mètode write, per sobreescriure el camp city original
        """
        res = {}
        for address in self.browse(cursor, uid, ids, context):
            city = address.city  # per defecte el què ja hi tingui
            if address.id_poblacio:  # primer mirem la població
                city = address.id_poblacio.name
            elif address.id_municipi:  # si no en té, el municipi
                city = address.id_municipi.name
            else:
                city = ''
            res[address.id] = city
        return res

    _columns = {
        'id_poblacio': fields.many2one('res.poblacio', 'Població'),
        'city': fields.function(
            _city,
            method=True,
            string="City",
            type="char",
            size=128,
            readonly=True,
            store={
                'res.partner.address': (
                    _trg_city, ['id_municipi', 'id_poblacio'], 10
                ),
                'res.poblacio': (
                    _trg_city_pob, ['name'], 10
                ),
                'res.municipi': (
                    _trg_city_mun, ['name'], 10
                )
            }
        ),
    }
ResPartnerAddress()
