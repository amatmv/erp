# -*- coding: utf-8 -*-
{
    "name": "GISCE Partner Address Tipo Via Trigger",
    "description": """Afegeix trigger per composar l'adreça""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "partner_address_tipovia"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
