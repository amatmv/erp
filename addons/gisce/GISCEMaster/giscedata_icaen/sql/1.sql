select
  p.name as "numero polissa",
  c.name as "cups",
  rp.name as "Nom de l'abonat",
  c.direccio as "Adreça subministrament",
  m.ine as "Codi municipi",
  m.name as "Nom municipi",
  pm.cnae as "Codi activitat economica",
  cn.descripcio as "Descripcio activitat economica",
  4 as "Tipus activitat economica",
  tr.name as "Tarifa d'acces",
  tr.descripcio as "Descripcio tarifa d'acces",
  pm.potencia as "Potencia electrica",
  coalesce(e.energia,0) as "Energia electrica consumida",
  com.ref as "Codi empresa comercialitzadora",
  com.name as "Nom empresa comercialitzadora",
  CASE WHEN p.data_alta > %(data_inici)s THEN p.data_alta ELSE null END as "Data d'alta",
  CASE WHEN p.data_baixa < %(data_final)s THEN p.data_baixa ELSE null END as "Data baixa"
from giscedata_polissa_modcontractual pm
left join giscedata_polissa p on pm.polissa_id = p.id
inner join (select
  m.polissa_id,
  max(m.data_final) as data_final
from
  giscedata_polissa_modcontractual m
where
  (m.data_final BETWEEN %(data_inici)s AND %(data_final)s or m.data_final >= %(data_final)s)
  and (m.data_inici BETWEEN %(data_inici)s AND %(data_final)s or m.data_inici <= %(data_inici)s)
and m.potencia >= 50
group by m.polissa_id
) as lastmodcon on lastmodcon.data_final = pm.data_final and lastmodcon.polissa_id = pm.polissa_id
left join (select
  p.id as polissa,
            sum(a.quantity * case when i.type = 'out_refund' then -1 else 1 end) as energia
           FROM
            giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN giscedata_facturacio_factura_linia l ON f.id = l.factura_id
            INNER JOIN account_invoice_line a ON l.invoice_line_id = a.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
           WHERE
            l.tipus = 'energia'
            and f.data_final between %(data_inici)s and %(data_final)s
           GROUP BY
            p.id
          ) as e on e.polissa = pm.polissa_id
left join giscedata_cups_ps c on pm.cups = c.id
left join res_partner rp on pm.titular = rp.id
left join res_municipi m on c.id_municipi = m.id
left join giscemisc_cnae cn on pm.cnae = cn.id
left join giscedata_polissa_tarifa tr on pm.tarifa = tr.id
left join res_partner com on pm.comercialitzadora = com.id
where pm.potencia >= 50