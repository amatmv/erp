SELECT
 m.ine as "Codi municipal",
 m.name as "Nom municipi",
 cnae.name as "Codi activitat economica",
 cnae.descripcio as "Descripcio activitat",
 4 as "Tipus activitat economica",
 count(pm.polissa_id) as "Nombre d'abonats",
 coalesce(sum(pm.potencia), 0) as "Potencia elèctrica",
 coalesce(sum(energia.energia), 0) as "Energia elèctrica",
 coalesce(sum(potencia.import), 0) as "Import terme potencia",
 coalesce(sum(energia.import), 0) as "Import terme energia",
 coalesce(sum(reactiva.import), 0) as "Import energia reactiva",
 coalesce(sum(exces_potencia.import), 0) as "Import excés potencia",
 coalesce(sum(iese.import), 0) as "Import impost electricitat",
 coalesce(sum(lloguer.import), 0) as "Import lloguer",
 coalesce(sum(iva.import), 0) as "Import impost iva",
 coalesce(sum(total.import), 0) as "Import total factural amb impostos"
FROM
 giscedata_polissa_modcontractual pm
 LEFT JOIN giscemisc_cnae cnae on pm.cnae = cnae.id
 LEFT JOIN giscedata_cups_ps c on pm.cups = c.id
 LEFT JOIN res_municipi m on c.id_municipi = m.id
 LEFT JOIN (
 select
  p.id as polissa,
  sum(a.quantity * case when i.type = 'out_refund' then -1 else 1 end) as energia,
  sum(a.price_subtotal * case when i.type = 'out_refund' then -1 else 1 end) as import
 FROM
            giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN giscedata_facturacio_factura_linia l ON f.id = l.factura_id
            INNER JOIN account_invoice_line a ON l.invoice_line_id = a.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
           WHERE
            l.tipus = 'energia'
            and f.data_final between %(data_inici)s and %(data_final)s
           GROUP BY
            p.id
          ) as energia on energia.polissa = pm.polissa_id
LEFT JOIN (
 select
  p.id as polissa,
  sum(a.price_subtotal * case when i.type = 'out_refund' then -1 else 1 end) as import
 FROM
            giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN giscedata_facturacio_factura_linia l ON f.id = l.factura_id
            INNER JOIN account_invoice_line a ON l.invoice_line_id = a.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
           WHERE
            l.tipus = 'potencia'
            and f.data_final between %(data_inici)s and %(data_final)s
           GROUP BY
            p.id
          ) as potencia on potencia.polissa = pm.polissa_id
LEFT JOIN (
 select
  p.id as polissa,
  sum(a.price_subtotal * case when i.type = 'out_refund' then -1 else 1 end) as import
 FROM
            giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN giscedata_facturacio_factura_linia l ON f.id = l.factura_id
            INNER JOIN account_invoice_line a ON l.invoice_line_id = a.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
           WHERE
            l.tipus = 'reactiva'
            and f.data_final between %(data_inici)s and %(data_final)s
           GROUP BY
            p.id
          ) as reactiva on reactiva.polissa = pm.polissa_id
LEFT JOIN (
 select
  p.id as polissa,
  sum(a.price_subtotal * case when i.type = 'out_refund' then -1 else 1 end) as import
 FROM
            giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN giscedata_facturacio_factura_linia l ON f.id = l.factura_id
            INNER JOIN account_invoice_line a ON l.invoice_line_id = a.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
           WHERE
            l.tipus = 'exces_potencia'
            and f.data_final between %(data_inici)s and %(data_final)s
           GROUP BY
            p.id
          ) as exces_potencia on exces_potencia.polissa = pm.polissa_id
LEFT JOIN (
select
  f.polissa_id as polissa,
  sum(t.amount * case when i.type = 'out_refund' then -1 else 1 end) as import
from
  account_invoice_tax t
  left join account_invoice i on t.invoice_id = i.id
  left join giscedata_facturacio_factura f on f.invoice_id = i.id
where
  t.name ilike '%%elec%%'
  and f.data_final between %(data_inici)s and %(data_final)s
group by
  f.polissa_id
) as iese on iese.polissa = pm.polissa_id
LEFT JOIN (
 select
  p.id as polissa,
  sum(a.price_subtotal * case when i.type = 'out_refund' then -1 else 1 end) as import
 FROM
            giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN giscedata_facturacio_factura_linia l ON f.id = l.factura_id
            INNER JOIN account_invoice_line a ON l.invoice_line_id = a.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
           WHERE
            l.tipus = 'lloguer'
            and f.data_final between %(data_inici)s and %(data_final)s
           GROUP BY
            p.id
          ) as lloguer on lloguer.polissa = pm.polissa_id
LEFT JOIN (
select
  f.polissa_id as polissa,
  sum(t.amount * case when i.type = 'out_refund' then -1 else 1 end) as import
from
  account_invoice_tax t
  left join account_invoice i on t.invoice_id = i.id
  left join giscedata_facturacio_factura f on f.invoice_id = i.id
where
  t.name ilike '%%iva%%'
  and f.data_final between %(data_inici)s and %(data_final)s
group by
  f.polissa_id
) as iva on iva.polissa = pm.polissa_id
LEFT JOIN (
select
  f.polissa_id as polissa,
  sum(i.amount_total * case when i.type = 'out_refund' then -1 else 1 end) as import
from
  account_invoice i
  left join giscedata_facturacio_factura f on f.invoice_id = i.id
where
  f.data_final between %(data_inici)s and %(data_final)s
group by
  f.polissa_id
) as total on total.polissa = pm.polissa_id
where ((pm.data_final >= %(data_inici)s::date + INTERVAL '1 YEAR' or (pm.data_final >= %(data_final)s and pm.modcontractual_seg is not null)) and pm.data_inici <= %(data_final)s)

group by m.ine, m.name, cnae.name, cnae.descripcio