# -*- coding: utf-8 -*-
{
    "name": "Fitxers ICAEN",
    "description": """Fitxers ICAEN, actualment fitxer 1, 3, 4, 5 i 6""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica_gencat",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_icaen_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
