# -*- encoding: utf-8 -*-
from osv import osv, fields
import netsvc
from tools import config
import base64
from datetime import datetime

class giscedata_icaen(osv.osv_memory):

    _name = 'giscedata.icaen'

    def _obte_tipus_informe(self, cursor, uid, context=None):
        """ Retorna la llista amb els fitxers que es poden generar """

        opcions = [(1, "1 - Dades de subministrament a clients "
                       "amb potencia major o igual a 50kW"),
                   (3, "3 - Dades per tarifa d'accés a nivell "
                       "municipal de subministraments"),
                   (4, "4 - Dades agrupades per sectors d'activitat "
                       "econòmica a nivell municipal de subministraments"),
                   (5, "5 - Dades agrupades per tarifa d'accés a nivell "
                       "municipal de subministraments a tots els "
                       "clients mitjançant empreses d'últim recurs"),
                   (6, "6 - Dades agrupades per sectors d'activitat econòmica a"
                       " nivell municipal de subministraments a tots els "
                       "clients mitjançant empreses d'últim recurs")]
        return opcions

    _columns = {
        'type': fields.selection(_obte_tipus_informe, 'Tipus', required=True),
        'anyo': fields.integer('Any', required=True),
        'name': fields.char('Nom', size=256),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }

    _defaults = {
        'anyo': lambda *a: datetime.now().year,
        'state': lambda *a: 'init'
    }


    def onchange_state(self, cursor, uid, ids, context=None):
        return {'value': {'state': 'init', 'file': False}}

    def export_file(self, cursor, uid, ids, context=None):

        sql_name = {1: '1.sql',
                    3: '3.sql',
                    4: '4.sql',
                    5: '5.sql',
                    6: '6.sql'}

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Començant l'exportació ICAEN")

        # Executem informes
        wizard = self.browse(cursor, uid, ids[0], context=context)
        sql_file = sql_name.get(wizard.type, False)
        sql = open('%s/%s/sql/%s' % (config['addons_path'], 'giscedata_icaen',
                                     sql_file)).read()

        params = {'data_inici': str(wizard.anyo) + '-01-01',
                  'data_final': str(wizard.anyo) + '-12-31'}
        cursor.execute(sql, params)

        informe = []
        for line in cursor.fetchall():
            line = list(line)
            informe.append(';'.join([unicode(a or '') for a in line]))
        informe = '\n'.join(informe)
        informe += '\n'
        mfile = base64.b64encode(informe.encode('utf-8'))

        filename = 'ICAEN_%s_%s.txt' % (wizard.anyo, wizard.type)
        self.write(cursor, uid, ids, {'state': 'end', 'file': mfile,
                                      'name': filename}, context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Finalitzant l'exportació ICAEN")

giscedata_icaen()