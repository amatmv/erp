# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataRevisionsAtTipusvaloracio(osv.osv):
    _name = 'giscedata.revisions.at.tipusvaloracio'
    _description = 'GISCE Afegit camp pes a valoracio AT'
    _inherit = 'giscedata.revisions.at.tipusvaloracio'

    _columns = {
      'pes': fields.integer('Pes'),
    }

GiscedataRevisionsAtTipusvaloracio()