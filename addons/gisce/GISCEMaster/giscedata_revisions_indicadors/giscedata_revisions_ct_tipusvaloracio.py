# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataRevisionsCtTipusvaloracio(osv.osv):
    _name = 'giscedata.revisions.ct.tipusvaloracio'
    _description = 'GISCE Afegit camp pes a valoracio'
    _inherit = 'giscedata.revisions.ct.tipusvaloracio'

    _columns = {
      'pes': fields.integer('Pes'),
    }

GiscedataRevisionsCtTipusvaloracio()