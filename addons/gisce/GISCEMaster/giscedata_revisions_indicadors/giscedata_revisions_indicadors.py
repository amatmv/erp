# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataRevisionsCtRevisio(osv.osv):
    _name = 'giscedata.revisions.ct.revisio'
    _inherit = 'giscedata.revisions.ct.revisio'

    def _ff_calc_ir1_ct(self, cr, uid, ids, field_name, arg, context):
        obj_revisio = self.pool.get('giscedata.revisions.ct.revisio')

        #Sum revisio
        sum_revisio = {}

        for revisio in obj_revisio.browse(cr, uid, ids):
            #Sumartori
            sum = 0
            if revisio.defectes_ids:
                for defecte in revisio.defectes_ids:
                    sum += defecte['valoracio']['pes']

            sum_revisio[revisio.id] = sum

        return sum_revisio

    def _get_revisions_ids(self, cursor, uid, ids, context):
        #Agafar les ids de les revisions per cada defecte
        list_rev = []
        for defecte in self.read(cursor, uid, ids, ['revisio_id']):
            list_rev.append(defecte['revisio_id'][0])

        return list_rev

    def _get_defectes_ids(self, cursor, uid, ids, context):
        # agafar les revisions a partir dels defectes que tenen el tipus
        obj_defectes = self.pool.get('giscedata.revisions.ct.defectes')

        list_tipus = []
        list_rev = []

        for tipus in self.read(cursor, uid, ids, ['id']):
            list_tipus.append(tipus['id'])

        for tipus_id in list_tipus:
            defectes_id = obj_defectes.search(cursor, uid,
                                              [('valoracio', '=', tipus_id)])

            for defecte in obj_defectes.read(cursor, uid, defectes_id,
                                             ['revisio_id']):
                list_rev.append(defecte['revisio_id'][0])

        return list_rev

    _columns = {
        'calc_ir1_ct': fields.function(
            _ff_calc_ir1_ct, method=True,
            store={'giscedata.revisions.ct.defectes': (
                _get_revisions_ids, ['valoracio'], 10
            ), 'giscedata.revisions.ct.tipusvaloracio': (
                _get_defectes_ids, ['pes'], 10)},
            type='float', string='IR1 CTS'),
    }

GiscedataRevisionsCtRevisio()


class GiscedataRevisionsAtRevisio(osv.osv):
    _name = 'giscedata.revisions.at.revisio'
    _inherit = 'giscedata.revisions.at.revisio'

    def _ff_calc_ir1_at(self, cr, uid, ids, field_name, arg, context):
        obj_revisio = self.pool.get('giscedata.revisions.at.revisio')
        obj_linia = self.pool.get('giscedata.at.linia')

        #Sum revisio
        sum_revisio = {}

        for revisio in obj_revisio.browse(cr, uid, ids):
            #Sumartori
            sum = 0
            #longitud de la lina
            #Agafem la longitud de l'autocad i sumem la aeria
            if revisio.name.id:
                longi = obj_linia.read(cr, uid, revisio.name.id,
                                       ['longitud_aeria_cad',
                                        'longitud_sub_cad'])
            try:
                long_linia = longi['longitud_aeria_cad'] + longi[
                    'longitud_sub_cad']
            except:
                print "Error, no s'ha trobat la longitud aeria"
                long_linia = 0

            if revisio.defectes_ids:
                for defecte in revisio.defectes_ids:
                    sum += defecte['valoracio']['pes']

            if long_linia:
                long_kms = long_linia / 1000
                sum_revisio[revisio.id] = sum / long_kms
            else:
                print "Error, no s'ha trobat la longitud"

        return sum_revisio

    def _get_revisions_ids(self, cursor, uid, ids, context):
        #Agafar les ids de les revisions per cada defecte
        list_rev = []
        for defecte in self.read(cursor, uid, ids, ['revisio_id']):
            list_rev.append(defecte['revisio_id'][0])

        return list_rev

    def _get_defectes_ids(self, cursor, uid, ids, context):
        # agafar les revisions a partir dels defectes que tenen el tipus
        obj_defectes = self.pool.get('giscedata.revisions.at.defectes')

        list_tipus = []
        list_rev = []

        for tipus in self.read(cursor, uid, ids, ['id']):
            list_tipus.append(tipus['id'])

        for tipus_id in list_tipus:
            defectes_id = obj_defectes.search(cursor, uid,
                                              [('valoracio', '=', tipus_id)])

            for defecte in obj_defectes.read(cursor, uid, defectes_id,
                                             ['revisio_id']):
                list_rev.append(defecte['revisio_id'][0])

        return list_rev

    _columns = {
        'calc_ir1_at': fields.function(
            _ff_calc_ir1_at, method=True,
            store={'giscedata.revisions.at.defectes': (
                _get_revisions_ids, ['valoracio'], 10
            ), 'giscedata.revisions.at.tipusvaloracio': (
                _get_defectes_ids, ['pes'], 10)},
            type='float', string='IR1 LAT'),
    }

GiscedataRevisionsAtRevisio()


class GiscedataRevisionsBtRevisio(osv.osv):
    _name = 'giscedata.revisions.bt.revisio'
    _inherit = 'giscedata.revisions.bt.revisio'

    def _ff_calc_ir1_bt(self, cr, uid, ids, field_name, arg, context):
        obj_revisio = self.pool.get('giscedata.revisions.bt.revisio')
        obj_cts = self.pool.get('giscedata.cts')

        #Sum revisio
        sum_revisio = {}

        for revisio in obj_revisio.browse(cr, uid, ids):
            #Sumartori
            sum = 0
            #longitud de la lina
            linia = obj_cts.read(cr, uid, revisio.name.id, ['longitud_bt'])
            long_linia = linia['longitud_bt']

            if revisio.defectes_ids:
                for defecte in revisio.defectes_ids:
                    sum += defecte['valoracio']['pes']

            if long_linia:
                long_kms = long_linia / 1000
                sum_revisio[revisio.id] = sum / long_kms
            else:
                print "Error, no s'ha trobat la longitud"

        return sum_revisio

    def _get_revisions_ids(self, cursor, uid, ids, context):
        #Agafar les ids de les revisions per cada defecte
        list_rev = []
        for defecte in self.read(cursor, uid, ids, ['revisio_id']):
            list_rev.append(defecte['revisio_id'][0])

        return list_rev

    def _get_defectes_ids(self, cursor, uid, ids, context):
        # agafar les revisions a partir dels defectes que tenen el tipus
        obj_defectes = self.pool.get('giscedata.revisions.bt.defectes')

        list_tipus = []
        list_rev = []

        for tipus in self.read(cursor, uid, ids, ['id']):
            list_tipus.append(tipus['id'])

        for tipus_id in list_tipus:
            defectes_id = obj_defectes.search(cursor, uid,
                                              [('valoracio', '=', tipus_id)])

            for defecte in obj_defectes.read(cursor, uid, defectes_id,
                                             ['revisio_id']):
                list_rev.append(defecte['revisio_id'][0])

        return list_rev

    _columns = {
        'calc_ir1_bt': fields.function(
            _ff_calc_ir1_bt, method=True,
            store={'giscedata.revisions.bt.defectes': (
                _get_revisions_ids, ['valoracio'], 10
            ), 'giscedata.revisions.bt.tipusvaloracio': (
                _get_defectes_ids, ['pes'], 10)},
            type='float', string='IR1 BT'),
    }

GiscedataRevisionsBtRevisio()


class GiscedataRevisionsIR2Resultats(osv.osv):
    _name = 'giscedata.revisions.ir2.resultats'

    _columns = {
        'name': fields.char('Any', size=7),
        'data_inici': fields.char('Data inici', size=15),
        'data_final': fields.char('Data final', size=15),
        'ir2_ct': fields.float('IR2 CT'),
        'ir2_bt': fields.float('IR2 BT'),
        'ir2_at': fields.float('IR2 AT'),
        'ir2_global': fields.float('IR2 Global'),
        'create_date': fields.datetime('Data del càlcul')
    }

    _order = 'name desc'

    _defaults = {
        'ir2_ct': lambda *a: 0.0,
        'ir2_bt': lambda *a: 0.0,
        'ir2_at': lambda *a: 0.0,
        'ir2_global': lambda *a: 0.0,
    }

GiscedataRevisionsIR2Resultats()


class GiscedataRevisionsIM1(osv.osv):
    _name = 'giscedata.revisions.im1'

    _columns = {
        'name': fields.char('Any', size=7),
        'create_date': fields.datetime('Data del càlcul'),
        'ct_im1_ids': fields.one2many('giscedata.revisions.im1.ct.resultats',
                                      'year', 'CT IM1'),
        'bt_im1_ids': fields.one2many('giscedata.revisions.im1.bt.resultats',
                                      'year', 'BT IM1'),
        'at_im1_ids': fields.one2many('giscedata.revisions.im1.at.resultats',
                                      'year', 'AT IM1')
    }

    _order = 'name desc'

GiscedataRevisionsIM1()


class GiscedataRevisionsIM1CTResultats(osv.osv):
    _name = 'giscedata.revisions.im1.ct.resultats'

    _columns = {
        'name': fields.char('CT', size=7),
        'year': fields.many2one('giscedata.revisions.im1', 'Any'),
        'im1': fields.float('IM1'),
    }

    _order = 'name desc'

GiscedataRevisionsIM1CTResultats()


class GiscedataRevisionsIM1BTResultats(osv.osv):
    _name = 'giscedata.revisions.im1.bt.resultats'

    _columns = {
        'name': fields.char('BT', size=7),
        'year': fields.many2one('giscedata.revisions.im1', 'Any'),
        'im1': fields.float('IM1'),
    }

    _order = 'name desc'

GiscedataRevisionsIM1BTResultats()


class GiscedataRevisionsIM1ATResultats(osv.osv):
    _name = 'giscedata.revisions.im1.at.resultats'

    _columns = {
        'name': fields.char('Linia', size=7),
        'year': fields.many2one('giscedata.revisions.im1', 'Any'),
        'im1': fields.float('IM1'),
    }

    _order = 'name desc'

GiscedataRevisionsIM1ATResultats()
