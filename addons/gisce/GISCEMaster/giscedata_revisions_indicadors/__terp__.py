# -*- coding: utf-8 -*-
{
    "name": "Revisions Indicadors",
    "description": """
    This module provide :
        * Indicadors dels resultats de les revisions per calcular la qualitat.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_revisions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_revisions_view.xml",
        "wizard/wizard_revisions_ir2_view.xml",
        "wizard/wizard_revisions_ir3_view.xml",
        "wizard/wizard_revisions_im1_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
