# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
import calendar
from tools.translate import _

class WizardRevisionsIm1(osv.osv_memory):
    _name = 'wizard.revisions.im1'

    def mostraravis(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        year = wizard.year
        ultim_dia_any = calendar.monthrange(year, 12)[1]
        dfinal = datetime(year=year, month=12,
                          day=ultim_dia_any).strftime('%d-%m-%Y')
        dinici = datetime(year=year, month=1,
                          day=1).strftime('%d-%m-%Y')

        wizard.write({'state': 'avis',
                      'avis': _("IM1 es calcularà entre el %s i el %s") %
                              (dinici, dfinal)})


    def calcul_im1(self, cursor, uid, ids, context=None, tipus=None):
        wizard = self.browse(cursor, uid, ids[0])
        obj_im1 = self.pool.get("giscedata.revisions.im1")

        #tipus i any en que volem calcular im1
        tipus = wizard.tipus
        year = wizard.year

        #establim la data inici i final
        ultim_dia_any = calendar.monthrange(year, 12)[1]
        dfinal = datetime(year=year, month=12,
                          day=ultim_dia_any).strftime('%d-%m-%Y')
        dinici = datetime(year=year, month=1,
                          day=1).strftime('%d-%m-%Y')

        #creo l'objecte de l'any
        any_id = obj_im1.create(cursor, uid, {'name': str(year)})

        obj_rev_at = self.pool.get("giscedata.revisions.at.revisio")
        im1_at_obj = self.pool.get("giscedata.revisions.im1.at.resultats")
        defecte_obj_at = self.pool.get("giscedata.revisions.at.defectes")

        #buscar les ids de l'any corresponent
        ids_rev_at = obj_rev_at.search(cursor, uid,
                                       [('data', '<=', dfinal),
                                        ('data', '>=', dinici)])

        for revisio in obj_rev_at.read(cursor, uid, ids_rev_at,
                                       ['defectes_ids']):
            #Numero de defectes totals
            num_def_to = 0
            #Numero de defectes corregits al plaç
            num_def_pl = 0
            #Inicialitzo a 0 per cada revisio el calcul
            im1 = 0

            if revisio['defectes_ids']:
                #Per tots els defectes de la revisio
                defectes = defecte_obj_at.read(cursor, uid, revisio[
                    'defectes_ids'], ['data_limit_reparacio', 'reparat',
                                      'data_reparacio'])
                for defecte in defectes:
                    if defecte['reparat']:
                        if defecte['data_limit_reparacio'] >= \
                                defecte['data_reparacio'] and \
                                defecte['data_reparacio']:
                            num_def_pl += 1
                        num_def_to += 1
                    #En el cas que hi hagin revisions reparades
                if num_def_to:
                    #Càlcul del IM1
                    im1 = (num_def_pl * 100) / num_def_to
            #creo l'objecte de la taula de at
            im1_at_obj.create(cursor, uid, {'name': revisio['id'],
                                            'year': any_id,
                                            'im1': im1
                                            })

        obj_rev_ct = self.pool.get("giscedata.revisions.ct.revisio")
        im1_ct_obj = self.pool.get("giscedata.revisions.im1.ct.resultats")
        defecte_obj_ct = self.pool.get("giscedata.revisions.ct.defectes")

        #buscar les ids de l'any corresponent
        ids_rev_ct = obj_rev_ct.search(cursor, uid,
                                       [('data', '<=', dfinal),
                                        ('data', '>=', dinici)])

        for revisio in obj_rev_ct.read(cursor, uid, ids_rev_ct,
                                       ['defectes_ids']):
            #Numero de defectes totals
            num_def_to = 0
            #Numero de defectes corregits al plaç
            num_def_pl = 0
            #Inicialitzo a 0 per cada revisio el calcul
            im1 = 0

            if revisio['defectes_ids']:
                #Per tots els defectes de la revisio
                defectes = defecte_obj_ct.read(cursor, uid, revisio[
                    'defectes_ids'], ['data_limit_reparacio', 'reparat',
                                      'data_reparacio'])
                for defecte in defectes:
                    if defecte['reparat']:
                        if defecte['data_limit_reparacio'] >= \
                                defecte['data_reparacio'] and \
                                defecte['data_reparacio']:
                            num_def_pl += 1
                        num_def_to += 1
                    #En el cas que hi hagin revisions reparades
                if num_def_to:
                    #Càlcul del IM1
                    im1 = (num_def_pl * 100) / num_def_to

            im1_ct_obj.create(cursor, uid, {'name': revisio['id'],
                                            'year': any_id,
                                            'im1': im1
                                            })

        obj_rev_bt = self.pool.get("giscedata.revisions.bt.revisio")
        im1_bt_obj = self.pool.get("giscedata.revisions.im1.bt.resultats")
        defecte_obj_bt = self.pool.get("giscedata.revisions.bt.defectes")

        #buscar les ids de l'any corresponent
        ids_rev_bt = obj_rev_bt.search(cursor, uid,
                                       [('data', '<=', dfinal),
                                        ('data', '>=', dinici)])

        for revisio in obj_rev_bt.read(cursor, uid, ids_rev_bt,
                                       ['defectes_ids']):
            #Numero de defectes totals
            num_def_to = 0
            #Numero de defectes corregits al plaç
            num_def_pl = 0
            #Inicialitzo a 0 per cada revisio el calcul
            im1 = 0

            if revisio['defectes_ids']:
                #Per tots els defectes de la revisio
                defectes = defecte_obj_bt.read(cursor, uid, revisio[
                    'defectes_ids'], ['data_limit_reparacio', 'reparat',
                                      'data_reparacio'])
                for defecte in defectes:
                    if defecte['reparat']:
                        if defecte['data_limit_reparacio'] >= \
                                defecte['data_reparacio'] and \
                                defecte['data_reparacio']:
                            num_def_pl += 1
                        num_def_to += 1
                    #En el cas que hi hagin revisions reparades
                if num_def_to:
                    #Càlcul del IM1
                    im1 = (num_def_pl * 100) / num_def_to

            im1_bt_obj.create(cursor, uid, {'name': revisio['id'],
                                            'year': any_id,
                                            'im1': im1
                                            })

        return {
            'name': "IM1 de l'any %s" % year,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.revisions.im1',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', '=', %d)]" % any_id or False,
        }

    _columns = {
        'year': fields.integer('Any'),
        'data_inici': fields.char('Data inici', size=15),
        'data_final': fields.char('Data final', size=15),
        'avis': fields.text(),
        'resultat': fields.text(),
        'tipus': fields.selection([('at', 'AT'),
                                   ('ct', 'CT'),
                                   ('bt', 'BT'),
                                   ('global', 'Global')], 'Tipus'),
        'state': fields.selection([('init', 'Init'),
                                   ('avis', 'Avís'),
                                   ('end', 'End')], 'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardRevisionsIm1()
