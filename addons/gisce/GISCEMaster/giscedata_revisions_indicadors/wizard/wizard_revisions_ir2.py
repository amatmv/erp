# -*- encoding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
import calendar

class WizardRevisionsIr2(osv.osv_memory):
    _name = 'wizard.revisions.ir2'

    def calcul_ir2(self, cursor, uid, ids, context, tipus=None):
        wizard = self.browse(cursor, uid, ids[0])
        ir2_obj = self.pool.get('giscedata.revisions.ir2.resultats')

        # Any per a calcular l'ir2
        year = wizard.year

        ultim_dia_any = calendar.monthrange(year, 12)[1]
        dfinal = datetime(year=year, month=12,
                          day=ultim_dia_any).strftime('%d-%m-%Y')
        dinici = datetime(year=year, month=1,
                          day=1).strftime('%d-%m-%Y')

        obj_rev_at = self.pool.get("giscedata.revisions.at.revisio")
        obj_rev_ct = self.pool.get("giscedata.revisions.ct.revisio")
        obj_rev_bt = self.pool.get("giscedata.revisions.bt.revisio")

        #Cerco per les dates
        ids_rev_at = obj_rev_at.search(cursor, uid, [('data', '<=', dfinal),
                                                     ('data', '>=', dinici)])
        ids_rev_ct = obj_rev_ct.search(cursor, uid, [('data', '<=', dfinal),
                                                     ('data', '>=', dinici)])
        ids_rev_bt = obj_rev_bt.search(cursor, uid, [('data', '<=', dfinal),
                                                     ('data', '>=', dinici)])

        # llista amb totes les ids de revisions
        ids_rev = ids_rev_at + ids_rev_ct + ids_rev_bt

        # número de revisions
        num_ids_at = len(ids_rev_at)
        num_ids_ct = len(ids_rev_ct)
        num_ids_bt = len(ids_rev_bt)
        num_ids_total = len(ids_rev)

        # sumatoris de cada tipus
        sumatori_at = 0
        sumatori_ct = 0
        sumatori_bt = 0

        for revisio in obj_rev_at.browse(cursor, uid, ids_rev_at):
            #Afegeixo la valoracio total de cada revisio
            sumatori_at += revisio.calc_ir1_at

        for revisio in obj_rev_ct.browse(cursor, uid, ids_rev_ct):
            #Afegeixo la valoracio total de cada revisio
            sumatori_ct += revisio.calc_ir1_ct

        for revisio in obj_rev_bt.browse(cursor, uid, ids_rev_bt):
            #Afegeixo la valoracio total de cada revisio
            sumatori_bt += revisio.calc_ir1_bt

        # sumatori de les valoracions de ir1
        sumatori_total = sumatori_at + sumatori_ct + sumatori_bt

        # càlcul de ir2 per cada tipus
        try:
            resul_ct = sumatori_ct / num_ids_ct
            resul_bt = sumatori_bt / num_ids_bt
            resul_at = sumatori_at / num_ids_at
            resul_total = sumatori_total / num_ids_total
        except ZeroDivisionError:
            resul_ct = 0
            resul_bt = 0
            resul_at = 0
            resul_total = 0
            wizard.write({'state': 'end',
                          'resultat': "No existeixen revisions per aquest any"})

        # Escriure els resultats al model de IR2
        any_id = ir2_obj.create(cursor, uid, {'name': str(year),
                                              'ir2_ct': resul_ct,
                                              'ir2_bt': resul_bt,
                                              'ir2_at': resul_at,
                                              'ir2_global': resul_total,
                                              'data_inici': dinici,
                                              'data_final': dfinal})

        # tenco el wizard per mostrar la taula
        wizard.write({'state': 'end',
                      'resultat': "Tanca aquesta finestra per veure els "
                                  "resultats"})
        return {
            'name': "IR2 de l'any %s" % year,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.revisions.ir2.resultats',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', '=', %d)]" % any_id or False,
        }

    _columns = {
        'year': fields.integer('Any'),
        'data_inici': fields.char('Data inici', size=19),
        'data_final': fields.char('Data final', size=19),
        'resultat': fields.text(),
        'state': fields.selection([('init', 'Init'),
                                   ('error_data', 'Error Data'),
                                   ('error_tipus', 'Error Tipus'),
                                   ('end', 'End')], 'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardRevisionsIr2()
