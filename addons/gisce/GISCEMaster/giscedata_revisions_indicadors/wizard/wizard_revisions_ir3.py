# -*- encoding: utf-8 -*-

from osv import osv, fields


class WizardRevisionsIr3(osv.osv_memory):
    _name = 'wizard.revisions.ir3'

    def calcul_ir3(self, cr, uid, ids, context):
        wizard = self.browse(cr, uid, ids[0])

        #Tipus que l'usuari entra
        type = wizard.tipus
        if type == False:
            wizard.write({'state': 'error_tipus'})
            return False

        #Dates inici i final que l'usuari entra
        dinici = wizard.data_inici
        dfinal = wizard.data_final
        if dinici == False or dfinal == False:
            wizard.write({'state': 'error_data'})
            return False

        #Resultat final
        resul_total = 0

        #Càlcul de la PG= 50W+25X+5Y+1Z
        if type == "at":
            obj_rev_at = self.pool.get("giscedata.revisions.at.revisio")
            obj_rev_at_def = self.pool.get("giscedata.revisions.at.defectes")
            #Cerco per les dates
            ids_rev = obj_rev_at.search(cr, uid, [('data', '<=', dfinal),
                                                  ('data', '>=', dinici)])

            sumatori = 0

            for revisio in obj_rev_at.browse(cr, uid, ids_rev):
                print "REVISIO :{}".format(revisio.id)
                for defecte in revisio.defectes_ids:
                    print "NAMEVAL: {}".format(defecte.valoracio[1])



        elif type == "ct":
            obj_rev_ct = self.pool.get("giscedata.revisions.ct.revisio")
            #Cerco per les dates
            ids_rev = obj_rev_ct.search(cr, uid, [('data', '<=', dfinal),
                                                  ('data', '>=', dinici)])

            #Per fer la mitjana aritmetica
            num_ids = len(ids_rev)
            sumatori = 0

            for revisio in obj_rev_ct.browse(cr, uid, ids_rev):
                #Afegeixo la valoracio total de cada revisio
                sumatori += revisio.calc_ir1_ct

            #Faig la mitjana
            resul_total = sumatori / num_ids

        elif type == "bt":
            obj_rev_bt = self.pool.get("giscedata.revisions.bt.revisio")
            #Cerco per les dates
            ids_rev = obj_rev_bt.search(cr, uid, [('data', '<=', dfinal),
                                                  ('data', '>=', dinici)])

            #Per fer la mitjana aritmetica
            num_ids = len(ids_rev)
            sumatori = 0

            for revisio in obj_rev_bt.browse(cr, uid, ids_rev):
                #Afegeixo la valoracio total de cada revisio
                sumatori += revisio.calc_ir1_bt

            #Faig la mitjana
            resul_total = sumatori / num_ids

        else:
            print "TODO"

        wizard.write({'resultat_ir3': resul_total, 'state': 'end'})

    _columns = {
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'resultat_ir3': fields.float('Suma Resultat', digits=(4, 4)),
        'tipus': fields.selection([('at', 'AT'),
                                   ('ct', 'CT'),
                                   ('bt', 'BT')], 'Tipus'),
        'state': fields.selection([('init', 'Init'),
                                   ('end', 'End')], 'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardRevisionsIr3()
