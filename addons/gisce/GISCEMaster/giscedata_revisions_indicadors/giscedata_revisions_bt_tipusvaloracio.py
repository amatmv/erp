# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataRevisionsBtTipusvaloracio(osv.osv):
    _name = 'giscedata.revisions.bt.tipusvaloracio'
    _description = 'GISCE Afegit camp pes a valoracio'
    _inherit = 'giscedata.revisions.bt.tipusvaloracio'

    _columns = {
      'pes': fields.integer('Pes'),
    }

GiscedataRevisionsBtTipusvaloracio()