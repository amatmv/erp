# -*- coding: utf-8 -*-
{
    "name": "product_category_tree",
    "description": """Mostra la categoria al llistat de productes""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMster",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_view.xml"
    ],
    "active": False,
    "installable": True
}
