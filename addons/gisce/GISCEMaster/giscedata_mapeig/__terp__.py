# -*- coding: utf-8 -*-
{
    "name": "Informes Mapeig Gencat",
    "description": """
    This module provide :
      * Mòdul de mapeig.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica_gencat",
        "giscedata_cts",
        "giscedata_trieni",
        "giscedata_revisions",
        "giscedata_cts_subestacions",
        "giscedata_at",
        "giscedata_circuits"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_mapeig_view.xml",
        "giscedata_mapeig_data.xml",
        "wizard/giscedata_mapeig_view.xml",
        "ir.model.access.csv",
        "security/giscedata_mapeig_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
