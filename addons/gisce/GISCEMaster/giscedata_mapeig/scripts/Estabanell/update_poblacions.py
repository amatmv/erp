#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Actualitza les poblacions segons el llistat
# cas [86326]

####################################################
# ATENCIÓ:                                         #
# Aquests scripts s'han fet anar en v4.            #
# Cal verificar-ne el correcte funcionament en v5. #
entenc = False
####################################################
if not entenc:
    print """
        ####################################################
        # ATENCIÓ:                                         #
        # Aquests scripts s'han fet anar en v4.            #
        # Cal verificar-ne el correcte funcionament en v5. #
        ####################################################
    """
    exit() 

import csv
import sys

from ooop import OOOP

###############
# Configuració
###############
nom_mun = 0
_1st_pob = 1

uri, dbname, port, user, pwd, mute, dfile = sys.argv[1:]
mute = eval(mute)
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)
reader = csv.reader(open(dfile), delimiter=';')

for row in reader:
    try:
        print 'processant el municipi %s' % row[nom_mun]
        if not row[_1st_pob] or not row[nom_mun]:
            continue
        ref_pos = nom_mun
        # Si existeix la població segons la primera columna (nom_mun) ens
        # la quedem sinò anem buscant fins a trobar la primera problació 
        id_ref = O.ResPoblacio.search([('name', '=', row[nom_mun])])
        if not id_ref:
            for i in range(_1st_pob, len(row)):
                id_ref = O.ResPoblacio.search([('name', '=', row[i])])
                if id_ref:
                    ref_pos = i
                    break
        if not id_ref:
            sys.stderr.write(" -> [!] no s'ha pogut identificar la"
                             " poblacio de referència pel municipi %s\n" \
                             % (row[nom_mun]))
            continue

        # Localitzar LATs i CTs amb la resta de poblacions i vincular-hi
        # la primera. Eliminar les poblacions en acabar
        for i in range(_1st_pob + ref_pos, len(row)):
            if not row[i]:
                continue
            _parse_pob = O.ResPoblacio.search([('name', '=', row[i])])
            if not _parse_pob:
                sys.stderr.write(" -> [!] no s'ha pogut identificar la"
                                 " poblacio %s\n" % (row[i]))
                continue
            ids_lat = O.GiscedataAtLinia.search(
                                        [('poblacio', '=', _parse_pob[0])])
            O.GiscedataAtLinia.write(ids_lat, {'poblacio': id_ref[0]})
            ids_ct = O.GiscedataCts.search(
                                        [('id_poblacio', '=', _parse_pob[0])])
            O.GiscedataCts.write(ids_ct, {'id_poblacio': id_ref[0]})
            O.ResPoblacio.unlink(_parse_pob)
            sys.stdout.write(" -> s'ha actualitzat correctament la"
                             " poblacio %s\n" % row[i])

        # Actualitzar el nom de la primera de les poblacions segons el municipi
        if ref_pos > nom_mun:
            O.ResPoblacio.write(id_ref, {'name': row[nom_mun]})
    except Exception, e:
        sys.stderr.write(" -> error processant la línia %d\n" % i)
