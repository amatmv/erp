#!/usr/bin/env python
# -*- coding: utf-8 -*-

####################################################
# ATENCIÓ:                                         #
# Aquests scripts s'han fet anar en v4.            #
# Cal verificar-ne el correcte funcionament en v5. #
entenc = False
####################################################
if not entenc:
    print """
        ####################################################
        # ATENCIÓ:                                         #
        # Aquests scripts s'han fet anar en v4.            #
        # Cal verificar-ne el correcte funcionament en v5. #
        ####################################################
    """
    exit() 

# Vincular circuits als trams
# python update_circuits_en_trams.py http://localhost estabanell3 8069 admin ************ CARACTERISTIQUES_TRAMS_AT_AMB_CIRCUITS_MODif.csv 2> /tmp/err

from datetime import datetime
import csv
import sys

from ooop import OOOP

################
# Camps fitxer
################
linia = 0
tram = 1
circuit = 2
################

t0 = datetime.now()

uri, dbname, port, user, pwd, dfile = sys.argv[1:]
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)
reader = csv.reader(open(dfile), delimiter=';')
for lin, row in enumerate(reader):
    try:
        print 'processant la línia %d' % lin
        # identificar el tram
        ordre = row[tram][row[tram].rfind('_')+1:]
        search_params = [('linia.name', '=', row[linia]),
                         ('ordre', '=', ordre)]
        tram_id = O.GiscedataAtTram.search(search_params)
        if not tram_id or len(tram_id) > 1:
            sys.stderr.write(" -> Línia %d. No s'ha pogut identificar el "
                             " tram %s\n" % (lin + 1, search_params))
            continue
        # identificar el circuit
        search_params = [('name', '=', row[circuit])]
        pos_id = O.GiscedataCtsSubestacionsPosicio.search(search_params)
        if not pos_id or len(pos_id) > 1:    
            sys.stderr.write(" -> Línia %d. No s'ha pogut identificar la "
                             " posició %s\n" % (lin + 1, search_params))
            continue
        # actualitzar el circuit
        vals = {'circuit': pos_id[0]}
        try:
            O.GiscedataAtTram.write(tram_id, vals)
        except:
            sys.stderr.write(" -> Línia %d. No s'ha pogut actualitzar el tram "
                             "id %s\n" % (lin + 1, str(tram_id[0])))
            continue
    except:
        sys.stderr.write(" -> error processant la línia %d\n" % lin)

print u"Temps d'execució: %s" % (datetime.now() - t0)
