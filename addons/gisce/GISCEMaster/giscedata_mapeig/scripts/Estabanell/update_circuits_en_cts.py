#!/usr/bin/env python
# -*- coding: utf-8 -*-

####################################################
# ATENCIÓ:                                         #
# Aquests scripts s'han fet anar en v4.            #
# Cal verificar-ne el correcte funcionament en v5. #
entenc = False
####################################################
if not entenc:
    print """
        ####################################################
        # ATENCIÓ:                                         #
        # Aquests scripts s'han fet anar en v4.            #
        # Cal verificar-ne el correcte funcionament en v5. #
        ####################################################
    """
    exit() 

# Vincular circuits als cts
# python update_circuits_en_trams.py http://localhost estabanell3 8069 admin ************ CT_AMB_CIRCUITS.csv 2> /tmp/err

from datetime import datetime
import csv
import sys

from ooop import OOOP

################
# Camps fitxer
################
name = 0
descripcio = 1
circuit = 2
er = 3
################

t0 = datetime.now()

uri, dbname, port, user, pwd, dfile = sys.argv[1:]
O = OOOP(dbname=dbname, user=user, pwd=pwd, port=int(port), uri=uri)
reader = csv.reader(open(dfile), delimiter=';')
for lin, row in enumerate(reader):
    try:
        print 'processant la línia %d' % lin
        # identificar el ct
        search_params = [('name', '=', row[name])]
        ct_id = O.GiscedataCts.search(search_params)
        if not ct_id or len(ct_id) > 1:
            sys.stderr.write(" -> Línia %d. No s'ha pogut identificar el "
                             " ct %s\n" % (lin + 1, search_params))
            continue
        # identificar el circuit
        search_params = [('name', '=', row[circuit])]
        pos_id = O.GiscedataCtsSubestacionsPosicio.search(search_params)
        if not pos_id or len(pos_id) > 1:    
            sys.stderr.write(" -> Línia %d. No s'ha pogut identificar la "
                             " posició %s\n" % (lin + 1, search_params))
            continue
        # actualitzar el circuit
        vals = {'circuit': pos_id[0]}
        try:
            O.GiscedataCts.write(ct_id, vals)
        except:
            sys.stderr.write(" -> Línia %d. No s'ha pogut actualitzar el tram "
                             "id %s\n" % (lin + 1, str(ct_id[0])))
            continue
    except:
        sys.stderr.write(" -> error processant la línia %d\n" % lin)

print u"Temps d'execució: %s" % (datetime.now() - t0)
