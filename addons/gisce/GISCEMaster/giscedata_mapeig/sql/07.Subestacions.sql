SELECT se.id,
    ct.name,
    ct.descripcio,
    CASE WHEN ct.propietari IS True THEN 'E' ELSE 'C' END AS propietat,
    trim(to_char(v.x, '9999999D999999'), ' ') AS x,
    trim(to_char(v.y, '9999999D999999'), ' ') AS y,
    '31' AS Fus,
    ct.adreca,
    ct.id_municipi AS poblacio
  FROM giscedata_cts ct
    INNER JOIN giscegis_blocs_ctat g ON g.ct = ct.id
    INNER JOIN giscegis_vertex v ON v.id = g.vertex
    INNER JOIN  giscedata_cts_subestacions se ON ct.id = se.ct_id
  WHERE ct.active IS True
    AND ct.propietari
    AND ct.id_provincia in (select id from res_country_state
      WHERE comunitat_autonoma=
        (SELECT id
         FROM res_comunitat_autonoma
          WHERE codi='09'))
    ORDER BY se.id
