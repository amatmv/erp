SELECT at.id::text AS id_unidad_revision,
    at.name AS tramo_traza,
    at.descripcio AS descripcio,
    to_char(max(tri.data_final), 'DD/MM/YYYY') as fecha_prevista_revision,
    at.provincia AS id_provincia
FROM giscedata_revisions_at_revisio rev
    INNER JOIN giscedata_at_linia at ON at.id = rev.name
    INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
    INNER JOIN giscedata_at_tram t ON t.linia = at.id
    INNER JOIN giscedata_at_cables c ON t.cable = c.id
    INNER JOIN giscedata_at_tipuscable tc ON c.tipus = tc.id
WHERE at.tensio >= 40000
    AND rev.trimestre IN %(trimestres)s
    AND at.propietari
    AND provincia IN (select id from res_country_state
        WHERE comunitat_autonoma=
        (SELECT id
            FROM res_comunitat_autonoma
            WHERE codi='09'))
    AND at.name != '1'
    AND tc.codi != 'E'
GROUP BY at.id
ORDER BY at.id;