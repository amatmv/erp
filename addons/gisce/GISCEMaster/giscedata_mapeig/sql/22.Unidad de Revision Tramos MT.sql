SELECT  at.id::text AS id_unidad_revision,
		at.name AS tramo_traza,
		at.descripcio AS descripcio,
	rev.fecha_prevista_revision,
  at.provincia AS id_provincia
FROM giscedata_at_linia at
    INNER JOIN
        (SELECT
            rev.name,to_char(max(tri.data_final), 'DD/MM/YYYY') AS fecha_prevista_revision
            FROM giscedata_revisions_at_revisio rev
            INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
	          GROUP BY rev.name) AS rev ON  at.id=rev.name
	  INNER JOIN giscedata_at_tram tr ON tr.linia=at.id
WHERE at.tensio < 40000
    AND at.active IS TRUE
    AND at.baixa IS FALSE
    AND at.propietari
    AND at.provincia IN
        (SELECT id FROM res_country_state
        WHERE comunitat_autonoma=
          (SELECT id
	        FROM res_comunitat_autonoma
	        WHERE codi='09'))
GROUP BY at.id, rev.fecha_prevista_revision
ORDER BY at.id;