-- CT
SELECT '1'||def.revisio_id::text AS id_inspeccion,
    '' AS apoyo,
    '1'||trim(to_char(def.descripcio_id, '9999'), ' ') AS id_defecto_tipo,
    CASE
        WHEN trim(substring(val.name FROM '.*,'),',') = 'T' THEN 'CI'
        WHEN val.name = 'MENOR' THEN 'CT'
        WHEN val.name = 'A corregir a termini' THEN 'CT'
        ELSE 'CT'
    END AS criticidad,
    '' AS consolidado,
    to_char(def.data_reparacio, 'DD/MM/YYYY') AS fecha_correccion
  FROM giscedata_revisions_ct_defectes def
    INNER JOIN giscedata_revisions_ct_revisio rev ON rev.id = def.revisio_id
    INNER JOIN giscedata_revisions_ct_tipusvaloracio val ON def.valoracio = val.id
    INNER JOIN giscedata_cts ct ON ct.id= rev.name
    INNER JOIN giscedata_revisions_ct_tipusdefectes tdef ON tdef.id = def.descripcio_id
  WHERE rev.trimestre IN %(trimestres)s
    AND val.active = True
    AND def.estat = 'B'
    AND tdef.intern IS NOT True
    AND ct.propietari IS True
    AND ct.active IS True
    AND ct.id_provincia IN (
      SELECT id FROM res_country_state
      WHERE comunitat_autonoma = (
        SELECT id
        FROM res_comunitat_autonoma
        WHERE codi='09'
      )
    )
UNION
-- LAT
SELECT '2'||def.revisio_id::text AS id_inspeccion,
    suport.name AS apoyo,
    '2'||trim(to_char(def.defecte_id, '9999'), ' ') AS id_defecto_tipo,
    CASE
        WHEN trim(substring(val.name FROM '.*,'),',') = 'T' THEN 'CI'
        WHEN val.name = 'MENOR' THEN 'CT'
        WHEN val.name = 'A corregir a termini' THEN 'CT'
        ELSE 'CT'
    END AS criticidad,
    '' AS consolidado,
    to_char(def.data_reparacio, 'DD/MM/YYYY') AS fecha_correccion
  FROM giscedata_revisions_at_defectes def
  INNER JOIN giscedata_revisions_at_revisio rev ON rev.id = def.revisio_id
  INNER JOIN giscedata_revisions_at_tipusvaloracio val ON def.valoracio = val.id
  INNER JOIN giscedata_at_suport suport ON suport.id = def.suport
  INNER JOIN giscedata_at_linia linia ON suport.linia=linia.id
  INNER JOIN giscedata_revisions_at_tipusdefectes tdef ON tdef.id = def.defecte_id
  WHERE rev.trimestre IN %(trimestres)s
    AND val.active = True
    AND tdef.intern IS NOT True
    AND linia.propietari IS True
    AND linia.active IS True
    AND linia.provincia IN (
      SELECT id FROM res_country_state
	    WHERE comunitat_autonoma = (
        SELECT id
        FROM res_comunitat_autonoma
        WHERE codi='09'
      )
    )
ORDER BY id_inspeccion;