SELECT t.id as ID_CMT,
       'TR'||t.ordre_dins_ct AS CMT_NOMBRE,
       se.id AS ID_EST,
       parcs1.id as ID_PARC1,
       '' as ID_PARC2,
       '' as ID_PARC3,
       t.potencia_nominal/1000 as POTENCIA
    FROM giscedata_transformador_trafo t
    INNER JOIN giscedata_cts c ON c.id = t.ct
    INNER JOIN giscedata_cts_subestacions se ON se.ct_id = c.id
    INNER JOIN giscedata_transformador_estat e ON t.id_estat = e.id
    INNER JOIN giscedata_transformador_connexio con ON con.trafo_id = t.id
    INNER JOIN (
        -- De 09.Parcs.sql
        SELECT parc.id,
          ct.id as ct_id,
          tensio.tensio AS v_name
        FROM giscedata_parcs AS parc
          INNER JOIN giscedata_tensions_tensio AS tensio ON tensio.id = parc.tensio_id
          INNER JOIN giscedata_cts_subestacions AS se ON se.id = parc.subestacio_id
          INNER JOIN giscedata_cts AS ct ON ct.id = se.ct_id
        WHERE tensio.tensio IS NOT Null
        GROUP BY parc.id, ct.id, v_name
        ORDER BY v_name
    ) AS parcs1 ON parcs1.ct_id = c.id
                AND con.tensio_primari < (parcs1.v_name * 1.2) AND con.tensio_primari > (parcs1.v_name * 0.8)
    WHERE t.reductor IS True    --Reductor
      AND e.codi = 1            --Functionament
      AND c.active IS True
      AND id_provincia IN (SELECT id FROM res_country_state
      WHERE comunitat_autonoma=
        (SELECT id
         FROM res_comunitat_autonoma
         WHERE codi='09'))
    GROUP BY t.id, CMT_NOMBRE, se.id, parcs1.id, POTENCIA;
