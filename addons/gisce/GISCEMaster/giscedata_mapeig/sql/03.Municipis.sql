SELECT DISTINCT m.id,
  m.name,
  p.id AS id_prov,
  c.id AS id_comarca
  FROM res_municipi AS m
    INNER JOIN res_comarca AS c ON c.id = m.comarca
    INNER JOIN res_country_state AS p ON m.state = p.id
  WHERE p.id IN (select id from res_country_state
    WHERE comunitat_autonoma=
          (SELECT id FROM res_comunitat_autonoma
          WHERE codi='09'))
  AND (m.id in (SELECT municipi FROM giscedata_at_linia
        WHERE active AND provincia IN
          (select id from res_country_state
            WHERE comunitat_autonoma=
              (SELECT id
                FROM res_comunitat_autonoma
                WHERE codi='09'))) OR
      m.id in (SELECT id_municipi FROM giscedata_cts
        WHERE active AND id_provincia IN
          (select id from res_country_state
            WHERE comunitat_autonoma=
              (SELECT id
                FROM res_comunitat_autonoma
                WHERE codi='09')))
  )
ORDER BY m.id;
