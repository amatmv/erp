SELECT
  t.name AS id_tram,
  t.origen||' - '||t.final,
  trim(to_char(longitud_cad/1000, '99999990D999999'), ' ') AS longitud,
  CASE WHEN t.tipus = 2 THEN 'SUB' WHEN t.tipus = 1 THEN 'AER' END AS tipus,
  t.cable,
  t.circuit AS circuit,
  t.linia
  FROM giscedata_at_tram t
    INNER JOIN giscedata_at_linia l ON t.linia = l.id
    INNER JOIN giscedata_at_cables c ON t.cable = c.id
    INNER JOIN giscedata_at_tipuscable tc ON c.tipus = tc.id
       WHERE l.tensio >= 40000
        AND t.baixa IS False
        AND l.baixa IS False
        -- S'exclou la línia 'FICTICIA'
        AND l.name != '1'
        AND l.propietari
        AND l.provincia IN (SELECT id FROM res_country_state
          WHERE comunitat_autonoma=
          (SELECT id
            FROM res_comunitat_autonoma
            WHERE codi='09'))
        AND tc.codi != 'E'
       ORDER BY id_tram