SELECT DISTINCT st.id,
       st.name
    FROM res_country_state AS st
        INNER JOIN res_country AS c ON st.country_id = c.id
    WHERE c.code = 'ES'
          AND (st.id IN (SELECT id_provincia FROM giscedata_cts WHERE active IS True)
               OR st.id IN (SELECT provincia FROM giscedata_at_linia WHERE active IS True))
          AND comunitat_autonoma=(select id from res_comunitat_autonoma where codi='09')
    ORDER BY st.id;