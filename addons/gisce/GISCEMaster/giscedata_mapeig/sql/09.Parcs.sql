
-- Si es modifica aquesta consulta, cal modificar també les consultes:
-- 08.Conjunts_Transformacio.sql, 10.Circuits_AT.sql i 11.Circuits_MT.sql
SELECT parc.id as ID_PARC,
    parc.name|| '-' || (tensio.tensio/1000)::text as NOMBRE,
    parc.subestacio_id AS ID_EST,
    tensio.tensio/1000 AS V,
    parc.tecnologia AS TIPO,
    CASE WHEN parc.propietari IS True THEN 'N' WHEN parc.propietari IS False THEN 'S' END AS AJENO
  FROM giscedata_parcs AS parc
    INNER JOIN giscedata_tensions_tensio AS tensio ON tensio.id = parc.tensio_id
    INNER JOIN giscedata_cts_subestacions AS se ON se.id = parc.subestacio_id
    INNER JOIN giscedata_cts AS ct ON ct.id = se.ct_id
  WHERE parc.active AND
    tensio.tensio IS NOT Null AND
    ct.id_provincia IN (SELECT id FROM res_country_state
      WHERE comunitat_autonoma=
        (SELECT id
         FROM res_comunitat_autonoma
         WHERE codi='09'))
  GROUP BY ID_PARC, NOMBRE, ID_EST, V, TIPO, AJENO
  ORDER BY ID_EST, V;
