SELECT DISTINCT m.id,
  m.name,
  m.id as id_mun
  FROM res_municipi AS m
    INNER JOIN res_country_state state on m.state = state.id
    INNER JOIN res_comunitat_autonoma ca on state.comunitat_autonoma = ca.id
  where  ca.codi='09'
  AND (m.id in (SELECT municipi FROM giscedata_at_linia
        WHERE active AND provincia IN
          (select id from res_country_state
            WHERE comunitat_autonoma=
              (SELECT id
                FROM res_comunitat_autonoma
                WHERE codi='09'))) OR
      m.id in (SELECT id_municipi FROM giscedata_cts
        WHERE active AND id_provincia IN
          (select id from res_country_state
            WHERE comunitat_autonoma=
              (SELECT id
                FROM res_comunitat_autonoma
                WHERE codi='09')))
  );