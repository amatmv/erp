SELECT DISTINCT l.id, l.municipi
  FROM  giscedata_at_linia l
  INNER JOIN giscedata_revisions_at_revisio rev ON rev.name = l.id
  INNER JOIN giscedata_at_tram t ON t.linia = l.id
  INNER JOIN giscedata_at_cables c ON t.cable = c.id
  INNER JOIN giscedata_at_tipuscable tc ON c.tipus = tc.id
  WHERE l.tensio >= 40000
    AND l.baixa is False
    and l.propietari
    AND rev.trimestre IN %(trimestres)s
    AND l.provincia IN (SELECT id FROM res_country_state
      WHERE comunitat_autonoma=
        (SELECT id
          FROM res_comunitat_autonoma
          WHERE codi='09'))
    AND l.name != '1'
    AND tc.codi != 'E'
  GROUP BY l.id
  ORDER BY l.id;