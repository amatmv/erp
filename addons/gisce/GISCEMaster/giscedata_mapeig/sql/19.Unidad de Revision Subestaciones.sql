SELECT
  ct.id::text AS id_unidad_revision,
  se.id AS id_modelo_red,
  ct.name AS nombre,
  to_char(max(tri.data_final), 'DD/MM/YYYY') AS fecha_prevista_revision,
  ct.id_municipi AS id_localidad
FROM giscedata_cts ct
	INNER JOIN giscedata_revisions_ct_revisio rev ON ct.id = rev.name
	INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
	INNER JOIN giscedata_cts_subtipus tip ON tip.id = ct.id_subtipus
	INNER JOIN  giscedata_cts_subestacions se ON ct.id = se.ct_id
  INNER JOIN res_country_state state on ct.id_provincia = state.id
  INNER JOIN res_comunitat_autonoma ca on state.comunitat_autonoma = ca.id
WHERE (ct.ct_baixa IS NULL or ct.ct_baixa = FALSE )
  AND (ct.active = 'True')
  AND (ct.id_installacio in (2))
  AND ct.propietari
  AND ca.codi='09' -- CT in Catalunya
GROUP BY ct.id,se.id,tip.tipus_id,ca.codi
ORDER BY ct.name, ct.id;