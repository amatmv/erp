-- Subestacions
SELECT '1' || rev.id AS id_inspeccion,
    ct.id AS id_unidad_revision,
    'S' AS tipo_unidad_revision,
    to_char(rev.data, 'DD/MM/YYYY') AS fecha_inspeccion,
    rev.tecnic AS id_colegiado_acta,
    rev.tecnic AS id_colegiado_certificado
  FROM giscedata_cts ct
    INNER JOIN giscedata_cts_installacio inst ON ct.id_installacio = inst.id
    INNER JOIN giscedata_revisions_ct_revisio rev ON ct.id = rev.name
    INNER JOIN (
      SELECT
      rev.id as id,
      max(rev.data) over (partition by rev.name ) AS rev_data,
      rank() over (partition by rev.name order by rev.data desc nulls last, rev.id desc)
      FROM giscedata_revisions_ct_revisio rev
      where rev.trimestre IN %(trimestres)s
    ) AS rev_data ON rev_data.id = rev.id AND rev_data.rank = 1
WHERE inst.name IN ('SE')
    AND ct.active IS TRUE
    AND ct.ct_baixa IS FALSE
    AND ct.propietari IS TRUE
    AND ct.id_provincia IN (
      SELECT id
      FROM res_country_state
      WHERE comunitat_autonoma = (
        SELECT id
        FROM res_comunitat_autonoma
        WHERE codi='09'
      )
    )
UNION
-- Centres de distribució
SELECT '1' || rev.id AS id_inspeccion,
    ct.id AS id_unidad_revision,
    'Z' AS tipo_unidad_revision,
    to_char(rev.data, 'DD/MM/YYYY') AS fecha_inspeccion,
    rev.tecnic AS id_colegiado_acta,
    rev.tecnic AS id_colegiado_certificado
  FROM giscedata_cts ct
    INNER JOIN giscedata_cts_installacio inst ON ct.id_installacio = inst.id
    INNER JOIN giscedata_revisions_ct_revisio rev ON ct.id = rev.name
    INNER JOIN (
      SELECT
      rev.id as id,
      max(rev.data) over (partition by rev.name ) AS rev_data,
      rank() over (partition by rev.name order by rev.data desc nulls last, rev.id desc)
      FROM giscedata_revisions_ct_revisio rev
      where rev.trimestre IN %(trimestres)s
    ) AS rev_data ON rev_data.id = rev.id AND rev_data.rank = 1
WHERE inst.name NOT IN ('SE')
    AND ct.active IS TRUE
    AND ct.ct_baixa IS FALSE
    AND ct.propietari IS TRUE
    AND ct.id_provincia IN (
      SELECT id
      FROM res_country_state
      WHERE comunitat_autonoma = (
        SELECT id
        FROM res_comunitat_autonoma
        WHERE codi='09'
      )
    )
UNION
-- Linies de mitja tensió
SELECT
  '2' || rev_at.id AS id_inspeccion,
  at.id AS id_unidad_revision,
  'L' AS tipo_unidad_revision,
  to_char(rev_data.rev_data, 'DD/MM/YYYY') AS fecha_inspeccion,
  rev_at.tecnic AS id_colegiado_certificado,
  rev_at.tecnic AS id_colegiado_acta
FROM giscedata_revisions_at_revisio AS rev_at
INNER JOIN (
  SELECT
  rev.id as id,
  max(rev.data) over (partition by rev.name ) AS rev_data,
  rank() over (partition by rev.name order by rev.data desc nulls last, rev.id desc)
  FROM giscedata_revisions_at_revisio rev
  where rev.trimestre IN %(trimestres)s
) AS rev_data ON rev_data.id = rev_at.id AND rev_data.rank = 1
INNER JOIN giscedata_at_linia AS at ON rev_at.name = at.id
WHERE at.tensio < 40000
  AND at.active
  AND provincia IN (
    SELECT id
    FROM res_country_state
    WHERE comunitat_autonoma = (
      SELECT id
      FROM res_comunitat_autonoma
      WHERE codi='09'
    )
  )
UNION
-- Linies d'alta tensió
SELECT
  '2' || rev_at.id AS id_inspeccion,
  at.id AS id_unidad_revision,
  'A' AS tipo_unidad_revision,
  to_char(rev_data.rev_data, 'DD/MM/YYYY') AS fecha_inspeccion,
  rev_at.tecnic AS id_colegiado_certificado,
  rev_at.tecnic AS id_colegiado_acta
FROM giscedata_revisions_at_revisio AS rev_at
INNER JOIN (
  SELECT
  rev.id as id,
  max(rev.data) over (partition by rev.name ) AS rev_data,
  rank() over (partition by rev.name order by rev.data desc nulls last, rev.id desc)
  FROM giscedata_revisions_at_revisio rev
  where rev.trimestre IN %(trimestres)s
) AS rev_data ON rev_data.id = rev_at.id AND rev_data.rank = 1
INNER JOIN giscedata_at_linia AS at ON rev_at.name = at.id
WHERE at.tensio >= 40000
  AND at.active
  AND provincia IN (
    SELECT id
    FROM res_country_state
    WHERE comunitat_autonoma = (
      SELECT id
      FROM res_comunitat_autonoma
      WHERE codi='09'
    )
  )
ORDER BY tipo_unidad_revision, id_inspeccion;