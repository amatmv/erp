SELECT c.id, c.name,
  CASE
    WHEN tc.codi = 'S' THEN 'SUB'
    ELSE 'AER'
  END AS tipus,
  intensitat_admisible,
  trim(to_char(resistencia, '99999990D999999'), ' ') AS resistencia,
  trim(to_char(reactancia, '99999990D999999'), ' ') AS reactancia
  FROM giscedata_at_cables AS c
  INNER JOIN giscedata_at_tipuscable tc ON (c.tipus = tc.id AND tc.codi NOT IN ('E', 'I'))
ORDER BY c.id;
