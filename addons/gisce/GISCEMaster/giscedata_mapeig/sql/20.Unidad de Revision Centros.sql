SELECT
  ct.id::text AS id_unidad_revision,
  ct.id AS id_modelo_red,
  ct.name AS nombre,
  to_char(max(tri.data_final), 'DD/MM/YYYY') AS fecha_prevista_revision,
  ct.id_municipi AS id_localidad,
  CASE WHEN tip.tipus_id = 1 THEN 'Intempèrie'
    WHEN tip.tipus_id = 2 THEN 'Interior'
    ELSE 'Desconegut'
  END AS tipo
FROM giscedata_cts ct
	INNER JOIN giscedata_revisions_ct_revisio rev ON ct.id = rev.name
	INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
	INNER JOIN giscedata_cts_subtipus tip ON tip.id = ct.id_subtipus
	INNER JOIN giscedata_mapeig_ct_tipus mp ON mp.id = tip.categoria_mapeig
WHERE (ct.ct_baixa IS NULL or ct.ct_baixa = FALSE )
  AND (ct.active = 'True')
  AND (ct.id_installacio not in (2))
  AND ct.propietari
GROUP BY ct.id,tip.tipus_id
ORDER BY ct.name, ct.id;