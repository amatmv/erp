SELECT
    pos.id AS id_cir,
    pos.name AS nom_cir,
    pos.parc_id as parc_id
FROM giscedata_cts_subestacions_posicio pos
    INNER JOIN giscedata_tensions_tensio AS tensio ON tensio.id = pos.tensio
    INNER JOIN giscedata_cts_subestacions AS se ON pos.subestacio_id = se.id
    INNER JOIN giscedata_cts AS ct ON ct.id = se.ct_id
    INNER JOIN res_country_state state on ct.id_provincia = state.id
    INNER JOIN res_comunitat_autonoma ca on state.comunitat_autonoma = ca.id
WHERE tensio.tensio < 40000
  AND ca.codi='09' -- CT in Catalunya
ORDER BY pos.id;

