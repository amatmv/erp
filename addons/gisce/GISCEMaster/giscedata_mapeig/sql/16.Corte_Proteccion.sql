CREATE OR REPLACE FUNCTION traduir_tipus(src text) RETURNS text as $$
BEGIN
    RETURN CASE src
        WHEN 'FUS_AT' THEN 'FUS'
        WHEN 'INT_AT' THEN 'IS'
        WHEN 'DISJ_AT' THEN 'IS'
        WHEN 'INT_AUTO_AT' THEN 'INT'
        WHEN 'RF_AT' THEN 'INT'
        WHEN 'SEC_B' THEN 'SEC'
        WHEN 'SEC_C' THEN 'SEC'
        WHEN 'SEC-M' THEN 'SEC'
        WHEN 'SEC-MPR' THEN 'SEC'
        WHEN 'SECCIONADOR' THEN 'SEC'
        WHEN 'SECCIONADOR_SF6' THEN 'IS'
        ELSE 'SEC' END;
END;
$$ LANGUAGE plpgsql;


SELECT
	id,
	codi,
	x,
	y,
	fus,
	tipo_funcional ,
	id_cd,
	id_linia
FROM
	(
	SELECT
		s.id AS id,
		s.codi,
		v.x::TEXT AS x,
		v.y::TEXT AS y,
		'31' AS Fus,
		traduir_tipus(b.name) AS tipo_funcional,
		CASE
			WHEN (REGEXP_SPLIT_TO_ARRAY(cella.installacio, '\,'))[1] = 'giscedata.cts' THEN (REGEXP_SPLIT_TO_ARRAY(cella.installacio, '\,'))[2]::INT
			WHEN (REGEXP_SPLIT_TO_ARRAY(cella.installacio, '\,'))[1] = 'giscedata.at.suport' THEN NULL
		END AS ID_CD,
		MAX(pos.id) AS ID_LINIA
	FROM
		giscegis_blocs_seccionadorat s
	INNER JOIN giscegis_vertex v ON
		s.vertex = v.id
	INNER JOIN giscegis_blocs_seccionadorat_blockname b ON
		b.id = s.blockname
	INNER JOIN giscegis_polyline_vertex pv ON
		pv.vertex = v.id
	INNER JOIN giscegis_polyline_vertex_rel pvr ON
		pvr.vertex_id = pv.id
	INNER JOIN giscegis_edge e ON
		e.polyline = pvr.polyline_id
	INNER JOIN giscedata_at_tram gt ON
		gt.name = e.id_linktemplate
	INNER JOIN giscedata_at_linia lat ON
		lat.id = gt.linia
	INNER JOIN giscedata_cts_subestacions_posicio AS pos ON
		gt.circuit = pos.id
	INNER JOIN giscedata_celles_cella AS cella ON
		cella.name = s.codi
	WHERE
		s.codi IN (
			SELECT cell.name
		FROM
			giscedata_celles_cella cell
		WHERE
			cell.active
			AND cell.propietari)
		AND lat.provincia IN (
			SELECT id
		FROM
			res_country_state
		WHERE
			comunitat_autonoma = (
				SELECT id
			FROM
				res_comunitat_autonoma
			WHERE
                codi = '09'))
	GROUP BY
		s.id,
		v.x,
		v.y,
		b.name,
		cella.installacio
UNION ALL
	SELECT
		s.id AS id,
		s.codi,
		v.x::TEXT AS x,
		v.y::TEXT AS y,
		'31' AS Fus,
		traduir_tipus(b.name) AS tipo_funcional,
		(REGEXP_SPLIT_TO_ARRAY(cell_cts.installacio, '\,'))[2]::INT AS id_cd,
		lat.id AS id_linia
	FROM
		giscegis_blocs_seccionadorunifilar s
	INNER JOIN giscegis_vertex v ON
		s.vertex = v.id
	INNER JOIN giscegis_blocs_seccionador_unifilar_blockname b ON
		b.id = s.blockname
	INNER JOIN giscedata_celles_cella cell_sup ON
		s.codi = cell_sup.name
		AND cell_sup.installacio ILIKE 'giscedata.at.suport,%%'
	INNER JOIN giscedata_at_suport gs ON
		(REGEXP_SPLIT_TO_ARRAY(cell_sup.installacio, '\,'))[2]::INT = cell_sup.id
	INNER JOIN giscedata_at_linia lat ON
		lat.id = gs.linia
	INNER JOIN giscedata_celles_cella cell_cts ON
		s.codi = cell_cts.name
		AND cell_cts.installacio ILIKE 'giscedata.cts,%%'
	INNER JOIN giscedata_cts ct ON
		ct.id = (REGEXP_SPLIT_TO_ARRAY(cell_cts.installacio, '\,'))[2]::INT
	WHERE
		s.codi IN (
			SELECT cell.name
		FROM
			giscedata_celles_cella cell
		WHERE
			cell.active
			AND cell.propietari)
		AND ( lat.provincia IN (
			SELECT id
		FROM
			res_country_state
		WHERE
			comunitat_autonoma = (
				SELECT id
			FROM
				res_comunitat_autonoma
			WHERE
				codi = '09'))
		OR ct.id_provincia IN (
			SELECT id
		FROM
			res_country_state
		WHERE
			comunitat_autonoma = (
				SELECT id
			FROM
				res_comunitat_autonoma
			WHERE
				codi = '09')) )
	GROUP BY
		s.id,
		v.x,
		v.y,
		b.name,
		cell_cts.installacio,
		lat.id
UNION ALL
	SELECT
		s.id AS id,
		s.codi,
		v.x::TEXT AS x,
		v.y::TEXT AS y,
		'31' AS Fus,
		traduir_tipus(b.name) AS tipo_funcional,
		(REGEXP_SPLIT_TO_ARRAY(cell_cts.installacio, '\,'))[2]::INT AS id_cd,
		lat.id AS id_linia
	FROM
		giscegis_blocs_interruptorat s
	INNER JOIN giscegis_vertex v ON
		s.vertex = v.id
	INNER JOIN giscegis_blocs_interruptorat_blockname b ON
		b.id = s.blockname
	INNER JOIN giscedata_celles_cella cell_sup ON
		s.codi = cell_sup.name
		AND cell_sup.installacio ILIKE 'giscedata.at.suport,%%'
	INNER JOIN giscedata_at_suport gs ON
		(REGEXP_SPLIT_TO_ARRAY(cell_sup.installacio, '\,'))[2]::INT = cell_sup.id
	INNER JOIN giscedata_at_linia lat ON
		lat.id = gs.linia
	INNER JOIN giscedata_celles_cella cell_cts ON
		s.codi = cell_cts.name
		AND cell_cts.installacio ILIKE 'giscedata.cts,%%'
	INNER JOIN giscedata_cts ct ON
		ct.id = (REGEXP_SPLIT_TO_ARRAY(cell_cts.installacio, '\,'))[2]::INT
	WHERE
		s.codi IN (
			SELECT cell.name
		FROM
			giscedata_celles_cella cell
		WHERE
			cell.active
			AND cell.propietari)
		AND ( lat.provincia IN (
			SELECT id
		FROM
			res_country_state
		WHERE
			comunitat_autonoma = (
				SELECT id
			FROM
				res_comunitat_autonoma
			WHERE
				codi = '09'))
		OR ct.id_provincia IN (
			SELECT id
		FROM
			res_country_state
		WHERE
			comunitat_autonoma = (
				SELECT id
			FROM
				res_comunitat_autonoma
			WHERE
				codi = '09')) )
	GROUP BY
		s.id,
		v.x,
		v.y,
		b.name,
		cell_cts.installacio,
		lat.id
UNION ALL
	SELECT
		s.id AS id,
		s.codi,
		v.x::TEXT AS x,
		v.y::TEXT AS y,
		'31' AS Fus,
		traduir_tipus(b.name) AS tipo_funcional,
		CASE
			WHEN (ARRAY_UPPER(REGEXP_SPLIT_TO_ARRAY(codi, '-'), 1) - ARRAY_LOWER(REGEXP_SPLIT_TO_ARRAY(codi, '-'), 1) + 1) = 3 THEN (
				SELECT id
			FROM
				giscedata_cts
			WHERE
				NAME LIKE '%%-' || SUBSTRING(codi FROM 'FA-#"%%#"-%%' FOR '#'))
			WHEN (ARRAY_UPPER(REGEXP_SPLIT_TO_ARRAY(codi, '-'), 1) - ARRAY_LOWER(REGEXP_SPLIT_TO_ARRAY(codi, '-'), 1) + 1) = 2 THEN (
				SELECT id
			FROM
				giscedata_cts
			WHERE
				NAME LIKE 'CT-' || SUBSTRING(codi FROM 'FA-#"[0-9]+#"%%' FOR '#'))::INT
		END AS id_cd,
		MAX(pos.id) AS ID_LINIA
	FROM
		giscegis_blocs_fusiblesat s
	INNER JOIN giscegis_vertex v ON
		s.vertex = v.id
	INNER JOIN giscegis_blocs_fusiblesat_blockname b ON
		s.blockname = b.id
	INNER JOIN giscegis_polyline_vertex pv ON
		pv.vertex = v.id
	INNER JOIN giscegis_polyline_vertex_rel pvr ON
		pvr.vertex_id = pv.id
	INNER JOIN giscegis_edge e ON
		e.polyline = pvr.polyline_id
	INNER JOIN giscedata_at_tram gt ON
		gt.name = e.id_linktemplate
	INNER JOIN giscedata_at_linia lat ON
		lat.id = gt.linia
	INNER JOIN giscedata_cts_subestacions_posicio AS pos ON
		gt.circuit = pos.id
	WHERE
		s.codi IN (
			SELECT cell.name
		FROM
			giscedata_celles_cella cell
		WHERE
			cell.active
			AND cell.propietari)
		AND lat.provincia IN (
			SELECT id
		FROM
			res_country_state
		WHERE
			comunitat_autonoma = (
				SELECT id
			FROM
				res_comunitat_autonoma
			WHERE
				codi = '09'))
	GROUP BY
		s.id,
		v.x,
		v.y,
		b.name
	ORDER BY
		id) AS t;
