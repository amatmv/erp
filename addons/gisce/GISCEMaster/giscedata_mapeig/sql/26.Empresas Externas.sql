SELECT
  row_number() OVER() AS id_empresa_externa,
  tec.organisme,
  tec.domicili,
  tec.telefon,
  tec.municipi
FROM (
       SELECT distinct max(id) AS id,
         organisme as empresa
       FROM giscedata_revisions_tecnic
       GROUP BY organisme
       ORDER BY organisme
     ) AS ext,
  giscedata_revisions_tecnic tec
WHERE ext.id = tec.id
  AND tec.active IS TRUE;

