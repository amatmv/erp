SELECT
  pos.id AS ID_CIR,
  pos.name AS NOM_CIR,
  parcs.id AS ID_PARC1,
  '' AS ID_PARC2,
  '' AS ID_PARC3,
  '' AS ID_PARC4,
  '' AS ID_PARC5,
  '' AS ID_PARC6,
  p.name AS COMPA
FROM giscedata_cts_subestacions_posicio pos
  INNER JOIN giscedata_tensions_tensio AS tensio ON tensio.id = pos.tensio
  INNER JOIN (
  -- De 09.Parcs.sql
    SELECT parc.id,
    parc.subestacio_id AS id_est,
    tensio.tensio AS v_name
  FROM giscedata_parcs AS parc
    INNER JOIN giscedata_tensions_tensio AS tensio ON tensio.id = parc.tensio_id
    INNER JOIN giscedata_cts_subestacions AS se ON se.id = parc.subestacio_id
    INNER JOIN giscedata_cts AS ct ON ct.id = se.ct_id
  WHERE tensio.tensio IS NOT Null
    AND ct.id_provincia IN (SELECT id FROM res_country_state
      WHERE comunitat_autonoma=
        (SELECT id
         FROM res_comunitat_autonoma
         WHERE codi='09'))
  GROUP BY parc.id, id_est, v_name
  ORDER BY id_est, v_name
            ) AS parcs ON parcs.id_est = pos.subestacio_id AND tensio.tensio = parcs.v_name
  INNER JOIN giscedata_cts_subestacions AS se ON pos.subestacio_id = se.id
  INNER JOIN giscedata_cts AS ct ON ct.id = se.ct_id
  INNER JOIN res_company AS p ON ct.propietat_de = p.id
WHERE tensio.tensio >= 40000 AND
  ct.id_provincia IN (SELECT id FROM res_country_state
  WHERE comunitat_autonoma=
    (SELECT id
    FROM res_comunitat_autonoma
    WHERE codi='09'))
ORDER BY pos.id;
