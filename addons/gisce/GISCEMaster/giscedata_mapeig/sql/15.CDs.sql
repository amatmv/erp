SELECT ct.id,
    ct.name,
    ct.descripcio,
    count(t.id) AS n_trafos,
    mp.codi AS tipo_constructivo,
    CASE
      WHEN inst.name = 'CR' THEN 'SC'
      WHEN inst.name = 'CS' THEN 'CM'
      WHEN inst.name = 'CRM' THEN 'CT'
      ELSE inst.name END AS tipus,
    coalesce(sum(t.potencia_nominal),0) AS pot_inst,
    coalesce(poli.pot_contr, '0,00') AS pot_contr,
    trim(to_char(v.x, '9999999D999999'), ' ') AS x,
    trim(to_char(v.y, '9999999D999999'), ' ') AS y,
    '31' AS Fus,
    --array_to_string(get_all_layers(gis.node), ',') as id_circuit,
    ct.circuit AS id_circuit,
    ct.id_municipi AS id_loca
  FROM giscedata_cts ct
  INNER JOIN giscedata_transformador_trafo t ON ct.id = t.ct
  INNER JOIN giscedata_transformador_estat e ON t.id_estat = e.id AND e.codi = 1
  INNER JOIN giscedata_cts_subtipus sub ON sub.id = ct.id_subtipus
  INNER JOIN giscedata_mapeig_ct_tipus mp ON mp.id = sub.categoria_mapeig
  INNER JOIN giscegis_blocs_ctat g ON g.ct = ct.id
  INNER JOIN giscegis_vertex v ON v.id = g.vertex
  INNER JOIN giscedata_cts_installacio inst ON ct.id_installacio = inst.id
  INNER JOIN (
    SELECT cups.et AS et, trim(to_char(sum(p.potencia), '99999990D99'), ' ') AS pot_contr
            FROM giscedata_cups_ps cups
            INNER JOIN giscedata_polissa p
                ON p.cups = cups.id AND p.active IS True
            group by cups.et
            ) AS poli ON poli.et = ct.name
    INNER JOIN giscegis_blocs_ctat gis ON gis.ct = ct.id
    WHERE ct.active IS True
          AND inst.name NOT IN ('SE', 'CH', 'CP', 'DS')
          AND ct.propietari
          AND ct.id_provincia IN (SELECT id FROM res_country_state
            WHERE comunitat_autonoma=
            (SELECT id
              FROM res_comunitat_autonoma
              WHERE codi='09'))
    GROUP BY ct.id, ct.name, ct.descripcio, v.x, v.y, inst.name, poli.pot_contr, mp.codi, ct.id_municipi, gis.node, id_circuit
    ORDER BY ct.id;
