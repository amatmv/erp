-- Subestacions
SELECT 'S' AS tipo_unidad_revision,
       ct.id AS id_unidad_revision,
       exp.industria AS numero_expediente
    FROM giscedata_expedients_expedient exp
        INNER JOIN giscedata_cts_expedients_rel ct_rel ON ct_rel.expedient_id = exp.id
        INNER JOIN giscedata_cts ct ON ct_rel.ct_id = ct.id
        INNER JOIN giscedata_revisions_ct_revisio rev ON rev.name = ct.id
        INNER JOIN (
            SELECT max(rev.id) AS id_revisio,
                   max(exp.id) AS id_expedient
                FROM giscedata_expedients_expedient exp
                    INNER JOIN giscedata_cts_expedients_rel ct_rel ON ct_rel.expedient_id = exp.id
                    INNER JOIN giscedata_cts ct ON ct_rel.ct_id = ct.id
                    INNER JOIN giscedata_revisions_ct_revisio rev ON rev.name = ct.id
                    INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
                    INNER JOIN giscedata_cts_installacio inst ON inst.id = ct.id_installacio
                    INNER JOIN (
                               SELECT max(exp.industria_data) AS data, ct_rel.ct_id AS ct_id
                                    FROM giscedata_expedients_expedient exp
                                        INNER JOIN giscedata_cts_expedients_rel ct_rel ON ct_rel.expedient_id = exp.id
                                    GROUP BY ct_rel.ct_id
                                    ORDER BY ct_rel.ct_id) q_max
                                ON q_max.ct_id = ct_rel.ct_id AND exp.industria_data = q_max.data
                WHERE rev.trimestre IN %(trimestres)s
                  AND inst.name IN ('SE')
                  AND ct.active IS True
                  AND id_provincia IN (SELECT id FROM res_country_state
                      WHERE comunitat_autonoma=
                      (SELECT id
                        FROM res_comunitat_autonoma
                        WHERE codi='09'))
                GROUP BY exp.industria_data, ct.id, tri.trieni, ct.name
                ORDER BY ct.id) AS sq ON sq.id_revisio = rev.id AND sq.id_expedient = exp.id
    GROUP BY ct.id, exp.industria
UNION
-- Centres de distribució
SELECT 'Z' AS tipo_unidad_revision,
       ct.id AS id_unidad_revision,
       exp.industria AS numero_expediente
    FROM giscedata_expedients_expedient exp
        INNER JOIN giscedata_cts_expedients_rel ct_rel ON ct_rel.expedient_id = exp.id
        INNER JOIN giscedata_cts ct ON ct_rel.ct_id = ct.id
        INNER JOIN giscedata_revisions_ct_revisio rev ON rev.name = ct.id
        INNER JOIN (
            SELECT max(rev.id) AS id_revisio,
                   max(exp.id) AS id_expedient,
                   exp.industria_data AS exp_industria_data,
                   tri.trieni AS trieni,
                   ct.id AS ct_id,
                   ct.name AS cd_name
            FROM giscedata_expedients_expedient exp
                INNER JOIN giscedata_cts_expedients_rel ct_rel ON ct_rel.expedient_id = exp.id
                INNER JOIN giscedata_cts ct ON ct_rel.ct_id = ct.id
                INNER JOIN giscedata_revisions_ct_revisio rev ON rev.name = ct.id
                INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
                INNER JOIN giscedata_cts_installacio inst ON inst.id = ct.id_installacio
                INNER JOIN (
                            SELECT max(exp.industria_data) AS data, ct.id AS ct_id
                                FROM giscedata_expedients_expedient exp
                                    INNER JOIN giscedata_cts_expedients_rel ct_rel ON ct_rel.expedient_id = exp.id
                                    INNER JOIN giscedata_cts ct ON ct_rel.ct_id = ct.id
                                GROUP BY ct.id
                                ORDER BY ct.id) q_max
                            ON q_max.ct_id = ct.id AND exp.industria_data = q_max.data
            WHERE rev.trimestre IN %(trimestres)s
                AND inst.name NOT IN ('SE', 'CH', 'CP', 'DS')
                AND ct.id_provincia IN (select id from res_country_state
                    WHERE comunitat_autonoma=
                        (SELECT id
                         FROM res_comunitat_autonoma
                         WHERE codi='09'))
                GROUP BY exp.industria_data, ct.id, tri.trieni, ct.name
                ORDER BY ct.id
        ) AS sq ON sq.id_revisio = rev.id AND sq.id_expedient = exp.id
    WHERE ct.active IS True
    AND ct.propietari
    GROUP BY ct.id, exp.industria
UNION
-- Línies de mitja tensió
SELECT 'L' AS tipo_unidad_revision,
       lin.id AS id_unidad_revision,
       exp.industria AS numero_expediente
    FROM giscedata_expedients_expedient exp
        INNER JOIN giscedata_at_tram_expedient tram_rel ON tram_rel.expedient_id = exp.id
        INNER JOIN giscedata_at_tram tram ON tram_rel.tram_id = tram.id
        INNER JOIN giscedata_at_linia lin ON tram.linia = lin.id
        INNER JOIN giscedata_revisions_at_revisio rev ON rev.name = lin.id
        INNER JOIN (
            SELECT max(rev.id) AS id_revisio,
                   max(exp.id) AS id_expedient
                FROM giscedata_expedients_expedient exp
                    INNER JOIN giscedata_at_tram_expedient tram_rel ON tram_rel.expedient_id = exp.id
                    INNER JOIN giscedata_at_tram tram ON tram_rel.tram_id = tram.id
                    INNER JOIN giscedata_at_linia lin ON tram.linia = lin.id
                    INNER JOIN giscedata_revisions_at_revisio rev ON rev.name = lin.id
                    INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
                    INNER JOIN giscedata_at_installacio inst ON inst.id = lin.tipus_installacio
                    INNER JOIN (
                               SELECT max(exp.industria_data) AS data, lin.id AS linia_id
                                    FROM giscedata_expedients_expedient exp
                                        INNER JOIN giscedata_at_tram_expedient tram_rel ON tram_rel.expedient_id = exp.id
                                        INNER JOIN giscedata_at_tram tram ON tram_rel.tram_id = tram.id
                                        INNER JOIN giscedata_at_linia lin ON tram.linia = lin.id
                                    GROUP BY lin.id
                                    ORDER BY lin.id) q_max
                                ON q_max.linia_id = lin.id AND exp.industria_data = q_max.data
                WHERE rev.trimestre IN %(trimestres)s
                    AND lin.tensio < 40000
                    AND lin.baixa IS False
                    AND lin.provincia IN (SELECT id FROM res_country_state
                      WHERE comunitat_autonoma=
                      (SELECT id
                        FROM res_comunitat_autonoma
                        WHERE codi='09'))
                GROUP BY  exp.industria_data, tri.trieni, lin.id, lin.name
                ORDER BY lin.id) AS sq ON sq.id_revisio = rev.id AND sq.id_expedient = exp.id
    GROUP BY lin.id, exp.industria
UNION
-- Línies d'alta tensió
SELECT 'A' AS tipo_unidad_revision,
       lin.id AS id_unidad_revision,
       exp.industria AS numero_expediente
    FROM giscedata_expedients_expedient exp
        INNER JOIN giscedata_at_tram_expedient tram_rel ON tram_rel.expedient_id = exp.id
        INNER JOIN giscedata_at_tram tram ON tram_rel.tram_id = tram.id
        INNER JOIN giscedata_at_linia lin ON tram.linia = lin.id
        INNER JOIN giscedata_revisions_at_revisio rev ON rev.name = lin.id
        INNER JOIN (
            SELECT max(rev.id) AS id_revisio,
                   max(exp.id) AS id_expedient
                FROM giscedata_expedients_expedient exp
                    INNER JOIN giscedata_at_tram_expedient tram_rel ON tram_rel.expedient_id = exp.id
                    INNER JOIN giscedata_at_tram tram ON tram_rel.tram_id = tram.id
                    INNER JOIN giscedata_at_linia lin ON tram.linia = lin.id
                    INNER JOIN giscedata_revisions_at_revisio rev ON rev.name = lin.id
                    INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
                    INNER JOIN giscedata_at_installacio inst ON inst.id = lin.tipus_installacio
                    INNER JOIN (
                               SELECT max(exp.industria_data) AS data, lin.id AS linia_id
                                    FROM giscedata_expedients_expedient exp
                                        INNER JOIN giscedata_at_tram_expedient tram_rel ON tram_rel.expedient_id = exp.id
                                        INNER JOIN giscedata_at_tram tram ON tram_rel.tram_id = tram.id
                                        INNER JOIN giscedata_at_linia lin ON tram.linia = lin.id
                                    GROUP BY  lin.id
                                    ORDER BY lin.id) q_max
                               ON q_max.linia_id = lin.id AND exp.industria_data = q_max.data
                WHERE tri.id IN %(trienis)s
                    AND lin.tensio >= 40000
                    AND lin.provincia IN (SELECT id FROM res_country_state
                      WHERE comunitat_autonoma=
                      (SELECT id
                        FROM res_comunitat_autonoma
                        WHERE codi='09'))
                GROUP BY exp.industria_data, tri.trieni, lin.id, lin.name
                ORDER BY lin.id) AS sq ON sq.id_revisio = rev.id AND sq.id_expedient = exp.id
    GROUP BY lin.id, exp.industria
ORDER BY tipo_unidad_revision, id_unidad_revision;