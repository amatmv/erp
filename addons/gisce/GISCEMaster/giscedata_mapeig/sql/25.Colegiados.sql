SELECT tec.id,
    tec.n_collegiat,
    tec.name,
    tec.n_collegiat,
    tec.domicili,
    tec.telefon,
    tec.email,
    id_ext.id_empresa_externa,
    tec.municipi,
    tec.titolacio
FROM giscedata_revisions_tecnic tec,
    (SELECT DISTINCT  row_number() OVER() AS id_empresa_externa,organisme
      FROM giscedata_revisions_tecnic
      GROUP BY organisme
      ORDER BY organisme) AS id_ext
WHERE tec.organisme = id_ext.organisme
  AND tec.active is TRUE
ORDER BY tec.id;

