SELECT n.id,
       n.name,
       nodes_suma.suma AS n_conex,
       trim(to_char(v.x, '9999999D999999'), ' ') AS x,
       trim(to_char(v.y, '9999999D999999'), ' ') AS y,
       '31' AS Fus,
       nodes_suma.circuit AS id_circuit
FROM giscegis_nodes n
INNER JOIN giscegis_vertex v ON n.vertex = v.id
INNER JOIN
    (
    SELECT node,max(circuit) AS circuit,
           count(*) AS suma
        FROM
        (
            SELECT e.start_node AS node, circuit
            FROM giscegis_edge e
            INNER JOIN giscedata_at_tram t ON e.id_linktemplate = t.name
            INNER JOIN giscedata_at_linia l ON t.linia = l.id AND l.tensio < 40000
            WHERE e.layer IN ('_LINIES MT', 'EMBARRAT_AT_LINIES')
              AND l.provincia IN (SELECT id FROM res_country_state
              WHERE comunitat_autonoma=
                    (SELECT id
                     FROM res_comunitat_autonoma
                     WHERE codi = '09'))
            UNION ALL
            SELECT e.end_node AS node,circuit
            FROM giscegis_edge e
            INNER JOIN giscedata_at_tram t ON e.id_linktemplate = t.name
            INNER JOIN giscedata_at_linia l ON t.linia = l.id AND l.tensio < 40000
            WHERE e.layer in ('_LINIES MT', 'EMBARRAT_AT_LINIES')
              AND l.provincia IN (SELECT id FROM res_country_state
              WHERE comunitat_autonoma=
                    (SELECT id
                     FROM res_comunitat_autonoma
                     WHERE codi = '09' ))
            ORDER BY node
        ) AS nodes
    GROUP BY node
    HAVING count(*) > 1) AS nodes_suma ON nodes_suma.node = n.id
ORDER BY n.id;
