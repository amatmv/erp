SELECT
    t.name AS id_tram,
    ST_AsGeoJSON(at_geom.geom)
FROM giscedata_at_tram t
    INNER JOIN giscedata_at_linia l ON t.linia = l.id
    inner JOIN giscegis_lat_geom at_geom ON at_geom.name = t.id
WHERE l.tensio >= 40000
    AND l.name != '1'
    AND l.baixa is False
ORDER BY  id_tram;