SELECT DISTINCT c.id, c.name
  FROM res_comarca AS c
  WHERE c.id IN (
    SELECT id_comarca FROM giscedata_cts
    WHERE active IS True
      AND id_provincia IN
        (SELECT id FROM res_country_state
          WHERE comunitat_autonoma=
            (SELECT id FROM res_comunitat_autonoma
            WHERE codi='09')))
      OR c.id IN
        (SELECT comarca FROM giscedata_at_linia
          WHERE active IS True AND provincia IN
            (select id from res_country_state
              WHERE comunitat_autonoma=
                (SELECT id
                  FROM res_comunitat_autonoma
                  WHERE codi='09')))
    ORDER BY c.id;