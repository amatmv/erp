SELECT DISTINCT l.id, l.municipi
  FROM giscedata_at_linia l
    INNER JOIN giscedata_revisions_at_revisio rev ON rev.name = l.id
    INNER JOIN giscedata_trieni tri ON tri.id = rev.trimestre
  WHERE l.tensio < 40000
    AND l.baixa IS False
    AND rev.trimestre IN %(trimestres)s
    -- S'exclou la línia 'FICTICIA'
    AND l.name != '1'
    AND l.propietari
    AND l.provincia IN
      (SELECT id FROM res_country_state
        WHERE comunitat_autonoma=
        (SELECT id
          FROM res_comunitat_autonoma
          WHERE codi='09'))
    ORDER BY l.id;
