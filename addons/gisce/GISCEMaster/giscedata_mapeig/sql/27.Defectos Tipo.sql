-- CT
SELECT '1'||trim(to_char(tip.id, '9999'), ' ') AS id_defecto_tipo,
       tip.codi AS codigo,
       tip.descripcio AS descripcion
    FROM giscedata_revisions_ct_tipusdefectes tip
        INNER JOIN giscedata_revisions_ct_normativa norm ON tip.normativa_id = norm.id
    WHERE tip.intern IS NOT True AND norm.vigent = True
UNION
-- LAT
SELECT '2'||trim(to_char(def.id, '9999'), ' ') AS id_defecto_tipo,
       def.name AS codigo,
       def.descripcio AS descripcion
    FROM giscedata_revisions_at_tipusdefectes def
        INNER JOIN giscedata_revisions_at_normativa norm ON def.normativa = norm.id
    WHERE def.intern IS NOT True AND norm.vigent = True;

