# -*- coding: utf-8 -*-

import os
from os import listdir
from os.path import isfile, join, basename, splitext
import csv
import base64
import StringIO
import zipfile
from datetime import datetime
import tempfile
import json

import pooler
from osv import osv, fields
import tools
from tools.translate import _
import fiona
from fiona.crs import from_epsg
import netsvc


class WizardMapeig(osv.osv_memory):
    """
    Wizard per generar els informes de mapeig
    """

    _name = 'wizard.giscedata.mapeig'

    sql_path = "%s/giscedata_mapeig/sql" % tools.config['addons_path']
    no_pob = '.sense_poblacio'

    def action_exec_mapeig(self, cursor, uid, ids, context=None):
        """
            Generar els fitxers de mapeig

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected elements id
        :param context: OpenERP context
        :return:
        """

        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context)
        data = ''
        f_name = ''
        msg = []
        if not wiz:
            return False
        if wiz.complet:
            db = pooler.get_db_only(cursor.dbname)
            zf_io = StringIO.StringIO()
            writer = zipfile.ZipFile(
                zf_io, 'w',
                compression=zipfile.ZIP_DEFLATED)
            noms = self._informes_a_mostrar(cursor, uid, context)
            for i in noms:
                cr = db.cursor()
                if unicode(i[0]).isnumeric():
                    try:
                        fitxer = self.localitzar_fitxer(cr, uid, i[0], context)
                        val = self.generate_csv(cr, uid, ids, fitxer, context)
                        cr.commit()
                    except osv.except_osv as e:
                        msg.append(e.value)
                        cr.rollback()
                        continue
                    finally:
                        cr.close()
                    f_name, data = val.items()[0]
                    writer.writestr(f_name, base64.b64decode(data))
            cr = db.cursor()
            if wiz.shp:
                fitxers_shp = ['AT.sql', 'MT.sql']
                for fitxer in fitxers_shp:
                    val_shp = self.generate_shp(cr, uid, ids, fitxer, context)
                    for filename, value in val_shp.iteritems():
                        writer.writestr(filename, value)
            cr.close()
            writer.close()
            data = base64.b64encode(zf_io.getvalue())
            f_name = 'resum_mapeig.zip'
            zf_io.close()
        else:
            if wiz.shp:
                fitxers = ['AT.sql', 'MT.sql']
            else:
                fitxers = [self.localitzar_fitxer(cursor, uid, wiz.informe,
                                                  context)]
            try:
                zf_io = StringIO.StringIO()
                writer = zipfile.ZipFile(zf_io, 'w',
                                         compression=zipfile.ZIP_DEFLATED)
                for fitxer in fitxers:
                    if unicode(wiz.informe).isnumeric() and not wiz.shp:
                        val = self.generate_csv(cursor, uid, ids,
                                                fitxer, context)
                        f_name, data = val.items()[0]

                        f_name = os.path.splitext(f_name)[0]+'.csv'

                    else:
                        val_shp = self.generate_shp(cursor, uid, ids, fitxer,
                                                    context)
                        for filename, value in val_shp.iteritems():
                            writer.writestr(filename, value)
                        f_name = 'SHP.zip'

                if not (unicode(wiz.informe).isnumeric() and not wiz.shp):
                    writer.close()
                    data = base64.b64encode(zf_io.getvalue())

            except osv.except_osv as e:
                raise e

        msg = '\n'.join(msg)
        wiz.write({'state': 'done', 'name': f_name, 'file': data, 'info': msg})
        return True

    def purge_sequence(self, cursor, uid, seq):
        """
            Eliminar les sequences de la BD

        :param cursor: Database cursor
        :param uid: User id
        :param seq: List or strign of sequence name
        :return: None
        """

        if isinstance(seq, tuple):
            query = "SELECT relname FROM pg_class where relname in %s;"
        else:
            query = "SELECT relname FROM pg_class where relname = '%s';"
        query = query % str(seq)
        cursor.execute(query)
        dades = cursor.fetchall()
        for i in dades:
            query = "DROP sequence %s;" % i
            cursor.execute(query)
        return True

    def check_db_requirements(self, cursor, uid, ids, fname):
        """
            Satisfà els requeriments previs per a l'execució fname
            en el cas de ser necessari

        :param cursor: Database Cursor
        :param uid: User id
        :param ids: Afected elements id
        :param fname: File name
        :return:True
        """

        plpgsql = ('16.Corte_Proteccion.sql')
        if fname in plpgsql:
            query = """
                      SELECT lanname
                      FROM pg_language
                      WHERE lanname LIKE 'plpgsql';
                      """
            cursor.execute(query)
            dades = cursor.fetchall()
            if not dades:
                query = "CREATE LANGUAGE plpgsql;"
                cursor.execute(query)
        elif fname in ('09.Parcs.sql',
                       '10.Circuits_AT.sql',
                       '11.Circuits_MT.sql'):
            seq = ('seq_mapeig_parcs')
            self.purge_sequence(cursor, uid, seq)
        elif fname == '08.Conjunts_Transformacio.sql':
            seq = ('seq_mapeig_parcs_1',
                   'seq_mapeig_parcs_2',
                   'seq_mapeig_parcs_3')
            self.purge_sequence(cursor, uid, seq)
        return True

    def get_trimestres(self, cursor, uid, ids, trienis, context=None):
        """
            Returns ids of trimestres from the given trienis

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of afected ids
        :param trienis: List of trienis
        :param context: OpenERP context
        :return: Tuple of trimestres ids
        """

        trimestres = []
        trieni_obj = self.pool.get('giscedata.trieni')
        for trieni in trienis:
            search_filter = [('trieni', '=', trieni)]
            trimestres_ids = trieni_obj.search(cursor, uid, search_filter)
            trimestres += trimestres_ids
        return tuple(trimestres)

    def get_trieni_actual(self, cursor, uid, ids, context=None):
        """
            Retorna el trieni actual

        :param cursor: Database curosr
        :param uid: User id
        :param ids: Lis of afected ids
        :param context: OpenERP context
        :return: Tuple of trienis
        """

        trieni_obj = self.pool.get('giscedata.trieni')
        now = datetime.now()
        trimestre = ((now.month - 1) / 3) + 1
        nom = '%s/%s' % (trimestre, now.year)
        trieni_id = trieni_obj.search(cursor, uid, [('name', '=', nom)])
        trieni = trieni_obj.read(cursor, uid, trieni_id, ['trieni'])
        if not trieni:
            return False
        ret = (trieni[0]['trieni'], trieni[0]['trieni']-1,)
        return ret

    def get_query(self, cursor, uid, ids, fname, context=None):
        """
            Retorna la consulta a executar

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of ids of afected elemens
        :param fname: File name
        :param context: OpenERP context
        :return: String with the query
        """

        fname_path = join(self.sql_path, fname)
        query = open(fname_path).read()
        return query

    def generate_shp(self, cursor, uid, ids, fname, context=None):
        """
            Generate shp data using sql queries

        :param cursor: Database cursro
        :param uid: User id
        :param ids: List of ids
        :param fname: Filename
        :param context: OpenERP context
        :return: dict {filename:data}
        """
        logger = netsvc.Logger()
        query = self.get_query(cursor, uid, ids, fname, context)
        trieni = self.get_trieni_actual(cursor, uid, ids, context)
        conf_obj = self.pool.get('res.config')
        epsg = int(conf_obj.get(cursor, uid, 'giscegis_srid', 25831))

        schema = {
            'geometry': 'LineString',
            'properties': {
                ('FID', 'int'),
                ("SHAPE", "str"),
                ("ID_BDE", "int"),
                ("ID", "int")
            }
        }
        temp = tempfile.NamedTemporaryFile().name

        output = fiona.open(
            temp,
            'w',
            driver='ESRI Shapefile',
            crs=from_epsg(epsg),
            schema=schema)
        cursor.execute(query, {'trieni': trieni})
        data = cursor.fetchall()
        fid = 1
        for row in data:
            try:
                if row[1]:
                    geom_data = json.loads(row[1])
                    line = geom_data['coordinates']

                    feature = {
                        'geometry': {
                             'coordinates': line, 'type': 'LineString'
                        },
                        'properties': {
                            'FID': fid,
                            "SHAPE": "LWPOLYLINE",
                            'ID_BDE': row[0],
                            "ID": fid
                        }
                    }
                    output.write(feature)
                else:
                    logger.notifyChannel('giscedata_mapeig',
                                         netsvc.LOG_WARNING,
                                         'Row sense geom:{}'.format(row[0]))
            except ValueError:
                pass
            fid += 1
        try:
            output.close()
        except RuntimeError:
            pass
        with open(join(temp, '{0}.shp'.format(basename(temp))), 'r') as f:
            data_shp = f.read()
        with open(join(temp, '{0}.dbf'.format(basename(temp))), 'r') as f:
            data_dbf = f.read()
        with open(join(temp, '{0}.shx'.format(basename(temp))), 'r') as f:
            data_shx = f.read()
        with open(join(temp, '{0}.prj'.format(basename(temp))), 'r') as f:
            data_prj = f.read()
        with open(join(temp, '{0}.cpg'.format(basename(temp))), 'r') as f:
            data_cpg = f.read()
        filename = splitext(basename(fname))[0]
        return {
            '{0}.shp'.format(filename): data_shp,
            '{0}.dbf'.format(filename): data_dbf,
            '{0}.shx'.format(filename): data_shx,
            '{0}.prj'.format(filename): data_prj,
            '{0}.cgp'.format(filename): data_cpg,
        }

    def generate_csv(self, cursor, uid, ids, fname, context=None):
        """
            Executa la consulta sql continguda en el fitxer fname
           retorna un CSV amb el resultat

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of afected ids
        :param fname: Filename
        :param context: OpenERP context
        :return: {fname: file data}
        """

        if not context:
            context = {}
        self.check_db_requirements(cursor, uid, ids, fname)
        query = self.get_query(cursor, uid, ids, fname, context)
        trienis = self.get_trieni_actual(cursor, uid, ids, context)
        trimestres = self.get_trimestres(cursor, uid, ids, trienis, context)
        try:
            params = {'trienis': trienis, 'trimestres': trimestres}
            cursor.execute(query, params)
        except Exception as e:
            error_msg = _(u"Error executant la consulta %s error:%s")
            raise osv.except_osv(
                _(u"Atenció"),
                error_msg % (fname, e.message))
        dades = cursor.fetchall()
        fname_out = '%s.csv' % fname[:-4]
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE,
                            escapechar="\\")
        if not dades:
            writer.writerow(dades)
        for row in dades:
            writer.writerow(row)
        val = {fname_out: base64.b64encode(output.getvalue())}
        output.close()
        return val

    def localitzar_fitxer(self, cursor, uid, prefix,  context=None):
        """
            Identifica el fitxer que comença amb prefix

        :param cursor: Database cursor
        :param uid: User id
        :param prefix: Numeric prefix of the report
        :param context: OpenERP context
        :return: File name
        """

        fitxers = self._informes_disponibles(cursor, uid, context)

        trobats = [i[1] for i in fitxers if i[0] == prefix]
        if not trobats:
            if unicode(prefix).isnumeric():
                raise osv.except_osv(
                    _(u"Atenció"),
                    _(u"No s'ha pogut identificar el fitxer especificat"))
        nom = [i for i in trobats]
        nom = '{0}.sql'.format(nom[0])
        return nom

    def _informes_disponibles(self, cursor, uid, context=None):
        """
            Retorna el llistat d'informes disponibles

        :param cursor: Database curosr
        :param uid: User id
        :param context: OpenERP context
        :return:  List of avaible reports
        """

        sql = self.sql_path
        files = [f for f in listdir(sql) if (isfile(join(sql, f))
                                             and f[-4:] == '.sql')]
        files.remove('AT.sql')
        files.remove('MT.sql')
        files.sort()
        return [(i[:2], i[:-4]) for i in files]

    def _informes_a_mostrar(self, cursor, uid, context=None):
        """
            Retorna el llistat d'informes a mostrar en el selection

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP context
        :return: Return a list of reports to show in the select
        """

        sql = self.sql_path
        no_pob = self.no_pob
        dbl_check = set()
        dbl_add = dbl_check.add
        files = [f for f in listdir(sql) if (isfile(join(sql, f))
                                             and f[-4:] == '.sql'
                                             and f[:2] not in dbl_check
                                             and not dbl_add(f[:2]))]
        files.remove('AT.sql')
        files.remove('MT.sql')
        files.sort()
        return [no_pob in i and (i[:2], i.replace(no_pob, '')[:-4])
                or (i[:2], i[:-4]) for i in files]

    _columns = {
        'name': fields.char('Filename', size=128),
        'file': fields.binary('Fitxer'),
        'state': fields.char('State', size=16),
        'complet': fields.boolean('Tots els fitxers',
                                  help=u"Generar tots els"
                                       u" continguts en un zip"),
        'informe': fields.selection(_informes_a_mostrar, 'Informe'),
        'info': fields.text('Resultat'),
        'shp': fields.boolean('Generar SHPs',
                              help=u"Generar fitxers SHP")
    }

    _defaults = {
        'state': lambda *a: 'init',
        'informe': lambda *a: '',
        'complet': lambda *a: False,
        'info': lambda *a: ''
    }

WizardMapeig()
