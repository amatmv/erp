# -*- coding: utf-8 -*-
from osv import osv, fields


class giscedataMapeigCtTipus(osv.osv):

    _name = 'giscedata.mapeig.ct.tipus'

    _columns = {
      'name': fields.char('Nom', size=64),
      'codi': fields.char('Codi', size=3),
    }


giscedataMapeigCtTipus()


class giscedataCtsSubtipus(osv.osv):

    _name = 'giscedata.cts.subtipus'
    _inherit = 'giscedata.cts.subtipus'

    _columns = {
      'categoria_mapeig': fields.many2one('giscedata.mapeig.ct.tipus', 'Categoria Mapeig'),
    }

giscedataCtsSubtipus()
