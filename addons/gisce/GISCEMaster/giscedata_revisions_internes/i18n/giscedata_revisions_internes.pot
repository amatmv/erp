# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# * giscedata_revisions_internes
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 12:46+0000\n"
"PO-Revision-Date: 2019-07-29 11:54+0000\n"
"Last-Translator: Eduard Berloso <eberloso@gisce.net>\n"
"Language-Team: Catalan (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Generated-By: Babel 2.7.0\n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,observacions:0
#: field:giscedata.revisions.internes.revisio,observacions:0
#: view:giscedata.revisions.internes.defecte:0
#: view:giscedata.revisions.internes.revisio:0
msgid "Observacions"
msgstr "Observacions"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,tancada_per:0
msgid "Tancada per"
msgstr "Tancada per"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,state:0
msgid "Tancada"
msgstr "Tancada"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,data:0
msgid "Data programada"
msgstr "Data programada"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,template_name:0
msgid "Plantilla de nom"
msgstr "Plantilla de nom"

#. module: giscedata_revisions_internes
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.normativa:0
msgid "Municipis"
msgstr "Municipis"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,menu_own_id:0
msgid "Menú usuari"
msgstr "Menú usuari"

#. module: giscedata_revisions_internes
#: model:ir.actions.report.xml,name:giscedata_revisions_internes.report_ri_llistat_defectes
msgid "Revisions Internes: Llistat de Defectes"
msgstr "Revisions Internes: Llistat de Defectes"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,local_name:0
msgid "Aclaració"
msgstr "Aclaració"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,state:0
msgid "Assignada"
msgstr "Assignada"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,normativa_id:0
#: field:giscedata.revisions.internes.item,normativa_id:0
#: field:giscedata.revisions.internes.normativa,name:0
#: view:giscedata.revisions.internes.normativa:0
msgid "Normativa"
msgstr "Normativa"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,type:0
msgid "Tipus"
msgstr "Tipus"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.defecte:0
msgid "Reparació"
msgstr "Reparació"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.valoracio,t_type:0
msgid "Dia"
msgstr "Dia"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,type:0
#: selection:giscedata.revisions.internes.item,type:0
msgid "Boleà"
msgstr "Boleà"

#. module: giscedata_revisions_internes
#: view:wizard.planificacio:0
msgid "Planificació de revisions"
msgstr "Planificació de revisions"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,descripcio:0
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:48
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:66
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:48
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:67
#: view:giscedata.revisions.internes.item:0
msgid "Descripció"
msgstr "Descripció"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_giscedata_revisions_internes_valoracio
msgid "giscedata.revisions.internes.valoracio"
msgstr "giscedata.revisions.internes.valoracio"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:140
#, python-format
msgid "Revisions de %s"
msgstr "Revisions de %s"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,ignorar_no_revisats:0
msgid "Ignorar items no revisats"
msgstr "Ignorar items no revisats"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Assignar"
msgstr "Assignar"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.item:0
msgid "Item a revisar"
msgstr "Item a revisar"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,name_str:0
msgid "A revisar"
msgstr "A revisar"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,action_id:0
msgid "Acció totes"
msgstr "Acció totes"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,data_real:0
msgid "Data real"
msgstr "Data real"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,sql:0
#: field:giscedata.revisions.internes.normativa,sql:0
#: view:giscedata.revisions.internes.model:0
#: view:giscedata.revisions.internes.normativa:0
msgid "SQL"
msgstr "SQL"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,state:0
msgid "Oberta"
msgstr "Oberta"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_giscedata_revisions_internes_item
msgid "giscedata.revisions.internes.item"
msgstr "giscedata.revisions.internes.item"

#. module: giscedata_revisions_internes
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_giscedata_revisions_internes_revisio_tree
#: view:giscedata.revisions.internes.revisio:0
msgid "Revisions"
msgstr "Revisions"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:617
msgid "Ja se li ha generat una revisió planificada"
msgstr "Ja se li ha generat una revisió planificada"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,dades:0
#: view:giscedata.revisions.internes.defecte:0
msgid "Dades"
msgstr "Dades"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.valoracio:0
msgid "Temps límit de la reparació"
msgstr "Temps límit de la reparació"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,revisio_id:0
msgid "Revisio"
msgstr "Revisio"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.normativa:0
msgid "Reassignar a revisions creades"
msgstr "Reassignar a revisions creades"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,name:0
#: field:giscedata.revisions.internes.item,name:0
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:45
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:63
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:45
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:64
msgid "Codi"
msgstr "Codi"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,data_reparacio:0
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:49
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:67
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:51
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:70
msgid "Data de reparació"
msgstr "Data de reparació"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,observacions_rep:0
msgid "Obseracions de reparació"
msgstr "Obseracions de reparació"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,items_str:0
#: view:giscedata.revisions.internes.item:0
msgid "Items a revisar"
msgstr "Items a revisar"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:604
msgid "No té ben configurada la periodicitat"
msgstr "No té ben configurada la periodicitat"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:408
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:418
msgid "Aquesta referència per la revisió no és correcta"
msgstr "Aquesta referència per la revisió no és correcta"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,reparat:0
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:50
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:69
msgid "Reparat"
msgstr "Reparat"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,state:0
msgid "Incorrecte"
msgstr "Incorrecte"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.valoracio,t_type:0
msgid "Mes"
msgstr "Mes"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,menu_name:0
msgid "Nom pel menú"
msgstr "Nom pel menú"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:633
msgid "No s'han generat revisions planificades"
msgstr "No s'han generat revisions planificades"

#. module: giscedata_revisions_internes
#: model:ir.actions.report.xml,name:giscedata_revisions_internes.report_ri_full_reparacio
msgid "Revisions Internes: Full de Reparació"
msgstr "Revisions Internes: Full de Reparació"

#. module: giscedata_revisions_internes
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Revisió"
msgstr "Revisió"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.normativa,data_inici:0
msgid "Data inici"
msgstr "Data inici"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.normativa,data_final:0
msgid "Data final"
msgstr "Data final"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.model:0
msgid "Crear Menú"
msgstr "Crear Menú"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,data_tancament:0
msgid "Data tancament"
msgstr "Data tancament"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,items_ids:0
#: view:giscedata.revisions.internes.defecte:0
msgid "Defectes"
msgstr "Defectes"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.valoracio,t_type:0
msgid "Hora"
msgstr "Hora"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.model:0
#: view:giscedata.revisions.internes.normativa:0
msgid "Domain"
msgstr "Domain"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,sequence:0
#: field:giscedata.revisions.internes.item,sequence:0
msgid "Ordre"
msgstr "Ordre"

#. module: giscedata_revisions_internes
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_revisons_internes
msgid "Revisions Internes"
msgstr "Revisions Internes"

#. module: giscedata_revisions_internes
#: model:ir.actions.act_window,name:giscedata_revisions_internes.action_wizard_planificacio_form
msgid "Crear revisions internes planificades"
msgstr "Crear revisions internes planificades"

#. module: giscedata_revisions_internes
#: view:wizard.planificacio:0
msgid ""
"Aquest assistent crearà les noves revisions segons la planificació "
"configurada en cadascuna."
msgstr "Aquest assistent crearà les noves revisions segons la planificació configurada en cadascuna."

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:649
msgid "S'ha eliminat l'element de la revisió"
msgstr "S'ha eliminat l'element de la revisió"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.item,active:0
#: field:giscedata.revisions.internes.model,active:0
msgid "Actiu"
msgstr "Actiu"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,state:0
msgid "No comprovat"
msgstr "No comprovat"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,valor:0
msgid "Valor"
msgstr "Valor"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/wizard/wizard_planificacio.py:23
msgid "Revisions planificades creades"
msgstr "Revisions planificades creades"

#. module: giscedata_revisions_internes
#: model:ir.module.module,description:giscedata_revisions_internes.module_meta_information
msgid ""
"Revisions\n"
"   * Possibilitat de fer revisions per qualsevol element de 'ERP.\n"
"  "
msgstr "Revisions\n   * Possibilitat de fer revisions per qualsevol element de 'ERP.\n  "

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,num_defectes:0
msgid "Núm. defectes reparats"
msgstr "Núm. defectes reparats"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,periodicitat_type:0
msgid "Periodicitat tipus"
msgstr "Periodicitat tipus"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.valoracio,t_type:0
msgid "Unitat"
msgstr "Unitat"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.item,type:0
msgid "Tipus de camp"
msgstr "Tipus de camp"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,state:0
msgid "Correcte"
msgstr "Correcte"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,state:0
msgid "Esborrany"
msgstr "Esborrany"

#. module: giscedata_revisions_internes
#: view:wizard.planificacio:0
msgid "Continuar"
msgstr "Continuar"

#. module: giscedata_revisions_internes
#: model:ir.actions.act_window,name:giscedata_revisions_internes.action_giscedata_revisions_internes_model_tree
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_giscedata_revisions_internes_model_tree
#: view:giscedata.revisions.internes.model:0
msgid "Models"
msgstr "Models"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,periodicitat_type:0
msgid "Dies"
msgstr "Dies"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.defecte:0
msgid "Observacions reparació"
msgstr "Observacions reparació"

#. module: giscedata_revisions_internes
#: view:wizard.planificacio:0
msgid "Cancel·lar"
msgstr "Cancel·lar"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.valoracio,active:0
msgid "Active"
msgstr "Active"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.normativa,active:0
msgid "Activa"
msgstr "Activa"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:158
#, python-format
msgid "Les meves revisions de %s"
msgstr "Les meves revisions de %s"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Planificació"
msgstr "Planificació"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_giscedata_revisions_internes_model
msgid "giscedata.revisions.internes.model"
msgstr "giscedata.revisions.internes.model"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,type:0
#: selection:giscedata.revisions.internes.item,type:0
msgid "Numèric"
msgstr "Numèric"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:89
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:407
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:417
msgid "Error"
msgstr "Error"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.normativa:0
msgid "Domain del model"
msgstr "Domain del model"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Passar a esborrany"
msgstr "Passar a esborrany"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/wizard/wizard_planificacio.py:17
msgid "No s'han detectat revisions a planificar"
msgstr "No s'han detectat revisions a planificar"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,state:0
#: field:giscedata.revisions.internes.revisio,state:0
msgid "Estat"
msgstr "Estat"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.defecte:0
#: view:giscedata.revisions.internes.item:0
#: view:giscedata.revisions.internes.model:0
#: view:giscedata.revisions.internes.normativa:0
#: view:giscedata.revisions.internes.revisio:0
#: view:giscedata.revisions.internes.valoracio:0
msgid "General"
msgstr "General"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,defectes_reparats:0
msgid "Defectes Reparats"
msgstr "Defectes Reparats"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,periodicitat_qty:0
msgid "Periodicitat quantitat"
msgstr "Periodicitat quantitat"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.valoracio,t_type:0
msgid "Any"
msgstr "Any"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.normativa,items_ids:0
msgid "Items de la normativa"
msgstr "Items de la normativa"

#. module: giscedata_revisions_internes
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_revisions_internes_configuracio
msgid "Configuració"
msgstr "Configuració"

#. module: giscedata_revisions_internes
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,periodicitat_type:0
msgid "Anys"
msgstr "Anys"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:1040
msgid "Manteniment"
msgstr "Manteniment"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_wizard_planificacio
msgid "wizard.planificacio"
msgstr "wizard.planificacio"

#. module: giscedata_revisions_internes
#: model:ir.actions.act_window,name:giscedata_revisions_internes.action_giscedata_revisions_internes_valoracio_tree
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_giscedata_revisions_internes_valoracio_tree
#: view:giscedata.revisions.internes.valoracio:0
msgid "Valoracions"
msgstr "Valoracions"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,data_limit_reparacio:0
msgid "Data límit reparació"
msgstr "Data límit reparació"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:90
msgid ""
"Hi ha revisions que estan utilitzant aquest objecte és millor fer servir "
"l'opció de desactivar"
msgstr "Hi ha revisions que estan utilitzant aquest objecte és millor fer servir l'opció de desactivar"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Tancar"
msgstr "Tancar"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Identificació"
msgstr "Identificació"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_giscedata_revisions_internes_defecte
msgid "giscedata.revisions.internes.defecte"
msgstr "giscedata.revisions.internes.defecte"

#. module: giscedata_revisions_internes
#: model:ir.actions.act_window,name:giscedata_revisions_internes.action_giscedata_revisions_internes_normativa_tree
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_giscedata_revisions_internes_normativa_tree
msgid "Normatives"
msgstr "Normatives"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,menu_id:0
msgid "Menú totes"
msgstr "Menú totes"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.valoracio,t_qty:0
msgid "Temps"
msgstr "Temps"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,operari:0
msgid "Tècnic"
msgstr "Tècnic"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,domain:0
#: field:giscedata.revisions.internes.normativa,domain:0
msgid "Expressió de cerca"
msgstr "Expressió de cerca"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_giscedata_revisions_internes_revisio
msgid "giscedata.revisions.internes.revisio"
msgstr "giscedata.revisions.internes.revisio"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:631
msgid ""
"\n"
" Revisió '{0}': '{1}'"
msgstr "\n Revisió '{0}': '{1}'"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:202
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:302
msgid "**ERROR: Expressió no vàlida"
msgstr "**ERROR: Expressió no vàlida"

#. module: giscedata_revisions_internes
#: model:ir.module.module,shortdesc:giscedata_revisions_internes.module_meta_information
msgid "Revisions internes"
msgstr "Revisions internes"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.normativa,model_domain:0
msgid "Domini del model"
msgstr "Domini del model"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,state:0
msgid "No aplicable"
msgstr "No aplicable"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,valoracio:0
#: field:giscedata.revisions.internes.valoracio,name:0
#: view:giscedata.revisions.internes.valoracio:0
msgid "Valoració"
msgstr "Valoració"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,item_id:0
msgid "Item"
msgstr "Item"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.item,descripcio:0
msgid "Descripció del defecte"
msgstr "Descripció del defecte"

#. module: giscedata_revisions_internes
#: code:addons/giscedata_revisions_internes/giscedata_revisions_internes.py:573
#, python-format
msgid ""
"No es pot donar la revisió per tancada, queden %s ítems que no s'han "
"comprovat."
msgstr "No es pot donar la revisió per tancada, queden %s ítems que no s'han comprovat."

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,model_id:0
#: field:giscedata.revisions.internes.normativa,model_id:0
#: field:giscedata.revisions.internes.revisio,model:0
#: field:giscedata.revisions.internes.revisio,name:0
#: view:giscedata.revisions.internes.model:0
msgid "Model"
msgstr "Model"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.normativa:0
#: view:giscedata.revisions.internes.revisio:0
msgid "Ítems a revisar"
msgstr "Ítems a revisar"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.revisio,empresa:0
msgid "Empresa"
msgstr "Empresa"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.defecte:0
msgid "Defecte"
msgstr "Defecte"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.revisio,periodicitat_type:0
msgid "Mesos"
msgstr "Mesos"

#. module: giscedata_revisions_internes
#: selection:giscedata.revisions.internes.defecte,type:0
#: selection:giscedata.revisions.internes.item,type:0
msgid "Texte"
msgstr "Texte"

#. module: giscedata_revisions_internes
#: view:giscedata.revisions.internes.revisio:0
msgid "Obrir"
msgstr "Obrir"

#. module: giscedata_revisions_internes
#: model:ir.model,name:giscedata_revisions_internes.model_giscedata_revisions_internes_normativa
msgid "giscedata.revisions.internes.normativa"
msgstr "giscedata.revisions.internes.normativa"

#. module: giscedata_revisions_internes
#: model:ir.ui.menu,name:giscedata_revisions_internes.menu_giscedata_own_revisions_internes_revisio_tree
msgid "Les meves revisions"
msgstr "Les meves revisions"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.model,action_own_id:0
msgid "Acció usuari"
msgstr "Acció usuari"

#. module: giscedata_revisions_internes
#: field:giscedata.revisions.internes.defecte,data_comprovacio:0
msgid "Data de comprovació"
msgstr "Data de comprovació"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:22
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:22
msgid "No disponible"
msgstr "No disponible"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:26
msgid "Full de reparació de {}"
msgstr "Full de reparació de {}"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:29
msgid "LLISTAT DE DEFECTES NO REPARATS EN {}"
msgstr "LLISTAT DE DEFECTES NO REPARATS EN {}"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:32
msgid "FULL DE REPARACIÓ"
msgstr "FULL DE REPARACIÓ"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:39
msgid ""
"S'han d'omplir els apartats 'Data de reparació', 'Observacions' i "
"'Operari/s'."
msgstr "S'han d'omplir els apartats 'Data de reparació', 'Observacions' i 'Operari/s'."

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:46
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:64
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:46
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:65
msgid "Data"
msgstr "Data"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:47
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:65
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:47
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:66
msgid "Instal·lació"
msgstr "Instal·lació"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:50
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:68
msgid "Operari/s"
msgstr "Operari/s"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:84
#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:96
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:86
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:98
msgid "<br>"
msgstr "<br>"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:95
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:97
msgid "No Disponible"
msgstr "No Disponible"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:122
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:160
msgid "OBSERVACIONS RECONEIXEMENT"
msgstr "OBSERVACIONS RECONEIXEMENT"

#: rml:giscedata_revisions_internes/report/full_de_reparacio.mako:134
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:172
msgid "OBSERVACIONS REPARACIÓ"
msgstr "OBSERVACIONS REPARACIÓ"

#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:26
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:29
msgid "LLISTAT DE DEFECTES EN {}"
msgstr "LLISTAT DE DEFECTES EN {}"

#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:32
msgid "LLISTAT DE DEFECTES"
msgstr "LLISTAT DE DEFECTES"

#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:49
#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:68
msgid "Data límit"
msgstr "Data límit"

#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:119
msgid " "
msgstr " "

#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:145
msgid "Sí"
msgstr "Sí"

#: rml:giscedata_revisions_internes/report/llistat_de_defectes.mako:149
msgid "No"
msgstr "No"
