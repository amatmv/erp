<%
    from datetime import datetime
    page_break = 1
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions_internes/report/llistat_de_defectes.css"/>
    </head>
    %for revisio in objects:
        %if revisio.data:
            <%
                date_temp = datetime.strptime(revisio.data, '%Y-%m-%d %H:%M:%S')
                date = date_temp.strftime('%d/%m/%Y')
            %>
        %else:
            <%
                date= _(u"No disponible")
            %>
        %endif
        <body>
            <div class="doc_title"><strong>${_(u"LLISTAT DE DEFECTES EN {}").format(revisio.model.local_name.upper())}</strong></div>
            <div>
                <div class="haederbox_left">
                    <p>${_(u"LLISTAT DE DEFECTES EN {}").format(revisio.model.local_name.upper())}</p>
                </div>
                <div class="haederbox_center">
                    <p><strong>${_(u"LLISTAT DE DEFECTES")}</strong></p>
                </div>
                <div class="haederbox_right">
                    <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                    <div>${company.name}</div>
                </div>
                <div id="nota">
                    <br>
                </div>
            </div>
            <table>
                <thead>
                    <tr>
                        <th style="width: 8%">${_(u"Codi")}</th>
                        <th style="width: 8%">${_(u"Data")}</th>
                        <th style="width: 16%">${_(u"Instal·lació")}</th>
                        <th style="width: 30%">${_(u"Descripció")}</th>
                        <th style="width: 10%">${_(u"Data límit")}</th>
                        <th style="width: auto">${_(u"Reparat")}</th>
                        <th style="width: 15%">${_(u"Data de reparació")}</th>
                    </tr>
                </thead>
                <tbody>
                    %for defecte in revisio.items_ids:
                        %if defecte.item_id.active and defecte.state == 'B':
                            %if page_break == 0:
                                    </tbody>
                                </table>
                                <p style="page-break-after:always"></p>
                                <table>
                                    <thead>
                                        <tr>
                                            <th style="width: 8%">${_(u"Codi")}</th>
                                            <th style="width: 8%">${_(u"Data")}</th>
                                            <th style="width: 16%">${_(u"Instal·lació")}</th>
                                            <th style="width: 30%">${_(u"Descripció")}</th>
                                            <th style="width: 10%">${_(u"Data límit")}</th>
                                            <th style="width: auto">${_(u"Reparat")}</th>
                                            <th style="width: 15%">${_(u"Data de reparació")}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            %endif
                            %if revisio.name:
                                <%
                                    model, elem_id = revisio.name.split(',')
                                    model_obj = revisio.pool.get(model)
                                    municipi_obj = revisio.pool.get("res.municipi")
                                    temp = model_obj.read(
                                        cursor,
                                        uid,
                                        int(elem_id),
                                        ['name', 'descripcio', 'id_municipi']
                                    )
                                    municipi_element = temp.get('id_municipi', _(u"<br>"))
                                    if isinstance(municipi_element, tuple):
                                        municipi_element = municipi_element[1]
                                    descripcio = "{} {}".format(
                                        temp.get('name',""),
                                        temp.get('descripcio',"")
                                    )
                                    page_break += 1
                                %>
                            %else:
                                <%
                                    descripcio = _(u"No Disponible")
                                    municipi_element = _(u"<br>")
                                    page_break += 1
                                %>
                            %endif
                            %if defecte.data_limit_reparacio:
                                <%
                                    date_tmp_limit = datetime.strptime(defecte.data_limit_reparacio, '%Y-%m-%d %H:%M:%S')
                                    date_limit = date_tmp_limit.strftime('%d/%m/%Y')
                                %>
                            %else:
                                <%
                                    date_limit = False
                                %>
                            %endif
                            %if defecte.data_reparacio:
                                <%
                                    date_temp = datetime.strptime(defecte.data_reparacio, '%Y-%m-%d %H:%M:%S')
                                    date_rep = date_temp.strftime('%d/%m/%Y')
                                %>
                            %else:
                                <%
                                    date_rep= _(u" ")
                                %>
                            %endif
                            <tr class="data_row">
                                <td>
                                    <strong>${defecte.id}</strong>
                                </td>
                                <td >
                                    ${date}
                                </td>
                                <td>
                                    <strong><span class="lletra_petita">${descripcio}</span></strong>
                                </td>
                                <td>
                                    <span class="lletra_petita">${defecte.item_id.name} ${defecte.item_id.descripcio}</span>
                                </td>
                                %if defecte.data_limit_reparacio:
                                    <td>
                                        ${date_limit}
                                    </td>
                                %else:
                                    <td>
                                    </td>
                                %endif
                                %if defecte.reparat:
                                    <td>
                                        ${_(u"Sí")}
                                    </td>
                                %else:
                                    <td>
                                        ${_(u"No")}
                                    </td>
                                %endif
                                <td>
                                    ${date_rep}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="5" rowspan="2" class="bordered_cell">
                                    <strong>${_(u"OBSERVACIONS RECONEIXEMENT")}</strong><br><br>
                                    ${defecte.observacions or "<br>"}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="5" rowspan="2" class="bordered_cell">
                                    <strong>${_(u"OBSERVACIONS REPARACIÓ")}</strong><br><br>
                                    ${defecte.observacions_rep or ""}
                                    <strong><div class="municipi">${municipi_element}</div></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        %endif
                        %if page_break >= 4:
                            <%
                                page_break = 0
                            %>
                        %endif
                    %endfor
                </tbody>
            </table>
        </body>
    %endfor
</html>