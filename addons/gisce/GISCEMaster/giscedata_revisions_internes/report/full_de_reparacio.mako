<%
    from datetime import datetime
    page_break = 1
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_revisions_internes/report/full_de_reparacio.css"/>
    </head>
    %for revisio in objects:
        %if revisio.data:
            <%
                date_temp = datetime.strptime(revisio.data, '%Y-%m-%d %H:%M:%S')
                date = date_temp.strftime('%d/%m/%Y')
            %>
        %else:
            <%
                date= _(u"No disponible")
            %>
        %endif
        <body>
            <div class="doc_title"><strong>${_(u"Full de reparació de {}").format(revisio.model.local_name.lower())}</strong></div>
            <div>
                <div class="haederbox_left">
                    <p>${_(u"LLISTAT DE DEFECTES NO REPARATS EN {}").format(revisio.model.local_name.upper())}</p>
                </div>
                <div class="haederbox_center">
                    <p><strong>${_(u"FULL DE REPARACIÓ")}</strong></p>
                </div>
                <div class="haederbox_right">
                    <img id="logo" src="data:image/jpeg;base64,${company.logo}"/>
                    <div>${company.name}</div>
                </div>
                <div id="nota">
                    ${_(u"S'han d'omplir els apartats 'Data de reparació', 'Observacions' i 'Operari/s'.")}
                </div>
            </div>
            <table>
                <thead>
                    <tr>
                        <th style="width: 8%">${_(u"Codi")}</th>
                        <th style="width: 8%">${_(u"Data")}</th>
                        <th style="width: 16%">${_(u"Instal·lació")}</th>
                        <th style="width: 40%">${_(u"Descripció")}</th>
                        <th style="width: 15%">${_(u"Data de reparació")}</th>
                        <th style="width: auto">${_(u"Operari/s")}</th>
                    </tr>
                </thead>
                <tbody>
                    %for defecte in revisio.items_ids:
                        %if defecte.item_id.active and defecte.state == 'B' and not defecte.reparat:
                            %if page_break == 0:
                                    </tbody>
                                </table>
                                <p style="page-break-after:always"></p>
                                <table>
                                    <thead>
                                        <tr>
                                            <th style="width: 8%">${_(u"Codi")}</th>
                                            <th style="width: 8%">${_(u"Data")}</th>
                                            <th style="width: 16%">${_(u"Instal·lació")}</th>
                                            <th style="width: 40%">${_(u"Descripció")}</th>
                                            <th style="width: 15%">${_(u"Data de reparació")}</th>
                                            <th style="width: auto">${_(u"Operari/s")}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            %endif
                            %if revisio.name:
                                <%
                                    model, elem_id = revisio.name.split(',')
                                    model_obj = revisio.pool.get(model)
                                    municipi_obj = revisio.pool.get("res.municipi")
                                    temp = model_obj.read(
                                        cursor,
                                        uid,
                                        int(elem_id),
                                        ['name', 'descripcio', 'id_municipi']
                                    )
                                    municipi_element = temp.get('id_municipi', _(u"<br>"))
                                    if isinstance(municipi_element, tuple):
                                        municipi_element = municipi_element[1]
                                    descripcio = "{} {}".format(
                                        temp.get('name',""),
                                        temp.get('descripcio',"")
                                    )
                                    page_break += 1
                                %>
                            %else:
                                <%
                                    descripcio = _(u"No Disponible")
                                    municipi_element = _(u"<br>")
                                    page_break += 1
                                %>
                            %endif
                            <tr class="data_row">
                                <td>
                                    <strong>${defecte.id}</strong>
                                </td>
                                <td >
                                    ${date}
                                </td>
                                <td>
                                    <strong><span class="lletra_petita">${descripcio}</span></strong>
                                </td>
                                <td>
                                    <span class="lletra_petita">${defecte.item_id.name} ${defecte.item_id.descripcio}</span>
                                </td>
                                <td class="bordered_cell">
                                </td>
                                <td class="bordered_cell" rowspan="5">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="4" rowspan="2" class="bordered_cell">
                                    <strong>${_(u"OBSERVACIONS RECONEIXEMENT")}</strong><br><br>
                                    ${defecte.observacions or "<br>"}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td colspan="4" rowspan="2" class="bordered_cell">
                                    <strong>${_(u"OBSERVACIONS REPARACIÓ")}</strong><br><br>
                                    <strong><div class="municipi">${municipi_element}</div></strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        %endif
                        %if page_break >= 4:
                            <%
                                page_break = 0
                            %>
                        %endif
                    %endfor
                </tbody>
            </table>
        </body>
    %endfor
</html>