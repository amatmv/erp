# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config


class ReportWebkitHtmlRevisionsInternes(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(
            ReportWebkitHtmlRevisionsInternes, self
        ).__init__(cursor, uid, name, context=context)

        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path']
        })


webkit_report.WebKitParser(
    'report.giscedata.revisions.internes.full.reparacio',
    'giscedata.revisions.internes.revisio',
    'giscedata_revisions_internes/report/full_de_reparacio.mako',
    parser=ReportWebkitHtmlRevisionsInternes
)

webkit_report.WebKitParser(
    'report.giscedata.revisions.internes.llistat.defectes',
    'giscedata.revisions.internes.revisio',
    'giscedata_revisions_internes/report/llistat_de_defectes.mako',
    parser=ReportWebkitHtmlRevisionsInternes
)