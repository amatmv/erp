# -*- coding: utf-8 -*-
from __future__ import absolute_import
from datetime import datetime
import logging
from osv import osv, fields
from osv.expression import expression
from tools.translate import _
from tools.safe_eval import safe_eval as eval

import report
import types
import pooler

from lxml import etree

ITEM_TYPES = [
    ('numeric', 'Numèric'),
    ('boolean', 'Boleà'),
    ('text', 'Texte')
]

DEFECTE_STATES = [
    ('A', 'Correcte'),
    ('B', 'Incorrecte'),
    ('C', 'No comprovat'),
    ('D', 'No aplicable')
]


class GiscedataRevisionsInternesModel(osv.osv):
    _name = 'giscedata.revisions.internes.model'
    _inherits = {'ir.model': 'model_id'}

    def __init__(self, pool, cr):
        super(GiscedataRevisionsInternesModel, self).__init__(pool, cr)
        patch_register_all()

    def create(self, cursor, uid, vals, context=None):
        """
        Method that patches the view when a new revision is added

        :param cursor: Database cursor
        :param uid: User id
        :param vals: Create vaules
        :param context: OpenERP context
        :return: super create return
        """
        s = super(GiscedataRevisionsInternesModel, self)
        res = s.create(cursor, uid, vals, context)
        att = self.browse(cursor, uid, res)
        obj = self.pool.get(att.model)
        patch_object(obj)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        """Mirem si hi ha alguna revisió assignada a aquest objecte.
        """
        rev_obj = self.pool.get('giscedata.revisions.internes.revisio')
        menu_obj = self.pool.get('ir.ui.menu')
        act_obj = self.pool.get('ir.actions.act_window')
        modrev_obj = self.pool.get('giscedata.revisions.internes.model')

        # Asignem uid = 1 perquè pugui crear els menus, accions, ir.model.data
        uid = 1
        menu_ids = []
        action_ids = []
        for model in self.browse(cursor, uid, ids, context):
            if model.menu_id:
                menu_ids.append(model.menu_id.id)
            if model.action_id:
                action_ids.append(model.action_id.id)
            if model.menu_own_id:
                menu_ids.append(model.menu_own_id.id)
            if model.action_own_id:
                action_ids.append(model.action_own_id.id)

            res = rev_obj.search(cursor, uid, [
                ('name', 'ilike', '%s,' % model.model)
            ], context={'active_test': False})
            res_mod = modrev_obj.search(
                cursor, uid, [('model_id', '=', model.model_id.id)])

            if len(res_mod) == 1:
                att = self.browse(cursor, uid, res_mod[0])
                obj = self.pool.get(att.model)
                unpatch_object(obj)
            if res:
                raise osv.except_osv(
                    _(u"Error"),
                    _(u"Hi ha revisions que estan utilitzant aquest objecte "
                      u"és millor fer servir l'opció de desactivar")
                )

        if menu_ids:
            menu_obj.unlink(cursor, uid, menu_ids)
        if action_ids:
            act_obj.unlink(cursor, uid, action_ids)
        super(GiscedataRevisionsInternesModel,
              self).unlink(cursor, uid, ids, context)
        return True

    def crear_menu(self, cursor, uid, ids, context=None):
        """Crearà el menú per les revisions internes d'aquest model.

        El codi està basat en tools.convert._tag_menuitem()
        """

        act_obj = self.pool.get('ir.actions.act_window')
        menu_obj = self.pool.get('ir.ui.menu')
        imd_obj = self.pool.get('ir.model.data')
        # Menu de totes les revisions
        imd_all_id = imd_obj._get_id(
            cursor, uid, 'giscedata_revisions_internes',
            'menu_giscedata_revisions_internes_revisio_tree')
        res_all_id = imd_obj.read(cursor, uid, imd_all_id, ['res_id'])['res_id']
        # Menu de les revisions propies
        imd_own_id = imd_obj._get_id(
            cursor, uid, 'giscedata_revisions_internes',
            'menu_giscedata_own_revisions_internes_revisio_tree')
        res_own_id = imd_obj.read(cursor, uid, imd_own_id, ['res_id'])['res_id']

        # Asignem uid = 1 perquè pugui crear els menus, accions, ir.model.data
        uid = 1
        for model in self.browse(cursor, uid, ids, context):
            # Netejem de revisions el model.
            if model.action_id:
                model.write({'action_id': False})
                model.action_id.unlink()
            if model.menu_id:
                model.write({'menu_id': False})
                model.menu_id.unlink()
            if model.action_own_id:
                model.write({'action_own_id': False})
                model.action_own_id.unlink()
            if model.menu_own_id:
                model.write({'menu_own_id': False})
                model.menu_own_id.unlink()

            # Creem l'acció per a totes les revisions
            name = _('Revisions de %s') % (model.menu_name or model.local_name)
            act_all_id = act_obj.create(cursor, uid, {
                'name': name,
                'view_id': False,
                'domain': [('model', '=', model.id)],
                'res_model': 'giscedata.revisions.internes.revisio',
                'context': {'revisio_model': model.id},
                'view_type': 'form',
                'view_mode': 'tree,form'
            })
            menu_all_id = menu_obj.create(cursor, uid, {
                'name': name,
                'parent_id': res_all_id,
                'icon': 'STOCK_JUSTIFY_FILL',
                'action': 'ir.actions.act_window,%s' % act_all_id,
            }, context=context)

            # Creem l'acció per a les revisions propies
            name = _('Les meves revisions de %s') % (model.menu_name or model.local_name)
            act_own_id = act_obj.create(cursor, uid, {
                'name': name,
                'view_id': False,
                'domain': "[('model', '=', {}), ('operari', '=', uid), '|', ('state','ilike','assignada'), ('state','ilike','oberta')]".format(model.id),
                'res_model': 'giscedata.revisions.internes.revisio',
                'context': {'revisio_model': model.id},
                'view_type': 'form',
                'view_mode': 'tree,form'
            })
            menu_own_id = menu_obj.create(cursor, uid, {
                'name': name,
                'parent_id': res_own_id,
                'icon': 'STOCK_JUSTIFY_FILL',
                'action': 'ir.actions.act_window,%s' % act_own_id,
            }, context=context)

            model.write(
                {
                    'menu_id': menu_all_id,
                    'action_id': act_all_id,
                    'menu_own_id': menu_own_id,
                    'action_own_id': act_own_id
                }
            )

        return True

    def __sql(self, cursor, uid, ids, field_name, args, context=None):
        res = {}
        for model in self.browse(cursor, uid, ids, context):
            try:
                table = self.pool.get(model.model_id.model)
                exp = expression(eval(model.domain))
                exp.parse(cursor, uid, table, context)
                where, params = exp.to_sql()
                sql = 'SELECT %s.* FROM %s' % (
                    table._table, table._table
                )
                if where:
                    sql += ' WHERE %s' % where
                sql += ';'
                res[model.id] = sql % tuple(params)
            except:
                res[model.id] = _(u'**ERROR: Expressió no vàlida')
        return res

    _rec_name = 'local_name'

    _columns = {
        'local_name': fields.char('Aclaració', size=64, required=True),
        'template_name': fields.char('Plantilla de nom', size=64),
        'menu_name': fields.char(
            'Nom pel menú',
            size=256,
        ),
        'model_id': fields.many2one(
            'ir.model',
            'Model',
            required=True
        ),
        'active': fields.boolean('Actiu'),
        'menu_id': fields.many2one(
            'ir.ui.menu',
            'Menú totes',
            ondelete='restrict'
        ),
        'action_id': fields.many2one(
            'ir.actions.act_window',
            'Acció totes',
            ondelete='restrict'
        ),
        'menu_own_id': fields.many2one(
            'ir.ui.menu',
            'Menú usuari',
            ondelete='restrict'
        ),
        'action_own_id': fields.many2one(
            'ir.actions.act_window',
            'Acció usuari',
            ondelete='restrict'
        ),
        'domain': fields.text('Expressió de cerca'),
        'sql': fields.function(__sql, string='SQL', type='text', method=True),
    }

    _defaults = {
        'active': lambda *a: 1,
        'domain': lambda *a: '[]',
        'template_name': lambda *a: '{obj.name}'
    }

    _sql_constraints = [
        ('local_name_uniq', 'unique (local_name)',
         'Ja existeix un model amb aquest nom')
    ]

GiscedataRevisionsInternesModel()


class GiscedataRevisionsInternesValoracio(osv.osv):
    _name = 'giscedata.revisions.internes.valoracio'

    _columns = {
        'name': fields.char('Valoració', size=60, required=True),
        'active': fields.boolean('Active'),
        't_qty': fields.integer('Temps', required=True),
        't_type': fields.selection(
            [('hour', 'Hora'), ('day', 'Dia'),
             ('month', 'Mes'), ('year', 'Any')],
            'Unitat',
            required=True
        )
    }

    _defaults = {
        'active': lambda *a: 1,
        't_type': lambda *a: 'day',
    }

GiscedataRevisionsInternesValoracio()


class GiscedataRevisionsInternesNormativa(osv.osv):
    _name = 'giscedata.revisions.internes.normativa'

    def __sql(self, cursor, uid, ids, field_name, args, context=None):
        res = {}
        for normativa in self.browse(cursor, uid, ids, context):
            try:
                table = self.pool.get(normativa.model_id.model)
                exp = expression(
                    eval(normativa.model_domain) + eval(normativa.domain)
                )
                exp.parse(cursor, uid, table, context)
                where, params = exp.to_sql()
                sql = 'SELECT %s.* FROM %s' % (
                    table._table, table._table
                )
                if where:
                    sql += ' WHERE %s' % where
                sql += ';'
                res[normativa.id] = sql % tuple(params)
            except:
                res[normativa.id] = _(u'**ERROR: Expressió no vàlida')
        return res

    def reassignar_defectes(self, cursor, uid, ids, context=None):
        rev_obj = self.pool.get('giscedata.revisions.internes.revisio')
        if not context:
            context = {}
        for normativa in self.browse(cursor, uid, ids, context=context):
            # Busquem totes les revisions que afectan a aquest model i que
            # estan assignades
            search_params = [
                ('state', '=', 'assignada'),
                ('model', '=', normativa.model_id.id)
            ]
            rev_ids = rev_obj.search(
                cursor, uid, search_params, context=context)
            rev_obj.assignar(cursor, uid, rev_ids)
        return True

    _columns = {
        'name': fields.char('Normativa', size=50, required=True),
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'model_id': fields.many2one(
            'giscedata.revisions.internes.model',
            'Model',
            required=True
        ),
        'model_domain': fields.related(
            'model_id',
            'domain',
            string='Domini del model',
            readonly=True,
            type='text',
        ),
        'domain': fields.text('Expressió de cerca'),
        'sql': fields.function(__sql, string='SQL', type='text', method=True),
        'active': fields.boolean('Activa'),
        'items_ids': fields.one2many(
            'giscedata.revisions.internes.item',
            'normativa_id',
            'Items de la normativa',
            context={'active_test': False}
        )
    }

    _defaults = {
        'active': lambda *a: 1,
        'domain': lambda *a: '[]'
    }

GiscedataRevisionsInternesNormativa()


class GiscedataRevisionsInternesItem(osv.osv):
    _name = 'giscedata.revisions.internes.item'

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        for item in self.read(cursor, uid, ids, ['name', 'descripcio']):
            res += [(item['id'], '%s-%s' % (item['name'], item['descripcio']))]
        return res

    def _default_normativa_id(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('normativa_id', False)

    _columns = {
        'name': fields.char('Codi', size=50, required=True),
        'descripcio': fields.text('Descripció del defecte', required=True),
        'normativa_id': fields.many2one(
            'giscedata.revisions.internes.normativa',
            'Normativa',
            required=True,
            ondelete='cascade'
        ),
        'active': fields.boolean('Actiu'),
        'sequence': fields.integer('Ordre'),
        'type': fields.selection(
            ITEM_TYPES,
            'Tipus de camp',
            required=True
        )
    }

    _defaults = {
        'active': lambda *a: 1,
        'sequence': lambda *a: 10,
        'type': lambda *a: 'boolean',
        'normativa_id': _default_normativa_id
    }

    _order = 'sequence asc'

GiscedataRevisionsInternesItem()


class GiscedataRevisionsInternesRevisio(osv.osv):
    _name = 'giscedata.revisions.internes.revisio'

    def create(self, cursor, uid, vals, context=None):
        name = vals.get('name', False)
        if not name or len(name.split(',')) != 2:
            raise osv.except_osv(
                _(u'Error'),
                _(u'Aquesta referència per la revisió no és correcta')
            )
        model, model_id = name.split(',')
        obj = self.pool.get(model)
        model_id = int(model_id)
        res_id = obj.search(cursor, uid, [('id', '=', model_id)],
                            context={'active_test': False})
        if not res_id:
            raise osv.except_osv(
                _(u'Error'),
                _(u'Aquesta referència per la revisió no és correcta')
            )

        return super(GiscedataRevisionsInternesRevisio,
                     self).create(cursor, uid, vals, context)

    def copy(self, cursor, uid, id, default, context=None):
        if not default:
            default = {}
        vals = {
            'state': 'esborrany',
            'data_real': False,
            'data_tancament': False,
            'items_ids': []
        }
        vals.update(default)
        return super(GiscedataRevisionsInternesRevisio,
                     self).copy(cursor, uid, id, vals, context)

    def _objectes_revisions_selection(self, cursor, uid, context=None):
        if not context:
            context = {}
        ctx = context.copy()
        ctx['active_test'] = False
        model_obj = self.pool.get('giscedata.revisions.internes.model')
        search_params = []
        model = context.get('revisio_model', False)
        if model:
            search_params += [('id', '=', model)]
        models = model_obj.search(cursor, uid, search_params, context=ctx)
        res = []
        for model in model_obj.read(cursor, uid, models, ['model', 'local_name'],
                                    context=ctx):
            res += [(model['model'], model['local_name'])]
        return res

    def _buscar_defectes(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        normativa_obj = self.pool.get('giscedata.revisions.internes.normativa')
        defecte_obj = self.pool.get('giscedata.revisions.internes.defecte')
        ctx = context.copy()
        ctx['active_test'] = False
        for revisio in self.read(cursor, uid, ids, ['name', 'data'], context):
            items_ids = []
            model, model_id = revisio['name'].split(',')
            obj = self.pool.get(model)
            model_id = int(model_id)
            # Busquem totes les normatives que encaixen amb aquest model
            normatives = normativa_obj.search(cursor, uid, [
                ('model_id.model', '=', model),
                '|', ('data_inici', '<=', revisio['data']),
                     ('data_inici', '=', False),
                '|', ('data_final', '>=', revisio['data']),
                     ('data_final', '=', False)
            ])
            # Per a cada normativa hauriem d'evaular el domain
            fields_to_read = ['domain', 'items_ids', 'model_domain']
            for normativa in normativa_obj.read(cursor, uid, normatives,
                                                fields_to_read):
                domain = eval(normativa['domain'])
                domain += eval(normativa['model_domain'])
                domain += [('id', '=', model_id)]
                if obj.search_count(cursor, uid, domain, context=ctx):
                    for item in normativa['items_ids']:
                        items_ids.append(defecte_obj.create(cursor, uid, {
                            'revisio_id': revisio['id'],
                            'item_id': item
                        }))
            defectes = defecte_obj.read(cursor, uid, items_ids, ['name',
                                                                 'descripcio'])
            items_str = '\n'.join([
                ('{d[name]: <10} {d[descripcio]}'.format(d=d)) for d in defectes
            ])
            self.write(cursor, uid, [revisio['id']], {'items_str': items_str})

        return True

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        """
        Patches the view to add the domain

        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param view_id: View id
        :type view_type: int
        :param view_type: Type of view (form or list)
        :type view_id: str
        :param context: OpenERP context
        :type context: dict
        :param toolbar:
        :return: View
        :rtype: str
        """

        if context is None:
            context = {}
        result = super(GiscedataRevisionsInternesRevisio, self).fields_view_get(
            cr, uid, view_id, view_type, context, toolbar
        )
        model_id = context.get('revisio_model')
        if view_type == 'form' and model_id:
            rev_model_obj = self.pool.get('giscedata.revisions.internes.model')
            model = rev_model_obj.read(cr, uid, model_id, ['domain'])
            if eval(model['domain']):
                arch = result['arch']
                if isinstance(arch, unicode):
                    arch = arch.encode('utf-8')
                xml = etree.fromstring(arch)
                node = xml.xpath("//field[@name='name']")[0]
                node.attrib["domain"] = model['domain']
                result['arch'] = etree.tostring(xml)
        return result


    def _eliminar_defectes(self, cursor, uid, ids, context=None):
        defecte_obj = self.pool.get('giscedata.revisions.internes.defecte')
        for revisio in self.read(cursor, uid, ids, ['items_ids'], context):
            defecte_obj.unlink(cursor, uid, revisio['items_ids'])
        self.write(cursor, uid, ids, {'items_str': False})
        return True

    def obrir(self, cursor, uid, ids, context=None):
        """Obre la revisió.
        """
        self.write(cursor, uid, ids, {
            'state': 'oberta',
            'data_real': datetime.now().strftime('%Y-%m-%d %H:%M:00'),
            'data_tancament': False,
        })
        return True

    def assignar(self, cursor, uid, ids, context=None):
        """Assigna els defectes a la revisó
        """
        self._eliminar_defectes(cursor, uid, ids, context)
        self._buscar_defectes(cursor, uid, ids, context)
        self.write(cursor, uid, ids, {'state': 'assignada'})
        return True

    def tancar(self, cursor, uid, ids, context=None):
        """Tanca una revisió.
        """
        # Comprovem que no hi ha cap defecte a "No comprovat"
        for revisio in self.browse(cursor, uid, ids, context):
            items = 0
            if not revisio.ignorar_no_revisats:
                for defecte in revisio.items_ids:
                    if defecte.state == 'C' and defecte.item_id.active:
                        items += 1
            if items:
                raise osv.except_osv(
                    'Error',
                    _(u"No es pot donar la revisió per tancada, queden "
                      u"%s ítems que no s'han comprovat.") % items
                )
        self.write(cursor, uid, ids, {
            'state': 'tancada',
            'data_tancament': datetime.now().strftime('%Y-%m-%d %H:%M:00'),
            'tancada_per': uid
        })
        return True

    def esborrany(self, cursor, uid, ids, context=None):
        """La passa a esborrany.
        """
        self._eliminar_defectes(cursor, uid, ids, context)
        self.write(cursor, uid, ids, {'state': 'esborrany'})
        return True

    def crear_planificacio(self, cursor, uid, ids, context=None):
        """Crearà la revisió futura, segons la planficació establerta si no
        existeix.
        """
        if not context:
            context = {}
        fields_to_read = ['data', 'periodicitat_qty', 'periodicitat_type',
                          'name']
        creats = []
        error = []
        for revisio in self.read(cursor, uid, ids, fields_to_read):
            if (not revisio['periodicitat_qty']
                    or not revisio['periodicitat_type']):
                error.append(
                    (revisio['id'], _("No té ben configurada la periodicitat"))
                )
                continue
            # Calculem la data futura
            revs = self.search(cursor, uid, [
                ('id', '!=', revisio['id']),
                ('data', '>', revisio['data']),
                ('name', '=', revisio['name'])
            ])
            if revs:
                error.append(
                    (
                        revisio['id'],
                        _("Ja se li ha generat una revisió planificada")
                    )
                )
                continue
            inteval = '%s %s' % (revisio['periodicitat_qty'],
                                 revisio['periodicitat_type'])
            cursor.execute("select %s::timestamp + %s::interval",
                           (revisio['data'], inteval))
            next_date = cursor.fetchall()[0]
            creats.append(self.copy(cursor, uid, revisio['id'],
                                    {'data': next_date}))
        if not creats:
            val = ''
            for rev, err in error:
                val += _("\n Revisió '{0}': '{1}'").format(rev, err)
            raise osv.except_osv(
                _("No s'han generat revisions planificades"), val
            )
        return creats

    def _fnc_name_str(self, cursor, uid, ids, field_name, arg, context=None):
        """Field function name_str.
        """
        res = {}.fromkeys(ids, False)
        for revisio in self.browse(cursor, uid, ids):
            if revisio.name and len(revisio.name.split(',')) == 2:
                model, obj_id = revisio.name.split(',')
                obj = self.pool.get(model).browse(cursor, uid, int(obj_id))
                if obj_id == '0':
                    # Em principi aixó no es donara ja que els objectius de les
                    # revision no es solen eliminar, si no que pasen a no
                    # actius
                    res[revisio.id] = _("S'ha eliminat l'element de la revisió")
                else:
                    res[revisio.id] = revisio.model.template_name.format(obj=obj)
        return res

    def _defectes_reparats(self, cursor, uid, ids, field_name, args,
                           context=None):
        def_obj = self.pool.get('giscedata.revisions.internes.defecte')
        res = dict.fromkeys(ids, 1)
        for revisio in self.read(cursor, uid, ids, ['items_ids', 'state'],
                                 context):
            if revisio['state'] != 'tancada':
                continue
            for defecte in def_obj.read(cursor, uid, revisio['items_ids'],
                                        ['state', 'reparat']):
                if defecte['state'] == 'B':
                    if not defecte['reparat']:
                        res[revisio['id']] = 0
                        break
        return res

    def _num_defectes(self, cursor, uid, ids, field_name, args, context=None):
        def_obj = self.pool.get('giscedata.revisions.internes.defecte')
        res = dict.fromkeys(ids, '0/0')
        for revisio in self.read(cursor, uid, ids, ['items_ids', 'state'],
                                 context):
            reparats = 0
            total = 0
            if revisio['state'] != 'tancada':
                continue
            for defecte in def_obj.read(cursor, uid, revisio['items_ids'],
                                        ['state', 'reparat']):
                if defecte['state'] == 'B':
                    total += 1
                    if defecte['reparat']:
                        reparats += 1
            res[revisio['id']] = '%s/%s' % (reparats, total)
        return res

    def _get_revisions_ids(self, cursor, uid, ids, context=None):
        r_ids = []
        for defecte in self.read(cursor, uid, ids, ['revisio_id']):
            r_ids.append(defecte['revisio_id'][0])
        return r_ids

    def _fnct_name_str_search(self, cr, uid, obj, name, args, context=None):
        """
        Obtains the domain that determines which revisions show in the tree view
        when using the filter by name_str
        :param cr:
        :param uid: <res.users> id
        :type uid: long
        :param obj: <giscedata.revisions.internes.revisio> object.
        :type obj: OpenERP object.
        :param name: ?
        :param args: OpenERP domain constructed by the filter
        :type args: [()]
        :param context: OpenERP context.
        :type context: {}
        :return:
        """
        ids = []
        revisio_model_id = context['revisio_model']
        dmn = [('model', '=', revisio_model_id)]
        revisio_f = ['name_str']
        revisio_ids = self.search(cr, uid, dmn, context=context)
        revisio_vs = self.read(cr, uid, revisio_ids, revisio_f, context=context)

        search_value = args[0][2].lower()
        for revisio_v in revisio_vs:
            revisio_id = revisio_v['id']
            if search_value in revisio_v['name_str'].lower():
                ids.append(revisio_id)

        return [('id', 'in', ids)]

    _columns = {
        'name': fields.reference(
            'Model',
            _objectes_revisions_selection,
            required=True,
            size=250,
            readonly=True,
            states={
                'esborrany': [('readonly', False)]
            }
        ),
        'name_str': fields.function(
            _fnc_name_str,
            fnct_search=_fnct_name_str_search,
            string='A revisar',
            type='char',
            size=256,
            method=True
        ),
        'data': fields.datetime(
            'Data programada',
            readonly=True,
            required=True,
            states={
                'esborrany': [('readonly', False)]
            }
        ),
        'data_real': fields.datetime(
            'Data real',
            readonly=True,
            states={
                'oberta': [('readonly', False), ('required', True)]
            }
        ),
        'data_tancament': fields.datetime(
            'Data tancament',
            readonly=True
        ),
        'periodicitat_qty': fields.integer('Periodicitat quantitat'),
        'periodicitat_type': fields.selection(
            [('day', 'Dies'), ('month', 'Mesos'), ('year', 'Anys')],
            'Periodicitat tipus'
        ),
        'state': fields.selection(
            [('esborrany', 'Esborrany'),
             ('assignada', 'Assignada'),
             ('oberta', 'Oberta'),
             ('tancada', 'Tancada')],
            'Estat',
            required=True,
            readonly=True
        ),
        'operari': fields.many2one(
            'res.users',
            'Tècnic',
            required=True,
            ondelete='restrict',
            readonly=True,
            states={
                'esborrany': [('readonly', False)]
            }
        ),
        'tancada_per': fields.many2one(
            'res.users',
            'Tancada per',
            ondelete='restrict',
            readonly=True
        ),
        'empresa': fields.many2one(
            'res.partner',
            'Empresa',
            required=True,
            ondelete='restrict',
            readonly=True,
            states={
                'esborrany': [('readonly', False)]
            }
        ),
        'observacions': fields.text('Observacions'),
        'items_ids': fields.one2many(
            'giscedata.revisions.internes.defecte',
            'revisio_id',
            'Defectes',
            readonly=True,
            states={
                'oberta': [('readonly', False)]
            }
        ),
        'items_str': fields.text(
            'Items a revisar',
            readonly=True,
        ),
        'defectes_reparats': fields.function(
            _defectes_reparats,
            string='Defectes Reparats',
            type='boolean',
            method=True,
            store={
                'giscedata.revisions.internes.defecte': (
                    _get_revisions_ids, ['state', 'reparat'], 10
                ),
                'giscedata.revisions.internes.revisio': (
                    lambda *a: a[3], ['state'], 10
                )
            }
        ),
        'num_defectes': fields.function(
            _num_defectes,
            string='Núm. defectes reparats',
            type='char',
            size=8,
            method=True,
            store={
                'giscedata.revisions.internes.defecte': (
                    _get_revisions_ids, ['state', 'reparat'], 10
                ),
                'giscedata.revisions.internes.revisio': (
                    lambda *a: a[3], ['state'], 10
                )
            }
        ),
        'model': fields.many2one(
            'giscedata.revisions.internes.model',
            'Model',
            ondelete='restrict',
            required=True
        ),
        'ignorar_no_revisats': fields.boolean('Ignorar items no revisats')

    }

    _defaults = {
        'data': lambda *a: datetime.now().strftime('%Y-%m-%d %H:00:00'),
        'state': lambda *a: 'esborrany',
        'ignorar_no_revisats': lambda *a: 0
    }

GiscedataRevisionsInternesRevisio()


class GiscedataRevisionsInternesDefecte(osv.osv):
    _name = 'giscedata.revisions.internes.defecte'

    def _data_limit_reparacio(self, cursor, uid, ids, field_name, args,
                              context=None):
        """Fem una consulta SQL per millorar el rendiment del camp funció.
        """
        cursor.execute("""SELECT
d.id, d.data_comprovacio + (v.t_qty || ' ' || v.t_type)::interval
  from giscedata_revisions_internes_defecte d
  left join giscedata_revisions_internes_valoracio v on (d.valoracio = v.id)
  where d.id in %s""", (tuple(ids), ))
        return dict(cursor.fetchall())

    def _get_defectes(self, cursor, uid, ids, context=None):
        return ids

    def calc_data_limit(self, cursor, uid, ids, data_comprovacio, valoracio,
                        context=None):
        if not context:
            context = {}
        res = {'value': {}}
        if data_comprovacio and valoracio:
            cursor.execute("""SELECT %s::timestamp
              + (v.t_qty || ' ' || v.t_type)::interval
              from giscedata_revisions_internes_valoracio v where v.id = %s""",
                           (data_comprovacio, valoracio))
            res['value']['data_limit_reparacio'] = cursor.fetchone()[0]
        return res

    _columns = {
        'name': fields.related(
            'item_id',
            'name',
            string='Codi',
            type='char',
            size=50,
            readonly=True,
        ),
        'descripcio': fields.related(
            'item_id',
            'descripcio',
            string='Descripció',
            type='text',
            readonly=True,
        ),
        'item_id': fields.many2one(
            'giscedata.revisions.internes.item',
            'Item',
            required=True,
            readonly=True,
            ondelete='restrict'
        ),
        'normativa_id': fields.related(
            'item_id',
            'normativa_id',
            string='Normativa',
            type='many2one',
            relation='giscedata.revisions.internes.normativa',
            readonly=True,
        ),
        'type': fields.related(
            'item_id',
            'type',
            string='Tipus',
            type='selection',
            selection=ITEM_TYPES,
            readonly=True,
        ),
        'valor': fields.float('Valor', digits=(16, 3)),
        'dades': fields.text('Dades'),
        'observacions': fields.text('Observacions'),
        'observacions_rep': fields.text('Obseracions de reparació'),
        'data_comprovacio': fields.datetime('Data de comprovació'),
        'data_reparacio': fields.datetime('Data de reparació'),
        'data_limit_reparacio': fields.function(
            _data_limit_reparacio,
            type='datetime',
            string='Data límit reparació',
            method=True,
            store={
                'giscedata.revisions.internes.defecte': (
                    _get_defectes, ['valoracio'], 10
                )
            }
        ),
        'valoracio': fields.many2one(
            'giscedata.revisions.internes.valoracio',
            'Valoració',
            ondelete='restrict'
        ),
        'reparat': fields.boolean('Reparat'),
        'state': fields.selection(
            DEFECTE_STATES,
            'Estat',
            required=True
        ),
        'revisio_id': fields.many2one(
            'giscedata.revisions.internes.revisio',
            'Revisio',
            required=True,
            ondelete='cascade'
        ),
        'sequence': fields.related(
            'item_id',
            'sequence',
            string='Ordre',
            type='integer',
            readonly=True,
            store=True,
        ),
    }

    _defaults = {
        'reparat': lambda *a: 0,
        'state': lambda *a: 'C'
    }

    _order = 'sequence asc'

GiscedataRevisionsInternesDefecte()


def _ff_manteniments_field(self, cursor, uid, ids, field_name, arg,
                           context=None):
    """
        Function that supplies the data ids into the view

    :param self: Class
    :param cursor: Database cursor
    :param uid: User id
    :param ids: Ids of elements required
    :param field_name:
    :param arg:
    :param context: OpenERP Context
    :return: Dict {id:[element id]}
    """
    if not context:
        context = {}
    res = dict.fromkeys(ids, False)
    att_obj = self.pool.get('giscedata.revisions.internes.revisio')
    for res_id in ids:
        res[res_id] = att_obj.search(cursor, uid, [
            ('name', '=', self._name+','+str(res_id))
        ])
    return res


manteniments_field = fields.function(
    _ff_manteniments_field, method=True, string='Manteniments',
    type='one2many', relation='giscedata.revisions.internes.revisio'
)


def fields_view_get(self, cursor, user, view_id=None, view_type='form',
                    context=None, toolbar=False):
    """
        Function that gives the field description of the view

    :param self: Class
    :param cursor: Database cursor
    :param user: User id
    :param view_id: View identifier
    :param view_type: View type
    :param context: OpenERP Context
    :param toolbar: Boolen , True if if include contextual actions
    :return:
    """

    res = self.fields_view_get_unpatched_m(cursor, user, view_id, view_type,
                                         context, toolbar)
    res['fields'].update(self.fields_get(cursor, user, ['_manteniments_field']))
    xml = etree.fromstring(res['arch'])
    notebook = xml.xpath('//notebook')
    if notebook:
        string = _('Manteniment')
        notebook[0].append(etree.fromstring("""
            <page string="{0}">
                <field name="_manteniments_field" colspan="4" nolabel="1"/>
            </page>
        """.format(string)))
        res['arch'] = etree.tostring(xml)

    return res


old_register_all = report.interface.register_all


def unpatch_object(obj):
    """
        Function that unpatches the model

    :param obj: Mddoel object
    :return: True
    """
    if '_manteniments_field' in obj._columns:
        del obj._columns['_manteniments_field']
    if hasattr(obj, 'fields_view_get_unpatched'):
        obj.fields_view_get = obj.fields_view_get_unpatched_m
    return True


def patch_object(obj):
    """
        Functiont that patches the model

    :param obj: Model object
    :return: True
    """
    # Add the column
    if '_manteniments_field' not in obj._columns:
        obj._columns['_manteniments_field'] = manteniments_field
        # Monkeypatch fields_view_get
        method_obj = getattr(obj, 'fields_view_get_unpatched_m', None)
        if not callable(method_obj):
            obj.fields_view_get_unpatched_m = obj.fields_view_get
        obj.fields_view_get = types.MethodType(
            fields_view_get, obj, osv.osv
        )
    return True


def manteniments_tab_register(db):
    """
        Function that registers the tab
    :param db: Database conection
    :return:
    """
    value = old_register_all(db)
    cursor = db.cursor()
    try:
        patch_objects(cursor)
        cursor.commit()
    except:
        cursor.rollback()
    finally:
        cursor.close()
    return value


def patch_objects(cursor):
    pool = pooler.get_pool(cursor.dbname)

    model_obj = pool.get('ir.model')
    sql_model = ''' SELECT distinct(model_id)
      FROM giscedata_revisions_internes_model;'''
    cursor.execute(sql_model)
    models = cursor.fetchall()
    for model in model_obj.read(cursor, 1, models, ['model']):
        obj = pool.get(model['model'])
        if obj:
            patch_object(obj)


def patch_register_all():
    logger = logging.getLogger('openerp.addons.giscedata_revisions_internes')
    logger.info('Patching report.interface.register_all')
    report.interface.register_all = manteniments_tab_register
