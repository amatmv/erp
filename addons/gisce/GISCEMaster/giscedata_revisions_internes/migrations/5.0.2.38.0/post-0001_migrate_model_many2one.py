# -*- coding: utf-8 -*-
import pooler

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    model_obj = pool.get('giscedata.revisions.internes.model')
    normativa_obj = pool.get('giscedata.revisions.internes.normativa')
    cursor.execute("SELECT * from giscedata_revisions_internes_normativa")
    for normativa in cursor.dictfetchall():
        model_id = model_obj.search(cursor, uid, [
            ('model', '=', normativa['model'])
        ])
        if model_id:
            normativa_obj.write(cursor, uid, [normativa['id']], {
                'model_id': model_id[0]
            })
    cursor.execute("ALTER TABLE giscedata_revisions_internes_normativa "
                   "DROP COLUMN model")