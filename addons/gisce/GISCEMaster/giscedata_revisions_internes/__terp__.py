# -*- coding: utf-8 -*-
{
    "name": "Revisions internes",
    "description": """Revisions
   * Possibilitat de fer revisions per qualsevol element de 'ERP.
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Revisions",
    "depends":[
        "base",
	"c2c_webkit_report"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/giscedata_revisions_internes_security.xml",
        "giscedata_revisions_internes_view.xml",
        "wizard/wizard_planificacio_view.xml",
        "security/ir.model.access.csv",
	    "giscedata_revisions_internes_report.xml"
    ],
    "active": False,
    "installable": True
}
