# -*- coding: utf-8 -*-

from osv import osv
from tools.translate import _


class WizardPlanificacio(osv.osv_memory):
    _name = 'wizard.planificacio'

    def crear(self, cursor, uid, ids, context=None):
        if not context:
            context = None
        revisions_ids = context.get('active_ids', [])
        if not revisions_ids:
            raise osv.except_osv(
                'Error',
                _("No s'han detectat revisions a planificar")
            )
        rev_obj = self.pool.get('giscedata.revisions.internes.revisio')
        revs = rev_obj.crear_planificacio(cursor, uid, revisions_ids)
        return {
            'domain': "[('id','in', %s)]" % str(revs),
            'name': _('Revisions planificades creades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.revisions.internes.revisio',
            'type': 'ir.actions.act_window'
        }
WizardPlanificacio()