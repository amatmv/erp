# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardPerfilar(osv.osv_memory):
    _name = 'wizard.perfilar'

    def _default_info(self, cursor, uid, context=None):
        raise NotImplementedError

    def action_perfilar(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wiz_obj = self.browse(cursor, uid, ids[0], context=context)
        invoice_ids = context.get('active_ids', [])
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        msg = []
        # Check if 'check_profilable' must be checked
        skip_check_profilable = wiz_obj['skip_check']
        for x in invoice_ids:
            if skip_check_profilable:
                invoice_obj.encua_perfilacio(cursor, uid, x, context=context)
                msg.append("S'ha encuat la factura: {0} per Perfilar".format(x))
            else:
                try:
                    if invoice_obj.check_profilable(cursor, uid, x, context=context):
                        invoice_obj.encua_perfilacio(cursor, uid, x, context=context)
                        msg.append("S'ha encuat la factura: {0} per Perfilar".format(x))
                    else:
                        msg.append("La factura: {0} no s'ha pogut encuar per Perfilar".format(x))
                except Exception:
                    msg.append("La factura: {0} no s'ha pogut encuar per Perfilar".format(x))

        wiz_obj.write({'state': 'end', 'info': '\n'.join(msg)})

    _columns = {
        'state': fields.selection(
            [('init', 'Inici'), ('end', 'Fi')], 'State'
        ),
        'info': fields.text('Perfilació', readonly=True),
        'skip_check': fields.boolean(_("Saltar validació"),
                                     help=_('Si es marca aquesta casella, '
                                            'es forçarà la perfilació de les factures no perfilables.')),
    }

    _defaults = {
        'state': lambda *x: 'init',
        'info': _default_info,
        'skip_check': lambda *a: False,
    }


WizardPerfilar()
