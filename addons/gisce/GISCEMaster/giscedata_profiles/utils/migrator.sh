#!/bin/bash

# Assert DB and TABLE args
if [[ -z $1 || -z $2 ]]; then
    echo "Remember to pass all needed arguments: $0 DB TABLE";
    exit 1;
fi

DB=$1
TABLE=$2
MONGO_HOST=$3
PSQL_HOST=$4
BATCH=$5

DI="2017`echo $BATCH`0101"
DF="2017`echo $BATCH`3200"

DESTINATION_TABLE="giscedata_old_profiles_profile"

TMP_FILE=/tmp/$DB"_"$TABLE.json

MONGOCLIENT="/usr/bin/mongo --host `echo $MONGO_HOST`"
MONGOIMPORT=/usr/bin/mongoimport
PSQL="/usr/bin/psql --host `echo $PSQL_HOST`"
WC=/usr/bin/wc
SED=/bin/sed
AWK=/usr/bin/awk

echo "Migrating Batch $DB/$TABLE - batch: $BATCH to MongoDB..."

echo "1) Dumping psql data"
dump_result=`$PSQL --dbname $DB -c "copy(SELECT row_to_json(to_migrate) FROM $TABLE as to_migrate WHERE name>='$DI' AND name<='$DF') to '"$TMP_FILE"'"`

if [[ $? -eq 1 ]];
    then echo "  [!] Error dumping data. Review provided arguments db:$DB table:$TABLE and write access to tmp_file:$TMP_FILE" ;
    exit 1;
fi

expected_lines=`echo $dump_result | $AWK '{print $2}'`
echo "  Exported #$expected_lines rows"
echo


echo "2) Validate export"
sql_result=`$PSQL --dbname $DB -t -c "select count(1) from $TABLE"`
if [[ $? -eq 1 ]];
    then echo "  [!] Error validating $DB/$TABLE expected count";
    exit 1;
fi
sql_count=`echo $sql_result | sed 's/ //g'`
file_count=`$WC -l $TMP_FILE | $AWK '{print $1}'`

if [[ $sql_count -eq $file_count ]]; then
    echo "  Export performed succesfully for $sql_count rows"
else
    echo "  [!] Error exporting $DB/$TABLE, real rows #$sql_count vs dumped rows #$file_count"
    exit 1
fi

latest_ID=`$PSQL --dbname $DB -t -c "select id from $TABLE order by id desc limit 1"`
echo


echo "3) Adapt data"
echo $TMP_FILE
sed_result=`$SED -i 's/\\n//g; s/create_date":"/create_date":ISODate("/g; s/","write_date/Z"),"write_date/g' $TMP_FILE`
if [[ $? -eq 1 ]];
    then echo "  [!] Error adapting data. Review vigence of applied patterns for current schema" ;
    exit 1;
fi
echo


echo "4) Import to MongoDB"
# Preventive delete of existing data, and index definition over an empty collection
preventive_drop=`$MONGOCLIENT $DB --eval "db.$DESTINATION_TABLE.drop()"`
preventive_counter_delete=`$MONGOCLIENT $DB --eval "db.counters.remove({ '_id' : '"$DESTINATION_TABLE"'})"`
index_creation=`$MONGOCLIENT $DB --eval "db.$DESTINATION_TABLE.createIndexes([{"cups":1}, {"name":1}, {"cups":1, "name":1}])"`
# If createIndexes do not exist on destination MongoDB server version, use createIndex for each index:
## index_creation=`$MONGOCLIENT $DB --eval "db.$DESTINATION_TABLE.createIndex([{"cups":1})"`
## index_creation=`$MONGOCLIENT $DB --eval "db.$DESTINATION_TABLE.createIndex([{"name":1})"`
## index_creation=`$MONGOCLIENT $DB --eval "db.$DESTINATION_TABLE.createIndex([{"cups":1, "name":1}])"`

import=`$MONGOIMPORT --db $DB --collection $DESTINATION_TABLE --file $TMP_FILE`
if [[ $? -eq 1 ]];
    then echo "  [!] Error importing data. Ensure that MongoDB is up and the current user have enought grants. Also review available space :)";
    exit 1;
fi
echo


echo "5) Validate MongoDB import"
mongo_count=`$MONGOCLIENT --db $DB --eval "db.$DESTINATION_TABLE.count()" | tail -n1`

if [[ $sql_count -eq $mongo_count ]]; then
    echo "  Import at MongoDB performed succesfully for $mongo_count rows"
else
    echo "  [!] Error importing $DB/$TABLE, real rows #$sql_count vs import rows #$mongo_count"
    exit 1
fi
echo


echo "6) Add the ID counter for the migrated table!"
latest_ID=$(($latest_ID+1))
counter_creation=`$MONGOCLIENT --db $DB --eval "db.counters.insert({ '_id' : '"$DESTINATION_TABLE"', 'counter' : $latest_ID })"`

if [[ $? -eq 1 || $counter_creation == *"duplicate key"* ]];
    then echo "  [!] Error defining the $DESTINATION_TABLE counter to #$latest_ID. Review if counter for $DESTINATION_TABLE already exists at $DB/counters collection.";
    exit 1;
fi
echo


echo
echo "Migration done!!!"
