# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from collections import Counter
from datetime import timedelta, datetime
import random
import re
from sets import Set
import click
from erppeek import Client


def fetch_hours_from_invoice_dates(start, end):
    _start = (datetime.strptime(start + " 1", '%Y-%m-%d %H')).strftime('%Y%m%d%H')
    _end = (datetime.strptime(end  + " 0", '%Y-%m-%d %H') + timedelta(days=1)).strftime('%Y%m%d%H')
    return _start, _end



def profile_invoice(c, invoice):
    fact_obj = c.model('giscedata.facturacio.factura')
    profile_obj = c.model('giscedata.profiles.profile')
    perfil_obj = c.model('giscedata.perfils.perfil')

    # Debug current invoice
    the_invoice = fact_obj.read(invoice)

    # Profile invoice
    result = fact_obj.perfilar(invoice)
    if not result:
        print ("Profile invoice '{}' do not work".format(invoice))
        return False

    # Validate profiled elements
    search_params = [
        ('factura', '=', the_invoice['number']),
    ]
    profile_ids = profile_obj.search(search_params, limit=0, order="timestamp")

    profiled_total = 0
    profiled_energy = []
    profiled_hours = []
    for a_profile in profile_obj.read(profile_ids, ['lectura', 'timestamp']):
        profiled_total += a_profile['lectura']
        profiled_energy.append(a_profile['lectura'])
        profiled_hours.append(a_profile['timestamp'])

    # [!] Validate profiled total energy
    assert profiled_total == int(the_invoice['energia_kwh']), "Profiled total energy '{}' must match the invoiced energy '{}'".format(profiled_total, int(the_invoice['energia_kwh']))

    # Validate cross old profiled elements
    start_hour, end_hour = fetch_hours_from_invoice_dates(the_invoice['data_inici'], the_invoice['data_final'])
    search_params = [
        ('cups', '=', the_invoice['cups_id'][1]),
        ('name', '>=', start_hour),
        ('name', '<=', end_hour),
    ]
    old_profiled_ids = perfil_obj.search(search_params, order="name")

    assert old_profiled_ids, "There are no OLD profiles to review"
    old_profiled_total = 0
    old_profiled_energy = []
    old_profiled_hours = []
    for a_profile in perfil_obj.read(old_profiled_ids, ['aprox', 'name']):
        old_profiled_total += a_profile['aprox']
        old_profiled_energy.append(a_profile['aprox'])
        old_profiled_hours.append(a_profile['name'])

    print
    print (profiled_hours[0], profiled_hours[-1])
    print (old_profiled_hours[0], old_profiled_hours[-1])
    print

    # [!] Validate match of hours
    assert len(profiled_energy) == len(old_profiled_energy), "Number of hours '{}' must match the old ones '{}'".format(len(profiled_energy), len(old_profiled_energy))

    # [!] Validate hour to hour match
    try:
        assert profiled_energy == old_profiled_energy, "OLD Profiled energy '{}' must match the invoiced energy '{}'".format(profiled_energy, old_profiled_energy)
    except:
        # Validate that the maximum difference for each hour is +-1kWh
        for idx, profiled in enumerate(profiled_energy):
            assert -1 <= (profiled - old_profiled_energy[idx]) <= 1, "Hour '{}' OLD Profiled energy '{}' must match the invoiced energy '{}'".format(idx, profiled, old_profiled_energy[idx])

    # [!] Validate OLD profiled total energy
    assert old_profiled_total == int(the_invoice['energia_kwh']), "OLD Profiled total energy '{}' must match the invoiced energy '{}'".format(old_profiled_total, int(the_invoice['energia_kwh']))


@click.command()
@click.option('--protocol', default='http', help='ERP host')
@click.option('--host', default='localhost', help='ERP host')
@click.option('--database', default='database', help='ERP host')
@click.option('--port', default='8069', help='ERP port', type=click.INT)
@click.option('--user', default=None, help='ERP user')
@click.option('--password', default=None, help='ERP password')
@click.option('--invoices', default=None, help='Invoices to profile in a comma-separated string', type=click.STRING)
@click.option('--tariff', default=None, help='Type of tariff where to lookup N (limit) invoices', type=click.INT)
@click.option('--limit', default=20, help='Number of invoices to profile (if invoice not provided)', type=click.INT)
def main(protocol, host, port, user, password, database, invoices, tariff, limit):
    config = {
        'connection_url': "{}://{}:{}".format(protocol, host, port),
        'db': database,
        'user': user,
        'password': password,
    }

    with Client(config['connection_url'], config['db'], config['user'], config['password']) as c:

        # If invoices are not detailed, random it!
        if not invoices:
            fact_obj = c.model('giscedata.facturacio.factura')

            # Extract valid invoices
            search_params = [
                ("data_inici", "!=", None),
                ("data_final", "!=", None),
                ("data_inici", "<", "2017-09-28"),
                ("energia_kwh", "!=", None),
                ("polissa_tg", "!=", "1"),
            ]

            if tariff:
                search_params.append(("tarifa_acces_id", "=", tariff))

            all_invoices = fact_obj.search(search_params)
            invoices = random.sample(all_invoices, limit)

        else:
            invoices = invoices.split(",")

        for invoice in list(invoices):
            print ("Processing {}".format(invoice))
            try:
                profile_invoice(c, int(invoice))
            except AssertionError as e:
                print ("[ERROR!] {}: {}".format(invoice, e))
                print ("  select tarifa_acces_id, data_inici, data_final, energia_kwh, cups_id from giscedata_facturacio_factura  where id = {};".format(invoice))
                print ('  db.giscedata_profiles_profile.aggregate( { $match: { factura: ' + str(invoice) + '} } , { $group: { _id: {factura: "$factura" } , sum: {$sum: "$lectura"} } })')
            except:
                print ("[ERROR!] Unkwnown error processing {}".format(invoice))
                print ("  select tarifa_acces_id, data_inici, data_final, energia_kwh, cups_id from giscedata_facturacio_factura  where id = {};".format(invoice))
                print ('  db.giscedata_profiles_profile.aggregate( { $match: { factura: ' + str(invoice) + '} } , { $group: { _id: {factura: "$factura" } , sum: {$sum: "$lectura"} } })')

if __name__ == '__main__':
    main()


# Extract one invoice by tariff
"""
WITH summary AS (
    SELECT factura.id,
           factura.cups_id,
           factura.data_inici,
           factura.data_final,
           tarifa.name,
           factura.tarifa_acces_id,
           ROW_NUMBER() OVER(PARTITION BY factura.tarifa_acces_id
                                 ORDER BY factura.tarifa_acces_id DESC) AS rk
      FROM giscedata_facturacio_factura factura
        INNER JOIN giscedata_polissa_tarifa tarifa ON (tarifa.id = factura.tarifa_acces_id)
      WHERE factura.data_inici IS NOT NULL
        AND factura.data_final IS NOT NULL
        AND factura.data_final <= current_date - integer '90'
    )
SELECT s.*
  FROM summary s
 WHERE s.rk = 1


   id   | cups_id | data_inici | data_final |  name   | tarifa_acces_id | VALIDATED?
--------+---------+------------+------------+---------+-----------------+----
 154395 |   59873 | 2017-11-01 | 2017-11-30 | RE12    |              30 |  1
  91152 |   28400 | 2017-07-01 | 2017-07-31 | 6.1B    |              20 |  1
  85963 |   22590 | 2017-07-01 | 2017-07-31 | 6.1A    |              19 |  1
  92336 |   25506 | 2017-07-01 | 2017-07-31 | 3.1A LB |              14 |  1
  92424 |   32769 | 2017-07-01 | 2017-07-24 | 3.1A    |               6 |  OK
  92461 |   22914 | 2017-07-01 | 2017-07-31 | 3.0A    |               5 |  OK
  84081 |   20948 | 2017-07-01 | 2017-07-31 | 2.1DHA  |               4 |  OK
  92264 |   23170 | 2017-07-01 | 2017-07-31 | 2.0DHA  |               3 |  OK
  85141 |   35255 | 2017-07-01 | 2017-07-31 | 2.1A    |               2 |  OK
  92446 |   22422 | 2017-07-01 | 2017-07-31 | 2.0A    |               1 |  OK
(10 rows)

"""
