# -*- coding: utf-8 -*-
from mongodb_backend import osv_mongodb
import utils as facturacio_utils
from datetime import datetime, timedelta
from enerdata.profiles import Dragger
from osv import osv, fields


MAGNITUDS = {'AE': 'A', 'R1': 'R'}

TIPOS = [('1', 'Tipo 1'),
         ('2', 'Tipo 2'),
         ('3', 'Tipo 3'),
         ('4', 'Tipo 4'),
         ('5', 'Tipo 5')]

MEASURE_TYPE_FROM_PROFILE_TO_REPORT = {
    'AE': 'active_input',
    'R1': 'reactive_quadrant1',
    'R4': 'reactive_quadrant4'
}
MEASURE_TYPE_FROM_TG_PROFILE_TO_REPORT = {
    'ai': 'active_input',
    'r1': 'reactive_quadrant1',
    'r4': 'reactive_quadrant4'
}

INDICADOR_MESURA = {
    '31': 'A',
    '30': 'B',
    '21': 'B',
    '2A': 'B'
}


class GiscedataProfilesProfile(osv_mongodb.osv_mongodb):

    _name = 'giscedata.profiles.profile'

    def get_perfils_cups(self, cursor, uid, cups_name, data_inici, data_final,
                         context=None):
        """Returns the list of ids of giscedata.profiles.profile corresponent
        to cups_name between data_inici and data_final

        :param cursor: database cursor
        :param uid: user identifier
        :param cups_name: name of the CUPS we want the "prefils" of
        (ex. ES1234000000000001JN0F)
        :param data_inici: date from which we start to get "perfils" (included)
        :param data_final: last date from where we will get "perfils" (included)
        :param context: current context
        :return: the list of "perfils" between data_inici and data_final for
        the given CUPS
        """
        if context is None:
            context = {}

        inici, final = facturacio_utils.prepare_hours_from_date(
            data_inici, data_final
        )

        return self.search(cursor, uid, [
            ('cups', '=', cups_name),
            ('timestamp', '>=',
             facturacio_utils.datetime_string_from_date(inici)),
            ('timestamp', '<=',
             facturacio_utils.datetime_string_from_date(final)),
        ])

    def get_sum_perfils_cups(self, cursor, uid, cups_name, data_inici,
                             data_final, context=None):
        """Returns the total of all the "perfils" for the given CUPS between
        data_inici and data_final

        :param cursor: database cursor
        :param uid: user identifier
        :param cups_name: name of the CUPS we want the total of "prefils" of
        (ex. ES1234000000000001JN0F)
        :param data_inici: date from which we start to sum "perfils" (included)
        :param data_final: last date from where we will sum "perfils" (included)
        :param context: current context
        :return: the total of the sum of all "perfils" between data_inici and
        data_final for the given CUPS
        """
        if context is None:
            context = {}

        pids = self.get_perfils_cups(
            cursor, uid, cups_name, data_inici, data_final, context
        )

        perfils = self.read(cursor, uid, pids, ['lectura'], context)
        return sum([x['lectura'] for x in perfils])

    def borrar_perfils_cups(self, cursor, uid, cups_name, data_inici,
                            data_final, context=None):
        """Deletes all the "prefils" for the given CUPS between data_inici
        and data_final

        :param cursor: database cursor
        :param uid: user identifier
        :param cups_name: name of the CUPS we want the total of "prefils" of
        (ex. ES1234000000000001JN0F)
        :param data_inici: date from which we start to delete them (included)
        :param data_final: last date from where we will delete them (included)
        :param context: current context
        :return: the number of deleted "perfils"
        """
        if context is None:
            context = {}

        pids = self.get_perfils_cups(
            cursor, uid, cups_name, data_inici, data_final
        )

        self.unlink(cursor, uid, pids, context)

        return len(pids)

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'cups' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(GiscedataProfilesProfile,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('Timestamp', size=10, required=True, readonly=True),
        'cups': fields.char('CUPS', size=25, required=True, readonly=True),
        'estacio': fields.selection([(0, 'Hivern'), (1, 'Estiu')], 'Estació',
                                    required=True, readonly=True),
        'lectura': fields.integer('Lectura', required=True, readonly=True),
        'decimals': fields.float('Decimals sobrants', digits=(13, 12),
                                 required=True, readonly=True),
        'timestamp': fields.datetime('Hour timestamp', required=True,
                                     readonly=True),
        'factura': fields.char('Invoice', size=32, required=True,
                               readonly=True)
    }

    _defaults = {
    }

    _sql_constraints = [('name_cups_estacio_magnitud',
                         'unique (cups,timestamp,estacio)',
                         'Ya existe la hora perfilada por este CUPS')]

    _order = "name asc"


GiscedataProfilesProfile()
