# -*- coding: utf-8 -*-
from __future__ import division
from datetime import timedelta, datetime
import logging

from destral import testing
from destral.transaction import Transaction

from enerdata.contracts import tariff as enerdata_tariff
from enerdata.datetime.timezone import TIMEZONE as enerdata_timezone

enerdata_log = logging.getLogger("enerdata")
enerdata_log.setLevel(logging.ERROR)


def convert_string_to_datetime(string):
    """
    Auxiliar method desired to simulate the expected parse of a datetime
    """
    return datetime.strptime(string, "%Y-%m-%d %H")


class TestsPerfilacio(testing.OOTestCase):

    def setUp(self):
        self.profile_obj = self.openerp.pool.get('giscedata.profiles.profile')
        self.fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.openerp.pool.get('giscedata.polissa')
        self.modcon_obj = self.openerp.pool.get(
            'giscedata.polissa.modcontractual')

        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        self.fact_id = self.fact_obj.search(
            self.cursor, self.uid, [('energia_kwh', '!=', False)], limit=1)[0]
        self.fact_object = self.fact_obj.browse(
            self.cursor, self.uid, self.fact_id)
        self.fact = self.fact_obj.read(self.cursor, self.uid, self.fact_id, [])

        self.polissa_id = int(self.fact['polissa_id'][0])

        # Activate related contract
        self.polissa_obj.send_signal(self.cursor, self.uid, [self.polissa_id], [
            'validar', 'contracte'
        ])

        # Daylight saving transition dates for 2016, 2017 and 2018
        self.transition_dates = {
            2016: {
                "spring": datetime(2016, 3, 27, 1, 0),
                "autumn": datetime(2016, 10, 30, 1, 0),
            },
            2017: {
                "spring": datetime(2017, 3, 26, 1, 0),
                "autumn": datetime(2017, 10, 29, 1, 0),
            },
            2018: {
                "spring": datetime(2018, 3, 25, 1, 0),
                "autumn": datetime(2018, 10, 28, 1, 0),
            },
        }

        self.transition_operation = {
            "spring": "-1",
            "autumn": "+1",
        }

    def tearDown(self):
        self.txn.stop()

    def get_invoice(self):
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_facturacio',
            'factura_0001'
        )[1]

        return inv_obj.browse(self.cursor, self.uid, inv_id)

    def test_profile_an_invoice(self):
        """
        Test that a profile is generated as expected
        """

        # Get invoice
        invoice = self.get_invoice()

        # Preventive delete of existing profiles
        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        self.profile_obj.unlink(self.cursor, self.uid, perfils_ids)

        profile = self.fact_obj.perfilar(self.cursor, self.uid, [invoice.id])

        assert profile is True, "Profile creation must work"

        # Pending to add invoice_id at Profile to simplify reviewing of
        # resultant profile
        energy_sum = float(0)

        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        perfils_count = len(perfils_ids)
        number_of_hours = perfils_count
        prof_ids = self.profile_obj.search(
            self.cursor, self.uid, [
                ('cups', '=', invoice.cups_id.name),
                ('factura', '=', invoice.number)
            ]
        )
        energy_sum = sum(
            [x['lectura'] for x in self.profile_obj.read(
                self.cursor, self.uid, prof_ids, ['lectura'])]
        )

        # [!] Assertion: Profiled estimation total energy == invoice total
        invoice_total_energy = float(invoice.energia_kwh)
        self.assertEqual(invoice_total_energy, energy_sum,
                         "Profile estimation total energy ['{}'] must match "
                         "invoice total energy ['{}']"\
                         .format(energy_sum, invoice_total_energy))

        # [!] Assertion: Profile number of elements must match the number of
        #  ours between start and end invoice dates

        invoice_start_date = convert_string_to_datetime(
            invoice.data_inici + " 0") + timedelta(hours=1)
        invoice_end_date = convert_string_to_datetime(
            invoice.data_final + " 0") + timedelta(days=1)

        dates_difference_seconds = (
            invoice_end_date - invoice_start_date).total_seconds()
        # Invoice hours with fixed first hour (timedelta performs natural
        # substraction, so first hour must be handled)
        number_of_hours_of_the_invoice = (dates_difference_seconds / 3600) + 1

        # Adapt invoice number of hours based on daylight saving definition
        # Clarify if hour addition/subs must be performed due to daylight
        # saving process
        invoice_year = invoice_end_date.year

        # Ensure that current year is in test scope -- to detect if a test
        # update must be performed with new invoices dates
        assert invoice_year in self.transition_dates.keys()
        current_transition_dates = self.transition_dates[invoice_year]

        # Substract 1hour if daylight saving spring is inside invoice scope
        if current_transition_dates["spring"] > invoice_start_date and \
           current_transition_dates["spring"] < invoice_end_date:
            number_of_hours_of_the_invoice -= 1

        # Add 1 hour if daylight saving spring is inside invoice scope
        if current_transition_dates["autumn"] > invoice_start_date and \
           current_transition_dates["autumn"] < invoice_end_date:
            number_of_hours_of_the_invoice += 1

        # Validate the matching of the number of hours
        self.assertEqual(number_of_hours, perfils_count,
                         "The number of hours integrated in all profiles \
                          ('{}') must be the same than the hours between the \
                          invoice dates range ('{}')"\
                          .format(perfils_count,
                                  number_of_hours))

    def test_profile_an_simple_invoice(self):
        """
        Test that the profile handles the daylight autumn change of hour (+1)
        """

        fact = {
            'measures': [],
            'tariff': enerdata_tariff.get_tariff_by_code("2.0A")(),
            'periods_energy': {"P1": 55},
        }

        # Autumn hour change 2017/10/29
        fact['data_inici'] = "2017-02-01"
        fact['data_final'] = "2017-02-01"
        expected_hours = (24 * 1)

        start_date = enerdata_timezone.localize(convert_string_to_datetime(
            fact['data_inici'] + " 0")) + timedelta(hours=1)
        end_date = enerdata_timezone.localize(convert_string_to_datetime(
            fact['data_final'] + " 0")) + timedelta(days=1)

        # Call the internal method that estimates an invoice
        estimation = self.fact_obj._estimate(start_date, end_date,
                                             fact['measures'], fact['tariff'],
                                             fact['periods_energy'])

        number_of_hours_expected = expected_hours
        number_of_hours_of_the_profile = len(estimation.measures)
        assertion_message = "Profile hours for simple scenario ('{}') must be "\
                            "the same than the expected ('{}')"\
                            .format(number_of_hours_of_the_profile,
                                      number_of_hours_expected)

        self.assertEqual(number_of_hours_expected,
                         number_of_hours_of_the_profile,
                         assertion_message)


    def test_profile_an_invoice_with_autumn_daylight_saving_change(self):
        """
        Test that the profile handles the daylight autumn change of hour (+1)
        """

        fact = {
            'measures': [],
            'tariff': enerdata_tariff.get_tariff_by_code("2.0A")(),
            'periods_energy': {"P1": 55},
        }

        # Autumn hour change 2017/10/29
        fact['data_inici'] = "2017-10-28"
        fact['data_final'] = "2017-10-30"
        expected_hours = (24 * 3) + 1

        start_date = enerdata_timezone.localize(convert_string_to_datetime(
            fact['data_inici'] + " 0")) + timedelta(hours=1)
        end_date = enerdata_timezone.localize(convert_string_to_datetime(
            fact['data_final'] + " 0")) + timedelta(days=1)

        # Call the internal method that estimates an invoice
        estimation = self.fact_obj._estimate(start_date, end_date,
                                             fact['measures'], fact['tariff'],
                                             fact['periods_energy'])

        number_of_hours_expected = expected_hours
        number_of_hours_of_the_profile = len(estimation.measures)
        assertion_message = "Profile hours for autumn daylight saving change \
                             ('{}') must be the same than the expected ('{}')\
                             ".format(number_of_hours_of_the_profile,
                                      number_of_hours_expected)

        self.assertEqual(number_of_hours_expected,
                         number_of_hours_of_the_profile,
                         assertion_message)


    def test_profile_an_invoice_with_spring_daylight_saving_change(self):
        """
        Test that the profile handles the daylight spring change of hour (-1)
        """

        fact = {
            'measures': [],
            'tariff': enerdata_tariff.get_tariff_by_code("2.0A")(),
            'periods_energy': {"P1": 55},
        }

        # Autumn hour change 2017/03/26
        fact['data_inici'] = "2017-03-25"
        fact['data_final'] = "2017-03-27"
        expected_hours = (24 * 3) - 1

        start_date = enerdata_timezone.localize(convert_string_to_datetime(
            fact['data_inici'] + " 0")) + timedelta(hours=1)
        end_date = enerdata_timezone.localize(convert_string_to_datetime(
            fact['data_final'] + " 0")) + timedelta(days=1)

        # Call the internal method that estimates an invoice
        estimation = self.fact_obj._estimate(start_date, end_date,
                                             fact['measures'], fact['tariff'],
                                             fact['periods_energy'])

        number_of_hours_expected = expected_hours
        number_of_hours_of_the_profile = len(estimation.measures)

        assert_message = "Profile hours for spring daylight saving change \
                         ('{}') must be the same than the expected ('{}')"\
                         .format(number_of_hours_of_the_profile,
                                 number_of_hours_expected)

        self.assertEqual(number_of_hours_expected,
                         number_of_hours_of_the_profile,
                         assert_message)


    def test_profile_an_invoice_with_metter_change(self):
        """
        Test to check the hours profiled on invoices with metter change
        """
        # Get invoice
        invoice = self.get_invoice()

        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        self.profile_obj.unlink(self.cursor, self.uid, perfils_ids)

        # Two metters assign to invoice
        self.fact_obj.write(
            self.cursor, self.uid, invoice.id, {'comptadors': [1, 2]})

        # Profiling
        self.fact_obj.perfilar(self.cursor, self.uid, [invoice.id])
        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)

        # Profiled hours vs estimated hours
        hours = len(
            self.profile_obj.read(
                self.cursor, self.uid, perfils_ids, ['timestamp'])
        )
        estimated_hours = (
            ((datetime.strptime(invoice.data_final, '%Y-%m-%d') -
             datetime.strptime(invoice.data_inici, '%Y-%m-%d')).days + 1)
            * 24)
        hours = estimated_hours

        # Leave the policy as before
        self.fact_obj.write(
            self.cursor, self.uid, invoice.id, {'comptadors': [1]})

        # Including +1h (change of hour!)
        self.assertEqual(estimated_hours, hours)

    def test_no_61_profile(self):

        self.fact_obj.write(
            self.cursor, self.uid, self.fact_id, {'tarifa_acces_id': 11})

        self.assertRaises(
            Exception, self.fact_obj.perfilar, self.cursor, self.uid,
            [self.fact_id]
        )

    def test_profiling_adjust_reads(self):
        """
        Test profile one invoice with adjusted reads
        """
        lectures_obj = self.openerp.pool.get(
            'giscedata.facturacio.lectures.energia'
        )

        factura_id = self.fact_object.id
        search_params = [("factura_id", "=", factura_id)]
        lectures_energia_ids = lectures_obj.search(
            self.cursor, self.uid, search_params, order="data_anterior asc"
        )

        fake_adjust = 2
        expected = 0
        for lectura in lectures_obj.browse(self.cursor, self.uid,
                                          lectures_energia_ids):
            lectura.write({
                'ajust': fake_adjust, 'consum': lectura.consum + fake_adjust
            })
            expected += (lectura.consum + fake_adjust)

        # invoice open & recalc energia_kwh
        self.fact_object.invoice_open()
        self.fact_object.total_energia_kwh(tipus='activa')
        balance = self.fact_object.get_profiles_balance_per_meter()
        total_energy_from_lectures = balance[factura_id]['total']['all']

        self.assertEqual(total_energy_from_lectures, expected)

    def test_profiling_adjust_negative_reads(self):
        """
        Test profile one invoice with adjusted negative reads
        """
        lectures_obj = self.openerp.pool.get(
            'giscedata.facturacio.lectures.energia'
        )

        invoice = self.get_invoice()
        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        self.profile_obj.unlink(self.cursor, self.uid, perfils_ids)

        search_params = [("factura_id", "=", invoice.id)]
        lectures_energia_ids = lectures_obj.search(
            self.cursor, self.uid, search_params, order="data_anterior asc"
        )

        fake_adjust = -1
        expected = 0
        for lectura in lectures_obj.browse(self.cursor, self.uid,
                                           lectures_energia_ids):
            lectura.write({
                'ajust': fake_adjust, 'consum': lectura.consum + fake_adjust
            })
            expected += lectura.consum + fake_adjust

        # Refresh invoice & recalc energia_kwh
        invoice = self.get_invoice()
        balance = invoice.get_profiles_balance_per_meter()
        total_energy_from_lectures = balance[invoice.id]['total']['all']

        self.assertEqual(total_energy_from_lectures, float(expected))
