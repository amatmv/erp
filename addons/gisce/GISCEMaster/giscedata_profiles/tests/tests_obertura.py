# -*- coding: utf-8 -*-
from __future__ import division
from datetime import datetime, timedelta
import logging

from destral import testing
from destral.transaction import Transaction

from enerdata.datetime.timezone import TIMEZONE
from enerdata.profiles.profile import Profile

enerdata_log = logging.getLogger("enerdata")
enerdata_log.setLevel(logging.ERROR)


class TestsObertura(testing.OOTestCase):

    def setUp(self):
        self.profile_obj = self.openerp.pool.get('giscedata.profiles.profile')
        self.fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.openerp.pool.get('giscedata.polissa')
        self.modcon_obj = self.openerp.pool.get(
            'giscedata.polissa.modcontractual')

        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user


        imd_obj = self.openerp.pool.get('ir.model.data')
        self.fact_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_facturacio',
            'factura_0001'
        )[1]
        self.facts_id = self.fact_obj.search(
            self.cursor, self.uid, [('energia_kwh', '!=', False),
                          ('tipo_factura', '=', '01')])[2]
        self.fact_object = self.fact_obj.browse(self.cursor, self.uid, self.fact_id)
        self.fact = self.fact_obj.read(self.cursor, self.uid, self.fact_id, [])
        self.polissa_id = int(self.fact['polissa_id'][0])

    def tearDown(self):
        self.txn.stop()

    def crear_modcon(self, potencia, ini, fi):

        pol = self.polissa_obj.browse(self.cursor, self.uid, self.polissa_id)
        pol.send_signal(['modcontractual'])
        potencia += + 0.1
        self.polissa_obj.write(
            self.cursor, self.uid, self.polissa_id, {'potencia': potencia}
        )
        wz_crear_mc_obj = self.openerp.pool.get(
            'giscedata.polissa.crear.contracte'
        )

        ctx = {'active_id': self.polissa_id}
        params = {'duracio': 'nou'}
        wz_id_mod = wz_crear_mc_obj.create(self.cursor, self.uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(self.cursor, self.uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            self.cursor, self.uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def get_profiled_dates(self, fact_id):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lect_obj = self.openerp.pool.get('giscedata.facturacio.lectures.energia')
        lines = factura_obj.read(self.cursor, self.uid, fact_id, ['linies_energia'])['linies_energia']
        lect_dates = []
        for lect in lect_obj.read(self.cursor, self.uid, lines, ['tipus', 'data_actual', 'data_anterior']):
            if lect['tipus'] != 'activa':
                continue
            lect_dates.append(lect['data_actual'])
            lect_dates.append(lect['data_anterior'])

        # first lect + 1 day
        # end lect + 1 day
        di = datetime.strftime((datetime.strptime(min(lect_dates), '%Y-%m-%d') + timedelta(days=1)), '%Y-%m-%d')
        df = datetime.strftime((datetime.strptime(max(lect_dates), '%Y-%m-%d') + timedelta(days=1)), '%Y-%m-%d')

        return di + ' 01:00:00', df + ' 00:00:00'

    @staticmethod
    def get_number_of_hours(date_from, date_to):
        """
        Returns number of hours from two dates
        :param date_from: Start of curve: Y-m-d 01:00:00
        :param date_to: End of curve Y-m-d 00:00:00
        :return: n_hours int
        """

        start = TIMEZONE.localize(datetime.strptime(date_from,
                                                    '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(date_to,
                                                  '%Y-%m-%d %H:00:00'))
        profile = Profile(start, end, [])

        return profile.n_hours

    def test_open_invoice_profile_it(self):

        # Confirm that the invoice is just a draft
        assert self.fact_object.state == u'draft', "Target invoice must be not opened ({})".format(self.fact_object.state)

        # Perform a preventive delete of existing profiles (of other testing execution)
        # mongo OSV transactional logic do not exist, so all garbage data is not cleaned...
        profile_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, self.fact['cups_id'][1],
            self.fact['data_inici'], self.fact['data_final'])

        # Open the invoice
        encua_perfilacio_sync = self.fact_obj.encua_perfilacio_sync
        self.fact_obj.encua_perfilacio = encua_perfilacio_sync

        invoice_is_open = self.fact_object.invoice_open()
        self.fact_object.perfilar()

        assert invoice_is_open, "Invoice '{}' open action must be performed as expected".format(self.fact_id)
        assert self.fact_object.state != u'open', "Target invoice must be opened ({})".format(self.fact_object.state)


        di, df = self.get_profiled_dates(self.fact_object.id)
        expected_hours = self.get_number_of_hours(di, df)
        # Confirm that profiles are created at opening time
        profile_ids = self.profile_obj.search(self.cursor, self.uid, [
            ('cups', '=', self.fact['cups_id'][1]),
            ('timestamp', '>=', di),
            ('timestamp', '<=', df)
        ])

        # We know that it must be 1464 (61 days) + 1h (change of hour!) profiles related to this invoice
        msg = 'Profiled hours {} do not match the expected ones {}'.format(
            len(profile_ids), expected_hours
        )
        assert (len(profile_ids) == expected_hours), msg
