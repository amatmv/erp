# -*- coding: utf-8 -*-

from osv import osv

class CrmCase(osv.osv):

    _inherit = 'crm.case'
    _name = 'crm.case'

    def create_profiling_crm_case(self, cursor, uid, factura_id, factura, title,
                                  msg, diffs=None, polissa_id=None,
                                  context=None):
        """
        Create a crm_case with profiling error
        :return: crm_case_id
        """
        crm_obj = self.pool.get('crm.case')
        crm_section_obj = self.pool.get('crm.case.section')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        section_id = crm_section_obj.search(
            cursor, uid, [('code', '=', 'PERF')]
        )[0]
        crm_id = crm_obj.search(
            cursor, uid,
            [('section_id', '=', section_id), ('name', '=', title)]
        )
        if crm_id:
            # Append crm case info
            crm_id = self.append_description_to_case(cursor, uid, crm_id, msg)
        else:
            if isinstance(factura_id, (list, tuple)):
                factura_id = factura_id[0]
            if isinstance(polissa_id, (list, tuple)):
                polissa_id = polissa_id[0]
            try:
                tariff = factura_obj.read(cursor, uid, factura_id, ['tarifa_acces_id'])['tarifa_acces_id'][1]
                title = "{} ({})".format(title, tariff)
            except Exception:
                title = "{} ({})".format(title, '')
            crm_case_vals = {
                'name': title,
                'section_id': section_id,
                'user_id': uid,
                'description': msg,
                'ref': 'giscedata.facturacio.factura,{0}'.format(factura_id),
                'polissa_id': polissa_id,
            }
            if diffs:
                energy_fact = diffs['facturada']
                energy_perf = diffs['perfilada']
                try:
                    crm_case_vals['probability'] = (
                            (float(energy_fact) - float(energy_perf)) /
                            float(energy_fact) * 100
                    )
                except ZeroDivisionError:
                    crm_case_vals['probability'] = 100
                crm_case_vals['planned_revenue'] = (
                        float(energy_fact) - float(energy_perf)
                )
            # Create crm case
            crm_id = crm_obj.create(cursor, uid, crm_case_vals)
        return self.case_open(cursor, uid, [crm_id])

    def append_description_to_case(self, cursor, uid, case_id, msg):
        """
        Append info to crm case
        :return crm_case_id
        """
        crm_obj = self.pool.get('crm.case')
        if isinstance(case_id, (list, tuple)):
            case_id = case_id[0]

        case_data = crm_obj.read(cursor, uid, case_id, ['description'])
        if case_data['description']:
            description = case_data['description'] + '\n' + msg
            crm_obj.write(cursor, uid, case_id, {'description': description})
        else:
            crm_obj.write(cursor, uid, case_id, {'description': msg})

        return int(case_id)


CrmCase()
