# New Profiles model stored at MongoDB

It provides a new module that store the `profile` model at MongoDB

Also improves an `invoice` to be able to profile it using `enerdata` via `giscedata_facturacio_factura.perfilar()`

## Migrator tool

A migrator tool is provided at `/utils/migrator.sh`, desired to automatize the migration from already existing profiles stored at PostgreSQL.

This will dump, validate, adapt and import all objects inside the requested table. Destination db and collection will match the original DB and table, and the internal mongo schema fit the needs for the OSV mongo ORM.

**Important!!!** This tool will **erase all existing data** at MongoDB destination collection

### Usage
```
$ bash utils/migrator.sh <DATABASE> <TABLE>

Migrating <DATABASE>/<TABLE> to MongoDB...
1) Dumping psql data

2) Validate export
  Export performed succesfully for 24816835 rows

3) Adapt data

4) Import to MongoDB
2018-01-18T16:33:00.233+0100    connected to: localhost
2018-01-18T16:33:00.233+0100    dropping: <DB>.<COLLECTION>
2018-01-18T16:33:02.124+0100    [........................] <DATABASE>.<TABLE>       0.0 B/14.0 GB (0.0%)
...
...
2018-01-18T16:33:59.125+0100    [........................] <DATABASE>.<TABLE>       195.6 MB/14.0 GB (1.4%)
...
...
2018-01-18T16:39:20.124+0100    [####....................] <DATABASE>.<TABLE>       2.5 GB/14.0 GB (17.9%)
...
...
2018-01-18T16:40:56.125+0100    [#####...................] <DATABASE>.<TABLE>       3.4 GB/14.0 GB (24.7%)
...
...
...
...
...
2018-01-18T16:55:56.125+0100    [########################] <DATABASE>.<TABLE>       14.0 GB/14.0 GB (100.0%)
2018-01-18T17:55:56.840+0100    imported 24816835 documents

5) Validate MongoDB import
  Import at MongoDB performed succesfully for 24816835 rows

6) Add the ID counter for the migrated table!                                                                                                                                                                      

Migration done!!!

```
, where
- `<DATABASE>` is the DB where are stored the data to migrate, and the destination MongoDB database
- `<TABLE>` is the table that isolates all the elements to migrate, and the destination MongoDB collection.
