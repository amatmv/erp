# -*- coding: utf-8 -*-
from __future__ import absolute_import
import re
import time
import netsvc
from collections import Counter
from datetime import datetime, timedelta
from dateutil.rrule import rrule, DAILY
from oorq.decorators import job
from osv import osv, fields
from rq import Queue, get_current_job
from enerdata.profiles.profile import Profile
from enerdata.contracts import tariff as enerdata_tariff
from giscedata_facturacio.giscedata_facturacio import (
    REFUND_RECTIFICATIVE_INVOICE_TYPES
)
import giscedata_profiles.utils as facturacio_utils
import logging
from tools.translate import _

INVOICE_PROFILE_TYPES = ["N", "R", "RA"]
REGULARIZER_TYPE = "G"
COMPLEMENTARY_TYPE = "C"

logger = logging.getLogger("PROFILE")
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
user_path = r'/tmp/perfilacio.log'
hdlr = logging.FileHandler(user_path)
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    JOB_QUEUES = {
        'profiling': 'profiling',
        'pending_cofs': 'profiling_pending_cofs'
    }

    @staticmethod
    def fix_dates(comptador, data_inici, data_final):
        # Override in comer profile
        return facturacio_utils.fix_dates_distri(
            comptador, data_inici, data_final)

    @staticmethod
    def get_period_by_read_name(period_name):
        period = re.findall('.*(P[1-6]).*', period_name)[0]

        return period

    def get_profiles_balance_per_meter(self, cursor, uid, ids, context=None):
        """Obtain the balance of an invoice.
        Keeps reading adjustments. Sum or subtraction depending on the sign,
        if 0 is not applied. consumption = (read + adjust)
        The goal is obtain a dict with the following structure:
        {invoice_id: {
            'dates': {
                'meter_serial_number': (min_date, max_date)
            },
            'total': {
                'meter_serial_number': Total_meter_consumption,
                'all': Total_consumption
            },
            'balance': {
                'meter_serial_number': {
                    'Period_Number': Consum
                }
            },
            'origin': {
                'meter_serial_number': 'codi-subcodi',
            },
            'tariff': 'Tariff name'
            'order': [meter_serial_number, meter_serial_number2, ...]
        }
        """
        lectures_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        res = {}
        for invoice in self.browse(cursor, uid, ids, context=context):
            metters = {
                'balance': {},
                'dates': {},
                'tariff': None,
                'origin': {},
                'total': {'all': 0.0},
                'order': [],
            }
            balances = metters['balance']
            totals = metters['total']
            dates = metters['dates']
            metters['tariff'] = invoice.tarifa_acces_id.name
            order = metters['order']

            # Get sorted reads
            search_params = [("factura_id", "=", ids[0])]
            lectures_energia_ids = lectures_obj.search(cursor, uid,
                                                       search_params,
                                                       order="data_anterior\
                                                              asc")

            for lectura in lectures_obj.browse(cursor, uid,
                                               lectures_energia_ids):
                if lectura.tipus == 'reactiva':
                    continue

                comptador = str(lectura.comptador_id.name)

                if comptador not in balances:
                    balances[comptador] = Counter()

                if comptador not in totals:
                    totals[comptador] = 0.0

                period = self.get_period_by_read_name(lectura.name)
                if invoice.tarifa_acces_id.codi_ocsum == u'011' and period == "P4":
                    period = "P1"

                balances[comptador][period] += lectura.consum
                totals[comptador] += lectura.consum
                totals['all'] += lectura.consum
                metters['origin'][comptador] = '{0}-{1}'.format(
                    lectura.origen_id.codi, lectura.origen_id.subcodi
                )
                lect_dates = self.fix_dates(
                    lectura.comptador_id, lectura.data_anterior,
                    lectura.data_actual
                )
                if comptador not in dates:
                    dates[comptador] = lect_dates
                    order.append(comptador)
                else:
                    start_meter, end_meter = dates[comptador]
                    start_lect, end_lect = lect_dates
                    start = min(start_meter, start_lect)
                    end = max(end_meter, end_lect)
                    dates[comptador] = start, end
            res[invoice.id] = metters.copy()
        return res

    def get_sorted_meters_grouped_by_dates(self, dataset):
        """
        :param dateset: Dict LIKE:
        {
            'meter1': ('init_date', 'end_date'),
            'meter2': ('init_date', 'end_date'),
            'meter3': ('init_date', 'end_date'),
        }
        :return: Dict of meters grouped by dates and ordered
        list by meter date LIKE:
        {
            ('init_date', 'end_date'): [meter1, meter2],
            ('init_date', 'end_date'): [meter3],
            order: [meter1, meter2, meter3]
        }
        """
        # Todo: empty dateset?
        # Set data structure date: meters
        res = {}
        for meter, date in dataset.items():
            if date in res:
                res[date].append(meter)
            else:
                res[date] = [meter]

        # Sort meters
        order = []
        for date in sorted(res.keys()):
            for meter in res[date]:
                order.append(meter)
        res['order'] = order

        return res

    def regularizer_profile(self, cursor, uid, invoice_id, cups, context=None):
        profile_obj = self.pool.get('giscedata.profiles.profile')
        crm_obj = self.pool.get('crm.case')
        g_data = self.get_regularized_info(cursor, uid, invoice_id,
                                           context=context)
        invoice_number = self.get_invoice_vals(
            cursor, uid, invoice_id, context
        )['factura']
        if g_data['consumption'] < 0:
            msg = (
                "The G invoice: {} contains negative consumptions: {}".format(
                    g_data['consumption'], invoice_id
                )
            )
            title = "G Invoice: {}, contains negative consumptions:".format(
                invoice_number
            )
            crm_obj.create_profiling_crm_case(
                cursor, uid, invoice_id, invoice_number, title, msg
            )
            return False
        start_date = g_data['start_date']
        end_date = g_data['end_date']
        # Cancel profiles between dates
        search_params = [
            ('cups', '=', cups),
            ('timestamp', '>=', start_date),
            ('timestamp', '<=', end_date),
        ]
        profile_ids_to_delete = profile_obj.search(cursor, uid, search_params)
        if profile_ids_to_delete:
            profile_obj.unlink(cursor, uid, profile_ids_to_delete)

        start_hour, end_hour = facturacio_utils.prepare_hours_from_date(
            start_date, end_date, start_delay=-1, end_delay=-24
        )

        measures = []
        accumulated = 0.0
        tariff_name = g_data['tariff']
        tariff = (enerdata_tariff.get_tariff_by_code(tariff_name))()
        balance = g_data['consumption']
        estimation = self._estimate(
            start_hour, end_hour, [], tariff, balance, accumulated)

        # Prepare Profiles.Profile object for all estimated measure
        for measure in estimation.measures:
            timestamp = facturacio_utils.datetime_string_from_date(
                measure.date
            )
            profile_vals = {
                'cups': cups,
                'factura': invoice_number,
                'timestamp': timestamp,
                'lectura': measure.measure,
                'decimals': measure.accumulated,
                'estacio': measure.date.timetuple().tm_isdst,
            }
            profile_obj.create(cursor, uid, profile_vals)

    def get_regularized_info(self, cursor, uid, ids, context=None):
        """
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return: Dict like: {
            tariff: tariff, dates: [di, df], consumption: consumption
        }
        """
        lectures_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        dates = []
        lectures_energia_ids = lectures_obj.search(
            cursor, uid, [("factura_id", "=", ids), ('tipus', '=', 'activa')]
        )
        invoice = self.browse(cursor, uid, ids, context=context)
        tariff = invoice.tarifa_acces_id.name
        balances = Counter()
        for lectura in lectures_obj.browse(cursor, uid, lectures_energia_ids):
            period = self.get_period_by_read_name(lectura.name)
            balances[period] += lectura.consum
            di, df = self.fix_dates(
                lectura.comptador_id, lectura.data_anterior,
                lectura.data_actual
            )
            dates.append(di)
            dates.append(df)
        di = min(dates)
        df = max(dates)

        res = {
            'tariff': tariff, 'start_date': di, 'end_date': df,
            'consumption': balances
        }
        return res

    def prepare_30A_with_incoherent_periods(self, profile, tariff, balance):
        """
        Prepare the balance that should come with 3 periods for non-holidays,
        and come with 6. Divide the consumption between the 3 periods
        :param profile: Profile instance
        :param tariff: Tariff instance
        :param balance: The balance per period LIKE {'P1': x, 'P2': 'y}
        :return: The balance per period with 3 periods if it's necessary
        """
        cofs_per_period = []
        for gap in profile.gaps:
            period = tariff.get_period_by_date(gap)
            cofs_per_period.append(period.code)

        cofs_per_period = sorted(list(set(cofs_per_period)))
        if sorted(balance.keys()) != cofs_per_period:
            for x, y in zip(('P1', 'P2', 'P3'), ('P4', 'P5', 'P6')):
                balance[x] += balance[y]
                balance.pop(y, 0)

        return balance

    def _estimate(self, start_hour, end_hour, measures, tariff,
                  total_by_period, initial_accumulate=0.0):
        """
        Auxiliar method that initializes a Profile and return it's estimation

        :param start_hour: the first of the profile
        :param end_hour: the last hour of the profile (inclusive)
        :param measures: list of existing measures, default expected []
        :param tariff: tariff name
        :param total_by_period: dict {<period_name>: <total_energy_by_period>}
        :param initial_estimate: float/Decimal of preceding accumulated energy
        :return: an enerdata.Profile instance with the related estimation
        """

        # Create base profile and estimate it
        profile = Profile(start_hour, end_hour, measures, initial_accumulate)
        if tariff.code == '3.0A':
            total_by_period = self.prepare_30A_with_incoherent_periods(
                profile, tariff, total_by_period
            )
        return profile.estimate(tariff, total_by_period)

    def perfilar(self, cursor, uid, ids, context=None):
        """
        Profile an invoice (start + end dates, consumption by periods, tariff)
        and estimate it using REE coeficients.

        It validates that the reads used to calculate the energy total by
        period match this invoice total energy.

        Internally, it creates a profile for each read period and each counter,
        to be able to handle:
        - more than one counter by invoice
        - more than one read date range by counter

        For each temporaly created profile, save the estimated measure for each
        hour using a Profiles.Profile.

        Return True if it works!

        :param cursor: database cursor
        :param uid: user identifier
        :param invoice_id: the invoice id to profile
        :param context: current context
        :return: True/False profile creation result
        """
        if context is None:
            context = {}

        if type(ids) == int:
            ids = [ids]

        profile_obj = self.pool.get('giscedata.profiles.profile')
        acc_invoice_obj = self.pool.get('account.invoice')
        polissa_obj = self.pool.get('giscedata.polissa')
        crm_obj = self.pool.get('crm.case')

        last_profiled_hour = None

        # TODO: ordered for
        factura_columns = [
            'data_inici', 'data_final', 'cups_id', 'energia_kwh',
            'tarifa_acces_id', 'cups', 'polissa_id', 'invoice_id',
            'tipo_rectificadora', 'number', 'rectifying_id', 'potencia', 'dies',
            'lectures_energia_ids'
        ]

        for fact in self.read(cursor, uid, list(ids), factura_columns, context):
            acci = acc_invoice_obj.q(cursor, uid).read(['journal_id.code']).where([('id', '=', fact['invoice_id'][0])])
            invoice_type = acci[0]['journal_id.code']

            invoice_id = fact['id']
            invoice_number = self.get_invoice_vals(
                cursor, uid, invoice_id, context
            )['factura']

            cups_name = str(fact['cups_id'][1])

            # Nullifier
            if fact['tipo_rectificadora'] in REFUND_RECTIFICATIVE_INVOICE_TYPES:
                factura_id = self._get_factura_from_invoice(
                    cursor, uid, [fact['rectifying_id'][0]])
                # when distri: number / when comer: origin
                inv_number = self.get_invoice_vals(
                    cursor, uid, factura_id[0]
                )
                search_params = [
                    ('factura', '=', inv_number['factura']),
                    ('cups', '=', cups_name)
                ]
                profile_ids_to_delete = profile_obj.search(cursor, uid, search_params)
                profilable = self.check_profilable(cursor, uid, factura_id[0], context)
                if profile_ids_to_delete:
                    profile_obj.unlink(cursor, uid, profile_ids_to_delete)
                elif not profilable:
                    continue
                else:
                    msg = _(
                        "The Nullifier: {} doesn't find profiles in the "
                        "original invoice: {} - Profiles couldn't be "
                        "deleted".format(
                            invoice_number, inv_number['factura']
                        )
                    )
                    title = _("{}: Nullifier: {} doesn't find profiles:".format(
                        cups_name, invoice_number
                    ))
                    crm_obj.create_profiling_crm_case(
                        cursor, uid, invoice_id, invoice_number, title, msg,
                        polissa_id=fact['polissa_id'][0]
                    )
                # Next factura
                continue
            # Regularized G
            if fact['tipo_rectificadora'] == REGULARIZER_TYPE:
                self.regularizer_profile(
                    cursor, uid, invoice_id, cups_name, context=context
                )
                # Next factura
                continue

            # Prepare the periods of this tariff
            tariff_name = fact['tarifa_acces_id'][1]
            flag_low_voltage_measure = False
            if '6.1' in tariff_name or '3.1' in tariff_name:
                # Flag lectura_en_baja
                flag_low_voltage_measure = polissa_obj.read(
                    cursor, uid, fact['polissa_id'][0], ['lectura_en_baja'],
                    context={'date': fact['data_inici']}
                )
                # More secure consult if the field exists in db
                if 'lectura_en_baja' in flag_low_voltage_measure:
                    flag_low_voltage_measure = flag_low_voltage_measure[
                        'lectura_en_baja'
                    ]

            # Validate that invoice is "profilable"
            # - Invoice journal code type is ENERGIA% or CENERGIA%
            # - That are not power
            # - Validate that not 6.1 tariff without low_voltage_measure
            # - Validate that not Regularizer (G)
            assert "ENERGIA" in invoice_type, "Invoice journal code must be ENERGIA/CENERGIA* ({invoice_type})".format(invoice_type=invoice_type)
            assert '6.1' not in fact['tarifa_acces_id'][1] and not flag_low_voltage_measure, "Can't profile 6.1 tariff: {inv_id}".format(inv_id=invoice_id)
            assert COMPLEMENTARY_TYPE not in fact['tipo_rectificadora'], "Can't profile Complementary invoices: {inv_id}".format(inv_id=invoice_id)

            # Profile with low_voltage_measure: 3.1A LB Tariffs and
            # contracts with lectura_en_baja flag is activated
            kwargs = {}
            if tariff_name == '3.1A LB' or flag_low_voltage_measure:
                kva = polissa_obj.read(
                    cursor, uid, fact['polissa_id'][0], ['trafo'],
                    context={'date': fact['data_inici']}
                )['trafo']
                kwargs = {'kva': kva}

            # Profile (3.1A/3.0A/3.1A LB) 3 periods
            if '3.' in tariff_name:
                lect_energy_obj = self.pool.get('giscedata.facturacio.lectures.energia')
                lect_data = lect_energy_obj.read(cursor, uid, fact['lectures_energia_ids'], ['tipus', 'name', 'magnitud'])
                if len(list(set([x['name'] for x in lect_data
                                 if x['tipus'] == 'activa' and x['magnitud'] == 'AE']))) < 6:
                    tariff_name = tariff_name.replace(' LB', '')
                    tariff_name += ' C2'

            tariff = (enerdata_tariff.get_tariff_by_code(tariff_name))(**kwargs)

            # Get balances per period and meter
            balance_per_meter = self.get_profiles_balance_per_meter(cursor, uid,
                                                           [invoice_id]
                                                          )[invoice_id]

            # Validate sum(balance_per_meter) = invoice_energy
            total_energy_from_lectures = balance_per_meter['total']['all']

            # Calc max theoretical consumption in Watts
            power = fact['potencia']
            days = fact['dies']
            maximum_theoretical_consumption = (power * 1000) * days

            # Create CRMCase
            if total_energy_from_lectures < 0:
                msg = _(
                    "The invoice: {} contains negative consumptions: {}".format(
                        total_energy_from_lectures, invoice_id
                    )
                )
                title = _(
                    "{}: Invoice: {}, contains negative consumptions:".format(
                        cups_name, invoice_number
                    )
                )
                crm_obj.create_profiling_crm_case(
                    cursor, uid, invoice_id, invoice_number, title, msg,
                    polissa_id=fact['polissa_id'][0]
                )
            elif total_energy_from_lectures > maximum_theoretical_consumption:
                msg = _(
                    "Energy readings larger than the theoretical maximum "
                    "allowed. Consumption energy: {} vs Theoretical energy:"
                    " {} from invoice: {}".format(
                        total_energy_from_lectures,
                        maximum_theoretical_consumption, invoice_id
                    )
                )
                title = _("{}: Energy readings larger than the theoretical "
                          "maximum allowed from invoice: {} - "
                          "[NOT PROFILED]".format(cups_name, invoice_number))
                crm_obj.create_profiling_crm_case(
                    cursor, uid, invoice_id, invoice_number, title, msg,
                    polissa_id=fact['polissa_id'][0]
                )
                # Next factura
                continue
            elif total_energy_from_lectures != fact['energia_kwh']:
                msg = _(
                    "perf: {} vs fact: {} The sum of all energy "
                    "lectures must match with the total energy of the invoice: "
                    "{}".format(
                        total_energy_from_lectures,
                        fact['energia_kwh'], invoice_id
                    )
                )
                title = _("{}: The energy of the invoice {} does not match the "
                         "profile".format(cups_name, invoice_number))
                if tariff_name != '3.1A LB':
                    diffs = {
                        'facturada': fact['energia_kwh'],
                        'perfilada': total_energy_from_lectures
                    }
                    crm_obj.create_profiling_crm_case(
                        cursor, uid, invoice_id, invoice_number, title, msg, diffs,
                        polissa_id=fact['polissa_id'][0]
                    )

            # Prepare an empty measures list to start a fresh profile
            measures = []

            # Sort counters list grouped by energy dates
            meters_by_dates = self.get_sorted_meters_grouped_by_dates(
                balance_per_meter['dates']
            )

            # Preventive delete before going through the counters by date.
            # If it is done in the loop, there is a risk of deleting meter
            # readings with the same dates
            if meters_by_dates:
                search_params = [
                    ('factura', '=', invoice_number),
                    ('cups', '=', cups_name),
                ]
                # Preventive delete
                profile_ids_to_delete = profile_obj.search(cursor, uid,
                                                           search_params)
                profile_obj.unlink(cursor, uid, profile_ids_to_delete)

            for a_counter in meters_by_dates['order']:
                the_balance = balance_per_meter['balance'][a_counter]

                # Extract and adapt dates to needed measures interval
                # #Read start_date must be fixed to +24h
                the_dates = balance_per_meter['dates'][a_counter]
                start_hour, end_hour = facturacio_utils.prepare_hours_from_date(
                    the_dates[0], the_dates[1], start_delay=-1, end_delay=-24
                )

                # Try to reach last hour accumulated value
                accumulated = 0.0
                last_hour = start_hour - timedelta(hours=1)
                last_hour = facturacio_utils.datetime_string_from_date(
                                                                     last_hour)
                last_hour_search_params = [
                    ('timestamp', '=', last_hour),
                    ('cups', '=', cups_name),
                ]
                last_hour_id = profile_obj.search(cursor, uid,
                                                  last_hour_search_params,
                                                  limit=1)

                if last_hour_id:
                    last_hour = profile_obj.read(cursor, uid, last_hour_id)[0]
                    if last_hour and "decimals" in last_hour:
                        accumulated = float(last_hour['decimals'])

                # Estimate "normal" and "rectificadora" invoices [N, R, RA]
                if fact['tipo_rectificadora'] in INVOICE_PROFILE_TYPES:
                    # Estimate current counter balances
                    estimation = self._estimate(start_hour, end_hour, measures,
                                                tariff, the_balance,
                                                accumulated)

                    # Prepare Profiles.Profile object for all estimated measure
                    for measure in estimation.measures:
                        timestamp = facturacio_utils.datetime_string_from_date(
                            measure.date
                        )
                        profile_vals = {
                            'cups': cups_name,
                            'factura': invoice_number,
                            'timestamp': timestamp,
                            'lectura': measure.measure,
                            'decimals': measure.accumulated,
                            'estacio': measure.date.timetuple().tm_isdst,
                        }

                        profile_obj.create(cursor, uid, profile_vals)

                        last_profiled_hour = max(
                            last_profiled_hour, measure.date
                        ) if last_profiled_hour else measure.date
                    self.update_inv_lot(cursor, uid, fact['id'], context=context)

            # Update polissa.data_ultima_lectura_perfilada for normal invoices
            if fact['tipo_rectificadora'] == "N" and last_profiled_hour:
                vals = {
                    'data_ultima_lectura_perfilada':
                        facturacio_utils.date_string_from_date(
                                                           last_profiled_hour),
                }
                polissa_obj.write(cursor, uid, fact['polissa_id'][0], vals)

        return True

    @job(queue='profiling', timeout=2*3600)
    def encua_perfilacio(self, cursor, uid, ids, context=None):
        return self.encua_perfilacio_sync(cursor, uid, ids, context=context)

    def encua_perfilacio_sync(self, cursor, uid, ids, context=None):
        """
        Auxiliar profiling method that trigger it over the working queue
        "profiling" handles exceptions and requeues tasks for identified errors
        """
        log_channel = "Invoice profiling"
        peding_cofs_queue = self.JOB_QUEUES['pending_cofs']
        logger = netsvc.Logger()
        logger.notifyChannel(log_channel, netsvc.LOG_INFO,
                             "Profiling invoice/s '{}'...".format(ids))

        try:
            return self.perfilar(cursor, uid, ids, context)
        except Exception as e:
            logger.notifyChannel(log_channel, netsvc.LOG_ERROR, e)

            if e.message == 'Profiles from REE not found':
                logger.notifyChannel(log_channel, netsvc.LOG_ERROR, 
                                     "Requeuing task to {}".format(
                                                            peding_cofs_queue))
                self.encua_perfilacio_falta_coef(cursor, uid, ids, context)
            else:
                raise
    
            return False

    @job(queue='profiling_pending_cofs')
    def encua_perfilacio_falta_coef(self, cursor, uid, ids, context=None):
        raise NotImplementedError

    def check_profilable(self, cursor, uid, ids, context=None):
        raise NotImplementedError

    def get_invoice_vals(self, cursor, uid, invoice_id, context=None):
        # must be implemented by comer module
        if context is None:
            context = {}
        invoice = self.read(cursor, uid, invoice_id, ['number'], context)

        return {'factura': invoice['number']}

    def update_inv_lot(self, cursor, uid, fact_id, context=None):
        """
        Update inv_measures_batch state
        Only distri modules
        Overrided in comer
        """
        inv_lot_obj = self.pool.get(
            'giscedata.profiles.factura.lot'
        )
        if inv_lot_obj:
            # todo: check number or origin distri/comer
            search_inv_lot_params = [
                ('factura_id', '=', fact_id), ('type', '=', 'perfil')
            ]
            inv_lot_ids = inv_lot_obj.search(
                cursor, uid, search_inv_lot_params
            )
            if inv_lot_ids:
                inv_lot_obj.write(
                    cursor, uid, inv_lot_ids, {'state': 'done'}
                )
        return True


GiscedataFacturacioFactura()
