#!/usr/bin/env python
import sys
from redis import from_url
from rq import use_connection, Queue
from rq.job import Job
from erppeek import Client
import click
import configparser
import logging


def connection(host, db, user, passwd):
    conn = Client(host, db, user, passwd)

    return conn


def setup_logger():
    logger = logging.getLogger('PENDING_COFS')
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )
    user_path = r'/home/erp/var/log/profiling_pending_cofs.log'
    hdlr = logging.FileHandler(user_path)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger


@click.option('-u', '--redis-url', help='URL of redis server',
              default='redis://localhost', show_default=True)
@click.option('-a', '--host', help='Name of connection host')
@click.option('-e', '--user', help='User of connection host')
@click.option('-p', '--password', help='Password of connection host')
@click.option('-c', '--config-file', help='Server configuration file',
              default='/home/erp/conf/server.conf', show_default=True)
@click.command(context_settings=dict(help_option_names=['-h', '--help']))
def requeue_pending_cofs(**kwargs):
    """Requeue all invoices that are in the queue profiling_pending_cofs"""
    redis_conn = from_url(kwargs['redis_url'])
    use_connection(redis_conn)
    profiling_queue = Queue(name='profiling')
    profiling_pending_cofs_queue = Queue(name='profiling_pending_cofs')

    logger = setup_logger()

    config = configparser.ConfigParser()
    config.read(kwargs['config_file'])
    option = config.sections()[0]
    if ('password' not in config[option].keys()
            or not config[option]['db_password']):
        passwd = str(kwargs['password'])
    else:
        passwd = str(config[option]['db_password'])
    c = connection(
        str(kwargs['host']), str(config[option]['db_name']),
        str(kwargs['user']), passwd
    )
    factura_obj = c.GiscedataFacturacioFactura

    job_ids = profiling_pending_cofs_queue.get_job_ids()
    logger.info('Moving {} pending_cofs jobs'.format(len(job_ids)))
    for job_id in job_ids:
        try:
            job = Job.fetch(job_id)
            factura_id = job.args[5]
            job.cancel()
            factura_obj.encua_perfilacio(factura_id)
            _msg = (
                "Try profiling the invoice {inv}. Moving from {queue_pending} "
                "to {queue_profiling} queue".format(
                    inv=factura_id,
                    queue_pending=profiling_pending_cofs_queue.name,
                    queue_profiling=profiling_queue.name
                )
            )
            logger.info(_msg)
        except Exception as e:
            logger.error(str(e))


if __name__ == '__main__':
    requeue_pending_cofs()
