from datetime import datetime, timedelta
from enerdata.datetime.timezone import TIMEZONE as enerdata_timezone
import calendar


def fix_dates_distri(comptador, start, end):
    """Try to fix common dates problems with measures and metters.
    """

    end = datetime.strptime(end[0:10], '%Y-%m-%d')
    start = datetime.strptime(start[0:10], '%Y-%m-%d')

    modcons = comptador.polissa.get_modcontractual_intervals(
        (start - timedelta(days=1)).strftime('%Y-%m-%d'),
        end.strftime('%Y-%m-%d'), {'ffields': ['tarifa']}
    )
    changes = modcons.get(
        start.strftime('%Y-%m-%d'), {}
    ).get('changes', [])

    data_alta = datetime.strptime(comptador.data_alta[0:10], '%Y-%m-%d')
    # End is the end date + 1 day at our 00:00
    end += timedelta(days=1)
    end = end.strftime('%Y-%m-%d 00:00:00')

    if start != data_alta and 'tarifa' not in changes:
        # Start is and old measure date, we have to sum one day
        # and start at hour 01:00
        start += timedelta(days=1)

    start = start.strftime('%Y-%m-%d 01:00:00')
    return start, end

def convert_string_to_datetime(string, hour=0):
    """
    Convert a date string to a datetime for the first hour of this day

    :param start_hour: string representation of a date with mask "%Y-%m-%d %H"
    :return: datetime representation of the incoming date for the first hour
    """

    if string.find(":") > -1:
        time_config = {
            "append": "",
            "mask": "%Y-%m-%d %H:%M:%S",
            "hours_to_add": 0,
        }
    else:
        time_config = {
            "append": " {}".format(hour),
            "mask": "%Y-%m-%d %H",
            "hours_to_add": 0,
        }

    return datetime.strptime(string + time_config['append'], time_config['mask']) + timedelta(hours=time_config['hours_to_add'])


def prepare_hours_from_date(start_date, end_date, start_delay=None, end_delay=None):
    """
    Prepare the start and ending date of a range based on invoice or read dates string

    Start hour = first hour of start_date
    End hour = the last hour of the ending day

    :param start: start day string "%Y-%m-%d"
    :param end: start day string "%Y-%m-%d"

    :param start_delay: delay of hours to apply over the start_hour
    :param end_delay:  delay of hours to apply over the end_hour

    :return: Fixed hours in format (start_hour, end_hour)
    """

    # Prepare the range of dates
    ## Start hour = first hour of start_date
    start_hour = enerdata_timezone.localize(convert_string_to_datetime(start_date, hour=0))
    start_hour += timedelta(hours=1)
    if start_delay:
        start_hour += timedelta(hours=start_delay)

    ## End hour = the last hour of the ending day
    end_hour = enerdata_timezone.localize(convert_string_to_datetime(end_date, hour=0))
    end_hour += timedelta(days=1)
    if end_delay:
        end_hour += timedelta(hours=end_delay)

    return start_hour, end_hour


def datetime_string_from_date(hour):
    """
    Return an string representation of provided datetime.

    This is the one used to interact with the ORM datetime field

    :param hour: datetime to convert
    :return: string representation valid for an ORM datetime field
    """
    return hour.strftime("%Y-%m-%d %H:%M:%S")


def date_string_from_date(hour):
    """
    Return an string representation of provided date.

    This is the one used to interact with the ORM datetime field

    :param hour: datetime to convert
    :return: string representation valid for an ORM datetime field
    """
    return hour.strftime("%Y-%m-%d")


def crear_lots_perfilacio(year=None):
    """
    Return a list of dictionaries with the name and monts of the tyear
    :param year: if not specified, get months from actual year.
    Dict like:
    {
        'name': 01/2018, 'data_inici': '2018-01-01', 'data_final' '2018-01-31'
    }

    :return: list of dictionaries with months and name representations
    """
    if not year:
        year = calendar.datetime.datetime.now().year

    months_vals = []
    for month in range(1, 13):
        vals = {}
        vals['name'] = '{month}/{year}'.format(
            month=str(month).zfill(2), year=year
        )
        vals['data_inici'] = '{year}-{month}-{day}'.format(
            year=year, month=str(month).zfill(2), day='01'
        )
        vals['data_final'] = '{year}-{month}-{day}'.format(
            year=year, month=str(month).zfill(2),
            day=calendar.monthrange(year, month)[1]
        )
        months_vals.append(vals)
    return months_vals


def check_profiled_vs_invoiced():
    import pandas as pd
    from erppeek import Client

    c = Client('http://ecasa-erp.clients:28069', 'ecasa_distri', 'gisce')
    profile_ids = c.model('giscedata.profiles.profile').search(
        [('timestamp', '>=', '2018-02-01'), ('timestamp', '<=', '2018-03-01')])
    df_profiles = pd.DataFrame(
        data=c.model('giscedata.profiles.profile').read(profile_ids))

    invoice_ids = c.model('giscedata.facturacio.factura').search(
        [('number', '!=', False), ('energia_kwh', '!=', False)])
    df_facts = pd.DataFrame(
        data=c.model('giscedata.facturacio.factura').read(invoice_ids,
                                                          ['energia_kwh',
                                                           'number']))
    df_profiles = df_profiles.groupby(['factura']).aggregate({'lectura': 'sum'})
    df_profiles = df_profiles.reset_index()
    df_facts.drop(['id'], axis=1, inplace=True, errors='ignore')
    df_facts = df_facts.rename(index=str, columns={'energia_kwh': 'lectura'})
    df_facts = df_facts.rename(index=str, columns={'number': 'factura'})

    df_concat = pd.concat([df_profiles, df_facts])
    df_concat.drop_duplicates(subset=['factura', 'lectura'], keep=False)