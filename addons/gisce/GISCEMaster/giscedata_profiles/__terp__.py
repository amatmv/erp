# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures a Mongo",
    "description": """
Sistema per la perfilació de les lectures
  * Permet transformar lectures en perfils.
  * Transformar curves horàries en tarifes.
  * Convertir fitxers de curva en fitxers F1.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_facturacio",
        "mongodb_backend",
        "giscedata_polissa",
        "crm",
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_profiles_demo.xml",
    ],
    "update_xml":[
        "giscedata_profiles_view.xml",
        "giscedata_profiles_data.xml",
        "crm_view.xml",
        "crm_data.xml",
        "wizard/wizard_perfilar_view.xml",
        "security/giscedata_profiles_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
