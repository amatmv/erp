# -*- coding: utf-8 -*-
import calendar
from datetime import datetime, timedelta

from osv import osv, fields
from tools.misc import cache
from tools.translate import _
from giscedata_profiles import MAGNITUDS
import netsvc
import pooler
from libfacturacioatr.tarifes import Tarifa31ALB, get_dies_laborables_festius
from oorq.decorators import job

def apply_31A_LB_cof(lectures, kva, data_inici, data_final,
                     dies_festius, perdues=0.04):
    lect_cof = lectures.copy()
    # Construïm el diccionari de consums a través de les lectures PX: Consum
    consums = dict(
            (p, l['actual'].get('consum', 0)) for p, l in lectures.items()
    )
    hores_pp = Tarifa31ALB.hores_per_periode
    laborables, festius = get_dies_laborables_festius(
        datetime.strptime(data_inici, '%Y-%m-%d'),
        datetime.strptime(data_final, '%Y-%m-%d'),
        dies_festius
    )
    cofs = {}
    for periode in hores_pp:
        if periode > 'P3':
            cofs[periode] = hores_pp.get(periode, 0) * festius
        else:
            cofs[periode] = hores_pp.get(periode, 0) * laborables
    for periode, consum in consums.items():
        c = lect_cof[periode]['actual']
        c['consum'] = (round(consum * (1 + perdues), 2)
                       + round(0.01 * cofs[periode] * kva, 2))
    return lect_cof


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def write(self, cursor, uid, ids, vals, context=None):
        """Sobreescrivim el write() per capturar el lot de perfilació.
        """
        lot_id = vals.get('lot_perfilacio', False)
        if lot_id:
            self.assignar_al_lot_perfil(cursor, uid, ids, lot_id, context)
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals,
                                                  context)
        return res

    _columns = {
        'data_ultima_lectura_perfilada': fields.date('Data última lectura '
                                                     'perfilada'),
    }

GiscedataPolissa()