# coding=utf-8
import binascii
import hashlib
import re

from destral import testing
from destral.transaction import Transaction
from giscedata_ov.giscedata_ov import (
    DEFAULT_HASH_ALG, DEFAULT_HASH_ITERATIONS, Hasher
)


class TestOVUser(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def create_account(self, cursor, uid):
        acc_obj = self.openerp.pool.get('poweremail.core_accounts')

        acc_id = acc_obj.create(cursor, uid, {
            'name': 'Test account',
            'user': self.uid,
            'email_id': 'test@example.com',
            'smtpserver': 'smtp.example.com',
            'smtpport': 587,
            'smtpuname': 'test',
            'smtppass': 'test',
            'company': 'yes'
        })
        return acc_id

    def test_reset_password(self):
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        ov_user_obj = self.openerp.pool.get('ov.users')
        pm_mailbox_obj = self.openerp.pool.get('poweremail.mailbox')
        pm_template_obj = self.openerp.pool.get('poweremail.templates')

        acc_id = self.create_account(self.cursor, self.uid)
        tmpl_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_ov', 'reset_password_usuari_email'
        )[1]
        pm_template_obj.write(self.cursor, self.uid, [tmpl_id], {
            'enforce_from_account': acc_id
        })
        # Fix destral duplicated hooks https://github.com/gisce/poweremail/pull/69
        ov_user_obj.template_hooks['sow'] = list(set(ov_user_obj.template_hooks['sow']))

        partner_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'base', 'res_partner_asus'
        )[1]

        partner = partner_obj.read(self.cursor, self.uid, partner_id, ['name'])

        user = partner['name']
        password = user

        user_id = partner_obj.enable_ov_user(
            self.cursor, self.uid, partner_id, user, 'test@example.com', None,
            password
        )
        ov_user_obj.reset_password(self.cursor, self.uid, [user_id])
        res = ov_user_obj.read(self.cursor, self.uid, user_id, ['password', 'force_change_password'])
        self.assertNotEqual(res['password'], hashlib.sha256(password).hexdigest())
        self.assertEqual(res['force_change_password'], True)

        email_ids = pm_mailbox_obj.search(self.cursor, self.uid, [
            ('pem_subject', 'ilike', "Reset contrasenya a l'Oficina Virtual de")
        ])
        self.assertEqual(
            len(email_ids),
            1
        )

        email = pm_mailbox_obj.read(self.cursor, self.uid, email_ids[0])
        new_password = re.findall('Contrasenya: (.*)</li>', email['pem_body_text'])[0]

        result = ov_user_obj.login(self.cursor, self.uid, user, new_password)
        self.assertEqual(result, user_id)

    def test_create_ov_user_for_partner(self):
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        ov_user_obj = self.openerp.pool.get('ov.users')

        partner_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'base', 'res_partner_asus'
        )[1]

        partner = partner_obj.read(self.cursor, self.uid, partner_id, ['name'])

        user = partner['name']
        phone = '+34 123 456 789'
        password = user

        user_id = partner_obj.enable_ov_user(
            self.cursor, self.uid, partner_id, user, 'test@example.com', phone,
            password
        )

        ov_user = ov_user_obj.read(self.cursor, self.uid, user_id, load='_classic_write')

        params = ov_user['password'].split('$')
        h = Hasher(password, params[2], params[0], int(params[1]))
        password_hashed = '{}${}${}${}'.format(
            DEFAULT_HASH_ALG, h.iterations, h.salt, h.salted
        )

        self.assertDictContainsSubset({
            'user': user,
            'password': password_hashed,
            'partner_id': partner_id
        }, ov_user)

    def test_change_password(self):
        cursor = self.cursor
        uid = self.uid
        pool = self.openerp.pool

        partner_obj = pool.get('res.partner')
        imd_obj = pool.get('ir.model.data')
        ov_user_obj = pool.get('ov.users')

        partner_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'base', 'res_partner_asus'
        )[1]

        user = 'test@example.com'
        password = user

        user_id = partner_obj.enable_ov_user(
            cursor, uid, partner_id, 'test@example.com', 'test@example.com',
            '11111', password
        )

        result = ov_user_obj.login(cursor, uid, user, password)
        self.assertEqual(result, user_id)

        new_password = 'new_{}'.format(password)
        ov_user_obj.change_password(cursor, uid, user, password, new_password)

        result = ov_user_obj.login(cursor, uid, user, new_password)
        self.assertEqual(result, user_id)

    def test_login_correct(self):
        cursor = self.cursor
        uid = self.uid
        pool = self.openerp.pool

        partner_obj = pool.get('res.partner')
        imd_obj = pool.get('ir.model.data')
        ov_user_obj = pool.get('ov.users')

        partner_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'base', 'res_partner_asus'
        )[1]

        user = 'test@example.com'
        password = user

        user_id = partner_obj.enable_ov_user(
            cursor, uid, partner_id, 'test@example.com', 'test@example.com',
            '11111', password
        )

        result = ov_user_obj.login(cursor, uid, user, password)
        self.assertEqual(result, user_id)

    def test_login_md5_correct(self):
        cursor = self.cursor
        uid = self.uid
        pool = self.openerp.pool

        partner_obj = pool.get('res.partner')
        imd_obj = pool.get('ir.model.data')
        ov_user_obj = pool.get('ov.users')

        partner_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'base', 'res_partner_asus'
        )[1]

        user = 'test@example.com'
        salt = '1234'
        password = user

        user_id = partner_obj.enable_ov_user(
            cursor, uid, partner_id, 'test@example.com', 'test@example.com',
            '11111', password
        )
        md5_hashed = hashlib.md5(salt + password).hexdigest()
        ov_user_obj.write(cursor, uid, [user_id], {
            'password': 'md5${}${}'.format(salt, md5_hashed)
        })

        result = ov_user_obj.login(cursor, uid, user, password)
        self.assertEqual(result, user_id)

    def test_no_salted_converted_to_salted(self):
        cursor = self.cursor
        uid = self.uid
        pool = self.openerp.pool

        partner_obj = pool.get('res.partner')
        imd_obj = pool.get('ir.model.data')
        ov_user_obj = pool.get('ov.users')

        partner_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'base', 'res_partner_asus'
        )[1]

        user = 'test@example.com'
        password = user

        user_id = partner_obj.enable_ov_user(
            cursor, uid, partner_id, 'test@example.com', 'test@example.com',
            '11111', password
        )

        result = ov_user_obj.login(cursor, uid, user, password)
        self.assertEqual(result, user_id)

        params = ov_user_obj.read(cursor, uid, user_id, ['password'])['password'].split('$')
        self.assertEqual(len(params), 4)
        self.assertEqual(params[0], DEFAULT_HASH_ALG)
        self.assertEqual(int(params[1]), DEFAULT_HASH_ITERATIONS)
        hasher = getattr(hashlib, DEFAULT_HASH_ALG)
        hashed = binascii.hexlify(
            hashlib.pbkdf2_hmac(
                DEFAULT_HASH_ALG, bytes(password), bytes(params[2]), int(params[1])
            )
        )
        self.assertEqual(params[3], hashed)

        result = ov_user_obj.login(cursor, uid, user, password)
        self.assertEqual(result, user_id)
