# coding=utf-8
from __future__ import absolute_import

from osv import osv, fields
from .giscedata_ov import password_generator, recreate_password_value


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def enable_ov_user(self, cursor, uid, partner_id, user, email, mobile=None,
                       password=None, context=None):
        if context is None:
            context = {}
        if isinstance(partner_id, (list, tuple)):
            partner_id = partner_id[0]
        if password is None:
            password = password_generator()
        ctx = context.copy()
        ctx['ov_password'] = password
        ov_user = self.pool.get('ov.users')
        partner_name = self.read(cursor, uid, partner_id, ['name'])['name']
        user_id = ov_user.create(cursor, uid, {
            'name': partner_name,
            'user': user,
            'password': recreate_password_value(password),
            'partner_id': partner_id,
            'email': email,
            'mobile': mobile,
            'type': 'ov'
        }, context=ctx)
        return user_id

    _columns = {
        'ov_users_ids': fields.one2many(
            'ov.users', 'partner_id', u'Usuaris Oficina Virtual',
            context={'active_test': False}
        )
    }


ResPartner()
