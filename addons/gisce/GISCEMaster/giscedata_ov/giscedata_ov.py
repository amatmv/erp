# coding=utf-8
import binascii
from osv import osv, fields
from osv.orm import browse_record
import hashlib
import string
import random
from tools import config
import logging
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer


logger = logging.getLogger('openerp.ov')

DEFAULT_HASH_ALG = 'sha256'
DEFAULT_HASH_ITERATIONS = 1000000


def password_generator(size=8, chars=string.ascii_letters + string.digits):
    """
    Returns a string of random characters, useful in generating temporary
    passwords for automated password resets.

    size: default=8; override to provide smaller/larger passwords
    chars: default=A-Za-z0-9; override to provide more/less diversity

    Credit: Ignacio Vasquez-Abrams
    Source: http://stackoverflow.com/a/2257449
    """
    return ''.join(random.choice(chars) for _ in range(size))


class Hasher(object):
    def __init__(
            self, password, salt=None, alg=DEFAULT_HASH_ALG, iterations=0,
            key_deriv='pbkdf2_hmac'
    ):
        if iterations == 0:
            self.alg = getattr(hashlib, alg, None)
            self.iterations = 0
            if self.alg is None:
                raise ValueError(
                    'Algorithm {} is not supported by hashlib'.format(alg)
                )
            self.password = password
            self.salt = salt
        else:
            self.key_deriv = getattr(hashlib, key_deriv, None)
            self.alg = alg
            self.iterations = iterations
            if self.key_deriv is None:
                raise ValueError(
                    'Algorithm {} is not supported by hashlib pbkdf2_hmac'.format(alg)
                )
            self.password = password
            self.salt = salt

    def check(self, hashed):
        return hashed == self.salted

    def renew_salt(self):
        self.salt = password_generator()

    @property
    def salted(self):
        if self.salt and self.iterations > 0:
            password = binascii.hexlify(
                self.key_deriv(
                    self.alg, bytes(self.password), bytes(self.salt), int(self.iterations)
                )
            )
        elif self.salt:
            salted_password = '{}{}'.format(self.salt, self.password)
            password = self.alg(salted_password).hexdigest()
        else:
            salted_password = self.password
            password = self.alg(salted_password).hexdigest()
        return password


def recreate_password_value(password):
    h = Hasher(password, iterations=DEFAULT_HASH_ITERATIONS)
    h.renew_salt()
    return '{}${h.iterations}${h.salt}${h.salted}'.format(DEFAULT_HASH_ALG, h=h)


class OVUsers(osv.osv):
    _name = 'ov.users'
    _inherits = {'res.partner.address': 'partner_address_id'}

    def login(self, cursor, uid, user, password):
        # Always use superuser ID

        q = self.q(cursor, uid)
        result = q.read(['id', 'password'], limit=1).where([
            ('user', '=', user)
        ])
        if result:
            result = result[0]
            params = result['password'].split('$')
            new_values = {}
            iterations = 0
            if len(params) == 4:
                hash_method, iterations, salt, hashed_password = params
                iterations = int(iterations)
            elif len(params) == 3:
                hash_method, salt, hashed_password = params
            elif len(params) == 2:
                salt, hashed_password = params
                hash_method = DEFAULT_HASH_ALG
            else:
                raise ValueError('Invalid value')

            h = Hasher(password, salt, hash_method, iterations)
            if h.check(hashed_password):
                if len(params) != 4:
                    logger.info('Generating a new hash with salt')
                    self.write(cursor, uid, [result['id']], {
                        'password': recreate_password_value(password)
                    })
                return result['id']
        return False

    def serialize(self, cursor, uid, user, context=None):
        if context is None:
            context = {}
        if not isinstance(user, (browse_record, int)):
            raise ValueError('Invalid user parameter')
        if not isinstance(user, browse_record):
            user = self.browse(cursor, uid, user, context)
        partner = user.partner_id
        data = {
            'id': user.id,
            'name': user.name,
            'user_id': str(partner.id),
            'login': user.user,
            'email': user.email,
            'mobile': user.mobile,
            'address_id': user.partner_address_id.id,
            'force_change_password': user.force_change_password,
            'readonly': False
        }
        if partner.lang:
            data['locale'] = partner.lang
        return data

    def change_password(self, cursor, uid, login, current, password):
        res = self.login(cursor, uid, login, current)
        if res:
            new_hashed = recreate_password_value(password)
            self.write(cursor, 1, [res], {
                'password': new_hashed,
                'force_change_password': 0
            })
            return True
        return False

    def reset_password(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        for user_id in ids:
            ctx = context.copy()
            new_password = password_generator()
            ctx['reset_password'] = new_password
            self.write(cursor, uid, [user_id], {
                'password': recreate_password_value(new_password),
                'force_change_password': 1
            }, context=ctx)
        return True

    def _fnc_token(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict(
            (_id, {'token': '', 'view_as_user_url': ''}) for _id in ids
        )
        secret = config.get('ov_secret')
        base_url = config.get('ov_base_url')
        if not secret:
            return res
        expires = config.get('ov_token_expires')
        logger.debug('Using secret: {} and expires: {}'.format(secret, expires))
        if expires:
            expires = int(expires)
        s = Serializer(secret, expires)

        for user in self.browse(cursor, uid, ids, context=context):
            data = self.serialize(cursor, uid, user)
            data.update({'readonly': True})
            token = s.dumps(data).decode('utf-8')
            res[user.id]['token'] = token
            res[user.id]['view_as_user_url'] = '{}?token={}'.format(
                base_url, token
            )
        return res

    def _default_pass_change(self, cursor, uid, context=None):
        conf_obj = self.pool.get('res.config')
        ov_default_force_pass_change = conf_obj.get(
            cursor, uid, 'ov_default_force_pass_change', '1'
        )
        return ov_default_force_pass_change

    _columns = {
        'user': fields.char(u'Username', size=255, required=True),
        'password': fields.char(u'Password', size=255, required=True),
        'partner_address_id': fields.many2one(
            'res.partner.address', 'Partner Address'),
        'token': fields.function(
            _fnc_token, type='text', method=True, string='Token', multi='token'
        ),
        'view_as_user_url': fields.function(
            _fnc_token, type='text', method=True, string='URL', multi='token'
        ),
        'force_change_password': fields.boolean('Force change password')
    }

    _sql_constraints = [
        ('user_uniq', 'unique (user)', 'The name of the user must be unique !'),
    ]

    _defaults = {
        'active': lambda *a: True,
        'force_change_password': _default_pass_change
    }


OVUsers()
