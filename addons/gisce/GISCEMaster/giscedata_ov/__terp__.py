# coding=utf-8
# -*- coding: utf-8 -*-
{
  "name": "Oficina Virtual",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Basic models to GISCE Oficina Virtual
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
    'base',
    'base_extended',
    'poweremail',
    'poweremail_references',
    'ws_transactions',
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['security/giscedata_ov_security.xml', 'security/ir.model.access.csv', 'wizard/ov_user_enable_view.xml', 'giscedata_ov_view.xml', 'partner_view.xml', 'giscedata_ov_data.xml'],
  "active": False,
  "installable": True
}
