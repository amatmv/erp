# coding=utf-8
import pooler
import logging


def up(cursor, installed_version):
    uid = 1
    logger = logging.getLogger('openerp.migration')
    if not installed_version:
        return

    logger.info(
        'Moving ov.users\' email field to res.partner.address\'s '
        'email field.'
    )
    pool = pooler.get_pool(cursor.dbname)
    ov_user_obj = pool.get('ov.users')
    partner_addr_obj = pool.get('res.partner.address')
    partner_obj = pool.get('res.partner')
    query_del = """
            SELECT id, partner_id_pre_2_98_0, email 
            FROM ov_users 
            WHERE partner_address_id is NULL 
        """
    cursor.execute(query_del)
    for info in cursor.dictfetchall():
        name = partner_obj.read(cursor, uid, info['partner_id_pre_2_98_0'], ['name'])['name']
        partner_addr_id = partner_addr_obj.create(cursor, uid, {
            'partner_id': info['partner_id_pre_2_98_0'],
            'type': 'ov',
            'email': info['email'],
            'name': name
        })
        ov_user_obj.write(cursor, uid, [info['id']], {'partner_address_id': partner_addr_id})


def down(cursor, installed_version):
    pass


migrate = up
