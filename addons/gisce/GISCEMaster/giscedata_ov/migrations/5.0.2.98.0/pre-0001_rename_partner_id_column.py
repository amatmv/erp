# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if oopgrade.column_exists(cursor, 'ov_users', 'partner_id'):
        oopgrade.rename_columns(cursor, {
            'ov_users': [('partner_id', 'partner_id_pre_2_98_0')]
        })


def down(cursor, installed_version):
    if oopgrade.column_exists(cursor, 'ov_users', 'partner_id_pre_2_98_0'):
        oopgrade.rename_columns(cursor, {
            'ov_users': [('partner_id_pre_2_98_0', 'partner_id')]
        })
