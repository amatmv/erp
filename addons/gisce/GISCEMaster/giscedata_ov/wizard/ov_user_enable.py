# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from giscedata_ov.giscedata_ov import password_generator


class OvUserEnable(osv.osv_memory):
    _name = 'wizard.ov.user.enable'

    def _default_partner(self, cursor, uid, context=None):
        return context['active_id']

    def _default_email(self, cursor, uid, context=None):
        addr_obj = self.pool.get('res.partner.address')
        sql = addr_obj.q(cursor, uid).select(['email'], limit=1).where([
            ('partner_id', '=', context['active_id']),
            ('email', '!=', None)
        ])
        cursor.execute(*sql)
        if cursor.rowcount:
            return cursor.fetchone()[0]
        return False

    def _default_mobile(self, cursor, uid, context=None):
        addr_obj = self.pool.get('res.partner.address')
        sql = addr_obj.q(cursor, uid).select(['mobile'], limit=1).where([
            ('partner_id', '=', context['active_id']),
            ('mobile', '!=', None)
        ])
        cursor.execute(*sql)
        if cursor.rowcount:
            return cursor.fetchone()[0]
        return False

    def _default_user(self, cursor, uid, context=None):
        partner_obj = self.pool.get("res.partner")
        partner_vat = partner_obj.read(cursor, uid, context['active_id'], ['vat'])['vat']
        if partner_vat:
            partner_vat = partner_vat[2:]
        return partner_vat

    def action_alta_usuari_ov(self, cursor, uid, ids, context=None):
        if not context:
            context = None
        wiz = self.browse(cursor, uid, ids[0], context=context)
        partner = wiz.partner_id
        if not wiz.force_password:
            wiz.password = password_generator()
        partner.enable_ov_user(
            wiz.user, wiz.email, wiz.mobile, wiz.password, context=context)
        info = _(u"S'ha donat d'alta un nou usuari pel client %s amb les següents dades: \n\n"
                 u" - Usuari: %s\n"
                 u" - Contrasenya: %s\n"
                 u" - E-mail: %s" % (partner.name, wiz.user, wiz.password, wiz.email))
        wiz.write({
            'state': 'end',
            'info': info
        })
        return True

    _columns = {
        'state': fields.char(u'Estat', size=16),
        'partner_id': fields.many2one('res.partner', 'Client', readonly=True),
        'user': fields.char(u'Usuari', size=255),
        'force_password': fields.boolean('Forçar contrasenya'),
        'password': fields.char(u'Contrasenya', size=255),
        'email': fields.char(
            u'E-mail',
            size=256,
            help=u"Cal introduïr una adreça de correu vàlida per tal de poder "
                 u"activar l'usuari correctament."
        ),
        'mobile': fields.char(u'Telèfon Mòbil', size=256, required=True),
        'info': fields.text(_(u'Informació'), readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'email': _default_email,
        'mobile': _default_mobile,
        'partner_id': _default_partner,
        'user': _default_user,
        'force_password': lambda *a: 0
    }


OvUserEnable()
