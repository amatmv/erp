#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
import pandas as pd
import sys
import click
from tqdm import tqdm

@click.command()
@click.option('-d', '--database', required=True, help='Nom de la DB')
@click.option('-l', '--user', required=True, default='admin', help='Usuari ERP')
@click.option('-w', '--password', required=True, help=u'Clau usuari ERP')
@click.option('-p', '--port', default='8069', help='Port servidor ERP', type=click.INT)
@click.option('-f', '--file', required=True, help='Fitxer CSV a processar')
@click.option('-u', '--uri', default="http://local", help='URI protocol://host')
def create_ov_user(**kwargs):
    O = OOOP(dbname=kwargs['database'], user=kwargs['user'], pwd=kwargs['password'],
             port=kwargs['port'], uri=kwargs['uri'])

    df = pd.read_csv(kwargs['file'], delimiter=";")

    with tqdm(total=len(df.index)) as pbar:
        for row in df.iterrows():
            pbar.update(1)
            polissa_name = str(row[1].contract)
            password = str(row[1].password)

            print (polissa_name)
            try:
                search_params = [
                    ('name', "=", polissa_name)
                ]
                polissa_id = O.GiscedataPolissa.search(search_params, 0, 0, False, {'active_test': False})
                polissa = O.GiscedataPolissa.read(polissa_id, ['pagador'] )[0]
            except Exception as e:
                print ("ERROR reaching payer for contract: {} '{}'".format(polissa_name, e))
                continue

            try:
                search_params = [
                    ('id', '=', polissa['pagador'][0])
                ]
                partner_ids = O.ResPartner.search(search_params)
            except Exception as e:
                print ("ERROR reaching partner for contract: {} '{}'".format(polissa_name, e))
                continue

            for partner_id in partner_ids:
                pagador = O.ResPartner.read([partner_id], ['vat'])[0]
                username = pagador['vat'].replace("ES","")
                try:
                    O.ResPartner.enable_ov_user(partner_id, username, password)
                except Exception as e:
                    print ("ERROR creating OV user for contract: {} [{},{},{}] '{}'".format(polissa_name,
                                                                                            partner_id,
                                                                                            username,
                                                                                            password, e))
                    continue


if __name__ == '__main__':
    create_ov_user()
