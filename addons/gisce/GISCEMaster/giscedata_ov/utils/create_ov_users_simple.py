#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ooop import OOOP
import pandas as pd
import sys
import click
from tqdm import tqdm


@click.command()
@click.option('-d', '--database', required=True, help='Nom de la DB')
@click.option('-l', '--user', required=True, default='admin', help='Usuari ERP')
@click.option('-w', '--password', required=True, help=u'Clau usuari ERP')
@click.option('-p', '--port', default='8069', help='Port servidor ERP', type=click.INT)
@click.option('-f', '--file', required=True, help='Fitxer CSV a processar')
@click.option('-u', '--uri', default="http://local", help='URI protocol://host')
def create_ov_user(**kwargs):
    O = OOOP(dbname=kwargs['database'], user=kwargs['user'], pwd=kwargs['password'],
             port=kwargs['port'], uri=kwargs['uri'])

    df = pd.read_csv(kwargs['file'], delimiter=";")

    with tqdm(total=len(df.index)) as pbar:
        for row in df.iterrows():
            pbar.update(1)
            partner = str(row[1].id)
            vat = str(row[1].vat)
            password = str(row[1].passwd)

            try:
                search_params = [
                    ('id', '=', partner)
                ]
                partner_ids = O.ResPartner.search(search_params)
            except Exception as e:
                print ("ERROR reaching partner:{} '{}".format(partner, e))
                continue

            for partner_id in partner_ids:
                pagador = O.ResPartner.read([partner_id], ['vat'])[0]
                assert pagador['vat'] == vat
                username = pagador['vat'].replace("ES","")

                try:
                    O.ResPartner.enable_ov_user(partner_id, username, password)
                except Exception as e:
                    print ("ERROR creating OV user for partner:{} [{},{}] '{}".format(partner,
                                                                          username,
                                                                          password, e))


if __name__ == '__main__':
    create_ov_user()

