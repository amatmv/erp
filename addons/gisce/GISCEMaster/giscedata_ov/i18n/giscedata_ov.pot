# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_ov
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2018-11-22 06:54\n"
"PO-Revision-Date: 2018-11-22 06:54\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_ov
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_ov
#: field:ov.users,active:0
msgid "Actiu"
msgstr ""

#. module: giscedata_ov
#: field:ov.users,email:0
#: field:wizard.ov.user.enable,email:0
msgid "E-mail"
msgstr ""

#. module: giscedata_ov
#: field:wizard.ov.user.enable,state:0
msgid "Estat"
msgstr ""

#. module: giscedata_ov
#: code:addons/giscedata_ov/wizard/ov_user_enable.py:62
#: view:wizard.ov.user.enable:0
#: field:wizard.ov.user.enable,info:0
#, python-format
msgid "Informació"
msgstr ""

#. module: giscedata_ov
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_ov
#: view:wizard.ov.user.enable:0
msgid "Activació d'usuaris a la Oficina Virtual"
msgstr ""

#. module: giscedata_ov
#: help:wizard.ov.user.enable,email:0
msgid "Cal introduïr una adreça de correu vàlida per tal de poder activar l'usuari correctament."
msgstr ""

#. module: giscedata_ov
#: field:ov.users,user:0
msgid "Username"
msgstr ""

#. module: giscedata_ov
#: field:wizard.ov.user.enable,user:0
msgid "Usuari"
msgstr ""

#. module: giscedata_ov
#: model:poweremail.templates,def_subject:giscedata_ov.alta_usuari_email
msgid "Alta Usuari a l'Oficina Virtual de ${env['user'].company_id.name}"
msgstr ""

#. module: giscedata_ov
#: field:ov.users,password:0
msgid "Password"
msgstr ""

#. module: giscedata_ov
#: view:wizard.ov.user.enable:0
msgid "Tanca"
msgstr ""

#. module: giscedata_ov
#: model:ir.model,name:giscedata_ov.model_wizard_ov_user_enable
msgid "wizard.ov.user.enable"
msgstr ""

#. module: giscedata_ov
#: model:ir.model,name:giscedata_ov.model_ov_users
msgid "ov.users"
msgstr ""

#. module: giscedata_ov
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_ov
#: field:wizard.ov.user.enable,partner_id:0
msgid "Client"
msgstr ""

#. module: giscedata_ov
#: view:wizard.ov.user.enable:0
msgid "Donar d'alta"
msgstr ""

#. module: giscedata_ov
#: model:ir.module.module,description:giscedata_ov.module_meta_information
msgid "Aquest mòdul afegeix les següents funcionalitats:\n"
"  * Basic models to GISCE Oficina Virtual\n"
""
msgstr ""

#. module: giscedata_ov
#: view:wizard.ov.user.enable:0
msgid "Client a activar"
msgstr ""

#. module: giscedata_ov
#: model:ir.actions.act_window,name:giscedata_ov.action_wizard_ov_user_enable_form
msgid "Activar usuari a l'Oficina Virtual"
msgstr ""

#. module: giscedata_ov
#: model:poweremail.templates,def_body_text:giscedata_ov.alta_usuari_email
msgid "\n"
"\n"
"<!doctype html>\n"
"<html>\n"
"<head></head>\n"
"<body>\n"
"Benvolgut/da ${object.partner_id.name},\n"
"<br>\n"
"L'informem que hem creat un usuari de l'Oficina Virtual amb les següents credencials:\n"
"<br>\n"
"<ul>\n"
"    <li>Usuari: ${object.user}</li>\n"
"    <li>Contrasenya: ${env['ov_password']}</li>\n"
"</ul>\n"
"<br><br>\n"
"Atentament,\n"
"\n"
"${env['user'].company_id.name}\n"
"</body>\n"
"</html>\n"
"\n"
"            "
msgstr ""

#. module: giscedata_ov
#: field:wizard.ov.user.enable,force_password:0
msgid "Forçar contrasenya"
msgstr ""

#. module: giscedata_ov
#: field:wizard.ov.user.enable,password:0
msgid "Contrasenya"
msgstr ""

#. module: giscedata_ov
#: model:ir.module.module,shortdesc:giscedata_ov.module_meta_information
#: view:ov.users:0
#: view:res.partner:0
msgid "Oficina Virtual"
msgstr ""

#. module: giscedata_ov
#: code:addons/giscedata_ov/wizard/ov_user_enable.py:43
#, python-format
msgid "S'ha donat d'alta un nou usuari pel client %s amb les següents dades: \n"
"\n"
" - Usuari: %s\n"
" - Contrasenya: %s\n"
" - E-mail: %s"
msgstr ""

#. module: giscedata_ov
#: field:res.partner,ov_users_ids:0
msgid "Usuaris Oficina Virtual"
msgstr ""

#. module: giscedata_ov
#: field:ov.users,partner_id:0
msgid "Partner"
msgstr ""

