from __future__ import absolute_import
from base.res.partner import ADDRESS_TYPE_SEL


if ('ov', 'Oficina Virtual') not in ADDRESS_TYPE_SEL:
    ADDRESS_TYPE_SEL.append(('ov', 'Oficina Virtual'))
