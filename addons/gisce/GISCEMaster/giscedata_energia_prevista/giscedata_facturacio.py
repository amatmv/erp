# -*- coding: utf-8 -*-
from datetime import datetime

from osv import osv, fields
from oorq.decorators import job
from addons.mongodb_backend import osv_mongodb

from .energia import Factura, Consum, Normalizer, extreure_periode


class GiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def invoice_open(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        # Background processing.
        for fid in ids:
            self.normalize(cursor, uid, fid)
        return super(GiscedataFacturacioFactura,
                     self).invoice_open(cursor, uid, ids, context)

    @job(queue='invoice_normalizer')
    def normalize(self, cursor, uid, factura_id, context=None):
        if context is None:
            context = {}
        mensual_obj = self.pool.get('giscedata.facturacio.mensual')
        lectdia_obj = self.pool.get('giscedata.energia.facturada.diaria')
        factura = self.browse(cursor, uid, factura_id)
        if not factura.journal_id.code.startswith('ENERGIA'):
            return False
        # Normalitzem la facturació
        data_inici = datetime.strptime(factura.data_inici, '%Y-%m-%d').date()
        data_final = datetime.strptime(factura.data_final, '%Y-%m-%d').date()
        fact = Factura(factura.cups_id.name, data_inici, data_final,
                       factura.amount_total)
        men_ids = mensual_obj.search(cursor, uid, [
            ('datetime', '>=', factura.data_inici),
            ('datetime', '<=', factura.data_final),
            ('cups', '=', factura.cups_id.name)
        ])
        if men_ids:
            mensual_obj.unlink(cursor, uid, men_ids)
        norm = Normalizer([fact], 'total')
        cups = factura.cups_id.name
        comercial = factura.polissa_id.user_id.id
        mcp = factura.polissa_id.mcp
        for d in norm.normalize():
            val = {'datetime': d[0].strftime('%Y-%m-%d'), 'total': d[1],
                   'cups': cups, 'comercial': comercial}
            mensual_obj.create(cursor, uid, val)

        # Normalitzem les energies facturades
        # 1. Passem tots els periodes a totalitzadors (P0)
        consums = []
        for lectura in factura.lectures_energia_ids:
            if lectura.tipus != 'activa':
                continue
            periode = extreure_periode(lectura.name)
            di = datetime.strptime(lectura.data_anterior, '%Y-%m-%d').date()
            df = datetime.strptime(lectura.data_actual, '%Y-%m-%d').date()
            c = Consum(cups, periode, di, df, lectura.consum)
            consums.append(c)

        totals = Consum.totalizer(consums)
        norm = Normalizer(totals, 'consum')
        normalized = norm.normalize()
        if not normalized:
            return False
        # 2. Busquem si ja existeixen lectures per aquestes dates i les
        #    eliminem
        lect_ids = lectdia_obj.search(cursor, uid, [
            ('cups', '=', cups),
            ('date', '>=', normalized[0][0].strftime('%Y-%m-%d')),
            ('date', '<=', normalized[-1][0].strftime('%Y-%m-%d'))
        ])
        if lect_ids:
            lectdia_obj.unlink(cursor, uid, lect_ids)
        # 3. Normalitzem
        for d in normalized:
            vals = {
                'cups': cups,
                'comercial': comercial,
                'date': d[0].strftime('%Y-%m-%d'),
                'consum': d[1],
                'mcp': mcp
            }
            lectdia_obj.create(cursor, uid, vals)

        return True

GiscedataFacturacioFactura()


class GiscedataFacturacioMensual(osv_mongodb.osv_mongodb):
    _name = 'giscedata.facturacio.mensual'

    _columns = {
        'cups': fields.char('CUPS', size=22),
        'comercial': fields.integer('Comercial'),
        'datetime': fields.date('Data inici'),
        'total': fields.integer('Total')
    }


GiscedataFacturacioMensual()


