# -*- coding: utf-8 -*-

from osv import osv, fields
from addons.mongodb_backend.mongodb2 import mdbpool


class GiscedataCartera(osv.osv):
    _name = 'giscedata.cartera'

    def create_or_update(self, cursor, uid, comercial=None, context=None):
        # Busquem els contractes que te aquest comercial
        # Fem una consulta directe al mongo
        col = mdbpool.get_collection("giscedata_energia_prevista_diaria")
        energia_pipeline = [
            {
                '$group': {
                    '_id': {
                        'month': {'$month': "$date"},
                        'year': {'$year': "$date"},
                        'comercial': "$comercial",
                        'cups': "$cups",
                    },
                    'mcpCupsMes': {'$sum': {'$multiply': ["$mcp", "$consum"]}},
                    'consumMes': {'$sum': "$consum" },
                },
            },
            {
                '$group': {
                    '_id': {
                        'month': "$_id.month",
                        'year': "$_id.year",
                        'comercial': "$_id.comercial"
                    },
                    'mcpCups': {'$sum': "$mcpCupsMes"},
                    'total': {'$sum': "$consumMes"}
                }
            },
            {
                '$project': {
                    '_id': 0,
                    'month': "$_id.month",
                    'year': "$_id.year",
                    'comercial': "$_id.comercial",
                    'consum_previst': "$total",
                    'mcp': {'$divide': ["$mcpCups", "$total"]}
                }
            }
        ]
        idx = 2
        if comercial:
            idx += 1
            energia_pipeline.insert(0, {'$match': {'comercial': comercial}})
        mcr = col.aggregate(pipeline=energia_pipeline)
        result = mcr.get("result", [])
        col = mdbpool.get_collection("giscedata_energia_facturada_diaria")
        # Modifiquem el pipeline pel facturat
        facturat_pipeline = energia_pipeline[:]
        del facturat_pipeline[idx]['$project']['consum_previst']
        facturat_pipeline[idx]['$project']['consum_facturat'] = "$total"
        del facturat_pipeline[idx]['$project']['mcp']
        facturat_pipeline[idx]['$project']['mcr'] = {'$divide': ["$mcpCups", "$total"]}
        mcr = col.aggregate(pipeline=facturat_pipeline)
        facturat_result = mcr.get("result")
        if facturat_result:
            result.extend(facturat_result)
        col = mdbpool.get_collection("giscedata_facturacio_mensual")
        facturacio_pipeline = [
            {
                '$group': {
                    '_id': {
                        'month': {'$month': "$datetime"},
                        'year': {'$year': "$datetime"},
                        'comercial': "$comercial",
                    },
                    'facturacio': {'$sum': "$total"},
                }
            },
            {
                '$project': {
                    '_id': 0,
                    'month': "$_id.month",
                    'year': "$_id.year",
                    'comercial': "$_id.comercial",
                    'facturacio': 1,
                },
            }
        ]
        if comercial:
            facturacio_pipeline.insert(0, {'$match': {'comercial': comercial}})
        mcr = col.aggregate(pipeline=facturacio_pipeline)
        facturacio_result = mcr.get("result")
        if facturacio_result:
            result.extend(facturacio_result)
        # Reduce
        reduced = {}
        for res in result:
            key = '{year}_{month}_{comercial}'.format(**res)
            reduced.setdefault(key, {})
            reduced[key].update(res)
        vals = reduced.values()
        for value in vals:
            self.create(cursor, uid, value)
        return vals

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        key = ('comercial', 'month', 'year')
        search_params = []
        for k in key:
            search_params += [(k, '=', vals[k])]
        ids = self.search(cursor, uid, search_params)
        if ids:
            self.write(cursor, uid, ids, vals)
            res_id = ids[0]
        else:
            res_id = super(GiscedataCartera, self).create(cursor, uid, vals)
        return res_id

    _columns = {
        'mcp': fields.float('MCP Consum previst (€)'),
        'mcr': fields.float('MCP Consum facturat (€)'),
        'comercial': fields.many2one('res.users', 'Comercial', required=True, select=True),
        'month': fields.integer('Mes', required=True, select=True),
        'year': fields.integer('Año', required=True, select=True),
        'consum_previst': fields.integer('Consum Previst (kWh)'),
        'consum_facturat': fields.integer('Consum Facturat (kWh)'),
        'facturacio': fields.integer('Facturació (€)')
    }

    _order = "year asc, month asc, comercial asc"


GiscedataCartera()