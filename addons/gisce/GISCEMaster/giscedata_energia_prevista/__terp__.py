# -*- coding: utf-8 -*-
{
    "name": "Energia prevista",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Energia prevista per una pòlissa
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "mongodb_backend",
        "giscedata_facturacio",
        "oorq"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cartera_view.xml"
    ],
    "active": False,
    "installable": True
}
