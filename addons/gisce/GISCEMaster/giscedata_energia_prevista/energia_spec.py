from expects import *
from energia import *

with description("El mòdul d'energia prevista"):
    with it("Ha de saber extreure el nom d'un període"):
        str = "P1(3.1A)"
        assert extreure_periode(str) == "P1"

        str = "12PP23"
        assert extreure_periode(str) == "P2"

    with description("Model d'energies"):
        with context('Serialització'):
            with it("ha de serialitzar a un diccionari amb dates i datetimes"):
                e = Consum('CUPS', 'P1', date(2014, 1, 1), date(2014, 1, 31), 10)
                res = e.as_dict()
                assert isinstance(res, dict)
                assert res['cups'] == 'CUPS'
                assert res['data_inici'] == '2014-01-01'
                assert res['data_final'] == '2014-01-31'
                assert res['consum'] == 10
                assert res['periode'] == 'P1'

                assert set(res.keys()) == {'cups', 'data_inici', 'data_final',
                                           'consum', 'periode'}

        with it("ha de poder sumar un any a les dates de les lectures"):

            c = ConsumPrevist('XX', 'P1', date(2014, 12, 1), date(2013, 12, 31), 10)
            r = c.reproduce(years=1)
            assert id(r) != id(c)
            assert isinstance(r, ConsumPrevist)
            assert r.cups == 'XX'
            assert r.periode == 'P1'
            assert r.data_inici == date(2015, 12, 1)
            assert r.data_final == date(2014, 12, 31)
            assert r.consum == 10

            c = ConsumPrevist('XX', 'P1', date(2015, 1, 1), date(2014, 1, 31), 10)
            r = c.reproduce(years=-1)
            assert r.data_inici == date(2014, 1, 1)
            assert r.data_final == date(2013, 1, 31)

            c = ConsumPrevist('XX', 'P1', date(2015, 2, 1), date(2015, 2, 28), 10)
            r = c.reproduce(years=1)
            assert r.data_inici == date(2016, 2, 1)
            assert r.data_final == date(2016, 2, 29)

            c = ConsumPrevist('XX', 'P1', date(2016, 2, 1), date(2016, 2, 29), 10)
            r = c.reproduce(years=-1)
            assert r.data_inici == date(2015, 2, 1)
            assert r.data_final == date(2015, 2, 28)

    with description("Normalitzant l'energia"):
        with context("Passant una llista de consums"):
            with before.each:
                self.consums = [
                    Consum('XX', 'P1', date(2014, 1, 1), date(2014, 1, 31), 10),
                    Consum('XX', 'P1', date(2013, 12, 1), date(2013, 12, 31), 10),
                    Consum('XX', 'P1', date(2014, 2, 1), date(2014, 2, 28), 10)
                ]
            with it("ha de tenir els conums ordenats sempre"):

                consums = list(self.consums)
                norm = Normalizer(consums, 'consum')
                data_ant = None
                for c in norm.items:
                    if data_ant is not None:
                        assert c.data_inici > data_ant
                    data_ant = c.data_inici

            with it("ha de saber totalitzar consums de diferents períodes"):
                consums = list(self.consums)
                consums.extend([
                    Consum('CUPS', 'P2', date(2014, 1, 1), date(2014, 1, 31), 1),
                    Consum('CUPS', 'P2', date(2013, 12, 1), date(2013, 12, 31), 2),
                    Consum('CUPS', 'P2', date(2014, 2, 1), date(2014, 2, 28), 3)
                ])
                total_consums = Consum.totalizer(consums)
                assert len(total_consums) == 3
                assert total_consums[0].consum == 12
                assert total_consums[1].consum == 11
                assert total_consums[2].consum == 13

            with it("no ha de permetre forats entre els consums"):
                consums = list(self.consums)
                consums.append(Consum('XX', 'P1', date(2014, 3, 3), date(2014, 3, 31), 10))

                def callback():
                    check_gaps_dates(consums)

                expect(callback).to(raise_error(GapsException))

            with it("no ha de permetre interseccions de consums"):
                consums = list(self.consums)
                consums.append(Consum('XX', 'P1', date(2014, 2, 12), date(2014, 3, 31), 10))

                def callback():
                    check_gaps_dates(consums)

                expect(callback).to(raise_error(InersectionException))

            with it("ha d'arreglar les dates si la final és igual a l'anterior"):

                consums = list(self.consums)
                consums.extend([
                    Consum('XX', 'P1', date(2014, 2, 28), date(2014, 4, 1), 10),
                    Consum('XX', 'P1', date(2014, 4, 1), date(2014, 4, 30), 10),
                ])

                norm = Normalizer(consums, 'consum')
                assert len(norm.items) == 5

                consums = [
                    Consum('XX', 'P1', None, date(2014, 2, 28), 10),
                    Consum('XX', 'P1', None, date(2014, 3, 31), 10)
                ]
                consums = check_gaps_dates(consums)
                assert consums[0].data_inici is None
                assert consums[0].data_final == date(2014, 2, 28)
                assert consums[1].data_inici == date(2014, 3, 1)
                assert consums[1].data_final == date(2014, 3, 31)


            with it("ha de retornar consums normalitzats amb la granulatitat que li diem"):
                consums = [
                    Consum('XX', 'P1', date(2014, 1, 1), date(2014, 1, 31), 100),
                    Consum('XX', 'P1', date(2013, 12, 1), date(2013, 12, 31), 200),
                    Consum('XX', 'P1', date(2014, 2, 1), date(2014, 2, 28), 150)
                ]
                totals = Consum.totalizer(consums)
                norm = Normalizer(totals, 'consum')
                cons = norm.normalize()
                assert len(cons) == 31 + 31 + 28
                total_before = sum(c.consum for c in consums)
                total_norm = sum(c[1] for c in cons)
                assert total_before == total_norm
                consum_q = 0
                for c in cons:
                    if date(2014, 1, 1) <= c[0] <= date(2014, 12, 31):
                        consum_q += c[1]
                assert consum_q == 250
