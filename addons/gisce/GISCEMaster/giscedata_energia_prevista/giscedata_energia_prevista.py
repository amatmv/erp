# -*- coding: utf-8 -*-
import time
from osv import osv, fields
from addons.mongodb_backend import osv_mongodb
from addons.oorq.decorators import job

from .energia import *


PERIODES_SEL = [(x, x) for x in PERIODES]


class GiscedataEnergiaPrevistaConsums(osv.osv):
    _name = 'giscedata.energia.prevista.consums'

    def _ff_comercial(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        pol_obj = self.pool.get('giscedata.polissa')
        for consum in self.read(cursor, uid, ids, ['cups']):
            pol_ids = pol_obj.search(cursor, uid, [('cups.id', '=', consum['cups'][0])])
            for pol in pol_obj.read(cursor, uid, pol_ids, ['user_id', 'cups']):
                res[consum['id']] = pol['user_id'][0]
        return res

    _columns = {
        'cups': fields.many2one('giscedata.cups.ps', 'CUPS'),
        'comercial': fields.function(
            _ff_comercial, method=True, type='many2one', relation='res.users',
            string='Comercial', store=True
        ),
        'data_inici': fields.date('Data inici'),
        'data_final': fields.date('Data final'),
        'periode': fields.selection(PERIODES_SEL, 'Periode', size=2),
        'consum': fields.integer('Consum'),
    }

    def reproduce(self, cursor, uid, cups_id, year, years):
        cursor.execute("SELECT id from giscedata_energia_prevista_consums "
        "where (to_char(data_inici, 'YYYY') = %s or to_char(data_final, 'YYYY') = %s) "
        "and cups = %s", (str(year), str(year), cups_id))
        ids = [x[0] for x in cursor.fetchall()]
        res = []
        self.normalize_energia_prevista(cursor, uid, ids)
        for consum in self.read(cursor, uid, ids):
            for d in ('data_inici', 'data_final'):
                consum[d] = datetime.strptime(consum[d], '%Y-%m-%d')
            c = ConsumPrevist(consum['cups'][0], consum['periode'],
                              consum['data_inici'], consum['data_final'],
                              consum['consum'])
            r = c.reproduce(years=years)
            res.append(self.create(cursor, uid, r.as_dict()))
        self.normalize_energia_prevista(cursor, uid, res)
        return res

    @job(queue='energia_prevista_normalizer')
    def normalize_energia_prevista(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        ene_obj = self.pool.get('giscedata.energia.prevista.diaria')
        pol_obj = self.pool.get('giscedata.polissa')
        consums = []
        read_consums = self.read(cursor, uid, ids)
        max_time = 5
        elapsed_time = 0
        while not read_consums:
            time.sleep(0.5)
            elapsed_time += 0.5
            if elapsed_time > max_time:
                raise Exception('Timeout waiting consums')
            read_consums = self.read(cursor, uid, ids)
        for consum in read_consums:
            for d in ('data_inici', 'data_final'):
                consum[d] = datetime.strptime(consum[d], '%Y-%m-%d')
            c = ConsumPrevist(consum['cups'][1], consum['periode'],
                              consum['data_inici'], consum['data_final'],
                              consum['consum'])
            comercial = consum['comercial'][0]
            consums.append(c)
        totals = Consum.totalizer(consums)
        norm = Normalizer(totals, 'consum')
        # Només si > data alta contracte
        pol_id = pol_obj.search(cursor, uid, [
            ('cups.name', '=', totals[0].cups)]
        )
        if pol_id:
            polissa = pol_obj.browse(cursor, uid, pol_id[0])
            data_alta = polissa.data_alta
            mcp = polissa.mcp
        else:
            data_alta = '0000-00-00'
            mcp = 0
        hours = norm.normalize()
        unlink_ids = ene_obj.search(cursor, uid, [
            ('date', '>=', hours[0][0].strftime('%Y-%m-%d')),
            ('date', '<=', hours[-1][0].strftime('%Y-%m-%d')),
            ('cups', '=', totals[0].cups)
        ])
        ene_obj.unlink(cursor, uid, unlink_ids)
        for n in hours:
            date_str = n[0].strftime('%Y-%m-%d')
            if date_str >= data_alta:
                val = {
                    'date': date_str, 'consum': n[1],
                    'cups': totals[0].cups,
                    'comercial': comercial,
                    'mcp': mcp
                }
                ene_obj.create(cursor, uid, val)
        return True


GiscedataEnergiaPrevistaConsums()


class GiscedataEnergiaPrevistaDiaria(osv_mongodb.osv_mongodb):
    _name = 'giscedata.energia.prevista.diaria'

    _columns = {
        'cups': fields.char('CUPS', size=22),
        'comercial': fields.integer('Comercial'),
        'date': fields.date('Data'),
        'consum': fields.integer('Consum'),
        'mcp': fields.float('MCP'),
    }

GiscedataEnergiaPrevistaDiaria()


class GiscedataEnergiaFacturadaDiaria(osv_mongodb.osv_mongodb):
    _name = 'giscedata.energia.facturada.diaria'

    _columns = {
        'cups': fields.char('CUPS', size=22),
        'comercial': fields.integer('Comercial'),
        'date': fields.date('Data'),
        'consum': fields.integer('Consum'),
        'mcp': fields.float('MCP')
    }

GiscedataEnergiaFacturadaDiaria()
