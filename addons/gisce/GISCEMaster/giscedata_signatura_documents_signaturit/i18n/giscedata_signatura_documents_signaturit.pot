# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_signatura_documents_signaturit
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-29 13:42\n"
"PO-Revision-Date: 2019-07-29 13:42\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,type:0
msgid "Simple"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "Preparado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,status:0
msgid "Expirado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.documents:0
msgid "Documents"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.recipients,type:0
msgid "Tipo"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "Borrador"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,reminders:0
msgid "Recordatorio"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,delivery_type:0
msgid "SMS"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: model:ir.module.module,shortdesc:giscedata_signatura_documents_signaturit.module_meta_information
msgid "Signatura de documents (Signaturit)"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
#: selection:giscedata.signatura.process,type:0
msgid "Avanzado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,delivery_type:0
#: field:giscedata.signatura.recipients,email:0
msgid "E-mail"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,create_uid:0
msgid "Creador"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.recipients,name:0
msgid "Nombre"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "Firmando"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: help:giscedata.signatura.process,all_signed:0
msgid "Si se marca esta casilla todos los documentos deben de estar firmados para poder continuar con el proceso ya que son bloqueantes."
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,doc_file:0
msgid "Documento"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.recipients,type:0
msgid "Firmante"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: model:ir.model,name:giscedata_signatura_documents_signaturit.model_giscedata_signatura_process
msgid "giscedata.signatura.process"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.recipients,type:0
msgid "Validador"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.recipients,phone:0
msgid "Teléfono"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
#: field:giscedata.signatura.process,status:0
msgid "Estado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "Rechazado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Generar"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,type:0
msgid "Tipo de firma"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "Caducado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,expire_time:0
msgid "Expiración (días)"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,files:0
msgid "Documentos  "
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:ir.actions.report.xml,signature_in_coordinates:0
msgid "Signature Coordinates"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
#: selection:giscedata.signatura.process,status:0
msgid "Completado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "En cola"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,status:0
msgid "En Espera"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: model:ir.module.module,description:giscedata_signatura_documents_signaturit.module_meta_information
msgid "Módul de signatura de documents (Signaturit)"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Signatura digital"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,delivery_type:0
msgid "Modo de envio"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,status:0
msgid "En Proceso"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,delivery_type:0
#: field:giscedata.signatura.process,signature_url:0
msgid "URL"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Código QR"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Todos firmados"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,subject:0
msgid "Asunto"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,report_id:0
msgid "Report"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,qr:0
msgid "QR"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: model:ir.model,name:giscedata_signatura_documents_signaturit.model_wizard_signature_model
msgid "wizard.signature.model"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
#: selection:giscedata.signatura.process,status:0
msgid "Error"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,model_str:0
msgid "Model"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: model:ir.model,name:giscedata_signatura_documents_signaturit.model_giscedata_signatura_recipients
msgid "giscedata.signatura.recipients"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Mensaje"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,signature_id:0
msgid "Id signatura"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,write_date:0
msgid "Fecha de modificación"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.process,type:0
msgid "Automático"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Documentos"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,status:0
msgid "Status"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
#: field:giscedata.signatura.process,recipients:0
msgid "Destinatarios"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Iniciar"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,data:0
msgid "Data"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:ir.actions.report.xml:0
msgid "Posición firma digital"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.recipients,process_id:0
msgid "Process"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,body:0
msgid "Texto"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,all_signed:0
msgid "Todos los documentos firmados"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "General"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,cc:0
msgid "Copias"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,process_id:0
msgid "Signatura"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
msgid "Actualizar"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.recipients,partner_address_id:0
msgid "Partner"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: selection:giscedata.signatura.documents,status:0
msgid "Cancelado"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: view:giscedata.signatura.process:0
#: model:ir.actions.act_window,name:giscedata_signatura_documents_signaturit.action_giscedata_signatura_process_tree
#: model:ir.ui.menu,name:giscedata_signatura_documents_signaturit.menu_giscedata_signatura_process_tree
msgid "Signatures digitals"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.process,signature_id:0
msgid "Id"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,category_id:0
msgid "Categoria del Adjunto"
msgstr ""

#. module: giscedata_signatura_documents_signaturit
#: field:giscedata.signatura.documents,filename:0
msgid "Nombre fichero"
msgstr ""

