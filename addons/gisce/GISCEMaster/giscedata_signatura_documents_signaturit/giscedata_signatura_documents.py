# -*- coding: utf-8 -*-
import os
import json
from StringIO import StringIO
import logging
from osv import osv, fields
from signaturit_sdk.signaturit_client import SignaturitClient
from tools import config
import base64
import tempfile
import netsvc
from slugify import slugify
import qrcode
from mongodb_backend.fields import gridfs


logger = logging.getLogger('openerp.' + __name__)


def get_signaturit_client():
    client = SignaturitClient(
        config['signaturit_token'],
        config.get('signaturit_production', False)
    )
    return client


class GiscedataSignaturaProcess(osv.osv):
    _name = 'giscedata.signatura.process'
    _rec_name = 'subject'

    def signed_document(self, cursor, uid, signature, doc, context=None):
        client = get_signaturit_client()
        return client.download_signed_document(signature, doc)

    def default_attach_document(self, cursor, uid, resource_id, signature_id,
                                signature_doc_id, doc_filename, model, context=None):
        if context is None:
            context = {}
        attach_obj = self.pool.get('ir.attachment')
        doc_obj = self.pool.get('giscedata.signatura.documents')
        doc_id = doc_obj.search(cursor, uid, [('signature_id', '=', signature_doc_id)])
        signed_document = self.signed_document(
            cursor, uid, signature_id, signature_doc_id, context=context)
        document_data = doc_obj.read(
            cursor, uid, doc_id[0], ['category_id'])
        vals = {
            'name': doc_filename,
            'datas': base64.b64encode(signed_document),
            'datas_fname': doc_filename,
            'res_model': model,
            'res_id': resource_id,
        }
        if document_data:
            if document_data['category_id']:
                vals['category_id'] = document_data['category_id'][0]

        attach_obj.create(cursor, uid, vals)

    def start(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        for signature in self.browse(cursor, uid, ids, context):
            if signature.signature_id:
                continue

            document_obj = self.pool.get('giscedata.signatura.documents')

            tmp_dir = tempfile.mkdtemp(prefix='signaturit-')
            logger.debug('Creating a directory to save files: {}'.format(
                tmp_dir
            ))
            
            documents = []
            signatures_coords = []
            for document in signature.files:
                if document.model and document.report_id:
                    logger.debug('Generating report')
                    document.generate_report(context=context)
                    # Hack to refresh object
                    document_data = document_obj.read(
                        cursor, uid, document.id, ['filename', 'doc_file']
                    )
                    if document.report_id.signature_in_coordinates:
                        signatures_coords.append(
                            json.loads(document.report_id.signature_in_coordinates)
                        )
                    document_path = os.path.join(
                        tmp_dir, document_data['filename']
                    )
                    with open(document_path, 'w') as tmp_document:
                        tmp_document.write(base64.b64decode(document_data['doc_file']))
                        documents.append(document_path)

            client = get_signaturit_client()
            recipients = []
            for idx, recipient in enumerate(signature.recipients):
                recipient_dict = {
                    'name': recipient.name,
                    'email': recipient.email
                }
                if idx == 0 and signatures_coords:
                    recipient_dict.update({
                        'require_signature_in_coordinates': signatures_coords
                    })
                recipients.append(recipient_dict)
            response = client.create_signature(
                documents,
                recipients,
                {
                    'subject': signature.subject,
                    'body': signature.body,
                    'delivery_type': signature.delivery_type,
                    'expire_time': signature.expire_time,
                    'reminders': signature.reminders,
                    'type': signature.type,
                    'cc': [
                        {'name': x.name, 'email': x.email}
                        for x in signature.cc
                    ],
                    'data': signature.data
                }
            )
            values = {
                'signature_id': response['id'],
                'status': 'wait'
            }
            if signature.delivery_type == 'url':
                values['signature_url'] = response['url']
            signature.write(values)
            signature.generate_qr()
            for doc, rdoc in zip(signature.files, response['documents']):
                doc.write({
                    'signature_id': rdoc['id'],
                    'status': rdoc['status']
                })
        return True

    def update_process_cron(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wait_status = ('wait', 'doing')
        search_params = [('status', 'in', wait_status)]
        all_ids = self.search(cursor, uid, search_params, context=context)
        self.update(cursor, uid, all_ids, context=context)

    def update(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        client = get_signaturit_client()
        doc_obj = self.pool.get('giscedata.signatura.documents')
        for signature in self.browse(cursor, uid, ids, context):
            completed = signature.status == 'completed'
            if completed:
                continue
            response = client.get_signature(signature.signature_id)
            remote_status = dict(
                (rdoc['id'], rdoc['status']) for rdoc in response['documents']
            )
            data = signature.data
            all_signed = signature.all_signed
            signed = []
            for doc in signature.files:
                status = remote_status.get(doc.signature_id)
                if status:
                    doc.write({'status': status})
                    if status == 'completed':
                        signed.append(True)
                        signature.write({'status': 'doing'})
                    else:
                        signed.append(False)
                else:
                    signed.append(False)
            if ((all_signed and all(signed)) or not all_signed):
                signature_file = self.read(
                    cursor, uid, signature.id, ['files'])['files']
                for document in signature_file:
                    search_params = ['model', 'signature_id', 'id', 'filename', 'status']
                    doc = doc_obj.read(cursor, uid, document, search_params)
                    if doc['status'] == 'completed':
                        model, resource_id = doc['model'].split(',')
                        if model:
                            resource_id = int(resource_id)
                            obj = self.pool.get(model)
                            method = getattr(
                                obj, 'process_signature_callback', None,
                            )
                            if method:
                                ctx = context.copy()
                                ctx['signature_document_id'] = doc['id']
                                ctx['process_data'] = data
                                method(cursor, uid, resource_id, context=ctx)

                            # Attachment
                            resource_id = int(resource_id)
                            obj = self.pool.get(model)
                            method = getattr(
                                obj, 'process_attachment_callback', None,
                            )
                            ctx = context.copy()
                            ctx['document_signature_id'] = doc['signature_id']
                            ctx['signature_signature_id'] = signature.signature_id
                            ctx['process_data'] = data
                            if method:
                                method(
                                    cursor, uid, resource_id, doc['filename'],
                                    signature.signature_id, doc['signature_id'],
                                    model, context=ctx
                                )
                            else:
                                self.default_attach_document(
                                    cursor, uid, resource_id, signature.signature_id,
                                    doc['signature_id'], doc['filename'], model,
                                    context=ctx
                                )
                if all(signed):
                    signature.write({'status': 'completed'})

        return True
    
    def generate_qr(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        for signature in self.browse(cursor, uid, ids):
            if signature.signature_url:
                qr = qrcode.QRCode()
                qr.add_data(signature.signature_url)
                img = qr.make_image()
                buff = StringIO()
                img.save(buff, format='PNG')
                signature.write({
                    'qr': base64.b64encode(buff.getvalue())
                })
                buff.close()
        return True

    _columns = {
        'signature_id': fields.char('Id', size=36),
        'subject': fields.char('Asunto', size=256, required=True),
        'body': fields.text('Texto'),
        'delivery_type': fields.selection(
            [
                ('email', 'E-mail'),
                ('sms', 'SMS'),
                ('url', 'URL')
            ], "Modo de envio"
        ),
        'expire_time': fields.integer('Expiración (días)'),
        'files': fields.one2many(
            'giscedata.signatura.documents', 'process_id', 'Documentos  '
        ),
        'recipients': fields.one2many(
            'giscedata.signatura.recipients', 'process_id', 'Destinatarios'
        ),
        'cc': fields.many2many(
            'res.partner.address',
            'giscedata_signatura_cc_rel',
            'process_id',
            'address_id',
            'Copias'
        ),
        'reminders': fields.integer(
            'Recordatorio'
        ),
        'type': fields.selection(
            [
                ('simple', 'Simple'),
                ('advanced', 'Avanzado'),
                ('smart', 'Automático')
            ], 'Tipo de firma'
        ),
        'data': fields.json('Data'),
        'all_signed': fields.boolean(
            'Todos los documentos firmados',
            help="Si se marca esta casilla todos los documentos deben de estar "
                 "firmados para poder continuar con el proceso ya que son"
                 " bloqueantes."),
        'signature_url': fields.char('URL', size=256),
        'qr': fields.binary('QR'),
        'write_date': fields.datetime('Fecha de modificación', readonly=True,
                                      required=False),
        'create_uid': fields.many2one('res.users', 'Creador',
                                      readonly=True, required=False),
        'status': fields.selection(
            [
                ('wait', 'En Espera'),
                ('doing', 'En Proceso'),
                ('completed', 'Completado'),
                ('error', 'Error'),
                ('expired', 'Expirado')
            ], 'Estado'
        )
    }

    _defaults = {
        'delivery_type': lambda *a: 'email',
        'reminders': lambda *a: 0,
        'type': lambda *a: 'advanced',
        'all_signed': lambda *a: False
    }

    _order = 'write_date desc'


GiscedataSignaturaProcess()


class GiscedataSignaturaRecipients(osv.osv):
    _name = 'giscedata.signatura.recipients'

    def on_change_partner_address_id(self, cursor, uid, ids, address_id,
        context=None):
        if context is None:
            context = {}
        res = {'value': {}}
        if address_id:
            addr_obj = self.pool.get('res.partner.address')
            rfields = ['name', 'email', 'mobile']
            addr = addr_obj.read(cursor, uid, address_id, rfields)
            res['value'] = {
                'name': addr['name'],
                'email': addr['email'],
                'phone': addr['mobile']
            }
        return res

    _columns = {
        'process_id': fields.many2one(
            'giscedata.signatura.process', 'Process',
            required=True,
            ondelete='cascade'
        ),
        'partner_address_id': fields.many2one(
            'res.partner.address',
            'Partner'
        ),
        'name': fields.char('Nombre', size=256, required=True),
        'email': fields.char('E-mail', size=256, required=True),
        'phone': fields.char('Teléfono', size=15),
        'type': fields.selection(
            [
                ('signer', 'Firmante'),
                ('validator', 'Validador')
            ], 'Tipo'
        )
    }

    _defaults = {
        'type': lambda *a: 'signer',
    }


GiscedataSignaturaRecipients()


class GiscedataSignaturaDocuments(osv.osv):
    _name = 'giscedata.signatura.documents'
    _inherit = 'giscedata.signatura.documents'

    def generate_report(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        for document in self.browse(cursor, uid, ids, context=context):
            if document.report_id and document.model:
                report_name = document.report_id.report_name
                rs = netsvc.LocalService('report.{}'.format(report_name))
                model, model_id = document.model.split(',')
                model_id = int(model_id)
                if model != document.report_id.model:
                    logger.error('Model is {} and report is {}'.format(
                        model, document.report_id.model
                    ))
                report_ids = [model_id]
                datas = {'active_id': model_id}
                result, format = rs.create(
                    cursor, uid, report_ids, datas, context=context)
                document.write({
                    'doc_file': base64.b64encode(result),
                    'filename': '{}.{}'.format(
                        slugify('{}-{}'.format(document.report_id.name, model_id)),
                        format
                    )
                })
    
    def _fnc_model_str(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}.fromkeys(ids, '')
        for r in self.read(cursor, uid, ids, ['model']):
            if not r['model']:
                continue
            m = r['model'].split(',')
            if len(m) == 2:
                name = self.pool.get(m[0]).name_get(
                    cursor, uid, [int(m[1])]
                )
                if name:
                    res[r['id']] = name[0][1]
        return res

    _columns = {
        'model_str': fields.function(
            _fnc_model_str,
            string='Model',
            type='char',
            size=256,
            method=True
        ),
        'filename': fields.char(
            'Nombre fichero', size=256
        ),
        'signature_id': fields.char(
            'Id signatura', size=36,
            readonly=True
        ),
        'process_id': fields.many2one(
            'giscedata.signatura.process',
            'Signatura',
            required=True,
            ondelete='cascade'
        ),
        'report_id': fields.many2one(
            'ir.actions.report.xml',
            'Report'
        ),
        'doc_file': gridfs(
            'Documento'
        ),
        'status': fields.selection(
            [
                ('draft', 'Borrador'),
                ('in_queue', 'En cola'),
                ('ready', 'Preparado'),
                ('signing', 'Firmando'),
                ('completed', 'Completado'),
                ('expired', 'Caducado'),
                ('canceled', 'Cancelado'),
                ('declined', 'Rechazado'),
                ('error', 'Error')
            ], 'Status',
            readonly=True
        ),
        'category_id': fields.many2one(
            'ir.attachment.category', 'Categoria del Adjunto'
        )
    }

    _defaults = {
        'status': lambda *a: 'draft',
    }

    _order = "id asc"


GiscedataSignaturaDocuments()
