# -*- coding: utf-8 -*-
{
    "name": "Signatura de documents (Signaturit)",
    "description": """Módul de signatura de documents (Signaturit)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_signatura_documents",
        "mongodb_backend",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml": [
        "giscedata_signatura_documents_view.xml",
        "giscedata_signatura_data.xml",
        "ir_actions_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
