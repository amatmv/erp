# coding: utf-8
from osv import osv, fields


class IrActionsReportXml(osv.osv):
    _name = 'ir.actions.report.xml'
    _inherit = 'ir.actions.report.xml'

    _columns = {
        'signature_in_coordinates': fields.text('Signature Coordinates')
    }


IrActionsReportXml()
