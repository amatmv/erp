# -*- coding: utf-8 -*-
from osv import osv


class WizardSignatureModel(osv.osv_memory):
    _name = 'wizard.signature.model'

    def list_all_processes(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not context.get('model_name') or not context.get('active_ids'):
            return {}

        model = context['model_name']
        processes_ids = []
        if model == 'res.partner':
            address_obj = self.pool.get('res.partner.address')
            recipient_obj = self.pool.get('giscedata.signatura.recipients')
            active_ids = context.get("active_ids", [])

            # Busco les adreces dels partners
            address_ids = address_obj.search(
                cursor, uid, [('partner_id', 'in', active_ids)]
            )

            # Busco els recipients del proces que tenen l'adreça del partner
            recipient_ids = recipient_obj.search(
                cursor, uid, [('partner_address_id', 'in', address_ids)]
            )

            # Busco els procesos que tenen els recipients trobats
            process = recipient_obj.read(
                cursor, uid, recipient_ids, ['process_id']
            )
            process = [x['process_id'][0] for x in process if x['process_id']]
            processes_ids.extend(process)

        else:
            document_obj = self.pool.get('giscedata.signatura.documents')
            for active_id in context.get('active_ids'):
                model_name = model + ',' + str(active_id)
                search_params = [('model', '=', model_name)]
                documents_ids = document_obj.search(cursor, uid, search_params)
                for document_id in documents_ids:
                    process = document_obj.read(
                        cursor, uid, document_id, ['process_id']
                    )
                    if process['process_id']:
                        processes_ids.append(process['process_id'][0])
        return {
            'domain': [('id', 'in', processes_ids)],
            'name': 'Procesos de firma relacionados',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.signatura.process',
            'type': 'ir.actions.act_window',
            'view_id': False
        }


WizardSignatureModel()
