# -*- coding: utf-8 -*-

from osv import fields, osv

class giscedata_bt_caixes(osv.osv):
  _name = 'giscedata.bt.caixes'
  _inherit = 'giscedata.bt.caixes'

  def read_distinct(self, cr, uid, values, id_municipi):
    cr.execute("select e.name,e.id, e.id_municipi as id_municipi from giscedata_bt_caixes e where e.id_municipi = %s and e.name::int not in ("+','.join(map(str, map(int, values)))+")", (int(id_municipi),))
    return cr.dictfetchall()

  def read_distinct2(self, cr, uid, values, id_municipi):
    in_array = []
    for value in values:
      cr.execute("select name from giscedata_bt_caixes where name = %s and id_municipi = %s", (str(value), int(id_municipi)))
      if not cr.fetchone():
        in_array.append(value)
    return in_array
  
  _columns = {
      'id_municipi': fields.many2one('res.municipi','Municipi', required=True),
  }

giscedata_bt_caixes()

