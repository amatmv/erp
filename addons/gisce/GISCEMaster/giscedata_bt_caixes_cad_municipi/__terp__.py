# -*- coding: utf-8 -*-
{
    "name": "GISCE Data BT Caixes CAD",
    "description": """Funcions per les eines CAD a les caixes BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CAD",
    "depends":[
        "giscedata_bt_caixes"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
