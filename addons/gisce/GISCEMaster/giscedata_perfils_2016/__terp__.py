# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2016)",
    "description": """
Actualitza els valors de perfilació per l'any 2016
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_perfils"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_2016_data.xml"
    ],
    "active": False,
    "installable": True
}
