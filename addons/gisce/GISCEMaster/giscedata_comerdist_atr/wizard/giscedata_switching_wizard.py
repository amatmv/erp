# -*- coding: utf-8 -*-
import base64
import collections
from ast import literal_eval

import pooler
from osv import osv
from tools.translate import _

from erppeek import Client


class GiscedataSwitchingWizard(osv.osv_memory):
    _name = 'giscedata.switching.wizard'
    _inherit = 'giscedata.switching.wizard'

    def get_user_id(self, atr_case):
        user_id = False
        if atr_case.section_id.user_id:
            user_id = atr_case.section_id.user_id.id
        return user_id

    def get_passos_pendents_enviament(self, cursor, uid, sw_id, context=None):
        '''Retorna un ordered diccionari amb:
         {date_create : {step_id, header_id, stepname, pas_id } }
        amb els passos pendents d'enviar del cas sw_id, ordenats per date_create
        '''
        step_header = {}
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        proces_obj = self.pool.get('giscedata.switching.proces')

        sw_br = sw_obj.browse(cursor, uid, sw_id)
        proces_name = sw_br.proces_id.name
        whereiam = sw_br.whereiam
        emisorsteps = proces_obj.get_emisor_steps(cursor, uid, proces_name, whereiam)
        stepinfos = sw_br.step_ids

        hids = header_obj.search(cursor, uid, [('sw_id', '=', sw_id),
                                               ('enviament_pendent', '=', True)])
        headers = header_obj.read(cursor, uid, hids)

        # mirar per cada header amb quins passos del procés encaixa
        # per obtenir el step_id
        for info in stepinfos:
            mdl = self.pool.get(info.pas_id.split(',')[0])
            for header in headers:
                encaixa = mdl.search(cursor, uid,
                                     [('header_id','=',header['id'])])
                if encaixa and info.step_id.name in emisorsteps:
                    model = "giscedata.switching.{0}.{1}".format(proces_name.lower(), info.step_id.name)
                    pas_id = self.pool.get(model).search(
                        cursor, uid, [('header_id', '=', header['id'])], limit=1
                    )[0]
                    d = {'stepinfo': info,
                         'header': header,
                         'stepname': info.step_id.name,
                         'pas_id': pas_id}
                    step_header.update({header['date_created']: d})
        od = collections.OrderedDict(sorted(step_header.items()))

        return od

    def action_enviar_xml(self, cursor, uid, ids, context=None):
        """En el cas que sigui una empresa configurada a comerdist via XML-RPC.
        """
        sw_obj = self.pool.get('giscedata.switching')
        comerdist_config = self.pool.get('giscedata.comerdist.config')
        res_config = self.pool.get('res.config')

        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        # Tots els passos del/s cas/os seleccionat/s pendents d'enviar han
        # de tenir el mateix receptor. Tant si es multicas com si es un sol cas
        sw_ids = context['active_ids']
        sw_pendents = {}
        rcpts = []
        for sw_id in sw_ids:
            od = self.get_passos_pendents_enviament(cursor, uid, sw_id)
            sw_pendents[sw_id] = od
            for date, d in od.iteritems():
                if wizard.step_id and eval(wizard.step_id)[0] == d['stepinfo'].step_id.id:
                    rcpts.append(d['header']['receptor_id'])
                elif not wizard.step_id:
                    rcpts.append(d['header']['receptor_id'])

        if len(set(rcpts)) > 1 and not wizard.step_id:
            raise osv.except_osv(
                _(u"Error"),
                _(u"S'han trobat diversos receptors en els passos "
                  u"pendents d'enviar dels casos seleccionats. "
                  u"No s'ha enviat cap cas.")
            )
        if not rcpts:
            raise osv.except_osv(
                _(u"Error"),
                _(u"No hi ha passos per enviar.")
            )

        comerdist_atr = comerdist_config.search(cursor, uid, [
            ('partner_id.id', '=', rcpts[0][0]),
            ('mode', '=', 'atr')
        ], limit=1)
        if comerdist_atr:
            # Hem d'obtenir l'XML
            config = comerdist_config.browse(cursor, uid, comerdist_atr[0])
            c = Client("http://%s:%s" % (config.host, config.port),
                               db=config.dbname, user=config.user,
                               password=config.password)
            r_wiz_obj = c.GiscedataSwitchingWizard
            r_sw_obj = c.GiscedataSwitching
            info = _(u'Resum:\n\n')
            for sw_id in sw_ids:
                try:
                    remote_cr = pooler.get_db(cursor.dbname).cursor()
                    od = sw_pendents[sw_id]
                    passos_enviar = dict(od)
                    if wizard.step_id:
                        step_id, pas_id = eval(wizard.step_id)
                        passos_enviar = {}
                        for date, d in od.iteritems():
                            if (int(d['stepinfo'].step_id) == step_id and
                                    d['pas_id'] == pas_id):
                                passos_enviar[date] = d
                                break

                    if not passos_enviar:
                        info += _(u'Id {0}: No té passos pdts d\'enviar\n\n'
                                  ).format(sw_id)
                        continue

                    for date, d in passos_enviar.iteritems():
                        pas_id = d['stepinfo'].step_id
                        ctx = context.copy()
                        ctx['mark_as_sended'] = True
                        res = sw_obj.exportar_xml(cursor, uid, sw_id,
                                                  step_id=pas_id.id,
                                                  pas_id=d['pas_id'],
                                                  context=ctx)
                        if not res[2].valid:
                            # exportar_xml now manage osv exeptions, so we have to
                            # check if exportation was correct and raise them.
                            raise osv.except_osv('Error', res[2].error)

                        data = base64.encodestring(res[1])
                        ctx = context.copy()
                        remote_id = r_wiz_obj.create({'file': data,
                                                      'name': res[0]},
                                                     context=ctx).id
                        r_wiz_obj.action_importar_xml([remote_id])
                        res = r_wiz_obj.read(remote_id)
                        if isinstance(res, (list, tuple)):
                            res = res[0]
                        casos_creats = literal_eval(res['cas'])
                        if casos_creats:
                            for sw in r_sw_obj.browse(casos_creats):
                                user_id = self.get_user_id(sw)
                                if user_id:
                                    sw.write({'user_id': user_id})
                        else:
                            remote_cr.rollback()
                        info += _(u'Id {0}: {1}\n').format(sw_id, res['info'])
                        remote_cr.commit()
                except Exception as e:
                    remote_cr.rollback()
                    sentry = self.pool.get('sentry.setup')
                    if sentry:
                        sentry.client.captureException()
                    info += _(u'Id {0}: Error al sincronitzar:\n{1}\n\n').format(sw_id, e)
                finally:
                    remote_cr.close()
            wizard.write({'name': '',
                          'file': '',
                          'state': 'done',
                          'info': info})
            res = True
        else:
            parent = super(GiscedataSwitchingWizard, self)
            res = parent.action_enviar_xml(cursor, uid, ids, context=context)
        return res

GiscedataSwitchingWizard()
