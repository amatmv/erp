# -*- coding: utf-8 -*-
{
    "name": "Comerdist ATR",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Syncrhonize two databases using ATR process
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_comerdist",
        "giscedata_switching"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_comerdist_view.xml"
    ],
    "active": False,
    "installable": True
}
