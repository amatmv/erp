from osv import osv, fields


class GiscedataComerdistConfig(osv.osv):
    _name = 'giscedata.comerdist.config'
    _inherit = 'giscedata.comerdist.config'

    _columns = {
        'mode': fields.selection([
            ('auto', 'Auto'),
            ('atr', 'ATR')
        ], 'Sync mode', required=True)
    }

    _defaults = {
        'mode': lambda *a: 'auto'
    }

GiscedataComerdistConfig()
