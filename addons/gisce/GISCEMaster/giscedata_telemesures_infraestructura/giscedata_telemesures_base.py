# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataRegistradorInfraestructura(osv.osv):

    _name = 'giscedata.registrador'
    _inherit = 'giscedata.registrador'

    _columns = {
        'trafo_id': fields.many2one(
            'giscedata.transformador.trafo', 'Transformador'
        ),
        'trafo_output': fields.selection(
            [('b1', 'B1'), ('b2', 'B2')], 'Sortida del Transformador',
            required=True,
            help=u'Sortida del transformador on està vinculat el registrador'
        )
    }

    _defaults = {
        'trafo_output': lambda *a: 'b1',
    }

GiscedataRegistradorInfraestructura()