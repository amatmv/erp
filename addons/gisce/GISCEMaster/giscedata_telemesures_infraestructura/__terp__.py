# -*- coding: utf-8 -*-
{
    "name": "Telemeasures infraestructure",
    "description": """Module for telemeasures (Tg and TM) """
                   """infraestructure relation""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_telemesures_base",
        "giscedata_transformadors",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_telemesures_base_view.xml",
    ],
    "active": False,
    "installable": True
}
