# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients Informes",
    "description": """Informes dels expedients""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Expedients",
    "depends":[
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_informes_view.xml"
    ],
    "active": False,
    "installable": True
}
