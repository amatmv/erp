# -*- coding: utf-8 -*-
{
    "name": "Account Invoice Comptabilitat Reports",
    "description": """
Funcionalitats i canvis introduïts en aquest módul:

    *   Es substitueix l'assistente wizard_journal_entries_report (Imprimeix diari per asentaments) per un nou asistent. 
         Es canvia a nivell de vista, fa que el botó antic cridi a l'assistent WizardDiariApuntsPerData.

    *   Es substitueix l'assistente wizard_general_ledger (Llibre Major) per un nou asistent. 
         Es canvia a nivell de vista, fa que el botó antic cridi a l'assistent WizardDiariApuntsPerDataICompte.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "account",
        "account_financial_report",
        "account_payment_extension",
        "c2c_webkit_report",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "report/account_invoice_comptabilitat_extended_reports.xml",
        "wizard/wizard_diari_apunts_per_data_view.xml",
        "wizard/wizard_diari_apunts_per_data_i_compte_view.xml",
        "menu_view.xml",
        'security/ir.model.access.csv',
    ],
    "active": False,
    "installable": True
}
