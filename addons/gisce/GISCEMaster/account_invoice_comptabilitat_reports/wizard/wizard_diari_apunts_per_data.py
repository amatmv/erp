# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import fields, osv
from datetime import datetime
from tools.translate import _


class WizardDiariApuntsPerData(osv.osv_memory):

    _name = 'wizard.diari.apunts.per.data'

    def action_llistar_diari_apunts_per_data(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids)[0]
        vref = self.pool.get("ir.model.data").get_object_reference(cursor, uid, "account_invoice_comptabilitat_reports", "view_move_line_tree_gisce")[1]
        return {
            'domain': "[('date','<=', '{0}'), ('date','>=', '{1}')]".format(wiz.data_fi, wiz.data_inici),
            'name': _(u'Diari (apunts entre {0} i {1})').format(wiz.data_inici, wiz.data_fi),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.move.line',
            'type': 'ir.actions.act_window',
            'view_id': [vref],
            'limit': wiz.limit
        }

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        if not context:
            context = {}
        params = (wiz.data_fi, wiz.data_inici)
        return {
            'model': 'account.move.line',
            'report_webkit': "'account_invoice_comptabilitat_reports/report/llibre_diari.mako'",
            'webkit_header': "header_comptabilitat_extended_base",
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'groups_id': [],
            'type': 'ir.actions.report.xml',
            'report_name': 'giscedata.llibre.diari',
            'datas': {
                'params': params,
                'data_inici': wiz.data_inici,
                'data_fi': wiz.data_fi
            },
        }

    _columns = {
        'data_inici': fields.date('Desde', required=True),
        'data_fi': fields.date('Fins', required=True),
        'limit': fields.integer('Limit', required=True),
    }

    _defaults = {
        'data_inici': lambda *a: datetime.today().strftime("%Y-%m-%d"),
        'data_fi': lambda *a: datetime.today().strftime("%Y-%m-%d"),
        'limit': lambda *a: 80
    }


WizardDiariApuntsPerData()
