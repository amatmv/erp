# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report.report_sxw import rml_parse

# <!--Report Llibre Diari (apunts per data)-->
webkit_report.WebKitParser(
    'report.giscedata.llibre.diari',
    'account.move.line',
    'account_invoice_comptabilitat_reports/report/llibre_diari.mako',
    parser=rml_parse
)
