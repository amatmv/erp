## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    from babel.dates import format_date
    from addons import get_module_resource

    pool = objects[0].pool
    sql_path = get_module_resource('account_invoice_comptabilitat_reports', 'sql', "llibre_major.sql")
    sql = open(sql_path, "r").read()
    cursor.execute(sql, data['params'])
    mlines_datas = cursor.dictfetchall()

    max_elems_for_page = 100
    tamany_capcalera = 4

    company = pool.get("res.company").browse(cursor, uid, pool.get("res.company").search(cursor, uid, [])[0])
    cdate = datetime.today().strftime("%d-%m-%Y")
    total_mlines = len(mlines_datas)
    data_inici = format_date(datetime.strptime(data['data_inici'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')
    data_fi = format_date(datetime.strptime(data['data_fi'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')
    acc_inici = data['account_inici']
    acc_fi = data['account_fi']
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <script>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </script>
        <link rel="stylesheet" href="${addons_path}/account_invoice_comptabilitat_reports/report/llibre_major.css"/>
    </head>
    <body>
        <%
            counter = 1
            page_counter = 1
            total_debit = 0.0
            total_credit = 0.0
            current_account_code = mlines_datas and mlines_datas[0]['account_id.code'] or ""
            current_color = "#ffffff"
        %>
        <table class="taula_mlines">
            <thead>
                <tr>
                  <th colspan="7" class="capcalera_principal_taula_mlines">${_(u"LLIBRE MAJOR")}</th>
                </tr>
                <tr>
                  <th> </th>
                  <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Empresa:")}</th>
                  <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${company.name}</th>
                  <th> </th>
                  <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">Data ${cdate}</th>
                </tr>
                <tr class="fila_capcalera_secundaria">
                  <th> </th>
                  <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Condicions ")}</th>
                  <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${_(u"Subcomptes: ")}${acc_inici} / ${acc_fi}; ${_(u"Periode: ")}${data_inici} - ${data_fi}</th>
                  <th colspan="3"> </th>
                </tr>
                <tr>
                  <th class="capcalera_columnes_textes" width="10%">${_(u"ASSENT.")}</th>
                  <th class="capcalera_columnes_textes" width="10%">${_(u"DATA")}</th>
                  <th class="capcalera_columnes_textes" width="10%">${_(u"SUBCOMPTE")}</th>
                  <th class="capcalera_columnes_textes" width="40%">${_(u"CONCEPTE")}</th>
                  <th class="capcalera_columnes_credit_debit" width="10%">${_(u"DEURE")}</th>
                  <th class="capcalera_columnes_credit_debit" width="10%">${_(u"HABER")}</th>
                  <th class="capcalera_columnes_credit_debit" width="10%">${_(u"SALDO")}</th>
                </tr>
            </thead>
            <tbody>
            <tr class="fila_header_subcompte" bgcolor=${current_color}>
                <td colspan="7" class="columna_header_subcompte">&nbsp;&nbsp;&nbsp;${mlines_datas[0]['account_id.code']}&nbsp;&nbsp;&nbsp;&nbsp;${mlines_datas[0]['account_id.name']}</td>
            </tr>
            <%
                counter += tamany_capcalera
            %>
            %for mline in mlines_datas:
                %if current_account_code != mline['account_id.code']:
                    <tr class="capcalera_suma_i_sigue">
                        <td colspan="4" class="capcalera_suma_i_sigue">${_(u"Total subcompte....")}</td>
                        <td class="capcalera_suma_i_sigue_vals">${str(round(total_debit, 2))}</td>
                        <td class="capcalera_suma_i_sigue_vals">${str(round(total_credit, 2))}</td>
                        <td class="capcalera_suma_i_sigue_vals">${str(round(total_debit - total_credit, 2))}</td>
                    </tr>
                    <tr class="fila_header_subcompte" bgcolor=${current_color}>
                        <td colspan="7" class="columna_header_subcompte">&nbsp;&nbsp;&nbsp;${mline['account_id.code']}&nbsp;&nbsp;&nbsp;&nbsp;${mline['account_id.name']}</td>
                    </tr>
                    <%
                        current_account_code = mline['account_id.code']
                        total_debit = 0
                        total_credit = 0
                        counter += tamany_capcalera
                    %>
                %endif
                <tr class="fila_taula_mlines" bgcolor=${current_color}>
                    <td class="columna_taula_mlines" width="10%">${mline['move_id.name']}</td>
                    <td class="columna_taula_mlines" width="10%">${mline['date']}</td>
                    <td class="columna_taula_mlines" width="10%">${mline['account_id.code']}</td>
                    <td class="columna_taula_mlines" width="40%">${mline['ref']} | ${mline['name']}</td>
                    <td class="columna_taula_mlines_credit_debit" width="10%">${mline['debit']}</td>
                    <td class="columna_taula_mlines_credit_debit" width="10%">${mline['credit']}</td>
                    <td class="columna_taula_mlines_credit_debit" width="10%">${mline['debit'] - mline['credit']}</td>
                </tr>
                <%
                    counter += 1
                    total_debit += mline['debit']
                    total_credit += mline['credit']
                %>
                %if len((mline['ref'] or "")+" | "+(mline['name'] or "")) > 70:
                    <%
                        counter += 1
                    %>
                %endif
                %if counter >= max_elems_for_page:
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7" class="capcalera_pagina">${_(u"Pàgina ")}${page_counter}</td>
                        </tr>
                    </tfoot>
                    </table>
                    <p style="page-break-after:always"></p>
                    <%
                        counter = 0
                        page_counter += 1
                    %>
                    <table class="taula_mlines">
                        <thead>
                            <tr>
                              <th colspan="7" class="capcalera_principal_taula_mlines">${_(u"LLIBRE MAJOR")}</th>
                            </tr>
                            <tr>
                              <th> </th>
                              <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Empresa:")}</th>
                              <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${company.name}</th>
                              <th> </th>
                              <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">Data ${cdate}</th>
                            </tr>
                            <tr class="fila_capcalera_secundaria">
                              <th> </th>
                              <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Condicions ")}</th>
                              <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${_(u"Subcomptes: ")}${acc_inici} / ${acc_fi}; ${_(u"Periode: ")}${data_inici} - ${data_fi}</th>
                              <th colspan="3"> </th>
                            </tr>
                            <tr>
                              <th class="capcalera_columnes_textes" width="10%">${_(u"ASSENT.")}</th>
                              <th class="capcalera_columnes_textes" width="10%">${_(u"DATA")}</th>
                              <th class="capcalera_columnes_textes" width="10%">${_(u"SUBCOMPTE")}</th>
                              <th class="capcalera_columnes_textes" width="40%">${_(u"CONCEPTE")}</th>
                              <th class="capcalera_columnes_credit_debit" width="10%">${_(u"DEURE")}</th>
                              <th class="capcalera_columnes_credit_debit" width="10%">${_(u"HABER")}</th>
                              <th class="capcalera_columnes_credit_debit" width="10%">${_(u"SALDO")}</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr class="fila_header_subcompte" bgcolor=${current_color}>
                            <td colspan="7" class="columna_header_subcompte">&nbsp;&nbsp;&nbsp;${mline['account_id.code']}&nbsp;&nbsp;&nbsp;&nbsp;${mline['account_id.name']}</td>
                        </tr>
                        <%
                            counter += tamany_capcalera
                        %>
                %endif
            %endfor
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="capcalera_suma_i_sigue">${_(u"Total subcompte....")}</td>
                        <td class="capcalera_suma_i_sigue_vals">${str(round(total_debit, 2))}</td>
                        <td class="capcalera_suma_i_sigue_vals">${str(round(total_credit, 2))}</td>
                        <td class="capcalera_suma_i_sigue_vals">${str(round(total_debit - total_credit, 2))}</td>
                </tr>
                <tr>
                    <td colspan="7" class="capcalera_pagina">${_(u"Pàgina ")}${page_counter}</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>
