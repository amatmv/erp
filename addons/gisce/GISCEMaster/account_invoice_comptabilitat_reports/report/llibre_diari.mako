## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    from babel.dates import format_date
    from addons import get_module_resource

    pool = objects[0].pool
    sql_path = get_module_resource('account_invoice_comptabilitat_reports', 'sql', "llibre_diari.sql")
    sql = open(sql_path, "r").read()
    cursor.execute(sql, data['params'])
    mlines_datas = cursor.dictfetchall()

    max_elems_for_page = 100

    company = pool.get("res.company").browse(cursor, uid, pool.get("res.company").search(cursor, uid, [])[0])
    cdate = datetime.today().strftime("%d-%m-%Y")
    total_mlines = len(mlines_datas)
    data_inici = format_date(datetime.strptime(data['data_inici'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')
    data_fi = format_date(datetime.strptime(data['data_fi'], '%Y-%m-%d'), "dd/MM/yyyy", locale='es_ES')
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <script>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </script>
        <link rel="stylesheet" href="${addons_path}/account_invoice_comptabilitat_reports/report/llibre_diari.css"/>
    </head>
    <body>
        <%
            counter = 1
            golbal_counter = 1
            page_counter = 1
            total_debit = 0.0
            total_credit = 0.0
            current_move_id = mlines_datas and mlines_datas[0]['move_id.name'] or ""
            current_color = "#ffffff"
        %>
        <table class="taula_mlines">
            <thead>
                <tr>
                  <th colspan="7" class="capcalera_principal_taula_mlines">${_(u"LLIBRE DIARI")}</th>
                </tr>
                <tr>
                  <th> </th>
                  <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Empresa:")}</th>
                  <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${company.name}</th>
                  <th> </th>
                  <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">Data ${cdate}</th>
                </tr>
                <tr class="fila_capcalera_secundaria">
                  <th> </th>
                  <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Condicions ")}</th>
                  <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${_(u"Apunts: ")}${golbal_counter}/${total_mlines}; ${_(u"Periode: ")}${data_inici} - ${data_fi}</th>
                  <th colspan="3"> </th>
                </tr>
                <tr>
                  <th class="capcalera_columnes_textes" width="5%">${_(u"ASSENT.")}</th>
                  <th class="capcalera_columnes_textes" width="5%">${_(u"DATA")}</th>
                  <th class="capcalera_columnes_textes" width="5%">${_(u"SUBCOMPTE")}</th>
                  <th class="capcalera_columnes_textes" width="40%">${_(u"DESCRIPCIÓ")}</th>
                  <th class="capcalera_columnes_textes" width="35%">${_(u"CONCEPTE")}</th>
                  <th class="capcalera_columnes_credit_debit" width="5%">${_(u"DEURE")}</th>
                  <th class="capcalera_columnes_credit_debit" width="5%">${_(u"HABER")}</th>
                </tr>
                <tr>
                    <th colspan="5" class="capcalera_suma_i_sigue">${_(u"Suma anterior....")}</td>
                    <th class="capcalera_suma_i_sigue_vals">${total_debit}</td>
                    <th class="capcalera_suma_i_sigue_vals">${total_credit}</td>
                </tr>
            </thead>
            <tbody>
            %for mline in mlines_datas:
                %if current_move_id != mline['move_id.name']:
                    <%
                        current_move_id = mline['move_id.name']
                        if current_color == "#ffffff":
                            current_color = "#efefef"
                        else:
                            current_color = "#ffffff"
                    %>
                %endif
                <tr class="fila_taula_mlines" bgcolor=${current_color}>
                    <td class="columna_taula_mlines" width="5%">${mline['move_id.name']}</td>
                    <td class="columna_taula_mlines" width="5%">${mline['date']}</td>
                    <td class="columna_taula_mlines" width="5%">${mline['account_id.code']}</td>
                    <td class="columna_taula_mlines" width="40%">${mline['account_id.name']}</td>
                    <td class="columna_taula_mlines" width="35%">${mline['ref']} | ${mline['name']}</td>
                    <td class="columna_taula_mlines_credit_debit" width="5%">${mline['debit']}</td>
                    <td class="columna_taula_mlines_credit_debit" width="5%">${mline['credit']}</td>
                </tr>
                <%
                    counter += 1
                    golbal_counter += 1
                    total_debit += mline['debit']
                    total_credit += mline['credit']
                %>
                %if len((mline['account_id.name'] or "")) > 65 or len((mline['ref'] or "")+" | "+(mline['name'] or "")) > 60:
                    <%
                        counter += 1
                    %>
                %endif
                %if counter >= max_elems_for_page:
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5" class="capcalera_suma_i_sigue">${_(u"Suma i Segueix....")}</td>
                            <td class="capcalera_suma_i_sigue_vals">${total_debit}</td>
                            <td class="capcalera_suma_i_sigue_vals">${total_credit}</td>
                        </tr>
                        <tr>
                            <td colspan="7" class="capcalera_pagina">${_(u"Pàgina ")}${page_counter}</td>
                        </tr>
                    </tfoot>
                    </table>
                    <p style="page-break-after:always"></p>
                    <%
                        counter = 0
                        page_counter += 1
                    %>
                    <table class="taula_mlines">
                        <thead>
                            <tr>
                              <th colspan="7" class="capcalera_principal_taula_mlines">${_(u"LLIBRE DIARI")}</th>
                            </tr>
                            <tr>
                              <th> </th>
                              <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Empresa:")}</th>
                              <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${company.name}</th>
                              <th> </th>
                              <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">Data ${cdate}</th>
                            </tr>
                            <tr class="fila_capcalera_secundaria">
                              <th> </th>
                              <th class="capcalera_secundaria_taula_mlines_etiqueta">${_(u"Condicions ")}</th>
                              <th colspan="2" class="capcalera_secundaria_taula_mlines_valor">${_(u"Apunts: ")}${golbal_counter}/${total_mlines}; ${_(u"Periode: ")}${data_inici} - ${data_fi}</th>
                              <th colspan="3"> </th>
                            </tr>
                            <tr>
                              <th class="capcalera_columnes_textes" width="5%">${_(u"ASSENT.")}</th>
                              <th class="capcalera_columnes_textes" width="5%">${_(u"DATA")}</th>
                              <th class="capcalera_columnes_textes" width="5%">${_(u"SUBCOMPTE")}</th>
                              <th class="capcalera_columnes_textes" width="40%">${_(u"DESCRIPCIÓ")}</th>
                              <th class="capcalera_columnes_textes" width="35%">${_(u"CONCEPTE")}</th>
                              <th class="capcalera_columnes_credit_debit" width="5%">${_(u"DEURE")}</th>
                              <th class="capcalera_columnes_credit_debit" width="5%">${_(u"HABER")}</th>
                            </tr>
                            <tr>
                                <th colspan="5" class="capcalera_suma_i_sigue">${_(u"Suma anterior....")}</td>
                                <th class="capcalera_suma_i_sigue_vals">${total_debit}</td>
                                <th class="capcalera_suma_i_sigue_vals">${total_credit}</td>
                            </tr>
                        </thead>
                        <tbody>
                %endif
            %endfor
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="5" class="capcalera_suma_i_sigue">${_(u"Suma i Segueix....")}</td>
                    <td class="capcalera_suma_i_sigue_vals">${total_debit}</td>
                    <td class="capcalera_suma_i_sigue_vals">${total_credit}</td>
                </tr>
                <tr>
                    <td colspan="7" class="capcalera_pagina">${_(u"Pàgina ")}${page_counter}</td>
                </tr>
            </tfoot>
        </table>
    </body>
</html>
