# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report.report_sxw import rml_parse

# <!--Report Llibre Major (apunts per data i compte)-->
webkit_report.WebKitParser(
    'report.giscedata.llibre.major',
    'account.move.line',
    'account_invoice_comptabilitat_reports/report/llibre_major.mako',
    parser=rml_parse
)
