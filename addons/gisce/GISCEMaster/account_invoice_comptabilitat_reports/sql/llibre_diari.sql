SELECT
    ml.id as "id",
    mv.name as "move_id.name",
    ml.date as "date",
    acc.code as "account_id.code",
    acc.name as "account_id.name",
    ml.ref as "ref",
    ml.name as "name",
    ml.credit as "credit",
    ml.debit as "debit"
FROM
    account_move_line ml
    JOIN account_move mv ON mv.id = ml.move_id
    JOIN account_account acc ON acc.id = ml.account_id
WHERE
    ml.date <= %s
    AND ml.date >= %s
ORDER BY ml.date asc, mv.id asc, acc.code asc
