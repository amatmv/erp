SELECT
    ml.id as "id",
    mv.name as "move_id.name",
    ml.date as "date",
    acc.code as "account_id.code",
    acc.name as "account_id.name",
    ml.ref as "ref",
    ml.name as "name",
    ml.credit as "credit",
    ml.debit as "debit"
FROM
    account_move_line ml
    JOIN account_move mv ON mv.id = ml.move_id
    JOIN account_account acc ON acc.id = ml.account_id
WHERE
    ml.date <= %s
    AND ml.date >= %s
    AND RPAD(acc.code, 6, '0')::int <= RPAD(%s, 6, '9')::int
    AND  RPAD(acc.code, 6, '0')::int >= RPAD(%s, 6, '0')::int
ORDER BY acc.code asc, ml.date asc, mv.id asc
