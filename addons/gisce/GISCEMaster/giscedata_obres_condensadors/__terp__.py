# -*- coding: utf-8 -*-
{
    "name": "Obres per Condensadors",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a Condensadors
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_condensadors",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_condensadors_view.xml"
    ],
    "active": False,
    "installable": True
}
