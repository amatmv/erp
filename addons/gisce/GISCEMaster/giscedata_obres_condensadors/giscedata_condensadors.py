# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCondensadors(osv.osv):
    _name = 'giscedata.condensadors'
    _inherit = 'giscedata.condensadors'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_condensador_rel',
            'name',
            'obra_id',
            'Obres'
        )
    }

GiscedataCondensadors()
