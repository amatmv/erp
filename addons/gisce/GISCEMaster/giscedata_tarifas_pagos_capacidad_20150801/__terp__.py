# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad Agosto 2015",
    "description": """
  Actualització de les tarifes de peatges segons el BOE nº 165 - 11/07/2015.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_pagos_capacidad"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_pagos_capacidad_20150801_data.xml"
    ],
    "active": False,
    "installable": True
}
