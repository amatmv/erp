# -*- coding: utf-8 -*-
{
    "name": "GISCE CTS multicompany",
    "description": """Multi-company support for CTS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_cts"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cts_multicompany_view.xml"
    ],
    "active": False,
    "installable": True
}
