# -*- coding: utf-8 -*-
import unittest
from lxml import etree
import copy
import csv
from giscedata_escila.xml import ESCILA
from addons import get_module_resource


def assertXmlEqual(self, got, want):
    from lxml.doctestcompare import LXMLOutputChecker
    from doctest import Example

    checker = LXMLOutputChecker()
    if checker.check_output(want, got, 0):
        return
    message = checker.output_difference(Example("", want), got, 0)
    raise AssertionError(message)

unittest.TestCase.assertXmlEqual = assertXmlEqual
unittest.TestCase.__str__ = unittest.TestCase.id


class TestESCILA(unittest.TestCase):

    def setUp(self):
        self.schema_file = get_module_resource(
            'giscedata_escila',
            'xml',
            'EsquemaComercializadora v2_gisce.xsd'
        )

    def test_xml_escila(self):
        escila = ESCILA.Escila()
        escila.cabecera.feed(
            {'version': '1.0.0',
             'tipocuestionario': 'COMERCIALIZADORA',
             'tipocuestionarioid': '3',
             'anio': 2015}
        )
        escila.empresa.feed(
            {'cif_vat': 'ES12345678J',
             'id_cnae_principal': 7}
        )

        provincias = {'08': {'desc': 'BARCELONA',
                             'tarifas': {'4': ('MERCADO LIBRE hasta 10 kW', '15', '1500,0', '0,15', '2,0'), },
                             'cnaes': {'1': ('AGRICULTURA, GANADERIA, SILVICULTURA, CAZA Y PESCA', '10', '0,1')}},
                      '28': {'desc': 'MADRID',
                             'tarifas': {'5': ('MERCADO LIBRE superior a 10 kW', '15', '1500,0', '0,15', '1,0'),
                                         '4': ('MERCADO LIBRE hasta 10 kW', '20', '2000,0', '0,2', '2,5'),

                                         },
                             'cnaes': {'1': ('AGRICULTURA, GANADERIA, SILVICULTURA, CAZA Y PESCA', '15', '0,15'),
                                       '33': ('USOS DOMESTICOS', '5', '0,05')}},
                      }

        lista_provincias = []
        for p, d in provincias.items():
            provincia = ESCILA.Provincia()
            provincia.set_codi_provincia(p)

            # TARIFAS
            tarifas = ESCILA.ListaTarifas()
            lista_tarifas = []
            for t, v in d['tarifas'].items():
                tarifa = ESCILA.Tarifa()
                tarifa.set_codi_tarifa(t)
                tarifa.feed(dict(zip(tarifa.sorted_fields(), v)))
                lista_tarifas.append(copy.deepcopy(tarifa))

            tarifas.feed({'tarifas': lista_tarifas})

            # CNAES
            cnaes = ESCILA.ListaCNAES()
            lista_cnaes = []
            for c, v in d['cnaes'].items():
                cnae = ESCILA.CNAE()
                cnae.set_codi_cnae(c)
                cnae.feed(dict(zip(cnae.sorted_fields(), v)))
                lista_cnaes.append(copy.deepcopy(cnae))

            cnaes.feed({'cnaes': lista_cnaes})

            provincia.feed({'desc_provincia': d['desc'],
                            'lista_tarifas': tarifas,
                            'lista_cnaes': cnaes})
            lista_provincias.append(copy.deepcopy(provincia))

        provincias = ESCILA.ListaProvincias()
        provincias.feed({'provincias': lista_provincias})

        escila.cuestionario.feed({'observaciones': 'OBSERVACIONES',
                                  'lista_provincias': provincias})

        escila.pretty_print = True
        escila.build_tree()

        xml_file = get_module_resource(
            'giscedata_escila',
            'tests',
            'ESCILA.xml'
        )

        f = open(xml_file, 'r')
        xml_ok = f.read()
        f.close()
        self.assertXmlEqual(str(escila), xml_ok)

        with open(self.schema_file, 'r') as f:
            schema = etree.XMLSchema(file=f)
            doc = etree.XML(str(escila))
            schema.validate(doc)

    def test_data_load(self):
        cnaes_file = get_module_resource(
            'giscedata_escila',
            'tests',
            'cnaes.csv'
        )
        tarifes_file = get_module_resource(
            'giscedata_escila',
            'tests',
            'tarifes.csv'
        )
        cnaes = []

        with open(cnaes_file, 'r') as cf:
            creader = csv.reader(cf)
            for row in creader:
                for i in xrange(2, 4):
                    row[i] = int(row[i])
                cnaes.append(row)

        tarifes = []
        with open(tarifes_file, 'r') as tf:
            treader = csv.reader(tf)
            for row in treader:
                for i in xrange(2, 6):
                    row[i] = int(row[i])
                tarifes.append(row)

        escila = ESCILA.Escila()
        escila.set_vat('ES12345678J')
        escila.set_year(2015)
        escila.set_observaciones('OBSERVACIONES')
        escila.load_data(tarifes, cnaes)

        escila.pretty_print = True
        escila.build_tree()

        xml_file = get_module_resource(
            'giscedata_escila',
            'tests',
            'ESCILA.xml'
        )

        f = open(xml_file, 'r')
        xml_ok = f.read()
        f.close()

        self.assertXmlEqual(str(escila), xml_ok)

        with open(self.schema_file, 'r') as f:
            schema = etree.XMLSchema(file=f)
            doc = etree.XML(str(escila))
            schema.validate(doc)

    def test_cnae_sector(self):
        cnaes = [('9820', '33'), ('8550', '32'), ('8870', '31'), ('1', '34'),
                 ('3514', '7')]

        escila = ESCILA.Escila()

        for c in cnaes:
            assert escila.get_cnae_sector(c[0]) == c[1]
