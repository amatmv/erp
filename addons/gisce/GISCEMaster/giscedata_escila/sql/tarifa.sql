SELECT prov.code AS provincia, tarifa.name AS tarifa,
  COUNT(p.id) AS num_polisses,
  SUM(p.potencia) AS potencia,
  MAX(factures.activa) AS energia,
  MAX(factures.facturada) AS facturada,
  STRING_AGG(p.name,',')
FROM giscedata_polissa p
LEFT JOIN giscedata_polissa_tarifa AS tarifa ON (tarifa.id=p.tarifa)
LEFT JOIN giscedata_cups_ps cups ON cups.id=p.cups
LEFT JOIN res_municipi muni ON muni.id=cups.id_municipi
LEFT JOIN res_country_state prov ON prov.id=muni.state
LEFT JOIN (
  SELECT prov.id provincia, tarifa.id tarifa ,
  SUM(COALESCE(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END),0.0)) AS activa,
  SUM(COALESCE(il.price_subtotal*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END),0.0)) AS facturada
  FROM giscedata_facturacio_factura f
  LEFT JOIN account_invoice i on f.invoice_id=i.id
  LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.factura_id=f.id)
  LEFT JOIN account_invoice_line il on (fl.invoice_line_id=il.id)
  LEFT JOIN giscedata_polissa p on p.id=f.polissa_id
  LEFT JOIN giscedata_polissa_tarifa AS tarifa ON (tarifa.id=p.tarifa)
  LEFT JOIN giscedata_cups_ps cups ON cups.id=p.cups
  LEFT JOIN res_municipi muni ON muni.id=cups.id_municipi
  LEFT JOIN res_country_state prov ON prov.id=muni.state
  WHERE i.type in ('out_refund','out_invoice') AND i.date_invoice >= %(inici)s AND i.date_invoice < %(fi)s
  GROUP BY prov.id, tarifa.id
    ) AS factures ON (factures.provincia=prov.id AND factures.tarifa=tarifa.id)
WHERE ((p.data_baixa >= %(inici)s AND p.data_baixa < %(fi)s)
      OR (p.data_alta < %(fi)s AND (p.data_baixa >= %(inici)s OR data_baixa IS NULL))) AND p.state not in ('esborrany', 'cancelada')
GROUP BY prov.code, tarifa.name
ORDER BY prov.code, tarifa.name;
