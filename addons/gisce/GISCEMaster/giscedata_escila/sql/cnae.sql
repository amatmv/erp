SELECT
    prov.code provincia,
    CASE WHEN cnae.name='9821' THEN '9820'
        ELSE cnae.name
    END as cnae,
    COUNT(p.id) num_polisses,
    MAX(factures.activa)AS activa,
    STRING_AGG(p.name,',')
FROM giscedata_polissa AS p
LEFT JOIN giscemisc_cnae AS cnae ON (cnae.id=p.cnae)
LEFT JOIN giscedata_cups_ps cups ON cups.id=p.cups
LEFT JOIN res_municipi muni ON muni.id=cups.id_municipi
LEFT JOIN res_country_state prov ON prov.id=muni.state
LEFT JOIN (
    SELECT p.cnae AS cnae, prov.id AS provincia,
    SUM(COALESCE(il.quantity*(fl.tipus='energia')::int*(CASE WHEN i.type='out_refund' THEN -1 ELSE 1 END),0.0)) AS activa
    FROM giscedata_facturacio_factura AS f
    LEFT JOIN account_invoice i on f.invoice_id=i.id
    LEFT JOIN giscedata_facturacio_factura_linia fl ON (fl.factura_id=f.id and fl.tipus='energia')
    LEFT JOIN account_invoice_line il on (fl.invoice_line_id=il.id)
    LEFT JOIN giscedata_polissa p on p.id=f.polissa_id
    LEFT JOIN giscedata_cups_ps cups ON cups.id=p.cups
    LEFT JOIN res_municipi muni ON muni.id=cups.id_municipi
    LEFT JOIN res_country_state prov ON prov.id=muni.state
    WHERE i.type in ('out_refund','out_invoice') AND i.date_invoice >= %(inici)s AND i.date_invoice < %(fi)s
    GROUP BY p.cnae, prov.id
) factures ON factures.cnae=cnae.id AND factures.provincia=muni.state
WHERE ((p.data_baixa >= %(inici)s AND p.data_baixa < %(fi)s)
      OR (p.data_alta < %(fi)s AND (p.data_baixa >= %(inici)s OR data_baixa IS NULL))) AND p.state not in ('esborrany', 'cancelada')
GROUP BY prov.code, cnae.name
ORDER BY prov.code, cnae.name;
