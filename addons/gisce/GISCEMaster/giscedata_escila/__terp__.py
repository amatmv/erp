# -*- coding: utf-8 -*-
{
    "name": "Ministeri Indústria ESCILA per Comercialitzadora",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Generació de fitxer XML ESCILA per Ministeri d'Industria Energia i Turisme
    (2015) amb potència contractada agrupada per província i CNAE
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa_comer",
        "giscedata_administracio_publica_industria"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_escila_view.xml",
        "wizard/wizard_escila_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
