# -*- coding: utf-8 -*-
from libcomxml.core import XmlField, XmlModel
import copy


CNAE_SECTORS = {
    '1': {'start': ['01', '02', '03'],
          'name': u'AGRICULTURA, GANADERIA, SILVICULTURA, CAZA Y PESCA'},
    '2': {'start': ['05', '099'],
          'name': u'EXTRACCION Y AGLOMERACION DE CARBONES'},
    '3': {'start': ['06', '091'],
          'name': u'EXTRACCION DE PETROLEO Y GAS'},
    '4': {'start': ['721', '2446', '353'],
          'name': u'COMBUSTIBLES NUCLEARES Y OTRAS ENERGIAS'},
    '5': {'start': ['191'],
          'name': u'COQUERIAS'},
    '6': {'start': ['192'],
          'name': u'REFINERIAS DE PETROLEO'},
    '7': {'start': ['351'],
          'name': u'PRODUCCION Y DISTRIBUCION DE ENERGIA ELECTRICA'},
    '8': {'start': ['352'],
          'name': u'FABRICAS DE GAS-DISTRIBUCION DE GAS'},
    '9': {'start': ['07', '08'],
          'except': ['0721'],
          'name': u'MINAS Y CANTERAS (NO ENERGETICAS)'},
    '10': {'start': ['24'],
           'except': ['244', '2453', '2454'],
           'name': u'SIDERURGIA Y FUNDICION'},
    '11': {'start': ['244', '2453', '2454'],
           'name': u'METALURGIA NO FERREA'},
    '12': {'start': ['231'],
           'name': u'INDUSTRIA DEL VIDRIO'},
    '13': {'start': ['235'],
           'name': u'CEMENTO, CALES Y YESOS'},
    '14': {'start': ['23'],
           'except': ['231', '235'],
           'name': u'OTROS MATERIALES DE CONSTRUCCION (LOZA, PORCELANA, '
                   u'REFRACTARIOS, ETC.)'},
    '15': {'start': ['20', '21'],
           'name': u'QUIMICA Y PETROQUIMICA'},
    '16': {'start': ['25', '26', '27', '28'],
           'name': u'MAQUINAS Y TRANSFORMADOS METALICOS'},
    '17': {'start': ['29', '309'],
           'name': u'CONSTRUCCION DE VEHÍCULOS A MOTOR. '
                   u'MOTOCICLETAS Y BICICLETAS'},
    '18': {'start': ['301', '3315'],
           'name': u'CONSTRUCCION Y REPARACION NAVAL'},
    '19': {'start': ['302', '303', '304'],
           'name': u'CONSTRUCCION DE OTROS MEDIOS DE TRANSPORTE'},
    '20': {'start': ['10', '11', '12'],
           'name': u'ALIMENTACION, BEBIDAS Y TABACO'},
    '21': {'start': ['13', '14', '15'],
           'name': u'IND. TEXTIL, CONFECCION, CUERO Y CALZADO'},
    '22': {'start': ['16'],
           'name': u'IND. DE MADERA Y CORCHO (EXC. FABRICACION DE MUEBLES)'},
    '23': {'start': ['17'],
           'name': u'PASTAS PAPELERAS, PAPEL, CARTON, MANIPULADOS'},
    '24': {'start': ['18', '581'],
           'name': u'ARTES GRAFICAS Y EDICION'},
    '25': {'start': ['22', '31', '32', '33'],
           'except': ['3315'],
           'name': u'IND. CAUCHO, MAT. PLASTICAS Y OTRAS NO ESPECIFICADAS'},
    '26': {'start': ['41', '42', '43'],
           'name': u'CONSTRUCCION Y OBRAS PUBLICAS'},
    '27': {'start': ['491', '492'],
           'name': u'TRANSPORTE INTERURBANO POR FF. CC.'},
    '28': {'start': ['4939', '494'],
           'name': u'TRANSPORTE INTERURBANO POR CARRETERA (VIAJEROS, '
                   u'MERCANCIAS)'},
    '29': {'start': ['4931', '4932', '495', '50', '51', '52'],
           'name': u'OTRAS EMPRESAS DE TRANSPORTE'},
    '30': {'start': ['55', '56'],
           'name': u'HOSTELERIA'},
    '31': {'start': ['36', '37', '38', '39', '53', '60', '61', '72',
                     '84', '85', '86', '87', '88', '91', '99'],
           'except': ['855', '856'],
           'name': u'ADMINISTRACION Y OTROS SERVICIOS PUBLICOS'},
    '32': {'start': ['45', '46', '47', '582', '59', '62', '63', '64', '64',
                     '65', '66', '67', '68', '69', '70', '71', '73', '74', '75',
                     '77',  '82', '855', '856', '90', '92', '93', '94', '95',
                     '96'],
           'name': u'COMERCIO Y SERVICIOS'},
    '33': {'start': ['97', '98'],
           'name': u'USOS DOMESTICOS'},
    '34': {'name': u'NO ESPECIFICADOS'}}

TARIFES_CONV = {
    '2.0A': '4',
    '2.0DHA': '4',
    '2.0DHS': '4',
    '2.1A': '5',
    '2.1DHA': '5',
    '2.1DHS': '5',
    '3.0A': '5',
    '3.1A': '6',
    '3.1A LB': '6',
    '6.1A': '11',
    '6.1B': '12',
    '6.1': '7',
    '6.2': '8',
    '6.3': '9',
    '6.4': '10',
    '6.5': '10',
}

TARIFES = {
    '1': 'Bono Social',
    '2': 'PVPC Con/Sin D.H.A hasta 10 kW',
    '3': 'PVPC (con recargo) Con/Sin D.H.A superior a 10 kW',
    '4': 'MERCADO LIBRE hasta 10 kW',
    '5': 'MERCADO LIBRE superior a 10 kW',
    '6': 'MERCADO LIBRE 3.1A de 1 kV a 36 kV',
    '7': 'MERCADO LIBRE 6.1 de 1 kV a 36 kV',
    '8': 'MERCADO LIBRE 6.2 de 36 kV a 72,5 kV',
    '9': 'MERCADO LIBRE 6.3 de 72,5 kV a 145 kV',
    '10': 'MERCADO LIBRE 6.4-6.5 mayor o igual a 145 kV',
    '11': 'MERCADO LIBRE 6.1A de 1 kV a 30 kV',
    '12': 'MERCADO LIBRE 6.1B de 30 kV a 36 kV',
}

PROVINCIES = {
    '00': 'DESCONOCIDA',
    '01': 'ARABA/ÁLAVA',
    '02': 'ALBACETE',
    '03': 'ALICANTE - ALACANT',
    '04': 'ALMERÍA',
    '05': 'ÁVILA',
    '06': 'BADAJOZ',
    '07': 'BALEARS (ILLES)',
    '08': 'BARCELONA',
    '09': 'BURGOS',
    '10': 'CÁCERES',
    '11': 'CÁDIZ',
    '12': 'CASTELLÓN - CASTELLÓ',
    '13': 'CIUDAD REAL',
    '14': 'CÓRDOBA',
    '15': 'CORUÑA (A)',
    '16': 'CUENCA',
    '17': 'GIRONA',
    '18': 'GRANADA',
    '19': 'GUADALAJARA',
    '20': 'GIPUZKOA',
    '21': 'HUELVA',
    '22': 'HUESCA',
    '23': 'JAÉN',
    '24': 'LEÓN',
    '25': 'LLEIDA',
    '26': 'RIOJA (LA)',
    '27': 'LUGO',
    '28': 'MADRID',
    '29': 'MÁLAGA',
    '30': 'MURCIA',
    '31': 'NAVARRA',
    '32': 'OURENSE',
    '33': 'ASTURIAS',
    '34': 'PALENCIA',
    '35': 'PALMAS (LA,S)',
    '36': 'PONTEVEDRA',
    '37': 'SALAMANCA',
    '38': 'SANTA CRUZ TENERIFE',
    '39': 'CANTABRIA',
    '40': 'SEGOVIA',
    '41': 'SEVILLA',
    '42': 'SORIA',
    '43': 'TARRAGONA',
    '44': 'TERUEL',
    '45': 'TOLEDO',
    '46': 'VALENCIA - VALÉNCIA',
    '47': 'VALLADOLID',
    '48': 'BIZKAIA',
    '49': 'ZAMORA',
    '50': 'ZARAGOZA',
    '51': 'CEUTA',
    '52': 'MELILLA',
    '60': 'EXTRANJERO',
}


class Cabecera(XmlModel):

    _sort_order = ('version', 'tipocuestionario', 'tipocuestionarioid', 'anio')

    def __init__(self):
        self.datos = XmlField('CABECERA')
        self.version = XmlField('VERSION_XML')
        self.tipocuestionario = XmlField('TIPOCUESTIONARIO')
        self.tipocuestionarioid = XmlField('TIPOCUESTIONARIOID')
        self.anio = XmlField('ANIO')
        super(Cabecera, self).__init__('CABECERA', 'datos')


class Empresa(XmlModel):

    _sort_order = ('cif_vat', 'id_cnae_principal')

    def __init__(self):
        self.datos = XmlField('EMPRESA')
        self.cif_vat = XmlField('CIF_VAT')
        self.id_cnae_principal = XmlField('ID_CNAE_PRINCIPAL')

        super(Empresa, self).__init__('EMPRESA', 'datos')


class Tarifa(XmlModel):

    _sort_order = ('id_tarifa', 'num_clientes', 'potencia_contratada',
                   'energia_comercializada', 'valor_facturacion')

    def __init__(self):
        self.datos = XmlField('TARIFA')
        self.id_tarifa = XmlField('ID_TARIFA',
                                  attributes={'CODIGO': ''})
        self.num_clientes = XmlField('NUM_CLIENTES')
        self.potencia_contratada = XmlField('POTENCIA_CONTRATADA')
        self.energia_comercializada = XmlField('ENERGIA_COMERCIALIZADA')
        self.valor_facturacion = XmlField('VALOR_FACTURACION')

        super(Tarifa, self).__init__('TARIFA', 'datos')

    def set_codi_tarifa(self, codi):
        self.id_tarifa.attributes.update({'CODIGO': codi})


class ListaTarifas(XmlModel):

    _sort_order = ('tarifas',)

    def __init__(self):
        self.datos = XmlField('LISTA_TARIFAS')
        self.tarifas = []

        super(ListaTarifas, self).__init__('LISTA_TARIFAS', 'datos')


class CNAE(XmlModel):

    _sort_order = ('id_cnae', 'num_clientes', 'energia_comercializada',)

    def __init__(self):
        self.datos = XmlField('CNAE')
        self.id_cnae = XmlField('ID_CNAE',
                                attributes={'CODIGO': ''})
        self.num_clientes = XmlField('NUM_CLIENTES')
        self.energia_comercializada = XmlField('ENERGIA_COMERCIALIZADA')

        super(CNAE, self).__init__('CNAE', 'datos')

    def set_codi_cnae(self, codi):
        self.id_cnae.attributes.update({'CODIGO': codi})


class ListaCNAES(XmlModel):

    _sort_order = ('cnaes',)

    def __init__(self):
        self.datos = XmlField('LISTA_CNAES')
        self.cnaes = []

        super(ListaCNAES, self).__init__('LISTA_CNAES', 'datos')


class Provincia(XmlModel):

    _sort_order = ('desc_provincia', 'lista_tarifas', 'lista_cnaes')

    def __init__(self):
        self.datos = XmlField('PROVINCIA')
        self.desc_provincia = XmlField('DESC_PROVINCIA',
                                       attributes={'CODIGO': ''})
        self.lista_tarifas = ListaTarifas()
        self.lista_cnaes = ListaCNAES()

        super(Provincia, self).__init__('PROVINCIA', 'datos')

    def set_codi_provincia(self, codi):
        self.desc_provincia.attributes.update({'CODIGO': codi})


class ListaProvincias(XmlModel):

    _sort_order = ('lista_provincias', 'provincias')

    def __init__(self):
        self.datos = XmlField('LISTA_PROVINCIAS')
        self.provincias = []

        super(ListaProvincias, self).__init__('LISTA_PROVINCIAS', 'datos')


class Cuestionario(XmlModel):

    _sort_order = ('lista_provincias', 'observaciones')

    def __init__(self):
        self.datos = XmlField('CUESTIONARIO')
        self.lista_provincias = ListaProvincias()
        self.observaciones = XmlField('OBSERVACIONES')

        super(Cuestionario, self).__init__('CUESTIONARIO', 'datos')


class Escila(XmlModel):

    provincies_dict = {}
    cnaes_dict = {}
    year = 2014
    vat = '11111111H'
    observaciones = ''

    def __init__(self):

        self.datos = XmlField('FORMULARIO')
        self.cabecera = Cabecera()
        self.empresa = Empresa()
        self.cuestionario = Cuestionario()

        super(Escila, self).__init__('FORMULARIO', 'datos')

    def get_cnae_sector(self, cnae):
        ''' Search cnae sector
        :param cnae: cnae code
        :return: sector id
        '''
        if not cnae:
            return '34'
        for s_id, s_info in CNAE_SECTORS.items():
            keys = s_info.keys()
            if 'except' in keys:
                for e in s_info['except']:
                    if cnae.startswith(e):
                        continue
            if 'start' in keys:
                for c in s_info['start']:
                    if cnae.startswith(c):
                        return s_id
        return '34'

    def set_vat(self, vat):
        self.vat = vat

    def set_year(self, year):
        self.year = year

    def set_observaciones(self, observaciones):
        self.observaciones = observaciones

    def load_data(self, tarifes, cnaes):
        '''Loads data from lists
        :param tarifes: [(provincia_code, tarifa_name, num_clients,
                          potencia_contract [kW], energia_comer[MWh],
                          valor_fact)]
        :param cnaes: [(provincia_code, cname, num_clients,
                        energia_comer [MWh])]
        :return: True or False on error
        '''
        self.cabecera.feed(
            {'version': '1.0.0',
             'tipocuestionario': 'COMERCIALIZADORA',
             'tipocuestionarioid': '3',
             'anio': self.year}
        )
        self.empresa.feed(
            {'cif_vat': self.vat,
             'id_cnae_principal': 7}
        )

        provincias = {}
        # Tarifes LOAD
        for t in tarifes:
            prov = t[0] or '00'
            if t[0] not in provincias.keys():
                provincias.setdefault(
                    prov, {'desc': PROVINCIES[prov], 'tarifas': {}, 'cnaes': {}}
                )
            t_code = TARIFES_CONV[t[1]] or ''
            t_name = TARIFES[t_code]
            vals = (t_name, t[2] or 0, t[3] or 0, t[4] or 0, t[5] or 0)
            # We must aggregate distinct tariff's with common minetur code
            cur_vals = (t_code in provincias[prov]['tarifas']
                        and provincias[prov]['tarifas'][t_code]
                        or (0,) * 5)
            new_vals = zip(vals[1:], cur_vals[1:])
            vals_total = tuple([sum(v) for v in new_vals])

            provincias[prov]['tarifas'][t_code] = (vals[0],) + vals_total

        # CNAES LOAD
        for c in cnaes:
            prov = c[0] or '00'
            if c[0] not in provincias.keys():
                provincias.setdefault(
                    prov, {'desc': PROVINCIES[prov], 'tarifas': {}, 'cnaes': {}}
                )
            c_code = self.get_cnae_sector(c[1])
            c_name = CNAE_SECTORS[c_code]['name']
            vals = (c_name, c[2] or 0, c[3] or 0)
            # We must aggregate distinct tariff's with common minetur code
            cur_vals = (c_code in provincias[prov]['cnaes']
                        and provincias[prov]['cnaes'][c_code]
                        or (0,) * 3)
            new_vals = zip(vals[1:], cur_vals[1:])
            vals_total = tuple([sum(v) for v in new_vals])

            provincias[prov]['cnaes'][c_code] = (vals[0],) + vals_total

        lista_provincias = []
        for p, d in provincias.items():
            provincia = Provincia()
            provincia.set_codi_provincia(p)

            # TARIFAS
            tarifas = ListaTarifas()
            lista_tarifas = []
            for t, v in d['tarifas'].items():
                tarifa = Tarifa()
                tarifa.set_codi_tarifa(t)
                vl = list(v)
                # num clientes filled
                vl[1] = v[1] or '0'
                # Potencia contratada with 88888888,22
                vl[2] = str(round(v[2], 2)).replace('.', ',') or '0,0'
                # Energia from kWh to MWh and with 88888888,22
                vl[3] = str(round(v[3] / 1000.0, 2)).replace('.', ',') or '0,0'
                # Facturada from € to k€ with 88888888,22
                vl[4] = str(round(v[4] / 1000.0, 2)).replace('.', ',') or '0,0'

                tarifa.feed(dict(zip(tarifa.sorted_fields(), tuple(vl))))
                lista_tarifas.append(copy.deepcopy(tarifa))

            tarifas.feed({'tarifas': lista_tarifas})

            # CNAES
            cnaes = ListaCNAES()
            lista_cnaes = []
            for c, v in d['cnaes'].items():
                cnae = CNAE()
                cnae.set_codi_cnae(c)
                vl = list(v)
                # num clientes filled
                vl[1] = v[1] or '0'
                # Energia from kWh to MWh and with 88888888,22
                vl[2] = str(round(v[2] / 1000.0, 2)).replace('.', ',') or '0,0'

                cnae.feed(dict(zip(cnae.sorted_fields(), tuple(vl))))
                lista_cnaes.append(copy.deepcopy(cnae))

            cnaes.feed({'cnaes': lista_cnaes})

            provincia.feed({'desc_provincia': d['desc'],
                            'lista_tarifas': tarifas,
                            'lista_cnaes': cnaes})
            lista_provincias.append(copy.deepcopy(provincia))

        provincias = ListaProvincias()
        provincias.feed({'provincias': lista_provincias})

        self.cuestionario.feed({'lista_provincias': provincias})
        if self.observaciones:
            self.cuestionario.feed({'observaciones': self.observaciones})

        return True
