# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config
from datetime import date
import csv
import StringIO
import base64
from slugify import slugify
from addons.giscedata_escila.xml import ESCILA
from lxml import objectify, etree


class GiscedataIndustriaEscila(osv.osv_memory):
    '''Assistent per generar el XML de la ESCILA del Ministeri de Indústria'''

    _name = 'wizard.escila'

    def genera_informe_xml(self, cursor, uid, ids, context=None):
        '''Genera XML'''
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context=context)
        schema_file ='%s/%s/xml/%s' % (config['addons_path'],
                                       'giscedata_escila',
                                       'EsquemaComercializadora v2_gisce.xsd')

        empresa_name = self.pool.get('res.users').browse(
            cursor, uid, uid, context).company_id.name
        empresa_vat = self.pool.get('res.users').browse(
            cursor, uid, uid, context).company_id.partner_id.vat[2:]
        empresa_name_txt = slugify(empresa_name, separator='_').upper()

        # PROVINCIES-CNAE
        sql_file = 'cnae.sql'
        sql = open('%s/%s/sql/%s' % (config['addons_path'],
                                     'giscedata_escila',
                                     sql_file)).read()
        cursor.execute(sql, {'inici': date(wiz.any, 1, 1),
                             'fi': date(wiz.any + 1, 1, 1)})

        output_cnae = StringIO.StringIO()
        writer_cnae = csv.writer(output_cnae)

        cnaes = []
        for row in cursor.fetchall():
            cnaes.append(row)
            writer_cnae.writerow(row)

        fitxer_cnae = base64.b64encode(output_cnae.getvalue())
        filename_cnae = 'ESCILA_CNAE_%s_%s.csv' % (empresa_name_txt, wiz.any)
        # PROVINCIES-TARIFA
        sql_file = 'tarifa.sql'
        sql = open('%s/%s/sql/%s' % (config['addons_path'],
                                     'giscedata_escila',
                                     sql_file)).read()
        cursor.execute(sql, {'inici': date(wiz.any, 1, 1),
                             'fi': date(wiz.any + 1, 1, 1)})

        output_tarifa = StringIO.StringIO()
        writer_tarifa = csv.writer(output_tarifa)

        tarifes = []
        for row in cursor.fetchall():
            tarifes.append(row)
            writer_tarifa.writerow(row)

        fitxer_tarifa = base64.b64encode(output_tarifa.getvalue())
        filename_tarifa = 'ESCILA_TARIFA_%s_%s.csv' % (empresa_name_txt,
                                                       wiz.any)
        # FITXER XML

        filename = 'ESCILA_%s_%s.xml' % (empresa_name_txt, wiz.any)

        escila = ESCILA.Escila()
        escila.set_year(wiz.any)
        escila.set_vat(empresa_vat)
        escila.load_data(tarifes, cnaes)

        escila.pretty_print = True
        escila.build_tree()

        fitxer = base64.b64encode(str(escila))
        txt = "ok"

        schema = etree.XMLSchema(file=open(schema_file, 'r'))
        parser = objectify.makeparser(schema=schema)
        try:
            objectify.fromstring(str(escila), parser)
        except Exception, e:
            txt = u"Invalid XML: %s" % e.message

        wiz.write({'cnae': fitxer_cnae, 'tarifa': fitxer_tarifa,
                   'fitxer': fitxer, 'filename': filename,
                   'filename_cnae': filename_cnae,
                   'filename_tarifa': filename_tarifa, 'info': txt})

        output_cnae.close()
        output_tarifa.close()
        return True

    _columns = {
        'state': fields.char('Estat', 10),
        'filename': fields.char('Nom Fitxer ESCILA', size=256),
        'filename_tarifa': fields.char('Nom Fitxer Tarifa', size=256),
        'filename_cnae': fields.char('Nom Fitxer CNAE', size=256),
        'any': fields.integer('Any'),
        'fitxer': fields.binary('Fitxer'),
        'tarifa': fields.binary('CSV Tarifa'),
        'cnae': fields.binary('CSV CNAE'),
        'info': fields.text('Info'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'any': lambda *a: date.today().year - 1,
        'info': lambda *a: u'ESCILA',
    }

GiscedataIndustriaEscila()
