import base64
import unittest
import tempfile
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
import xlrd
import unicodecsv


def xls2csv(xls_filename):
    # Converts an Excel file path, returns the content in CSV format.
    # If the excel file has multiple worksheets, only the first worksheet is converted.
    # Uses unicodecsv, so it will handle Unicode characters.
    # Uses a recent version of xlrd, so it should handle old .xls and new .xlsx equally well.

    wb = xlrd.open_workbook(xls_filename)
    sh = wb.sheet_by_index(0)

    with tempfile.NamedTemporaryFile() as fh:
        csv_out = unicodecsv.writer(fh, encoding='utf-8')
        for row_number in xrange(sh.nrows):
            csv_out.writerow(sh.row_values(row_number))
        fh.seek(0)
        content = fh.read()
        fh.close()
    return content


def iniciar_valors(txn, pool):
    imd_obj = pool.get('ir.model.data')
    fact_obj = pool.get('giscedata.facturacio.factura')
    linia_fact_obj = pool.get(
        'giscedata.facturacio.factura.linia'
    )

    cursor = txn.cursor
    uid = txn.user

    linia_fact_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio', 'linia_factura_0003'
    )[1]
    linia_fact_obj.write(cursor, uid, [linia_fact_id], {
        'tipus': 'subtotal_xml',
    })

    fact_01 = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio', 'factura_0001'
    )[1]
    fact_02 = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio', 'factura_0002'
    )[1]
    journal_energia_clients_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio', 'facturacio_journal_energia'
    )[1]
    fact_obj.write(cursor, uid, [fact_01, fact_02], {
        'state': 'open',
        'journal_id': journal_energia_clients_id
    })

    fact_03 = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio', 'factura_0003'
    )[1]
    journal_energia_proveidor_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_comer',
        'facturacio_journal_energia_prov'
    )[1]
    fact_obj.write(cursor, uid, [fact_03], {
        'state': 'open',
        'journal_id': journal_energia_proveidor_id
    })


class TestWizardMunicipalTaxes(testing.OOTestCase):
    def test_rejects_wrong_date(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2000-50-50',
                'end_date': '2000-70-70',
                'tax': 1.5,
                'file_type': 'xls',
                'invoice_list': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert not wiz.build_report(context=ctx)

    def test_rejects_wrong_tax(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2000-50-50',
                'end_date': '2000-70-70',
                'tax': False,
                'file_type': 'xls',
                'invoice_list': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert not wiz.build_report(context=ctx)

    def test_rejects_no_context_and_missing_active_ids(self):
        # Neither missing a context or having a context without active_ids
        # should work since it would mean we wouldn't be able to know what towns
        # we are supposed to do the report for
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2000-50-50',
                'end_date': '2000-70-70',
                'tax': False,
                'file_type': 'xls',
                'invoice_list': False,
            }
            wiz_id = wiz_obj.create(cursor, uid, vals, context=None)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert not wiz.build_report(context=None)

            useless_context = {'useless': 'context'}
            assert not wiz.build_report(context=useless_context)


class TestWizardMunicipalTaxesReport(testing.OOTestCase):

    def test_wizard_muni_taxes_creates_correct_report_summary_csv(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'summary.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.filename_report == 'municipal_taxes_invoicing.csv'
            fitxer_nou = base64.b64decode(wiz.report)
            assert fitxer_nou == fitxer_correcte

    def test_wizard_muni_taxes_creates_correct_report_summary_xls(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'summary.xls')

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'xls',
                'invoice_list': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.filename_report == 'municipal_taxes_invoicing.xls'

            with tempfile.NamedTemporaryFile() as aux:
                cont_fitxer_nou = base64.b64decode(wiz.report)
                aux.write(cont_fitxer_nou)
                aux.flush()
                content = xls2csv(aux.name)
                aux.close()

            cont_correcte = xls2csv(fitxer_correcte_path)
            assert content == cont_correcte

    def test_wizard_muni_taxes_creates_correct_report_complete_csv(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'complete.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': True,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.filename_report == 'municipal_taxes_invoicing.csv'
            fitxer_nou = base64.b64decode(wiz.report)
            assert fitxer_nou == fitxer_correcte

    def test_wizard_muni_taxes_creates_correct_report_complete_xls(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'complete.xls')

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'xls',
                'invoice_list': True,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.filename_report == 'municipal_taxes_invoicing.xls'

            with tempfile.NamedTemporaryFile() as aux:
                cont_fitxer_nou = base64.b64decode(wiz.report)
                aux.write(cont_fitxer_nou)
                aux.flush()
                content = xls2csv(aux.name)
                aux.close()

            cont_correcte = xls2csv(fitxer_correcte_path)
            assert content == cont_correcte

    def test_wizard_muni_taxes_creates_correct_report_no_data_csv(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'no_data.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2000-01-01',
                # There shouldn't be any invoices between this dates
                'end_date': '2000-01-01',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.filename_report == 'municipal_taxes_invoicing.csv'
            fitxer_nou = base64.b64decode(wiz.report)
            assert fitxer_nou == fitxer_correcte

    def test_wizard_muni_taxes_creates_correct_report_no_data_xls(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'no_data.xls')

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2000-01-01',
                # There shouldn't be any invoices between this dates
                'end_date': '2000-01-01',
                'tax': 1.5,
                'file_type': 'xls',
                'invoice_list': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.filename_report == 'municipal_taxes_invoicing.xls'

            with tempfile.NamedTemporaryFile() as aux:
                cont_fitxer_nou = base64.b64decode(wiz.report)
                aux.write(cont_fitxer_nou)
                aux.flush()
                content = xls2csv(aux.name)
                aux.close()

            cont_correcte = xls2csv(fitxer_correcte_path)
            assert content == cont_correcte


class TestWizardMunicipalTaxesProvider(testing.OOTestCase):

    def test_wizard_muni_taxes_creates_correct_provider_no_invoice_list(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'providers.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': False,
                'provider_detail': True,
                'client_detail': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            fitxer_nou = base64.b64decode(wiz.proveidor)
            assert fitxer_nou == fitxer_correcte

    def test_wizard_muni_taxes_creates_correct_provider_yes_invoice_list(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'providers.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': True,
                'provider_detail': True,
                'client_detail': False,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            fitxer_nou = base64.b64decode(wiz.proveidor)
            assert fitxer_nou == fitxer_correcte


class TestWizardMunicipalTaxesClient(testing.OOTestCase):
    def test_wizard_muni_taxes_creates_correct_client_no_invoice_list(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'clients.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': False,
                'provider_detail': False,
                'client_detail': True,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            fitxer_nou = base64.b64decode(wiz.client)
            assert fitxer_nou == fitxer_correcte

    def test_wizard_muni_taxes_creates_correct_client_yes_invoice_list(self):
        wiz_obj = self.openerp.pool.get(
            'giscedata.municipal.taxes.invoicing.wizard'
        )

        fitxer_correcte_path = get_module_resource(
            'giscedata_municipal_taxes', 'tests', 'fixtures', 'clients.csv')
        with open(fitxer_correcte_path, 'r') as f:
            fitxer_correcte = f.read()

        with Transaction().start(self.database) as txn:
            iniciar_valors(txn, self.openerp.pool)

            cursor = txn.cursor
            uid = txn.user

            vals = {
                'start_date': '2016-02-01',
                'end_date': '2016-07-02',
                'tax': 1.5,
                'file_type': 'csv',
                'invoice_list': True,
                'provider_detail': False,
                'client_detail': True,
            }
            ctx = {'active_ids': [1]}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            assert wiz.build_report(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            fitxer_nou = base64.b64decode(wiz.client)
            assert fitxer_nou == fitxer_correcte
