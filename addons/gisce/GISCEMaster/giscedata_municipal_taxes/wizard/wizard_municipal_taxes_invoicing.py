# -*- coding: utf-8 -*-report
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from datetime import datetime
import base64
from addons.giscedata_municipal_taxes.taxes.municipal_taxes_invoicing \
    import MunicipalTaxesInvoicingReport

import pooler
import threading
from oorq.oorq import JobsPool
from autoworker import AutoWorker
from oorq.decorators import job
import logging
import tempfile
import zipfile
from StringIO import StringIO
from slugify import slugify
from oorq.oorq import setup_redis_connection


class ProgressJobsPool(JobsPool):

    def __init__(self, wizard):
        self.wizard = wizard
        super(ProgressJobsPool, self).__init__()

    @property
    def all_done(self):
        logger = logging.getLogger('openerp.municipal.taxes.progress')
        logger.info('Progress: {0}/{1} {2}%'.format(
            len(self.done_jobs), self.num_jobs, self.progress
        ))
        self.wizard.write({'municipis_progress': self.progress})
        finished_txt = ""
        for res in self.results.values():
            finished_txt += res[0]+"\n"
        self.wizard.write({'info_progress': finished_txt})
        return super(ProgressJobsPool, self).all_done


class GiscedataMunicipalTaxesInvoicingWizard(osv.osv_memory):
    """ Municipal taxes - Invoicing - Wizard """

    _name = 'giscedata.municipal.taxes.invoicing.wizard'

    def build_report(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({'state': 'working'})
        setup_redis_connection()
        res = True
        if wizard.wiz_mode == "parallel":
            print_thread = threading.Thread(
                target=self.build_report_threaded,
                args=(cursor.dbname, uid, ids, context)
            )
            print_thread.start()
        else:
            context['cursor'] = cursor
            res = self.build_report_threaded(cursor.dbname, uid, ids, context=context)
        return res

    def build_report_threaded(self, dbname, uid, ids, context=None):
        """ Report """
        def valid_date(d):
            try:
                datetime.strptime(d, '%Y-%m-%d')
                return True
            except ValueError:
                return False
        if context is None:
            context = {}
        if context.get("cursor"):
            cursor = context.get("cursor")
        else:
            cursor = pooler.get_db(dbname).cursor()
        if not context or 'active_ids' not in context:
            return False  # We need the active_ids to execute the wizard
        wizard = self.browse(cursor, uid, ids[0], context=context)
        try:
            start_date = wizard.start_date
            end_date = wizard.end_date
            tax = wizard.tax
            file_type = wizard.file_type
            inv_list = wizard.invoice_list

            valid_dates = valid_date(start_date) and valid_date(end_date)
            if not valid_dates or not isinstance(tax, float):
                return False

            active_ids = context['active_ids']
            polissa_categ_imu_ex_id = (
                self.pool.get('ir.model.data').get_object_reference(
                    cursor, uid, 'giscedata_municipal_taxes', 'contract_categ_imu_ex'
                )[1]
            )
            whereiam = self.pool.get('giscedata.cups.ps').whereiam(cursor, uid)
            invoiced_states = self.pool.get('giscedata.facturacio.extra').get_states_invoiced(cursor, uid)

            taxes_invoicing_report_params = {
                'start_date': start_date, 'end_date': end_date, 'tax': tax,
                'file_type': file_type, 'inv_list': inv_list,
                'polissa_categ_imu_ex_id': polissa_categ_imu_ex_id,
                'whereiam': whereiam, 'invoiced_states': invoiced_states

            }
            if wizard.wiz_mode == "classic":
                mcodes, report1, report2, report3 = self.build_all_reports(
                    cursor, uid, ids, active_ids,
                    taxes_invoicing_report_params, context=context
                )
                filename = 'municipal_taxes_invoicing.{0}'.format(file_type)
                report1 = base64.b64encode(report1)
            else:
                filename = 'municipal_taxes_invoicing.zip'
                report1, report2, report3 = self.build_report_taxes_parallel(
                    cursor, uid, ids, active_ids, taxes_invoicing_report_params
                )

            wizard.write({
                'report': report1,
                'filename_report': filename,
                'state': 'done'
            })
            if wizard.provider_detail and report2:
                wizard.write({'proveidor': base64.b64encode(report2)})
            if wizard.client_detail and report3:
                wizard.write({'client': base64.b64encode(report3)})

            return True
        except Exception, e:
            wizard.write({'state': 'error'})
            return True

    def default_get(self, cr, uid, fields_list, context=None):
        # We use default_get as directly assigning the value in defaults like
        # the rest doesn't seem to work for tax (no idea why)
        res = super(GiscedataMunicipalTaxesInvoicingWizard, self).default_get(
            cr, uid, fields_list, context
        )

        res.update({
            'tax': 1.5
        })

        for val in res.keys():
            if val not in fields_list:
                del res[val]

        return res

    _columns = {
        'filename_report': fields.char('Nom report taxes', size=256),
        'filename_proveidor': fields.char('Nom report proveidor', size=256),
        'filename_client': fields.char('Nom report client', size=256),
        'start_date': fields.date('Data d\'inici'),
        'end_date': fields.date('Data de fi'),
        'tax': fields.float('Impost Aplicat'),
        'invoice_list': fields.boolean('Afegir llista factures'),
        'file_type': fields.selection(
            [('csv', 'CSV'), ('xls', 'Excel'), ('xml', 'XML')],
            string='Tipus fitxer',
            help=u'Només afectarà al fitxer de taxes'
        ),
        'provider_detail': fields.boolean('Generar detalls proveidor'),
        'client_detail': fields.boolean('Generar detalls client'),
        'report': fields.binary('Taxes report'),
        'proveidor': fields.binary('Detall proveidor'),
        'client': fields.binary('Detall client'),
        'state': fields.selection([('init', "Configuracio de parametres per la generacio"), ('working', "Generant informe"), ('done', "Informe generat"), ('error', "Hi ha hagut un problema durnat la generació")], "Fase"),
        'municipis_progress': fields.float(u"Municipis a generats"),
        'info_progress': fields.text(u"Informació sobre el progrés"),
        'wiz_mode': fields.selection([('classic', "Generar les dades de tots els municipis en un fitxer"), ('parallel', "Generant en paral.lel un fitxer per cada municipi")], "Mode"),
    }

    _defaults = {
        'filename_proveidor': lambda *a: _('detall_proveidor.csv'),
        'filename_client': lambda *a: _('detall_client.csv'),
        'tax': lambda *a: 1.5,
        'invoice_list': lambda *a: False,
        'file_type': lambda *a: 'csv',
        'provider_detail': lambda *a: False,
        'client_detail': lambda *a: False,
        'state': lambda *a: 'init',
        'wiz_mode': lambda *a: "classic",
        'info_progress': lambda *a: ""
    }

    def build_all_reports(self, cursor, uid, ids, active_ids, report_params, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids)[0]
        taxes_invoicing_report = MunicipalTaxesInvoicingReport(
            cursor, uid, report_params['start_date'], report_params['end_date'], report_params['tax'], report_params['file_type'], report_params['inv_list'],
            report_params['polissa_categ_imu_ex_id'], report_params['whereiam'], report_params['invoiced_states']
        )
        if not active_ids and context.get("records"):
            report = taxes_invoicing_report.build_report_taxes([], context.get("records"))
        else:
            report = taxes_invoicing_report.build_report_taxes(active_ids)
        res1 = report
        res2, res3 = "", ""

        if wizard.provider_detail:
            providers = taxes_invoicing_report.build_report_providers()
            res2 = providers

        if wizard.client_detail:
            clients = taxes_invoicing_report.build_report_clients()
            res3 = clients

        municipi_o = self.pool.get("res.municipi")
        minfo = municipi_o.read(cursor, uid, active_ids, ['name', 'ine'])
        if not active_ids and context.get("records"):
            mcodes = "_".join(list(set([x[0] for x in context.get("records")])))
        else:
            mcodes = "_".join([x['ine'] + "_" + x['name'] for x in minfo if x['id'] in active_ids])
        mcodes = slugify(mcodes)
        return mcodes, res1, res2, res3

    def build_report_taxes_parallel(self, cursor, uid, ids, active_ids, report_conf, context=None):
        wiz = self.browse(cursor, uid, ids)[0]
        j_pool = ProgressJobsPool(wiz)
        aw = AutoWorker(queue="informe_municipal_tasks", default_result_ttl=24 * 3600)
        taxes_invoicing_report = MunicipalTaxesInvoicingReport(
            cursor, uid, report_conf['start_date'], report_conf['end_date'],
            report_conf['tax'], report_conf['file_type'],
            report_conf['inv_list'],
            report_conf['polissa_categ_imu_ex_id'], report_conf['whereiam'],
            report_conf['invoiced_states']
        )
        all_records = taxes_invoicing_report.get_totals_by_city(active_ids)
        records_by_mun = {}
        for record in all_records:
            if not records_by_mun.get(record[0]):
                records_by_mun[record[0]] = []
            records_by_mun[record[0]].append(record)
        for records_mun in records_by_mun:
            j_pool.add_job(
                self.build_all_reports_async(cursor, uid, ids, records_by_mun[records_mun], report_conf, context=context)
            )
        if len(records_by_mun):
            aw.work()
            j_pool.join()
        wiz.write({'municipis_progress': 100.0})
        f = StringIO()
        general_zip = zipfile.ZipFile(f, 'w')
        for mcodes, res1, res2, res3 in j_pool.results.values():
            res_f = StringIO()
            res_zf = zipfile.ZipFile(res_f, 'w')
            fname = "municipal_taxes_invoicing_"+mcodes+"."+report_conf['file_type']
            res_zf.writestr(fname, res1)
            if res2:
                fname = "providers_detail_" + mcodes + "."+report_conf['file_type']
                res_zf.writestr(fname, res2)
            if res3:
                fname = "clients_detail_" + mcodes + "."+report_conf['file_type']
                res_zf.writestr(fname, res3)
            res_zf.close()
            general_zip.writestr("municipal_taxes_invoicing_"+mcodes+".zip", res_f.getvalue())
        general_zip.close()
        file = base64.encodestring(f.getvalue())
        f.close()
        return file, "", ""

    @job(queue="informe_municipal_tasks", timeout=3600*24)
    def build_all_reports_async(self, cursor, uid, ids, records, report_params, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['records'] = records
        return self.build_all_reports(cursor, uid, ids, [], report_params, context=ctx)


GiscedataMunicipalTaxesInvoicingWizard()
