# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *


class GiscedataTransformadorsIndexTests(testing.OOTestCase):

    def test_get_srid(self):
        bi_obj = self.openerp.pool.get('giscedata.transformador.trafo')
        with Transaction().start(self.database) as txn:
            srid = bi_obj.get_db_srid(txn.cursor, txn.user)
            expect(srid).to(equal("23031"))
