from base_index.base_index import BaseIndex


class GiscedataTransformadorTrafo(BaseIndex):
    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def __init__(self, pool, cursor):
        super(GiscedataTransformadorTrafo, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.name} - {obj.numero_fabricacio}'

    _index_fields = {
        'cini': True,
        'name': True,
        'numero_fabricacio': True,
        'id_marca.name': True,
        'id_connexioune.name': True,
        'potencia_nominal': lambda self, data: '{0}kVA'.format(data),
        'id_estat.name': True,
        'data_pm': True,
    }


GiscedataTransformadorTrafo()
