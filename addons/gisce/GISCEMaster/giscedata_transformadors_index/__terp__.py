# -*- coding: utf-8 -*-
{
    "name": "Transformadors index",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index per Transformadors
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_transformadors",
        "base_index",
        "giscegis_blocs_transformadors",
        "giscegis_blocs_transformadors_reductors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
