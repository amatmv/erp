# -*- coding: utf-8 -*-
"""Productes amb Fabricant"""
from osv import fields, osv


class ProductFabricant(osv.osv):
    _inherit = 'product.product'
    _name = 'product.product'

    _columns = {
        'fabricant_id': fields.many2one('res.partner', string='Fabricant'),
        'ref_fabricant': fields.char(size=150, string='Referència Fabricant')
    }

ProductFabricant()
