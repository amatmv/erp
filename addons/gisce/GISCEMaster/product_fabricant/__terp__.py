# -*- coding: utf-8 -*-
{
    "name": "product_fabricant",
    "description": """Relaciona els diferents Productes amb el seu corresponent Fabricant (res.partner)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_view.xml"
    ],
    "active": False,
    "installable": True
}
