# -*- encoding: utf-8 -*-
from __future__ import absolute_import

from l10n_ES_remesas.wizard.sepa19 import Sepa19Core as Sepa19CoreBase
from l10n_ES_remesas.wizard.converter import to_ascii


class Sepa19Core(Sepa19CoreBase):
    def _get_debtor_entity(self, line):
        return to_ascii(line['bank_id'].owner_name)
