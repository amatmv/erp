# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv



class GiscedataPolissa(osv.osv):
    """Extensió de la pòlissa amb les dades bàsiques de facturació.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def update_mandate(self, cursor, uid, polissa_id, context=None):
        vals = super(GiscedataPolissa, self).update_mandate(
            cursor, uid, polissa_id, context=context
        )
        if not context:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id)
        new_notes = ''

        debtor_name = polissa.bank.owner_name
        debtor_vat = polissa.bank.owner_id.vat
        vals.update({
            'debtor_name': debtor_name,
            'debtor_vat': debtor_vat,
        })
        if polissa.bank.owner_address_id:
            debtor_address = (polissa.bank.owner_address_id.
                              name_get(context=context)[0][1]).upper()
            debtor_contact = (polissa.bank.owner_address_id.name
                              and '%s, ' % polissa.bank.owner_address_id.name
                              or '').upper()
            if debtor_contact:
                debtor_address = debtor_address.replace(debtor_contact, '')
            debtor_state = (polissa.bank.owner_address_id.state_id
                            and polissa.bank.owner_address_id.state_id.name
                            or '').upper()
            debtor_country = (polissa.bank.owner_address_id.country_id
                              and polissa.bank.owner_address_id.country_id.name
                              or '').upper()
            vals.update({
                'debtor_address': debtor_address,
                'debtor_state': debtor_state,
                'debtor_country': debtor_country,
            })

            phone = polissa.bank.owner_address_id.phone
            if not phone:
                phone = polissa.bank.owner_address_id.mobile
                payment_data = u" Teléfono: {0}".format(phone)
            else:
                payment_data = u" Teléfono:"
            new_notes += vals['notes'] + '\n'
            new_notes += u"Debtor information:\n"
            new_notes += u"{0}\n".format(payment_data)
            vals['notes'] = new_notes
        return vals

GiscedataPolissa()