# -*- coding: utf-8 -*-
{
    "name": "Comerdist (pòlisses)",
    "description": """
    This module provide :
      * Sincronització de pòlisses
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_comerdist_polissa_data.xml"
    ],
    "active": False,
    "installable": True
}
