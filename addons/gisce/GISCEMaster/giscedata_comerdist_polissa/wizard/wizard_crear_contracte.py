# -*- coding: utf-8 -*-

from osv import osv, fields
from giscedata_comerdist import Sync, OOOPPool


class GiscedataPolissaCrearContracte(osv.osv_memory):
    """Wizard per crear les modificacions contractuals.
    """
    _name = 'giscedata.polissa.crear.contracte'
    _inherit = 'giscedata.polissa.crear.contracte'

    def action_crear_contracte(self, cursor, uid, ids, context=None):
        """Creem la modificació contractual.
        """

        wizard = self.browse(cursor, uid, ids[0], context)
        polissa = wizard.polissa_id

        if wizard.accio == 'modificar':
            if polissa.must_be_synched(context=context):
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop,
                            'giscedata.polissa',
                            config=config_id)
                mc_sync = Sync(cursor, uid, ooop,
                               'giscedata.polissa.modcontractual',
                               config=config_id)
                obj = sync.sync(polissa.id)
                mca = obj.modcontractual_activa
                vals = mc_sync.manager.get_vals_from_polissa(obj.id)
                vals.update({'data_inici': mca.data_inici,
                             'data_final': mca.data_final,
                             'tipus': mca.tipus,
                             'name': mca.name})
                mca.write(vals)

        return super(GiscedataPolissaCrearContracte,
                     self).action_crear_contracte(
            cursor, uid, ids, context=context
        )

GiscedataPolissaCrearContracte()
