.. giscedata_comerdist_polissa documentation master file, created by
   sphinx-quickstart on Mon Apr  8 10:05:57 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació d'usuari del mòdul comerdist per pòlisses
======================================================

Aquesta és la documentació d'usuari del mòdul comerdist per la
sincronització de pòlisses/contractes entre distribuidora i
comercialitzadora.

Accions freqüents
=================

#. Procés a seguir per donar d’alta a la COMER un contracte 
   d’un client de la nostra DISTRI que actualment està en una altra COMER 
   :ref:`acc_1`
#. Procediment per donar d’alta a la COMER i a la DISTRI un 
   contracte i una pòlissa que no estan creades a cap de les 2 empreses
   :ref:`acc_2`
#. Procés per fer un canvi de TITULAR de un contracte a la COMER i que es 
   realitzi també a la DISTRI :ref:`acc_3`
#. Quan l’usuari sync crea en la DISTRI una pòlissa en esborrany procedent de COMER. I quan l’usuari sync crea un contracte en esborrany a COMER procedent de DISTRI


.. _acc_1:

Procediment nova alta en la comercialitzadora
---------------------------------------------

El procés l’inicia la COMER tant si el client ja havia estat en la nostra 
COMER anteriorment (el cas d'un retorn) com si no.

1. Es crea un **nou contracte** en esborrany i somplen les caselles:

  - **Titular**
  - **Data firma contracte**
  - **Data d'alta**
  - **Comercialitzadora**
  - **Referència distribuidora** (cal posar el número de pòlissa de 
    DISTRI amb que es vol relacionar)
  - **CUPS**. Si no el cups no existeix en la comercialitzadora
    es pot usar la opció *Menú > Gestió de CUPS > Manteniment > CUPS > 
    Obtenció de CUPS de Distribució* per importar el cups automàticament
    i tota la seva informació.
  - La resta de camps de les pestanyes: **General**, **Facturació** i 
    **Contactes**
 
.. note::

   Mentre el contracte està en esborrany es pot guardar les vegades que 
   calgui i no sincronitza amb DISTRI degut a que l’estat de la pòlissa de 
   DISTRI (Activa) es diferent del del contracte de COMER (Esborrany).

2. Un cop les dades de COMER són les correctes, es pot validar i activar 
   el contracte. Les dates d’inici i fi del contracte s'usaran en la 
   modificació contractual (MC) número 1 del contracte de COMER. 
   En el moment d’activar el contracte  es sincronitzarà a DISTRI
   i es crearà una nova MC a DISTRI amb les mateixes dates que a COMER.


Camps que es sincronitzaran desde COMER a DISTRI:

* Tarifa d'accés
* Potència contractada (kW)
* Tensió normalitzada
* Facturació ( Mensual o Bimestral)


.. _acc_2:

Procediment nova alta en la comer i la distri
---------------------------------------------

El procés tant el pot iniciar tant la COMER com la DISTRI però en aquest 
cas i únic, és millor començar tot el procés des de la DISTRI. Aixó és així
perquè la distribuidora crea els comptadors amb el número de serie, inicialitza
les lectures, assigna el lloguer, la tarifa d’accés, etc.

1. **A DISTRI:** Es crea un nou contracte en esborrany i somplen tots els camps 
   possibles  i es deixa per l’últim el camp “Comercialitzadora”:

.. note::
   L’últim camp a omplir a la DISTRI és el de *Comercialitzadora*. 
   Quan s’assigna la comercialitzadora pròpia en aquest camp, al guardar 
   la pòlissa en esborrany es crea a COMER el contracte relacionat.

2. **A COMER:** Una vegada s’ha creat el nou contracte relacionat a COMER, es 
   comprova quer els camps omplerts a la DISTRI són els correctes i s’han 
   de completar les dades exclusives de COMER com ara:

   * **Tarifa de comercialitzadora**
   * **Dades del pagador**
   * **Dades de Notificació**
   * etc


.. _acc_3:

Procediment canvi de titular en la comer i la distri
----------------------------------------------------

1. Donar de Baixa la pòlissa a DISTRI i a COMER.
2. Crear una nova alta en la comer i distri segons s'explica en :ref:`acc_2`.
   En aquest cas es pot partir d'un **duplicat** de la pòlissa que s'ha donat
   de Baixa (mai abans de donar-la de baixa a DISTRI ia COMER).


DISTRI. Des de DISTRI es crea un contracte en esborrany a COMER al omplir el camp  Distribuidora.

COMER. Des de COMER es crea pòlissa en esborrany a DISTRI quan al camp Distribuidora s’omple amb el nom de la nostra Distribuidora.


Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

