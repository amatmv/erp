# -*- coding: utf-8 -*-
from osv import osv
from giscedata_comerdist import Sync, OOOPPool
import netsvc
from tools.translate import _
from datetime import date, datetime, timedelta

class GiscedataPolissa(osv.osv):
    """Pòlissa per sincronitzar.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def must_be_synched(self, cursor, uid, ids, context=None):
        """ Must be defined either in distri or in comer modules. """
        return False

    def write(self, cursor, uid, ids, vals, context=None):
        """Write per la sincronització.
        """
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals,
                                                  context)
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for polissa in self.browse(cursor, uid, ids, context):
            if polissa.must_be_synched(context):
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id, active_test=False,
                                      double_test=True)
                if not obj:
                    # Si no existeix en remot l'hem d'actualitzar amb totes
                    # les dades locals
                    vals = None
                elif not obj.active:
                    # Si l'hem trobat, però està de baixa (active == False)
                    # no sincronitzem res. Les pòlisses de baixa no es
                    # sincronitzen
                    continue
                if not obj or (obj and obj.state == polissa.state):
                    sync.sync(polissa.id, vals)
                    # Sincronitzem comptador si en té
                    if polissa.comptadors:
                        polissa.comptadors[0].write({})
        return res

    def ws_signal_esborrany(self, cursor, uid, ids, context=None):
        """Trigger per esborrany.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'esborrany', cursor)
        return True

    def wkf_esborrany(self, cursor, uid, ids):
        """Sincronització quan es crear un contracte.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if not obj:
                    obj = sync.sync(polissa.id)
                elif obj and obj.state == polissa.state:
                    obj.ws_signal_esborrany()
                    obj = sync.sync(polissa.id)
                # Sincronitzem l'últim comptador si en té
                if polissa.comptadors:
                    polissa.comptadors[0].write({})
        super(GiscedataPolissa, self).wkf_esborrany(cursor, uid, ids)
        return True

    def ws_signal_validar(self, cursor, uid, ids, context=None):
        """Trigger per activar.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'validar', cursor)
        return True

    def wkf_validar(self, cursor, uid, ids):
        """Sincronització quan es crear un contracte.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config = polissa.get_config()
                ooop = OOOPPool.get_ooop(cursor, uid, config.id)
                sync = Sync(cursor, uid, ooop, self._name, config=config.id)
                obj = sync.get_object(polissa.id)
                if obj and obj.state == polissa.state:
                    # Sincronitzem
                    obj = sync.sync(polissa.id)
                    # Validem
                    obj.ws_signal_validar()
                    # Obtenir si hi ha tots els camps remots required
                    remote_fields_error = []
                    required_fields = sync.get_required_fields(polissa.id)
                    for field, field_name in required_fields.items():
                        if not hasattr(obj, field) or getattr(obj, field) is None:
                            remote_fields_error.append(' - '+field_name)
                    if remote_fields_error:
                        raise osv.except_osv(_(u'Error en la validació'),
                            _(u'Els camps remots a %s:'
                              u'\n%s\n'
                              u'No tenen valor i són obligatoris')
                            % (config.partner_id.name,
                               '\n'.join(remote_fields_error)))
        super(GiscedataPolissa, self).wkf_validar(cursor, uid, ids)
        return True

    def ws_signal_modcontractual(self, cursor, uid, ids, context=None):
        """Trigger per modcontractual.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'modcontractual', cursor)
        return True

    def ws_signal_undo_modcontractual(self, cursor, uid, ids, context=None):
        """Trigger per undo modcontractual.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'undo_modcontractual', cursor)
        return True

    def ws_signal_contracte(self, cursor, uid, ids, context=None):
        """Trigger per contrace.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'contracte', cursor)
        return True

    def wkf_contracte(self, cursor, uid, ids):
        """Sincronització quan es crear un contracte.

        En aquest estat del workflow generem el contracte segons els valors
        que tingui la pòlissa. Ja sigui perquè és una pòlissa nova o perquè
        venim d'una modificació contractual.
        """
        mc_obj = self.pool.get('giscedata.polissa.modcontractual')

        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                data_final = polissa.modcontractual_activa.data_final
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj and obj.state == 'activa':
                    obj.ws_signal_modcontractual()
                obj = sync.sync(polissa.id)
                # Si la pòlissa local té una modificació contractual activa
                # la data d'inici remota serà la data_final d'aquesta més un
                # dia
                if obj.state == 'esborrany' and polissa.modcontractual_activa:
                    year, month, day = data_final.split('-')
                    data_alta = (date(int(year), int(month), int(day))
                                 + timedelta(days=1)).strftime('%Y-%m-%d')
                    obj.data_alta = data_alta
                # TODO: Check this if
                if obj.modcontractual_activa and polissa.modcontractual_activa:
                    obj.modcontractual_activa.data_final = data_final
                # Si té modificacions contractuals futures en estat pendent les
                # eliminem
                if len(obj.modcontractuals_ids):
                    ultima_modcont = obj.modcontractuals_ids[0]
                    if ultima_modcont.state == 'pendent':
                        ultima_modcont.unlink()
                # Si la pòlissa remota ja té una modificació contractual activa
                # i la local no, no quedaran lligades aquestes
                # Hem d'agafar la data d'alta menys un dia i assignar-la
                # com a final.
                r_mca = False
                if (obj.modcontractual_activa
                    and not polissa.modcontractual_activa):
                    if (obj.modcontractual_activa.data_inici ==
                        polissa.data_alta):
                        r_mca = obj.modcontractual_activa
                        data_final = mc_obj.get_data_final(cursor, uid,
                                                           polissa.data_alta)
                    else:
                        data_final = (datetime.strptime(polissa.data_alta,
                                                    '%Y-%m-%d')
                                  - timedelta(days=1)).strftime('%Y-%m-%d')
                    obj.modcontractual_activa.write({'data_final': data_final})
                    #Sync comer fields if needed
                    r_modc = Sync(cursor, uid, ooop,
                                  'giscedata.polissa.modcontractual',
                                  config=config_id)
                    r_modc_fields = r_modc.get_remote_fields()
                    if 'comercialitzadora' in r_modc_fields:
                        #We are syncing from comer to distri
                        partner_sync = Sync(cursor, uid, ooop,
                                     'res.partner', config=config_id)
                        vals = {}
                        l_comer = polissa.comercialitzadora
                        r_comer = partner_sync.get_object(l_comer.id)
                        if r_comer.id != obj.comercialitzadora.id:
                            vals.update({'comercialitzadora': r_comer.id})
                        if r_comer.id != obj.pagador.id:
                            vals.update()
                            r_comer_address = r_comer.address_get(['invoice'])
                            vals.update({
                                'pagador': r_comer.id,
                                'direccio_pagament': r_comer_address['invoice'], 
                                        })
                            if r_comer.payment_type_customer:
                                vals.update({
                                'tipo_pago': r_comer.payment_type_customer.id,
                                })
                                if (r_comer.payment_type_customer.code
                                        == 'RECIBO_CSB'
                                    and r_comer.bank_ids):
                                    vals.update({
                                        'bank': r_comer.bank_ids[0].id
                                    })
                                else:
                                    vals.update({'bank': False})
                        if vals:
                            if r_mca:
                                r_mca.write(vals)
                            else:
                                obj.write(vals)
                if r_mca:
                    polissa_vals = r_modc.manager.get_vals_from_polissa(obj.id)
                    del polissa_vals['data_final']
                    del polissa_vals['data_inici']
                    del polissa_vals['tipus']
                    del polissa_vals['name']
                    r_mca.write(polissa_vals)
                    obj.ws_signal_undo_modcontractual()
                else:
                    obj.ws_signal_contracte()
                # Sincronitzem comptador si en té
                if polissa.comptadors:
                    polissa.comptadors[0].write({})
        # Cridem la funció del workflow para
        super(GiscedataPolissa, self).wkf_contracte(cursor, uid, ids)
        return True

    def ws_signal_activar(self, cursor, uid, ids, context=None):
        """Trigger per activar.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'activar', cursor)
        return True

    def wkf_activa(self, cursor, uid, ids):
        """Sobreescrivim l'estat activa per sincronitzar.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj:
                    obj.ws_signal_activar()
        super(GiscedataPolissa, self).wkf_activa(cursor, uid, ids)
        return True

    def ws_signal_impagament(self, cursor, uid, ids, context=None):
        """Trigger per impagament.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'impagament', cursor)
        return True

    def wkf_impagament(self, cursor, uid, ids):
        """Sobreescrivim l'estat impagament per sincronitzar.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj:
                    obj.ws_signal_impagament()
        super(GiscedataPolissa, self).wkf_impagament(cursor, uid, ids)
        return True

    def ws_signal_tallar(self, cursor, uid, ids, context=None):
        """Trigger per impagament.
        """
        wf_service = netsvc.LocalService('workflow')
        for polissa_id in ids:
            wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id,
                                    'tallar', cursor)
        return True

    def wkf_tall(self, cursor, uid, ids):
        """Sobreescrivim l'estat impagament per sincronitzar.
        """
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj:
                    obj.ws_signal_tallar()
        super(GiscedataPolissa, self).wkf_tall(cursor, uid, ids)
        return True

    def undo_last_modcontractual(self, cursor, uid, ids, context=None):

        res = super(GiscedataPolissa,
                    self).undo_last_modcontractual(cursor, uid, ids,
                                                   context=context)
        for polissa in self.browse(cursor, uid, ids):
            if polissa.must_be_synched():
                config_id = polissa.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(polissa.id)
                if obj:
                    obj.undo_last_modcontractual()

        return res

GiscedataPolissa()

class GiscedataPolissaModcontractual(osv.osv):
    """Modificacions contractuals per sincronitzar.
    """
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode write per la sincronització.
        """
        res = super(GiscedataPolissaModcontractual,
                    self).write(cursor, uid, ids, vals, context)
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for modcont in self.browse(cursor, uid, ids, context):
            if modcont.must_be_synched(context):
                config_id = modcont.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(modcont.id)
                # Només sincronitzem quan les dues modificacions contractuals
                # les trobem tant en local com en remot.
                if obj:
                    sync.sync(modcont.id, vals)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        """Sincronitzem per eliminar.
        """
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for modcont in self.browse(cursor, uid, ids, context):
            if modcont.must_be_synched(context):
                config_id = modcont.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(modcont.id)
                if obj:
                    obj.unlink()
        super(GiscedataPolissaModcontractual, self).unlink(cursor, uid, ids,
                                                           context)
        return True

GiscedataPolissaModcontractual()
