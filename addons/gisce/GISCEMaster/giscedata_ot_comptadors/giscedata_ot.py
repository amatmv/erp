# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
from datetime import datetime


class GiscedataOtTancament(osv.osv):
    _name = 'giscedata.ot.tancament'

    _columns = {
        'name': fields.char(u"Codi Tancament", size=16, required=True),
        'blocs': fields.char('Blocs', size=60),
        'disconnect_old_meter': fields.boolean(
            u"Desconectar Comptador Actual",
            help=u"Al activar la ordre de treball es donarà de baixa el "
                 u"comptador actual."
        ),
        'disconnect_new_meter': fields.boolean(
            u"Desconectar Comptador Nou",
            help=U"Al activar la ordre de treball es donarà de baixa el "
                 U"comptador nou."
        ),
        'connect_old_meter': fields.boolean(
            u"Conectar Comptador Actual",
            help=u"Al activar la ordre de treball s'activarà el "
                 u"comptador actual si estava de baixa."
        ),
        'connect_new_meter': fields.boolean(
            u"Conectar Comptador Nou",
            help=U"Al activar la ordre de treball s'activarà el "
                 U"comptador nou si estava de baixa."
        ),
        'active': fields.boolean(
            u"Activo"
        ),
    }

    _defaults = {
        'disconnect_old_meter': lambda *a: False,
        'disconnect_new_meter': lambda *a: False,
        'connect_old_meter': lambda *a: False,
        'connect_new_meter': lambda *a: False,
        'active': lambda *a: True
    }

GiscedataOtTancament()


class GiscedataOt(osv.OsvInherits):

    _name = "giscedata.ot"
    _inherit = "giscedata.ot"

    """
        Dictionary to define the default close for each section. Structure:
        {
         'SECTION': 'CLOSE_CODE'
         'SECTION_2': 'CLOSE_CODE_2'
        }
        By default it's empty.
    """
    DEFAULT_CLOSE_CODE = {}

    def _ot_types(self, cursor, uid, context=None):
        res = super(GiscedataOt, self)._ot_types(cursor, uid, context=context)
        return res + [('comptadors', "Actuacions en comptdors")]

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id):  ## Done
        res = super(GiscedataOt, self).onchange_polissa_id(cursor, uid, ids, polissa_id)

        meter_id = False
        if len(ids) > 0 and res['value']['cups_id'] != False:
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            meter_ids = self.new_comptador_from_polissa(
                cursor, uid, ids[0], polissa, context={'ignore_lectures': True}
            )
            if len(meter_ids) == 1:
                meter_id = meter_ids[0]

        res['value']['old_meter_id'] = meter_id
        return res

    def update_meters(self, cursor, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pol_obj = self.pool.get('giscedata.polissa')
        case = self.read(cursor, uid, ids, ['polissa_id'])
        if case['polissa_id']:
            polissa = pol_obj.browse(cursor, uid, case['polissa_id'][0])
            meter_ids = self.new_comptador_from_polissa(
                cursor, uid, case['id'], polissa, context={'ignore_lectures': True}
            )
            if len(meter_ids) == 1:
                meter_id = meter_ids[0]
                self.write(cursor, uid, case['id'], {'old_meter_id': meter_id})

    def new_comptador_from_polissa(self, cr, uid, ot_id, polissa, context=None):
        if context is None:
            context = {}
        meters = []
        ot_meter_obj = self.pool.get('giscedata.ot.comptador')
        fields_to_fill = ot_meter_obj.get_fields_to_fill(cr, uid, context=context)
        for comptador in [c for c in polissa.comptadors if c.active]:
            ot_meter_id = ot_meter_obj.search(cr, uid,
                                              [('ot_id', '=', ot_id),
                                               ('name', '=', comptador.name)])
            if len(ot_meter_id) != 0:
                break
            vals = {}
            meter_obj = self.pool.get('giscedata.lectures.comptador')
            polissa_meter = meter_obj.read(cr, uid, comptador.id, [])
            for key in polissa_meter.keys():
                if key not in fields_to_fill:
                    continue
                atr = polissa_meter[key]
                if key in ['lectures', 'lectures_pot']:
                    atr = []
                if isinstance(atr, tuple):
                    atr = atr[0]
                if key == 'id':
                    continue
                vals.update({key: atr})
            vals.update({'ot_id': ot_id})
            new_meter = ot_meter_obj.create(cr, uid, vals)
            meters.append(new_meter)
            if context.get('ignore_lectures', False):
                continue
            last_lect_date = meter_obj.data_ultima_lectura(
                cr, uid, comptador.id, polissa.tarifa.id)
            lect_ids = self.create_lectures(
                cr, uid, polissa_meter['lectures'], last_lect_date, new_meter,
                'giscedata.lectures.lectura')
            lect_pot_ids = self.create_lectures(
                cr, uid, polissa_meter['lectures_pot'], last_lect_date,
                new_meter, 'giscedata.lectures.potencia')
            ot_meter_obj.write(cr, uid, new_meter,
                               {'lectures': [(6, 0, lect_ids)],
                                'lectures_pot': [(6, 0, lect_pot_ids)]})
        return meters

    def create_lectures(self, cursor, uid, lect_ids, date, meter_id, model):
        new_lect_ids = []
        new_lect_model = 'giscedata.ot.{0}'.format(model.split('.')[2])
        new_lect_obj = self.pool.get(new_lect_model)
        lectures_obj = self.pool.get(model)
        for lect_id in lect_ids:
            vals = {}
            lectura_info = lectures_obj.read(cursor, uid, lect_id)
            if lectura_info['name'] == date:
                for key in lectura_info:
                    atr = lectura_info[key]
                    if isinstance(atr, tuple):
                        atr = atr[0]
                    vals.update({key: atr})
                vals.update({'comptador': meter_id})
                new_lect_ids.append(new_lect_obj.create(cursor, uid, vals))
        return new_lect_ids

    def onchange_section_id(self, cursor, uid, ids, section_id, polissa_id,
                            cups_id, context=None):

        res = super(GiscedataOt, self).onchange_section_id(
            cursor, uid, ids, section_id, polissa_id, cups_id, context=context
        )

        if not res['value'].get('name', False):
            return res

        sect_obj = self.pool.get("crm.case.section")
        new_section_info = sect_obj.read(cursor, uid, section_id, ['name', 'code'])
        section_code = new_section_info['code']
        code_name = self.DEFAULT_CLOSE_CODE.get(section_code, False)
        if not code_name:
            return res

        close_obj = self.pool.get('giscedata.ot.tancament')
        close_id = close_obj.search(cursor, uid, [('name', '=', code_name)])
        if len(close_id) == 0:
            return res
        else:
            res['value'].update({'close_id': close_id[0]})
            return res

    def create(self, cursor, uid, vals, context=None):
        if vals.get('section_id', False) and not vals.get('close_id', False):
            sid = vals['section_id']
            pol_id = vals.get('polissa_id')
            cups_id = vals.get('cups_id')
            extr_vals = self.onchange_section_id(cursor, uid, None, sid, pol_id,
                                                 cups_id)
            vals.update(extr_vals['value'])

        res = super(GiscedataOt, self).create(cursor, uid, vals, context)
        if not vals.get('old_meter_id', False):
            self.update_meters(cursor, uid, res, context=context)
        return res

    def calc_ot_type(self, cursor, uid, section_id, context=None):
        imd_obj = self.pool.get('ir.model.data')
        ogtc_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_ot_comptadors', 'ot_ogtc'
        )
        ogtc_id = ogtc_id[1]

        section_obj = self.pool.get("crm.case.section")
        section_info = section_obj.read(cursor, uid, section_id, ['parent_id'], context=context)
        if section_info['parent_id'] and section_info['parent_id'][0] == ogtc_id:
            return 'comptadors'
        else:
            return super(GiscedataOt, self).calc_ot_type(cursor, uid, section_id, context=context)

    _columns = {
        'old_meter_id': fields.many2one('giscedata.ot.comptador', 'N.Serie', select="2"),
        'old_meter_lect': fields.related('old_meter_id', 'lectures',
                                         type='one2many',
                                         relation='giscedata.ot.lectura',
                                         string="Lectures", store=False),
        'old_meter_lect_pot': fields.related('old_meter_id', 'lectures_pot',
                                             type='one2many',
                                             relation='giscedata.ot.potencia',
                                             string="Lectures Potència",
                                             store=False),
        'old_meter_intensitat': fields.related('old_meter_id', 'icp_intensitat',
                                               type='integer',
                                               string="Intensitat"),
        'old_meter_num_pols': fields.related('old_meter_id', 'icp_num_pols',
                                             type='selection',
                                             selection=[('2', '2'), ('3', '3')],
                                             string="Num.Pols"),
        'new_meter_id': fields.many2one('giscedata.ot.comptador', 'N.Serie'),
        'new_meter_lect': fields.related('new_meter_id', 'lectures',
                                         type='one2many',
                                         relation='giscedata.ot.lectura',
                                         string="Lectures", store=False),
        'new_meter_lect_pot': fields.related('new_meter_id', 'lectures_pot',
                                             type='one2many',
                                             relation='giscedata.ot.potencia',
                                             string="Lectures Potència",
                                             store=False),
        'new_meter_intensitat': fields.related('new_meter_id', 'icp_intensitat',
                                               type='integer',
                                               string="Intensitat"),
        'new_meter_num_pols': fields.related('new_meter_id', 'icp_num_pols',
                                             type='selection',
                                             selection=[('2', '2'), ('3', '3')],
                                             string="Num.Pols"),
        'close_id': fields.many2one('giscedata.ot.tancament', 'Codi Tancament',
                                    select=1),
        'ot_type': fields.selection(_ot_types, string='Tipus de OT'),
        'tensio_id': fields.many2one('giscedata.tensions.tensio', 'Tensió'),

    }


GiscedataOt()


class GiscedataOtComptador(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.ot.comptador'
    _inherit = 'giscedata.ot.comptador'

    def _check_comptador_actiu_unic(self, obj, cursor, uid, ids):
        return True

    def __init__(self, pool, cursor):
        super(GiscedataOtComptador, self).__init__(pool, cursor)
        for i in range(len(self._constraints)):
            constraint = self._constraints[i]
            if constraint[0].func_name == '_check_comptador_actiu_unic':
                del self._constraints[i]
                break

    _columns = {
        'ot_id': fields.many2one('giscedata.ot', 'Ordre de Treball',
                                 required=True, ondelete='cascade'),
    }


GiscedataOtComptador()
