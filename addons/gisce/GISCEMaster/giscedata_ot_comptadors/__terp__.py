# -*- coding: utf-8 -*-
{
    "name": "GISCE Ordres de Treball",
    "description": """Ordres de Treball per treballs amb comptadors.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_ot",
        "giscedata_lectures_distri"
    ],
    "init_xml": [],
    "demo_xml": ['giscedata_ot_demo.xml'],
    "update_xml":[
        "wizard/wizard_new_comptador_view.xml",
        "wizard/wizard_activar_ot_view.xml",
        "giscedata_ot_report.xml",
        "giscedata_ot_comptador_view.xml",
        "giscedata_ot_view.xml",
        "giscedata_ot_data.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
