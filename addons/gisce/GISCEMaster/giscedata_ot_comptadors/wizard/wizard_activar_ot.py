# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _

from datetime import datetime

_INFO_PREFIX = u'\n-----' + _(u'COMPTADORS') + u'-----\n'


class WizardActivarOT(osv.osv_memory):
    """
    Wizard per a l'activació de Ordres de Treball.
    """
    _name = 'wizard.activar.ot'
    _inherit = 'wizard.activar.ot'    

    def _default_info(self, cursor, uid, context=None):
        super_text = super(WizardActivarOT, self)._default_info(
            cursor, uid, context=context)
        super_text += _INFO_PREFIX
        super_text += _(
            u"Es copiaran les lectures i els comptadors de les"
            u" Ordres de Treball a les seves polisses."
        )
        return super_text

    def action_active_ot(self, cursor, uid, wizard_id, context=None):
        """
        Activa la OT copiant les lectures i els comptadors a la
        pòlissa associada.
        """
        if context is None:
            context = {}
        if isinstance(wizard_id, list):
            wizard_id = wizard_id[0]

        ot_obj = self.pool.get('giscedata.ot')
        sw_obj = self.pool.get('giscedata.switching')
        pol_obj = self.pool.get('giscedata.polissa')
        wiz = self.browse(cursor, uid, wizard_id, context)

        context['product_lloguer_id'] = wiz.lloguer and wiz.product_id and wiz.product_id.id

        info = _INFO_PREFIX
        for active_id in context.get('active_ids', []):
            current_ot = ot_obj.browse(cursor, uid, active_id, context)
            info = _(u"{0}\n{1} (id {2}):").format(
                info, current_ot.name, current_ot.id)
            pol_id = current_ot.polissa_id
            if not pol_id:
                info += _(
                    u"No s'han mogut comptadors de la OT:"
                    u" No té polissa assignada"
                 ).format(current_ot.id, current_ot.name)
                continue
            pol_id = pol_id.id
            polissa = pol_obj.browse(cursor, uid, pol_id)
            close = current_ot.close_id
            if current_ot.old_meter_id:
                ctx = context.copy()
                ctx['force_connect_meter'] = close and close.connect_old_meter
                new_info = self.process_ot_meter(
                    cursor, uid, polissa, current_ot.old_meter_id, context=ctx
                )
                info = u"{0}\n{1}".format(info, new_info)
                polissa = pol_obj.browse(cursor, uid, pol_id)
                if (current_ot.new_meter_id or
                        (close and close.disconnect_old_meter)):
                    new_info = self.baixa_ot_meter(
                        cursor, uid, polissa, current_ot.old_meter_id, context=ctx
                    )
                    info = u"{0}\n{1}".format(info, new_info)
                    polissa = pol_obj.browse(cursor, uid, pol_id)

            if current_ot.new_meter_id:
                ctx = context.copy()
                ctx['force_connect_meter'] = close and close.connect_new_meter
                new_info = self.process_ot_meter(
                    cursor, uid, polissa, current_ot.new_meter_id, context=ctx
                )
                info = u"{0}\n{1}".format(info, new_info)
                polissa = pol_obj.browse(cursor, uid, pol_id)
                if close and close.disconnect_new_meter:
                    new_info = self.baixa_ot_meter(
                        cursor, uid, polissa, current_ot.new_meter_id, context=ctx
                    )
                    info = u"{0}\n{1}".format(info, new_info)

            ctx = context.copy()
            ctx['active_ids'] = [active_id]
            super(WizardActivarOT, self).action_active_ot(
                cursor, uid, wizard_id, context=ctx
            )

            sw_ref = None
            if current_ot.ref and current_ot.ref.split(",")[0] == "giscedata.switching":
                sw_ref = current_ot.ref
            elif current_ot.ref2 and current_ot.ref2.split(",")[0] == "giscedata.switching":
                sw_ref = current_ot.ref2

            if sw_ref:
                sw_id = int(sw_ref.split(",")[1])
                # No need to check for actual OT as it is already at 'done'
                if len(ot_obj.search(cursor, uid, [
                    '|', ('ref', '=', sw_ref), ('ref2', '=', sw_ref),
                    ('state', 'not in', ('done', 'cancel'))
                ])) == 0:
                    sw_obj.write(cursor, uid, sw_id, {'state': 'open'})
        wiz.write({'state': 'done', 'info': info})
        return True

    def process_ot_meter(self, cursor, uid, polissa, ot_meter, context=None):
        """ Creates a meter in the contract like the OT one and writes the
        readings in it."""
        if context is None:
            context = {}
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        lectura_pot_obj = self.pool.get('giscedata.lectures.potencia')
        lect_pot_ot_obj = self.pool.get('giscedata.ot.potencia')
        lectura_obj = self.pool.get('giscedata.lectures.lectura')
        lectura_ot_obj = self.pool.get('giscedata.ot.lectura')
        ot_meter_obj = self.pool.get('giscedata.ot.comptador')

        polissa_meter = False
        for comptador in polissa.comptadors:
            if comptador.name.zfill(9) == ot_meter.name.zfill(9):
                polissa_meter = comptador.id
                break
        if not polissa_meter:
            context.update({"polissa_id": polissa.id})
            vals = ot_meter_obj.get_vals_to_copy(cursor, uid, ot_meter.id, context=context)
            if context.get("product_lloguer_id") and 'product_lloguer_id' not in vals:
                vals.update({
                    'lloguer': True,
                    'product_lloguer_id': context.get("product_lloguer_id"),
                })
            polissa_meter = meter_obj.create(cursor, uid, vals)
            info = _(
                u"  * S'ha creat el comptador {0} a la pólissa {1}. "
            ).format(ot_meter.name, polissa.name)
        else:
            vals = ot_meter_obj.get_vals_to_update(cursor, uid, ot_meter.id, context=context)
            meter_obj.write(cursor, uid, polissa_meter, vals)
            info = _(
                u"  * El comptador {0} ja existia a la pólissa {1}. "
            ).format(ot_meter.name, polissa.name)

        if context.get("force_connect_meter"):
            meter_obj.write(
                cursor, uid, polissa_meter,
                {'data_baixa': False, 'active': True}
            )
        n_new_lects = 0
        n_new_pot_lects = 0
        for lect in ot_meter.lectures:
            equal_lectures = lectura_obj.search(
                cursor, uid, [
                    ('comptador', '=', polissa_meter),
                    ('tipus', '=', lect.tipus),
                    ('periode', '=', lect.periode.id),
                    ('name', '=', lect.name)
                ]
            )
            if not equal_lectures:
                new_lect_vals = lectura_ot_obj.copy_data(
                    cursor, uid, lect.id, context=context
                )[0]
                new_lect_vals.update({'comptador': polissa_meter})
                lectura_obj.create(cursor, uid, new_lect_vals, context=context)
                n_new_lects += 1

        for lect_pot in ot_meter.lectures_pot:
            equal_pot_lectures = lectura_pot_obj.search(
                cursor, uid, [
                    ('comptador', '=', polissa_meter),
                    ('periode', '=', lect_pot.periode.id),
                    ('name', '=', lect_pot.name)
                ]
            )
            if not equal_pot_lectures:
                new_lect_vals = lect_pot_ot_obj.copy_data(
                    cursor, uid, lect_pot.id, context=context
                )[0]
                new_lect_vals.update({'comptador': polissa_meter})
                lectura_pot_obj.create(
                    cursor, uid, new_lect_vals, context=context
                )
                n_new_pot_lects += 1
        info = _(
            u"{0}\n     S'han afegit {1} lectures d'energia i {2} de maxímetre."
        ).format(info, n_new_lects, n_new_pot_lects)
        return info

    def baixa_ot_meter(self, cursor, uid, polissa, ot_meter, context=None):
        """ Deactive the polissa meter that's like the OT one. """
        if context is None:
            context = {}
        polissa_meter_id = False
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        for comptador in polissa.comptadors:
            if comptador.name.zfill(9) == ot_meter.name.zfill(9):
                polissa_meter_id = comptador.id
                break

        if polissa_meter_id:
            most_recent_date = False
            for lectura in ot_meter.lectures:
                most_recent_date = max(lectura.name, most_recent_date)
            if not most_recent_date:
                most_recent_date = datetime.today().strftime('%Y-%m-%d')
            meter_obj.write(cursor, uid, polissa_meter_id, {
                'active': False, 'data_baixa': most_recent_date
            })
            info = _(
                u"  * S'ha donat de baixa el comptador {0} en data {1}."
            ).format(ot_meter.name, most_recent_date)
        else:
            info = (
                u"  * No s'ha donat de baixa el comptador {0}: no s'ha trobat "
                u"cap comptador amb el mateix numero de serie a la pólissa {1}."
            ).format(ot_meter.name, polissa.name)
        return info

    _columns = {
        'lloguer': fields.boolean('Aplicar lloguer'),
        'product_id': fields.many2one('product.product', 'Producte de lloguer'),
    }

    _defaults = {
        'lloguer': False,
        'info': _default_info,
    }


WizardActivarOT()
