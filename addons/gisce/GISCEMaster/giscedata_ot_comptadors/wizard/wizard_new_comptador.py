# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime, timedelta
from tools.translate import _


class NewComptadorOt(osv.osv_memory):
    _name = "wizard.new.comptador.ot"

    def create_meter(self, cursor, uid, ids, context=None):
        meter_ot_obj = self.pool.get('giscedata.ot.comptador')
        wizard = self.browse(cursor, uid, ids[0], context=context)
        vals = {
            'ot_id': wizard.ot_id.id,
            'polissa': wizard.polissa_id.id,
            'name': wizard.name,
            'lloguer': wizard.lloguer,
            'giro': wizard.giro,
            'active': wizard.active,
            'data_alta': wizard.data_alta,
            'data_baixa': wizard.data_baixa,
            'propietat': wizard.propietat,
            'num_precinte': wizard.num_precinte,
            'data_precinte': wizard.data_precinte,
            'icp_num_pols': wizard.icp_num_pols,
            'icp_intensitat': wizard.icp_intensitat
        }
        new = meter_ot_obj.create(cursor, uid, vals)
        ot_obj = self.pool.get('giscedata.ot')
        if context.get('new_meter'):
            old = ot_obj.read(cursor, uid, wizard.ot_id.id, ['new_meter_id'])
            if old['new_meter_id']:
                cursor.execute("DELETE FROM giscedata_ot_comptador WHERE id = {0}".format(old['new_meter_id'][0]))
            ot_obj.write(cursor, uid, wizard.ot_id.id, {'new_meter_id': new})
        elif context.get('old_meter'):
            old = ot_obj.read(cursor, uid, wizard.ot_id.id, ['old_meter_id'])
            if old['old_meter_id']:
                cursor.execute("DELETE FROM giscedata_ot_comptador WHERE id = {0}".format(old['old_meter_id'][0]))
            ot_obj.write(cursor, uid, wizard.ot_id.id, {'old_meter_id': new})
        return {}

    _columns = {
        'ot_id': fields.many2one('giscedata.ot', 'Ordre de Treball',
                                 required=True),
        'polissa_id': fields.many2one('giscedata.polissa', 'Pólissa',
                                      required=False),
        'name': fields.char('Num. de sèrie', size=32, required=True),
        'lloguer': fields.boolean('Lloguer'),
        'giro': fields.integer('Gir'),
        'active': fields.boolean('Actiu'),
        'data_alta': fields.date('Data Alta', select="1"),
        'data_baixa': fields.date('Data Baixa', select="1"),
        'propietat': fields.selection([('empresa', 'Empresa'),
                                       ('client', 'Client')], "Propietat de"),
        'num_precinte': fields.char("Num. Precinte", 50),
        'data_precinte': fields.date("Data Precinte"),
        'icp_num_pols': fields.selection([('2', '2'),
                                          ('3', '3')], 'Num. Pols',),
        'icp_intensitat': fields.integer(string="Intensitat"),
        'is_new_meter': fields.boolean()
    }

    def check_num_contador(self, cursor, uid, ids):
        lot_prod_obj = self.pool.get('stock.production.lot')
        for wizard in self.read(cursor, uid, ids, ['name', 'is_new_meter']):
            if wizard['is_new_meter']:
                lot_prod_ids = lot_prod_obj.search(
                    cursor, uid, [('name', '=', wizard['name'])])
                if not lot_prod_ids:
                    return False
        return True

    _constraints = [
        (check_num_contador, _(u"El Número de Serie no existeix al magatzem."),
         ["name"]),
    ]

    def _get_polissa_from_ot(self, cursor, uid, context=None):
        return context.get('polissa', '')

    def _get_date_from_ot(self, cursor, uid, context=None):
        new_date = datetime.today()
        if context.get('work_date', ''):
            work_date_str = context.get('work_date', '')
            work_date = datetime.strptime(work_date_str, '%Y-%m-%d %H:%M:%S')
            if context.get('new_meter'):
                new_date = work_date + timedelta(days=1)
            else:
                new_date = work_date
        return new_date.strftime("%Y-%m-%d")

    def _is_new_meter(self, cursor, uid, context=None):
        return context.get('new_meter')

    _defaults = {
        'polissa_id': _get_polissa_from_ot,
        'lloguer': lambda *a: 0,
        'giro': lambda *a: 0,
        'active': lambda *a: 1,
        'data_alta': _get_date_from_ot,
        'propietat': lambda *a: 'empresa',
        'is_new_meter': _is_new_meter,
    }

NewComptadorOt()
