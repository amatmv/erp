# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('Set ot_type to "comptadors" when section is child of OGTC')

    cursor.execute("""
        UPDATE giscedata_ot
        SET ot_type = 'comptadors'
        WHERE id in (
            SELECT ot.id FROM giscedata_ot ot JOIN crm_case cas ON ot.crm_id = cas.id JOIN crm_case_section sect ON sect.id=cas.section_id
            WHERE sect.parent_id = (SELECT id FROM crm_case_section WHERE code = 'OGTC') or sect.code = 'OGTC'
        )
    """)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
