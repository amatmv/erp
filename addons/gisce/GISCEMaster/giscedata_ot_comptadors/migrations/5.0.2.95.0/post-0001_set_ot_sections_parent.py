# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('Set the parent of all sections child of OOSS to OGTC')

    cursor.execute("""
        UPDATE crm_case_section
        SET parent_id = (SELECT id FROM crm_case_section WHERE code = 'OGTC')
        WHERE parent_id = (SELECT id FROM crm_case_section WHERE code = 'OOSS') and code != 'OGTC'
    """)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
