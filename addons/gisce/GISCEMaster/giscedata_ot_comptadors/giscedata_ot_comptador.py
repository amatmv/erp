# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
import time
from giscedata_lectures.defs import *
from giscedata_lectures.giscedata_lectures import recalc_consums
from giscedata_lectures_tecnologia.giscedata_lectures import TECHNOLOGY_TYPE_SEL
from osv.expression import OOQuery
from datetime import datetime, timedelta


class GiscedataOtComptador(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.ot.comptador'

    # Codi propi de OT
    def get_fields_to_fill(self, cursor, uid, context=None):
        return self._columns.keys()

    def get_vals_to_copy(self, cursor, uid, ot_meter_id, context=None):
        """
        Retorna els valors que s'utilitzaran per crear un
        giscedata.lectures.comptador utilitzant els valors
        del giscedata.ot.comptador
        """
        if context is None:
            context = {}

        if isinstance(ot_meter_id, (list, tuple)):
            ot_meter_id = ot_meter_id[0]

        vals = {}
        ot_meter = self.browse(cursor, uid, ot_meter_id)

        lectura_mes_antiga = datetime.today().strftime('%Y-%m-%d')
        have_lects = False
        for lect in ot_meter.lectures:
            have_lects = True
            lectura_mes_antiga = min(lect.name, lectura_mes_antiga)

        stock_obj = self.pool.get("stock.production.lot")
        serial_id = stock_obj.search(cursor, uid, [('name', '=', ot_meter.name)])
        if len(serial_id):
            serial_id = serial_id[0]
        else:
            serial_id = False

        if ot_meter.polissa:
            polissa_id = ot_meter.polissa.id
        elif context.get("polissa_id"):
            polissa_id = context.get("polissa_id")
        else:
            ot_obj = self.pool.get("giscedata.ot")
            ot_id = ot_obj.search(
                cursor, uid,
                ['|',
                 ('old_meter_id', '=', ot_meter_id),
                 ('new_meter_id', '=', ot_meter_id)
                ]
            )[0]
            polissa_id = ot_obj.read(cursor, uid, ot_id, ['polissa_id'])
            polissa_id = polissa_id['polissa_id'][0]

        data_alta = ot_meter.data_alta
        if have_lects:
            # If date is from first reading we add 1 day to avoid having 2
            # active meters at the same day
            data_alta = datetime.strptime(lectura_mes_antiga, "%Y-%m-%d")
            data_alta = data_alta + timedelta(days=1)
            data_alta = data_alta.strftime('%Y-%m-%d')

        vals.update({
            'polissa': polissa_id,
            'data_alta': data_alta,
            'name': ot_meter.name.zfill(9),
            'giro': ot_meter.giro,
            'num_precinte': ot_meter.num_precinte,
            'data_precinte': ot_meter.data_precinte,
            'serial': serial_id
        })

        self.update_vals_from_characteristic(cursor, uid, serial_id, vals, context=context)

        return vals

    def get_vals_to_update(self, cursor, uid, ot_meter_id, context=None):
        """
        Retorna els valors que s'utilitzaran per actualitzar un
        giscedata.lectures.comptador utilitzant els valors
        del giscedata.ot.comptador
        """
        if context is None:
            context = {}

        if isinstance(ot_meter_id, (list, tuple)):
            ot_meter_id = ot_meter_id[0]

        vals = {}
        ot_meter = self.browse(cursor, uid, ot_meter_id)
        if ot_meter.num_precinte:
            vals.update({
                'num_precinte': ot_meter.num_precinte,
                'data_precinte': ot_meter.data_precinte,
            })

        stock_obj = self.pool.get("stock.production.lot")
        serial_id = stock_obj.search(cursor, uid, [('name', '=', ot_meter.name)])
        if len(serial_id):
            serial_id = serial_id[0]
            self.update_vals_from_characteristic(cursor, uid, serial_id, vals, context=context)

        return vals

    def update_vals_from_characteristic(self, cursor, uid, serial_id, vals, context=None):
        if context is None:
            context = {}
        if serial_id:
            meter_obj = self.pool.get('giscedata.lectures.comptador')
            caracteristiques = meter_obj.get_caracteristiques_comptador(cursor, uid, serial_id, context=context)
            for name, value in caracteristiques.iteritems():
                if name == 'giro':
                    # actualizta el valor del giro
                    if value.isdigit():
                        vals.update({'giro': int(value)})
                if name == 'technology' and value in [x[0] for x in TECHNOLOGY_TYPE_SEL]:
                    # actualitza la tecnologia, i activa/desactiva la check de telegestió
                    vals.update({'technology_type': value})
                    if value in ['prime', 'smmweb']:
                        vals.update({'tg': True})
                    else:
                        vals.update({'tg': False})
                if name == 'alquiler':
                    prod_obj = self.pool.get('product.product')
                    prod_data = prod_obj.search(cursor, uid, [('default_code', '=', value)])
                    if prod_data:
                        vals.update({'product_lloguer_id': prod_data[0]})
                        vals.update({'lloguer': True})

    # Codi duplicat de comptadors de giscedata_lectures
    _METER_TYPE = [
        (u'PF', u'Punt frontera'),
        (u'G', u'Generació'),
        (u'C', u'Consum'),
    ]

    def create(self, cursor, uid, vals, context=None):
        if 'name' in vals:
            vals['name'] = vals['name'].strip()
        res_id = super(GiscedataOtComptador, self).create(cursor, uid, vals, context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        # Codi duplicat de comptadors de giscedata_lectures_distri
        if 'cini' in vals:
            for comptador in self.read(cursor, uid, ids, ['bloquejar_cini']):
                # En el cas que desbloquejem i escrivim el CINI alhora.
                comptador.update(vals)
                if comptador['bloquejar_cini']:
                    del vals['cini']
                    break
        # Codi duplicat de comptadors de giscedata_lectures
        if 'name' in vals:
            vals['name'] = vals['name'].strip()
        return super(GiscedataOtComptador, self).write(cursor, uid, ids, vals, context)

    def unlink(self, cursor, uid, ids, context=None):
        '''if a meter has reads, check if we can unlink it
        It depends on the config variable unlink_meter_with_reads'''

        config = self.pool.get('res.config')
        unlink_meter = int(config.get(cursor, uid,
                           'unlink_meter_with_reads', '0'))
        for comptador in self.browse(cursor, uid, ids):
            if not unlink_meter and comptador.lectures:
                raise osv.except_osv('Error',
                                     _(u"No es poden esborrar comptadors "
                                       u"amb lectures d'energia"))
            if not unlink_meter and comptador.lectures_pot:
                raise osv.except_osv('Error',
                                     _(u"No es poden esborrar comptadors "
                                       u"amb lectures de potència"))

        return super(GiscedataOtComptador, self).unlink(cursor, uid, ids, context=context)

    def _default_polissa(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('polissa_id', False)

    def _check_comptador_actiu_unic(self, cursor, uid, ids):
        '''No mes pot haver-hi un comptador actiu
        amb el mateix numero de serie en polisses actives'''
        q = OOQuery(self, cursor, uid)
        sql = q.select(['name', 'polissa.distribuidora']).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*sql)
        for comptador in cursor.dictfetchall():
            search_params = [
                ('name', '=', comptador['name']),
                ('polissa.state', '!=', 'esborrany'),
                ('active', '=', True),
                ('polissa.distribuidora', '=', comptador['polissa.distribuidora'])
            ]
            q = OOQuery(self, cursor, uid)
            sql = q.select(['id']).where(
                search_params
            )
            cursor.execute(*sql)
            if cursor.rowcount > 1:
                return False
        return True

    def _check_lloguer_required(self, cursor, uid, ids):
        '''Si el comptador es d'empresa ha de tindre lloguer'''
        cfg_obj = self.pool.get('res.config')
        if not int(cfg_obj.get(cursor, uid, 'lloguer_required', 0)):
            return True
        for comptador in self.browse(cursor, uid, ids):
            if (comptador.propietat == 'empresa'
                    and not comptador.lloguer):
                return False
        return True

    def get_consum(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   tipus='A', per_facturar=False, periodes=None, context=None):
        """Calcula el consum.
        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        consums = {}
        lectures = self.get_lectures(cursor, uid, comptador_id, tarifa_id,
                                     from_date, tipus, per_facturar, periodes,
                                     context)
        for periode in lectures.keys():
            consums[periode] = lectures[periode]['actual']['consum']
        return consums

    def get_generacio(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   tipus='A', per_facturar=False, periodes=None, context=None):
        """Calcula la generacio.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        generacions = {}
        lectures = self.get_lectures(cursor, uid, comptador_id, tarifa_id,
                                     from_date, tipus, per_facturar, periodes,
                                     context)
        for periode in lectures.keys():
            generacions[periode] = lectures[periode]['actual']['generacio']
        return generacions

    def get_lectures(self, cursor, uid, comptador_id, tarifa_id, from_date,
                    tipus='A', per_facturar=False, periodes=None, context=None):
        """Busca les lectures actuals i anteriors.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id)
        lectures = {}
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        if not periodes:
            ctx = context.copy()
            ctx.update({'active_test': False})
            periodes_ids = periodes_obj.search(cursor, uid, search_params,
                                               context=ctx)
        else:
            periodes_ids = periodes[:]
        operator = '='
        if per_facturar:
            operator = '>'
        search_params = [
            ('comptador.id', '=', comptador_id),
            ('periode.id', 'in', periodes_ids),
            ('name', operator, from_date),
            ('tipus', '=', tipus),
        ]
        # Si el comptador té data de baixa la lectura no pot ser més gran
        # que la data de baixa
        if comptador.data_baixa:
            search_params += [('name', '<=', comptador.data_baixa)]
        # Especial per les rectificatives, li podem dir fins a quina pot
        # arribar.
        if 'fins_lectura_fact' in context:
            search_params.extend([
                ('name', '<=', context['fins_lectura_fact'])
            ])
        lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
        # Si es per facturar cerquem si hi ha darreres lectures
        # per tindre-les en compte encara que hi hagin lectures posteriors
        # dins el mateix periode
        if per_facturar and not context.get('from_perfil', False):
            search_params.append(('origen_id.codi', '=', 'DR'))
            lectures_dl_ids = lectures_obj.search(cursor, uid, search_params,
                                                  context={'active_test': False}
                                                  )
            if lectures_dl_ids:
                lectures_ids = lectures_dl_ids

        # En el cas que no hi hagi lectures ho inicialitzem pel validador
        if not lectures_ids:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {}
                lectures[periode.name]['actual'] = {}
                lectures[periode.name]['anterior'] = {}
        for lectura in lectures_obj.browse(cursor, uid, lectures_ids, context):
            periode = lectura.periode.name
            # Si ja hem assignat la lectura per aqueset periode passem
            if periode in lectures:
                continue
            lectures[periode] = {}
            lectures[periode]['actual'] = lectures_obj.read(cursor, uid,
                                                            lectura.id)
            # Té el mateix siginificat que False pq evalua així, però podem
            # fet .get(key, default)
            lectures[periode]['anterior'] = {}
            # posem l'order per defecte (no facturar)
            order = "name desc"
            search_params = [
                ('comptador.id', '=', comptador_id),
                ('periode.id', '=', lectura.periode.id),
                ('name', '<', lectura.name),
                ('tipus', '=', tipus)
            ]
            # Si és per facturar mirem les lectures des d'on volem facturar
            # pel cas que tingui més d'una lectura en el mateix periode.
            # L'order = "name asc" ens assegura que vindran en el bon ordre
            if per_facturar:
                search_params.append(('name', '>=', from_date))
                order = "name asc"
            lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                               order=order,
                                               context={'active_test': False})
            if lectures_ids:
                lectures[periode]['anterior'] = lectures_obj.read(
                    cursor, uid, lectures_ids[0]
                )
                # En el cas que hi hagi més d'una lectura intermitja sumem
                # els consums
                for idx in range(1, len(lectures_ids)):
                    l_tmp = lectures_obj.browse(cursor, uid, lectures_ids[idx])
                    lectures[periode]['actual']['consum'] += l_tmp.consum
        return recalc_consums(lectures, comptador.giro)

    _columns = {
        # Columns propi de OT
        'polissa': fields.many2one('giscedata.polissa', 'Polissa',
                                   required=False, select="1",
                                   readonly=True,
                                   ondelete='cascade'),
        'lectures': fields.one2many('giscedata.ot.lectura', 'comptador', 'Lectures'),
        'lectures_pot': fields.one2many('giscedata.ot.potencia', 'comptador', 'Lectures Potència'),
        'icp_num_pols': fields.selection([('2', '2'), ('3', '3')], 'Num. Pols'),
        'icp_intensitat': fields.integer(string="Intensitat"),
        # Columns duplicats de comptadors de giscedata_lectures
        'name': fields.char('Nº de sèrie', size=32, required=True),
        'lloguer': fields.boolean('Lloguer'),
        'giro': fields.integer('Gir'),
        'active': fields.boolean('Actiu'),
        'data_alta': fields.date('Data Alta', select="1"),
        'data_baixa': fields.date('Data Baixa', select="1"),
        'propietat': fields.selection(PROPIETAT_COMPTADOR_SELECTION, "Propietat de"),
        'meter_type': fields.selection(_METER_TYPE, 'Tipus de comptador', required=True),
        # Columns duplicats de giscedata_lectures_distri
        'serial': fields.many2one("stock.production.lot",
                                  "Nº de sèrie (magatzem)", select="1"),
        'product_id': fields.related('serial', 'product_id', type='many2one',
                                     relation='product.product', store=True,
                                     readonly=True, string="Producte"),
        'tensio': fields.integer('Tensió'),
        'cini': fields.char('CINI', size=256),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'num_precinte': fields.char("Num. Precinte", 50),
        'data_precinte': fields.date("Data Precinte"),
    }

    # Codi duplicat de comptadors de giscedata_lectures
    _order = 'data_alta desc'

    _defaults = {
        # Defaults duplicats de comptadors de giscedata_lectures
        'lloguer': lambda *a: 0,
        'giro': lambda *a: 0,
        'active': lambda *a: 1,
        'data_alta': lambda *a: time.strftime('%Y-%m-%d'),
        'propietat': lambda *a: 'empresa',
        'polissa': _default_polissa,
        'meter_type': lambda *a: u'PF',
        # Defaults duplicats de comptadors de giscedata_lectures_distri
        'bloquejar_cini': lambda *a: 0,
    }

    _sql_constraints = [
        ('data_baixa_alta', 'CHECK (data_baixa >= data_alta)',
         _("La data de baixa del comptador no pot ser menor a la data d'alta")
         ),
    ]

    _constraints = [
        (_check_comptador_actiu_unic,
         _(u'Error: Ja existeix un comptador actiu amb aquest '
           u'numero de serie'),
         ['name']),
        (_check_lloguer_required,
         _(u"Un comptador d'empresa ha de tindre un lloguer associat"),
         ['propietat', 'lloguer'])
    ]


GiscedataOtComptador()


class GiscedataOtLectura(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.ot.lectura'
    _inherit = 'giscedata.lectures.lectura'

    def _default_origen(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        orig_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen30'
        )
        return orig_id[1]

    def onchange_meter(self, cursor, uid, ids, meter_id):
        if meter_id:
            meter_obj = self.pool.get('giscedata.ot.comptador')
            meter_info = meter_obj.read(cursor, uid, meter_id, ['ot_id'])
            if meter_info['ot_id']:
                ot_id = meter_info['ot_id'][0]
                ot_obj = self.pool.get('giscedata.ot')
                ot_work_date = ot_obj.read(cursor, uid, ot_id, ['work_date'])
                if ot_work_date['work_date']:
                    return {'value': {'name': ot_work_date['work_date']}}

    def _consum(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataOtLectura, self)._consum(
            cursor, uid, ids, field_name, arg, context
        )

    def _trg_consum(self, cursor, uid, ids, context=None):
        return super(GiscedataOtLectura, self)._trg_consum(
            cursor, uid, ids, context
        )

    _columns = {
        'comptador': fields.many2one('giscedata.ot.comptador', 'Comptador',
                                     required=True, ondelete='cascade'),
        'consum': fields.function(_consum, method=True, string="Consum",
                                  type="integer",
                                  store={'giscedata.ot.lectura': (
                                      _trg_consum, ['lectura', 'ajust'], 10)}),
        'generacio': fields.function(_consum, method=True, string="Generacio",
                                  type="integer",
                                  store={'giscedata.ot.lectura': (
                                      _trg_consum, ['lectura_exporta', 'ajust_exporta'], 10)}),
    }

    _defaults = {
        'origen_id': _default_origen,
    }

GiscedataOtLectura()


class GiscedataOtPotencia(osv.osv):
    """" Lectures de potència """
    _name = 'giscedata.ot.potencia'
    _inherit = 'giscedata.lectures.potencia'

    def onchange_meter(self, cursor, uid, ids, meter_id):
        if meter_id:
            meter_obj = self.pool.get('giscedata.ot.comptador')
            meter_info = meter_obj.read(cursor, uid, meter_id, ['ot_id'])
            if meter_info['ot_id']:
                ot_id = meter_info['ot_id'][0]
                ot_obj = self.pool.get('giscedata.ot')
                ot_work_date = ot_obj.read(cursor, uid, ot_id, ['work_date'])
                if ot_work_date['work_date']:
                    return {'value': {'name': ot_work_date['work_date']}}

    _columns = {
        'comptador': fields.many2one('giscedata.ot.comptador', 'Comptador',
                                     required=True, ondelete='cascade'),
    }

GiscedataOtPotencia()
