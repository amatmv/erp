## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    data_act = str(datetime.today().strftime('%d/%m/%Y'))
    logo = objects[0].user_id.company_id.logo
    _colours = ['white', 'gray']

    def modifData(data):
        data_temp = datetime.strptime(data, '%Y-%m-%d %H:%M:%S')
        _data = data_temp.strftime('%d/%m/%Y')
        return _data

    varconf_o = pool.get('res.config')
    meter_o = pool.get('giscedata.lectures.comptador')
    ct_cups_origin = varconf_o.get(cursor, uid, 'ot_ct_cups_value_origin', 'CUPS_ET')
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_ot_comptadors/report/orden_individual_cambio_contador_tg.css"/>
</head>
<body>
    %for os in objects:
            <div>
                <div class="header_left">
                    %if logo:
                        <img id="img_logo" src="data:image/jpeg;base64,${logo}">
                    %endif
                </div>
                <div class="header_right">
                    <div id="box-sup" style="font-weight: bold; text-align: center;">${"Órdenes de Servicio - "} ${data_act} ${" - "} ${len(objects)} ${" órdenes"}</div>
                </div>
            </div>
            <table class="main_table">
                <tr class="table_header">
                    <td colspan="3">${_("Número: ")}${os.id}</td>
                    <td colspan="7">${_("Actuació: ")}${os.section_id.name}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("F. Apertura:")}</td>
                    <td colspan="3" class="field_value">${os.date}</td>
                    <td colspan="2" class="field_name">${_("Gestor:")}</td>
                    <td colspan="3" class="field_value">${os.user_id.name}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Códi Client:")}</td>
                    <td colspan="3" class="field_value">${os.polissa_name}</td>
                    <td colspan="2" class="field_name">${"CUPS:"}</td>
                    <td colspan="3" class="field_value">${os.cups_name}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Tensió:")}</td>
                    <td colspan="3" class="field_value">${os.polissa_id.tensio_normalitzada.name}</td>
                    <td colspan="2" class="field_name">${_("Pot. Contractada:")}</td>
                    <td colspan="3" class="field_value">${os.polissa_id.potencia}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Tarifa:"}</td>
                    <%
                        ct_cups = ""
                        if ct_cups_origin == 'CUPS_ET':
                            ct_cups = os.cups_et
                        elif ct_cups_origin == 'CONCENTRADOR':
                            meter_id = os.polissa_id.comptadors_actius(os.date)
                            if meter_id:
                                meter = meter_o.browse(cursor, uid, meter_id[0])
                                if meter.tg:
                                    concentrador = meter.tg_cnc_id
                                    if concentrador:
                                        ct_cups = "{} ({})".format(
                                            concentrador.description,
                                            concentrador.name
                                        )
                                else:
                                    ct_cups = os.cups_et
                    %>
                    %if ct_cups:
                        <td colspan="3" class="field_value">${os.polissa_id.tarifa.name}</td>
                        <td colspan="2" class="field_name">${"CT CUPS:"}</td>
                        <td colspan="3" class="field_value">${ct_cups}</td>
                    %else:
                        <td colspan="8" class="field_value">${os.polissa_id.tarifa.name}</td>
                    %endif
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("P. Contacte:")}</td>
                    <td colspan="3" class="field_value">${os.partner_addr_name or ''}</td>
                    <td colspan="2" class="field_name">${_("Telèfon:")}</td>
                    <td colspan="3" class="field_value">${os.partner_telefon or ''}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("NIF/CIF Client:")}</td>
                    <td colspan="8" class="field_value">${os.partner_id.vat}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Nom Client:")}</td>
                    <td colspan="8" class="field_value">${os.ot_partner_name}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Comercialitzadora:")}</td>
                    <td colspan="8" class="field_value">${os.polissa_id.comercialitzadora.name}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Direcció:")}</td>
                    <td colspan="8" class="field_value">${os.cups_address}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Equips de Mesura:")}</td>
                    <td colspan="8" class="field_value">
                        ${"ACTIVA("}${os.old_meter_id.name}${") "}
                        % for i in range(len(os.polissa_id.icps)):
                              ${"ICP "}${i+1}${"("}${os.polissa_id.icps[i].name}${") "}
                        % endfor
                        ${_("MAXÍMETRE(")}${"Si" if os.polissa_id.facturacio_potencia == "max" else "No"}${" "}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Observacions:")}</td>
                    <td colspan="8" class="field_value">${os.description or ''}</td>
                </tr>
            </table>
            <br>
            <table class="main_table">
                <tr class="table_header">
                    <td colspan="10">${_("Dades a cumplimentar per l'operari")}</td>
                </tr>
                <tr class="table_header2">
                    <td colspan="10">${_("Dades generals")}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Data i hora:")}</td>
                    <td colspan="3" class="field_value">__ / __ / _____</td>
                    <td colspan="2" class="field_name">${_("Operari:")}</td>
                    <td colspan="3" class="field_value">${os.operator_id.name if os.operator_id else ''}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">CT:</td>
                    <td colspan="2" class="field_value">_______</td>
                    <td colspan="3" class="field_name">${_("Tipus d'Escomesa [A|S]:")}</td>
                    <td colspan="3" class="field_value">_______</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Línia:")}</td>
                    <td colspan="3" class="field_value">_______</td>
                    <td colspan="2" class="field_name">${_("Actuació:")}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name" style="padding-bottom: 70px;">${_("Observacions:")}</td>
                    <td colspan="3" class="field_value" style="padding-bottom: 70px;"></td>
                    <td colspan="2" class="field_name" style="padding-bottom: 70px;"></td>
                    <td colspan="3" class="field_value" style="padding-bottom: 70px;"></td>
                </tr>
                <tr class="table_header2">
                    <td colspan="10">${_("Dades tècniques")}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${_("Núm. Fases:")}</td>
                    <td colspan="3" class="field_value" style="padding-bottom: 20px;"></td>
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${_("Tensió:")}</td>
                    <td colspan="3" class="field_value" style="padding-bottom: 20px;"></td>
                </tr>
                <tr class="table_header2">
                    <td colspan="10">${_("Dades Equips Mesura NOU")}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Núm. Serie:"}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${_("Tipus Precinte:")}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Marca:")}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${_("Model:")}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("ICP Activat:")}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${_("Propietat:")}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Intensitat:")}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${_("Tensio:")}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Activa Punta:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Activa Llano:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Activa Valle:"}</td>
                    <td colspan="2" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Reactiva 1:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Reactiva 2:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Reactiva 3:"}</td>
                    <td colspan="2" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${_("Maxímetre 1:")}</td>
                    <td colspan="1" class="field_value" style="padding-bottom: 20px;"></td>
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${_("Maxímetre 2:")}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${_("Maxímetre 3:")}</td>
                    <td colspan="2" class="field_value" style="padding-bottom: 20px;"></td>
                </tr>
                <tr class="table_header2">
                    <td colspan="10">${"Dades Equip Mesura RETIRAT"}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Núm. Serie:")}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${_("Tipus ICP:")}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Activa Punta:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Activa Llano:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Activa Valle:"}</td>
                    <td colspan="2" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Reactiva 1:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Reactiva 2:"}</td>
                    <td colspan="1" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Reactiva 3:"}</td>
                    <td colspan="2" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${"Maxímetro 1:"}</td>
                    <td colspan="1" class="field_value" style="padding-bottom: 20px;"></td>
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${"Maxímetro 2:"}</td>
                    <td colspan="1" class="field_value" style="padding-bottom: 20px;"></td>
                    <td colspan="2" class="field_name" style="padding-bottom: 20px;">${"Maxímetro 3:"}</td>
                    <td colspan="2" class="field_value" style="padding-bottom: 20px;"></td>
                </tr>
                <tr class="table_header2">
                    <td colspan="10">${_("Dades de contacte")}</td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Nom:")}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${"NIF/CIF:"}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${_("Tlf. Fixe:")}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${"Tlf.Móvil:"}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
                <tr class="table_body">
                    <td colspan="2" class="field_name">${"Fax:"}</td>
                    <td colspan="3" class="field_value"></td>
                    <td colspan="2" class="field_name">${"E-mail:"}</td>
                    <td colspan="3" class="field_value"></td>
                </tr>
            </table>
            <p style="font-size:10px;"><b>${_("Signat per : {}".format(os.operator_id.name if os.operator_id else ''))}</b></p>
            <p style="font-size:10px;"><b>${_("En data : ")} __ / __ / _____</b></p>
        %if objects[-1] != os:
            <p style="page-break-after:always"></p>
        %endif
    %endfor
</body>
</html>
