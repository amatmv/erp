<%
    from datetime import datetime
    _colours = ['gray', 'white']
    index = 0
    n_jump = 1
    data_act = str(datetime.today().strftime('%d/%m/%Y'))
    logo = objects[0].user_id.company_id.logo
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_ot_comptadors/report/ordres_servei.css"/>
    </head>
    <body>
        <div>
            <div id="box-left">
                %if logo:
                    <img id="img_logo" src="data:image/jpeg;base64,${logo}">
                %endif
            </div>
            <div id="box-right">
                <div id="head_info">
                    <font color="red">
                        ${len(objects)}
                    </font>
                </div>
                <div id="head_title">
                    ${_("Ordres de Servei")} (${data_act})
                </div>
            </div>
        </div>
        <table class="gray" style="font-weight: bold; text-align: center;">
            <tr>
                <td>${_("NÚMERO")}</td>
                <td>${_("D.OBERTURA")}</td>
                <td colspan="4">${_("ACTUACIÓ")}</td>
                 <td colspan="5">${_("DIRECCIÓ")}</td>
            </tr>
            <tr>
                <td colspan="3">${_("GESTOR")}</td>
                <td colspan="3">${_("P.CONTACTE")}</td>
                <td colspan="2">${_("CITA")}</td>
                <td>${_("TELÈFON")}</td>
                <td>${_("TELÈFON 2")}</td>
                <td>${_("TELÈFON 3")}</td>
            </tr>
            <tr>
                <td colspan="2">${_("OPERARI")}</td>
                <td colspan="2">${_("COD. CLIENT")}</td>
                <td colspan="2">${_("CUPS")}</td>
                <td>${_("TENSIÓ")}</td>
                <td colspan="2">${_("P.CONTRACTE")}</td>
                <td colspan="2">${_("CLIENT")}</td>
            </tr>
            <tr>
                <td colspan="11">${_("OBSERVACIONS")}</td>
            </tr>
            <tr>
                <td colspan="11">${_("EQUIPS DE MESURA")}</td>
            </tr>
            <tr>
                <td colspan="2">${_("Notes: E. Mesura")}</td>
                <td colspan="2">${_("Notes: Lectura")}</td>
                <td colspan="2">${_("Notes: Data Lectura")}</td>
                 <td colspan="5">${_("NOTES")}</td>
            </tr>
        </table>
        %for os in sorted(objects, key=lambda k: k.id):
            <%
                index = (index + 1) % 2
                classe = _colours[index]
                n_jump = n_jump + 1
                if os.work_date:
                    data_temp = datetime.strptime(os.work_date, '%Y-%m-%d %H:%M:%S')
                    data = data_temp.strftime('%d/%m/%Y')
                else: data = ''

                comentaris_ot = os.description
                for c in os.history_line:
                    comentaris_ot = "{} {}".format(comentaris_ot, c.description)
            %>
            <table class=${classe}>
                <tr>
                    <td>${os.id}</td>
                    <td>${data}</td>
                    <td colspan="4">${os.section_id.name}</td>
                    <td colspan="5">${os.cups_address and os.cups_address or ''}</td>
                </tr>
                <tr>
                    <td colspan="3">${os.user_id.name}</td>
                    <td colspan="3">
                        %if os.ot_partner_name:
                            ${os.ot_partner_name}
                        %endif
                    </td>
                    <td colspan="2"><!--CITA--></td>
                    <td>
                        %if os.partner_address_id.phone:
                            ${os.partner_address_id.phone}
                        %endif
                    </td>
                    <td>
                        %if os.partner_address_id.mobile:
                            ${os.partner_address_id.mobile}
                        %endif
                    </td>
                    <td><!--TELF3--></td>
                </tr>
                <tr>
                    <td colspan="2">${os.operator_partner_name}</td>
                    <td colspan="2">
                        %if os.polissa_id.name:
                            ${os.polissa_id.name}
                        %endif
                    </td>
                    <td colspan="2">
                        %if os.cups_name:
                            ${os.cups_name}
                        %endif
                    </td>
                    <td>
                        %if os.tensio_id.name:
                            ${os.tensio_id.name}
                        %endif
                    </td>
                    <td colspan="2">
                        %if os.polissa_id.potencia:
                            ${os.polissa_id.potencia}
                        %endif
                    </td>
                    <td colspan="2">
                        %if os.partner_id.name:
                            ${os.partner_id.name}
                        %endif
                    </td>
                </tr>
                <tr>
                    <td colspan="11">
                        %if os.description:
                            ${os.description}
                        %else:
                            <p>&nbsp<p>
                        %endif
                    </td>
                </tr>
                <tr>
                    <td colspan="11">
                        %if os.polissa_id.comptador:
                            ${os.polissa_id.comptador}
                        %else:
                            <p>&nbsp<p>
                        %endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><!--Notas: E. Medidas--></td>
                    <td colspan="2"><!--Notas: Lectura--></td>
                    <td colspan="2"><!--Notas: Fecha Lectura--></td>
                     <td colspan="5">${os.user_id.company_id.name}</td>
                </tr>
                <tr>
                    <td colspan="11">${comentaris_ot}</td>
                </tr>
            </table>
            %if n_jump == 5 and objects[-1] != os:
                <p style="page-break-after:always;"></p><br><br>
                <%  n_jump = 0    %>
            %endif
        %endfor
    </body>
</html>
