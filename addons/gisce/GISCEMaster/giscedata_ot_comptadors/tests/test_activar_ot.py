# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction
from destral.patch import PatchNewCursors
from datetime import datetime, timedelta

from giscedata_polissa.tests.utils import activar_polissa


class TestWizardActiveOt(testing.OOTestCase):
    """ Tests of the wizard to activate OTs. Three cases can be presented:
        - The OT has an old meter that the contract doesn't.
        - The OT has an old meter that the contract has another that's equals.
        - The OT has a new meter that the contract doesn't.
        - The OT has a new meter and the contract has another that's equals.
    """
    
    def setUp(self):
        self.pool = self.openerp.pool
        self.compt_obj = self.pool.get('giscedata.lectures.comptador')
        self.reading_obj = self.pool.get('giscedata.lectures.lectura')
        self.ot_compt_obj = self.pool.get('giscedata.ot.comptador')
        self.ot_obj = self.pool.get('giscedata.ot')
        self.pol_obj = self.pool.get('giscedata.polissa')
        self.ot_wizard_obj = self.pool.get('wizard.activar.ot')
        self.ot_reading_obj = self.pool.get('giscedata.ot.lectura')
        self.imd_obj = self.pool.get('ir.model.data')
        self.close_obj = self.pool.get("giscedata.ot.tancament")
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_active_ot_if_contract_has_not_old_ot_meter(self):
        """ Testing OT activation if contract has not the same old meter.
        Then the wizard will create a meter like the one in the OT with
        its readings. """

        uid = self.txn.user
        cursor = self.txn.cursor

        with PatchNewCursors():
            polissa_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            activar_polissa(self.pool, cursor, uid, polissa_id)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)
            ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot', 'ot_0001'
            )[1]
            ot = self.ot_obj.browse(cursor, uid, ot_id)
            compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0001'
            )[1]
            ot_meter = self.ot_compt_obj.browse(cursor, uid, compt_ot_id)

            # Deleting all the lectures and renaming all the meters
            # associated with the contract in order to have a clean
            # work space.
            for i, meter in enumerate(polissa.comptadors):
                for lect in meter.lectures or []:
                    lect.unlink(context={})
                for lect in meter.lectures_pot or []:
                    lect.unlink(context={})
                # All meter names must be different
                meter.write({'name': 'XXXXX{0}'.format(i)})

            # Now we can assure that there isn't any meter in the contract
            # like the one in the OT
            self.assertFalse(
                [
                    meter.name
                    for meter in polissa.comptadors
                    if meter.name == ot_meter.name
                ]
            )

            # Now we create a stock.production.lot with the name of the ot meter
            # to check if it's used in OT activation to create the emter in
            # the contract
            stock_obj = self.pool.get("stock.production.lot")
            product_id = self.pool.get("product.product").search(cursor, uid, [])[0]
            stock_id = stock_obj.create(cursor, uid, {
                'name': ot_meter.name,
                'product_id': product_id,
                'date': datetime.today().strftime("%Y-%m-%d")
            })

            # Create the wizard to active the OT and pass the meter info
            # to the contract
            wiz_id = self.ot_wizard_obj.create(cursor, uid, {})
            context = {
                'active_id': ot_id,
                'active_ids': [ot_id]
            }

            # We assign manually the contract and the meter to the OT
            ot.write({'polissa_id': polissa_id})
            self.ot_wizard_obj.action_active_ot(cursor, uid, [wiz_id], context)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)

            # Once the OT is activated, there should be a meter like the one
            # in the OT
            comptador_pol = polissa.comptadors[0]
            for comptador in polissa.comptadors:
                if comptador.name.zfill(9) == ot_meter.name.zfill(9):
                    comptador_pol = comptador
                    break
            self.assertEquals(comptador_pol.name.zfill(9), ot_meter.name.zfill(9))

            # Check if contract meter has product and stock
            self.assertEqual(comptador_pol.serial.id, stock_id)
            self.assertEqual(comptador_pol.product_id.id, product_id)

            # We must assure that OT the meter has some readings (there are
            # in the demo data).
            self.assertTrue(ot.old_meter_id.lectures)

            # And now the contract will have the same one
            new_contart_meter_ids = self.compt_obj.search(
                cursor, uid,
                [('polissa', '=', polissa_id)]
            )
            readings_ids = self.compt_obj.read(
                cursor, uid, new_contart_meter_ids, ['lectures']
            )[0]['lectures']

            readings_dict = self.reading_obj.read(
                cursor, uid, readings_ids, ['name']
            )
            reading_names = [
                reading_name['name']
                for reading_name in readings_dict
            ]

            ot_reading_names = [
                ot_reading.name
                for ot_reading in ot.old_meter_id.lectures
            ]
            for reading_name in reading_names:
                self.assertTrue(reading_name in ot_reading_names)

    def test_active_ot_if_contract_has_old_ot_meter(self):
        """ Testing OT activation if contract has the same old meter.
        Then the wizard will only copy the readings to the contract. """

        uid = self.txn.user
        cursor = self.txn.cursor

        with PatchNewCursors():
            polissa_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            activar_polissa(self.pool, cursor, uid, polissa_id)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)
            ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot', 'ot_0001'
            )[1]
            ot = self.ot_obj.browse(cursor, uid, ot_id)
            compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0001'
            )[1]

            # Change the date of the readings because these must be after
            # meter creation date, that will be today
            lect_1_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'lectura_ot_0001'
            )[1]
            lect_2_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'lectura_ot_0002'
            )[1]
            lect_1_date = datetime.now() + timedelta(days=1)
            self.ot_reading_obj.write(cursor, uid, lect_1_ot_id, {
                'name': '{0}'.format(lect_1_date.strftime('%Y-%m-%d'))
            })
            lect_2_date = datetime.now() + timedelta(days=2)
            self.ot_reading_obj.write(cursor, uid, lect_2_ot_id, {
                'name': '{0}'.format(lect_2_date.strftime('%Y-%m-%d'))
            })
            ot_meter = self.ot_compt_obj.browse(cursor, uid, compt_ot_id)

            # Deleting all the lectures and renaming all the meters
            # associated with the contract in order to have a clean
            # work space.
            for i, meter in enumerate(polissa.comptadors):
                for lect in meter.lectures or []:
                    lect.unlink(context={})
                for lect in meter.lectures_pot or []:
                    lect.unlink(context={})
                # All meter names must be different
                meter.write({'name': 'XXXXX{0}'.format(i)})

            # We create a meter in the contract like the one in the OT
            contract_meter_id = self.compt_obj.create(cursor, uid, {
                'name': ot_meter.name,
                'polissa': polissa.id
            })

            # Create the wizard to active the OT and pass the meter info
            # to the contract
            wiz_id = self.ot_wizard_obj.create(cursor, uid, {})
            context = {
                'active_id': ot_id,
                'active_ids': [ot_id]
            }

            # We assign manually the contract
            ot.write({'polissa_id': polissa_id})
            self.ot_wizard_obj.action_active_ot(cursor, uid, [wiz_id], context)

            # And now the contract will have the same one
            readings_ids = self.compt_obj.read(
                cursor, uid, contract_meter_id, ['lectures']
            )
            if isinstance(readings_ids, list):
                readings_ids = readings_ids[0]['lectures']
            else:
                readings_ids = readings_ids['lectures']

            readings_dict = self.reading_obj.read(
                cursor, uid, readings_ids, ['name']
            )
            reading_names = [
                reading_name['name']
                for reading_name in readings_dict
            ]

            ot_reading_names = [
                ot_reading.name
                for ot_reading in ot.old_meter_id.lectures
            ]
            for reading_name in reading_names:
                self.assertTrue(reading_name in ot_reading_names)

    def test_active_ot_if_contract_has_not_new_ot_meter(self):
        """ Testing OT activation if contract has not the same new meter.
        Then the wizard will cancel the old meter and will create a new one
        like the new meter in the OT with its readings. """

        uid = self.txn.user
        cursor = self.txn.cursor

        with PatchNewCursors():
            polissa_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            activar_polissa(self.pool, cursor, uid, polissa_id)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)
            ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot', 'ot_0002'
            )[1]
            ot = self.ot_obj.browse(cursor, uid, ot_id)

            old_compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0001'
            )[1]
            new_compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0002'
            )[1]

            # Change the date of the readings because these must be after
            # meter creation date, that will be today
            lect_3_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'lectura_ot_0003'
            )[1]
            lect_4_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'lectura_ot_0004'
            )[1]
            lect_3_date = datetime.now() + timedelta(days=1)
            self.ot_reading_obj.write(cursor, uid, lect_3_ot_id, {
                'name': '{0}'.format(lect_3_date.strftime('%Y-%m-%d'))
            })
            lect_4_date = datetime.now() + timedelta(days=2)
            self.ot_reading_obj.write(cursor, uid, lect_4_ot_id, {
                'name': '{0}'.format(lect_4_date.strftime('%Y-%m-%d'))
            })

            # We create a meter in the contract like the one in the OT
            new_compt_ot_name = self.ot_compt_obj.read(
                cursor, uid, new_compt_ot_id, ['name']
            )['name']

            self.compt_obj.create(cursor, uid, {
                'name': new_compt_ot_name,
                'polissa': polissa.id
            })

            # Create the wizard to active the OT and pass the meter info
            # to the contract
            wiz_id = self.ot_wizard_obj.create(cursor, uid, {})
            context = {
                'active_id': ot_id,
                'active_ids': [ot_id]
            }

            # We assign manually the contract
            ot.write({'polissa_id': polissa_id})
            self.ot_wizard_obj.action_active_ot(cursor, uid, [wiz_id], context)

            # The meter that is equals to the old OT meter should be
            # non active
            old_ot_meter_name = self.ot_compt_obj.read(
                cursor, uid, old_compt_ot_id, ['name']
            )['name']
            non_active_old_contracts = self.compt_obj.search(
                cursor, uid, [
                    ('name', '=', old_ot_meter_name),
                    ('active', '=', False)
                ]
            )
            self.assertTrue(non_active_old_contracts)

            # Once the OT is activated, there should be a meter like the new
            # one in the OT
            new_ot_meter_name = self.ot_compt_obj.read(
                cursor, uid, new_compt_ot_id, ['name']
            )
            self.assertEquals(polissa.comptador, new_ot_meter_name['name'])

            # We must assure that OT the meter has some readings (there are
            # in the demo data).
            self.assertTrue(ot.new_meter_id.lectures)

            # And now the contract will have the same one
            new_contract_meter_ids = self.compt_obj.search(
                cursor, uid,
                [('polissa', '=', polissa_id)]
            )
            readings_ids = self.compt_obj.read(
                cursor, uid, new_contract_meter_ids, ['lectures']
            )[0]['lectures']

            readings_dict = self.reading_obj.read(
                cursor, uid, readings_ids, ['name']
            )
            reading_names = [
                reading_name['name']
                for reading_name in readings_dict
            ]

            ot_reading_names = [
                ot_reading.name
                for ot_reading in ot.new_meter_id.lectures
            ]
            for reading_name in reading_names:
                self.assertTrue(reading_name in ot_reading_names)

    def test_active_ot_if_contract_has_new_ot_meter(self):
        """ Testing OT activation if contract the same new meter.
        Then the wizard will cancel the old meter and will copy the new OT
        meter readings to the contract meter. """

        uid = self.txn.user
        cursor = self.txn.cursor

        with PatchNewCursors():
            polissa_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            activar_polissa(self.pool, cursor, uid, polissa_id)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)
            ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot', 'ot_0002'
            )[1]
            ot = self.ot_obj.browse(cursor, uid, ot_id)
            old_compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0001'
            )[1]
            new_compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0002'
            )[1]

            # Create the wizard to active the OT and pass the meter info
            # to the contract
            wiz_id = self.ot_wizard_obj.create(cursor, uid, {})
            context = {
                'active_id': ot_id,
                'active_ids': [ot_id]
            }

            # We assign manually the contract
            ot.write({'polissa_id': polissa_id})
            self.ot_wizard_obj.action_active_ot(cursor, uid, [wiz_id], context)

            # The meter that is equals to the old OT meter should be
            # non active
            old_ot_meter = self.ot_compt_obj.read(
                cursor, uid, old_compt_ot_id, ['name']
            )['name']
            old_contract_meter = self.compt_obj.search(
                cursor, uid, [
                    ('name', '=', old_ot_meter),
                    ('active', '=', False)
                ]
            )
            self.assertTrue(old_contract_meter)

            # Once the OT is activated, there should be a meter like the new
            # one in the OT
            new_ot_meter_name = self.ot_compt_obj.read(
                cursor, uid, new_compt_ot_id, ['name']
            )

            # New Contract meter
            comptador_pol = polissa.comptadors[0]
            for comptador in polissa.comptadors:
                if comptador.name.zfill(9) == new_ot_meter_name['name'].zfill(9):
                    comptador_pol = comptador
                    break
            self.assertEquals(comptador_pol.name.zfill(9), new_ot_meter_name['name'].zfill(9))

            # We must assure that OT the meter has some readings (there are
            # in the demo data).
            self.assertTrue(ot.new_meter_id.lectures)

            # And now the contract will have the same one
            readings_ids = comptador_pol.read(['lectures'])[0]['lectures']

            readings_dict = self.reading_obj.read(
                cursor, uid, readings_ids, ['name']
            )
            reading_names = [
                reading_name['name']
                for reading_name in readings_dict
            ]

            ot_reading_names = [
                ot_reading.name
                for ot_reading in ot.new_meter_id.lectures
            ]
            for reading_name in reading_names:
                self.assertTrue(reading_name in ot_reading_names)

    def test_active_ot_disconect_old_meter_without_new_meter(self):
        """ Testing OT activation if contract has not the same old meter.
        Then the wizard will create a meter like the one in the OT with
        its readings and disconnect it because the close_id says it."""

        uid = self.txn.user
        cursor = self.txn.cursor

        with PatchNewCursors():
            polissa_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            activar_polissa(self.pool, cursor, uid, polissa_id)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)
            ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot', 'ot_0001'
            )[1]
            ot = self.ot_obj.browse(cursor, uid, ot_id)

            # Write close_id to OT. The close:id says that we must disconnect
            # the old_meter on OT activation
            close_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'tancament_ot1'
            )[1]
            self.close_obj.write(cursor, uid, close_id, {'disconnect_old_meter': True})
            ot.write({'close_id': close_id})

            compt_ot_id = self.imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_comptadors', 'comptador_ot_0001'
            )[1]
            ot_meter = self.ot_compt_obj.browse(cursor, uid, compt_ot_id)

            # Deleting all the lectures and renaming all the meters
            # associated with the contract in order to have a clean
            # work space.
            for i, meter in enumerate(polissa.comptadors):
                for lect in meter.lectures or []:
                    lect.unlink(context={})
                for lect in meter.lectures_pot or []:
                    lect.unlink(context={})
                # All meter names must be different
                meter.write({'name': 'XXXXX{0}'.format(i)})

            # Now we can assure that there isn't any meter in the contract
            # like the one in the OT
            self.assertFalse(
                [
                    meter.name
                    for meter in polissa.comptadors
                    if meter.name == ot_meter.name
                ]
            )

            # Create the wizard to active the OT and pass the meter info
            # to the contract
            wiz_id = self.ot_wizard_obj.create(cursor, uid, {})
            context = {
                'active_id': ot_id,
                'active_ids': [ot_id]
            }

            # We assign manually the contract and the meter to the OT
            ot.write({'polissa_id': polissa_id})
            self.ot_wizard_obj.action_active_ot(cursor, uid, [wiz_id], context)
            polissa = self.pol_obj.browse(cursor, uid, polissa_id)

            # Once the OT is activated, there should be a meter like the one
            # in the OT
            comptador_pol = polissa.comptadors[0]
            for comptador in polissa.comptadors:
                if comptador.name.zfill(9) == ot_meter.name.zfill(9):
                    comptador_pol = comptador
                    break
            self.assertEquals(comptador_pol.name.zfill(9), ot_meter.name.zfill(9))

            # We must assure that OT the meter has some readings (there are
            # in the demo data).
            self.assertTrue(ot.old_meter_id.lectures)

            # And now the contract will have the same one
            readings_ids = comptador_pol.read(
                ['lectures'], context={'active_test': False}
            )[0]['lectures']

            readings_dict = self.reading_obj.read(
                cursor, uid, readings_ids, ['name']
            )
            reading_names = [
                reading_name['name']
                for reading_name in readings_dict
            ]
            last_reading = max(reading_names)

            ot_reading_names = [
                ot_reading.name
                for ot_reading in ot.old_meter_id.lectures
            ]
            for reading_name in reading_names:
                self.assertTrue(reading_name in ot_reading_names)

            # Check the meter has been disconnected
            self.assertFalse(comptador_pol.active)
            self.assertEqual(comptador_pol.data_baixa, last_reading)
