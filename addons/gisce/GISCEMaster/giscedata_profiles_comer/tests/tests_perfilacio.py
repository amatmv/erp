# -*- coding: utf-8 -*-
from __future__ import division
import logging
from destral import testing
from destral.transaction import Transaction
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from enerdata.datetime.timezone import TIMEZONE
from enerdata.profiles.profile import Profile

enerdata_log = logging.getLogger("enerdata")
enerdata_log.setLevel(logging.ERROR)


class TestsPerfilacio(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        self.profile_obj = self.openerp.pool.get('giscedata.profiles.profile')
        self.fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        self.polissa_obj = self.openerp.pool.get('giscedata.polissa')
        self.modcon_obj = self.openerp.pool.get(
            'giscedata.polissa.modcontractual'
        )

        imd_obj = self.openerp.pool.get('ir.model.data')
        self.fact_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_facturacio',
            'factura_0001'
        )[1]
        self.fact_object = self.fact_obj.browse(
            self.cursor, self.uid, self.fact_id
        )
        self.fact_object.write({'type': 'in_invoice'})
        self.fact = self.fact_obj.read(self.cursor, self.uid, self.fact_id, [])

    def tearDown(self):
        self.txn.stop()

    def get_dso_invoice(self):
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_facturacio',
            'factura_dso_0001'
        )[1]

        return inv_obj.browse(self.cursor, self.uid, inv_id)

    def get_regularized_invoice(self):
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_profiles_comer',
            'factura_rg_0001'
        )[1]

        return inv_obj.browse(self.cursor, self.uid, inv_id)

    def get_regularized_two_periods_invoice(self):
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_profiles_comer',
            'factura_rg_0002'
        )[1]

        return inv_obj.browse(self.cursor, self.uid, inv_id)

    def get_invoice(self):
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_id = imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'giscedata_facturacio',
            'factura_0001'
        )[1]

        return inv_obj.browse(self.cursor, self.uid, inv_id)

    def crear_modcon(self, pol_id, potencia, ini, fi):
        pol = self.polissa_obj.browse(self.cursor, self.uid, pol_id)
        pol.send_signal(['modcontractual'])
        potencia += + 0.1
        self.polissa_obj.write(
            self.cursor, self.uid, pol_id, {'potencia': potencia}
        )
        wz_crear_mc_obj = self.openerp.pool.get(
            'giscedata.polissa.crear.contracte'
        )

        ctx = {'active_id': pol_id}
        params = {'duracio': 'nou'}
        wz_id_mod = wz_crear_mc_obj.create(self.cursor, self.uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(self.cursor, self.uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            self.cursor, self.uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio, ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def get_profiled_dates(self, fact_id):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lect_obj = self.openerp.pool.get('giscedata.facturacio.lectures.energia')
        lines = factura_obj.read(self.cursor, self.uid, fact_id, ['linies_energia'])['linies_energia']
        lect_dates = []
        for lect in lect_obj.read(self.cursor, self.uid, lines, ['tipus', 'data_actual', 'data_anterior']):
            if lect['tipus'] != 'activa':
                continue
            lect_dates.append(lect['data_actual'])
            lect_dates.append(lect['data_anterior'])

        # first lect + 1 day
        # end lect + 1 day
        di = datetime.strftime((datetime.strptime(min(lect_dates), '%Y-%m-%d') + timedelta(days=1)), '%Y-%m-%d')
        df = datetime.strftime((datetime.strptime(max(lect_dates), '%Y-%m-%d') + timedelta(days=1)), '%Y-%m-%d')

        return di + ' 01:00:00', df + ' 00:00:00'

    @staticmethod
    def get_number_of_hours(date_from, date_to):
        """
        Returns number of hours from two dates
        :param date_from: Start of curve: Y-m-d 01:00:00
        :param date_to: End of curve Y-m-d 00:00:00
        :return: n_hours int
        """

        start = TIMEZONE.localize(datetime.strptime(date_from,
                                                    '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(date_to,
                                                  '%Y-%m-%d %H:00:00'))
        profile = Profile(start, end, [])

        return profile.n_hours

    def test_profile_one_invoice(self):
        """
        Test profile one invoice and check expected energy consumption when
        invoice is open
        """
        # Get & profile invoice
        invoice = self.get_invoice()
        profile_ids = self.profile_obj.search(self.cursor, self.uid, [
            ('cups', '=', invoice.cups_id.name)
        ])
        self.profile_obj.unlink(self.cursor, self.uid, profile_ids)
        invoice.perfilar()

        di, df = self.get_profiled_dates(invoice.id)
        expected_hours = self.get_number_of_hours(di, df)
        # Confirm that profiles are created at opening time
        profile_ids = self.profile_obj.search(self.cursor, self.uid, [
            ('cups', '=', invoice.cups_id.name),
            ('timestamp', '>=', di),
            ('timestamp', '<=', df)
        ])

        msg = 'Profiled hours {} do not match the expected ones {}'.format(
            len(profile_ids), expected_hours
        )
        assert (len(profile_ids) == expected_hours), msg

    def test_no_profile_distri_invoice(self):
        """
        Test to check no profile a distri invoices
        """
        self.fact_object.write({'type': 'out_invoice'})
        expected = self.fact_obj.check_profilable_comer(
            self.cursor, self.uid, self.fact_id
        )

        self.assertEqual(expected, False)

    def test_get_invoice_origin(self):
        """
        Test get invoice origin number from invoice
        """
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        invoice = self.get_dso_invoice()
        inv_origin_expected = inv_obj.get_invoice_vals(
            self.cursor, self.uid, invoice.id
        )['factura']
        origin = invoice.origin
        self.assertEqual(inv_origin_expected, origin)

    def no_profile_on_automatic(self):
        """
        Test no profile on invoice_open when conf var profile_on_invoice_open
        is activated
        :return:
        """
        res_config = self.openerp.pool.get('res.config')

        res_config.set(self.cursor, self.uid, 'profile_on_invoice_open', 0)
        invoice = self.get_dso_invoice()
        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        self.profile_obj.unlink(self.cursor, self.uid, perfils_ids)

        invoice.invoice_open()
        expected = 0
        n_profiled = self.profile_obj.search_count(
            self.cursor, self.uid, [('cups', '=', invoice.cups_id.name)]
        )

        self.assertEqual(n_profiled, expected)

    def test_profiling_consumption_meters_same_date(self):
        """
        Test profile one invoice and check expected energy consumption when
        invoice is open
        """
        # Get & profile invoice
        invoice = self.get_invoice()
        invoice.write({'comptadors': [1, 2]})
        # Refresh invoice
        invoice = self.get_invoice()
        # FakeMeter:
        meter = {
            'name': u'UE0131757580',
            'polissa': invoice.polissa_id,
            'data_alta': '2015-08-01'
        }
        # preventive delete profiles
        profile_ids = self.profile_obj.search(self.cursor, self.uid, [
            ('cups', '=', invoice.cups_id.name),
        ])
        self.profile_obj.unlink(self.cursor, self.uid, profile_ids)

        invoice.perfilar()

        di, df = self.get_profiled_dates(invoice.id)
        expected_hours = self.get_number_of_hours(di, df)
        # Confirm that profiles are created at opening time
        profile_ids = self.profile_obj.search(self.cursor, self.uid, [
            ('cups', '=', invoice.cups_id.name),
            ('timestamp', '>=', di),
            ('timestamp', '<=', df)
        ])

        msg = 'Profiled hours {} do not match the expected ones {}'.format(
            len(profile_ids), expected_hours
        )
        assert (len(profile_ids) == expected_hours), msg

    def test_profile_regularized_invoice(self):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        invoice = self.get_regularized_invoice()

        # Preventive unlink profiled
        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        self.profile_obj.unlink(self.cursor, self.uid, perfils_ids)

        # First profile a normal invoice
        dso_invoice = self.get_dso_invoice()
        factura_obj.write(self.cursor, self.uid, dso_invoice.id, {
            'journal_id': invoice.journal_id.id
        })
        dso_invoice = self.get_dso_invoice()
        dso_invoice.perfilar()

        # Then profile a G with inside period invoice
        invoice.perfilar()
        profile_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final
        )

        # Check profiled hours
        expected_hours = 720
        self.assertEqual(len(profile_ids), expected_hours)

        # Check energy profiled
        expected_energy_period = 25
        profiled_energy = sum(
            [x['lectura'] for x in self.profile_obj.read(
                self.cursor, self.uid, profile_ids, ['lectura']
            )]
        )
        self.assertEqual(profiled_energy, expected_energy_period)

        # Check canceled period
        profile_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, dso_invoice.cups_id.name,
            dso_invoice.data_inici, dso_invoice.data_final
        )
        profiled_energy = sum(
            [x['lectura'] for x in self.profile_obj.read(
                self.cursor, self.uid, profile_ids, ['lectura']
            )]
        )
        initial_consumption = dso_invoice.energia_kwh
        self.assertNotEqual(profiled_energy, initial_consumption)

    def test_profile_regularized_two_periods_invoice(self):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        invoice = self.get_regularized_two_periods_invoice()

        # Preventive unlink profiled
        perfils_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final)
        self.profile_obj.unlink(self.cursor, self.uid, perfils_ids)

        # First profile a normal invoice
        dso_invoice = self.get_dso_invoice()
        factura_obj.write(self.cursor, self.uid, dso_invoice.id, {
            'journal_id': invoice.journal_id.id
        })
        dso_invoice = self.get_dso_invoice()
        dso_invoice.perfilar()

        # Then profile a G with inside period invoice
        invoice.perfilar()
        profile_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, invoice.cups_id.name,
            invoice.data_inici, invoice.data_final
        )

        # Check profiled hours
        expected_hours = 720
        self.assertEqual(len(profile_ids), expected_hours)

        # Check energy profiled
        expected_energy_period = invoice.energia_kwh
        profiled_energy = sum(
            [x['lectura'] for x in self.profile_obj.read(
                self.cursor, self.uid, profile_ids, ['lectura']
            )]
        )
        self.assertEqual(profiled_energy, expected_energy_period)

        # Check canceled period
        profile_ids = self.profile_obj.get_perfils_cups(
            self.cursor, self.uid, dso_invoice.cups_id.name,
            dso_invoice.data_inici, dso_invoice.data_final
        )
        profiled_energy = sum(
            [x['lectura'] for x in self.profile_obj.read(
                self.cursor, self.uid, profile_ids, ['lectura']
            )]
        )
        initial_consumption = dso_invoice.energia_kwh
        self.assertNotEqual(profiled_energy, initial_consumption)