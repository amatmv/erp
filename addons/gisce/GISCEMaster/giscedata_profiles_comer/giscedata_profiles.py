# -*- coding: utf-8 -*-
from mongodb_backend import osv_mongodb

CUPS_SHORT_OPTION = 20


class GiscedataProfilesProfile(osv_mongodb.osv_mongodb):

    _name = 'giscedata.profiles.profile'
    _inherit = 'giscedata.profiles.profile'

    def search_multipurpose_profiles(self, cursor, uid, cups_name, data_inici, data_final, context=None):
        """
        Search a profiles with different CUPS configuration.
        First try original CUPS, and if not getted, try to different options:
        CUPS 20 chars + 0F + 1F + 1P and only CUPS 20 chars
        If get profile_ids with one option return this ids, if not continue checking
        :param cups_name: str cups name with 20 or 22 chars
        :param data_inici: str datetime
        :param data_final: str datetime
        :return: a list of ids if getted, otherwise empty list
        """
        if context is None:
            context = {}
        for option in ('original', '0F', '1F', '1P', CUPS_SHORT_OPTION):
            cups_search = cups_name
            if option == CUPS_SHORT_OPTION:
                cups_search = cups_search[:option]
            elif option != 'original':
                cups_search = cups_search[:CUPS_SHORT_OPTION] + option

            profile_ids = self.search(cursor, uid, [
                ('cups', '=', cups_search),
                ('timestamp', '>=', data_inici),
                ('timestamp', '<=', data_final)
            ], context=context)
            if profile_ids:
                break
        return profile_ids


GiscedataProfilesProfile()
