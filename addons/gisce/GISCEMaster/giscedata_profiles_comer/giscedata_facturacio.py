# -*- coding: utf-8 -*-
from osv import osv, fields

COMPLEMENTARY_TYPE = "C"


class GiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def update_inv_lot(self, cursor, uid, fact_id, context=None):
        return True

    def check_profilable(self, cursor, uid, fact_id, context=None):
        if isinstance(fact_id, (list, tuple)):
            fact_id = fact_id[0]
        return self.check_profilable_comer(cursor, uid, fact_id, context)

    @staticmethod
    def fix_dates(comptador, data_inici, data_final):
        from utils import fix_dates_comer
        return fix_dates_comer(comptador, data_inici, data_final)

    def get_invoice_vals(self, cursor, uid, inv_id, context=None):
        """
        Get origin number from invoice
        :param inv_id: factura_id
        :return: dict LIKE {'factura': 'F0000'}
        """
        if context is None:
            context = {}
        invoice = self.read(cursor, uid, inv_id, ['origin'], context)

        return {'factura': invoice['origin']}

    def get_automatic_profile(self, cursor, uid, context=None):
        """
        Get from res_config
        :return: profile_on_invoice_open value or False if not exist
        """
        res_config = self.pool.get('res.config')
        automatic_profile = int(res_config.get(
            cursor, uid, 'profile_on_invoice_open', '1'
        ))

        return automatic_profile

    def check_profilable_comer(self, cursor, uid, inv_id, context=None):
        """
        Check if a invoice is profilable in comer module
        :param inv_id: factura_id
        :return: bool
        """
        account_invoice_obj = self.pool.get('account.invoice')
        pol_obj = self.pool.get('giscedata.polissa')
        read_params = ['invoice_id', 'type', 'polissa_id', 'data_inici', 'tipo_rectificadora']

        # Check Profilable
        invoice = self.read(cursor, uid, inv_id, read_params)
        account_invoice = account_invoice_obj.browse(
            cursor, uid, invoice['invoice_id'][0]
        )
        invoice_type = account_invoice.journal_id.code

        # - Only profile energy invoices
        if not invoice_type.startswith("CENERGIA"):
            return False

        # - Only profile a in_invoices or in_refunds
        if invoice['type'] in ['out_invoice', 'out_refund']:
            return False

        # - Can't profile complementary invoices
        if invoice['tipo_rectificadora'] == COMPLEMENTARY_TYPE:
            return False

        # - Profile invoices with dates
        if 'data_inici' not in invoice.keys() or not invoice['data_inici']:
            return False

        pol_data = pol_obj.read(
            cursor, uid, invoice['polissa_id'][0],
            ['agree_tipus'], context={'date': invoice['data_inici']}
        )
        agree_type = pol_data['agree_tipus']

        # - Profile REE types (03, 04, 05)
        if agree_type < '03':
            return False

        return True

    def invoice_open(self, cursor, uid, ids, context=None):
        """
        Workflow: check_automatic_profile -> check_profilable -> profile
        """

        invoice_is_open = super(GiscedataFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context)

        if self.get_automatic_profile(cursor, uid, context):
            for inv_id in ids:
                if self.check_profilable_comer(cursor, uid, inv_id, context):
                    if invoice_is_open:
                        self.encua_perfilacio(cursor, uid, [inv_id], context)

        return invoice_is_open


GiscedataFacturacioFactura()
