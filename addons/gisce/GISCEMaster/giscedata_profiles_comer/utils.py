# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from enerdata.datetime.timezone import TIMEZONE as enerdata_timezone
import calendar


def fix_dates_comer(comptador, start, end):
    """Try to fix common dates problems with measures and metters.
    CNMC new criteria
    """

    end = datetime.strptime(end[0:10], '%Y-%m-%d')
    start = datetime.strptime(start[0:10], '%Y-%m-%d')

    modcons = comptador.polissa.get_modcontractual_intervals(
        (start - timedelta(days=1)).strftime('%Y-%m-%d'),
        end.strftime('%Y-%m-%d'), {'ffields': ['tarifa']}
    )
    changes = modcons.get(
        start.strftime('%Y-%m-%d'), {}
    ).get('changes', [])

    data_alta = datetime.strptime(comptador.data_alta[0:10], '%Y-%m-%d')
    # End is the end date + 1 day at our 00:00
    end += timedelta(days=1)
    end = end.strftime('%Y-%m-%d 00:00:00')

    if start != data_alta and 'tarifa' not in changes:
        # Start is and old measure date, we have to sum one day
        # and start at hour 01:00
        start += timedelta(days=1)

    start = start.strftime('%Y-%m-%d 01:00:00')
    return start, end

def fix_dates_two_cnmc_criterias_comer(comptador, start, end):
    """Try to fix common dates problems with measures and metters.
    CNMC new criteria and CNMC old criteria
    """

    datetime_format = '%Y-%m-%d'
    end = datetime.strptime(end[0:10], datetime_format)
    start = datetime.strptime(start[0:10], datetime_format)

    modcons = comptador.polissa.get_modcontractual_intervals(
        (start - timedelta(days=1)).strftime(datetime_format),
        end.strftime(datetime_format), {'ffields': ['tarifa']}
    )
    changes = modcons.get(
        start.strftime(datetime_format), {}
    ).get('changes', [])

    alta_polissa = datetime.strptime(comptador.polissa.data_alta,
                                     datetime_format)
    alta_comptador = datetime.strptime(comptador.data_alta[0:10],
                                       datetime_format)

    # Alta Contracte: Criteri CNMC (lectura dia anterior)
    if start + timedelta(days=1) == alta_polissa:
        # Start is and old measure date, we have to sum one day
        # and start at hour 01:00
        start += timedelta(days=1)
    # Alta Contracte: Criteri Antic (lectura amb alta de contracte)
    elif start == alta_polissa:
        start = start
    # Canvis contador: Criteri CNMC correcte
    elif start + timedelta(days=1) == alta_comptador:
        start += timedelta(days=1)
    # Canvis contador: Criteri antic
    elif start == alta_comptador:
        # Comprovem que no estem en una data alta de contador erronea.
        # Si la alta del contador nou es el mateix dia que la baixa del
        # contador antic, ho esta fent malament
        tenia_error = False
        for meter in comptador.polissa.comptadors:
            if meter.data_baixa == alta_comptador:
                # Criteri CNMC MAL FET, "camuflat" com a criteri antic
                start += timedelta(days=1)
                tenia_error = True
                break
        # Criteri Antic BEN FET
        if not tenia_error:
            start = start
    else:
        start += timedelta(days=1)

    start = start.strftime('%Y-%m-%d 01:00:00')
    # End is the end date + 1 day at our 00:00
    end += timedelta(days=1)
    end = end.strftime('%Y-%m-%d 00:00:00')

    return start, end
