from erppeek import Client

class Profiler:
  """
  First use: stablish a connection params
  How to use: p = Profiler(inv_ids)
  where inv_ids is an a list of integer invoice ids
  p.profile()
  """

  def __init__(self, invoices):
    self.invoices = invoices
    conn = ''
    db = ''
    user = ''
    self.c = Client(conn, db, user)
    self.inv_obj = self.c.model('giscedata.facturacio.factura')
  
  def profile(self):
    n_inv = 0
    total = len(self.invoices)
    for inv_id in self.invoices:
      n_inv += 1
      try:
        self.inv_obj.perfilar([inv_id])
        print('# {} to {} - Profiled inv: {}'.format(n_inv, total, inv_id))
      except Exception as err:
        print('# {} to {} - NOT profiled inv: {} - Error {}!!!!'.format(
          n_inv, total, inv_id, err
        ))
