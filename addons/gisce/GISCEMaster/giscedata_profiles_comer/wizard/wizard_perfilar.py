# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _


class WizardPerfilar(osv.osv_memory):
    _name = 'wizard.perfilar'
    _inherit = 'wizard.perfilar'

    def _default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        invoice_ids = context.get('active_ids', [])
        invoices = invoice_obj.read(
            cursor,
            uid,
            invoice_ids,
            ['origin'],
            context=context
        )
        info = _("We will profile the curves for the invoices: \n{0}").format(
            ', '.join([x['origin'] for x in invoices])
        )

        return info

    _defaults = {
        'state': lambda *x: 'init',
        'info': _default_info,
        'skip_check': lambda *a: False,
    }


WizardPerfilar()
