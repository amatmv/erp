# -*- coding: utf-8 -*-
{
  "name": "Perfilació comercialitzadora",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Transformar lectures en perfils a Comercialitzadora.
  * Sistema de tractament Objeccions
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "GISCEMaster",
  "depends": [
      "giscedata_profiles",
      "giscedata_facturacio_comer",
  ],
  "init_xml": [],
  "demo_xml": ["giscedata_profiles_demo.xml"],
  "update_xml": [
      "giscedata_facturacio_view.xml",
      "security/ir.model.access.csv",
  ],
  "active": False,
  "installable": True
}
