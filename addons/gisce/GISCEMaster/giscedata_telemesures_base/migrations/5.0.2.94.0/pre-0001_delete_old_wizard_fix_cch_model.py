# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Deleting old wizard model fix_cch_fact_again')
    cursor.execute("delete from ir_model_data where name like '%%fix_cch_fact_again%%';")


def down(cursor, installed_version):
    pass

migrate = up
