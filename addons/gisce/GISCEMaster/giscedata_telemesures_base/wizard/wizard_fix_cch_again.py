# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from giscedata_telemesures_base.giscedata_facturacio import CCH_FIX_TECHNOLOGY


class WizardFixCCHAgain(osv.osv_memory):
    """Wizard per fer el fix CCH de les factures que no el tenen
    disponible"""

    _name = 'wizard.fix.cch.again'

    def _default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        obj_factura = self.pool.get('giscedata.facturacio.factura')
        factures_ids = context.get('active_ids', [])
        factures = obj_factura.read(
            cursor,
            uid,
            factures_ids,
            ['number'],
            context=context
        )
        info = _("We will adjust the curves for the invoices: {0}").format(
            ', '.join([x['number'] for x in factures])
        )

        return info

    def check_meter_tg(self, cursor, uid, invoice_id, context=None):
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        for meter in invoice.comptadors:
            if meter.tg:
                return True
        return False

    def check_meter_tm(self, cursor, uid, invoice_id, context=None):
        invoice_obj = self.pool.get('giscedata.facturacio.factura')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        invoice = invoice_obj.browse(cursor, uid, invoice_id)
        for meter in invoice.comptadors:
            tech = meter_obj.read(cursor, uid, meter.id, ['technology_type'],
                                  context=context)['technology_type']
            tariff_name = invoice.tarifa_acces_id.name
            if tech in ['telemeasure', 'electronic'] and not \
                    tariff_name.startswith('6.'):
                return True
        return False

    def fix_cch_again(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        obj_factura = self.pool.get('giscedata.facturacio.factura')
        factures_ids = context.get('active_ids', [])
        force = wizard.force
        done = {}
        for factura in obj_factura.read(cursor, uid, factures_ids,
                                        ['number', 'polissa_tg',
                                         'cch_fact_available',
                                         'lectures_energia_ids', 'polissa_id'],
                                        context=context):
            done[factura['number']] = False
            if factura['cch_fact_available'] and not force:
                continue
            meter_tg = False
            meter_tm = False
            if 'telemeasure' in dict(CCH_FIX_TECHNOLOGY).keys():
                meter_tm = self.check_meter_tm(cursor, uid, factura['id'],
                                               context=context)
            if 'prime' in dict(CCH_FIX_TECHNOLOGY).keys():
                meter_tg = self.check_meter_tg(cursor, uid, factura['id'],
                                               context=context)
            if meter_tm:
                obj_factura.enqueue_fix_cch_tm(cursor, uid, [factura['id']],
                                               context=context)
                done[factura['number']] = True
            if meter_tg:
                obj_factura.fix_cch_fact(cursor, uid, [factura['id']],
                                         context=context)
                done[factura['number']] = True

        right = [k for k, v in done.items() if v]
        wrong = [k for k, v in done.items() if not v]
        message = ''
        if right:
            message += _("The adjustment has been done again for the invoices: "
                         "{}\n\n").format(right)
        if wrong:
            message += _("Is not possible to do the adjustment fot these "
                         "invoices: {}").format(wrong)

        res = {
            'state': 'end',
            'info': message
        }
        wizard.write(res)

    _columns = {
        'state': fields.selection(
            [
                ('init', 'Start'),
                ('end', 'End')
            ],
            'State'
        ),
        'info': fields.text('Info'),
        'force': fields.boolean('Force', help="FIX CCH FACT to invoices which "
                                "have it available already"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'force': lambda *a: False,
        'info': _default_info,
    }

WizardFixCCHAgain()
