# -*- coding: utf-8 -*-
{
    "name": "Telemasures base",
    "description": """Base module for telemeasures (Tg and TM)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "infraestructura",
        "mongodb_backend",
        "giscedata_polissa_crm",
        "giscedata_lectures_tecnologia"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_telegestio_demo.xml",
    ],
    "update_xml":[
        "security/giscedata_telemesures_base_security.xml",
        "crm_data.xml",
        "giscedata_telemesures_base_view.xml",
        "giscedata_lectures_view.xml",
        "wizard/wizard_fix_cch_again_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
