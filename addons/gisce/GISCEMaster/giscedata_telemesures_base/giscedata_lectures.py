# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataLecturesComptadorTelemesures(osv.osv):
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    _columns = {
        'registrador_id': fields.many2one('giscedata.registrador', 'Registrador')
    }


GiscedataLecturesComptadorTelemesures()
