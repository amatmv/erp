from pytz import timezone
from datetime import datetime

TIMEZONE = timezone('Europe/Madrid')


def hour_change(date_list):
    res_dates = []
    last_date = []
    dst = None
    for date_nol in date_list:
        if not last_date:
            dst = bool(TIMEZONE.localize(date_nol).dst())
        if date_nol in last_date and date_nol.minute == 0:
            dst = not dst
        date_loc = TIMEZONE.localize(date_nol, is_dst=dst)
        last_date.append(date_nol)
        res_dates.append(date_loc)
    return res_dates


def prepare_hour_change_profiles(profiles):
    last_date = []
    dst = None
    for p in profiles:
        date_nol = datetime.strptime(p['timestamp'], '%Y-%m-%d %H:%M:%S')
        if not last_date:
            dst = bool(TIMEZONE.localize(date_nol).dst())
        if date_nol in last_date and date_nol.minute == 0:
            dst = not dst
        date_loc = TIMEZONE.localize(date_nol, is_dst=dst)
        last_date.append(date_nol)
        p['season'] = 'S' if date_loc.dst() else 'W'
    return profiles
