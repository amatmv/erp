# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import cache

TECHNOLOGY_TYPE_METER_SEL = [('unknown', 'Desconocido')]


class GiscedataRegistrador(osv.osv):

    _name = 'giscedata.registrador'

    _columns = {
        'name': fields.char('name', required=True, size=100),
        'description': fields.char('description', size=100),
#        'ct_id': fields.many2one('giscedata.cts', 'CT'),
#        'trafo_id': fields.many2one(
#            'giscedata.transformador.trafo', 'Transformador'
#        ),
        'type': fields.selection(
            [('billing', 'Billing'),
             ('supervisor', 'Supervisor'),
             ('repeater', 'Repetidor'),
             ('disabled', 'Disabled contract'),
             ('foreign', 'Foreign meter'),
             ('unknown', 'Unknown'),
             ], 'Tipo', required=True,
            help=u"Meter type of use:\n"
                 u" * Billing: Meter with active contract\n"
                 u" * Supervisor: Supervisor meter\n"
                 u" * Repeater: Repeater meter\n"
                 u" * Disabled contract: Installed meter with no contract\n"
                 u" * Foreign meter: Meter of a foreign net\n"
                 u" * Unknown: Not categorized\n"
        ),
        'technology': fields.selection(
            TECHNOLOGY_TYPE_METER_SEL, 'Tecnologia', required=True
        ),
        'notes': fields.text('Notes'),
        'timestamp': fields.date('Last status update'),
        'managed': fields.boolean('Active'),
        'status': fields.selection([(0, 'Permanent Failure'),
                                    (1, 'Temporal Failure'),
                                    (2, 'Registered')], 'Connection status',
                                   size=1, readonly=True),
    }

    _defaults = {
        'type': lambda *a: 'unknown',
        'technology': lambda *a: 'unknown'
    }


GiscedataRegistrador()
