# -*- coding: utf-8 -*-
import time
from tools.translate import _
from dateutil.relativedelta import relativedelta
from osv import osv, fields
from osv.expression import OOQuery
from oorq.decorators import job
from datetime import datetime
import StringIO
import base64


class WizardExportCurve(osv.osv_memory):
    _name = 'wizard.export.curve'

    def _default_r1(self, cursor, uid, context=None):
        ''' Gets REE code from company if defined '''
        if not context:
            context = {}

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref

        return str(r1)

    def get_f1_meters(self, cursor, uid, di, df, context=None):
        """
        Get the ids of telemesura meters type 1 and 2 (REE). Implemented on telemesura_distri
        """
        raise osv.except_osv("NotImplemented", _("Falta el modulo de telemedida"))

    def get_p1_meters(self, cursor, uid, di, df, context=None):
        """
        Get the ids of meters type 1, 2, 3 and 4 (REE)
        """
        if context is None:
            context = {}

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = []

        where_params = [
            ('potencia', '>', 15),
            ('data_inici', '<=', df),
            ('data_final', '>=', di)
        ]

        selected_comers = context.get('selected_comers', [])
        if selected_comers:
            where_params.append(('comercialitzadora.ref', 'in', selected_comers))

        q = OOQuery(modcon_obj, cursor, uid)
        sql = q.select(
            ['polissa_id', 'data_inici', 'data_final'],
            only_active=False
        ).where(where_params)
        cursor.execute(*sql)
        modcons = cursor.dictfetchall()
        for modcon in modcons:
            qc = OOQuery(meter_obj, cursor, uid)
            sql = qc.select(['id'], only_active=False).where(
                [('polissa', '=', modcon['polissa_id']),
                 ('data_alta', '<=', modcon['data_final']),
                 '|',
                 ('data_baixa', '>=', modcon['data_inici']),
                 ('data_baixa', '=', None)])
            cursor.execute(*sql)
            try:
                meter_ids.append(cursor.fetchone()[0])
            except:
                raise osv.except_osv(_(u"ERROR"), _(
                    u"No hi han comptadors"
                ))
        return meter_ids

    def create_crm_case_and_attatchment(self, cursor, uid, fecha_inicio_exp, info, file_name=None, file_b64=None, context=None):
        crm_obj = self.pool.get('crm.case')
        imd_obj = self.pool.get('ir.model.data')

        section_tm_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_telemesures_base', 'crm_case_section_telemesures'
        )[1]

        res = 'Correcto\n' if file_b64 else 'Error'
        crm_case_vals = {
            'name': u'Exportación fichero F1/P1 REE {} {}'.format(fecha_inicio_exp, res),
            'section_id': section_tm_id,
            'user_id': uid,
            'description': '{}: \n\n{}'.format(res, info),
        }

        case_id = crm_obj.create(cursor, uid, crm_case_vals, context=context)
        att_id = False
        if file_b64:
            att_id = self.create_attachment(
                cursor, uid, file_b64, file_name, res_id=case_id, res_model='wizard.export.curve', context=context
            )
        return att_id

    def create_attachment(self, cursor, uid, data, filename, res_id=None, res_model=None, context=None):
        if context is None:
            context = {}
        ir_attachment_obj = self.pool.get('ir.attachment')
        attachment_values = {
            'name': filename,
            'res_id': res_id,
            'res_model': res_model,
            'datas': data
        }
        att_id = ir_attachment_obj.create(
            cursor,
            uid,
            attachment_values,
            context=context
        )
        return att_id

    def export_curve_file(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        tms_files = self.pool.get('telemesures.files')
        from_meter = context.get('from_meter', False)
        if from_meter:
            meter_ids = context.get('active_ids', False)
        else:
            selected_comers = [comer.ref for comer in wizard.comers]
            context.update({'selected_comers': selected_comers})
            if wizard.type == 'f1':
                if not wizard.all_types:
                    # Select tm meters type 1 and 2 (>450 kWh)
                    meter_ids = self.get_f1_meters(
                        cursor, uid, wizard.date_from, wizard.date_to, context=context
                    )
                else:
                    # Select tm meters type 1, 2, 3 and 4 (>15 kWh)
                    meter_ids = self.get_p1_meters(
                        cursor, uid, wizard.date_from, wizard.date_to, context=context
                    )
            else:
                # Select tm meters type 1, 2, 3 and 4 (>15 kWh)
                meter_ids = self.get_p1_meters(
                    cursor, uid, wizard.date_from, wizard.date_to, context=context
                )

        date_from = (datetime.strptime(wizard.date_from, '%Y-%m-%d'
                                       ) + relativedelta(hours=1)
                     ).strftime('%Y-%m-%d %H:%M:%S')
        date_to = (datetime.strptime(wizard.date_to, '%Y-%m-%d'
                                     ) + relativedelta(days=1)
                   ).strftime('%Y-%m-%d %H:%M:%S')

        if wizard.type == 'f1':
            prefix = 'F1_'
        else:
            prefix = 'P1_'
        file_data = {
            'date_from': date_from,
            'date_to': date_to,
            'file_type': wizard.type,
            'all_profiles': wizard.all_profiles,
            'r1_code': wizard.r1,
            'version': wizard.version,
            'origin': 'meter',
        }

        wizard.write({'state': 'on'})

        if not meter_ids:
            wizard.write({'info': _('No files generated. We didn\'t '
                                    'find the meters.'),
                          'state': 'end'})
        else:

            fecha_inicio_exp = str(datetime.strptime(wizard.date_from, '%Y-%m-%d').strftime('%Y-%m-%d')) \
                               + '/' \
                               + str(datetime.strptime(wizard.date_to, '%Y-%m-%d').strftime('%Y-%m-%d'))

            f_name = '{prefix}{code}_{measures_date}_{generation_date}.zip'.format(
                prefix=prefix, code=str(wizard.r1), measures_date=date_from.replace('-', '')[0:8],
                generation_date=str(time.strftime("%Y%m%d"))
            )

            if wizard.backgroun_export:
                wizard.write(
                    {'info': _('Se esta realizando la exportación en '
                               'segundo plano, cuando el fichero '
                               'este exportado se abrira un caso CRM con el '
                               'resultado como adjunto'
                               ),
                     'state': 'end'
                     }
                )
                ctx = context.copy()
                if wizard.upload:
                    provider_obj = self.pool.get("giscedata.ftp.provider")
                    pdata = self.get_partners_by_meters(
                        cursor, uid, wizard.date_from, wizard.date_to, meter_ids, context=context)
                    for partner_id, meter_ids in pdata.items():
                        ctx["upload_to_server"] = provider_obj.get_provider(cursor, uid, wizard.type, partner_id=partner_id, context=context)
                        self.export_ree_f1_curve_file_background(
                            cursor, uid, meter_ids, file_data,
                            f_name, fecha_inicio_exp, context=ctx
                        )
                else:
                    self.export_ree_f1_curve_file_background(
                        cursor, uid, meter_ids, file_data,
                        f_name, fecha_inicio_exp, context=ctx
                    )
            else:
                if wizard.upload:
                    provider_obj = self.pool.get("giscedata.ftp.provider")
                    final_info = ""
                    pdata = self.get_partners_by_meters(
                        cursor, uid, wizard.date_from, wizard.date_to, meter_ids, context=context)
                    for partner_id, meter_ids in pdata.items():
                        result = tms_files.create_file_ree(
                            cursor, uid, meter_ids, file_data, context=context
                        )
                        provider = provider_obj.get_provider_browse(cursor, uid, wizard.type, partner_id=partner_id, context=context)
                        if result.get('data', False):
                            data = StringIO.StringIO(result['data'].getvalue())
                            path = provider[wizard.type+"_upload"]
                            provider.upload(data, f_name, path)
                            final_info += _(u"* Fitxers pujats a {0}.\n").format(provider.name)
                        else:
                            final_info += _(u"* No s'ha pujat cap fitxer a {0}.\n").format(provider.name)

                    wizard.write({'state': 'end',
                                  'info': final_info})
                else:
                    result = tms_files.create_file_ree(
                        cursor, uid, meter_ids, file_data, context=context
                    )
                    if result.get('data', False):
                        data = base64.b64encode(result['data'].getvalue())

                        wizard.write({'fileF1': data,
                                      'filenameF1': f_name,
                                      'state': 'end',
                                      'info': result.get('info', '')})
                    else:
                        wizard.write({'info': _('\nNo file generated. {}').format(
                            result.get('info', '')),
                            'state': 'end'})

    def get_partners_by_meters(self, cursor, uid, di, df, meter_ids, context=None):
        """
        Get partner id and meter_ids. Check modcontractual_intervals by period
        :param di: str date
        :param df: str date
        :param meter_ids: list of meter_ids int
        :return: dict like {parnter_id: [meter_ids]
        """
        if not context:
            context = {}

        pol_obj = self.pool.get('giscedata.polissa')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        partner_data = {}
        mc_fields = ['comercialitzadora']
        for meter_id in meter_ids:
            pol_id = meter_obj.read(cursor, uid, meter_id, ['name', 'polissa'])['polissa'][0]
            mc = pol_obj.get_modcontractual_intervals(cursor, uid, pol_id, di, df, context={'ffields': mc_fields})
            for mc_data in mc.values():
                c_ = modcon_obj.read(cursor, uid, mc_data['id'], ['comercialitzadora'])
                c_id = c_['comercialitzadora'][0]
                if c_id in partner_data:
                    partner_data[c_id].append(meter_id)
                else:
                    partner_data[c_id] = [meter_id]
        return partner_data

    @job(queue='low', timeout=3600*12)
    def export_ree_f1_curve_file_background(self, cursor, uid, meter_ids, file_data, f_name, fecha_inicio_exp,
                                            context=None):
        tms_files = self.pool.get('telemesures.files')
        result = tms_files.create_file_ree(
            cursor, uid, meter_ids, file_data, context=context
        )
        if result.get('data', False):
            data = base64.b64encode(result['data'].getvalue())

            att_id = self.create_crm_case_and_attatchment(
                cursor, uid, fecha_inicio_exp, result.get('info', ''),
                file_name=f_name, file_b64=data, context=context
            )
        else:
            att_id = self.create_crm_case_and_attatchment(
                cursor, uid, fecha_inicio_exp, result.get('info', ''),
                file_name=None, file_b64=None, context=context
            )
        if att_id and context.get("upload_to_server"):
            attachment = self.pool.get("ir.attachment").browse(cursor, uid, att_id, context=None)
            data = StringIO.StringIO(base64.b64decode(attachment.datas))
            provider_obj = self.pool.get("giscedata.ftp.provider")
            provider = provider_obj.browse(cursor, uid, context.get("upload_to_server"))
            path = provider[file_data['file_type'] + "_upload"]
            provider.upload(data, f_name, path)

    _columns = {
        'date_from': fields.date(
            'Date from', required=True,
            help='Full month, both days included. Example: 1 to 31'
        ),
        'date_to': fields.date(
            'Date to', required=True,
            help='Full month, both days included. Example: 1 to 31'
        ),
        'filenameF1': fields.char('Name', size=256),
        'fileF1': fields.binary(_('Result file')),
        'version': fields.integer(_('Version:')),
        'r1': fields.char(_('REE code'), size=12,
                          help=_('Automatically takes the ref2 field from the '
                                 'company as value.')),
        'state': fields.selection([('init', 'Init'), ('end', 'End'), ('on', 'On')], 'State'),
        'info': fields.text('Information', readonly=True),
        'type': fields.selection([('f1', 'F1'), ('p1', 'P1')], 'File type'),
        'all_profiles': fields.boolean(
            'All profiles', help='If this is enabled exporting a P1 file all '
                                 'profiles will be exported. Valid and not '
                                 'valid profiles.'
        ),
        'backgroun_export': fields.boolean('Exportar en background'),
        'comers': fields.many2many(
            'res.partner',
            'res_partner_f1p1_rel',
            'comer',
            'partner_id',
            'Comercialitzadores',
            domain=[('ref', '!=', ''), ('supplier', '=', True)]
        ),
        'upload': fields.boolean(
            'Upload files to SFTP server',
            help='Upload the generated files to SFTP server'
        ),
        'all_types': fields.boolean('All types', help='Export 1, 2, 3, 4 types (Not only 1, 2 REE types)')
    }

    _defaults = {
        'version': 1,
        'r1': _default_r1,
        'state': lambda *a: 'init',
        'info': lambda *a: _('Wizard to export F1 and P1 REE file from the '
                             'meter profiles.\n\n - F1 files contain the '
                             'invoiced and validated profiles.\n - P1 files '
                             'contain the validated but not yet invoiced '
                             'profiles.\n'),
        'all_profiles': lambda *a: False,
        'backgroun_export': lambda *a: False,
        'all_types': lambda *a: False,
    }


WizardExportCurve()
