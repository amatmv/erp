# -*- coding: utf-8 -*-
from enerdata.datetime.timezone import TIMEZONE
from datetime import datetime

WIZARD_STATES = [
    ('standby', 'Stand by'),
    ('request', 'Request data'),
    ('end', 'Finished request')
]


def get_season(dt):
    if dt.dst().seconds > 0:
        return 'S'
    else:
        return 'W'


def get_hours_amount(date_from, date_to):
    date_from_tz = datetime.strptime(date_from, '%Y-%m-%d %H:%M:%S')
    date_to_tz = datetime.strptime(date_to, '%Y-%m-%d %H:%M:%S')
    delta = date_to_tz.date() - date_from_tz.date()
    hours = delta.days * 24
    season_ini = get_season(TIMEZONE.localize(date_from_tz))
    season_end = get_season(TIMEZONE.localize(date_to_tz))
    if season_ini != season_end:
        if season_ini == 'W':
            hours -= 1
        else:
            hours += 1
    return hours

