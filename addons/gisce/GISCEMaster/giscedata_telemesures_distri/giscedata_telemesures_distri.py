from osv import osv, fields
from pytz import timezone
from tools.translate import _
import logging
from .utils import *
import csv
import cStringIO as StringIO
import time
from dateutil.relativedelta import relativedelta
from enerdata.profiles import Dragger
from datetime import datetime, time as dtime
import zipfile

TIMEZONE = timezone('Europe/Madrid')

logger = logging.getLogger('openerp' + __name__)

TECHNOLOGY_TYPE = {
    'tg': ('smmweb', 'prime'),
    'tm': ('electronic', 'telemeasure')
}


class TelemesuresFiles(osv.osv):

    _name = 'telemesures.files'

    def get_consumption(self, cursor, uid, meter_id, di, df, context=None):
        if not context:
            context = None

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        if isinstance(meter_id, list or tuple):
            meter_id = meter_id[0]

        meter_data = meter_obj.read(
            cursor, uid, meter_id, ['technology_type', 'name', 'meter_tg_name'], context=context
        )

        search_params = [
            ('timestamp', '>=', di),
            ('timestamp', '<=', df),
        ]
        conversor = 1
        direction = 'ai'
        profile_obj = self.pool.get('tm.profile')
        if meter_data['technology_type'] in TECHNOLOGY_TYPE['tg']:
            profile_obj = self.pool.get('tg.profile')
            conversor = 1000
            search_params.append(('name', '=', meter_data['meter_tg_name']))
        else:
            search_params += [('name', '=', meter_data['name']), ('type', '=', 'p')]
        profiles_ids = profile_obj.search(cursor, uid, search_params, order='timestamp asc')
        consumption = False
        if not profiles_ids:
            return consumption
        dragger = Dragger()
        for profile in profile_obj.read(cursor, uid, profiles_ids, [direction]):
            if conversor > 1:
                consumption += dragger.drag(profile[direction] / conversor)
            else:
                consumption += dragger.drag(profile[direction] / conversor)
        return consumption

    @staticmethod
    def get_totals_on_period(cursor, uid, profile_obj, profiles_ids,
                             context=None):
        totals = {
            'ai': 0,
            'ae': 0,
            'r1': 0,
            'r2': 0,
            'r3': 0,
            'r4': 0,
            'ai_fact': 0,
            'ae_fact': 0,
            'r1_fact': 0,
            'r2_fact': 0,
            'r3_fact': 0,
            'r4_fact': 0,
            'total_hours': 0,
            'meters': []
        }
        for profile_id in profiles_ids:
            profile = profile_obj.read(cursor, uid, profile_id, ['ai', 'ae',
                                       'r1', 'r2', 'r3', 'r4', 'ai_fact',
                                       'ae_fact', 'name', 'r1_fact', 'r2_fact',
                                       'r3_fact', 'r4_fact'])
            if profile['name'] not in totals['meters']:
                totals['meters'].append(profile['name'])
            totals['total_hours'] += 1
            totals['ai'] += profile.get('ai', 0)
            totals['ae'] += profile.get('ae', 0)
            totals['r1'] += profile.get('r1', 0)
            totals['r2'] += profile.get('r2', 0)
            totals['r3'] += profile.get('r3', 0)
            totals['r4'] += profile.get('r4', 0)
            totals['ai_fact'] += profile.get('ai_fact', 0)
            totals['ae_fact'] += profile.get('ae_fact', 0)
            totals['r1_fact'] += profile.get('r1_fact', 0)
            totals['r2_fact'] += profile.get('r2_fact', 0)
            totals['r3_fact'] += profile.get('r3_fact', 0)
            totals['r4_fact'] += profile.get('r4_fact', 0)
        return totals

    def create_file_ree_from_invoices(self, cursor, uid, invoices_ids,
                                      file_data, context=None):
        """
        :param invoices_ids: IDs of the invoices
        :param file_data: Dictionaty:
        {
            'date_from': string format '%Y-%m-%d %H:%M:%S',
            'date_to': string format '%Y-%m-%d %H:%M:%S',
            'file_type': 'f1',
            'all_profiles': False,
            'r1_code': R1 code of marketer,
            'version': Version number of the file,
            'origin': 'invoice',
        }
        :return: Dictionary:
        {
            'data': zip file(if correctly generated),
            'info': msg(if error while generating),
            ...
        }
        """
        inv_obj = self.pool.get('giscedata.facturacio.factura')
        daily_files = {}
        info = ''
        for invoice_id in invoices_ids:
            invoice = inv_obj.read(cursor, uid, invoice_id, ['data_inici',
                                                             'data_final',
                                                             'comptadors'])
            date_from = datetime.strptime(invoice['data_inici'], '%Y-%m-%d')
            date_from = date_from + relativedelta(hours=1)
            date_from = date_from.strftime('%Y-%m-%d %H:%M:%S')
            date_to = datetime.strptime(invoice['data_final'], '%Y-%m-%d')
            date_to = date_to + relativedelta(days=1)
            date_to = date_to.strftime('%Y-%m-%d %H:%M:%S')
            file_data['date_from'] = date_from
            file_data['date_to'] = date_to
            meters_ids = invoice['comptadors']
            result = self.create_file_ree(cursor, uid, meters_ids, file_data,
                                          daily_files=daily_files, context=context)
            info += result['info']
            daily_files = result['data']
        # Create a ZIP for every invoice?
        result_io = StringIO.StringIO()
        zip_result = zipfile.ZipFile(result_io, 'w',
                                     compression=zipfile.ZIP_DEFLATED)
        for filename, content in daily_files.items():
            zip_result.writestr(filename, content.getvalue())
            content.close()
        zip_result.close()
        return {
            'data': result_io,
            'info': info,
        }

    def create_file_ree(self, cursor, uid, meters_ids, file_data, daily_files=None, context=None):
        """
        :param meters_ids: IDs of the meters
        :param file_data: Dictionary:
        {
            'date_from': string format '%Y-%m-%d %H:%M:%S',
            'date_to': string format '%Y-%m-%d %H:%M:%S',
            'file_type': 'f1' or 'p1',
            'all_profiles': Boolean ,
            'r1_code': R1 code of marketer,
            'version': Version number of the file,
            'origin': 'meter',
        }
        :return: Dictionary:
        {
            'data': zip file(if correctly generated),
            'info': msg(if error while generating),
            ...
        }
        """
        tm_profile_obj = self.pool.get('tm.profile')
        tg_profile_obj = self.pool.get('tg.profile')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        contract_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        logger = logging.getLogger('openerp.{0}.export_f1_p1'.format(__name__))
        result = {
            'info': '',
            'data': False
        }
        try:
            date_from = file_data['date_from']
            date_to = file_data['date_to']
            file_type = file_data['file_type']
            all_profiles = file_data['all_profiles']
            r1_code = file_data['r1_code']
            version = file_data['version']
        except Exception as e:
            info = _('The data introduced to generate the file was not correct.'
                     '\n{}'.format(e.message))
            return info
        if not isinstance(meters_ids, (tuple, list)):
            meters_ids = [meters_ids]

        zf_io = StringIO.StringIO()
        zip_writer = zipfile.ZipFile(zf_io, 'w',
                                     compression=zipfile.ZIP_DEFLATED)
        meters = meter_obj.read(cursor, uid, meters_ids, ['name', 'polissa',
                                                          'technology_type',
                                                          'data_alta',
                                                          'data_baixa', 'tg',
                                                          'meter_tg_name'])
        meters = sorted(meters, key=lambda k: (k['name'], k['data_alta']))
        if daily_files is None:
            daily_files = {}
        to_check = {}
        logger.info('Exporting #{} Meters'.format(len(meters)))
        for n_meter, meter in enumerate(meters, 1):
            logger.info('Exporting {}/{}'.format(n_meter, len(meters)))
            contract = contract_obj.read(cursor, uid, meter['polissa'][0],
                                         ['cups', 'potencia'])
            cups = cups_obj.read(cursor, uid, contract['cups'][0], ['name'])[
                'name']
            hours = get_hours_amount(file_data['date_from'], date_to)

            if 'tg' in meter and meter['tg']:
                profile_obj = tg_profile_obj
            else:
                profile_obj = tm_profile_obj
            valid = False
            cch_fact = False
            if file_type == 'f1' or (file_type == 'p1' and not all_profiles):
                valid = True
            if file_type == 'f1':
                cch_fact = True

            conversor = profile_obj.CONVERSOR
            profiles_ids = profile_obj.get_curve(cursor, uid, meter['id'], date_from, date_to, cch_fact, valid)
            meter_name = meter['name']

            if not profiles_ids:
                msg = _('No profiles found for the requested meter '
                        '{} \n\n').format(meter_name)
                result['info'] += msg
                logger.info(msg)
                continue
            if len(profiles_ids) > hours:
                info = _('Meter: {}. Found more profiles than expected. The period of time '
                         'you requested should have {} hours. You currently have '
                         '{} profiles.'.format(meter_name, hours, len(profiles_ids)))
            elif len(profiles_ids) < hours:
                info = _('Meter: {}. Found less profiles than expected. The period of time '
                         'you requested should have  {} hours. You currently have '
                         '{} profiles.'.format(meter_name, hours, len(profiles_ids)))
            else:
                info = ""
            if info and file_type == 'f1':
                result['info'] += info + '\n\n'
                continue
            try:
                daily_files = self.write_file_rows(cursor, uid, profile_obj,
                                                   profiles_ids, cups, file_data,
                                                   daily_files, context=context)
                to_check[cups] = (meter['name'], profile_obj, profiles_ids, conversor)
            except Exception:
                msg = _('Error trying to write file for meter '
                        '{} \n\n').format(meter_name)
                result['info'] += msg
                logger.info(msg)

        for filename, content in daily_files.items():
            zip_writer.writestr(filename, content.getvalue())
            if file_data['origin'] == 'meter':
                content.close()
        zip_writer.close()

        check_info, correct = self.check_files_created(
            cursor, uid, zf_io, file_type, to_check, context=context)
        result['info'] += check_info
        if file_data['origin'] == 'meter':
            if correct:
                result['data'] = zf_io
        else:  # Invoice origin
            result['data'] = daily_files
        return result

    @staticmethod
    def write_file_rows(cursor, uid, profile_obj, profiles_ids, cups,
                        file_data, daily_files, context=None):
        file_type = file_data['file_type']
        r1_code = file_data['r1_code']
        version = file_data['version']
        conversor = profile_obj.CONVERSOR
        if file_type == 'f1':
            prefix = 'F1_'
        else:
            prefix = 'P1_'
        current_day = 0
        ai_drag = Dragger()
        ae_drag = Dragger()
        r1_drag = Dragger()
        r2_drag = Dragger()
        r3_drag = Dragger()
        r4_drag = Dragger()
        for profile_id in profiles_ids:
            profile = profile_obj.read(cursor, uid, profile_id, [], context)
            current_hour = int(profile['timestamp'][11:13])
            if current_day != int(profile['timestamp'][8:10]):
                change = True
            if change and current_hour > 0:
                change = False
                daily_file_name = prefix + str(r1_code) + '_' + \
                                  profile['timestamp'].replace('-', '')[0:8] \
                                  + '_' + str(time.strftime("%Y%m%d")) + '.' \
                                  + str(version)
                aux = daily_files.get('data', False)
                if daily_files.get(daily_file_name, False) or (
                        aux and aux.get(daily_file_name, False)):
                    daily_file = daily_files[daily_file_name]
                else:
                    daily_file = StringIO.StringIO()
                    daily_files[daily_file_name] = daily_file
                writer = csv.writer(daily_file, delimiter=';')
            current_day = int(profile['timestamp'][8:10])
            timestamp = profile['timestamp'].replace('-', '/')
            if profile['season'] == 'W':
                season = '0'
            else:
                season = '1'

            if file_type == 'f1':
                if conversor == 1000:  # Profiles from TG convert to Kw
                    f1_best = {
                        'ai': int(ai_drag.drag(profile['ai_fact'] / conversor))
                        if profile.get('ai_fact', False) else 0,
                        'ae': int(ae_drag.drag(profile['ae_fact'] / conversor))
                        if profile.get('ae_fact', False) else 0,
                        'r1': int(r1_drag.drag(profile['r1_fact'] / conversor))
                        if profile.get('r1_fact', False) else 0,
                        'r2': int(r2_drag.drag(profile['r2_fact'] / conversor))
                        if profile.get('r2_fact', False) else 0,
                        'r3': int(r3_drag.drag(profile['r3_fact'] / conversor))
                        if profile.get('r3_fact', False) else 0,
                        'r4': int(r4_drag.drag(profile['r4_fact'] / conversor))
                        if profile.get('r4_fact', False) else 0,
                    }
                else:  # Profiles from TM use raw Kw value
                    f1_best = {
                        'ai': int(profile['ai_fact'])
                        if profile.get('ai_fact', False) else 0,
                        'ae': int(profile['ae_fact'])
                        if profile.get('ae_fact', False) else 0,
                        'r1': int(profile['r1_fact'])
                        if profile.get('r1_fact', False) else 0,
                        'r2': int(profile['r2_fact'])
                        if profile.get('r2_fact', False) else 0,
                        'r3': int(profile['r3_fact'])
                        if profile.get('r3_fact', False) else 0,
                        'r4': int(profile['r4_fact'])
                        if profile.get('r4_fact', False) else 0,
                    }
                writer.writerow(
                    [cups, '11', timestamp, season, f1_best['ai'],
                     f1_best['ae'], f1_best['r1'],
                     f1_best['r2'], f1_best['r3'],
                     f1_best['r4'], '', '', str(1), str(1)])
            else:
                if conversor == 1000:  # Profiles from TG set qualities at 0
                    writer.writerow([cups, '11', timestamp, season,
                                     format(float(profile.get('ai', 0)) /
                                            conversor, '.3f'), 0,
                                     format(float(profile.get('ae', 0)) /
                                            conversor, '.3f'), 0,
                                     format(float(profile.get('r1', 0)) /
                                            conversor, '.3f'), 0,
                                     format(float(profile.get('r2', 0)) /
                                            conversor, '.3f'), 0,
                                     format(float(profile.get('r3', 0)) /
                                            conversor, '.3f'), 0,
                                     format(float(profile.get('r4', 0)) /
                                            conversor, '.3f'), 0,
                                     '', '', '', '', str(1), str(1)])
                else:  # Profiles from TM use quality fields
                    writer.writerow([cups, '11', timestamp, season,
                                     format(float(profile.get('ai', 0)), '.3f'),
                                     int(profile.get('quality_ai', 0) or '0'),
                                     format(float(profile.get('ae', 0)), '.3f'),
                                     int(profile.get('quality_ae', 0) or '0'),
                                     format(float(profile.get('r1', 0)), '.3f'),
                                     int(profile.get('quality_r1', 0) or '0'),
                                     format(float(profile.get('r2', 0)), '.3f'),
                                     int(profile.get('quality_r2', 0) or '0'),
                                     format(float(profile.get('r3', 0)), '.3f'),
                                     int(profile.get('quality_r3', 0) or '0'),
                                     format(float(profile.get('r4', 0)), '.3f'),
                                     int(profile.get('quality_r4', 0) or '0'),
                                     '', '', '', '', str(1), str(1)])

        return daily_files

    def check_files_created(self, cursor, uid, z_file, file_type, to_check,
                            context=None):
        for cups, values in to_check.items():
            totals_tm = self.get_totals_on_period(cursor, uid, values[1],
                                                  values[2], context=context)
            to_check[cups] = (values[0], totals_tm, values[3])
        return self.check_file_created(cursor, uid, to_check, z_file, file_type,
                                       context=context)

    def check_file_created(self, cursor, uid, totals_db, res_file, f_type,
                           context=None):
        f_totals = {}
        measures = {'ai': 0, 'ae': 0, 'r1': 0, 'r2': 0, 'r3': 0, 'r4': 0}
        f_cols = {
            'f1': {'ai': 4, 'ae': 5, 'r1': 6, 'r2': 7, 'r3': 8, 'r4': 9},
            'p1': {'ai': 4, 'ae': 6, 'r1': 8, 'r2': 10, 'r3': 12, 'r4': 14}
        }
        f_zip = zipfile.ZipFile(res_file)
        for cups in totals_db:
            f_totals.setdefault(cups, measures.copy())
        for name in f_zip.namelist():
            uncompressed = f_zip.read(name)
            csv_file = StringIO.StringIO(uncompressed)
            csv_reader = csv.reader(csv_file, delimiter=';')
            for line in csv_reader:
                if line[0] in totals_db.keys():
                    f_totals[line[0]]['ai'] += float(line[f_cols[f_type]['ai']])
                    f_totals[line[0]]['ae'] += float(line[f_cols[f_type]['ae']])
                    f_totals[line[0]]['r1'] += float(line[f_cols[f_type]['r1']])
                    f_totals[line[0]]['r2'] += float(line[f_cols[f_type]['r2']])
                    f_totals[line[0]]['r3'] += float(line[f_cols[f_type]['r3']])
                    f_totals[line[0]]['r4'] += float(line[f_cols[f_type]['r4']])
        if f_type == 'f1':
            result = self.check_f1_created(totals_db, f_totals, context=context)
        else:
            result = self.check_p1_created(totals_db, f_totals, context=context)
        return result

    @staticmethod
    def check_f1_created(totals_db, totals_f1, context=None):
        info = ''
        error = False
        for cups, t_f1 in totals_f1.items():
            if error:
                break
            t_db = totals_db[cups][1]
            conv = totals_db[cups][2]
            if (
                    int(t_db['ai_fact'] / conv) <= int(t_f1['ai']) <= int(t_db['ai_fact'] / conv)+1 and
                    int(t_db['ae_fact'] / conv) <= int(t_f1['ae']) <= (int(t_db['ae_fact'] / conv)+1) and
                    int(t_db['r1_fact'] / conv) <= int(t_f1['r1']) <= (int(t_db['r1_fact'] / conv)+1) and
                    int(t_db['r2_fact'] / conv) <= int(t_f1['r2']) <= (int(t_db['r2_fact'] / conv)+1) and
                    int(t_db['r3_fact'] / conv) <= int(t_f1['r3']) <= (int(t_db['r3_fact'] / conv)+1) and
                    int(t_db['r4_fact'] / conv) <= int(t_f1['r4']) <= (int(t_db['r4_fact'] / conv)+1)
            ):
                info += _('CUPS: {} - Meter: {} \n Created correctly.\n\n'
                          ).format(cups, totals_db[cups][0])
            else:
                error = True
                error_info = _("CUPS: {}\n Meter: {}\nError.\n"
                               "Total on database --> Total on F1 file\n"
                               "Total active import: {} --> {}\n "
                               "Total active export: {} --> {}\n "
                               "Total reactive R1: {} --> {}\n "
                               "Total reactive R2: {} --> {}\n "
                               "Total reactive R3: {} --> {}\n "
                               "Total reactive R4: {} --> {}\n\n").format(
                    cups, totals_db[cups][0],
                    t_db['ai_fact']/conv, t_f1['ai'],
                    t_db['ae_fact']/conv, t_f1['ae'],
                    t_db['r1_fact']/conv, t_f1['r1'],
                    t_db['r2_fact']/conv, t_f1['r2'],
                    t_db['r3_fact']/conv, t_f1['r3'],
                    t_db['r4_fact']/conv, t_f1['r4'])
                info += error_info
        if error:
            return info, False
        else:
            return info, True

    @staticmethod
    def check_p1_created(totals_db, totals_p1, context=None):
        info = ''
        error = False
        for cups, t_p1 in totals_p1.items():
            if error:
                break
            t_db = totals_db[cups][1]
            conv = float(totals_db[cups][2])
            if (
                    round(t_db['ai']/conv) <= round(t_p1['ai']) <= round((t_db['ai']/conv)+1) and
                    round(t_db['ae']/conv) <= round(t_p1['ae']) <= round((t_db['ae']/conv)+1) and
                    round(t_db['r1']/conv) <= round(t_p1['r1']) <= round((t_db['r1']/conv)+1) and
                    round(t_db['r2']/conv) <= round(t_p1['r2']) <= round((t_db['r2']/conv)+1) and
                    round(t_db['r3']/conv) <= round(t_p1['r3']) <= round((t_db['r3']/conv)+1) and
                    round(t_db['r4']/conv) <= round(t_p1['r4']) <= round((t_db['r4']/conv)+1)
            ):
                info += _('CUPS: {} - Meter: {} \n Created correctly.\n\n'
                          ).format(cups, totals_db[cups][0])
            else:
                error = True
                error_info = _("CUPS: {}\n Meter: {}\nError.\n"
                               "Total on database --> Total on P1 file\n"
                               "Total active import: {} --> {}\n "
                               "Total active export: {} --> {}\n "
                               "Total reactive R1: {} --> {}\n "
                               "Total reactive R2: {} --> {}\n "
                               "Total reactive R3: {} --> {}\n "
                               "Total reactive R4: {} --> {}\n\n").format(
                    cups, totals_db[cups][0],
                    t_db['ai']/conv, t_p1['ai'],
                    t_db['ae']/conv, t_p1['ae'],
                    t_db['r1']/conv, t_p1['r1'],
                    t_db['r2']/conv, t_p1['r2'],
                    t_db['r3']/conv, t_p1['r3'],
                    t_db['r4']/conv, t_p1['r4'])
                info += error_info
        if error:
            return info, False
        else:
            return info, True

    _columns = {

    }


TelemesuresFiles()
