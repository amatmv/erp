# -*- coding: utf-8 -*-
{
    "name": "Telemesures Distri",
    "description": """
    Afageix l'assistent base per exportar curves
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_telemesures_base",
        "giscedata_ftp_connections",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "giscedata_telemesures_distri_view.xml",
        "giscedata_ftp_provider_view.xml",
        "wizard/wizard_export_measures_curve_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
