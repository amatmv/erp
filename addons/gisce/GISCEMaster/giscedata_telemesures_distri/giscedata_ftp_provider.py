# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataFTPProvider(osv.osv):

    _inherit = 'giscedata.ftp.provider'

    _columns = {
        'f1_enabled': fields.boolean('Enabled for F1'),
        'f1_upload': fields.char(
            "F1 Upload folder",
            size=256,
        ),
        'p1_enabled': fields.boolean('Enabled for P1'),
        'p1_upload': fields.char(
            "P1 Upload folder",
            size=256,
        ),
    }

    _defaults = {
        'f1_enabled': lambda *a: False,
        'p1_enabled': lambda *a: False,
    }


GiscedataFTPProvider()
