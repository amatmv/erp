# -*- coding: utf-8 -*-

from osv import osv, fields


class SignatureDocumentStatus(osv.osv_memory):
    _name = 'wizard.sign.document.status'

    def action_status_proces_signatura(self, cursor, uid, ids, context=None):
        signatura_obj = self.pool.get('giscedata.signatura.documents')
        context['wizard'] = True
        signatura_obj.update_from_api(cursor, uid, ids, context=context)

        signatura_obj = self.pool.get('giscedata.signatura.documents')

        partner = context['active_id']
        sign_partner = signatura_obj.search(cursor, uid, [(
            'partner', '=', partner), ('active', '=', True)])

        if not sign_partner:
            self.write(cursor, uid, ids, {
                'state': 'done'
            })

    def close(self, cursor, uid, ids, context=None):
        return {}

    def _default_signature_id(self, cursor, uid, context):
        actual_partner = context['active_id']
        signatura_obj = self.pool.get('giscedata.signatura.documents')

        doc_ids = signatura_obj.search(
            cursor, uid, [('partner', '=', actual_partner)])
        doc_vals = signatura_obj.read(
            cursor, uid, doc_ids, ['status', 'name', 'signatory_id'])

        return doc_vals

    def _default_state(self, cursor, uid, context):
        actual_partner = context['active_id']
        signatura_obj = self.pool.get('giscedata.signatura.documents')

        doc_ids = signatura_obj.search(
            cursor, uid, [('partner', '=', actual_partner)])
        doc_vals = signatura_obj.read(
            cursor, uid, doc_ids, ['status', 'name', 'signatory_id'])
        if not doc_vals:
            return 'sense_docs'
        else:
            return 'init'

    def _get_signature_ids(
            self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}

        actual_partner = context['active_id']
        signatura_obj = self.pool.get('giscedata.signatura.documents')

        doc_ids = signatura_obj.search(
            cursor, uid, [('partner', '=', actual_partner)])
        doc_vals = signatura_obj.read(
            cursor, uid, doc_ids, ['status', 'name', 'signatory_id'])
        res = dict.fromkeys(ids, None)
        for self_id in ids:
            res[self_id] = [e['id'] for e in doc_vals]
        return res

    _columns = {
        'signature_ids': fields.function(
            _get_signature_ids, type='one2many',
            obj='giscedata.signatura.documents', method=True),
        'state': fields.selection([
            ('init', 'Estat Normal'),
            ('done', 'Sense firmes'),
            ('sense_docs', 'Sense documents per firmar')
        ], 'Estat'),
    }

    _defaults = {
        'signature_ids': _default_signature_id,
        'state': _default_state
    }


SignatureDocumentStatus()
