# -*- coding: utf-8 -*-

import datetime
from tools import config
from osv import osv, fields
import six.moves.urllib as urllib
from lleida_net.click_sign import Client
from giscedata_signatura_documents.giscedata_signatura_documents import _get_signatura_models


class SignDocument(osv.osv_memory):
    _name = 'wizard.sign.document'

    def _default_partner(self, cursor, uid, context=None):
        return context['active_id']

    def _default_email(self, cursor, uid, context=None):
        addr_obj = self.pool.get('res.partner.address')
        sql = addr_obj.q(cursor, uid).select(['email'], limit=1).where([
            ('partner_id', '=', context['active_id']),
            ('email', '!=', None)
        ])
        cursor.execute(*sql)
        if cursor.rowcount:
            return cursor.fetchone()[0]
        return False

    def _default_mobile(self, cursor, uid, context=None):
        addr_obj = self.pool.get('res.partner.address')
        sql = addr_obj.q(cursor, uid).select(['mobile'], limit=1).where([
            ('partner_id', '=', context['active_id']),
            ('mobile', '!=', None)
        ])
        cursor.execute(*sql)
        if cursor.rowcount:
            return cursor.fetchone()[0]
        else:
            sql = addr_obj.q(cursor, uid).select(['phone'], limit=1).where([
                ('partner_id', '=', context['active_id']),
                ('phone', '!=', None)
            ])
            cursor.execute(*sql)
            if cursor.rowcount:
                return cursor.fetchone()[0]
        return False

    def close(self, cursor, uid, ids, context=None):
        return {}

    def action_iniciar_proces_signatura(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        user = wiz.user
        email = wiz.email
        description = wiz.name
        document = wiz.document
        mobile = wiz.mobile
        partner_id = wiz.partner_id.id
        model = wiz.model

        user_splitted = user.split(' ')
        surname = ''
        name = user_splitted[0]
        if len(user_splitted) > 3:
            name = str(user_splitted[0]) + ' ' + str(user_splitted[1])
            surname = ' '.join(user_splitted[2:])
        elif len(user_splitted) > 2:
            name = str(user_splitted[0])
            surname = ' '.join(user_splitted[1:])
        elif len(user_splitted) > 1:
            surname = user_splitted[1]

        signature_obj = self.pool.get('giscedata.signatura.documents')
        res = signature_obj.new_signature(
            cursor, uid, ids, model, email, description, name, surname,
            document, mobile, partner_id, context
        )
        if res:
            self.write(cursor, uid, ids, {'state': 'done'})

    def onchange_model(self, cursor, uid, ids, model, partner_id):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if model:
            true_model, resource_id = model.split(',')
            model_obj = self.pool.get(true_model)

            # Ajusto perque nomes sutin les polisses del partner
            # Aquest ajust s'ha de fer per cada model disponible
            if true_model == 'giscedata.polissa':
                res['domain']['model'] = [('titular', '=', partner_id)]

            # Autoempleno la descripcio amb els valors del model
            if resource_id != '0':
                resource_id = model_obj.read(
                    cursor, uid, int(resource_id), ['name'])['name']
                if not resource_id:
                    res['value'].update({
                            'name': model_obj._description
                    })
                else:
                    res['value'].update({
                        'name': model_obj._description + " " + resource_id
                    })
        return res

    def _default_user(self, cursor, uid, context=None):
        partner_obj = self.pool.get("res.partner")
        partner_name = partner_obj.read(
            cursor, uid, context['active_id'], ['name'])['name']
        return partner_name

    _columns = {
        'model': fields.reference(
            'Model',
            _get_signatura_models,
            128,
            method=False,
            required=False
        ),
        'partner_id': fields.many2one('res.partner', 'Client', readonly=True),
        'user': fields.char(u'Usuari', size=255),
        'mobile': fields.char(u'Mòbil', size=255),
        'email': fields.char(
            u'E-mail',
            size=256,
            help=u"Cal introduïr una adreça de correu vàlida per tal de poder "
                 u"signar el document."
        ),
        'document_filename': fields.char('Nom document', size=255),
        'document': fields.binary('Document', required=True),
        'name': fields.char('Descripció', required=True, size=255),
        'state': fields.selection([
            ('init', 'Estat Normal'),
            ('done', 'Firmat')
        ], 'Estat')
    }

    _defaults = {
        'email': _default_email,
        'mobile': _default_mobile,
        'partner_id': _default_partner,
        'user': _default_user,
        'state': lambda *a: 'init'
    }


SignDocument()
