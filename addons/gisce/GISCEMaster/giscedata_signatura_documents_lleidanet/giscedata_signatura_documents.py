# -*- coding: utf-8 -*-
from osv import osv, fields
from lleida_net.click_sign import Client
from tools import config
from datetime import datetime
import base64
import six.moves.urllib as urllib
from tools.translate import _


class GiscedataSignaturaDocuments(osv.osv):
    _name = 'giscedata.signatura.documents'
    _inherit = 'giscedata.signatura.documents'

    def new_signature(self, cursor, uid, ids, model, email, description, name,
                      surname, document, mobile, partner_id, context):
        """
        This funcions starts a digitalSignature.process
        :param cursor:
        :param uid:
        :param ids:
        :param model: The model of the signed document
        :param email: The email of the entity who signes the document
        :param description: A small description as a hint for the ERP user
        :param name: Name of the entity who signes the document
        :param surname: Surname of the entity who signes the document
        :param document: Document in
        :param mobile:
        :param partner_id:
        :param context:
        :return:
        """
        encoded_pdf = urllib.parse.quote_plus(document)
        data = {
            "config_id": config['lleidanet_config'],
            "contract_id": "ContractID",
            "level": [
                {
                    "level_order": 0,
                    "signatories": [
                        {
                            "phone": mobile,
                            "email": email,
                            "name": name,
                            "surname": surname,
                        },
                    ]
                },
            ],
            "file": [
                {
                    "filename": "contract.pdf",
                    "content": encoded_pdf,
                    "file_group": "contract_files"
                }
            ]
        }

        client = Client(config['lleidanet_user'], config['lleidanet_password'])
        response = client.signature.start(data)
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        signatura_obj = self.pool.get('giscedata.signatura.documents')
        values = {
            'partner': partner_id,
            'signature_id': response['result']['signature']['signature_id'],
            'signatory_id': response['result']['signature']['signatories'][0]['signatory_id'],
            'status': "requested",
            'status_timestamp': timestamp,
            'document': document,
            'name': description,
            'active': True,
            'model': model
        }
        if response['code'] == 200:
            signatura_obj.create(cursor, uid, values, context=context)
            return True
        return False

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        return super(GiscedataSignaturaDocuments, self).create(
            cursor, uid, vals, context=context
        )

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        return super(GiscedataSignaturaDocuments, self).write(
            cursor, uid, ids, vals, context=context
        )

    def update_from_api(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wizard = 'wizard' in context
        # Codi per actualitzar
        if not wizard:
            partner = self.read(cursor, uid, ids[0], False)['partner'][0]
        else:
            partner = context['active_id']

        sign_partner = self.search(cursor, uid, [('partner', '=', partner),
                                                 ('active', '=', True)])
        client = Client(config['lleidanet_user'], config['lleidanet_password'])

        for proces in self.browse(cursor, uid, sign_partner, context=context):
            response = client.signature.status(
                int(proces.read(['signatory_id'])[0]['signatory_id']))
            if response['signatory_status'] != proces['status']:
                values = {
                    'status': response['signatory_status'],
                    'status_timestamp': datetime.fromtimestamp(
                        float(response['signatory_status_date'])).strftime(
                        '%Y-%m-%d %H:%M:%S'),  # Unix Time conversion
                }
                self.write(
                    cursor, uid, proces['id'], values, context=context)

            # Quan l'estat sigui finalitzat/signat/OK
            # S'ha de cridar el següent codi
            if response['signatory_status'] in ['signed']:

                # Consulto i guardo l'estampat i l'evidencia com attachment
                data = {
                    "signatory_id": proces['signatory_id'],
                    "signature_id": proces['signature_id']
                }
                try:
                    stamp = client.signature.get_document_stamp(
                        data)['file_doc'][0]['content']
                    evidence = client.signature.get_document_evidence(
                        data)['file_doc'][0]['content']

                    stamp_decoded = base64.urlsafe_b64decode(str(stamp) + "==")
                    evidence_decoded = base64.urlsafe_b64decode(
                        str(evidence) + "==")

                    stamp_store = base64.encodestring(stamp_decoded)
                    evidence_store = base64.encodestring(evidence_decoded)

                except Exception:
                    raise osv.except_osv(
                        _('Error!'),
                        "Malgrat que el document ja esta signat encara no "
                        "s'han generat els documents d'evidencia. Torni a "
                        "intentar-ho en uns minuts"
                    )

                self.write(cursor, uid, proces['id'], {
                    'document_signed_stamp': str(stamp_store),
                    'document_signed_evidence': str(evidence_store),
                    'active': False
                }, context=context)

                # Executo el codi especific del model
                model, resource_id = proces.model.split(',')
                if model:
                    obj_document = self.pool.get(model)
                    method = getattr(obj_document, 'process_signature_callback')

                    result = method(
                        cursor, uid, int(resource_id), context=context
                    )
        return self.read(cursor, uid, ids, [], context=context)

    def _documents(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = {}
        attach_obj = self.pool.get('ir.attachment')
        to_read = ['name', 'datas', 'datas_fname']
        for process_id in ids:
            res[process_id] = {
                'document': False,
                'document_filename': False,
                'document_signed_stamp': False,
                'document_signed_evidence': False
            }
            attach_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'giscedata.signatura.documents'),
                ('res_id', '=', process_id),
                ('name', 'in', (
                    'ORIGINAL_DOCUMENT', 'SIGNED_DOCUMENT_EVIDENCE',
                    'SIGNED_DOCUMENT_STAMP'))
            ])
            for attach in attach_obj.read(cursor, uid, attach_ids, to_read):
                res[process_id]['document_filename'] = attach['datas_fname']
                if attach['name'] == 'ORIGINAL_DOCUMENT':
                    res[process_id]['document'] = attach['datas']
                elif attach['name'] == 'SIGNED_DOCUMENT_STAMP':
                    res[process_id]['document_signed_stamp'] = attach['datas']
                elif attach['name'] == 'SIGNED_DOCUMENT_EVIDENCE':
                    res[process_id]['document_signed_evidence'] = attach['datas']
        return res

    def _document_inv(self, cursor, uid, res_id, name, value, args, context=None):
        if context is None:
            context = {}

        name_map = {
            'document': 'ORIGINAL_DOCUMENT',
            'document_signed_stamp': 'SIGNED_DOCUMENT_STAMP',
            'document_signed_evidence': 'SIGNED_DOCUMENT_EVIDENCE'
        }

        fname = '{}_filename'.format(name)

        attach_obj = self.pool.get('ir.attachment')
        attach_obj.create(cursor, uid, {
            'datas': value,
            'res_model': 'giscedata.signatura.documents',
            'res_id': res_id,
            'name': name_map[name],
            'datas_fname': context.get(fname, name_map[name]),
        })
        return True

    _columns = {
        'partner': fields.many2one('res.partner', 'Partner', required=True),
        'name': fields.char('Descripció', required=True, size=100),
        'document': fields.function(
            _documents, type='binary',
            fnct_inv=_document_inv,
            method=True, multi='documents',
            string="Document"
        ),
        'document_filename': fields.function(
            _documents, type='char', size=255,
            method=True, multi='documents'
        ),
        'document_signed_evidence': fields.function(
            _documents, type='binary',
            fnct_inv=_document_inv,
            method=True, multi='documents',
            string="Document evidencia signatura",
        ),
        'document_signed_stamp': fields.function(
            _documents, type='binary',
            fnct_inv=_document_inv,
            method=True, multi='documents',
            string="Document signat",
        ),
        'signature_id': fields.char('Id signatura', size=18),
        'signatory_id': fields.char('Id del Signant', size=18),
        'status': fields.char('Estat', required=True, size=30),
        'status_timestamp': fields.datetime('Data modificació estat'),
    }

    _order = 'status_timestamp desc'


GiscedataSignaturaDocuments()
