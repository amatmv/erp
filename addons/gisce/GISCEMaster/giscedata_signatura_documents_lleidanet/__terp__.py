# -*- coding: utf-8 -*-
{
    "name": "Signatura de documents (Base)",
    "description": """Módul de signatura de documents (Base)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_signatura_documents",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscedata_signatura_documents_view.xml",
        "wizard/giscedata_signatura_document_view.xml",
        "wizard/giscedata_signatura_document_status_view.xml",
        "res_partner_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
