# -*- coding: utf-8 -*-
{
    "name": "Módul Comercial Signatura Digital",
    "description": """
    Módul complementari per a la gestió de comercials. Afageix el menu de signatura digital als comercials.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_comercial_signatura_documents",
        "giscedata_signatura_documents_lleidanet",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        'menu_admin_comercial_view.xml',
        'menu_comercial_view.xml',
    ],
    "active": False,
    "installable": True
}
