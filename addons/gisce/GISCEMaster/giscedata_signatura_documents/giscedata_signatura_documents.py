# -*- coding: utf-8 -*-
from osv import osv, fields


def _get_signatura_models(obj, cursor, uid, context=None):
    if context is None:
        context = {}
    signatura_model_obj = obj.pool.get('giscedata.signatura.models')
    model_ids = signatura_model_obj.search(cursor, uid, [], context=context)
    res = []
    for model in signatura_model_obj.read(cursor, uid, model_ids, context=context):
        res.append((model['model'], model['name'][1]))
    return res


class GiscedataSignaturaModels(osv.osv):
    _name = 'giscedata.signatura.models'
    _columns = {
        'name': fields.many2one(
            'ir.model',
            'Model'
        ),
        'model': fields.related(
            'name', 'model',
            string='Internal name'
        )
    }


GiscedataSignaturaModels()


class GiscedataSignaturaDocuments(osv.osv):
    _name = 'giscedata.signatura.documents'

    def update_from_api(self, cursor, uid, ids, context=None):
        raise NotImplementedError

    _columns = {
        'model': fields.reference(
            'Model',
            _get_signatura_models,
            128,
            method=False,
        ),
        'active': fields.boolean('Actiu')
    }

    _defaults = {
        'active': lambda *a: True
    }


GiscedataSignaturaDocuments()
