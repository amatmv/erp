# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    if not installed_version:
        return

    logger.info(
        'Delete ir_model from old wizards.'
    )
    query_del = """
        DELETE FROM  ir_model_data
        WHERE
            module = 'giscedata_signatura_documents'
            AND name like ('%wizard_sign_document%');
        DELETE FROM ir_model_access WHERE model_id in (SELECT id FROM ir_model WHERE model LIKE 'wizard.sign.document%');
        DELETE FROM ir_model WHERE model LIKE 'wizard.sign.document%'
    """
    cursor.execute(query_del)
    logger.info('Migration succesful!')


def down(cursor, installed_version):
    pass


migrate = up
