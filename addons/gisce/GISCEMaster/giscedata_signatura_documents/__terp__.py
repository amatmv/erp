# -*- coding: utf-8 -*-
{
    "name": "Signatura de documents (Base)",
    "description": """Módul de signatura de documents (Base)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscedata_signatura_documents_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
