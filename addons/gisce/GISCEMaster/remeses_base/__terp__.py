# -*- coding: utf-8 -*-
{
    "name": "GISCE Remeses Base",
    "description": """Modificació de remeses de facturació""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Facturació",
    "depends": [
        "account_invoice_base",
        "l10n_ES_remesas",
    ],
    "init_xml": [],
    "demo_xml": [
        "bank_demo.xml",
    ],
    "update_xml": [
        "wizard/afegir_factures_fitxer_view.xml",
        "wizard/crear_remesa_view.xml",
        "wizard/pagar_remesa_wizard_view.xml",
        "invoice_view.xml",
        "payment_order_history_view.xml",
        "payment_report.xml",
        "payment_view.xml",
        "payment_data.xml",
        "security/remeses_base_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
