# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class ResPartner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def update_mandate(self, cursor, uid, partner_id, context=None):

        if not context:
            context = {}

        if isinstance(partner_id, (list, tuple)):
            partner_id = partner_id[0]

        addr_obj = self.pool.get("res.partner.address")
        partner = self.browse(cursor, uid, partner_id)
        default_address = partner.address_get()['default']
        default_address = addr_obj.browse(cursor, uid, default_address, context=context)
        debtor_address = (default_address.name_get(context=context)[0][1]).upper()

        debtor_state = (default_address.state_id and default_address.state_id.name or '').upper()
        debtor_country = (default_address.country_id and default_address.country_id.name or '').upper()
        notes = u"Cliente: %s\n" % partner.name

        default_partner_bank = len(partner.bank_ids) and partner.bank_ids[0]
        for b in partner.bank_ids:
            if b.default_bank:
                default_partner_bank = b
                break

        if default_partner_bank and default_partner_bank.owner_name:
            payment_data = u""
            bank_obj = self.pool.get('res.partner.bank')
            fields_bank = bank_obj.fields_get(cursor, uid, fields=['owner_id', 'owner_address_id']).keys()
            if fields_bank:
                if default_partner_bank.owner_id:
                    payment_data += u" NIF: {0}".format(
                        default_partner_bank.owner_id.vat
                    )
                else:
                    payment_data += u" NIF:"
                if default_partner_bank.owner_address_id:
                    phone = default_partner_bank.owner_address_id.phone
                    if not phone:
                        phone = default_partner_bank.owner_address_id.mobile
                    payment_data += u" Teléfono: {0}".format(phone)
                else:
                    payment_data += u" Teléfono:"

            notes += u"Pagador:\n"
            notes += u"Nombre y apellidos: {0}{1}\n".format(
                default_partner_bank.owner_name, payment_data
            )

        vals = {
            'debtor_name': partner.name,
            'debtor_vat': partner.vat,
            'debtor_address': debtor_address,
            'debtor_state': debtor_state,
            'debtor_country': debtor_country,
            'debtor_iban': default_partner_bank and default_partner_bank.iban,
            'reference': '%s,%s' % ('res.partner', partner_id),
            'notes': notes,
        }

        return vals

ResPartner()
