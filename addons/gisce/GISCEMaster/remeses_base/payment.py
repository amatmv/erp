# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
from datetime import datetime
from l10n_ES_remesas.remesas import TIPOS_REMESAS_EXTENDED
import pooler
import netsvc
from oorq.oorq import ProgressJobsPool
from autoworker import AutoWorker
from oorq.decorators import job
from oorq.oorq import setup_redis_connection
from osv.osv import TransactionExecute


class PaymentOrder(osv.osv):
    """Modificació payment.order per cridar el wizard
    """

    _name = 'payment.order'
    _inherit = 'payment.order'
    _order = 'date_created desc'

    def onchange_mode(self, cursor, uid, ids, mode_pagament, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not mode_pagament:
            return res
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        if context is None:
            context = {}
        payment_mode_o = self.pool.get("payment.mode")
        pmode_info = payment_mode_o.read(cursor, uid, mode_pagament, ['tipo', 'name'])
        pmode_type = pmode_info['tipo']
        if not pmode_type:
            return res
        pmode_type = TIPOS_REMESAS_EXTENDED.get(pmode_type, {}).get('type')
        for info in self.read(cursor, uid, ids, ['type']):
            if info['type'] != pmode_type:
                printed_text = info['type']
                if printed_text == "receivable":
                    printed_text = _(u"a cobrar")
                elif printed_text == "payable":
                    printed_text = _(u"a pagar")
                res['warning'].update({
                    'title': _(u'Error Usuari'),
                    'message': _(u"S'ha seleccionat un mode de pagament ({0}) incompatible amb el tipus de remesa ({1}).").format(pmode_info['name'], printed_text)
                })
        return res

    def _total(self, cursor, user, ids, name, args, context=None):
        res = {}
        query = '''SELECT porder.id, coalesce(sum(pline.amount), 0)
                FROM payment_order porder
                INNER JOIN payment_line pline
                ON porder.id = pline.order_id
                GROUP BY porder.id'''
        cursor.execute(query)
        for order_id, amount in cursor.fetchall():
            res[order_id] = amount
        return res

    def _launch_wizard(self, cursor, uid, ids, context=None):
        """Mètode per llençar el wizard
        """
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.seleccionar.factures.remesa',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def _get_n_line_order_ids(self, cursor, uid, ids, context=None):
        """ids are payment_lines.
        """
        query = '''SELECT distinct order_id
                FROM payment_line
                WHERE id in %s'''
        cursor.execute(query, (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _ff_n_lines_order(self, cursor, uid, ids, field_name, arg,
                             context=None):
        res = {}
        query = '''SELECT porder.id, coalesce(count(pline.id), 0)
                FROM payment_order porder
                INNER JOIN payment_line pline
                ON porder.id = pline.order_id
                WHERE porder.id in %s
                GROUP BY porder.id'''
        cursor.execute(query, (tuple(ids),))
        for order_id, n_lines in cursor.fetchall():
            res[order_id] = n_lines
        return res

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        res = super(PaymentOrder, self).write(cursor, uid, ids, vals, context)
        if 'date_planned' in vals:
            pl_obj = self.pool.get('payment.line')
            for order in self.read(cursor, uid, ids, ['date_prefered']):
                if order['date_prefered'] == 'fixed':
                    pl_ids = pl_obj.search(cursor, uid, [
                        ('order_id', '=', order['id'])
                    ])
                    pl_obj.write(
                        cursor, uid, pl_ids, {'date': vals['date_planned']}
                    )
        return res

    def add_move_lines(self, cursor, uid, order_id, ml_ids, context=None):

        ml_obj = self.pool.get('account.move.line')
        order_obj = self.pool.get('payment.order')
        pl_obj = self.pool.get('payment.line')

        order = order_obj.browse(cursor, uid, order_id)

        pm_id = order.mode and order.mode.type.id or None
        l2b = ml_obj.line2bank(cursor, uid, ml_ids, pm_id, context)

        res = []

        for line in ml_obj.browse(cursor, uid, ml_ids, context=context):
            if order.date_prefered == "now":
                # no payment date => immediate payment
                date_to_pay = False
            elif order.date_prefered == 'due':
                date_to_pay = line.date_maturity
            elif order.date_prefered == 'fixed':
                date_to_pay = order.date_planned
            # Check if line.id already exists in this payment order
            existing_ids = pl_obj.search(cursor, uid, [
                ('move_line_id', '=', line.id),
                ('order_id', '=', order.id)
            ])
            if existing_ids:
                continue
            # Payable orders must be positive
            # Receivable order must be negative
            if ((order.type == 'payable' and line.amount_to_pay > 0) or
                    (order.type == 'receivable' and line.amount_to_pay < 0)):
                plid = pl_obj.create(cursor, uid,{
                    'move_line_id': line.id,
                    'amount_currency': line.amount_to_pay,
                    'bank_id': l2b.get(line.id),
                    'order_id': order.id,
                    'partner_id': (line.partner_id and line.partner_id.id or
                                   False),
                    'communication': (line.ref and line.name!='/' and
                                      line.ref+'. '+line.name) or (
                                      line.ref or line.name or '/'),
                    'date': date_to_pay,
                    'currency': (line.invoice and line.invoice.currency_id.id or
                                 context.get('currency', False)),
                    'account_id': line.account_id.id,
                    }, context=context)
                res.append(plid)
        return res

    def cnd_check_paid(self, cursor, uid, ids):
        """Comprovem la condició de contracte actiu."""
        if isinstance(ids, (long, int)):
            ids = [ids]

        res = super(PaymentOrder, self).cnd_check_paid(cursor, uid, ids)

        for order in self.read(cursor, uid, ids, ['paid']):
            if order['paid']:
                raise osv.except_osv(
                    _(u'Error!'),
                    _(u'La remesa ja està pagada')
                )
        return True

    def pay_and_reconcile_multiple(
            self, cursor, uid, ids, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, period_id=None, context=None
    ):
        """
        Pay and reconcile all the lines of a payment order.
        :param cursor:
        :param uid:
        :param ids: <payment.order> ids
        :param writeoff_acc_id:
        :param writeoff_period_id:
        :param writeoff_journal_id:
        :param period_id: if None, it will be used the order payment's period.
        :param context:
        :return:
        """
        invoice_o = self.pool.get("account.invoice")
        if not isinstance(ids, list):
            ids = [ids]
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['from_payment_line'] = True
        payment_orders = self.browse(cursor, uid, ids, context=context)
        total_failed = []
        for payment_order in payment_orders:
            pids = [x.id for x in payment_order.line_ids]
            mode = payment_order.mode
            if payment_order.paid:
                raise osv.except_osv(
                    _("Error Usuari"),
                    _(u"La remesa {0} ja està pagada").format(payment_order.name)
                )
            if payment_order.payment_move_id:
                invoice_o.clean_moves(cursor, uid, payment_order.payment_move_id.id)
            failed = invoice_o.pay_and_reconcile_multiple(
                cursor, uid, payment_order.total, pids, payment_order.reference,
                mode.journal.id,
                pay_partner_id=mode.partner_id.id,
                pay_account_id=mode.journal.default_credit_account_id.id,
                date=payment_order.date_planned,
                period_id=period_id,
                writeoff_acc_id=writeoff_acc_id,
                writeoff_period_id=writeoff_period_id,
                writeoff_journal_id=writeoff_journal_id,
                cobraments_journal_id=mode.cobraments_journal_id and mode.cobraments_journal_id.id,
                context=ctx
            )
            total_failed.extend(failed)
        return total_failed

    @job(queue="pay_and_reconcile_tasks")
    def pay_and_reconcile_async(
            self, cursor, uid, line_id, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context=None, name=''
    ):
        return self.pay_and_reconcile(
            cursor, uid, line_id, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context, name
        )

    def pay_and_reconcile(
            self, cursor, uid, line_id, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context=None, name=''
    ):
        if context is None:
            context = {}
        invoice_o = self.pool.get("account.invoice")
        payment_line_o = self.pool.get("payment.line")
        line = payment_line_o.browse(cursor, uid, line_id)
        grouped = False
        order_type = line.order_id.type
        if not line.ml_inv_ref:
            group_account = line.move_line_id.account_id
            inv_id = invoice_o.search(cursor, uid, [
                ('group_move_id', '=', line.move_line_id.move_id.id),
                ('account_id', '=', group_account.id)
            ])
            if not inv_id:
                ml_obj = self.pool.get('account.move.line')
                move_obj = self.pool.get('account.move')
                ref = line.move_line_id.ref
                name = line.move_line_id.name
                date = line.date
                partner_id = line.partner_id.id
                move_id = move_obj.create(cursor, uid, {
                    'ref': ref,
                    'journal_id': pay_journal_id,
                    'period_id': period_id,
                    'date': date
                }, context=context)

                amount = line.amount_currency
                if order_type == 'payable':
                    debit_amount = amount
                    credit_amount = 0
                elif order_type == 'receivable':
                    debit_amount = 0
                    credit_amount = abs(amount)
                move_line_orig = ml_obj.create(cursor, uid, {
                    'name': name,
                    'credit': credit_amount,
                    'debit': debit_amount,
                    'account_id': line.move_line_id.account_id.id,
                    'partner_id': partner_id,
                    'ref': ref,
                    'date': date,
                    'move_id': move_id,
                })
                move_line_bank = ml_obj.create(cursor, uid, {
                    'name': name,
                    'credit': debit_amount,
                    'debit': credit_amount,
                    'account_id': pay_account_id,
                    'partner_id': partner_id,
                    'ref': ref,
                    'date': date,
                    'move_id': move_id,
                })
                move_lines = [line.move_line_id.id, move_line_orig]
                ml_obj.reconcile_lines(
                    cursor, uid, move_lines, 'manual', writeoff_acc_id,
                    writeoff_period_id, writeoff_journal_id, context=context
                )
                line.write({'payment_move_id': move_id}, context=context)
                return move_id
            inv_id = inv_id[0]
            grouped = True
        elif line.ml_inv_ref.state != 'open':
            return False
        else:
            inv_id = line.ml_inv_ref.id
        context.update({'date_p': line.date})
        if grouped:
            if order_type == 'payable':
                amount = -1.0 * abs(line.amount_currency)
            else:  # order_type == 'receivable'
                amount = abs(line.amount_currency)
        else:
            amount = abs(line.amount_currency)
        move_id = invoice_o.pay_and_reconcile(
            cursor, uid, [inv_id], amount, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context=context, name=name
        )
        payment_line_o.write(cursor, uid, line.id, {'payment_move_id': move_id})
        return move_id

    _columns = {
        'n_lines': fields.function(
            _ff_n_lines_order,
            method=True,
            string="Lines",
            type='integer',
            store={
                'payment.line': (_get_n_line_order_ids, ['order_id'], 10),
                'payment.order': (
                    lambda self, cr, uid, ids, c=None: ids, [], 10
                )
            }),
        'total': fields.function(_total, string="Total", method=True,
                                 type='float'),
        'paid': fields.boolean('Pagat', select=1, readonly=True),
        'forcar_sense_fitxer': fields.boolean("Forçar pagaments sense exportar fitxer"),
        'payment_move_id': fields.many2one(
            "account.move", "Moviment de pagament", ondelete="set null"
        ),
    }

    _defaults = {
        'paid': lambda *a: False,
        'create_account_moves': lambda *a: 'bank-statement',
        'date_prefered': lambda *a: 'fixed'
    }


PaymentOrder()


class PaymentLine(osv.osv):

    _name = 'payment.line'
    _inherit = 'payment.line'

    def _get_ml_inv_ref(self, cr, uid, ids, *a):
        res = {}
        query = '''SELECT pl.id, coalesce(i.id,0)
                FROM payment_line pl
                LEFT JOIN account_move_line ml
                ON pl.move_line_id = ml.id
                LEFT JOIN account_invoice i
                ON i.move_id = ml.move_id
                WHERE pl.id in %s'''
        cr.execute(query, (tuple(ids),))
        for payment_line_id, invoice_id in cr.fetchall():
            res[payment_line_id] = invoice_id
        return res

    def _get_ml_maturity_date(self, cr, uid, ids, *a):
        res = {}
        query = '''SELECT pl.id, coalesce(ml.date_maturity::varchar,'')
                FROM payment_line pl
                LEFT JOIN account_move_line ml
                ON pl.move_line_id = ml.id
                WHERE pl.id in %s'''
        cr.execute(query, (tuple(ids),))
        for payment_line_id, date_maturity in cr.fetchall():
            res[payment_line_id] = date_maturity
        return res

    def unlink(self, cursor, uid, ids, context=None):
        # Al eliminar una linia de remesa, s'ha d'esborrar la referència
        # a la factura.
        invoices = []
        line = self.pool.get('account.move.line')
        move = self.pool.get('account.move')
        invoice_obj = self.pool.get('account.invoice')
        pay_hist_obj = self.pool.get('payment.order.history')
        for linia in self.read(cursor, uid, ids, ['move_line_id', 'order_id']):
            invoices_per_line = set()
            if not linia['move_line_id']:
                continue
            inv = line.read(cursor, uid, linia['move_line_id'][0],
                            ['invoice', 'move_lines', 'move_id'])
            if inv['invoice']:
                invoices_per_line.add(inv['invoice'][0])
            else:
                # Agrupació de factures. Obtenim les invoices de l'agrupació
                id_move = inv['move_id'][0]
                moves = move.read(cursor, uid, id_move, ['line_id'])['line_id']
                for elem in moves:
                    invoice = line.read(
                        cursor, uid, elem, ['invoice'])['invoice']
                    if invoice:
                        invoices_per_line.add(invoice[0])
            # Remove the history
            hist_ids = pay_hist_obj.search(cursor, uid, [
                ('invoice_id', 'in', list(invoices_per_line)),
                ('payment_order_id', '=', linia['order_id'][0])
            ])
            if hist_ids:
                pay_hist_obj.unlink(cursor, uid, hist_ids)
            invoices += list(invoices_per_line)
        values = {
            'payment_order_id': False
        }
        invoice_obj.write(cursor, uid, invoices, values, context)
        super(PaymentLine, self).unlink(cursor, uid, ids, context)

    _columns = {
        'ml_maturity_date': fields.function(_get_ml_maturity_date,
                                            method=True,
                                            type='date',
                                            string='Maturity Date'),
        'ml_inv_ref': fields.function(_get_ml_inv_ref, method=True,
                                      type='many2one',
                                      relation='account.invoice',
                                      string='Invoice Ref.'),
    }


PaymentLine()


class payment_mode(osv.osv):
    _inherit = 'payment.mode'
    _columns = {
        'cobraments_journal_id': fields.many2one(
            'account.journal', 'Diari Gestió Cobraments',
            help="Si s'emplena aquest diari, al pagar desde una remesa es fan "
                 "uns moviments intermitjos: En la data que es marca per pagada "
                 "la remesa, es fa el moviment cap a (4312) Efectos comerciales en gestión de cobro. "
                 "En la data de venciment de la remesa, es fa el moviment de la 4312 al diari de pagament (p.e. Caixa).",
            required=False)
    }
    _defaults = {
        'cobraments_journal_id': lambda *a: False,
    }

payment_mode()

