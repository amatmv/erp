# -*- coding: utf-8 -*-
from osv import fields, osv
import time
from tools.translate import _


class AccountInvoice(osv.osv):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    _columns = {
        'payment_order_id': fields.many2one(
            'payment.order', u'Remesa', size=128, select=1),
        'payment_order_history_ids': fields.one2many(
            'payment.order.history', 'invoice_id', u'Històric de remeses', readonly=True)
    }

    def is_not_payable_manually(self, cursor, uid, invoice_id, context=None):
        if context is None:
            context = {}
        if isinstance(invoice_id, (list, tuple)):
            invoice_id = invoice_id[0]

        is_not_payable = super(AccountInvoice, self).is_not_payable_manually(
            cursor, uid, invoice_id, context=context)
        if is_not_payable:
            return is_not_payable

        invoice = self.browse(cursor, uid, invoice_id)
        remesa = invoice.payment_order_id
        if remesa:
            if remesa.state in ('cancel', 'done'):
                is_not_payable = False
            else:
                is_not_payable = {
                    'title': _(
                        'No es poden pagar factures en remesa no pagada!'
                    ),
                    'message': _(
                        'La factura {} ja té la remesa "{}" assignada.\n'
                        'S\'ha de treure de la remesa abans de pagar-la'
                    ).format(invoice.number, remesa.name)
                }
        return is_not_payable

    def afegeix_a_remesa(self, cursor, uid, ids, order_id, context=None):
  
        if not context:
            context = {}

        res = []
        added_invoices = []
        group_move_ids = set()

        order_obj = self.pool.get('payment.order')
        order_history_obj = self.pool.get('payment.order.history')
        ctx = context.copy()
        for invoice in self.browse(cursor, uid, ids, context=context):
            ctx['currency'] = invoice.currency_id.id
            if invoice.group_move_id:
                ml_ids = []
                ref = invoice.group_move_id.ref
                for move_line in invoice.group_move_id.line_id:
                    if move_line.name == ref:
                        ml_ids.append(move_line.id)
            else:
                ml_ids = self.move_line_id_payment_get(cursor, uid, [invoice.id])
            is_added = order_obj.add_move_lines(
                cursor, uid, order_id, ml_ids, context=ctx
            )
            if is_added:
                res += is_added
                added_invoices.append(invoice.id)
                if invoice.group_move_id:
                    group_move_ids.add(invoice.group_move_id.id)
        if group_move_ids:
            search_params = [('group_move_id', 'in', list(group_move_ids))]
            fids = self.search(cursor, uid, search_params)
            added_invoices = list(
                set(added_invoices).union(set(fids))
            )
        for invid in added_invoices:
            order_history_obj.create(cursor, uid, {
                'invoice_id': invid,
                'payment_order_id': order_id,
                'modify_date': time.strftime('%Y-%m-%d %H:%M:%S')
            })
        self.write(cursor, uid, added_invoices,
                   {'payment_order_id': order_id})

        return res


AccountInvoice()
