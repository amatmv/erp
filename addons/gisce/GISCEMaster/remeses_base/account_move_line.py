# -*- encoding: utf-8 -*-
from osv import fields, osv

class account_move_line(osv.osv):
    _inherit = "account.move.line"

    def get_invoice(self, cursor, uid, line, context=None):
        """
        Returns invoice associated to line.
        """
        invoice = super(account_move_line, self).get_invoice(cursor, uid, line, context)
        if invoice and invoice.partner_bank:
            return invoice

        # Search in other lines of the same move if there is any invoice with
        # partner_bank
        for line in line.move_id.line_id:
            if line.invoice and line.invoice.partner_bank:
                break
        return line.invoice

account_move_line()
