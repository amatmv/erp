# -*- coding: utf-8 -*-
from osv import fields, osv


class PaymentOrderHistory(osv.osv):
    _name = 'payment.order.history'

    _columns = {
        'invoice_id': fields.many2one(
            'account.invoice', u'Factura', size=128, select=1, ondelete="cascade"
        ),
        'payment_order_id': fields.many2one(
            'payment.order', u'Remesa', size=128, select=1
        ),
        'modify_date': fields.date(u'Data'),
    }


PaymentOrderHistory()
