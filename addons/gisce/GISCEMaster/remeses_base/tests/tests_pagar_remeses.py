# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from destral.patch import PatchNewCursors
import netsvc
import time
import random


class TestsPagarRemesa(testing.OOTestCase):

    def set_unique_payment_varconf(self, cursor, uid, value):
        """
        :param cursor:
        :param uid:
        :param value: 1 to activate the pay order feature, else 0.
        :return:
        """
        pool = self.openerp.pool

        varconf_obj = pool.get('res.config')

        dmn = [('name', '=', 'payment_order_unique_account_move')]
        varconf_id = varconf_obj.search(cursor, uid, dmn)
        varconf_wv = {'value': value}
        varconf_obj.write(cursor, uid, varconf_id, varconf_wv)

    def invoice_add_products_and_open(self, cursor, uid, invoice_ids):
        pool = self.openerp.pool

        imd_obj = pool.get('ir.model.data')
        product_obj = pool.get('product.product')
        invoice_obj = pool.get('account.invoice')
        invoice_line_obj = pool.get('account.invoice.line')

        product_ids = product_obj.search(cursor, uid, [])

        account_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'account_receivable_0002'
        )[1]

        wf_service = netsvc.LocalService("workflow")

        for invoice_id in invoice_ids:
            # Add random products to each invoice
            add_n_products = random.randint(1, 10)
            for n_product in range(0, add_n_products):
                random_product_index = random.randint(0, len(product_ids) - 1)
                random_product_id = product_ids[random_product_index]
                invoice_line_cv = {
                    'product_id': random_product_id,
                    'invoice_id': invoice_id,
                    'account_id': account_id,
                    'name': "random product",
                    'price_unit': random_product_index * random.random(),
                    'quantity': random_product_id
                }
                invoice_line_obj.create(
                    cursor, uid, invoice_line_cv
                )

            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            # Open invoices
            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

    def setup_payment_order(self, cursor, uid, type):
        """
        Set ups a payment order.
            - Payment date type: fixed (today).
            - State: draft.
            - Paid: not paid.
            - Partner: Tiny sprl.
        :param cursor:
        :param uid:
        :param type: payment order type. Allowed values: receivable, payable
        :type: str
        :return: <payment.order> browse record.
        """
        pool = self.openerp.pool

        imd_obj = pool.get('ir.model.data')
        payment_order_obj = pool.get('payment.order')

        remesa_id = imd_obj.get_object_reference(
            cursor, uid, 'account_payment', 'payment_order_demo'
        )[1]

        # We always do the payment on a fixed date
        remesa = payment_order_obj.browse(cursor, uid, remesa_id)
        remesa_wv = {
            'date_prefered': 'fixed',
            'date_planned': time.strftime('%Y-%m-%d'),
            'type': type
        }
        remesa.write(remesa_wv)
        remesa_mode_wv = {
            'partner_id': 1
        }
        remesa.mode.write(remesa_mode_wv)

        # Create payment order not finished
        payment_order_obj.write(
            cursor, uid, [remesa_id], {'state': 'draft', 'paid': False}
        )

        return remesa

    def pay_payment_order(self, cursor, uid, payment_order_id):
        pool = self.openerp.pool
        wz_pay_payment_order = pool.get('pagar.remesa.wizard')

        context = {
            'active_id': payment_order_id,
            'active_ids': [payment_order_id],
        }
        with PatchNewCursors():
            wz_id = wz_pay_payment_order.create(cursor, uid, {'work_async': False}, context=context)
            wz_pay_payment_order.action_pagar_remesa_threaded(
                cursor.dbname, uid, [wz_id], context=context
            )

    def unpay_invoice(self, cursor, uid, invoice):
        pool = self.openerp.pool
        period_obj = pool.get('account.period')
        imd_obj = pool.get('ir.model.data')

        account_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'account_unpaid'
        )[1]

        period_id = period_obj.find(cursor, uid, time.strftime('%Y-%m-%d'))[0]

        invoice.unpay(
            invoice.amount_total, account_id, period_id,
            invoice.payment_ids[-1].journal_id.id,
            name='Devolució rebut factura: {}'.format(invoice.number)
        )

    def test_pagar_remesa_unic_moviment(self):
        """
        Checks if works correctly paying a payment order
        (only customer invoices)
        :return:
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        move_obj = pool.get('account.move')
        move_line_obj = pool.get('account.move.line')
        invoice_obj = pool.get('account.invoice')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.set_unique_payment_varconf(cursor, uid, 1)

            invoice_ids = [
                imd_obj.get_object_reference(
                    cursor, uid, 'account', 'invoice_0001'
                )[1],
                imd_obj.get_object_reference(
                    cursor, uid, 'account', 'invoice_0002'
                )[1]
            ]

            remesa = self.setup_payment_order(cursor, uid, 'receivable')

            self.invoice_add_products_and_open(cursor, uid, invoice_ids)

            # Add the invoice to the payment order
            invoice_obj.afegeix_a_remesa(cursor, uid, invoice_ids, remesa.id)

            self.pay_payment_order(cursor, uid, remesa.id)

            # Search for the account_moves associated with the payment_order.
            dmn = [('ref', '=', remesa.reference)]
            account_move_id = move_obj.search(cursor, uid, dmn)

            # Fetch the account move lines of the account move
            move_line_f = ['credit', 'debit']
            dmn = [('move_id', '=', account_move_id[0])]
            move_lines = move_line_obj.q(cursor, uid).read(move_line_f).where(dmn)

            # There must be 3 move lines: one for each invoice and one that
            # balances the move.
            self.assertEquals(len(move_lines), 3)

            # Check if effectively the lines are balanced
            balance = 0
            for move_line in move_lines:
                balance += move_line['debit'] - move_line['credit']

            self.assertEquals(round(balance, 6), 0)

    def test_pagar_remesa_unic_moviment_amb_pagament_retornat(self):
        """

        :return:
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        payment_order_obj = pool.get('payment.order')
        invoice_obj = pool.get('account.invoice')
        account_move_line_obj = pool.get('account.move.line')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.set_unique_payment_varconf(cursor, uid, 1)

            invoice_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'invoice_0001'
            )[1]

            remesa = self.setup_payment_order(cursor, uid, 'receivable')
            remesa_vals = payment_order_obj.copy_data(cursor, uid, remesa.id)[0]

            self.invoice_add_products_and_open(cursor, uid, [invoice_id])

            # Add the invoice to the payment order
            invoice_obj.afegeix_a_remesa(cursor, uid, [invoice_id], remesa.id)

            self.pay_payment_order(cursor, uid, remesa.id)

            invoice = invoice_obj.browse(cursor, uid, invoice_id)

            self.unpay_invoice(cursor, uid, invoice)

            # Check if, within all the moves of the invoice, three of them
            # need to be partial reconciled:
            account_move_line_id = invoice.move_line_id_payment_get()

            # Theoretically there should be only one move line that represents
            # the total amoun of the invoice
            self.assertEqual(len(account_move_line_id), 1)

            account_move_line_id = account_move_line_id[0]
            account_move_line = account_move_line_obj.browse(
                cursor, uid, account_move_line_id
            )

            self.assertEqual(
                len(account_move_line.reconcile_partial_id.line_partial_ids), 3
            )
            self.assertFalse(account_move_line.reconcile_id)

            # Create another payment order.
            remesa_vals.update({'reference': '0001-00000000000000000000000002'})
            remesa_2_id = payment_order_obj.create(cursor, uid, remesa_vals)

            invoice_obj.afegeix_a_remesa(cursor, uid, [invoice_id], remesa_2_id)
            self.pay_payment_order(cursor, uid, remesa_2_id)

            # Check if, within all the moves of the invoice, four of them
            # are reconciled in the same reconciliation
            account_move_line = account_move_line_obj.browse(
                cursor, uid, account_move_line_id
            )

            self.assertFalse(account_move_line.reconcile_partial_id, 0)
            self.assertEqual(len(account_move_line.reconcile_id.line_id), 4)

    def test_pagar_remesa_unic_moviment_amb_pagament_retornat_i_factura_neta(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        move_obj = pool.get('account.move')
        account_move_line_obj = pool.get('account.move.line')
        invoice_obj = pool.get('account.invoice')
        payment_order_obj = pool.get('payment.order')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.set_unique_payment_varconf(cursor, uid, 1)

            def setup_invoices(invoice_refs):
                invoice_ids = [imd_obj.get_object_reference(cursor, uid, 'account', invoice_ref)[1] for invoice_ref in invoice_refs]
                self.invoice_add_products_and_open(cursor, uid, invoice_ids)
                return invoice_ids

            def pay_invoices(invoice_ids, payment_order_id):
                invoice_obj.afegeix_a_remesa(
                    cursor, uid, invoice_ids, payment_order_id
                )
                self.pay_payment_order(cursor, uid, payment_order_id)

            def pay_and_unpay_invoice(invoice_ref):
                invoice_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', invoice_ref
                )[1]

                remesa = self.setup_payment_order(cursor, uid, 'receivable')
                remesa_vals = payment_order_obj.copy_data(cursor, uid, remesa.id)[0]
                remesa_vals.update({'reference': '0001-00000000000000000000000002'})
                remesa_2_id = payment_order_obj.create(cursor, uid, remesa_vals)

                self.invoice_add_products_and_open(cursor, uid, [invoice_id])

                pay_invoices([invoice_id], remesa.id)

                invoice = invoice_obj.browse(cursor, uid, invoice_id)
                self.unpay_invoice(cursor, uid, invoice)
                return remesa_2_id

            def check_unpayed_invoice(unpayed_invoice_id):
                invoice_001 = invoice_obj.browse(
                    cursor, uid, unpayed_invoice_id
                )
                account_move_line_id = invoice_001.move_line_id_payment_get()[0]

                account_move_line = account_move_line_obj.browse(
                    cursor, uid, account_move_line_id
                )

                self.assertFalse(account_move_line.reconcile_partial_id, 0)
                self.assertEqual(len(account_move_line.reconcile_id.line_id), 4)

            def check_balance(payment_order_id):
                # Search for the account_moves associated with the
                # payment_order.
                payment_order = payment_order_obj.browse(
                    cursor, uid, payment_order_id
                )
                dmn = [('ref', '=', payment_order.reference)]
                account_move_id = move_obj.search(cursor, uid, dmn)

                # Fetch the account move lines of the account move
                move_line_f = ['credit', 'debit']
                dmn = [('move_id', '=', account_move_id[0])]
                move_lines = account_move_line_obj.q(cursor, uid).read(
                    move_line_f).where(dmn)

                # There must be 3 move lines: one for each invoice and one that
                # balances the move.
                self.assertEquals(len(move_lines), 3)

                # Check if effectively the lines are balanced
                balance = 0
                for move_line in move_lines:
                    balance += move_line['debit'] - move_line['credit']

                self.assertEquals(round(balance, 6), 0)

            inv_ids = setup_invoices(['invoice_0001', 'invoice_0002'])
            new_payment_order_id = pay_and_unpay_invoice('invoice_0001')
            pay_invoices(inv_ids, new_payment_order_id)

            check_unpayed_invoice(inv_ids[0])
            check_balance(new_payment_order_id)
