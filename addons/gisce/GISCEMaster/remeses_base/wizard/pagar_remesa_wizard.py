# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
import pooler
import netsvc
from oorq.oorq import ProgressJobsPool
from autoworker import AutoWorker

_STATES = [
    ('init', 'init'),
    ('running', 'running'),
    ('end', 'end'),
]

class WizardPagarRemesa(osv.osv_memory):
    """Wizard per donar per pagada una remesa
    """
    _name = 'pagar.remesa.wizard'

    def _get_period(self, cursor, uid, context=None):
        if context is None:
            context = {}
        pay_order = context.get('active_id', False)
        if pay_order:
            query = self.pool.get('payment.order').q(
                cursor, uid
            ).select(['date_planned', 'date_prefered']).where([('id', '=', pay_order)])
            cursor.execute(*query)
            res = cursor.dictfetchone()
            if res['date_prefered'] == 'fixed':
                return self.pool.get('account.period').find(cursor, uid, res['date_planned'])[0]
        return False

    def action_pagar_remesa(self, cursor, uid, ids, context=None):
        import threading
        wiz = self.browse(cursor, uid, ids, context)[0]
        wiz.write({'state': 'running'})
        print_thread = threading.Thread(
            target=self.action_pagar_remesa_threaded_try,
            args=(cursor.dbname, uid, ids, context)
        )
        print_thread.start()
        return True

    def action_pagar_remesa_threaded_try(self, dbname, uid, ids, context=None):
        """Acció per pagar la remesa
        """
        try:
            return self.action_pagar_remesa_threaded(dbname, uid, ids, context=context)
        except Exception, e:
            cursor = pooler.get_db(dbname).cursor()
            self.write(cursor, uid, ids, {'state': 'end', 'errors': "ERROR: "+ e.message})

    def action_pagar_remesa_threaded(self, dbname, uid, ids, context=None):
        """Acció per pagar la remesa
        """
        payment_order_o = self.pool.get('payment.order')
        payment_line_o = self.pool.get('payment.line')
        invoice_o = self.pool.get('account.invoice')

        if not context:
            context = {}
        cursor = pooler.get_db(dbname).cursor()

        db = pooler.get_db_only(cursor.dbname)

        conf_o = self.pool.get('res.config')
        payment_order_unique_account_move = bool(int(conf_o.get(
            cursor, uid, 'payment_order_unique_account_move', '0'
        )))

        wiz = self.browse(cursor, uid, ids, context)[0]
        context['work_async'] = wiz.work_async
        period_id = wiz.period_id.id

        payment_order_ids = context.get('active_ids', False)

        errors = ''

        logger = netsvc.Logger()
        if not payment_order_unique_account_move:

            if payment_order_ids:
                payment_order_id = payment_order_ids[0]
            else:
                raise osv.except_osv(
                    'Error',
                    "S'ha de cridar aquest wizard des d'una remesa.",
                    'error'
                )

            order = payment_order_o.browse(
                cursor, uid, payment_order_id, context=context
            )

            journal_id = order.mode.journal.id
            pay_account_id = order.mode.journal.default_credit_account_id.id

            context.update({'type': 'out_invoice'})
            if not context.get("work_async"):
                total = len(order.line_ids)
                count = 0.0
                for line in order.line_ids:
                    tmp_cr = db.cursor()
                    try:
                        payment_order_o.pay_and_reconcile(
                            tmp_cr, uid, line.id,
                            pay_account_id, period_id, journal_id, False, period_id,
                            False, context, order.name
                        )
                        tmp_cr.commit()
                    except Exception, exc:
                        logger.notifyChannel("objects", netsvc.LOG_ERROR, exc)
                        number = line.ml_inv_ref.number
                        errors += _('payment line id {0}, number {1}: {2}\n').format(
                            line.id, number, exc
                        )
                        tmp_cr.rollback()
                    finally:
                        tmp_cr.close()
                        count += 1
                        wiz.write({
                            'progress_1': (count/total)*100,
                            'progress_2': (count/total)*100
                        })
            else:
                self.pool.get("account.invoice").check_account_sequence_standard_configured(cursor, uid, context=context)
                j_pool = ProgressJobsPool(wiz, 'progress_2', "openerp.pay_and_reconcile.progress")
                amax_proc = int(self.pool.get("res.config").get(cursor, uid, "pay_and_reconcile_tasks_max_procs", "0"))
                if not amax_proc:
                    amax_proc = None
                aw = AutoWorker(queue="pay_and_reconcile_tasks", default_result_ttl=24 * 3600, max_procs=amax_proc)
                for line in order.line_ids:
                    j_pool.add_job(
                        payment_order_o.pay_and_reconcile_async(
                            cursor, uid, line.id,
                            pay_account_id, period_id, journal_id, False,
                            period_id,
                            False, context, order.name
                    ))
                aw.work()
                j_pool.join()
                if len(j_pool.failed_jobs):
                    errors = _(u"No s'han pogut pagar totes les factures. Revisa les factures que no s'han pogut pagar.")
                wiz.write({
                    'progress_1': 100,
                    'progress_2': 100
                })
        else:
            tmp_cursor = db.cursor()
            try:
                context['jpool_data'] = {
                    'progress_obj': 'pagar.remesa.wizard',
                    'progress_obj_id': wiz.id,
                    'progress_field_mlines': 'progress_1',
                    'progress_field_reconcile': 'progress_2',
                }
                failed_ids = payment_order_o.pay_and_reconcile_multiple(
                    tmp_cursor, uid, payment_order_ids, False, period_id, False,
                    period_id, context=context
                )
                if len(failed_ids):
                    errors += _("An error ocurred during the payments. All account moves have been cleaned. Check the failed lines and start the process again.\n")
                for failed in failed_ids:
                    errors += _('* Payment line id {0} failed. Check the invoice.\n').format(failed)
                tmp_cursor.commit()
                tmp_cursor.close()
            except Exception, exc:
                logger.notifyChannel("objects", netsvc.LOG_ERROR, exc)
                errors = exc.message
                tmp_cursor.rollback()
                tmp_cursor.close()

        unpayed_lines = payment_line_o.search(
            cursor, uid,
            [('order_id', '=', payment_order_ids[0]), ('payment_move_id', '=', None)]
        )
        if len(unpayed_lines):
            errors = _("Hi ha hagut problemes pagant les linies amb ids: {0}.\n\n").format(unpayed_lines) + errors
        if not errors:
            errors = _('No hi han hagut errors durant el procés\n')
            payment_order_o.write(
                cursor, uid, payment_order_ids, {'paid': True}, context=context
            )

        wiz.write({'state': 'end', 'errors': errors})
        cursor.commit()
        cursor.close()

    _columns = {
        'state': fields.selection(_STATES),
        'period_id': fields.many2one('account.period', 'Periodo',
                                     required=True),
        'errors': fields.text('Errors'),
        'progress_1': fields.float("Assentaments generats"),
        'progress_2': fields.float("Factures conciliades"),
        'work_async': fields.boolean("Treballa en paral.lel"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'period_id': _get_period,
        'work_async': lambda *a: True
    }


WizardPagarRemesa()
