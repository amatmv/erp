# -*- coding: utf-8 -*-

"""Wizard per crear remeses des de lot de facturació
"""
SAFE_CONSTANTS = {'None': None, 'True': True, 'False': False}

try:
    from ast import literal_eval
except ImportError:
    import _ast as ast

    def _convert(node):
        if isinstance(node, ast.Str):
            return node.s
        elif isinstance(node, ast.Num):
            return node.n
        elif isinstance(node, ast.Tuple):
            return tuple(map(_convert, node.elts))
        elif isinstance(node, ast.List):
            return list(map(_convert, node.elts))
        elif isinstance(node, ast.Dict):
            return dict((_convert(k), _convert(v)) for k, v
                        in zip(node.keys, node.values))
        elif isinstance(node, ast.Name):
            if node.id in SAFE_CONSTANTS:
                return SAFE_CONSTANTS[node.id]
        raise ValueError('malformed or disallowed expression')

    def parse(expr, filename='<unknown>', mode='eval'):
        """parse(source[, filename], mode]] -> code object
        Parse an expression into an AST node.
        Equivalent to compile(expr, filename, mode, PyCF_ONLY_AST).
        """
        return compile(expr, filename, mode, ast.PyCF_ONLY_AST)

    def literal_eval(node_or_string):
        """literal_eval(expression) -> value
        Safely evaluate an expression node or a string containing a Python
        expression.  The string or node provided may only consist of the
        following Python literal structures: strings, numbers, tuples,
        lists, dicts, booleans, and None.

        >>> literal_eval('[1,True,"spam"]')
        [1, True, 'spam']

        >>> literal_eval('1+3')
        Traceback (most recent call last):
        ...
        ValueError: malformed or disallowed expression
        """
        if isinstance(node_or_string, basestring):
            node_or_string = parse(node_or_string)
        if isinstance(node_or_string, ast.Expression):
            node_or_string = node_or_string.body
        return _convert(node_or_string)

from osv import fields, osv
from tools.translate import _

_STATES = [
    ('init', 'Init'),
    ('resum', 'Resum'),
    ('end', 'Final'),
]

class WizardSeleccionarFacturesRemesa(osv.osv_memory):
    """Classe del wizard
    """
    _name = 'wizard.seleccionar.factures.remesa'
    _description = __doc__

    def action_buscar_factures(self, cursor, uid, ids, context=None):
        """Mètode que cerca les factures
        """
        if not context:
            context = {}

        inv_obj = self.pool.get('account.invoice')
        ml_obj = self.pool.get('account.move.line')
        order_obj = self.pool.get('payment.order')
        order_id = context.get('active_ids', False)
        if order_id:
            order_id = order_id[0]
        else:
            raise osv.except_osv('Error', "S'ha de cridar aquest wizard des "
                                          "d'una remesa.", 'error')
        order = order_obj.browse(cursor, uid, order_id, context)
        wiz = self.browse(cursor, uid, ids, context)[0]
        search_params = [
            ('residual', '<>', 0),
            ('reconciled', '=', False),
            ('journal_id', '=', wiz.journal.id),
        ]
        if wiz.use_order_payment_type:
            search_params.append(('payment_type.id', '=', order.mode.type.id))
        if wiz.s_date_from:
            search_params.append(('date_invoice', '>=', wiz.s_date_from))
        if wiz.s_date_to:
            search_params.append(('date_invoice', '<=', wiz.s_date_to))

        inv_ids = inv_obj.search(cursor, uid, search_params)
        ml_ids = []
        for invoice in inv_obj.browse(cursor, uid, inv_ids, context=context):
            if invoice.group_move_id:
                ref = invoice.group_move_id.ref
                for move_line in invoice.group_move_id.line_id:
                    if move_line.name == ref:
                        ml_ids.append(move_line.id)
            else:
                ml_ids.extend(inv_obj.move_line_id_payment_get(cursor, uid,
                                                               [invoice.id]))
        total = 0
        n_fac = 0
        for mli in ml_obj.read(cursor, uid, ml_ids, ['amount_to_pay']):
            total += mli['amount_to_pay']
            if mli['amount_to_pay']:
                n_fac += 1

        msg = u"Se creará una remesa de %s facturas que " % n_fac
        msg += u"corresponden a un total de %s euros" % total
        wiz.write({'msg': msg, 'state': 'resum', 'fids': str(inv_ids)})

    def action_afegir_factures(self, cursor, uid, ids, context=None):
        """Mètode per afegir les factures trobades a la remesa
        """
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids, context)[0]
        inv_obj = self.pool.get('account.invoice')
        inv_ids = literal_eval(wiz.fids)

        order_id = context.get('active_ids', False)
        if order_id:
            order_id = order_id[0]
        else:
            raise osv.except_osv('Error', "S'ha de cridar aquest wizard des "
                                          "d'una remesa.", 'error')
        inv_obj.afegeix_a_remesa(cursor, uid, inv_ids, order_id, context)
        wiz.write({'state': 'end'})

    _columns = {
        's_date_from': fields.date('Fecha desde', required=True),
        's_date_to': fields.date('Fecha hasta', required=True),
        'use_order_payment_type': fields.boolean(
            "Utilitzar el tipus de pagament de l'ordre",
            help=u"Força a seleccionar només les factures que coincideixin "
                 u"amb el tipus de pagament de l'ordre"
        ),
        'msg': fields.text('Missatge'),
        'fids': fields.text('Factures IDS'),
        'state': fields.selection(_STATES, 'Estado', required=True),
        'journal': fields.many2one('account.journal', 'Diario de facturación',
                                   required=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'use_order_payment_type': lambda *a: 1
    }

WizardSeleccionarFacturesRemesa()

