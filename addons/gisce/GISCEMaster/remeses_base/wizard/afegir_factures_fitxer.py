# -*- coding: utf-8 -*-

from osv import fields, osv
import base64
import csv
from tools.translate import _
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO


class WizardAfegirFacturesFitxer(osv.osv_memory):
    """Wizard per afegir factures a una remesa des d'un fitxer"""
    _name = 'wizard.afegir.factures.fitxer'

    def _default_info(self, cursor, uid, context=None):
        informacio = _(u"Es carregaràn les factures des d'un fitxer a la remesa"
                       u" sel·leccionada.\n"
                       u"El format del fitxer ha de ser CSV amb delimitador "
                       u"';', UTF-8 i una sola columna amb el numero "
                       u"de factura.")
        return informacio

    def seguent(self, cursor, uid, ids, context=None):
        vals = {
            'state': 'load'
        }
        self.write(cursor, uid, ids, vals, context)

    def anterior(self, cursor, uid, ids, context=None):
        vals = {
            'state': 'init'
        }
        self.write(cursor, uid, ids, vals, context)

    def generarEstadistiques(self, done, notDone, notDoneList, lines):
        res = u""
        res += _(u"Total fitxer: %s\n") % str(lines)
        res += _(u"Total afegides: %s\n") % str(done)
        res += _(u"Total no afegides: %s\n\n") % str(notDone)
        if notDone > 0:
            res += _(u"Llistat no afegides: \n\n")
            for elem in notDoneList:
                res += _(u"\t- %s\n") % str(elem)
        return res

    def importar(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context)
        try:
            if wiz.file_fact:
                inv_obj = self.pool.get('account.invoice')
                done = 0
                notDone = 0
                notDoneList = []
                fitxer = wiz.file_fact
                txt = base64.decodestring(str(fitxer))
                csv_file = StringIO.StringIO(txt)
                reader = csv.reader(csv_file, delimiter='\t')
                remesa_id = context['active_id']
                counter = 0
                for row in reader:
                    numFactura = row[0].split(';')[0]
                    # Obtenir ids de account.invoice
                    inv_ids = inv_obj.search(cursor, uid, [('number', '=', numFactura)])
                    model_dict = inv_obj.get_factura_ids_from_invoice_id(cursor, uid, inv_ids)
                    for model, (model_ids, model_obj) in model_dict.items():
                        if model_ids:
                            res = model_obj.afegeix_a_remesa(cursor, uid, model_ids, remesa_id)
                            if res:
                                done += 1
                            else:
                                notDoneList.append(numFactura)
                                notDone += 1
                        else:
                            notDoneList.append(numFactura)
                            notDone += 1
                    counter += 1
                res = self.generarEstadistiques(done, notDone, notDoneList,
                                                counter)
                vals = {
                    'state': 'finish',
                    'info': _(u"La importació de factures ha finalitzat "
                              u"correctament.\n\n%s") % str(res)
                }
                self.write(cursor, uid, ids, vals, context)
        except Exception, e:
            raise osv.except_osv(_(u"Atenció"),
                                 _(u"Error llegint el fitxer de factures: \n\n"
                                   u"%s\n\n"
                                   u"Aquest format no és correcte")
                                 % str(e))
        finally:
            csv_file.close()

    _columns = {
        'state': fields.char('Estat', size=16),
        'info': fields.text('Informació', readonly=True),
        'file_fact': fields.binary('Fitxer')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info
    }

WizardAfegirFacturesFitxer()
