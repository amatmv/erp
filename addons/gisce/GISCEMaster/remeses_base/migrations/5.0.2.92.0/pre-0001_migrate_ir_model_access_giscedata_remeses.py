# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Updating access rules from giscedata_remeses and setting them to 
        remeses_base module.
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module='remeses_base'
        WHERE module = 'giscedata_remeses' 
        AND name IN (
            'model_payment_order_history', 
            'model_wizard_afegir_factures_fitxer', 
            'model_pagar_remesa_wizard',
            'model_wizard_seleccionar_factures_remesa'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up
