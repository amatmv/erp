# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_ot_comptadors_base_data
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-01-21 11:07\n"
"PO-Revision-Date: 2019-01-21 11:07\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_cctg
msgid "Canvi a Contador TG"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_ots
msgid "Ordre de Tall de Suministre"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:ir.module.module,description:giscedata_ot_comptadors_base_data.module_meta_information
msgid "Crea seccions i codis de tancament per Ordres de Treball de comptadors."
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_cc
msgid "Canvi de Comercialitzadora"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_od
msgid "Ordres de Desconexió - Baixa Suministre"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_or
msgid "Ordre de Reposició"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_oc
msgid "Ordres de Connexió"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: constraint:crm.case.section:0
msgid "Error ! You cannot create recursive sections."
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_riam
msgid "Revisió de Instalacións i Aparells de Mesura"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:ir.module.module,shortdesc:giscedata_ot_comptadors_base_data.module_meta_information
msgid "GISCE Ordres de Treball"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_ct
msgid "Canvi de Titular"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_ipf
msgid "Informe de Possible Frau"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_sam
msgid "Substitució de Aparells de Mesura"
msgstr ""

#. module: giscedata_ot_comptadors_base_data
#: model:crm.case.section,name:giscedata_ot_comptadors_base_data.ot_arp
msgid "Ampliació o Reducció de Poténcia"
msgstr ""

