# -*- coding: utf-8 -*-
{
    "name": "GISCE Ordres de Treball",
    "description": """Crea seccions i codis de tancament per Ordres de Treball de comptadors.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_ot_comptadors",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_ot_data.xml"
    ],
    "active": False,
    "installable": True
}
