# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataOt(osv.osv):
    _name = 'giscedata.ot'
    _inherit = 'giscedata.ot'

    DEFAULT_CLOSE_CODE = {
        'OC': 'OC',
        'OD': 'OD',
        'SAM': 'SAM',
        'RIAM': 'RIAM',
        'CT': 'CT',
        'CC': 'CC',
        'OTS': 'OTS',
        'OR': 'OR',
        'IPF': 'IPF',
        'ARP': 'ARP',
        'CCTG': 'CCTG'
    }

GiscedataOt()
