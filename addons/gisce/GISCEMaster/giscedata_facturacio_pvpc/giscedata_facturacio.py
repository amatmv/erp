# -*- coding: utf-8 -*-
from osv import osv
from ast import literal_eval
import csv
import base64
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
from libfacturacioatr.pool.pvpc import *
from tools import config
from tools.translate import _


TARIFFS_FACT = {
    '2.0A': Tarifa20APoolPVPC,
    '2.0DHA': Tarifa20DHAPoolPVPC,
    '2.1A': Tarifa21APoolPVPC,
    '2.1DHA': Tarifa21DHAPoolPVPC
}


class GiscedataFacturacioFacturador(osv.osv):

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def crear_linia(self, cursor, uid, factura_id, vals, context=None):
        """Funció base per crear la línia de factura.
        """
        if vals.get('tipus') == 'energia':
            factura_obj = self.pool.get('giscedata.facturacio.factura')
            polissa_obj = self.pool.get('giscedata.polissa')
            if context is None:
                context = {}
            # Gess if this is a PVPC invoice
            factura = factura_obj.browse(
                cursor, uid, factura_id, context=context
            )
            polissa_id = factura.polissa_id.id
            ctx = context.copy()
            ctx['date'] = factura.data_inici
            polissa = polissa_obj.browse(cursor, uid, polissa_id, context=ctx)
            if polissa.mode_facturacio == 'pvpc':
                context['pricelist_base_price'] = vals.pop('force_price', None)

        return super(GiscedataFacturacioFacturador, self).crear_linia(
            cursor, uid, factura_id, vals, context=context
        )

    def get_tarifa_class(self, modcontractual):
        parent = super(GiscedataFacturacioFacturador, self).get_tarifa_class
        if modcontractual.mode_facturacio == 'pvpc':
            tariff_class = TARIFFS_FACT.get(modcontractual.tarifa.name, None)
            if not tariff_class:
                tariff_name = modcontractual.tarifa.name
                raise ValueError(
                    _('La tarifa {0} no es pot facturar per PVPC').format(
                        tariff_name)
                )
            return tariff_class
        else:
            return parent(modcontractual)

    def versions_de_preus(self, cursor, uid, polissa_id, data_inici,
                          data_final, context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        pricelist_obj = self.pool.get('product.pricelist')
        imd_obj = self.pool.get('ir.model.data')

        # Fem un browse amb la data final per obtenir quina tarifa té
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context={
            'date': data_final
        })

        if polissa.mode_facturacio != 'pvpc':
            res = super(GiscedataFacturacioFacturador, self).versions_de_preus(
                cursor, uid, polissa_id, data_inici, data_final, context
            )
        else:
            polissa_intervals = polissa_obj.get_modcontractual_intervals(
                cursor, uid, polissa_id, data_inici, data_final, context=context
            )
            # We have to get prices for every contract interval
            res = {}
            for interval, dades in polissa_intervals.items():
                inici = max(dades['dates'][0], data_inici)
                fi = min(dades['dates'][1], data_final)
                prices = super(
                    GiscedataFacturacioFacturador,
                    self
                ).versions_de_preus(
                    cursor, uid, polissa_id, inici, fi, context
                )
                res.update(prices)

            for date_version in res:
                res[date_version] = {}

        return res

    def config_facturador(self, cursor, uid, facturador, polissa_id,
                          context=None):
        """Gets audit data params from res.config `fact_indexed_audit_fields`
        :param facturador: Tariff Class
        :return: True
        """
        super(GiscedataFacturacioFacturador, self).config_facturador(
            cursor, uid, facturador, polissa_id, context=context
        )

        if not isinstance(facturador, TarifaPoolPVPC):
            return True

        import logging
        logger = logging.getLogger('openerp.pvpc')

        conf_obj = self.pool.get('res.config')
        str_val = conf_obj.get(cursor, uid, 'fact_pvpc_audit_fields', '[]')
        try:
            params = list(eval(str_val))
        except (NameError, TypeError) as e:
            # not a list
            params = []
            logger.info(
                u"Bad 'fact_pvpc_audit_fields' param. "
                u"Must be a list: {0}".format(str_val)
            )

        audit_keys = [
            key for key in params
            if key in facturador.get_available_audit_coefs().keys()
        ]
        facturador.conf['audit'] = audit_keys

        logger.info(u"Audit fields: {0}".format(facturador.conf['audit']))

        return True

GiscedataFacturacioFacturador()
