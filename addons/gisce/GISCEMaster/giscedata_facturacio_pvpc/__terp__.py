# -*- coding: utf-8 -*-
{
    "name": "Facturació indexada amb PVPC",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Facturació indexada amb PVPC
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_facturacio_indexada"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_facturacio_pvpc_demo.xml"
    ],
    "update_xml":[
        "giscedata_facturacio_pvpc_data.xml",
        "giscedata_polissa_data.xml",
    ],
    "active": False,
    "installable": True
}
