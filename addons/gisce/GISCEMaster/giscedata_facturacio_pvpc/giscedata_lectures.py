# coding=utf-8
from datetime import timedelta
import logging
try:
    import cPickle as pickle
except ImportError:
    import pickle

from osv import osv
from enerdata.datetime.timezone import TIMEZONE
from enerdata.profiles.profile import Coefficients
from giscedata_facturacio_indexada.giscedata_lectures import INVOICE_WITH_CURVE
from .utils import ESIOS_PROFILE_MAP
from oorq.oorq import setup_redis_connection


INVOICE_WITH_CURVE += ['pvpc']

logger = logging.getLogger('openerp.' + __name__)


class GiscedataLecturesComptador(osv.osv):
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_cofs(self, polissa, start, end):
        if polissa.mode_facturacio == 'pvpc':
            c = Coefficients()
            redis_conn = setup_redis_connection()
            Profile = ESIOS_PROFILE_MAP[polissa.tarifa.name]
            start = TIMEZONE.localize(start)
            end = TIMEZONE.localize(end + timedelta(hours=23))
            key = '{} {} {}'.format(polissa.tarifa.name, start, end)
            cofs = redis_conn.get(key)
            if cofs:
                cofs = pickle.loads(cofs)
            else:
                logger.debug('cofs not in cache for key: {}'.format(key))
                cofs = Profile.get_range(start, end)
                logger.debug('setting cofs to cache fr key: {}'.format(key))
                redis_conn.set(key, pickle.dumps(cofs), 3600)
            c.insert_coefs(cofs)
            return c
        else:
            return super(GiscedataLecturesComptador, self).get_cofs(
                polissa, start, end
            )

GiscedataLecturesComptador()
