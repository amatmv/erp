# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from dateutil import parser
from enerdata.profiles.profile import Coefficent
from enerdata.datetime.timezone import TIMEZONE
from esios import Esios

from tools import config

ESIOS_TOKEN = config.get('esios_token', '')


def localize_esios_dt(dt):
    assert isinstance(dt, datetime)
    d = datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second)
    if dt.utcoffset().seconds == 7200:
        dst = True
    elif dt.utcoffset().seconds == 3600:
        dst = False
    else:
        raise Exception('Not a valid offset')
    return TIMEZONE.localize(d, is_dst=dst)


class EsiosProfile(object):

    @classmethod
    def get_range(cls, start, end):
        cofs = []
        e = Esios(ESIOS_TOKEN)
        method = getattr(e, cls.method)
        profs = method().get(start, end)
        for prof in profs['indicator']['values']:
            dt = localize_esios_dt(parser.parse(prof['datetime']))
            # Add one hour due coeficients are for the previous our
            dt += timedelta(hours=1)
            cofs.append(Coefficent(
                TIMEZONE.normalize(dt), dict(
                    (k, float(prof['value'])) for i, k in enumerate('ABCD', 5)
                ))
            )
        return cofs


class EsiosProfile20A(EsiosProfile):
    method = 'profile_pvpc_20A'


class EsiosProfile20DHA(EsiosProfile):
    method = 'profile_pvpc_20DHA'


class EsiosProfile20DHS(EsiosProfile):
    method = 'profile_pvpc_20DHS'


ESIOS_PROFILE_MAP = {
    '2.0A': EsiosProfile20A,
    '2.1A': EsiosProfile20A,
    '2.0DHA': EsiosProfile20DHA,
    '2.1DHA': EsiosProfile20DHA,
    '2.0DHS': EsiosProfile20DHS,
    '2.1DHS': EsiosProfile20DHS
}
