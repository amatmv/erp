# coding=utf-8
from destral import testing


class TestModeFacturacioPVPC(testing.OOTestCaseWithCursor):

    def test_polissa_mode_facturacio_selection(self):

        cursor = self.cursor
        uid = self.uid

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        f = pol_obj.fields_get(cursor, uid, ['mode_facturacio'])
        self.assertIn(
            ('pvpc', 'PVPC'),
            f['mode_facturacio']['selection'],
        )

        mc_obj = self.openerp.pool.get('giscedata.polissa.modcontractual')
        f = mc_obj.fields_get(cursor, uid, ['mode_facturacio'])
        self.assertIn(
            ('pvpc', 'PVPC'),
            f['mode_facturacio']['selection'],
        )
