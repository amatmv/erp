# -*- coding: utf-8 -*-
import base64
import hashlib
import logging
import os
import re
import pooler
from StringIO import StringIO
from facturae.facturae_parser import FacturaeParser
from lxml import etree
from oorq.decorators import job
from tools.translate import _
from osv import osv, fields

_states_selection = [
        ('erroni', 'Error en la importació'),
        ('valid', 'Importat correctament'),
        ('incident', 'Incidents en la importació'),
    ]

TABLA_102 = [
    ('A', u'Anuladora'),
    ('N', u'Normal'),
    ('R', u'Rectificadora'),
    ('C', u'Complementaria'),
    ('G', u'Regularizadora'),
]

invoice_types = {
    'OO': 'N',
    'OR': 'R',
    'OC': 'G',
}

facturae_version_xsd = {
    'v3.1': 'Facturaev3_1.xsd',
    'v3.2': 'Facturaev3_2.xsd',
    'v3.2.1': 'Facturaev3_2_1.xsd',
    'v3.2.2': 'Facturaev3_2_2.xsd',
}

def get_md5(data):
    cleaned_data = data.replace(" ", "")
    cleaned_data = cleaned_data.replace("\n", "")
    cleaned_data = cleaned_data.replace("\t", "")
    md5 = hashlib.md5(cleaned_data)
    return md5.hexdigest()


class AccountImportFacturae(osv.osv):
    """Agrupació d'importacions"""

    _name = 'account.import.facturae'
    _description = "Agrupació d'importacions"

    def _ff_data_carrega(self, cursor, uid, ids, field_name, arg, context=None):

        """Data de càrrega del fitxer"""

        if not context:
            context = {}

        dates = self.perm_read(cursor, uid, ids)
        res = dict([(d['id'], d['create_date'].split('.')[0]) for d in dates])

        return res

    def _ff_check_status(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = {}

        line_obj = self.pool.get('account.import.facturae.line')
        line_facte_obj = self.pool.get('account.import.facturae.line.facturae')
        invoice_obj = self.pool.get('account.invoice')

        for i_id in ids:
            # Ids of the lines in the import
            lines = line_obj.search(cursor, uid, [('importacio_id', '=', i_id)], context=context)

            # Total number of lines in the import
            n_linies = len(lines)

            # Total number of already processed lines
            n_linies_proc = line_obj.search_count(cursor, uid, [('importacio_id', '=', i_id),
                                                                ('state', '!=', False)])

            # Total number of correct lines
            n_linies_ok = line_obj.search_count(cursor, uid, [('importacio_id', '=', i_id),
                                                              ('state', '=', 'valid')])

            line_facte_ids = line_facte_obj.search(cursor, uid, [('linia_id', 'in', lines)])

            # Total number of created invoices
            fact_count = len(line_facte_ids)

            linfact_data = line_facte_obj.read(cursor, uid, line_facte_ids, ['invoice_id'])

            fact_ids = [x['invoice_id'][0] for x in linfact_data]

            fact_data = invoice_obj.read(cursor, uid, fact_ids, ['rectificative_type'])

            resum = {}

            for fact in fact_data:
                tipus_rect = fact['rectificative_type']
                if not resum.get(tipus_rect, False):
                    resum[tipus_rect] = 0

                resum[tipus_rect] += 1

            # Create resum text
            digest_text = ''
            types_dict = dict(TABLA_102)
            for key, value in resum.items():
                title = types_dict.get(key[0], key[0])
                digest_text += '{0}: {1}\n'.format(title, value)

            complet = (n_linies_ok == n_linies)

            if n_linies:
                progress = float(n_linies_proc)/n_linies * 100
                progress_importation = float(n_linies_ok)/n_linies * 100

            else:
                # If we don't have any line to import we have imported all
                progress = 100
                progress_importation = 100

            res[i_id] = {
                'importat_ok': complet,
                'num_xml': n_linies,
                'num_fact_creades': fact_count,
                'info': u"{0} XML importats, {1} factures creades".format(
                    n_linies_proc, fact_count
                ),
                'digest': digest_text,
                'progres': progress,
                'progres_importation': progress_importation,
            }
        return res

    def importat_ok_search(self, cursor, uid, obj, name, args, context=None):
        cursor.execute("SELECT DISTINCT importacio_id from account_import_facturae_line where state != 'valid'")
        incorrect_ids = cursor.fetchall()
        incorrect_ids = [x[0] for x in incorrect_ids]
        operator = 'not in' if args[0][2] == 1 else 'in'
        return [('id', operator, incorrect_ids)]

    def process_import(self, cursor, uid, import_id, context=None):
        if context is None:
            context = {}

        line_obj = self.pool.get('account.import.facturae.line')
        search_params = [('importacio_id', '=', import_id)]

        if context.get('import_errors', False):
            search_params += [
                '|',
                ('state', '=', False),
                ('state', '=', 'erroni')
            ]

        line_ids = line_obj.search(cursor, uid, search_params)

        db = pooler.get_db_only(cursor.dbname)

        try:
            tmp_cr = db.cursor()
            line_obj.process_line(tmp_cr, uid, line_ids, context=context)
            tmp_cr.commit()
        except Exception, e:
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
            tmp_cr.rollback()
        finally:
            tmp_cr.close()

    _columns = {
        'name': fields.char('Nom Fitxer', size=1024, required=True),

        'info': fields.function(_ff_check_status, string='Informació',
                                type='text', multi='proc', method=True),

        'digest': fields.function(_ff_check_status, string='Resum',
                                  type='text', multi='proc', method=True),

        'linia_ids': fields.one2many('account.import.facturae.line', 'importacio_id',
                                     'Linies', ondelete='cascade'),

        'progres': fields.function(_ff_check_status, string='Progrés',
                                   readonly=True, method=True,
                                   type='float', multi='proc'),

        'progres_importation': fields.function(_ff_check_status, string='Finalitzats',
                                               readonly=True, method=True,
                                               type='float', multi='proc'),

        'importat_ok': fields.function(_ff_check_status, method=True,
                                       string="Sense errors", type='boolean',
                                       multi='proc', fnct_search=importat_ok_search),

        'num_xml': fields.function(_ff_check_status, method=True,
                                   string="Total XMLs", type='integer',
                                   multi='proc'),

        'num_fact_creades': fields.function(_ff_check_status, method=True,
                                            string="Total factures creades",
                                            type='integer', multi='proc'),

        'data_carrega': fields.function(_ff_data_carrega, type='datetime',
                                        string='Data Càrrega', method=True)
    }


AccountImportFacturae()


class AccountImportFacturaeLine(osv.osv):
    """Línia d'importació"""

    _name = 'account.import.facturae.line'
    _description = "Llistat de fitxers importats"

    def _ff_data_carrega(self, cursor, uid, ids, field_name, arg, context=None):

        """Data de càrrega del fitxer"""

        if not context:
            context = {}

        dates = self.perm_read(cursor, uid, ids)
        res = dict([(d['id'], d['create_date'].split('.')[0]) for d in dates])

        return res

    def _data_carrega_search(self, cursor, uid, ids, field, arg, context=None):
        """Cerca segons data creació"""
        if not context:
            context = {}

        search_vals = [('create_date', a[1], a[2]) for a in arg]
        trobats = self.search(cursor, uid, search_vals)

        return [('id', 'in', trobats)]

    def _ff_check_factures(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}

        for linia_id in ids:

            linia_facturae = self.read(cursor, uid, linia_id, ['linia_facturae_id'], context)

            existeix = False
            if len(linia_facturae.get('linia_facturae_id')) > 0:
                existeix = True

            res[linia_id] = existeix

        return res

    def create_from_xml(self, cursor, uid, import_id, journal_id, name, xml_data, context=None):
        if context is None:
            context = {}

        vals = {
            'importacio_id': import_id,
            'journal_id': journal_id.id,
            'name': name
        }

        line_id = self.create(cursor, uid, vals)

        is_valid_xml, version = self.validate_xml(cursor, uid, xml_data, context=context)

        if not is_valid_xml:
            # This XML is not formatted as XSD specifications
            error_code = '0099'
            self.notify_error(cursor, uid, line_id, error_code, version, context=context)
            self.update_errors(cursor, uid, line_id, context=context)
            self.write(cursor, uid, [line_id], {'state': 'erroni'})
        else:
            header_dict = self.get_header_dict(cursor, uid, xml_data, context=context)
            vals.update(header_dict)
            self.write(cursor, uid, line_id, vals)

        self.adjuntar_xml(cursor, uid, line_id, xml_data, name, context=context)
        return line_id

    def get_header_dict(self, cursor, uid, xml_data, context=None):
        """Returns dict to update line header fields"""

        res = {}
        partner_obj = self.pool.get('res.partner')

        sollicitud = re.findall('<BatchIdentifier>(.*)</BatchIdentifier>', xml_data)
        res['codi_sollicitud'] = sollicitud[0] if sollicitud else False

        invoice_number_text = re.findall('<InvoiceNumber>(.*)</InvoiceNumber>', xml_data)
        res['invoice_number_text'] = invoice_number_text[0] if invoice_number_text else False

        invoice_class = re.findall('<InvoiceClass>(.*)</InvoiceClass>', xml_data)
        invoice_class_code = invoice_class[0] if invoice_class else False
        type_factura = invoice_types[invoice_class_code]
        res['type_factura'] = type_factura or False

        data_factura = re.findall('<IssueDate>(.*)</IssueDate>', xml_data)
        res['data_factura'] = data_factura[0] if data_factura else False

        data_venciment = re.findall('<InstallmentDueDate>(.*)</InstallmentDueDate>', xml_data)
        res['data_venciment'] = data_venciment[0] if data_venciment else False

        xml_temp = xml_data.replace("\n", '')
        seller_data = re.findall('<SellerParty>(.*)</SellerParty>', xml_temp)
        if seller_data:
            seller_id_doc = re.findall('<TaxIdentificationNumber>(.*)</TaxIdentificationNumber>', seller_data[0])[0]
            if seller_id_doc:
                seller_vat = seller_id_doc.replace('-', '')
                if 'ES' not in seller_vat:
                    seller_vat = 'ES' + seller_vat
                seller_id = partner_obj.search(cursor, uid, [('vat', '=', seller_vat)])
            else:
                seller_id = False
            res['emissor'] = seller_id[0] if seller_id else False

        buyer_data = re.findall('<BuyerParty>(.*)</BuyerParty>', xml_temp)
        if buyer_data:
            buyer_id_doc = re.findall('<TaxIdentificationNumber>(.*)</TaxIdentificationNumber>', buyer_data[0])[0]
            if buyer_id_doc:
                buyer_vat = buyer_id_doc.replace('-', '')
                if 'ES' not in buyer_vat:
                    buyer_vat = 'ES' + buyer_vat
                buyer_id = partner_obj.search(cursor, uid, [('vat', '=', buyer_vat)])
            else:
                buyer_id = False
            res['receptor'] = buyer_id[0] if buyer_id else False

        return res

    def adjuntar_xml(self, cursor, uid, line_id, xml_data, fname, context=None):
        """Desa l'xml en l'erp"""

        if not context:
            context = {}

        attach_obj = self.pool.get('ir.attachment')
        md5_hash = get_md5(xml_data)

        desc = _(u"Fitxer adjunt: {0}".format(fname))

        vals = {
            'name': fname,
            'datas': base64.b64encode(xml_data),
            'datas_fname': fname,
            'res_model': 'account.import.facturae.line',
            'res_id': line_id,
            'description': desc,
        }

        attch_id = attach_obj.create(cursor, uid, vals, context=context)

        vals = {
            'md5_hash': md5_hash,
            'attachment_id': attch_id
        }

        self.write(cursor, uid, [line_id], vals)

        return attch_id

    @job(queue='import_xml')
    def process_line(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}

        logger = logging.getLogger('openerp.{}.process_file'.format(__name__))

        if isinstance(line_id, (int, long)):
            line_id = [line_id]

        for line in line_id:
            self.process_line_sync(cursor, uid, line, context=context)
            logger.info('Processed line id {0}'.format(line))

    def process_line_sync(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}

        state = 'erroni'
        res = True

        partner_obj = self.pool.get('res.partner')
        company_obj = self.pool.get('res.company')
        line_facturae_obj = self.pool.get('account.import.facturae.line.facturae')

        try:
            facturae_id = 0
            data = self.get_xml_from_adjunt(cursor, uid, [line_id], context=context)
            self.reset_errors(cursor, uid, line_id, context=None)
            is_valid_xml, version = self.validate_xml(cursor, uid, data, context=context)

            if not is_valid_xml:
                # This XML is not formatted as XSD specifications
                error_code = '0001'
                self.notify_error(cursor, uid, line_id, error_code, version,  context=context)
            else:
                my_company = company_obj.read(cursor, uid, 1, ['partner_id'])['partner_id']

                if data:

                    facturae_parser = FacturaeParser(data)

                    seller_vat = facturae_parser.seller['TaxIdentificationNumber'].replace('-', '')
                    if 'ES' not in seller_vat:
                        seller_vat = 'ES' + seller_vat
                    seller_id = partner_obj.search(cursor, uid, [('vat', '=', seller_vat)])
                    if seller_id and len(seller_id) > 1:
                        # There are many Providers with the same NIF in the DB
                        error_code = '0002'
                        self.notify_error(cursor, uid, line_id, error_code, seller_vat, context=context)

                    buyer_vat = facturae_parser.buyer['TaxIdentificationNumber'].replace('-', '')
                    if 'ES' not in buyer_vat:
                        buyer_vat = 'ES' + buyer_vat
                    buyer_id = partner_obj.search(cursor, uid, [('vat', '=', buyer_vat)])
                    if buyer_id and len(buyer_id) > 1:
                        # There are many Receivers with the same NIF in the DB
                        error_code = '0003'
                        self.notify_error(cursor, uid, line_id, error_code, buyer_vat, context=context)

                    issuer_type = facturae_parser.issuer_type
                    emissor = 1
                    receptor = 1

                    if issuer_type == 'EM':
                        if seller_id:
                            emissor = seller_id[0]
                        else:
                            # Error: Provider partner not found
                            error_code = '0004'
                            self.notify_error(cursor, uid, line_id, error_code, seller_vat, context=context)
                        if buyer_id and buyer_id[0] == my_company[0]:
                            receptor = buyer_id[0]
                        else:
                            # This invoice is not directed to our company
                            error_code = '0005'
                            self.notify_error(cursor, uid, line_id, error_code, context=context)

                    elif issuer_type == 'RE':
                        if seller_id and seller_id[0] == my_company[0]:
                            emissor = seller_id[0]
                        else:
                            # This invoice is not directed to our company
                            error_code = '0006'
                            self.notify_error(cursor, uid, line_id, error_code, context=context)
                        if buyer_id:
                            receptor = buyer_id[0]
                        else:
                            # Error: Provider partner not found
                            error_code = '0004'
                            self.notify_error(cursor, uid, line_id, error_code, buyer_vat, context=context)

                    factures_importades = []
                    total_invoice_amount = 0.0

                    for facturae in facturae_parser.factures:
                        facturae_id = self.create_invoice(cursor, uid, facturae, issuer_type, line_id,
                                                          emissor, receptor, context=context)
                        factures_importades.append(facturae_id)
                        total_invoice_amount += float(facturae['InvoiceTotal']) if 'InvoiceTotal' in facturae else 0.0

                        for line in facturae['InvoiceLines']:
                            self.create_line(cursor, uid, line_id, line, facturae_id, context=context)

                        line_facturae_obj.create(cursor, uid, {'linia_id': line_id, 'invoice_id': facturae_id})

                    if len(factures_importades) != int(facturae_parser.num_factures):
                        # Error: Invoice count do not match
                        error_code = '0007'
                        self.notify_error(cursor, uid, line_id, error_code, context=context)

                    if total_invoice_amount != float(facturae_parser.total_factures):
                        # Error: Invoice totals de not match
                        error_code = '0008'
                        self.notify_error(cursor, uid, line_id, error_code, context=context)

        except Exception as e:
            state = 'erroni'
            res = False
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
        finally:
            has_errors = self.update_errors(cursor, uid, line_id, context=context)

            if not has_errors and res:
                state = 'valid'
            else:
                if facturae_id:
                    self.delete_facturae(cursor, uid, facturae_id)

            self.write(cursor, uid, [line_id], {'state': state})

        return res

    def validate_xml(self, cursor, uid, xml, context=None):
        version = re.search('http://www.facturae.es/Facturae/(.+?)/Facturae', xml)
        if version:
            version = version.group(1).split('/')[1] if version else False
            xsd_file_name = facturae_version_xsd[version]

            path = os.path.abspath(os.path.dirname(__file__))
            xsd_path = os.path.join(path, 'data', xsd_file_name)

            with open(xsd_path, 'r') as xsd_file:
                xmlschema_doc = etree.parse(xsd_file)
                xmlschema = etree.XMLSchema(xmlschema_doc)

                xml_file = StringIO(xml)
                xml_doc = etree.parse(xml_file)
                result = xmlschema.validate(xml_doc)
        else:
            result = False
            version = '"Version Not Found in file"'

        return result, version

    def create_invoice(self, cursor, uid, facturae, issuer_type, line_id, emissor, receptor, context=None):
        invoice_obj = self.pool.get('account.invoice')
        line_obj = self.pool.get('account.import.facturae.line')

        if facturae['InvoiceClass'] == 'OR':
            # If invoice class is Rectificative Invoice (OR) -> Not implemented yet
            error_code = '1001'
            self.notify_error(cursor, uid, line_id, error_code, context=context)

        elif facturae['InvoiceClass'] != 'OO':
            # If invoice type different from Regular Invoice (OO) -> Not implemented yet
            error_code = '1002'
            self.notify_error(cursor, uid, line_id, error_code, context=context)

        if issuer_type == 'EM':
            invoice_type = 'in_invoice'
            partner_id = emissor
        elif issuer_type == 'RE':
            invoice_type = 'out_invoice'
            partner_id = receptor
        else:
            # If issuer type different from "Emissor" (EM) or "Receptor" (RE) -> Not implemented yet
            error_code = '1003'
            self.notify_error(cursor, uid, line_id, error_code, context=context)

        vals = invoice_obj.onchange_partner_id(cursor, uid, [], invoice_type, partner_id)['value']
        journal_id = line_obj.read(cursor, uid, line_id, ['journal_id'])['journal_id']

        vals.update({
            'origin': facturae['InvoiceNumber'] if 'InvoiceNumber' in facturae else '',
            'type': invoice_type,
            'date_invoice': facturae['IssueDate'] if 'IssueDate' in facturae else '',
            'origin_date_invoice': facturae['IssueDate'] if 'IssueDate' in facturae else '',
            'check_total': float(facturae['InvoiceTotal']) if 'InvoiceTotal' in facturae else '',
            'date_due': facturae['InstallmentDueDate'] if 'InstallmentDueDate' in facturae else '',
            'comment': facturae['AdditionalInformation'] if 'AdditionalInformation' in facturae else '',
            'partner_id': partner_id,
            'journal_id': journal_id[0],
            'state': 'draft'
        })

        facturae_id = invoice_obj.create(cursor, uid, vals)
        return facturae_id

    def delete_facturae(self, cursor, uid, facturae_id, context=None):
        invoice_obj = self.pool.get('account.invoice')
        invoice_obj.unlink(cursor, uid, [facturae_id])

    def create_line(self, cursor, uid, line_id, line, facturae_id, context=None):
        line_obj = self.pool.get('account.invoice.line')
        invoice_obj = self.pool.get('account.invoice')

        account_id = invoice_obj.read(cursor, uid, facturae_id, ['account_id'])['account_id'][0]
        tax_obj = self.pool.get('account.tax')
        tax_codes = [tax['TaxTypeCode'] for tax in line['TaxesOutputs']]

        line_tax_ids = []

        for tax_code in tax_codes:
            tax_ids = tax_obj.search(cursor, uid, [('facturae_code', '=', tax_code)])
            if not tax_ids:
                # The tax has no match in the DB
                error_code = '0009'
                self.notify_error(cursor, uid, line_id, error_code, extra_info=tax_code, context=context)

            elif len(tax_ids) > 1:
                # There are more than one tax with the same code in the DB
                error_code = '0010'
                self.notify_error(cursor, uid, line_id, error_code, extra_info=tax_code, context=context)

            else:
                line_tax_ids.append(tax_ids[0])

        vals = {
            'name': line['ItemDescription'],
            'quantity': line['Quantity'],
            'price_unit': line['UnitPriceWithoutTax'],
            'account_id': account_id,
            'invoice_id': facturae_id,
            'invoice_line_tax_id': [(6, 0, line_tax_ids)],
        }

        line_id = line_obj.create(cursor, uid, vals)
        return line_id

    def get_xml_from_adjunt(self, cursor, uid, line_id, context=None):
        """Reads XML from current attachment"""

        att_obj = self.pool.get('ir.attachment')

        attachment_id = self.read(cursor, uid, line_id[0], ['attachment_id'])['attachment_id']

        if attachment_id:
            b64_data = att_obj.read(cursor, uid, attachment_id[0], ['datas'])['datas']
            if b64_data:
                return base64.b64decode(b64_data)

        return ''

    def change_attachment_id(self, cursor, uid, line_id, attachment_id):
        self.write(cursor, uid, [line_id], {'attachment_id': attachment_id})
        xml_data = self.get_xml_from_adjunt(cursor, uid, [line_id])
        md5_hash = get_md5(xml_data)
        vals = {'md5_hash': md5_hash}
        self.write(cursor, uid, [line_id], vals)

        return True

    def reset_errors(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}

        error_obj = self.pool.get('account.import.facturae.error')
        error_ids = error_obj.search(cursor, uid, [('line_id', '=', line_id), ('active', '=', True)])
        error_obj.unlink(cursor, uid, error_ids)

    def notify_error(self, cursor, uid, line_id, error_code, extra_info=None, context=None):
        if context is None:
            context = {}

        error_obj = self.pool.get('account.import.facturae.error')
        message = error_obj.get_message_from_code(cursor, uid, error_code, context=context)

        vals = {
            'name': error_code,
            'message': message.format(extra_info if extra_info is not None else ''),
            'line_id': line_id,
            'active': True,
        }

        error_obj.create(cursor, uid, vals)

    def update_errors(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}

        info = ''

        error_obj = self.pool.get('account.import.facturae.error')
        error_ids = error_obj.search(cursor, uid, [('line_id', '=', line_id), ('active', '=', True)])

        if error_ids:
            for error_id in error_ids:
                error_data = error_obj.read(cursor, uid, error_id, ['name', 'message'])
                info += '[{}] {}\n'.format(error_data['name'], error_data['message'])

        self.write(cursor, uid, line_id, {'info': info})
        return bool(error_ids)

    _columns = {
        'name': fields.char('Fitxer', size=128),

        'state': fields.selection(_states_selection, 'Estat', size=32),

        'importacio_id': fields.many2one('account.import.facturae', 'Importacio',
                                         required=True, select=True),

        'journal_id': fields.many2one('account.journal', 'Diari',
                                      required=True),

        'linia_facturae_id': fields.one2many('account.import.facturae.line.facturae', 'linia_id',
                                             'Factures', ondelete='cascade'),

        'info': fields.text('Info', readonly=True, size=4000),

        'conte_factures': fields.function(_ff_check_factures, method=True,
                                          string="Conté factures", type='boolean'),

        # Factura-e header fields
        'codi_sollicitud': fields.char('Codi sol·licitud', size=12, readonly=True),

        'invoice_number_text': fields.char('Numero Factura Origen', size=30, readonly=True),

        'emissor': fields.many2one('res.partner', 'Emissor', readonly=True,
                                   required=False, select=True),

        'receptor': fields.many2one('res.partner', 'Receptor', readonly=True,
                                    required=False, select=True),

        'data_carrega': fields.function(_ff_data_carrega, type='datetime',
                                        string="Data Carrega", method=True,
                                        fnct_search=_data_carrega_search),

        'md5_hash': fields.char('Hash', size=32, readonly=True),

        'attachment_id': fields.many2one('ir.attachment', 'Adjunt utilitzat',
                                         readonly=True, select=True),

        'type_factura': fields.char('Tipus de factura', size=4, readonly=True,
                                    help=u'Indica de quin tipus de factura es '
                                         u'tracta (N, R...)'),

        'data_factura': fields.date('Data Factura', readonly=True),

        'data_venciment': fields.date('Data de venciment', readonly=True),
    }

    _order = 'create_date desc, write_date desc'


AccountImportFacturaeLine()


class AccountImportFacturaeLineFacturae(osv.osv):
    """Model relacional linia-facturae"""

    _name = 'account.import.facturae.line.facturae'
    _description = 'relació entre linies d\'importació i factures electròniques'
    _inherits = {'account.invoice': 'invoice_id'}

    _columns = {
        'linia_id': fields.many2one('account.import.facturae.line', 'Linia',
                                    requiered=True, select=1),

        'invoice_id': fields.many2one('account.invoice', 'Factura', requiered=True,
                                      select=1, ondelete="cascade"),
    }


AccountImportFacturaeLineFacturae()


