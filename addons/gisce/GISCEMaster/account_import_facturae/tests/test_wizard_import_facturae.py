# -*- coding: utf-8 -*-
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource


class TestWizardImportFacturae(testing.OOTestCase):

    require_demo_data = True

    def test_import_xml_all_right(self):
        xml_path = get_module_resource(
            'account_import_facturae', 'tests', 'fixtures',
            'facturae_test.xsig'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        wiz_obj = self.openerp.pool.get('wizard.importacio.facturae')
        import_obj = self.openerp.pool.get('account.import.facturae')
        line_obj = self.openerp.pool.get('account.import.facturae.line')
        linea_factura_obj = self.openerp.pool.get('account.import.facturae.line.facturae')
        invoice_obj = self.openerp.pool.get('account.invoice')
        tax_obj = self.openerp.pool.get('account.tax')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vals = {
                'file': base64.b64encode(xml_file),
                'filename': 'FILE.xml',
                'journal_id': 10,
            }

            tax_obj.write(cursor, uid, 18, {'facturae_code': '01'})
            tax_obj.write(cursor, uid, 129, {'facturae_code': '07'})

            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            before_import_ids = import_obj.search(cursor, uid, [])
            before_line_ids = line_obj.search(cursor, uid, [])

            wiz.action_importar_facturae()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))

            after_import_ids = import_obj.search(cursor, uid, [])
            after_line_ids = line_obj.search(cursor, uid, [])

            new_import_ids = list(set(after_import_ids) - set(before_import_ids))
            new_line_ids = list(set(after_line_ids) - set(before_line_ids))

            # Only one import and line are created
            expect(len(new_import_ids)).to(equal(1))
            expect(len(new_line_ids)).to(equal(1))

            import_data = import_obj.read(cursor, uid, new_import_ids[0], [])
            expect(import_data['name']).to(equal('FILE.xml'))
            expect(len(import_data['linia_ids'])).to(equal(1))

            line_obj.process_line_sync(cursor, uid, import_data['linia_ids'][0])

            line_facturae_id = linea_factura_obj.search(cursor, uid, [('linia_id', '=', import_data['linia_ids'][0])])
            expect(len(line_facturae_id)).to(equal(1))

            invoice_id = linea_factura_obj.read(cursor, uid, line_facturae_id, ['invoice_id'])
            expect(len(invoice_id)).to(equal(1))

            invoice_data = invoice_obj.read(cursor, uid, invoice_id[0]['invoice_id'][0], ['invoice_line'])
            expect(len(invoice_data['invoice_line'])).to(equal(3))

    def test_import_xml_with_errors_and_info(self):
        xml_path = get_module_resource(
            'account_import_facturae', 'tests', 'fixtures',
            'facturae_test_fail.xsig'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
        wiz_obj = self.openerp.pool.get('wizard.importacio.facturae')
        wiz_xml_obj = self.openerp.pool.get('wizard.import.facturae.xml.import')
        import_obj = self.openerp.pool.get('account.import.facturae')
        line_obj = self.openerp.pool.get('account.import.facturae.line')
        linea_factura_obj = self.openerp.pool.get('account.import.facturae.line.facturae')
        tax_obj = self.openerp.pool.get('account.tax')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            vals = {
                'file': base64.b64encode(xml_file),
                'filename': 'FILE.xml',
                'journal_id': 10,
            }

            tax_obj.write(cursor, uid, 18, {'facturae_code': '01'})
            tax_obj.write(cursor, uid, 19, {'facturae_code': '01'})
            tax_obj.write(cursor, uid, 129, {'facturae_code': '07'})

            ctx = {}
            wiz_id = wiz_obj.create(cursor, uid, vals, context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            before_import_ids = import_obj.search(cursor, uid, [])
            before_line_ids = line_obj.search(cursor, uid, [])

            wiz.action_importar_facturae()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            expect('done').to(equal(wiz.state))

            after_import_ids = import_obj.search(cursor, uid, [])
            after_line_ids = line_obj.search(cursor, uid, [])

            new_import_ids = list(set(after_import_ids) - set(before_import_ids))
            new_line_ids = list(set(after_line_ids) - set(before_line_ids))

            # Only one import and line are created
            expect(len(new_import_ids)).to(equal(1))
            expect(len(new_line_ids)).to(equal(1))

            import_data = import_obj.read(cursor, uid, new_import_ids[0], [])
            expect(import_data['name']).to(equal('FILE.xml'))
            expect(len(import_data['linia_ids'])).to(equal(1))

            line_id = import_data['linia_ids'][0]
            ctx.update({'active_id': line_id,
                        'fitxer_xml': 'FILE.xml'})

            vals = {
                'xml': base64.b64encode(xml_file),
                'filename': 'FILE.xml'
            }
            wiz_xml_id = wiz_xml_obj.create(cursor, uid, vals, context=ctx)
            wiz_xml = wiz_xml_obj.browse(cursor, uid, wiz_xml_id, context=ctx)

            wiz_xml.action_importar_facturae(context=ctx)

            line_facturae_id = linea_factura_obj.search(cursor, uid, [('linia_id', '=', line_id)])
            expect(len(line_facturae_id)).to(equal(0))

            info = line_obj.read(cursor, uid, line_id, ['info'])['info']
            expect(info).to(contain('0004'))
            expect(info).to(contain('0006'))
            expect(info).to(contain('0007'))
            expect(info).to(contain('0008'))
            expect(info).to(contain('0009'))
            expect(info).to(contain('0010'))
            expect(info).to(contain('1001'))
