# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


error_messages = {
    '0001': _('El format del fitxer no compleix amb la versió {} de Factura-e'),
    '0002': _('Hi ha més d\'un proveïdor a la base de dades amb el NIF {}'),
    '0003': _('Hi ha més d\'un receptor a la base de dades amb el NIF {}'),
    '0004': _('Empresa amb NIF {} no trobada a la base de dades'),
    '0005': _('La factura no està dirigida a la nostra empresa'),
    '0006': _('La factura no està emesa per la nostra empresa'),
    '0007': _('El número de factures no coincideix amb l\'informat a la factua-e'),
    '0008': _('Els total no coincideixen amb els informats a la factura-e'),
    '0009': _('No hi ha cap impost amb codi de factura-e ({}) definit a la base de dades'),
    '0010': _('Hi ha més d\'un impost amb el codi de factura-e ({}) a la base de dades'),

    '1001': _('La importació de factures electròniques de tipus rectificadora no està disponible encara. '
              'Només es poden importar factures ordinàries (Tipus \'OO\')'),

    '1002': _('La importació de factures electròniques diferents del tipus "factures ordinàries" no està '
              'encara disponible. (Diferents al tipus \'OO\')'),

    '1003': _('En aquests moments, només es poden importar factures ordinàries de '
              'proveïdor o client (Tipus \'EM\' i \'RE\')'),
}


class AccountImportFacturaeError(osv.osv):

    _name = 'account.import.facturae.error'

    def get_message_from_code(self, cursor, uid, error_code, context=None):
        return error_messages[error_code]

    _columns = {
        'name': fields.char(string='Codi error', size=14, required=True, readonly=True, select=True),

        'message': fields.char(string='Missatge d\'error', size=2048, required=True, readonly=True),

        'line_id': fields.many2one('account.import.facturae.line', string='Línia d\'importació',
                                   select=1, readonly=True),

        'active': fields.boolean('Actiu', readonly=True, select=1),
    }


AccountImportFacturaeError()