# -*- coding: utf-8 -*-

from osv import osv


class WizardMostrarFacturaeImportades(osv.osv_memory):

    _name = 'wizard.mostrar.factuae.importades'

    def action_get_factures(self, cursor, uid, ids, context=None):
        active_id = context['active_id']
        linia_obj = self.pool.get('account.import.facturae.line')
        lin_fac_obj = self.pool.get('account.import.facturae.line.facturae')

        result = lin_fac_obj.q(cursor, uid).read(['invoice_id']).where([
            ('linia_id.importacio_id', '=', active_id)
        ])
        inv_ids = [x['invoice_id'] for x in result]

        return {
            'name': 'Llistat de factures',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': "account.invoice",
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(inv_ids)),
        }


WizardMostrarFacturaeImportades()
