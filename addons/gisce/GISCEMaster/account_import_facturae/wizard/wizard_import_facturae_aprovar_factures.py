# -*- coding: utf-8 -*-

from tools import config
from tools.translate import _

import pooler
from osv import osv, fields


class GiscedataFacturacioSwitchingAprovarFactures(osv.osv_memory):
    """Wizard per aprovar les factures des del form de la importació"""
    _name = 'wizard.import.facturae.aprovar.factures'

    def action_aprovar_factures(self, cursor, uid, ids, context=None):
        """Aprovem totes les factures que haguem filtrat (ids).
        """
        if context is None:
            context = {}

        db = pooler.get_db_only(cursor.dbname)
        wizard = self.browse(cursor, uid, ids[0])

        invoice_obj = self.pool.get('account.invoice')
        import_obj = self.pool.get('account.import.facturae')
        line_obj = self.pool.get('account.import.facturae.line')
        line_facturae_obj = self.pool.get('account.import.facturae.line.facturae')


        active_ids = context.get('active_ids', [])

        if context.get('model') == 'account.import.facturae':
            factures = []

            for line in import_obj.browse(cursor, uid, active_ids[0], context).linia_ids:
                for line_facturae in line.linia_facturae_id:
                    factures.append(line_facturae.invoice_id)
        else:
            factures = invoice_obj.browse(cursor, uid, active_ids)

        aprovades = []
        no_aprovades = []

        for factura in factures:
            if factura.rectificative_type == 'BRA':
                continue
            # comprovar que la factura no és oberta
            if factura.state == 'open':
                no_aprovades.append(factura.id)
                continue
            if not factura.origin:
                no_aprovades.append(factura.id)
                continue

            # localitzar factura generada amb el mateix origen i emisor
            origen = factura.origin + '/comp'
            ch_vals = [('origin', '=', origen),
                       ('polissa_id.id', '=', factura.polissa_id.id)]

            f_fact = invoice_obj.search(cursor, uid, ch_vals, context={'active_test': False})

            # si es tracta d'un comparada no se'n fa cas
            if not f_fact and factura.origin[-5:] == '/comp':
                continue
            try:
                cr = db.cursor()

                if factura.type == 'in_invoice':
                    check_total = factura.check_total
                    amount_total = factura.amount_total

                    if wizard.open_1ct_diff_invoices and check_total != amount_total \
                            and abs(check_total - amount_total) <= 0.015:
                        # Accepts 1 ct diff invoices
                        invoice_obj.write(cr, uid, [factura.id], {'check_total': factura.amount_total})


                invoice_obj.invoice_open(cr, uid, [factura.id])
                cr.commit()
                aprovades.append(factura.id)
            except Exception, e:
                cr.rollback()
                no_aprovades.append(factura.id)
                continue
            finally:
                cr.close()

            if f_fact:
                # esborrar les facturades amb el mateix origen
                lfid = line_facturae_obj.search(cursor, uid, [('factura_id', 'in', f_fact)])
                line_facturae_obj.unlink(cursor, uid, lfid)
                invoice_obj.unlink(cursor, uid, f_fact)
            # setejar la línia de factura com a vàlida
            lf_id = line_facturae_obj.search(cursor, uid, [('factura_id', '=', factura.id)])
            if not lf_id:
                continue
            l_id = line_facturae_obj.read(cursor, uid, lf_id, ['linia_id'])[0]
            l_info = line_obj.read(cursor, uid, l_id['linia_id'][0], ['info'])
            info = l_info['info'] + _(u' - Aprovat!')
            l_vals = {'state': 'valid', 'info': info}
            line_obj.write(cursor, uid, l_id['linia_id'][0], l_vals)

        wizard.write({'num_factures_aprovades': len(aprovades),
                      'num_factures_no_aprovades': len(no_aprovades),
                      'fact_aprovades':
                                ', '.join(['%s' % x for x in aprovades]),
                      'fact_no_aprovades':
                                ', '.join(['%s' % x for x in no_aprovades]),
                      'state': 'end'})

    def action_get_factures(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        fact = wizard.fact_no_aprovades
        return {
            'name': _('Factures no aprovades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % \
                            str(tuple([int(x) for x in fact.split(', ') if x])),
        }

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    _columns = {
        'num_factures_aprovades': fields.integer('Factures aprovades',
                                                 readonly=True),
        'num_factures_no_aprovades': fields.integer('Factures no aprovades',
                                                 readonly=True),
        'fact_aprovades': fields.text('Factures aprovades'),
        'fact_no_aprovades': fields.text('Factures no aprovades'),
        'open_1ct_diff_invoices': fields.boolean(
            u"Aprova amb error d'1 cèntim",
            help=u"Aprova la factura encara que la diferència entre el total "
                 u"i el residual sigui d'un cèntim"
        ),
        'state': fields.char('State', size=4),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'open_1ct_diff_invoices': lambda *a: False,
    }


GiscedataFacturacioSwitchingAprovarFactures()
