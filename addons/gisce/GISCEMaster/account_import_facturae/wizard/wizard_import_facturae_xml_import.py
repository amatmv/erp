# -*- coding: utf-8 -*-
import base64
import hashlib

from giscedata_facturacio_switching.giscedata_facturacio_switching_utils import get_cups_from_xml
from tools.translate import _
from osv import osv, fields


def get_md5(data):
    _data = base64.decodestring(data)
    cleaned_data = _data.replace(" ", "")
    cleaned_data = cleaned_data.replace("\n", "")
    cleaned_data = cleaned_data.replace("\t", "")
    md5 = hashlib.md5(cleaned_data)
    return md5.hexdigest()


class WizardImportFacturaeXMLImport(osv.osv_memory):
    """Wizard per facturar via F1"""
    _name = 'wizard.import.facturae.xml.import'

    def action_importar_facturae(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        line_obj = self.pool.get('account.import.facturae.line')
        fitxer_xml = context.get('fitxer_xml', False)
        if wizard.file or wizard.origen == 'exist':
            line_id = context.get('active_id', False)
            if fitxer_xml:
                if wizard.origen == 'nou':
                    xml_data = base64.b64decode(wizard.file)
                    if self.file_is_xml(xml_data):
                        fname = wizard.filename
                        if line_id:
                            line_obj.adjuntar_xml(
                                cursor, uid, line_id, xml_data, fname,
                                context=context
                            )
                    else:
                        raise osv.except_osv('Error', _('El fitxer a reimportar ha de ser un fitxer XML'))
                elif wizard.origen == 'exist':
                    line_obj.change_attachment_id(cursor, uid, line_id, wizard.attachment_id)

                line_obj.process_line_sync(cursor, uid, line_id, context=context)
                wiz_msg = _('Fitxer processat.')
                wizard.write({'state': 'done', 'info': wiz_msg})
                return 0

        elif context.get('active_ids', False):
            for line in context.get('active_ids', []):
                line_obj.process_line(cursor, uid, line, context=context)
            return 0

    def file_is_xml(self, xml_data):
        cups = get_cups_from_xml(xml_data)
        if not cups:
            return False
        else:
            return True

    def _default_state(self, cursor, uid, context=None):
        if not context:
            context = {}

        return 'init'

    def _default_xml(self, cursor, uid, context=None):
        if not context:
            context = {}
        conte_xml = False

        if context.get('fitxer_xml', False):
            attachment_obj = self.pool.get('ir.attachment')
            attachment = attachment_obj.search(cursor, uid,
                                               [('res_id', '=', context['active_id']),
                                                ('res_model', '=', 'account.import.facturae.line')])
            if attachment:
                conte_xml = True

        return conte_xml

    def _default_origen(self, cursor, uid, context=None):
        if not context:
            context = {}

        val = 'nou'
        if context.get('fitxer_xml', False):
            attachment_obj = self.pool.get('ir.attachment')
            attachment = attachment_obj.search(cursor, uid,
                                               [('res_id', '=', context['active_id']),
                                                ('res_model', '=', 'account.import.facturae.line')])
            if attachment:
                val = 'exist'

        return val

    def _default_attachment(self, cursor, uid, context=None):
        if context is None:
            context = {}

        line_id = context['active_id']
        line_obj = self.pool.get('account.import.facturae.line')
        attachment_id = line_obj.read(cursor, uid, line_id, ['attachment_id'])['attachment_id']
        res = False
        if attachment_id:
            res = attachment_id[0]
        return res

    def _attachments(self, cursor, uid, context=None):
        if context is None:
            context = {}

        line_id = context.get('active_id', False)
        model = 'account.import.facturae.line'
        att_obj = self.pool.get('ir.attachment')

        if not line_id:
            return []

        fitxers = att_obj.search(cursor, uid,
                                 [('res_id', '=', line_id),
                                  ('res_model', '=', model)])

        res = [(x['id'], x['name']) for x in att_obj.read(cursor, uid, fitxers, ['name'])]
        return res

    def _default_info_aux(self, cursor, uid, context=None):
        if context is None:
            context = {}
        lines_ids = context.get('active_ids', '')
        msg = _('Es procedirà a la reimportació de {0} fitxers de Factura-e')

        if lines_ids:
            n_files = str(len(lines_ids))
            msg = msg.format(n_files)
            for line in lines_ids:
                msg = msg + '\n' + str(line)
        else:
            msg = _('No s\'ha seleccionat cap fitxer')
        return msg

    def sub_action_importar_f1(self, cursor, uid, ids, context=None):
        if context.get('active_ids', False):
            self.write(
                cursor, uid, ids, {
                    'state': 'end',
                    'msg_info_aux':
                        _("S'està realitzant la reimportació en "
                          "segon pla dels fitxers")
                }
            )
            return self.action_importar_facturae(cursor, uid, ids, context=context)
        else:
            self.write(cursor, uid, ids,
                       {'state': 'end',
                        'msg_info_aux': _("No es realitzarà la reimportació ja " 
                                          "que no hi ha cap fitxer seleccionat")})

    ORIGEN_SELECTION = [('exist', 'Reimportar el fitxer existent'),
                        ('nou', 'Importar un fitxer nou')]

    _columns = {
        'file': fields.binary('Fitxer'),
        'filename': fields.char('Nom', size=128),
        'state': fields.char('State', size=16),
        'info': fields.text('Info'),
        'xml': fields.boolean('XML'),
        'origen': fields.selection(ORIGEN_SELECTION, 'Tipus'),
        'attachment_id': fields.selection(_attachments, 'Adjunts'),
        'msg_info_aux': fields.text('Info2'),
    }

    _defaults = {
        'state': _default_state,
        'xml': _default_xml,
        'origen': _default_origen,
        'attachment_id': _default_attachment,
        'msg_info_aux': _default_info_aux,
    }


WizardImportFacturaeXMLImport()
