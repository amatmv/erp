# -*- coding: utf-8 -*-
{
    "name": "Import Facturae",
    "description": """Import Facturae. 
        Mòdul d'importació de factures electròniques""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "base",
        "account",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_import_facturae_aprovar_factures_view.xml",
        "wizard/wizard_import_facturae_xml_import_view.xml",
        "wizard/wizard_importacio_facturae_view.xml",
        "wizard/wizard_mostrar_factuae_importades_view.xml",
        "account_import_facturae_view.xml",
        "account_tax_extension_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
