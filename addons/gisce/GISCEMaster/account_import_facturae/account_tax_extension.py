# -*- coding: utf-8 -*-
from osv import osv, fields


class AccountTaxExtension(osv.osv):

    _name = 'account.tax'
    _inherit = 'account.tax'

    _columns = {
        'facturae_code': fields.char(string='Codi facturae', size=40)
    }


AccountTaxExtension()
