from base_index.base_index import BaseIndex


class GiscedataBtElement(BaseIndex):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def __init__(self, pool, cursor):
        super(GiscedataBtElement, self).__init__(pool, cursor)

    _index_title = '{obj.name}'
    _index_summary = '{obj.name} - {obj.ct.name}-{obj.num_linia}'

    _index_fields = {
        'cini': True,
        'name': True,
        'ct.name': True,
        'ct.descripcio': True,
        'municipi.name': True,
        'num_linia': lambda self, data: data and '{0} L{0}'.format(data) or '',
        'cable.name': True,
        'cable.seccio': True,
        'cable.material.name': True,
        'voltatge': lambda self, data: data and '{0}V'.format(data) or '',
        'data_pm': True,
    }

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """
        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)

        if not item.num_linia:
            self._index_summary = '{obj.name} - {obj.ct.name}'

        return {
            'id': item.id,
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': self._index_title.format(obj=item),
            'content': ' '.join(set(content)),
            'gistype': self._name,
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }


GiscedataBtElement()
