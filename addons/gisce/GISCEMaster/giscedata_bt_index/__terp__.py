# -*- coding: utf-8 -*-
{
    "name": "Index LBT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index giscedata_bt
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
