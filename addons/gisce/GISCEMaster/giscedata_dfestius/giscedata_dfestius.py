# -*- coding: utf-8 -*-
import calendar
from osv import osv, fields


class GiscedataDFestius(osv.osv):

    _name = 'giscedata.dfestius'

    def is_festiu(self, cursor, uid, data):
        anny, mes, dia = map(int, data.split('-'))
        if calendar.weekday(anny, mes, dia) in (5, 6):
            return True
        # mirar si és un festiu nacional
        return self.search_count(cursor, uid, [('name', '=', data)])

    _columns = {
        'name': fields.date('Dia', required=True, select=True),
    }

    _sql_constraints = [('name_uniq', 'unique (name)',
                         'Ja existeix aquest dia festiu')]

GiscedataDFestius()