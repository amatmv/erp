# -*- coding: utf-8 -*-
{
    "name": "Dies festius nacionals",
    "description": """Dies festius nacionals per REE

  Aquest mòdul incorpora els dies festius nacionals i es pot utlitzar en altres
  mòduls, com el de facturació, perfils i OMEL.

  Només els festius Nacionals amb data fixe (definició REE). No s'inclou
  Divendres Sant (data no dixe) ni Reis (Substituïble)
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_dfestius_view.xml",
        "security/ir.model.access.csv",
        "giscedata_dfestius_2009_data.xml",
        "giscedata_dfestius_2010_data.xml",
        "giscedata_dfestius_2011_data.xml",
        "giscedata_dfestius_2012_data.xml",
        "giscedata_dfestius_2013_data.xml",
        "giscedata_dfestius_2014_data.xml",
        "giscedata_dfestius_2015_data.xml",
        "giscedata_dfestius_2016_data.xml",
        "giscedata_dfestius_2017_data.xml",
        "giscedata_dfestius_2018_data.xml",
        "giscedata_dfestius_2019_data.xml",
        "giscedata_dfestius_2020_data.xml",
    ],
    "active": False,
    "installable": True
}
