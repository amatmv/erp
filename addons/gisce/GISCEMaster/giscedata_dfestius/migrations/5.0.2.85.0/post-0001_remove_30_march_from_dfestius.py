# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Remove 2018-03-30 from free days table on db')

    query = '''
        DELETE FROM giscedata_dfestius
            WHERE name = '2018-03-30'
    '''

    cursor.execute(query)

    cursor.execute(
        "DELETE FROM ir_model_data where module = %s and name = %s",
        ('giscedata_festius', 'festiu_20180330')
    )

    logger.info('Removed 2018-03-30 successfully!!')


def down(cursor, installed_version):
    pass


migrate = up
