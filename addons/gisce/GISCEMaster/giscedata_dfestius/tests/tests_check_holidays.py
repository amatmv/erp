# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta


class TestsHolidays(testing.OOTestCase):
    def test_get_and_check_holidays_2018(self):
        head_id = 'festiu_'
        holidays_2018_list = ['20180101', '20180501', '20180815',
                              '20181012', '20181101', '20181206', '20181208',
                              '20181225']
        imd_obj = self.openerp.pool.get('ir.model.data')
        df_obj = self.openerp.pool.get('giscedata.dfestius')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            dates_ids = []
            for tail_id in holidays_2018_list:
                holiday_id = head_id + tail_id

                date_id = imd_obj.get_object_reference(cursor, uid,
                                                       'giscedata_dfestius',
                                                       holiday_id
                                                       )[1]
                dates_ids.append(date_id)

            dates = df_obj.read(cursor, uid, dates_ids, ['name'])

            for d in dates:
                self.assertTrue(df_obj.is_festiu(cursor, uid, d['name']))

    def test_check_no_holidays_return_false(self):
        not_holidays_list = [
            '2018-03-23', '2018-03-26', '2018-06-06', '2018-09-24'
        ]
        df_obj = self.openerp.pool.get('giscedata.dfestius')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            for day in not_holidays_list:
                self.assertFalse(df_obj.is_festiu(cursor, uid, day))
