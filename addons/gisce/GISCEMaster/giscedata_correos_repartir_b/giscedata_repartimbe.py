# -*- coding: utf-8 -*-
from osv import osv, fields
from delivery_status_lib import DELIVERY_STATUS, RECEPTION_STATE_TO_NOTIFICATION_STATE
from tools.translate import _


class GiscedataRepartimBeOrderLine(osv.osv):
    _name = 'giscedata.correos.order.line'
    _inherit = 'giscedata.correos.order.line'

    def _get_partner_address(self, cursor, uid, ids, field_name, arg, context=None):
        result = {}
        addr_obj = self.pool.get('res.partner.address')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_obj = self.pool.get('account.invoice')

        for record in self.read(cursor, uid, ids, ['partner_id', 'invoice_id'], context=context):
            invoice_id = record['invoice_id'][0]

            partner_name_info = ''
            partner_vat_info = ''
            missing_info = no_partner_found = False

            la_kueri = inv_obj.q(cursor, uid)
            la_kueri = la_kueri.select(
                ['address_contact_id.street',
                 'address_contact_id.city',
                 'address_contact_id.zip',
                 'address_contact_id.partner_id.name',
                 'address_contact_id.name',
                 'address_contact_id.partner_id.vat'
                 ]
            ).where([('id', '=', invoice_id)])
            cursor.execute(*la_kueri)
            address_info = cursor.dictfetchall()

            if address_info:
                address_info = address_info[0]
                result[record['id']] = {
                    'partner_street': address_info[
                        'address_contact_id.street'],
                    'partner_city': address_info[
                        'address_contact_id.city'],
                    'partner_zip': str(
                        address_info['address_contact_id.zip']
                    ),
                    'partner_name': address_info['address_contact_id.name']

                }
                short_result = result[record['id']]
                if not short_result['partner_street'] or not ['partner_city'] or not ['partner_zip']:
                    missing_info = True
                    partner_name_info = address_info[
                        'address_contact_id.partner_id.name'] or ''
                    partner_vat_info = address_info[
                        'address_contact_id.partner_id.vat'] or ''
            else:
                missing_info = True
                no_partner_found = True

            if missing_info:
                name_factura = inv_obj.read(
                    cursor, uid, invoice_id, ['number'], context=context
                )['number']
                if no_partner_found:
                    raise Exception(
                        _('Cal configurar la direccio de '
                          'notificació per a la factura {0}'
                          ).format(name_factura)
                    )
                else:
                    raise Exception(
                        _('Cal configurar la direccio del client {0} amb '
                          'vat {1} associat a la factura {2}'
                          ).format(
                            partner_name_info, partner_vat_info, name_factura
                        )
                    )
        return result

    def get_selection_delivery(self, cursor, uid, context=None):
        '''Method to get selection'''
        return [(k, DELIVERY_STATUS[k][0]) for k in sorted(DELIVERY_STATUS.keys())]

    def _ff_delivered(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        for line_f in self.read(
                cursor, uid, ids, ['delivery_status'], context=context):
            delivery_key = DELIVERY_STATUS.get(line_f['delivery_status'], False)

            if delivery_key:
                delivery_key = delivery_key[1]

            res[line_f['id']] = delivery_key
        return res

    def _get_cups_from_line(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)) and isinstance(ids, (list, tuple)):
            ids = [ids]

        res = dict((k, {'cups': False}) for k in ids)

        select_cups_from_fact_related_with_invoice = (
            "SELECT ol.id AS line_id, cups.name AS cups_name "
            "FROM giscedata_correos_order_line AS ol "
            "RIGHT JOIN giscedata_facturacio_factura AS f "
            "ON (ol.invoice_id = f.invoice_id) "
            "RIGHT JOIN giscedata_cups_ps AS cups "
            "ON (f.cups_id = cups.id) "
            "WHERE ol.id in %s"
        )

        cursor.execute(
            select_cups_from_fact_related_with_invoice,
            (tuple(ids), )
        )

        for row in cursor.dictfetchall():
            res[row['line_id']] = row['cups_name']

        return res

    def _get_fact_ids(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)) and isinstance(ids, (list, tuple)):
            ids = [ids]

        select_fact_ids_as_list_from_order_lines = (
            "SELECT array_agg(f.id) AS fact_ids "
            "FROM giscedata_correos_order_line AS ol "
            "RIGHT JOIN JOIN giscedata_facturacio_factura AS f "
            "ON (ol.invoice_id = f.invoice_id) "
            "WHERE ol.id in %s"
        )
        cursor.execute(
            select_fact_ids_as_list_from_order_lines,
            (tuple(ids),)
        )

        res = cursor.dictfetchall()

        return res[0]['fact_ids']

    def update_pending_states_invoice_state(self, cursor, uid, line_ids,
                                            context=None):
        if not isinstance(line_ids, (list, tuple)):
            line_ids = [line_ids]
        pend_obj = self.pool.get('account.invoice.pending.state')
        inv_obj = self.pool.get('account.invoice')
        imd_obj = self.pool.get('ir.model.data')
        res = {}
        params = ['invoice_id', 'invoice_state', 'delivery_status']
        line_reads = self.read(cursor, uid, line_ids, params, context=context)

        correct_state = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'correct_bono_social_pending_state'
        )[1]

        for line in line_reads:
            delivery_status = line['delivery_status']
            invoice_id = line['invoice_id'][0]
            p_state = line['invoice_state'][0]

            inv_reads = inv_obj.read(
                cursor, uid, invoice_id, ['state', 'pending_state'],
                context=context
            )
            inv_state = inv_reads['state']
            inv_pending_state = inv_reads['pending_state'][0]

            if inv_state != 'paid':
                if inv_pending_state != p_state:
                    return 'distinct_state'
                else:
                    if delivery_status:
                        noti_type = RECEPTION_STATE_TO_NOTIFICATION_STATE[
                            delivery_status]

                        if noti_type == 'entregado':
                            p_id = pend_obj.notification_delivered(
                                cursor, uid, invoice_id, p_state,
                                context=context)
                        elif noti_type == 'enviat':
                            p_id = pend_obj.notification_sent(
                                cursor, uid, invoice_id, p_state,
                                context=context)
                        elif noti_type == 'perdida':
                            p_id = pend_obj.notification_lost(
                                cursor, uid, invoice_id, p_state,
                                context=context)
                        elif noti_type == 'no_entrega':
                            p_id = pend_obj.notification_not_delivered(
                                cursor, uid, invoice_id, p_state,
                                context=context)
                    return 'processed'
            else:
                return 'paid'

    _STORE_CUPS_CHANGE = {
        'giscedata.correos.order.line': (
            lambda self, cursor, uid, ids, context=None: ids, ['invoice_id'], 20
        ),
    }

    _store_delivered = {'giscedata.correos.order.line':
                        (lambda self, cr, uid, ids, c=None:
                         ids, ['delivery_status'], 20)
                        }

    _columns = {
        'cups': fields.function(_get_cups_from_line,
                                type='char',
                                size=25,
                                method=True,
                                string='Cups',
                                store=_STORE_CUPS_CHANGE
                                ),
        'delivery_status': fields.selection(
            get_selection_delivery, 'Estat de l\'enviament', select=True
        ),
        'delivered': fields.function(_ff_delivered, method=True,
                                     string='Entregat?',
                                     type='boolean',
                                     store=_store_delivered,
                                     select=True),
        'exportada': fields.boolean('Exportat'),
        'partner_city': fields.function(_get_partner_address, type='char',
                                        size=60,
                                        method=True, string='Ciutat',
                                        multi='adreça', store=True),
        'partner_street': fields.function(_get_partner_address, type='char',
                                          size=254,
                                          method=True, string='Adreça',
                                          multi='adreça', store=True
                                          ),
        'partner_zip': fields.function(_get_partner_address, type='char',
                                       size=10, method=True,
                                       string='Codi Postal',
                                       multi='adreça', store=True
                                       ),
        'partner_name': fields.function(_get_partner_address,
                                        type='char',
                                        size=250, method=True,
                                        string='Nom Notificacio',
                                        multi='adreça', store=True
                                        )
    }

    _defaults = {
        'exportada': lambda *a: False
    }

GiscedataRepartimBeOrderLine()