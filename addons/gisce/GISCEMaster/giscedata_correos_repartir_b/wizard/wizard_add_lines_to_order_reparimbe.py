# -*- encoding: utf-8 -*-

from tools.translate import _
from osv import osv, fields
from collections import defaultdict


class WizardAddOrderLinesRepartimBe(osv.osv_memory):
    """Wizard per afegir linies a les remeses de correus espanya"""

    _name = 'wizard.add.order.lines'
    _inherit = 'wizard.add.order.lines'


WizardAddOrderLinesRepartimBe()
