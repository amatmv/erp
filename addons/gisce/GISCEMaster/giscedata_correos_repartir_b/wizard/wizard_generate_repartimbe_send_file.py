# -*- encoding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
import pandas as pd
from base64 import b64encode
from datetime import datetime
from StringIO import StringIO
import zipfile


class WizardRepartimBeSendFile(osv.osv_memory):
    """Wizard per gestionar comandes de correus per Reparim a base de bé"""

    _name = 'wizard.repartimbe.send.file'

    MAPPING_PENDING_STATES = {}

    def create(self, cr, user, vals, context=None):
        self.MAPPING_PENDING_STATES = self.generate_pending_mapping(
            cr, user, context=context
        )
        return super(WizardRepartimBeSendFile,
                     self).create(cr, user, vals, context=context)

    def generate_pending_mapping(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        cod_uno = ['carta_1_pendent_pending_state']
        cod_dos = ['carta_2_pendent_pending_state']
        cod_tres = ['pendent_carta_avis_tall_pending_state']
        # account_invoice_pending

        res = {}

        for idd in cod_uno:
            res[imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social', idd
            )[1]] = '01'
        for idd in cod_dos:
            res[imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social', idd
            )[1]] = '02'
        for idd in cod_tres:
            res[imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social', idd
            )[1]] = '03'
        return res

    def get_tipo_carta(self, cursor, uid, pending_id=False, context=None):
        return self.MAPPING_PENDING_STATES[pending_id]

    def generate_pdf_report(self, cursor, uid, line_ids, context=None):
        return False, False

    def generate_file_from_order(self, cursor, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        order_obj = self.pool.get('giscedata.correos.order')
        ol_obj = self.pool.get('giscedata.correos.order.line')
        att_order_obj = self.pool.get('giscedata.correos.attatchment')

        self_fields = self.read(
            cursor, uid, ids,
            ['order_id', 'next_pending_states', 'file_name', 'file_pdf_name'],
            context=context
        )
        order_id = False
        next_pendings = False
        attc_vals = False

        if isinstance(self_fields, (list, tuple)):
            order_id = self_fields[0]['order_id']
            next_pendings = self_fields[0]['next_pending_states']
            file_name_xls = self_fields[0]['file_name']
            file_name_pdf = self_fields[0]['file_pdf_name']
        else:
            order_id = self_fields['order_id']
            next_pendings = self_fields['next_pending_states']
            file_name_xls = self_fields['file_name']
            file_name_pdf = self_fields['file_pdf_name']

        order_line_ids = order_obj.read(
            cursor, uid, order_id, ['order_lines'], context=context
        )['order_lines']

        # Filter paid invoices and exported
        select_paid_invoices = (
            "SELECT array_agg(ol.id) AS paid_ids "
            "FROM giscedata_correos_order_line AS ol "
            "RIGHT JOIN account_invoice AS i "
            "ON (ol.invoice_id = i.id) "
            "WHERE ol.id in %s and (i.state = 'paid' OR ol.exportada)"
        )
        cursor.execute(
            select_paid_invoices,
            (tuple(order_line_ids), )
        )
        res = cursor.dictfetchall()

        paid_ids = res[0]['paid_ids']

        if paid_ids:
            order_line_ids = list(set(order_line_ids) - set(paid_ids))

        file_rows_dicts = ol_obj.read(
            cursor, uid, order_line_ids,
            ['cups', 'partner_id',
             'partner_city', 'partner_street', 'partner_zip', 'partner_name'
             ], context=context
        )

        _columns = [
            'id', 'cups', 'partner_name',
            'partner_street', 'partner_zip', 'partner_city'
        ]
        _rename = {
            'id': "Codi Carta",
            'cups': "CUPS / abonat",
            'partner_name': "Nom complet",
            'partner_street': u"Adreça",
            'partner_zip': "Codi Postal",
            'partner_city': u"Població"
        }

        same_order_dict_line_ids = [dic['id'] for dic in file_rows_dicts]

        #
        pdf, error_line_ids = self.generate_pdf_report(
            cursor, uid, same_order_dict_line_ids, context=context
        )

        df_file = pd.DataFrame(data=file_rows_dicts, columns=_columns)

        self.write(cursor, uid, [ids], {'info': ""})
        if error_line_ids:
            df_file = df_file.loc[~df_file.id.isin(error_line_ids), :]

        df_file.rename(columns=_rename, inplace=True)

        df_file["Codi Carta"] = df_file["Codi Carta"].apply(
            lambda x: str(x).zfill(11)
        )

        output_file = StringIO()
        writer = pd.ExcelWriter(output_file, engine='xlwt')
        df_file.to_excel(writer, index=None, encoding='iso-8859-15')
        writer.close()
        xls_file = output_file.getvalue()
        gen_file = b64encode(xls_file)
        output_file.close()

        upd_params = {
            'state': 'confirm',
            'file': gen_file
        }

        if pdf:
            exported_line_ids = list(set(order_line_ids) - set(error_line_ids))

            upd_params.update(
                {'file_pdf': b64encode(pdf)}
            )
            # Entendemos que si no se genera ningun pdf
            # todas las lineas dan errores
            if next_pendings:

                ol_obj.write(
                    cursor, uid, exported_line_ids,
                    {'exportada': True}, context=context
                )
                pen_res = order_obj.go_next_pendings(
                    cursor, uid, order_id, force_update=True, o_line_ids=order_line_ids, context=context
                )

                error_pend_changes = pen_res[1]

                error_sep_remesa = ['  Remesa {}:\n{}'.format(
                    rem, '\n'.join(
                        (
                            '    - FACTURA {} :   Origen ({})  ->  Destino ({})'
                        ).format(
                            err.inv_num, err.p_state_inv, err.p_state_dest
                        ) for err in error_pend_changes[rem]
                    )
                ) for rem in error_pend_changes.keys()]

                errores_str = ''
                if error_pend_changes:
                    errores_str = 'ERRORES AL CAMBIAR ESTADOS:\n{}\n\n'.format(
                        '\n'.join(error_sep_remesa))
                else:
                    errores_str = 'Estados pendientes actualizados correctamente'
                self.write(cursor, uid, [ids], {'info': errores_str})

                # Create zip to store as attatchment
                zip_name = "Enviament_{}.zip".format(
                    datetime.today().strftime('%d-%m-%Y')
                )

                zip_file = StringIO()

                zf = zipfile.ZipFile(
                    zip_file, mode="w", compression=zipfile.ZIP_DEFLATED
                )

                zf.writestr(file_name_xls, xls_file)
                zf.writestr(file_name_pdf, pdf)
                zf.close()

                zip_content = zip_file.getvalue()

                attc_vals = {
                    'order_ids': [order_id],
                    'file': b64encode(zip_content),
                    'file_name': zip_name,
                }

                zip_file.close()

        if error_line_ids:
            inffo = self.read(
                cursor, uid, ids, ['info'], context=context
            )[0]['info']
            join_str = ', '.join([str(it) for it in error_line_ids])
            mensaje_de_error = '{}{}'.format(inffo, '\nError al generar la carta para las lineas con ids: {}'.format(join_str))
            self.write(cursor, uid, [ids], {'info': mensaje_de_error})

        if attc_vals:
            inffo = self.read(
                cursor, uid, ids, ['info'], context=context
            )[0]['info']
            attc_vals.update({'result': inffo})
            att_order_obj.create_attachment_for_orders(
                cursor, uid, attc_vals, context=context
            )

        self.write(
            cursor, uid, [ids],
            upd_params,
            context=context
        )

    def return_order_view(self, cursor, uid, ids, context=None):
            order_id = self.read(
                cursor, uid, ids, ['order_id'], context=context
            )[0]['order_id']
            return {
                'domain': [('id', '=', order_id)],
                'name': _('Remesas actualitzada'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.correos.order',
                'type': 'ir.actions.act_window',
                'view_id': False,
            }

    _columns = {
        'file': fields.binary('Fichero'),
        'state': fields.selection(
            [('init', 'Init'), ('confirm', 'Confirm'), ('end', 'End')], 'State'
        ),
        'info': fields.text('Información', readonly=True),
        'order_id': fields.many2one(
            'giscedata.correos.order', 'Remesa de correos', required=True,
        ),
        'file_name': fields.char('Nombre del fichero', size=256),
        'file_pdf': fields.binary('Fichero pdf'),
        'file_pdf_name': fields.char('Nombre fichero pdf', size=256),
        'next_pending_states': fields.boolean(
            'Siguiente estado',
            help='Si esta opción se encuentra seleccionada, al generar '
                 'el ficheros se pasaran las lineas de la remesa al '
                 'siguiente estado pendiente'
        ),

    }

    def onchange_order(self, cursor, uid, ids, order_id, context=None):
        """
        Update info if order change
        """
        if not order_id:
            return {}

        context.update({'active_ids': [order_id]})
        info = self._default_info(cursor, uid, context=context)

        return {
            'value': {'info': info}
        }

    def _default_order(self, cursor, uid, context=None):
        if not context:
            context = {}

        order_ids = context.get('active_ids', [])

        if order_ids:
            return order_ids[0]
        else:
            return False

    def _default_info(self, cursor, uid, context=None):
        corder_obj = self.pool.get('giscedata.correos.order')
        if not context:
            context = {}
        order_id = context.get('active_ids', [])
        ord_name = 'No encontrada'
        n_lines = 0

        if order_id:
            ord_inf = corder_obj.read(
                cursor, uid, order_id[0], ['name', 'n_lines'], context=context
            )

            ord_name = ord_inf['name']
            n_lines = ord_inf['n_lines']

        base_info = _('Se procederá a la generación del fichero de '
                      'comunicaciones con formato definido por RepartimBe '
                      'de la remesa {0} con {1} lineas'
                      ).format(ord_name, n_lines)
        return base_info

    def _default_filename(self, cursor, uid, context=None):
        return "Enviament_{}.xls".format(datetime.today().strftime('%d-%m-%Y'))

    def _default_pdf_filename(self, cursor, uid, context=None):
        return "Enviament_{}.pdf".format(datetime.today().strftime('%d-%m-%Y'))

    _defaults = {
        'state': lambda *a: 'init',
        'order_id': _default_order,
        'info': _default_info,
        'file_name': _default_filename,
        'file_pdf_name': _default_pdf_filename,
        'next_pending_states': lambda *a: False
    }

WizardRepartimBeSendFile()
