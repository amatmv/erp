# -*- encoding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
import pandas as pd
from StringIO import StringIO
from base64 import b64encode, b64decode
from xlrd.compdoc import CompDoc, EOCSID, CompDocError

# Codi Carta
# CUPS / abonat
# Nom complet
# Adreça
# Codi Postal
# Població
# Data Entrada
# Data Estat
# Estat
# Codi Correus


def _locate_stream(self, mem, base, sat, sec_size, start_sid, expected_stream_size, qname, seen_id):
    s = start_sid
    if s < 0:
        raise CompDocError("_locate_stream: start_sid (%d) is -ve" % start_sid)
    p = -99 # dummy previous SID
    start_pos = -9999
    end_pos = -8888
    slices = []
    tot_found = 0
    found_limit = (expected_stream_size + sec_size - 1) // sec_size
    while s >= 0:
        if self.seen[s]:
            pass
        self.seen[s] = seen_id
        tot_found += 1
        if tot_found > found_limit:
            raise CompDocError(
                "%s: size exceeds expected %d bytes; corrupt?"
                % (qname, found_limit * sec_size)
                ) # Note: expected size rounded up to higher sector
        if s == p+1:
            # contiguous sectors
            end_pos += sec_size
        else:
            # start new slice
            if p >= 0:
                # not first time
                slices.append((start_pos, end_pos))
            start_pos = base + s * sec_size
            end_pos = start_pos + sec_size
        p = s
        s = sat[s]
    assert s == EOCSID
    assert tot_found == found_limit

    if not slices:
        # The stream is contiguous ... just what we like!
        return (mem, start_pos, expected_stream_size)
    slices.append((start_pos, end_pos))
    return (b''.join([mem[start_pos:end_pos] for start_pos, end_pos in slices]), 0, expected_stream_size)


class WizardInterpretateRepartimBeResponseFile(osv.osv_memory):

    _name = 'wizard.interpretate.repartimbe.response.file'
    _description = ('Asistente para la importación de '
                    'fichero de respuesta Repartimbé'
                    )
    CompDoc._locate_stream = _locate_stream

    def _default_info(self, cursor, uid, context=None):
        return _('Assistent para la importación de fitxeros de resultados de '
                 'carga y actualitzación de las lineas de las remeses')

    def return_updated_orders_tree_view(self, cursor, uid, ids, context=None):
        return {
            'domain': [('id', 'in', self.order_ids)],
            'name': _('Remeses actualitzades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.correos.order',
            'type': 'ir.actions.act_window',
            'view_id': False,
        }

    def ch_state(self, cursor, uid, ids, state, context=None):
        self.order_ids = list(self.order_ids)
        if self.order_ids:
            # Create attatchment
            att_order_obj = self.pool.get('giscedata.correos.attatchment')
            self_fields = self.read(
                cursor, uid, ids, ['info', 'file', 'file_name', 'auto_import'], context=context
            )[0]

            ftp_import = self_fields['auto_import']

            if not ftp_import:
                info = self_fields['info']

                imported_file = self_fields['file']

                imported_file_name = self_fields['file_name']

                attc_vals = {
                    'order_ids': self.order_ids,
                    'file': imported_file,
                    'file_name': imported_file_name,
                    'result': info
                }

                att_order_obj.create_attachment_for_orders(
                    cursor, uid, attc_vals, context=context
                )

        self.write(cursor, uid, ids, {'state': state}, context=context)

    def write_state_line(self, cursor, uid, ids, row, context=None):
        line_obj = self.pool.get('giscedata.correos.order.line')
        line_id = int(row["Codi Carta"])
        state = str(row["Estat"])

        data_entrada = str(row["Data Entrada"])
        data_estat = str(row["Data Estat"])

        if data_estat.startswith('0000'):
            data_estat = False

        original_values = None
        try:
            original_values = line_obj.read(
                cursor, uid, line_id,
                ['delivery_status', 'reception_date', 'order_id'],
                context=context
            )

            line_obj.write(
                cursor, uid, line_id,
                {'delivery_status': state,
                 'reception_date': data_estat},
                context=context
            )

            update_result = line_obj.update_pending_states_invoice_state(
                cursor, uid, line_id, context=context
            )
            if update_result == 'distinct_state':
                line_data = line_obj.read(
                    cursor, uid, line_id,
                    ['invoice_id', 'invoice_state']
                )

                inv_number = line_data['invoice_id'][1]
                line_inv_state = line_data['invoice_state'][1]

                info = self.read(
                    cursor, uid, ids, ['info'], context=context
                )[0]['info']

                # Restore original values
                line_obj.write(
                    cursor, uid, line_id,
                    {'delivery_status': original_values['delivery_status'],
                     'reception_date': original_values['reception_date']},
                    context=context
                )

                err_msg = _(
                    'La línia amb id {idd} de la '
                    'factura {fact} té un estat d\'impagament '
                    'diferent de {state}.\n'.format(
                        idd=line_id, fact=inv_number,
                        state=line_inv_state)
                    )
                err_msg = '{}{}'.format(info, err_msg)
                self.write(cursor, uid, ids, {'info': err_msg})
            self.order_ids.add(original_values['order_id'][0])

        except Exception as e:
            info = self.read(
                cursor, uid, ids, ['info'], context=context
            )[0]['info']
            err_msg = _("Linea {} no encontrada/actualizada\n").format(line_id)
            err_msg = '{}{}'.format(info, err_msg)
            self.write(cursor, uid, ids, {'info': err_msg})
            # Restore original values
            if original_values:
                line_obj.write(
                    cursor, uid, line_id,
                    {'delivery_status': original_values['delivery_status'],
                     'reception_date': original_values['reception_date']},
                    context=context
                )

    def update_lines(self, cursor, uid, ids, context=None):
        self.order_ids = set()
        interprete_file = self.read(
            cursor, uid, ids, ['file'], context=context
        )[0]['file']

        interprete_file = StringIO(b64decode(interprete_file))

        df_file = pd.read_excel(interprete_file)

        # Reset info
        self.write(cursor, uid, ids, {'info': ''}, context=context)

        df_file.apply(
            lambda x: self.write_state_line(
                cursor, uid, ids, x, context=context
            ),
            axis=1
        )

        self.ch_state(cursor, uid, ids, state="end", context=context)

    def get_parsed_full_path_and_sorted_ftp_paths(self, parsed_file_info_list):
        return parsed_file_info_list

    def update_lines_from_ftp(self, cursor, uid, ids, context=None):
        hash_import_obj = self.pool.get('giscedata.repartirbe.hash.imports')
        ftp_obj = self.pool.get('giscedata.ftp.connections')
        self.order_ids = set()

        self.write(cursor, uid, ids, {'info': ''}, context=context)

        server_id = self.read(
            cursor, uid, ids, ['ftp_config'], context=context
        )[0]['ftp_config']

        # Connect with ftp configuration
        ftp_conn = ftp_obj.login(cursor, uid, server_id, context=context)

        # Gets path where to read, can be more than one
        paths_to_read = ftp_obj.get_read_dirs(
            cursor, uid, [], server_id, context=context
        )

        paths_files_list = []

        for ftp_path in paths_to_read:
            # Returns a long string with file info
            files_in_path = ftp_obj.get_files_full_info(
                cursor, uid, [], ftp_conn, path=ftp_path, context=context
            )

            # Parse long string spliting params and add path on first position
            # of arraya after parse
            for f_info_pos in range(len(files_in_path)):
                files_in_path[f_info_pos] = ftp_obj.parse_list_fromat(
                    files_in_path[f_info_pos]
                )
                files_in_path[f_info_pos][0] = ftp_path

            paths_files_list.extend(files_in_path)

        # Sort parsed stings like custom function and
        # with path in first position
        paths_files_list = self.get_parsed_full_path_and_sorted_ftp_paths(
            paths_files_list
        )
        # In this moment we have files sorted in every path

        for c_file in paths_files_list:
            folder_sep = u''
            folder = c_file[0]
            if not folder.endswith(u'/'):
                folder_sep = u'/'
            file_name = c_file[-1]
            if not file_name.endswith(u'.xls'):
                continue
            path_and_file = u'{}{}{}'.format(folder, folder_sep, file_name)

            str_file = ftp_obj.read_file(
                cursor, uid, [], ftp_conn, path_and_file, context=context
            )

            can_import, res = hash_import_obj.create_hash_instance(
                cursor, uid, str_file, hash_conv=True, file_name=file_name, context=context
            )

            if can_import:
                try:
                    io_file = StringIO(str_file)
                    df_file = pd.read_excel(io_file)
                except Exception as e:
                    hash_import_obj.unlink(cursor, uid, res, context=context)
                    err_msg = _(
                        u'No se ha podido leer el fichero {}, '
                        u'esta corrupto o no es un excel'
                    ).format(path_and_file)
                    self.update_info(cursor, uid, ids, err_msg, context=context)
                    continue

                self.update_info(
                    cursor, uid, ids, path_and_file, context=context
                )
                df_file.apply(
                    lambda x: self.write_state_line(
                        cursor, uid, ids, x, context=context
                    ),
                    axis=1
                )
            else:
                err_msg = u'{}: {}'.format(path_and_file, res)
                self.update_info(cursor, uid, ids, err_msg, context=context)

        ftp_obj.close(cursor, uid, [], ftp_conn, context=context)
        self.ch_state(cursor, uid, ids, state="end", context=context)

    def update_info(self, cursor, uid, ids, message, context=None):
        current_info = self.read(
            cursor, uid, ids, ['info'], context=context
        )[0]['info']
        if current_info:
            current_info = u'{}\n'.format(current_info)
        new_info = u'{}{}\n'.format(current_info, message)

        self.write(cursor, uid, ids, {'info': new_info}, context=context)

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End')], 'State',
            readonly=True
        ),
        'file': fields.binary(
            'File', help='Fichero de resultados de carga'
        ),
        'info': fields.text('Información', readonly=True),
        'file_name': fields.char('File name', size=40),
        'auto_import': fields.boolean('Importación FTP'),
        'ftp_config': fields.many2one(
            'giscedata.ftp.connections',
            'Conexión FTP',
            domain="[('category', '=', correos)]"
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'auto_import': lambda *a: False
    }
WizardInterpretateRepartimBeResponseFile()
