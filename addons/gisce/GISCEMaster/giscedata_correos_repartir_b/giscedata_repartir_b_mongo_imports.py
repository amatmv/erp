# -*- coding: utf-8 -*-

from osv import fields
from datetime import datetime
from addons.mongodb_backend import osv_mongodb
import hashlib
from tools.translate import _


class GiscedataRepartirBeHashImports(osv_mongodb.osv_mongodb):
    _name = 'giscedata.repartirbe.hash.imports'

    def hash_file(self, file_str):
        """
        :param file_str: text to be hashed
        :return: hash sha224 size 56
        """

        return hashlib.sha224(file_str).hexdigest()

    def create_hash_instance(self, cursor, uid, str_hash, hash_conv=None, file_name='', context=None):
        if context is None:
            context = {}

        if hash_conv:
            str_hash = self.hash_file(str_hash)

        if len(str_hash) != 56:
            return False, _(u'Error: El string no es un hash sha224')

        hash_found = self.search(
            cursor, uid, [('hash', '=', str_hash)], context=context
        )

        if hash_found:
            return False, _(
                u'Error: Este hash ya existe, fichero ya ha sido importado'
            )

        params_create = {
            'hash': str_hash,
            'name': file_name
        }

        import_id = self.create(cursor, uid, params_create, context=context)

        return True, import_id

    _columns = {
        # 56 because use sha224
        # hashlib.sha224("file text").hexdigest()
        'hash': fields.text('Hash', size=56, requiered=True, readonly=True, select=1),
        'import_date': fields.datetime(
            'Data de creació', select=True, required=True
        ),
        'name': fields.char('Nom del fitxer', size=256, required=True)

    }

    _defaults = {
        'import_date': lambda *a: datetime.strftime(
            datetime.now(), '%Y-%m-%d %H:%M:%S'
        )
    }

GiscedataRepartirBeHashImports()
