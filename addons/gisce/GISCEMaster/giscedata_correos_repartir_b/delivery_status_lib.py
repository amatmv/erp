# -*- encoding: utf-8 -*-

DELIVERY_STATUS = {
    '0': ("En transit", False),
    '1': ("Entregat", True),
    '3': ("Avís deixat", False),
    '4': ('Direcció incorrecta', False),
    '5': ('Defunció', False),
    '6': ('Rebutjat', True),
    '7': ('Bústia agrupada(Avís deixat)', False),
    '8': ('Correus', False),
    '9': ('Desconegut', False),
    '10': ('No es fa càrrec', True),
    '11': ("A l'espera a l'oficina", False),
    '12': ('Entregat oficina', True),
    '13': ('No entregat oficina', False),
    '99': ('Carta no repartida, anul·lat', False),
}

RECEPTION_STATE_TO_NOTIFICATION_STATE = {
    '0': 'enviat',
    '1': 'entregado',
    '3': 'enviat',
    '4': 'perdida',
    '5': 'no_entrega',
    '6': 'no_entrega',
    '7': 'enviat',
    '8': 'enviat',
    '9': 'perdida',
    '10': 'no_entrega',
    '11': 'enviat',
    '12': 'entregado',
    '13': 'enviat',
    '99': 'enviat',
}
