# -*- coding: utf-8 -*-
{
    "name": "Giscedata Repartim Bé",
    "description": """
    Modulo para aprovechar los modulos de correos para sistema Repartimbe:
    * Sistema de generacion e interpretacion de ficheros
    """,
    "version": "0-dev",
    "author": "Gisce",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_correos",
        "giscedata_correos_bono_social",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_repartimbe_view.xml",
        "wizard/wizard_generate_repartimbe_send_file_view.xml",
        "wizard/wizard_interpretate_response_file_view.xml",
        "security/ir.model.access.csv"

    ],
    "active": False,
    "installable": True
}