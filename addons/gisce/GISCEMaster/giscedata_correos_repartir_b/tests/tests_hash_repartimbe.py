# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from base64 import b64encode, b64decode
from StringIO import StringIO
from addons import get_module_resource


class TestsHashRepartimBe(testing.OOTestCase):
    def setUp(self):
        self.txn = Transaction().start(self.database)

        example_file_path = get_module_resource(
            'giscedata_correos_repartir_b',
            'tests', 'fixtures',
            'ejemplo_retorno_hash_tests.xls'
        )

        with open(example_file_path, 'r') as f:
            example_file_str = f.read()

        self.example_file_str = example_file_str

    def tearDown(self):
        hash_files_obj = self.openerp.pool.get(
            'giscedata.repartirbe.hash.imports'
        )
        cursor = self.txn.cursor
        uid = self.txn.user
        h_ids = hash_files_obj.search(cursor, uid, [])
        if h_ids:
            hash_files_obj.unlink(cursor, uid, h_ids)

        self.txn.stop()

    def test_create_and_compare_hash_correctly(self):
        hash_files_obj = self.openerp.pool.get(
            'giscedata.repartirbe.hash.imports'
        )

        example_file_str = self.example_file_str

        hash_file_1 = hash_files_obj.hash_file(example_file_str)

        # sha224 size 56
        self.assertEqual(len(hash_file_1), 56)

        hash_file_2 = hash_files_obj.hash_file(example_file_str)

        self.assertEqual(hash_file_1, hash_file_2)

    def test_check_constrain_hash_unique(self):
        hash_files_obj = self.openerp.pool.get(
            'giscedata.repartirbe.hash.imports'
        )

        cursor = self.txn.cursor
        uid = self.txn.user

        example_file_str = self.example_file_str

        hash_file_1 = hash_files_obj.hash_file(example_file_str)

        create_res = hash_files_obj.create_hash_instance(
            cursor, uid, hash_file_1,
            hash_conv=None
        )
        first_insert_id = create_res[0]
        self.assertTrue(create_res[0])

        self.assertTrue(isinstance(create_res[1], (int, long)))

        # Si intentamos crear otra instancia con el metodo no dejara
        create_res = hash_files_obj.create_hash_instance(
            cursor, uid, hash_file_1,
            hash_conv=None
        )

        self.assertFalse(create_res[0])

        self.assertTrue(isinstance(create_res[1], (basestring, str, unicode)))

        id_mes_1 = first_insert_id + 1

        should_void_list_ids = hash_files_obj.search(
            cursor, uid, [('id', '=', id_mes_1)]
        )

        self.assertFalse(should_void_list_ids)

    def test_create_method_conv_option_work(self):
        hash_files_obj = self.openerp.pool.get(
            'giscedata.repartirbe.hash.imports'
        )

        cursor = self.txn.cursor
        uid = self.txn.user

        example_file_str = self.example_file_str

        hash_file_1 = hash_files_obj.hash_file(example_file_str)

        create_res = hash_files_obj.create_hash_instance(
            cursor, uid, example_file_str,
            hash_conv=True
        )
        self.assertTrue(create_res[1])

        import_id = create_res[1]

        hash_func = hash_files_obj.read(
            cursor, uid, import_id, ['hash']
        )['hash']

        self.assertEqual(len(hash_func), 56)

        self.assertEqual(hash_file_1, hash_func)
