# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from stubserver import FTPStubServer
import mock
# from giscedata_correos_repartir_b.wizard.wizard_generate_repartimbe_send_file import WizardRepartimBeSendFile
import pandas as pd
from StringIO import StringIO
from io import BytesIO


class TestsRepartimBe(testing.OOTestCase):

    def add_factura_0001_on_order_0001(self, cursor, uid):
        wiz_obj = self.openerp.pool.get('wizard.add.order.lines')
        order_obj = self.openerp.pool.get('giscedata.correos.order')

        context = {
            'active_ids': [self.fact_id],
            'model': 'giscedata.facturacio.factura'
        }

        wiz_ord_id = wiz_obj.create(
            cursor, uid, {'order_id': self.correos_order_id}, context=context
        )

        wiz = wiz_obj.browse(cursor, uid, wiz_ord_id, context=context)
        wiz.confirm_state(context=context)
        wiz.add_lines_to_order(context=context)

        self.assertEqual(
            wiz.info, 'S\'han afegit correctament totes les línies'
        )

        self.lines = order_obj.read(
            cursor, uid, self.correos_order_id,
            ['order_lines'], context=context
        )['order_lines']

        self.assertEqual(len(self.lines), 1)

    def fact_to_pendiente_carta_1(self, cursor, uid):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        fact_obj.write(cursor, uid, self.fact_id, {'address_contact_id': 1})

        fact_obj.invoice_open(cursor, uid, [self.fact_id])

        fact_obj.set_pending(
            cursor, uid, [self.fact_id], self.pending_id
        )

        fact_reads = fact_obj.read(
            cursor, uid, self.fact_id, ['state', 'pending_state']
        )

        self.assertEqual(fact_reads['state'], 'open')
        self.assertEqual(fact_reads['pending_state'][0], self.pending_id)

    @mock.patch(
        (
                'giscedata_correos_repartir_b.'
                'wizard.wizard_generate_repartimbe_send_file.'
                'WizardRepartimBeSendFile.generate_pdf_report'
        )
    )
    @mock.patch(
        (
                'giscedata_correos_repartir_b_agri.'
                'wizard.wizard_generate_repartimbe_send_file.'
                'WizardRepartimBeSendFile.generate_pdf_report'
        )
    )
    def send_order_file(self, cursor, uid, mock_function, mock_rewrite):
        mock_function.return_value = (b'more data', [])
        mock_rewrite.return_value = (b'more data', [])
        wiz_gen_file_obj = self.openerp.pool.get('wizard.repartimbe.send.file')

        ctx = {
            'active_ids': [self.correos_order_id]
        }

        create_vals = {
            'next_pending_states': True,
        }

        wiz_id = wiz_gen_file_obj.create(cursor, uid, create_vals, context=ctx)

        wiz_gen_file_obj.generate_file_from_order(
            cursor, uid, [wiz_id], context=ctx
        )

    def get_poliza_ready_fror_bono_social_disponible(self, cursor, uid, polissa_id):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner = polissa_obj.read(
            cursor, uid, polissa_id, ['titular']
        )['titular'][0]

        # Set vat as no enterprise
        res_obj = self.openerp.pool.get('res.partner')
        res_obj.write(cursor, uid, partner, {'vat': 'ES71895522Y'})

        cnae_9820 = imd_obj.get_object_reference(
            cursor, uid, 'giscemisc_cnae', 'cnae_9820'
        )[1]

        # Here current owner vat is enterprise and power is under 10 kW
        polissa_obj.write(
            cursor, uid, polissa_id,
            {'cnae': cnae_9820, 'tipus_vivenda': 'habitual', 'state': 'activa'}
        )

        bono_value = polissa_obj.read(
            cursor, uid, polissa_id, ['bono_social_disponible']
        )['bono_social_disponible']

        self.assertTrue(bono_value)

    def set_client_code_config(self, cursor, uid):
        res_obj = self.openerp.pool.get('res.config')
        # Init client code config var str(size=8) ex. 99999999
        cli_cod = '12345678'
        conf_id = res_obj.search(cursor, uid,
                                 [('name', '=', 'sicer_partner_code')]
                                 )
        res_obj.write(cursor, uid, conf_id, {'value': cli_cod})
        self.client_code = cli_cod

    def setUp(self):
        self.txn = Transaction().start(self.database)
        hash_files_obj = self.openerp.pool.get(
            'giscedata.repartirbe.hash.imports'
        )
        h_ids = hash_files_obj.search(self.txn.cursor, self.txn.user, [])
        if h_ids:
            hash_files_obj.unlink(self.txn.cursor, self.txn.user, h_ids)

        # FTP Server defs
        self.server = FTPStubServer(0)
        self.server.run()
        self.port = self.server.server.server_address[1]
        # END

        cursor = self.txn.cursor
        uid = self.txn.user
        # SICER configuration

        self.set_client_code_config(cursor, uid)
        # END

        # gets contract 0001
        imd_obj = self.openerp.pool.get('ir.model.data')
        self.polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        # Config Bono Social and notification address
        self.get_poliza_ready_fror_bono_social_disponible(
            cursor, uid, self.polissa_id
        )

        # Get Remesa id
        self.correos_order_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos', 'co_0001'
        )[1]

        self.fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]

        self.pending_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio_comer_bono_social',
            'carta_1_pendent_pending_state'
        )[1]

        self.pending_enviado = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_correos_bono_social',
            'pendent_carta_1_enviada_pending_state'
        )[1]

        self.aviso_pending_state = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'avis_tall_pending_state'
        )[1]

        self.fact_to_pendiente_carta_1(cursor, uid)

        self.add_factura_0001_on_order_0001(cursor, uid)

    def tearDown(self):
        hash_files_obj = self.openerp.pool.get(
            'giscedata.repartirbe.hash.imports'
        )
        cursor = self.txn.cursor
        uid = self.txn.user
        h_ids = hash_files_obj.search(cursor, uid, [])
        if h_ids:
            hash_files_obj.unlink(cursor, uid, h_ids)

        self.server.stop()
        self.txn.stop()

    def prepare_server_with_return_file(self, cursor, uid):
        ftp_obj = self.openerp.pool.get('giscedata.ftp.connections')

        params_create = {
            'name': 'FTP Mock Example',
            'ip_address': 'localhost',
            'user': 'user1',
            'password': 'passwd',
            'root_dir': '/',
            'read_dir': '/Sortida',
            'port': self.port,
            'category': 'correos'
        }

        self.ftp_id = ftp_obj.create(cursor, uid, params_create)
        self.conn = ftp_obj.login(cursor, uid, self.ftp_id)

        lines_obj = self.openerp.pool.get('giscedata.correos.order.line')

        unique_line_info = lines_obj.read(cursor, uid, self.lines, [])[0]

        data = [{
            u'Codi Carta': unicode(unique_line_info['id']),
            u'CUPS / abonat': unique_line_info['cups'],
            u'Nom complet': unique_line_info['partner_name'],
            u'Adreça': unique_line_info['partner_street'],
            u'Codi Postal': unique_line_info['partner_zip'],
            u'Població': unique_line_info['partner_city'],
            u'Data Entrada': u'2019-02-07',
            u'Data Estat': u'2019-02-07',
            u'Estat': u'1',
            u'Codi Correus': u''
        }]

        output_file = StringIO()
        writer = pd.ExcelWriter(output_file, engine='xlwt')

        df = pd.DataFrame(data=data)

        df.to_excel(writer, index=None, encoding='iso-8859-15')
        writer.close()

        # str_file = output_file.getvalue()

        # Up example file into newdir folder to check model functions
        file_str = output_file.getvalue()

        b_io = BytesIO()

        b_io.write(file_str)
        b_io.seek(0)

        self.conn.storbinary(
            'STOR /Sortida/retorn.xls', b_io
        )

        folder_files = ftp_obj.get_files(cursor, uid, [], self.conn, path='/Sortida')

        ftp_obj.close(cursor, uid, [], self.conn)

        self.assertTrue(len(folder_files))

        self.assertEqual(folder_files[0], 'retorn.xls')

    @mock.patch(
        (
                'giscedata_ftp_connections.'
                'giscedata_ftp_connections.'
                'GiscedataFtpConnections.get_files_full_info'
        )
    )
    def test_full_race(self, mock_function):
        uid = self.txn.user
        cursor = self.txn.cursor

        mock_function.return_value = (
            ['-rw-------    1 1001     1001         6144 Apr 01 09:57 retorn.xls'])

        self.send_order_file(cursor, uid)

        lines_obj = self.openerp.pool.get('giscedata.correos.order.line')

        unique_line_info = lines_obj.read(cursor, uid, self.lines, [])[0]

        self.assertEqual(
            self.pending_enviado, unique_line_info['invoice_state'][0]
        )

        # Proces return file from ftp
        self.prepare_server_with_return_file(cursor, uid)

        wiz_import_file_obj = self.openerp.pool.get(
            'wizard.interpretate.repartimbe.response.file'
        )

        create_values = {
            'auto_import': True,
            'ftp_config': self.ftp_id
        }
        wiz_id = wiz_import_file_obj.create(cursor, uid, create_values)

        wiz_import_file_obj.update_lines_from_ftp(cursor, uid, [wiz_id])

        wiz = wiz_import_file_obj.browse(cursor, uid, wiz_id)

        updated_line = lines_obj.read(
            cursor, uid, self.lines, ['delivered', 'delivery_status']
        )[0]

        self.assertTrue(updated_line['delivered'])
        self.assertEqual(updated_line['delivery_status'], u'1')  # Entregat

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        fact_reads = fact_obj.read(
            cursor, uid, self.fact_id, ['pending_state', 'pending_history_ids']
        )

        self.assertEqual(
            fact_reads['pending_state'][0], self.aviso_pending_state
        )
