from osv import osv, fields
import time


class WizardBaixaSuports(osv.osv_memory):

    _name = 'wizard.baixa.suports'

    def action_baixa_suports(self, cursor, uid, ids, context=None):
        """
        De-activates suports and adds to an obra

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Suports ids
        :type ids: list(int)
        :param context: OpenERP context
        :type context: dict
        :return: None
        :rtype: None
        """

        at_suport_obj = self.pool.get("giscedata.at.suport")

        wizard = self.browse(cursor, uid, ids[0])

        suports_ids = context.get("active_ids")

        suports_write_data = dict.fromkeys(suports_ids, None)
        actual_obres = at_suport_obj.read(
            cursor, uid, suports_ids, ["obres"], context=context
        )

        for ident in suports_ids:
            actual_obres = at_suport_obj.read(
                cursor, uid, ident, ["obres"], context=context
            )
            suports_write_data[ident] = {
                "baixa": True,
                "data_baixa": wizard.data_baixa,
                "obres": [(6, 0, [wizard.obra.id]+actual_obres["obres"])],
                "active": False
            }
            at_suport_obj.write(
                cursor, uid, suports_ids, suports_write_data[ident], context=context
            )

        wizard.write(
            {'state': 'end'}
        )

    _columns = {
        "data_baixa": fields.date("Data baixa", required=True),
        "obra": fields.many2one("giscedata.obres.obra", "Obra", required=True),
        "state": fields.selection(
            [
                ("init", "Init"),
                ("end", "End")
            ], "State"),
    }

    _defaults = {
        "data_baixa": lambda *a: time.strftime("%Y-%m-%d"),
        'state': lambda *a: 'init',
    }


WizardBaixaSuports()
