# -*- coding: utf-8 -*-
{
    "name": "Obres per Trams AT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a Trams AT
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_at",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_at_view.xml",
        "wizard/wizard_baixa_suports_view.xml",
    ],
    "active": False,
    "installable": True
}
