# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataAtTram(osv.osv):
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_tram_at_rel',
            'tram_id',
            'obra_id',
            'Obres'
        )
    }


GiscedataAtTram()


class GiscedataAtSuport(osv.osv):
    _name = 'giscedata.at.suport'
    _inherit = 'giscedata.at.suport'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_suport_at_rel',
            'id',
            'obra_id',
            'Obres'
        )
    }


GiscedataAtSuport()
