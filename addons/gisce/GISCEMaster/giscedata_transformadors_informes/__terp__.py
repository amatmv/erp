# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Transformadors Informes",
    "description": """Informes dels transformadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Informes",
    "depends":[
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_transformadors_informes_view.xml"
    ],
    "active": False,
    "installable": True
}
