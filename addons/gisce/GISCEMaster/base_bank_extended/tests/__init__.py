# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *


class BaseBankExtendedTests(testing.OOTestCase):

    def test_defaults_partner(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {'partner_id': partner_id,
                 'nv': 'Street',
                 'pnp': 'Num'}
            )
            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid,  ['owner_id', 'owner_address_id'], context=ctx)

            expect(default_vals['owner_id']).to(
                equal(partner_id)
            )
            expect(default_vals['owner_address_id']).to(
                equal(address_id)
            )

    def test_create_values(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        country_obj = self.openerp.pool.get('res.country')
        state_obj = self.openerp.pool.get('res.country.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            country_id = country_obj.create(
                cursor, uid, {'name': 'New country', 'code': '99'})

            state_id = state_obj.create(
                cursor, uid,
                {
                    'name': 'New State',
                    'country_id': country_id,
                    'code': '999'
                 })

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'street': 'Street Num',
                    'country_id': country_id,
                    'state_id': state_id,
                    'zip': '55555',
                    'city': 'CITY'
                }
            )
            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid,  ['owner_id', 'owner_address_id'], context=ctx)

            expect(default_vals['owner_id']).to(
                equal(partner_id)
            )
            expect(default_vals['owner_address_id']).to(
                equal(address_id)
            )

            default_vals.update({
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': partner_id
            })

            bank_id = par_bnk_obj.create(
                cursor, uid, default_vals
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)

            expect(bank.owner_name).to(equal(u'Smith, John'))
            expect(bank.street).to(equal(u'Street Num'))
            expect(bank.zip).to(equal(u'55555'))
            expect(bank.city).to(equal(u'CITY'))
            expect(bank.country_id.id).to(equal(country_id))
            expect(bank.state_id.id).to(equal(state_id))


    def test_create_values_without_ctx(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        country_obj = self.openerp.pool.get('res.country')
        state_obj = self.openerp.pool.get('res.country.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            country_id = country_obj.create(
                cursor, uid, {'name': 'New country', 'code': '99'})

            state_id = state_obj.create(
                cursor, uid,
                {
                    'name': 'New State',
                    'country_id': country_id,
                    'code': '999'
                 })

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'street': 'Street Num',
                    'country_id': country_id,
                    'state_id': state_id,
                    'zip': '55555',
                    'city': 'CITY'
                }
            )

            vals = {
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': partner_id
            }
            bank_id = par_bnk_obj.create(
                cursor, uid, vals
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)

            expect(bank.owner_id.id).to(
                equal(partner_id)
            )
            expect(bank.owner_address_id.id).to(
                equal(address_id)
            )

            expect(bank.owner_name).to(equal(u'Smith, John'))
            expect(bank.street).to(equal(u'Street Num'))
            expect(bank.zip).to(equal(u'55555'))
            expect(bank.city).to(equal(u'CITY'))
            expect(bank.country_id.id).to(equal(country_id))
            expect(bank.state_id.id).to(equal(state_id))

    def test_update_values(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        country_obj = self.openerp.pool.get('res.country')
        state_obj = self.openerp.pool.get('res.country.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            country_id = country_obj.create(
                cursor, uid,
                {
                    'name': 'New country',
                    'code': '99'
                }
            )

            state_id = state_obj.create(
                cursor, uid,
                {
                    'name': 'New State',
                    'country_id': country_id,
                    'code': '999'
                }
            )

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'street': 'Street Num',
                    'country_id': country_id,
                    'state_id': state_id,
                    'zip': '55555',
                    'city': 'CITY'
                }
            )
            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid,  ['owner_id', 'owner_address_id'], context=ctx)

            expect(default_vals['owner_id']).to(
                equal(partner_id)
            )
            expect(default_vals['owner_address_id']).to(
                equal(address_id)
            )
            default_vals.pop('owner_address_id', None)

            default_vals.update({
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': partner_id
            })

            bank_id = par_bnk_obj.create(
                cursor, uid, default_vals
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)

            expect(bank.owner_name).to(equal(u'Smith, John'))
            expect(bank.street).to(be_false)
            expect(bank.zip).to(be_false)
            expect(bank.city).to(be_false)
            expect(bank.country_id.id).to(be_false)
            expect(bank.state_id.id).to(be_false)
            bank.write({'owner_address_id': address_id})
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.street).to(equal(u'Street Num'))
            expect(bank.zip).to(equal(u'55555'))
            expect(bank.city).to(equal(u'CITY'))
            expect(bank.country_id.id).to(equal(country_id))
            expect(bank.state_id.id).to(equal(state_id))

    def test_trigger_owner_name(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        country_obj = self.openerp.pool.get('res.country')
        state_obj = self.openerp.pool.get('res.country.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            country_id = country_obj.create(
                cursor, uid,
                {
                    'name': 'New country',
                    'code': '99'
                }
            )

            state_id = state_obj.create(
                cursor, uid,
                {
                    'name': 'New State',
                    'country_id': country_id,
                    'code': '999'
                }
            )

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'street': 'Street Num',
                    'country_id': country_id,
                    'state_id': state_id,
                    'zip': '55555',
                    'city': 'CITY'
                }
            )
            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid,  ['owner_id', 'owner_address_id'], context=ctx)

            expect(default_vals['owner_id']).to(
                equal(partner_id)
            )
            expect(default_vals['owner_address_id']).to(
                equal(address_id)
            )
            default_vals.pop('owner_address_id', None)

            default_vals.update({
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': partner_id
            })
            bank_id = par_bnk_obj.create(
                cursor, uid, default_vals
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.owner_name).to(equal(u'Smith, John'))
            partner_obj.write(
                cursor, uid, [partner_id],
                {
                    'name': 'Snow, John'
                }
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.owner_name).to(equal(u'Snow, John'))

    def test_trigger_address_simple(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'street': 'Street Num',
                    'zip': '55555',
                    'city': 'CITY'
                }
            )
            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid,  ['owner_id', 'owner_address_id'], context=ctx)

            expect(default_vals['owner_id']).to(
                equal(partner_id)
            )
            expect(default_vals['owner_address_id']).to(
                equal(address_id)
            )
            default_vals.update({
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': partner_id
            })
            bank_id = par_bnk_obj.create(
                cursor, uid, default_vals
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.street).to(equal(u'Street Num'))
            expect(bank.zip).to(equal(u'55555'))
            expect(bank.city).to(equal(u'CITY'))
            par_adr_obj.write(
                cursor, uid, [address_id],
                {
                    'street': 'RUE 13',
                    'zip': '12345',
                    'city': 'Winterfall'
                }
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.street).to(equal(u'RUE 13'))
            expect(bank.zip).to(equal(u'12345'))
            expect(bank.city).to(equal(u'Winterfall'))

    def test_trigger_address_many2one(self):
        partner_obj = self.openerp.pool.get('res.partner')
        par_adr_obj = self.openerp.pool.get('res.partner.address')
        par_bnk_obj = self.openerp.pool.get('res.partner.bank')
        country_obj = self.openerp.pool.get('res.country')
        state_obj = self.openerp.pool.get('res.country.state')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            country_id = country_obj.create(
                cursor, uid,
                {
                    'name': 'New country',
                    'code': '99'
                }
            )

            state_id = state_obj.create(
                cursor, uid,
                {
                    'name': 'New State',
                    'country_id': country_id,
                    'code': '999'
                }
            )

            partner_id = partner_obj.create(
                cursor, uid,
                {
                    'name': 'Smith, John',
                    'vat': 'ES12291804Y'
                }
            )
            address_id = par_adr_obj.create(
                cursor, uid,
                {
                    'partner_id': partner_id,
                    'street': 'Street Num',
                    'zip': '55555',
                    'city': 'CITY'
                }
            )
            ctx = {'partner_id': partner_id}
            default_vals = par_bnk_obj.default_get(
                cursor, uid,  ['owner_id', 'owner_address_id'], context=ctx)

            expect(default_vals['owner_id']).to(
                equal(partner_id)
            )
            expect(default_vals['owner_address_id']).to(
                equal(address_id)
            )
            default_vals.update({
                'state': 'iban',
                'iban': 'ES9431830800809999999999',
                'partner_id': partner_id
            })
            bank_id = par_bnk_obj.create(
                cursor, uid, default_vals
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.street).to(equal(u'Street Num'))
            expect(bank.zip).to(equal(u'55555'))
            expect(bank.city).to(equal(u'CITY'))
            par_adr_obj.write(
                cursor, uid, [address_id],
                {
                    'country_id': country_id,
                    'state_id': state_id,
                }
            )
            bank = par_bnk_obj.browse(cursor, uid, bank_id)
            expect(bank.country_id.id).to(equal(country_id))
            expect(bank.state_id.id).to(equal(state_id))
