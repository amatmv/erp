# -*- coding: utf-8 -*-
{
    "name": "Base Bank extended",
    "description": """
 * Add ResPartner as Owner
 * Add ResPartnerAddress as Owner Address """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base_iban",
        "account_payment"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml"
    ],
    "active": False,
    "installable": True
}
