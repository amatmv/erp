# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 12:45\n"
"PO-Revision-Date: 2018-05-16 10:06+0000\n"
"Last-Translator: Jaume  Florez Valenzuela <jflorez@gisce.net>\n"
"Language-Team: English (United States) (http://trad.gisce.net/projects/p/erp/language/en_US/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: base_bank_extended
#: model:ir.module.module,shortdesc:base_bank_extended.module_meta_information
msgid "Base Bank extended"
msgstr "Base Bank extended"

#. module: base_bank_extended
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: base_bank_extended
#: model:ir.module.module,description:base_bank_extended.module_meta_information
msgid ""
"\n"
" * Add ResPartner as Owner\n"
" * Add ResPartnerAddress as Owner Address "
msgstr "\n * Add ResPartner as Owner\n * Add ResPartnerAddress as Owner Address "

#. module: base_bank_extended
#: field:res.partner.bank,owner_id:0
msgid "Owner"
msgstr "Owner"

#. module: base_bank_extended
#: help:res.partner.bank,owner_id:0
msgid "If no owner is selected, the related partner will be used as owner"
msgstr "If no owner is selected, the related partner will be used as owner"

#. module: base_bank_extended
#: field:res.partner.bank,owner_address_id:0
msgid "Owner Address"
msgstr "Owner Address"
