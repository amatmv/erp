# -*- coding: utf-8 -*-
from osv import fields, osv


class ResPartnerBank(osv.osv):
    _name = 'res.partner.bank'
    _inherit = 'res.partner.bank'

    def _default_owner_id(self, cursor, uid, context=None):
        if not context:
            context = {}
        partner_id = context.get('partner_id', False)
        if partner_id:
            return partner_id
        else:
            return False

    def _default_owner_address_id(self, cursor, uid, context=None):
        if not context:
            context = {}
        partner_id = context.get('partner_id', False)
        if partner_id:
            partner_obj = self.pool.get('res.partner')
            partner_address = partner_obj.read(
                cursor, uid, partner_id, ['address'])
            address = partner_address.get('address', False)
            if address:
                return address[0]
        else:
            return False

    def fill_from_owner(self, cursor, uid, vals):
        if vals.get('owner_id', False):
            partner_obj = self.pool.get('res.partner')
            partner_name = partner_obj.read(
                cursor, uid, vals['owner_id'], ['name'])
            vals['owner_name'] = partner_name['name']

        if vals.get('owner_address_id', False):
            address_obj = self.pool.get('res.partner.address')
            fields_to_read = ['street', 'city', 'zip', 'country_id', 'state_id']
            address_data = address_obj.read(
                cursor, uid, vals['owner_address_id'], fields_to_read)
            vals['street'] = address_data['street']
            vals['city'] = address_data['city']
            vals['zip'] = address_data['zip']
            if address_data['country_id']:
                vals['country_id'] = address_data['country_id'][0]
            if address_data['state_id']:
                vals['state_id'] = address_data['state_id'][0]

        return True

    def create(self, cursor, uid, vals, context=None):
        if not vals.get('owner_id'):
            vals['owner_id'] = vals.get('partner_id', False)
            if not vals.get('owner_address_id'):
                if vals['owner_id']:
                    address_obj = self.pool.get('res.partner.address')
                    address_id = address_obj.search(
                        cursor, uid, [('partner_id', '=', vals['owner_id'])])
                    if address_id:
                        vals['owner_address_id'] = address_id[0]

        self.fill_from_owner(cursor, uid, vals)

        return super(ResPartnerBank,
                     self).create(cursor, uid, vals, context=context)

    def write(self, cursor, uid, ids, vals, context=None):

        self.fill_from_owner(cursor, uid, vals)

        return super(ResPartnerBank,
                     self).write(cursor, uid, ids, vals, context=context)

    def _owner_address(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        if not context:
            context = {}
        field_name = arg.get('field_name')
        field = self.fields_get(cursor, uid, [field_name])
        for owner_address in self.read(cursor, uid, ids, ['owner_address_id']):
            if owner_address['owner_address_id']:
                vals = {
                    'owner_address_id': owner_address['owner_address_id'][0]
                }
                self.fill_from_owner(cursor, uid, vals)
                bank_id = owner_address['id']
                if field[field_name]['type'] not in ('many2one'):
                    res[bank_id] = vals[field_name]
                elif field[field_name]['type'] in ('many2one'):
                    if vals.get(field_name, False):
                        res[bank_id] = vals[field_name]
        return res

    def _owner_name(self, cursor, uid, ids, field_name, arg, context=None):
        res = dict.fromkeys(ids, False)
        for owner in self.read(cursor, uid, ids, ['owner_id']):
            if not owner['owner_id']:
                continue
            vals = {'owner_id': owner['owner_id'][0]}
            self.fill_from_owner(cursor, uid, vals)
            res[owner['id']] = vals['owner_name']
        return res

    def _trg_owner_name(self, cursor, uid, ids, context=None):
        bank_obj = self.pool.get('res.partner.bank')
        banks_ids = bank_obj.search(cursor, uid, [('owner_id', 'in', ids)])
        return banks_ids

    def _trg_change_bank(self, cursor, uid, ids, context=None):
        return ids

    def _trg_owner_address(self, cursor, uid, ids, context=None):
        bank_obj = self.pool.get('res.partner.bank')
        banks_ids = bank_obj.search(
            cursor, uid, [('owner_address_id', 'in', ids)]
        )
        return banks_ids

    _columns = {
        'owner_id': fields.many2one(
            'res.partner', 'Owner', required=True, select=True,
            help=u"If no owner is selected, the related partner will be used as owner"
        ),
        'owner_address_id': fields.many2one(
            'res.partner.address', 'Owner Address', select=True),
        'owner_name': fields.function(
            _owner_name,  method=True, string="Account Owner", type="char",
            size=128, readonly=True, store={
                'res.partner.bank': (
                    _trg_change_bank, ['owner_id'], 10
                ),
                'res.partner': (_trg_owner_name, ['name'], 10)
            }
        ),
        'street': fields.function(
            _owner_address, method=True, string='Street', type="char", size=128,
            readonly=True, arg={'field_name': 'street'},
            store={
                'res.partner.bank': (
                    _trg_change_bank, ['owner_address_id'], 10
                ),
                'res.partner.address': (_trg_owner_address, ['street'], 10)
            }
        ),
        'zip': fields.function(
            _owner_address, method=True, string='Zip', type="char", size=24,
            readonly=True, arg={'field_name': 'zip'},
            store={
                'res.partner.bank': (
                    _trg_change_bank, ['owner_address_id'], 10
                ),
                'res.partner.address': (_trg_owner_address, ['zip'], 10)
            }
        ),
        'city': fields.function(
            _owner_address, method=True, string='City', type="char", size=128,
            readonly=True, arg={'field_name': 'city'},
            store={
                'res.partner.bank': (
                    _trg_change_bank, ['owner_address_id'], 10
                ),
                'res.partner.address': (_trg_owner_address, ['city'], 10)
            }
        ),
        'country_id': fields.function(
            _owner_address, method=True, string='Country', readonly=True,
            type="many2one", obj='res.country',
            arg={'field_name': 'country_id'},
            store={
                'res.partner.bank': (
                    _trg_change_bank, ['owner_address_id'], 10
                ),
                'res.partner.address': (_trg_owner_address, ['country_id'], 10)
            }
        ),
        'state_id': fields.function(
            _owner_address, method=True, string='State', readonly=True,
            type="many2one", obj='res.country.state',
            arg={'field_name': 'state_id'},
            store={
                'res.partner.bank': (
                    _trg_change_bank, ['owner_address_id'], 10
                ),
                'res.partner.address': (_trg_owner_address, ['state_id'], 10)
            }
        ),
    }

    _defaults = {
        'owner_id': _default_owner_id,
        'owner_address_id': _default_owner_address_id
    }


ResPartnerBank()
