SELECT
      pol.name AS "Cliente"
    , titular.name AS "NombreAbonado"
    , CASE WHEN i.type LIKE 'out_%%' THEN i.number
    WHEN i.type LIKE 'in_%%' THEN i.origin
    END AS factura
    , TO_CHAR(i.date_invoice, 'DD/MM/YYYY') AS "Fecha"
    , TO_CHAR(f.data_inici, 'DD/MM/YYYY') AS "Desde"
    , TO_CHAR(f.data_final, 'DD/MM/YYYY') AS "Hasta"
    , SUBSTRING(c.name,0,21) AS CUPS
    , CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-it_21.base,NULL)
        ELSE COALESCE(it_21.base,NULL)
    END AS "Base1"
    , CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-it_3.base,NULL)
        ELSE COALESCE(it_3.base,NULL)
    END AS "Base2"
    , COALESCE(REGEXP_REPLACE(it_21.name, '([^0-9]*)', '', 'g'),'') AS IVA1
    , COALESCE(REGEXP_REPLACE(it_3.name, '([^0-9]*)', '', 'g'),'') AS IVA2
    , CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-it_21.amount,NULL)
        ELSE COALESCE(it_21.amount,NULL)
    END AS "Cuota1"
    , CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN COALESCE(-it_3.amount,NULL)
        ELSE COALESCE(it_3.amount,NULL)
    END AS "Cuota2"
    , CASE
        WHEN i.type IN ('out_refund', 'in_refund') THEN -i.amount_total
        ELSE i.amount_total
      END AS "Importe"
    , SUBSTRING(pay.reference from '%%-#"%%#"' for '#') "Remesa"
FROM giscedata_facturacio_factura f
LEFT JOIN giscedata_cups_ps c ON (f.cups_id=c.id)
LEFT JOIN giscedata_polissa pol ON (pol.id=f.polissa_id)
LEFT JOIN res_partner AS titular ON (pol.titular=titular.id)
LEFT JOIN account_invoice i ON (i.id=f.invoice_id)
LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=i.id AND (it_21.name LIKE '21%% IVA%%' OR it_21.name LIKE 'IGIC 7%%'))
LEFT JOIN account_invoice_tax it_3 ON (it_3.invoice_id=i.id AND it_3.name LIKE 'IGIC 3%%')
LEFT JOIN payment_order pay ON (pay.id=i.payment_order_id)
WHERE
f.id in (%s)
ORDER BY pol.name