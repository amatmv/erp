# coding=utf-8
from oopgrade import oopgrade

import netsvc


def migrate(cursor, installed_version):
    if not installed_version:
        return

    logger = netsvc.Logger()

    logger.notifyChannel('new f1 summary migration', netsvc.LOG_INFO,
                         'Deleting old action for the wizard')

    cursor.execute(
        "DELETE FROM ir_actions WHERE name ILIKE 'Resum factures F1'"
    )

    logger.notifyChannel('new f1 summary migration', netsvc.LOG_INFO,
                         'Deleting old wizard\'s ir_model_data rows')

    cursor.execute(
        "DELETE FROM ir_model_data "
        "WHERE name LIKE '%giscedata_facturacio_switching_f1_resum_wizard%'"
    )

    logger.notifyChannel('new f1 summary migration', netsvc.LOG_INFO,
                         'All old data deleted')

up = migrate
