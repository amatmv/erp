# coding=utf-8
from oopgrade import oopgrade


def migrate(cursor, installed_version):
    if not installed_version:
        return
    cursor.execute(
        "UPDATE payment_order set paid = true WHERE state = 'done'")

up = migrate
