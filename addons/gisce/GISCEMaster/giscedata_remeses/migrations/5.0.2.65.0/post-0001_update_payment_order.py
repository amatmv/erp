# -*- coding: utf-8 -*-
"""
Update giscedata_facturacio_factura with payment_order_id from payment_line and move_line_id
"""

import netsvc

def migrate(cursor, installed_version):

    logger = netsvc.Logger()

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Copying from payment orders receivable '
                         'to giscedata_factures')
    query = """
        UPDATE giscedata_facturacio_factura
        SET payment_order_id = sub.order_id
        FROM (
            select po.id as order_id, ai.id as num_fact
            from payment_order po,
                 payment_line pol,
                 account_move_line aml,
                 account_invoice ai
            where po.id = pol.order_id and pol.move_line_id = aml.id
                  and ai.number = aml.ref
                  and ai.type in ('out_invoice', 'out_refund')
                  and po.type like 'receivable'
                  order by po.id desc
            ) as sub
        WHERE giscedata_facturacio_factura.invoice_id = sub.num_fact
    """
    cursor.execute(query)
    
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration finished')
