# -*- coding: utf-8 -*-
"""
Move payment_order_id from giscedata_facturacio_factura to account_invoice
"""

import netsvc


def migrate(cursor, installed_version):
    logger = netsvc.Logger()

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Move payment_order_id from '
                         'giscedata_facturacio_factura to account_invoice'
                         )

    query = """
            UPDATE account_invoice
            SET payment_order_id = ff.payment_order_id
            FROM giscedata_facturacio_factura AS ff
                WHERE account_invoice.id = ff.invoice_id
        """
    query_rm = """ALTER TABLE giscedata_facturacio_factura 
                    DROP COLUMN payment_order_id
                """

    cursor.execute(query)
    cursor.execute(query_rm)

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration finished')

up = migrate
