# -*- coding: utf-8 -*-

from osv import fields, osv
from tools.translate import _
from datetime import datetime
from collections import Counter
import json


_TYPES = [
    ('payable', 'A pagar'),
    ('receivable', 'A cobrar')
]


_STATES = [('init', 'Inici'),
           ('pendents', 'Pendents'),
           ('digest', 'Estadístiques'),
           ('end', 'Final')]
"""States of the wizard
"""


VALID_MODELS = [
    'giscedata.facturacio.lot',
    'giscedata.facturacio.factura',
]
"""Valid models to use this wizard
"""


class WizardAfegirFacturesRemesa(osv.osv_memory):
    """Wizard per afegir factures a una remesa des del lot
    """
    _name = 'wizard.afegir.factures.remesa'
    _description = __doc__

    def get_invoices(self, cursor, uid, context=None):
        """
        Gets selected invoices
        :return: list with invoices ids
        """
        if context is None:
            context = {}

        model = context.get('model')
        factura_ids = []
        if model == 'giscedata.facturacio.lot':
            fact_obj = self.pool.get('giscedata.facturacio.factura')
            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]
            factura_ids = fact_obj.search(
                cursor, uid, [('lot_facturacio', '=', lot_id)]
            )
            if not factura_ids:
                raise osv.except_osv(
                    _(u'Error'), _(u'No hi ha cap factura a afegir a la remesa!')
                )
        elif model == 'giscedata.facturacio.factura':
            factura_ids = context.get('active_ids', [])

        return factura_ids

    def default_over_limit_amount(self, cursor, uid, context=None):
        """
        Reads over limit amount config value
        :return: The value
        """
        conf_obj = self.pool.get('res.config')

        value = int(conf_obj.get(
            cursor, uid, 'add_to_payorder_over_limit_amount', 10000
        ))

        return value

    def get_grouped_invoices_ids(self, cursor, uid, fact_ids, context=None):
        """
        :return: els ids de les factures que estan agrupades amb les *fact_ids*
        """

        if not isinstance(fact_ids, (list, tuple)):
            fact_ids = [fact_ids]
        if not fact_ids:
            return []
        query = """
            SELECT fact.id
            FROM giscedata_facturacio_factura gff
            JOIN account_invoice ai on ai.id = gff.invoice_id
            JOIN (
                SELECT gff.id AS id, ai.group_move_id AS group_move_id
                FROM giscedata_facturacio_factura gff
                JOIN account_invoice ai on ai.id = gff.invoice_id
            ) AS fact ON gff.id != fact.id
            WHERE ai.group_move_id = fact.group_move_id
            AND gff.id IN %(fact_ids)s
        """
        cursor.execute(query, {'fact_ids': tuple(fact_ids)})
        return map(lambda elem: elem[0], cursor.fetchall())

    def get_invoices_info(self, cursor, uid, invoice_ids, context=None):
        """
         Gets digest info of selected invoices to audit the result and solve
         errors before payment order creation
        :return: dict with statistical info
        {
            'total_num': Number of selected invoices
            'total_amount': Total invoices amount
            'max_amount': the highest amount of selected invoices
            'over_limit_invoices': Num invoices with amount over limit €
            'negative': Num invoices with negative amount
            'max_date_due': More recent date due
            'min_date_due': Oldest date due
            'states_digest': dict with per state invoice count
        }
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        res = {
            'total_num': 0,
            'total_amount': 0.0,
            'max_amount': 0.0,
            'over_limit_invoices': 0,
            'negative': 0,
            'refund': 0,
            'max_due_date': '2010-01-01',
            'min_due_date': '2100-01-01',
            'origin_filled': 0,
            'states_digest': Counter(),
            'payment_type_digest': Counter(),
        }

        max_due_date = '2010-01-01'
        min_due_date = '2100-01-01'
        max_amount = 0.0

        invoice_fields = [
            'residual', 'invoice_id.date_due', 'invoice_id.state', 'type',
            'invoice_id.payment_type.name', 'invoice_id.origin'
        ]
        grouped_ids = self.get_grouped_invoices_ids(
            cursor, uid, invoice_ids, context
        )
        invoices_to_check_ids = grouped_ids + invoice_ids

        if invoices_to_check_ids:
            invoices_vals = fact_obj.q(cursor, uid).read(invoice_fields).where([
                ('id', 'in', invoices_to_check_ids)
            ])
            over_limit_amount = self.default_over_limit_amount(
                cursor, uid, context=context
            )
            digest = Counter()
            for invoice_vals in invoices_vals:
                digest += Counter({
                    'total_num': 1,
                    'total_amount': invoice_vals['residual'],
                    'over_limit_invoices': (
                        invoice_vals['residual'] > over_limit_amount and 1 or 0
                    ),
                    'negative': invoice_vals['residual'] < 0 and 1 or 0,
                    'refund': (
                        invoice_vals['type'] in ('out_refund', 'in_refund') and 1
                        or 0
                    ),
                    'origin_filled': (
                        len(invoice_vals['invoice_id.origin'] or '') > 0 and 1 or 0
                    ),
                })

                max_amount = max(max_amount, invoice_vals['residual'])
                max_due_date = max(max_due_date, invoice_vals['invoice_id.date_due'])
                min_due_date = min(min_due_date, invoice_vals['invoice_id.date_due'])

                if not invoice_vals['invoice_id.payment_type.name']:
                    payment_type = _('No definit')
                else:
                    payment_type = invoice_vals['invoice_id.payment_type.name']

                res['states_digest'] += Counter({invoice_vals['invoice_id.state']: 1})
                res['payment_type_digest'] += Counter({payment_type: 1})
            res.update(digest)

        res.update(
            dict(
                max_amount=max_amount,
                max_due_date=max_due_date or False,
                min_due_date=min_due_date or False,
            )
        )
        return dict(res)

    def default_model(self, cursor, uid, context=None):
        if not context:
            context = {}

        model = context.get('model')

        if model not in VALID_MODELS:
            raise osv.except_osv(
                'Error',
                u'Model {0} no és vàlid per utilitzar aquest assistent'.format(
                    model
                )
            )

        return model

    def update_ibans_invoices(self, cursor, uid, factures_ids, context=None):
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.pool.get('giscedata.polissa')
        for factura in fact_obj.read(cursor, uid, factures_ids, ['polissa_id']):
            polissa_id = factura['polissa_id'][0]
            polissa_info = polissa_obj.get_current_bank(
                cursor, uid, polissa_id, context=context
            )
            if polissa_info['partner_bank']:
                fact_obj.write(cursor, uid, [factura['id']], polissa_info)

        return True

    def action_afegir_factures(self, cursor, uid, ids, context):
        """Afegeix les factures del lot a la remesa
        """
        if not context:
            context = {}
        remesades = False
        model = context.get('model')
        if model not in VALID_MODELS:
            raise osv.except_osv(
                'Error',
                u'Model {0} no és vàlid per utilitzar aquest assistent'.format(
                    model
                )
            )
        if isinstance(ids, list):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids, context)

        if model == 'giscedata.facturacio.lot':
            lot_id = context.get('active_ids', False)
            if lot_id:
                lot_id = lot_id[0]
            lot_obj = self.pool.get('giscedata.facturacio.lot')
            lot = lot_obj.browse(cursor, uid, lot_id, context)
            ctx = context.copy()
            if wiz.invoices_limit:
                ctx['invoices_limit'] = wiz.invoices_limit
            if wiz.not_ordered:
                ctx['not_ordered'] = wiz.not_ordered
            if wiz.use_order_payment_type:
                ctx['use_order_payment_type'] = wiz.use_order_payment_type
            if wiz.work_async:
                ctx['work_async'] = wiz.work_async
                info = _(u"S'ha iniciat el procés de afegir factures a la "
                          u"remesa en segon plà.\nPots seguir el seu progrés "
                          u"desde el menú 'Tasques de Facturació'")
            else:
                info = ""
            try:
                lot.afegir_factures_remesa(wiz.order.id, context=ctx)
            except osv.except_osv, e:
                # It's the exeption raised when background is called, ignore it
                if e.name != u'Procés en background':
                    raise e
            wiz.write({'state': 'end', 'info': info})

        elif model == 'giscedata.facturacio.factura':
            invoice_ids = context.get('active_ids', [])
            factura_obj = self.pool.get('giscedata.facturacio.factura')
            none_ids = factura_obj.search(
                cursor, uid,
                ['&', ('payment_order_id', '=', False),
                 ('id', 'in', invoice_ids)]
            )
            remesades_ids = list(set(invoice_ids) - set(none_ids))
            if remesades_ids:
                # Existeixen factures ja remesades
                str_list = []
                for elem in remesades_ids:
                    str_list.append(str(elem))
                remesades_ids_str = ','.join(str_list)
                remesades = True
                info = _(u"Hi ha factures obertes que ja tenen remesa. "
                         u"Caldria revisar si són correctes.\n\n"
                         u"Les factures incorrectes s'han de suprimir del "
                         u"llistat.")
                wiz.write({'remesades': [[6, 0, remesades_ids]],
                           'info': info,
                           'factures_remesades_ids': str(remesades_ids_str)})
                wiz.write({'state': 'pendents'})
            else:
                factura_obj.afegeix_a_remesa(
                    cursor, uid, invoice_ids, wiz.order.id
                )
                wiz.write({'state': 'end'})

    def show_payment_order(self, cursor, uid, ids, context=None):
        if isinstance(ids, list):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids, context)
        return {
            "type": "ir.actions.act_window",
            "name": _("Ordre de cobrament: {0}".format(wiz.order.name)),
            "res_model": "payment.order",
            "domain": [('id', '=', wiz.order.id)],
            "view_mode": "tree,form",
            "view_type": "form"
        }

    def get_groupable_invoices(self, cursor, uid, ids, context=None):
        factures_lot_ids = self.get_invoices(cursor, uid, context)

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        fact_to_show_ids = []
        fields_to_read = ['partner_id', 'amount_total']
        for factura in fact_obj.read(cursor, uid, factures_lot_ids,
                                     fields_to_read):
            partner_id = factura['partner_id'] and factura['partner_id'][0]
            if partner_id:
                fact_id = factura['id']
                amount_total_actual = factura['amount_total']
                search_params = [
                    # que no tingui cap remesa
                    ('payment_order_id', '=', False),
                    # que no estigui pagada
                    ('reconciled', '=', False),
                    # que sigui del mateix partner
                    ('partner_id', '=', partner_id),
                    # amount_total + amount_total_actual > 0
                    ('amount_total', '>', -amount_total_actual),
                    # que no sigui la mateixa factura
                    ('id', '!=', fact_id)
                ]
                fact_to_show_ids += fact_obj.search(cursor, uid, search_params)

        fact_to_show_ids += factures_lot_ids
        fact_to_show_ids = list(set(fact_to_show_ids))

        res = {
            'name': _('Factures agrupables'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(fact_to_show_ids)),
        }

        return res

    def go_back(self, cursor, uid, ids, context):
        if not context:
            context = {}
        vals = {
            'state': 'init'
        }
        self.write(cursor, uid, ids, vals, context)

    def override_payment_order(self, cursor, uid, ids, context=None):
        if isinstance(ids, list):
            ids = ids[0]
        invoice_ids = context.get('active_ids', [])
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        wiz = self.browse(cursor, uid, ids, context)
        none_ids = fact_obj.search(
            cursor, uid,
            ['&', ('payment_order_id', '=', False),
             ('id', 'in', invoice_ids)]
        )
        if none_ids:
            fact_obj.afegeix_a_remesa(cursor, uid, none_ids, wiz.order.id)
        for elem in wiz.remesades:
            if wiz.update_iban:
                self.update_ibans_invoices(
                    cursor, uid, [elem.id], context=context
                )
            fact_obj.afegeix_a_remesa(cursor, uid, elem.id, wiz.order.id)
        wiz.write({'state': 'end'})

    def onchange_tipus(self, cursor, uid, ids, tipus, context=None):
        res = {'domain': {}}
        res['domain'].update(
            {'order': [('type', '=', tipus), ('state', '=', 'draft')]}
        )
        return res

    _columns = {
        'order': fields.many2one('payment.order', 'Remesa', required=True),
        'state': fields.selection(_STATES, 'Estats', required=True),
        'tipus': fields.selection(_TYPES, 'Tipus', required=True),
        'factures_remesades_ids': fields.json('IDS factures ja remesades'),
        'info': fields.text(_('Informació'), readonly=True),
        'remesades': fields.many2many('giscedata.facturacio.factura',
                                      'payment_order',
                                      'payment_order_id',
                                      'id',
                                      'Factures ja remesades'),
        'model': fields.char('Origin model', size=255),
        # digest fields
        'total_num': fields.integer('Num. Factures', readonly=True),
        'total_amount': fields.float(
            'Total Factures', digits=(9, 3), readonly=True
        ),
        'max_amount': fields.float(
            'Factura més alta', digits=(9, 3), readonly=True
        ),
        'over_limit_invoices': fields.integer(
            'Factures massa grans', readonly=True
        ),
        'over_limit_amount': fields.float(
            'Límit factures massa grans [€]', digits=(9, 3), readonly=True
        ),
        'negative': fields.integer('Factures negatives', readonly=True),
        'refund': fields.integer('Factures abonadores', readonly=True),
        'max_due_date': fields.date('Máxima data de venciment', readonly=True),
        'min_due_date': fields.date('Mínima data de venciment', readonly=True),
        'origin_filled': fields.integer(
            'Factures amb comentaris', readonly=True,
            help=u"Factures que tenen el camp origin amb contingut. En alguns "
                 u"casos es marquen les factures a vigilar omplint el camp "
                 u"origen"
        ),
        'states_digest': fields.text("Resum d'estats", readonly=True),
        'payment_type_digest': fields.text("Resum tipus de pagament", readonly=True),
        'invoices_limit': fields.integer(
            'Límit de factures',
            help=u'Indica el núm. màxim factures '
                 u'a remesar. Si es deixa a 0 es considerarà '
                 u'sense limit de factures'
        ),
        'not_ordered': fields.boolean(
            'No remesades',
            help=u'Només selecciona factures que no tinguin remesa'
        ),
        'update_iban': fields.boolean(
            'Actualitzar iban',
            help=u"Si es marca aquesta opció, s'actualitzarà l'IBAN de les "
                 u"factures que ja estaven remesades. "
        ),
        'work_async': fields.boolean(
            'Treball en segon plà',
            help=u"Es realitzarà el procés d'afegir factures a la remeses en "
                 u"segon plà. Pots seguir el seu progrés desde el menú "
                 u"'Tasques de Facturació'"
        ),
    }

    def default_get(self, cursor, uid, fields, context=None):
        """
        Sets Statistical fields values
        :return:
        """

        res = super(WizardAfegirFacturesRemesa, self).default_get(
            cursor, uid, fields, context
        )

        invoice_ids = self.get_invoices(cursor, uid, context=context)
        info = self.get_invoices_info(
            cursor, uid, invoice_ids, context=context
        )
        if info:
            res.update(info)
            states_digest_txt = '\n'.join(
                [': '.join([_(k), str(v)])
                 for k, v in info['states_digest'].items()]
            )
            payment_type_digest_txt = '\n'.join(
                [': '.join([k, str(v)])
                 for k, v in info['payment_type_digest'].items()
                 ]
            )

            res.update({
                'states_digest': states_digest_txt,
                'payment_type_digest': payment_type_digest_txt
            })

        return res

    _defaults = {
        'model': default_model,
        'tipus': lambda *a: 'receivable',
        'over_limit_amount': default_over_limit_amount,
        'state': lambda *a: 'init',
        'total_num': lambda *a: 0,
        'total_amount': lambda *a: 0.0,
        'max_amount': lambda *a: 0.0,
        'over_limit_invoices': lambda *a: 0,
        'negative': lambda *a: 0,
        'refund': lambda *a: 0,
        'max_due_date': lambda *a: '2010-01-01',
        'min_due_date': lambda *a: '2100-01-01',
        'origin_filled': lambda *a: 0,
        'invoices_limit': lambda *a: 0,
        'not_ordered': lambda *a: True,
        'work_async': lambda *a: False
    }

WizardAfegirFacturesRemesa()
