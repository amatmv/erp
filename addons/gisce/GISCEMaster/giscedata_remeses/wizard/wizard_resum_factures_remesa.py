# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from tools import config
import StringIO
import base64

from base_extended.excel import Sheet

CSV_HEADERS = [
    _(u'Cliente'),
    _(u'Nombre Abonado'),
    _(u'Factura'),
    _(u'Fecha'),
    _(u'Desde'),
    _(u'Hasta'),
    _(u'CUPS'),
    _(u'Base1'),
    _(u'Base2'),
    _(u'IVA1'),
    _(u'IVA2'),
    _(u'Cuota1'),
    _(u'Cuota2'),
    _(u'Importe'),
    _(u'Remesa')
]


class WizardResumFacturesRemesa(osv.osv_memory):
    """Wizard per facturar via F1"""
    _name = 'wizard.resum.factures.remesa'

    def generar_resumen(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)

        # search invoices by payment order id
        payment_order_id = wizard.payment_order_id.id

        inv_obj = self.pool.get('giscedata.facturacio.factura')
        inv_ids = inv_obj.search(
            cursor, uid, [('payment_order_id', '=', payment_order_id)]
        )

        sql_filename = u'{0}/giscedata_remeses/' \
                       u'sql/F1_invoices_list.sql'.format(config['addons_path'])
        sql = open(sql_filename, 'r').read()

        cursor.execute(sql % ','.join(str(x) for x in inv_ids or [0]))
        linies = cursor.fetchall()
        remesa_name = wizard.payment_order_id.reference

        output = StringIO.StringIO()
        # Arrange remesa name to match xlwt limitations (<31 chars)
        remesa_type = wizard.payment_order_id.type

        # This is only for provider invoices, which have a format
        # like 0031-00000000000000000001164283 and where we only want
        # the last digits (1164283)
        if remesa_type == 'payable' and '-' in remesa_name:
            remesa_parts = remesa_name.split('-')
            remesa_name = str(int(remesa_parts[1]))
        sheet_name = _(u'Remesa {0}').format(remesa_name)
        if len(sheet_name) > 30:
            sheet_name = sheet_name[:29]
        # The symbols : \ / ? * [ ] are not accepted in sheet names
        sheet_name = sheet_name.replace('/', '-')
        sheet = Sheet(sheet_name)
        sheet.add_row([_(h).encode('utf8') for h in CSV_HEADERS])
        for lin in linies:
            sheet.add_row(lin)

        sheet.save(output)
        res = {'xls': base64.b64encode(output.getvalue())}
        output.close()

        wizard.write({'state': 'done',
                      'name': '{0}.xls'.format(remesa_name),
                      'file': res['xls']})

    def _get_default_payment_order(self, cursor, uid, context=None):
        """If context contains active_ids and it only contains one id we return
        the id of the payment order for that invoice. Otherwise we return false.

        :param cursor: database cursor
        :param uid: user id
        :param context: openERP context
        :return: if context contains active_ids and it only contains one invoice
        id we return it's payment order's id. Otherwise false.
        """
        if context is None:
            return False

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        active_ids = context.get('active_ids', [])
        id_factura = False
        if active_ids:
            if len(active_ids) == 1:
                id_factura = active_ids[0]

        if id_factura:
            payment_order = fact_obj.read(
                cursor, uid, id_factura, ['payment_order_id']
            )['payment_order_id']
            if payment_order:
                return payment_order[0]

        return False

    _columns = {
        'file': fields.binary('Fitxer'),
        'name': fields.char('Nom', size=128),
        'payment_order_id': fields.many2one('payment.order', 'Remesa'),
        'state': fields.char('State', size=16),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'payment_order_id': _get_default_payment_order,
    }

WizardResumFacturesRemesa()
