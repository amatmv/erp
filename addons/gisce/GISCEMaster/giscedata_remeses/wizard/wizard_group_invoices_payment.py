from osv import osv, fields
from tools.translate import _


class WizardGroupInvoicesPayment(osv.osv_memory):
    _name = 'wizard.group.invoices.payment'
    _inherit = 'wizard.group.invoices.payment'


    def check_differents_remeses(self, cursor, uid, ids, context=None):
        """
        Returns True if there are invoices with diferent remeses (group_id).
        :param ids: account.invoice or giscedata.facturacio.factura ids
        """
        model = context and context.get("model", "account.invoice")
        obj = self.pool.get(model)
        remeses = []
        for invoice_info in obj.read(cursor, uid, ids, ['payment_order_id']):
            if invoice_info['payment_order_id'] not in remeses:
                remeses.append(invoice_info['payment_order_id'])
            if len(remeses) > 1:
                return True
        return False

    def agrupar_remesar(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        imd_obj = self.pool.get('ir.model.data')

        self.group_invoices(cursor, uid, ids, context=context)
        wiz = self.browse(cursor, uid, ids[0], context=context)

        res = {
            'name': _('Remesar factures agrupades'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.afegir.factures.remesa',
            'context': {
                'model': 'giscedata.facturacio.factura',
                'active_ids': [context.get('active_ids')[0]]
            },
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

        if wiz.amount_total < 0:
            res['view_id'] = (imd_obj.get_object_reference(
                cursor, uid, 'giscedata_remeses',
                'view_afegir_factures_remesa_payable_form'
            )[1],)

        return res

    def _get_default_info(self, cr, uid, context):
        fids = context.get('active_ids', [])
        if self.check_differents_remeses(cr, uid, fids, context=context):
            return _(u"Warning!\n"
                     u"S'estan intentant agrupar factures amb remeses diferents!")
        return ""

    _columns = {
        'info': fields.text('Info')
    }

    _defaults = {
        'info': _get_default_info
    }

WizardGroupInvoicesPayment()
