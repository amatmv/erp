# -*- coding: utf-8 -*-
{
    "name": "GISCE Remeses",
    "description": """Modificació de remeses de facturació""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "Facturació",
    "depends":[
        "remeses_base",
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml":[
        "payment_order_demo.xml"
    ],
    "update_xml":[
        "wizard/wizard_resum_factures_remesa_view.xml",
        "wizard/wizard_group_invoices_payment_view.xml",
        "wizard/afegir_factures_remesa_view.xml",
        "payment_view.xml",
        "giscedata_facturacio_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
