# -*- coding: utf-8 -*-
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction


class TestWizardChangeDatesFact(testing.OOTestCase):
    def test_default_payment_order_is_false_on_no_context(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.resum.factures.remesa'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            wiz_id = wiz_obj.create(cursor, uid, {}, context=None)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            assert not wizard.payment_order_id

    def test_default_payment_order_is_false_on_no_active_ids(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.resum.factures.remesa'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            wiz_id = wiz_obj.create(cursor, uid, {}, context={})
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            assert not wizard.payment_order_id

    def test_default_payment_order_is_false_on_more_than_one_active_id(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.resum.factures.remesa'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {'active_ids': [1, 2]}

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            assert not wizard.payment_order_id

    def test_default_payment_order_is_correct_on_one_active_id(self):
        wiz_obj = self.openerp.pool.get(
            'wizard.resum.factures.remesa'
        )
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0003'
            )[1]
            pay_ord_id = imd_obj.get_object_reference(
                cursor, uid, 'account_payment', 'payment_order_demo'
            )[1]

            fact_obj.write(
                cursor, uid, fact_id, {'payment_order_id': pay_ord_id}
            )

            context = {'active_ids': [fact_id]}

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wizard = wiz_obj.browse(cursor, uid, wiz_id)

            assert wizard.payment_order_id.id == pay_ord_id

    def test_sql_query_works_correctly_for_providers(self):
        factura_0003 = [
            (
                u'0001',
                u'Camptocamp',
                None,
                u'01/07/2016',
                u'01/05/2016',
                u'30/06/2016',
                u'ES1234000000000001JN',
                None,
                None,
                u'',
                u'',
                None,
                None,
                10.0,
                None
            )
        ]

        sql_path = get_module_resource(
            'giscedata_remeses', 'sql', 'F1_invoices_list.sql')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with open(sql_path, 'r') as f:
            sql_query = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0003'
            )[1]

            cursor.execute(sql_query % fact_id)

            self.assertListEqual(cursor.fetchall(), factura_0003)

    def test_sql_query_works_correctly_for_clients(self):
        factura_0001 = [
            (
                u'0001',
                u'Camptocamp',
                u'0001/F',
                u'01/03/2016',
                u'01/01/2016',
                u'29/02/2016',
                u'ES1234000000000001JN',
                None,
                None,
                u'',
                u'',
                None,
                None,
                10.0,
                None
            )
        ]

        sql_path = get_module_resource(
            'giscedata_remeses', 'sql', 'F1_invoices_list.sql')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with open(sql_path, 'r') as f:
            sql_query = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            cursor.execute(sql_query % fact_id)

            self.assertListEqual(cursor.fetchall(), factura_0001)
