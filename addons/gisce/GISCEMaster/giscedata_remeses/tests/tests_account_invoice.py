# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import netsvc
from osv import osv


class AccountInvoiceTests(testing.OOTestCase):
    def test_invoice_function_is_not_payable_with_payment_order(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        remesa_obj = pool.get('payment.order')
        invoice_obj = pool.get('account.invoice')
        factura_obj = pool.get('giscedata.facturacio.factura')
        wz_afegir_factures = pool.get('wizard.afegir.factures.remesa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, factura_id, ['invoice_id'])['invoice_id'][0]

            remesa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_remeses', 'remesa_0001'
            )[1]

            # Create payment order not finished
            remesa_obj.write(
                cursor, uid, [remesa_id], {'state': 'draft', 'paid': False}
            )

            # Add the invoice to the payment order
            ctx = {
                'model': 'giscedata.facturacio.factura',
                'active_ids': [factura_id]
            }
            wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
            wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

            wiz.write({'order': remesa_id})
            res = wz_afegir_factures.action_afegir_factures(
                cursor, uid, [wz_id], context=ctx
            )
            factura_obj.write(
                cursor, uid, [factura_id],
                {
                    'state': 'open',
                    'reconciled': False,
                    'payment_order_id': remesa_id
                }
            )

            # Assert invoice is not payable with payment order not finished
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id)
            self.assertTrue(is_not_payable)

            # Set the payment order as finished and paid
            remesa_obj.write(
                cursor, uid, [remesa_id], {'state': 'done', 'paid': True}
            )

            # Assert the invoice is payable
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id)
            self.assertFalse(is_not_payable)

            # Set the payment order as finished and not paid
            remesa_obj.write(
                cursor, uid, [remesa_id], {'state': 'done', 'paid': False}
            )

            # Assert the invoice is payable
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id)
            self.assertFalse(is_not_payable)

            # Set the payment order as cancelled
            remesa_obj.write(
                cursor, uid, [remesa_id], {'state': 'cancel'}
            )
            # Assert the invoice is payable
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id)
            self.assertFalse(is_not_payable)

    def test_order_raise_exception_if_paid_try_to_doner_to_cancel(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        remesa_obj = pool.get('payment.order')
        invoice_obj = pool.get('account.invoice')
        factura_obj = pool.get('giscedata.facturacio.factura')
        wz_afegir_factures = pool.get('wizard.afegir.factures.remesa')
        wf_service = netsvc.LocalService("workflow")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, factura_id, ['invoice_id'])['invoice_id'][0]

            remesa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_remeses', 'payment_mode_0001'
            )[1]

            remesa_info = remesa_obj.read(
                cursor, uid, remesa_id, ['state', 'paid']
            )
            self.assertFalse(remesa_info['paid'])
            self.assertEqual(remesa_info['state'], 'draft')

            # Add the invoice to the payment order
            ctx = {
                'model': 'giscedata.facturacio.factura',
                'active_ids': [factura_id]
            }
            wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
            wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

            wiz.write({'order': remesa_id})
            res = wz_afegir_factures.action_afegir_factures(
                cursor, uid, [wz_id], context=ctx
            )

            factura_obj.write(
                cursor, uid, [factura_id],
                {
                    'payment_order_id': remesa_id
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            fact_reads = factura_obj.read(cursor, uid, factura_id,
                                          [
                                              'state', 'reconciled',
                                              'payment_order_id'
                                          ]
                                          )

            self.assertFalse(fact_reads['reconciled'])
            self.assertEqual(fact_reads['payment_order_id'][0], remesa_id)
            self.assertEqual(fact_reads['state'], 'open')

            # Assert invoice is not payable with payment order not finished
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id)
            self.assertTrue(is_not_payable)


            wf_service.trg_validate(
                uid, 'payment.order', remesa_id, 'open', cursor

            )

            rem_reads = remesa_obj.read(
                cursor, uid, remesa_id, ['state', 'paid']
            )

            self.assertEqual(rem_reads['state'], 'open')

            wf_service.trg_validate(
                uid, 'payment.order', remesa_id, 'done', cursor
            )

            # Todo revisar porque no se pone a done a traves del workflow

            remesa_obj.write(cursor, uid, remesa_id, {'state': 'done', 'paid': True})

            rem_reads = remesa_obj.read(
                cursor, uid, remesa_id, ['state', 'paid']
            )

            self.assertEqual(rem_reads['state'], 'done')
            self.assertTrue(rem_reads['state'])

            with self.assertRaises(osv.except_osv) as err_rem_pagada:
                wf_service.trg_validate(
                    uid, 'payment.order', remesa_id, 'cancel', cursor
                )
            self.assertIn(
                u'La remesa ja està pagada',
                err_rem_pagada.exception.message
            )

            rem_state = remesa_obj.read(
                cursor, uid, remesa_id, ['state']
            )['state']

            self.assertEqual(rem_state, 'done')

    def test_order_dont_raise_exception_if_not_paid_try_to_doner_to_cancel(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        remesa_obj = pool.get('payment.order')
        invoice_obj = pool.get('account.invoice')
        factura_obj = pool.get('giscedata.facturacio.factura')
        wz_afegir_factures = pool.get('wizard.afegir.factures.remesa')
        wf_service = netsvc.LocalService("workflow")
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, factura_id, ['invoice_id'])['invoice_id'][0]

            remesa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_remeses', 'payment_mode_0001'
            )[1]

            remesa_info = remesa_obj.read(
                cursor, uid, remesa_id, ['state', 'paid']
            )
            self.assertFalse(remesa_info['paid'])
            self.assertEqual(remesa_info['state'], 'draft')

            # Add the invoice to the payment order
            ctx = {
                'model': 'giscedata.facturacio.factura',
                'active_ids': [factura_id]
            }
            wz_id = wz_afegir_factures.create(cursor, uid, {}, ctx)
            wiz = wz_afegir_factures.browse(cursor, uid, wz_id, ctx)

            wiz.write({'order': remesa_id})
            res = wz_afegir_factures.action_afegir_factures(
                cursor, uid, [wz_id], context=ctx
            )

            factura_obj.write(
                cursor, uid, [factura_id],
                {
                    'payment_order_id': remesa_id
                }
            )

            wf_service.trg_validate(
                uid, 'account.invoice', invoice_id, 'invoice_open', cursor
            )

            fact_reads = factura_obj.read(cursor, uid, factura_id,
                                          [
                                              'state', 'reconciled',
                                              'payment_order_id'
                                          ]
                                          )

            self.assertFalse(fact_reads['reconciled'])
            self.assertEqual(fact_reads['payment_order_id'][0], remesa_id)
            self.assertEqual(fact_reads['state'], 'open')

            # Assert invoice is not payable with payment order not finished
            is_not_payable = invoice_obj.is_not_payable_manually(
                cursor, uid, invoice_id)
            self.assertTrue(is_not_payable)


            wf_service.trg_validate(
                uid, 'payment.order', remesa_id, 'open', cursor

            )

            rem_reads = remesa_obj.read(
                cursor, uid, remesa_id, ['state', 'paid']
            )

            self.assertEqual(rem_reads['state'], 'open')

            wf_service.trg_validate(
                uid, 'payment.order', remesa_id, 'done', cursor
            )

            # Todo revisar porque no se pone a done a traves del workflow

            remesa_obj.write(cursor, uid, remesa_id, {'state': 'done', 'paid': False})

            rem_reads = remesa_obj.read(
                cursor, uid, remesa_id, ['state', 'paid']
            )

            self.assertEqual(rem_reads['state'], 'done')
            self.assertTrue(rem_reads['state'])
            self.assertFalse(rem_reads['paid'])

            wf_service.trg_validate(
                uid, 'payment.order', remesa_id, 'cancel', cursor
            )

            rem_state = remesa_obj.read(
                cursor, uid, remesa_id, ['state']
            )['state']

            self.assertEqual(rem_state, 'cancel')

            # Now we test cancel -> draft -> open

            # wf_service.trg_validate(
            #     uid, 'payment.order', remesa_id, 'draft', cursor
            #
            # )
            # Todo hacer que el workflow ejecute los canvios i no una funcion
            remesa_obj.set_to_draft(cursor, uid, [remesa_id])

            rem_state = remesa_obj.read(
                cursor, uid, remesa_id, ['state']
            )['state']

            self.assertEqual(rem_state, 'draft')

            # wf_service.trg_validate(
            #     uid, 'payment.order', remesa_id, 'done', cursor
            #
            # )
            # Todo hacer que el workflow ejecute los canvios i no una funcion
            remesa_obj.set_done(cursor, uid, remesa_id)

            rem_state = remesa_obj.read(
                cursor, uid, remesa_id, ['state']
            )['state']

            self.assertEqual(rem_state, 'done')
