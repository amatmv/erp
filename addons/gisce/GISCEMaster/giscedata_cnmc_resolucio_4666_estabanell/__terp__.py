# -*- coding: utf-8 -*-
{
    "name": "GISCE CNMC 4666",
    "description": """Este modulo customiza la resolucion de la 4666""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_cnmc_resolucio_4666"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
