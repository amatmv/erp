# -*- coding: utf-8 -*-

from osv import osv, fields


class PolissaIeseExemption(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _field_states={'esborrany': [('readonly', False)],
                   'validar': [('readonly', False)],
                   'modcontractual': [('readonly', False)]
                  }

    _columns = {
        'fiscal_position_id': fields.many2one('account.fiscal.position',
                                              'Fiscal position',
                                              readonly=True,
                                              states=_field_states),
        'cie': fields.char('CIE', size=30, readonly=True,
                           states=_field_states) 
    }

PolissaIeseExemption()


class PolissaModcontractualIeseExemption(osv.osv):

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'fiscal_position_id': fields.many2one('account.fiscal.position',
                                                   'Fiscal position'),
        'cie': fields.char('CIE', size=20) 
    }

PolissaModcontractualIeseExemption()
