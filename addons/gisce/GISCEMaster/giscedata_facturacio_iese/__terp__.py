# -*- coding: utf-8 -*-
{
    "name": "IESE Exemption",
    "description": """
    IESE reductions or exemptions
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_facturacio",
        "account_chart_iese",
        "account_tax_factor"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "iese_data.xml",
        "polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
