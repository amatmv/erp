# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataFacturacioFactura(osv.osv):
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        if context is None:
            context = {}
        res = super(GiscedataFacturacioFactura, self).onchange_polissa(
            cursor, uid, ids, polissa_id, type_, context=context
        )
        fiscal_position = res['value'].get('fiscal_position', False)
        if polissa_id:
            polissa_obj = self.pool.get('giscedata.polissa')
            polissa = polissa_obj.read(
                cursor, uid, polissa_id, ['fiscal_position_id'], context=context
            )
            if polissa['fiscal_position_id']:
                fiscal_position = polissa['fiscal_position_id'][0]
        res['value']['fiscal_position'] = fiscal_position

        return res

GiscedataFacturacioFactura()


class FacturadorIeseExemption(osv.osv):

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def create_invoice(self, cursor, uid, modcontractual_id, data_inici,
                       data_final, context=None):

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        factura_id = super(FacturadorIeseExemption,
                           self).create_invoice(cursor, uid, modcontractual_id,
                                                data_inici, data_final,
                                                context=context)

        modcon_vals = modcon_obj.read(cursor, uid, modcontractual_id,
                                      ['fiscal_position_id'], context=context)
        if modcon_vals['fiscal_position_id']:
            fact_vals = {'fiscal_position':
                                modcon_vals['fiscal_position_id'][0]}
            fact_obj.write(cursor, uid, factura_id, fact_vals,
                           context=context)
        return factura_id        

FacturadorIeseExemption()
