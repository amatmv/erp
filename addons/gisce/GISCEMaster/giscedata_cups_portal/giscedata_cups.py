# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCupsPs(osv.osv):
    """CUPS.
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def _direccio(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataCupsPs,
                     self)._direccio(cursor, uid, ids, field_name, arg,
                                     context)

    def _get_cups_ids(self, cursor, uid, ids, context=None):
        return super(GiscedataCupsPs,
                     self)._get_cups_ids(cursor, uid, ids, context)

    def get_direccio_fields(self, cursor, uid, field_name, context=None):
        d_fields = super(GiscedataCupsPs,
                         self).get_direccio_fields(cursor, uid, field_name,
                                                   context)
        if field_name == 'direccio':
            try:
                idx = d_fields['fields_normal'].index('pnp')
                d_fields['fields_normal'].insert(idx + 1, 'portal')
            except ValueError:
                d_fields['fields_normal'] += ['portal']
        return d_fields

    _columns = {
        'portal': fields.char('Portal', size=64),
        'direccio': fields.function(_direccio, method=True, string="Direcció",
            store={'giscedata.cups.ps': (
                        _get_cups_ids,
                        ['tv', 'nv', 'pnp', 'es', 'pt', 'pu', 'portal', 'cpo',
                         'cpa', 'dp', 'id_municipi', 'localitat', 'aclarador'],
                        10
                    )
            }, type='char', size=256),
    }

GiscedataCupsPs()
