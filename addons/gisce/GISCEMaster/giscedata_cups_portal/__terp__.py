# -*- coding: utf-8 -*-
{
    "name": "CUPS Portal",
    "description": """
    This module provide :
        * Camp portal pel CUPS
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
