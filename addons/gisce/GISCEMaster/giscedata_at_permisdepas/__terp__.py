# -*- coding: utf-8 -*-
{
    "name": "Permís de Pas (AT)",
    "description": """
    This module provide :
      * Permet definir els permisos de pas que té un tram de línia AT: 
      Contacte, data i observacions
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_at_permisdepas_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
