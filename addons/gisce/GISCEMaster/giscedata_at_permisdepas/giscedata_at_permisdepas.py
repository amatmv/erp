# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_at_permisdepas(osv.osv):

    _name = 'giscedata.at.permisdepas'

    _columns = {
        'name': fields.many2one('res.partner.address', 'Contacte', required=True),
        'tram_id': fields.many2one('giscedata.at.tram', 'Tram', required=True, ondelete='cascade'),
        'data_permis': fields.date('Data del permís'),
        'observacions': fields.text('Observacions')
    }

    _order = 'data_permis desc'

giscedata_at_permisdepas()
