# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_at_tram(osv.osv):

    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _columns = {
      'permisdepas_ids': fields.one2many('giscedata.at.permisdepas', 'tram_id', 'Tram'),
    }

giscedata_at_tram()
