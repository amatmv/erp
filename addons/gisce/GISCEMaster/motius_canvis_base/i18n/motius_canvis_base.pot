# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* motius_canvis_base
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-07-29 11:42\n"
"PO-Revision-Date: 2019-07-29 11:42\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: motius_canvis_base
#: selection:giscedata.motiu.canvi,type:0
msgid "Baixa"
msgstr ""

#. module: motius_canvis_base
#: selection:giscedata.motiu.canvi,type:0
msgid "Cancelació"
msgstr ""

#. module: motius_canvis_base
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: motius_canvis_base
#: selection:giscedata.motiu.canvi,type:0
msgid "Alta"
msgstr ""

#. module: motius_canvis_base
#: field:giscedata.motiu.canvi,codi:0
msgid "Codi"
msgstr ""

#. module: motius_canvis_base
#: model:ir.module.module,description:motius_canvis_base.module_meta_information
msgid "Mòdul base que afegeix els motius de alta, baixa i tall per a les pólisses"
msgstr ""

#. module: motius_canvis_base
#: field:giscedata.motiu.canvi,name:0
msgid "Descripcio"
msgstr ""

#. module: motius_canvis_base
#: model:ir.model,name:motius_canvis_base.model_giscedata_motiu_canvi
msgid "giscedata.motiu.canvi"
msgstr ""

#. module: motius_canvis_base
#: model:ir.module.module,shortdesc:motius_canvis_base.module_meta_information
msgid "Mòdul base per als motius d'alta, baixa i tall per pólisses"
msgstr ""

#. module: motius_canvis_base
#: field:giscedata.motiu.canvi,type:0
msgid "Tipus"
msgstr ""

