# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Updating access rules from giscedata_polissa_motius_canvis and setting 
        them to motius_canvis_base module.
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module='motius_canvis_base'
        WHERE module = 'giscedata_polissa_motius_canvis'
        AND name = 'model_giscedata_motiu_canvi'
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
