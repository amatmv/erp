# -*- coding: utf-8 -*-
{
    "name": "Mòdul base per als motius d'alta, baixa i tall per pólisses",
    "description": """Mòdul base que afegeix els motius de alta, baixa i tall per a les pólisses""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "security/motius_canvis_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
