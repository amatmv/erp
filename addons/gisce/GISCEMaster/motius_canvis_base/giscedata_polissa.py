# -*- coding: utf-8 -*-
from osv import osv, fields

tipus_canvis = [
    ('alta', u'Alta'),
    ('baixa', u'Baixa'),
    ('cancelacio', u'Cancelació')
]


class GiscedataMotiuCanvi(osv.osv):

    _name = 'giscedata.motiu.canvi'

    _columns = {
        'codi': fields.char('Codi', size=3, required=True, select=1),
        'name': fields.char('Descripcio', size=64, required=True, select=1),
        'type': fields.selection(tipus_canvis, 'Tipus', required=True, select=1)
    }


GiscedataMotiuCanvi()
