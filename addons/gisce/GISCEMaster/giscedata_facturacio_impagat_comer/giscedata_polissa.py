# -*- coding: utf-8 -*-
import logging

from osv import fields, osv
from tools.translate import _
from osv.orm import OnlyFieldsConstraint
from giscedata_polissa_comer.giscedata_polissa import (
    STORE_PROC_BONOSOCIAL_FIELD
)


class GiscedataPolissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def generar_reposicio(self, cursor, uid, polissa_id, data_inici, data_final, context=None):
        logger = logging.getLogger('openerp.{}.generar_reposicio'.format(
            __name__
        ))
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.pool.get('giscedata.polissa')
        facturador = self.pool.get('giscedata.facturacio.facturador')
        journal_obj = self.pool.get('account.journal')
        product_obj = self.pool.get('product.product')
        config_obj = self.pool.get('res.config')

        ctx = context
        ctx['date'] = data_final
        polissa = polissa_obj.browse(cursor, uid, polissa_id, context=context)
        lang = polissa.direccio_notificacio.partner_id.lang

        mod_id = polissa_obj._get_modcon_date(
            cursor, uid, polissa_id, data_final
        )

        logger.info((
            u'Generem factura reposició per la pòlissa {polissa.name} i mod '
            u'contractual id {mod_id} per {data_inici} - '
            u'{data_final}'

        ).format(
            polissa=polissa, mod_id=mod_id, data_inici=data_inici,
            data_final=data_final
        ))

        repo_id = facturador.create_invoice(
            cursor, uid, mod_id, data_inici, data_final
        )
        journal_name = config_obj.get(
            cursor, uid, 'fact_impagat_comer_journal_name', 'ENERGIA'
        )
        journal_id = journal_obj.search(cursor, uid, [
            ('code', '=', journal_name)
        ])[0]
        fact_obj.write(cursor, uid, [repo_id], {
            'journal_id': journal_id,
            'lot_facturacio': False
        })
        product_id = product_obj.search(cursor, uid, [
            ('default_code', '=', 'CON19')
        ])[0]
        ctx = context.copy()
        if lang:
            ctx['lang'] = lang
        product_data = product_obj.read(
            cursor, uid, product_id,
            ['name', 'description_sale', 'description'], context=ctx
        )

        product_name = (
            product_data['description_sale'] or
            product_data['description'] or
            product_data['name']
        )
        # El preu es pot configurar a través del producte (precio coste)
        # o amb una llista de preu
        facturador.crear_linies_altres(cursor, uid, repo_id, {
            'name': product_name,
            'product_id': product_id,
            'quantity': 1,
            'pricelist_base_price': 1.0
        })
        fact_obj.button_reset_taxes(cursor, uid, [repo_id], context=context)        
        
        return repo_id

    def _get_default_process(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        pending_state_process_obj = self.pool.get(
            'account.invoice.pending.state.process')
        return pending_state_process_obj.get_default_process(
            cursor, uid, context)

    def get_bono_social_process(self, cursor, uid, context=None):
        return self.pool.get('ir.model.data').get_object_reference(
            cursor, uid, 'giscedata_facturacio_comer_bono_social',
            'bono_social_pending_state_process'
        )[1]

    def wrapper_constraint_process_id(self, cursor, uid, ids):
        bono_social_process_id = self.get_bono_social_process(cursor, uid)

        res = True

        fields_to_read_polissa = ['bono_social_disponible', 'process_id']

        for pol in self.read(cursor, uid, ids, fields_to_read_polissa):
            if pol['bono_social_disponible']:
                pol_process = pol['process_id']
                res = pol_process and pol_process[0] == bono_social_process_id
                if not res:
                    break
        return res

    def _check_process_id(self, cursor, uid, ids):
        return self.wrapper_constraint_process_id(cursor, uid, ids)

    def _get_pending_states(self, cursor, uid, polissa_id, context=None):
        '''Returns dictionary with pending default values'''

        if context is None:
            context = {}

        state_obj = self.pool.get('account.invoice.pending.state')
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_process_id = polissa_obj.read(
            cursor, uid, polissa_id, ['process_id']
        )['process_id'][0]
        res = {}

        state_ids = state_obj.search(
            cursor, uid, [('process_id.id', '=', polissa_process_id)],
            context=context
        )
        for state in state_obj.read(cursor, uid, state_ids, ['name', 'weight'], context=context):
            res.update({state['weight']: state['name']})
        return res

    @staticmethod
    def get_max_weight(weight, factura):
        fact_process_id = factura.pending_state.process_id.id
        polissa_process_id = factura.polissa_id.process_id.id
        if fact_process_id == polissa_process_id:
            return max(weight, factura.pending_state.weight)
        return weight

    def _fnct_inv_set_process(self, cursor, uid, ids, name, value, fnct_inv_arg,
                              context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        cursor.execute(
            "UPDATE giscedata_polissa "
            "SET process_id = %s"
            "WHERE id IN %s",
            (value or None, tuple(ids))
        )
        return True

    def get_polisses_force_process_id(self, cursor, uid, ids, field_name, args,
                                      context=None):
        """
        Funció que retorna el procés de tall a les pòlisses que se'ls ha de
        forçar el process_id.
        Retorna un diccionari amb les ids on el valor és el procés de tall
        """
        if context is None:
            context = {}
        polisses_bono_social_disp = self._aplicacion_bono(
            cursor, uid, ids, field_name, args, context=context
        )
        bono_social_process_id = self.get_bono_social_process(cursor, uid)
        default_process_id = self._get_default_process(cursor, uid, None)
        res = {}
        for pol_id in ids:
            if polisses_bono_social_disp[pol_id]:
                res[pol_id] = bono_social_process_id
            else:
                if not context.get("ignore_default_process_id"):
                    res[pol_id] = default_process_id
        return res

    def wrapper_ff_process_id(self, cursor, uid, ids, field_name, args,
                              context=None):
        """
        Funció wrapper pel camp funció process_id per permetre sobreescriure-la

        Retorna el procés de tall per les pòlisses si ja en tenien un i es força
        el procés de tall per les pòlisses que se'ls ha de forçar
        """
        pending_state_process_obj = self.pool.get(
            'account.invoice.pending.state.process'
        )
        default_process_id = pending_state_process_obj.get_default_process(
            cursor, uid, context=context
        )
        res = dict.fromkeys(ids, default_process_id)

        polisses_process = self.read(
            cursor, uid, ids, ['process_id'], context=context
        )
        for polissa_process in polisses_process:
            polissa_id = polissa_process['id']
            process_id = (
                polissa_process['process_id'] and
                polissa_process['process_id'][0]
            )
            if process_id:
                res[polissa_id] = process_id

        # Nomes volem forçar els processos que no son el per defecte, es a dir, els "especials" (bo social, no cortable, etc.)
        ctx = context.copy()
        ctx['ignore_default_process_id'] = True
        dict_polisses_force_process_id = self.get_polisses_force_process_id(
            cursor, uid, ids, field_name, args, context=ctx
        )

        res.update(dict_polisses_force_process_id)
        return res

    def _ff_process_id(self, cursor, uid, ids, field_name, args, context=None):
        return self.wrapper_ff_process_id(
            cursor, uid, ids, field_name, args, context=context
        )

    _columns = {
        'process_id': fields.function(
            fnct=_ff_process_id, fnct_inv=_fnct_inv_set_process,
            relation='account.invoice.pending.state.process',
            method=True, type='many2one', string=u'Procés de tall',
            required=True, store=STORE_PROC_BONOSOCIAL_FIELD, readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)]
            },
        )
    }

    _defaults = {
        'process_id': _get_default_process
    }

    _constraints = [
        OnlyFieldsConstraint(
            _check_process_id,
            _("La pólissa actual no pot accedir a aquest procés de tall ja que "
              "no compleix les condicions"),
            ['process_id']
        ),
    ]


GiscedataPolissa()
