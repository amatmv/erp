# -*- encoding: utf-8 -*-

from osv import osv


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def get_default_pending_state(self, cursor, uid, invoice_id, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fact_id = fact_obj.search(
            cursor, uid, [('invoice_id', '=', invoice_id)])
        if fact_id:
            fact_id = fact_id[0]
            polissa_id = fact_obj.read(
                cursor, uid, fact_id, ['polissa_id'])['polissa_id'][0]
            polissa_obj = self.pool.get('giscedata.polissa')
            process_id = polissa_obj.read(
                cursor, uid, polissa_id, ['process_id'])['process_id'][0]
            process_obj = self.pool.get('account.invoice.pending.state.process')
            return process_obj.get_default_state(cursor, uid, process_id)
        return super(AccountInvoice, self).get_default_pending_state(
            cursor, uid, context=context)


AccountInvoice()
