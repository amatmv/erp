# -*- coding: utf-8 -*-
import logging

from osv import fields, osv
from tools.translate import _


class WizardPayInvoice(osv.osv_memory):
    """Wizard class
    """
    _name = "facturacio.pay.invoice"
    _inherit = 'facturacio.pay.invoice'

    def __init__(self, pool, cursor):
        super(WizardPayInvoice, self).__init__(pool, cursor)
        new_sel = ('reposicio', u'Reposició')
        selection = self._columns['state'].selection
        if new_sel not in selection:
            selection.append(new_sel)

    def _default_cal_reposicio(self, cursor, uid, context=None):
        if not context:
            context = {}
        factura_id = context.get('active_ids')
        if isinstance(factura_id, list) or isinstance(factura_id, tuple):
            factura_id = factura_id[0]
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura = factura_obj.browse(cursor, uid, factura_id, context=context)
        if factura.polissa_id.state == 'tall':
            return 1
        return 0

    def _default_state(self, cursor, uid, contex=None):
        if contex is None:
            contex = {}
        if self._default_cal_reposicio(cursor, uid, contex):
            return 'reposicio'
        else:
            return 'init'

    def no_reposicio(self, cursor, uid, ids, context=None):
        """Acció del botó per no generar la reposició."""
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        wiz.write({'state': 'init'})

    def generar_reposicio(self, cursor, uid, ids, context=None):
        """Acció per generar la factura de reposició
        """
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.pool.get('giscedata.polissa')

        wiz = self.browse(cursor, uid, ids[0], context=context)
        factura_id = context.get('active_ids')[0]
        factura = fact_obj.browse(cursor, uid, factura_id)
        polissa_id = factura.polissa_id.id

        repo_id = polissa_obj.generar_reposicio(
            cursor, uid, polissa_id, factura.data_inici, factura.data_final
        )

        if wiz.reposicio_agrupar:
            fact_obj.invoice_open(cursor, uid, [repo_id], context=context)
            fact_obj.make_move_differences(cursor, uid, [factura_id, repo_id])

        return {
            'domain': [('id','in', [factura_id, repo_id])],
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'cal_reposicio': fields.boolean(u'Cal reposició'),
        'reposicio': fields.selection(
            [('comer', 'Ara'), ('distri', 'F1')], u'Mètode facturació'
        ),
        'reposicio_agrupar': fields.boolean(u'Agrupar reposició')

    }
    _defaults = {
        'cal_reposicio': _default_cal_reposicio,
        'reposicio': lambda *a: 'distri',
        'reposicio_agrupar': lambda *a: 0,
        'state': _default_state
    }

WizardPayInvoice()

