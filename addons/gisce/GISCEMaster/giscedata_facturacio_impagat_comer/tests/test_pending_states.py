# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import date, timedelta
from osv.orm import ValidateException


class TestPendingstates(testing.OOTestCase):

    def test_open_empty_invoice(self):
        """ Test when an empty invoice is opened, a pending_state is set"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        inv_obj = pool.get('account.invoice')
        fact_obj = pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_1_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            inv_id = fact_obj.read(
                cursor, uid, fact_1_id, ['invoice_id']
            )['invoice_id'][0]

            inv_pstate = inv_obj.read(
                cursor, uid, inv_id, ['pending_state']
            )['pending_state']

            # Whenever an invoice is created, there hasn't to be any p. state
            # line. Only when it's opened, a pending state will be set
            self.assertFalse(inv_pstate)

            inv_obj.write(cursor, uid, inv_id, {
                'state': 'open'
            })

            inv_pstate = inv_obj.read(
                cursor, uid, inv_id, ['pending_state']
            )['pending_state'][0]
            default_pstate = inv_obj._get_default_pending(cursor, uid)
            self.assertEqual(inv_pstate, default_pstate)

    def test_date_is_updated_when_open_invoice(self):
        """ Test if, given an invoice with a default state set, the change_date
        is updated when re-opening it"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pstate_hist_obj = pool.get('account.invoice.pending.history')
        inv_obj = pool.get('account.invoice')
        fact_obj = pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_1_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            inv_id = fact_obj.read(
                cursor, uid, fact_1_id, ['invoice_id']
            )['invoice_id'][0]

            inv_obj.write(cursor, uid, inv_id, {
                'state': 'open'
            })
            # We modify the date which the state has been changed
            pstate_hist_id = inv_obj.get_current_pending_state_info(
                cursor, uid, inv_id)[inv_id]['id']
            today = date.today()
            yesterday = (today - timedelta(days=1)).strftime('%Y-%m-%d')
            pstate_hist_obj.write(cursor, uid, pstate_hist_id, {
                'change_date': yesterday
            })
            change_date = pstate_hist_obj.read(
                cursor, uid, pstate_hist_id, ['change_date'])['change_date']
            self.assertEqual(change_date, yesterday)
            inv_obj.write(cursor, uid, inv_id, {
                'state': 'draft'
            })
            inv_obj.write(cursor, uid, inv_id, {
                'state': 'open',
                'date_invoice': today
            })
            change_date = pstate_hist_obj.read(
                cursor, uid, pstate_hist_id, ['change_date'])['change_date']
            self.assertEqual(change_date, today.strftime('%Y-%m-%d'))

    def test_pending_change_of_bono_social_contract(self):
        """ Test if an invoice of the bono social process if opened with bono
        social state and a pending_state is set"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        inv_obj = pool.get('account.invoice')
        fact_obj = pool.get('giscedata.facturacio.factura')
        pol_obj = pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_1_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            read = fact_obj.read(
                cursor, uid, fact_1_id, ['invoice_id', 'polissa_id']
            )
            inv_id = read['invoice_id'][0]
            polissa_id = read['polissa_id'][0]

            cnae_9820 = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_cnae', 'cnae_9820'
            )[1]
            titular_id = pol_obj.read(
                cursor, uid, polissa_id, ['titular']
            )['titular'][0]

            res_obj = pool.get('res.partner')
            res_obj.write(
                cursor, uid, titular_id, {'vat': 'ES71895522Y'}
            )

            bono_social_process_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social',
                'bono_social_pending_state_process'
            )[1]

            # First set cotract settings to enable bono social acces
            pol_obj.write(cursor, uid, polissa_id, {
                'cnae': cnae_9820, 'titular': titular_id,

            })
            # Now field bono_social_diponible (fields.function) was set as True
            # Then we can change process id.
            pol_obj.write(cursor, uid, polissa_id, {
                'process_id': bono_social_process_id
            })

            inv_obj.write(cursor, uid, inv_id, {
                'state': 'open'
            })
            inv_pstate = inv_obj.read(
                cursor, uid, inv_id, ['pending_state']
            )['pending_state'][0]

            bono_social_correct_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social',
                'correct_bono_social_pending_state'
            )[1]
            self.assertEqual(inv_pstate, bono_social_correct_id)

    def test_constraint_jumps_if_we_try_to_set_an_incorrect_process(self):
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pol_obj = pool.get('giscedata.polissa')
        res_obj = pool.get('res.partner')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]

            polissa_id = polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            # Invalid cnae
            cnae_9820 = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_cnae', 'cnae_9820'
            )[1]

            # Set inv vat no enterprise
            titular_id = pol_obj.read(
                cursor, uid, polissa_id, ['titular']
            )['titular'][0]

            res_obj.write(
                cursor, uid, titular_id, {'vat': 'ES71895522Y'}
            )

            # Setting invalid cnae for bono social acces
            pol_obj.write(cursor, uid, polissa_id, {
                'cnae': cnae_9820, 'titular': titular_id, }
            )

            # Now we try to set a bono social process (SHOULD JUMPS CONSTAINT)
            def write_process():
                pol_obj.write(cursor, uid, polissa_id, {
                    'process_id': default_process_id

                })
            self.assertRaises(ValidateException, write_process)
