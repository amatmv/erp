# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime

from giscedata_facturacio_impagat.giscedata_facturacio import find_B1


class TestGenerarReposicio(testing.OOTestCase):
    def setUp(self):
        self.openerp.install_module('giscedata_tarifas_peajes_20160101')
        self.openerp.install_module(
            'giscedata_tarifas_peajes_{0}0101'.format(
                datetime.now().strftime('%Y')
            )
        )

    def test_generar_reposicio(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        product_obj = self.openerp.pool.get('product.product')
        categ_obj = self.openerp.pool.get('product.category')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            product_id = product_obj.search(cursor, uid, [
                ('default_code', '=', 'CON19')
            ])[0]

            categ_id = categ_obj.search(
                cursor, uid, [('name', 'ilike', 'All products')]
            )[0]

            product = product_obj.browse(cursor, uid, product_id)

            product.product_tmpl_id.write({'categ_id': categ_id})

            # Enable contact query
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.send_signal(cursor, uid, [polissa_id], [
                'validar', 'contracte'
            ])
            res = polissa_obj.generar_reposicio(
                cursor, uid, polissa_id, '2017-01-01', '2017-01-01'
            )
            assert res

