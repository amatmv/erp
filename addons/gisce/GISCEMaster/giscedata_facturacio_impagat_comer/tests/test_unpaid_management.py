# -*- coding: utf-8 -*-
from giscedata_facturacio_impagat.tests import AbstractTestContractCreationWithDebt
from giscedata_switching.giscedata_switching import SwitchingException


class TestContractCreationWithDebtComer(AbstractTestContractCreationWithDebt):

    def test_check_debt_on_contract_c201_creation(self):
        cursor, uid, fact_id, journal_id, partner_id, debt_account = self.environment_variables

        self.switch(self.txn, 'comer', other_id_name='res_partner_asus')

        contract_id = self.fact_obj.read(
            cursor, uid, fact_id, ['polissa_id']
        )['polissa_id'][0]

        asus = self.imd_obj.get_object_reference(
            cursor, uid, 'base',  'res_partner_asus'
        )[1]

        self.partner_obj.write(cursor, uid, asus, {
            'vat': 'ES06938188P'
        })
        self.fact_obj.write(cursor, uid, fact_id, {
            'partner_id': asus
        })
        self.contract_obj.write(cursor, uid, contract_id, {
            'titular': asus,
            'distribuidora': asus
        })
        self.fact_obj.invoice_open(cursor, uid, [fact_id])

        # We pay the invoice
        self.pay_invoice(cursor, uid, fact_id, journal_id)

        # And we unpay it in order to have some debt amount
        self.unpay_invoice(
            cursor, uid, fact_id, debt_account, journal_id, amount=5
        )

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'ATR'}
        )

        wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')

        wiz_id = wiz_obj.create(
            cursor, uid, {}, context={'cas': 'C2', 'pol_id': contract_id})

        context = {
            'pol_id': contract_id, 'cas': 'C2', 'active_ids': [1],
            'active_id': False, 'contract_id': contract_id
        }

        wiz_obj.write(cursor, uid, [wiz_id], {
            'owner': 4, 'change_atr': False, 'change_adm': True
        })

        wiz_obj.genera_casos_atr(cursor, uid, [wiz_id], context=context)

        res_info = wiz_obj.read(cursor, uid, [wiz_id], ['info'])[0]['info']

        self.assertIn(self.activation_error_msg, res_info)

        created_cases = self.sw_obj.search(
            cursor, uid, [('cups_polissa_id', '=', contract_id),
                          ('proces_id.name', '=', 'ATR')]
        )

        self.assertEqual(len(created_cases), 0)
