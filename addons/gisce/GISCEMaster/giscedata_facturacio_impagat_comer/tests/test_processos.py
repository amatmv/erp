# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestProcessos(testing.OOTestCase):

    def test_process_changes_when_bono_social_does(self):
        """ Test if the field process_id changes to 'Bono social' process
        when field 'bono_social_disponible' does."""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pol_obj = pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            pol = pol_obj.browse(cursor, uid, polissa_id)
            default_process_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_pending_state_process'
            )[1]

            current_process_id = pol.process_id.id
            self.assertEquals(default_process_id, current_process_id)

            cnae_9820 = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_cnae', 'cnae_9820'
            )[1]
            partner = pol_obj.read(
                cursor, uid, polissa_id, ['titular']
            )['titular'][0]

            res_obj = pool.get('res.partner')
            res_obj.write(
                cursor, uid, partner, {'vat': 'ES71895522Y'}
            )
            pol.write({'cnae': cnae_9820, 'titular': 4})

            current_process_id = pol_obj.read(
                cursor, uid, polissa_id, ['process_id']
            )['process_id'][0]

            bono_social_process_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_bono_social',
                'bono_social_pending_state_process'
            )[1]
            self.assertEquals(current_process_id, bono_social_process_id)
