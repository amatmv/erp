# -*- coding: utf-8 -*-
{
    "name": "Impagaments (comer)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Fer la factura de reposició en pagar una factura que està en tall
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_contractacio_comer",
        "giscedata_facturacio_comer_bono_social"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_facturacio_impagat_comer_demo.xml",
    ],
    "update_xml":[
        "giscedata_facturacio_impagat_comer_data.xml",
        "giscedata_polissa_view.xml",
        "wizard/wizard_pay_invoice_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
