# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        pass
    logger = logging.getLogger('openerp.migration')
    logger.info('Deleting unused config variable "acc_inv_unpaid_payment_type"')
    cursor.execute("""
        DELETE FROM res_config
        WHERE name = 'acc_inv_unpaid_payment_type'
    """)
    cursor.execute("""
        DELETE FROM ir_model_data
        WHERE module = 'giscedata_facturacio_impagat_comer'
        AND name = 'acc_inv_unpaid_payment_type'
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
