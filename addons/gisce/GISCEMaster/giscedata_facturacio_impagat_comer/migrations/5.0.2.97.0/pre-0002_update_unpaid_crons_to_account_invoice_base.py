# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        pass
    logger = logging.getLogger('openerp.migration')
    logger.info('Updating models from giscedata_facturacio_impagat_comer to '
                'account_invoice_pending')
    cursor.execute("""
        UPDATE ir_model_data
        SET module = 'account_invoice_pending'
        WHERE module = 'giscedata_facturacio_impagat_comer' 
        AND name IN (
            'model_update_pending_states', 
            'model_move_to_unpaid',
            'ir_cron_pending_states',
            'ir_cron_move_to_unpaid'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
