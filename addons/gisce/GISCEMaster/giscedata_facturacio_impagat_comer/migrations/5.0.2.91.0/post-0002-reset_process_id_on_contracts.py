# -*- coding: utf-8 -*-
import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')

    default_process_id = imd_obj.get_object_reference(
        cursor, uid, 'account_invoice_pending',
        'default_pending_state_process'
    )[1]

    bono_social_process_id = imd_obj.get_object_reference(
        cursor, uid, 'giscedata_facturacio_comer_bono_social',
        'bono_social_pending_state_process'
    )[1]

    logger.info('Reset process id on contracts')
    query = """
        UPDATE giscedata_polissa
        SET process_id = (CASE WHEN bono_social_disponible THEN %s else %s END)
    """

    cursor.execute(query, (bono_social_process_id, default_process_id))
    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
