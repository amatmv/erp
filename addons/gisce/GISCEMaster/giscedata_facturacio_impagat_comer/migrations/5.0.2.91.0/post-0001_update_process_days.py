# coding=utf-8
import logging

import pooler


def up(cursor, installed_version):
    if not installed_version:
        return
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    bono_social_data_module = 'giscedata_facturacio_comer_bono_social'
    logger = logging.getLogger('openerp.migration')
    pprocess_obj = pool.get('account.invoice.pending.state.process')
    imd_obj = pool.get('ir.model.data')

    logger.info('Updating bono social process 0 business days')

    # Bono social cutoff process
    bono_social_process_id = imd_obj.get_object_reference(
        cursor, uid, bono_social_data_module,
        'bono_social_pending_state_process'
    )[1]
    pprocess_obj.write(cursor, uid, [bono_social_process_id], {
        'cutoff_days': 0, 'days_type': 'business'
    })

    logger.info('Updating default process cutoff days and days type')

    # Bono social carta 1 pending state
    pstate_obj = pool.get('account.invoice.pending.state')
    logger.info('Updating default process cutoff days and days type')
    carta_1_pending_state = imd_obj.get_object_reference(
        cursor, uid, bono_social_data_module,
        'carta_1_pending_state'
    )[1]
    pstate_obj.write(cursor, uid, [carta_1_pending_state], {
        'pending_days_type': 'business'
    })

    # Bono social carta 2 pending state
    carta_2_pending_state = imd_obj.get_object_reference(
        cursor, uid, bono_social_data_module,
        'carta_2_pending_state'
    )[1]
    pstate_obj.write(cursor, uid, [carta_2_pending_state], {
        'pending_days_type': 'natural'
    })

    # Bono social avis de tall pending state
    avis_tall_pending_state = imd_obj.get_object_reference(
        cursor, uid, bono_social_data_module,
        'avis_tall_pending_state'
    )[1]
    pstate_obj.write(cursor, uid, [avis_tall_pending_state], {
        'pending_days_type': 'natural'
    })

    logger.info('All updates finished')


def down(cursor, installed_version):
    pass


migrate = up
