# -*- coding: utf-8 -*-
{
    "name": "Facturació Pool de Lectures",
    "description": """
    Mòdul que permet gestionar la càrrega de lectures del pool des dels lots de
    facturació
    """,
    "version": "0-dev",
    "author": "Gisce-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_lectures_pool",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_view.xml"
    ],
    "active": False,
    "installable": True
}
