# -*- encoding: utf-8 -*-

from osv import osv, fields


class GiscedataFacturacioLot(osv.osv):

    _name = 'giscedata.facturacio.lot'
    _inherit = 'giscedata.facturacio.lot'

    def get_comptadors(self, cursor, uid, ids, context=None):
        """ Retorna els comptadors associats a la/les pólisses"""
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')

        polisses_ids = polissa_obj.search(cursor, uid,
                                          [('lot_facturacio', 'in', ids),
                                           ('state', 'not in', ['esborrany']),
                                           ],
                                           context={'active_test': False})

        search_vals = [('polissa', 'in', polisses_ids)]
        comptadors_ids = comptador_obj.search(cursor, uid, search_vals,
                                              context={'active_test': False})

        return comptadors_ids

GiscedataFacturacioLot()