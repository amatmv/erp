SELECT
cups.name AS A,
distri.ref AS B,
comer.ref AS C,
agree_tipus::int AS D,
agree_tensio AS E,
agree_tarifa AS F,
agree_dh AS G,
prov.ree_code AS H,
modcon.potencia AS I,
CASE WHEN modcon.tg != '2' THEN 's' ELSE 'n' END AS J,
TO_CHAR(GREATEST(modcon.data_inici, %(start)s), 'YYYY/MM/DD 00') AS K,
TO_CHAR(LEAST(modcon.data_final, %(end)s), 'YYYY/MM/DD 00') AS L
FROM giscedata_polissa_modcontractual modcon
LEFT JOIN giscedata_cups_ps cups ON cups.id=modcon.cups 
LEFT JOIN res_partner distri ON distri.id=cups.distribuidora_id
LEFT JOIN res_partner comer ON comer.id=modcon.comercialitzadora
LEFT JOIN res_municipi municipi ON municipi.id=cups.id_municipi
LEFT JOIN res_country_state prov ON prov.id=municipi.state
WHERE modcon.agree_tipus='05' AND ((%(start)s BETWEEN data_inici AND data_final))
ORDER by
comer.ref,
cups.name,
modcon.data_inici