SELECT
    %s as distri,
    c.ref as comer,
    m.agree_tensio,
    m.agree_tarifa,
    m.agree_dh,
    m.agree_tipus::INTEGER ,
    round(sum(
        CASE WHEN i.type = 'out_refund'
            THEN -1
        ELSE
            1
        END
        *
        il.quantity / f.facturacio
    ))::int AS quantity,
    to_char(p.data_inici, 'YYYYMM') as mes
FROM
    giscedata_facturacio_factura f
    INNER JOIN account_invoice i on (f.invoice_id = i.id)
    INNER JOIN account_journal j on (i.journal_id = j.id)
    INNER JOIN giscedata_facturacio_factura_linia l on (l.factura_id = f.id and tipus = 'energia')
    INNER JOIN account_invoice_line il on (l.invoice_line_id = il.id)
    INNER JOIN giscedata_polissa_modcontractual m on (m.polissa_id = f.polissa_id and f.data_final >= m.data_inici and f.data_final <= m.data_final)
    INNER JOIN giscedata_facturacio_lot p on (f.data_inici BETWEEN p.data_inici AND p.data_final OR f.data_final BETWEEN p.data_inici AND p.data_final)
    INNER JOIN res_partner c on (m.comercialitzadora = c.id)
    INNER JOIN giscedata_polissa_tarifa t ON m.tarifa = t.id
WHERE
    (
        (
            f.data_inici BETWEEN %s::date - INTERVAL '6 month'
                             AND %s::date - INTERVAL '2 month' - INTERVAL '1 day'
        ) OR (
            f.data_final BETWEEN %s::date - INTERVAL '6 month'
                             AND %s::date - INTERVAL '2 month' - INTERVAL '1 day'
        )
    )
    AND p.data_inici >= %s::date - INTERVAL '6 month'
    AND p.data_final <= %s::date - INTERVAL '2 month' - INTERVAL '1 day'
    AND j.code ilike 'ENERGIA%%'
    AND i.state in ('open', 'paid')
    AND t.name NOT IN ('RE', 'RE2')
GROUP BY
    c.ref,
    m.agree_tensio,
    m.agree_tarifa,
    m.agree_dh,
    m.agree_tipus,
    p.data_inici
HAVING (ROUND(SUM(CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END * il.quantity / f.facturacio))) > 0
ORDER BY c.ref asc
