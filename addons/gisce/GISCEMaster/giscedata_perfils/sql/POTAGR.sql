SELECT
  %s as dist,
  c.ref,
  m.agree_tensio,
  m.agree_tarifa,
  m.agree_dh,
  m.agree_tipus,
  round(sum(m.potencia))::int,
  %s as mes
FROM
  giscedata_polissa_modcontractual m 
INNER JOIN res_partner c ON (m.comercialitzadora = c.id)
INNER JOIN giscedata_polissa_tarifa t ON m.tarifa = t.id
WHERE m.data_inici <= %s 
  and m.data_final >= %s
  and m.agree_tipus not in ('01', '02')
  AND t.name NOT IN ('RE', 'RE2')
GROUP BY
  c.ref, m.agree_tensio,
  m.agree_tarifa,
  m.agree_dh,
  m.agree_tipus,
  dist,
  mes
ORDER BY
  c.ref asc,
  m.agree_tensio asc,
  m.agree_tarifa asc,
  m.agree_dh asc,
  m.agree_tipus asc
