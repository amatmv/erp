# -*- coding: utf-8 -*-
from osv import osv, fields

class ResCountryState(osv.osv):
    """Afegim camps pels perfils a la província.
    """
    _name = 'res.country.state'
    _inherit = 'res.country.state'
    _columns = {
        'ree_code': fields.char('Codi REE', size=2)
    }
ResCountryState()
