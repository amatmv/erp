# -*- coding: utf-8 -*-
""" Eliminem el camp skip_validation de la base de dades"""
import pooler
from datetime import datetime


def migrate(cursor, installed_version):
    uid = 1
    cursor.execute("""ALTER TABLE giscedata_perfils_contracte_lot
                      DROP COLUMN skip_validation""")
