# -*- coding: utf-8 -*-
"""Actualitzem els codis de referència a ir_model_data per l10n_ES_toponyms
per tal que facin referència als nostres (si existeixen o són diferents).
"""
import pooler
import netsvc

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    model_data_obj = pool.get('ir.model.data')
    state_obj = pool.get('res.country.state')
    search_params = [('model', '=', 'res.country.state'),
                     ('module', '=', 'l10n_ES_toponyms')]
    ids = model_data_obj.search(cursor, uid, search_params)
    for md in model_data_obj.read(cursor, uid, ids, ['name', 'res_id']):
        codi = md['name'][-2:]
        search_params = [('country_id.code', '=', 'ES'),
                         ('code', '=', codi)]
        states_ids = state_obj.search(cursor, uid, search_params)
        if states_ids:
            model_data_obj.write(cursor, uid, [md['id']],
                                 {'res_id': states_ids[0]})
