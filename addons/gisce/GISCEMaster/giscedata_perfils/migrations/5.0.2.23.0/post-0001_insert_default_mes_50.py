# -*- coding: utf-8 -*-
"""Assignem per defecte no perfilar els de més de 50kW.
"""
import pooler
import netsvc

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    cfg_obj = pool.get('res.config')
    cfg_obj.get(cursor, uid, 'perfils_perfilar_mes_50', 0)
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Setting perfils_perfilar_mes_50 to 0 as default')
    # TODO: It is doing nothing
