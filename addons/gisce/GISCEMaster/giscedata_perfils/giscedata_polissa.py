# -*- coding: utf-8 -*-
import calendar
from datetime import datetime, timedelta

from osv import osv, fields
from tools.misc import cache
from tools.translate import _
from giscedata_perfils import MAGNITUDS
import netsvc
import pooler
from libfacturacioatr.tarifes import Tarifa31ALB, get_dies_laborables_festius
from oorq.decorators import job

custom_timeout = 360

def apply_31A_LB_cof(lectures, kva, data_inici, data_final,
                     dies_festius, perdues=0.04):
    lect_cof = lectures.copy()
    # Construïm el diccionari de consums a través de les lectures PX: Consum
    consums = dict(
            (p, l['actual'].get('consum', 0)) for p, l in lectures.items()
    )
    hores_pp = Tarifa31ALB.hores_per_periode
    laborables, festius = get_dies_laborables_festius(
        datetime.strptime(data_inici, '%Y-%m-%d'),
        datetime.strptime(data_final, '%Y-%m-%d'),
        dies_festius
    )
    cofs = {}
    for periode in hores_pp:
        if periode > 'P3':
            cofs[periode] = hores_pp.get(periode, 0) * festius
        else:
            cofs[periode] = hores_pp.get(periode, 0) * laborables
    for periode, consum in consums.items():
        c = lect_cof[periode]['actual']
        c['consum'] = (round(consum * (1 + perdues), 2)
                       + round(0.01 * cofs[periode] * kva, 2))
    return lect_cof


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def write(self, cursor, uid, ids, vals, context=None):
        """Sobreescrivim el write() per capturar el lot de perfilació.
        """
        lot_id = vals.get('lot_perfilacio', False)
        if lot_id:
            self.assignar_al_lot_perfil(cursor, uid, ids, lot_id, context)
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals,
                                                  context)
        return res

    def get_inici_final_a_perfilar(self, cursor, uid, ids, use_lot=False,
                                   context=None):
        """Retorna una tuple (data_inici, data_final) a perfilar.
        """
        if not context:
            context = {}
        if isinstance(ids, list) or isinstance(ids, tuple):
            polissa_id = ids[0]
        polissa = self.pool.get('giscedata.polissa').browse(cursor, uid,
                                                            polissa_id,
                                                            context)
        if use_lot:
            lot_id = use_lot
        else:
            if polissa.lot_perfilacio:
                lot_id = polissa.lot_perfilacio.id
            else:
                raise osv.except_osv("Error", "No s'ha passat el lot per "
                                     "referència, ni la pòlissa té lot de "
                                     "perfilació assignat")

        lot_obj = self.pool.get('giscedata.perfils.lot')
        lot = lot_obj.browse(cursor, uid, lot_id, context)
        if polissa.data_baixa:
            lot.data_inici = min(lot.data_inici, polissa.data_baixa)
            lot.data_final = min(lot.data_final, polissa.data_baixa)
        if use_lot:
            data_inici = lot.data_inici
        else:
            data_inici = max(polissa.data_ultima_lectura_perfilada,
                             lot.data_inici)
        data_final = lot.data_final
        return (data_inici, data_final)

    def get_ultim_perfil(self, cursor, uid, ids, data_inici=None,
                         context=None):
        """Obté l'últim perfil fet per aquesta pòlissa amb l'arrastre.
        """
        perf_obj = self.pool.get('giscedata.perfils.perfil')
        res = {}
        for polissa in self.browse(cursor, uid, ids, context):
            if not data_inici:
                data_inici = polissa.get_inici_final_a_perfilar()[0]
            for mag in MAGNITUDS:
                res[mag] = (data_inici, 0, False)
            if polissa.data_ultima_lectura_perfilada:
                last_ts = polissa.data_ultima_lectura_perfilada.replace('-', '')
                search_params = [('cups', '=', polissa.cups.name),
                                 ('name', '=', '%s24' % last_ts)]
                for perf in perf_obj.search_reader(cursor, uid, search_params,
                                                   ['year', 'month', 'day',
                                                    'decimals', 'magnitud'],
                                                   limit=1):
                    # Si trobem que s'ha perfilat, hem de començar a perfilar
                    # un dia després
                    from_date = (datetime(int(perf['year']), int(perf['month']),
                                          int(perf['day']))
                                 + timedelta(days=1)).strftime('%Y-%m-%d')
                    res[perf['magnitud']] = (from_date, perf['decimals'], True)
            return res

    def get_seguent_lot_perfil(self, cursor, uid, ids, lot_id=None,
                               context=None):
        """Retorna el següent lot de perfilació per la pòlissa.
        """
        if not context:
            context = {}
        lp_obj = self.pool.get('giscedata.perfils.lot')
        for polissa in self.browse(cursor, uid, ids):
            if lot_id:
                # No estem escrivint res, només sobreescrivim una posició de
                # memòria per aquesta funció.
                polissa.lot_perfilacio = lp_obj.browse(cursor, uid, lot_id)
            data_inici = (datetime.strptime(polissa.lot_perfilacio.data_final,
                                            '%Y-%m-%d')
                          + timedelta(days=1))
            dti = data_inici.strftime('%Y-%m-%d')
            if polissa.data_baixa and polissa.data_baixa < dti:
                res = False
            else:
                res = lp_obj.search(cursor, uid, [
                    ('data_inici', '=', dti)
                ], limit=1)
                if res:
                    res = res[0]
            return res

    def assignar_al_lot_perfil(self, cursor, uid, ids, lot_id, context=None):
        """Assigna la pòlissa a un lot de perfilació.
        """
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        if not context:
            context = {}
        ctx = context.copy()
        ctx['active_test'] = False
        contracte_lot_obj = self.pool.get('giscedata.perfils.contracte_lot')
        lot_obj = self.pool.get('giscedata.perfils.lot')
        lot = lot_obj.browse(cursor, uid, lot_id, context)
        for polissa in self.browse(cursor, uid, ids):
            # Busquem els contractes que estiguin associats a lots més
            # antics i que estiguin en estat esborrany
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('state', '=', 'esborrany'),
                ('lot_id.data_final', '<', lot.data_inici),
            ]
            unlink_ids = contracte_lot_obj.search(cursor, uid, search_params,
                                                  context=ctx)
            contracte_lot_obj.unlink(cursor, uid, unlink_ids)
            # Busquem els posteriors que estiguin en estat esborrany
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('state', '=', 'esborrany'),
                ('lot_id.data_inici', '>', lot.data_final),
            ]
            unlink_ids = contracte_lot_obj.search(cursor, uid, search_params,
                                                  context=ctx)
            contracte_lot_obj.unlink(cursor, uid, unlink_ids)
            # Si no existeix ja al lot actual li assignem
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('lot_id.id', '=', lot.id),
            ]
            cl_ids = contracte_lot_obj.search(cursor, uid, search_params,
                                                   context=ctx)
            if not cl_ids:
                vals = {
                    'polissa_id': polissa.id,
                    'lot_id': lot.id,
                }
                cl_id = contracte_lot_obj.create(cursor, uid, vals)
                cl_ids.append(cl_id)
            # Si el lot ja està obert hem d'obrir el contracte lot
            if lot.state == 'obert':
                contracte_lot_obj.write(cursor, uid, cl_ids, {'state': 'obert'})
        return True

    @job(queue='profiling', timeout=custom_timeout)
    def perfilar_async(self, cursor, uid, ids, lot_id, context=None):
        return self.perfilar(cursor, uid, ids, lot_id, context=context)

    def perfilar(self, cursor, uid, ids, lot_id, context=None):
        """Perfilem aquesta pòlissa segons el lot per paràmetre.
        """
        if not context:
            context = {'recursivitat': 0}
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        logger = netsvc.Logger()
        _mags = {'A': 'AE'} #'R': 'R1'}
        mc_fields = ['agree_tensio', 'agree_tarifa', 'agree_dh', 'agree_tipus',
                     'comercialitzadora']
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        perf_ree_obj = self.pool.get('giscedata.perfils.ree')
        perf_obj = self.pool.get('giscedata.perfils.perfil')
        cfg_obj = self.pool.get('res.config')
        fest_obj = self.pool.get('giscedata.dfestius')
        max_rec = int(cfg_obj.get(cursor, uid, 'perfils_max_recursivitat', 2))
        #Canviat de 0 a 1 per poder perfilar mes de 50 ARA NO
        mes_50 = int(cfg_obj.get(cursor, uid, 'perfils_perfilar_mes_50', 0))
        lectures_breakpoint = False
        lot = self.pool.get('giscedata.perfils.lot').browse(cursor, uid, lot_id)
        perfils_con_lot = self.pool.get('giscedata.perfils.contracte_lot')
        for p_obj in self.read(cursor, uid, ids,
                                        ['data_ultima_lectura_perfilada']):
            ultima_lect_perf = p_obj['data_ultima_lectura_perfilada']
            if ultima_lect_perf:
                polissa = self.browse(cursor, uid, p_obj['id'],
                                  {'date': p_obj['data_ultima_lectura_perfilada']})
            else:
                raise osv.except_osv('Error', _(u"La pòlissa %s no està "
                                                u"assignada a cap lot de "
                                                u"perfilació" % p_obj['id']))

            if polissa.data_baixa:
                last_profile_date = min(
                    polissa.lot_perfilacio.data_final,
                    polissa.data_baixa
                )
            else:
                last_profile_date = polissa.lot_perfilacio.data_final
            data_inici, data_final = polissa.get_inici_final_a_perfilar()
            # Si el lot que volem perfilar és diferent al lot que tenim assignat
            # a la pòlissa és que venim d'una altre perfilació on no teníem
            # prou lectures per finalitzar, per tant inici i final serà
            # la suma d'intervals d'aquests.
            if lot_id != polissa.lot_perfilacio.id:
                di_l2, df_l2 = polissa.get_inici_final_a_perfilar(lot_id)
                data_inici = min(data_inici, di_l2)
                data_final = max(data_final, df_l2)
            intervals = polissa.get_modcontractual_intervals(data_inici,
                                    data_final, context={'ffields': mc_fields})
            # Perfilem cada modificació contractual
            mod_ids = []
            mod_dates = {}
            for mod_data in sorted(intervals.keys()):
                mod_id = intervals[mod_data]['id']
                mod_ids.append(mod_id)
                # Interval real que duren amb possibilitat de més d'una
                # modificació contractual
                mod_dates[mod_id] = intervals[mod_data]['dates']
            logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                 u"Perfilant P:%s num modcon: %s"
                                 % (polissa.name, len(mod_ids)))
            for modcon in modcon_obj.browse(cursor, uid, mod_ids):
                if (
                    modcon.agree_tipus < '03'
                ) or (
                    modcon.agree_tipus == '03' and not mes_50
                ):
                    continue
                # Refresquem la pòlissa
                polissa = self.browse(cursor, uid, modcon.polissa_id.id)
                # Mirem els comptadors actius que té la modificació contractual
                data_inici_m = max(data_inici, mod_dates[modcon.id][0])
                data_final_m = min(data_final, mod_dates[modcon.id][1])
                comptadors_actius = modcon.polissa_id.comptadors_actius(
                              data_inici_m, data_final_m, order='data_alta asc')
                logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                 u"Perfilant P:%s MC:%s (%s - %s) NCompt:%s"
                                 % (polissa.name, modcon.name, data_inici_m,
                                    data_final_m, len(comptadors_actius)))
                vals = {
                    'cups': modcon.cups.name,
                    'distribuidora': modcon.cups.distribuidora_id.ref,
                    'comercialitzadora': modcon.comercialitzadora.ref,
                    'agree_tensio': modcon.agree_tensio,
                    'agree_tarifa': modcon.agree_tarifa,
                    'agree_dh': modcon.agree_dh,
                    'agree_tipo': modcon.agree_tipus,
                    'provincia': modcon.cups.id_municipi.state.ree_code,
                    'mesura_ab': modcon.tarifa.mesura_ab,
                    'cof': modcon.tarifa.cof,
                }
                # Per cada comptador obtenim la lectura a perfilar
                for compt in comptador_obj.browse(cursor, uid,
                                                  comptadors_actius):
                    # Obtenim l'últim perfil per continuar les magintuds
                    if len(comptadors_actius) > 1 \
                            and compt.id == comptadors_actius[-1]:
                        polissa = self.browse(cursor, uid, polissa.id)
                    ultim_perfil_mags = polissa.get_ultim_perfil(
                        ultima_lect_perf
                    )
                    if hasattr(compt, 'tg') and compt.tg:
                        context.update({'six_periods': True})
                    for tipus in _mags:
                        ultim_perfil = ultim_perfil_mags[_mags[tipus]]

                        # Busquem si hi ha lectures intermitges
                        lects_inter = lectures_obj.search_reader(cursor, uid, [
                            ('name', '>', ultima_lect_perf),
                            ('name', '<=', data_final_m),
                            ('tipus', '=', tipus),
                            ('comptador.id', '=', compt.id)
                        ], ['name'], context={'active_test': False},
                            order='name asc')

                        read_dates = []
                        for lect_inter in lects_inter:
                            if lect_inter['name'] != compt.data_alta:
                                read_date = lect_inter['name']
                            else:
                                read_date = data_final_m
                            if read_date not in read_dates:
                                    read_dates.append(read_date)

                        for data_final_lectures in read_dates:
                            if ultima_lect_perf >= last_profile_date:
                                continue
                            logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                     u"Perfilant P:%s MC:%s C:%s lectures %s a %s"
                                     % (polissa.name, modcon.name, compt.name,
                                        ultima_lect_perf, data_final_lectures))
                            lectures = compt.get_lectures_per_facturar(
                                      modcon.tarifa.id, tipus,
                                      context={'ult_lectura_fact': ultima_lect_perf,
                                               'fins_lectura_fact': data_final_lectures,
                                               'from_perfil': True})
                            if 'name' not in lectures['P1']['actual']:
                                logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                     u"Perfilant P:%s MC:%s C:%s No hi ha lectures."
                                     % (polissa.name, modcon.name, compt.name))
                                # Si no hi ha més comptadors
                                # i hi ha una altre modcontractual a radera hem
                                # d'allargar-ho!
                                if (compt.id == comptadors_actius[-1]
                                    and modcon.id != mod_ids[-1]):
                                    found = False
                                    for data_tall, mod in sorted(intervals.items()):
                                        if found:
                                            break
                                        if modcon.id == mod['id']:
                                            found = True
                                    data_tall = (datetime.strptime(data_tall,
                                                                   '%Y-%m-%d')
                                                 - timedelta(days=1)
                                                 ).strftime('%Y-%m-%d')
                                    lectures = compt.get_lectures(modcon.tarifa.id,
                                                                  ultima_lect_perf,
                                                                  tipus)
                                    for lect in lectures.values():
                                        lect['actual']['name'] = data_tall
                                    logger.notifyChannel("objects",
                                                         netsvc.LOG_DEBUG,
                                        u"Perfilant P:%s MC:%s C:%s Allarguem la "
                                        u"perfilació fins a %s per canvi de mod"
                                        u"contractual."
                                        % (polissa.name, modcon.name, compt.name,
                                           data_tall))
                                elif (context['recursivitat'] == max_rec
                                      and compt.id == comptadors_actius[-1])      :
                                    # Estem al punt màxim de recursivitat, per tant
                                    # hauríem d'entrar lectures breakpoint a data
                                    # final del període
                                    data_bp = polissa.lot_perfilacio.data_final
                                    for lect in lectures.values():
                                        lect['actual']['name'] = data_bp
                                    lectures_breakpoint = True
                                    # Fem breakpoint, per tant ens quedem al lot
                                    # on hem fet breakpoint
                                    lot_id = polissa.lot_perfilacio.id
                                else:
                                    continue
                            # New policy to profile
                            if modcon.data_inici == ultima_lect_perf:
                                data_ant = ultim_perfil[0]
                            # last_profiled + 1 day
                            else:
                                data_ant = (datetime.strptime(ultima_lect_perf,
                                                              '%Y-%m-%d')
                                            + timedelta(days=1)).strftime('%Y-%m-%d')

                            data_act = lectures['P1']['actual']['name']
                            # Assignem que l'última lectura perfilada serà l'actual
                            ultima_lect_perf = data_act
                            sum_cofs = perf_ree_obj.sum_cofs(cursor, uid,
                                                             modcon.tarifa.id,
                                                             data_ant, data_act,
                                                             context=context)
                            logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                     u"Perfilant P:%s MC:%s C:%s Agafem "
                                     u"coeficients des de %s a %s."
                                     % (polissa.name, modcon.name, compt.name,
                                        data_ant, data_act))
                            # Agafem els decimals de l'últim perfil
                            arrastre = ultim_perfil[1]
                            # Eliminem perfils (per si es torna a reperfilar)
                            search_params = [
                                ('cups', '=', vals['cups']),
                                ('name', '>=', '%s00' % data_ant.replace('-', '')),
                                ('name', '<=', '%s24' % data_act.replace('-', ''))
                            ]
                            unlink_ids = perf_obj.search(cursor, uid, search_params)
                            if unlink_ids:
                                logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                    u"Eliminem perfils trobats des de %s fins a %s"
                                    % (ultim_perfil[0], data_act))
                                perf_obj.unlink(cursor, uid, unlink_ids)
                            if modcon.tarifa.name == '3.1A LB':
                                festius = fest_obj.search_reader(cursor, uid, [
                                    ('name', '>', data_ant),
                                    ('name', '<', data_act)
                                ])
                                dies_festius = [
                                    datetime.strptime(d['name'], '%Y-%m-%d').date()
                                    for d in festius
                                ]
                                lectures = apply_31A_LB_cof(lectures, modcon.trafo,
                                                            data_ant, data_act,
                                                            dies_festius)
                            if modcon.agree_tarifa == '31':
                                # Fix tarifes amb consum al P4
                                if lectures.get('P4', []):
                                    cp4 = lectures.get('P4').get(
                                        'actual', {}).get('consum', 0)
                                    a = lectures['P1'].setdefault('actual', {})
                                    a['consum'] = a.get('consum', 0) + cp4
                                    logger.notifyChannel(
                                        "objects",
                                        netsvc.LOG_WARNING,
                                        ("Consum de %s al P4 en una tarifa 3.1A. "
                                        "L'agreguem al P1. Total: %s")
                                        % (cp4, a['consum'])
                                    )
                                # Comptador d'una 3.1 amb 3 periodes
                                for period in ('P4', 'P5', 'P6'):
                                    if not lectures.get(period, []):
                                        lectures[period] = {
                                            'actual': {'consum': 0}
                                        }
                            for perf in perf_ree_obj.get_cofs(cursor, uid,
                                                              data_ant,
                                                              data_act):
                                dia = '%s-%s-%s' % (perf['year'], perf['month'],
                                                    perf['day'])
                                hora = '%02i:00:00' % (perf['hour'] - 1)
                                # Això ens pot fer anar lent la perfilació, sino
                                # ho integrarem com hem fet en el get_cofs
                                periode = modcon.tarifa.get_periode_ts(dia, hora,
                                                                    perf['estacio'],
                                                                    context=context)
                                consum = lectures[periode].get('actual',
                                                               {}).get('consum', 0)
                                cof = perf[modcon.tarifa.cof]
                                vals.update({
                                    'year': perf['year'],
                                    'month': perf['month'],
                                    'day': perf['day'],
                                    'estacio': perf['estacio'],
                                    'hour': perf['hour'],
                                    'name': '%s%s%s%02i' % (perf['year'],
                                                             perf['month'],
                                                             perf['day'],
                                                             perf['hour']),
                                    'cof': cof,
                                    'lectura': consum,
                                    'sum_cofs': sum_cofs[periode],
                                    'mesura_anual': cof * consum,
                                    'magnitud': _mags[tipus]
                                })
                                vals['mesura_mensual'] = ((vals['mesura_anual']
                                                          / vals['sum_cofs'])
                                                          + arrastre)
                                vals['aprox'] = int(round(vals['mesura_mensual']))
                                vals['decimals'] = (vals['mesura_mensual']
                                                    - vals['aprox'])
                                arrastre = vals['decimals']
                                logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                                     "Creant %s" % vals)
                                perf_obj.create(cursor, uid, vals)
                        if lectures_breakpoint:
                            polissa.write(
                                {'data_ultima_lectura_perfilada_bp':
                                 ultima_lect_perf},
                                context={'sync': False}
                            )
                        else:
                            polissa.write(
                                {'data_ultima_lectura_perfilada_bp': False,
                                 'data_ultima_lectura_perfilada':
                                  ultima_lect_perf},
                                context={'sync': False}
                            )

            # Si dia < data_final, hem de mirar quin seria el següent lot i
            # perfilar el següent lot.
            s_lot_id = polissa.get_seguent_lot_perfil(lot_id)
            if ultima_lect_perf < last_profile_date:
                if not s_lot_id:
                    message = u"Perfilant P:{0} MC:{1} Baixa a {2} no " \
                              u"continuem perfilant."
                    message = message.format(
                        polissa.name,
                        modcon.name,
                        polissa.data_baixa
                    )
                    logger.notifyChannel(
                        "objects",
                        netsvc.LOG_INFO,
                        message
                    )
                    profiled = False
                    continue
                message = u"Perfilant P:{0} MC:{1} Hem de continuar, no hi " \
                          u"ha prous lectures fins a {2}, hem d'arribar a {3}."
                message = message.format(
                    polissa.name,
                    modcon.name,
                    ultima_lect_perf,
                    polissa.lot_perfilacio.data_final
                )
                logger.notifyChannel(
                    "objects",
                    netsvc.LOG_INFO,
                    message
                )
                context['recursivitat'] += 1
                if context['recursivitat'] > max_rec:
                    # No podem excedir la recursivitat de perfilació
                    raise osv.except_osv('Error', _(u"S'ha excedit la "
                                                    u"recusivitat màxima %s."
                                                    % max_rec))
                if polissa.perfilar(s_lot_id, context=context):
                    profiled = True
                else:
                    profiled = False
            else:
                profiles_policy_batch_ids = perfils_con_lot.search(
                    cursor,
                    uid,
                    [
                        ('polissa_id.id', '=', polissa.id),
                        ('lot_id.id', '=', polissa.lot_perfilacio.id)
                    ],
                    context={'active_test': False}
                )
                profiles_policy_batchs = perfils_con_lot.browse(
                    cursor,
                    uid,
                    profiles_policy_batch_ids,
                    context={'active_test': False}
                )
                for profile_policy_batch in profiles_policy_batchs:
                    policy_batch_values = {'state':'finalitzat'}
                    if profile_policy_batch.origen_real == 'sense_determinar':
                            policy_batch_values.update({'origen_real': 'perfil'})
                    profile_policy_batch.write(policy_batch_values)
                profiled = True

            if s_lot_id and profiled:
                polissa.write(
                    {'lot_perfilacio': s_lot_id},
                    context={'sync': False}
                )

        return profiled

    _columns = {
        'data_ultima_lectura_perfilada': fields.date('Data última lectura '
                                                     'perfilada'),
        'data_ultima_lectura_perfilada_bp': fields.date('Data última lectura '
                                                        'perfilada fictícia'),
        'lot_perfilacio': fields.many2one('giscedata.perfils.lot',
                                          'Lot de perfilació'),
    }

GiscedataPolissa()


class GiscedataPolissaTarifa(osv.osv):
    """Camps especials per les tarifes i la perfilació.
    """
    _name = 'giscedata.polissa.tarifa'
    _inherit = 'giscedata.polissa.tarifa'

    @cache()
    def get_periode_ts(self, cursor, uid, ids, dia, hora, estacio,
                       context=None):
        """Ens retorna el periode segons el dia que li passem.
        """
        if not context:
            context = {}
        festiu_obj = self.pool.get('giscedata.dfestius')
        hfestiu_obj = self.pool.get('giscedata.perfils.hfestius')
        hlaborable_obj = self.pool.get('giscedata.perfils.hlaborables')
        for tarifa in self.browse(cursor, uid, ids, context):
            periodes = {}
            for p in tarifa.periodes:
                if p.tipus == 'te':
                    periodes[p.id] = p.name
            if len(periodes) > 1:
                # Busquem el periode que toca segons si és festiu o laborable
                search_params = [
                    ('estacio', '=', estacio),
                    ('hinici', '<=', hora),
                    ('hfinal', '>', hora),
                    ('dinici', '<=', dia),
                    ('dfinal', '>=', dia),
                    ('periode.id', 'in', periodes.keys())
                ]
                if festiu_obj.is_festiu(cursor, uid, dia):
                    # Busquem a la taula de festius
                    p = hfestiu_obj.search_reader(
                        cursor,
                        uid,
                        search_params,
                        ['periode'],
                        limit=1
                    )
                else:
                    # Busquem a la taula de laborables
                    p = hlaborable_obj.search_reader(
                        cursor,
                        uid,
                        search_params,
                        ['periode'],
                        limit=1
                    )

                try:
                    period = p[0]
                except IndexError:
                    raise osv.except_osv(
                        _('Error'),
                        _(u"No s'ha trobat el periode, cal instal·lar "
                          u"giscedata_perfils_ANY.")
                    )

                return periodes[period['periode']]
            else:
                return periodes.values()[0]

GiscedataPolissaTarifa()
