# -*- coding: utf-8 -*-
import logging

from destral import testing
from destral.transaction import Transaction
from giscedata_perfils.giscedata_perfils import GiscedataPerfilsLot


class TestCheckCLMAG(testing.OOTestCase):

    CLMAG_without_change_time = """30;10;2017;0336;0927;GI;E0;30;E3;04;AE;12;10;
    12;10;12;10;12;10;13;10;13;10;11;10;11;10;13;10;13;10;14;10;14;10;14;10;14;
    10;16;10;16;10;18;10;18;10;18;10;18;10;21;10;21;10;20;10;20;10;19;10;19;10;
    21;10;21;10;18;10;18;10;17;10;17;10;17;10;17;10;16;10;16;10;19;10;19;10;16;
    10;16;10;18;10;18;10;14;10;14;10;12;10;12;10;13;10;13;10;0;0;0;0;"""

    CLMAG_with_change_time = """29;10;2017;0336;0927;GI;E0;30;E3;04;AE;14;10;14;
    10;15;10;15;10;13;10;13;10;13;10;13;10;12;10;12;10;13;10;13;10;12;10;12;10;
    15;10;15;10;13;10;13;10;11;10;11;10;10;10;10;10;12;10;12;10;13;10;13;10;16;
    10;16;10;12;10;12;10;11;10;11;10;13;10;13;10;13;10;13;10;13;10;13;10;12;10;
    12;10;12;10;12;10;15;10;15;10;16;10;16;10;13;10;13;10;14;10;14;10;"""

    # Consumptions: get first element, to each 4-tuple
    CLMAG_without_change_time_consumption = [
        12, 12, 13, 11, 13, 14, 14, 16, 18, 18, 21, 20, 19, 21, 18, 17, 17, 16,
        19, 16, 18, 14, 12, 13, 0
    ]
    CLMAG_with_change_time_consumption = [
        14, 15, 13, 13, 12, 13, 12, 15, 13, 11, 10, 12, 13, 16, 12, 11, 13, 13,
        13, 12, 12, 15, 16, 13, 14
    ]

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_check_clmag(self):
        consumption = 0
        clmag_line = self.CLMAG_without_change_time.split(';')
        for pos in GiscedataPerfilsLot.clmag_positions:
            consumption += int(clmag_line[pos])

        expected = {
            'hours': len(self.CLMAG_without_change_time_consumption),
            'consumption': sum(self.CLMAG_without_change_time_consumption)
        }
        result = {
            'hours': len(GiscedataPerfilsLot.clmag_positions),
            'consumption': consumption
        }
        self.assertEqual(expected, result)

    def test_check_clmag_with_time_change(self):
        consumption = 0
        clmag_line = self.CLMAG_with_change_time.split(';')
        for pos in GiscedataPerfilsLot.clmag_positions:
            consumption += int(clmag_line[pos])

        expected = {
            'hours': len(self.CLMAG_with_change_time_consumption),
            'consumption': sum(self.CLMAG_with_change_time_consumption)
        }
        result = {
            'hours': len(GiscedataPerfilsLot.clmag_positions),
            'consumption': consumption
        }
        self.assertEqual(expected, result)

