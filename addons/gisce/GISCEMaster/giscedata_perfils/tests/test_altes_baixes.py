# -*- coding: utf-8 -*-
import logging

from destral import testing
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from dateutil.relativedelta import relativedelta

class TestBaixesAgregacio(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def parse_first_day_of_last_month(self, date):
        to_date = datetime.strptime(date, '%Y-%m-%d')
        from_date = to_date + relativedelta(days=1)

        return from_date.strftime('%Y-%m-%d')

    def get_agg(self):
        """
        Obtain a browse demo aggregation
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        aggr_obj = self.openerp.pool.get('giscedata.perfils.agregacions')
        pol_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_perfils', 'agregacio_01'
        )[1]

        return aggr_obj.browse(self.cursor, self.uid, pol_id)

    def test_baixes(self):
        aggr_obj = self.openerp.pool.get('giscedata.perfils.agregacions')
        first_agg = self.get_agg()

        # Create second aggregation with same values, changing province
        agg_data = aggr_obj.read(self.cursor, self.uid, first_agg.id)
        agg_data.pop('id')
        agg_data['lot_id'] = first_agg.lot_id.id
        scnd_agg_id = aggr_obj.create(self.cursor, self.uid, agg_data)
        scnd_agg = aggr_obj.browse(self.cursor, self.uid, scnd_agg_id)

        # Stablish di, df for first aggregation
        # Set date end agg to day 5 of the month
        first_agg.write({
            'data_inici': first_agg.lot_id.data_inici,
            'data_final': first_agg.lot_id.data_final[:8] + '05'
        })

        # Stablish di, df, and province for second aggregation
        scnd_agg.write({
            'data_inici': first_agg.lot_id.data_inici,
            'data_final': self.parse_first_day_of_last_month(
                first_agg.lot_id.data_final
            ),
            'provincia': 'FP'
        })

        # Refresh aggs
        first_agg = aggr_obj.browse(self.cursor, self.uid, first_agg.id)
        scnd_agg = aggr_obj.browse(self.cursor, self.uid, scnd_agg.id)

        expect = aggr_obj.is_baixa_ag(self.cursor, self.uid, first_agg)
        self.assertEqual(expect, True, "Baja agregación no correcta")
