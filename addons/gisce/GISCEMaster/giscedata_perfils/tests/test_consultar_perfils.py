from destral import testing
from destral.transaction import Transaction


class TestConsultarPerfils(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_generic_polissa(self, pol_num):
        """
        Obtain a generic_policy browse
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', pol_num
        )[1]
        pol = pol_obj.browse(self.cursor, self.uid, pol_id)

        return pol

    def crear_lots_perfilacio(self, data_alta):
        """
        Create a year of profiling batches getting year from data_alta
        """
        year = data_alta[:4]
        lot_obj = self.openerp.pool.get('giscedata.perfils.lot')
        lot_obj.crear_lots_mensuals(self.cursor, self.uid, int(year))
        lot_id = lot_obj.search(self.cursor, self.uid,
                                [('data_inici', '=', data_alta)])[0]
        # Open batch
        lot_obj.browse(self.cursor, self.uid, lot_id).obrir()

        return lot_id

    @staticmethod
    def parse_wiz_onchange(data):
        return data['domain']['polissa'][0][2]

    def test_no_filtrar_polisses_lot(self):
        wiz_obj = self.openerp.pool.get('wizard.filter.profiles')
        lot_id = 1
        wiz_id = wiz_obj.create(self.cursor, self.uid, {'lot': lot_id})
        wiz = wiz_obj.browse(self.cursor, self.uid, wiz_id)
        filter = wiz.on_change_lot(lot_id)
        self.assertEqual(
            len(self.parse_wiz_onchange(filter)), 0,
            'Polisses no filtrades correctament'
        )

    def test_filtrar_polisses_lot(self):
        """
        Test to check profile new assigned policy
        """
        pol = self.get_generic_polissa('polissa_0001')
        lot_id = self.crear_lots_perfilacio(pol.data_alta)
        pol.write({'lot_perfilacio': lot_id})
        pol_two = self.get_generic_polissa('polissa_0002')
        pol_two.write({'lot_perfilacio': lot_id})

        wiz_obj = self.openerp.pool.get('wizard.filter.profiles')
        wiz_id = wiz_obj.create(self.cursor, self.uid, {'lot': lot_id})
        wiz = wiz_obj.browse(self.cursor, self.uid, wiz_id)
        filter = wiz.on_change_lot(lot_id)
        self.assertEqual(
            len(self.parse_wiz_onchange(filter)), 2,
            'Polisses no filtrades correctament'
        )

        self.assertEqual(
            sorted([pol.id, pol_two.id]),
            sorted(self.parse_wiz_onchange(filter)),
            'Polisses no filtrades correctament'
        )
