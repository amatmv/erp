from destral import testing
from destral.transaction import Transaction
from giscedata_polissa.tests.utils import activar_polissa, crear_modcon
import random
from datetime import datetime
from osv import osv
from dateutil.relativedelta import relativedelta


class TestPerfilar(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_polissa(self):
        """
        Obtain a browse demo policy
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = pol_obj.browse(self.cursor, self.uid, pol_id)

        return pol

    def get_factura_dso(self):
        """
        Obtain a browse dso invoice
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        inv_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_facturacio', 'factura_dso_0001'
        )[1]
        inv = inv_obj.browse(self.cursor, self.uid, inv_id)

        return inv

    def set_ree_partner_code(self, comer_id, distri_id=None):
        """
        Set fake REE partner code to policy
        """
        partner_obj = self.openerp.pool.get('res.partner')
        if distri_id:
            fake_distri = '1234'
            partner_obj.write(
                self.cursor, self.uid, distri_id, {'ref': fake_distri}
            )
        if comer_id:
            fake_comer = random.randint(9999, 99999)
            partner_obj.write(
                self.cursor, self.uid, comer_id, {'ref': str(fake_comer)}
            )

    def crear_lots_perfilacio(self, data_alta):
        """
        Create a year of profiling batches getting year from data_alta
        """
        year = data_alta[:4]
        lot_obj = self.openerp.pool.get('giscedata.perfils.lot')
        lot_obj.crear_lots_mensuals(self.cursor, self.uid, int(year))
        lot_id = lot_obj.search(self.cursor, self.uid,
                                [('data_inici', '=', data_alta)])[0]
        # Open batch
        lot_obj.browse(self.cursor, self.uid, lot_id).obrir()

        return lot_id

    def tancar_lot_perfilacio(self, lot_id):
        """
        Closes profiling batch
        """
        lot_obj = self.openerp.pool.get('giscedata.perfils.lot')
        lot_obj.browse(self.cursor, self.uid, lot_id).tancar()

    def activar_polissa(self, polissa_id, data_alta):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        polissa_obj.write(self.cursor, self.uid, [polissa_id], {
            'data_alta': data_alta, 'data_baixa': False,
            'lot_facturacio': False, 'comercialitzadora': 1
        })
        activar_polissa(self.openerp.pool, self.cursor, self.uid, polissa_id)

        return polissa_id

    def test_perfilar_nova_polissa_sense_assignar(self):
        """
        Test profile a new policy without assign, should give an error of not
        assigned to profiling batch
        """
        pol = self.get_polissa()
        data_alta = '2017-01-01'
        self.activar_polissa(pol.id, data_alta)
        lot_id = self.crear_lots_perfilacio(pol.data_alta)

        # Assign batch
        pol.write({'lot_perfilacio': lot_id,
                   'data_ultima_lectura_perfilada': '2016-12-31',
                   'potencia': 3.300, 'tg': '2'})

        distri_id = pol.modcontractual_activa.cups.distribuidora_id.id
        comer_id = pol.modcontractual_activa.comercialitzadora.id
        self.set_ree_partner_code(comer_id=comer_id, distri_id=distri_id)

        self.assertRaises(osv.except_osv, pol.perfilar, lot_id)

    def test_perfilar_canvi_agregacio(self):
        """
        First profile a new policy, activate new modcon and profile second time
        Profile first month: 2016-12-01 to 2016-12-01
        """
        perfil_obj = self.openerp.pool.get('giscedata.perfils.perfil')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol = self.get_polissa()
        data_alta = '2016-12-01'
        self.activar_polissa(pol.id, data_alta)
        lot_id = self.crear_lots_perfilacio(pol.data_alta)

        # Assign batch
        pol.write({'lot_perfilacio': lot_id,
                   'data_ultima_lectura_perfilada': '2016-12-01',
                   'potencia': 3.300, 'tg': '2'})

        distri_id = pol.modcontractual_activa.cups.distribuidora_id.id
        comer_id = pol.modcontractual_activa.comercialitzadora.id
        self.set_ree_partner_code(comer_id=comer_id, distri_id=distri_id)

        pol.perfilar(lot_id=lot_id)

        # Refresh the policy
        pol_data = pol_obj.read(self.cursor, self.uid, pol.id,
                                ['data_ultima_lectura_perfilada', 'cups'])
        ultima_perf_pol = datetime.strptime(
            pol_data['data_ultima_lectura_perfilada'], '%Y-%m-%d'
        )
        ultima_perf_theo = datetime.strptime(
            data_alta, '%Y-%m-%d'
        ) + relativedelta(months=1) - relativedelta(days=1)

        self.assertEqual(
            ultima_perf_pol, ultima_perf_theo,
            'data_ultima_lectura_perfilada incorrecte'
        )

        spected = ultima_perf_theo.day * 24
        profiled = len(perfil_obj.search(self.cursor, self.uid, [
            ('cups', '=', pol_data['cups'][1])]))

        self.assertEqual(profiled, spected, 'No perfilat correctament')

        # New modcon (comer change)
        self.set_ree_partner_code(comer_id=3)
        modcon_changes = {'comercialitzadora': 3}
        crear_modcon(self.openerp.pool, self.cursor, self.uid, pol.id,
                     modcon_changes, '2017-01-01', '2018-01-31')
        pol_obj.send_signal(
            self.cursor, self.uid, [pol.id], ['validar', 'contracte']
        )
        self.tancar_lot_perfilacio(lot_id)
        lot_id = self.crear_lots_perfilacio('2017-01-01')

        # Refresh the policy
        pol = self.get_polissa()
        # Assign batch
        pol.write({'lot_perfilacio': lot_id})
        pol.perfilar(lot_id=lot_id)

        profiled = len(
            perfil_obj.search(self.cursor, self.uid, [
                ('cups', '=', pol_data['cups'][1]),('name', '>=', '2017010100'),
                ('name', '<=', '2017013124')])
        )

        self.assertEqual(profiled, 744)

    def test_perfilar_nova_polissa_lot_data(self):
        """
        Test profile a new policy with assigned batch and
        data_ultima_lectura_perfilada assigned
        """
        perfil_obj = self.openerp.pool.get('giscedata.perfils.perfil')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol = self.get_polissa()
        data_alta = '2017-01-01'
        self.activar_polissa(pol.id, data_alta)
        lot_id = self.crear_lots_perfilacio(pol.data_alta)

        # Assign batch
        pol.write({'lot_perfilacio': lot_id,
                   'data_ultima_lectura_perfilada': data_alta,
                   'potencia': 3.300, 'tg': '2'})

        pol = self.get_polissa()
        distri_id = pol.modcontractual_activa.cups.distribuidora_id.id
        comer_id = pol.modcontractual_activa.comercialitzadora.id
        self.set_ree_partner_code(comer_id=comer_id, distri_id=distri_id)

        pol.perfilar(lot_id=lot_id)

        # Refresh the policy
        pol_data = pol_obj.read(self.cursor, self.uid, pol.id,
                                ['data_ultima_lectura_perfilada', 'cups'])
        ultima_perf_pol = datetime.strptime(
            pol_data['data_ultima_lectura_perfilada'], '%Y-%m-%d'
        )
        ultima_perf_theo = datetime.strptime(
            data_alta, '%Y-%m-%d'
        ) + relativedelta(months=1) - relativedelta(days=1)

        self.assertEqual(
            ultima_perf_pol, ultima_perf_theo,
            'data_ultima_lectura_perfilada incorrecte'
        )

        spected = ultima_perf_theo.day * 24
        profiled = len(perfil_obj.search(self.cursor, self.uid, [
            ('cups', '=', pol_data['cups'][1])]))

        self.assertEqual(profiled, spected, 'No perfilat correctament')

    def test_perfilar_nova_polissa_assignada(self):
        """
        Test to check profile new assigned policy
        """
        perfil_obj = self.openerp.pool.get('giscedata.perfils.perfil')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol = self.get_polissa()
        data_alta = '2017-01-01'
        self.activar_polissa(pol.id, data_alta)
        lot_id = self.crear_lots_perfilacio(pol.data_alta)

        pol.write({'potencia': 3.300, 'tg': '2'})

        # Prepare invoice to assign policy to profile
        inv = self.get_factura_dso()
        inv.write({
            'data_inici': data_alta, 'data_final': '2017-01-31',
            'check_total': inv.amount_total
        })
        inv.invoice_open()

        # Assign batch
        wiz_obj = self.openerp.pool.get('perfils.assignar.polisses')
        wiz_id = wiz_obj.create(self.cursor, self.uid, {'lot_id': lot_id})
        wiz = wiz_obj.browse(self.cursor, self.uid, wiz_id)
        wiz.buscar_in()
        wiz.assignar()

        pol = self.get_polissa()
        pol.write({'data_ultima_lectura_perfilada': data_alta})
        pol.write({'lot_perfilacio': lot_id})
        distri_id = pol.modcontractual_activa.cups.distribuidora_id.id
        comer_id = pol.modcontractual_activa.comercialitzadora.id
        self.set_ree_partner_code(comer_id=comer_id, distri_id=distri_id)

        pol.perfilar(lot_id=lot_id)

        # Refresh the policy
        pol_data = pol_obj.read(self.cursor, self.uid, pol.id,
                                ['data_ultima_lectura_perfilada', 'cups'])
        ultima_perf_pol = datetime.strptime(
            pol_data['data_ultima_lectura_perfilada'], '%Y-%m-%d'
        )
        ultima_perf_theo = datetime.strptime(
            data_alta, '%Y-%m-%d'
        ) + relativedelta(months=1) - relativedelta(days=1)

        self.assertEqual(
            ultima_perf_pol, ultima_perf_theo,
            'data_ultima_lectura_perfilada incorrecte'
        )

        spected = ultima_perf_theo.day * 24
        profiled = len(perfil_obj.search(self.cursor, self.uid, [
            ('cups', '=', pol_data['cups'][1])]))

        self.assertEqual(profiled, spected, 'No perfilat correctament')
