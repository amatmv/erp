# -*- coding: utf-8 -*-
import calendar
import multiprocessing
import traceback
import time
import os
import csv
from datetime import datetime, timedelta
from StringIO import StringIO

import netsvc
from osv import osv, fields
from osv.osv import TransactionExecute
from tools import config, misc, cache
from tools.translate import _
from base_extended.base_extended import MultiprocessBackground, NoDependency

from zipfile import ZipFile
import bz2
from enerdata.profiles import Dragger
from oorq.decorators import job
from autoworker import AutoWorker
import pandas as pd
from base64 import b64encode
import os
import tempfile

import logging


MAGNITUDS = {'AE': 'A', 'R1': 'R'}

TIPOS = [('1', 'Tipo 1'),
         ('2', 'Tipo 2'),
         ('3', 'Tipo 3'),
         ('4', 'Tipo 4'),
         ('5', 'Tipo 5')]

MEASURE_TYPE_FROM_PROFILE_TO_REPORT = {
    'AE': 'active_input',
    'R1': 'reactive_quadrant1',
    'R4': 'reactive_quadrant4'
}
MEASURE_TYPE_FROM_TG_PROFILE_TO_REPORT = {
    'ai': 'active_input',
    'r1': 'reactive_quadrant1',
    'r4': 'reactive_quadrant4'
}

def period_to_tg_profile_timestamps(first_date, last_date):
    """
    Get fist and last tg.profile slot timestamp from a period.

    :param fisrt_date: first date of the period
    :param last_date: last date of the period
    :return: two tg.profile timestamps
    """
    first_datetime = datetime.strptime(first_date[:10], '%Y-%m-%d')
    first_timestamp = first_datetime.strftime('%Y-%m-%d 01:00:00')

    last_datetime = datetime.strptime(last_date[:10], '%Y-%m-%d')
    next_day = last_datetime + timedelta(days=1)
    last_timestamp = next_day.strftime('%Y-%m-%d 00:00:00')

    return first_timestamp, last_timestamp


def last_sunday(year, month):
    """Retorna l'últim diumenge del mes, serveix per determinar quin dia
    s'ha de canviar l'hora.
    """
    for day in reversed(xrange(1, calendar.monthrange(year, month)[1] + 1)):
        if calendar.weekday(year, month, day) == calendar.SUNDAY:
            return day


def timestamp_from_profile(profile):
    """
    Get timestamp from profile.

    :param profile: the profile
    :return: string with the timestamp
    """

    if profile.hour == 24:
        hour = 0
        delta = timedelta(days=1)
    else:
        hour = profile.hour
        delta = timedelta(0)

    timestamp = datetime(
        int(profile.year),
        int(profile.month),
        int(profile.day),
        hour
    ) + delta

    return timestamp.strftime("%Y/%m/%d %H:00:00")


def create_perfils_crm_case(self, cursor, uid, title, msg):
    """
    Create a crm_case with perfils measure file error
    :return: crm_case_id
    """
    crm_obj = TransactionExecute(cursor.dbname, uid, 'crm.case')
    crm_section_obj = self.pool.get('crm.case.section')
    section_id = crm_section_obj.search(cursor, uid, [('code', '=', 'PERFILS')])[0]

    # check if crm case already exists
    crm_id = crm_obj.search(cursor, uid, [('section_id', '=', section_id), ('name', '=', title)])
    if crm_id:
        if isinstance(crm_id, (list, tuple)):
            crm_id = crm_id[0]

        # check if there already is a description
        case_data = crm_obj.read(cursor, uid, crm_id, ['description'])
        if case_data['description']:
            description = case_data['description'] + '\n' + msg
            crm_obj.write(cursor, uid, crm_id, {'description': description})
        else:
            crm_obj.write(cursor, uid, crm_id, {'description': msg})
    else:
        crm_case_vals = {
            'name': title,
            'section_id': section_id,
            'user_id': uid,
            'description': msg,
        }
        # Create crm case
        crm_id = crm_obj.create(cursor, uid, crm_case_vals)

    return self.case_open(cursor, uid, [crm_id])


class GiscedataPerfilsPerfil(osv.osv):
    _name = 'giscedata.perfils.perfil'

    def get_perfils_cups(self, cursor, uid, cups_name, data_inici, data_final,
                         context=None):
        """Returns the list of ids of giscedata.perfils.perfil corresponent
        to cups_name between data_inici and data_final

        :param cursor: database cursor
        :param uid: user identifier
        :param cups_name: name of the CUPS we want the "prefils" of
        (ex. ES1234000000000001JN0F)
        :param data_inici: date from which we start to get "perfils" (included)
        :param data_final: last date from where we will get "perfils" (included)
        :param context: current context
        :return: the list of "perfils" between data_inici and data_final for
        the given CUPS
        """
        if context is None:
            context = {}

        inici = '%s00' % data_inici.replace('-', '')
        final = '%s24' % data_final.replace('-', '')

        return self.search(cursor, uid, [
            ('cups', '=', cups_name),
            ('name', '>=', inici),
            ('name', '<=', final)
        ])

    def get_sum_perfils_cups(self, cursor, uid, cups_name, data_inici,
                             data_final, context=None):
        """Returns the total of all the "perfils" for the given CUPS between
        data_inici and data_final

        :param cursor: database cursor
        :param uid: user identifier
        :param cups_name: name of the CUPS we want the total of "prefils" of
        (ex. ES1234000000000001JN0F)
        :param data_inici: date from which we start to sum "perfils" (included)
        :param data_final: last date from where we will sum "perfils" (included)
        :param context: current context
        :return: the total of the sum of all "perfils" between data_inici and
        data_final for the given CUPS
        """
        if context is None:
            context = {}

        pids = self.get_perfils_cups(
            cursor, uid, cups_name, data_inici, data_final, context
        )

        perfils = self.read(cursor, uid, pids, ['aprox'], context)
        return sum([x['aprox'] for x in perfils])

    def borrar_perfils_cups(self, cursor, uid, cups_name, data_inici,
                            data_final, context=None):
        """Deletes all the "prefils" for the given CUPS between data_inici
        and data_final

        :param cursor: database cursor
        :param uid: user identifier
        :param cups_name: name of the CUPS we want the total of "prefils" of
        (ex. ES1234000000000001JN0F)
        :param data_inici: date from which we start to delete them (included)
        :param data_final: last date from where we will delete them (included)
        :param context: current context
        :return: the number of deleted "perfils"
        """
        if context is None:
            context = {}

        pids = self.get_perfils_cups(
            cursor, uid, cups_name, data_inici, data_final
        )

        self.unlink(cursor, uid, pids, context)

        return len(pids)

    _columns = {
        'name': fields.char('Timestamp', size=10, required=True),
        'cups': fields.char('CUPS', size=25, required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char(
            'Comercialitzadora',
            size=4,
            required=True
        ),
        'provincia': fields.char('Provincia', size=2, required=True),
        'agree_tensio': fields.char('Nivell Tensió', size=4, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=4, required=True),
        'agree_autoproductor': fields.selection(
            [
                ('0', 'No autoproductor'),
                ('1', 'Autoproductor')
            ],
            'Autoproductor',
        ),
        'agree_tipo': fields.selection(
            [
                ('01', 'Tipo 1'),
                ('02', 'Tipo 2'),
                ('03', 'Tipo 3'),
                ('04', 'Tipo 4'),
                ('05', 'Tipo 5')
            ],
            'Tipo de Punto',
            required=True
        ),
        'magnitud': fields.char('Maginitud', size=2, required=True),
        'mesura_ab': fields.char(
            'Indicador mesura ALTA/BAIXA',
            size=1,
            required=True
        ),
        'year': fields.char('Any', size=4, required=True),
        'month': fields.char('Mes', size=2, required=True),
        'day': fields.char('Dia', size=2, required=True),
        'hour': fields.integer('Hora', required=True),
        'estacio': fields.selection([('0', 'Hivern'), ('1', 'Estiu')], 'Estació',
                                    required=True),
        'cof': fields.float('Coeficient', digits=(13, 12), required=True),
        'sum_cofs': fields.float('Sumatori Coficient', digits=(16, 12),
                                 required=True),
        'lectura': fields.integer('Lectura', required=True),
        'mesura_anual': fields.float('Mesura Anual', digits=(16, 12),
                                     required=True),
        'mesura_mensual': fields.float('Mesura Mensual', digits=(16, 12),
                                       required=True),
        'aprox': fields.integer('Arrodoniment (Aprox.)', required=True),
        'decimals': fields.float('Decimals sobrants', digits=(13, 12),
                                 required=True),
        'real': fields.boolean('Origen Real', required=True)
    }

    _defaults = {
        'real': lambda *a: 0,
    }

    _sql_constraints = [('name_cups_estacio_magnitud',
                         'unique (cups,year,month,day,hour,estacio,magnitud)',
                         'Ya existe la hora perfilada por este CUPS')]

    _order = "name asc"


GiscedataPerfilsPerfil()


class GiscedataPerfilsVigencia(osv.osv):
    _name = 'giscedata.perfils.vigencia'

    _columns = {
        'name': fields.char('Descripció', size=256, required=True),
        'inici': fields.date('Inici Aplicació', required=True),
        'final': fields.date('Final Aplicació', required=True),
    }


GiscedataPerfilsVigencia()


class GiscedataPerfilsHLaborables(osv.osv):
    _name = 'giscedata.perfils.hlaborables'

    def get_hores(self, cr, uid, periode, inici, final):
        res = 0
        search_params = [('estacio', '=', 1),
                         ('periode.id', '=', periode),
                         ('vigencia.inici', '<=', inici),
                         ('vigencia.final', '>=', final)]
        ids = self.search(cr, uid, search_params)
        for h in self.read(cr, uid, ids, ['hinici', 'hfinal']):
            res += int(h['hfinal'][:2]) - int(h['hinici'][:2])
        return res


    _columns = {
        'periode': fields.many2one('giscedata.polissa.tarifa.periodes', 'Periode',
                                   required=True, select=True),
        'dinici': fields.date('Dia Inici', required=True, select=True),
        'dfinal': fields.date('Dia Final', required=True, select=True),
        'hinici': fields.time('Hora Inici', required=True),
        'hfinal': fields.time('Hora Final', required=True),
        'estacio': fields.selection([('0', 'Hivern'), ('1', 'Estiu')], 'Estació',
                                    select=True),
        'vigencia': fields.many2one('giscedata.perfils.vigencia',
                                    'Vigencia', required=True)
    }


GiscedataPerfilsHLaborables()


class GiscedataPerfilsHFestius(osv.osv):
    _name = 'giscedata.perfils.hfestius'

    def get_hores(self, cr, uid, periode, inici, final):
        res = 0
        search_params = [('estacio', '=', 1),
                         ('periode.id', '=', periode),
                         ('vigencia.inici', '<=', inici),
                         ('vigencia.final', '>=', final)]
        ids = self.search(cr, uid, search_params)
        for h in self.read(cr, uid, ids, ['hinici', 'hfinal']):
            res += int(h['hfinal'][:2]) - int(h['hinici'][:2])
        return res

    _columns = {
        'periode': fields.many2one('giscedata.polissa.tarifa.periodes', 'Periode',
                                   required=True),
        'dinici': fields.date('Dia Inici', required=True),
        'dfinal': fields.date('Dia Final', required=True),
        'hinici': fields.time('Hora Inici', required=True),
        'hfinal': fields.time('Hora Final', required=True),
        'estacio': fields.selection([('0', 'Hivern'), ('1', 'Estiu')], 'Estació'),
        'vigencia': fields.many2one('giscedata.perfils.vigencia',
                                    'Vigencia', required=True)
    }


GiscedataPerfilsHFestius()


class GiscedataPerfilsREE(osv.osv):
    _name = 'giscedata.perfils.ree'

    _columns = {
        'name': fields.char('Timestamp', size=10, required=True, readonly=True),
        'year': fields.char('Any', size=4, required=True, readonly=True),
        'month': fields.char('Mes', size=2, required=True, readonly=True),
        'day': fields.char('Dia', size=2, required=True, readonly=True),
        'hour': fields.integer('Hora', required=True, readonly=True),
        'estacio': fields.selection([('1', 'Estiu'), ('0', 'Hivern')], 'Estació',
                                    required=True, readonly=True),
        'cof_a': fields.float('Cof. Perfil A', digits=(13, 12), readonly=True),
        'cof_b': fields.float('Cof. Perfil B', digits=(13, 12), readonly=True),
        'cof_c': fields.float('Cof. Perfil C', digits=(13, 12), readonly=True),
        'cof_d': fields.float('Cof. Perfil D', digits=(13, 12), readonly=True),
    }

    _sql_constraints = [('line_uniq',
                         'unique (year, month, day, hour, estacio)',
                         'Ja existeix la clau (any, mes, dia, hora) en els '
                         'perfils de la REE.')]

    _bloc_code = multiprocessing.Lock()

    @cache()
    def get_cofs(self, cursor, uid, data_inici, data_final):
        """Aquesta funció retorna els coeficients entre les dates passades.

        Si no existeixen els coeficients entre dates se'ls descarrega.
        """
        logger = netsvc.Logger()
        tsi = '%s00' % data_inici.replace('-', '')
        tsf = '%s24' % data_final.replace('-', '')
        try:
            self._bloc_code.acquire()

            # Check availability of coefficients and download it
            first_day_of_month = datetime.strptime(data_inici[:-3], '%Y-%m')
            first_day_last_month = datetime.strptime(data_final[:-3], '%Y-%m')
            while first_day_of_month <= first_day_last_month:
                name = first_day_of_month.strftime('%Y%m%d01')
                if not self.search(cursor, uid, [('name','=', name)]):
                    self.download(
                        cursor,
                        uid,
                        first_day_of_month.year,
                        first_day_of_month.month
                    )
                days_of_month = calendar.monthrange(
                    first_day_of_month.year,
                    first_day_of_month.month
                )[1]
                first_day_of_month += timedelta(days=days_of_month)

            search_params = [('name', '>', tsi),
                             ('name', '<=', tsf)]
            logger.notifyChannel('objects', netsvc.LOG_INFO,
                                 'Obtenint coficients des de %s a %s' % (tsi, tsf))
            perf_ids = self.search(cursor, uid, search_params)
            vals = sorted(self.read(cursor, uid, perf_ids), key=lambda k: k['name'])
            return vals
        except Exception as e:
            raise Exception(_(u"Error obtenint els coeficients"))
        finally:
            self._bloc_code.release()
        return vals

    def download(self, cursor, uid, year, month):
        year = str(year)
        month = str(month).zfill(2)
        import httplib

        logger = netsvc.Logger()
        perff_file = 'PERFF_%s%s.gz' % (year, month)
        conn = httplib.HTTPSConnection('www.ree.es')
        conn.request('GET', '/sites/default/files/simel/perff/%s' % perff_file)
        r = conn.getresponse()
        if r.msg.type == 'application/x-gzip':
            # Tenim el fitxer de perfils
            logger.notifyChannel("objects", netsvc.LOG_INFO,
                                 'REE::Descarregant %s...' % perff_file)
            # Existeix, el podem descomprimir i importar
            import gzip

            c = StringIO(r.read())
            m = StringIO(gzip.GzipFile(fileobj=c).read())
            c.close()

            # Importem el fitxer
            reader = csv.reader(m, delimiter=';')
            skip = False
            logger.notifyChannel("objects", netsvc.LOG_INFO,
                                 'REE:Importat %s' % perff_file)
            for row in reader:
                name = '%s%s%s%s' % (row[0], row[1], row[2], row[3].zfill(2))
                if skip:
                    vals = {
                        'name': name,
                        'year': row[0],
                        'month': row[1],
                        'day': row[2],
                        'hour': row[3],
                        'estacio': row[4],
                        'cof_a': row[5],
                        'cof_b': row[6],
                        'cof_c': row[7],
                        'cof_d': row[8]
                    }
                    try:
                        search_params = [('year', '=', vals['year']),
                                         ('month', '=', vals['month']),
                                         ('day', '=', vals['day']),
                                         ('hour', '=', vals['hour']),
                                         ('estacio', '=', vals['estacio'])]
                        exist_ids = self.search(cursor, uid, search_params)
                        if not exist_ids:
                            self.create(cursor, uid, vals)
                    except Exception:
                        pass
                skip = True
            m.close()
            conn.close()
        else:
            conn.close()
            raise Exception(
                _(u"Fitxers de coeficients de REE no trobats.")
            )
        return True

    def sum_cofs(self, cursor, uid, tarifa_id, data_inici, data_final,
                 context=None):
        """Retorna la repartició de coeficients segons la data inici i final.
        """
        if not context:
            context = {}
        logger = netsvc.Logger()
        logger.notifyChannel('objects', netsvc.LOG_INFO,
                             'Sumatori de coficients des de %s a %s'
                             % (data_inici, data_final))
        tarifa = self.pool.get('giscedata.polissa.tarifa').browse(cursor, uid,
                                                                  tarifa_id)
        sum_cofs = {}
        # Inicialitzem el diccionari de coeficients per cada periode d'energia
        # que tingui la tarifa
        periodes = {}
        for p in tarifa.periodes:
            if p.tipus == 'te':
                sum_cofs[p.name] = 0
                periodes[p.id] = p.name
        # Busquem tots els coeficients desde data_inici fins a data_final
        for perf in self.get_cofs(cursor, uid, data_inici, data_final):
            if len(periodes) > 1:
                dia = '%s-%s-%s' % (perf['year'], perf['month'], perf['day'])
                hour = '%02i:00:00' % (perf['hour'] - 1)
                # Al fer servir cached a aquesta funció la primera tarda a
                # calcular-la però després respon molt bé.
                p_name = tarifa.get_periode_ts(dia, hour, perf['estacio'],
                                               context=context)
            else:
                p_name = sum_cofs.keys()[0]
            # Sumem al periode el coficient
            sum_cofs[p_name] += perf[tarifa.cof]
        return sum_cofs


GiscedataPerfilsREE()


class GiscedataPerfilsLot(osv.osv):
    """Lots per perfilar les pòlisses, així les podem agrupar i saber l'estat.
    """
    _name = 'giscedata.perfils.lot'

    columns = [
        'distribuidora', 'comercialitzadora', 'provincia', 'agree_tensio',
        'agree_tarifa', 'agree_dh', 'agree_tipo'
    ]
    aggregations_columns = [
        'distribuidora', 'comercialitzadora', 'provincia', 'agree_tensio',
        'agree_tarifa', 'agree_dh', 'agree_tipo', 'agree_dh', 'consum'
    ]

    clmag_positions = [x for x in range(11, 111, 4)]

    def check_files(self, cursor, uid, ids, context=None):
        """ Treu un sumatori amb els consums de: agregacions, CLMAG, CLMAG5A i
        CLINME d'aquest lot
        """
        df_clmag = self.check_clmag(cursor, uid, ids, context)
        df_clmag5a = self.check_clmag5a(cursor, uid, ids, context)
        df_clinme = self.check_clinme(cursor, uid, ids, context)
        df_aggregations = self.check_aggregations(cursor, uid, ids, context)

        df_merged = pd.DataFrame.merge(df_aggregations, df_clinme)
        df_merged = pd.DataFrame.merge(df_merged, df_clmag, how='outer')
        df_merged = pd.DataFrame.merge(df_merged, df_clmag5a, how='outer')
        self.aggregations_columns.append('clmag_consumption')
        self.aggregations_columns.append('clinme_consumption')
        self.aggregations_columns.append('clmag5a_consumption')

        _id, _file = tempfile.mkstemp()
        _date = datetime.now().strftime('%d%m%Y')
        filename = 'consumptions_validations_{date}.csv'.format(date=_date)
        df_merged.to_csv(_file, sep=';', columns=self.aggregations_columns)
        with open(_file) as f:
            file_content = f.read()

        self.pool.get('ir.attachment').create(cursor, uid, {
            'name': filename,
            'datas': b64encode(file_content),
            'datas_fname': filename,
            'res_model': 'giscedata.perfils.lot',
            'res_id': ids[0]
        })
        agg_val_obj = self.pool.get('giscedata.perfils.agregacions.validacions')
        agg_val_obj.create(cursor, uid, {'filename': _file, 'lot_id': ids[0]})
        os.unlink(_file)

    def check_aggregations(self, cursor, uid, ids, context=None):
        """ Retorna les agregacions i els seus consums
        :return: DataFrame amb agregació > consum
        """
        agregacions_ids = self.pool.get('giscedata.perfils.agregacions').search(
            cursor, uid, [('lot_id', '=', ids)]
        )
        df_aggregations = pd.DataFrame(
            data=self.pool.get('giscedata.perfils.agregacions').read(
                cursor, uid, agregacions_ids, self.aggregations_columns
            )
        )

        return df_aggregations.drop('id', axis=1)

    def check_clmag(self, cursor, uid, ids, context=None):
        """ Retorna el sumatori de consums per agregació dels tipus 3, 4
        :return: DataFrame amb agregació tipus 3, 4 -> consum
        """
        clmag_obj = self.pool.get('giscedata.perfils.clmag')

        # Get last CLMAG file version
        cursor.execute("""SELECT id FROM giscedata_perfils_clmag WHERE lot_id=%s
        ORDER BY create_date DESC LIMIT 1""", (ids[0],))
        clmag_id = cursor.fetchone()[0]
        clmag = clmag_obj.browse(cursor, uid, clmag_id)

        # Parse aggregations
        consumptions_clmag = []
        lines = clmag.gen_file()
        clmag_aggs = [agg[11:35] for agg in lines]
        df_aggs = pd.DataFrame(data=clmag_aggs).drop_duplicates()
        clmag_aggs = [x[0] for x in df_aggs.values.tolist()]

        for agg in clmag_aggs:
            consumption_iter = 0
            consumption_clmag = 0
            for x in lines:
                if agg in x:
                    consumption_tmp = x.split(';')
                    for pos in self.clmag_positions:
                        consumption_iter += int(consumption_tmp[pos])
                    consumption_clmag += consumption_iter
                    consumption_iter = 0

            aux = dict(zip(self.columns, agg.split(';')))
            aux['clmag_consumption'] = consumption_clmag
            consumptions_clmag.append(aux)

        return pd.DataFrame(data=consumptions_clmag)

    def check_clmag5a(self, cursor, uid, ids, context=None):
        """ Retorna el sumatori de consums per agregació dels tipus 3, 4
        :return: DataFrame amb agregació tipus 5 -> consum
        """
        clmag5a_obj = self.pool.get('giscedata.perfils.clmag5')

        # Get last CLMAG5A file version
        cursor.execute("""SELECT id FROM giscedata_perfils_clmag5 WHERE batch=%s
                ORDER BY create_date DESC LIMIT 1""", (ids[0],))
        clmag5a_id = cursor.fetchone()[0]
        clmag5a = clmag5a_obj.browse(cursor, uid, clmag5a_id)

        res = clmag5a.build_file()
        data = []
        for line in res.split(';\n'):
            line = line.split(';')
            vals = dict(zip(
                ['distribuidora', 'comercialitzadora', 'agree_tensio',
                 'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia'],
                line[:7]))
            vals['clmag5a_consumption'] = int(line[10])
            data.append(vals)
        df_clmag5a = pd.DataFrame(data=data).groupby(self.columns).aggregate(
            {'clmag5a_consumption': 'sum'})
        df_clmag5a = df_clmag5a.reset_index()

        return df_clmag5a

    def check_clinme(self, cursor, uid, ids, context=None):
        """ Agrega les dades del fitxer CLINME, reuinides per CUPS, en un
        sumatori de consums per agregació
        :return: DataFrame amb agregacions tipus 3, 4, 5 -> consum
        """
        clinme_obj = self.pool.get('giscedata.perfils.clinme')

        # Get last CLINME file version
        cursor.execute("""SELECT id FROM giscedata_perfils_clinme WHERE lot_id=%s
                ORDER BY create_date DESC LIMIT 1""", (ids[0],))
        clinme_id = cursor.fetchone()[0]
        clinme = clinme_obj.browse(cursor, uid, clinme_id)

        lines_tot = []
        for x in clinme.build_file(str_mode=True):
            for y in x.split(';\n'):
                try:
                    res = y.split(';')
                    value = {
                        'cups': res[0],
                        'distribuidora': res[1],
                        'comercialitzadora': res[2],
                        'agree_tensio': res[3],
                        'agree_tarifa': res[4],
                        'agree_dh': res[5],
                        'agree_tipo': res[6],
                        'provincia': res[7],
                        'baixa': res[8],
                        'clinme_consumption': int(res[11]),
                    }
                    lines_tot.append(value)
                except Exception:
                    continue  # last clinme aggregation nothing to do
        df_clinme = pd.DataFrame(data=lines_tot).groupby(
            self.columns).aggregate(
            {'clinme_consumption': 'sum'})
        return df_clinme.reset_index()

    def dump_perfils(self, cursor, uid, ids, fields=None, context=None, measure_type=None):
        """
        Create a dump of giscedata_perfils_perfil table of database. WARNING: This is vulnerable to SQL Injection.

        :param cursor: database cursor
        :param uid: user identificator
        :param ids: ids of batchs
        :param fields: the fields of giscedata_perfils_perfil table of database
        :param context: dict with context
        :param measure_type: list of measure types to include. If None all types are included. By default is None.
        :return: StringIO object with a table of profiles.
        """
        logger = netsvc.Logger()
        if not fields:
            fields = ['*']
        fields = ', '.join(fields)
        measure_type_filter = ''
        if measure_type is not None:
            if isinstance(measure_type, list):
                measure_type = ', '.join("'{0}'".format(item) for item in measure_type)
            measure_type_filter = " and agree_tipo in ({0})".format(measure_type)
        sql = ("SELECT %s FROM giscedata_perfils_perfil where name >= '%s' and name <= '%s'" + measure_type_filter)
        cmd = ['psql']
        if config['db_user']:
            cmd += ['--username=%s' % config['db_user']]
        if config['db_host']:
            cmd += ['--host=%s' % config['db_host']]
        if config['db_port']:
            cmd += ['--port=%s' % config['db_port']]
        cmd += [cursor.dbname, '-tA', '-c']
        for lot in self.browse(cursor, uid, ids, context):
            # Hem de fer un dump de la taula de perfils
            export_cmd = []
            export_cmd.extend(cmd)
            inici = '%s00' % lot.data_inici.replace('-', '')
            final = '%s24' % lot.data_final.replace('-', '')
            export_cmd += ['"%s"' % (sql % (fields, inici, final))]
            logger.notifyChannel("objects", netsvc.LOG_DEBUG, "Volcant perfils:"
                                                              " %s" % ' '.join(export_cmd))
            stdin, stdout = misc.exec_pg_command_pipe(*tuple(export_cmd))
            stdin.close()
            perf_file = StringIO(stdout.read())
            stdout.close()
            return perf_file

    def gen_clmag(self, cursor, uid, ids, context=None):
        """Genera el fitxer CLMAG.

        retorna una llista amb les línies del CLMAG.
        """
        logger = netsvc.Logger()
        logger_perfils = logging.getLogger('openerp.{}'.format(__name__))
        fields = ['comercialitzadora', 'agree_tensio', 'month', 'year',
                  'agree_dh', 'estacio', 'agree_tipo', 'distribuidora',
                  'aprox', 'day', 'hour', 'agree_tarifa', 'magnitud',
                  'provincia', 'real']
        for lot in self.browse(cursor, uid, ids, context):
            try:
                res = []
                year, month = [int(x) for x in lot.data_inici.split('-')[:2]]
                dia_canvi = last_sunday(year, month)
                # Hem de fer un dump de la taula de perfils
                measure_type = ['01','02','03','04']
                perf_file = lot.dump_perfils(fields, measure_type=measure_type)
                logger.notifyChannel("objects", netsvc.LOG_DEBUG, "Calculant "
                                                                  "perfils")
                perfs = csv.reader(perf_file, delimiter='|')
                agres = {}
                for n_perf, reg in enumerate(perfs, start=1):
                    extra = 0
                    key = "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;" % (
                        reg[9], reg[2], reg[3], reg[7], reg[0], reg[13], reg[1],
                        reg[11], reg[4], reg[6], reg[12])
                    if key not in agres:
                        agres[key] = {}
                        for i in range(1, 26):
                            #Utiltizem els quatre camps
                            agres[key][i] = [0, 0, 0, 0]
                    if reg[2] == '10':
                        # Hi ha canvi d'hora. 25 hores.
                        # Condició: Dia de canvi d'hora, ser hivern i que
                        # l'hora sigui més gran o igual que la 2
                        if int(reg[9]) == dia_canvi and reg[5] == '0' \
                            and int(reg[10]) >= 2:
                            extra = 1
                        else:
                            extra = 0
                    elif reg[2] == '03':
                        if int(reg[9]) == dia_canvi and int(reg[10]) == 3:
                            extra = -1
                        else:
                            extra = 0
                    agres[key][int(reg[10]) + extra][0] += int(reg[8])
                    agres[key][int(reg[10]) + extra][1] += 1
                    if reg[14]:
                        agres[key][int(reg[10]) + extra][2] += int(reg[8])
                        agres[key][int(reg[10]) + extra][3] += 1
                    logger_perfils.info('End perf, #{}'.format(n_perf))
                logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                     "Escrivint línies")
                for line, consums_dict in sorted(agres.items()):
                    consums = []
                    for i in xrange(1, 26):
                        consums.extend(consums_dict[i])
                    res.append("%s%s;\n" % (line, ";".join(map(str, consums))))
                logger.notifyChannel("objects", netsvc.LOG_DEBUG, "Feina feta")
                return res
            finally:
                # Sempre tanquem el fitxer d'exportació per evitar memory leaks
                logger.notifyChannel("objects", netsvc.LOG_DEBUG,
                                     "Tancant stream de perfils")
                perf_file.close()

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def make_clmag(self, cursor, uid, ids, context=None):
        """Genera el CLMAG i el posa a l'objecte de CLMAGs.
        """
        clmag_obj = self.pool.get('giscedata.perfils.clmag')
        for lot in self.browse(cursor, uid, ids, context):
            clmag_id = clmag_obj.create(cursor, uid, {'lot_id': lot.id})
            clmag_str = ''.join(lot.gen_clmag())
            clmag = clmag_obj.browse(cursor, uid, clmag_id)
            clmag.parse(clmag_str)
            if lot.clmag5_ids:
                lot.make_aggr()
        return True

    def make_clmag5(self, cursor, uid, ids, context=None):
        """
        Build CLMAG5 file.

        :param cursor: database cursor
        :param uid: user identification
        :param ids: batch identifications
        :param context: dict with context
        :return: CLMAG5 identifications
        """

        giscedata_perfils_clmag5 = self.pool.get('giscedata.perfils.clmag5')

        clmag5_ids = []
        for batch in self.browse(cursor, uid, ids, context=context):
            clmag5_id = giscedata_perfils_clmag5.create(
                cursor,
                uid,
                {'batch': batch.id}
            )
            clmag5_ids.append(clmag5_id)
            if batch.clmag_ids:
                batch.make_aggr()

        return clmag5_ids

    def make_clinme(self, cursor, uid, ids, context=None):
        """
        Build CLINME file.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: batch identifiers
        :param context: dict with context
        :return: CLINME identifiers
        """

        giscedata_perfils_clinme = self.pool.get('giscedata.perfils.clinme')

        clinme_ids = []
        for batch in self.browse(cursor, uid, ids, context=context):
            clinme_id = giscedata_perfils_clinme.create(
                cursor,
                uid,
                {'lot_id': batch.id}
            )
            clinme_ids.append(clinme_id)
        return clinme_ids

    def make_agcl(self, cursor, uid, ids, context=None):
        """Genera el AGCL.
        """
        agcl_obj = self.pool.get('giscedata.perfils.agcl')
        for lot_id in ids:
            agcl_obj.create(cursor, uid, {'lot_id': lot_id})
        return True

    def make_aggr(self, cursor, uid, ids, context=None):
        """Genera els nivells d'agregació segons l'últim CLMAG del lot.
        """
        aggr_obj = self.pool.get('giscedata.perfils.agregacions')
        for lot in self.browse(cursor, uid, ids, context):
            aggr_obj.generate(cursor, uid, lot.id)
            if not lot.agcl_ids:
                lot.make_agcl()
        return True

    def _cron_crear_lots_mensuals(self, cursor, uid, context=None):
        """Funció cron per crear els lots mensuals.
        """
        year = calendar.datetime.datetime.now().year
        self.crear_lots_mensuals(cursor, uid, year, context)

    def crear_lots_mensuals(self, cursor, uid, year, context=None):
        """Creem els lots mensuals.

        Si no li passem l'any ho fa de l'actual.
        """
        for month in range(1, 13):
            vals = {}
            vals['name'] = '%02i/%04i' % (month, year)
            vals['data_inici'] = '%04i-%02i-01' % (year, month)
            vals['data_final'] = '%04i-%02i-%02i' % (year, month,
                                                     calendar.monthrange(year, month)[1])
            self.create(cursor, uid, vals, context)
        return True


    def profile(self, cursor, uid, lot_id, context=None):
        """
        Profile the batch

        :param cursor: database cursor
        :param uid: user identifier
        :param lot_id: batch identifier
        :param context: dictionary with the context
        :return: True
        """
        if context is None:
            context = {}
        logger = netsvc.Logger()
        polissa_obj = self.pool.get('giscedata.polissa')
        if isinstance(lot_id, tuple) or isinstance(lot_id, list):
            lot_id = lot_id[0]
        lot = self.browse(cursor, uid, lot_id, context)
        if lot.state != 'obert':
            cursor.close()
            raise osv.except_osv('Error', 'No es pot perfilar un lot que no '
                                 'estigui obert')
        # Agrupem per CUPS. Cups que han canviat de pòlisses s'han de perfilar
        # per ordre i el CUPS hauria de ser perfilat pel mateix procés
        cursor.execute("SELECT cl.id as id,cups.name as cups from "
                       "giscedata_perfils_contracte_lot cl "
                       "left join giscedata_polissa p on cl.polissa_id = p.id "
                       "left join giscedata_cups_ps cups on p.cups = cups.id "
                       "where cl.lot_id = %s and cl.state = 'obert' "
                       "order by p.data_alta asc", (lot_id, ))
        cups = {}
        for clot in cursor.dictfetchall():
            cups.setdefault(clot['cups'], [])
            cups[clot['cups']] += [clot['id']]
        ids_no_perfilats = cups.values()
        cl_obj = self.pool.get('giscedata.perfils.contracte_lot')
        for cl_id in ids_no_perfilats:
            clot = cl_obj.browse(cursor, uid, cl_id[0])
            lot_id = clot.lot_id.id
            polissa = clot.polissa_id
            logger.notifyChannel(
                "objects",
                netsvc.LOG_DEBUG,
                u"Profiling policy %s of batch %s" % (
                    polissa.name,
                    clot.lot_id.name
                )
            )
            polissa.perfilar_async(lot_id)

        return True

    @MultiprocessBackground.background()
    def perfils_button(self, cursor, uid, ids, context=None):
        """Perfilació en mode background.
        """
        with NoDependency():
            for lot in self.browse(cursor, uid, ids, context):
                lot.profile()

        aw = AutoWorker(queue='profiling')
        aw.work()

        return True

    def obrir(self, cursor, uid, ids, context=None):
        """Funció per obrir el lot.

        Només es pot tenir un lot obert alhora, per tant ho hem de comprovar.
        """
        ob_ids = self.search(cursor, uid, [('state', '=', 'obert')])
        oberts = []
        for lot_ob in self.read(cursor, uid, ob_ids, ['name']):
            oberts.append(lot_ob['name'])
        if oberts:
            raise osv.except_osv(u'Error', u'Els següents lots estàn oberts '
                                 u'i només se\'n pot tenir un.\n'
                                 u'  %s' % '\n  '.join(oberts))
        # Obrim els contractes lots que hi hagin
        cl_obj = self.pool.get('giscedata.perfils.contracte_lot')
        # Només obrim els que no estiguin finalitzats
        cl_ids = cl_obj.search(cursor, uid, [('lot_id.id', 'in', ids),
                                             ('state', '!=', 'finalitzat')])
        if cl_ids:
            cl_obj.write(cursor, uid, cl_ids, {'state': 'obert'})
        # Ens descarreguem els coeficients de REE aprofitant
        ree_obj = self.pool.get('giscedata.perfils.ree')
        for lot in self.browse(cursor, uid, ids):
            # Ens assegurem que tenim els coefients de REE
            ree_obj.get_cofs(cursor, uid, lot.data_inici, lot.data_final)
        return self.write(cursor, uid, ids, {'state': 'obert'}, context)

    def tancar(self, cursor, uid, ids, context=None):
        """Funció per tancar el lot.
        """
        return self.write(cursor, uid, ids, {'state': 'tancat'}, context)

    def anterior(self, cursor, uid, ids, context=None):
        """Retorna l'id del lot anterior a l'actual o False.
        """
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        lot = self.browse(cursor, uid, ids)
        a_ids = self.search(cursor, uid, [('data_final', '<', lot.data_inici)],
                            order="data_final desc", limit=1)
        if a_ids:
            return a_ids[0]
        return False

    def actualitzar(self, cursor, uid, ids, context=None):
        """Update progres"""
        def truncate(number, decimals):
            return int(number * 10**decimals) / 10.0**decimals

        cl_obj = self.pool.get('giscedata.perfils.contracte_lot')
        for lot in self.browse(cursor, uid, ids):
            search_params = [('lot_id.id', '=', lot.id)]
            total = cl_obj.search_count(cursor, uid, search_params) * 1.0
            search_params += [('state', '=', 'finalitzat')]
            perfilats = cl_obj.search_count(cursor, uid, search_params)
            try:
                progres = (perfilats / total) * 100
            except ZeroDivisionError:
                progres = 0.0
            lot.write({'progres': truncate(progres, decimals=2)})

        return True

    _states_selection = [
        ('esborrany', 'Esborrany'),
        ('obert', 'Obert'),
        ('tancat', 'Tancat'),
    ]

    def _ff_n_polisses(self, cursor, uid, ids, field_name, args, context=None):
        """Camp funció que retorna el número de pòlisses assignades al lot.
        """
        res = {}
        cl_obj = self.pool.get('giscedata.perfils.contracte_lot')
        for lot_id in ids:
            res[lot_id] = cl_obj.search_count(cursor, uid,
                                              [('lot_id.id', '=', lot_id)])
        return res

    _columns = {
        'name': fields.char('Lot', size=16, required=True, readonly=True),
        'data_inici': fields.date('Data inici', required=True, readonly=True),
        'data_final': fields.date('Data final', required=True, readonly=True),
        'contracte_lot_ids': fields.one2many(
            'giscedata.perfils.contracte_lot',
            'lot_id', 'A perfilar', limit=80),
        'clmag_ids': fields.one2many('giscedata.perfils.clmag', 'lot_id',
                                     'CLMAGs'),
        'clmag5_ids': fields.one2many('giscedata.perfils.clmag5', 'batch',
                                     'CLMAG5s'),
        'clinme_ids': fields.one2many('giscedata.perfils.clinme', 'lot_id',
                                      'CLINMEs'),
        'agcl_ids': fields.one2many('giscedata.perfils.agcl', 'lot_id',
                                    'AGCLs'),
        'aggr_ids': fields.one2many('giscedata.perfils.agregacions', 'lot_id',
                                    'Nivells d\'agregació'),
        'aggr_val_ids': fields.one2many(
            'giscedata.perfils.agregacions.validacions', 'lot_id',
            'Comprovació de consums'
        ),
        'ultima_comprovacio_consums': fields.char(
            'Última comprovació consums', readonly=True, size=19
        ),
        'aggr_ree_ids': fields.one2many('giscedata.perfils.agregacions.ree', 'lot_id',
                                    'Nivells d\'agregació REE'),
        'n_polisses': fields.function(_ff_n_polisses, method=True,
                                      string="Número pòlisses", store=False,
                                      type='integer'),
        # TODO: fields.function per poguer fer servir els triggers
        'progres': fields.float('Progrés de perfilació', readonly=True),
        'state': fields.selection(_states_selection, 'Estat', size=64,
                                  readonly=True),
        'eta': fields.char('Estimació per finalitzar', size=16),
        'active': fields.boolean('Actiu'),
    }

    _defaults = {
        'state': lambda *a: 'esborrany',
        'active': lambda *a: 1,
        'eta': lambda *a: False,
        'ultima_comprovacio_consums': lambda *a: 'No comprovat',
    }

    _sql_constraints = [
        ('name_uniq', 'unique (name)',
         'Aquest lot de facturació ja existex.'),
    ]

    _order = 'data_final desc'


GiscedataPerfilsLot()


class GiscedataPerfilsContracteLot(osv.osv):
    """Cadascuna de les pòlisses a perfilar dins d'un lot.
    """
    _name = 'giscedata.perfils.contracte_lot'

    _states_selection = [
        ('esborrany', 'Esborrany'),
        ('obert', 'Obert'),
        ('finalitzat', 'Finalitzat')
    ]

    _origen_real_selection = [
        ('sense_determinar', 'Sense Determinar'),
        ('corba', 'Corba'),
        ('perfil', 'Perfil')
    ]

    def _origen_teoric(self, cursor, uid, ids, field_name, arg, context=None):
        res = {}
        for contr in self.browse(cursor, uid, ids, context):
            if contr.polissa_id.potencia > 50:
                res[contr.id] = 'corba'
            else:
                res[contr.id] = 'perfil'
        return res

    def _origens_diferents(self, cursor, uid, ids, field_name, arg, context=None):
        """"Retorna True o False per un contracte_lot"""
        res = {}
        contr = self.browse(cursor, uid, ids, context)
        for c in contr:
            if c.origen_teoric == c.origen_real:
                res[c.id] = 0
            else:
                res[c.id] = 1
        return res

    def _origens_diferents_search(self, cursor, uid, obj, field_name, arg, context=None):
        """Busca les polisses que tenen origen_teoric i origen_real diferent"""
        cont_obj = self.pool.get('giscedata.perfils.contracte_lot')

        if not len(arg):
            return []

        contractes = []
        if arg[0][2] == 0:
            cursor.execute("""select id, origen_real, origen_teoric
            from giscedata_perfils_contracte_lot
            where origen_real = origen_teoric""")
            for rec in cursor.fetchall():
                #contractes.append(cont_obj.read(cursor, uid, rec[0]))
                contractes.append(rec[0])
        else:
            cursor.execute("""select id, origen_real, origen_teoric
            from giscedata_perfils_contracte_lot
            where origen_real != origen_teoric""")
            for rec in cursor.fetchall():
                #contractes.append(cont_obj.read(cursor, uid, rec[0]))
                contractes.append(rec[0])
        return [('id', 'in', contractes)]

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True),
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot',
                                  required=True),
        'state': fields.selection(_states_selection, 'Estat', size=32,
                                  required=True, readonly=True),
        'origen_real': fields.selection(_origen_real_selection, string='Origen real', size=32,
                                        required=True, readonly=True, store=True),
        'origen_teoric': fields.function(_origen_teoric, string='Origen teòric', size=32,
                                         required=True, readonly=True, method=True, type='selection',
                                         selection=_origen_real_selection, store=True),
        'origens_diferents': fields.function(_origens_diferents, fnct_search=_origens_diferents_search,
                                             string='Origens Diferents', size=32, required=True, readonly=True,
                                             method=True, type='boolean'),
        'status': fields.text('Status', readonly=True),
        'skip_validation': fields.boolean('Saltar validació'),
        'polissa_potencia': fields.related('polissa_id', 'potencia', type='float',
                                           string="Potència de la polissa", readonly=True, store=True),
        'polissa_tarifa': fields.related('polissa_id', 'tarifa', type='many2one',
                                         string="Tarifa de la polissa", store=True,
                                         readonly=True,
                                         relation='giscedata.polissa.tarifa')
    }

    _defaults = {
        'origen_teoric': lambda *a: 'perfil',
        'state': lambda *a: 'esborrany',
        'skip_validation': lambda *a: 0,
        'origen_real': lambda *a: 'sense_determinar'
    }


GiscedataPerfilsContracteLot()


class GiscedataPerfilsCLMAG(osv.osv):
    """Fitxer CLMAG.
    """
    _name = 'giscedata.perfils.clmag'


    def parse(self, cursor, uid, ids, clmag_str, context=None):
        """Parseja l'string CLMAG i la carrega en la base de dades.
        """
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        clmag_line_obj = self.pool.get('giscedata.perfils.clmag.line')
        fields_pos = {'day': 0, 'month': 1, 'year': 2, 'distribuidora': 3,
                      'comercialitzadora': 4, 'provincia': 5, 'agree_tensio': 6,
                      'agree_tarifa': 7, 'agree_dh': 8, 'agree_tipo': 9,
                      'magnitud': 10}
        rep_fields = ['ei', 'nt', 'er', 'nse']
        for line in map(str.strip, clmag_str.split('\n')):
            if not line:
                continue
            if line.endswith(';'):
                line = line[:-1]
            vals = {'clmag_id': ids}
            values = line.split(';')
            # Camps normals de l'agregació
            for key, pos in fields_pos.items():
                vals[key] = values[pos]
                # Ara les repeticions, a partir de max(fiels_pos.values())
            idx = max(fields_pos.values())
            for h in range(1, 26):
                for key in rep_fields:
                    idx += 1
                    key += '_%s' % h
                    vals[key] = values[idx]
            clmag_line_obj.create(cursor, uid, vals)
        return True

    def gen_file(self, cursor, uid, ids, context=None):
        """Retorna el fitxer.
        """
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        line_obj = self.pool.get('giscedata.perfils.clmag.line')
        line_ids = line_obj.search(cursor, uid, [('clmag_id.id', '=', ids)])
        res_file = []
        for line in line_obj.browse(cursor, uid, line_ids):
            line_file = [line.day,
                         line.month,
                         line.year,
                         line.distribuidora,
                         line.comercialitzadora,
                         line.provincia,
                         line.agree_tensio,
                         line.agree_tarifa,
                         line.agree_dh,
                         line.agree_tipo,
                         line.magnitud]
            for h in range(1, 26):
                for key in ['ei', 'nt', 'er', 'nse']:
                    key += '_%s' % h
                    line_file += [getattr(line, key)]
            res_file.append('%s;\n' % ';'.join(map(str, line_file)))
        return res_file

    def create(self, cursor, uid, vals, context=None):
        if 'lot_id' not in vals:
            raise osv.except_osv('Error', 'CLMAG ha de tenir lot', 'error')
        data_creacio = vals.get('data_creacio', time.strftime('%Y%m%d'))
        vals['data_creacio'] = data_creacio
        if not 'periode' in vals:
            lot = self.pool.get('giscedata.perfils.lot').browse(cursor, uid,
                                                                vals['lot_id'])
            vals['periode'] = lot.data_final.replace('-', '')[:6]
        # Busquem si ja hi ha CLMAGs amb aquesta data de creació
        clids = self.search(cursor, uid, [('lot_id.id', '=', vals['lot_id']),
                                          ('data_creacio', '=', data_creacio)],
                            order="versio desc", limit=1)
        versio = 0
        if clids:
            versio = int(self.browse(cursor, uid, clids[0]).versio)
        # Agafem l'última versió i l'incrementem
        versio += 1
        vals['versio'] = str(versio)
        vals['distribuidora'] = self.default_distribuidora(cursor, uid, context)
        vals['name'] = 'CLMAG_%s_%s_%s.%s' % (vals['distribuidora'],
                                              vals['periode'],
                                              vals['data_creacio'],
                                              vals['versio'])
        res_id = super(GiscedataPerfilsCLMAG, self).create(cursor, uid, vals)
        return res_id

    def download(self, cursor, uid, ids, context=None):
        """Retornem el wizard.
        """
        return {
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'giscedata.perfils.clmag.download',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    _columns = {
        'name': fields.char('Nom', size=256, required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'periode': fields.char('Període', size=6, required=True),
        'data_creacio': fields.char('Data creació', size=8, required=True),
        'versio': fields.char('Versio', size=2, required=True),
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot', required=True),
        'linies': fields.one2many('giscedata.perfils.clmag.line', 'clmag_id',
                                  'Línies CLMAG'),
        'bz2': fields.boolean('bz2'),
    }
    _order = "data_creacio desc, versio desc"


    def default_distribuidora(self, cursor, uid, context=None):
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        return user.company_id.partner_id.ref

    _defaults = {
        'distribuidora': default_distribuidora,
    }


GiscedataPerfilsCLMAG()


class GiscedataPerfilsCLMAGLine(osv.osv):
    """Línia d'un fitxer CLMAG.
    """
    _name = 'giscedata.perfils.clmag.line'

    _columns = {
        'clmag_id': fields.many2one('giscedata.perfils.clmag', 'CLMAG',
                                    required=True, ondelete='cascade'),
        'day': fields.char('Dia', size=2, required=True),
        'month': fields.char('Mes', size=2, required=True),
        'year': fields.char('Any', size=4, required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'magnitud': fields.char('Magnitud', size=2, required=True),
        'ei_1': fields.integer('Energia incremental H1', required=True),
        'nt_1': fields.integer('Número punts frontera H1', required=True),
        'er_1': fields.integer('Energia registrador H1', required=True),
        'nse_1': fields.integer('Número sense estimació H1', required=True),
        'ei_2': fields.integer('Energia incremental H2', required=True),
        'nt_2': fields.integer('Número punts frontera H2', required=True),
        'er_2': fields.integer('Energia registrador H2', required=True),
        'nse_2': fields.integer('Número sense estimació H2', required=True),
        'ei_3': fields.integer('Energia incremental H3', required=True),
        'nt_3': fields.integer('Número punts frontera H3', required=True),
        'er_3': fields.integer('Energia registrador H3', required=True),
        'nse_3': fields.integer('Número sense estimació H3', required=True),
        'ei_4': fields.integer('Energia incremental H4', required=True),
        'nt_4': fields.integer('Número punts frontera H4', required=True),
        'er_4': fields.integer('Energia registrador H4', required=True),
        'nse_4': fields.integer('Número sense estimació H4', required=True),
        'ei_5': fields.integer('Energia incremental H5', required=True),
        'nt_5': fields.integer('Número punts frontera H5', required=True),
        'er_5': fields.integer('Energia registrador H5', required=True),
        'nse_5': fields.integer('Número sense estimació H5', required=True),
        'ei_6': fields.integer('Energia incremental H6', required=True),
        'nt_6': fields.integer('Número punts frontera H6', required=True),
        'er_6': fields.integer('Energia registrador H6', required=True),
        'nse_6': fields.integer('Número sense estimació H6', required=True),
        'ei_7': fields.integer('Energia incremental H7', required=True),
        'nt_7': fields.integer('Número punts frontera H7', required=True),
        'er_7': fields.integer('Energia registrador H7', required=True),
        'nse_7': fields.integer('Número sense estimació H7', required=True),
        'ei_8': fields.integer('Energia incremental H8', required=True),
        'nt_8': fields.integer('Número punts frontera H8', required=True),
        'er_8': fields.integer('Energia registrador H8', required=True),
        'nse_8': fields.integer('Número sense estimació H8', required=True),
        'ei_9': fields.integer('Energia incremental H9', required=True),
        'nt_9': fields.integer('Número punts frontera H9', required=True),
        'er_9': fields.integer('Energia registrador H9', required=True),
        'nse_9': fields.integer('Número sense estimació H9', required=True),
        'ei_10': fields.integer('Energia incremental H10', required=True),
        'nt_10': fields.integer('Número punts frontera H10', required=True),
        'er_10': fields.integer('Energia registrador H10', required=True),
        'nse_10': fields.integer('Número sense estimació H10', required=True),
        'ei_11': fields.integer('Energia incremental H11', required=True),
        'nt_11': fields.integer('Número punts frontera H11', required=True),
        'er_11': fields.integer('Energia registrador H11', required=True),
        'nse_11': fields.integer('Número sense estimació H11', required=True),
        'ei_12': fields.integer('Energia incremental H12', required=True),
        'nt_12': fields.integer('Número punts frontera H12', required=True),
        'er_12': fields.integer('Energia registrador H12', required=True),
        'nse_12': fields.integer('Número sense estimació H12', required=True),
        'ei_13': fields.integer('Energia incremental H13', required=True),
        'nt_13': fields.integer('Número punts frontera H13', required=True),
        'er_13': fields.integer('Energia registrador H13', required=True),
        'nse_13': fields.integer('Número sense estimació H13', required=True),
        'ei_14': fields.integer('Energia incremental H14', required=True),
        'nt_14': fields.integer('Número punts frontera H14', required=True),
        'er_14': fields.integer('Energia registrador H14', required=True),
        'nse_14': fields.integer('Número sense estimació H14', required=True),
        'ei_15': fields.integer('Energia incremental H15', required=True),
        'nt_15': fields.integer('Número punts frontera H15', required=True),
        'er_15': fields.integer('Energia registrador H15', required=True),
        'nse_15': fields.integer('Número sense estimació H15', required=True),
        'ei_16': fields.integer('Energia incremental H16', required=True),
        'nt_16': fields.integer('Número punts frontera H16', required=True),
        'er_16': fields.integer('Energia registrador H16', required=True),
        'nse_16': fields.integer('Número sense estimació H16', required=True),
        'ei_17': fields.integer('Energia incremental H17', required=True),
        'nt_17': fields.integer('Número punts frontera H17', required=True),
        'er_17': fields.integer('Energia registrador H17', required=True),
        'nse_17': fields.integer('Número sense estimació H17', required=True),
        'ei_18': fields.integer('Energia incremental H18', required=True),
        'nt_18': fields.integer('Número punts frontera H18', required=True),
        'er_18': fields.integer('Energia registrador H18', required=True),
        'nse_18': fields.integer('Número sense estimació H18', required=True),
        'ei_19': fields.integer('Energia incremental H19', required=True),
        'nt_19': fields.integer('Número punts frontera H19', required=True),
        'er_19': fields.integer('Energia registrador H19', required=True),
        'nse_19': fields.integer('Número sense estimació H19', required=True),
        'ei_20': fields.integer('Energia incremental H20', required=True),
        'nt_20': fields.integer('Número punts frontera H20', required=True),
        'er_20': fields.integer('Energia registrador H20', required=True),
        'nse_20': fields.integer('Número sense estimació H20', required=True),
        'ei_21': fields.integer('Energia incremental H21', required=True),
        'nt_21': fields.integer('Número punts frontera H21', required=True),
        'er_21': fields.integer('Energia registrador H21', required=True),
        'nse_21': fields.integer('Número sense estimació H21', required=True),
        'ei_22': fields.integer('Energia incremental H22', required=True),
        'nt_22': fields.integer('Número punts frontera H22', required=True),
        'er_22': fields.integer('Energia registrador H22', required=True),
        'nse_22': fields.integer('Número sense estimació H22', required=True),
        'ei_23': fields.integer('Energia incremental H23', required=True),
        'nt_23': fields.integer('Número punts frontera H23', required=True),
        'er_23': fields.integer('Energia registrador H23', required=True),
        'nse_23': fields.integer('Número sense estimació H23', required=True),
        'ei_24': fields.integer('Energia incremental H24', required=True),
        'nt_24': fields.integer('Número punts frontera H24', required=True),
        'er_24': fields.integer('Energia registrador H24', required=True),
        'nse_24': fields.integer('Número sense estimació H24', required=True),
        'ei_25': fields.integer('Energia incremental H25', required=True),
        'nt_25': fields.integer('Número punts frontera H25', required=True),
        'er_25': fields.integer('Energia registrador H25', required=True),
        'nse_25': fields.integer('Número sense estimació H25', required=True),
    }
    _order = "year asc, month asc, day asc"


GiscedataPerfilsCLMAGLine()


class GiscedataPerfilsCLMAG5(osv.osv):
    """
    CLMAG5 file.
    """
    _name = 'giscedata.perfils.clmag5'

    def add_measure_to_report(
            self,
            report,
            policy_amendment,
            timestamp,
            season,
            active_input,
            remote_managed=False,
            real_measure=False
    ):
        """
        Add values to CLMAG5 report.

        :param report: dict representing the CLMAG5 report
        :param policy_amendment: giscedata.polissa.modcontractual object
        :param timestamp: timestamp
        :param season: season (0 winter / 1 summer)
        :param active_input: active input measure (kWh)
        :param remote_managed: True if is a remote managed meter, False if not.
        :param real_measure: True if is a real measure, False if is estimated.
        """
        marketer = policy_amendment.comercialitzadora.ref
        voltage = policy_amendment.agree_tensio
        tariff = policy_amendment.agree_tarifa
        hourly_discrimination = policy_amendment.agree_dh
        border_type = policy_amendment.agree_tipus
        province =  policy_amendment.cups.id_provincia.ree_code

        key = (
            marketer,
            voltage,
            tariff,
            hourly_discrimination,
            border_type,
            province,
            timestamp,
            season
        )
        if key not in report:
            report[key] = {}
            report[key]['active_input'] = 0
            report[key]['border_points'] = 0
            report[key]['real_active'] = 0
            report[key]['border_points_real'] = 0
            report[key]['estimation_active'] = 0
            report[key]['border_points_estimation'] = 0
        report[key]['active_input'] += active_input
        report[key]['border_points'] += 1

        if remote_managed:
            if real_measure:
                report[key]['real_active'] += active_input
                report[key]['border_points_real'] += 1
            else:
                report[key]['estimation_active'] += active_input
                report[key]['border_points_estimation'] += 1

    def add_remote_managed_meter_to_report(
            self,
            cursor,
            uid,
            meter_ids,
            report,
            policy_amendment,
            start_date,
            end_date,
            context=None
    ):
        """
        Add remote managed meter measures to a CLMAG5 report.

        :param cursor: database cursor
        :param uid: user identifier
        :param meter_ids: meter identifiers
        :param report: dict representing the CLMAG5 report
        :param policy_amendment: the policy amendment to get aggregations
        :param start_date: string with first day, 'YYYY-MM-DD' formated
        :param end_date: string with last day, 'YYYY-MM-DD' formated
        :param context: dict with the context
        :return: None
        """

        giscedata_lectures_comptador = self.pool.get(
            'giscedata.lectures.comptador'
        )
        tg_profile = self.pool.get('tg.profile')
        if not tg_profile:
            return

        if context is None:
            context_no_active_test = {}
        else:
            context_no_active_test = context.copy()
        context_no_active_test['active_test'] = False

        for meter in giscedata_lectures_comptador.browse(
            cursor,
            uid,
            meter_ids,
            context=context_no_active_test
        ):
            if meter.tg:
                dragger = Dragger()
                meter_start = max(start_date, meter.data_alta)
                meter_end = end_date
                if meter.data_baixa:
                    meter_end = min(end_date, meter.data_baixa)
                start_timestamp, end_timestamp = \
                    period_to_tg_profile_timestamps(meter_start, meter_end)
                meter_name = meter.build_name_tg()
                tg_profile_ids = tg_profile.search(
                    cursor,
                    uid,
                    [
                        ('name', '=', meter_name),
                        ('timestamp', '>=', start_timestamp),
                        ('timestamp', '<=', end_timestamp),
                        ('cch_fact', '=', True)
                    ],
                    context=context
                )

                for remote_managed_profile in tg_profile.read(
                    cursor,
                    uid,
                    tg_profile_ids,
                    [
                        'kind_fact',
                        'ai_fact',
                        'magn',
                        'timestamp',
                        'season'
                    ],
                    context=context
                ):
                    if remote_managed_profile['kind_fact'] == '1':
                        real_measure = True
                    else:
                        real_measure = False
                    measure = remote_managed_profile['ai_fact']

                    active_input = dragger.drag(
                        measure * remote_managed_profile['magn'] / 1000
                    )

                    if remote_managed_profile['season'] == 'S':
                        season = '1'
                    else:
                        season = '0'

                    timestamp = (
                        remote_managed_profile['timestamp'].replace('-', '/')
                    )

                    self.add_measure_to_report(
                        report,
                        policy_amendment,
                        timestamp,
                        season,
                        active_input,
                        remote_managed=True,
                        real_measure=real_measure
                    )

            else:
                msg_ = _(u"El comptador {0} de la pòlissa {1} no és telegestionat.").format(
                    meter.name, policy_amendment.polissa_id.name
                )
                # Create crm case
                title = "CLMAG5A {0} / {1}".format(start_date, end_date)
                create_perfils_crm_case(title, msg_)

                raise osv.except_osv(
                    _("Error"),
                    msg_
                )

    def build(self, cursor, uid, clmag5_ids, context=None):
        """
        Build CLMAG5.

        :param cursor: database cursor
        :param uid: user identification
        :param clmag5_ids: CLMAG5 identifications
        :param context: dict with the context
        :return: CLMAG5 lines identification
        """

        giscedata_polissa_modcontractual = self.pool.get(
            'giscedata.polissa.modcontractual'
        )
        giscedata_perfils_perfil = self.pool.get('giscedata.perfils.perfil')
        giscedata_perfils_clmag5_line = \
            self.pool.get('giscedata.perfils.clmag5.line')

        if context is None:
            context_no_active_test = {}
        else:
            context_no_active_test = context.copy()
        context_no_active_test['active_test'] = False

        logger_perfils = logging.getLogger('openerp.{}'.format(__name__))

        report = {}
        clmag5_lines = []

        if not (isinstance(clmag5_ids, list) or isinstance(clmag5_ids, tuple)):
            clmag5_ids = [clmag5_ids]

        for clmag5 in self.browse(cursor, uid, clmag5_ids, context=context):
            policy_amendment_ids = giscedata_polissa_modcontractual.search(
                cursor,
                uid,
                [
                    ('potencia', '<=', 15),
                    ('data_inici', '<=', clmag5.batch.data_final),
                    ('data_final', '>=', clmag5.batch.data_inici),
                ],
                context=context_no_active_test
            )

            for n_modcons, policy_amendment in enumerate(giscedata_polissa_modcontractual.browse(
                cursor,
                uid,
                policy_amendment_ids,
                context=context
            ), start=1):
                if policy_amendment.data_inici > clmag5.batch.data_inici:
                    start_date = policy_amendment.data_inici
                else:
                    start_date = clmag5.batch.data_inici

                if policy_amendment.data_final < clmag5.batch.data_final:
                    end_date = policy_amendment.data_final
                else:
                    end_date = clmag5.batch.data_final

                # No contracts in canceled status
                if policy_amendment.polissa_id.state == 'cancelada':
                    continue

                if policy_amendment.tg == '1':
                    # policy_amendment.comptador is not always the meter of
                    # the policy amendment period
                    meter_ids = policy_amendment.polissa_id.comptadors_actius(
                        start_date,
                        end_date
                    )
                    if not meter_ids:
                        logger_perfils.error("No s'ha trobat cap comptador per la pòlissa"
                                              " {0} durant el periode {1} {2}".format(policy_amendment.polissa_id.name,
                                                                                      start_date,
                                                                                      end_date))
                        msg_ = _(u"No s'ha trobat cap comptador per la pòlissa {0} durant el periode {1} {2}").format(
                            policy_amendment.polissa_id.name, start_date, end_date)
                        # Create crm case
                        title = "CLMAG5A {0} / {1}".format(start_date, end_date)
                        create_perfils_crm_case(title, msg_)

                        raise osv.except_osv(
                            _("Error"),
                            msg_
                        )
                    else:
                        self.add_remote_managed_meter_to_report(
                            cursor,
                            uid,
                            meter_ids,
                            report,
                            policy_amendment,
                            start_date,
                            end_date,
                            context=context
                        )
                else:
                    profile_ids = giscedata_perfils_perfil.search(
                        cursor,
                        uid,
                        [
                            (
                                'cups',
                                '=',
                                policy_amendment.polissa_id.cups.name
                            ),
                            ('name', '>=', start_date.replace('-','')),
                            ('name', '<=', end_date.replace('-','') + '24')
                        ],
                        context=context
                    )
                    for profile in giscedata_perfils_perfil.browse(
                        cursor,
                        uid,
                        profile_ids,
                        context=context
                    ):
                        self.add_measure_to_report(
                            report,
                            policy_amendment,
                            timestamp_from_profile(profile),
                            profile.estacio,
                            profile.aprox
                        )
                logger_perfils.info(
                'End modcon {}, #{}'.format(policy_amendment.polissa_id.cups.name, n_modcons))

            distributor = self.default_distributor(
                        cursor,
                        uid,
                        context=context
                    )
            for key in report:
                line = {
                    'clmag5': clmag5.id,
                    'distributor': distributor,
                    'marketer': key[0],
                    'voltage': key[1],
                    'tariff': key[2],
                    'hourly_discrimination': key[3],
                    'border_type': key[4],
                    'province': key[5],
                    'timestamp': key[6],
                    'season': key[7],
                    'active_input': report[key]['active_input'],
                    'border_points': report[key]['border_points'],
                    'real_active': report[key]['real_active'],
                    'border_points_real': report[key]['border_points_real'],
                    'estimation_active': report[key]['estimation_active'],
                    'border_points_estimation':
                        report[key]['border_points_estimation'],
                }
                clmag5_lines.append(
                    giscedata_perfils_clmag5_line.create(
                        cursor,
                        uid,
                        line,
                        context=context
                    )
                )
        return clmag5_lines

    def filename(self, cursor, uid, clmag5_id, context=None):
        """
        CLMAG5A filename.

        :param cursor: database cursor
        :param uid: user identifier
        :param clmag5_id: CLMAG5 identifier
        :param context: dict with context
        :return: string with the CLMAG5A filename
        """

        if isinstance(clmag5_id, list) or isinstance(clmag5_id, tuple):
            clmag5_id = clmag5_id[0]

        clmag5 = self.read(
            cursor,
            uid,
            clmag5_id,
            [
                'distributor',
                'period',
                'creation_date',
                'version'
            ],
            context=context
        )

        clmag5_filename = 'CLMAG5A_{0}_{1}_{2}.{3}'.format(
            clmag5['distributor'],
            clmag5['period'],
            clmag5['creation_date'],
            clmag5['version']
        )

        return clmag5_filename

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def create(self, cursor, uid, values, context=None):
        """
        Create a new CLMAG5.

        :param cursor: database cursor
        :param uid: user identification
        :param values: dict with the CLMAG5 values, batch required.
        :param context: dict with the context
        :return: CLMAG5 identification
        """
        if 'batch' not in values:
            raise osv.except_osv('Error', 'CLMAG5 ha de tenir lot', 'error')
        creation_date = values.get('creation_date', time.strftime('%Y%m%d'))
        values['creation_date'] = creation_date
        if not 'period' in values:
            batch = self.pool.get('giscedata.perfils.lot').browse(
                cursor,
                uid,
                values['batch']
            )
            values['period'] = batch.data_final.replace('-', '')[:6]
        # Search if exists CLMAG5 with same creation date
        clmag5_ids = self.search(
            cursor,
            uid,
            [
                ('batch.id', '=', values['batch']),
                ('creation_date', '=', creation_date)
            ],
            order="version desc", limit=1
        )
        version = 0
        if clmag5_ids:
            version = int(self.browse(cursor, uid, clmag5_ids[0]).version)
        # Increment the last version
        version += 1
        values['version'] = str(version)
        values['distributor'] = self.default_distributor(
            cursor,
            uid,
            context=context
        )
        clmag5_id = super(GiscedataPerfilsCLMAG5, self).create(
            cursor,
            uid,
            values,
            context=context
        )
        self.build(cursor, uid, clmag5_id, context=context)
        return clmag5_id

    def download(self, cursor, uid, ids, context=None):
        """
        Returns the wizard.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: identifiers, not used
        :param context: dict with the context, not used
        :return: dict with the wizard
        """
        return {
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'giscedata.perfils.clmag5.download',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def build_file(self, cursor, uid, clmag5_id, context=None):
        """
        Returns the CLMAG5A files.

        :param cursor: database cursor
        :param uid: user identifier
        :param clmag5_id: CLMAG5 identifier
        :param context: dict with the context
        :return: zip file with CLMAG5A files
        """
        if isinstance(clmag5_id, list) or isinstance(clmag5_id, tuple):
            clmag5_id = clmag5_id[0]

        giscedata_perfils_clmag5_line = \
            self.pool.get('giscedata.perfils.clmag5.line')

        line_ids = giscedata_perfils_clmag5_line.search(
            cursor,
            uid,
            [('clmag5.id', '=', clmag5_id)],
            context=context
        )

        file_lines = []
        for line in giscedata_perfils_clmag5_line.browse(
            cursor,
            uid,
            line_ids,
            context=context
        ):
            line_file = []
            for value in [
                'distributor',
                'marketer',
                'voltage',
                'tariff',
                'hourly_discrimination',
                'border_type',
                'province',
                'timestamp',
                'season'
            ]:
                line_file.append(str(line[value]))
            line_file.append('AE')
            for value in [
                'active_input',
                'border_points',
                'real_active',
                'border_points_real',
                'estimation_active',
                'border_points_estimation'
            ]:
                line_file.append(str(line[value]))
            file_lines.append(';'.join(line_file))
        clmag5_file_content = ';\n'.join(file_lines) + ';'

        return clmag5_file_content

    _columns = {
        'distributor': fields.char("Distribuidora", size=4, required=True),
        'period': fields.char("Període", size=6, required=True),
        'creation_date': fields.char("Data creació", size=8, required=True),
        'version': fields.char("Versió", size=2, required=True),
        'batch': fields.many2one(
            'giscedata.perfils.lot',
            "Lot",
            required=True
        ),
        'lines': fields.one2many(
            'giscedata.perfils.clmag5.line',
            'clmag5',
            "Línies CLMAG"
        ),
        'bz2': fields.boolean('bz2'),
    }
    _order = "creation_date desc, version desc"

    def default_distributor(self, cursor, uid, context=None):
        user = self.pool.get('res.users').browse(
            cursor,
            uid,
            uid,
            context=context
        )
        return user.company_id.partner_id.ref

    _defaults = {
        'distributor': default_distributor,
    }


GiscedataPerfilsCLMAG5()


class GiscedataPerfilsCLMAG5Line(osv.osv):
    """
    Line of CLMAG5 file.
    """
    _name = 'giscedata.perfils.clmag5.line'

    _columns = {
        'clmag5': fields.many2one(
            'giscedata.perfils.clmag5',
            'CLMAG5',
            required=True,
            ondelete='cascade'
        ),
        'distributor': fields.char("Distribuidora", size=4, required=True),
        'marketer': fields.char("Comercialitzadora", size=4, required=True),
        'voltage': fields.char("Nivell de tensió", size=2, required=True),
        'tariff': fields.char("Tarifa d'accés", size=2, required=True),
        'hourly_discrimination': fields.char(
            "Discriminació horaria",
            size=2,
            required=True
        ),
        'border_type': fields.char(
            "Tipus de punts agregats",
            size=2,
            required=True
        ),
        'province': fields.char(
            "Provincia/Subsistema",
            size=2,
            required=True
        ),
        'timestamp': fields.char("Timestamp", size=16, required=True),
        'season': fields.char(
            "Estació (0 hivern, 1 estiu)",
            size=1,
            required=True
        ),
        'active_input': fields.integer(
            "Mesura de la magnitud activa entrant (kWh)",
            required=True
        ),
        'border_points': fields.integer(
            "Número de punts frontera de l'agregació pel periode horari",
            required=True
        ),
        'real_active': fields.integer(
            "Mesura de la magnitud activa entrant de clients amb mesura real "
            "amb equip efectivament integrat",
            required=True
        ),
        'border_points_real': fields.integer(
            "Número de punts frontera de l'agregació amb mesura real pel "
            "periode horari amb equip efectivament integrat",
            required=True
        ),
        'estimation_active': fields.integer(
            "Mesura de la magnitud activa entrant de clients amb mesura real "
            "amb equip efectivament integrat",
            required=True
        ),
        'border_points_estimation': fields.integer(
            "Número de punts frontera de l'agregació amb mesura real pel "
            "periode horari amb equip efectivament integrat",
            required=True
        )
    }
    _order = "timestamp asc, season desc"


GiscedataPerfilsCLMAG5Line()


class GiscedataPerfilsCLINME(osv.osv):
    """Fitxer CLINME.
    """
    _name = 'giscedata.perfils.clinme'

    FIELDS_POS = {
        'cups': 0,
        'distribuidora': 1,
        'comercialitzadora': 2,
        'agree_tensio': 3,
        'agree_tarifa': 4,
        'agree_dh': 5,
        'agree_tipo': 6,
        'provincia': 7,
        'mesura_ab': 8,
        'data_inici': 9,
        'data_final': 10,
        'consum_ae': 11,
        'consum_r1': 12,
        'consum_r4': 13,
        'n_estimats': 14,
        'consum_ae_estimada': 15
    }

    def add_measure_to_report(
        self,
        report,
        policy_amendment,
        date,
        measure,
        measure_type,
        estimated=False
    ):
        """
        Add values to CLINME report.

        :param report: dict representing the CLINME report
        :param policy_amendment: giscedata.polissa.modcontractual object
        :param date: date of the measure
        :param measure: value of the measure
        :param measure_type: type of the measure; 'active_input',
            \ 'reactive_quadrant1' or 'reactive_quadrant4'
        :param estimated: True if the measure is estimated, otherwise False
        :return: None
        """
        cups = policy_amendment.polissa_id.cups.name
        marketer = policy_amendment.comercialitzadora.ref
        voltage = policy_amendment.agree_tensio
        tariff = policy_amendment.agree_tarifa
        hourly_discrimination = policy_amendment.agree_dh
        border_type = policy_amendment.agree_tipus
        province =  policy_amendment.cups.id_provincia.ree_code
        high_voltage = policy_amendment.tarifa.tipus[0]

        key = (
            cups,
            marketer,
            voltage,
            tariff,
            hourly_discrimination,
            border_type,
        )
        if key not in report:
            report[key] = {}
            report[key]['province'] = province
            report[key]['high_voltage'] = high_voltage
            report[key]['start_date'] = date
            report[key]['end_date'] = date
            report[key]['active_input'] = 0
            report[key]['reactive_quadrant1'] = 0
            report[key]['reactive_quadrant4'] = 0
            if estimated and measure_type == 'active_input':
                report[key]['estimated_periods'] = 1
                report[key]['estimated_active_input'] = measure
            else:
                report[key]['estimated_periods'] = 0
                report[key]['estimated_active_input'] = 0
        else:
            report[key]['start_date'] = min(report[key]['start_date'], date)
            report[key]['end_date'] = max(report[key]['end_date'], date)
            if estimated and measure_type == 'active_input':
                report[key]['estimated_periods'] += 1
                report[key]['estimated_active_input'] += measure
        report[key][measure_type] += measure

    def add_remote_managed_meter_to_report(
            self,
            cursor,
            uid,
            meter_ids,
            report,
            policy_amendment,
            start_date,
            end_date,
            context=None
    ):
        """
        Add remote managed meter measures to a CLINME report.

        :param cursor: database cursor
        :param uid: user identifier
        :param meter_ids: meter identifiers
        :param report: dict representing the CLINME report
        :param policy_amendment: the policy amendment to get aggregations
        :param start_date: string with first day, 'YYYY-MM-DD' formated
        :param end_date: string with last day, 'YYYY-MM-DD' formated
        :param context: dict with the context
        :return: None
        """

        giscedata_lectures_comptador = self.pool.get(
            'giscedata.lectures.comptador'
        )
        tg_profile = self.pool.get('tg.profile')
        if not tg_profile:
            return

        if context is None:
            context_no_active_test = {}
        else:
            context_no_active_test = context.copy()
        context_no_active_test['active_test'] = False

        for meter in giscedata_lectures_comptador.browse(
            cursor,
            uid,
            meter_ids,
            context=context_no_active_test
        ):
            if meter.tg:
                dragger = Dragger()
                meter_start = max(start_date, meter.data_alta)
                meter_end = end_date
                if meter.data_baixa:
                    meter_end = min(end_date, meter.data_baixa)
                start_timestamp, end_timestamp = \
                    period_to_tg_profile_timestamps(meter_start, meter_end)
                meter_name = meter.build_name_tg()
                tg_profile_ids = tg_profile.search(
                    cursor,
                    uid,
                    [
                        ('name', '=', meter_name),
                        ('timestamp', '>=', start_timestamp),
                        ('timestamp', '<=', end_timestamp),
                        ('cch_fact', '=', True)
                    ],
                    context=context
                )
                for remote_managed_profile in tg_profile.read(
                    cursor,
                    uid,
                    tg_profile_ids,
                    [
                        'ai_fact',
                        'r1_fact',
                        'r4_fact',
                        'magn',
                        'timestamp',
                        'kind_fact',
                    ],
                    context=context
                ):
                    if remote_managed_profile['kind_fact'] == '1':
                        estimated = False
                    else:
                        estimated = True

                    for measure_type_fact in ['ai_fact','r1_fact','r4_fact']:
                        if measure_type_fact in remote_managed_profile:
                            measure_value = remote_managed_profile[
                                measure_type_fact
                            ]
                            operation = (measure_value *
                                         remote_managed_profile['magn'] / 1000)
                            measure_value_round = dragger.drag(
                                operation,
                                key=measure_type_fact[:2]
                            )
                            timestamp = (
                                remote_managed_profile['timestamp'].replace(
                                    '-', '/'
                                )
                            )
                            self.add_measure_to_report(
                                report,
                                policy_amendment,
                                timestamp,
                                measure_value_round,
                                MEASURE_TYPE_FROM_TG_PROFILE_TO_REPORT[
                                    measure_type_fact[:2]
                                ],
                                estimated=estimated
                            )
            else:
                msg_ = _(
                    u"El comptador {0} de la pòlissa {1} no és "
                    u"telegestionat."
                ).format(
                    meter.name,
                    policy_amendment.polissa_id.name
                )
                # Create crm case
                title = "CLINME {0} / {1}".format(start_date, end_date)
                create_perfils_crm_case(title, msg_)

                raise osv.except_osv(
                    _("Error"),
                    msg_
                )

    def build(self, cursor, uid, clinme_ids, context=None):
        """
        Build CLINME.

        :param cursor: database cursor
        :param uid: user identifier
        :param clinme_ids: CLINME identifications
        :param context: dict with the context
        :return: CLINME line identifiers
        """

        giscedata_polissa_modcontractual = self.pool.get(
            'giscedata.polissa.modcontractual'
        )
        giscedata_perfils_perfil = self.pool.get('giscedata.perfils.perfil')
        giscedata_perfils_clinme_line = \
            self.pool.get('giscedata.perfils.clinme.line')

        if context is None:
            context_no_active_test = {}
        else:
            context_no_active_test = context.copy()
        context_no_active_test['active_test'] = False

        report = {}
        clinme_lines = []

        logger_perfils = logging.getLogger('openerp.{}'.format(__name__))

        if not (isinstance(clinme_ids, list) or isinstance(clinme_ids, tuple)):
            clinme_ids = [clinme_ids]

        for clinme in self.browse(cursor, uid, clinme_ids, context=context):
            policy_amendment_ids = giscedata_polissa_modcontractual.search(
                cursor,
                uid,
                [
                    ('data_inici', '<=', clinme.lot_id.data_final),
                    ('data_final', '>=', clinme.lot_id.data_inici),
                ],
                context=context_no_active_test
            )
            for n_modcons, policy_amendment in enumerate(giscedata_polissa_modcontractual.browse(
                cursor,
                uid,
                policy_amendment_ids,
                context=context
            ), start=1):
                if policy_amendment.data_inici > clinme.lot_id.data_inici:
                    start_date = policy_amendment.data_inici
                else:
                    start_date = clinme.lot_id.data_inici

                if policy_amendment.data_final < clinme.lot_id.data_final:
                    end_date = policy_amendment.data_final
                else:
                    end_date = clinme.lot_id.data_final

                # No contracts in canceled status
                if policy_amendment.polissa_id.state == 'cancelada':
                    continue

                if policy_amendment.tg == '1':
                    # policy_amendment.comptador is not always the meter of
                    # the policy amendment period
                    meter_ids = policy_amendment.polissa_id.comptadors_actius(
                        start_date,
                        end_date
                    )
                    if not meter_ids:
                        logger_perfils.error("No s'ha trobat cap comptador per la pòlissa "
                                             "{0} durant el periode {1} {2}".format(policy_amendment.polissa_id.name,
                                                                                    start_date,
                                                                                    end_date)
                                             )
                        msg_ = _(u"No s'ha trobat cap comptador per la pòlissa {0} durant el periode {1} {2}").format(
                            policy_amendment.polissa_id.name, start_date, end_date
                        )
                        # Create crm case
                        title = "CLINME {0} / {1}".format(start_date, end_date)
                        create_perfils_crm_case(title, msg_)

                        raise osv.except_osv(
                            _("Error"),
                            msg_
                        )
                    else:
                        self.add_remote_managed_meter_to_report(
                            cursor,
                            uid,
                            meter_ids,
                            report,
                            policy_amendment,
                            start_date,
                            end_date,
                            context=context
                        )
                else:
                    profile_ids = giscedata_perfils_perfil.search(
                        cursor,
                        uid,
                        [
                            (
                                'cups',
                                '=',
                                policy_amendment.polissa_id.cups.name
                            ),
                            ('name', '>=', start_date.replace('-','')),
                            ('name', '<=', end_date.replace('-','') + '24')
                        ],
                        context=context
                    )
                    for profile in giscedata_perfils_perfil.browse(
                        cursor,
                        uid,
                        profile_ids,
                        context=context
                    ):
                        measure_type = MEASURE_TYPE_FROM_PROFILE_TO_REPORT[
                            profile.magnitud
                        ]
                        self.add_measure_to_report(
                            report,
                            policy_amendment,
                            timestamp_from_profile(profile),
                            profile.aprox,
                            measure_type
                        )
                logger_perfils.info(
                    'End modcon {}, #{}'.format(policy_amendment.polissa_id.cups.name, n_modcons))
            distributor = self.default_distribuidora(
                        cursor,
                        uid,
                        context=context
                    )
            for key in report:
                line = {
                    'clinme_id': clinme.id,
                    'cups': key[0],
                    'distribuidora': distributor,
                    'comercialitzadora': key[1],
                    'agree_tensio': key[2],
                    'agree_tarifa': key[3],
                    'agree_dh': key[4],
                    'agree_tipo': key[5],
                    'provincia': report[key]['province'],
                    'mesura_ab': report[key]['high_voltage'],
                    'data_inici': report[key]['start_date'],
                    'data_final': report[key]['end_date'],
                    'consum_ae': report[key]['active_input'],
                    'consum_r1': report[key]['reactive_quadrant1'],
                    'consum_r4': report[key]['reactive_quadrant4'],
                    'n_estimats': report[key]['estimated_periods'],
                    'consum_ae_estimada': report[key]['estimated_active_input']
                }
                clinme_lines.append(
                    giscedata_perfils_clinme_line.create(
                        cursor,
                        uid,
                        line,
                        context=context
                    )
                )
        return clinme_lines

    def filename(self, cursor, uid, clinme_id, marketer=None, context=None):
        """
        CLINME filename. If marketer is not provided then 'all' is used and \
            '.zip' appended.

        :param cursor: database cursor
        :param uid: user identifier
        :param clinme_id: CLINME identifier
        :param marketer: the marketer code
        :param context: dict with context
        :return: string with the CLINME filename
        """

        if isinstance(clinme_id, list) or isinstance(clinme_id, tuple):
            clinme_id = clinme_id[0]

        clinme = self.read(
            cursor,
            uid,
            clinme_id,
            [
                'distribuidora',
                'periode',
                'data_creacio',
                'versio'
            ],
            context=context
        )

        if marketer is None:
            marketer = 'all'
            extension = '.zip'
        else:
            extension = ''

        clinme_filename = 'CLINME_{0}_{1}_{2}_{3}.{4}{5}'.format(
            clinme['distribuidora'],
            marketer,
            clinme['periode'],
            clinme['data_creacio'],
            clinme['versio'],
            extension
        )

        return clinme_filename

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def create(self, cursor, uid, vals, context=None):
        """
        Create a new CLINME.

        :param cursor: database cursor
        :param uid: user identifier
        :param vals: dict with the CLINME values, batch required.
        :param context: dict with the context
        :return: CLINME identifiers
        """
        if 'lot_id' not in vals:
            raise osv.except_osv('Error', 'CLINME ha de tenir lot')
        data_creacio = vals.get('data_creacio', time.strftime('%Y%m%d'))
        vals['data_creacio'] = data_creacio
        if not 'periode' in vals:
            lot = self.pool.get('giscedata.perfils.lot').browse(cursor, uid,
                                                                vals['lot_id'])
            vals['periode'] = lot.data_final.replace('-', '')[:6]
        # Busquem si ja hi ha CLINMEs amb aqusta data de creació
        search_params = [('lot_id.id', '=', vals['lot_id']),
                         ('data_creacio', '=', data_creacio)]
        clids = self.search(cursor, uid, search_params, order="versio desc",
                            limit=1)
        versio = 0
        if clids:
            versio = int(self.browse(cursor, uid, clids[0]).versio)
        # Agafem l'última versió i l'incrementem
        versio += 1
        vals['versio'] = str(versio)
        vals['distribuidora'] = self.default_distribuidora(cursor, uid, context)
        res_id = super(GiscedataPerfilsCLINME, self).create(cursor, uid, vals)
        self.build(cursor, uid, res_id, context=context)
        return res_id

    def download(self, cursor, uid, ids, context=None):
        """Retornem el wizard.
        """
        return {
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'giscedata.perfils.clinme.download',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def build_file(self, cursor, uid, clinme_id, str_mode=False, context=None):
        """
        Returns the CLINME files.

        :param cursor: database cursor
        :param uid: user identifier
        :param clinme_id: CLINME identifier
        :param context: dict with the context
        :return: zip file with CLINME files
        """
        if isinstance(clinme_id, list) or isinstance(clinme_id, tuple):
            clinme_id = clinme_id[0]

        giscedata_perfils_clinme_line = \
            self.pool.get('giscedata.perfils.clinme.line')

        line_ids = giscedata_perfils_clinme_line.search(
            cursor,
            uid,
            [('clinme_id.id', '=', clinme_id)],
            context=context
        )
        marketer_lines = giscedata_perfils_clinme_line.read(
            cursor,
            uid,
            line_ids,
            ['comercialitzadora'],
            context=context
        )
        res = []
        marketers = []
        for line in marketer_lines:
            if line['comercialitzadora'] not in marketers:
                marketers.append(line['comercialitzadora'])

        clinmes_file = StringIO()
        zip_clinmes_file = ZipFile(clinmes_file, 'w')
        for marketer in marketers:
            marketer_file_lines = []
            line_ids = giscedata_perfils_clinme_line.search(
                cursor,
                uid,
                [
                    ('clinme_id.id', '=', clinme_id),
                    ('comercialitzadora', '=', marketer)
                ],
                context=context
            )
            for line in giscedata_perfils_clinme_line.browse(
                cursor,
                uid,
                line_ids,
                context=context
            ):
                line_file = []
                for value in [
                    'cups',
                    'distribuidora',
                    'comercialitzadora',
                    'agree_tensio',
                    'agree_tarifa',
                    'agree_dh',
                    'agree_tipo',
                    'provincia',
                    'mesura_ab',
                    'data_inici',
                    'data_final',
                    'consum_ae',
                    'consum_r1',
                    'consum_r4',
                    'n_estimats',
                    'consum_ae_estimada'
                ]:
                    if value == 'data_inici' or value == 'data_final':
                        item = line[value][:13].replace('-', '/')
                    else:
                        item = str(line[value])
                    line_file.append(item)
                marketer_file_lines.append(';'.join(line_file))
            marketer_file = ';\n'.join(marketer_file_lines) + ';\n'

            clinme_filename = self.filename(
                cursor,
                uid,
                clinme_id,
                marketer=marketer,
                context=context
            )
            res.append(marketer_file)
            if self.read(cursor, uid, clinme_id)['bz2']:
                bzdata = bz2.compress(marketer_file)
                zip_clinmes_file.writestr(clinme_filename, bzdata)
            else:
                zip_clinmes_file.writestr(clinme_filename, marketer_file)
        if str_mode:
            return res
        zip_clinmes_file.close()
        clinmes_file_value = clinmes_file.getvalue()
        clinmes_file.close()

        return clinmes_file_value

    _columns = {
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'periode': fields.char('Període', size=6, required=True),
        'data_creacio': fields.char('Data creació', size=8, required=True),
        'versio': fields.char('Versio', size=2, required=True),
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot', required=True),
        'linies': fields.one2many('giscedata.perfils.clinme.line', 'clinme_id',
                                  'Línies CLINME'),
        'bz2': fields.boolean('bz2'),
    }
    _order = "data_creacio desc, versio desc"

    def default_distribuidora(self, cursor, uid, context=None):
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        return user.company_id.partner_id.ref

    _defaults = {
        'distribuidora': default_distribuidora,
    }


GiscedataPerfilsCLINME()


class GiscedataPerfilsCLINMELine(osv.osv):
    """Línies del fitxer CLINME.
    """
    _name = 'giscedata.perfils.clinme.line'
    _columns = {
        'clinme_id': fields.many2one('giscedata.perfils.clinme', 'CLINME',
                                     required=True, ondelete='cascade'),
        'cups': fields.char('CUPS', size=22, required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'agree_tensio': fields.char('Nivell Tensió', size=4, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=4, required=True),
        'agree_tipo': fields.char('Tipo de Punto', size=2, required=True),
        'provincia': fields.char('Provincia', size=2, required=True),
        'mesura_ab': fields.char('Indicador mesura ALTA/BAIXA', size=1,
                                 required=True),
        'data_inici': fields.datetime('Data inici', required=True),
        'data_final': fields.datetime('Data final', required=True),
        'consum_ae': fields.integer('Consum AE', required=True),
        'consum_r1': fields.integer('Consum R1', required=True),
        'consum_r4': fields.integer('Consum R4', required=True),
        'n_estimats': fields.integer('Periodes estimats', required=True),
        'consum_ae_estimada': fields.integer('Consum AE Estimada',
                                             required=True),
    }

    _defaults = {
        'consum_ae': lambda *a: 0,
        'consum_r1': lambda *a: 0,
        'consum_r4': lambda *a: 0,
        'n_estimats': lambda *a: 0,
        'consum_ae_estimada': lambda *a: 0
    }


GiscedataPerfilsCLINMELine()


class GiscedataPerfilsAGCL(osv.osv):
    """Fitxer AGCL.
    """
    _name = 'giscedata.perfils.agcl'

    FIELDS_POS = {
        'operacio': 0,
        'tipo_c': 1,
        'distribuidora': 2,
        'comercialitzadora': 3,
        'agree_tensio': 4,
        'agree_tarifa': 5,
        'agree_dh': 6,
        'agree_tipo': 7,
        'provincia': 8,
        'data_inici_ag': 9,
        'data_final_ag': 10
    }

    def gen_file(self, cursor, uid, ids, context=None):
        """Genera el contingut del fitxer AGCL.
        """
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        pos_fields = {}
        for key, pos in self.FIELDS_POS.items():
            pos_fields[pos] = key
        line_obj = self.pool.get('giscedata.perfils.agcl.line')
        line_ids = line_obj.search(cursor, uid, [('agcl_id.id', '=', ids)])
        res_file = []
        for line in line_obj.read(cursor, uid, line_ids, pos_fields.values()):
            # Inicialitzem l'array per tantes posicions com camps tinguem
            line_file = [''] * len(pos_fields.values())
            for pos, key in pos_fields.items():
                if not line[key]:
                    line[key] = ''
                    continue
                if key in ('data_inici_ag', 'data_final_ag'):
                    line[key] = datetime.strptime(line[key], '%Y-%m-%d')
                    if key == 'data_final_ag':
                        line[key] += timedelta(days=1)
                    line[key] = line[key].strftime('%Y/%m/%d 00')
                line_file[pos] = str(line[key])
            res_file.append('%s;\n' % ';'.join(line_file))
        return res_file

    def download(self, cursor, uid, ids, context=None):
        """Retornem el wizard.
        """
        return {
            'view_type': 'form',
            "view_mode": 'form',
            'res_model': 'giscedata.perfils.agcl.download',
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def create(self, cursor, uid, vals, context=None):
        if 'lot_id' not in vals:
            raise osv.except_osv('Error', 'AGCL ha de tenir lot')
        lot = self.pool.get('giscedata.perfils.lot').browse(cursor, uid,
                                                            vals['lot_id'])
        if not lot.aggr_ids:
            raise osv.except_osv('Error', 'El lot ha de tenir les agregacions '
                                          'generades.')
        data_creacio = vals.get('data_creacio', time.strftime('%Y%m%d'))
        vals['data_creacio'] = data_creacio
        # Busquem si ja hi ha AGCL amb aqusta data de creació
        search_params = [('lot_id.id', '=', vals['lot_id']),
                         ('data_creacio', '=', data_creacio)]
        agids = self.search(cursor, uid, search_params, order="versio desc",
                            limit=1)
        versio = 0
        if agids:
            versio = int(self.browse(cursor, uid, agids[0]).versio)
        # Agafem l'última versió i l'incrementem
        versio += 1
        vals['versio'] = str(versio)
        vals['distribuidora'] = self.default_distribuidora(cursor, uid, context)
        vals['name'] = 'AGCL_%s_%s.%s' % (vals['distribuidora'],
                                          vals['data_creacio'], vals['versio'])
        res_id = super(GiscedataPerfilsAGCL, self).create(cursor, uid, vals)
        agcl_line_obj = self.pool.get('giscedata.perfils.agcl.line')
        aggr_obj = self.pool.get('giscedata.perfils.agregacions')
        agcl_fields = agcl_line_obj.fields_get(cursor, uid).keys()
        aggr_fields = aggr_obj.fields_get(cursor, uid).keys()
        agcl = False
        for agg in lot.aggr_ids:
            if agg.data_inici_ag >= lot.data_inici:
                # Alta
                vals = {'operacio': 'A', 'agcl_id': res_id}
                for attr in agcl_fields:
                    if attr not in vals and attr in aggr_fields:
                        vals[attr] = getattr(agg, attr)
                agcl_line_obj.create(cursor, uid, vals)
                agcl = True
            if agg.data_final_ag:
                # Baixa
                vals = {'operacio': 'B', 'agcl_id': res_id}
                for attr in agcl_fields:
                    if attr not in vals and attr in aggr_fields:
                        vals[attr] = getattr(agg, attr)
                agcl_line_obj.create(cursor, uid, vals)
                agcl = True
        if not agcl:
            self.unlink(cursor, uid, [res_id], context)
            res_id = False
        return res_id

    _columns = {
        'name': fields.char('Nom', size=256, required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'data_creacio': fields.char('Data creació', size=8, required=True),
        'versio': fields.char('Versio', size=2, required=True),
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot',
                                  required=True),
        'linies': fields.one2many('giscedata.perfils.agcl.line', 'agcl_id',
                                  'Línies AGCL'),
        'bz2': fields.boolean('bz2'),
    }

    _order = "data_creacio desc, versio desc"

    def default_distribuidora(self, cursor, uid, context=None):
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        return user.company_id.partner_id.ref

    _defaults = {
        'distribuidora': default_distribuidora,
    }


GiscedataPerfilsAGCL()


class GiscedataPerfilsAGCLLine(osv.osv):
    """Línies d'una AGCL.
    """
    _name = 'giscedata.perfils.agcl.line'
    _columns = {
        'agcl_id': fields.many2one('giscedata.perfils.agcl', 'AGCL',
                                   required=True, ondelete='cascade'),
        'operacio': fields.selection([('A', 'Alta'),
                                      ('B', 'Baixa'),
                                      ('C', 'Correcció')], 'Operació', size=1,
                                     required=True),
        'tipo_c': fields.selection([('A', 'Alta'),
                                    ('B', 'Baixa')], 'Tipo correcció', size=1),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'agree_tensio': fields.char('Nivell Tensió', size=4, required=True),
        'agree_tarifa': fields.char('Tarifa', size=4, required=True),
        'agree_dh': fields.char('DH', size=4, required=True),
        'agree_tipo': fields.char('Tipo de Punto', size=2, required=True),
        'provincia': fields.char('Provincia', size=2, required=True),
        'data_inici_ag': fields.date('Data inici'),
        'data_final_ag': fields.date('Data final')
    }


GiscedataPerfilsAGCLLine()


class GiscedataPerfilsAgregacions(osv.osv):
    """Diferents agregacions d'un perfilat.
    """
    _name = 'giscedata.perfils.agregacions'

    FIELDS_POS = {'distribuidora': 3,
                  'comercialitzadora': 4,
                  'provincia': 5,
                  'agree_tensio': 6,
                  'agree_tarifa': 7,
                  'agree_dh': 8,
                  'agree_tipo': 9,
                  'magnitud': 10}

    def generate(self, cursor, uid, lot_id, context=None):
        """Genera les agregacions amb els seus consums.
        """
        giscedata_perfils_clmag5 = self.pool.get('giscedata.perfils.clmag5')
        giscedata_perfils_clmag5_line = self.pool.get(
            'giscedata.perfils.clmag5.line'
        )

        fields_pos = {'distribuidora': 3, 'comercialitzadora': 4,
                      'provincia': 5, 'agree_tensio': 6, 'agree_tarifa': 7,
                      'agree_dh': 8, 'agree_tipo': 9, 'magnitud': 10}
        lot = self.pool.get('giscedata.perfils.lot').browse(cursor, uid, lot_id)
        # Agafem l'últim CLMAG (ordenats correctament segons ORM)
        if not lot.clmag_ids or not lot.clmag5_ids:
            raise osv.except_osv(
                _("Error"),
                _(
                    u"S'han de tenir generats el CLMAG i el CLMAG5 per poder "
                    u"generar els nivells d'agregació"
                )
            )
        # Eliminem agregacions antigues
        ul_ids = self.search(cursor, uid, [('lot_id.id', '=', lot_id)])
        if ul_ids:
            self.unlink(cursor, uid, ul_ids)
        clmag = lot.clmag_ids[0].gen_file()
        values = {}
        for line in map(str.strip, clmag):
            if not line:
                continue
            if line.endswith(';'):
                line = line[:-1]
            line = line.split(';')
            ag = tuple(line[3:10])
            data_ag = '%s-%s-%s' % tuple(reversed(line[0:3]))
            if ag not in values:
                values[ag] = {'lot_id': lot_id, 'consum': 0,
                              'data_inici': data_ag, 'data_final': data_ag}
                for key, index in fields_pos.items():
                    values[ag][key] = line[index]
            # Per cada linia fem el processat de les 25h
            for i in range(0, 25):
                index = (4 * i) + 11
                values[ag]['consum'] += int(line[index])
            values[ag]['data_inici'] = min(data_ag, values[ag]['data_inici'])
            values[ag]['data_final'] = max(data_ag, values[ag]['data_final'])
        giscedata_perfils_clmag5_ids = giscedata_perfils_clmag5.search(
            cursor,
            uid,
            [('batch', '=', lot_id)],
            context=context
        )
        giscedata_perfils_clmag5_line_ids = \
            giscedata_perfils_clmag5_line.search(
                cursor,
                uid,
                [('clmag5', '=', giscedata_perfils_clmag5_ids[0])],
                context=context
            )
        for clmag5_line in giscedata_perfils_clmag5_line.browse(
            cursor,
            uid,
            giscedata_perfils_clmag5_line_ids,
            context=context
        ):
            aggregation = (
                clmag5_line.distributor,
                clmag5_line.marketer,
                clmag5_line.voltage,
                clmag5_line.tariff,
                clmag5_line.hourly_discrimination,
                clmag5_line.border_type,
                clmag5_line.province
            )
            aggregation_date = (
                clmag5_line.timestamp.split(' ')[0].replace('/', '-')
            )
            if aggregation not in values:
                values[aggregation] = {
                    'lot_id': lot_id,
                    'consum': 0,
                    'data_inici': aggregation_date,
                    'data_final': aggregation_date,
                    'distribuidora': clmag5_line.distributor,
                    'comercialitzadora': clmag5_line.marketer,
                    'agree_tensio': clmag5_line.voltage,
                    'agree_tarifa': clmag5_line.tariff,
                    'agree_dh': clmag5_line.hourly_discrimination,
                    'agree_tipo': clmag5_line.border_type,
                    'provincia': clmag5_line.province,
                    'magnitud': 'AE',
                }
            values[aggregation]['consum'] += clmag5_line.active_input
            values[aggregation]['data_inici'] = min(
                aggregation_date,
                values[aggregation]['data_inici']
            )
            values[aggregation]['data_final'] = max(
                aggregation_date,
                values[aggregation]['data_final']
            )
            # s'endarrereix un dia la data_final per la comunicació de l'hora 00
            end_date = datetime.strptime(
                values[aggregation]['data_final'], '%Y-%m-%d'
            ) - timedelta(days=1)
            values[aggregation]['data_final'] = end_date.strftime(
                '%Y-%m-%d'
            )

        res = ""
        agg_ids = []
        for ag, vals in values.items():
            res += '[{0}]: {1}\n'.format(';'.join(ag), vals['consum'])
            agg_ids += [self.create(cursor, uid, vals, context)]
        res += '(%i nivells)\n' % (len(values.keys()))
        self.check_agcl(cursor, uid, agg_ids)
        return res

    def search_inici_ag(self, cursor, uid, ids, context=None):
        if isinstance(ids, list) or isinstance(ids, tuple):
            ids = ids[0]
        agg = self.browse(cursor, uid, ids)
        lot_ant_id = agg.lot_id.anterior()
        if not lot_ant_id:
            # No existeix el lot anterior, per tant no hi ha agregació
            return agg.data_inici
        search_params = [(k, '=', getattr(agg, k)) for k in self.FIELDS_POS]
        search_params += [('lot_id.id', '=', lot_ant_id)]
        # Busquem l'agregació en el lot anterior
        agg_ant_id = self.search(cursor, uid, search_params, limit=1)
        if not agg_ant_id:
            # No existeix aquesta agregació en el lot anterior
            return agg.data_inici
        agg_ant = self.browse(cursor, uid, agg_ant_id[0])
        if agg_ant.data_final_ag:
            # Aquesta agregació al lot anterior té data de baixa d'agregació
            return agg.data_inici
        return agg_ant.data_inici_ag

    def is_baixa_ag(self, cursor, uid, agg, context=None):
        '''Search for modcontractual associated with this aggregation.
        If we do not have any modcontractual with data_inici
        previous or equal to the agg.data_final and data_final later than
        agg.data_final, agg data_final_ag must be agg data_final'''
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        # Les modificacions contractuals són intervals tancats per tant la
        # data d'inici que hem de buscar com a continuació de l'agregació és
        # la data_final + 1 dia
        data_inici = (datetime.strptime(agg.data_final, '%Y-%m-%d')
                      + timedelta(days=1)).strftime('%Y-%m-%d')

        from osv.expression import OOQuery
        q = OOQuery(modcon_obj, cursor, uid)
        sql = q.select(
            ['id'], only_active=False
        ).where([
            ('agree_dh', '=', agg.agree_dh),
            ('agree_tensio', '=', agg.agree_tensio),
            ('agree_tipus', '=', agg.agree_tipo),
            ('agree_tarifa', '=', agg.agree_tarifa),
            ('comercialitzadora.ref', '=', agg.comercialitzadora),
            ('data_inici', '<=', data_inici),
            ('data_final', '>', agg.data_final),
            ('cups.id_municipi.state.ree_code', '=', agg.provincia)
        ])
        cursor.execute(*sql)

        if cursor.rowcount:
            return False
        return True

    def check_agcl(self, cursor, uid, ids, context=None):
        """Comprova si és una agregació d'alta o de baixa
        """
        for agg in self.browse(cursor, uid, ids):
            if agg.data_inici > agg.lot_id.data_inici:
                # Detectem alta
                agg.write({'data_inici_ag': agg.data_inici})
                # Detectem baixa
            if self.is_baixa_ag(cursor, uid, agg,
                                context=context):
                agg.write({'data_final_ag': agg.data_final})
            if agg.data_inici == agg.lot_id.data_inici:
                # Hem de mirar si aquesta agregació existeix al lot anterior
                # i si existeix si té data final d'agregació
                agg.write({'data_inici_ag': agg.search_inici_ag()})
        return True

    _columns = {
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot',
                                  required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'magnitud': fields.char('Magnitud', size=2, required=True),
        'consum': fields.integer('Consum', required=True),
        'data_inici': fields.date('Data inici (min)'),
        'data_final': fields.date('Data final (max)'),
        'data_inici_ag': fields.date('Data inici (aggr)'),
        'data_final_ag': fields.date('Data final (aggr)'),
        'validat_ree': fields.boolean('Comprovat REE'),
        'checked_agcl': fields.boolean('Comprovat AGCL'),
    }

    _defaults = {
        'consum': lambda *a: 0,
        'validat_ree': lambda *a: False,
        'checked_agcl': lambda *a: False,
    }


GiscedataPerfilsAgregacions()


class GiscedataPerfilsAgregacionsValidacions(osv.osv):
    _name = 'giscedata.perfils.agregacions.validacions'

    _rec_name = 'distribuidora'

    def create(self, cursor, uid, vals, context=None):
        perf_lot_obj = self.pool.get('giscedata.perfils.lot')
        agg_ids = self.search(cursor, uid, [('lot_id', '=', vals['lot_id'])])
        if agg_ids:
            self.unlink(cursor, uid, agg_ids, context)

        keys = ['clmag5a_consumption', 'clmag_consumption',
                'clinme_consumption', 'consum']
        with open(vals['filename']) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=';')
            for row in reader:
                row.pop('', None)
                row['lot_id'] = vals['lot_id']
                # Aggregate sum pandas generate a float number
                for key in keys:
                    row[key] = row[key].split('.')[0]

                super(GiscedataPerfilsAgregacionsValidacions, self).create(
                    cursor, uid, row
                )
        perf_lot_obj.browse(cursor, uid, vals['lot_id']).write(
            {'ultima_comprovacio_consums': datetime.now().strftime(
                '%d-%m-%Y %H:%M:%S'
            )}
        )

    _columns = {
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot',
                                  required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'consum': fields.integer('Consum'),
        'clmag_consumption': fields.integer('Consum CLMAG'),
        'clmag5a_consumption': fields.integer('Consum CLMAG5A'),
        'clinme_consumption': fields.integer('Consum CLINME'),
    }

GiscedataPerfilsAgregacionsValidacions()



class GiscedataPerfilsAgregacionsREE(osv.osv):

    _name = 'giscedata.perfils.agregacions.ree'

    _columns = {
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot',
                                  required=True),
        'distribuidora': fields.char('Distribuidora', size=4, required=True),
        'comercialitzadora': fields.char('Comercialitzadora', size=4,
                                         required=True),
        'provincia': fields.char('Provincia/Subsistema', size=2, required=True),
        'agree_tensio': fields.char('Nivell de tensió', size=2, required=True),
        'agree_tarifa': fields.char('Tarifa', size=2, required=True),
        'agree_dh': fields.char('DH', size=2, required=True),
        'agree_tipo': fields.char('Tipo', size=2, required=True),
        'magnitud': fields.char('Magnitud', size=2),
        'data_inici_ag': fields.date('Data inici (aggr)', required=True),
        'data_final_ag': fields.date('Data final (aggr)'),
        'consum': fields.integer('Consum'),
        'consum_ree': fields.integer('Consum REE'),
        'consum_diff': fields.integer('Diferència de consum'),
        'validat_ree': fields.boolean('Comprovat REE'),
    }

    _defaults = {
        'consum': lambda *a: 0,
        'consum_diff': lambda *a: 0,
        'validat_ree': lambda *a: False,
    }

GiscedataPerfilsAgregacionsREE()


class GiscedataPerfilsCurva(osv.osv):
    _name = 'giscedata.perfils.curva'

    _columns = {
        'name': fields.char('Descripció', size=64, required=True),
        'inici': fields.datetime('Inici', required=True, readonly=True),
        'final': fields.datetime('Final', required=True, readonly=True),
        'compact': fields.boolean('Compactada', required=True, readonly=True),
        'observacions': fields.text('Observacions'),
        'hores': fields.one2many('giscedata.perfils.curva.lectura', 'curva', 'Hores'),
    }

    _defaults = {
        'compact': lambda *a: 0,
    }

    _order = "final desc"


GiscedataPerfilsCurva()


class GiscedataPerfilsCurvaLectura(osv.osv):
    _name = 'giscedata.perfils.curva.lectura'

    _columns = {
        'curva': fields.many2one('giscedata.perfils.curva', 'Corba', required=True,
                                 ondelete='cascade'),
        'name': fields.char('Timestamp', size=10, required=True),
        'cups': fields.char('CUPS', size=25, required=True),
        'distribuidora': fields.char('Distribuidora', size=4),
        'tipo': fields.selection(TIPOS, 'Tipus de Punts'),
        'magnitud': fields.char('Magnitud', size=2),
        'year': fields.char('Any', size=4, required=True),
        'month': fields.char('Mes', size=2, required=True),
        'day': fields.char('Dia', size=2, required=True),
        'hour': fields.integer('Hora', required=True),
        'estacio': fields.integer('Estació', selection=[(0, 'Hivern'),
                                                        (1, 'Estiu')],
                                  required=True),
        'lectura_real': fields.integer('Lectura Real'),
        'perdues': fields.float('Pèrdues'),
        'lectura': fields.integer('Lectura', required=True),
        'arrastre': fields.float('Arrossegament', digits=(3, 3)),
        'upr': fields.char('Unitat Programació', size=8, required=True),
        'provincia': fields.char('Provincia', size=2, required=True)
    }

    _order = "name asc, magnitud asc"


GiscedataPerfilsCurvaLectura()
