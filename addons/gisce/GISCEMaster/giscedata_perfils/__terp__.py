# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures",
    "description": """
Sistema per la perfilació de les lectures
  * Permet transformar lectures en perfils.
  * Transformar curves horàries en tarifes.
  * Convertir fitxers de curva en fitxers F1.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_lectures",
        "giscedata_polissa_distri",
        "giscedata_administracio_publica_ree",
        "giscedata_facturacio",
        "l10n_ES_toponyms",
        "spawn_oop",
        "crm",
        "giscedata_dfestius"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_perfils_demo.xml",
    ],
    "update_xml": [
        "giscedata_perfils_data.xml",
        "giscedata_perfils_cronjobs.xml",
        "wizard/wizard_importar_curves_view.xml",
        "wizard/wizard_reperfilar_view.xml",
        "giscedata_perfils_wizard.xml",
        "wizard/wizard_aggregations_export_view.xml",
        "wizard/wizard_validacions_medidas_view.xml",
        "giscedata_perfils_view.xml",
        "wizard/wizard_clmag_download_view.xml",
        "wizard/wizard_clinme_download_view.xml",
        "wizard/wizard_agcl_download_view.xml",
        "wizard/wizard_assignar_polisses_view.xml",
        "wizard/wizard_ree_potagr_view.xml",
        "wizard/wizard_ree_enefac_view.xml",
        "giscedata_polissa_view.xml",
        "security/giscedata_perfils_security.xml",
        "wizard/wizard_ree_f1_curve_view.xml",
        "wizard/wizard_ree_cups_view.xml",
        "wizard/wizard_ree_cups5_view.xml",
        "wizard/wizard_filter_profiles_view.xml",
        "crm_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
