# -*- coding: utf-8 -*-
from StringIO import StringIO
import base64
import bz2
from osv import osv, fields

class GiscedataPerfilsCLMAGDownload(osv.osv_memory):
    """Wizard per descarregar els fitxer CLMAG.
    """
    _name = 'giscedata.perfils.clmag.download'

    def _get_clmag_obj(self, cursor, uid, context=None):
        """Retorna l'objecte CLMAG.
        """
        clmag_obj = self.pool.get('giscedata.perfils.clmag')
        if not context:
            context = {}
        active_id = context.get('active_ids', False)
        if active_id:
            active_id = active_id[0]
            return clmag_obj.browse(cursor, uid, active_id)
        return False

    def _get_clmag_bz2(self, cursor, uid, context=None):
        """Retorna el valor del checkbox bz2.
        """
        clmag = self._get_clmag_obj(cursor, uid, context)
        if clmag:
            return clmag.bz2
        return False

    def default_name(self, cursor, uid, context=None):
        """Retorna el nom del fitxer per defecte.
        """
        clmag = self._get_clmag_obj(cursor, uid, context)
        if clmag:
            return clmag.name
        return False

    def default_file(self, cursor, uid, context=None):
        """Retorna el fitxer per defecte.
        """
        clmag = self._get_clmag_obj(cursor, uid, context)
        _bz2 = self._get_clmag_bz2(cursor, uid, context)
        if clmag:
            lines = clmag.gen_file()
            clmag = StringIO()
            clmag.writelines(lines)
            if _bz2:
                bzdata = bz2.compress(clmag.getvalue())
                res_file = base64.b64encode(bzdata)
            else:
                res_file = base64.b64encode(clmag.getvalue())
            clmag.close()
            return res_file
        return False

    _columns = {
        'name': fields.char('Nom del fitxer', size=256),
        'file': fields.binary('Fitxer'),
    }

    _defaults = {
        'name': default_name,
        'file': default_file
    }

GiscedataPerfilsCLMAGDownload()