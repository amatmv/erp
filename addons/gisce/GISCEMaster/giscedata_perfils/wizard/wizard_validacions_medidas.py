# -*- coding: utf-8 -*-
from osv import osv, fields
import csv
import bz2
import base64
import StringIO
import pandas as pd
from datetime import datetime
from tools.translate import _

REE_END_DATE = '3000/01/01 00'

class ImportarValidacionsMedidas(osv.osv_memory):

    _name = 'giscedata.importar.validacions.medidas'

    def validate_aggregations(self, cursor, uid, ids, aggr_ree_ids,
                              context=None):
        """ Check aggregations with aggregations REE
        """
        lot_obj = self.pool.get('giscedata.perfils.lot')
        aggr_obj = self.pool.get('giscedata.perfils.agregacions')
        aggr_ree_obj = self.pool.get('giscedata.perfils.agregacions.ree')
        lot_id = context.get('active_ids', False)

        if lot_id:
            lot_id = lot_id[0]
        aggr_ids = lot_obj.read(cursor, uid, lot_id, ['aggr_ids'])['aggr_ids']
        columns_to_analyze = [
            'distribuidora', 'comercialitzadora', 'agree_tensio',
            'agree_tarifa', 'agree_dh', 'agree_tipo', 'provincia'
        ]
        data = aggr_obj.read(cursor, uid, aggr_ids, columns_to_analyze)
        for elem in aggr_ree_obj.read(
                cursor, uid, aggr_ree_ids, columns_to_analyze
        ):
            data.append(elem)
        df = pd.DataFrame(data=data)
        df_t_2 = df.drop_duplicates(subset=columns_to_analyze, keep=False)
        # validation table aggregations ree
        for aggr_ree in aggr_ree_obj.browse(cursor, uid, aggr_ree_ids):
            if aggr_ree.id not in df_t_2.values:
                aggr_ree.write({'validat_ree': True})
        # validation table aggregations
        for aggr in aggr_obj.browse(cursor, uid, aggr_ids):
            if aggr.id not in df_t_2.values:
                aggr.write({'validat_ree': True})
            aggr.write({'checked_agcl': True})

    def _check_filename(self, cursor, uid, ids, filename, lot_id, context=None):
        """ Check that the filename is correct, parsing year, month, period and
        checking if it matches the batch
        :param filename: str with filename
        :param lot_id: batch_id
        :return: True if the filename is correct, otherwise raise an exception
        """
        lot_obj = self.pool.get('giscedata.perfils.lot')
        try:
            year = filename.split('.')[0].rsplit('_', 1)[1][:4]
            month = filename.split('.')[0].rsplit('_', 1)[1][4:]
            period = '{month}/{year}'.format(month=month, year=year)
            if period == lot_obj.read(cursor, uid, lot_id, ['name'])['name']:
                return True
            else:
                _msg = _('El fitxer no correspon al lot, cal importar un '
                         'fitxer del lot: {0}'.format(period))
                raise osv.except_osv('Error', _msg)
        except Exception:
            raise osv.except_osv('Error', _('Fitxer erroni'))

    def action_import_files(self, cursor, uid, ids, context=None):
        """ Import AGCL, AGCLACUM and AGCLACUM5 files. Can import all three at
        once, but you need to import at least the AGCL
        """
        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        aggr_ree_obj = self.pool.get('giscedata.perfils.agregacions.ree')
        aggr_obj = self.pool.get('giscedata.perfils.agregacions')
        lot_id = context.get('active_ids', False)
        if lot_id:
            lot_id = lot_id[0]

        # AGCL File
        if wizard.file_agcl:
            self._check_filename(cursor, uid, ids, wizard.filename_agcl, lot_id,
                                 context)
            if 'bz2' in wizard.filename_agcl:
                _file_agcl = StringIO.StringIO(
                    bz2.decompress(base64.b64decode(wizard.file_agcl))
                )
            else:
                _file_agcl = StringIO.StringIO(
                    base64.b64decode(wizard.file_agcl)
                )

            reader = csv.reader(_file_agcl, delimiter=';')
            aggr_ree_old_ids = aggr_ree_obj.search(
                cursor, uid, [('lot_id', '=', lot_id)]
            )
            aggr_ree_ids = []
            for row in reader:
                vals = {
                    'lot_id': lot_id,
                    'distribuidora': row[0],
                    'comercialitzadora': row[1],
                    'agree_tensio': row[2],
                    'agree_tarifa': row[3],
                    'agree_dh': row[4],
                    'agree_tipo': row[5].zfill(2),
                    'provincia': row[6],
                    'data_inici_ag': row[7],
                }
                vals['data_inici_ag'] = datetime.strptime(
                    vals['data_inici_ag'], '%Y/%m/%d %H'
                )
                if row[8] and row[8] != REE_END_DATE:
                    vals['data_final_ag'] = datetime.strptime(
                        row[8], '%Y/%m/%d %H'
                    )
                aggr_ree_ids.append(aggr_ree_obj.create(cursor, uid, vals))
            aggr_ree_obj.unlink(cursor, uid, aggr_ree_old_ids)
            self.validate_aggregations(cursor, uid, ids, aggr_ree_ids, context)

        # AGCLACUM File
        if wizard.file_agclacum:
            self._check_filename(cursor, uid, ids, wizard.filename_agclacum,
                                 lot_id, context)
            if 'bz2' in wizard.filename_agclacum:
                _file_agclacum = StringIO.StringIO(
                    bz2.decompress(base64.b64decode(wizard.file_agclacum))
                )
            else:
                _file_agclacum = StringIO.StringIO(
                    base64.b64decode(wizard.file_agclacum)
                )

            reader = csv.reader(_file_agclacum, delimiter=';')

            for row in reader:
                search_params = [
                    ('lot_id', '=', lot_id),
                    ('distribuidora', '=', row[0]),
                    ('comercialitzadora', '=', row[1]),
                    ('agree_tensio', '=', row[2]),
                    ('agree_tarifa', '=', row[3]),
                    ('agree_dh', '=', row[4]),
                    ('agree_tipo', '=', row[5].zfill(2)),
                    ('provincia', '=', row[6]),
                ]
                aggr_ree_id = aggr_ree_obj.search(cursor, uid, search_params)
                if aggr_ree_id:
                    aggr_id = aggr_obj.search(cursor, uid, search_params)
                    magnitud = row[7]
                    consum_ree = int(row[8])
                    vals = {
                        'magnitud': magnitud,
                        'consum_ree': consum_ree,
                    }
                    if aggr_id:
                        consum = aggr_obj.read(
                            cursor, uid, aggr_id, ['consum']
                        )[0]['consum']
                        consum_diff = consum - consum_ree
                        vals['consum'] = consum
                        vals['consum_diff'] = consum_diff
                    else:
                        vals['validat_ree'] = False
                    aggr_ree_obj.write(cursor, uid, aggr_ree_id, vals)
                else:
                    osv.except_osv('Error',
                        _('Cal tenir importades les agregacions de REE')
                    )

        # AGCLACUM5 File
        if wizard.file_agclacum5:
            self._check_filename(cursor, uid, ids, wizard.filename_agclacum5,
                                 lot_id, context)
            if 'bz2' in wizard.filename_agclacum5:
                _file_agclacum5 = StringIO.StringIO(
                    bz2.decompress(base64.b64decode(wizard.file_agclacum5))
                )
            else:
                _file_agclacum5 = StringIO.StringIO(
                    base64.b64decode(wizard.file_agclacum5)
                )

            reader = csv.reader(_file_agclacum5, delimiter=';')

            for row in reader:
                if row[7] == 'AS':
                    continue
                search_params = [
                    ('lot_id', '=', lot_id),
                    ('distribuidora', '=', row[0]),
                    ('comercialitzadora', '=', row[1]),
                    ('agree_tensio', '=', row[2]),
                    ('agree_tarifa', '=', row[3]),
                    ('agree_dh', '=', row[4]),
                    ('agree_tipo', '=', row[5].zfill(2)),
                    ('provincia', '=', row[6]),
                ]
                aggr_ree_id = aggr_ree_obj.search(cursor, uid, search_params)
                if aggr_ree_id:
                    aggr_id = aggr_obj.search(cursor, uid, search_params)
                    magnitud = row[7]
                    consum_ree = int(row[8])
                    vals = {
                        'magnitud': magnitud,
                        'consum_ree': consum_ree,
                    }
                    if aggr_id:
                        consum = aggr_obj.read(
                            cursor, uid, aggr_id, ['consum']
                        )[0]['consum']
                        consum_diff = consum - consum_ree
                        vals['consum'] = consum
                        vals['consum_diff'] = consum_diff
                    else:
                        vals['validat_ree'] = False
                    aggr_ree_obj.write(cursor, uid, aggr_ree_id, vals)
        _msg = _("Fitxers importats correctament, revisar la taula "
                 "d'agregacions REE")
        wizard.write({'state': 'done', 'info': _msg})

    _columns = {
        'file_agcl': fields.binary('AGCL'),
        'filename_agcl': fields.char('Nom', size=32),
        'file_agclacum': fields.binary('AGCLACUM'),
        'filename_agclacum': fields.char('Nom', size=32),
        'file_agclacum5': fields.binary('AGCLACUM5'),
        'filename_agclacum5': fields.char('Nom', size=32),
        'state': fields.char('State', size=4),
        'info': fields.text('Info', readonly=True),
    }

    _defaults = {
        'state': lambda * x: 'init',
    }


ImportarValidacionsMedidas()
