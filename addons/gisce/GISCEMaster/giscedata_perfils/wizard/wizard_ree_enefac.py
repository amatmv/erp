# -*- coding: utf-8 -*-
from datetime import datetime
import base64
import zipfile
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO
from osv import osv, fields
from tools import config
from tools.translate import _


class WizardReeEnefac(osv.osv_memory):
    """Assistent per generar els fitxers ENEFAC.
    """
    _name = 'wizard.ree.enefac'

    def _default_period(self, cursor, uid, context=None):
        """Retornem per defecte el periode actual.
        """
        return datetime.now().strftime('%m/%Y')

    _columns = {
        'name': fields.char('Nom', size=256),
        'period': fields.char('Període (MM/YYYY)', size=7, required=True,
                              help="Període que es vol generar MM/YYYY"),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }
    _defaults = {
        'state': lambda *a: 'init',
        'period': _default_period
    }

    def export_file(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        ree_code = user.company_id.cups_code
        try:
            date = datetime.strptime(wizard.period, '%m/%Y')
        except ValueError:
            raise osv.except_osv(_('Error'),
                                 _('El format del període no és correcte'))
        sql = open('%s/giscedata_perfils/sql/ENEFAC.sql'
                   % (config['addons_path'])).read()
        cursor.execute(sql, (ree_code, ) + (date.strftime('%Y-%m-%d'), ) * 6)
        enefacs = {}
        for line in cursor.fetchall():
            mes = line[-1]
            if mes not in enefacs:
                enefacs.update({mes: []})
            enefacs[mes].append(';'.join([str(a) for a in line]) + ';')
        the_zip_io = StringIO.StringIO()
        the_zip = zipfile.ZipFile(the_zip_io, 'w',
                                  compression=zipfile.ZIP_DEFLATED)
        for mes in enefacs.keys():
            fname = 'ENEFAC_%s_%s_%s.0' % (ree_code,
                                           mes,
                                           datetime.now().strftime('%Y%m%d'))
            the_zip.writestr(fname, '\n'.join(enefacs[mes]))
        the_zip.close()
        vals = {
            'state': 'end',
            'name': 'ENEFAC_%s_%s.zip' % (ree_code,
                                          datetime.now().strftime('%Y%m%d')),
            'file': base64.b64encode(the_zip_io.getvalue())
        }
        the_zip_io.close()
        self.write(cursor, uid, ids, vals, context)

WizardReeEnefac()
