import base64

from osv import osv, fields


class GiscedataPerfilsCLINMEDownload(osv.osv_memory):
    """
    Wizard to download the CLINME file.
    """
    _name = 'giscedata.perfils.clinme.download'

    def _get_giscedata_perfils_clinme(self, cursor, uid, context=None):
        """
        Returns the giscedata.perfils.clinme object.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: giscedata.perfils.clinme object
        """

        giscedata_perfils_clinme = self.pool.get('giscedata.perfils.clinme')
        if context is None:
            context = {}
        active_id = context.get('active_ids', False)
        if active_id:
            active_id = active_id[0]
            return giscedata_perfils_clinme.browse(cursor, uid, active_id)
        return False

    def default_name(self, cursor, uid, context=None):
        """
        Returns the CLINME filename.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: string with the CLINME filename.
        """
        clinme = self._get_giscedata_perfils_clinme(
            cursor,
            uid,
            context=context
        )
        if clinme:
            return clinme.filename(context=context)
        return False

    def default_file(self, cursor, uid, context=None):
        """
        Returns the CLINME file.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: the CLINME file

        Retorna el fitxer per defecte.
        """
        clinme = self._get_giscedata_perfils_clinme(
            cursor,
            uid,
            context
        )
        if clinme:
            clinme_file = clinme.build_file()
            base64_file = base64.b64encode(clinme_file)
            return base64_file
        return False

    _columns = {
        'name': fields.char('Nom del fitxer', size=256),
        'file': fields.binary('Fitxer'),
    }

    _defaults = {
        'name': default_name,
        'file': default_file
    }

GiscedataPerfilsCLINMEDownload()
