# -*- coding: utf-8 -*-
from StringIO import StringIO
import base64
import csv
from osv import osv, fields

class GiscedataPerfilsAggregationsExport(osv.osv_memory):

    _name = 'giscedata.perfils.agregacions.export'

    def default_filename(self, cursor, uid, context=None):
        lot_obj = self.pool.get('giscedata.perfils.lot')
        lot_id = context.get('active_ids', False)
        if lot_id:
            lot_id = lot_id[0]
            lot_name = lot_obj.read(
                cursor, uid, lot_id, ['name']
            )['name']
            lot_name = lot_name.replace('/', '-')
            _default_filename = 'aggregations_{lot}.csv'.format(lot=lot_name)
        else:
            _default_filename = 'aggregations.csv'

        return _default_filename

    def action_export_csv(self, cursor, uid, context=None):
        if not context:
            context = {}

        ord = 'comercialitzadora, agree_tarifa, agree_dh, agree_tipo, provincia'
        aggr_obj = self.pool.get('giscedata.perfils.agregacions')
        lot_id = context.get('active_ids', False)
        if lot_id:
            lot_id = lot_id[0]
            csv_file = StringIO()
            aggr_ids = aggr_obj.search(
                cursor, uid, [('lot_id', '=', lot_id)], order=ord
            )
            for aggr_line in aggr_obj.browse(cursor, uid, aggr_ids):
                if not aggr_line.data_final_ag:
                    final_aggregation = ''
                else:
                    final_aggregation = aggr_line.data_final_ag.replace('-', '/')
                line = [
                    aggr_line.distribuidora,
                    aggr_line.comercialitzadora,
                    aggr_line.provincia,
                    aggr_line.agree_tensio,
                    aggr_line.agree_tarifa,
                    aggr_line.agree_dh,
                    aggr_line.agree_tipo,
                    aggr_line.magnitud,
                    aggr_line.consum,
                    aggr_line.data_inici.replace('-', '/'),
                    aggr_line.data_final.replace('-', '/'),
                    aggr_line.data_inici_ag.replace('-', '/'),
                    final_aggregation,
                ]
                csv_file.write('%s;\n' % ';'.join(map(str, line)))
            res_file = base64.b64encode(csv_file.getvalue())
            csv_file.close()
            return res_file
        else:
            return False

    _columns = {
        'name': fields.char('Nom del fitxer', size=256),
        'file': fields.binary('Fitxer'),
    }

    _defaults = {
        'file': action_export_csv,
        'name': default_filename,
    }

GiscedataPerfilsAggregationsExport()