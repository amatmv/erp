# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import timedelta
import datetime
import base64
import calendar
import zipfile
import cStringIO as StringIO
from lxml import etree
from curva import Curva
from tools.translate import _


class ImportarCurva(osv.osv_memory):

    _name = 'giscedata.perfils.importar.curva'

    def retry(self, cursor, uid, ids, context=None):

        self.write(cursor, uid, [ids[0]], {'state': 'init',
                                           'file': False,
                                           'errors': ''})

    def translate(self, xml):
        #Per si es amb format PRIME
        salida = ''
        tree = etree.fromstring(xml)
        for element in tree.iter():
            if element.tag == 'S02':
                time = '%s/%s/%s %s:00' % (element.get('Fh')[6:8],
                                         element.get('Fh')[4:6],
                                         element.get('Fh')[:4],
                                         element.get('Fh')[8:10],)
                linea = '%s,%s,%s,%s,%s,%s,%s\n' % (time,
                                           element.get('AI'),
                                           element.get('AE'),
                                           element.get('R1'),
                                           element.get('R2'),
                                           element.get('R3'),
                                           element.get('R4'),)
                salida += linea

        return salida

    def get_file_content(self, wizard):
        file_content = []
        noms_ziv = []
        contingut = {}
        #Comprovar si és zip i si ho és descomprimir
        _data = base64.decodestring(wizard.file)
        fileHandle = StringIO.StringIO(_data)
        try:
            fitxerZip = zipfile.ZipFile(fileHandle, "r")
            #print fitxerZip.namelist()
            for nom in fitxerZip.namelist():
                file_content.append(fitxerZip.read(nom))
                noms_ziv.append(nom)
                contingut[nom] = fitxerZip.read(nom)
        except zipfile.BadZipfile, e:
            #L'arxiu no es zip
            # Nom del fitxer
            nom = wizard.filename
            file_content.append(base64.b64decode(wizard.file))
            contingut = {nom: base64.b64decode(wizard.file)}

        return file_content, noms_ziv, contingut


    def check_format(self, cursor, uid, ids, context=None):

        file_content = []
        noms_ziv = []

        wizard = self.browse(cursor, uid, ids[0])

        #Comprovar si és zip i si ho és descomprimir
        _data = base64.decodestring(wizard.file)
        fileHandle = StringIO.StringIO(_data)
        try:
            fitxerZip = zipfile.ZipFile(fileHandle, "r")
            #print fitxerZip.namelist()
            for nom in fitxerZip.namelist():
                file_content.append(fitxerZip.read(nom))
                noms_ziv.append(nom)
        except zipfile.BadZipfile, e:
            #L'arxiu no es zip
            file_content.append(base64.b64decode(wizard.file))

        #canvio l'estat a check per anar al següent menu
        wizard.write({'state': 'check'})

        return True

    def escriure_polissa(self, cursor, uid, obj_curva, id_polissa_contador):
        obj_polissa = self.pool.get('giscedata.polissa')

        #Buscar id associada a la polisa per buscar CUPS
        polissa_id = id_polissa_contador
        setted = False
        if polissa_id:
            #el cas de que hi ha id de polissa
            setted = obj_polissa.write(cursor, uid, [polissa_id[0]], {
                'data_ultima_lectura_perfilada': obj_curva.data_final
            })
        return setted

    def assignar_corba_polissa(self, cursor, uid, cups_name,
                               obj_corba, context=None):
        obj_perfils_cont_lot = self.pool.get('giscedata.perfils.contracte_lot')
        obj_polissa = self.pool.get('giscedata.polissa')

        ctx = {}
        if context:
            ctx.update(context)
        ctx.update({'active_test': False})

        #Buscar id associada
        cont_id = obj_perfils_cont_lot.search(cursor, uid, [
            ('polissa_id.cups.name', '=', cups_name),
            ('lot_id.data_inici', '<', obj_corba.data_final),
            ('lot_id.data_final', '>=', obj_corba.data_inicial),
        ], context=ctx)

        setted = False
        # Aqui estic assignant la polissa al lot en el cas que ja existeixi
        if cont_id:
            #el cas de que hi ha ids del lot
            setted = obj_perfils_cont_lot.write(cursor, uid, [cont_id[0]], {
                'origen_real': 'corba'
            })

        return setted

    def calcular_estacio(self, any, mes, dia, hora):
        # Busquem el dia de canvi d'hora pel mes/any que passem
        estacio = ''
        dia_canvi_hora = {}
        dia_canvi_hora['Estiu'] = 0
        dia_canvi_hora['Hivern'] = 0

        i = -1
        while not dia_canvi_hora['Estiu']:
            dia_canvi_hora['Estiu'] = calendar.monthcalendar(any,
                                                             3)[i][-1]
            i -= 1
        i = -1
        while not dia_canvi_hora['Hivern']:
            dia_canvi_hora['Hivern'] = calendar.monthcalendar(any,
                                                              10)[i][-1]
            i -= 1

        if mes in (1, 2, 11, 12):
            # Son mesos d'hivern normals (24h)
            estacio = 'Hivern'
        if mes in (4, 5, 6, 7, 8, 9):
            # Son mesos d'estiu normals (24h)
            estacio = 'Estiu'
        if mes == 10:
            if dia < dia_canvi_hora['Hivern']:
                # dies normals abans del canvi d'hora d'hivern (24h)
                estacio = 'Estiu'
            if dia == dia_canvi_hora['Hivern']:
                # dia d'estiu/hivern (25h)
                if hora in xrange(1, 3):
                    estacio = 'Estiu'
                elif hora in xrange(3, 25):
                    estacio = 'Hivern'

            if dia > dia_canvi_hora['Hivern']:
                # dies normals després del canvi d'hora d'hivern (24h)
                if hora in xrange(1, 25):
                    estacio = 'Hivern'
        if mes == 3:
            if dia < dia_canvi_hora['Estiu']:
                # dies normals abans del canvi d'hora d'estiu (24h)
                if hora in xrange(1, 25):
                    estacio = 'Hivern'
            if dia == dia_canvi_hora['Estiu']:
                # dia hivern/estiu (25h)
                if hora in xrange(1, 2):
                    estacio = 'Hivern'
                elif hora in xrange(3, 25):
                    estacio = 'Estiu'
            if dia > dia_canvi_hora['Estiu']:
                # dies normals després del canvi d'hora d'estiu (24h)
                if hora in xrange(1, 25):
                    estacio = 'Estiu'

        #Per defecte retorno que es Hivern amb 0, si es Estiu 1.
        ret_estacio = '0'
        if estacio == 'Estiu':
            ret_estacio = '1'

        return ret_estacio

    def get_cups(self, cursor, uid, obj_curva, filetype, context=None):
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        #Busco l'identificador del comptador per el numero de contador, la data
        #d'alta i baixa, inclosos
        #els que no estan actius
        ctx = {}
        if context:
            ctx.update(context)
        ctx.update({'active_test': False})

        cups = ('', '')
        id_polissa_contador = ''

        if filetype == 'tpl':
            #busco en les lectures del comptador amb el nom que a mes
            # coincideixi amb la data inicial i final de la curva
            id_comptador = comptador_obj.search(
                cursor, uid, [('name', '=', obj_curva.num_contador),
                              ('data_alta', '<', obj_curva.data_final),
                              '|',
                              ('data_baixa', '>=', obj_curva.data_inicial),
                              ('data_baixa', '=', False)],
                context=ctx)

            if id_comptador:
                #Amb l'id de la polissa corresponent
                comptador_vals = comptador_obj.read(cursor, uid, id_comptador,
                                                    ["polissa"])

                id_polissa_contador = comptador_vals[0]['polissa']

                polissa_obj = self.pool.get('giscedata.polissa')
                polissa_det = polissa_obj.read(cursor, uid,
                                               [id_polissa_contador[0]],
                                               ['cups'])
                cups = polissa_det[0]['cups']
        elif filetype == 'ziv':
            cups_obj = self.pool.get('giscedata.cups.ps')
            polissa_obj = self.pool.get('giscedata.polissa')
            cups = obj_curva.cups_name
            id_cups = cups_obj.search(cursor, uid, [('name', '=',
                                                     obj_curva.cups_name)])
            id_polissa_contador = polissa_obj.search(cursor, uid,
                                                     [('cups', '=', id_cups)])

        return cups, id_polissa_contador

    def guardar_curva(self, cursor, uid, obj_curva, id_polissa_contador,
                      valors):
        obj_polissa = self.pool.get('giscedata.polissa')
        obj_perfil = self.pool.get('giscedata.perfils.perfil')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')

        # Camps de la modificació contractual
        mc_fields = ['agree_tensio', 'agree_tarifa', 'agree_dh', 'agree_tipus',
                     'comercialitzadora']

        data_inici = obj_curva.data_inicial
        data_final = obj_curva.data_final

        polissa_id = id_polissa_contador

        # Busco els intervals de la modificació contractual
        intervals = obj_polissa.get_modcontractual_intervals(cursor, uid,
            polissa_id[0], str(data_inici), str(data_final),
            context={'ffields': mc_fields})

        mod_ids = []
        mod_dates = {}
        for mod_data in sorted(intervals.keys()):
            mod_id = intervals[mod_data]['id']
            mod_ids.append(mod_id)
            # Interval real que duren amb possibilitat de més d'una
            # modificació contractual
            mod_dates[mod_id] = intervals[mod_data]['dates']

        modvals = {}
        for modcon in modcon_obj.browse(cursor, uid, mod_ids):
            # Refresquem la pòlissa
            # polissa = self.browse(cursor, uid, modcon.polissa_id.id)
            vals = {
                'cups': modcon.cups.name,
                'distribuidora': modcon.cups.distribuidora_id.ref,
                'comercialitzadora': modcon.comercialitzadora.ref,
                'agree_tensio': modcon.agree_tensio,
                'agree_tarifa': modcon.agree_tarifa,
                'agree_dh': modcon.agree_dh,
                'agree_tipo': modcon.agree_tipus,
                'provincia': modcon.cups.id_municipi.state.ree_code,
                'mesura_ab': modcon.tarifa.mesura_ab,
            }

            modvals[modcon.id] = vals

        #Calculo el sumatori dels valors de la energia
        lectura = 0
        for lect in obj_curva.llista_valors:
            lectura += lect[1]

        # Valors de la primera modificacio contractual
        cont_mod = 0
        mod_id = mod_ids[cont_mod]
        vals_mod = modvals[mod_id]

        #Afegeixo els elements de cada linia del fitxer al dict de valors
        linia = 0
        nova_curva = None
        llista_tipus = []
        # Inicialitzo la hora anterior
        horaant = ''

        # Recorro la llista menys l'ultima linia
        for row in obj_curva.llista_valors:
            if str(row[0])[11:13] == str(24):
                # Per tenir el dia i el mes bé de l'hora 24
                ultimdia = datetime.datetime.strptime(
                    row[0][0:10], "%d-%m-%Y"
                ) - timedelta(days=1)
                ultimdiastr = ultimdia.strftime("%Y-%m-%d %H:%M")

                anyo = ultimdiastr[0:4]
                mes = ultimdiastr[5:7]
                dia = ultimdiastr[8:10]
                hora = str(24)
            else:
                anyo = str(row[0])[6:10]
                hora = str(row[0])[11:13]
                mes = str(row[0])[3:5]
                dia = str(row[0])[0:2]

            estacio = self.calcular_estacio(int(anyo), int(mes), int(dia),
                                            int(hora))

            # Per el canvi de hores tindrem dates duplicades
            if mes == '10':
                if hora == horaant:
                    #Recalculo l'estacio de la segona hora duplicada
                    estacio = self.calcular_estacio(
                        int(anyo), int(mes), int(dia), int(hora)+1)
                horaant = hora

            datacorba = '%s%s%s%s' % (anyo, mes, dia, hora)
            datacorbacomparar = '%s-%s-%s' % (anyo, mes, dia)
            if datetime.datetime.strptime(datacorbacomparar, '%Y-%m-%d') > \
                    datetime.datetime.strptime(mod_dates[mod_id][1],
                                               '%Y-%m-%d'):
                mod_id = mod_ids[cont_mod+1]
                vals_mod = modvals[mod_id]

            #No creo la corba si es de tipus inferior a 03 o si es 03
            #pero amb potencia superior a 450
            if vals_mod['agree_tipo'] < '03':
                llista_tipus.append(vals_mod['agree_tipo'])
                continue

            # Valors de cada linia
            vals = {
                'name': datacorba,
                'year': anyo,
                'month': mes,
                'day': dia,
                'hour': hora,
                'estacio': estacio,
                'lectura': lectura,
                'aprox': row[1]
            }
            #Afegeixo els valors que tenen en comú totes les curves
            vals.update(valors)
            #Afegeixo els valors de la modificacio contractual
            vals.update(vals_mod)
            #print vals
            nova_curva = obj_perfil.create(cursor, uid, vals)
            linia += 1

        return nova_curva, llista_tipus


    def importar_curva(self, cursor, uid, ids, context=None):

        obj_perfil = self.pool.get('giscedata.perfils.perfil')
        valors = {
            'magnitud': 'AE',
            'cof': 0,
            'sum_cofs': 0,
            'mesura_anual': 0,
            'mesura_mensual': 0,
            'decimals': 0,
            'real': 1
        }

        wizard = self.browse(cursor, uid, ids[0])

        #retorno el fitxer i el nom del ziv
        file_content, noms_ziv, contingut = self.get_file_content(wizard)

        #Tipus del format
        filetype = wizard.div2

        #Nom del fitxer
        filename = wizard.filename

        #Unitat selecionada
        unitat = wizard.div

        #Inicialitzo les llistes
        llista_check = []
        llista_resultats = []

        for nomarxiu in contingut.keys():
            #Cas per obtenir el nom dels fitxers ziv si estan en un zip
            if filetype == 'ziv':
                if noms_ziv:
                    nomarxiu = noms_ziv.pop(0)

            #Crear objecte Curva del fitxer
            self.obj_curva = Curva(contingut[nomarxiu], filetype, nomarxiu,
                                   unitat)

            # En el cas que el procés tingui errors, mostrar-los.
            llista_errors = self.obj_curva.llista_errors

            # Check if user allows to import incomplete curves
            allow_incomplete_curves = wizard['allow_incomplete']
            # Check if there is only an error
            only_one_error = len(llista_errors) == 1
            # Ignore when the only error is "missing profiles" and incomplete curve importation is allowed
            if llista_errors and not (
                    only_one_error and llista_errors[0].message == "Missing profile data" and allow_incomplete_curves):
                llista_check.append([nomarxiu, 'errorformat'])
                continue
            else:
                #Si el check format em dona l'error de les dates
                if 'dates' in self.obj_curva.comprova_format():
                    self.obj_curva.fix_format(filetype)

                resultat = False
                cups_data = []

                # Per el format TPL
                if filetype == 'tpl' or filetype == 'ziv':
                    #Buscar la cups
                    cups_name, id_polissa_contador = self.get_cups(
                        cursor, uid, self.obj_curva, filetype
                    )

                    cups_name = cups_name[1].encode('ascii', 'ignore')

                    if cups_name:
                        first_datetime = self.obj_curva.data_inicial
                        first_date = first_datetime.strftime('%Y%m%d')
                        one_day = timedelta(days=1)
                        last_datetime = self.obj_curva.data_final + one_day
                        last_date = last_datetime.strftime('%Y%m%d')
                        profile_ids = obj_perfil.search(
                            cursor,
                            uid,
                            [
                                ('cups', '=', cups_name),
                                ('name', '>', first_date),
                                ('name', '<', last_date)
                            ]
                        )

                        obj_perfil.unlink(cursor, uid, profile_ids)

                        #Guarda la curva
                        resultat, tipus = self.guardar_curva(
                            cursor, uid, self.obj_curva,
                            id_polissa_contador, valors)

                        #Si resultat és False és que la cups no es troba
                        # a la base de dades
                        if resultat is False:
                            llista_check.append([nomarxiu, 'false'])
                            continue
                        if tipus:
                            llista_check.append([nomarxiu, 'tipus<03'])
                            continue

                        #Afegir els resultats a la llista de resultats
                        llista_resultats.append([resultat])
                        #Actualitzar la data de la ultima lectura perfilada
                        if not self.escriure_polissa(cursor,
                                                     uid, self.obj_curva,
                                                     id_polissa_contador):
                            llista_check.append([nomarxiu, 'nohoraperfil'])
                        #Assignar com a corba el lot de perfilacio
                        setted = self.assignar_corba_polissa(cursor, uid,
                                                             cups_name,
                                                             self.obj_curva)
                        if not setted:
                            llista_check.append([nomarxiu, 'noassigncorba'])
                    else:
                        llista_check.append([nomarxiu, _("No s'ha trobat a la "
                                                         "base de dades aquest "
                                                         "comptador per les "
                                                         "dates d'alta i baixa")
                        ])

        #Tipus de unitat que utilitzarem
        uni = 'kWh'
        if unitat == 1000:
            uni = 'Wh'

        llista_elements = []
        for element in llista_resultats:
            obj_res = obj_perfil.read(cursor, uid,
                                      element[0], ["cups", "lectura"])
            llista_elements.append([obj_res["cups"],
                                    obj_res["lectura"], element[0]])

        if not llista_check:
            #El procés ha estat realitzat amb èxit.
            self.write(cursor, uid, [ids[0]], {'state': 'end', 'result':
                "\n".join([_("La curva ha estat importada amb èxit:\n"
                           "\tCUPS: %s\tEnergia Total: %s %s\t\n")
                % (ele[0], ele[1], uni) for ele in llista_elements])})

            self.write(cursor, uid, [ids[0]],
                       {'state': 'end',
                        'num_curves': "S'han importat %d curves"
                                      % len(llista_elements)})
        else:
            # Hi han hagut errors en el procés
            self.write(cursor, uid, [ids[0]], {'state': 'end', 'result':
                "\n".join([_("La curva ha estat importada amb èxit:\n"
                           "\tCUPS: %s\tEnergia Total: %s %s\t\n")
                % (ele[0], ele[1], uni) for ele in llista_elements])})
            self.write(cursor, uid,
                       [ids[0]], {'state': 'end', 'file': False,
                                  'resulterror': '\n'.join([_("La corba %s no "
                                                            "s'ha importat: %s")
                                                            % (erro[0], erro)
                                                            for erro in
                                                            llista_check])})
            self.write(cursor, uid, [ids[0]],
                       {'state': 'end',
                        'num_curves': _("S'han importat %d curves")
                                      % len(llista_elements)})


        #Borro la llista d'errors
        self.obj_curva.llista_errors[:] = []
        #I borra el fitxer
        file_content[:] = []

        return True

    #Borrar els camps de les columnes que no utilitzem
    _columns = {
        'filename': fields.char('Nom', size=128),
        'div2': fields.selection([('tpl', 'TPL'),
                                 ('ziv', 'ZIV')], "Tipus Corba", required=True),
        'perd': fields.float('Pèrdues', digits=(2, 2), required=True),
        'div': fields.selection([(1, 'kWh'),
                                 (1000, 'Wh')],
                                "Unitat",
                                required=True),
        'file': fields.binary('Fitxer', required=True),
        'compressed': fields.boolean('BZip2'),
        'p_ae': fields.boolean('Activa Entrant'),
        'p_as': fields.boolean('Activa Sortint'),
        'errors': fields.text('Errors'),
        'result': fields.text('Arrossegaments'),
        'resulterror': fields.text('Results Errors'),
        'num_curves': fields.text('Numero corbes'),
        'n_lines': fields.integer('# Línies procesades'),
        'state': fields.selection([('init', 'Init'),
                                   ('check', 'Check'),
                                   ('retry', 'Retry'),
                                   ('end', 'End'), ],
                                  'State'),
        'allow_incomplete': fields.boolean(_("Permetre corbes incompletes"),
                                     help=_('Si es marca aquesta caixa de selecció, '
                                            'les corbes es carregaran encara que els faltin perfils.')),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'allow_incomplete': lambda *a: False,
    }

ImportarCurva()
