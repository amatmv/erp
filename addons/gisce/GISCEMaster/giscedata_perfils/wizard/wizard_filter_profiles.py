# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataPerfilsFiltrar(osv.osv_memory):
    """Wizard per filtrar perfils.
    """
    _name = 'wizard.filter.profiles'

    def on_change_lot(self, cursor, uid, ids, lot, context=None):
        """Filtra totes les pòlisses del contracte_lot_perfils
        """
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if lot:
            clot_obj = self.pool.get('giscedata.perfils.contracte_lot')
            clot_ids = clot_obj.search(cursor, uid, [('lot_id', '=', lot)])
            pol_ids = []
            for pol_id in clot_obj.read(cursor, uid, clot_ids, ['polissa_id']):
                pol_ids.append(pol_id['polissa_id'][0])
            res['domain'].update({'polissa': [('id', 'in', pol_ids)]})

        return res

    def on_change_policy(self, cursor, uid, ids, lot, context=None):
        """Actualitza les dates a mostrar perfils amb les dates del lot al
        seleccionar una pòlissa.
        """
        if context is None:
            context = {}

        lot_obj = self.pool.get('giscedata.perfils.lot')
        dates = lot_obj.read(
            cursor, uid, [lot], ['data_inici', 'data_final']
        )[0]

        return {
            'value': {
                'data_inici': dates['data_inici'],
                'data_final': dates['data_final']
            }
        }

    def _default_lot(self, cursor, uid, context=None):
        """Lot de perfilació per defecte
        """
        if context is None:
            context = {}

        lot_id = context.get('active_ids', [])
        if lot_id:
            return lot_id[0]
        else:
            return False

    def filter_profiles(self, cursor, uid, ids, context=None):
        """Obre una nova pestanya amb els perfils generats filtrats per pòlissa,
        data inici i data_final
        """
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        cups_name = wizard.polissa.cups.name
        domain = [
            ('cups', '=', cups_name)
        ]
        if wizard.data_inici and wizard.data_final:
            data_inici = wizard.data_inici.replace('-', '') + '00'
            domain.append(('name', '>', data_inici))
            data_final = wizard.data_final.replace('-', '') + '24'
            domain.append(('name', '<=', data_final))

        vals_view = {
            'name': _('Perfils de {} '.format(wizard.polissa.name)),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.perfils.perfil',
            'limit': 80,
            'type': 'ir.actions.act_window',
            'domain': domain,
        }

        return vals_view

    _columns = {
        'polissa': fields.many2one('giscedata.polissa', 'Pòlissa',
                                   required=True),
        'lot': fields.many2one('giscedata.perfils.lot', 'Lot',
                               required=True, readonly=True),
        'forcar_dates': fields.boolean(
            'Forçar dates', help="Seleccionar perfils d'unes altres dates"
        ),
        'data_inici': fields.date('Data inici', required=True),
        'data_final': fields.date('Data final', required=True),
    }

    _defaults = {
        'lot': _default_lot,
    }


GiscedataPerfilsFiltrar()
