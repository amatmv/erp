# -*- coding: utf-8 -*-
from datetime import datetime
import base64

from osv import osv, fields
from tools import config


class WizardReePotagr(osv.osv_memory):
    """Assistent per generar els fitxers POTAGR.
    """
    _name = 'wizard.ree.potagr'

    def _default_period(self, cursor, uid, context=None):
        """Retornem per defecte el periode actual.
        """
        return datetime.now().strftime('%m/%Y')

    _columns = {
        'name': fields.char('Nom', size=256),
        'period': fields.char('Període (MM/YYYY)', size=7, required=True,
            help="Període que es vol generar MM/YYYY"),
        'file': fields.binary('Fitxer'),
        'state': fields.selection([('init', 'Init'), ('end', 'End')],
                                  'Estat')
    }
    _defaults = {
        'state': lambda *a: 'init',
        'period': _default_period
    }

    def export_file(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        ree_code = user.company_id.cups_code
        try:
            date = datetime.strptime(wizard.period, '%m/%Y')
        except ValueError:
            raise osv.except_osv('Error',
                                 'El format del període no és correcte')
        sql = open('%s/giscedata_perfils/sql/POTAGR.sql'
                   % config['addons_path']).read()
        cursor.execute(sql, (ree_code, date.strftime('%Y%m'),
                             date.strftime('%Y-%m-%d'),
                             date.strftime('%Y-%m-%d')))
        potagr = []
        for line in cursor.fetchall():
            potagr.append(';'.join([str(a) for a in line]) + ';')
        mfile = base64.b64encode('\n'.join(potagr))
        vals = {'name': 'POTAGR_%s_%s_%s.0' % (ree_code, date.strftime('%Y%m'),
                    datetime.now().strftime('%Y%m%d')),
                'state': 'end',
                'file': mfile
        }
        self.write(cursor, uid, ids, vals, context)

WizardReePotagr()
