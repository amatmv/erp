  # coding: utf-8
import StringIO
import csv
import sys
import datetime
from datetime import timedelta
from enerdata.datetime.timezone import TIMEZONE as TZ
import pytz


class Curva(object):

    num_contador = ''
    num_serie = 0
    versio_firm = 0
    data_proto = datetime.time(0, 0)
    versio_proto = 0
    model = ''
    fabricant = ''
    data_lectura_tpl = datetime.time(0, 0)
    data_inicial = datetime.time(0, 0)
    data_final = datetime.time(0, 0)
    tipus_curva = ''

    #llista de totes les rows
    llista_valors = []
    #reader
    readercsv = None
    #cups
    cups_name = ''
    #Errors
    llista_errors = []


    def __unicode__(self):
        return unicode(self.data_proto)

    def __init__(self, file_content, filetype, filename, unitat):

        if filetype == 'tpl':
            self.llista_valors = []
            self.llista_errors = []
            try:
                #Carrego l'arxiu
                csv_file = StringIO.StringIO(file_content)
                self.reader = csv.reader(csv_file, delimiter='\t')
                self.curves_TPL(self.reader, unitat)
            except Exception, e:
                print("Error: {0}".format(e))
                self.llista_errors.append(e)

        elif filetype == 'ziv':
            self.llista_valors = []
            self.llista_errors = []
            try:
                txt_file = StringIO.StringIO(file_content)
                csv.register_dialect('Dialecte', delimiter='\t',
                                     lineterminator='\r\n')
                self.readercsv = csv.reader(txt_file, "Dialecte")
                self.curves_ZIV(self.readercsv, filename, unitat)
            except Exception as e:
                print("Error a ziv: {0}".format(e))
                self.llista_errors.append(e)

    def curves_ZIV(self, readercsv, filename, unitat):
        try:
            data_lectura_tpl = ''
            #extreure la cups del nom de l'arxiu
            nom = filename.split("_")[0]
            self.cups_name = nom
            linia = 1
            #extreure altres dades
            for row in readercsv:
                if len(row) < 11:
                    # Fila incorrecte
                    continue
                hora = row[5].replace("\"", "")
                if linia == 1:
                    self.data_inicial = datetime.datetime.strptime(
                        row[4]+hora, '%Y%m%d%H%M') + timedelta(hours=1)

                data_lectura_tpl = datetime.datetime.strptime(row[4]+hora,
                                                              '%Y%m%d%H%M')
                #Activa i reactiva, per la unitat
                activa_entrant = int(row[6])/unitat
                reactiva = int(row[10])/unitat
                #Afegeix a la llista de valors
                self.llista_valors.append([data_lectura_tpl, activa_entrant,
                                           reactiva])
                linia += 1
            self.readercsv = readercsv
            #Poso com a data final la ultima data de lectura del tpl
            self.data_final = data_lectura_tpl - timedelta(days=1)
        except csv.Error as e:
            sys.exit('ERROR a la linia %d: %s' % (self.reader.line_num, e))

    def curves_TPL(self, readercsv, unitat):
        #Per contar les linies del fitxer
        line = 1

        try:
            for row in readercsv:
             if line == 1:
                self.num_contador = row[0]
                self.num_serie = int(row[1])
                self.versio_firm = int(row[2])
                self.data_proto = datetime.datetime.strptime(row[3],
                                                             '%Y%m%d%H%M')
                self.versio_proto = int(row[4])
                self.model = row[5]
                self.fabricant = row[6]

             if line == 2:
                self.data_lectura_tpl = datetime.datetime.strptime(row[0],
                                                                   '%Y%m%d%H%M')
                self.data_inicial = datetime.datetime.strptime(row[1],
                                                               '%Y%m%d%H%M')
                self.data_final = datetime.datetime.strptime(row[2],
                                                             '%Y%m%d%H%M')
                self.data_final = self.data_final - timedelta(days=1)
                self.tipus_curva = row[3]

             if line >= 3:
                self.llista_valors.append([
                    datetime.datetime.strptime(row[0], '%Y%m%d%H%M'),
                    int(row[1])/unitat, int(row[2]), int(row[3])/unitat,
                    int(row[4]), int(row[5])/unitat, int(row[6]),
                    int(row[7])/unitat, int(row[8]), int(row[9])/unitat,
                    int(row[10]), int(row[11])/unitat, int(row[12])])

             line+= 1

        except csv.Error as e:
            sys.exit('ERROR a la linia %d: %s' % (self.reader.line_num, e))

        start = TZ.localize(self.data_inicial-datetime.timedelta(hours=1))
        end = TZ.localize(self.data_final+datetime.timedelta(days=1))
        hours = int((end-start).total_seconds())//3600
        if len(self.llista_valors) != hours:
            raise EOFError('Missing profile data')

        entries = [value[0] for value in self.llista_valors]
        seen = []
        duplicates = []
        for entry in entries:
            if entry in seen:
                duplicated = True
                # Only accept one duplicated entry for DST boundaries
                entry_utc = TZ.localize(entry).astimezone(pytz.utc)
                entry_utc_without_offset = entry_utc.replace(tzinfo=None)
                if entry_utc_without_offset in TZ._utc_transition_times:
                    if entry not in duplicates:
                        duplicated = False
                    duplicates.append(entry)
                if duplicated:
                    raise KeyError(
                        'Profile with duplicated entries, duplicated entry: {}'
                        .format(str(entry))
                    )
            seen.append(entry)

    def comprovar_cont_i_num_serie(self):
        #Verificar que el número de contador i de serie concideixen
        iguals = False
        if self.num_contador == self.num_serie:
            iguals = True

        return iguals

    def comprova_format(self):
        errors = []

        #Comprovar primera linia
        if (self.num_contador or self.num_serie or self.versio_firm or
                self.data_proto or self.versio_proto or self.model or
                self.fabricant) is None:
            errors.append("primera")

        #Comprovar dates
        try:
            datetime.datetime.strptime(str(self.data_proto), '%d-%m-%Y  %H:00')
            datetime.datetime.strptime(str(self.data_lectura_tpl),
                                       '%d-%m-%Y    %H:00')
            datetime.datetime.strptime(str(self.data_inicial),
                                       '%d-%m-%Y    %H:00')
            datetime.datetime.strptime(str(self.data_final), '%d-%m-%Y  %H:00')
            for row in self.llista_valors:
                datetime.datetime.strptime(str(row[0]), '%d-%m-%Y   %H:00')
        except Exception, e:
            errors.append("dates")

        #Comprovar que siguin digits
        try:
            int(self.versio_firm)
            int(self.versio_proto)
            #Els elements a partir de la linia 2
            for row in self.llista_valors:
                int(row[1])
                int(row[2])
                int(row[3])
                int(row[4])
                int(row[5])
                int(row[6])
                int(row[7])
                int(row[8])
                int(row[9])
                int(row[10])
                int(row[11])
                int(row[12])
        except:
            errors.append("digits")

        #Comprovar tipus
        tipus = ['1A', '1I', '2A', '2I']
        try:
            self.tipus_curva in tipus
        except:
            errors.append("tipus")


        return errors

    def fix_format_ZIV(self):
        for row in self.llista_valors:
            #Sumem 1 a l'hora per coincidir amb el format que fan servir a RE
            datanova = row[0] + timedelta(hours=1)
            if str(datanova.hour) == '0':
                row[0] = datetime.datetime.strftime(datanova, '%d-%m-%Y 24:%M')
            else:
                row[0] = datetime.datetime.strftime(datanova, '%d-%m-%Y %H:%M')

    #canviar el format de les dates del fitxer curves del tpl
    def fix_format_TPL(self):

        #per el format %YYYY/MM/DD  HH00
        datanova = '%s-%s-%s %s:00' % ((str(self.data_proto)[8:10]),
                                           (str(self.data_proto)[5:7]),
                                           (str(self.data_proto)[0:4]),
                                           (str(self.data_proto)[11:13]),)

        self.data_proto = datetime.datetime.strptime(datanova, '%d-%m-%Y %H:%M')

        datanova = '%s-%s-%s %s:00' % ((str(self.data_lectura_tpl)[8:10]),
                                           (str(self.data_lectura_tpl)[5:7]),
                                           (str(self.data_lectura_tpl)[0:4]),
                                           (str(self.data_proto)[11:13]),)

        self.data_lectura_tpl = datetime.datetime.strptime(datanova,
                                                           '%d-%m-%Y %H:%M')

        datanova = '%s-%s-%s %s:00' % ((str(self.data_inicial)[8:10]),
                                           (str(self.data_inicial)[5:7]),
                                           (str(self.data_inicial)[0:4]),
                                           (str(self.data_proto)[11:13]),)

        self.data_inicial = datetime.datetime.strptime(datanova,
                                                       '%d-%m-%Y %H:%M')

        datanova = '%s-%s-%s %s:00' % ((str(self.data_final)[8:10]),
                                           (str(self.data_final)[5:7]),
                                           (str(self.data_final)[0:4]),
                                           (str(self.data_proto)[11:13]),)

        self.data_final = datetime.datetime.strptime(datanova, '%d-%m-%Y %H:%M')


        for row in self.llista_valors:
            #Canviem l'hora per coincidir amb el format que fan servir a RE
            hora = str(row[0])[11:13]
            if hora == '00':
                hora = '24'

            datanova = '%s-%s-%s %s:00' % ((str(row[0])[8:10]),
                                           (str(row[0])[5:7]),
                                           (str(row[0])[0:4]),
                                           hora,)

            row[0] = datanova

        return "El format ha estat canviat a %d-%m-%Y %s:%M"

    def fix_format(self, filetype):
        if filetype == 'tpl':
            self.fix_format_TPL()
        if filetype == 'ziv':
            self.fix_format_ZIV()