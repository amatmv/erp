# -*- coding: utf-8 -*-
from osv import osv, fields
import base64
from datetime import datetime, timedelta

MONITOR_FIELDS = [
    'titular_nif',
    'comercialitzadora',
    'agree_tipus',
    'agree_tensio',
    'agree_tarifa',
    'agree_dh',
    'tensio_normalitzada',
    'potencia',
    'cnae'
]

POINT_TYPES = ['01', '02']


def cups_formatted_last_date_to_add(date):
    """
    Format the date for REE CUPS file.

    :param date: string with the date formatted like "%Y-%m-%d"
    :return: string with the timestamp of the end of the day formatted like \
        "%Y/%m/%d 00:00:00"
    """

    last_date = datetime.strptime(
        date,
        '%Y-%m-%d'
    )
    last_date += timedelta(days=1)
    last_date_cups_formatted = last_date.strftime(
        '%Y/%m/%d 00:00:00'
    )

    return last_date_cups_formatted


class GiscedataREECUPSWizard(osv.osv_memory):
    """
    REE CUPS file generation Wizard.
    """
    _name = 'giscedata.ree.cups.wizard'

    def cups_formatted_fist_date_to_remove(
            self,
            cursor,
            uid,
            policy_id,
            date,
            context=None
    ):
        """
        Get the formatted start date of the last policy amendment with \
        relevant changes for REE CUPS file.

        :param cursor: database cursor
        :param uid: user identifier
        :param policy_id: policy identifier
        :param date: string with the remove date formated like "%Y-%m-%d"
        :param context: dictionary with the context
        :return: string with the timestamp formatted like "%Y/%m/%d 00:00:00"
        """
        if context is None:
            context = {}
        giscedata_polissa = self.pool.get('giscedata.polissa')

        start_date = giscedata_polissa.read(
            cursor,
            uid,
            [policy_id],
            ['data_alta'],
            context=context
        )[0]['data_alta']

        if context:
            context.update({'ffields': MONITOR_FIELDS})
        else:
            context = {'ffields': MONITOR_FIELDS}

        old_intervals = giscedata_polissa.get_modcontractual_intervals(
            cursor,
            uid,
            policy_id,
            start_date,
            date,
            context=context
        )
        if date in old_intervals:
            old_intervals.pop(date)
        old_date = max(old_intervals.keys())

        first_date_cups_formatted = '{} 00:00:00'.format(
            old_date.replace('-', '/')
        )

        return first_date_cups_formatted

    def build_cups(self, cursor, uid, ids, context=None):
        """
        Generate CUPS REE file

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: wizard identifier
        :param context: dictionary with the context
        :return: None
        """
        giscedata_polissa = self.pool.get('giscedata.polissa')
        giscedata_polissa_modcontractual = \
            self.pool.get('giscedata.polissa.modcontractual')
        wizard = self.browse(cursor, uid, ids[0], context)
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        ree_code = user.company_id.partner_id.ref

        if context is None:
            context = {'active_test': False}
        else:
            context.update({'active_test': False})

        lines = []

        context_intervals = context.copy()
        context_intervals.update({'ffields': MONITOR_FIELDS})
        policy_amendment_ids = giscedata_polissa_modcontractual.search(
            cursor,
            uid,
            [
                ('data_inici', '>=', wizard.start_date),
                ('data_inici', '<=', wizard.end_date)
            ],
            context=context
        )
        policy_amendments = giscedata_polissa_modcontractual.read(
            cursor,
            uid,
            policy_amendment_ids,
            ['polissa_id'],
            context=context
        )
        policy_ids = set([a['polissa_id'][0] for a in policy_amendments])
        for policy_id in policy_ids:
            intervals = giscedata_polissa.get_modcontractual_intervals(
                cursor,
                uid,
                policy_id,
                wizard.start_date,
                wizard.end_date,
                context=context_intervals
            )
            for date in sorted(intervals):
                policy_amendment = giscedata_polissa_modcontractual.browse(
                    cursor,
                    uid,
                    intervals[date]['id'],
                    context=context
                )

                if policy_amendment.tarifa.name in ['RE', 'RE2']:
                    continue

                if intervals[date]['changes']:

                    last_amendment = policy_amendment.modcontractual_ant

                    in_current = policy_amendment.agree_tipus in POINT_TYPES
                    in_last = last_amendment.agree_tipus in POINT_TYPES
                    if not in_current and not in_last:
                        continue
                    elif in_current and in_last:
                        action = 'M'
                    elif in_current and not in_last:
                        action = 'A'
                    elif not in_current and in_last:
                        action = 'B'

                        first_date_cups_formatted = \
                            self.cups_formatted_fist_date_to_remove(
                                cursor,
                                uid,
                                policy_id,
                                date,
                                context=context
                            )

                        last_date_cups_formatted = \
                            cups_formatted_last_date_to_add(
                                last_amendment.data_final
                            )

                        report_policy_amendment = last_amendment

                    if action != 'B':
                        last_date_cups_formatted = '3000/01/01 00:00:00'

                        first_date_cups_formatted = '{} 00:00:00'.format(
                            intervals[date]['dates'][0].replace('-', '/')
                        )

                        report_policy_amendment = policy_amendment

                else:
                    if (
                        policy_amendment.agree_tipus in POINT_TYPES
                    ) and (
                        date >= wizard.start_date
                    ) and (
                        date <= wizard.end_date
                    ):
                        action = 'A'
                        first_date_cups_formatted = "{} 00:00:00".format(
                            date.replace('-', '/')
                        )
                        last_date_cups_formatted = ''

                        report_policy_amendment = policy_amendment
                    else:
                        continue

                lines.append(
                    [
                        report_policy_amendment,
                        action,
                        first_date_cups_formatted,
                        last_date_cups_formatted
                    ]
                )

        action = 'B'
        policy_ids = giscedata_polissa.search(
            cursor,
            uid,
            [
                ('data_baixa', '>=', wizard.start_date),
                ('data_baixa', '<=', wizard.end_date),
                ('agree_tipus', 'in', POINT_TYPES)
            ],
            context=context
        )
        for policy in giscedata_polissa.browse(
                cursor,
                uid,
                policy_ids,
                context=context
        ):
            last_date_cups_formatted = cups_formatted_last_date_to_add(
                policy.data_baixa
            )

            first_date_cups_formatted = \
                self.cups_formatted_fist_date_to_remove(
                    cursor,
                    uid,
                    policy.id,
                    policy.data_baixa,
                    context=context
                )

            lines.append(
                [
                    policy.modcontractual_activa,
                    action,
                    first_date_cups_formatted,
                    last_date_cups_formatted
                ]
            )

        cups = []
        for line in lines:
            policy_amendment = line[0]
            action = line[1]
            first_date_cups_formatted = line[2]
            last_date_cups_formatted = line[3]
            meter_q = meter_obj.q(cursor, uid)
            meter = meter_q.read(['lloguer']).where([
                ('name', '=', policy_amendment.comptador)
            ])
            data = [
                action,
                '',
                policy_amendment.cups.name,
                policy_amendment.polissa_id.titular.name,
                policy_amendment.polissa_id.titular_nif[2:11],
                ree_code,
                policy_amendment.comercialitzadora.ref,
                policy_amendment.agree_tipus,
                policy_amendment.agree_tensio,
                policy_amendment.agree_tarifa,
                policy_amendment.agree_dh,
                policy_amendment.tensio_normalitzada.tipus[0],
                policy_amendment.cups.id_provincia.ree_code,
                int(policy_amendment.potencia),
                policy_amendment.cnae.name.replace('.', ''),
                first_date_cups_formatted,
                last_date_cups_formatted,
                policy_amendment.cups.dp if policy_amendment.cups.dp else '',
                'S' if meter[0]['lloguer'] else 'N',
                (policy_amendment.tensio_normalitzada
                 and policy_amendment.tensio_normalitzada.cnmc_code),
                '',
            ]
            cups.append(';'.join(list(map(str, data))))
        cups_file_content = '\n'.join(cups)
        vals = {
            'state': 'end',
            'name': 'CUPS_%s_%s.0' % (
                ree_code,
                datetime.now().strftime('%Y%m%d')
            ),
            'file': base64.b64encode(cups_file_content)
        }
        self.write(cursor, uid, ids, vals, context)

    _columns = {
        'state': fields.char('Estat', size=10),
        'start_date': fields.date('Data inicial'),
        'end_date': fields.date('Data final'),
        'file': fields.binary('CUPS'),
        'name': fields.char('Nom fitxer', size=256)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'start_date': lambda *a: (
            datetime.today() - timedelta(days=4)
        ).strftime('%Y-%m-%d'),
        'end_date': lambda *a: datetime.today().strftime('%Y-%m-%d')
    }

GiscedataREECUPSWizard()
