# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO
import base64
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import zipfile
import calendar
import bz2


class GiscedataREECUPS5Wizard(osv.osv_memory):
    """CUPS5 REE generation Wizard.
    """
    _name = 'giscedata.ree.cups5.wizard'

    def get_cups_data(self, cursor, uid, cups_id):
        sql = '''SELECT cups.name, distri.ref as distri, prov.ree_code as state,
                 cups.dp as cp
                 FROM giscedata_cups_ps cups
                 LEFT JOIN res_partner distri ON distri.id=cups.distribuidora_id
                 LEFT JOIN res_municipi municipi
                    ON municipi.id=cups.id_municipi
                 LEFT JOIN res_country_state prov ON prov.id=municipi.state
                 WHERE cups.id=%s'''
        cursor.execute(sql, (cups_id,))
        res = cursor.fetchall()
        return res[0]

    def get_cups5(self, cursor, uid, ids, context=None):
        """REE CUPS5"""
        wiz = self.browse(cursor, uid, ids[0], context)
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        ree_code = user.company_id.partner_id.ref

        year_month = '{0}-{1}'.format(wiz.year, str(wiz.month).zfill(2))
        start_date_str = '{0}-01'.format(year_month)
        start_date = datetime.strptime(start_date_str, '%Y-%m-%d')
        last_day = calendar.monthrange(wiz.year, wiz.month)[1]
        end_date_str = '{0}-{1}'.format(year_month,last_day)

        cups34 = {}
        cups5 = {}

        # By code
        lines_erp = []
        pol_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        partner_obj = self.pool.get('res.partner')
        cnae_obj = self.pool.get('giscemisc.cnae')

        search_vals = [('data_alta', '<=', start_date_str),
                       '|', ('data_baixa', '<=', end_date_str),
                            ('data_baixa', '=', False)
                       ]
        pol_ids = pol_obj.search(cursor, uid, search_vals,
                                 context={'active_test': False})

        monitor_fields = ['comercialitzadora', 'agree_tipus', 'agree_tensio',
                          'agree_tarifa', 'agree_dh', 'potencia', 'tg']
        for pol_id in pol_ids:
            intervals = pol_obj.get_modcontractual_intervals(
                cursor, uid, pol_id, start_date_str, end_date_str,
                context={'ffields': monitor_fields}
            )
            for interval in intervals.values():
                modcon_vals = modcon_obj.read(
                    cursor,
                    uid,
                    interval['id'],
                    [
                        'cups',
                        'comercialitzadora',
                        'agree_tipus',
                        'agree_tensio',
                        'agree_tarifa',
                        'agree_dh',
                        'potencia',
                        'tg',
                        'modcontractual_ant',
                        'tarifa',
                        'cnae'
                    ],
                    context=context
                )
                if modcon_vals['agree_tipus'] not in ['03', '04', '05']:
                    continue
                if modcon_vals['tarifa'][1] in ['RE', 'RE2']:
                    continue
                cups_id = modcon_vals['cups'][0]
                cups_vals = self.get_cups_data(cursor, uid, cups_id)
                comer_vals = False
                if modcon_vals['comercialitzadora']:
                    comer_vals = partner_obj.read(
                        cursor, uid, modcon_vals['comercialitzadora'][0],
                        ['ref']
                    )

                cnae_code = False
                if modcon_vals['cnae']:
                    cnae_code = cnae_obj.read(
                        cursor, uid, modcon_vals['cnae'][0], ['name']
                    )['name']

                cups_name = cups_vals[0]
                cups = len(cups_name) == 20 and cups_name + '0F' or cups_name

                interval_last_date = datetime.strptime(
                    interval['dates'][1],
                    '%Y-%m-%d'
                )
                period_last_date = datetime.strptime(
                    end_date_str,
                    '%Y-%m-%d'
                )
                last_date = min(interval_last_date, period_last_date)
                next_last_date = last_date + relativedelta(days=1)
                last_date_cups5_formated = next_last_date.strftime(
                    '%Y/%m/%d 00'
                )

                if modcon_vals['agree_tipus'] in ['03','04']:
                    tg = ''
                else:
                    if modcon_vals.get('tg', '2') == '1':
                        tg = 'S'
                    else:
                        tg = 'N'

                lines_erp.append(
                    [
                        cups,
                        cups_vals[1],
                        comer_vals.get('ref', ''),
                        modcon_vals['agree_tipus'][1],
                        modcon_vals['agree_tensio'],
                        modcon_vals['agree_tarifa'],
                        modcon_vals['agree_dh'],
                        cups_vals[2],
                        modcon_vals['potencia'],
                        tg,
                        max(datetime.strptime(interval['dates'][0], '%Y-%m-%d'),
                            start_date).strftime('%Y/%m/%d 00'),
                        last_date_cups5_formated,
                        cnae_code if cnae_code else '',
                        cups_vals[3] if cups_vals and cups_vals[3] else '',
                        '',
                    ]
                )

                if \
                    interval['dates'][0] == start_date_str \
                    and \
                    modcon_vals['modcontractual_ant']:

                    last_policy_amendment = modcon_obj.read(
                        cursor,
                        uid,
                        modcon_vals['modcontractual_ant'][0],
                        [
                            'comercialitzadora',
                            'agree_tipus',
                            'agree_tensio',
                            'agree_tarifa',
                            'agree_dh',
                            'potencia',
                            'tg'
                        ],
                        context=context
                    )
                    if \
                        modcon_vals['comercialitzadora'] \
                        != \
                        last_policy_amendment['comercialitzadora']:

                        last_comer = partner_obj.read(
                            cursor,
                            uid,
                            last_policy_amendment['comercialitzadora'][0],
                            ['ref']
                        )

                        if last_policy_amendment['agree_tipus'] in ['03','04']:
                            tg = ''
                        else:
                            if last_policy_amendment.get('tg', '2') == '1':
                                tg = 'S'
                            else:
                                tg = 'N'

                        lines_erp.append(
                            [
                                cups,
                                cups_vals[1],
                                last_comer.get('ref', ''),
                                last_policy_amendment['agree_tipus'][1],
                                last_policy_amendment['agree_tensio'],
                                last_policy_amendment['agree_tarifa'],
                                last_policy_amendment['agree_dh'],
                                cups_vals[2],
                                last_policy_amendment['potencia'],
                                tg,
                                start_date_str.replace('-','/') + " 00",
                                start_date_str.replace('-','/') + " 00",
                                cnae_code if cnae_code else '',
                                cups_vals[3] if cups_vals and cups_vals[3] else '',
                                '',
                            ]
                        )

        for line in lines_erp:
            comer = line[2]
            if line[3] in ['3','4']:
                cups34or5 = cups34
            elif line[3] == '5':
                cups34or5 = cups5
            if comer not in cups34or5:
                cups34or5.update({comer: []})
            cups34or5[comer].append(';'.join([str(a) for a in line]))

        compress_bz2 = self.read(
            cursor, uid, ids, ['compression_type'], context=context
        )[0]['compression_type']

        the_zip_io = StringIO.StringIO()
        the_zip = zipfile.ZipFile(the_zip_io, 'w',
                                  compression=zipfile.ZIP_DEFLATED)

        pattern_name = '%s_%s_%s_%04d%02d_%s.0'
        if compress_bz2:
            pattern_name = '%s_%s_%s_%04d%02d_%s.0.bz2'

        for cups34or5 in ('CUPS34', cups34), ('CUPS5', cups5):
            for comer in cups34or5[1].keys():
                fname = pattern_name % (
                    cups34or5[0],
                    ree_code,
                    comer,
                    int(wiz.year),
                    int(wiz.month),
                    datetime.now().strftime('%Y%m%d')
                )
                write_data = '\n'.join(cups34or5[1][comer])

                if compress_bz2:
                    write_data = bz2.compress(write_data)

                the_zip.writestr(fname, write_data)
        the_zip.close()
        vals = {
            'state': 'end',
            'name': 'CUPS34_CUPS5_%s_%04d%02d_%s.zip' % (
                ree_code,
                wiz.year,
                wiz.month,
                datetime.now().strftime('%Y%m%d')
            ),
            'file': base64.b64encode(the_zip_io.getvalue())
        }
        the_zip_io.close()
        self.write(cursor, uid, ids, vals, context)

    _columns = {
        'state': fields.char('State', size=10),
        'year': fields.integer('Any'),
        'month': fields.integer('Mes'),
        'file': fields.binary('CUPS34 i CUPS5'),
        'name': fields.char('Nom fitxer', size=256),
        'compression_type': fields.boolean('BZ2')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'year': lambda *a: (datetime.today() - timedelta(days=4)).year,
        'month': lambda *a: (datetime.today() - timedelta(days=4)).month,
        'compression_type': lambda *a: True
    }

GiscedataREECUPS5Wizard()
