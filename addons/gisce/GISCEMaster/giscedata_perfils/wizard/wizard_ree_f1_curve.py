# -*- coding: utf-8 -*-
import zipfile
import base64
import csv
import time
import StringIO
from tools.translate import _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from osv import osv, fields


class WizardReeF1Curve(osv.osv_memory):
    """Assistent per convertir curves a fitxers F1.
    """
    _name = 'wizard.ree.f1.curve'
    _files = []
    _cups = []
    _date = []
    _info = ''

    def create(self, cursor, uid, ids, context=None):
        self.clear(cursor, uid, ids)
        return super(WizardReeF1Curve,
                     self).create(cursor, uid, ids, context=context)

    def _default_r1(self, cursor, uid, context=None):
        ''' Gets R1 code from company if defined '''
        if not context:
            context = {}

        company_obj = self.pool.get('res.company')
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref

        return str(r1)

    def _default_info(self, cursor, uid, context=None):
        informacio = _('Aquest assistent generarà un '
                       'fitxer F1 a partir d\'un fitxer de corba.')
        return informacio

    def get_meter(self, cursor, uid, cups, data_mes, context=None):
        '''Gets meter serial number from CUPS'''
        # data_mes = '201403'
        # cups = 'ES0174000000026026YK0F'
        # contador ha de ser 97599353
        data_ini = data_mes[0:4]+'-'+data_mes[4:6]+'-'+'01 01:00:00'
        data_ini = datetime.strptime(data_ini, '%Y-%m-%d %H:%M:%S')
        data_fi = data_ini+relativedelta(months=+1)
        data_fi = data_fi+relativedelta(hours=-1)
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        comptador_name = _('No trobat')
        cups_id = cups_obj.search(
            cursor, uid, [('name', '=', cups)],
            context={'active_test': False})
        if cups_id:
            dades_cups = cups_obj.read(cursor, uid, cups_id[0], ['id'])
            polissa_id = polissa_obj.search(
                cursor, uid, [('cups', '=', dades_cups['id'])])
            if polissa_id:
                comptador_id = comptador_obj.search(
                    cursor, uid, [('polissa', '=', polissa_id[0]),
                                  ('data_alta', '<=',
                                   data_ini.strftime('%Y-%m-%d %H:%M:%S')),
                                  '|',
                                  ('data_baixa', '>=',
                                   data_fi.strftime('%Y-%m-%d %H:%M:%S')),
                                  ('data_baixa', '=', False)],
                    context={'active_test': False})
                if comptador_id:
                    comptador_name = comptador_obj.read(cursor, uid,
                                                        comptador_id[0],
                                                        ['name'])['name']
        return comptador_name

    def get_cups(self, cursor, uid, comptador, data_mes, context=None):
        '''Gets CUPS from the meter serial number.'''
        data_ini = data_mes[0:4]+'-'+data_mes[4:6]+'-'+'01'
        data_ini = datetime.strptime(data_ini, '%Y-%m-%d')
        data_fi = data_ini+relativedelta(months=+1)
        cups_str = 'ES00000000000000000000'
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        comptador_id = comptador_obj.search(
            cursor, uid, [('name', '=', comptador),
                          ('data_alta', '<=', data_ini.strftime('%Y-%m-%d')),
                          '|',
                          ('data_baixa', '>=', data_fi.strftime('%Y-%m-%d')),
                          ('data_baixa', '=', False)],
            context={'active_test': False})
        if comptador_id:
            dades_comptador = comptador_obj.read(cursor, uid,
                                                 comptador_id[0], ['polissa'])

            polissa_id = polissa_obj.search(
                cursor, uid, [('id', '=', dades_comptador['polissa'][0])])
            if polissa_id:
                cups_id = polissa_obj.read(cursor, uid, polissa_id[0],
                                           ['cups'])
                cups_str = cups_obj.read(
                    cursor, uid, cups_id['cups'][0], ['name'])['name']

        return cups_str

    def merge_f1(self, cursor, uid, ids, files, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        contador = 0
        llista_fitxers = []
        llista_noms = []
        i = 0
        while i < len(files):
            arxiu = zipfile.ZipFile(files[i])
            for name in arxiu.namelist():
                uncompressed = arxiu.read(name)
                file = StringIO.StringIO(uncompressed)
                if i == 0:
                    file_sortida = StringIO.StringIO()
                else:
                    file_sortida = llista_fitxers[contador]
                for linia in file:
                    file_sortida.write(linia)
                if i == 0:
                    llista_noms.append(name)
                    llista_fitxers.append(file_sortida)
                contador += 1
            i += 1
            contador = 0
        nom_zip = str(wizard.filename_f1)
        contador = 0
        zip_io = StringIO.StringIO()
        zip_writer = zipfile.ZipFile(zip_io, 'w',
                                     compression=zipfile.ZIP_DEFLATED)
        for elem in llista_noms:
            zip_writer.writestr(elem, llista_fitxers[contador].getvalue())
            contador += 1
        zip_writer.close()
        return zip_io

    def checker(self, cursor, uid, ids, zf_io, comptador, cups, data_mes,
                context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        txt = base64.decodestring(str(wizard.fileCurve))
        csv_file = StringIO.StringIO(txt)
        corba_reader = csv.reader(csv_file, delimiter='\t')
        total_activa_entrant = 0
        total_activa_sortint = 0
        total_reactiva_q1 = 0
        total_reactiva_q2 = 0
        total_reactiva_q3 = 0
        total_reactiva_q4 = 0
        total_activa_entrantF1 = 0
        total_activa_sortintF1 = 0
        total_reactiva_q1_f1 = 0
        total_reactiva_q2_f1 = 0
        total_reactiva_q3_f1 = 0
        total_reactiva_q4_f1 = 0
        headers = 0
        comptador_trobat = False
        for linia in corba_reader:
            if len(linia) >= 4:
                if len(linia) == 25:
                    if not comptador_trobat:
                        comptador = self.get_meter(cursor, uid, cups, data_mes,
                                                   context)
                        comptador_trobat = True
                    total_activa_entrant += int(linia[6])
                    total_activa_sortint += int(linia[8])
                    total_reactiva_q1 += int(linia[10])
                    total_reactiva_q2 += int(linia[12])
                    total_reactiva_q3 += int(linia[14])
                    total_reactiva_q4 += int(linia[16])
                else:
                    if headers == 2:
                        total_activa_entrant += int(linia[1])
                        total_activa_sortint += int(linia[3])
                        total_reactiva_q1 += int(linia[5])
                        total_reactiva_q2 += int(linia[7])
                        total_reactiva_q3 += int(linia[9])
                        total_reactiva_q4 += int(linia[11])
                    else:
                        headers += 1

        fitxer_zip = zipfile.ZipFile(zf_io)
        for name in fitxer_zip.namelist():
            uncompressed = fitxer_zip.read(name)
            csv_file = StringIO.StringIO(uncompressed)
            csv_reader = csv.reader(csv_file, delimiter=';')
            for linia in csv_reader:
                total_activa_entrantF1 += int(linia[4])
                total_activa_sortintF1 += int(linia[5])
                total_reactiva_q1_f1 += int(linia[6])
                total_reactiva_q2_f1 += int(linia[7])
                total_reactiva_q3_f1 += int(linia[8])
                total_reactiva_q4_f1 += int(linia[9])
        if total_activa_entrant ==\
                total_activa_entrantF1 and total_activa_sortint\
                == total_activa_sortintF1 and total_reactiva_q1\
                == total_reactiva_q1_f1 and total_reactiva_q2\
                == total_reactiva_q2_f1 and total_reactiva_q3\
                == total_reactiva_q3_f1 and total_reactiva_q4\
                == total_reactiva_q4_f1:
            num_cups_actual = int(wizard.numCupsActual)
            num_cups_actual += 1
            linia = _(u'Fitxer número %s\n') % num_cups_actual
            linia += u'================\n\n'
            self._info += linia
            self._info += _(
                'Fitxers F1 generats correctament.\n\n')+'CUPS: '+str(
                cups)+'\n'+_('Comptador: ')+str(comptador)+'\n\n'+_(
                'Fitxer corba --> Fitxers F1\n')+_(
                'Total activa entrant: ')+str(
                total_activa_entrant)+' --> '+str(
                total_activa_entrantF1)+'\n'+_(
                'Total activa sortint: ')+str(
                total_activa_sortint)+' --> '+str(
                total_activa_sortintF1)+'\n'+'Total reactiva Q1: '+str(
                total_reactiva_q1)+' --> '+str(
                total_reactiva_q1_f1)+'\n'+'Total reactiva Q2: '+str(
                total_reactiva_q2)+' --> '+str(
                total_reactiva_q2_f1)+'\n'+'Total reactiva Q3: '+str(
                total_reactiva_q3)+' --> '+str(
                total_reactiva_q3_f1)+'\n'+'Total reactiva Q4: '+str(
                total_reactiva_q4)+' --> '+str(
                total_reactiva_q4_f1)+'\n\n'
            vals = {
                'numCupsActual': str(num_cups_actual)
            }
            self.write(cursor, uid, ids, vals, context)
            return True
        else:
            self._info += 'ERROR.\n\n'+'CUPS: '+str(
                cups)+'\n'+_('Comptador: ')+str(comptador)+'\n\n'+_(
                'Fitxer corba --> Fitxers F1\n')+_(
                'Total activa entrant: ')+str(
                total_activa_entrant)+' --> '+str(
                total_activa_entrantF1)+'\n'+_(
                'Total activa sortint: ')+str(
                total_activa_sortint)+' --> '+str(
                total_activa_sortintF1)+'\n'+'Total reactiva Q1: '+str(
                total_reactiva_q1)+' --> '+str(
                total_reactiva_q1_f1)+'\n'+'Total reactiva Q2: '+str(
                total_reactiva_q2)+' --> '+str(
                total_reactiva_q2_f1)+'\n'+'Total reactiva Q3: '+str(
                total_reactiva_q3)+' --> '+str(
                total_reactiva_q3_f1)+'\n'+'Total reactiva Q4: '+str(
                total_reactiva_q4)+' --> '+str(
                total_reactiva_q4_f1)+'\n\n'
            return False

    def generate(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        if not wizard.r1:
            vals = {
                'info': _('Siusplau, entri un codi R1.')
            }
            self.write(cursor, uid, ids, vals, context)
        elif not wizard.fileCurve:
            vals = {
                'info': _('Siusplau, seleccioni un fitxer de corba.')
            }
            self.write(cursor, uid, ids, vals, context)
        elif int(wizard.numCups) <= 0:
            vals = {
                'info': _('El número de CUPS ha de ser positiu i més gran que '
                          'zero.')
            }
            self.write(cursor, uid, ids, vals, context)
        elif int(wizard.versio) <= 0:
            vals = {
                'info': _('El número de versió ha de ser positiu i més gran'
                          ' que zero.')
            }
            self.write(cursor, uid, ids, vals, context)
        elif not wizard.numCups:
            vals = {
                'info': _('Siusplau, entri un número de CUPS.')
            }
            self.write(cursor, uid, ids, vals, context)
        elif not wizard.versio:
            vals = {
                'info': _('Siusplau, entri un número de versió.')
            }
            self.write(cursor, uid, ids, vals, context)
        else:
            if wizard.state == 'init':
                vals = {
                    'state': 'on'
                }
                self.write(cursor, uid, ids, vals, context)
            wizard = self.browse(cursor, uid, ids[0], context)
            num_cups = int(wizard.numCups)
            num_cups_actual = int(wizard.numCupsActual)
            num_cups_actual += 1
            vals = {
                'info': _(u'Fitxer número %s generat.') % num_cups_actual
            }
            self.write(cursor, uid, ids, vals, context)
            # generate single
            res = self.generate_single(cursor, uid, ids, context)
            if res:
                self._files.append(res)
            vals = {
                'fileCurve': False
            }
            self.write(cursor, uid, ids, vals, context)
            wizard = self.browse(cursor, uid, ids[0], context)
            num_cups_actual = int(wizard.numCupsActual)
            if num_cups <= num_cups_actual:
                # merge
                fitxer_final = self.merge_f1(cursor, uid, ids, self._files)
                data = base64.b64encode(fitxer_final.getvalue())
                wizard.write({'fileF1': data})
                vals = {
                    'state': 'end',
                    'info': self._info
                }
                self.write(cursor, uid, ids, vals, context)
                self.clear(cursor, uid, ids)

    def comprovar_cups_repetits(self, cursor, uid, cups, context=None):
        for elem in self._cups:
            if elem == cups:
                return True
        return False

    def comprovar_dates_diferents(self, cursor, uid, data_mes, context=None):
        for elem in self._date:
            if elem != data_mes:
                return True
        return False

    def generate_single(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        cups = ''
        fitxer_llegit = False
        txt = base64.decodestring(str(wizard.fileCurve))
        csv_file = StringIO.StringIO(txt)
        reader = csv.reader(csv_file, delimiter='\t')
        primera_capcalera = False
        segona_capcalera = False
        nom_fitxer_generat = False
        zf_io = StringIO.StringIO()
        missatge_error = ''
        zip_writer = zipfile.ZipFile(zf_io, 'w',
                                     compression=zipfile.ZIP_DEFLATED)
        try:
            for linia in reader:
                if cups == 'ES00000000000000000000':
                    missatge_error = (_(u"No s'ha pogut trobar el CUPS"
                                        u" per el comptador %s.")
                                      % numero_contador)
                    break
                elif self.comprovar_cups_repetits(cursor, uid, cups):
                    missatge_error = (_(u"Ja s'ha generat un fitxer F1 amb el"
                                        u" CUPS %s.") % cups)
                    break
                elif len(linia) == 4 or len(linia) == 7 \
                        or len(linia) == 25 or len(linia) == 13:
                    fitxer_llegit = True
                    if len(linia) == 25:
                        primera_capcalera = True
                        segona_capcalera = True
                        numero_contador = ''
                    if not primera_capcalera and len(linia) == 7:
                        # Llegim la primera capcalera
                        numero_contador = linia[0]
                        numero_serie = linia[1]
                        versio_firmware = linia[2]
                        data_protocol = linia[3]
                        versio_protocol = linia[4]
                        model = linia[5]
                        fabricant = linia[6]
                        primera_capcalera = True
                    elif not segona_capcalera and len(linia) == 4:
                        # Llegim la segona capcalera
                        data_lectura = linia[0]
                        data_inicial = linia[1]
                        data_final = linia[2]
                        tipus_corba = linia[3]
                        segona_capcalera = True
                    elif primera_capcalera and segona_capcalera:
                        if not nom_fitxer_generat:
                            if len(linia) == 25:
                                data_interval = linia[4]
                            else:
                                data_interval = linia[0]
                            data_mes = data_interval[0:6]
                            if self.comprovar_dates_diferents(cursor, uid,
                                                              data_mes):
                                missatge_error = _(u"El fitxer de corba entrat"
                                                   u" no té la mateixa data"
                                                   u" que el/s anterior/s.")
                                break
                            nom_fitxer_zip = 'F1_'+str(
                                wizard.r1)+'_'+data_mes+'_'+str(
                                time.strftime("%Y%m%d"))+'.zip'
                            hora = 0
                            mes = data_interval[4:6]
                            primer = True
                            flag_horari = 0
                            if int(mes) > 3 and int(mes) <= 10:
                                flag_horari = 1
                            if len(linia) == 25:
                                cups = str(wizard.filename_curve)
                                cups = cups[0:20]
                            else:
                                cups = self.get_cups(cursor, uid,
                                                     numero_contador,
                                                     data_mes)
                            nom_fitxer_generat = True
                        if not primer:
                            if len(linia) == 25:
                                data_interval = linia[4]
                            else:
                                data_interval = linia[0]
                        hora_ant = hora
                        mes_ant = mes
                        if len(linia) == 25:
                            hora = linia[5]
                            hora = int(hora[0:2])
                            mes = data_interval[4:6]
                        else:
                            hora = int(data_interval[8:10])
                            mes = data_interval[4:6]
                        if (hora == 1 and mes == mes_ant) or primer:
                            # hem de generar un nou fitxer
                            if not primer:
                                zip_writer.writestr(
                                    nom_fitxer_sortida,
                                    fitxer_sortida.getvalue())
                            primer = False
                            nom_fitxer_sortida = 'F1_'+str(
                                wizard.r1)+'_'+data_interval[0:8]+'_'+str(
                                time.strftime("%Y%m%d"))+'.'+str(wizard.versio)
                            fitxer_sortida = StringIO.StringIO()
                            writer = csv.writer(fitxer_sortida,
                                                delimiter=';')
                        if len(linia) == 25:
                            activa_entrant = linia[6]
                        else:
                            activa_entrant = linia[1]
                        if len(linia) == 25:
                            qualitat_activa_entrant = linia[7]
                        else:
                            qualitat_activa_entrant = linia[2]
                        if len(linia) == 25:
                            activa_sortint = linia[8]
                        else:
                            activa_sortint = linia[3]
                        if len(linia) == 25:
                            qualitat_activa_sortint = linia[9]
                        else:
                            qualitat_activa_sortint = linia[4]
                        if len(linia) == 25:
                            reactiva_q1 = linia[10]
                        else:
                            reactiva_q1 = linia[5]
                        if len(linia) == 25:
                            qualitat_reactiva_q1 = linia[11]
                        else:
                            qualitat_reactiva_q1 = linia[6]
                        if len(linia) == 25:
                            reactiva_q2 = linia[12]
                        else:
                            reactiva_q2 = linia[7]
                        if len(linia) == 25:
                            qualitat_reactiva_q2 = linia[13]
                        else:
                            qualitat_reactiva_q2 = linia[8]
                        if len(linia) == 25:
                            reactiva_q3 = linia[14]
                        else:
                            reactiva_q3 = linia[9]
                        if len(linia) == 25:
                            qualitat_reactiva_q3 = linia[15]
                        else:
                            qualitat_reactiva_q3 = linia[10]
                        if len(linia) == 25:
                            reactiva_q4 = linia[16]
                        else:
                            reactiva_q4 = linia[11]
                        if len(linia) == 25:
                            qualitat_reactiva_q4 = linia[17]
                        else:
                            qualitat_reactiva_q4 = linia[12]
                        tipus_mesura = 11
                        hora_mesura = data_interval[0:4] + '/'
                        hora_mesura += data_interval[4:6]
                        hora_mesura += '/'+data_interval[6:8]+' '
                        if len(str(hora)) == 1:
                            hora_format = '0'+str(hora)
                        else:
                            hora_format = str(hora)
                        hora_mesura += hora_format+':00:00'
                        if data_interval[4:6] == '03' \
                                or data_interval[4:6] == '10':
                            if hora-hora_ant == 2 or hora-hora_ant == 0:
                                if data_interval[4:6] == '03':
                                    flag_horari = 1
                                else:
                                    flag_horari = 0

                        writer.writerow([cups, str(tipus_mesura),
                                         hora_mesura,
                                         str(flag_horari), activa_entrant,
                                         activa_sortint,
                                         reactiva_q1,
                                         reactiva_q2,
                                         reactiva_q3,
                                         reactiva_q4, '', '',
                                         str(1), str(1)])
                elif not fitxer_llegit:
                    missatge_error = _('Format de fitxer desconegut.')
                    break
            if missatge_error == '':
                zip_writer.writestr(nom_fitxer_sortida,
                                    fitxer_sortida.getvalue())
                zip_writer.close()
                fitxer_sortida.close()
                fitxer_correcte = self.checker(cursor, uid, ids, zf_io,
                                               numero_contador, cups,
                                               data_mes, context)
                if fitxer_correcte:
                    vals = {
                        'filename_f1': nom_fitxer_zip
                    }
                    self.write(cursor, uid, ids, vals, context)
                    self._cups.append(cups)
                    self._date.append(data_mes)
                    return zf_io
                else:
                    vals = {
                        'info': missatge_error
                    }
                    self.write(cursor, uid, ids, vals, context)
            else:
                vals = {
                    'info': missatge_error
                }
                self.write(cursor, uid, ids, vals, context)
        except csv.Error:
            missatge_error = _('Format de fitxer desconegut.')
            vals = {
                'info': missatge_error
            }
            if int(wizard.numCupsActual) == 0:
                vals = {
                    'state': 'init',
                    'info': missatge_error
                }
            self.write(cursor, uid, ids, vals, context)
        except:
            missatge_error = _(u"S'ha produït un error al llegir el fitxer.")
            vals = {
                'info': missatge_error
            }
            if int(wizard.numCupsActual) == 0:
                vals = {
                    'state': 'init',
                    'info': missatge_error
                }
            self.write(cursor, uid, ids, vals, context)

    def seg(self, cursor, uid, ids, context=None):
        vals = {
            'state': 'init',
            'fileCurve': '',
            'fileF1': '',
            'numCupsActual': '0',
            'info': _('Aquest assistent generarà un '
                      'fitxer F1 a partir d\'un fitxer de corba.')
        }
        self.write(cursor, uid, ids, vals, context)

    def clear(self, cursor, uid, ids, context=None):
        self._files = []
        self._cups = []
        self._date = []
        self._info = ''

    _columns = {
        'fileF1': fields.binary(_('Fitxer F1')),
        'filename_f1': fields.char('nom', size=256),
        'numCups': fields.integer('Número de CUPS:'),
        'versio': fields.integer(_('Versió:')),
        'numCupsActual': fields.char('Número de CUPS:', size=256),
        'fileCurve': fields.binary(_('Fitxer de corba')),
        'filename_curve': fields.char('nom_curve', size=256),
        'info': fields.text(_('Informació'), readonly=True),
        'state': fields.selection([('init', 'Init'), ('end', 'End'),
                                   ('on', 'On')], 'Estat'),
        'r1': fields.char(_('Codi R1'), size=12,
                          help=_('Agafa automàticament el '
                                 'camp ref2 de la Empresa '
                                 'configurada a la Companyia'))
    }

    _defaults = {
        'state': lambda *a: 'init',
        'r1': _default_r1,
        'numCups': 3,  # 3 sera el valor per defecte
        'numCupsActual': '0',
        'versio': 1,
        'info': _default_info
    }

WizardReeF1Curve()
