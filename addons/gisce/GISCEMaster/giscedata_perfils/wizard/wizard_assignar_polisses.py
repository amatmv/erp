# -*- coding: utf-8 -*-
import sys

import netsvc
from osv import osv, fields
from tools.translate import _

from giscedata_facturacio.giscedata_facturacio \
    import RECTIFYING_RECTIFICATIVE_INVOICE_TYPES

class WizardPerfilsAssignarPolisses(osv.osv_memory):
    """Assistent per assignar pòlisses i treure'n segons la facturació.
    """
    _name = 'perfils.assignar.polisses'
    # https://bugs.launchpad.net/openobject-server/+bug/555271
    _max_count = sys.maxint

    def get_polisses_perfil(self, cursor, uid, ids, context=None):
        """Retorna una llista amb les pòlisses assignades a lot de perfilació.
        """
        cl_obj = self.pool.get('giscedata.perfils.contracte_lot')
        wiz = self.browse(cursor, uid, ids[0])
        # Obtenim totes les pòlisses que hi ha aquest lot
        cl_ids = cl_obj.search(cursor, uid, [('lot_id.id', '=', wiz.lot_id.id)],
                               context={'active_test': False})
        p_ids = [a['polissa_id'][0] for a in cl_obj.read(cursor, uid, cl_ids,
                                                         ['polissa_id'])]
        return p_ids

    def get_factures_lot(self, cursor, uid, ids, context=None):
        """Retorna una llista amb les factures assignades al lot de facturació.
        """
        logger = netsvc.Logger()
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        wiz = self.browse(cursor, uid, ids[0])
        search_params = [
            ('data_inici', '<=', wiz.lot_id.data_final),
            ('data_final', '>=', wiz.lot_id.data_inici),
            ('state', 'in', ('open', 'paid')),
            ('tipo_factura', '=', '01'),
            ('invoice_id.journal_id.code', 'ilike', 'ENERGIA%'),
            ('tipo_rectificadora', 'in', RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']),
            ('refund_by_id', '=', False)
        ]
        f_ids = factura_obj.search(cursor, uid, search_params)
        logger.notifyChannel('objects', netsvc.LOG_DEBUG,
                             "S'han trobat %s factures al lot." % len(f_ids))
        return f_ids

    def get_polisses_facturacio(self, cursor, uid, ids, context=None):
        """Retorna una llista amb les pòlisses facturades.
        """
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        wiz = self.browse(cursor, uid, ids[0])
        f_ids = wiz.get_factures_lot()
        res = [a['polissa_id'][0] for a in factura_obj.read(cursor, uid, f_ids,
                                                            ['polissa_id'])]
        return res

    def buscar_in(self, cursor, uid, ids, context=None):
        """Busquem les pòlisses a assignar.
        """
        if context is None:
            context = {}
        context_no_active_test = context.copy()
        context_no_active_test.update({'active_test': False})

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.pool.get('giscedata.polissa')
        in_obj = self.pool.get('perfils.assignar.polisses.in')
        cfg_obj = self.pool.get('res.config')
        reading_obj = self.pool.get('giscedata.lectures.lectura')
        wiz = self.browse(cursor, uid, ids[0])
        mes_50 = int(cfg_obj.get(cursor, uid, 'perfils_perfilar_mes_50', 0))
        if wiz.polisses_in:
            [a.unlink() for a in wiz.polisses_in]
        # Obtenim totes les pòlisses que hi ha aquest lot
        p_ids = wiz.get_polisses_perfil()
        policy_datas = {}
        f_ids = wiz.get_factures_lot()
        for factura in factura_obj.read(cursor, uid, f_ids, ['polissa_id']):
            if factura['polissa_id'][0] in p_ids:
                continue
            factura = factura_obj.browse(cursor, uid, factura['id'])
            polissa = polissa_obj.browse(cursor, uid, factura.polissa_id.id,
                                         {'date': factura.data_final})
            if polissa.tarifa.name in ['RE', 'RE12']:
                continue
            if polissa.agree_tipus < '03':
                continue
            if polissa.agree_tipus == '03' and not mes_50:
                continue
            dates = []
            if factura.lectures_energia_ids:
                dates = [x.data_anterior for x in factura.lectures_energia_ids
                         if x.tipus == 'activa']
                from_date = min(dates)
            else:
                from_date = factura.data_inici
            if polissa.id not in policy_datas:
                policy_datas[polissa.id] = {
                    'date': from_date,
                    'power': factura.potencia,
                    'create_date': polissa.data_alta,
                    'last_read_profiled': polissa.data_ultima_lectura_perfilada
                }
            else:
                policy_datas[polissa.id]['date'] = min(
                    [
                        policy_datas[polissa.id]['date'],
                        from_date
                    ]
                )
                policy_datas[polissa.id]['power'] = min(
                    [
                        policy_datas[polissa.id]['power'],
                        factura.potencia
                    ]
                )
        batch_start_date = wiz.lot_id.data_inici
        for policy_id in policy_datas:
            policy_data = policy_datas[policy_id]
            if (
                policy_data['last_read_profiled'] < batch_start_date
            ) and (
                policy_data['date'] > batch_start_date
            ):
                dates = []
                invoice_ids = factura_obj.search(
                    cursor,
                    uid,
                    [
                        ('polissa_id', '=', policy_id),
                        ('data_final', '<', batch_start_date)
                    ],
                    context=context
                )
                for invoice in factura_obj.browse(
                    cursor,
                    uid,
                    invoice_ids,
                    context=context
                ):
                    for read in invoice.lectures_energia_ids:
                        if (
                            read.tipus == 'activa'
                        ) and (
                            read.data_anterior < batch_start_date
                        ):
                            dates.append(read.data_anterior)
                if dates:
                    policy_datas[policy_id]['date'] = max(dates)
                else:
                    read_ids = reading_obj.search(
                        cursor,
                        uid,
                        [
                            ('comptador.polissa', '=', policy_id),
                            ('name', '<=', batch_start_date),
                            ('tipus', '=', 'A')
                        ],
                        context=context_no_active_test
                    )
                    reads = reading_obj.read(
                        cursor,
                        uid,
                        read_ids,
                        ['name'],
                        context=context
                    )
                    dates = [x['name'] for x in reads]

                    if dates:
                        policy_datas[policy_id]['date'] = max(dates)
                    else:
                        policy_datas[policy_id]['date'] = \
                            policy_datas[policy_id]['create_date']

            in_obj.create(
                cursor,
                uid,
                {
                    'ass_id': wiz.id,
                    'polissa_id': policy_id,
                    'data_lectura_perfilar': policy_datas[policy_id]['date'],
                    'potencia': policy_datas[policy_id]['power'],
                    'data_alta': policy_datas[policy_id]['create_date']
                }
            )
        return True

    def assignar(self, cursor, uid, ids, context=None):
        """Assignem les pòlisses al lot.
        """
        wiz = self.browse(cursor, uid, ids[0])
        for p_in in wiz.polisses_in:
            p_in.polissa_id.write(
                {'lot_perfilacio': wiz.lot_id.id,
                 'data_ultima_lectura_perfilada': p_in.data_lectura_perfilar},
                context={'sync': False})
        wiz.buscar_in()
        return True

    def buscar_out(self, cursor, uid, ids, context=None):
        """Busquem les pòlisses a treure.
        """
        logger = netsvc.Logger()
        polissa_obj = self.pool.get('giscedata.polissa')
        out_obj = self.pool.get('perfils.assignar.polisses.out')
        wiz = self.browse(cursor, uid, ids[0])
        if wiz.polisses_out:
            [a.unlink() for a in wiz.polisses_out]
        # Obtenim totes les pòlisses que hi ha aquest lot
        p_ids = wiz.get_polisses_perfil()
        pf_ids = wiz.get_polisses_facturacio()
        for polissa_id in p_ids:
            if polissa_id in pf_ids:
                continue
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            logger.notifyChannel('objects', netsvc.LOG_DEBUG,
                             _(u"Treient pòlissa %s (id:%s) %s")
                             % (polissa.id, polissa.name, polissa.data_baixa))
            out_obj.create(cursor, uid, {'ass_id': wiz.id,
                                         'polissa_id': polissa.id,
                                         'data_baixa': polissa.data_baixa})
        return True

    def treure(self, cursor, uid, ids, context=None):
        """Treu les pòlisses que no s'han de perfilar del lot.
        """
        cl_obj = self.pool.get('giscedata.perfils.contracte_lot')
        wiz = self.browse(cursor, uid, ids[0])
        p_ids = [a.polissa_id.id for a in wiz.polisses_out]
        search_params = [('polissa_id.id', 'in', p_ids),
                         ('lot_id.id', '=', wiz.lot_id.id)]
        unlink_ids = cl_obj.search(cursor, uid, search_params,
                                   context={'active_test': False})
        cl_obj.unlink(cursor, uid, unlink_ids)
        wiz.buscar_out()
        return True

    _columns = {
        'polisses_in': fields.one2many('perfils.assignar.polisses.in', 'ass_id',
                                       'Pòlisses a assignar'),
        'polisses_out': fields.one2many('perfils.assignar.polisses.out',
                                        'ass_id', 'Polisses a treure'),
        'lot_id': fields.many2one('giscedata.perfils.lot', 'Lot',
                                  required=True),
        'state': fields.char('Estat', size=10),
    }

    def default_lot_id(self, cursor, uid, context=None):
        """Retorna el lot id.
        """
        if not context:
            context = {}
        return context.get('active_id', False)

    _defaults = {
        'state': lambda *a: 'init',
        'lot_id': default_lot_id,
    }
WizardPerfilsAssignarPolisses()

class WizardPerfilsAssignarPolissesIn(osv.osv_memory):
    """Polisses a assignar.
    """
    _name = 'perfils.assignar.polisses.in'
    # https://bugs.launchpad.net/openobject-server/+bug/555271
    _max_count = sys.maxint
    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True),
        'data_lectura_perfilar': fields.date('Data de lectura a perfilar',
                                             required=True),
        'potencia': fields.float('Potencia', digits=(16, 3)),
        'data_alta': fields.date('Data alta'),
        'ass_id': fields.many2one('perfils.assignar.polisses', 'Assignació',
                                  required=True),
    }
    _order = "data_alta asc"
WizardPerfilsAssignarPolissesIn()

class WizardPerfilsAssignarPolissesOut(osv.osv_memory):
    """Polisses a treure.
    """
    _name = 'perfils.assignar.polisses.out'
    # https://bugs.launchpad.net/openobject-server/+bug/555271
    _max_count = sys.maxint
    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True),
        'ass_id': fields.many2one('perfils.assignar.polisses', 'Assignació',
                                  required=True),
        'data_baixa': fields.date('Data baixa'),
    }
WizardPerfilsAssignarPolissesOut()
