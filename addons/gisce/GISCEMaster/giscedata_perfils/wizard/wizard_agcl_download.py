# -*- coding: utf-8 -*-
from StringIO import StringIO
import base64
import bz2

from osv import osv, fields

class GiscedataPerfilsAGCLDownload(osv.osv_memory):
    """Wizard per descarregar els fitxer AGCL.
    """
    _name = 'giscedata.perfils.agcl.download'

    def _get_agcl_obj(self, cursor, uid, context=None):
        """Retorna l'objecte AGCL.
        """
        agcl_obj = self.pool.get('giscedata.perfils.agcl')
        if not context:
            context = {}
        active_id = context.get('active_ids', False)
        if active_id:
            active_id = active_id[0]
            return agcl_obj.browse(cursor, uid, active_id)
        return False

    def _get_agcl_bz2(self, cursor, uid, context=None):
        """Retorna el valor del checkbox bz2.
        """
        agcl = self._get_agcl_obj(cursor, uid, context)
        if agcl:
            return agcl.bz2
        return False

    def default_name(self, cursor, uid, context=None):
        """Retorna el nom del fitxer per defecte.
        """
        agcl = self._get_agcl_obj(cursor, uid, context)
        if agcl:
            return agcl.name
        return False

    def default_file(self, cursor, uid, context=None):
        """Retorna el fitxer per defecte.
        """
        agcl = self._get_agcl_obj(cursor, uid, context)
        _bz2 = self._get_agcl_bz2(cursor, uid, context)
        if agcl:
            lines = agcl.gen_file()
            agcl = StringIO()
            agcl.writelines(lines)
            if _bz2:
                bzdata = bz2.compress(agcl.getvalue())
                res_file = base64.b64encode(bzdata)
            else:
                res_file = base64.b64encode(agcl.getvalue())
            agcl.close()
            return res_file
        return False

    _columns = {
        'name': fields.char('Nom del fitxer', size=256),
        'file': fields.binary('Fitxer'),
    }

    _defaults = {
        'name': default_name,
        'file': default_file
    }

GiscedataPerfilsAGCLDownload()