import base64
import bz2

from osv import osv, fields

class GiscedataPerfilsCLMAG5Download(osv.osv_memory):
    """
    Wizard to download the CLMAG5 file.
    """

    _name = 'giscedata.perfils.clmag5.download'

    def _get_giscedata_perfils_clmag5(self, cursor, uid, context=None):
        """
        Returns the giscedata.perfils.clmag5 object.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: giscedata.perfils.clmag5 object
        """

        giscedata_perfils_clmag5 = self.pool.get('giscedata.perfils.clmag5')
        if context is None:
            context = {}
        active_id = context.get('active_ids', False)
        if active_id:
            active_id = active_id[0]
            return giscedata_perfils_clmag5.browse(cursor, uid, active_id)
        return False

    def _get_clmag5_bz2(self, cursor, uid, context=None):
        """
        Returns bz2 checkbox value.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: boolean references checkbox value
        """
        clmag5 = self._get_giscedata_perfils_clmag5(cursor, uid, context)
        if clmag5:
            return clmag5.bz2
        return False

    def default_name(self, cursor, uid, context=None):
        """
        Returns the CLMAG5 filename.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: string with the CLMAG5 filename.
        """
        clmag5 = self._get_giscedata_perfils_clmag5(cursor, uid, context)
        if clmag5:
            return clmag5.filename(context=context)
        return False

    def default_file(self, cursor, uid, context=None):
        """
        Returns the CLMAG5 file.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: the CLMAG5 file
        """
        clmag5 = self._get_giscedata_perfils_clmag5(cursor, uid, context)
        _bz2 = self._get_clmag5_bz2(cursor, uid, context)
        if clmag5:
            clmag5_file = clmag5.build_file()
            if _bz2:
                bzdata = bz2.compress(clmag5_file)
                base64_file = base64.b64encode(bzdata)
            else:
                base64_file = base64.b64encode(clmag5_file)
            return base64_file
        return False

    _columns = {
        'name': fields.char('Nom del fitxer', size=256),
        'file': fields.binary('Fitxer'),
    }

    _defaults = {
        'name': default_name,
        'file': default_file
    }

GiscedataPerfilsCLMAG5Download()
