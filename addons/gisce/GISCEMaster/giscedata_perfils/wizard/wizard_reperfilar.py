# -*- coding: utf-8 -*-

from osv import osv, fields
from giscedata_facturacio.giscedata_facturacio \
    import RECTIFYING_RECTIFICATIVE_INVOICE_TYPES
from tools.translate import _


class Reperfilar(osv.osv_memory):
    _name = 'giscedata.perfils.reperfilar'

    def seleccionar_lot(self, cursor, uid, ids, lot, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if lot:
            # Get the polisses:ids
            #  of giscedata.perfils.contracte_lot with lot_id = lot
            contracte_lot_obj = self.pool.get('giscedata.perfils.contracte_lot')
            cont_lot_ids = contracte_lot_obj.search(
                cursor, uid, [('lot_id', '=', lot)]
            )
            llista_polisses_lot = [
                cont_lot['polissa_id'][0] for cont_lot in
                contracte_lot_obj.read(
                    cursor, uid, cont_lot_ids, ['polissa_id']
                )
            ]

            res['domain'].update(
                {'polissa': [('id', 'in', llista_polisses_lot)]}
            )
        else:
            res['domain'].update({'polissa': []})

        return res

    def seleccionar_polissa(self, cursor, uid, ids, polissa, context=None):
        if context is None:
            context = {}

        res = {'value': {}, 'domain': {}, 'warning': {}}

        if polissa:
            obj_lot = self.pool.get('giscedata.perfils.contracte_lot')

            cont_lot = obj_lot.search(cursor, uid,
                                      [('polissa_id', '=', polissa)])

            lots = []
            for i in cont_lot:
                lots.append(
                    obj_lot.read(cursor, uid, i, ['lot_id'])['lot_id'][0])

            res['domain'].update(
                {'lot': [('state', '!=', 'esborrany'), ('id', 'in', lots)]})
        else:
            res['domain'].update({'lot': [('state', '!=', 'esborrany')]})

        return res

    def reiniciar(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        self.write(cursor, uid, ids[0], {
            'state': 'init',
            'polissa': ''
        }, context)

    def reperfilar(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context)
        obj_factura = self.pool.get('giscedata.facturacio.factura')
        perf_obj = self.pool.get('giscedata.perfils.perfil')

        lot = wizard.lot
        polissa = wizard.polissa

        data_inici = lot.data_inici
        data_final = lot.data_final
        cups = polissa.cups.name

        cons_ant = '{}'.format(
            perf_obj.get_sum_perfils_cups(
                cursor, uid, cups, data_inici, data_final, context
            )
        )

        ultima_facturada = polissa.data_ultima_lectura_perfilada
        ultima_perfilada = polissa.data_ultima_lectura_perfilada_bp

        old_vals = {
            'lot_perfilacio': polissa.lot_perfilacio.id,
            'data_ultima_lectura_perfilada': ultima_facturada,
            'data_ultima_lectura_perfilada_bp': ultima_perfilada
        }
        old_lot_perf = '{}'.format(polissa.lot_perfilacio.id)
        old_ultima_lect = polissa.data_ultima_lectura_perfilada
        old_ultima_lect_bp = '{}'.format(
            polissa.data_ultima_lectura_perfilada_bp)

        elim = '{}'.format(
            perf_obj.borrar_perfils_cups(
                cursor, uid, cups, data_inici, data_final, context
            )
        )

        fids = obj_factura.search(cursor, uid, [
            ('polissa_id.id', '=', polissa.id),
            ('lot_facturacio.name', '=', lot.name),
            (
                'tipo_rectificadora', 'in',
                RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']
            )
        ])
        factures = [obj_factura.browse(cursor, uid, fid, context)
                    for fid in fids]
        del_factures = [f.ref for f in factures if f.ref]
        from_date = False
        for factura in factures:
            if factura.id in del_factures:
                continue
            from_date = min(
                [l.data_anterior for l in factura.lectures_energia_ids
                 if l.tipus == 'activa']
            )
        # Si la pòlissa no està assignada al lot actual, no es pot reperfilar
        if not from_date:
            message = _(u'La pòlissa {}, no està assignada al lot {}')
            raise osv.except_osv(
                _('Atenció'),
                message.format(polissa.name, lot.name)
            )
        new_vals = {
            'data_ultima_lectura_perfilada_bp': False,
            'data_ultima_lectura_perfilada': from_date,
            'lot_perfilacio': lot.id
        }
        new_lot_perf = '{}'.format(lot.id)
        new_ultima_lect = from_date
        new_ultima_lect_bp = 'False'

        polissa.write(new_vals)

        polissa.perfilar(lot.id)

        cons_act = '{}'.format(perf_obj.get_sum_perfils_cups(
            cursor, uid, cups, data_inici, data_final, context
        ))

        polissa.write(old_vals)

        wizard.write(
            {
                'state': 'end', 'cons_ant': cons_ant,
                'old_lot_perf': old_lot_perf,
                'old_ultima_lect': old_ultima_lect,
                'old_ultima_lect_bp': old_ultima_lect_bp,
                'elim': elim, 'new_lot_perf': new_lot_perf,
                'new_ultima_lect': new_ultima_lect,
                'new_ultima_lect_bp': new_ultima_lect_bp,
                'cons_act': cons_act
            }
        )

    def _default_lot(self, cursor, uid, context=None):
        if context is None:
            context = {}

        if context.get('from_model', '') == 'giscedata.perfils.lot':
            return context['active_ids'][0]
        elif context.get('from_model', '') == 'giscedata.perfils.contracte_lot':
            return self.pool.get(
                'giscedata.perfils.contracte_lot'
            ).read(
                cursor, uid, context['active_ids'][0], ['lot_id']
            )['lot_id']
        else:
            return False

    def _default_polissa(self, cursor, uid, context=None):
        if context is None:
            context = {}

        if context.get('from_model', '') == 'giscedata.perfils.contracte_lot':
            return self.pool.get(
                'giscedata.perfils.contracte_lot'
            ).read(
                cursor, uid, context['active_ids'][0], ['polissa_id']
            )['polissa_id']
        else:
            return False

    # Borrar els camps de les columnes que no utilitzem

    _columns = {
        'state': fields.selection([
            ('init', 'Init'),
            ('lot_triat', 'Lot Triat'),
            ('end', 'End'),
        ], 'State'),
        'lot': fields.many2one('giscedata.perfils.lot', 'Lot', required=True,
                               domain=[('state', '!=', 'esborrany')]),
        'polissa': fields.many2one(
            'giscedata.polissa', 'Polissa', required=True
        ),

        'cons_ant': fields.char('Consum anterior', size=50),
        'old_lot_perf': fields.char('Antic lot perfilacio', size=50),
        'old_ultima_lect': fields.char('Antiga ultima lectura perfilada',
                                       size=50),
        'old_ultima_lect_bp': fields.char('Antiga utlima lectura perfilada bp',
                                          size=50),
        'elim': fields.char('Numero d\'eliminats', size=50),
        'new_lot_perf': fields.char('Nou lot perfilacio', size=50),
        'new_ultima_lect': fields.char('Nova ultima lectura perfilada',
                                       size=50),
        'new_ultima_lect_bp': fields.char('Nova utlima lectura perfilada bp',
                                          size=50),
        'cons_act': fields.char('Consum actual', size=50),
    }

    _defaults = {
        'lot': _default_lot,
        'polissa': _default_polissa,
        'state': lambda *a: 'init',
    }


Reperfilar()
