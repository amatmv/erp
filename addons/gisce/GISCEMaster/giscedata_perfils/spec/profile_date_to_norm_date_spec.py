from datetime import datetime, timedelta
from collections import namedtuple

from enerdata.datetime.timezone import TIMEZONE

from expects import *

TimeStamp = namedtuple('TimeStamp', ['year', 'month', 'day', 'hour'])
"""TimeStamp struct with year, month, day and hour attributes
"""

REE_WINTER = 0
"""REE Winter constant
"""
REE_SUMMER = 1
"""REE Summer constant
"""


def ts_to_struct(ts):
    """Convert a profile timestamp YYYYMMDDHH to a struct
    """
    year = ts[0:4]
    month = ts[4:6]
    day = ts[6:8]
    hour = ts[8:10]
    return TimeStamp(*map(int, (year, month, day, hour)))


def normalize_date(timestamp, winter_summer):
    """Convert a profile timestamp to a datetime object standard

    :param timestamp: string with format %Y%m%d%H
    :param winter_summer: '1' for summer, 0 for winter
    :return datetime object with timezone
    """
    is_dst = convert_dst(winter_summer)
    ts = ts_to_struct(timestamp)
    if ts.hour == 24:
        dt = datetime(ts.year, ts.month, ts.day) + timedelta(days=1)
    else:
        dt = datetime(ts.year, ts.month, ts.day, ts.hour)
    return TIMEZONE.normalize(TIMEZONE.localize(dt, is_dst=is_dst))


def convert_dst(winter_summer):
    """Convert REE Winter/Summer flag to timezone dst
    """
    return bool(not int(winter_summer))


with description('Converting a timestamp to struct'):
    with before.all:
        self.ts = '2015123124'

    with it('must to return all the components'):
        struct = ts_to_struct(self.ts)
        expect(len(struct)).to(equal(4))

    with it('must return as a named tuple'):
        struct = ts_to_struct(self.ts)
        expect(struct.year).to(equal(2015))
        expect(struct.month).to(equal(12))
        expect(struct.day).to(equal(31))
        expect(struct.hour).to(equal(24))

with description('Converting profile dates to standard dates'):

    with it('must convert profile sumer/winter to is_dst'):
        expect(convert_dst(REE_SUMMER)).to(equal(False))
        expect(convert_dst(REE_WINTER)).to(equal(True))

    with it('must return the day +1 if the hour is 24'):
        d = '2015013124'
        date_compare = normalize_date(d, REE_WINTER).strftime('%Y-%m-%d %H')
        expect(date_compare).to(equal('2015-02-01 00'))

    with it('must act as normal if the hour is not 24'):
        d = '2015081223'
        date_compare = normalize_date(d, REE_SUMMER).strftime('%Y-%m-%d %H')
        expect(date_compare).to(equal('2015-08-12 23'))

    with it('must return correct hour with daylight saving hour'):
        change_hour = '2015102502'
        dt = normalize_date(change_hour, REE_SUMMER)
        expect(dt.strftime('%Y-%m-%d %H')).to(equal('2015-10-25 02'))
        expect(dt.dst().total_seconds()).to(equal(0))

        change_hour = '2015102502'
        dt = normalize_date(change_hour, REE_WINTER)
        expect(dt.strftime('%Y-%m-%d %H')).to(equal('2015-10-25 02'))
        expect(dt.dst().total_seconds()).to(equal(3600))

        change_hour = '2015102503'
        dt = normalize_date(change_hour, REE_WINTER)
        expect(dt.strftime('%Y-%m-%d %H')).to(equal('2015-10-25 03'))

        change_hour = '2016032701'
        dt = normalize_date(change_hour, REE_WINTER)
        expect(dt.strftime('%Y-%m-%d %H')).to(equal('2016-03-27 01'))

        change_hour = '2016032702'
        dt = normalize_date(change_hour, REE_SUMMER)
        expect(dt.strftime('%Y-%m-%d %H')).to(equal('2016-03-27 03'))

        change_hour = '2016032703'
        dt = normalize_date(change_hour, REE_SUMMER)
        expect(dt.strftime('%Y-%m-%d %H')).to(equal('2016-03-27 03'))
