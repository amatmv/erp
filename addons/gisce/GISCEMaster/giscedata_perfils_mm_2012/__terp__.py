# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2012) Balears",
    "description": """
Actualitza els valors de perfilació per l'any 2012 a Balears
""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_perfils_2012"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_mm_2012_data.xml"
    ],
    "active": False,
    "installable": True
}
