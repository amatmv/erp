# -*- coding: utf-8 -*-

from osv import osv, fields


# ToDo: Old model, to remove
class GiscedataBtCaixesBlockname(osv.osv):
    _name = 'giscedata.bt.caixes.blockname'
    _description = 'Blocknames per les caixes BT'

    def create(self, cr, uid, vals, context={}):
        if not vals.has_key('code'):
            cr.execute("SELECT code from giscedata_bt_caixes_blockname ORDER BY code DESC LIMIT 1")
            codi = cr.fetchone()
            if codi and len(codi):
                vals['code'] = int(codi[0]) + 1
            else:
                vals['code'] = 1
        return super(osv.osv, self).create(cr, uid, vals, context)

    _columns = {
        'name': fields.char('Blockname', size=10, required=True),
        'description': fields.char('Descripció', size=255),
        'code': fields.integer('Codi'),
    }

    _defaults = {

    }

    _order = "name, id"


GiscedataBtCaixesBlockname()


# ToDo: Old model, to remove
class GiscedataBtCaixes(osv.osv):
    _name = 'giscedata.bt.caixes'
    _description = 'Model per les caixes de BT'

    def _get_nextnumber(self, cr, uid, context={}):
        cr.execute("select coalesce(max(name::int),0)+1 from giscedata_bt_caixes")
        return str(cr.fetchone()[0])

    _columns = {
        'name': fields.char('Codi', size=8, required=True),
        'id_municipi': fields.many2one('res.municipi', 'Municipi'),
        'blockname': fields.many2one('giscedata.bt.caixes.blockname', 'Blockname'),
    }

    _defaults = {
        'name': _get_nextnumber,
    }


GiscedataBtCaixes()


class GiscedataBtCaixaTipus(osv.osv):
    _name = 'giscedata.bt.caixa.tipus'
    _description = 'Tipus de Caixes BT'

    def create(self, cr, uid, vals, context=None):
        """
        This method overloads the original create by adding the logic to avoid
        the creation of duplicated GiscedataBtCaixaTipus
        :param cr: Database Cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param vals: Values of the object that is going to be created.
        :type vals: dict[str, Any]
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Id of the new created element
        :rtype: int
        """

        if context is None:
            context = {}

        ids = self.search(
            cr, uid,
            ['&', ('code', '=', vals['code']), ('name', '=', vals['name'])]
        )

        if ids and len(ids):
            return ids[0]
        else:
            return super(GiscedataBtCaixaTipus, self).create(
                cr, uid, vals
            )

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'code': fields.char('Codi', size=64, required=True),
        'description': fields.char('Descripció', size=256),
    }


GiscedataBtCaixaTipus()


class GiscedataBtCaixa(osv.osv):
    _name = 'giscedata.bt.caixa'
    _description = 'Caixes BT'

    def _get_next_number(self, cursor, uid, context=None):
        """
        Returns the next value for the field name from the sequence
        giscedata.bt.caixa
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The next value of the sequence
        :rtype str
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscedata.bt.caixa'
        )

    _columns = {
        'name': fields.char('Numeració', size=64, required=True),
        'codi': fields.char('Codi', size=64),
        'tipus': fields.many2one(
            'giscedata.bt.caixa.tipus', 'Tipus', required=True
        ),
        'intensitat': fields.float('Intensitat'),
        'intensitat_maxima': fields.float('Intensitat Màxima'),
        'active': fields.boolean('Actiu'),
        'baixa': fields.boolean('Baixa'),
        'data_pm': fields.datetime('Data APM'),
        'data_baixa': fields.datetime('Data Baixa'),
        'observacions': fields.text('Observacions')
    }

    _defaults = {
        'active': lambda *a: 1,
        'name': _get_next_number
    }


GiscedataBtCaixa()


class GiscedataBtCaixaElementTipus(osv.osv):
    _name = 'giscedata.bt.caixa.element.tipus'
    _description = 'Tipus Elements de Caixes BT'

    def create(self, cr, uid, vals, context=None):
        """
        This method overloads the original create by adding the logic to avoid
        the creation of duplicated GiscedataBtCaixaElementTipus
        :param cr: Database Cursor
        :type cr: Cursor
        :param uid: User ID
        :type uid: int
        :param vals: Values of the object that is going to be created.
        :type vals: dict[str, Any]
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: Id of the new created element
        :rtype: int
        """

        if context is None:
            context = {}

        ids = self.search(
            cr, uid,
            ['&', ('code', '=', vals['code']), ('name', '=', vals['name'])]
        )

        if ids and len(ids):
            return ids[0]
        else:
            return super(GiscedataBtCaixaElementTipus, self).create(
                cr, uid, vals
            )

    _columns = {
        'name': fields.char('Nom', size=64, required=True),
        'code': fields.char('Codi', size=64, required=True),
        'description': fields.char('Descripció', size=256),
    }


GiscedataBtCaixaElementTipus()


class GiscedataBtCaixaElement(osv.osv):
    _name = 'giscedata.bt.caixa.element'
    _description = 'Elements de Caixes BT'

    def _get_next_number(self, cursor, uid, context=None):
        """
        Returns the next value for the field name from the sequence
        giscedata.bt.caixa.element
        :param cursor: Database Cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The next value of the sequence
        :rtype str
        """

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscedata.bt.caixa.element'
        )

    _columns = {
        'name': fields.char('Numeració', size=64, required=True),
        'codi': fields.char('Codi', size=64),
        'tipus': fields.many2one(
            'giscedata.bt.caixa.element.tipus', 'Tipus', required=True
        ),
        'intensitat': fields.float('Intensitat'),
        'active': fields.boolean('Actiu'),
        'baixa': fields.boolean('Baixa'),
        'data_pm': fields.datetime('Data PM'),
        'data_baixa': fields.datetime('Data Baixa')
    }

    _defaults = {
        'active': lambda *a: 1,
        'name': _get_next_number
    }


GiscedataBtCaixaElement()
