# -*- coding: utf-8 -*-
{
    "name": "giscedata_bt_caixes",
    "description": """Model per les caixes de BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Baixa tensió",
    "depends":[
        "base",
        "base_extended",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_bt_caixes_demo.xml",
        "giscedata_bt_caixes_sequence_demo.xml"
    ],
    "update_xml":[
        "ir.model.access.csv",
        "security/ir.model.access.csv",
        "giscedata_bt_caixes_data.xml",
        "giscedata_bt_caixes_view.xml",
        "giscedata_bt_caixes_sequence.xml"
    ],
    "active": False,
    "installable": True
}
