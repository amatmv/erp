SELECT
  ca.codi AS "ccaa",
  CASE
    WHEN tarifa.codi = (
      SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1A'
    )
    THEN (SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1.A')
    ELSE tarifa.codi
  END AS "tarifa",
  setu.tipus AS "tipus_client",
  import.num_clients,
  energia.energia,
  import.import
FROM giscedata_liquidacio_suplement_territorial_data AS setu
INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
INNER JOIN (
  SELECT
    ca.codi AS "ccaa",
    tarifa.codi AS "tarifa",
    setu.tipus AS "tipus_client",
    ROUND(SUM(setu.energia_total), 2) AS "energia"
  FROM giscedata_liquidacio_suplement_territorial_data AS setu
  INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
  INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
  WHERE setu.proporcio != 0
  AND setu.cups_id IN (
    SELECT DISTINCT f.cups_id
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    WHERE inv.date_invoice >= '2017-08-01'
    AND inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
  )
  GROUP BY ca.codi, tarifa.codi, setu.tipus
) AS energia ON (
  energia.ccaa = ca.codi
  AND energia.tarifa = tarifa.codi
  AND energia.tipus_client = setu.tipus
)
INNER JOIN (
  SELECT
    ca.codi AS "ccaa",
    tarifa.codi AS "tarifa",
    setu.tipus AS "tipus_client",
    COUNT(DISTINCT inv.partner_id) AS num_clients,
    ROUND(SUM((inv_line.price_subtotal * setu.proporcio) * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END), 2)::text AS "import"
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  INNER JOIN giscedata_liquidacio_suplement_territorial_data AS setu ON f.cups_id = setu.cups_id
  INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
  INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
  WHERE inv.date_invoice >= '2017-08-01'
  AND inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
  GROUP BY ca.codi, tarifa.codi, setu.tipus
) AS import ON (
  import.ccaa = ca.codi
  AND import.tarifa = tarifa.codi
  AND import.tipus_client = setu.tipus
)
WHERE setu.proporcio != 0
AND setu.cups_id IN (
  SELECT DISTINCT f.cups_id
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  WHERE inv.date_invoice >= '2017-08-01'
  AND inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
)
GROUP BY ca.codi, tarifa.codi, setu.tipus, import.num_clients, energia.energia, import.import
ORDER BY tarifa, setu.tipus
