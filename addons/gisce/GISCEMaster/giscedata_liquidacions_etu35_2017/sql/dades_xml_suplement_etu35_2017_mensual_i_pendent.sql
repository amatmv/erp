SELECT
  ca.codi AS "ccaa",
  CASE
    WHEN tarifa.codi = (
      SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1A'
    )
    THEN (SELECT codi FROM giscedata_liquidacio_tarifes WHERE NAME = '6.1.A')
    ELSE tarifa.codi
  END AS "tarifa",
  import.num_clients::text AS num_clients,
  energia.energia::text AS energia,
  import.import::text AS import
FROM giscedata_liquidacio_suplement_territorial_data AS setu
INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
INNER JOIN (
  SELECT
    ca.codi AS ccaa,
    tarifa.codi AS tarifa,
    ROUND(SUM(f.total_energia * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END), 0) AS energia
  FROM giscedata_facturacio_factura AS f
  INNER JOIN account_invoice AS inv ON (inv.id = f.invoice_id)
  INNER JOIN giscedata_liquidacio_suplement_territorial_data AS setu ON f.cups_id = setu.cups_id
  INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
  INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
  WHERE setu.proporcio != 0
  AND setu.cups_id IN (
    SELECT DISTINCT f.cups_id
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    WHERE inv.date_invoice >= %(data_inici)s
    AND inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
  )
  GROUP BY ca.codi, tarifa.codi
) AS energia ON (
  energia.ccaa = ca.codi
  AND energia.tarifa = tarifa.codi
)
INNER JOIN (
  SELECT
    foo.ccaa AS "ccaa",
    foo.tarifa AS "tarifa",
    COUNT(DISTINCT foo.client) AS num_clients,
    ROUND(SUM(foo.import), 2) AS import
   FROM (
    SELECT
      ca.codi AS "ccaa",
      tarifa.codi AS "tarifa",
      inv.partner_id AS client,
      SUM((inv_line.price_subtotal * setu.proporcio) * CASE WHEN inv.type = 'out_refund' THEN -1 ELSE 1 END) AS "import"
    FROM account_invoice_line AS inv_line
    INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
    INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
    INNER JOIN giscedata_liquidacio_suplement_territorial_data AS setu ON f.cups_id = setu.cups_id
    INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
    INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
    WHERE inv.date_invoice >= %(data_inici)s
    AND inv.state IN ('open', 'paid')
    AND inv_line.product_id = %(product_id)s
    GROUP BY ca.codi, tarifa.codi, inv.partner_id
    UNION (
      SELECT
        ca.codi AS "ccaa",
        tarifa.codi AS "tarifa",
        pol.pagador AS client,
        SUM(extra.total_amount_pending * setu.proporcio) AS "import"
      FROM giscedata_facturacio_extra AS extra
      INNER JOIN giscedata_polissa AS pol ON (pol.id = extra.polissa_id)
      INNER JOIN giscedata_liquidacio_suplement_territorial_data AS setu ON pol.cups = setu.cups_id
      INNER JOIN giscedata_liquidacio_tarifes AS tarifa ON tarifa.id = setu.tarifa_id
      INNER JOIN res_comunitat_autonoma AS ca ON ca.id = setu.ccaa_id
      WHERE extra.product_id = %(product_id)s
      AND extra.total_amount_pending != 0
      -- AND setu.proporcio != 0
      AND pol.state = 'activa'
      GROUP BY ca.codi, tarifa.codi, pol.pagador
    )
  ) AS foo
  GROUP BY foo.ccaa, foo.tarifa
) AS import ON (
  import.ccaa = ca.codi
  AND import.tarifa = tarifa.codi
)
WHERE setu.proporcio != 0
AND setu.cups_id IN (
  SELECT DISTINCT f.cups_id
  FROM account_invoice_line AS inv_line
  INNER JOIN account_invoice AS inv ON (inv.id = inv_line.invoice_id)
  INNER JOIN giscedata_facturacio_factura AS f ON (f.invoice_id = inv.id)
  WHERE inv.date_invoice >= %(data_inici)s
  AND inv.state IN ('open', 'paid')
  AND inv_line.product_id = %(product_id)s
)
GROUP BY ca.codi, tarifa.codi, import.num_clients, energia.energia, import.import
ORDER BY tarifa
