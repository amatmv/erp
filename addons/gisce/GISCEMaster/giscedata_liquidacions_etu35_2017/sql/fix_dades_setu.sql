-- UPDATE CUPS NAME FROM CUPS_ID
UPDATE giscedata_liquidacio_suplement_territorial_data setu
SET cups = (SELECT name FROM giscedata_cups_ps where id=setu.cups_id);

-- UPDATE CCAA FROM CUPS_ID
UPDATE giscedata_liquidacio_suplement_territorial_data setu
SET ccaa_id = (
  SELECT ca.id from res_comunitat_autonoma AS ca
  LEFT JOIN res_country_state AS cs ON ca.id = cs.comunitat_autonoma
  LEFT JOIN res_municipi AS mun ON cs.id = mun.state
  LEFT JOIN giscedata_cups_ps AS cups ON  mun.id = cups.id_municipi
  WHERE cups.id = setu.cups_id
);

-- UPDATE CCAA FROM THE REST
UPDATE giscedata_liquidacio_suplement_territorial_data
SET ccaa_id = (
  SELECT DISTINCT s.ccaa_id FROM giscedata_liquidacio_suplement_territorial_data s
  WHERE s.ccaa_id IS NOT NULL
  LIMIT 1
)
WHERE ccaa_id IS NULL;
