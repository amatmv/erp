# -*- coding: utf-8 -*-
{
  "name": "Liquidació de suplements territorials ETU/35/2017",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Permet imprimir les liquidacions dels suplements territorials ETU/35/2017
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": ['base', 'giscedata_facturacio', 'giscedata_contractacio_distri', 'giscedata_liquidacio'],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": ['wizard/wizard_crear_xml_suplement_etu35_2017_view.xml', 'giscedata_liquidacio_view.xml', 'security/ir.model.access.csv'],
  "active": False,
  "installable": True
}
