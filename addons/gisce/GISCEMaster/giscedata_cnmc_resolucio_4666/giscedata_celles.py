# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCellesCella(osv.osv):
    _name = 'giscedata.celles.cella'
    _inherit = 'giscedata.celles.cella'

    def _4666_idenfificador(self, cursor, uid, ids, name, args, context=None):
        """
        Method that calculates the field funciont identificador_4666

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Celles identifier
        :type ids: list of int
        :param name: Field name(unused)
        :param args: Arguments(unused)
        :param context: OpenERP conetext
        :return: identificadors used on 4666
        :rtype: dict {int: str}
        """

        ret = dict.fromkeys(ids, False)
        if context is None:
            context = {}

        celles = self.read(cursor, uid, ids, ["name"], context)
        for cel in celles:
            ret[cel["id"]] = cel["name"]
        return ret

    _columns = {
        "4666_identificador": fields.function(
            _4666_idenfificador, type='char',
            method=True, string="Identificador CNMC 4666"),
    }


GiscedataCellesCella()