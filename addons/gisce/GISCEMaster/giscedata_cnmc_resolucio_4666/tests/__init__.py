# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
import base64


class TestLoad(testing.OOTestCase):

    def test_load_n(self):
        """
        Tests the load of the N year

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.maxDiff = None

            model = self.openerp.pool.get('wizard.generar.cnmc.resolucio.4666')
            cts_obj = self.openerp.pool.get('giscedata.cts')

            wizard_id = model.create(cursor, uid, {})
            wizard_load_n = model.browse(cursor, uid, wizard_id)
            fixture_4666_url = get_module_resource(
                'giscedata_cnmc_resolucio_4666', 'tests', 'fixtures',
                '4666_fixture.zip'
            )
            with open(fixture_4666_url) as f:
                data_n = f.read()
            wizard_data = {"file_n": base64.encodestring(data_n)}
            wizard_load_n.write(wizard_data)
            wizard_load_n.load_n()

            ct_id = cts_obj.search(cursor, uid, [("name", "=", "Subestacio 1")])
            data = cts_obj.read(cursor, uid, ct_id[0], ["4666_entregada_2019"])
            self.assertDictEqual(
                {u'participacion': u'0.000', u'fecha_aps': u'12/01/2001',
                 u'cini': u'I21242H0', u'estado': 0,
                 u'identificador': u'Subestacio 1', u'codigo_ccaa': 9,
                 u'denominacion': u'SE GIRONA', u'fecha_baja': u'',
                 u'posiciones': 16}, data["4666_entregada_2019"]
            )


    def test_load_n1(self):
        """
        Tests the load of the N-1 year

        :return: None
        """

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.maxDiff = None

            model = self.openerp.pool.get('wizard.generar.cnmc.resolucio.4666')
            cts_obj = self.openerp.pool.get('giscedata.cts')

            wizard_id = model.create(cursor, uid, {})
            wizard_load_n = model.browse(cursor, uid, wizard_id)
            fixture_4666_url = get_module_resource(
                'giscedata_cnmc_resolucio_4666', 'tests', 'fixtures',
                '4666_fixture.zip'
            )
            with open(fixture_4666_url) as f:
                data_n = f.read()
            wizard_data = {"file_n1": base64.encodestring(data_n)}
            wizard_load_n.write(wizard_data)
            wizard_load_n.load_n1()

            ct_id = cts_obj.search(cursor, uid, [("name", "=", "Subestacio 1")])
            data = cts_obj.read(cursor, uid, ct_id[0], ["4666_entregada_2018"])
            self.assertDictEqual(
                {u'participacion': u'0.000', u'fecha_aps': u'12/01/2001',
                 u'cini': u'I21242H0', u'estado': 0,
                 u'identificador': u'Subestacio 1', u'codigo_ccaa': 9,
                 u'denominacion': u'SE GIRONA', u'fecha_baja': u'',
                 u'posiciones': 16}, data["4666_entregada_2018"]
            )



