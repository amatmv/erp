# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import datetime
import os
from tools import config
from StringIO import StringIO
import csv

from osv import osv, fields
import tools
from tools.translate import _
from datetime import date
from zipfile import ZipFile, BadZipfile

from libcnmc.models import F1Res4666, F2Res4666, F3Res4666, F4Res4666
from libcnmc.models import F5Res4666, F6Res4666, F7Res4666, F8Res4666

import netsvc


OPCIONS = [
    ('lat', 'LAT CSV (1)'),
    ('lbt', 'BT CSV (2)'),
    ('sub', 'Subestacions CSV (3)'),
    ('pos', 'Posicions CSV (4)'),
    ('maq', 'Maquines CSV (5)'),
    ('con', 'Condensadors CSV(5)'),
    ('desp', 'Despatx CSV (6)'),
    ('fia', 'Fiabilitat CSV (7)'),
    ('cts', 'CTS CSV (8)'),
    ('mod', 'Modificacions')
]


class WizardGenerarCnmcResolucio4666(osv.osv_memory):
    """
    Wizard per generar els XML de CNMC de inventari
    """

    _name = 'wizard.generar.cnmc.resolucio.4666'
    _max_hours = 10
    logger = netsvc.Logger()

    def add_status(self, cursor, uid, ids, status):
        wizard = self.browse(cursor, uid, ids[0], None)
        if wizard.status:
            status = wizard.status + '\n' + status
        wizard.write({'status': status})

    def _load_4666(self, cursor, uid, ids, csv_data, res_file, dest_field, context=None):
        """
        Private method to load the 4666 file in the database

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Affected ids
        :param csv_data: csv data of the file
        :param res_file: Report rile name ex f1
        :param context: OpenERP context
        :return: True if its loaded properly
        """
        if res_file == 'f1':
            model = 'giscedata.at.tram'
            cnmc_obj = F1Res4666
        elif res_file == 'f2':
            model = 'giscedata.bt.element'
            cnmc_obj = F2Res4666
        elif res_file == 'f3':
            model = 'giscedata.cts.subestacions'
            cnmc_obj = F3Res4666
        elif res_file == 'f4':
            model = 'giscedata.cts.subestacions.posicio'
            cnmc_obj = F4Res4666
        elif res_file == 'f5':
            model = 'giscedata.transformador.trafo'
            cnmc_obj = F5Res4666
        elif res_file == 'f6':
            model = 'giscedata.despatx'
            cnmc_obj = F6Res4666
        elif res_file == 'f7':
            model = 'giscedata.celles.cella'
            cnmc_obj = F7Res4666
        elif res_file == 'f8':
            model = 'giscedata.cts'
            cnmc_obj = F8Res4666
        model = self.pool.get(model)

        # Avoid overwritte the value set on the SE.
        if res_file == 'f8':
            model_sub = self.pool.get("giscedata.cts.subestacions")
            sub_ids = model_sub.search(cursor, uid, [])
            excluded_ids = model_sub.read(cursor, uid, sub_ids, ['ct_id'])
            excluded_ids = [x['ct_id'][0] for x in excluded_ids]
            model_ids = model.search(
                cursor, uid, [('id', 'not in', tuple(excluded_ids))]
            )
            model.write(cursor, uid, model_ids, {dest_field: False})
        else:
            model_ids = model.search(cursor, uid, [])
            model.write(cursor, uid, model_ids, {dest_field: False})

        csv_file = StringIO(csv_data)
        reader = csv.reader(csv_file, delimiter=';', quotechar='|')
        for line in reader:
            if line:
                cnmc = cnmc_obj(*line)
                name = cnmc.ref
                try:
                    identifier = model.search(
                        cursor, uid, [('name', '=', name)], 0, 0, False,
                        {'active_test': False})
                except ValueError:
                    if cnmc_obj == F5Res4666:
                        model = self.pool.get('giscedata.condensadors')
                        identifier = model.search(
                            cursor, uid, [('name', '=', name)], 0, 0, False,
                            {'active_test': False})
                    else:
                        raise ValueError
                try:
                    model.write(
                        cursor,
                        uid, identifier,
                        {dest_field: cnmc.dump()},
                        context=context)
                except UnicodeDecodeError:
                    self.add_status(cursor, uid, ids,
                                    _("Error de codificacio, els fitxers han d'esta en UTF-8"))
                    return False
        return True

    def load_n1(self, cursor, uid, ids, context=None):
        """
        Loads the n-1(4666) file into the model

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: Boolean
        """

        n1_year = datetime.datetime.now().year - 1
        wizard = self.browse(cursor, uid, ids[0], context)
        data_zip = base64.decodestring(wizard.file_n1)
        s = StringIO(data_zip)
        input_zip = ZipFile(s)
        try:
            filenames = sorted(input_zip.namelist())
        except BadZipfile:
            self.logger.notifyChannel(
                'load_n-1', netsvc.LOG_WARNING,
                _('Fitxer corrupte'))
            self.add_status(cursor, uid, ids, _('Fitxer corrupte'))
            return False
        result = False
        if filenames:
            for filename in filenames:
                csv_data = input_zip.read(filename)
                if filename.split('_')[-1] == '1.txt':
                    result = self._load_4666(
                        cursor, uid, ids, csv_data, 'f1',
                        "4666_entregada_{}".format(n1_year), context=context)
                    continue
                elif filename.split('_')[-1] == '2.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f2',
                        "4666_entregada_{}".format(n1_year), context=context)
                elif filename.split('_')[-1] == '3.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f3',
                        "4666_entregada_{}".format(n1_year), context=context)
                elif filename.split('_')[-1] == '4.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f4',
                        "4666_entregada_{}".format(n1_year), context=context)
                elif filename.split('_')[-1] == '5.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f5',
                        "4666_entregada_{}".format(n1_year), context=context)
                elif filename.split('_')[-1] == '6.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f6',
                        "4666_entregada_{}".format(n1_year), context=context)
                elif filename.split('_')[-1] == '7.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f7',
                        "4666_entregada_{}".format(n1_year), context=context)
                elif filename.split('_')[-1] == '8.txt':
                    self._load_4666(
                        cursor, uid, ids, csv_data, 'f8',
                        "4666_entregada_{}".format(n1_year), context=context)
        else:
            self.logger.notifyChannel(
                'load_n-1', netsvc.LOG_WARNING,
                _('No hi ha fitxers en el ZIP'))
            self.add_status(cursor, uid, ids, _('No hi ha fitxers en el ZIP'))
            pass

        if not result:
            self.add_status(cursor, uid, ids,
                            _("No s'han carregat correctament"))
        else:
            self.add_status(cursor, uid, ids,
                            _("S'han carregat correctament"))
        return True

    def load_n(self, cursor, uid, ids, context=None):
            """
            Loads the n(4666) file into the model

            :param cursor: Database cursor
            :param uid: User id
            :param ids: Afected ids
            :param context: OpenERP context
            :return: Boolean
            """
            n_year = datetime.datetime.now().year
            wizard = self.browse(cursor, uid, ids[0], context)
            data_zip = base64.decodestring(wizard.file_n)
            s = StringIO(data_zip)
            input_zip = ZipFile(s)
            try:
                filenames = sorted(input_zip.namelist())
            except BadZipfile:
                self.logger.notifyChannel(
                    'load_4666', netsvc.LOG_WARNING,
                    _('Fitxer corrupte'))
                self.add_status(cursor, uid, ids, _('Fitxer corrupte'))
                return False
            result = False
            if filenames:
                for filename in filenames:
                    csv_data = input_zip.read(filename)
                    if filename.split('_')[-1] == '1.txt':
                        result = self._load_4666(
                            cursor, uid, ids, csv_data, 'f1',
                            "4666_entregada_{}".format(n_year), context=context)
                        continue
                    elif filename.split('_')[-1] == '2.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f2',
                            "4666_entregada_{}".format(n_year), context=context)
                    elif filename.split('_')[-1] == '3.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f3',
                            "4666_entregada_{}".format(n_year), context=context)
                    elif filename.split('_')[-1] == '4.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f4',
                            "4666_entregada_{}".format(n_year), context=context)
                    elif filename.split('_')[-1] == '5.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f5',
                            "4666_entregada_{}".format(n_year), context=context)
                    elif filename.split('_')[-1] == '6.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f6',
                            "4666_entregada_{}".format(n_year), context=context)
                    elif filename.split('_')[-1] == '7.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f7',
                            "4666_entregada_{}".format(n_year), context=context)
                    elif filename.split('_')[-1] == '8.txt':
                        self._load_4666(
                            cursor, uid, ids, csv_data, 'f8',
                            "4666_entregada_{}".format(n_year), context=context)
            else:
                self.logger.notifyChannel(
                    'load_n', netsvc.LOG_WARNING,
                    _('No hi ha fitxers en el ZIP'))
                self.add_status(cursor, uid, ids, _('No hi ha fitxers en el ZIP'))
                pass

            if not result:
                self.add_status(cursor, uid, ids,
                                _("No s'han carregat correctament"))
            else:
                self.add_status(cursor, uid, ids,
                                _("S'han carregat correctament"))
            return True

    def action_exec_script(self, cursor, uid, ids, context=None):
        """
        Llença l'script per l'informe de l'inventari.

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP cursor
        :return: None
        """

        script_name = {
            'lat': ['res_4666_lat', 'LAT CSV'],
            'lbt': ['res_4666_lbt', 'BT CSV'],
            'sub': ['res_4666_sub', 'Subestacions CSV'],
            'pos': ['res_4666_pos', 'Posicions CSV'],
            'maq': ['res_4666_maq', 'Maquina CSV'],
            'desp': ['res_4666_des', 'Despatx CSV'],
            'fia': ['res_4666_fia', 'Fiabilitat CSV'],
            'cts': ['res_4666_cts', 'CTS CSV'],
            'con': ['res_4666_con', 'Condensadors'],
            'mod': ['res_4666_mod', 'Modificacions'],
                       }

        wizard = self.browse(cursor, uid, ids[0], context)

        # Agafar l'script que seleccionem al tipus
        exe = script_name.get(wizard.tipus)[0]

        # Crido la llibreria de CNMC
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069
        filename = tempfile.mkstemp()[1]
        filename_mod = tempfile.mkstemp()[1]
        args = ['cnmc', exe, '-d', cursor.dbname, '-p', port, '-u', user.login,
                '-w', user.password, '--no-interactive', '-o', filename, '-m', filename_mod,
                '-c', wizard.r1, '-y', wizard.anyo]
        if wizard.inc_emb:
            args.append('--embarrats')
        if wizard.tipus == 'lat' and wizard.dividir_multiples:
            args += ['--div']
        if wizard.tipus in ('lat', 'fia'):
            args += ['-pf', wizard.trams_at_prefix]
        if wizard.tipus == 'lbt':
            args += ['-pf', wizard.trams_bt_prefix]
        # Afegir la crida al log
        logger = netsvc.Logger()
        logger.notifyChannel(
            'server', netsvc.LOG_INFO,
            'libcnmc executed: {}'.format(' '.join(map(str, args)))
        )

        # Carrega el fitxer CSV
        env = os.environ.copy()
        sentry_dsn = config.get('sentry_dsn')
        if sentry_dsn:
            env['SENTRY_DSN'] = sentry_dsn
        env['PYTHONPATH'] = ':'.join([
            config['root_path'], config['addons_path']
        ])
        for k, v in config.options.iteritems():
            env['OPENERP_%s' % k.upper()] = str(v)
        retcode = subprocess.call(map(str, args), env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _('El procés de generar el fitxer ha fallat. Codi: %s')
                % retcode
            )

        tmpxmlfile = open(filename, 'r+')
        tmpxmlfile_mod = open(filename_mod, 'r+')

        report = base64.b64encode(tmpxmlfile.read())
        report_mod = base64.b64encode(tmpxmlfile_mod.read())
        filenom = ''
        if wizard.tipus == 'lat':
            filenom = 'INVENTARIO_R1-{}_1.txt'.format(wizard.r1)
        elif wizard.tipus == 'lbt':
            filenom = 'INVENTARIO_R1-{}_2.txt'.format(wizard.r1)
        elif wizard.tipus == 'sub':
            filenom = 'INVENTARIO_R1-{}_3.txt'.format(wizard.r1)
        elif wizard.tipus == 'pos':
            filenom = 'INVENTARIO_R1-{}_4.txt'.format(wizard.r1)
        elif wizard.tipus == 'maq':
            filenom = 'INVENTARIO_R1-{}_5.txt'.format(wizard.r1)
        elif wizard.tipus == 'desp':
            filenom = 'INVENTARIO_R1-{}_6.txt'.format(wizard.r1)
        elif wizard.tipus == 'fia':
            filenom = 'INVENTARIO_R1-{}_7.txt'.format(wizard.r1)
        elif wizard.tipus == 'cts':
            filenom = 'INVENTARIO_R1-{}_8.txt'.format(wizard.r1)
        elif wizard.tipus == 'con':
            filenom = 'INVENTARIO_R1-{}_9.txt'.format(wizard.r1)
        elif wizard.tipus == "mod":
            filenom = 'MODIFICACIONES_R1-{}.txt'.format(wizard.r1)

        wizard.write({
            'name': filenom,
            'file': report,
            "file_modificacions": report_mod,
            'state': 'done'
        })
        os.unlink(filename)
        os.unlink(filename_mod)


    def action_another(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)

        wizard.write({'state': 'init'})

    def _default_r1(self, cursor, uid, context=None):
        """
            Gets R1 code from company if defined

        :param cursor: Database cursor
        :param uid: User id
        :param context: OpenERP Context
        :return: R1 code as string or empty string
        """
        if not context:
            context = {}

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref2

        if r1 and r1.startswith('R1-'):
            r1 = r1[3:]

        return r1 or ''

    _columns = {
        'state': fields.selection([('init', 'Init'),
                                   ('done', 'Done'), ],
                                  'State'),
        'name': fields.char('File name', size=64),
        'file': fields.binary('Fitxer generat'),
        'file_modificacions': fields.binary('Fitxer explicació modificacions',help="Fitxer que explica els registres amb estat '1'"),
        'r1': fields.char('Codi R1', size=12,
                          help=u'Agafa automàticament el camp ref2 de la '
                               u'Empresa configurada a la Companyia'),
        'tipus': fields.selection(OPCIONS, 'Tipus', required=True),
        "inc_emb": fields.boolean("Incluir embarrados"),
        'anyo': fields.integer('Any', size=4),
        'file_n1': fields.binary('Fitxer entregar en n-1'),
        'file_n': fields.binary('Fitxer entregar en n'),
        'csv_lat': fields.binary('Fitxer LAT'),
        'csv_lbt': fields.binary('Fitxer BT'),
        'csv_sub': fields.binary('Fitxer Subestacions'),
        'csv_pos': fields.binary('Fitxer Posicions'),
        'csv_maq': fields.binary('Fitxer Màquines'),
        'csv_desp': fields.binary('Fitxer Despatx'),
        'csv_fia': fields.binary('Fitxer Fiabilitat'),
        'csv_trans': fields.binary('Fitxer CTS'),
        'status': fields.text(_('Resultat')),
        'trams_at_prefix': fields.char(
            'Prefixs Trams AT', size=10, required=True
        ),
        'trams_bt_prefix': fields.char(
            'Prefixs Trams BT', size=10, required=True
        ),
        'dividir_multiples': fields.boolean(
            "Repartir Longitud Núm. Circuits",
            help="A l'activar aquesta opció es dividirà la longitud del tram "
                 "pel núm. de circuits que tingui"
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'tipus': lambda *a: 'lat',
        'anyo': lambda *a: date.today().year - 1,
        'r1': _default_r1,
        'trams_at_prefix': lambda *a: 'A',
        'trams_bt_prefix': lambda *a: 'B',
        'dividir_multiples': lambda *a: False,
    }


WizardGenerarCnmcResolucio4666()
