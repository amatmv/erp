# -*- coding: utf-8 -*-

from osv import osv


class GiscedataTransformadorTrafo(osv.osv):
    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'

    def get_codi_instalacio_xmlrpc(self, cursor, uid, ids, context=None):
        res = self.get_codi_instalacio(cursor, uid, ids, context)

        return [(str(k), v) for k, v in res.items()]

    def get_codi_instalacio(self, cursor, uid, ids, context=None):
        res = {}

        trafo_fields = ['reductor', 'state', 'cini', 'tensio_primari_actual',
                        'tensio_b1', 'tensio_b2', 'tensio_b3', 'localitzacio']

        loc_obj = self.pool.get('giscedata.transformador.localitzacio')

        for trafo in self.read(cursor, uid, ids, trafo_fields, context):
            codi = 0
            trafo_id = trafo['id']
            localitzacio = loc_obj.read(cursor, uid, trafo['localitzacio'][0],
                                        ['code'], context)
            if not trafo['reductor']:
                codi = 165
            # Aquest cini es el de les maquines amb estat reserva, posarem codi
            # 0 igual que en l'estat. El codi 4 de localitzacio es magatzem.
            elif (trafo['state'] == 'reserva' or trafo['cini'] == 'I290060'
                  or localitzacio['code'] == 4):
                codi = 0
            else:
                # tensio primaria
                try:
                    bt = int(max([trafo['tensio_b1'] or 0,
                                  trafo['tensio_b2'] or 0,
                                  ]))
                except Exception:
                    res[trafo_id] = ''
                    continue
                at = int(trafo['tensio_primari_actual'] or 0)
                if at == 220000:
                    # tensio secundaria
                    if 110000 <= bt < 220000:
                        codi = 159
                    if 36000 <= bt < 110000:
                        codi = 160
                    if 1000 <= bt < 36000:
                        codi = 161
                # tensio primaria
                if 110000 <= at <= 132000:
                    # tensio secundaria
                    if 36000 <= bt < 110000:
                        codi = 162
                    if 1000 <= bt < 36000:
                        codi = 163
                # tensio primaria
                if 36000 <= at <= 66000:
                    codi = 164
                # tensio primaria
                if 1000 <= at < 36000:
                    codi = 165

            res[trafo_id] = 'TI-%s' % ('%s' % codi).zfill(3)

        return res


GiscedataTransformadorTrafo()
