# -*- coding: utf-8 -*-

from osv import osv


class GiscedataBtElement(osv.osv):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def get_codi_instalacio_xmlrpc(self, cursor, uid, ids, context=None):
        res = self.get_codi_instalacio(cursor, uid, ids, context)

        return [(str(k), v) for k, v in res.items()]

    def get_codi_instalacio(self, cursor, uid, ids, context=None):
        res = {}

        tram_fields = ['cini']

        cnmc_tipus_obj = self.pool.get('giscedata_cnmc.tipo_instalacion')

        for tram in self.read(cursor, uid, ids, tram_fields):
            tram_id = tram['id']

            tipus_id = cnmc_tipus_obj.search(cursor, uid,
                                             [('cini', '=', tram['cini'])],
                                             context)
            if tipus_id:
                tipus_vals = cnmc_tipus_obj.read(cursor, uid, tipus_id[0],
                                                 ['codi'], context)
                if tipus_vals:
                    codi = 'TI-' + ('%s' % tipus_vals['codi']).zfill(3)
                else:
                    codi = ' '

                res[tram_id] = codi

        return res

GiscedataBtElement()
