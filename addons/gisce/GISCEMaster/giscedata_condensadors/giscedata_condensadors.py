# -*- coding: utf-8 -*-
from osv import osv, fields
import time
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI


class GiscedataCondensadors(osv.osv):

    _name = 'giscedata.condensadors'
    _description = "Condensadors"

    def _get_element_nobloc_ti(self, cursor, uid, ids, context=None):
        """
        Function that indicates when the tipus installacio is locked

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: List of non locked elements
        """
        search_params = [
            ('bloquejar_cnmc_tipus_install', '!=', 1),
            ('id', 'in', ids)
        ]
        return self.search(cursor, uid, search_params, context=context)

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name,
                                  arg, context=None):
        """
        Function that handles the assignation of the tipus instalacio

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param field_name: Name of the modified element
        :param arg:
        :param context: OpenERP Context
        :return: dict of condensador id , tipus installacio id
        """
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import Condensador, Reactancia

        for cond in self.browse(cursor, uid, ids, context=context):
            if cond.bloquejar_cnmc_tipus_install:
                res[cond.id] = cond.tipus_instalacio_cnmc_id.id
            if cond.tipus == '1':
                ti = Reactancia()
            else:
                ti = Condensador()
            ti.tension = cond.tensio_id.tensio/1000.0
            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                ti_ids = ti_obj.search(cursor, uid, [
                    ('name', '=', ti.tipoinstalacion)
                ])
                if ti_ids:
                    res[cond.id] = ti_ids[0]
                else:
                    mess = 'TI {0} not found in giscedata.tipus.installacio'
                    raise mess.format(ti.tipoinstalacion)
        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value,
                                      args, context=None):
        """
        Function that handles the assignation of the tipus_instalacio_cnmc_id

        :param cursor: Database cursor
        :param uid: user id
        :param ids: Afected ids
        :param name: Name of the tipus instalacio
        :param value: Id of the tipus instalacio
        :param args:
        :param context: OpenERP context
        :return: True
        """
        if not context:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        sql = """
        UPDATE giscedata_condensadors SET tipus_instalacio_cnmc_id=%s
        WHERE id IN %s AND bloquejar_cnmc_tipus_install
        """
        cursor.execute(sql, (value or None, tuple(ids)))
        return True

    def unlink(self, cursor, uid, ids, context=None):
        """
        Function that manages the deletion of the model

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param context: OpenERP context
        :return:
        """

        if context is None:
            context = {}
        if context.get('permanent'):
            parcs = super(GiscedataCondensadors, self)
            return parcs.unlink(cursor, uid, ids, context)
        else:
            no_data_baixa = []
            for element in self.read(cursor, uid, ids, ['data_baixa']):
                if not element['data_baixa']:
                    no_data_baixa.append(element['id'])
                self.write(cursor, uid, ids, {'active': 0})
                if no_data_baixa:
                    self.write(cursor, uid, no_data_baixa, {
                        'data_baixa': time.strftime('%Y-%m-%d')
                    })
            return True

    def write(self, cursor, uid, ids, vals, context=None):
        """
        Function that manages the write of the model

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param vals: values
        :param context: OpenERP context
        :return:
        """
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        if 'cini' in vals:
            lst_cond = self.read(cursor, uid, ids, ['bloquejar_cini'])
            for condensador in lst_cond:
                # En el cas que desbloquejem i escrivim el CINI alhora.
                condensador.update(vals)
                if condensador['bloquejar_cini']:
                    del vals['cini']
                    break
        s_obj = super(GiscedataCondensadors, self)
        return s_obj.write(cursor, uid, ids, vals, context)

    _columns = {
        'name': fields.char('Codi', 25, required=True),
        'ct_id': fields.many2one('giscedata.cts', 'CT', required=True),
        'tensio_id': fields.many2one(
            'giscedata.tensions.tensio', 'Tensio(V)', required=True),
        'potencia_instalada': fields.float(
            'Potencia instal·lada(KVAr)', required=True),
        'serial': fields.many2one(
            'stock.production.lot', 'Número de sèrie', select='1',
            required=True),
        'product_id': fields.related(
            'serial', 'product_id', type='many2one',
            relation='product.product', store=True,
            readonly=True, string='Marca i model'),
        'data_pm': fields.date('Data APM', required=True),
        'active': fields.boolean('Actiu'),
        'data_baixa': fields.date('Data baixa'),
        'cini': fields.char('CINI', size=8, required=True),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'propietari': fields.boolean('Propietari'),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string="Tipus d'instal·lació",
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={'giscedata.condensadors': (
                _get_element_nobloc_ti, [
                    'tensio_id', 'tipus', 'bloquejar_cnmc_tipus_install'
                ], 10
            )
            }),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            'Bloquejar TI', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        'observacions': fields.text('Observacons'),
        'participacio': fields.integer('% pagat per la companyia'),
        'tipus': fields.selection(
            [('1', 'Reactancia'), ('2', 'Condensador')], 'Tipus',
            required=True),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019')
    }

    _defaults = {
        'active': lambda *a: 1,
        'propietari': lambda *a: 1,
        'bloquejar_cnmc_tipus_install': lambda *a: 0,
        "criteri_regulatori": lambda *a: "criteri"
    }


GiscedataCondensadors()
