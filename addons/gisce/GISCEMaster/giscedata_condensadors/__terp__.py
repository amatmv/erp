# -*- coding: utf-8 -*-
{
    "name": "Condensadors",
    "description": """
    * Permet gestionar els condensadors
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended_distri",
        "infraestructura",
        "stock",
        "giscedata_tensions",
        "giscedata_administracio_publica_cnmc_distri",
        "giscedata_celles",
        "giscedata_cts_subestacions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_condensadors_view.xml",
        "giscedata_cts_view.xml",
        "security/giscedata_condensadors_security.xml",
        "security/ir.model.access.csv",
        "giscedata_tipus_installacio_condensadors_data.xml"
    ],
    "active": False,
    "installable": True
}
