# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCts(osv.osv):

    _name = 'giscedata.cts'

    _inherit = 'giscedata.cts'
    _columns = {
        'condensadors_ids': fields.one2many(
            'giscedata.condensadors', 'ct_id', 'Condensadors'
        )
    }

GiscedataCts()
