Documentació del mòdul de condensadors i reactancies
====================================================

===========
Introducció
===========

Aquest mòdul permet el manteniment de la informacio relativa als condensadors i reactancies


========================
Introduccio de les dades
========================

* Omplir tos els camps en blau

.. image:: _static/imatge_1.png

* Per entrar el nuermo de serie:

  * Obrir la lupa i crear-ne un de nou
  * Si sabem el numero el posem, sino posem el mateix codi que el condensador

.. image:: _static/imatge_2.png

*  Si el producte "Condensador generic" no esta creat:

   * Producte nou
   * Crear la categoria "distribucio"
   * La resta dels camps no son obligatoris

.. image:: _static/imatge_3.png
