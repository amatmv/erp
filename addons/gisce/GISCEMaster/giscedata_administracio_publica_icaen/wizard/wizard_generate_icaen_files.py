# -*- encoding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
import base64
from tools import config
import netsvc
from collections import OrderedDict


class WizardICAENFiles(osv.osv_memory):
    """Assistent per exportar fitxers d'icaen."""
    _name = 'wizard.icaen.files'

    def _default_year(self, cursor, uid, context=None):
        return datetime.today().year

    def get_file_name(self, file_type):
        sql_name = {'1': 'tipo_1.sql',
                    '3': 'tipo_3.sql',
                    '4': 'tipo_4.sql',
                    }
        return sql_name.get(file_type, False)

    def export_file(self, cursor, uid, ids, context=None):
        wiz_fields = self.read(
            cursor, uid, ids, ['type', 'year'], context=context
        )[0]
        file_name = self.get_file_name(wiz_fields['type'])

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Començant l'exportació ICAEN")
        try:
            sql = open(
                '%s/%s/sql/%s' % (
                    config['addons_path'], 'giscedata_administracio_publica_icaen',
                    file_name)
            ).read()

            if wiz_fields['type'] == '1':
                params = {'data_i': str(wiz_fields['year']) + '-01-01',
                          'data_f': str(wiz_fields['year']) + '-12-31'
                          }
            else:
                params = {'year_sol': str(wiz_fields['year'])}

            cursor.execute(sql, params)

            informe = []
            # remove duplicated rows
            lines = cursor.fetchall()
            lines = list(OrderedDict.fromkeys(lines))
            for line in lines:
                line = list(line)
                informe.append(';'.join([unicode(str(a) or '') for a in line]))
            informe = '\n'.join(informe)
            informe += '\n'
            mfile = base64.b64encode(informe.encode('utf-8'))
            filename = 'ICAEN_Tipo_%s_%s.csv' % (
                wiz_fields['type'], wiz_fields['year']
            )
            self.write(cursor, uid, ids, {
                'state': 'end',
                'file_name': filename,
                'file': mfile
            })
            logger.notifyChannel("addons", netsvc.LOG_INFO,
                                 "Finalitzant l'exportació ICAEN")
        except Exception, e:
            raise osv.except_osv(
                'Error',
                'S\'ha produit un error al realitzar '
                'les consultes:\n{0}'.format(e)
            )

    _columns = {
        'type': fields.selection(
            [
                ('1', '1 - Dades de subministrament a clients '
                      'amb potencia major o igual a 50kW'),
                ('3', "3 - Dades per tarifa d'accés a nivell municipal de "
                      "subministraments"),
                ('4', '4 - Dades agrupades per sectors d\'activitat '
                      'econòmica a nivell municipal de subministraments')],
            'Tipus', required=True, select=1),
        'year': fields.integer('Any', required=True, select=1),
        'file': fields.binary('Fitxer', readonly=1),
        'file_name': fields.text('Nom del fitxer'),
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End')],
            'State'
        ),
    }

    _defaults = {
        'year': _default_year,
        'state': lambda *a: 'init'
    }

WizardICAENFiles()
