SELECT
    p.name AS "1",
    c.name AS "2",
    tit.name AS "3",
    c.nv AS "4",
    m.ine AS "5",
    m.name AS "6",
    cnae.name AS "7",
    cnae.descripcio AS "8",
    '4: CNAE-2009' AS "9",
    t.name AS "10",
    t.name AS "11",
    pm.potencia AS "12",
    e.energia AS "13",
    com.ref AS "14",
    com.name AS "15",
    CASE WHEN least(p.data_alta, pm.data_inici) >= %(data_i)s
        THEN pm.data_inici
        ELSE null
    END as "16",
    CASE WHEN greatest(COALESCE(p.data_baixa, '3000-01-01'::DATE), pm.data_final) <= %(data_f)s
        THEN pm.data_final
        ELSE NULL
    END AS "17"
FROM
    giscedata_polissa_modcontractual pm
    LEFT JOIN giscedata_polissa p ON pm.polissa_id = p.id
    LEFT JOIN res_partner tit ON pm.titular = tit.id
    LEFT JOIN res_partner com ON pm.comercialitzadora = com.id
    LEFT JOIN giscedata_polissa_tarifa t ON pm.tarifa = t.id
    LEFT JOIN giscedata_cups_ps c ON pm.cups = c.id
    LEFT JOIN res_municipi m ON c.id_municipi = m.id
    LEFT JOIN res_country_state provincia ON m.state = provincia.id
    LEFT JOIN res_comunitat_autonoma comunitat ON provincia.comunitat_autonoma = comunitat.id
    LEFT JOIN giscemisc_cnae cnae ON cnae.id = p.cnae
    LEFT JOIN (
        SELECT SUM(inn.energia) AS energia,
        inn.polissa AS polissa,
        inn.partner_id AS partner_id
        FROM(
            SELECT f.energia_kwh AS energia,
            p.id AS polissa,
            i.date_invoice,
            i.partner_id
            FROM giscedata_facturacio_factura f
            INNER JOIN giscedata_polissa p ON f.polissa_id = p.id
            INNER JOIN account_invoice i ON f.invoice_id = i.id
            LEFT JOIN account_journal j ON i.journal_id = j.id
            WHERE i.date_invoice BETWEEN %(data_i)s AND %(data_f)s
            AND f.tipo_factura = '01'
            AND j.code LIKE 'ENERGIA%%'
        ) AS inn
        GROUP BY polissa, partner_id
    ) AS e ON e.polissa = p.id AND pm.comercialitzadora = e.partner_id
WHERE
    pm.potencia >= 50
    AND (pm.data_final >= %(data_i)s OR pm.data_final IS NULL)
    AND (pm.data_inici <= %(data_f)s)
    AND comunitat.codi = '09'
ORDER BY
    pm.polissa_id,
    pm.name
