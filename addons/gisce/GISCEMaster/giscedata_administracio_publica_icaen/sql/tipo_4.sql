SELECT
    final.municipi AS "1_codi_municipal",
    final.nom_municipi AS "2_nom_municipi",
    final.cnae AS "3_cnae",
    final.desc_cnae AS "4_descripcio_cnae",
    NULL AS "5_tipus_classificacio_cnae",
    COALESCE(COUNT(DISTINCT final.polissa_id), 0) AS "6_num_abonats",
    COALESCE(MAX(final.potencia), 0) AS "7_potencia_contractada_kWh",
    COALESCE(SUM(final.energia_consumida), 0) AS "8_energia_consumida",
    COALESCE(SUM(final.qty_pot), 0) AS "9_terme_potencia",
    COALESCE(SUM(final.qty_energia), 0) AS "10_terme_energia",
    COALESCE(SUM(final.qty_react), 0) AS "11_reactiva",
    COALESCE(SUM(final.qty_exces), 0) AS "12_excesos",
    0 AS "13_import_IESE",
    COALESCE(SUM(final.qty_lloguer), 0) AS "14_lloguer",
    COALESCE(SUM(iva), 0) AS "15_IVA",
    COALESCE(SUM(total), 0) AS "16_total_amb_impostos"
FROM (
    SELECT
        i.number,
        p.id AS polissa_id,
        mun.ine AS municipi,
        mun.name AS nom_municipi,
        cnae.intgr AS cnae,
        cnae.descripcio AS desc_cnae,
        ene.energia * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS energia_consumida,
        (
            SELECT MAX(pm.potencia)
            FROM giscedata_polissa_modcontractual pm
            LEFT JOIN giscedata_polissa pol ON (pol.id = pm.polissa_id)
            WHERE pol.id = p.id
        ) AS potencia,
        ene.qty * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS qty_energia,
        pot.qty * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS qty_pot,
        react.qty * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS qty_react,
        exces.qty * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS qty_exces,
        lloguer.qty * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS qty_lloguer,
        i.amount_tax * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS iva,
        i.amount_total * CASE WHEN i.type = 'out_refund' THEN -1 ELSE 1 END AS total,
        f.id AS factura_id
    FROM giscedata_facturacio_factura f
        INNER JOIN account_invoice i ON (i.id=f.invoice_id)
        INNER JOIN giscedata_polissa p ON (f.polissa_id = p.id)
        INNER JOIN giscedata_cups_ps cups ON (p.cups = cups.id)
        INNER JOIN res_municipi mun ON (cups.id_municipi = mun.id)
        LEFT JOIN res_country_state provincia ON mun.state = provincia.id
        LEFT JOIN res_comunitat_autonoma comunitat ON provincia.comunitat_autonoma = comunitat.id
        LEFT JOIN (
            SELECT
                lf.factura_id AS factura_id,
                SUM(li.quantity) AS energia,
                SUM(li.price_subtotal) AS qty
            FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE lf.tipus='energia'
            GROUP BY lf.factura_id
        ) AS ene ON (ene.factura_id=f.id)
        LEFT JOIN (
            SELECT
                lf.factura_id AS factura_id,
                SUM(li.price_subtotal) AS qty
            FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE lf.tipus='potencia'
            GROUP BY lf.factura_id
        ) AS pot ON (pot.factura_id=f.id)
        LEFT JOIN (
            SELECT
                lf.factura_id AS factura_id,
                SUM(li.price_subtotal) AS qty
            FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE lf.tipus='reactiva'
            GROUP BY lf.factura_id
        ) AS react ON (react.factura_id=f.id)
        LEFT JOIN (
            SELECT
                lf.factura_id AS factura_id,
                SUM(li.price_subtotal) AS qty
            FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE lf.tipus='exces_potencia'
            GROUP BY lf.factura_id
        ) AS exces ON (exces.factura_id=f.id)
        LEFT JOIN (
            SELECT
                lf.factura_id AS factura_id,
                SUM(li.price_subtotal) AS qty
            FROM giscedata_facturacio_factura_linia lf
            LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
            WHERE lf.tipus='lloguer'
            GROUP BY lf.factura_id
        ) AS lloguer ON (lloguer.factura_id=f.id)
        INNER JOIN account_journal j ON (i.journal_id = j.id)
        INNER JOIN giscedata_liquidacio_fpd fpd ON (f.periode_liquidacio = fpd.id)
        INNER JOIN giscemisc_cnae cnae ON (p.cnae = cnae.id)
    WHERE
        fpd.year = %(year_sol)s
        AND comunitat.codi = '09'
        AND i.journal_id NOT IN
        (
            SELECT res_id
            FROM ir_model_data
            WHERE
                module = 'giscedata_contractacio_distri'
                AND name LIKE 'journal_contractacio_distri%%'
        )
    ) final
GROUP BY
    final.municipi,
    final.nom_municipi,
    final.cnae,
    final.desc_cnae
ORDER BY final.cnae
