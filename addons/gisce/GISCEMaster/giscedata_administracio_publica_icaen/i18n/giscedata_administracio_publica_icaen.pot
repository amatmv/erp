# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_administracio_publica_icaen
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2019-08-12 14:48\n"
"PO-Revision-Date: 2019-08-12 14:48\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_administracio_publica_icaen
#: selection:wizard.icaen.files,state:0
msgid "End"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "                                     "
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Nom del fitxer:"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: model:ir.model,name:giscedata_administracio_publica_icaen.model_wizard_icaen_files
msgid "wizard.icaen.files"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: field:wizard.icaen.files,state:0
msgid "State"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Exportació de fitxers de l'ICAEN"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Exportar"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Cancel·lar"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: model:ir.module.module,description:giscedata_administracio_publica_icaen.module_meta_information
msgid "Modul per a la generació de fixer ICAEN"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Tancar"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: field:wizard.icaen.files,type:0
msgid "Tipus"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Any d'exportació:"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: model:ir.module.module,shortdesc:giscedata_administracio_publica_icaen.module_meta_information
msgid "GISCE ICAEN"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: model:ir.ui.menu,name:giscedata_administracio_publica_icaen.menu_admin_pub_icaen
msgid "ICAEN"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: field:wizard.icaen.files,file_name:0
msgid "Nom del fitxer"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: view:wizard.icaen.files:0
msgid "Tipus de fitxer:"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: selection:wizard.icaen.files,type:0
msgid "1 - Dades de subministrament a clients amb potencia major o igual a 50kW"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: selection:wizard.icaen.files,type:0
msgid "3 - Dades per tarifa d'accés a nivell municipal de subministraments"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: selection:wizard.icaen.files,type:0
msgid "4 - Dades agrupades per sectors d'activitat econòmica a nivell municipal de subministraments"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: selection:wizard.icaen.files,state:0
msgid "Init"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: field:wizard.icaen.files,year:0
msgid "Any"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: field:wizard.icaen.files,file:0
msgid "Fitxer"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: model:ir.ui.menu,name:giscedata_administracio_publica_icaen.menu_giscedata_icaen_files
msgid "Informació sobre la distribució d'energia elèctrica"
msgstr ""

#. module: giscedata_administracio_publica_icaen
#: model:ir.actions.act_window,name:giscedata_administracio_publica_icaen.action_giscedata_icaen_files_form
msgid "Exportació fitxers ICAEN"
msgstr ""

