# -*- coding: utf-8 -*-
{
    "name": "GISCE ICAEN",
    "description": """Modul per a la generació de fixer ICAEN""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_administracio_publica",
        "giscedata_facturacio",
        "giscedata_liquidacio",
        "giscedata_cups",
        "giscedata_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_administracio_publica_icaen_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
