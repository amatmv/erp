# -*- coding: utf-8 -*-

from osv import osv, fields

class giscedata_at_linia(osv.osv):

    _name = 'giscedata.at.linia'
    _inherit = 'giscedata.at.linia'

    def _tensio_selection(self, cr, uid, context={}):
        res = [
          (3150, '3150'),
          (5000, '5000'),
          (5250, '5250'),
          (20000, '20000'),
          (25000, '25000'),
          (40000, '40000'),
        ]
        return res

    _columns = {
      'tensio': fields.selection(_tensio_selection, 'Tensió'),
    }

giscedata_at_linia()
