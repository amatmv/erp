# -*- coding: utf-8 -*-
{
    "name": "AT Tensions Selection (EYPESA)",
    "description": """
Fa que el camp tensió de les línies AT sigui un camp 'selection'.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
