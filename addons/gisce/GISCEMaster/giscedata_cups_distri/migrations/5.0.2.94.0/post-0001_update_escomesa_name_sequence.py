# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Setting UP the Escomeses sequence for the field NAME.')

    sql = """
        UPDATE ir_sequence AS seq 
        SET number_next = data.num
        FROM (
          SELECT coalesce(max(esc.name::int),0)+1 AS num,
                 seq.id AS id
          FROM giscedata_cups_escomesa esc, ir_sequence seq
          WHERE seq.name='Numeració Escomesa'
          AND seq.code='giscedata.cups.escomesa'
          GROUP BY seq.id
        ) AS data
        WHERE seq.id=data.id
    """
    cursor.execute(sql)

    logger.info('Escomeses NAME sequence set up with the current value')


def down(cursor, installed_version):
    pass


migrate = up
