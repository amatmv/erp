# -*- coding: utf-8 -*-
{
    "name": "GISCE Data CUPS (Distribuidora)",
    "description": """Codi Universal de Punt de Provisionament""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CUPS",
    "depends":[
        "base",
        "c2c_webkit_report",
        "base_extended_distri",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_cups_distri_demo.xml",
        "giscedata_cups_distri_sequence_demo.xml"
    ],
    "update_xml":[
        "giscedata_cups_distri_view.xml",
        "giscedata_cups_distri_report.xml",
        "giscedata_cups_distri_wizard.xml",
        "giscedata_cups_distri_sequence.xml",
        "giscedata_cups_distri_data.xml",
        "res_company_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
