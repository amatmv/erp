# -*- coding: utf-8 -*-
"""Classes que modifiquen al res_company."""

from osv import fields, osv


class ResCompany(osv.osv):
    """Modifiquem el model res_company afegint-hi el codi de l'empresa per
    generar els CUPS."""
    _name = 'res.company'
    _inherit = 'res.company'

    _columns = {
      'cups_code': fields.char('Codi CUPS', size=4),
    }

ResCompany()
