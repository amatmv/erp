<%
    from datetime import datetime
    today = datetime.today().strftime('%d/%m/%Y %H:%M')
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en"><html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<html>
    <head>
    	<style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_cups_distri/report/estils.css"/>
    </head>
    <body>
        %for cups in objects:
            <div class="logo_container">
                <img class="logo" src="data:image/jpeg;base64,${company.logo}" align="right">
            </div>
                <div class="data">${_(u"Data:")} ${today}</div>
            <div style="clear: both;"></div>
            <div class="titol">${_(u"Fitxa del CUPS")}</div>
            <div style="clear: both;"></div>
            <div class="seccio mt_35" style="width: 70%;">
                <div class="titol_seccio">
                    ${_(u"Informació")}
                </div>
                <div class="contingut_seccio">
                    <table class="taula_dades">
                        <tr>
                            <td class="w_50">${_(u"CUPS: ")}</td>
                            <td class="field">${cups.name}</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_50">${_(u"Contracte: ")}</td>
                            <td class="field">${cups.polissa_polissa.name}</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_100">${_(u"Nº comptador: ")}</td>
                            <td class="field w_75">${cups.polissa_comptador}</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_140">${_(u"Potència contractada: ")}</td>
                            <td class="field">${cups.polissa_potencia} kW</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_50">${_(u"Titular: ")}</td>
                            <td class="field">${cups.titular}</td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="seccio mt_25">
                <div class="titol_seccio">
                    ${_(u"Adreça")}
                </div>
                <div class="contingut_seccio">
                    <table class="taula_dades">
                        <tr>
                            <td class="w_50">${_(u"Municipi: ")}</td>
                            <td class="field w_100">${cups.id_municipi.name}</td>
                            <td class="w_50">${_(u"Població: ")}</td>
                            <td class="field w_100">${cups.id_poblacio.name}</td>
                            <td class="w_50">${_(u"Provincia: ")}</td>
                            <td class="field w_100">${cups.id_provincia.name}</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_100">${_(u"Codi postal: ")}</td>
                            <td class="field w_82">${cups.dp}</td>
                            <td class="w_40">${_(u"Direcció: ")}</td>
                            <td class="field">${cups.direccio}</td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_90">${_(u"Ref. catastral: ")}</td>
                            <td class="field w_82">${cups.ref_catastral}</td>
                            <td class="w_30">${_(u"Zona: ")}</td>
                            <td class="field w_50">${cups.zona}</td>
                            <td class="w_30">${_(u"Ordre: ")}</td>
                            <td class="field w_50">${cups.ordre}</td>
                            <td></td>
                        </tr>
                    </table>
                    <table class="taula_dades">
                        <tr>
                            <td class="w_20">${_(u"ET: ")}</td>
                            <td class="field w_50">${cups.et}</td>
                            <td class="w_20">${_(u"Linia: ")}</td>
                            <td class="field w_20">${cups.linia}</td>
                            <td class="w_50">${_(u"Escomesa: ")}</td>
                            <td class="field w_55">${cups.id_escomesa.name}</td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="seccio mt_25">
                <div class="titol_seccio">
                    ${_(u"Altres")}
                </div>
                <div class="contingut_seccio">
                    <table class="taula_dades">
                        <tr>
                            <td>${_(u"Data:")}</td>
                        </tr>
                        <tr>
                            <td>${_(u"Sol·licitat per:")}</td>
                        </tr>
                        <tr>
                            <td>${_(u"Remès per:")}</td>
                        </tr>
                        <tr>
                            <td>${_(u"Potència sol·licitada:")}</td>
                        </tr>
                        <tr>
                            <td>${_(u"Tensió:")}</td>
                        </tr>
                    </table>
                </div>
            </div>
        %endfor
    </body>
</html>