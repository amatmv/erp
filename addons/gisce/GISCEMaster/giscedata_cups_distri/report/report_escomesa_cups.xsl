<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cupsescomesa-list"/>
  </xsl:template>

  <xsl:template match="cupsescomesa-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
        <blockTableStyle id="tablestyle">
          <lineStyle kind="GRID" colorName="silver"/>
        </blockTableStyle>
      </stylesheet>
      
      <story>
        <xsl:apply-templates select="cupsescomesa" mode="story"/>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="cupsescomesa" mode="story">
    <h1 t="1">Fitxa d'Escomesa</h1>
    <para style="parastyle" t="1">ID Escomesa : <xsl:text> </xsl:text><xsl:value-of select="name"/></para>
    <para style="parastyle" t="1">BlockName : <xsl:text> </xsl:text><xsl:value-of select="blockname"/></para>
    <para style="parastyle" t="1">Municipi : <xsl:text> </xsl:text></para>
    <para style="parastyle" t="1">N�mero de CUPS Associats : <xsl:text> </xsl:text><xsl:value-of select="ncups"/></para>

    <h2 t="1">CUPS Associats a l'Esomesa</h2>
    <blockTable style="tablestyle" colWidths="3.8cm,3.8cm,3.8cm,3.8cm,3.8cm">
    <tr>
      <td t="1">CUPS</td>
      <td t="1">Poblaci�</td>
      <td t="1">Municipi</td>
      <td t="1">N�mero</td>
      <td t="1">Escala</td>
    </tr>
    <xsl:apply-templates select="cups-item"/>
    </blockTable>
  </xsl:template>

  <xsl:template match="cups-item">
    <tr>
    <td><xsl:value-of select="cups-cups"/></td>
    <td><xsl:value-of select="cups-poblacio"/></td>
    <td><xsl:value-of select="cups-numero"/></td>
    <td><xsl:value-of select="cups-numero"/></td>
    <td><xsl:value-of select="cups-escala"/></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
