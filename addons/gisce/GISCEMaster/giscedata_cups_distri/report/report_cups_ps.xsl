<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cups-list"/>
  </xsl:template>

  <xsl:template match="cups-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
      </stylesheet>
      
      <story>
        <xsl:apply-templates select="cups" mode="story"/>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="cups" mode="story">
    <h1 t="1">Fitxa del CUPS</h1>
    <para style="parastyle" t="1">CUPS : <xsl:text> </xsl:text><xsl:value-of select="name"/></para>
    <para style="parastyle" t="1">Municipi : <xsl:text> </xsl:text><xsl:value-of select="municipi"/></para>
    <para style="parastyle" t="1">Poblaci� : <xsl:text> </xsl:text><xsl:value-of select="poblacio"/></para>
    <para style="parastyle" t="1">Carrer : <xsl:text> </xsl:text><xsl:value-of select="carrer"/></para>
    <para style="parastyle" t="1">N�mero : <xsl:text> </xsl:text><xsl:value-of select="numero"/></para>
    <para style="parastyle" t="1">Escala : <xsl:text> </xsl:text><xsl:value-of select="escala"/></para>

    <h2 t="1">Escomesa Associada</h2>
    <para style="parastyle" t="1">Escomesa : <xsl:text> </xsl:text><xsl:value-of select="escomesa_"/></para>
    <para style="parastyle" t="1">BlockName : <xsl:text> </xsl:text><xsl:value-of select="escomesa_blockname"/></para>
    <para style="parastyle" t="1">N. de CUPS Associats : <xsl:text> </xsl:text><xsl:value-of select="escomesa_ncups"/></para>
  </xsl:template>

</xsl:stylesheet>
