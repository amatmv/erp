<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cups-list"/>
  </xsl:template>

  <xsl:template match="cups-list">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>

      <stylesheet>
        <paraStyle name="titol" 
        	fontName="Helvetica-Bold"
        	fontSize="14"
        	leading="28"
        />
        
        <paraStyle name="para"
        	fontName="Helvetica"
        	fontSize="8"
        />
        
        <blockTableStyle id="taula">
        	<blockBackground colorName="grey" start="0,0" stop="-1,0" />
        	<blockFont name="Helvetica" size="8" />
        	<blockFont name="Helvetica-Bold" size="8" start="0,0" stop="-1,0"/>
        	<lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>
        
      </stylesheet>

      <story>
        <para style="titol" t="1">Llistat de CUPS</para>
        <blockTable style="taula" colWidths="1cm,1cm,1.4cm,4.5cm,5cm,1cm,1cm,1cm,1.4cm,1.7cm" repeatRows="1">
        	<tr>
        		<td t="1">Zona</td>
        		<td t="1">Ordre</td>
        		<td t="1">P�lissa</td>
        		<td t="1">Client</td>
        		<td t="1">Carrer</td>
        		<td t="1">N�m.</td>
        		<td t="1">Pis</td>
        		<td t="1">Porta</td>
        		<td t="1">ET</td>
        		<td t="1">Escomesa</td>
        	</tr>
	        <xsl:apply-templates select="cups" mode="story"/>
	    </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="cups" mode="story">
  	<tr>
		<td><para style="para"><xsl:value-of select="zona" /></para></td>
		<td><para style="para"><xsl:value-of select="ordre" /></para></td>
		<td><para style="para"><xsl:value-of select="polissa" /></para></td>
		<td><para style="para"><xsl:value-of select="client" /></para></td>
		<td><para style="para"><xsl:value-of select="carrer" /></para></td>
		<td><para style="para"><xsl:value-of select="numero" /></para></td>
		<td><para style="para"><xsl:value-of select="pis" /></para></td>
		<td><para style="para"><xsl:value-of select="porta" /></para></td>
		<td><para style="para"><xsl:value-of select="et" /></para></td>
		<td><para style="para"><xsl:value-of select="escomesa_name" /></para></td>
	</tr>
  </xsl:template>
</xsl:stylesheet>
