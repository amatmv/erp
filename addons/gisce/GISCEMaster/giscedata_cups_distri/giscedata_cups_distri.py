# -*- coding: utf-8 -*-
"""Classes pel mòdul giscedata_cups de distribució."""
import time
from osv import osv, fields
from tools.translate import _
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI


class GiscedataCupsBlockname(osv.osv):
    """Tipus d'escomesa."""
    
    def _get_next_code(self, cursor, uid, context=None):
        """Retorna el següent codi."""
        cursor.execute("""select code from giscedata_cups_blockname 
                        order by code desc limit 1""")
        codi = cursor.fetchone()
        if codi and len(codi):
            codi = int(codi[0]) + 1
        else:
            codi = 1
        return codi

    _name = 'giscedata.cups.blockname'
    _description = 'Tipus Escomesa'
    _columns = {
      'name': fields.char('Tipus Escomesa', size=10, required=True),
      'description': fields.char('Descripció', size=255),
      'code': fields.integer('Code'),
    }

    _defaults = {
        'code': _get_next_code,
    }

    _order = "name, id"

GiscedataCupsBlockname()

class GiscedataCupsEscomesa(osv.osv):

    def _get_nextnumber(self, cursor, uid, context=None):
        """Retorna el següent número d'escomesa corresponent."""

        return self.pool.get('ir.sequence').get(
            cursor, uid, 'giscedata.cups.escomesa'
        )

    def _ncups(self, cursor, uid, ids, name, arg, context=None):
        """Número de CUPS per escomesa."""
        res = {}
        ids = [int(id_tmp) for id_tmp in ids]
        # Inicialitzem tots els ids
        map(lambda id: res.setdefault(id, 0), ids)
        cursor.execute("""select id_escomesa,count(id) 
        from giscedata_cups_ps where id_escomesa in (%s) 
        group by id_escomesa""" % ','.join(map(str, ids)))
        for result in cursor.fetchall():
            res[result[0]] = result[1]
        return res

    def _nescomeses(self, cursor, uid, ids, name, arg,context=None):
        """Número d'escomeses."""
        res = {}
        for escomesa in self.browse(cursor, uid, ids, context=context):
            cursor.execute("""select count(id) as quantitat 
            from giscedata_cups_escomesa""")
            sqlresult = cursor.fetchall()
            for item in sqlresult:
                res[escomesa.id] = item[0]
        return res

    def _cupsnotassigned_write(self, cursor, uid, ids, name, value, arg,
                               context=None):
        """Escriu els CUPS sense assignar."""
        res = {}
        for cups in value:
            self.pool.get('giscedata.cups.ps').write(cursor, uid, cups[1],
                                                     cups[2])
        return res

    def read_distinct(self, cr, uid, values):
        """Retorna el valors que no coincideixen amb els passats.
        
        Aquesta funció s'utilitza a l'eina GESCOMESA de l'autocad.
        TODO: Treure-la a un mòdul de _cad."""
        cr.execute("""\
select 
  e.name,
  e.id,
  coalesce(e.id_municipi, 0) as id_municipi 
from 
  giscedata_cups_escomesa e 
where 
  e.name::int not in ("""+','.join(map(str, map(int, values)))+""")""")
        return cr.dictfetchall()

    def read_costumer(self, cursor, uid, values):
        """Retorna camps del client associat a una escomesa."""
        cursor.execute("""
SELECT 
  e.name AS escomesa, 
  c.name AS cups, 
  coalesce(c.zona, '') as zona, 
  coalesce(c.ordre, '') as ordre, 
  p.name AS polissa, 
  rp.name AS costumer, 
  coalesce(c.carrer, '') as carrer, 
  coalesce(c.numero , '') as numero, 
  coalesce(c.pis, '') as pis, 
  coalesce(c.porta, '') as porta, 
  coalesce(p.potencia, 0) as potencia 
FROM 
  giscedata_cups_escomesa e, 
  giscedata_cups_ps c, 
  giscedata_polissa p, 
  res_partner rp 
WHERE 
  e.id = c.id_escomesa 
  AND p.cups = c.id 
  AND rp.id = p.titular 
  AND e.name in ("""+','.join(map(str, map(int, values)))+""")""")
        return cursor.dictfetchall()

    def read_distinct2(self, cursor, uid, values):
        """Ens retorna els els elements que se'ns passen i que no existeixen
        a la base de dades.
        
        S'utilitza en el GESCOMESA."""
        in_array = []
        for value in values:
            cursor.execute("""\
select name from giscedata_cups_escomesa where name = %s""", (str(value),))
            if not cursor.fetchone():
                in_array.append(value)
        return in_array

    _name = 'giscedata.cups.escomesa'
    _description = "Escomeses"
    _columns = {
        'name': fields.char('Codi', size=10, required=True),
        'id_municipi': fields.many2one('res.municipi', 'Municipi'),
        'cups': fields.one2many('giscedata.cups.ps', 'id_escomesa', 'CUPS'),
        'blockname': fields.many2one('giscedata.cups.blockname', 'Tipus'),
        'ncups': fields.function(_ncups, method=True, type='integer',
                                 string='Numero de Cups'),
        'nescomeses': fields.function(_nescomeses, method=True,
                                      type='integer', string='Total Escomeses'),
        'active': fields.boolean('Activa', select=True),
        'ultima_x': fields.float('Ultima X'),
        'ultima_y': fields.float('Ultima Y')
    }

    _defaults = {
      'name': _get_nextnumber,
      'active': lambda *a: 1,
    }

    _order = "name asc"

    def change_active(self, cursor, uid, ids, active, context=None):
        """Comprova si es pot desactivar una escomesa."""
        if not active:
            for escomesa in self.browse(cursor, uid, ids, context):
                if len(escomesa.cups):
                    error_message = "No es pot desactivar l'escomesa %s degut a"
                    error_message += " que té %i CUPS actius.\n"
                    error_message += "Torni-la a activar."
                    error_message = error_message % (escomesa.name,
                                                     len(escomesa.cups))
                    raise osv.except_osv('Error', error_message)
        return {'value': {'active': active}}


    def write(self, cursor, uid, ids, vals, context=None):
        """Al escriure en una escomesa comprova si es vol desactiva i si té
        CUPS associats."""
        # Primer mirem si es vol modificar el camp 'active'
        #i si aquest es vol desactivar
        if vals.has_key('active') and not vals['active']:
            # Per cada escomesa hem de mirar si té cups.
            for escomesa in self.browse(cursor, uid, ids, context):
                if len(escomesa.cups):
                    error_message = _(u"No es pot desactivar l'escomesa %s "
                                      u"degut a que té %i CUPS actius.")
                    error_message = error_message % (escomesa.name,
                                                     len(escomesa.cups))
                    raise osv.except_osv('Error', error_message)
        return super(GiscedataCupsEscomesa, self).write(cursor, uid, ids, vals,
                                                        context)

    def unlink(self, cursor, uid, ids, context=None, check=True):
        """Abans d'eliminar comprova que l'escomesa no tingui CUPS associats."""
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        for cups_id in ids:
            cursor.execute("""
                select count(id) from giscedata_cups_ps 
                where id_escomesa = %s
            """, (cups_id,))
            if cursor.fetchone()[0] > 0:
                raise osv.except_osv(
                    'Error',
                    'No es pot eliminar una escomesa que té CUPS assignats.'
                )
            else:
                result = super(GiscedataCupsEscomesa, self).unlink(
                    cursor, uid, [cups_id], context=context
                )
        return result

    _sql_constraints = [
        ('name_unique', 'unique (name)',
         'Ya existe una Acometida con este número.')
    ]


GiscedataCupsEscomesa()


class GiscedataCupsPs(osv.osv):
    """Model de Punt de servei (CUPS)."""

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def whereiam(self, cursor, uid, context=None):
        '''retorna si estem a distri o a comer depenent de si el
            cups pertany a la nostra companyia o no'''
        return 'distri'

    _cups_checksum_table = 'TRWAGMYFPDXBNJZSQVHLCKE'

    def get_next_number(self, cursor, uid, ids, *args):
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        config_obj = self.pool.get('res.config')
        cups_from_seq = int(config_obj.get(cursor, uid, 'cups_from_seq', 0))
        if not user.company_id.cups_code:
            raise osv.except_osv("No es pot generar el codi del CUPS",
                                 """L'empresa seleccionada (%s) no té codi de\
                                  cups assignat, haurà de canviar a una \
                                  empresa que en tingui o entrar-hi \
                                  el corresponent""" % (user.company_id.name))
        if cups_from_seq:
            n_cups = self.pool.get('ir.sequence').get(
                cursor, uid, 'giscedata.cups.ps'
            )
        else:
            cursor.execute("""select (max(substring(name, 7, 12))::bigint)+1
                from giscedata_cups_ps
                where substring(name, 7, 12) similar to '[0-9]+'""")
            n_cups = str(cursor.fetchone()[0]).zfill(12)

        cupsname = '%s%s' % (user.company_id.cups_code, n_cups)
        checksum = self.gen_checksum(cursor, uid, cupsname)
        cups = 'ES%s%s0F' % (cupsname, checksum)

        if self.cups_exists(cursor, uid, cups):
            return self.get_next_number(cursor, uid, ids)
                
        return cups

    def onchange_auto(self, cursor, uid, ids, auto, cups, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if auto and not cups:
            res['value'].update({'name': self.get_next_number(cursor, uid, 
                                                              ids)})
        return res

    def init(self, cursor):
        """Funció que es crea quan s'inicialitza el model."""
        cursor.execute("""create or replace view giscedata_cups_ps_direccio as (
        select
          c.id as cups_id,
          coalesce(c.nv, '') || ' ' ||
          coalesce(c.pnp, '') || ' ' ||
          coalesce(c.es, '') || ' ' ||
          coalesce(c.pt, '') || ' ' ||
          coalesce(c.pu, '') || ' (' ||
          coalesce(p.name,'') || ')' as direccio
        from
          giscedata_cups_ps c
        left join
          res_poblacio p on (c.id_poblacio = p.id)
        )""")

    _us_ps = [
      ('V','Vivienda habitual'),
      ('N','Vivienda NO habitual'),
    ]

    _description = 'Punt de Servei'

    def default_distribuidora_id(self, cursor, uid, context=None):
        """Obtenim el valor per defecte de la distribuidora.
        """
        user = self.pool.get('res.users').browse(cursor, uid, uid, context)
        return user.company_id.partner_id.id

    def get_zones(self, cursor, uid, context=None):
        """Retorna les diferents zones.
        """
        cursor.execute("""SELECT distinct zona FROM giscedata_cups_ps""")
        return [zona[0] for zona in cursor.fetchall() if zona and zona[0]]

    def search(self, cr, user, args, offset=0, limit=None, order=None,
            context=None, count=False):
        """Funció per fer cerques per name exacte, enlloc d'amb 'ilike'.
        """
        if context is None:
            context = {}
        for idx, arg in enumerate(args):
            if len(arg) == 3:
                field, operator, match = arg
                if field == 'linia' and isinstance(match, (unicode, str)):
                    args[idx] = (field, '=', match)
        return super(GiscedataCupsPs, self).search(cr, user, args, offset,
                                                   limit, order, context, count)

    _columns = {
        'id_escomesa': fields.many2one('giscedata.cups.escomesa', 'Escomesa',
                                       ondelete='restrict'),
        'ordre': fields.char('Ordre', size=10),
        'zona': fields.char('Zona', size=10),
        'et': fields.char('ET', size=60),
        'linia': fields.char('Linea', size=60),
        'auto': fields.boolean('Auto numeració'),
        'distribuidora_id': fields.many2one('res.partner', 'Distribuidora',
                                            required=True),
        'cne_anual_activa':
            fields.float('Energía activa anual consumida (CNMC)',
                         readonly=True),
        'cne_anual_reactiva':
            fields.float('Energía reactiva anual consumida (CNMC)',
                         readonly=True),
        'cnmc_potencia_facturada': fields.float(
            'Potencia Facturada (CNMC)',
            readonly=True
        ),
        'cnmc_numero_lectures': fields.integer(
            'Número de lectures (CNMC)',
            readonly=True
        ),
        'potencia_conveni': fields.float('Potència conveni (kW)', digits=(16,3),
                                         help=('Potència per la qual está '
                                               'dissenyada la instal·lació')),
        'data_pm': fields.date('Data Connexió'),
        'data_baixa': fields.date('Data de baixa'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Forçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
    }

    _defaults = {
        #      'us_ps': lambda *a: 'N',
        'auto': lambda *a: 0,
        'distribuidora_id': default_distribuidora_id,
        'cne_anual_activa': lambda *a: 0.0,
        'cne_anual_reactiva': lambda *a: 0.0,
        "criteri_regulatori": lambda *a: "criteri"
    }

    _order = "active desc, name asc"

    def unlink(self, cursor, uid, ids, context=None, check=True):
        """Comprova que no hi hagi cap pòlissa activa en eliminar un CUPS."""
        for cups_id in ids:
            cursor.execute("""select count(id) from giscedata_polissa 
            where cups = %s""", (cups_id,))
            if cursor.fetchone()[0] > 0:
                raise osv.except_osv('Error', 
                                     "No es pot eliminar un CUPS que tingui\
                                      pòlisses assignades")
            else:
                result = super(GiscedataCupsPs, self).unlink(cursor, uid,
                                                             [cups_id],
                                                             context=context)
        return result

    _sql_constraints = [('name_unique', 'unique (name)',
                         'Ya existe un CUPS con este número.')]

GiscedataCupsPs()
