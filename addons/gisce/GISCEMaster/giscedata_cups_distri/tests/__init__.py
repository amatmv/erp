# coding=utf-8
from destral import testing


class TestCupsSequence(testing.OOTestCaseWithCursor):

    def test_get_nextnumber_by_sequence_fill_gaps(self):
        cfg_obj = self.openerp.pool.get('res.config')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.cursor
        uid = self.uid

        cfg_obj.set(cursor, uid, 'cups_from_seq', '1')

        cups = cups_obj.get_next_number(cursor, uid, [])
        self.assertEqual(
            cups,
            'ES9999000000000001DS0F'
        )

        created_cups = [
            'ES9999000000000002DQ0F',
            'ES9999000000000003DV0F',
            'ES9999000000000004DH0'
        ]

        id_municipi = imd_obj.get_object_reference(
            cursor, uid, 'base_extended', 'ine_17079'
        )[1]

        for cups_name in created_cups:

            cups_obj.create(cursor, uid, {
                'name': cups_name,
                'id_municipi': id_municipi
            })

        cups = cups_obj.get_next_number(cursor, uid, [])
        next_cups = '9999000000000005'
        checksum = cups_obj.gen_checksum(cursor, uid, next_cups)

        self.assertEqual(
            cups,
            'ES{}{}0F'.format(next_cups, checksum)
        )
