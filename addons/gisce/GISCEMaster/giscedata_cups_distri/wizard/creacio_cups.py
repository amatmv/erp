# -*- coding: utf-8 -*-
"""Wizard per la creació automàtica de CUPS."""
import wizard
import pooler


class WizardCreacioCups(wizard.interface):
    """Wizard que ens permet crear N CUPS."""

    def _crear(self, cursor, uid, data, context=None):
        """Creem els CUPS."""
        pool = pooler.get_pool(cursor.dbname)
        cups_obj = pool.get('giscedata.cups.ps')
        i = 0
        ids = []
        qty = data['form']['qty']
        while i < qty:
            vals = {
                'name': cups_obj.get_next_number(cursor, uid, None),
                'id_municipi': data['form']['id_municipi']
            }
            cups_id = cups_obj.create(cursor, uid, vals)
            ids.append(cups_id)
            i += 1

        action = {
          'domain': "[('id', 'in', [%s])]" % ','.join(map(str, map(int, ids))),
                      'name': 'CUPS',
                      'view_type': 'form',
                      'view_mode': 'tree,form',
                      'res_model': 'giscedata.cups.ps',
                      'view_id': False,
                      'limit': len(ids),
                      'type': 'ir.actions.act_window'
        }
        return action
    _main_form = """<?xml version="1.0"?>
<form string="Creació de CUPS" >
  <field name="qty" />
  <field name="id_municipi" />
</form>
"""

    _main_fields = {
        'qty': {'string': 'Quantitat', 'type': 'integer'},
        'id_municipi': {'string': 'Municipi', 'type': 'many2one',
                        'relation': 'res.municipi', 'required': True}
    }

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'state', 'state': 'main'}
      },
      'main': {
          'actions': [],
          'result': {'type': 'form', 'arch': _main_form,
                     'fields': _main_fields,
                     'state': [('crear', 'Crear', 'gtk-ok'),
                               ('end', 'Cancel·lar', 'gtk-cancel')]}
      },
      'crear': {
          'actions': [],
          'result': {'type': 'action', 'action': _crear, 'state': 'end'}
      },
    }

WizardCreacioCups('giscedata.cups.creacio')
