# -*- coding: utf-8 -*-
"""Wizard per l'assignació de CUPS a escomeses."""
import wizard
import pooler
from osv import osv


_MAIN_FORM = """<?xml version="1.0"?>
<form string="Assignació de cups" >
  <notebook tabpos="up">
    <page string="Assignació">
      <group colspan="4">
        <field name="escomesa" on_change="escomesa_change(escomesa)"/>
      </group>
      <group string="Assignades" colspan="4">
        <field name="assignades" widget="many2many" width="760" height="300" nolabel="1" domain="[('id_escomesa', '=', False)]" on_change="assignades_change(assignades)"/>
      </group>
      <group string="Filtres" colspan="4" col="6">
        <field name="filtre_et" on_change="filtre_change(filtre_et, filtre_municipi, filtre_ordre)" />
        <field name="filtre_municipi" on_change="filtre_change(filtre_et, filtre_municipi, filtre_ordre)" />
        <field name="filtre_ordre" on_change="filtre_change(filtre_et, filtre_municipi, filtre_ordre)" />
      </group>
      <button string="Guardar" name="assign" type="object" icon="gtk-save" />
    </page>
    <page string="Consulta">
      <group colspan="4">
        <field name="c_escomesa" on_change="c_escomesa_change(c_escomesa)" />
      </group>
      <group string="Assignades" colspan="4">
        <field name="c_assignades" widget="one2many" height="340" nolabel="1" readonly="1" />
      </group>
    </page>
  </notebook>
</form>"""

_MAIN_FIELDS = {
  'assignades': {'string': 'Assignades', 'type': 'one2many',
                 'relation': 'giscedata.cups.ps'},
  'escomesa': {'string': 'Escomesa', 'type': 'many2one',
               'relation': 'giscedata.cups.escomesa'},
  'filtre_et': {'string': 'ET', 'type': 'char', 'size': 10},
  'filtre_municipi': {'string': 'Municipi', 'type': 'char', 'size': 100},
  'filtre_ordre': {'string': 'Ordre', 'type': 'char', 'size': 10},
  'c_escomesa': {'string': 'Escomesa', 'type': 'many2one',
                 'relation': 'giscedata.cups.escomesa'},
  'c_assignades': {'string': 'Assignades', 'type': 'one2many',
                   'relation': 'giscedata.cups.ps'},
}



class GiscedataCupsWizard(osv.osv):
    """Classe per l'assignació de CUPS a escomeses."""
    _name = 'wizard.giscedata.cups.wizard.assignar.cups'
    _auto = False

    _escomesa = 0
    _modified = False
    _saved = False

    

    def read(self, cursor, uid, ids, fields=None, context=None):
        """Creem un dummy read, a la v5 es crida el read() desrprés d'executar
        l'acció"""
        return []
        
    def write(self, cursor, uid, ids, vals, context=None):
        """Assignem el CUPS a una escomesa."""
        self._saved = True
        if vals.has_key('escomesa'):
            self.create(cursor, uid, vals, context)
            return True
        else:
            self.create(cursor, uid, vals, context)
            return False


    def filtre_change(self, cursor, uid, ids, *f_values):
        """Permet canviar el filtre."""
        filtres = [('id_escomesa', '=', False)]
        filtres_fields = ['et', 'id_municipi', 'ordre']
        i = 0
        for field in filtres_fields:
            if f_values[i]:
                filtres.append((field, 'ilike', f_values[i]))
            i += 1
        return {'domain': {'assignades': filtres}}


    def escomesa_change(self, cursor, uid, ids, escomesa, context=None):
        """Refresca els resultats si es canvia l'escomesa."""
        if escomesa:
            self._escomesa = escomesa
            cups_obj = pooler.get_pool(cursor.dbname).get('giscedata.cups.ps')
            cups_assignades = cups_obj.search(cursor, uid,
                                              [('id_escomesa', '=', escomesa)],
                                              0, 80)
            return {'value': {'assignades': cups_assignades}}
        else:
            self._escomesa = 0
            return {'value': {'assignades': False}}

    def assignades_change(self, cursor, uid, ids, cups, context=None):
        return {}

    def c_escomesa_change(self, cursor, uid, ids, escomesa, context=None):
        if escomesa:
            self._escomesa = escomesa
            cups_obj = pooler.get_pool(cursor.dbname).get('giscedata.cups.ps')
            cups_assignades = cups_obj.search(cursor, uid,
                                              [('id_escomesa', '=', escomesa)],
                                              0, 80)
            return {'value': {'c_assignades': cups_assignades}}
        else:
            return {'value': {'c_assignades': False}}

    def assign(self, cursor, uid, ids, args):
        if self._escomesa:
            return True
        else:
            return False

    def create(self, cursor, uid, vals, context=None):
        self._saved = True
        if not vals.has_key('escomesa'):
            vals['escomesa'] = self._escomesa
        if vals['escomesa'] > 0:
            cups_obj = pooler.get_pool(cursor.dbname).get('giscedata.cups.ps')
            cups_obj.write(cursor, uid,
                           cups_obj.search(cursor, uid,
                                    [('id_escomesa','=', vals['escomesa'])]),
                           {'id_escomesa': False})
            cups_obj.write(cursor, uid, vals['assignades'][0][2],
                           {'id_escomesa': vals['escomesa']})
        return 1

GiscedataCupsWizard()

class WizardAssignarCups(wizard.interface):
    """Wizard que ens permetrà assignar CUPS a escomeses."""
    states = {
      'init': {
          'actions': [],
        'result': {'type': 'state', 'state': 'main'}
      },
      'main': {
          'actions': [],
          'result': {'type': 'form', 'arch': _MAIN_FORM,'fields': _MAIN_FIELDS,
                     'state':[('end', 'Finalitzar', 'gtk-cancel')]}
      },
      'guardar': {
          'actions': [],
          'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

WizardAssignarCups('giscedata.cups.wizard.assignar.cups')
