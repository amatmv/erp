# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from tools import config
from datetime import date
import csv
import StringIO
import base64

HEADERS = {'abonats': [_('Sortida'), _('Escomesa'), _('Nom'), _('Potencia'),
                       _('Tensió'), _('Pòlissa'), _('Direcció'), _('CUPS')]
           }

INFORMES = {'abonats': {'desc': _("Llistat d'Abonats"),
                        'sql': 'abonats.sql',
                        'head': HEADERS['abonats']}
            }


class GiscedataCupsInformesWizard(osv.osv_memory):
    """Assistent per generar diferents informes de CUPS"""
    _name = "giscedata.cups.informes.wizard"

    def _ff_informes_sel(self, cursor, uid, context=None):
        '''Crea el selection'''
        return [(k, _(v['desc'])) for k, v in INFORMES.items()]

    def genera_informe_sql(self, cursor, uid, ids, context=None):
        ''' Genera CSV a partir d'un SQL'''
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context=context)

        sql_file = INFORMES[wiz.informe]['sql']
        sql = open('%s/%s/sql/%s' % (config['addons_path'],
                                     'giscedata_cups_distri',
                                     sql_file)).read()
        cursor.execute(sql)

        filename = 'CUPS_%s.csv' % date.today().strftime('%d%m%Y')
        header = INFORMES[wiz.informe]['head']

        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';')
        writer.writerow([h.encode('utf8') for h in header])

        for row in cursor.fetchall():
            writer.writerow(row)

        fitxer = base64.b64encode(output.getvalue())

        wiz.write({'fitxer': fitxer, 'name': filename})

        output.close()
        return True

    _columns = {
        'state': fields.char('Estat', 10),
        'informe': fields.selection(_ff_informes_sel, 'Informe'),
        'name': fields.char('Nom', size=256),
        'fitxer': fields.binary('Fitxer'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'informe': lambda *a: 'abonats',
    }

GiscedataCupsInformesWizard()