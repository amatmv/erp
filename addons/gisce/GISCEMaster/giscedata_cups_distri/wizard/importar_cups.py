# -*- coding: utf-8 -*-
"""Wizard per la importació de CUPS."""
import wizard
import pooler
import base64
import csv
import StringIO
import bz2

_TITLE = "Importación de CUPS"

def _triar_vals(self, cursor, uid, data, context=None):
    """Inicialitzem dades del wizard."""
    return {
      'compressed': 0,
    }

_TRIAR_VALS_FORM = """<?xml version="1.0"?>
<form string="%s" col="4">
  <group colspan="4">
    <field name="file"/>
    <field name="compressed" />
  </group>
</form>""" % (_TITLE)

_TRIAR_VALS_FIELDS = {
  'file': {'string': 'Fichero', 'type': 'binary', 'required': True},
  'compressed': {'string': 'BZip2', 'type': 'boolean'}
}


def _check_format(self, cursor, uid, data, context=None):
    """Comprovem el format del fitxer."""
    form = data['form']
    data['municipis'] = {}
    data['poblacions'] = {}

    municipi_obj = pooler.get_pool(cursor.dbname).get('res.municipi')
    poblacio_obj = pooler.get_pool(cursor.dbname).get('res.poblacio')

    if form['compressed']:
        file_content = bz2.decompress(base64.b64decode(form['file']))
    else:
        file_content = base64.b64decode(form['file'])

    csv_file = StringIO.StringIO(file_content)
    reader = csv.reader(csv_file, delimiter=';')
    line = 0
    errors = []
    for row in reader:
        line += 1
        # Comprovem que hi hagi 7 columnes
        if not len(row) == 12:
            errors.append("Línea %i: No tiene todos los valores o el\
             delimitador no es correcto. Debe ser punto y coma (;)" % line)
            continue

        # Comprovem que hi hagi els camps obligatoris
        # CUPS
        if not row[0]:
            errors.append('Línea %i: No tiene el campo CUPS' % (line))
            continue

        # Municipi
        if not row[1]:
            errors.append("Línea %i: No tiene definido el código INE del\
             municipio" % (line))
            continue
        else:
            # busquem l'INE si el trobem el guardem a un diccionari per no
            # haber de tornar a fer la consulta en el següent pas
            if not data['municipis'].has_key(row[0]):
                id_municipi = municipi_obj.search(cursor, uid, 
                                                  [('ine', '=', row[1])])
                if len(id_municipi):
                    data['municipis'][row[1]] = id_municipi[0]
                # Si no el trobem hem de donar un error.
                else:
                    errors.append("Línea %i: El código INE no se ha \
                    encontrado en la base de datos" % (line))
                    continue
        # Población
        if not row[2]:
            errors.append('Línea %i: No tiene definido la población' % (line))
            continue
        else:
            # busquem la Població i la guardem en un diccionari pel següent pas.
            # Si no la trobem, ens queixem
            search_vals = [('name', 'ilike', row[2]),
                           ('municipi_id', '=', id_municipi[0])]
            id_poblacio = poblacio_obj.search(cursor, uid, search_vals)
            if len(id_poblacio):
                data['poblacions'][row[2]] = id_poblacio[0]
            else:
                errors.append("Línea %i: La población no se ha encontrado en la"
                              "base de datos" % (line))
                continue
        # Calle
        if not row[3]:
            errors.append('Línea %i: No tiene definido la calle' % (line))
            continue

        # El camp US no és obligatori, però si es defineix 
        # només pot ser V o N
        if row[10] and row[10] not in ('V', 'N'):
            errors.append("Línea %i: El valor de Uso del punto de suminitro\
             solo puede ser V o N" % (line))
            continue


    if len(errors):
        self.states['check_format']['result']['state'] = [('end', 'Cancelar'),
                                                        ('retry', 'Reintentar')]
    else:
        data['file_content'] = file_content
        self.states['check_format']['result']['state'] = [('end', 'Cancelar'), 
                                                          ('import', 'Import')]
    return {
      'errors': '\n'.join(errors) or "Ningún error detectado, se puede proceder"
                                     " con la importación",
    }

_CHECK_FORMAT_FORM = """<?xml version="1.0"?>
<form string="%s" col="4">
  <separator string="Errores detectados" colspan="4" />
  <field name="errors" colspan="4" nolabel="1" readonly="1" width="400"/>
</form>""" % (_TITLE)

_CHECK_FORMAT_FIELDS = {
  'errors': {'string': 'Errores', 'type': 'text'}
}

def _retry(self, cursor, uid, data, context=None):
    """Tornem a provar."""
    data['form']['file'] = False
    return data['form']

def _import(self, cursor, uid, data, context=None):
    """Importem els CUPS."""
    cups_obj = pooler.get_pool(cursor.dbname).get('giscedata.cups.ps')
    n_lines = 0

    results = {
      'Creados': 0,
      'Actualizados': 0,
    }

    csv_file = StringIO.StringIO(data['file_content'])
    reader = csv.reader(csv_file, delimiter=';')

    for row in reader:

        id_municipi = data['municipis'][row[1]]
        id_poblacio = data['poblacions'][row[2]]

        vals = {
          'name': row[0],
          'id_municipi': id_municipi,
          'id_poblacio': id_poblacio,
          'carrer': row[3][:60],
          'numero': row[4][:10],
          'escala': row[5][:10],
          'pis': row[6][:10],
          'porta': row[7][:10],
          'zona': row[8][:10],
          'ordre': row[9][:10],
          'ref_catastral': row[11][:20],
          'active': 1,
        }

        id_cups = cups_obj.search(cursor, uid, [('name', '=', vals['name'])],
                                  context={'active_test': False})

        if len(id_cups):
            # Ja existeix l'hem d'actualitzar
            cups_obj.write(cursor, uid, id_cups, vals)
            results['Actualizados'] += 1
        else:
            # No existeix l'hem de crear de nou
            cups_obj.create(cursor, uid, vals)
            results['Creados'] += 1
        n_lines += 1

    return {
      'result': '\n'.join(["%s: %s" % (v[0], v[1]) for v in
                           sorted(results.items())]),
      'n_lines': n_lines
    }
    
    
    
_IMPORT_FORM = """<?xml version="1.0"?>
<form string="%s" col="4">
    <label colspan="4" string="Importación de CUPS efectuada correctamente." />
    <field name="n_lines" readonly="1"/>
    <newline />
    <separator string="Resultados" colspan="4" />
    <field name="result" colspan="4" readonly="1" nolabel="1"/>
</form>""" % (_TITLE)

_IMPORT_FIELDS = {
  'result': {'string': 'Resultado', 'type': 'text'},
  'n_lines': {'string': '# Líneas procesadas', 'type': 'integer'},
}

class WizardImportarCups(wizard.interface):
    """Wizard per importar CUPS a través d'un fitxer."""
    
    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'triar_vals'},
      },
      'triar_vals': {
        'actions': [_triar_vals],
        'result': {'type': 'form', 'arch': _TRIAR_VALS_FORM,
                   'fields': _TRIAR_VALS_FIELDS,
                   'state': [('check_format', 'Siguiente')]}
      },
      'check_format': {
        'actions': [_check_format],
        'result': {'type': 'form', 'arch': _CHECK_FORMAT_FORM,
                   'fields': _CHECK_FORMAT_FIELDS, 'state': []}
      },
      'retry': {
        'actions': [_retry],
        'result': {'type': 'form', 'arch': _TRIAR_VALS_FORM,
                   'fields': _TRIAR_VALS_FIELDS,
                   'state': [('check_format', 'Importar')]}
      },
      'import': {
        'actions': [_import],
        'result': {'type': 'form', 'arch': _IMPORT_FORM,
                   'fields': _IMPORT_FIELDS, 'state': [('end', 'Finalizar')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'},
      }
    }

WizardImportarCups('giscedata.cups.import')
