-- Llistat d'abonats
SELECT
    cups.et || '-T' || trafo.ordre_dins_ct AS "IdCT",cups.linia AS "Sortida",
    escomesa.name AS "Escomesa",
    titular.name AS "Nom",
    polissa.potencia AS "Potencia",
    polissa.tensio AS "Tensió",
    polissa.name AS "Pòlissa",
    cups.direccio AS "Direcció" ,
    cups.name AS "CUPS"
FROM giscedata_cups_ps AS cups
LEFT JOIN giscedata_cups_escomesa AS escomesa ON escomesa.id=cups.id_escomesa
LEFT JOIN giscedata_polissa AS polissa ON polissa.cups=cups.id AND polissa.active
LEFT JOIN res_partner AS titular ON titular.id=polissa.titular
LEFT JOIN giscegis_escomeses_traceability trace ON trace.escomesa=cups.id_escomesa
LEFT JOIN giscedata_transformador_trafo AS trafo ON trafo.id=trace.trafo
WHERE cups.active
ORDER BY cups.name;