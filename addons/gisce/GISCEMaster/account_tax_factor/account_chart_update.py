# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class AccountChartUpdateFactor(osv.osv_memory):

    _name = 'wizard.update.charts.accounts'
    _inherit = 'wizard.update.charts.accounts'

    def _find_taxes(self, cr, uid, wizard, context=None):

        tax_obj = self.pool.get('account.tax')
        template_obj = self.pool.get('account.tax.template')
        tax_facade_obj = self.pool.get('wizard.update.charts.accounts.tax')

        res = super(AccountChartUpdateFactor,
                    self)._find_taxes(cr, uid, wizard, context=context)

        for template_id, tax_id in res['mapping'].iteritems():
            if wizard.update_tax:
                template = template_obj.browse(cr, uid, template_id)
                search_params = [('tax_id', '=', template_id),
                                 ('update_chart_wizard_id', '=', wizard.id),
                                 ('type', '=', 'updated')]
                tax_facade_ids = tax_facade_obj.search(cr, uid, search_params)
                if not tax_facade_ids:
                    template = template_obj.browse(cr, uid, template_id)
                    tax = tax_obj.browse(cr, uid, tax_id)
                    if template.factor != tax.factor:
                        notes = _("The factor field is different.\n")
                        res['updated'] += 1
                        tax_facade_obj.create(cr, uid, {
                                        'tax_id': template_id,
                                        'update_chart_wizard_id': wizard.id,
                                        'type': 'updated',
                                        'update_tax_id': tax_id,
                                        'notes': notes,
                                        }, context)
        return res

    def _update_taxes(self, cr, uid, wizard, log, tax_code_template_mapping,
                      context=None):

        tax_obj = self.pool.get('account.tax')
        template_obj = self.pool.get('account.tax.template')

        res = super(AccountChartUpdateFactor,
                    self)._update_taxes(cr, uid, wizard, log,
                                        tax_code_template_mapping,
                                        context=context)

        for wiz_tax in wizard.tax_ids:
            tax_template = wiz_tax.tax_id
            if tax_template.id in res['mapping']:
                tax_id = res['mapping'][tax_template.id]
                vals_tax = {'factor': tax_template.factor}
                # Update factor
                if (wiz_tax.type == 'new' or
                   (wizard.update_tax and wiz_tax.update_tax_id)):
                    tax_obj.write(cr, uid, [tax_id], vals_tax)

        return res

AccountChartUpdateFactor()
