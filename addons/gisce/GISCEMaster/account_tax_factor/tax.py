# -*- coding: utf-8 -*-
from osv import osv, fields


class AccountTaxFactor(osv.osv):

    _name = 'account.tax'
    _inherit = 'account.tax'

    _columns = {
        'factor': fields.float('Factor', digits=(16, 2),
                               help=u"Base will be multiplied by factor "
                                    u"before computing amount in invoices"),
    }

    _defaults = {
        'factor': lambda *a: 1.0,
    }

AccountTaxFactor()


class AccountTaxTemplateFactor(osv.osv):

    _name = 'account.tax.template'
    _inherit = 'account.tax.template'

    _columns = {
        'factor': fields.float('Factor', digits=(16, 2),
                               help=u"Base will be multiplied by factor "
                                    u"before computing amount in invoices"),
    }

    _defaults = {
        'factor': lambda *a: 1.0,
    }

AccountTaxTemplateFactor()
