# -*- coding: utf-8 -*-
{
    "name": "Account tax factor",
    "description": """
    Adds factor to account.tax in order to multiply base by factor.
    This allows modifying base amount needed in some cases 
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Account",
    "depends":[
        "base",
        "account",
        "account_chart_update"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "tax_view.xml",
        "invoice_view.xml"
    ],
    "active": False,
    "installable": True
}
