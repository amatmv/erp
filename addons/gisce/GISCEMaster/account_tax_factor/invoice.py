# -*- coding: utf-8 -*-
import time

from osv import osv, fields
from tools import config


class AccountInvoiceTaxFactor(osv.osv):

    _name = 'account.invoice.tax'
    _inherit = 'account.invoice.tax'

    def compute(self, cr, uid, invoice_id, line_id=None, context=None):

        if not context:
            context = {}

        tax_obj = self.pool.get('account.tax')
        cur_obj = self.pool.get('res.currency')
        inv_obj = self.pool.get('account.invoice')

        inv = inv_obj.browse(cr, uid, invoice_id, context)
        cur = inv.currency_id
        company_currency = inv.company_id.currency_id.id
        tax_tree = {}
        tax_grouped = {}
        # For each line in the invoice compute taxes
        for line in inv.invoice_line:
            if line_id is not None and line.id != line_id:
                continue
            to_include = None
            # Ensure we evaluate taxes using sequence order
            line_taxes_full = []
            for tax in line.invoice_line_tax_id:
                if tax.child_depend:
                    line_taxes_full.extend([x for x in tax.child_ids])
                else:
                    line_taxes_full.append(tax)
            line_taxes = sorted([(x, x.sequence)
                                 for x in line_taxes_full],
                                key=lambda a: a[1])
            for tax, sequence in line_taxes:
                val = {}
                tax_id = val['tax_id'] = tax.id
                val['invoice_id'] = inv.id
                val['name'] = tax.name
                val['manual'] = False
                val['sequence'] = tax.sequence
                val['base'] = cur_obj.float_round(
                    cr, uid, cur, getattr(line, tax.field_based_on)
                )
                val['tax_factor'] = tax.factor

                ctx = {'date': inv.date_invoice or time.strftime('%Y-%m-%d')}
                if inv.type in ('out_invoice', 'in_invoice'):
                    val['base_code_id'] = tax.base_code_id.id
                    val['tax_code_id'] = tax.tax_code_id.id
                    val['account_id'] = (tax.account_collected_id and
                                         tax.account_collected_id.id or
                                         line.account_id.id)
                    base_sign = tax.base_sign
                else:
                    val['base_code_id'] = tax.ref_base_code_id.id
                    val['tax_code_id'] = tax.ref_tax_code_id.id
                    val['account_id'] = (tax.account_paid_id and
                                         tax.account_paid_id.id or
                                         line.account_id.id)
                    base_sign = tax.ref_base_sign

                val['base_amount'] = cur_obj.compute(cr, uid,
                                                     inv.currency_id.id,
                                                     company_currency,
                                                     val['base'] * base_sign,
                                                     context=ctx, round=False)
                val['base_amount'] = cur_obj.float_round(cr, uid, cur,
                                                         val['base_amount'])
                # Group taxes by key
                tax_key = (val['tax_code_id'],
                           val['base_code_id'],
                           val['account_id'])
                if tax_key not in tax_grouped:
                    tax_grouped[tax_key] = val
                else:
                    tax_grouped[tax_key]['base'] += val['base']
                    tax_grouped[tax_key]['base_amount'] += val['base_amount']
                # If previous tax_amount must be included in this tax base
                # store it for later computing
                tax_tree_key = (tax_id, tax_key)
                tax_tree.setdefault(tax_tree_key, {})
                if to_include is not None:
                    inc_tax_id = to_include['tax_id']
                    key_value = tax_tree[tax_tree_key]
                    key_value.setdefault(inc_tax_id,
                                         {'base': 0, 'base_amount': 0,
                                          'tax_factor':
                                          to_include['tax_factor']})
                    key_value[inc_tax_id]['base'] += to_include['base']
                    key_value[inc_tax_id]['base_amount'] += to_include['base_amount']
                # If tax_amount must be included in next tax base, store it
                if tax.include_base_amount:
                    to_include = val
                else:
                    to_include = None

        # TODO - Separar esta parte en otra funcion en el modulo base
        # asi la herencia es mas facil y toca menos cosas

        # With all base amounts already summed
        # and rounded compute tax amounts
        for tax_multikey, include_taxes in tax_tree.iteritems():
            tax_id = tax_multikey[0]
            tax_key = tax_multikey[1]
            tax = tax_obj.browse(cr, uid, tax_id)
            base_add = 0
            base_amount_add = 0
            # Include tax_amount from taxes inside tax
            for inc_tax_id, values in include_taxes.iteritems():
                inc_tax = tax_obj.browse(cr, uid, inc_tax_id)
                round_base = values['base']
                round_base_amount = values['base_amount']
                if values['tax_factor'] != 1:
                    round_base = cur_obj.float_round(cr, uid, cur,
                                                     (values['base'] *
                                                      values['tax_factor']))
                    round_base_amount = cur_obj.float_round(cr, uid, cur,
                                                       (values['base_amount'] *
                                                        values['tax_factor']))
                amount = tax_obj._unit_compute(cr, uid, [inc_tax],
                                               round_base)[0]['amount']
                base_amount = tax_obj._unit_compute(cr, uid, [inc_tax],
                                               round_base_amount)[0]['amount']
                base_add += cur_obj.float_round(cr, uid, cur, amount)
                base_amount_add += cur_obj.float_round(cr, uid, cur,
                                                       base_amount)
            tax_value = tax_grouped[tax_key]
            tax_value['base'] += base_add
            tax_value['base_amount'] += base_amount_add

            # Store in factor base fields, bases before multiply by factor
            tax_value['factor_base'] = tax_value['base']
            tax_value['factor_base_amount'] = tax_value['base_amount']

            # Multiply by factor base fields
            if tax_value['tax_factor'] != 1:
                tax_value['base'] = cur_obj.float_round(cr, uid, cur,
                                                (tax_value['base'] *
                                                 tax_value['tax_factor']))
                tax_value['base_amount'] = cur_obj.float_round(cr, uid, cur,
                                                (tax_value['base_amount'] *
                                                 tax_value['tax_factor']))
            # Compute taxes for all four base fields
            amount = tax_obj._unit_compute(cr, uid, [tax],
                                    tax_value['base'])[0]['amount']
            tax_amount = tax_obj._unit_compute(cr, uid, [tax],
                                    tax_value['base_amount'])[0]['amount']
            if tax_value['tax_factor'] != 1:
                factor_amount = tax_obj._unit_compute(cr, uid, [tax],
                                    tax_value['factor_base'])[0]['amount']
                factor_tax_amount = tax_obj._unit_compute(cr, uid, [tax],
                                tax_value['factor_base_amount'])[0]['amount']

            tax_value['amount'] = cur_obj.float_round(cr, uid, cur, amount)

            tax_value['tax_amount'] = cur_obj.float_round(cr, uid, cur,
                                                          tax_amount)
            if tax_value['tax_factor'] != 1:
                tax_value['factor_amount'] = cur_obj.float_round(cr, uid, cur,
                                                             factor_amount)
                tax_value['factor_tax_amount'] = cur_obj.float_round(cr, uid,
                                                          cur,
                                                          factor_tax_amount)
            else:
                tax_value['factor_amount'] = tax_value['amount']
                tax_value['factor_tax_amount'] = tax_value['tax_amount']

        if context.get('string_key', False):
            res = {}
            for tax_key, tax_value in tax_grouped.iteritems():
                tax_id = str(tax_value['tax_id'])
                res[tax_id] = tax_value
            return res
        return tax_grouped

    _columns = {
        'tax_factor': fields.float('Factor', digits=(16, 2)),
        'factor_base': fields.float('Base',
                                digits=(16, int(config['price_accuracy']))),
        'factor_amount': fields.float('Amount',
                                digits=(16, int(config['price_accuracy']))),
        'factor_base_amount': fields.float('Base Code Amount',
                                digits=(16, int(config['price_accuracy']))),
        'factor_tax_amount': fields.float('Tax Code Amount',
                                digits=(16, int(config['price_accuracy']))),
    }

    _defaults = {
        'tax_factor': lambda *a: 1.0,
    }

AccountInvoiceTaxFactor()
