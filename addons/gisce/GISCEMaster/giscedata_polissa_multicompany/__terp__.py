# -*- coding: utf-8 -*-
{
    "name": "Polissa multicompany",
    "description": """Multi-company support for polissa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_polissa"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_multicompany_view.xml"
    ],
    "active": False,
    "installable": False
}
