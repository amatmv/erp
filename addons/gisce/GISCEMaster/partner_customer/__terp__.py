# -*- coding: utf-8 -*-
{
    "name": "Partner extension show only customers",
    "description": """Show only partners that are also customers""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules/Customers",
    "depends":[
        "base",
        "giscedata_transformadors",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_customer_view.xml"
    ],
    "active": False,
    "installable": True
}
