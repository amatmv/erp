from osv import osv,fields

class res_partner_customer_tarifa(osv.osv):
  _name = 'res.partner.customer.tarifa'
  _description = 'Tarifa del client'
  _columns = {
    'name': fields.char('Codi', size=10, required=True),
    'description': fields.text('Descripcio'),
  }
  _defaults = {
  }
  _order = "name,id"
res_partner_customer_tarifa()

class res_partner_customer_activitat(osv.osv):
  _name = 'res.partner.customer.activitat'
  _description = 'Activitat del client'
  _columns = {
    'name': fields.char('Activitat', size=100, required=True),
    'codi': fields.char('Codi Activitat', size=10, required=True),
  }
  _defaults = {

  }

  _order = "name, codi, id"
res_partner_customer_activitat()

class res_partner(osv.osv):
  _inherit = "res.partner"
  _name = "res.partner"

  _columns = {
    'cups' : fields.many2one('giscedata.cups.ps', 'CUPS'),
    'codi_abonat' : fields.char('Codi Abonat', size=10),
    'n_comptador' : fields.char('N Comptador', size=20),
    'n_comptador_2' : fields.char('N Comptador 2', size=20),
    'n_comptador_3' : fields.char('N Comptador 3', size=20),
    'n_comptador_reactiva' : fields.char('N Comptador Reactiva', size=20),
    'polissa' : fields.char('Polissa', size=20),
    'tarifa' : fields.many2one('res.partner.customer.tarifa','Tarifa'),
    'data_alta' : fields.datetime('Data Alta'),
    'data_baixa' : fields.datetime('Data Baixa'),
    'zona' : fields.char('Zona', size=10),
    'ordre' : fields.char('Ordre', size=10),
    'pot' : fields.char('Potencia', size=10),
    'et' : fields.char('ET', size=10),
    'linea' : fields.char('Linea', size=10),
    'escomesa' : fields.many2one('giscedata.cups.escomesa', 'Escomesa'),
    'tensio': fields.char('Tensio', size=10),
    'icp': fields.char('ICP', size=10),
    'consum_any': fields.float('Consum Any'),
    'activitat_id':fields.many2one('res.partner.customer.activitat', 'Activitat'),
    'discriminacio': fields.char('Discriminacio', size=10),
    'estat': fields.char('Estat', size=10),
    'dispinterr': fields.char('Disp. Interr', size=10),
    'deracceso': fields.char('Derechos Acceso', size=10),
    'municipi_id': fields.many2one('res.municipi', 'Municipi'),
  }
res_partner()
