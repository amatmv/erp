# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from osv import osv, fields
from tools.translate import _
import json


class WizardFacturarComissioFromPolissa(osv.osv_memory):

    _name = 'wizard.facturar.comissio.from.polissa'

    def action_facturar_comissio_from_polissa(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        wizard = self.browse(cursor, uid, ids[0], context=context)
        pol_obj = self.pool.get('giscedata.polissa')
        contracts = context['active_ids']
        result = pol_obj.facturar_comissio_from_polissa(cursor, uid, contracts, context=context)
        ordres_ids = json.dumps(result[0])
        wizard.write({'info': result[1], 'state': 'end', 'ordres_ids': ordres_ids})

    def action_obrir_ordres_generades(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.ordres_ids
        casos_ids = json.loads(casos_str or "{}")
        return {
            'domain': "[('id','in', {0})]".format(str(casos_ids)),
            'name': _('Comissions a Pagar'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'hr.expense.expense',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'info': fields.text('Info'),
        'state': fields.char('Estat', size=8),
        'ordres_ids': fields.text(_('Comissions a Pagar')),
    }

    _defaults = {
        'info': lambda *a: 'Es generaran fulls de despeses amb les comissions a pagar als comercials.',
        'state': lambda *a: 'init'
    }

WizardFacturarComissioFromPolissa()
