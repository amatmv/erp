# -*- coding: utf-8 -*-
{
    "name": "Modul Comercial Comissions",
    "description": """
    Modul base per a la gestió de comissions de comercials.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_comercial",
        "hr_expense",
    ],
    "init_xml": [],
    "demo_xml": [
    ],
    "update_xml":[
        "hr_employee_view.xml",
        "giscedata_polissa_view.xml",
        "wizard/wizard_facturar_comissio_from_polissa_view.xml",
        "giscedata_comercial_comissio_data.xml",
        "menu_comercial_view.xml",
        "menu_admin_comercial_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
