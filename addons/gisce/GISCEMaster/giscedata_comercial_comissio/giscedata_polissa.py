# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def write(self, cursor, uid, ids, vals, context=None):
        old_comissio_infos = []
        new_comissio = False
        if 'comissio' in vals:
            old_comissio_infos = self.read(cursor, uid, ids, ['comissio'], context=context)
            new_comissio = vals['comissio']
        res = super(GiscedataPolissa, self).write(cursor, uid, ids, vals, context)
        self.clean_comissions(cursor, uid, old_comissio_infos, new_comissio, context=context)
        return res

    def clean_comissions(self, cursor, uid, old_comissio_infos, new_comissio, context=None):
        if not isinstance(old_comissio_infos, (list, tuple)):
            old_comissio_infos = [old_comissio_infos]
        if not len(old_comissio_infos):
            return
        if not new_comissio:
            new_comissio = ""
        for old_comisio_info in old_comissio_infos:
            if not old_comisio_info['comissio']:
                continue
            # El replace es fa perque a vegades arriben amb espai despres de la
            # coma i a vegades no
            elif old_comisio_info['comissio'].replace(" ", "") == new_comissio.replace(" ", ""):
                continue
            model_obj, model_id = old_comisio_info['comissio'].split(",")
            if model_id == "0":
                continue
            self.pool.get(model_obj).unlink(cursor, uid, int(model_id), context=context)
        return True

    def action_asignar_comissio_from_comercial(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        comercial_obj = self.pool.get("hr.employee")
        for info in self.read(cursor, uid, ids, ['name', 'comercial_id', 'comissio']):
            if not info['comercial_id']:
                raise osv.except_osv(
                    _("Error Usuari"),
                    _(u"No s'ha pogut asignar automàticament una comissió al "
                      u"contracte {0} perqué no te comercial asignat.").format(
                        info['name']
                    )
                )
            if info['comissio'] and info['comissio'].split(",")[1].strip() != '0':
                raise osv.except_osv(
                    _("Error Usuari"),
                    _(u"No s'ha pogut asignar automàticament una comissió al "
                      u"contracte {0} perqué ja te una comissió asignada. "
                      u"Si vols asignar-ne una de nova primer has de esborrar "
                      u"l'actual.").format(
                        info['name']
                    )
                )
            cid = info['comercial_id'][0]
            res = comercial_obj.create_comissio_polissa_from_comercial(cursor, uid, cid, info['id'], context=context)
            if res:
                self.write(cursor, uid, info['id'], {'comissio': res})

        return True

    def _comissions_disponibles(self, cursor, uid, context=None):
        cursor.execute("""
          select m.model, m.name from ir_model m where m.model like 'giscedata.polissa.comissio.%' order by m.model
        """)
        return cursor.fetchall()

    def facturar_comissio_from_polissa(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        res_msg = u""
        per_facturar = []
        for pol_info in self.read(cursor, uid, ids, ['comissio', 'name']):
            if not pol_info['comissio'] or pol_info['comissio'].split(",")[1].strip() == '0':
                res_msg += _(u"* Contracte {0}: No te cap comissio asignada.\n").format(pol_info['name'])
            else:
                per_facturar.append(pol_info['comissio'])
        res_ids, res_msg2 = self.pool.get("giscedata.polissa.comissio").facturar_comissio_polissa(cursor, uid, per_facturar, context=context)
        res_msg = _(u"S'han generat {0} comissions a pagar.\n\n{1}{2}").format(len(res_ids), res_msg, res_msg2)
        return res_ids, res_msg

    _columns = {
        'comissio': fields.reference(u"Comissió Asignada", _comissions_disponibles, size=256),
    }


GiscedataPolissa()
