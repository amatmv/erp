# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

# Aqust modul afageix la logica per asignar comissions als comercials i contractes, pero no en crea cap. 
# Per fer els tests ens inventem un parell de  Comissions TEST


class GiscedataComercialComissioTest(osv.osv):
    _name = 'giscedata.comercial.comissio.test'
    _inherit = 'giscedata.comercial.comissio'
    _description = "Comissió Test"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['quantitat'], context=context)
        return _(u"Comissio de {0}€ per cada contracte nou").format(vals['quantitat'])

    def get_comissio_polissa_vals(self, cursor, uid, comissio_id, context=None):
        vals = self.read(cursor, uid, comissio_id, ['quantitat', 'comercial_id'], context=context)
        if vals.get('comercial_id'):
            vals['comercial_id'] = vals.get('comercial_id')[0]
        del vals['id']
        return vals


    _columns = {
        'quantitat': fields.integer(u"Quantitat a pagar"),
    }


GiscedataComercialComissioTest()


class GiscedataPolissaComissioTest(osv.osv):
    _name = 'giscedata.polissa.comissio.test'
    _inherit = 'giscedata.polissa.comissio'
    _description = "Comissió Test"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['quantitat', 'pagada'], context=context)
        estat = "Pagada" if vals['pagada'] else "No pagada"
        return _(u"Comissio de {0}€ per contracte nou - {1}").format(vals['quantitat'], estat)

    _columns = {
        'quantitat': fields.integer(u"Quantitat a pagar"),
        'pagada': fields.boolean(u"Pagada al comercial"),
    }


GiscedataPolissaComissioTest()


class GiscedataComercialComissioTestAlternativa(osv.osv):
    _name = 'giscedata.comercial.comissio.test.alternativa'
    _inherit = 'giscedata.comercial.comissio'
    _description = "Comissió Test KW"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['kw_comissio'], context=context)
        return _(u"Comissio de {0}% per kW facturat").format(vals['kw_comissio'])

    def get_comissio_polissa_vals(self, cursor, uid, comissio_id, context=None):
        vals = self.read(cursor, uid, comissio_id, ['kw_comissio', 'comercial_id'], context=context)
        if vals.get('comercial_id'):
            vals['comercial_id'] = vals.get('comercial_id')[0]
        del vals['id']
        return vals

    _columns = {
        'kw_comissio': fields.integer(u"Kw comissio"),
    }


GiscedataComercialComissioTestAlternativa()


class GiscedataPolissaComissioTestAlternativa(osv.osv):
    _name = 'giscedata.polissa.comissio.test.alternativa'
    _inherit = 'giscedata.polissa.comissio'
    _description = "Comissió Test KW"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['kw_comissio', 'ultima_pagada'], context=context)
        estat = vals['ultima_pagada']
        return _(u"Comissio de {0}% per cada kW facturat- Ultim pagament {1}").format(vals['kw_comissio'], estat)

    _columns = {
        'kw_comissio': fields.integer(u"Kw comissio"),
        'ultima_pagada': fields.date(u"Ultima Pagada al comercial"),
    }


GiscedataPolissaComissioTestAlternativa()
