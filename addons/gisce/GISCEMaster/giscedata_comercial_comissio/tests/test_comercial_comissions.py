# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from osv import osv


class TestComercials(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.pool = self.openerp.pool
        self.comercial_obj = self.pool.get("hr.employee")
        self.polissa_obj = self.pool.get("giscedata.polissa")
        self.comissio_test_obj = self.pool.get("giscedata.comercial.comissio.test")
        self.pol_comissio_test_obj = self.pool.get("giscedata.polissa.comissio.test")
        self.comissio2_test_obj = self.pool.get("giscedata.comercial.comissio.test.alternativa")
        self.pol_comissio2_test_obj = self.pool.get("giscedata.polissa.comissio.test.alternativa")
        self.imd_obj = self.pool.get('ir.model.data')
        self.comercial_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'hr', 'employee1')[1]
        self.polissa_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0002')[1]

    def tearDown(self):
        self.txn.stop()

    def test_crear_comissio_a_comercial_sense_comissio(self):
        # Creem una comissio, no esta asociada a cap comercial
        comissio_id = self.comissio_test_obj.create(self.cursor, self.uid, {'quantitat': 10})
        info = self.comissio_test_obj.read(self.cursor, self.uid, comissio_id, ['quantitat', 'comercial_id'])
        self.assertEqual(info['quantitat'], 10)
        self.assertFalse(info['comercial_id'])
        # Si l'assignem a un comercial, tambe s'ha de emplenar el camp comercial_id de la comissio
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.test, {0}".format(comissio_id)})
        info = self.comissio_test_obj.read(self.cursor, self.uid, comissio_id, ['quantitat', 'comercial_id'])
        self.assertEqual(info['quantitat'], 10)
        self.assertEqual(info['comercial_id'][0], self.comercial_id)
        # Finalment desasignem la comissio del comercial, s'ha de eliminar la comissio
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': False})
        trobades = self.comissio_test_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(trobades), 0)

    def test_crear_comissio_a_comercial_amb_comissio(self):
        # Creem una comissio i l'asignem a un comercial
        comissio_id = self.comissio_test_obj.create(self.cursor, self.uid, {'quantitat': 10})
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.test, {0}".format(comissio_id)})
        info = self.comissio_test_obj.read(self.cursor, self.uid, comissio_id, ['quantitat', 'comercial_id'])
        self.assertEqual(info['quantitat'], 10)
        self.assertEqual(info['comercial_id'][0], self.comercial_id)
        # En creem un añtre de diferent i l'asignem al mateix comercial. La antiga s'ha de esborrar sola
        comissio2_id = self.comissio2_test_obj.create(self.cursor, self.uid, {'kw_comissio': 7})
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.test.alternativa, {0}".format(comissio2_id)})
        info = self.comissio2_test_obj.read(self.cursor, self.uid, comissio2_id, ['kw_comissio', 'comercial_id'])
        self.assertEqual(info['kw_comissio'], 7)
        self.assertEqual(info['comercial_id'][0], self.comercial_id)
        trobades = self.comissio_test_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(trobades), 0)
        trobades = self.comissio2_test_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(trobades), 1)
        self.assertEqual(trobades[0], comissio2_id)

    def test_modificar_comissio_a_comercial(self):
        # Creem una comissio i l'asignem a un comercial
        comissio_id = self.comissio_test_obj.create(self.cursor, self.uid, {'quantitat': 10})
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.test, {0}".format(comissio_id)})
        info = self.comercial_obj.read(self.cursor, self.uid, self.comercial_id, ['comissio'])
        self.assertEqual(info['comissio'], "giscedata.comercial.comissio.test, {0}".format(comissio_id))
        info = self.comissio_test_obj.read(self.cursor, self.uid, comissio_id, ['quantitat', 'comercial_id'])
        self.assertEqual(info['quantitat'], 10)
        self.assertEqual(info['comercial_id'][0], self.comercial_id)
        # Al modificarla ha de seguir al mateix comercial
        self.comissio_test_obj.write(self.cursor, self.uid, comissio_id, {'quantitat': 25})
        info = self.comissio_test_obj.read(self.cursor, self.uid, comissio_id, ['quantitat', 'comercial_id'])
        self.assertEqual(info['quantitat'], 25)
        self.assertEqual(info['comercial_id'][0], self.comercial_id)
        info = self.comercial_obj.read(self.cursor, self.uid, self.comercial_id, ['comissio'])
        self.assertEqual(info['comissio'], "giscedata.comercial.comissio.test, {0}".format(comissio_id))

    def test_asignar_comissio_a_polissa_sense_comercial(self):
        # Intentem asignar una comissio a un contracte sense comercial. Ha de donar una excepcio
        self.polissa_obj.write(self.cursor, self.uid, self.polissa_id, {'user_id': False})
        with self.assertRaises(osv.except_osv) as error_usuari:
            self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa_id)
        self.assertEqual(error_usuari.exception.value, u"No s'ha pogut asignar automàticament una comissió al contracte 0002 perqué no te comercial asignat.")

    def test_asignar_comissio_a_polissa_amb_comercial_sense_comissio(self):
        # Intentem asignar una comissio a un contracte amb comercial pero el comercial no te comissio definida. Ha de donar una excepcio
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': False})
        with self.assertRaises(osv.except_osv) as error_usuari:
            self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa_id)
        self.assertEqual(error_usuari.exception.value, u"No s'ha pogut asignar automàticament una comissió al contracte perqué el comercial Fabien Pinckaers no te cap comissio asignada.")

    def test_asignar_comissio_a_polissa_ok(self):
        # Asignem una comissio a un comercial
        comissio_id = self.comissio_test_obj.create(self.cursor, self.uid, {'quantitat': 10})
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.test, {0}".format(comissio_id)})
        # Anem a un contracte seu i l'hi asigem la comissio. No ha de donar cap error i ha de tenir els valors de la comissio del comercial
        self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa_id)
        info = self.polissa_obj.read(self.cursor, self.uid, self.polissa_id, ['comissio'])
        self.assertTrue(info['comissio'])
        comissio_polissa_vals = self.pol_comissio_test_obj.read(self.cursor, self.uid, int(info['comissio'].split(",")[1]))
        self.assertEqual(comissio_polissa_vals['quantitat'], 10)
        self.assertEqual(comissio_polissa_vals['comercial_id'][0], self.comercial_id)
        self.assertEqual(comissio_polissa_vals['polissa_id'][0], self.polissa_id)
        self.assertFalse(comissio_polissa_vals['pagada'])
        # Si intentem asignar un altre cop comissio sense eliminar la altre, fallara
        with self.assertRaises(osv.except_osv) as error_usuari:
            self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa_id)
        self.assertEqual(error_usuari.exception.value, u"No s'ha pogut asignar automàticament una comissió al contracte 0002 perqué ja te una comissió asignada. Si vols asignar-ne una de nova primer has de esborrar l'actual.")
        # Ara la desvinculem del contracte i la comercial s'ha de esborrar sola
        self.polissa_obj.write(self.cursor, self.uid, self.polissa_id, {'comissio': False})
        trobades = self.pol_comissio_test_obj.search(self.cursor, self.uid, [])
        self.assertEqual(len(trobades), 0)
