# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime


class hr_employee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    def write(self, cursor, uid, ids, vals, context=None):
        old_comissio_infos = []
        new_comissio = False
        if 'comissio' in vals:
            old_comissio_infos = self.read(cursor, uid, ids, ['comissio'], context=context)
            new_comissio = vals['comissio']
            self.asignar_comercial_a_comissio(cursor, uid, new_comissio, ids, context=context)
        res = super(hr_employee, self).write(cursor, uid, ids, vals, context)
        self.clean_comissions(cursor, uid, old_comissio_infos, new_comissio, context=context)
        return res

    def clean_comissions(self, cursor, uid, old_comissio_infos, new_comissio, context=None):
        if not isinstance(old_comissio_infos, (list, tuple)):
            old_comissio_infos = [old_comissio_infos]
        if not len(old_comissio_infos):
            return
        if not new_comissio:
            new_comissio = ""
        for old_comisio_info in old_comissio_infos:
            if not old_comisio_info['comissio']:
                continue
            # El replace es fa perque a vegades arriben amb espai despres de la
            # coma i a vegades no
            elif old_comisio_info['comissio'].replace(" ", "") == new_comissio.replace(" ", ""):
                continue
            model_obj, model_id = old_comisio_info['comissio'].split(",")
            if model_id == "0":
                continue
            self.pool.get(model_obj).unlink(cursor, uid, int(model_id), context=context)
        return True

    def asignar_comercial_a_comissio(self, cursor, uid, new_comissio, comercial_id, context=None):
        if isinstance(comercial_id, (list, tuple)):
            comercial_id = comercial_id[0]
        if not new_comissio:
            return
        model_obj, model_id = new_comissio.split(",")
        if model_id != "0":
            model_obj = self.pool.get(model_obj)
            model_id = int(model_id)
            new_info = model_obj.read(cursor, uid, model_id, ['comercial_id'], context=context)
            if not new_info['comercial_id']:
                model_obj.write(cursor, uid, model_id, {'comercial_id': comercial_id}, context=context)
        return True

    def create_comissio_polissa_from_comercial(self, cursor, uid, cid, polissa_id, context=None):
        if context is None:
            context = {}
        comercial_info = self.read(cursor, uid, cid, ['comissio', 'name'])
        if not comercial_info['comissio'] or comercial_info['comissio'].split(",")[-1].replace(" ", "") == "0":
            raise osv.except_osv(
                _("Error Usuari"),
                _(u"No s'ha pogut asignar automàticament una comissió "
                  u"al contracte perqué el comercial {0} no te cap "
                  u"comissio asignada.").format(
                    comercial_info['name']
                )
            )
        else:
            ctx = context.copy()
            ctx['polissa_id'] = polissa_id
            model_obj, model_id = comercial_info['comissio'].split(",")
            model_obj = model_obj.replace('giscedata.comercial', 'giscedata.polissa')
            res = self.pool.get(model_obj).create_from_comissio_comercial(
                cursor, uid, comercial_info['comissio'], context=ctx
            )
            if res:
                res = "{0}, {1}".format(model_obj, res)
            return res

    def _comissions_disponibles(self, cursor, uid, context=None):
        cursor.execute("""
          select m.model, m.name from ir_model m where m.model like 'giscedata.comercial.comissio.%' order by m.model
        """)
        return cursor.fetchall()

    _columns = {
        'comissio': fields.reference(u"Comissió Asignada", _comissions_disponibles, size=256),
    }


hr_employee()


class GiscedataComercialComissio(osv.osv):
    _name = 'giscedata.comercial.comissio'
    _description = "Comissió Base"

    def _ff_generar_descripcio(self, cursor, uid, ids,  field_name, arg, context=None):
        res = {}.fromkeys(ids, "Comissio Base")
        for cid in ids:
            res[cid] = self.generar_descripcio(cursor, uid, cid, context=context)
        return res

    def generar_descripcio(self, cursor, uid, cid, context=None):
        return "Comissio Base"

    def get_comissio_polissa_vals(self, cursor, uid, comissio_id, context=None):
        vals = self.read(cursor, uid, comissio_id, [], context=context)
        del vals['id']
        del vals['name']
        return vals

    def _get_current_comercial(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get("comercial_id", False)

    _columns = {
        'name': fields.function(_ff_generar_descripcio, string=u"Comissió", type="char", size=256, method=True),
        'comercial_id': fields.many2one("hr.employee", "Comercial")
    }

    _defaults = {
        'comercial_id': _get_current_comercial
    }

GiscedataComercialComissio()


class GiscedataPolissaComissio(osv.osv):
    _name = 'giscedata.polissa.comissio'
    _inherit = 'giscedata.comercial.comissio'
    _description = "Comissió Base per Contracte"

    def create_from_comissio_comercial(self, cursor, uid, comissio_comercial_info, context=None):
        if context is None:
            context = {}
        model_obj, model_id = comissio_comercial_info.split(",")
        vals = self.pool.get(model_obj).get_comissio_polissa_vals(cursor, uid, int(model_id), [])
        res = self.create(cursor, uid, vals, context=context)
        return res

    def facturar_comissio_polissa(self, cursor, uid, comissions_models_ids, context=None):
        if context is None:
            context = {}
        if not isinstance(comissions_models_ids, (list, tuple)):
            ids = [comissions_models_ids]
        res_msg = u""
        res_ids = []
        # Per defecte fem una ordre de compra per cada comercial, pero es pot utilitzar qualsevol camp de giscedata.polissa.comissio
        camp_agrupador = context.get("camp_agrupador", "comercial_id")
        expenses_generades = {}
        for comissions_models_id in comissions_models_ids:
            model_obj, model_id = comissions_models_id.split(",")
            model_obj = self.pool.get(model_obj)
            model_id = int(model_id)
            comissio_polissa_info = model_obj.read(cursor, uid, model_id, [camp_agrupador])
            comissio_polissa_id = comissio_polissa_info['id']
            camp_agrupador_value = comissio_polissa_info[camp_agrupador]
            if isinstance(camp_agrupador_value, (list, tuple)) and len(camp_agrupador_value) > 1:
                camp_agrupador_value = camp_agrupador_value[1]
            # Primer comprovem si per aquest la comissio del contracte falta algo per cobrar
            check_result, check_msg = model_obj.check_cobrar_comissio(cursor, uid, comissio_polissa_id, context=context)
            res_msg += check_msg
            if not check_result:
                continue
            # Mirem si ja hi ha una ordre generada per el camp agrupador. Si no hi es l'hem de crear.
            if camp_agrupador_value not in expenses_generades:
                exp_id = model_obj.crear_expense_from_comissio(cursor, uid, comissio_polissa_id, camp_agrupador_value, context=context)
                expenses_generades[camp_agrupador_value] = exp_id
            else:
                exp_id = expenses_generades.get(camp_agrupador_value)
            if exp_id:
                # Afegim una nova linia a la ordre de compra
                linies_vals = model_obj.calc_linia_expense_vals(cursor, uid, comissio_polissa_id, context=context)
                for linia_vals in linies_vals:
                    model_obj.afegir_linia_a_expense(cursor, uid, exp_id, linia_vals, context=context)
                res_ids.append(exp_id)
        return res_ids, res_msg

    def check_cobrar_comissio(self, cursor, uid, comissio_polissa_id, context=None):
        polinfo = self.read(cursor, uid, comissio_polissa_id, ['polissa_id'])['polissa_id']
        return False, _(u"* Contracte {0}: No s'ha de pagar cap comissio per aquest contracte.\n").format(polinfo[1])

    def crear_expense_from_comissio(self, cursor, uid, comissio_polissa_id, exp_name, context=None):
        if context is None:
            context = {}
        exp_obj = self.pool.get("hr.expense.expense")
        info = self.read(cursor, uid, comissio_polissa_id, ['polissa_id', 'comercial_id'])
        journal_id = self.pool.get("ir.model.data").get_object_reference(
            cursor, uid, 'giscedata_comercial_comissio', 'journal_comercials'
        )[1]
        fname = _(u"Comissions per {0} generades el {1}").format(exp_name, datetime.today().strftime("%d-%m-%y"))
        vals = {
            'name': fname,
            'employee_id': info['comercial_id'][0],
            'user_id': uid,
            'journal_id': journal_id
        }
        pid = exp_obj.create(cursor, uid, vals, context=context)
        return pid

    def calc_linia_expense_vals(self, cursor, uid, comissio_polissa_id, context=None):
        return {}

    def afegir_linia_a_expense(self, cursor, uid, exp_id, linia_vals, context=None):
        if context is None:
            context = {}
        exp_line_obj = self.pool.get("hr.expense.line")
        linia_vals['expense_id'] = exp_id
        res_id = exp_line_obj.create(cursor, uid, linia_vals)
        return res_id

    def _get_current_polissa(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get("polissa_id", False)

    _columns = {
        'polissa_id': fields.many2one("giscedata.polissa", "Polissa", required=1)
    }

    _defaults = {
        'polissa_id': _get_current_polissa
    }

GiscedataPolissaComissio()
