# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class hr_expense_expense(osv.osv):
    _name = 'hr.expense.expense'
    _inherit = 'hr.expense.expense'

    def action_invoice_create(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['extra_vals'] = {'price_type': 'tax_excluded'}
        res = super(hr_expense_expense, self).action_invoice_create(cr, uid, ids, context=ctx)
        return res


hr_expense_expense()
