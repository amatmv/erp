# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Agosto 2009",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 156 - 29/06/2009.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20090701_data.xml"
    ],
    "active": False,
    "installable": True
}
