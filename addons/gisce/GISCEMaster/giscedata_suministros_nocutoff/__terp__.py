# -*- coding: utf-8 -*-
{
    "name": "Suministros no cortables",
    "description": """Módulo para gestionar el ciclo de impagos para 
    suministros no cortables
    Art.52 P.4: https://www.boe.es/boe/dias/2013/12/27/pdfs/BOE-A-2013-13645.pdf
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "account_invoice_pending"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_suministros_nocutoff_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
