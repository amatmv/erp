# -*- encoding: utf-8 -*-
from giscedata_correos_base_flux.account_invoice import MAPPING_NOTI_STATES
from osv import osv


_MAPPING_NOTI_STATES = {
    'entregado': {
        'carta_5_pendiente_envio_nocutoff_pending_state': 'carta_5_nocutoff_pending_state',
        'carta_5_nocutoff_pending_state': False,

        'carta_6_pendiente_envio_nocutoff_pending_state': 'carta_6_nocutoff_pending_state',
        'carta_6_nocutoff_pending_state': False,

        'nocutoff_avis_c1_pending_state': False,
        'nocutoff_c1_done_state': False
    },
    'enviat': {
        'carta_5_pendiente_envio_nocutoff_pending_state': 'carta_5_nocutoff_pending_state',
        'carta_5_nocutoff_pending_state': False,

        'carta_6_pendiente_envio_nocutoff_pending_state': 'carta_6_nocutoff_pending_state',
        'carta_6_nocutoff_pending_state': False,

        'nocutoff_avis_c1_pending_state': False,
        'nocutoff_c1_done_state': False
    },
    'perdida': {
        'carta_5_pendiente_envio_nocutoff_pending_state': False,
        'carta_5_nocutoff_pending_state': 'carta_5_pendiente_envio_nocutoff_pending_state',

        'carta_6_pendiente_envio_nocutoff_pending_state': False,
        'carta_6_nocutoff_pending_state': 'carta_6_pendiente_envio_nocutoff_pending_state',

        'nocutoff_avis_c1_pending_state': False,
        'nocutoff_c1_done_state': False
    },
    'no_entrega': {
        'carta_5_pendiente_envio_nocutoff_pending_state': 'carta_5_nocutoff_pending_state',
        'carta_5_nocutoff_pending_state': False,

        'carta_6_pendiente_envio_nocutoff_pending_state': 'carta_6_nocutoff_pending_state',
        'carta_6_nocutoff_pending_state': False,

        'nocutoff_avis_c1_pending_state': False,
        'nocutoff_c1_done_state': False
    }
}


def main():
    for key in MAPPING_NOTI_STATES.keys():
        MAPPING_NOTI_STATES[key].update(_MAPPING_NOTI_STATES[key])


class AccountInvoicePendingState(osv.osv):

    _name = 'account.invoice.pending.state'
    _inherit = 'account.invoice.pending.state'

    main()

AccountInvoicePendingState()
