# -*- coding: utf-8 -*-
from osv import osv, fields
from cini.models import Subestacion, Transformador


class GiscedataCts(osv.osv):

    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def _ff_tipus_install_name(self, cursor, uid, ids, field_name, args,
                                context):
        res = dict.fromkeys(ids, False)

        vals = self.read(cursor, uid, ids, ['id_installacio'])
        for val in vals:
            if val['id_installacio']:
                res[val['id']] = val['id_installacio'][1]

        return res

    def _ff_search_tipus_install_name(self, cursor, uid, ids, field_name, args,
                                 context):

        return [('id_installacio.name', arg[1], arg[2]) for arg in args]

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = super(GiscedataCts, self)._cini(
            cursor, uid, ids, field_name, arg, context
        )
        # per tots aquests ids, els que tinguin subestació actualitzem el CINI
        # amb el càlcul de subestació
        sub_obj = self.pool.get('giscedata.cts.subestacions')
        sub_ids = sub_obj.search(cursor, uid, [
            ('ct_id', 'in', ids)
        ], context={'active_test': False})
        for sub in sub_obj.browse(cursor, uid, sub_ids, context=context):
            subestacion = Subestacion()
            try:
                tensio = int(sub.tensio_p) / 1000.0
            except (TypeError, ValueError):
                tensio = 0
            subestacion.tension = tensio
            try:
                tensio = int(sub.tensio_s) / 1000.0
            except (TypeError, ValueError):
                tensio = 0
            subestacion.tension_s = tensio
            subestacion.tension_p = subestacion.tension
            subestacion.reparto = True

            subestacion.tipo = sub.tecnologia
            for trafo in sub.transformadors:
                if trafo.id_estat:
                    if trafo.id_estat.codi == 1:
                        subestacion.reparto = False
                        tr = Transformador()
                        tr.potencia = trafo.potencia_nominal / 1000.0
                        subestacion.transformadores.append(tr)

            res[sub.ct_id.id] = str(subestacion.cini)
        return res

    def _get_cts_nobloc_cini(self, cursor, uid, ids, context=None):
        return super(GiscedataCts, self)._get_cts_nobloc_cini(
            cursor, uid, ids, context
        )

    def _get_cts_subestacio(self, cursor, uid, ids, context=None):
        """Busquem si aquest CT és una subestació
        """
        sub_obj = self.pool.get('giscedata.cts.subestacions')
        cts = []
        for sub in sub_obj.read(cursor, uid, ids, ['ct_id']):
            if sub['ct_id']:
                cts.append(sub['ct_id'][0])
        if cts:
            ct_obj = self.pool.get('giscedata.cts')
            cts = ct_obj._get_cts_nobloc_cini(cursor, uid, cts)
        return cts

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        return super(GiscedataCts, self)._cini_inv(
            cursor, uid, ids, name, value, args, context
        )

    _columns = {
        'id_installacio_name': fields.function(_ff_tipus_install_name,
            type="char", method=True, string='Nom tipus Instal·lació',
            fnct_search = _ff_search_tipus_install_name,
            help="Tipus d'instal·lació per cercador",),
        'cini': fields.function(
            _cini, type='char', size=8, method=True, store={
                'giscedata.cts': (
                    _get_cts_nobloc_cini, [
                        'tensio_p', 'tensio_s', 'bloquejar_cini',
                        'transformadors', 'id_subtipus'
                    ], 10
                ),
                'giscedata.cts.subestacions': (
                    _get_cts_subestacio, [
                        'tecnologia'
                    ], 10
                )
            }, fnct_inv=_cini_inv, string='CINI'
        ),
    }

GiscedataCts()
