# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    oopgrade.rename_columns(cursor, {
        'giscedata_cts': [('cini', 'cini_pre_auto_sub')],
        'giscedata_parcs': [('cini', 'cini_pre_auto_parc')],
        'giscedata_cts_subestacions_posicio': [('cini', 'cini_pre_auto_pos')]
    })


def down(cursor):
    oopgrade.rename_columns(cursor, {
        'giscedata_cts': [('cini_pre_auto_sub', 'cini')],
        'giscedata_parcs': [('cini_pre_auto_parc', 'cini')],
        'giscedata_cts_subestacions_posicio': [('cini_pre_auto_pos', 'cini')]
    })


migrate = up
