# coding=utf-8
def migrate(cursor, installed_version):
    if not installed_version:
        return
    cursor.execute(
        "UPDATE giscedata_cts set cini = cini_pre_auto_sub where bloquejar_cini")
    cursor.execute(
        "UPDATE giscedata_parcs set cini = cini_pre_auto_parc where bloquejar_cini")
    cursor.execute(
        "UPDATE giscedata_cts_subestacions_posicio set cini = cini_pre_auto_pos where bloquejar_cini")


up = migrate
