# coding=utf-8
import pooler


def migrate(cursor, installed_version):
    """
    Migration script that fills the producte field

    :param cursor: Database cursor
    :param installed_version: Installed version on of the module
    :return: None
    """
    pool = pooler.get_pool(cursor.dbname)
    obj_pos = pool.get('giscedata.cts.subestacions.posicio')
    ids = obj_pos.search(cursor, 1, [])
    for identifier in ids:
        pos = obj_pos.browse(cursor, 1, identifier)
        if pos.serial:
            pos.write({'producte': pos.serial.product_id.id})


up = migrate
