# -*- coding: utf-8 -*-
import netsvc


def migrate(cursor, installed_version):
    logger = netsvc.Logger()

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         'Renaming expedients to expedients_tmp')

    sql_alter = """
        ALTER TABLE giscedata_cts_subestacions_posicio
        RENAME COLUMN  expedient TO expedient_tmp;
    """
    cursor.execute(sql_alter)
