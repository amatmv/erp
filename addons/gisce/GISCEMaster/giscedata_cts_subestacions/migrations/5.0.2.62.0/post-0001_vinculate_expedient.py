# -*- coding: utf-8 -*-
import netsvc


def migrate(cursor, installed_version):
    logger = netsvc.Logger()

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         'Linking the expedients with subestacions')

    sql_insert = """
        INSERT INTO giscedata_expedients_expedient_cts_subestacions_posicio_rel
          (SELECT "id",expedient_tmp
          FROM giscedata_cts_subestacions_posicio
          WHERE expedient_tmp IS NOT NULL)
    """

    sql_drop_colum = """
      ALTER TABLE giscedata_cts_subestacions_posicio
      DROP COLUMN expedient_tmp;
    """

    # Vincular expedients amb posicions
    cursor.execute(sql_insert)

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         'Drop temporal expedients column')
    cursor.execute(sql_drop_colum)
