# -*- coding: utf-8 -*-
{
    "name": "CTS Subestacions",
    "description": """
    This module provide :
        * Gestió de subestacions
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_revisions",
        "giscedata_transformadors",
        "giscedata_tensions",
        "product",
        "stock",
        "giscedata_administracio_publica_cnmc_distri"
    ],
    # "extra_depends":[
    #     "giscedata_cts"
    # ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_cts_subestacions_demo.xml"
    ],
    "update_xml":[
        "wizard/wizard_subest_2_cts.xml",
        "wizard/wizard_cts_2_subest.xml",
        "giscedata_cts_subestacions_view.xml",
        "giscedata_centrestransformadors_view.xml",
        "security/ir.model.access.csv",
        "giscedata_parcs_view.xml",
        "giscedata_tipus_installacio_posicions_data.xml"
    ],
    "active": False,
    "installable": True
}
