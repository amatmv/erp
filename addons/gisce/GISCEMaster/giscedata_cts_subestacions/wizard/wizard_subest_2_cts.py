# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class subest2cts(osv.osv_memory):
    _name = 'giscedata.subest.2.cts'

    def convertir(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        msg = ''
        state = 'error'
        if wizard.subest.posicions and wizard.subest.parc_id:
            msg = _(
                u'No es pot convertir a CT mentre '
                u'estigui vinculada a PARC o tingui POSICIONS'
            )
        elif wizard.subest.posicions:
            msg = _(
                u'No es pot convertir a CT mentre '
                u'tingui POSICIONS'
            )
        elif wizard.subest.parc_id:
            msg = _(
                u'No es pot convertir a CT mentre '
                u'estigui vinculada a PARC'
            )
        else:
            cts_obj = self.pool.get('giscedata.cts')
            values = {
                'id_installacio': wizard.itype,
            }
            cts_obj.write(cursor, uid, wizard.subest.ct_id.id, values)
            subest_obj = self.pool.get('giscedata.cts.subestacions')
            subest_obj.unlink(cursor, uid, wizard.subest.id)
            state = 'end'
        wizard.write({
            'msg': msg,
            'state': state,
        })

    def _tipus_installacio(self, cr, uid, context={}):
        obj = self.pool.get('giscedata.cts.installacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = [(r['id'], r['name']) for r in res]
        se = obj.search(cr, uid, [('name', '=', 'SE')])
        if se and len(se):
            res.remove((se[0], u'SE'))
        return res

    def _subest(self, cursor, uid, context={}):
        if context.get('from_model', '') == 'giscedata.cts.subestacions':
            return context.get('active_ids')[0]
        return False

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('error', 'Error'), ('end', 'End'), ], 'State'
        ),
        'subest': fields.many2one(
            'giscedata.cts.subestacions', 'Subestació a convertir',
            required=True
        ),
        'itype': fields.selection(_tipus_installacio, 'Tipus d\'instal·lació', required=True),
        'msg': fields.text('Error', readonly=True)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'subest': _subest,
    }

subest2cts()
