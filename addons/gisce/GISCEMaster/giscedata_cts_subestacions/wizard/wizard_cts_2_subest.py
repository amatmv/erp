# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class cts2subest(osv.osv_memory):
    _name = 'giscedata.cts.2.subest'

    def convertir(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        cts = wizard.cts_sel['id']
        subs_obj = self.pool.get('giscedata.cts.subestacions')
        search_params = [('ct_id', '=', cts)]
        found = subs_obj.search(cursor, uid, search_params, 0)
        if found:
            raise Exception(_(u'Aquest CT ja és una subestació'))
        search_params = [('name', '=', 'SE')]
        installacio_obj = self.pool.get('giscedata.cts.installacio')
        id_instal = installacio_obj.search(cursor, uid, search_params, 0)[0]
        values = {
            'ct_id': cts, 'tipus_parc': 'C', 'tecnologia': 'C',
            'id_installacio': id_instal
        }
        subs_obj.create(cursor, uid, values)
        wizard.write({
            'state': 'end',
        })

    def _cts(self, cursor, uid, context=None):
        if context.get('from_model', '') == 'giscedata.cts':
            return context.get('active_ids')[0]
        else:
            return False

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End'), ], 'State'
        ),
        'cts_sel': fields.many2one(
            'giscedata.cts', 'CTs a transformar', required=True
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'cts_sel': _cts,
    }

cts2subest()
