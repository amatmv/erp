# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
#   <>, 2017, 2018, 2019.
#   <misernroca@gisce.net>, 2017.
# Agustí Fita <afita@gisce.net>, 2014.
# Jaume  Florez Valenzuela <jflorez@gisce.net>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 11:56\n"
"PO-Revision-Date: 2019-07-29 10:38+0000\n"
"Last-Translator: lcbautista <>\n"
"Language-Team: Spanish (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions.posicio:0
#: field:giscedata.cts.subestacions.posicio,bloquejar_cini:0
#: field:giscedata.parcs,bloquejar_cini:0
msgid "Bloquejar CINI"
msgstr "Bloquear CINI"

#. module: giscedata_cts_subestacions
#: model:ir.model,name:giscedata_cts_subestacions.model_giscedata_parcs
msgid "giscedata.parcs"
msgstr "giscedata.parcs"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions,tecnologia:0
#: selection:giscedata.cts.subestacions.posicio,tipus_posicio:0
#: selection:giscedata.parcs,tecnologia:0
msgid "Blindada"
msgstr "Blindada"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions.posicio:0
#: field:giscedata.cts.subestacions.posicio,observacions:0
msgid "Observacions"
msgstr "Observaciones"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,4666_entregada_2018:0
msgid "Dades entregades 4666 2018"
msgstr "Datos entregados 4666 2018"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,4666_entregada_2019:0
msgid "Dades entregades 4666 2019"
msgstr "Datos entregadas 4666 2019"

#. module: giscedata_cts_subestacions
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: giscedata_cts_subestacions
#: selection:giscedata.parcs,localitzacio:0
msgid "Interior"
msgstr "Interior"

#. module: giscedata_cts_subestacions
#: view:giscedata.subest.2.cts:0
msgid "Transformar en CT"
msgstr "Transformar en CT"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,4666_entregada_2017:0
msgid "Dades entregades 4666 2017"
msgstr "Datos entregados 4666 2017"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,tensio:0
msgid "Tensió"
msgstr "Tensión"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,barres:0
#: selection:giscedata.parcs,barres:0
msgid "Simple barra partida"
msgstr "Simple barra partida"

#. module: giscedata_cts_subestacions
#: view:giscedata.subest.2.cts:0
msgid "Transformar Subestacions en CTs"
msgstr "Transformar Subestaciones en CTs"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,4771_entregada:0
msgid "Dades 4771 entregada"
msgstr "Datos 4771 entregada"

#. module: giscedata_cts_subestacions
#: help:giscedata.cts.subestacions.posicio,bloquejar_cnmc_tipus_install:0
msgid ""
"Si està activat, permet modificar manualment el tipus d'instal·lació de la "
"CNMC"
msgstr "Si esta activado, permite modificar manualmente el tipo de instalacion de la CNMC"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,producte:0
msgid "Marca i model"
msgstr "Marca y modelo"

#. module: giscedata_cts_subestacions
#: model:ir.model,name:giscedata_cts_subestacions.model_giscedata_cts_2_subest
msgid "giscedata.cts.2.subest"
msgstr "giscedata.cts.2.subest"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,posicio_linia:0
msgid "Funció"
msgstr "Función"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,tipus:0
msgid "Tipus de parc"
msgstr "Tipo de parque"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,perc_financament:0
msgid "% pagat per la companyia"
msgstr "% pagado por la compañía"

#. module: giscedata_cts_subestacions
#: code:addons/giscedata_cts_subestacions/wizard/wizard_subest_2_cts.py:15
#, python-format
msgid ""
"No es pot convertir a CT mentre estigui vinculada a PARC o tingui POSICIONS"
msgstr "No se puede convertir a CT mientras esté vinculada a PARQUE o tenga POSICIONES"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,4131_entregada_2016:0
msgid "Dades 4131 entregada 2016"
msgstr "Datos 4131 entregados 2016"

#. module: giscedata_cts_subestacions
#: model:ir.module.module,description:giscedata_cts_subestacions.module_meta_information
msgid ""
"\n"
"    This module provide :\n"
"        * Gestió de subestacions\n"
"    "
msgstr "\n    This module provide :\n        * Gestión de subestaciones\n    "

#. module: giscedata_cts_subestacions
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_wizard_subest_2_cts_menu_form
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_wizard_subest_2_cts_se_form
msgid "Convertir Subestació a CT"
msgstr "Convertir Subestación a CT"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,subestacio_id:0
msgid "Subestacio"
msgstr "Subestación"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.2.subest:0
msgid "Transformar en subestació"
msgstr "Transformar en subestación"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,cini:0
#: field:giscedata.parcs,cini:0
msgid "CINI"
msgstr "CINI"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions:0
msgid "Incidències BT"
msgstr "Incidencias BT"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,posicio_linia:0
msgid "Línia"
msgstr "Línea"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,barres:0
#: selection:giscedata.parcs,barres:0
msgid "Tipus H"
msgstr "Tipo H"

#. module: giscedata_cts_subestacions
#: code:addons/giscedata_cts_subestacions/wizard/wizard_cts_2_subest.py:17
#, python-format
msgid "Aquest CT ja és una subestació"
msgstr "Este CT ya es una subestación"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions.posicio:0 view:giscedata.parcs:0
msgid "Dades administratives"
msgstr "Datos administrativos"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions:0
msgid "Sortides BT"
msgstr "Salidas BT"

#. module: giscedata_cts_subestacions
#: model:ir.model,name:giscedata_cts_subestacions.model_giscedata_subest_2_cts
msgid "giscedata.subest.2.cts"
msgstr "giscedata.subest.2.cts"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.2.subest,state:0
#: selection:giscedata.subest.2.cts,state:0
msgid "End"
msgstr "End"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.2.subest,state:0 field:giscedata.subest.2.cts,state:0
msgid "State"
msgstr "State"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,posicio_linia:0
msgid "Acoblament"
msgstr "Acoblamiento"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,name:0
#: field:giscedata.parcs,name:0
msgid "Codi"
msgstr "Código"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,interruptor:0
msgid "Parc"
msgstr "Parque"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.2.subest:0
msgid "Convertir a Subestacio"
msgstr "Convertir a Subestación"

#. module: giscedata_cts_subestacions
#: selection:giscedata.parcs,tipus:0
msgid "Distribucio"
msgstr "Distribución"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,interruptor:0
msgid "Tipus Interruptor"
msgstr "Tipo Interruptor"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,criteri_regulatori:0
#: selection:giscedata.parcs,criteri_regulatori:0
msgid "Segons criteri"
msgstr "Según criterio"

#. module: giscedata_cts_subestacions
#: help:giscedata.cts,id_installacio_name:0
msgid "Tipus d'instal·lació per cercador"
msgstr "Tipo de instalación para buscador"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,bloquejar_pm:0
msgid "Bloquejar APM"
msgstr "Bloquear APM"

#. module: giscedata_cts_subestacions
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions,tecnologia:0
#: selection:giscedata.cts.subestacions.posicio,tipus_posicio:0
#: selection:giscedata.parcs,tecnologia:0
msgid "Convencional"
msgstr "Convencional"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,barres:0
#: selection:giscedata.parcs,barres:0
msgid "Doble barra partida"
msgstr "Doble barra partida"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,criteri_regulatori:0
#: selection:giscedata.parcs,criteri_regulatori:0
msgid "Forçar incloure"
msgstr "Forzar incluir"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,data_pm:0
#: field:giscedata.parcs,data_apm:0
msgid "Data APM"
msgstr "Fecha APM"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.2.subest,cts_sel:0
msgid "CTs a transformar"
msgstr "CTs a transformar"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions.posicio:0
#: field:giscedata.cts.subestacions.posicio,expedient:0
msgid "Expedient"
msgstr "Expediente"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,posicio_linia:0
msgid "Transformador"
msgstr "Transformador"

#. module: giscedata_cts_subestacions
#: code:addons/giscedata_cts_subestacions/wizard/wizard_subest_2_cts.py:25
#, python-format
msgid "No es pot convertir a CT mentre estigui vinculada a PARC"
msgstr "No se puede convertir a CT mientras esté vinculada a PARQUE"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.2.subest:0
msgid "Transformar CTs en Subestacions"
msgstr "Transformar CTs en Subestaciones"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,barres:0
#: field:giscedata.parcs,barres:0
#: model:ir.ui.menu,name:giscedata_cts_subestacions.menu_subestacions_configuracio
msgid "Configuració"
msgstr "Configuración"

#. module: giscedata_cts_subestacions
#: field:giscedata.subest.2.cts,itype:0
msgid "Tipus d'instal·lació"
msgstr "Tipo de instalación"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,interruptor:0
msgid "Interruptor automàtic"
msgstr "Interruptor automático"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,posicio_ids:0
msgid "Posicio"
msgstr "Posición"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,active:0
msgid "Actiu"
msgstr "Activo"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,bloquejar_cnmc_tipus_install:0
msgid "Bloquejar Tipus Instal·lació CNMC"
msgstr "Bloquear Tipo Instalación CNMC"

#. module: giscedata_cts_subestacions
#: selection:giscedata.parcs,localitzacio:0
msgid "Intempèrie"
msgstr "Intemperie"

#. module: giscedata_cts_subestacions
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,cedida:0
msgid "Cedida"
msgstr "Cedida"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions:0
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_subestacions_tree
#: model:ir.model,name:giscedata_cts_subestacions.model_giscedata_cts_subestacions
#: model:ir.ui.menu,name:giscedata_cts_subestacions.menu_cts_manteniment_subestacions
#: model:ir.ui.menu,name:giscedata_cts_subestacions.menu_cts_manteniment_subestacions_subestacions
msgid "Subestacions"
msgstr "Subestaciones"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.2.subest:0 view:giscedata.subest.2.cts:0
msgid "Finalitzar"
msgstr "Finalitzar"

#. module: giscedata_cts_subestacions
#: field:giscedata.subest.2.cts,subest:0
msgid "Subestació a convertir"
msgstr "Subestación a convertir"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,active:0
msgid "Activa"
msgstr "Activa"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,interruptor:0
msgid "Sense interruptor"
msgstr "Sin interruptor"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,serial:0
msgid "Número sèrie"
msgstr "Número serie"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,criteri_regulatori:0
#: field:giscedata.parcs,criteri_regulatori:0
msgid "Criteri regulatori"
msgstr "Criterio regulatorio"

#. module: giscedata_cts_subestacions
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_wizard_cts_2_subest_cts_form
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_wizard_cts_2_subest_menu_form
msgid "Convertir CTs a Subestació"
msgstr "Convertir CTs a Subestación"

#. module: giscedata_cts_subestacions
#: help:giscedata.cts.subestacions.posicio,criteri_regulatori:0
msgid ""
"Indica si el criteri a seguir al genrar els fitxers d'inventari.\n"
"Segons criteri:S'usa el criteri normal\n"
" Froçar incloure:S'afegeix l'element al fitxer\n"
" Forçar excloure: L'element no apareixera mai"
msgstr "Indica si el criterio a seguir al generar los archivos de inventario.\nSegún criterio: Se usa el criterio normal\n Forzar incluir: Se añade el elemento al archivo\n Forzar excluir: El elemento no aparecerá nunca"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,cnmc_tipo_instalacion:0
msgid "CNMC Tipus Instal·lació"
msgstr "CNMC Tipos Instal·lación"

#. module: giscedata_cts_subestacions
#: help:giscedata.cts.subestacions.posicio,data_pm:0
msgid "Data de l'acta de posada en marxa de la instal·lació"
msgstr "Fecha de acta de puesta en marcha de la instalación"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts,id_installacio_name:0
msgid "Nom tipus Instal·lació"
msgstr "Nombre tipo Instalación"

#. module: giscedata_cts_subestacions
#: field:giscedata.subest.2.cts,msg:0 selection:giscedata.subest.2.cts,state:0
msgid "Error"
msgstr "Error"

#. module: giscedata_cts_subestacions
#: help:giscedata.parcs,criteri_regulatori:0
msgid ""
"Indica si el criteri a seguir al genrar els fitxers d'inventari.\n"
"Segons criteri:S'usa el criteri normal\n"
" Forçar incloure:S'afegeix l'element al fitxer\n"
" Forçar excloure: L'element no apareixera mai"
msgstr "Indica si el criterio a seguir al generar los ficheros de inventario.\nSegún criterio:Se utiliza el criterio normal\n Forzar incluir:Se agrega el elemento al fichero\n Forzar excluir: El elemento no aparecerá nunca"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions,ct_id:0
msgid "CT"
msgstr "CT"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions,tecnologia:0
#: field:giscedata.cts.subestacions.posicio,tipus_posicio:0
#: field:giscedata.parcs,tecnologia:0
msgid "Tecnologia"
msgstr "Tecnología"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,telemando:0
msgid "Telemando"
msgstr "Telemando"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions:0
#: field:giscedata.cts.subestacions.posicio,subestacio_id:0
msgid "Subestació"
msgstr "Subestación"

#. module: giscedata_cts_subestacions
#: selection:giscedata.parcs,tipus:0
msgid "Maniobra"
msgstr "Maniobra"

#. module: giscedata_cts_subestacions
#: view:giscedata.parcs:0
msgid "General"
msgstr "General"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,criteri_regulatori:0
#: selection:giscedata.parcs,criteri_regulatori:0
msgid "Forçar excloure"
msgstr "Forzar excluir"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,data_baixa:0
msgid "Data de Baixa"
msgstr "Fecha Baja"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,alies:0
msgid "Àlies"
msgstr "Alias"

#. module: giscedata_cts_subestacions
#: selection:giscedata.parcs,tipus:0
msgid "Condensador"
msgstr "Condensador"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,barres:0
#: selection:giscedata.parcs,barres:0
msgid "Altres"
msgstr "Otros"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,tipus_posicio:0
#: selection:giscedata.parcs,tecnologia:0
msgid "Híbrida"
msgstr "Híbrida"

#. module: giscedata_cts_subestacions
#: model:ir.model,name:giscedata_cts_subestacions.model_giscedata_cts_subestacions_posicio
msgid "Posicions de les Subestacions"
msgstr "Posiciones de las Subestaciones"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,tipus_instalacio_cnmc_id:0
msgid "Tipologia CNMC"
msgstr "Tipología CNMC"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.2.subest,state:0
#: selection:giscedata.subest.2.cts,state:0
msgid "Init"
msgstr "Init"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions:0
#: field:giscedata.cts.subestacions,parc_id:0
#: field:giscedata.cts.subestacions.posicio,parc_id:0 view:giscedata.parcs:0
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_giscedata_parcs_tree
#: model:ir.ui.menu,name:giscedata_cts_subestacions.menu_giscedata_parcs_tree
msgid "Parcs"
msgstr "Parques"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,tensio_id:0
msgid "Tensio"
msgstr "Tensión"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.2.subest:0
msgid "Tancar"
msgstr "Cerrar"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,localitzacio:0
msgid "Localitzacio"
msgstr "Localizacion"

#. module: giscedata_cts_subestacions
#: view:giscedata.subest.2.cts:0
msgid "Convertir a Centre Transformador"
msgstr "Convertir a Centro Transformador"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions:0
#: field:giscedata.cts.subestacions,posicions:0
#: view:giscedata.cts.subestacions.posicio:0 view:giscedata.parcs:0
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_posicions_tree
#: model:ir.ui.menu,name:giscedata_cts_subestacions.menu_cts_manteniment_subestacions_posicions
msgid "Posicions"
msgstr "Posiciones"

#. module: giscedata_cts_subestacions
#: model:ir.module.module,shortdesc:giscedata_cts_subestacions.module_meta_information
msgid "CTS Subestacions"
msgstr "CTS Subestaciones"

#. module: giscedata_cts_subestacions
#: selection:giscedata.parcs,tipus:0
msgid "Generacio"
msgstr "Generación"

#. module: giscedata_cts_subestacions
#: field:giscedata.parcs,data_baixa:0
msgid "Data baixa"
msgstr "Data Baja"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,barres:0
#: selection:giscedata.parcs,barres:0
msgid "Doble barra"
msgstr "Doble barra"

#. module: giscedata_cts_subestacions
#: model:ir.actions.act_window,name:giscedata_cts_subestacions.action_subestacions_tree_invalid_cini
#: model:ir.ui.menu,name:giscedata_cts_subestacions.menu_cts_manteniment_subestacions_subestacions_invalid_cini
msgid "Subestacions (CINI invàlid)"
msgstr "Subestaciones (CINI inválido)"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,propietari:0
#: field:giscedata.parcs,propietari:0
msgid "Propietari"
msgstr "Propietario"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,posicio_linia:0
msgid "Mesura"
msgstr "Medida"

#. module: giscedata_cts_subestacions
#: help:giscedata.cts.subestacions.posicio,bloquejar_pm:0
msgid "Si està activat, agafa la data entrada, si no, la de ,l'expedient"
msgstr "Si está activado, coge la fecha de entrada, sino la del expediente"

#. module: giscedata_cts_subestacions
#: field:giscedata.cts.subestacions.posicio,motoritzacio:0
msgid "Motorització"
msgstr "Motorización"

#. module: giscedata_cts_subestacions
#: view:giscedata.cts.subestacions.posicio:0 view:giscedata.parcs:0
msgid "Dades tècniques"
msgstr "Datos técnicos"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,posicio_linia:0
msgid "Reserva"
msgstr "Reserva"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions,tecnologia:0
#: selection:giscedata.parcs,localitzacio:0
msgid "Mòbil"
msgstr "Móvil"

#. module: giscedata_cts_subestacions
#: view:giscedata.subest.2.cts:0
msgid "Cancelar"
msgstr "Cancelar"

#. module: giscedata_cts_subestacions
#: selection:giscedata.cts.subestacions.posicio,barres:0
#: selection:giscedata.parcs,barres:0
msgid "Simple barra"
msgstr "Simple barra"

#. module: giscedata_cts_subestacions
#: code:addons/giscedata_cts_subestacions/wizard/wizard_subest_2_cts.py:20
#, python-format
msgid "No es pot convertir a CT mentre tingui POSICIONS"
msgstr "No se puede convertir a CT mientras tenga POSICIONES"
