#!/usr/bin/env python
import sys
from ooop import OOOP

dbname, port, user, pwd = sys.argv[1:]

O = OOOP(dbname=dbname, port=int(port), user=user, pwd=pwd)

subs = [x['ct_id'] and x['ct_id'][0] for x in
        O.GiscedataCtsSubestacions.read(
            O.GiscedataCtsSubestacions.search([]), ['ct_id']
        )]
search_params = [('id_installacio.name', '=', 'SE')]
if subs:
    search_params = [('id', 'not in', subs)]
cts2sub = O.GiscedataCts.search(search_params, 0, 0, False,
                                {'active_test': False})
for sub in cts2sub:
    O.GiscedataCtsSubestacions.create({'ct_id': sub, 'tipus_parc': 'C'})
print "%s Subestactions creades" % len(cts2sub)
