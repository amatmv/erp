# -*- coding: utf-8 -*-
from osv import osv, fields
from lxml.etree import fromstring as parse_view, tostring
import time
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI

MODELS_SELECTION.append(
    ('giscedata.cts.subestacions', 'Subestacions')
)

MODELS_SELECTION.append(
    ('giscedata.cts.subestacions.posicio', 'Posicions')
)

TIPUS_PARC = [
    ('B', 'Blindada'),
    ('C', 'Convencional'),
    ('M', 'Mòbil')
]


TIPUS_POSICIO = [
    ('B', 'Blindada'),
    ('C', 'Convencional'),
    ('H', 'Híbrida')
]

TIPUS_LOCALITZACIO = [
    ('I','Interior'),
    ('O','Intempèrie'),
    ('M','Mòbil'),
]

POSICIO_LINIA = [
    ('L', 'Línia'),
    ('T', 'Transformador'),
    ('C', 'Acoblament'),
    ('M', 'Mesura'),
    ('R', 'Reserva'),
]


TIPUS_INTERRUPTOR = [
    ('1', 'Parc'),
    ('2', 'Interruptor automàtic'),
    ('3', 'Sense interruptor')
]


TIPUS_BARRES = [
    ('A', 'Simple barra'),
    ('B', 'Simple barra partida'),
    ('C', 'Doble barra'),
    ('D', 'Doble barra partida'),
    ('E', 'Tipus H'),
    ('Z', 'Altres')
]


class GiscedataCtsSubestacions(osv.osv):
    """Subestacions"""
    _name = 'giscedata.cts.subestacions'
    _description = "Subestacions"
    _inherits = {'giscedata.cts': 'ct_id'}

    def write(self, cr, user, ids, vals, context=None):
        return super(GiscedataCtsSubestacions, self).write(
            cr, user, ids, vals, context
        )

    def _get_base_ids(self, cursor, uid, ids, context=None):
        """Definim una funció on li passarem els ids de la nostra factura i ens
        retornarà els ids de les factures bases relacionades
        """
        ct_o = self.pool.get('giscedata.cts')
        res = [a['ct_id'][0] for a in self.read(cursor, uid, ids, ['ct_id'], context)]
        return ct_o.search(cursor, uid, [('id', 'in', res)], order='id asc')

    def onchange_imax(self, cr, uid, id, u, ro, rm, xo):
        id = self._get_base_ids(cr, uid, id)
        res = self.pool.get('giscedata.cts').onchange_imax(
            cr, uid, id, u, ro, rm, xo
        )
        return res

    def onchange_prof_eq(self, cr, uid, id, dist_piques, resistencia, num):
        id = self._get_base_ids(cr, uid, id)
        res = self.pool.get('giscedata.cts').onchange_prof_eq(
            cr, uid, id, dist_piques, resistencia, num
        )
        return res

    def onchange_resistencia(self, cr, uid, id, resistencia, k, num):
        id = self._get_base_ids(cr, uid, id)
        res = self.pool.get('giscedata.cts').onchange_resistencia(
            cr, uid, id, resistencia, k, num
        )
        return res

    def name_search(self, cursor, uid, name='', args=[], operator='ilike',
                    context=None, limit=80):
        if not context:
            context = {}
        if name and len(name) > 2:
            ids = self.search(cursor, uid, [
                '|',
                ('name', operator, name),
                ('descripcio', operator, name)
            ] + args, limit=limit, context=context)

            return self.name_get(cursor, uid, ids)
        else:
            return super(GiscedataCtsSubestacions, self).name_search(
                cursor, uid, name, args, operator, context, limit
            )

    def name_get(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        if not ids:
            return []
        res = []
        for subestacio in self.read(cr, uid, ids, ['name', 'descripcio']):
            name = subestacio['name'] or ''
            if subestacio['descripcio']:
                format_data = (name.strip(),  subestacio['descripcio'].strip())
                name = '{0} "{1}"'.format(*format_data)
            res.append((subestacio['id'], name))
        return res

    def fields_view_get(self, cursor, user, view_id=None, view_type='form',
                        context=None, toolbar=False):
        """Sobreescrivim la vista per tal d'eliminar tots els elements 'BT'.
        """
        result = super(GiscedataCtsSubestacions,
                       self).fields_view_get(cursor, user, view_id, view_type,
                                             context, toolbar)
        if view_type == 'form':
            bt_fields = []
            for v_field, d_field in result['fields'].items():
                if ('BT' in d_field.get('string', '')
                        and not d_field.get('required', False)):
                    bt_fields.append(v_field)
            view = parse_view(result['arch'])
            # Eliminem pestanyes BT
            for page in view.getroottree().findall('//page'):
                if 'BT' in page.attrib.get('string', ''):
                    page.getparent().remove(page)
            # Eliminem camps que siguin BT no obligatoris
            for v_field in view.getroottree().findall('//fields'):
                if v_field.attrib.get('name', '') in bt_fields:
                    v_field.getparent().remove(v_field)
            result['arch'] = tostring(view)
        return result

    _columns = {
        'ct_id': fields.many2one('giscedata.cts', 'CT', required=True),
        'posicions': fields.one2many('giscedata.cts.subestacions.posicio',
                                     'subestacio_id', 'Posicions'),
        'parc_id': fields.one2many('giscedata.parcs', 'subestacio_id',
                                   'Parcs'),
        'tecnologia': fields.selection(TIPUS_PARC, 'Tecnologia',
                                       required=True),
    }

GiscedataCtsSubestacions()


class GiscedataParcs(osv.osv):

    _name = 'giscedata.parcs'

    TIPUS_DISTRIBUCIO = 1
    TIPUS_MANIOBRA = 2
    TIPUS_CONDENSADOR = 3
    TIPUS_GENERACIO = 4

    def unlink(self, cursor, uid, ids, context=None):
        '''
        Function that manages the deletion of the model

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param context: OpenERP context
        :return:
        '''

        if context is None:
            context = {}
        if context.get('permanent'):
            parcs = super(GiscedataParcs, self)
            return parcs.unlink(cursor, uid, ids, context)
        else:
            no_data_baixa = []
            for element in self.read(cursor, uid, ids, ['data_baixa']):
                if not element['data_baixa']:
                    no_data_baixa.append(element['id'])

            self.write(cursor, uid, ids, {'active': 0})
            if no_data_baixa:
                self.write(cursor, uid, no_data_baixa, {
                    'data_baixa': time.strftime('%Y-%m-%d')
                })
            return True

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from cini.models import Parque
        for p in self.browse(cursor, uid, ids):
            parque = Parque()
            if p.tensio_id:
                try:
                    parque.tension = p.tensio_id.tensio / 1000.0
                except TypeError:
                    parque.tension = 0
            if p.barres:
                parque.barras = p.barres

            if p.tecnologia:
                parque.tipo = p.tecnologia

            res[p.id] = str(parque.cini)
        return res

    def _cini_inv(self, cursor, ui9d, ids, name, value, args, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_parcs set cini=%s "
                       "where id in %s and bloquejar_cini",
                       (value or None, tuple(ids),))
        return True

    def _get_parcs_nobloc_cini(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_parcs "
                       "where not bloquejar_cini and id in %s", (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    _columns = {
        'name': fields.char('Codi', size=256, required=True),
        'subestacio_id': fields.many2one(
            'giscedata.cts.subestacions', 'Subestacio', required=True),
        'cini': fields.function(_cini,
            type='char', size=8, method=True, store={
                'giscedata.parcs': (
                    _get_parcs_nobloc_cini, [
                        'bloquejar_cini', 'barres', 'tecnologia', 'tensio_id'
                    ], 10
                )
            }, fnct_inv=_cini_inv, string='CINI'
        ),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'tipus': fields.selection([
            (TIPUS_DISTRIBUCIO, 'Distribucio'),
            (TIPUS_CONDENSADOR, 'Condensador'),
            (TIPUS_MANIOBRA, 'Maniobra'),
            (TIPUS_GENERACIO, 'Generacio')
        ], 'Tipus de parc', required=True),
        'barres': fields.selection(TIPUS_BARRES, 'Configuració'),
        'tecnologia': fields.selection(TIPUS_POSICIO, 'Tecnologia'),
        'tensio_id': fields.many2one('giscedata.tensions.tensio', 'Tensio',
                                     required=True),
        'data_apm': fields.date('Data APM', required=True),
        'data_baixa': fields.date('Data baixa'),
        'propietari': fields.boolean('Propietari'),
        'active': fields.boolean('Actiu'),
        'posicio_ids': fields.one2many(
            'giscedata.cts.subestacions.posicio', 'parc_id', 'Posicio'),
        'localitzacio': fields.selection(
            TIPUS_LOCALITZACIO, 'Localitzacio'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Forçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
    }

    _defaults = {
        'active': lambda *a: 1,
        'propietari': lambda *a: 1,
        "criteri_regulatori": lambda *a: "criteri"
    }

GiscedataParcs()


class GiscedataCtsSubestacionsPosicio(osv.osv):
    """
    Posicions de les Subestacions
    """

    _name = 'giscedata.cts.subestacions.posicio'
    _description = "Posicions de les Subestacions"

    def name_get(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        if not ids:
            return []
        res = []
        # Force ids to be list because if ids it's just an element
        # the read returns a single dictionary and not a list
        # what makes the for iterate over the dict keys and crash when
        # we try to read dictionary entries.
        if isinstance(ids, (int, long)):
            ids = [ids]
        for posicio in self.read(cr, uid, ids, ['alies', 'name']):
            name = posicio['name'] or ''
            if posicio['alies']:
                format_data = (name.strip(),  posicio['alies'].strip())
                name = '{0} "{1}"'.format(*format_data)
            res.append((posicio['id'], name))
        return res

    def _data_pm(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        for item in self.browse(cursor, uid, ids, context):
            if item.expedient and not item.bloquejar_pm:
                res[item.id] = item.expedient[0].industria_data
            else:
                res[item.id] = item.data_pm
        return res

    def _get_posicions(self, cursor, uid, ids, context=None):
        cursor.execute("""
            SELECT p.id
            FROM giscedata_cts_subestacions_posicio AS p
            INNER JOIN giscedata_expedients_expedient_cts_subestacions_posicio_rel e ON p.id = e.posicio_id
            WHERE e.expedient_id IN %s AND NOT p.bloquejar_pm;
        """, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_posicions_nobloc(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_cts_subestacions_posicio "
        "where not bloquejar_pm and id in %s", (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_posicions_nobloc_cini(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_cts_subestacions_posicio "
                       "where not bloquejar_cini and id in %s", (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _get_sub_nobloc_cini(self, cursor, uid, ids, context=None):
        """
        Gets the list of posicions to calculate the cini when the parc data
        changes

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Parcs ids
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict
        :return: List of posicions to recalculate the CINI
        :rtype: list of int
        """
        sql = """
          SELECT id
          FROM giscedata_cts_subestacions_posicio
          WHERE not bloquejar_cini and parc_id in %s
        """
        cursor.execute(sql, (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Function to calculate the cini field

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Posicions ids to calc
        :param field_name: Name of the field to calculate
        :param arg:
        :param context: OpenERP context
        :return: Calculated CINI ( posicio id , cini)
        :rtype: dict
        """

        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from cini.models import Posicion
        for pos in self.browse(cursor, uid, ids):
            posicion = Posicion()
            if pos.tensio:
                try:
                    posicion.tension = pos.tensio.tensio / 1000.0
                except (ValueError, TypeError):
                    posicion.tension = None
            if pos.interruptor == '3':
                # Sense interruptor
                posicion.interruptor = False
            elif pos.interruptor == '2':
                # Interruptor automàtic
                posicion.interruptor = True
            if pos.parc_id.localitzacio:
                cat = pos.parc_id.localitzacio
                if cat == 'I':
                    # Interior
                    posicion.situacion = 'I'
                elif cat == 'O':
                    # Intemperie
                    posicion.situacion = 'E'
                elif cat == 'M':
                    # Mobil
                    posicion.situacion = 'M'
            posicion.tipo = pos.tipus_posicio

            if pos.posicio_linia in ['L', 'T', 'M', 'R', 'A']:
                if pos.posicio_linia == 'A':
                    posicion.actuacion = 'L'
                else:
                    # Linia, Transformador, Mesura i Reserva
                    posicion.actuacion = pos.posicio_linia
            elif pos.posicio_linia == 'C':
                # Acoblament
                posicion.actuacion = 'A'
            res[pos.id] = str(posicion.cini)

        return res

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_cts_subestacions_posicio set cini=%s "
                       "where id in %s and bloquejar_cini",
                       (value or None, tuple(ids),))
        return True

    def _overwrite_apm(self, cursor, uid, ids, field_name, field_value, args,
                       context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_cts_subestacions_posicio "
                       "set data_pm=%s "
                       "where id in %s and bloquejar_pm",
                       (field_value or None, tuple(ids),))

    def _get_posicions_nobloc_ti(self, cursor, uid, ids, context=None):
        search_params = [
            ('bloquejar_cnmc_tipus_install', '!=', 1),
            ('id', 'in', ids)
        ]
        return self.search(cursor, uid, search_params, context=context)

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        """
        Calculem de forma automàtica el tipus d'instal·lació

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Posicions ids to calculate
        :type ids: list(int)
        :param field_name: name of the field to  calcualte
        :type field_name: str
        :param arg:
        :param context: OpenERP context
        :return: id,value
        :rtype: dict
        """

        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import Posicion

        for pos in self.browse(cursor, uid, ids, context):
            # TODO: Fix https://github.com/gisce/erp/issues/3252
            if pos.bloquejar_cnmc_tipus_install:
                res[pos.id] = pos.tipus_instalacio_cnmc_id.id
                continue
            ti = Posicion()
            if pos.tensio:
                try:
                    ti.tension = pos.tensio.tensio / 1000.0
                except (ValueError, TypeError):
                    ti.tension = None
            if pos.parc_id:
                if pos.parc_id.localitzacio == "O":
                    ti.situacion = "E"
                else:
                    ti.situacion = pos.parc_id.localitzacio
            elif pos.subestacio_id.id_subtipus.categoria_cne:
                cat = pos.subestacio_id.id_subtipus.categoria_cne.codi
                if cat in ['L', 'C', 'S']:
                    # Interior

                    ti.situacion = 'I'
                elif cat == 'I':
                    # Intemperie
                    ti.situacion = 'E'
            ti.tipo = pos.tipus_posicio

            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                ti_ids = ti_obj.search(cursor, uid, [
                    ('name', '=', ti.tipoinstalacion)
                ])
                res[pos.id] = ti_ids[0]

        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value, args,
                                      context=None):
        if not context:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute(
            "update giscedata_cts_subestacions_posicio set "
            "tipus_instalacio_cnmc_id=%s "
            "where id in %s and bloquejar_cnmc_tipus_install",
            (value or None, tuple(ids),)
        )
        return True

    _columns = {
        'name': fields.char('Codi', size=32, required=True),
        'alies': fields.char('Àlies', size=64),
        'active': fields.boolean('Activa'),
        'subestacio_id': fields.many2one('giscedata.cts.subestacions',
                                         'Subestació', required=True),
        'tensio': fields.many2one('giscedata.tensions.tensio', 'Tensió',
                                  required=True),
        'posicio_linia': fields.selection(POSICIO_LINIA, 'Funció',
                                          required=True),
        'interruptor': fields.selection(TIPUS_INTERRUPTOR, 'Tipus Interruptor',
                                        required=True),
        'barres': fields.selection(TIPUS_BARRES, 'Configuració', required=True),
        'cini': fields.function(
            _cini,
            type='char', size=8, method=True, store={
                'giscedata.cts.subestacions.posicio': (
                    _get_posicions_nobloc_cini, [
                        'tensio', 'bloquejar_cini', 'interruptor',
                        'tipus_posicio', 'barres', 'posicio_linia',
                        'parc_id.localitzacio'
                    ], 10
                ),
                'giscedata.parcs': (
                    _get_sub_nobloc_cini, ['localitzacio'
                    ], 10
                ),


            }, fnct_inv=_cini_inv, string='CINI'
        ),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'propietari': fields.boolean('Propietari'),
        'serial': fields.many2one('stock.production.lot', 'Número sèrie'),
        'producte': fields.related(
            'serial', 'product_id', type='many2one',
            relation='product.product', store=True,
            readonly=True, string='Marca i model'),
        'expedient': fields.many2many(
            'giscedata.expedients.expedient',
            'giscedata_expedients_expedient_cts_subestacions_posicio_rel',
            'posicio_id', 'expedient_id', 'Expedient'),
        'data_pm': fields.function(
            _data_pm, type='date', method=True, store={
                'giscedata.expedients.expedient': (
                    _get_posicions, ['industria_data'], 10
                ),
                'giscedata.cts.subestacions.posicio': (
                    _get_posicions_nobloc, ['expedient', 'bloquejar_pm'], 10
                )
            },
            fnct_inv=_overwrite_apm,
            string='Data APM', help=u"Data de l'acta de posada en marxa de la"
                                    u" instal·lació"
        ),
        'bloquejar_pm': fields.boolean('Bloquejar APM',
                                       help=u"Si està activat, agafa la data "
                                             u"entrada, si no, la de ,"
                                             u"l'expedient"),
        'tipus_posicio': fields.selection(TIPUS_POSICIO, 'Tecnologia',
                                          required=True),
        'perc_financament': fields.float('% pagat per la companyia',
                                         required=True),
        'data_baixa': fields.datetime('Data de Baixa'),
        'cedida': fields.boolean('Cedida'),
        'cnmc_tipo_instalacion': fields.char(
            u'CNMC Tipus Instal·lació', size=10, readonly=False),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            u'Bloquejar Tipus Instal·lació CNMC', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        'motoritzacio': fields.boolean('Motorització'),
        'telemando': fields.boolean('Telemando'),
        'parc_id': fields.many2one('giscedata.parcs',  'Parcs'),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string='Tipologia CNMC',
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={
                'giscedata.cts.subestacions.posicio': (
                    _get_posicions_nobloc_ti, [
                        'tensio', 'bloquejar_cnmc_tipus_install', 'interruptor',
                        'tipus_posicio', 'barres', 'posicio_linia'
                    ], 10
                )
            }
        ),
        '4771_entregada': fields.json('Dades 4771 entregada'),
        '4131_entregada_2016': fields.json('Dades 4131 entregada 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        'observacions': fields.text('Observacions'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019')
    }

    _defaults = {
        'active': lambda *a: 1,
        'interruptor': lambda *a: '2',
        'barres': lambda *a: 'A',
        'bloquejar_cini': lambda *a: 0,
        'propietari': lambda *a: 1,
        'perc_financament': lambda *a: 100,
        'bloquejar_pm': lambda *a: False,
        'cedida': lambda *a: False,
        'motoritzacio': lambda *a: 0,
        'telemando': lambda *a: 0,
        "criteri_regulatori": lambda *a: "criteri"
    }

    _sql_constraints = [
        ('perc_financament_0_100',
         'CHECK(perc_financament between 0 and 100 )',
         "El % de finançament a d'estar entre 0 i 100")
    ]


GiscedataCtsSubestacionsPosicio()
