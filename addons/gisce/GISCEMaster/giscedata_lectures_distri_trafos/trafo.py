# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from defs import *
from giscedata_lectures.defs import PROPIETAT_COMPTADOR_SELECTION


class GiscedataLecturesTrafoRelacio(osv.osv):

    _name = 'giscedata.lectures.trafo.relacio'
    _description = 'Relació de transformació'

    _order = 'tipus_tensio, name'

    _columns = {
        'name': fields.char('Relació', size=64,
                           required=True, readonly=False),
        'tipus_tensio': fields.selection(TIPUS_TENSIO_SELECTION,
                                         'Tipus tensió', required=False),
        'tipus_relacio': fields.selection(TIPUS_TRAFO_SELECTION,
                                          'Tipus relació', required=True),
    }

GiscedataLecturesTrafoRelacio()


class GiscedataLecturesTrafo(osv.osv):
    '''
    Transformadors d'intensitat i de tensio
    '''
    _name = 'giscedata.lectures.trafo'
    _description = 'Trafos'

    _order = 'tipus'

    _columns = {
        'name': fields.char('Nº de Serie', size=64,
                           required=True, readonly=False),
        'active': fields.boolean('Actiu', required=False),
        'serial_id': fields.many2one('stock.production.lot',
                                    'Nº Serie (Magatzem)', required=False),
        'product_id': fields.related('serial_id', 'product_id',
                                     type='many2one', readonly=True,
                                     relation='product.product',
                                     string='Producte', store=False),
        'date_instal': fields.date('Data instal·lació'),
        'date_verif': fields.date('Data verificació'),
        'date_baixa': fields.date('Data baixa'),
        'tipus': fields.selection(TIPUS_TRAFO_SELECTION,
                                  'Tipus', required=True),
        'propietat': fields.selection(PROPIETAT_COMPTADOR_SELECTION,
                                      'Propietat', required=True),
        'relacio_id': fields.many2one('giscedata.lectures.trafo.relacio',
                                       'Relació', required=True),
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                     required=True, ondelete='cascade'),
    }

    _defaults = {
        'active': lambda *a: True,
    }

GiscedataLecturesTrafo()
