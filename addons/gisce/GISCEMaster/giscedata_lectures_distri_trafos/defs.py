# -*- encoding: utf-8 -*-

TIPUS_TENSIO_SELECTION = [('at', 'Alta tensió'),
                          ('bt', 'Baixa Tensió'),]

TIPUS_TRAFO_SELECTION = [('intensitat', 'Intensitat'),
                  ('tensio', 'Tensió')]
