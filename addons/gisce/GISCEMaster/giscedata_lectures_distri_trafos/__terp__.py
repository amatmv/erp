# -*- coding: utf-8 -*-
{
    "name": "Trafos",
    "description": """
Model pels trafos de tensió i d'intensitat
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "trafo_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv",
        "trafo_data.xml"
    ],
    "active": False,
    "installable": True
}
