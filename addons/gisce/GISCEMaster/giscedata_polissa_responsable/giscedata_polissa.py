# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _default_comercial(self, cursor, uid, id, context=None):
        '''Usuari connectat'''
        return uid

    _columns = {
        'user_id': fields.many2one('res.users', 'Comercial')
    }

    _defaults = {
        'user_id': _default_comercial
    }

GiscedataPolissa()
