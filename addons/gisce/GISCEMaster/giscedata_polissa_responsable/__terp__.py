# -*- coding: utf-8 -*-
{
    "name": "Responsable de la pòlissa",
    "description": """
    This module provide :
      * Afegir Responsable a la pòlissa
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
