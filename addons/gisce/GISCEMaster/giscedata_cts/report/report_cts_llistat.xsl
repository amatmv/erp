<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cts"/>
  </xsl:template>

  <xsl:template match="cts">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <paraStyle name="titol"
					fontName="Helvetica"
					fontSize="12"
          leading="30"
				/>

        <blockTableStyle id="taula_contingut">
          <lineStyle kind="GRID" colorName="silver"/>
          <blockFont name="Helvetica" size="8" />
					<blockBackground colorName="grey" start="0,0" stop="7,0" />
					<blockFont name="Helvetica-Bold" size="8" start="0,0" stop="7,0" />
        </blockTableStyle>

      </stylesheet>
    
      <story>
      <para style="titol" t="1">Llistat de Centres Transformadors</para>
      <blockTable style="taula_contingut" colWidths="1.6cm,5cm,2.5cm,1cm,1.8cm,1.1cm,3.8cm,1cm">
           <tr t="1">
              <td>Codi</td>
              <td>Nom</td>
              <td>Municipi</td>
              <td>Trafo</td>
              <td>Marca</td>
              <td>Pot.</td>
              <td>Tipus</td>
              <td>Instal.</td>
           </tr>
           <xsl:apply-templates select="ct" mode="story">
        	    <xsl:sort select="codi" />
           </xsl:apply-templates>
      </blockTable>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="ct" mode="story">
    <xsl:choose>
      <xsl:when test="count(trafo-list/trafo)&gt;0" >
        <xsl:apply-templates select="trafo-list" mode="story" />
      </xsl:when>
      <xsl:otherwise>
        <tr>
          <td><xsl:value-of select="codi"/></td>
          <xsl:choose>
            <xsl:when test="string-length(descripcio) &gt; 25">
              <td><xsl:value-of select="concat(substring(descripcio, 1, 23), '...')" /></td>
            </xsl:when>
            <xsl:otherwise>
              <td><xsl:value-of select="descripcio" /></td>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="string-length(municipi) &gt; 15">
              <td><xsl:value-of select="concat(substring(municipi, 1, 15), '...')" /></td>
            </xsl:when>
            <xsl:otherwise>
              <tr>
              <td><xsl:value-of select="municipi" /></td>
              </tr>
            </xsl:otherwise>
          </xsl:choose>
          <td></td>
          <td></td>
          <td></td>
          <xsl:choose>
            <xsl:when test="string-length(subtipus) &gt; 19">
              <td><xsl:value-of select="concat(substring(subtipus, 1, 17), '...')" /></td>
            </xsl:when>
            <xsl:otherwise>
              <tr>
              <td><xsl:value-of select="subtipus" /></td>
              </tr>
            </xsl:otherwise>
          </xsl:choose>
          <td><xsl:value-of select="installacio"/></td>
        </tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="trafo-list" mode="story">
    <xsl:apply-templates select="trafo" mode="story" />
  </xsl:template>

  <xsl:template match="trafo" mode="story">
    <tr>
      <td><xsl:value-of select="../../codi"/></td>
      <!--<td><xsl:value-of select="../../descripcio"/></td>-->
      <xsl:choose>
        <xsl:when test="string-length(../../descripcio) &gt; 25">
          <td><xsl:value-of select="concat(substring(../../descripcio, 1, 23), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <td><xsl:value-of select="../../descripcio" /></td>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="string-length(../../municipi) &gt; 15">
          <td><xsl:value-of select="concat(substring(../../municipi, 1, 15), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <td><xsl:value-of select="../../municipi" /></td>
        </xsl:otherwise>
      </xsl:choose>
      <td><xsl:value-of select="trafo-name" /></td>
      <xsl:choose>
        <xsl:when test="string-length(trafo-marca) &gt; 8">
          <td><xsl:value-of select="concat(substring(trafo-marca, 1, 6), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <td><xsl:value-of select="trafo-marca" /></td>
        </xsl:otherwise>
      </xsl:choose>
      <td><xsl:value-of select="trafo-pot" /></td>
      <xsl:choose>
        <xsl:when test="string-length(../../subtipus) &gt; 19">
          <td><xsl:value-of select="concat(substring(../../subtipus, 1, 17), '...')" /></td>
        </xsl:when>
        <xsl:otherwise>
          <td><xsl:value-of select="../../subtipus" /></td>
        </xsl:otherwise>
      </xsl:choose>
      <td><xsl:value-of select="../../installacio"/></td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
