<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cts"/>
  </xsl:template>

  <xsl:template match="cts">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>

        <blockTableStyle id="taula1">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula2">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="4,0" />
          <blockBackground colorName="grey" start="0,0" stop="4,0" />
          <lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>

        <blockTableStyle id="taula3">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="1,0" stop="1,0" />
        </blockTableStyle>

        <blockTableStyle id="taula4">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="4,0" />
          <blockBackground colorName="grey" start="0,0" stop="4,0" />
          <lineStyle kind="GRID" colorName="silver" />
        </blockTableStyle>

        <blockTableStyle id="taula5">
          <blockFont name="Helvetica" size="8" />
          <blockFont name="Helvetica-Bold" start="0,0" stop="0,0" />
        </blockTableStyle>


        <paraStyle name="seccio"
          fontName="Helvetica-Bold"
          fontSize="8"
        />

	<paraStyle name="text"
	  fontName="Helvetica"
	  fontSize="8"
	/>

      </stylesheet>

      <story>
      
        <blockTable style="taula1" colWidths="2.5cm, 15.5cm">
          <tr>
            <td t="1">N�MERO:</td>
            <td><xsl:value-of select="ct/codi"/></td>
          </tr>
          <tr>
            <td t="1">NOM:</td>
            <td><xsl:value-of select="ct/descripcio"/></td>
          </tr>
          <tr>
            <td t="1">ADRE�A:</td>
            <td><xsl:value-of select="ct/adreca" /></td>          
          </tr>
          <tr>
            <td t="1">MUNICIPI:</td>
            <td><xsl:value-of select="ct/municipi" /></td>
          </tr>
        </blockTable>
        <spacer length="15" />
        <para style="seccio" t="1">TRANSFORMADORS:</para>
        <blockTable style="taula2" colWidths="3.6cm,3.6cm,3.6cm,3.6cm,3.6cm">
          <tr>
            <td t="1">N�mero</td>
            <td t="1">Pot�ncia</td>
            <td t="1">Tensi� B1</td>
            <td t="1">Tensi� B2</td>
            <td t="1">Refrigeraci�</td>
          </tr>
          <xsl:apply-templates select="ct/transformadors/transformador" mode="story" />
        </blockTable>
        <blockTable style="taula3" colWidths="3.6cm,3.6cm,3.6cm,3.6cm,3.6cm">
          <tr>
            <td t="1">POT�NCIA TOTAL</td>
            <td><xsl:value-of select="sum(ct/transformadors/transformador/potencia)" /> kVA</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
        </blockTable>
        <spacer length="15" />
        <para style="seccio" t="1">SORTIDES BT:</para>
        <blockTable style="taula4" colWidths="1.2cm,11.5cm,1.3cm,2.5cm,1.5cm">
          <tr>
            <td t="1">N�m.</td>
            <td t="1">Descripci�</td>
            <td t="1">Tensi�</td>
            <td t="1">Secci�</td>
            <td t="1">Material</td>
          </tr>
          <xsl:apply-templates select="ct/sortides_bt/sortida_bt" mode="story" />
        </blockTable>

        <nextFrame />

      </story>

    </document>

  </xsl:template>

  <xsl:template match="ct/transformadors/transformador" mode="story">
    <tr>
      <td><xsl:value-of select="name" /></td>
      <td><xsl:value-of select="potencia" /> kVA</td>
      <td><xsl:value-of select="tensio_b1" /></td>
      <td><xsl:value-of select="tensio_b2" /></td>
      <td><xsl:value-of select="refrigeracio" /></td>
    </tr>
  </xsl:template>

  <xsl:template match="ct/sortides_bt/sortida_bt" mode="story">
    <tr>
      <td><xsl:value-of select="linia" /></td>
      <td><para style="text"><xsl:value-of select="name" /></para></td>
      <xsl:if test="voltatge != ''">
      <td><para style="text"><xsl:value-of select="format-number(voltatge, '#')" /></para></td>
      </xsl:if>
      <xsl:if test="voltatge = ''">
      <td></td>
      </xsl:if>
      <td><para style="text"><xsl:value-of select="seccio" /></para></td>
      <td><para style="text"><xsl:value-of select="material" /></para></td>
    </tr>
  </xsl:template>


</xsl:stylesheet>
