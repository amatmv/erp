<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
   <document>
      <template pageSize="(297mm,19cm)" topMargin="1cm" bottomMargin="1cm" rightMargin="1cm">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="277mm" height="17.5cm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
        <paraStyle name="titol"
	  fontName="Helvetica"
	  fontSize="12"
          leading="30" />

       	<blockTableStyle id="header">
	  <blockFont name="Helvetica-Bold" size="8" start="0,0" stop="10,0"/>
	  <blockFont name="Helvetica" size="8" start="0,1" />
	</blockTableStyle>
      </stylesheet>
    
      <story>
      <xsl:apply-templates select="sortidesbt"/>
      </story>
    </document>
 
  </xsl:template>
  <xsl:template match="sortidesbt">
    <xsl:apply-templates select="sortidabt"/>
  </xsl:template>
  <xsl:template match="sortidabt">
    	<para  style="titol" t="1">Llistat de CUPS connectats a la sortida <xsl:value-of select="substring(linia, string-length(linia)-1)"/> del <xsl:value-of select="ct/name"/></para>

	<blockTable colWidths="2cm,2cm,4.3cm,1.7cm,7cm,5cm,0.9cm,0.9cm,0.9cm,0.9cm,0.9cm" repeatRows="1" style="header">
	<tr>
	    <td t="1">Escomesa</td>
	    <td t="1">Comptador</td>
	    <td t="1">CUPS</td>
	    <td t="1">P�lissa</td>
	    <td t="1">Titular</td>
	    <td t="1">Carrer</td>
	    <td t="1">N.</td>
	    <td t="1">Pis</td>
	    <td t="1">Porta</td>
	    <td t="1">kW</td>
	    <td t="1">Tensi�</td>
	  </tr>
	  <xsl:apply-templates select="cups" mode="story"/>
	</blockTable>
	<nextPage/>
        </xsl:template>

  <xsl:template match="cups" mode="story">
    <tr>
      <td><xsl:value-of select="escomesa"/></td>
      <td><xsl:value-of select="comptador"/></td>
      <td><xsl:value-of select="name"/></td>
      <td><xsl:value-of select="polissa/name"/></td> -->
      <td><xsl:value-of select="titular"/></td>
      <td><xsl:value-of select="carrer"/></td>
      <td><xsl:value-of select="num"/></td>
      <td><xsl:value-of select="pis"/></td>
      <td><xsl:value-of select="porta"/></td>
      <td><xsl:value-of select="polissa/kw"/></td> 
      <td><xsl:value-of select="polissa/tensio"/></td>
    </tr>
  </xsl:template>

</xsl:stylesheet>

