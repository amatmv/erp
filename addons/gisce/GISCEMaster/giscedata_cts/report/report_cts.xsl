<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <xsl:apply-templates select="cts"/>
  </xsl:template>

  <xsl:template match="cts">
    <document>
      <template>
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet>
        <paraStyle name="parastyle" fontName="Helvetica" fontsize="10" spaceBefore="0" spaceAfter="0"/>
        <blockTableStyle id="dadesaltres">
          <lineStyle kind="GRID" colorName="(0.8,0.8,0.8)" thickness="0.1"/>
          <blockBackground colorName="(0.9,0.9,0.9)" start="0,0" stop="0,1"/>
          <blockLeftPadding length="2"/>
          <blockTopPadding length="2"/>
          <blockRightPadding length="2"/>
          <blockBottomPadding length="2"/>
        </blockTableStyle>
        <blockTableStyle id="dadesnsuports">
          <lineStyle kind="GRID" colorName="(0.8,0.8,0.8)" thickness="0.1"/>
          <blockBackground colorName="(0.9,0.9,0.9)" start="0,0" stop="0,3"/>
          <blockLeftPadding length="2"/>
          <blockTopPadding length="2"/>
          <blockRightPadding length="2"/>
          <blockBottomPadding length="2"/>
        </blockTableStyle>
        <blockTableStyle id="dadeselements">
          <lineStyle kind="GRID" colorName="(0.8,0.8,0.8)" thickness="0.1"/>
          <blockBackground colorName="(0.9,0.9,0.9)" start="0,0" stop="5,0"/>
          <blockLeftPadding length="2"/>
          <blockTopPadding length="2"/>
          <blockRightPadding length="2"/>
          <blockBottomPadding length="2"/>
        </blockTableStyle>
        <blockTableStyle id="resistivitat">
          <lineStyle kind="GRID" colorName="(0.8,0.8,0.8)" thickness="0.1"/>
          <blockBackground colorName="(0.9,0.9,0.9)" start="0,0" stop="5,0"/>
          <blockLeftPadding length="2"/>
          <blockTopPadding length="2"/>
          <blockRightPadding length="2"/>
          <blockBottomPadding length="2"/>
        </blockTableStyle>
        <blockTableStyle id="terrestensions">
          <lineStyle kind="GRID" colorName="(0.8,0.8,0.8)" thickness="0.1"/>
          <blockBackground colorName="(0.9,0.9,0.9)" start="0,0" stop="2,0"/>
          <blockLeftPadding length="2"/>
          <blockTopPadding length="2"/>
          <blockRightPadding length="2"/>
          <blockBottomPadding length="2"/>
        </blockTableStyle>
      </stylesheet>
      <story>
        <xsl:apply-templates select="ct" mode="story"/>
      </story>
    </document>
  </xsl:template>

  <xsl:template match="ct" mode="story">
    <h1>Fitxa del Centre Transformador</h1>
    <h2>General</h2>
    <para style="parastyle" t="1">Codi : <xsl:text> </xsl:text><xsl:value-of select="codi"/></para>
    <para style="parastyle" t="1">Descripci� : <xsl:text> </xsl:text><xsl:value-of select="descripcio"/></para>
    <para style="parastyle" t="1">Adre�a : <xsl:text> </xsl:text><xsl:value-of select="adreca"/></para>
    <para style="parastyle" t="1">Poblaci� : <xsl:text> </xsl:text><xsl:value-of select="poblacio"/></para>
    <para style="parastyle" t="1">Comarca : <xsl:text> </xsl:text><xsl:value-of select="comarca"/></para>
    <para style="parastyle" t="1">Provincia : <xsl:text> </xsl:text><xsl:value-of select="provincia"/></para>
    <para style="parastyle" t="1">Municipi : <xsl:text> </xsl:text><xsl:value-of select="municipi"/></para>
    <para style="parastyle" t="1">Data de Baixa : <xsl:text> </xsl:text><xsl:value-of select="databaixa"/></para>
    <para style="parastyle" t="1">CT en Baixa ? : <xsl:text> </xsl:text><xsl:value-of select="ct_baixa"/></para>
    <para style="parastyle" t="1">CT en Obres ? : <xsl:text> </xsl:text><xsl:value-of select="ct_obres"/></para>
    <para style="parastyle" t="1">Projecte/Const ? : <xsl:text> </xsl:text><xsl:value-of select="projecte_const"/></para>
    <para style="parastyle" t="1">Tipus : <xsl:text> </xsl:text><xsl:value-of select="subtipus"/></para>
    <para style="parastyle" t="1">Tipus Instal�laci� : <xsl:text> </xsl:text><xsl:value-of select="tipus_instalacio"/></para>
    <para style="parastyle" t="1">Pot�ncia : <xsl:text> </xsl:text><xsl:value-of select="potencia"/></para>
    <h2>Dades</h2>
    <para style="parastyle" t="1">Zona : <xsl:text> </xsl:text><xsl:value-of select="zona"/></para>
    <para style="parastyle" t="1">CINI : <xsl:text> </xsl:text><xsl:value-of select="cini"/></para>
    <h3 t="1">Dades : Altres</h3>
    <blockTable colWidths="9.5cm,9.5cm" style="dadesaltres">
      <tr>
        <td>
        <para style="parastyle" t="1">N� de sortides de baixa tensi� :</para>
        </td>
        <td>
        <xsl:value-of select="nsortidesbt"/>
        </td>
      </tr>
      <tr>
        <td>
        <para style="parastyle" t="1">Ventilaci�</para>
        </td>
        <td>
        <xsl:value-of select="ventilacio"/>
        </td>
      </tr>
    </blockTable>
    <h3 t="1">Dades : Elements</h3>
    <blockTable colWidths="31.6mm,31.6mm,31.6mm,31.6mm,31.6mm,31.6mm" style="dadeselements">
    <tr>
      <td>
      <para style="parastyle" t="1">Extinci�</para>
      </td>
      <td>
      <para style="parastyle" t="1">Autov�lvules</para>
      </td>
      <td>
      <para style="parastyle" t="1">Disp�sit Recollida Oli</para>
      </td>
      <td>
      <para style="parastyle" t="1">Malla equipotencial</para>
      </td>
      <td>
      <para style="parastyle" t="1">Antiescalada</para>
      </td>
      <td>
      <para style="parastyle" t="1">Anell Terres</para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle"><xsl:value-of select="extincio"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="autovalvules"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="diposit_recollida_oli"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="malla_equipotencial"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="antiescalada"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="anell_terres"/></para>
      </td>
    </tr>
    </blockTable>
    <h3 t="1">Dades : N� de suports</h3>
    <blockTable colWidths="9.5cm,9.5cm" style="dadesnsuports">
    <tr>
      <td>
      <para style="parastyle" t="1">N� de suports met�lics :</para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="n_suports_metallics"/></para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle" t="1">N� de suports de formig� :</para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="n_suports_formigo"/></para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle" t="1">N� de suports de fusta :</para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="n_suports_fusta"/></para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle" t="1">N� de cadiretes o travessers :</para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="n_cadiretes_travessers"/></para>
      </td>
    </tr>
    </blockTable>
    <h2 t="1">Observacions</h2>
    <para style="parastyle"><xsl:value-of select="observacions"/></para>
    <h2 t="1">Terres</h2>
    <h3 t="1">Tensions de pas i contacte admissibles</h3>
    <blockTable colWidths="6.3cm,6.3cm,6.4cm" style="terrestensions">
    <tr>
      <td>
      <para style="parastyle" t="1">Contacte :</para>
      </td>
      <td>
      <para style="parastyle" t="1">Pas exterior :</para>
      </td>
      <td>
      <para style="parastyle" t="1">Pas interior :</para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle"><xsl:value-of select="contacte"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="pas_exterior"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="pas_interior"/></para>
      </td>
    </tr>
    </blockTable>
    <h3 t="1">C�lcul de la resistivitat del terra</h3>
    <para style="parastyle" t="1">Data Resistivitat : <xsl:text> </xsl:text><xsl:value-of select="data_resistivitat"/></para>
    <blockTable colWidths="31.6mm,31.6mm,31.6mm,31.6mm,31.6mm,31.6mm" style="resistivitat">
    <tr>
      <td>
      <para style="parastyle" t="1">Separaci� piques (e) (m)</para>
      </td>
      <td>
      <para style="parastyle" t="1">Profunditat equivalent</para>
      </td>
      <td>
      <para style="parastyle" t="1">Resit�ncia mesurada (r)</para>
      </td>
      <td>
      <para style="parastyle" t="1">Coeficient (K)</para>
      </td>
      <td>
      <para style="parastyle" t="1">Resistivitat</para>
      </td>
      <td>
      <para style="parastyle" t="1">Resistivitat Centre</para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle"><xsl:value-of select="dist_piques1"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="profunditat_eq1"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistencia1"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="coeficient1"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistivitat_a"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistivitat1"/></para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle"><xsl:value-of select="dist_piques2"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="profunditat_eq2"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistencia2"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="coeficient2"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistivitat_b"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistivitat2"/></para>
      </td>
    </tr>
    <tr>
      <td>
      <para style="parastyle"><xsl:value-of select="dist_piques3"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="profunditat_eq3"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistencia3"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="coeficient3"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistivitat_c"/></para>
      </td>
      <td>
      <para style="parastyle"><xsl:value-of select="resistivitat3"/></para>
      </td>
    </tr>
    </blockTable>
  </xsl:template>

</xsl:stylesheet>
