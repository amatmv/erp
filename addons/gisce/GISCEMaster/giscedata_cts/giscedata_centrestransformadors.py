# -*- coding: utf-8 -*-

from osv import osv, fields
from datetime import datetime
from dateutil import relativedelta

import math
import time
import logging
from giscedata_administracio_publica_cnmc_distri.wizard.wizard_recalcul_cinis_tis import MODELS_SELECTION
from giscedata_administracio_publica_cnmc_distri.giscedata_tipus_installacio import TIPUS_REGULATORI


MODELS_SELECTION.append(
    ('giscedata.cts', 'CTS')
)


class GiscedataCtsTipus(osv.osv):

    def create(self, cr, uid, vals, context={}):
        cr.execute("SELECT code FROM giscedata_cts_tipus ORDER BY code\
        DESC LIMIT 1")
        codi = cr.fetchone()
        if not codi:
            codi = (0,)
        codi = int(codi[0]) + 1
        vals['code'] = codi
        return super(osv.osv, self).create(cr, uid, vals, context)

    _name = 'giscedata.cts.tipus'
    _description = 'Tipus Centres Transformadors'
    _columns = {
      'name': fields.char('Tipus', size=60, required=True),
      'code': fields.integer('Codi'),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataCtsTipus()


class GiscedataCtsSubtipus(osv.osv):

    _name = "giscedata.cts.subtipus"
    _description = "Subtipus de Centre Transformador"

    _columns = {
      'codi': fields.char('Id', size=10),
      'name': fields.char('Nom', size=25),
      'tipus_id': fields.many2one('giscedata.cts.tipus', 'Tipus'),
    }

    _defaults = {}

    _order = "name, id"

GiscedataCtsSubtipus()


class GiscedataCtsInstallacio(osv.osv):

    _name = 'giscedata.cts.installacio'
    _description = 'Tipus d\'Instal·lació'

    _columns = {
      'name': fields.char('Tipus', size=3, required=True),
      'descripcio': fields.char('Descripció', size=60),
      'web': fields.char('Apartat web', size=1),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataCtsInstallacio()


class GiscedataCtsVentilacio(osv.osv):

    _name = 'giscedata.cts.ventilacio'
    _description = 'Tipus de Ventilació'

    _columns = {
      'name': fields.char('Ventilació', size=60, required=True),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataCtsVentilacio()


class GiscedataCtsZona(osv.osv):

    _name = 'giscedata.cts.zona'
    _description = 'Zona del Centre Transformador'

    _columns = {
      'name': fields.char('Zona', size=60),
      'qualitats': fields.one2many(
          'giscedata.cts.zona.qualitat', 'zona_id', 'Qualitats'),
    }

    _defaults = {

    }

    _order = "name, id"

GiscedataCtsZona()


class GiscedataCtsZonaQualitat(osv.osv):

    _name = 'giscedata.cts.zona.qualitat'
    _description = 'Qualitat requirida per la zona'

    _columns = {
      'name': fields.char('Nom', size=60, required=True),
      'inici': fields.date('Inici'),
      'final': fields.date('Fi'),
      'zona_id': fields.many2one('giscedata.cts.zona', 'Zona'),
      'at_hores': fields.float('Hores Alta Tensió'),
      'at_interrupcions': fields.integer('Interrupcions Alta Tensió'),
      'bt_hores': fields.float('Hores Baixa Tensió'),
      'bt_interrupcions': fields.integer('Interrupcions Baixa Tensió'),
      'tiepi_hores': fields.float('Hores TIEPI'),
      'percentil_hores': fields.float('Hores Percentil'),
      'niepi': fields.integer('NIEPI'),
    }

    _order = 'inici desc'

GiscedataCtsZonaQualitat()


class GiscedataCts(osv.osv):

    def _tipus_instalacio_cnmc_id(self, cursor, uid, ids, field_name, arg,
                                  context=None):
        """
        Function that handles the assignation of the tipus instalacio

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param field_name: Name of the modified element
        :param arg:
        :param context: OpenERP Context
        :return: dict of condensador id , tipus installacio id
        """
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        from tipoinstalacion.models import CT
        for ct in self.browse(cursor, uid, ids, context=context):
            if ct.bloquejar_cnmc_tipus_install:
                res[ct.id] = ct.tipus_instalacio_cnmc_id.id
                continue
            ti = CT()

            transformadors = ct.transformadors or []
            potencia = 0
            num_trafos = 0
            for trafo in transformadors:
                if trafo.id_estat.cnmc_inventari:
                    potencia += trafo.potencia_nominal
                    num_trafos += 1

            ti.tension = float(ct.tensio_p)/1000
            ti.numero_maquinas = num_trafos
            ti.potencia = potencia
            # By default we don't have categoria_cne field. It is added by
            # giscedata_cne module
            categoria_cne = ct.id_subtipus.categoria_cne
            ti.situacion = categoria_cne and categoria_cne.codi or None
            if ti.tipoinstalacion:
                ti_obj = self.pool.get('giscedata.tipus.installacio')
                ti_ids = ti_obj.search(
                    cursor, uid,
                    [('name', '=', ti.tipoinstalacion)])
                if ti_ids:
                    res[ct.id] = ti_ids[0]
                else:
                    mess = 'TI {0} not found in giscedata.tipus.installacio'
                    raise mess.format(ti.tipoinstalacion)

        return res

    def _tipus_instalacio_cnmc_id_inv(self, cursor, uid, ids, name, value,
                                      args, context=None):
        """
        Function that handles the assignation of the tipus_instalacio_cnmc_id

        :param cursor: Database cursor
        :param uid: user id
        :param ids: Afected ids
        :param name: Name of the tipus instalacio
        :param value: Id of the tipus instalacio
        :param args:
        :param context: OpenERP context
        :return: True
        """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
                ids = [ids]
        sql = """
        UPDATE giscedata_cts SET tipus_instalacio_cnmc_id=%s
        WHERE id IN %s AND bloquejar_cnmc_tipus_install
        """
        cursor.execute(sql, (value or None, tuple(ids),))
        return True

    def _get_element_nobloc_ti(self, cursor, uid, ids, context=None):
        """
        Function that indicates when the tipus installacio is locked

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Afected ids
        :param context: OpenERP context
        :return: List of non locked elements
        """
        search_params = [
            ('bloquejar_cnmc_tipus_install', '!=', 1),
            ('id', 'in', ids)
        ]
        return self.search(cursor, uid, search_params, context=context)

    def onchange_prof_eq(self, cr, uid, id, dist_piques, resistencia, num):
        return {
            'value': {
                'profunditat_eq'+str(num): (0.7 * dist_piques),
                'coeficient' + str(num): (6.28 * dist_piques),
                'resistivitat' + str(num): (6.28 * dist_piques * resistencia)
            }
        }

    def onchange_resistencia(self, cr, uid, id, resistencia, k, num):
        return {'value': {'resistivitat' + str(num): (k * resistencia)}}

    def _coeficient1(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.dist_piques1 * 6.28
        return res

    def _coeficient2(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.dist_piques2 * 6.28
        return res

    def _coeficient3(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.dist_piques3 * 6.28
        return res

    def _profunditat_eq1(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.dist_piques1 * 0.7
        return res

    def _profunditat_eq2(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.dist_piques2 * 0.7
        return res

    def _profunditat_eq3(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.dist_piques3 * 0.7
        return res

    def _resistivitat1(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.resistencia1 * ct.coeficient1
        return res

    def _resistivitat2(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.resistencia2 * ct.coeficient2
        return res

    def _resistivitat3(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = ct.resistencia3 * ct.coeficient3
        return res

    def _imax(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            try:
                res[ct.id] = ct.tensio_max / (math.sqrt(3) * \
                math.sqrt(math.pow((ct.res_pat_neutre + ct.res_pat_centre), 2) \
                + math.pow(ct.react_pat_neutre, 2)))
            except ZeroDivisionError:
                res[ct.id] = 0.0
        return res

    def onchange_imax(self, cr, uid, id, u, ro, rm, xo):
        try:
            return {'value': {'i_max': (u/(math.sqrt(3) * math.sqrt(math.pow((ro + rm), 2) + math.pow(xo, 2))))}}
        except ZeroDivisionError:
            return {'value': {'i_max': 0.0}}

    def get_trafos_nr(self, cr, uid, ids):
        res = {}
        for ct in self.browse(cr, uid, ids):
            for trafo in ct.transformadors:
                if trafo.id_estat:
                    if trafo.id_estat.codi == 1 and not trafo.reductor:
                        res['numero'] = trafo.name
                        res['potencia'] = trafo.potencia_nominal
        return res

    def _tipus_installacio(self, cr, uid, context=None):
        if context is None:
            context = {}
        obj = self.pool.get('giscedata.cts.installacio')
        ids = obj.search(cr, uid, [])
        res = obj.read(cr, uid, ids, ['name', 'id'], context)
        res = [(r['id'], r['name']) for r in res]
        return res

    def _n_sortides_bt(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for ct in self.browse(cr, uid, ids):
            res[ct.id] = len(ct.sortidesbt)
        return res

    def _n_sortides_bt_search(self, cr, uid, obj, name, args, context=None):
        if not len(args):
            return []
        else:
            search_param = ''
            for c in args:
                search_param = "%s%sCOUNT(*) %s %d " % (
                    search_param, len(search_param) and ' AND ' or '',
                    c[1], c[2]
                )
            sql = """SELECT ct,COUNT(*) FROM giscedata_cts_sortidesbt
                 GROUP BY ct HAVING %s""" % (search_param)
        cr.execute(sql)
        ids = [ct[0] for ct in cr.fetchall()]
        return [('id', 'in', ids)]

    def _data_cfo(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for id in ids:
            cr.execute(" select e.industria_data as data from giscedata_expedients_expedient e, giscedata_cts_expedients_rel r, giscedata_cts c where c.id = r.ct_id and r.expedient_id = e.id and c.id = "+str(id)+" order by data desc limit 1")
            data = cr.fetchall()
            if len(data):
                res[id] = data[0][0]
            else:
                res[id] = False
        return res

    def _ff_cfo(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, False)
        today = datetime.today().strftime('%Y-%m-%d')
        data_inici = context.get('data_inici_cfo', today)
        date_format = "%Y-%m-%d"
        data_inici_obj = datetime.strptime(data_inici, date_format)
        cfg_obj = self.pool.get('res.config')
        cfo_durada = int(cfg_obj.get(cursor, uid, 'giscedata_cts_durada_cfo',
                                     6))
        data_aux = data_inici_obj - relativedelta.relativedelta(
            years=cfo_durada)
        for ct in self.read(cursor, uid, ids, ['data_pm']):
            if not ct['data_pm']:
                continue
            data_APM = datetime.strptime(ct['data_pm'], date_format)
            res[ct['id']] = data_aux <= data_APM
        return res

    def _cfo_search(self, cursor, uid, obj, name, args, context=None):
        if not args:
            return []
        today = datetime.today().strftime('%Y-%m-%d')
        data_inici = context.get('data_inici_cfo', today)
        date_format = "%Y-%m-%d"
        data_inici_obj = datetime.strptime(data_inici, date_format)
        cfg_obj = self.pool.get('res.config')
        cfo_durada = int(cfg_obj.get(cursor, uid, 'giscedata_cts_durada_cfo',
                                     6))
        data_aux = data_inici_obj - relativedelta.relativedelta(
            years=cfo_durada)
        data_aux = data_aux.strftime('%Y-%m-%d %H:%M:%S')
        if args[0][2]:
            search_params = [
                ('data_pm', '>=', data_aux)
            ]
        else:
            search_params = [
                ('data_pm', '<', data_aux)
            ]
        return search_params

    def _expedient_actual(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for id in ids:
            cr.execute("select e.industria as codi from giscedata_expedients_expedient e, giscedata_cts ct, giscedata_cts_expedients_rel r where ct.id = r.ct_id and e.id = r.expedient_id and ct.id = %s and e.industria_data is not null order by e.industria_data desc limit 1", (int(id),))
            exp = cr.dictfetchone()
            if exp:
                res[id] = exp['codi']
            else:
                res[id] = False
        return res

    def _data_industria(self, cr, uid, ids, field_name, ar, context):
        res = {}
        ids = ",".join([str(t) for t in ids])
        cr.execute("""select ct.id,max(e.industria_data) as data_industria
    from giscedata_cts ct
        left join giscedata_cts_expedients_rel r on (ct.id = r.ct_id )
        left join giscedata_expedients_expedient e on
                  (e.id = r.expedient_id and e.industria_data is not null)
    where
        ct.id in (%s)
    group by ct.id""" % ids)
        sql = cr.dictfetchall()
        for exp in sql:
            res[exp["id"]] = exp['data_industria']
        return res

    def _data_pm(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        for item in self.browse(cursor, uid, ids, context):
            if item.expedients_ids and not item.bloquejar_pm:
                res[item.id] = max(e.industria_data
                                   for e in item.expedients_ids)
            else:
                res[item.id] = item.data_pm
        return res

    def _get_cts(self, cursor, uid, ids, context=None):
        sql = """
        SELECT e__t.ct_id FROM
        giscedata_cts_expedients_rel AS e__t
        LEFT JOIN giscedata_cts AS ct ON (e__t.ct_id=ct.id)
        WHERE e__t.expedient_id IN %s AND NOT ct.bloquejar_pm
        """

        cursor.execute(sql, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_cts_nobloc(self, cursor, uid, ids, context=None):
        sql = """
        SELECT id
        FROM giscedata_cts
        WHERE NOT bloquejar_pm AND id IN %s
        """
        cursor.execute(sql, (tuple(ids), ))
        return [x[0] for x in cursor.fetchall()]

    def _get_cts_nobloc_cini(self, cursor, uid, ids, context=None):
        cursor.execute("select id from giscedata_cts "
                       "where not bloquejar_cini and id in %s", (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def _get_cts_trafo(self, cursor, uid, ids, context=None):
        """
        Calculates the affected CTS that must recompute the CINI field.

        :param cursor: Database cursor
        :type cursor: Cursor
        :param uid: User ID
        :type uid: int
        :param ids: List of affected Trafos IDS
        :type ids: list of int
        :param context: OpenERP context
        :type context: dict[str, Any]
        :return: The IDs of the CTS where the CINI must be recalculated
        :rtype: list of int
        """

        cursor.execute("""
            SELECT ct
            FROM giscedata_transformador_trafo
            WHERE id in %(ids)s     
        """, {'ids': tuple(ids)})
        query_result = cursor.fetchall()
        return [x[0] for x in query_result]

    def _cini(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Method that calculates the CINI of the CT

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Id to calculate
        :param field_name:
        :param arg: Arguments
        :param context: OpenERP context
        :return: Dict {id: cini}
        """
        if context is None:
            context = {}
        logger = logging.getLogger('openerp.cini')
        res = dict.fromkeys(ids, False)
        from cini.models import CentroTransformador, Transformador
        for ct in self.browse(cursor, uid, ids):
            centro = CentroTransformador()
            # try:
            #     centro.tension = int(ct.tensio_p) / 1000.0
            # except TypeError:
            #     centro.tension = 0
            # centro.tension_p = centro.tension

            # try:
            #     centro.tension_s = int(ct.tensio_s) / 1000.0
            # except TypeError:
            #     centro.tension_s = 0

            # De moment hardcoded, doncs el camps tensió primària i secundària
            # de l'ERP són camps de text i no es poden dividir
            centro.tension_p = 25
            centro.tension_s = 0.40

            try:
                centro.tension = int(ct.tensio_p) / 1000.0
            except ValueError:
                logger.error('Error tensio: {0} can be parsed to int'.format(
                    ct.tensio_p
                ))

            centro.reparto = True

            if ct.id_subtipus.categoria_cne:
                centro.tipo = ct.id_subtipus.categoria_cne.codi

            # Hack to get an empty list if transformadors module isn't loaded
            transformadors = ct.transformadors or []

            for trafo in transformadors:
                if trafo.id_estat:
                    if trafo.id_estat.cnmc_inventari:
                        if trafo.id_estat.codi == 1:
                            centro.reparto = False
                        transformador = Transformador()
                        transformador.potencia = trafo.potencia_nominal
                        centro.transformadores.append(transformador)
            res[ct.id] = str(centro.cini)
        return res

    def _cini_inv(self, cursor, uid, ids, name, value, args, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_cts set cini=%s "
                       "where id in %s and bloquejar_cini",
                       (value or None, tuple(ids),))
        return True

    def _overwrite_apm(self, cursor, uid, ids, field_name, field_value, args,
                       context=None):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        cursor.execute("update giscedata_cts set data_pm=%s "
                       "where id in %s and bloquejar_pm",
                       (field_value or None, tuple(ids),))

    _name = 'giscedata.cts'
    _description = 'Centres Transformadors'
    _columns = {
        'name': fields.char('CT', size=22, required=True),
        'descripcio': fields.char('Descripció', size=60),
        'adreca': fields.char('Adreça', size=60),
        'id_poblacio': fields.many2one('res.poblacio', 'Població'),
        'id_comarca': fields.many2one('res.comarca', 'Comarca'),
        'id_provincia': fields.many2one('res.country.state', 'Provincia'),
        'id_municipi': fields.many2one('res.municipi', 'Municipi'),
        'ct_baixa': fields.boolean('CT de Baixa'),
        'active': fields.boolean('Actiu'),
        'ct_obres': fields.boolean('CT en Obres'),
        'projecte_const': fields.boolean('Projecte/Const'),
        'data_baixa': fields.datetime('Data de Baixa'),
        'observacions': fields.text('Observacions'),
        'id_subtipus': fields.many2one('giscedata.cts.subtipus', 'Tipus'),
        'id_installacio': fields.many2one(
            'giscedata.cts.installacio', 'Tipus Instal·lació',
            selection=_tipus_installacio, required=True),
        'cini': fields.function(
            _cini, type='char', size=8, method=True, store={
                'giscedata.cts': (
                    _get_cts_nobloc_cini, [
                        'tensio_p', 'tensio_s', 'bloquejar_cini',
                        'transformadors', 'id_subtipus'
                    ], 10
                ),
                'giscedata.transformador.trafo': (
                    _get_cts_trafo, ['ct'], 11
                )
            }, fnct_inv=_cini_inv, string='CINI'
        ),

        'cfo': fields.function(_ff_cfo, type='boolean', method=True,
                               string='CFO', fnct_search=_cfo_search),
        'zona_id': fields.many2one('giscedata.cts.zona', 'Zona'),
        'sortides_bt': fields.integer('Nº Sortides baixa tensió'),
        'n_sortides_bt': fields.function(
            _n_sortides_bt, fnct_search=_n_sortides_bt_search, method="True",
            type="integer", string="Nº Sortides baixa tensió"
        ),
        'id_ventilacio': fields.many2one(
            'giscedata.cts.ventilacio', 'Ventilació'
        ),
        'n_suports_metallics': fields.integer('Suports metàl·lics'),
        'n_suports_formigo': fields.integer('Suports de formigó'),
        'n_suports_fusta': fields.integer('Suports de fusta'),
        'n_cadiretes_travessers': fields.integer('Cadiretes o travessers'),
        'extincio': fields.boolean('Extinció'),
        'autovalvules': fields.boolean('Autovàlvules'),
        'diposit_recollida_oli': fields.boolean('Dipòsit recollida oli'),
        'malla_equipotencial': fields.boolean('Malla Equipotencial'),
        'antiescalada': fields.boolean('Antiescalada'),
        'anell_terres': fields.boolean('Anell de terres'),
        'dist_piques1': fields.float('Separació piques 1 (e)(m)'),
        'dist_piques2': fields.float('Separació piques 2 (e)(m)'),
        'dist_piques3': fields.float('Separació piques 3 (e)(m)'),
        'resistencia1': fields.float('Resistència mesurada 1 (R)'),
        'resistencia2': fields.float('Resistència mesurada 2 (R)'),
        'resistencia3': fields.float('Resistència mesurada 3 (R)'),
        'coeficient1': fields.function(_coeficient1, method=True),
        'coeficient2': fields.function(_coeficient2, method=True),
        'coeficient3': fields.function(_coeficient3, method=True),
        'resistivitat1': fields.function(_resistivitat1, method=True),
        'resistivitat2': fields.function(_resistivitat2, method=True),
        'resistivitat3': fields.function(_resistivitat3, method=True),
        'resistivitat_a': fields.float('Resistivitat 1'),
        'resistivitat_b': fields.float('Resistivitat 2'),
        'resistivitat_c': fields.float('Resistivitat 3'),
        'data_resistivitat': fields.datetime('Data Resistivitat'),
        'profunditat_eq1': fields.function(_profunditat_eq1, method=True),
        'profunditat_eq2': fields.function(_profunditat_eq2, method=True),
        'profunditat_eq3': fields.function(_profunditat_eq3, method=True),
        'mesures': fields.one2many(
            'giscedata.cts.mesures', 'id_ct', 'Històric de Mesures'
        ),
        'contacte': fields.float('Contacte'),
        'pas_exterior': fields.float('Pas exterior'),
        'pas_interior': fields.float('Pas interior'),
        'tensio_max': fields.integer('Tensió màx (U)'),
        'i_max': fields.function(_imax, method=True),
        'res_pat_neutre': fields.float('Resistència pat Neutre (Ro)'),
        'res_pat_centre': fields.float('Resistència pat Centre (Rm)'),
        'react_pat_neutre': fields.float('Reactància pat Neutre (Xo)'),
        'expedients_ids': fields.many2many(
            'giscedata.expedients.expedient', 'giscedata_cts_expedients_rel',
            'ct_id', 'expedient_id', 'Expedients'
        ),
        'propietat_de': fields.many2one('res.company', 'Propietari'),
        'sortidesbt': fields.one2many(
            'giscedata.cts.sortidesbt', 'ct', 'Sortides BT'
        ),
        'data_cfo': fields.function(_data_cfo, type='datetime', method=True),
        'expedient_actual': fields.function(
            _expedient_actual, type='char', method=True
        ),
        'data_industria': fields.function(
            _data_industria, type='date', method=True
        ),
        'tensio_p': fields.char('Tensió Primari [V]', size=50),
        'tensio_s': fields.char('Tensió Secundari [V]', size=50),
        'data_pm': fields.function(
                _data_pm, type='date', method=True, store={
                    'giscedata.expedients.expedient': (
                        _get_cts, ['industria_data'], 10
                    ),
                    'giscedata.cts': (
                        _get_cts_nobloc, ['expedients_ids', 'bloquejar_pm'], 10
                    )
                },
                fnct_inv=_overwrite_apm,
                string='Data APM', help=u"Data de l'acta de posada en marxa"
                                        u" de la instal·lació"
        ),
        'perc_financament': fields.float('% pagat per la companyia'),
        'propietari': fields.boolean('Propietari'),
        'numero_maxim_maquines': fields.integer('Número màxim de màquines'),
        'bloquejar_pm': fields.boolean(
            'Bloquejar APM',
            help=u"Si està activat, agafa la data entrada, si no, la de l'expedient"
        ),
        'data_connexio': fields.date('Data Connexió',
                                     help=u"Data real de connexió del CT"),
        'cnmc_tipo_instalacion': fields.char(
            u'CNMC Tipus Instal·lació', size=10, readonly=False),
        'bloquejar_cnmc_tipus_install': fields.boolean(
            u'Bloquejar Tipus Instal·lació CNMC', readonly=False,
            help=u"Si està activat, permet modificar manualment el "
                 u"tipus d'instal·lació de la CNMC"
        ),
        'bloquejar_cini': fields.boolean('Bloquejar CINI'),
        'tipus_instalacio_cnmc_id': fields.function(
            _tipus_instalacio_cnmc_id,
            method=True,
            string='Tipologia CNMC',
            relation='giscedata.tipus.installacio',
            type='many2one',
            fnct_inv=_tipus_instalacio_cnmc_id_inv,
            store={
                'giscedata.cts': (
                    _get_element_nobloc_ti, [
                        'tension_p', 'tension_s', 'potencia',
                        'id_subtipus', 'transformadors',
                        'bloquejar_cnmc_tipus_install'
                    ], 10
                )
            }
        ),
        '4771_entregada': fields.json('Dades 4771 entregada'),
        '4131_entregada_2016': fields.json('Dades 4131 entregada 2016'),
        '4666_entregada_2017': fields.json('Dades entregades 4666 2017'),
        '4666_entregada_2018': fields.json('Dades entregades 4666 2018'),
        "criteri_regulatori": fields.selection(
            TIPUS_REGULATORI,
            required=True,
            string="Criteri regulatori",
            help="Indica si el criteri a seguir al genrar els fitxers d'"
                 "inventari.\nSegons criteri:S'usa el criteri normal\n Froçar "
                 "incloure:S'afegeix l'element al fitxer\n Forçar excloure: "
                 "L'element no apareixera mai",
            select=1
        ),
        '4666_entregada_2019': fields.json('Dades entregades 4666 2019')
    }

    _defaults = {
        'active': lambda *a: 1,
        'n_suports_fusta': lambda *a: 0,
        'n_suports_formigo': lambda *a: 0,
        'n_suports_metallics': lambda *a: 0,
        'n_cadiretes_travessers': lambda *a: 0,
        'extincio': lambda *a: 0,
        'autovalvules': lambda *a: 0,
        'diposit_recollida_oli': lambda *a: 0,
        'malla_equipotencial': lambda *a: 0,
        'antiescalada': lambda *a: 0,
        'anell_terres': lambda *a: 0,
        'perc_financament': lambda *a: 100.00,
        'propietari': lambda *a: 1,
        'numero_maxim_maquines': lambda *a: 1,
        'bloquejar_pm': lambda *a: False,
        'bloquejar_cini': lambda *a: 0,
        'bloquejar_cnmc_tipus_install': lambda *a: 0,
        "criteri_regulatori": lambda *a: "criteri"
    }

    _order = "name, id"

    _sql_constraints = [
     ('name_uniq',
         'unique (name)', 'Ja existeix un CT amb aquest número'),
     ('perc_financament_0_100', 'CHECK(perc_financament between 0 and 100 )',
         "El % de finançament a d'estar entre 0 i 100")]

GiscedataCts()


class GiscedataExpedientsExpedient(osv.osv):

    _name = 'giscedata.expedients.expedient'
    _inherit = 'giscedata.expedients.expedient'

    _columns = {
        'cts': fields.many2many(
            'giscedata.cts', 'giscedata_cts_expedients_rel', 'expedient_id',
            'ct_id', 'CTS'),
    }

GiscedataExpedientsExpedient()


class GiscedataCtsMesures(osv.osv):

    _name = 'giscedata.cts.mesures'
    _description = 'Mesures'

    def _rpt_acoblament(self, cr, uid, ids, field_name, ar, context):
        # (Rn+Rf-Rfn)/2
        res = {}
        for mesura in self.browse(cr, uid, ids):
            res[mesura.id] = ((mesura.rpt_neutre + mesura.rpt_proteccio -\
            mesura.rpt_neutre_proteccio)/2)
        return res

    _columns = {
        'name': fields.datetime('Data', required=True),
        'id_ct': fields.many2one(
            'giscedata.cts', 'Centre Transformador', required=True),
        'trimestre': fields.many2one(
            'giscedata.trieni', string='Trimestre'),
        'rpt_unica': fields.float('Única (Ru)'),
        'rpt_neutre': fields.float('Neutre (Rn)'),
        'rpt_proteccio': fields.float('Protecció (Rf)'),
        'rpt_neutre_proteccio': fields.float('Neutre-Protecció (Rfn)'),
        'rpt_acoblament': fields.function(
            _rpt_acoblament,
            type='float',
            method=True,
            string='Acoblament (Rc)'),
        'rpt_autovalvules': fields.float('Autovàlvules'),
        'rpt_seccionador': fields.float('Seccionador'),
        'rpt_tensio': fields.float('Tensió N-F'),
        'rpt_i_neutre': fields.float('I neutre'),
        'tpc_i_prova': fields.float('I prova'),
        'tpc_pas_interior': fields.float('Vp. Int. mesurat'),
        'tpc_pas_exterior': fields.float('Vp. Ext. mesurat'),
        'tpc_contacte': fields.float('Vcte. mesurat'),
        'ps_interruptor': fields.boolean('Relé Interruptor Auto.'),
        'ps_ruptofusible': fields.boolean('Ruptofusible A.P.R.'),
        'ps_temperatura': fields.boolean('Temperatura'),
        'ailla_trafo_primari_secundari': fields.float('Primari-Secundari'),
        'ailla_trafo_primari_terra': fields.float('Primari-Terra'),
        'ailla_trafo_secundari_terra': fields.float('Secundari-Terra'),
        'observacions': fields.text('Observacions'),
    }

    _defaults = {
      'name': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
    }

    _order = "name, id"

GiscedataCtsMesures()


class GiscedataCtsSortidesBt(osv.osv):
    def _cups_connectats(self, cr, uid, ids, field_name, args, context=None):
        res = {}
        sql = """
        SELECT id
        FROM giscedata_cups_ps
        WHERE et = %s and linia::int = %s"
        """
        for sortida in self.browse(cr, uid, ids, context):
            if sortida.linia:
                cr.execute(sql, (sortida.ct.name, int(str(sortida.linia)[-2:])))
                res[sortida.id] = [a[0] for a in cr.fetchall()]
            else:
                res[sortida.id] = []
        return res

    _name = "giscedata.cts.sortidesbt"
    _description = "Sortides BT"

    _columns = {
        'name': fields.char('Descripció', size=255),
        'ct': fields.many2one('giscedata.cts', 'Centre transformador'),
        'linia': fields.integer('Línia', size=10),
        'seccio': fields.char('Secció', size=100),
        'material': fields.char('Material', size=20),
        'longitud': fields.float('Longitud'),
        'voltatge': fields.float('Tensió'),
        'municipi': fields.many2one('res.municipi', 'Municipi'),
        'cups_connectats': fields.function(
            _cups_connectats,
            method=True,
            type='one2many',
            relation='giscedata.cups.ps',
            string='Cups connectats')
    }

    _defaults = {
      'voltatge': lambda *a: 0.0,
    }

    _order = "linia"

GiscedataCtsSortidesBt()

"""
class giscedata_cts_expedient(osv.osv):

  def _get_data_intern(self, cr, uid, ids, field_name, ar, context):
    res = {}
    for id in ids:
      cr.execute("SELECT name FROM giscedata_expedients_expedient_datesct WHERE expedient="+str(id)+" UNION SELECT name FROM giscedata_expedients_expedient_dateslat WHERE expedient="+str(id)+" ORDER BY name ASC LIMIT 1")
      res[id] = cr.fetchone()
      if res[id] == None:
        res[id] = 'No iniciat'
    return res

  def _get_data_industria(self, cr, uid, ids, field_name, ar, context):
    res = {}
    for id in ids:
      cr.execute("SELECT industria_data FROM giscedata_expedients_expedient WHERE id = "+str(id))
      res[id] = cr.fetchone()[0]
      if res[id] == None:
        res[id] = 'No iniciat'
    return res

  def _get_n_intern(self, cr, uid, ids, field_name, ar, context):
    res = {}
    for id in ids:
      cr.execute("SELECT empresa FROM giscedata_expedients_expedient WHERE id = "+str(id))
      res[id] = cr.fetchone()[0]
    return res

  def _get_n_industria(self, cr, uid, ids, field_name, ar, context):
    res = {}
    for id in ids:
      cr.execute("SELECT industria FROM giscedata_expedients_expedient WHERE id = "+str(id))
      res[id] = cr.fetchone()[0]
    return res

  _name = "giscedata.cts.expedient"
  _description = "Expedients de la CT"

  _columns = {
    'name': fields.many2one('giscedata.expedients.expedient', 'Expedient'),
    'ct': fields.many2one('giscedata.cts', 'Centre Transformador'),
    'n_intern': fields.function(_get_n_intern, type='char', method=True, string="Expedient intern"),
    'n_industria': fields.function(_get_n_industria, type='char', method=True, string="Expedient indústria"),
    'data_industria': fields.function(_get_data_industria, type='char', method=True, string="Data d'autorització"),
    'data_intern': fields.function(_get_data_intern, type='char', method=True, string="Data posada en marxa"),
    'confirmat': fields.boolean('Confirmat'),
  }

  _defaults = {

  }

  _order = "name, id"

giscedata_cts_expedient()
"""
