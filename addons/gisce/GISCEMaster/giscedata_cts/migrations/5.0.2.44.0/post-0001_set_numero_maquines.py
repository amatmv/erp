# -*- coding: utf-8 -*-
import netsvc


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel("migration", level, msg)


def migrate(cursor, installed_version):
    log("Actualitzant el número màxim de màquines segons les instal·lades...")
    cursor.execute("""
        UPDATE giscedata_cts ct
            SET numero_maxim_maquines = query.numero_maquines
        FROM (
            SELECT c.id as ct_id, greatest(1, count(t.id)) as numero_maquines
            FROM giscedata_cts c
                LEFT JOIN giscedata_transformador_trafo t on (t.ct = c.id)
            GROUP BY c.id
        ) AS query
        WHERE query.ct_id = ct.id
    """)
    log("Fet.")