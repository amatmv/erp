# -*- coding: utf-8 -*-
import netsvc
import pooler


def migrate(cursor, installed_version):
    logger = netsvc.Logger()

    uid = 1
    pool = pooler.get_pool(cursor.dbname)

    inst_name = u"CTS"
    model = 'giscedata.cts'
    taula = 'giscedata_cts'

    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Modificar %s.bloquejar_pm " % inst_name)

    #Omplim bloquejar_pm amb false
    cursor.execute("update %s set bloquejar_pm=false " % taula)

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Modificar %s.bloquejar_pm realitzat "
                         u"correctament" % inst_name)

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Migrant data_pm de %s" % inst_name)
    inst_obj = pool.get(model)

    cursor.execute('SELECT id FROM %s ' % taula)

    insts = cursor.fetchall()
    total = len(insts)
    counter = 0
    for res in insts:
        inst_obj.write(cursor, uid, res[0], {})
        if (counter % 100) == 0:
            logger.notifyChannel('migration', netsvc.LOG_INFO,
                                 u"%d/%d" % (counter, total))
        counter += 1

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         u"Migrat data_pm de %d %s "
                         u"correctament" % (total, inst_name))
