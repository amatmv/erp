# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Renaming field rotation in giscedata_cts to rotation_old_float'
    )

    # Busquem l'id del camp en el model_fields
    query_model_fields_select = '''
        SELECT name, id FROM ir_model_fields 
        WHERE ttype='float' 
        AND model='giscedata.cts' 
        AND name='rotation';
    '''
    cursor.execute(query_model_fields_select)
    model_field_id = cursor.fetchone()

    if model_field_id:
        # Renombrem la columna
        query_rename_rotation = '''
            ALTER TABLE giscedata_cts 
            RENAME COLUMN rotation 
            TO rotation_old_float;
        '''
        cursor.execute(query_rename_rotation)

        # Eliminem l'entrada de la taula ir_model_data
        query_model_data_delete = '''
            DELETE FROM ir_model_data
            WHERE res_id = %(id)s
            AND module = 'giscedata_cts';
        '''
        cursor.execute(query_model_data_delete, {'id': model_field_id[1]})

        # Eliminem l'entrada de la taula ir_model_fields
        query_model_fields_delete = '''
            DELETE FROM ir_model_fields 
            WHERE ttype='float' 
            AND model='giscedata.cts' 
            AND name='rotation';
        '''
        cursor.execute(query_model_fields_delete)


def down(cursor, installed_version):
    pass


migrate = up
