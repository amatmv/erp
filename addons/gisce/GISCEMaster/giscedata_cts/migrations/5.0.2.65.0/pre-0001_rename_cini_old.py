# coding=utf-8
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return
    oopgrade.rename_columns(cursor, {
        'giscedata_cts': [('cini', 'cini_pre_auto_ct')]
    })
    # Avoid to recalculate cini
    oopgrade.add_columns(cursor, {
        'giscedata_cts': [('cini', 'VARCHAR(8)')]
    })


def down(cursor):
    oopgrade.rename_columns(cursor, {
        'giscedata_cts': [('cini_pre_auto_ct', 'cini')]
    })


migrate = up
