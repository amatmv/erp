# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info(
        'Renaming field trimestre in giscedata_cts_mesures to trimestre_old')

    query_rename = '''
        ALTER TABLE giscedata_cts_mesures RENAME COLUMN trimestre TO trimestre_old;
    '''

    cursor.execute(query_rename)


def down(cursor, installed_version):
    pass


migrate = up
