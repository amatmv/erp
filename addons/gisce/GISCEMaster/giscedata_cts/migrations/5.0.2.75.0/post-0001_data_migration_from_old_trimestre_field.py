# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Updating giscedata_cts_mesures')

    query_select = '''
        SELECT t.id, m.id
        FROM giscedata_cts_mesures AS m
        LEFT JOIN giscedata_trieni AS t ON (m.trimestre_old = t.name);
    '''

    cursor.execute(query_select)
    elements_list = cursor.fetchall()

    query_update = '''
        UPDATE giscedata_cts_mesures
        SET trimestre = %s
        WHERE id = %s
    '''

    for element in elements_list:
        trimestre_exists = element[0] is not None
        if trimestre_exists:
            cursor.execute(query_update, element)


def down(cursor, installed_version):
    pass


migrate = up
