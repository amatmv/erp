# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Centres Transformadors",
    "description": """Centres Transformadors


 $Id: __terp__.py 1247 2008-01-21 08:53:07Z raul $""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Centres Transformadors",
    "depends":[
        "base",
        "base_extended",
        "giscedata_trieni",
        "giscedata_expedients",
        "giscedata_administracio_publica_cnmc_distri"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_cts_demo.xml"
    ],
    "update_xml":[
        "giscedata_centrestransformadors_view.xml",
        "giscedata_centrestransformadors_report.xml",
        "giscedata_centrestransformadors_wizard.xml",
        "ir.model.access.csv",
        "security/giscedata_cts_security.xml",
        "security/ir.model.access.csv",
        "giscedata_tipus_instalacio_ct_data.xml"
    ],
    "active": False,
    "installable": True
}
