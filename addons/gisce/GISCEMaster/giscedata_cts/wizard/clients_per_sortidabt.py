# *-* coding: utf-8 *-*

import wizard

_init_form = """<?xml version="1.0"?>
<form string="Opcions d'entrada" col="2">
  <field name="ct" />
  <field name="sortida_bt" domain="[('ct' ,'=', ct)]" widget="many2many"/>
</form>
"""

_init_fields = {
  'ct': {'string': 'CT', 'type': 'many2one', 'relation': 'giscedata.cts'},
  'sortida_bt': {'string': 'Sortida BT', 'type': 'one2many', 'relation': 'giscedata.cts.sortidesbt'}
}

def _print(self, cr, uid, data, context={}):
  return {'ids': data['form']['sortida_bt'][0][2]}

class wizard_cts_clients_sortidabt(wizard.interface):

  states = {
    'init': {
      'actions': [],
      'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields, 'state': [('end', 'Cancelar', 'gtk-cancel'), ('print', 'Imprimir', 'gtk-print')]}
    },
    'print': {
      'actions': [_print],
      'result': {'type': 'print', 'report':
      'giscedata.cts.report4', 'get_id_from_action':True,
        'state':'end'}
    },
    'end': {
      'actions': [],
      'result': {'type': 'state', 'state': 'end'}
    }
  }


wizard_cts_clients_sortidabt('giscedata.cts.clients.sortidabt')
