# coding=utf-8
from __future__ import unicode_literals
from dateutil.relativedelta import relativedelta
from datetime import datetime

from destral import testing
from destral.transaction import Transaction

from expects import *


class TestCTs(testing.OOTestCase):
    def test_field_cfo_returns_false_if_not_data_pm(self):
        """
        Fem que si la data actual és inferior o igual a 6 anys el Centre té
        CFO i per tant el camp cfo ha de ser true
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            cts_obj = self.openerp.pool.get('giscedata.cts')

            ct_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cts', 'ct1'
            )[1]
            ct = cts_obj.browse(cursor, uid, ct_id)
            expect(ct.cfo).to(be_false)

    def test_field_cfo_returns_true_under_six_years(self):
        """
        Fem que si la data actual és inferior o igual a 6 anys el Centre té
        CFO i per tant el camp cfo ha de ser true
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            cts_obj = self.openerp.pool.get('giscedata.cts')

            ct_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cts', 'ct1'
            )[1]

            dt = (datetime.now() - relativedelta(years=5)).strftime('%Y-%m-%d')

            cts_obj.write(cursor, uid, [ct_id], {'bloquejar_pm': 1})
            cts_obj.write(cursor, uid, [ct_id], {'data_pm': dt})

            ct = cts_obj.browse(cursor, uid, ct_id)
            expect(ct.cfo).to(be_true)

    def test_field_cfo_returns_false_over_six_years(self):
        """
        Fem que si la data actual és inferior o igual a 6 anys el Centre té
        CFO i per tant el camp cfo ha de ser true
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            cts_obj = self.openerp.pool.get('giscedata.cts')

            ct_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cts', 'ct1'
            )[1]

            dt = (datetime.now() - relativedelta(years=7)).strftime('%Y-%m-%d')

            cts_obj.write(cursor, uid, [ct_id], {'bloquejar_pm': 1})
            cts_obj.write(cursor, uid, [ct_id], {'data_pm': dt})

            ct = cts_obj.browse(cursor, uid, ct_id)
            expect(ct.cfo).to(be_false)

    def test_field_cfo_is_calculated_depending_on_date_context(self):
        """
        Fem que si la data actual és inferior o igual a 6 anys el Centre té
        CFO i per tant el camp cfo ha de ser true
        """
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            cts_obj = self.openerp.pool.get('giscedata.cts')

            ct_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cts', 'ct1'
            )[1]

            dt = (datetime.now() - relativedelta(years=8))
            dt_context = dt + relativedelta(years=5)

            cts_obj.write(cursor, uid, [ct_id], {'bloquejar_pm': 1})
            cts_obj.write(cursor, uid, [ct_id], {
                'data_pm': dt.strftime('%Y-%m-%d')
            })

            ct = cts_obj.browse(cursor, uid, ct_id, context={
                'data_inici_cfo': dt_context.strftime('%Y-%m-%d')
            })
            expect(ct.cfo).to(be_true)


