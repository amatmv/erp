# -*- coding: utf-8 -*-
{
    "name": "Base module for pending invoices management",
    "description": "Base module",
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Facturació",
    "depends": [
        "base",
        "base_extended",
        "account_invoice_pending",
        "account_invoice_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "account_view.xml",
        "account_invoice_view.xml",
        "giscedata_facturacio_impagat_data.xml",
        "partner_view.xml",
        "partner_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
