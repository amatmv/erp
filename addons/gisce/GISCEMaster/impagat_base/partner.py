# coding=utf-8
from osv import osv, fields


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def _fnct_total_debt(self, cursor, uid, ids, field_names, arg, context):
        res = dict.fromkeys(ids, 0)
        partners = self.read(cursor, uid, ids, ['property_account_debtor'])
        accounts = set(
            p['property_account_debtor'][0] for p in partners
            if p['property_account_debtor']
        )
        query = self.pool.get('account.move.line')._query_get(
            cursor, uid, context=context
        )
        if not accounts:
            return res
        cursor.execute("""\
            SELECT l.partner_id, SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            WHERE
              a.id IN %s
              AND l.partner_id IN %s
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY l.partner_id
            """, (tuple(accounts), tuple(ids))
        )

        for pid, value in cursor.fetchall():
            res[pid] = value
        return res

    def _fnct_has_debt(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, 0)


        partners = self.read(cursor, uid, ids, ['property_account_debtor'])
        accounts = set(
            p['property_account_debtor'][0] for p in partners
            if p['property_account_debtor']
        )
        if not accounts:
            return res
        query = self.pool.get('account.move.line')._query_get(
            cursor, uid, context=context
        )
        cursor.execute("""\
            SELECT l.partner_id, SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            WHERE
              a.id IN %s
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY l.partner_id
              HAVING SUM(l.debit-l.credit) > 0
            """, (tuple(accounts), )
        )
        for partner, value in cursor.fetchall():
            res[partner] = 1
        return res

    def _fnct_has_debt_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        has_debt = args[0][2]
        query = self.pool.get('account.move.line')._query_get(
            cursor, uid, context=context
        )
        ids = self.search(cursor, uid, [], context=context)
        partners = self.read(cursor, uid, ids, ['property_account_debtor'])
        accounts = set(
            p['property_account_debtor'][0] for p in partners
            if p['property_account_debtor']
        )
        if not accounts:
            return [('id', '=', 0)]
        cursor.execute("""\
            SELECT l.partner_id, SUM(l.debit-l.credit)
            FROM account_move_line l
            INNER JOIN account_account a ON (l.account_id=a.id)
            WHERE
              a.id IN %s
              AND l.reconcile_id IS NULL
              AND """ + query + """
              GROUP BY l.partner_id
              HAVING {} SUM(l.debit-l.credit) > 0
            """.format(not has_debt and 'NOT' or ''), (tuple(accounts),)
                       )
        partner_ids = [p[0] for p in cursor.fetchall()]
        return [('id', 'in', partner_ids)]

    _columns = {
        'property_account_debtor': fields.property(
            'account.account',
            type='many2one',
            relation='account.account',
            string="Account Debtor",
            method=True,
            view_load=True,
            domain="[('type', '=', 'receivable')]",
            help="This account will be used as the debtor account for the current partner",
            required=True
        ),
        'total_debt': fields.function(
            _fnct_total_debt,
            method=True,
            string='Total Debt',
            help="Total amount this customer debts you."
        ),
        'has_debt': fields.function(
            _fnct_has_debt,
            type='boolean',
            fnct_search=_fnct_has_debt_search,
            method=True,
            string='Has debt',
        )
    }


ResPartner()
