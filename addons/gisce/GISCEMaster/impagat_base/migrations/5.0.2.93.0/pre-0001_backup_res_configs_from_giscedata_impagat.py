# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Duplicating res.configs from giscedata_facturacio_impagat to preserve it's values.
    ''')
    cursor.execute("""
        INSERT INTO res_config (name, value) (
            SELECT 'migration_' || name, value FROM res_config WHERE name IN (
                'devolucions_unpay_move_pending_state',
                'unpay_move_pending_state'
            )
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
