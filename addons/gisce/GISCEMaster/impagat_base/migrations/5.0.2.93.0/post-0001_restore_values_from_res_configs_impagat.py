# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Restoring old res.configs values from impagat module that have been 
        duplicated in and removing the duplicates.
    ''')
    for config_var in (
            'devolucions_unpay_move_pending_state',
            'unpay_move_pending_state'
    ):
        cursor.execute("""
            UPDATE res_config SET value = (
                SELECT value FROM res_config WHERE name = %s
            )
            WHERE name LIKE %s
        """, ('migration_{}'.format(config_var), config_var))

    cursor.execute("""
        DELETE FROM res_config WHERE name IN (
            'migration_devolucions_unpay_move_pending_state',
            'migration_unpay_move_pending_state'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
