# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Updating access rules from giscedata_facturacio_impagat and setting 
        them to impagat_base module.
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module='impagat_base'
        WHERE module = 'giscedata_facturacio_impagat' 
        AND name IN (
            'config_devolucions_unpay_move_pending_state',
            'config_unpay_move_pending_state'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up
