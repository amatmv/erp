# coding=utf-8
from __future__ import unicode_literals

from osv import osv
from tools.translate import _


class MoveToUnpaid(osv.osv_memory):

    _name = 'move.to.unpaid'
    _inherit = 'move.to.unpaid'

    def move_to_unpaid(self, cursor, uid, context=None):

        if context is None:
            context = {}

        invoice_ids = self.get_invoices_to_update(cursor, uid)
        unpay_obj = self.pool.get('wizard.unpay')

        for invoice_id in invoice_ids:
            ctx = {
                'model': 'account.invoice',
                'active_ids': [invoice_id]
            }
            unpay_obj_fields = unpay_obj.fields_get(cursor, uid).keys()
            values = unpay_obj.default_get(
                cursor, uid, unpay_obj_fields, context=ctx
            )
            values['name'] = _("Venciment data pagament")
            wiz_id = unpay_obj.create(cursor, uid, values, context=ctx)
            unpay_obj.unpay(cursor, uid, [wiz_id], context=ctx)

        return True


MoveToUnpaid()
