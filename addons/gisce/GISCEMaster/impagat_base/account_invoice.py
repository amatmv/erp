# -*- encoding: utf-8 -*-
from datetime import datetime, timedelta
from ast import literal_eval

from osv import osv, fields

from workalendar.europe import Spain
from account_invoice_pending.account_invoice import PENDING_DAYS_TYPE


class AccountInvoice(osv.osv):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def unpay(self, cursor, uid, ids, amount, pay_account_id, period_id,
              pay_journal_id, context=None, name=''):
        """Go on pending state on unpaying an invoice.
        """
        if context is None:
            context = {}
        config_obj = self.pool.get('res.config')

        res = super(AccountInvoice, self).unpay(
            cursor, uid, ids, amount, pay_account_id, period_id, pay_journal_id,
            context, name
        )
        invoice = self.browse(cursor, uid, ids[0])
        if not invoice.group_move_id or context.get('unpay_individual'):
            default_move_pstate = int(config_obj.get(
                cursor, uid, 'unpay_move_pending_state', 0
            ))
            move_pstate = context.get(
                'unpay_move_pending_state', default_move_pstate
            )
            pstate_obj = self.pool.get('account.invoice.pending.state')
            if pstate_obj is not None and move_pstate:
                self.go_on_pending(cursor, uid, ids, context)
        return res

    def pay_and_reconcile_group(self, cursor, uid, ids, pay_amount,
                                pay_account_id, period_id, pay_journal_id,
                                writeoff_acc_id, writeoff_period_id,
                                writeoff_journal_id, context=None, name=''):
        res = super(AccountInvoice, self).pay_and_reconcile_group(
            cursor, uid, ids, pay_amount, pay_account_id, period_id,
            pay_journal_id, writeoff_acc_id, writeoff_period_id,
            writeoff_journal_id, context, name
        )
        invoice = self.browse(cursor, uid, ids[0], context=context)
        for line in invoice.group_move_id.line_id:
            if not line.invoice:
                continue
            g_invoice = line.invoice
            correct_state = self._get_default_pending(
                cursor, uid, process_id=g_invoice.pending_state.process_id.id)
            if not g_invoice.residual and g_invoice.pending_state.weight != 0:
                g_invoice.set_pending(correct_state)
        return res

    def set_pending(self, cursor, uid, ids, pending_id, context=None):
        if context is None:
            context = {}
        for _, (hook_ids, fact_obj) in self.get_factura_ids_from_invoice_id(
                cursor, uid, ids, context={'no_account_invoice': True}
        ).items():
            fact_obj.set_pending_pre_hook(
                cursor, uid, hook_ids, pending_id, context=context
            )
            res = super(AccountInvoice, self).set_pending(
                cursor, uid, ids, pending_id, context=context
            )
            fact_obj.set_pending_post_hook(
                cursor, uid, hook_ids, pending_id, context=context
            )
            return res


AccountInvoice()


class AccountInvoicePendingStateProcess(osv.osv):

    _name = 'account.invoice.pending.state.process'
    _inherit = 'account.invoice.pending.state.process'

    def get_cutoff_day(self, cursor, uid, process_id,
                       not_allowed_days=None, allowed_days=None,
                       start_day=None, delta_days=None, delta_type=None,
                       context=None):
        """
        Funció que retorna un dia en què es pot tallar pel procés de tall
        especificat.

        :param cursor:
        :param uid:
        :param process_id: procés de tall pel qual buscar el dia de tall
        :type process_id: int
        :param not_allowed_days: dies no permesos a tallar,
                                 per defecte, divendres dissabte i diumenge
        :type not_allowed_days: list of int
        :param allowed_days: dies permesos a tallar,
                             per defecte, dilluns, dimarts, dimecres i dijous
        :type allowed_days: list of int
        :param start_day: data a partir de la qual es buscará un dia per tallar
        :type start_day: date in format YYYY-MM-DD
        :param delta_days: dies que s'han de sumar si encara no s'esta al proces
        :type delta_days: int
        :param delta_type: tipus de dies que s'han de sumar, bussines o natural
        :type delta_type: string
        :param context:
        :return:
        """

        if context is None:
            context = {}

        if isinstance(process_id, (list, tuple)):
            process_id = process_id[0]

        process_info = self.read(
            cursor, uid, process_id, ['cutoff_days', 'days_type'])
        calendar = Spain()

        # Si no rebem dia, agafem el dia d'avui
        if start_day is None:
            start_day = datetime.now()

        # Quan s'imprimeix la carta de Tall BS encara estem a un estat anterior,
        # aixi que l'start_day no es el correcte. Amb els delta_days i
        # delta_type calculem la data de l'start day real.
        if delta_days is not None and delta_type is not None:
            if delta_type == 'natural':
                # Si els dies són naturals, es sumen dies a la data d'inici
                start_day = (
                        start_day + timedelta(days=delta_days)
                )
            else:
                start_day = calendar.add_working_days(
                    start_day, delta_days
                )

        # Si no ens passen dies NO permesos per tallar, es busca:
        #   - La obtindrem del metode que es personalitzarà cada mòdul de
        #     impagaments
        #   - Per defecte són divendres, dissabte i diumenge
        if not_allowed_days is None:
            not_allowed_days = self.get_non_allowed_cutoff_days(
                cursor, uid, context=context
            )

        # Si no ens passen dies permesos per tallar, per defecte són dilluns,
        # dimarts, dimecres i dijous
        if allowed_days is None:
            allowed_days = [0, 1, 2, 3]

        # Busquem un dia permès
        assert (
            len(list(set(allowed_days) - set(not_allowed_days))) > 0,
            'No hi ha dies permesos per efectuar el tall'
        )
        allowed_days = list(set(allowed_days) - set(not_allowed_days))

        # Sumem els dies a partir dels quals el procés de tall ens indica que
        # es pot tallar
        if process_info['days_type'] == 'natural':
            # Si els dies són naturals, es sumen dies a la data d'inici
            action_day = (
                start_day + timedelta(days=process_info['cutoff_days'])
            )
        else:
            # Si els dies són laborals, es sumen dies laborals amb la llibreria
            # workalendar
            action_day = calendar.add_working_days(
                start_day, process_info['cutoff_days']
            )

        # Busquem el següent dia laboral que estigui en els dies permesos
        while action_day.weekday() not in allowed_days:
            action_day = calendar.add_working_days(
                action_day, 1
            )

        return action_day

    def get_non_allowed_cutoff_days(self, cursor, uid, context=None):
        """
        Get the non allowed cutoff days as a list of ints.
        Each module must implement it's method
        """
        raise Exception("Each custom module must implement it's method to get "
                        "the non allowed cutoff days")

    _columns = {
        'cutoff_days': fields.integer(
            'Number of days to cut off', required=True
        ),
        'days_type': fields.selection(
            PENDING_DAYS_TYPE, 'Days type', required=True
        )
    }

    _defaults = {
        'cutoff_days': lambda *a: 0,
        'days_type': lambda *a: 'natural'
    }


AccountInvoicePendingStateProcess()
