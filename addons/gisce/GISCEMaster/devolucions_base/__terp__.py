# -*- coding: utf-8 -*-
{
    "name": "Devolucions remeses (Base)",
    "description": """
Gestió de devolucions del banc
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Facturació",
    "depends": [
        "account_invoice_base",
        "c2c_webkit_report",
        "l10n_ES_remesas",
    ],
    "init_xml": [],
    "demo_xml": ["account_demo.xml"],
    "update_xml": [
        "giscedata_facturacio_view.xml",
        "wizard/wizard_carregar_devolucio_view.xml",
        "giscedata_facturacio_devolucions_data.xml",
        "devolucions_report.xml",
        "security/devolucions_base_security.xml",
        "wizard/wizard_desfer_devolucions.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
