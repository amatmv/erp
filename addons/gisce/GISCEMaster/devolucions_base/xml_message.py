# -*- coding: utf-8 -*-                                    

from lxml import objectify, etree

class Message(object):

    def __init__(self, xml, encoding='UTF-8'):
        if isinstance(xml, file):
            xml.seek(0)
            xml = xml.read()

        self.xml_orig = xml

        try:
            root = etree.fromstring(str(xml))
        except etree.XMLSyntaxError:
            raise Exception('Wrong XML')

        self.str_xml = etree.tostring(root).decode(encoding)

        try:
            self.obj = objectify.fromstring(self.str_xml)
        except:
            raise Exception('Wrong XML')
