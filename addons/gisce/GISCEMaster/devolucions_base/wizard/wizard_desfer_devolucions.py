# -*- coding: utf-8 -*-
from osv import fields, osv
from tools.translate import _


class WizardDesferDevolucions(osv.osv_memory):
    """Wizard"""
    _name = 'wizard.desfer.devolucions'
    _description = 'Asistent per desfer devolucions'

    def _default_info(self, cursor, uid, context=None):
        dev_ids = context.get('active_ids', [])
        info = _('Es procedira a desfer les seguents '
                 'devolucions mitjançant pagaments:\n {}'
                 )
        dev_obj = self.pool.get('giscedata.facturacio.devolucio')
        devs = dev_obj.read(cursor, uid, dev_ids, ['name'], context=context)
        devs_names = [dev['name'] for dev in devs]
        info = info.format('\n'.join(devs_names))
        return info

    def desfer_devolucions(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        dev_ids = context.get('active_ids', [])

        if not dev_ids:
            raise osv.except_osv(
                _('Error !'),
                _('No s\'han seleccionat devolucions')
            )
        dev_obj = self.pool.get('giscedata.facturacio.devolucio')

        res, msg = dev_obj.desfer_devolucio(
            cursor, uid, dev_ids, context=context
        )
        inf = ''
        if not res:
            inf = msg
        else:
            inf = 'S\'han desfet les devolucions'

        self.write(
            cursor, uid, ids, {'info': inf, 'state': 'end'}, context=context
        )

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'info': fields.text('Informació')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info
    }

WizardDesferDevolucions()