# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import base64
import zipfile
import StringIO
from libcomxml.core import clean_xml

from devolucions_base.xml_message import Message
from xml.dom.minidom import parseString
from osv import fields, osv
from tools.translate import _


def get_filename(filename):
    if isinstance(filename, str):
        return unicode(filename, errors='ignore')
    return filename


class WizardCarregarDevolucio(osv.osv_memory):
    """Wizard"""    
    _name = 'wizard.carregar.devolucio'
    
    def is_sepa_file(self, cursor, uid, file_to_check, context=None):
        file = StringIO.StringIO(file_to_check)

        header_th = 10
        i = 0
        for line in file.readlines():
            if '<?xml' in line:
                return True
            elif i > header_th:
                return False
            i += 1

        return False

    def import_devolution_file(self, cursor, uid, devolution_file, filename,
                               devolucio_id=None, context=None):

        devolution_name = '{}_{}'.format(_('devolucio'), filename)
        is_sepa_file = self.is_sepa_file(
            cursor, uid, devolution_file, context=context)

        conf_obj = self.pool.get('res.config')
        devolucio_obj = self.pool.get('giscedata.facturacio.devolucio')
        if devolucio_id is None:
            devolution_date_from_file = bool(int(conf_obj.get(
                cursor, uid, 'devolucio_data_des_de_fitxer', False
            )))
            devolucio_vals = self.set_devolucio_vals(cursor, uid, devolution_file, context=context)
            devolucio_vals.update({'name': devolution_name})
            if devolution_date_from_file and is_sepa_file:
                xml = clean_xml(devolution_file)
                devolution_message = Message(xml)
                devolution_date = devolution_message.obj.CstmrPmtStsRpt.GrpHdr.CreDtTm
                devolucio_vals.update({'date': devolution_date})
            devolucio_id = devolucio_obj.create(cursor, uid, devolucio_vals)
            devolucio = devolucio_obj.browse(cursor, uid, devolucio_id)
            devolucio_automatic_journal = bool(int(conf_obj.get(
                cursor, uid, 'devolucio_automatic_journal', '0'
            )))
            if not devolucio.pay_journal_id or devolucio_automatic_journal:
                payment_data = self.calc_payment_data_from_file(cursor, uid, devolution_file, context=context)
                devolucio_obj.write(cursor, uid, [devolucio_id], payment_data)
        else:
            # Estem en un zip, sumem tots els totals i nlines
            devolucio_vals = self.set_devolucio_vals(cursor, uid, devolution_file, context=context)
            old_vals = devolucio_obj.read(cursor, uid, devolucio_id, [])
            devolucio_vals['num_of_rows'] = devolucio_vals.get('num_of_rows', 0) + old_vals.get('num_of_rows', 0)
            devolucio_vals['total_devolution'] = devolucio_vals.get('total_devolution', 0) + old_vals.get('total_devolution', 0)
            devolucio_obj.write(cursor, uid, devolucio_id, devolucio_vals)

        attachment_obj = self.pool.get('ir.attachment')
        attachment_obj.create(cursor, uid, {
            'name': devolution_name,
            'datas': base64.b64encode(devolution_file),
            'datas_fname': filename,
            'res_model': 'giscedata.facturacio.devolucio',
            'res_id': devolucio_id,
        }, context=context)
        if is_sepa_file:
            self.import_sepa_file(
                cursor, uid, devolution_file, devolucio_id, context=context)
        else:
            self.import_text_file(
                cursor, uid, devolution_file, devolucio_id, context=context)
        return devolucio_id

    def set_devolucio_vals(self, cursor, uid, devolution_file, context={}):
        vals = {}

        xml_data = parseString(devolution_file)
        parent_tag = xml_data.getElementsByTagName('OrgnlPmtInfAndSts')

        if len(parent_tag) == 1:
            num_of_lines = parent_tag[0].getElementsByTagName('OrgnlNbOfTxs')
            if len(num_of_lines) == 1:
                vals['num_of_rows'] = int(num_of_lines[0].firstChild.data)

            total_sums = parent_tag[0].getElementsByTagName('OrgnlCtrlSum')
            if len(total_sums) == 1:
                vals['total_devolution'] = float(total_sums[0].firstChild.data)

        return vals


    def calc_payment_data_from_file(self, cursor, uid, devolution_file, context={}):
        xml_data = parseString(devolution_file)
        cdtr = xml_data.getElementsByTagName('CdtrAcct')

        if len(cdtr) != 0:
            iban_tag = cdtr[0].getElementsByTagName('IBAN')
            if len(iban_tag) != 0:
                iban = iban_tag[0].firstChild.data

                partner_bank_obj = self.pool.get('res.partner.bank')
                partner_bank = partner_bank_obj.search(cursor, uid, [('iban', '=', iban)])

                if len(partner_bank) == 1:
                    payment_mode_obj = self.pool.get('payment.mode')
                    payment_mode_id = payment_mode_obj.search(cursor, uid, [('bank_id', '=', partner_bank[0]), ('tipo', '=', 'sepa19')])

                    if len(payment_mode_id) == 1:
                        payment_mode = payment_mode_obj.browse(cursor, uid, payment_mode_id[0])

                        res = {'payment_mode_id': payment_mode_id[0], 'pay_journal_id': payment_mode.journal and payment_mode.journal.id}
                        return res
        return {}

    def import_zip_file(self, cursor, uid, zip_file, group_in_one=None, context=None):
        devolucions_creades_ids = []

        if group_in_one:
            first_loop = True
            devolucio_id = False
            for info in zip_file.infolist():
                if not info.file_size:
                    continue
                if not info.filename.endswith('/'):
                    if first_loop:
                        devolution_file = zip_file.read(info.filename)
                        filename = get_filename(info.filename.split('/')[-1])
                        devolucio_id = self.import_devolution_file(
                            cursor, uid, devolution_file, filename,
                            context=context
                        )
                        devolucions_creades_ids.append(devolucio_id)
                        first_loop = False
                    else:
                        devolution_file = zip_file.read(info.filename)
                        filename = get_filename(info.filename.split('/')[-1])
                        self.import_devolution_file(
                            cursor, uid, devolution_file, filename,
                            devolucio_id, context=context)

        else:
            for info in zip_file.infolist():
                if not info.file_size:
                    continue
                # check is not a folder
                if not info.filename.endswith('/'):
                    devolution_file = zip_file.read(info.filename)
                    filename = get_filename(info.filename.split('/')[-1])
                    devolucio_id = self.import_devolution_file(
                        cursor, uid, devolution_file, filename, context=context)
                    devolucions_creades_ids.append(devolucio_id)
        return devolucions_creades_ids

    def import_file(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])

        devolution_file = wizard.arxiu
        filename = wizard.filename
        import_file_from_menu = context.get('from_menu', False)

        data = base64.b64decode(devolution_file)
        file_handler = StringIO.StringIO(data)
        try:
            import_file = zipfile.ZipFile(file_handler, "r")
            is_zip_file = True
        except zipfile.BadZipfile:
            import_file = data
            is_zip_file = False

        if is_zip_file:
            devolucions_creades_ids = self.import_zip_file(
                cursor, uid, import_file, wizard.agrupar_devolucions,
                context=context
            )
        else:
            devolucio_id = None
            if not import_file_from_menu:
                devolucio_id = context.get('active_id')
            devolucio_creada_id = self.import_devolution_file(
                cursor, uid, import_file, filename, devolucio_id,
                context=context)
            devolucions_creades_ids = [devolucio_creada_id]

        str_list = map(lambda dev_id: str(dev_id), devolucions_creades_ids)
        dev_creades_ids_str = ','.join(str_list)
        wizard.write({
            'state': 'end',
            'devolucions_creades_ids': dev_creades_ids_str
        })

    def show_created_devolutions(self, cursor, uid, ids, context=None):
        return {
            'domain': "[('id', 'in', [{}])]".format(
                str(self.read(
                    cursor, uid, ids[0], ['devolucions_creades_ids']
                )[0]['devolucions_creades_ids'])
            ),
            'name': _('Devolucions creades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.devolucio',
            'type': 'ir.actions.act_window'
        }

    def import_sepa_file(self, cursor, uid, devolution_file, devolucio_id,
                         context=None):
        xml = clean_xml(devolution_file)
        devolucio_linia_obj = self.pool.get(
            'giscedata.facturacio.devolucio.linia'
        )
        
        devolution_message = Message(xml)
        for original_payment_info in (devolution_message.obj.CstmrPmtStsRpt.
                                      OrgnlPmtInfAndSts):
            # Normally this loop only iterate once
            for reason_status_info in original_payment_info.TxInfAndSts:
                numfactura = reason_status_info.OrgnlEndToEndId.text
                pagador = reason_status_info.OrgnlTxRef.Dbtr.Nm.text
                compte = reason_status_info.OrgnlTxRef.DbtrAcct.Id.IBAN.text
                import_factura = reason_status_info.OrgnlTxRef.Amt.InstdAmt.text
                reject_code = reason_status_info.StsRsnInf.Rsn.Cd.text

                vals = {
                    'devolucio_id': devolucio_id,
                    'numfactura': numfactura,
                    'pagador': pagador,
                    'ccc_carrec': compte,
                    'import': import_factura,
                    'reject_code': reject_code,
                }
                devolucio_linia_obj.create(cursor, uid, vals, context)

        return {}

    def import_text_file(self, cursor, uid, devolution_file, devolucio_id,
                         context=None):
        """Carregar fitxer
        """
        devolucio_linia_obj = self.pool.get(
            'giscedata.facturacio.devolucio.linia'
        )
        
        file = StringIO.StringIO(devolution_file)

        for line in file:
            if not line.startswith('56'):
                continue
            numfactura = line[104:114].lstrip('0')
            pagador = line[28:68]
            compte = line[68:88]
            import_factura = float(line[88:96] + '.' + line[96:98])

            vals = {
                'devolucio_id': devolucio_id,
                'numfactura':  numfactura,
                'pagador': pagador,
                'ccc_carrec': compte,
                'import': import_factura,
            }
            devolucio_linia_obj.create(cursor, uid, vals, context)
        return {}

    def _get_default_info(self, cursor, uid, context=None):
        return _('Seleccioni l\'arxiu de la devolució a carregar')
                        
    _columns = {
        'info': fields.text('Informació', readonly=True),
        'arxiu': fields.binary('Arxiu', required=True),
        'filename': fields.char('Nom del fitxer', size=128),
        'state': fields.selection([('init', 'Inici'), ('end', 'Fi')], 'Estat'),
        'devolucions_creades_ids': fields.json('IDS devolucions creades'),
        'agrupar_devolucions': fields.boolean(
            'Devolucions agrupades',
            help=u'Permet generar una sola devolució per a totes les factures'
        )
    }

    _defaults = {
        'info': _get_default_info,
        'devolucions_creades_ids': lambda *a: False,
        'state': lambda *a: 'init',
        'agrupar_devolucions': lambda *a: False
    }


WizardCarregarDevolucio()
