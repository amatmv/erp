## -*- coding: utf-8 -*-
<%
    from datetime import datetime
    import time
    from osv.expression import OOQuery

    today = datetime.today().strftime('%d/%m/%Y %H:%M')

    pool = objects[0].pool
    inv_obj = pool.get('account.invoice')
    pol_obj = pool.get('giscedata.polissa')
    cursor = objects[0]._cr
    uid = user.id
    invoices_number = []

    for dev in objects:
        for linia in dev.linies_ids:
            invoices_number.append(linia.numfactura)

    inv_querier = OOQuery(inv_obj, cursor, uid)
    inv_query = inv_querier.select(['id']).where(
        [('number', 'in', invoices_number)]
    )

    cursor.execute(*inv_query)
    invs_vals = cursor.dictfetchall()

    inv_ids = []
    for inv_vals in invs_vals:
        inv_ids.append(inv_vals['id'])

    # no ens interessa obtenir el model base ja que no té un camp que
    # referencii a una polissa.
    inv_id_by_model = inv_obj.get_factura_ids_from_invoice_id(
        cursor, uid, inv_ids, {'no_account_invoice': True}
    )
    polissa_by_contract = {}
    # suposarem que el nombre de la factura mai fa col·lisio entre els
    # moduls de gas i electricitat. Per tant suposarem que sempre
    # retorna un id o de gas o de electricitat i si no en retorna llavors
    # es que no te polissa.
    for model_name, (facts_id, model) in inv_id_by_model.items():
        inv_querier = OOQuery(model, cursor, uid)
        inv_query = inv_querier.select(
            ['number', 'polissa_id.name'], only_active=False
        ).where(
            [('id', 'in', facts_id)]
        )
        cursor.execute(*inv_query)
        facts_vals = cursor.dictfetchall()
        for fact_vals in facts_vals:
            polissa_by_contract[fact_vals['number']] = fact_vals['polissa_id.name']

%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en"><html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<html>
    <head>
    	<style type="text/css">
        ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/devolucions_base/report/estils.css"/>
    </head>
    <body>
        %for dev in objects:
            <div class="titol">${_(u"Devolució de rebuts impagats")}</div>
            <div class="data">${_(u"Data:")} ${today}</div>
            <div style="clear: both;"></div>
            <div class="seccio mt_25" style="width: 70%;">
                <div class="titol_seccio">
                    ${_(u"Informació de la devolució")}
                </div>
                <div class="contingut_seccio">
                    <table>
                        <tr>
                            <td>${_(u"Referència")}</td>
                            <td class="field">${dev.name}</td>
                            <td>${_(u"Data devolució")}</td>
                            <td class="field">${dev.date}</td>
                        </tr>
                        <tr>
                            <td>${_(u"Compte comptable impagament")}</td>
                            <td class="field">${dev.pay_account_id.code} ${dev.pay_account_id.name}</td>
                            <td>${_(u"Diari")}</td>
                            <td class="field">${dev.pay_journal_id.name}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <table class="factures" cellspacing="0">
                <thead>
                    %for elem in range(1,4):
                        <!--
                            Espai superior del llistat. S'ha fet d'aquesta manera
                            per tal de que, al fer salts de pagina, es respecti
                            l'espaiat superior.
                        -->
                        <tr>
                            <td colspan="5"></td>
                        </tr>
                    %endfor
                    <tr>
                        <td colspan="6" class="titol_seccio">
                            ${_(u"Llistat de factures")}
                        </td>
                    </tr>
                    <tr>
                        <td class="titol_taula">${_(u"Factura")}</td>
                        <td class="titol_taula">${_(u"Pòlissa")}</td>
                        <td class="titol_taula">${_(u"Pagador")}</td>
                        <td class="titol_taula">${_(u"Compte de càrrec")}</td>
                        <td class="titol_taula">${_(u"Import")}</td>
                        <td class="titol_taula">${_(u"Motiu")}</td>
                    </tr>
                </thead>
                <tbody>
                    <%
                        comptador = 0
                        total = 0
                    %>
                    %for linia in dev.linies_ids:
                        %if comptador % 2 == 1:
                            <tr class="fons_gris">
                        %else:
                            <tr class="border_left_gris">
                        %endif
                            <% factnum = linia.numfactura %>
                            %if comptador == len(dev.linies_ids)-1:
                                <td class="field radius_bottom_left">
                            %else:
                                <td class="field">
                            %endif
                                ${factnum}
                            </td>
                            <td class="field">${polissa_by_contract.get(factnum, "-")}</td>
                            <td class="field">${linia.pagador}</td>
                            <td class="field">${linia.ccc_carrec}</td>
                            <td class="field">${formatLang(getattr(linia,'import'), digits=2)}</td>
                            %if comptador == len(dev.linies_ids)-1:
                                <td class="field radius_bottom_right">
                            %else:
                                <td class="field border_right_gris">
                            %endif
                                ${linia.reject_msg}
                            </td>
                        </tr>
                        <%
                            comptador += 1
                            total += getattr(linia,'import')
                        %>
                    %endfor
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text_right">${_(u"Total import")}</td>
                        <td>${formatLang(total, digits=2)}</td>
                    </tr>
                </tfoot>
            </table>
        %endfor
    </body>
</html>
