# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Restoring old res.configs values that have been duplicated and removing 
        the duplicates.
    ''')
    cursor.execute("""
        UPDATE res_config SET value = (
            SELECT value FROM res_config WHERE name = 'migration_devolucions_unpay'
        )
        WHERE name LIKE 'devolucions_unpay'
    """)

    cursor.execute("""
        DELETE FROM res_config WHERE name = 'migration_devolucions_unpay'
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
