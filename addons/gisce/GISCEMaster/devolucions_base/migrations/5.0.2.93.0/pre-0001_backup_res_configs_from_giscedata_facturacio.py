# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Duplicating res.config from giscedata_facturacio_devolucions to preserve it's values.
    ''')
    cursor.execute("""
        INSERT INTO res_config (name, value) (
            SELECT 'migration_' || name, value FROM res_config WHERE name='devolucions_unpay'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up
