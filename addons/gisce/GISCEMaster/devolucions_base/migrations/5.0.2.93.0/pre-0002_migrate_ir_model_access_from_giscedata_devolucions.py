# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Migrating access rules from giscedata_facturacio_devolucions to devolucions_base.
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module = 'devolucions_base'
        WHERE module = 'giscedata_facturacio_devolucions'
        AND name IN (
            'model_giscedata_facturacio_devolucio_linia',
            'model_giscedata_facturacio_devolucio',
            'model_giscedata_facturacio_devolucio_motiu',
            'model_wizard_carregar_devolucio',
            'config_unpay'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up