# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
import netsvc
from account.wizard.wizard_pay_invoice import wizard_pay_invoice
from destral.patch import PatchNewCursors


class TestsDevolucions(testing.OOTestCase):

    def _init_services(self, cursor, uid):
        if 'wizard.account.invoice.pay' not in netsvc.SERVICES:
            wizard_pay_invoice('account.invoice.pay')

    def test_desfer_devolucio_paga_factura_con_importe_linea(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        inv_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')

        dev_obj = self.openerp.pool.get('giscedata.facturacio.devolucio')
        dev_line_obj = self.openerp.pool.get(
            'giscedata.facturacio.devolucio.linia'
        )
        wz_dev_des_obj = self.openerp.pool.get('wizard.desfer.devolucions')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                self._init_services(cursor, uid)
                invoice_0001_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', 'invoice_0001'
                )[1]

                account_id = inv_obj.read(
                    cursor, uid, invoice_0001_id, ['account_id']
                )['account_id'][0]

                secondary_account_id = imd_obj.get_object_reference(
                    cursor, uid,
                    'devolucions_base', 'account_receivable_devolucions_0001'
                )[1]

                inv_line_obj.create(
                    cursor, uid, {
                        'name': 'New line',
                        'price_unit': 10.0,
                        'quantity': 1.0,
                        'invoice_id': invoice_0001_id,
                        'account_id': secondary_account_id,
                    }
                )

                wf_service = netsvc.LocalService('workflow')

                wf_service.trg_validate(
                    uid, 'account.invoice', invoice_0001_id, 'invoice_open', cursor
                )

                devolucio_id = dev_obj.create(
                    cursor, uid, {'name': 'Devolvedme los dineros'}
                )

                factura = inv_obj.browse(cursor, uid, invoice_0001_id)

                self.assertEqual(factura.state, 'open')

                self.assertTrue(factura.residual == 10)

                un_tal_iban = 'ES6501829817328389236512'

                vals = {
                    'devolucio_id': devolucio_id,
                    'numfactura': factura.number,
                    'pagador': factura.partner_id.id,  # factura.polissa_id.pagador.id,
                    'ccc_carrec': un_tal_iban,
                    'import': factura.amount_total,
                }

                dev_line_id = dev_line_obj.create(cursor, uid, vals)

                ctx = {'active_ids': [devolucio_id], 'active_id': devolucio_id}

                wiz_id = wz_dev_des_obj.create(cursor, uid, {}, context=ctx)

                wiz = wz_dev_des_obj.browse(cursor, uid, wiz_id, context=ctx)

                wiz.desfer_devolucions(context=ctx)

                factura = inv_obj.browse(cursor, uid, invoice_0001_id)

                self.assertEqual(factura.residual, 0)
                self.assertEqual(factura.state, 'paid')

    def test_desfer_devolucio_paga_factura_con_importe_linea_partial_payment(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        # fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        inv_obj = self.openerp.pool.get('account.invoice')
        inv_line_obj = self.openerp.pool.get('account.invoice.line')

        dev_obj = self.openerp.pool.get('giscedata.facturacio.devolucio')
        dev_line_obj = self.openerp.pool.get(
            'giscedata.facturacio.devolucio.linia'
        )
        wz_dev_des_obj = self.openerp.pool.get('wizard.desfer.devolucions')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with PatchNewCursors():
                self._init_services(cursor, uid)

                invoice_0001_id = imd_obj.get_object_reference(
                    cursor, uid, 'account', 'invoice_0001'
                )[1]

                account_id = inv_obj.read(
                    cursor, uid, invoice_0001_id, ['account_id']
                )['account_id'][0]

                secondary_account_id = imd_obj.get_object_reference(
                    cursor, uid,
                    'devolucions_base', 'account_receivable_devolucions_0001'
                )[1]

                inv_line_obj.create(
                    cursor, uid, {
                        'name': 'New line',
                        'price_unit': 10.0,
                        'quantity': 1.0,
                        'invoice_id': invoice_0001_id,
                        'account_id': secondary_account_id,
                    }
                )

                wf_service = netsvc.LocalService('workflow')

                wf_service.trg_validate(
                    uid, 'account.invoice', invoice_0001_id, 'invoice_open', cursor
                )

                devolucio_id = dev_obj.create(
                    cursor, uid, {'name': 'Devolvedme los dineros'}
                )

                factura = inv_obj.browse(cursor, uid, invoice_0001_id)

                self.assertEqual(factura.state, 'open')

                self.assertTrue(factura.residual == 10)

                un_tal_iban = 'ES6501829817328389236512'

                expected_residual = 2

                vals = {
                    'devolucio_id': devolucio_id,
                    'numfactura': factura.number,
                    'pagador': factura.partner_id.id,  # factura.polissa_id.pagador.id,
                    'ccc_carrec': un_tal_iban,
                    'import': (factura.residual - expected_residual),
                }

                dev_line_id = dev_line_obj.create(cursor, uid, vals)

                ctx = {'active_ids': [devolucio_id], 'active_id': devolucio_id}

                wiz_id = wz_dev_des_obj.create(cursor, uid, {}, context=ctx)

                wiz = wz_dev_des_obj.browse(cursor, uid, wiz_id, context=ctx)

                wiz.desfer_devolucions(context=ctx)

                factura = inv_obj.browse(cursor, uid, invoice_0001_id)

                self.assertEqual(factura.residual, expected_residual)
                self.assertEqual(factura.state, 'open')
