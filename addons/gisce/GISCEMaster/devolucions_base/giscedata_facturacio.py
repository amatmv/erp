# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime

from osv import fields, osv
from tools.translate import _
from account_invoice_base.account_invoice import amount_grouped
from oorq.decorators import job, create_jobs_group
from autoworker import AutoWorker
from osv.expression import OOQuery
from sql import For

class GiscedataFacturacioDevolucio(osv.osv):
    """Modelo de devoluciones de factura de energia
    """
    _name = 'giscedata.facturacio.devolucio'
    _order = "id desc"

    _devolucio_states_selection = [
        ('esborrany', 'Esborrany'),
        ('validar', 'Validar'),
        ('confirmat', 'Confirmat')
    ]

    def generate_despeses_devolucio(self, cursor, uid, invoice_ids, context=None):
        # search for a facturacio.factura linked with the
        # invoice, and generate extra lines
        inv_obj = self.pool.get('account.invoice')
        conf_obj = self.pool.get('res.config')
        devolucions_percent = float(conf_obj.get(
            cursor, uid, 'devolucions_despeses_percent', '0.0'
        ))

        extra_created = {}
        for model_name, (model_ids, model_obj) in inv_obj.get_factura_ids_from_invoice_id(
                cursor, uid, invoice_ids, context={'no_account_invoice': True}
        ).items():
            gisce_ = model_name.split('.')[0]
            extra_obj = self.pool.get('{}.facturacio.extra'.format(gisce_))
            imd_obj = self.pool.get('ir.model.data')
            prod_obj = self.pool.get('product.product')
            fact_ids = model_ids
            devolucions_product_id = imd_obj.get_object_reference(
                cursor, uid, '{}_facturacio_devolucions'.format(gisce_),
                'product_devolucions'
            )[1]
            uos_id = imd_obj.get_object_reference(
                cursor, uid, 'product', 'product_uom_unit'
            )[1]
            journal_id = imd_obj.get_object_reference(
                cursor, uid, '{}_facturacio'.format(gisce_),
                'facturacio_journal_{}'.format(
                    'energia' if gisce_ == 'giscedata' else 'gas')
            )[1]
            devolucions_product = prod_obj.browse(
                cursor, uid, devolucions_product_id
            )
            devolution_text = (
                    devolucions_product.description_sale or
                    devolucions_product.description or
                    _(u'Despeses devolució factura')
            )
            for fact in model_obj.browse(cursor, uid, fact_ids, context=context):
                extra_add = 0.0
                if devolucions_percent > 0.0:
                    inv = model_name != 'account.invoice' and fact.invoice_id or fact
                    total_invoice = amount_grouped(inv, 'amount_total')
                    extra_add = round(
                        total_invoice * (devolucions_percent / 100.0), 2
                    )
                vals = {
                    'name': _(u'{0} {1}').format(
                        devolution_text, fact.number
                    ),
                    'polissa_id': fact.polissa_id.id,
                    'product_id': devolucions_product_id,
                    'uos_id': uos_id,
                    'date_from': fact.date_invoice,
                    'date_to': fact.date_invoice,
                    'price_unit': devolucions_product.list_price + extra_add,
                    'quantity': 1,
                    'term': 1,
                    'account_id': devolucions_product.property_account_income.id,
                    'tax_ids': [
                        (6, 0, [t.id for t in devolucions_product.taxes_id])
                    ],
                    'journal_ids': [(6, 0, [journal_id])]
                }
                id_extra = extra_obj.create(cursor, uid, vals)
                extra_created.update({id_extra: fact.number})
            break
        return extra_created

    def get_grouped_inv_number(self, cr, uid, linia=None, numfactura=None, context=None):
        '''Parametre linia: és una línia de la devolucio
        Comprova que totes les línies de l'agrupació estan pagades.
        Retorna algun numero de factura que pertanyi a l'agrupacio
        en cas que estiguin totes pagades'''
        factura_trobada = None

        move_obj = self.pool.get('account.move')
        invoice_obj = self.pool.get('account.invoice')
        move_line_obj = self.pool.get('account.move.line')

        if not numfactura and linia:
            numfactura = linia.numfatcura

        params = [('ref', '=', numfactura)]
        moves = move_obj.search(cr, uid, params)

        # check that all of them are paid
        if moves:
            all_lines = []
            for move_line in move_obj.read(cr, uid, moves, ['line_id']):
                all_lines.extend(move_line['line_id'])
            for numfra in move_line_obj.read(cr, uid, all_lines, ['ref']):
                params = [('number', '=', numfra['ref'])]
                fra_id = invoice_obj.search(cr, uid, params)
                if fra_id: # AG move lines are not linked to an invoice
                    factura_trobada = numfra['ref']
                    estat = invoice_obj.read(cr, uid, fra_id, ['state'])
                    if not estat[0]['state'] == 'paid':
                        factura_trobada = None
                        break
        return factura_trobada

    def is_grouped_invoice(self, cr, uid, linia=None, numfactura=None, context=None):
        move_obj = self.pool.get('account.move')
        invoice_obj = self.pool.get('account.invoice')
        move_line_obj = self.pool.get('account.move.line')
        if not numfactura and linia:
            numfactura = linia.numfactura

        # Buscar els account_move_line relacionats amb aquesta factura
        params = [('ref', '=', numfactura)]
        move_ids = move_obj.search(cr, uid, params)
        line_ids = move_line_obj.search(cr, uid, [('move_id', 'in', move_ids)])

        # Si les invoices que inclou tenen el camp group_move_id,
        # és agrupada
        for line in move_line_obj.read(cr, uid, line_ids, ['invoice']):
            if line['invoice']:
                group = invoice_obj.read(cr, uid, line['invoice'][0],
                                         ['group_move_id'])['group_move_id']
                if group:
                    return True
        return False

    def action_validar(self, cr, uid, ids, context=None):
        """Condicions per passar una devolució a validar
        """
        notrobades = 0
        notrobades_ref = []
        work_async = context.get("work_async", False)
        for devolucio_info in self.read(cr, uid, ids, ['name', 'linies_ids']):
            if work_async:
                job_ids = []
                for linia_id in devolucio_info['linies_ids']:
                    job = self.valida_devolucio_line_async(cr, uid, linia_id, context=context)
                    job_ids.append(job.id)
                # Create a jobs_group to see the status of the operation
                create_jobs_group(
                    cr.dbname, uid,
                    _('Validar devolucio {0}: {1} linies.').format(devolucio_info['name'], len(job_ids)),
                    'devolucions.confirm_devolucio_lines', job_ids
                )
                aw = AutoWorker(queue='confirm_devolucio_lines')
                aw.work()
            else:
                for linia_id in devolucio_info['linies_ids']:
                    trobada, numfactura = self.valida_devolucio_line(cr, uid, linia_id, context=context)
                    if not trobada:
                        notrobades += 1
                        notrobades_ref += ['N:{}'.format(numfactura)]
                if notrobades:
                    raise osv.except_osv(
                        'Error', _(
                            'No es poden trobar {} factures:\n{}'
                        ).format(notrobades, ' '.join(notrobades_ref)))
                self.write(cr, uid, ids, {'state': 'validar'})
        return True

    @job(queue='confirm_devolucio_lines')
    def valida_devolucio_line_async(self, cr, uid, line_id, context=None):
        self.valida_devolucio_line(cr, uid, line_id, context=context)

    def get_invoice(self, cr, uid, numfactura, context=None):
        if context is None:
            context = {}
        invoice_obj = self.pool.get('account.invoice')
        res = invoice_obj.q(cr, uid).read(['id', 'number']).where([
            ('number', '=', numfactura),
            ('state', '=', 'paid')
        ])
        if res and len(res) == 1:
            return res[0]
        else:
            return {}

    def valida_devolucio_line(self, cr, uid, line_id, context=None):
        invoice_obj = self.pool.get('account.invoice')
        line_obj = self.pool.get("giscedata.facturacio.devolucio.linia")

        numfactura = None
        numfactura_devolucio = line_obj.read(cr, uid, line_id, ['numfactura'])['numfactura']
        # If it is a grouped invoice, pick one of them as 'numfactura'
        if self.is_grouped_invoice(cr, uid, numfactura=numfactura_devolucio):
            numfactura = self.get_grouped_inv_number(cr, uid, numfactura=numfactura_devolucio)

        if not numfactura:
            # provem amb el tros de número de factura
            ctx = context.copy()
            ctx.update({'action': 'validar'})
            invoice = self.get_invoice(
                cr, uid, numfactura_devolucio, context=ctx)
            if invoice:
                numfactura = invoice['number']
        if numfactura:
            line_obj.write(cr, uid, line_id, {'numfactura': numfactura, 'linia_validada': True})
        return numfactura, numfactura_devolucio

    def action_marcar_confirmada(self, cr, uid, ids, context=None):
        self.write(cr, uid, ids, {'state': 'confirmat'})

    def action_confirmar(self, cr, uid, ids, context=None):
        """Passos per passar una devolució a confirmat
        """
        if context is None:
            context = {}
        config_obj = self.pool.get('res.config')

        do_unpay = int(config_obj.get(cr, uid, 'devolucions_unpay', 0))
        move_pstate = int(config_obj.get(
            cr, uid, 'devolucions_unpay_move_pending_state', 0)
        )
        devolucio_moviment_unic = int(config_obj.get(
            cr, uid, 'devolucio_moviment_unic', 0)
        )
        q = self.pool.get("giscedata.facturacio.devolucio.linia").q(cr, uid)
        for devolucio_info in self.read(cr, uid, ids, ['name']):
            q_sql = q.select(
                ['id', 'linia_processada'], for_=For('UPDATE', nowait=False)
            ).where([
                ('devolucio_id', '=', devolucio_info['id']),
                ('linia_processada', '!=', True)
            ])
            cr.execute(*q_sql)
            if devolucio_moviment_unic:
                dlines_ids = [x['id'] for x in cr.dictfetchall()]
                self.process_devolucio_lines_moviment_unic(cr, uid, devolucio_info['id'], dlines_ids, move_pstate, context=context)
            elif context.get("work_async", False):
                job_ids = []
                for linia_inf in cr.dictfetchall():
                    job = self.process_devolucio_line_async(
                        cr, uid, devolucio_info['id'], linia_inf['id'], do_unpay, move_pstate,
                        context=None
                    )
                    job_ids.append(job.id)
                # Create a jobs_group to see the status of the operation
                create_jobs_group(
                    cr.dbname, uid,
                    _('Confirmar devolucio {0}: {1} linies.').format(devolucio_info['name'], len(job_ids)),
                    'devolucions.confirm_devolucio_lines', job_ids
                )
                aw = AutoWorker(queue='confirm_devolucio_lines')
                aw.work()
            else:
                for linia_inf in cr.dictfetchall():
                    self.process_devolucio_line(
                        cr, uid, devolucio_info['id'], linia_inf['id'], do_unpay, move_pstate,
                        context=None
                    )
        return True

    @job(queue='confirm_devolucio_lines')
    def process_devolucio_line_async(self, cr, uid, devolucio_id, line_id, do_unpay, move_pstate, context=None):
        self.process_devolucio_line(cr, uid, devolucio_id, line_id, do_unpay, move_pstate, context=context)

    def process_devolucio_line(self, cr, uid, devolucio_id, line_id, do_unpay, move_pstate, context=None):
        if context is None:
            context = {}
        # Tornem a comprovar que no estigui processada i de passo al haver fet
        # un altre select for update ens asegurem que ningu més podrà processar
        # aquesta linia fins que el worker/transacció hagi acabat
        q_sql = self.pool.get("giscedata.facturacio.devolucio.linia").q(cr, uid).select(
            ['linia_processada'], for_=For('UPDATE', nowait=False)
        ).where([
            ('id', '=', line_id),
        ])
        cr.execute(*q_sql)
        res = cr.dictfetchall()
        # Si ja està processada no fem res
        if res[0]['linia_processada']:
            return True

        invoice_obj = self.pool.get('account.invoice')
        config_obj = self.pool.get('res.config')
        period_obj = self.pool.get('account.period')
        line_obj = self.pool.get("giscedata.facturacio.devolucio.linia")

        devolucio = self.browse(cr, uid, devolucio_id)
        linia = line_obj.browse(cr, uid, line_id)

        invoice = self.get_invoice(cr, uid, linia.numfactura, context)
        if not invoice:
            raise osv.except_osv(
                "Error",
                _(u"No s'ha trobat cap factura amb numero {0} que estigui "
                  u"pagada. No es pot fer la devolució.").format(linia.numfactura)
            )
        config_var = 'despeses_devolucio'
        gisce_ = self.process_factura_devolucio_vals(cr, uid, linia, invoice['id'], context=context)
        if gisce_ == 'giscegas.facturacio.factura':
            config_var = 'giscegas_despeses_devolucio'

        if do_unpay:
            # Call the unpay function
            ctx = context.copy()
            ctx['date_p'] = devolucio.date
            ctx['unpay_move_pending_state'] = move_pstate
            period_id = period_obj.find(cr, uid, devolucio.date)[0]
            # Hack to avoid access to import due is a reserved
            # word
            devolucio_name = _('Devolució %s factura %s') % (
                devolucio.name, invoice['number']
            )
            amount = getattr(linia, 'import')
            journal_id = devolucio.pay_journal_id.id
            invoice_obj.unpay(
                cr, uid, [invoice['id']], amount,
                devolucio.pay_account_id.id, period_id,
                journal_id, context=ctx, name=devolucio_name
            )
        else:
            invoice_obj.undo_payment(cr, uid, [invoice['id']], context)

        despeses_devolucio = int(config_obj.get(cr, uid, config_var))
        if despeses_devolucio and not linia.avoid_expenses:
            self.generate_despeses_devolucio(cr, uid, invoice['id'], context)
        line_obj.write(cr, uid, line_id, {'linia_processada': True})

    def onchange_payment_mode(self, cursor, uid, ids, payment_mode_id):
        payment_mode_obj = self.pool.get('payment.mode')
        payment_mode = payment_mode_obj.browse(cursor, uid, payment_mode_id)
        res = {'value': {'pay_journal_id': payment_mode.journal and payment_mode.journal.id}}
        return res

    def process_devolucio_lines_moviment_unic(self, cr, uid, devolucio_id, linies_ids, move_pstate, context=None):
        if context is None:
            context = {}

        line_obj = self.pool.get("giscedata.facturacio.devolucio.linia")
        invoice_o = self.pool.get("account.invoice")
        config_o = self.pool.get('res.config')
        period_o = self.pool.get('account.period')
        devolucio = self.browse(cr, uid, devolucio_id)

        inv_dict = {}  # Diccionari a passar al unpay_multiple
        inv_ids = []  # IDS a passar al unpay_multiple
        fer_despeses_devolucio = []  # IDS que hauran de processar despeses de devolucio
        invoices_dlinies = {}
        for linia in line_obj.browse(cr, uid, linies_ids):
            # Obtenim el invoice_id
            numf = linia.numfactura
            inv_id = self.get_invoice(cr, uid, linia.numfactura, context)
            if not inv_id:
                raise osv.except_osv(
                    "Error",
                    _(u"No s'ha trobat la factura amb numero {0}.").format(numf)
                )
            inv_ids.append(inv_id['id'])
            # Guardem l'import a retornar
            inv_dict[inv_id['id']] = linia.read(['import'])[0]['import']
            invoices_dlinies[inv_id['id']] = linia
            # Si esta marcada, processem despeses devolucions
            if not linia.avoid_expenses:
                fer_despeses_devolucio.append(inv_id['id'])

        # Processem totes les lines de devolucio en un moviment únic
        ctx = context.copy()
        ctx['date_p'] = devolucio.date
        ctx['unpay_move_pending_state'] = move_pstate
        period_id = period_o.find(cr, uid, devolucio.date)[0]
        devolucio_name = _('Devolució {0}').format(devolucio.name)
        journal_id = devolucio.pay_journal_id.id
        invoice_o.unpay_multiple(
            cr, uid, inv_ids, inv_dict, devolucio.pay_account_id.id, period_id,
            journal_id, context=ctx, name=devolucio_name
        )

        for invoice_id in inv_ids:
            linia = invoices_dlinies[invoice_id]
            gisce_ = self.process_factura_devolucio_vals(cr, uid, linia, invoice_id, context=context)
            # Processem despeses devolucio
            if invoice_id in fer_despeses_devolucio:
                config_var = 'despeses_devolucio'
                if gisce_ == 'giscegas.facturacio.factura':
                    config_var = 'giscegas_despeses_devolucio'
                despeses_devolucio = int(config_o.get(cr, uid, config_var))
                if despeses_devolucio:
                    self.generate_despeses_devolucio(cr, uid, invoice_id, context)

        # Marquem totes les lineas com a processades
        line_obj.write(cr, uid, linies_ids, {'linia_processada': True})

    def get_factura_devolucio_vals(self, cursor, uid, linia, context=None):
        """
        :param cursor:
        :param uid:
        :param devolucio: browse record of giscedata.facturacio.devolucio.linia
        :param context:
        :return: Retorna els valors a escriure a una factura al carregar una
                 devolució
        """
        if linia:
            return {'devolucio_id': linia.devolucio_id.id}
        else:
            return None

    def process_factura_devolucio_vals(self, cr, uid, linia, invoice_id, context=None):
        if context is None:
            context = {}

        invoice_o = self.pool.get('account.invoice')
        fact_info = invoice_o.get_factura_ids_from_invoice_id(
            cr, uid, invoice_id, {'no_account_invoice': True}
        )
        if fact_info:
            fact_obj = fact_info.values()[0][1]
            f_ids = fact_info.values()[0][0]
            if f_ids:
                fact_vals = self.get_factura_devolucio_vals(
                    cr, uid, linia, context=context
                )
                fact_obj.write(cr, uid, f_ids, fact_vals)
            return fact_info.keys()[0]
        return False

    def _do_unpay(self, cursor, uid, ids, field_name, args, context=None):
        config_obj = self.pool.get('res.config')
        do_unpay = int(config_obj.get(cursor, uid, 'devolucions_unpay', 0))
        return dict.fromkeys(ids, do_unpay)

    def _get_devolucio_from_linia(self, cursor, uid, ids, context=None):
        """
        :param ids: ids of the devolution lines
        """
        query = '''
            SELECT distinct devolucio_id 
            FROM giscedata_facturacio_devolucio_linia
            WHERE id in %s
        '''
        cursor.execute(query, (tuple(ids),))
        return [x[0] for x in cursor.fetchall()]

    def desfer_devolucio(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        dev_line_obj = self.pool.get('giscedata.facturacio.devolucio.linia')
        dev_line_ids = dev_line_obj.search(
            cursor, uid, [('devolucio_id', 'in', ids)], context=context
        )

        res_msg = dev_line_obj.retornar_devolucio(
            cursor, uid, dev_line_ids, context=context
        )

        if res_msg != 'OK':
            return False, res_msg

        return True, res_msg

    def _check_linies_processades(self, cursor, uid, ids, name, args, context=None):
        res = dict.fromkeys(ids, False)
        line_obj = self.pool.get('giscedata.facturacio.devolucio.linia')
        for devolucio_vals in self.read(cursor, uid, ids, ['linies_ids', 'state']):
            devolucio_id = devolucio_vals['id']

            line_f = ['id']
            dmn_processed = [('linia_processada', '=', True), ('devolucio_id', '=', devolucio_id)]
            q = line_obj.q(cursor, uid).select(line_f).where(dmn_processed)
            cursor.execute(*q)
            line_processed = cursor.dictfetchall()
            nprocessed = len(line_processed)
            res[devolucio_id] = nprocessed

            # Comprovem si ja estan totes les linies processades,
            # si es aixi passem la devolucio a finalitzada
            if len(devolucio_vals['linies_ids']) > 0 and len(devolucio_vals['linies_ids']) == nprocessed and devolucio_vals['state'] == 'validar':
                self.write(cursor, uid, devolucio_id, {'state': 'confirmat'})
        return res

    def _check_linies_validades(self, cursor, uid, ids, name, args, context=None):
        res = dict.fromkeys(ids, False)
        line_obj = self.pool.get('giscedata.facturacio.devolucio.linia')
        for devolucio_vals in self.read(cursor, uid, ids, ['linies_ids', 'state']):
            devolucio_id = devolucio_vals['id']

            line_f = ['id']
            dmn_processed = [('linia_validada', '=', True), ('devolucio_id', '=', devolucio_id)]
            q = line_obj.q(cursor, uid).select(line_f).where(dmn_processed)
            cursor.execute(*q)
            line_processed = cursor.dictfetchall()
            nprocessed = len(line_processed)
            res[devolucio_id] = nprocessed

            # Comprovem si ja estan totes les linies confirmades,
            # si es aixi passem la devolucio a finalitzada
            if len(devolucio_vals['linies_ids']) > 0 and len(devolucio_vals['linies_ids']) == nprocessed and devolucio_vals['state'] == 'esborrany':
                self.write(cursor, uid, devolucio_id, {'state': 'validar'})
        return res

    _columns = {
        'name': fields.char(
            'Referencia', size=64, required=True, readonly=False
        ),
        'create_date': fields.datetime(
            'Data creació', required=True, readonly=True
        ),
        'date': fields.date('Data devolució'),
        'linies_ids': fields.one2many(
            'giscedata.facturacio.devolucio.linia', 'devolucio_id', 'Linies',
            required=False
        ),
        'state': fields.selection(
            _devolucio_states_selection, 'Estado', required=True, readonly=True
        ),
        'do_unpay': fields.function(
            _do_unpay, type='boolean', method=True, store=True,
            string='Unpay method'
        ),
        'pay_account_id': fields.many2one(
            'account.account', 'Compte comptable impagament',
        ),
        'pay_journal_id': fields.many2one(
            'account.journal',
            'Diari',
        ),
        'payment_mode_id': fields.many2one(
            'payment.mode',
            'Mode de pagament'
        ),
        'num_of_rows': fields.integer(
            'Número de línies', readonly=True
        ),
        'total_devolution': fields.float(
            'Import total devolució', readonly=True, digits=(16,2)
        ),
        'linies_processades': fields.function(
            _check_linies_processades, method=True,
            string='Linies devolució processades', type="integer",
            store={
                'giscedata.facturacio.devolucio': (lambda self, cr, uid, ids, c=None: ids, ['linies_ids'], 10),
                'giscedata.facturacio.devolucio.linia': (_get_devolucio_from_linia, ['linia_processada'], 10)
            }
        ),
        'linies_validades': fields.function(
            _check_linies_validades, method=True,
            string='Linies devolució confirmades', type="integer",
            store={
                'giscedata.facturacio.devolucio': (lambda self, cr, uid, ids, c=None: ids, ['linies_ids'], 10),
                'giscedata.facturacio.devolucio.linia': (_get_devolucio_from_linia, ['linia_validada'], 10)
            }
        )
    }

    _defaults = {
        'state': lambda *a: 'esborrany',
        'date': lambda *a: datetime.now().strftime('%Y-%m-%d'),
    }

    def unlink(self, cursor, uid, ids, context=None):
        for devolucio in self.browse(cursor, uid, ids, context):
            if devolucio.state != 'esborrany':
                raise osv.except_osv(u'Error', u'No es poden eliminar '
                                     u'devolucions que no estiguin en estat '
                                     u'"esborrany"')
        return super(GiscedataFacturacioDevolucio,
                     self).unlink(cursor, uid, ids, context)


GiscedataFacturacioDevolucio()


class GiscedataFacturacioDevolucioLinia(osv.osv):
    """Linies d'una devolució
    """
    _name = 'giscedata.facturacio.devolucio.linia'

    def create(self, cursor, uid, vals, context=None):
        devolucio_linia_id = super(
            GiscedataFacturacioDevolucioLinia, self
        ).create(cursor, uid, vals, context=context)
        if vals.get('reject_code') and 'avoid_expenses' not in vals:
            expenses_value = self._get_pay_no_pay_by_motiu(
                cursor, uid, devolucio_linia_id, context=context
            )
            if expenses_value:
                self.write(
                    cursor, uid, devolucio_linia_id,
                    {'avoid_expenses': expenses_value}, context=context
                )

        return devolucio_linia_id

    def onchange_codi_motiu(self, cursor, uid, ids, codi_motiu, context=None):
        """
        Update bool field to pay or not pay devolucions
        """
        if context is None:
            context = {}
        res = {'value': {}}

        motiu_obj = self.pool.get('giscedata.facturacio.devolucio.motiu')
        avoid_taxes = motiu_obj.avoid_expenses(
            cursor, uid, codi_motiu, context=context
        )
        if avoid_taxes:
            res['value'].update({'avoid_expenses': avoid_taxes})
        return res

    def _get_pay_no_pay_by_motiu(self, cursor, uid, ids, context=None):
        motiu_obj = self.pool.get('giscedata.facturacio.devolucio.motiu')
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        reject_code = self.read(
            cursor, uid, ids, ['reject_code'], context=context
        )['reject_code']

        avoid_taxes = motiu_obj.avoid_expenses(
            cursor, uid, reject_code, context=context
        )
        return avoid_taxes

    def _reason_msg_from_code(self, cursor, uid, ids, name, arg, context=None):
        res = {}
        for id in ids:
            line = self.browse(cursor, uid, id)
            code = line.reject_code

            if not code:
                res[id] = "Desconocido"
            else:
                motiu_obj = self.pool.get(
                    'giscedata.facturacio.devolucio.motiu'
                )

                motiu_id = motiu_obj.search(
                    cursor, uid, [('code', '=', code)], context=context
                )

                if motiu_id:
                    if isinstance(motiu_id, (list, tuple)):
                        motiu_id = motiu_id[0]
                    motiu_obj = self.pool.get(
                        'giscedata.facturacio.devolucio.motiu'
                    )
                    reason = motiu_obj.read(
                        cursor, uid, motiu_id, ['description'],
                        context=context
                    )['description']
                    res[id] = "(%s) %s" % (code, reason)
                else:
                    res[id] = "(%s) Desconocido" % code
        return res

    def retornar_devolucio(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        lines_info = self.read(
            cursor, uid, ids,
            ['numfactura', 'import', 'date'],
            context=context
        )

        inv_obj = self.pool.get('account.invoice')

        res_msg = ''
        factures_no_trobades = []
        factures_dobles = []

        for line in lines_info:
            num_factura = line['numfactura']
            importe = line['import']
            # journal_id = line['pay_journal_id']

            fact_id = inv_obj.search(
                cursor, uid,
                [('number', '=', num_factura), ('state', '=', 'open')]  # todo open?
            )

            if len(fact_id) == 1:
                fact_id = fact_id[0]

                import netsvc

                service = netsvc.LocalService('wizard.account.invoice.pay')

                dict_pointer_ref = {'id': fact_id, 'form': {}}

                res_service_init_execute_dict = service.execute(
                    cursor.dbname, uid,
                    dict_pointer_ref,
                    state='init',
                    context=context
                )

                send_datas = {
                    'id': fact_id,
                    'form': res_service_init_execute_dict['datas']
                }

                journal_obj = self.pool.get('account.journal')

                search_params = [('type', '=', 'cash')]
                journal_id = journal_obj.search(
                    cursor, uid, search_params, context=context
                )[0]

                send_datas['form'].update({
                    'amount': importe,
                    'journal_id': journal_id,
                    'comment': "Desfer devolucio {}".format(num_factura),
                    'name': "Desfer devolucio {}".format(num_factura)
                })

                res_service_init_execute_dict = service.execute(
                    cursor.dbname, uid,
                    send_datas, 'reconcile',
                    context=context
                )

            else:
                if not fact_id:
                    factures_no_trobades.append(num_factura)
                else:
                    factures_dobles.append(num_factura)

        if not factures_no_trobades and not factures_dobles:
            res_msg = 'OK'
        else:
            if factures_no_trobades:
                res_msg = _('No s\'han trobat les factures: {}\n\n').format(
                   ', '.join(factures_no_trobades)
                )
            if factures_dobles:
                res_msg = _(
                    '{}Hi ha varies factures amb el mateix numero{}\n\n'
                ).format(res_msg, ', '.join(factures_dobles))

        return res_msg

    _columns = {
        'numfactura': fields.char('Factura', size=64, required=True,
                                  readonly=False),
        'pagador': fields.char('Pagador', size=128, required=True,
                               readonly=False),
        'ccc_carrec': fields.char('Compte de càrrec', size=25, required=True,
                                  readonly=False),
        'import': fields.float('Import', digits=(16, 2), required=True),
        'reject_code': fields.char('Codi motiu', size=4, required=False),
        'devolucio_id': fields.many2one(
            'giscedata.facturacio.devolucio', 'Devolució', required=True,
            ondelete='cascade'
        ),
        'reject_msg': fields.function(
            _reason_msg_from_code, method=True, type='char', size=128,
            string='Motiu', readonly=True,
        ),
        'avoid_expenses': fields.boolean(
            'No cobrar despeses', help=u"Estarà actiu quan s'hagi d'ometre la "
                                       u"generació de despeses de la devolució"
        ),
        'linia_processada': fields.boolean("Linia devolució realitzada"),
        'linia_validada': fields.boolean("Linia devolució validada")
    }

    _defaults = {
        'avoid_expenses': lambda *a: False,
        'linia_processada': lambda *a: False,
    }


GiscedataFacturacioDevolucioLinia()


class GiscedataFacturacioDevolucioMotiu(osv.osv):
    _name = 'giscedata.facturacio.devolucio.motiu'
    _description = "Motiu de devolucio"

    def avoid_expenses(self, cursor, uid, code_motiu=None, context=None):
        if context is None:
            context = {}
        if code_motiu:
            motiu_id = self.search(
                cursor, uid, [('code', '=', code_motiu)], context=context
            )

            if motiu_id:
                if isinstance(motiu_id, (list, tuple)):
                    motiu_id = motiu_id[0]

                no_pay_taxes = self.read(
                    cursor, uid, motiu_id, ['avoid_expenses'], context=context
                )['avoid_expenses']

                return no_pay_taxes

        return False

    _columns = {
        'code': fields.char('Codi motiu', size=4, required=True),
        'description': fields.char('Motiu', size=128, required=False),
        'avoid_expenses': fields.boolean('No cobrar despeses',
                                         help=u"Estarà actiu quan s'hagi "
                                              u"d'ometre la generació de "
                                              u"despeses de la devolució",
                                         ),

    }

    _defaults = {
        'avoid_expenses': lambda *a: False,
        'description': lambda *a: ''
    }

GiscedataFacturacioDevolucioMotiu()
