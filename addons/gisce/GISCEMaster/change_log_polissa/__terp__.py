# -*- coding: utf-8 -*-
{
    "name": "Change log for polissa",
    "description": """Audit changes for polissa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules",
    "depends":[
        "base",
        "change_log",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
