# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataPolissaTarifa(osv.osv):

    _name = 'giscedata.polissa.tarifa'
    _inherit = 'giscedata.polissa.tarifa'

    def get_periode_ts(self, cursor, uid, ids, dia, hora, estacio,
                       context=None):
        """Ens retorna el periode segons el dia que li passem.
        """
        if not context:
            context = {}

        festiu_obj = self.pool.get('giscedata.dfestius')

        six_periods = context.get('six_periods', False)
        six_period_map = {'P1': 'P4',
                          'P2': 'P5',
                          'P3': 'P6'}

        res = super(GiscedataPolissaTarifa,
                    self).get_periode_ts(cursor, uid, ids, dia, hora,
                                         estacio, context=context)

        tarifa = self.browse(cursor, uid, ids[0])
        if (six_periods
            and res in six_period_map.keys()
            and festiu_obj.is_festiu(cursor, uid, dia)
            and not tarifa.name.startswith('2.')):
            res = six_period_map[res]
        return res

GiscedataPolissaTarifa()
