# -*- coding: utf-8 -*-
import pooler

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    seq_obj = pool.get('ir.sequence')
    migrate_seqs = [
        ('seq_giscedata_factura_contractes', 'seq_factures_conceptes'),
    ]
    read_fields = ['number_next', 'padding', 'number_increment', 'prefix',
                   'suffix']
    for old_xmlid, new_xmlid in migrate_seqs:
        old_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio_conceptes',
                                 old_xmlid)
        seq_old_id = imd_obj.read(cursor, uid, old_id, ['res_id'])['res_id']
        vals = seq_obj.read(cursor, uid, seq_old_id, read_fields)
        del vals['id']
        new_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio_conceptes',
                                 new_xmlid)
        seq_new_id = imd_obj.read(cursor, uid, new_id, ['res_id'])['res_id']
        seq_obj.write(cursor, uid, [seq_new_id], vals)
        # Eliminem l'antic de ir_model_data per evitar que es processi al final
        imd_obj.unlink(cursor, uid, [old_id])
