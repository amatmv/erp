# -*- coding: utf-8 -*-
import pooler
from tools import convert_xml_import
from cStringIO import StringIO

XML = """<?xml version="1.0"?>
<openerp>
    <data>
        <record id="action_facturacio_conceptes_factures_old_tree" model="ir.actions.act_window">
            <field name="name">Factures conceptes (Antigues)</field>
            <field name="res_model">account.invoice</field>
            <field name="view_type">form</field>
            <field name="view_mode">tree,form</field>
            <field name="context">{'type':'out_invoice'}</field>
            <field name="domain">[('origin', '=', 'CONTRACT_ERP4')]</field>
        </record>
        <menuitem id="menu_facturacio_conceptes_factures_old" action="action_facturacio_conceptes_factures_old_tree"/>
    </data>
</openerp>"""

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    menu_obj = pool.get('ir.ui.menu')
    # Busquem totes les factures
    cursor.execute("""select id from account_invoice where series_id in
                      (select id from account_invoice_series where
                       name ilike '%CONTRACT%')""")
    ids = [a[0] for a in cursor.fetchall()]
    if ids:
        pool.get('account.invoice').write(cursor, uid, ids,
                                          {'origin': 'CONTRACT_ERP4'})
        fp = StringIO(XML)
        idref = {}
        convert_xml_import(cursor, 'giscedata_facturacio_accessos', fp, idref,
                           mode='update')
        imd_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio_conceptes',
                                 'menu_facturacio_conceptes')
        parent_id = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
        menu_obj.write(cursor, uid,
                       idref['menu_facturacio_conceptes_factures_old'],
                       {'parent_id': parent_id})
