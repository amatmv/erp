# -*- coding: utf-8 -*-
import pooler

def migrate(cursor, installed_version):
    """Busquem tots els productes al mòdul antic i els posem al nou
    """
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    search_params = [('module', '=', 'giscedata_contractes_acces'),
                     '|',
                     ('model', 'ilike', 'product%'),
                     ('model', 'ilike', 'ir.sequence%')]
    ids = imd_obj.search(cursor, uid, search_params)
    imd_obj.write(cursor, uid, ids,
                  {'module': 'giscedata_facturacio_conceptes'})
