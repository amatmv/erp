# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
#   <misern@gisce.net>, 2012.
# Eduard Carreras <ecarreras@gisce.net>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2012-04-25 16:10:46+0000\n"
"PO-Revision-Date: 2012-09-20 08:01+0000\n"
"Last-Translator: Eduard Carreras <ecarreras@gisce.net>\n"
"Language-Team: Spanish (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: es_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_supervisio_bt_product_template
msgid "Cuota de supervisión de instalaciones cedidas BT"
msgstr "Cuota de supervisión de instalaciones cedidas BT"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "TOTAL:"
msgstr "TOTAL:"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "BASE IMPONIBLE:"
msgstr "BASE IMPONIBLE:"

#. module: giscedata_facturacio_conceptes
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_at2_product_template
msgid "Cuota de verificación 72,5 kV < V"
msgstr "Cuota de verificación 72,5 kV < V"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Nombre-name}"
msgstr "$F{Nombre-name}"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_icp_product_template
msgid "Cuota de verificación ICP"
msgstr "Cuota de verificación ICP"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "IMPORT"
msgstr "IMPORTE"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Calle-street}"
msgstr "$F{Calle-street}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Descripcion_impuesto-name}"
msgstr "$F{Descripcion_impuesto-name}"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_bt_product_template
msgid "Cuota de verificación BT"
msgstr "Cuota de verificación BT"

#. module: giscedata_facturacio_conceptes
#: model:ir.module.module,shortdesc:giscedata_facturacio_conceptes.module_meta_information
msgid "Facturació Conctractes d'accés"
msgstr "Facturación Conctratos de acceso"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Numero_factura-number}"
msgstr "$F{Numero_factura-number}"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_supervisio_at1_product_template
msgid "Cuota de supervisión de instalaciones cedidas 72,5 kV < V"
msgstr "Cuota de supervisión de instalaciones cedidas 72,5 kV < V"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_ext_at1_product_template
msgid "Cuota de extensión 36 < V <= 72,5 kV"
msgstr "Cuota de extensión 36 < V <= 72,5 kV"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_ext_at2_product_template
msgid "Cuota de extensión 72,5 < V"
msgstr "Cuota de extensión 72,5 < V"

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_quotes_acces
msgid "Cuotas de acceso"
msgstr "Cuotas de acceso"

#. module: giscedata_facturacio_conceptes
#: constraint:product.category:0
msgid "Error ! You can not create recursive categories."
msgstr "Error ! You can not create recursive categories."

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_quotes_supervisio
msgid "Derechos supervisión instalaciones cedidas"
msgstr "Derechos supervisión instalaciones cedidas"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Fecha_factura-date_invoice}"
msgstr "$F{Fecha_factura-date_invoice}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "N. COMPTE:"
msgstr "N. CUENTA:"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Polissa-name}"
msgstr "$F{Polissa-name}"

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_quotes_enganche
msgid "Derechos de enganche"
msgstr "Derechos de enganche"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{CIF_NIF-vat}"
msgstr "$F{CIF_NIF-vat}"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_actuacions_mic_at2_product_template
msgid "Cuota de Actuación equipo medida y control 72,5 kV < V"
msgstr "Cuota de Actuación equipo medida y control 72,5 kV < V"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_supervisio_at0_product_template
msgid "Cuota de supervisión de instalaciones cedidas 36 kV < V <= 72,5 kV"
msgstr "Cuota de supervisión de instalaciones cedidas 36 kV < V <= 72,5 kV"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Subtotal_sin_imp.-price_subtotal}"
msgstr "$F{Subtotal_sin_imp.-price_subtotal}"

#. module: giscedata_facturacio_conceptes
#: constraint:product.template:0
msgid "Error: UOS must be in a different category than the UOM"
msgstr "Error: UOS must be in a different category than the UOM"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_enganche_at2_product_template
msgid "Cuota de enganche 72,5 kV < V"
msgstr "Cuota de enganche 72,5 kV < V"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Linies_Energia-linia_ids/Factura_Enegia-factura_id/id"
msgstr "Linies_Energia-linia_ids/Factura_Enegia-factura_id/id"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"Factura_Comptable-invoice_id/Direccion_factura-address_invoice_id/Poblacio-"
"id_poblacio/Poblacio-name"
msgstr "Factura_Comptable-invoice_id/Direccion_factura-address_invoice_id/Poblacio-id_poblacio/Poblacio-name"

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_quotes_actuacions_mic
msgid "Derechos por actuaciones en los equipos de medida y control"
msgstr "Derechos por actuaciones en los equipos de medida y control"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"/data/record/Factura_Comptable-invoice_id/Direccion_factura-"
"address_invoice_id/Calle-street"
msgstr "/data/record/Factura_Comptable-invoice_id/Direccion_factura-address_invoice_id/Calle-street"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "<b>NUM. FACTURA</b>"
msgstr "<b>NUM. FACTURA</b>"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Polissa-polissa_id/Polissa-name"
msgstr "Polissa-polissa_id/Polissa-name"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Base-base}"
msgstr "$F{Base-base}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "CONCEPTE"
msgstr "CONCEPTO"

#. module: giscedata_facturacio_conceptes
#: model:ir.actions.act_window,name:giscedata_facturacio_conceptes.act_factures_conceptes_per_polissa
msgid "Factures de conceptes"
msgstr "Facturas de conceptos"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_comptador_tri_product_template
msgid "Cuota de verificación contador trifásico"
msgstr "Cuota de verificación contador trifásico"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"Linies_Energia-linia_ids/Linia_Comptable-invoice_line_id/Descripcion-name"
msgstr "Linies_Energia-linia_ids/Linia_Comptable-invoice_line_id/Descripcion-name"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_ext_bt_product_template
msgid "Cuota de extensión BT"
msgstr "Cuota de extensión BT"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "<b>TOTAL FACTURA:</b>"
msgstr "<b>TOTAL FACTURA:</b>"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Factura_Comptable-invoice_id/Total-amount_total"
msgstr "Factura_Comptable-invoice_id/Total-amount_total"

#. module: giscedata_facturacio_conceptes
#: constraint:product.template:0
msgid ""
"Error: The default UOM and the purchase UOM must be in the same category."
msgstr "Error: The default UOM and the purchase UOM must be in the same category."

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_at0_product_template
msgid "Cuota de verificación V <= 36 kV"
msgstr "Cuota de verificación V <= 36 kV"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Importe-amount}"
msgstr "$F{Importe-amount}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"Factura_Comptable-invoice_id/Lineas_de_impuestos-tax_line/Importe-amount"
msgstr "Factura_Comptable-invoice_id/Lineas_de_impuestos-tax_line/Importe-amount"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_actuacions_mic_at1_product_template
msgid "Cuota de Actuación equipo medida y control 36 kV < V <= 72,5 kV"
msgstr "Cuota de Actuación equipo medida y control 36 kV < V <= 72,5 kV"

#. module: giscedata_facturacio_conceptes
#: model:account.journal,name:giscedata_facturacio_conceptes.journal_soller_contractacio_distri_drets
#: model:ir.actions.act_window,name:giscedata_facturacio_conceptes.action_facturacio_conceptes_factures_tree
#: model:ir.ui.menu,name:giscedata_facturacio_conceptes.menu_facturacio_conceptes_factures
msgid "Factures conceptes"
msgstr "Facturas conceptos"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_acc_at1_product_template
msgid "Cuota de acceso 36 < V <= 72,5 kV"
msgstr "Cuota de acceso 36 < V <= 72,5 kV"

#. module: giscedata_facturacio_conceptes
#: model:ir.module.module,description:giscedata_facturacio_conceptes.module_meta_information
msgid ""
"\n"
"  * Productes per poder facturar conceptes (Drets d'accés, augments...)\n"
msgstr "\n  * Productes per poder facturar conceptes (Drets d'accés, augments...)\n"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_at1_product_template
msgid "Cuota de verificación 36 kV < V <= 72,5 kV"
msgstr "Cuota de verificación 36 kV < V <= 72,5 kV"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "<b>NUM. ABONAT</b>"
msgstr "<b>NUM. ABONAT</b>"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "NIF:"
msgstr "NIF:"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Factura_Comptable-invoice_id/Lineas_de_impuestos-tax_line/Base-base"
msgstr "Factura_Comptable-invoice_id/Lineas_de_impuestos-tax_line/Base-base"

#. module: giscedata_facturacio_conceptes
#: model:ir.actions.act_window,name:giscedata_facturacio_conceptes.action_facturacio_conceptes_factures_obertes_tree
#: model:ir.ui.menu,name:giscedata_facturacio_conceptes.menu_facturacio_conceptes_factures_obertes
msgid "Factures conceptes obertes"
msgstr "Facturas conceptos abiertas"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_trafo_product_template
msgid "Cuota de verificación Trafos"
msgstr "Cuota de verificación Trafos"

#. module: giscedata_facturacio_conceptes
#: model:ir.actions.report.xml,name:giscedata_facturacio_conceptes.report_factura_conceptes
msgid "Factura Conceptes"
msgstr "Factura Conceptos"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_acc_at0_product_template
msgid "Cuota de acceso V <= 36"
msgstr "Cuota de acceso V <= 36"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_acc_at2_product_template
msgid "Cuota de acceso 72,5 < V"
msgstr "Cuota de acceso 72,5 < V"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"/data/record/Factura_Comptable-invoice_id/Empresa-partner_id/Nombre-name"
msgstr "/data/record/Factura_Comptable-invoice_id/Empresa-partner_id/Nombre-name"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Factura_Comptable-invoice_id/Fecha_factura-date_invoice"
msgstr "Factura_Comptable-invoice_id/Fecha_factura-date_invoice"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"/data/record/Factura_Comptable-invoice_id/Empresa-partner_id/CIF_NIF-vat"
msgstr "/data/record/Factura_Comptable-invoice_id/Empresa-partner_id/CIF_NIF-vat"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Descripcion-name}"
msgstr "$F{Descripcion-name}"

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_quotes_verificacio
msgid "Derechos de verificación"
msgstr "Derechos de verificación"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_enganche_at0_product_template
msgid "Cuota de enganche V <= 36 kV"
msgstr "Cuota de enganche V <= 36 kV"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Poblacio-name}"
msgstr "$F{Poblacio-name}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{C.P.-zip}"
msgstr "$F{C.P.-zip}"

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_quotes_extencio
msgid "Cuotas d'extensión"
msgstr "Cuotas de extensión"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_supervisio_at_linia_product_template
msgid "Cuota de supervisión de instalaciones cedidas V < 36 kV (Línea)"
msgstr "Cuota de supervisión de instalaciones cedidas V < 36 kV (Línea)"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_enganche_at1_product_template
msgid "Cuota de enganche 36 kV < V <= 72,5 kV"
msgstr "Cuota de enganche 36 kV < V <= 72,5 kV"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_actuacions_mic_at0_product_template
msgid "Cuota de Actuación equipo medida y control V <= 36 kV"
msgstr "Cuota de Actuación equipo medida y control V <= 36 kV"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Factura_Comptable-invoice_id/Numero_factura-number"
msgstr "Factura_Comptable-invoice_id/Numero_factura-number"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_verificacio_comptador_mono_product_template
msgid "Cuota de verificación contador monofásico"
msgstr "Cuota de verificación contador monofásico"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_acc_bt_product_template
msgid "Cuota de acceso BT"
msgstr "Cuota de acceso BT"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "Factura_Comptable-invoice_id/Base-amount_untaxed"
msgstr "Factura_Comptable-invoice_id/Base-amount_untaxed"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "CUPS:"
msgstr "CUPS:"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"/data/record/Factura_Comptable-invoice_id/Direccion_factura-"
"address_invoice_id/Ciudad-city"
msgstr "/data/record/Factura_Comptable-invoice_id/Direccion_factura-address_invoice_id/Ciudad-city"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Base-amount_untaxed}"
msgstr "$F{Base-amount_untaxed}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{id}"
msgstr "$F{id}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"Linies_Energia-linia_ids/Linia_Comptable-"
"invoice_line_id/Subtotal_sin_imp.-price_subtotal"
msgstr "Linies_Energia-linia_ids/Linia_Comptable-invoice_line_id/Subtotal_sin_imp.-price_subtotal"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_actuacions_mic_bt_product_template
msgid "Cuota de Actuación equipo medida y control BT"
msgstr "Cuota de Actuación equipo medida y control BT"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_enganche_bt_product_template
msgid "Cuota de enganche BT"
msgstr "Cuota de enganche BT"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_supervisio_at_ct_product_template
msgid "Cuota de supervisión de instalaciones cedidas V < 36 kV (CT)"
msgstr "Cuota de supervisión de instalaciones cedidas V < 36 kV (CT)"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"Factura_Comptable-invoice_id/Cuenta_bancaria-partner_bank/Numero_de_cuenta-"
"acc_number"
msgstr "Factura_Comptable-invoice_id/Cuenta_bancaria-partner_bank/Numero_de_cuenta-acc_number"

#. module: giscedata_facturacio_conceptes
#: model:product.template,name:giscedata_facturacio_conceptes.acc_quota_ext_at0_product_template
msgid "Cuota de extensión V <= 36 kV"
msgstr "Cuota de extensión V <= 36 kV"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Numero_de_cuenta-acc_number}"
msgstr "$F{Numero_de_cuenta-acc_number}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "<b>DATA FACTURA</b>"
msgstr "<b>DATA FACTURA</b>"

#. module: giscedata_facturacio_conceptes
#: model:ir.ui.menu,name:giscedata_facturacio_conceptes.menu_facturacio_conceptes
msgid "Conceptes"
msgstr "Conceptos"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"Factura_Comptable-invoice_id/Lineas_de_impuestos-tax_line"
"/Descripcion_impuesto-name"
msgstr "Factura_Comptable-invoice_id/Lineas_de_impuestos-tax_line/Descripcion_impuesto-name"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{CUPS-name}"
msgstr "$F{CUPS-name}"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid ""
"/data/record/Factura_Comptable-invoice_id/Direccion_factura-"
"address_invoice_id/C.P.-zip"
msgstr "/data/record/Factura_Comptable-invoice_id/Direccion_factura-address_invoice_id/C.P.-zip"

#. module: giscedata_facturacio_conceptes
#: constraint:product.product:0
msgid "Error: Invalid ean code"
msgstr "Error: Invalid ean code"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "$F{Total-amount_total}"
msgstr "$F{Total-amount_total}"

#. module: giscedata_facturacio_conceptes
#: model:product.category,name:giscedata_facturacio_conceptes.categ_drets_escomesa
msgid "Derechos de acometida y demás actuaciones"
msgstr "Derechos de acometida y demás actuaciones"

#. module: giscedata_facturacio_conceptes
#: rml:giscedata.facturacio.factura_conceptes:0
msgid "/data/record/CUPS-cups_id/CUPS-name"
msgstr "/data/record/CUPS-cups_id/CUPS-name"
