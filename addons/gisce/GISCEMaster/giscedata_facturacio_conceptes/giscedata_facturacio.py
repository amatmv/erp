# -*- coding: utf-8 -*-
from osv import osv
import time
class GiscedataFacturacioFactura(osv.osv):
    """Sobreescrivim per obtenir el diari en conceptes segons el context.
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def default_journal_id(self, cursor, uid, context):
        """Gets the default journal.
        """
        if not context:
            context = {}
        res = self.pool.get('account.invoice')._get_journal(cursor, uid,
                                                            context)
        if 'journal_code' in context:
            journal_code = context['journal_code']
            journal_obj = self.pool.get('account.journal')
            jor = journal_obj.search(cursor, uid, [('code', '=', journal_code)],
                                     limit=1)
            if jor:
                res = jor[0]
        return res

    def onchange_polissa(self, cursor, uid, ids, polissa_id, type_,
                         context=None):
        """Sobreescrivim per tal de fer la factura de drets d'access al titular.
        """
        if not context:
            context = {}
        if 'type' in context and not type_:
            type_ = context['type']
        # Obtenim els valors per defecte de la classe 'pare'
        res = super(GiscedataFacturacioFactura,
                    self).onchange_polissa(cursor, uid, ids, polissa_id, type_,
                                           context)
        # Si el tipus de diari és el de conceptes posem el pagador el titular.
        if context.get('journal_code', '') == 'CONCEPTES':
            polissa_obj = self.pool.get('giscedata.polissa')
            facturador_obj = self.pool.get('giscedata.facturacio.facturador')
            polissa =  polissa_obj.browse(cursor, uid, polissa_id)
            partner_id = polissa.titular.id
            upd = super(GiscedataFacturacioFactura,
                        self).onchange_partner_id(cursor, uid, ids, type_,
                                                  partner_id)
            upd = upd.get('value', {})
            upd['partner_id'] = partner_id
            date = time.strftime('%Y-%m-%d')
            upd['date_boe'] = facturador_obj.get_data_boe(cursor, uid, date)
            res['value'].update(upd)
        return res

    _defaults = {
        'journal_id': default_journal_id,
    }
GiscedataFacturacioFactura()
