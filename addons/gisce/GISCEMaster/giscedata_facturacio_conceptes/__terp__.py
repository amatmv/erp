# -*- coding: utf-8 -*-
{
    "name": "Facturació Conctractes d'accés",
    "description": """
  * Productes per poder facturar conceptes (Drets d'accés, augments...)
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa",
        "product",
        "giscedata_facturacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_conceptes_data.xml",
        "giscedata_facturacio_conceptes_view.xml",
        "giscedata_facturacio_conceptes_report.xml"
    ],
    "active": False,
    "installable": True
}
