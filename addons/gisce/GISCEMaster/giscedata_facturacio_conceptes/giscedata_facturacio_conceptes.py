# -*- coding: utf-8 -*-

# Afegim la categoria de conceptes per tal d'assignar els impostos als productes
from giscedata_facturacio_distri.wizard.config_taxes_facturacio_distri import \
TARIFES_XML_IDS
TARIFES_XML_IDS += [('giscedata_facturacio_conceptes', 'categ_drets_escomesa')]
