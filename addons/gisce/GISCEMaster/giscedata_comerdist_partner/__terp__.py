# -*- coding: utf-8 -*-
{
    "name": "Comerdist Partner",
    "description": """
    This module provide :
      * Sync of partners and partner address
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_comerdist"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_view.xml"
    ],
    "active": False,
    "installable": True
}
