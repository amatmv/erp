# -*- coding: utf-8 -*-
from osv import osv, fields
from giscedata_comerdist import OOOPPool, Sync


class ResPartnerComerdist(osv.osv):
    """sync partners
    """
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'remote_id': fields.integer('Comerdist RID', readonly=True),
    }

    _defaults = {
        'remote_id': lambda *a: 0,
    }

    def copy(self, cursor, uid, id, default=None, context=None):
        """No copiem el remote_id"""

        if not default:
            default = {}

        default.update({'remote_id': 0,
                       })
        
        res_id = super(ResPartnerComerdist,
                       self).copy(cursor, uid, id, default, context)
        return res_id

    def must_be_synched(self, cursor, uid, ids, context=None):
        '''partner must be synched if it is contained in
        one of the partner fields of a polissa
        with sync configuration'''

        if not context:
            context = {}
        if not context.get('sync', True):
            return False

        for partner in self.browse(cursor, uid, ids, context):
            sync_config = partner.get_config(context)
            if sync_config and sync_config.user_local.id != uid:
                return sync_config
            else:
                return False
    
    def create(self, cursor, uid, vals, context=None):
        """Mètode create per la sincronització.
        """
        # First do the job in local
        res_id = super(ResPartnerComerdist, self).create(cursor, uid,
                                                         vals, context)
        partner = self.browse(cursor, uid, res_id, context)
        # Check if we have to sync this partner
        if partner.must_be_synched(context):
            config_id = partner.get_config().id
            ooop = OOOPPool.get_ooop(cursor, uid, config_id)
            sync = Sync(cursor, uid, ooop, self._name, config=config_id)
            r_partner = sync.sync(partner.id)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode write per la sincronització.
        """
        res = super(ResPartnerComerdist, self).write(cursor, uid, ids,
                                                     vals, context)
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for partner in self.browse(cursor, uid, ids, context):
            config = partner.must_be_synched(context)
            if config:
                config_id = config.id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                r_partner = sync.get_object(partner.id)
                if not r_partner:
                    vals = None
                #Break relationship between partners
                #if not active. Also, do not sync
                if not partner.active:
                    if partner.remote_id:
                        partner.write({'remote_id': 0},
                                      context={'sync': False})
                    if r_partner:
                        r_partner.write({'remote_id': 0})
                    continue
                r_partner = sync.sync(partner.id, vals)
        return res

    def break_rel(self, cursor, uid, ids, context=None):
        '''Break relationship between local partner
        and remote partner, deleting remote_id'''
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for partner in self.browse(cursor, uid, ids):
            config = partner.must_be_synched(context)
            if config:
                config_id = config.id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                r_partner = sync.get_object(partner.id)
                self.write(cursor, uid, ids, {'remote_id': False},
                           context={'sync': False})
                if r_partner:
                    r_partner.write({'remote_id': False})
        return True

ResPartnerComerdist()

class ResPartnerAddressComerdist(osv.osv):
    """sync partners address
    """
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    _columns = {
        'remote_id': fields.integer('Comerdist RID', readonly=True),
    }

    _defaults = {
        'remote_id': lambda *a: 0,
    }

    def copy(self, cursor, uid, id, default=None, context=None):
        """No copiem el remote_id"""

        if not default:
            default = {}

        default.update({'remote_id': 0,
                       })
        
        res_id = super(ResPartnerAddressComerdist,
                       self).copy(cursor, uid, id, default, context)
        return res_id

    def must_be_synched(self, cursor, uid, ids, context=None):
        '''partner address must be synched if its partner is
        contained in one of the partner fields of a polissa
        with sync configuration'''

        if not context:
            context = {}
        if not context.get('sync', True):
            return False

        for address in self.browse(cursor, uid, ids, context):
            sync_config = address.get_config(context)
            if sync_config and sync_config.user_local.id != uid:
                return sync_config
            else:
                return False

    def create(self, cursor, uid, vals, context=None):
        """Mètode create per la sincronització.
        """
        # First do the job in local
        res_id = super(ResPartnerAddressComerdist, self).create(cursor,
                                                    uid, vals, context)
        address = self.browse(cursor, uid, res_id, context)
        # Check if we have to sync this address
        config = address.must_be_synched(context)
        if config:
            if 'partner_id' not in vals:
                vals.update({'partner_id': address.partner_id.id})
            config_id = config.id
            ooop = OOOPPool.get_ooop(cursor, uid, config_id)
            sync = Sync(cursor, uid, ooop, self._name, config=config_id)
            r_address = sync.sync(address.id)
            #Store remote_id for later processing
            address.write({'remote_id': r_address.id},
                          context={'sync': False})
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode write per la sincronització.
        """
        res = super(ResPartnerAddressComerdist, self).write(cursor, uid, ids,
                                                     vals, context)
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for address in self.browse(cursor, uid, ids, context):
            config = address.must_be_synched(context)
            if config:
                if 'partner_id' not in vals:
                    vals.update({'partner_id': address.partner_id.id})
                config_id = config.id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                r_address = sync.get_object(address.id)
                if not r_address:
                    vals = None
                #Break relationship between addresses if
                #address is not active. Also do not sync
                if not address.active:
                    if address.remote_id:
                        address.write({'remote_id': 0},
                                      context={'sync': False})
                    if r_address:
                        r_address.write({'remote_id': 0})
                    continue
                r_address = sync.sync(address.id, vals)
                if (not address.remote_id or
                    address.remote_id != r_address.id):
                    address.write({'remote_id': r_address.id},
                                  context={'sync': False})

        return res

    def break_rel(self, cursor, uid, ids, context=None):
        '''Break relationship between local address
        and remote address, deleting remote_id'''
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for address in self.browse(cursor, uid, ids):
            config = address.must_be_synched(context)
            if config:
                config_id = config.id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                r_address = sync.get_object(address.id)
                self.write(cursor, uid, ids, {'remote_id': False},
                           context={'sync': False})
                if r_address:
                    r_address.write({'remote_id': False})
        return True

ResPartnerAddressComerdist()
