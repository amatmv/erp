# -*- coding: utf-8 -*-
from __future__ import absolute_import
from giscedata_telemesura.telemesura import *
from mongodb_backend import osv_mongodb
import logging
from enerdata.contracts import TRE
from enerdata.profiles.profile import (
    Profile, ProfileHour, REProfileZone1, REProfileZone2, REProfileZone3,
    REProfileZone4, REProfileZone5)

CLIMATIC_ZONE_MAP = {
    1: REProfileZone1,
    2: REProfileZone2,
    3: REProfileZone3,
    4: REProfileZone4,
    5: REProfileZone5
}


class TmProfile(osv_mongodb.osv_mongodb):

    _name = 'tm.profile'
    _inherit = 'tm.profile'

    @staticmethod
    def convert_to_profilehour_re(measure):
        ph = TMProfileHour(
            localize_season(measure['timestamp'], measure['season']),
            measure['ae'],
            measure['valid'],
            Decimal(0),
            meta=measure
        )
        return ph

    @staticmethod
    def convert_to_mongodb_re(measure):
        """ measure fact is AE """
        assert isinstance(measure, (ProfileHour, TMProfileHour))
        return dict(
            timestamp=TIMEZONE.normalize(measure.date).strftime(
                '%Y-%m-%d %H:00:00'),
            ae_fact=measure.measure,
            valid=True,
            valid_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            cch_fact=True,
            magn=1000,
            season=get_season(measure.date)
        )

    def fix_re(self, cursor, uid, meter, start, end, tariff, balance, origin):
        """Fix a complete profile using enerdata lib.

        :param cursor: Database cursor
        :param uid: User id
        :param meter: Meter name
        :param start: Start date (included)
        :param end:  End date (included)
        :param tariff: Tariff code
        :param balance: Balance in kW
        :param origin: The origin code in form of `CODE-SUBCODE`. Eg:: `20-00`
        :return: True
        """
        balance = balance.copy()

        logger = logging.getLogger(__name__)
        # Delete all cch_fact True and cch_bruta False because they were
        # gaps and if you want to recalculate it maybe is a refacturation
        del_profiles_ids = self.search(cursor, uid, [
            ('name', '=', meter),
            ('timestamp', '>=', start),
            ('timestamp', '<=', end),
            ('cch_fact', '=', True),
            ('cch_bruta', '=', False),
            ('type', '=', 'p'),
        ])
        if del_profiles_ids:
            logger.info(
                'Already found {0} gaps filled. Deleting them...'.format(
                    len(del_profiles_ids)
                )
            )
            self.unlink(cursor, uid, del_profiles_ids)

        # First get all the profiles hours from start to end of meter
        profiles_ids = self.search(cursor, uid, [
            ('name', '=', meter),
            ('timestamp', '>=', start),
            ('timestamp', '<=', end),
            ('type', '=', 'p'),
        ])

        ph_measures = []
        for profile in self.read(cursor, uid, profiles_ids):
            ph_measures.append(self.convert_to_profilehour_re(profile))

        start = TIMEZONE.localize(datetime.strptime(start, '%Y-%m-%d %H:00:00'))
        end = TIMEZONE.localize(datetime.strptime(end, '%Y-%m-%d %H:00:00'))
        t = TRE()
        ph_measures_pass = ph_measures
        profile = Profile(start, end, ph_measures_pass)
        climatic_zone = tariff.get('RE', [])
        profile.profile_class = CLIMATIC_ZONE_MAP[climatic_zone]

        fixed_profile = profile.fixit(t, balance, 1)
        logger.info('Profile from meter: {0} ({1} - {2}) have {3} gaps'.format(
            meter, start, end, len(profile.gaps)
        ))
        invalid = dict((m.date, m) for m in ph_measures
                       if not m.valid or origin.startswith('40'))
        adjusted_periods = fixed_profile.adjusted_periods
        for measure in fixed_profile.measures:
            vals = self.convert_to_mongodb_re(measure)
            if isinstance(measure, ProfileHour):
                # Is a gap or a measure that is not valid
                logger.info('Meter {0} filling gap {1} with {2}'.format(
                    meter, measure.date, measure.measure
                ))
                if origin in ORIGIN_KIND_PROFILED:
                    kind = ORIGIN_KIND_PROFILED[origin]
                else:
                    logger.warning(
                        'Origin: {0} not in ORIGIN_KIND_PROFILED: {1}. '
                        'Setting KIND_REAL ({2}) as default'.format(
                            origin, ORIGIN_KIND_PROFILED.keys(), KIND_REAL
                        )
                    )
                    kind = KIND_REAL
                vals['kind_fact'] = kind
                vals['name'] = meter
                original = invalid.pop(measure.date, None)
                if original is None:
                    vals['cch_bruta'] = False
                    self.create(cursor, uid, vals)
                else:
                    # Is not a real gap, it was a invalid measure, fill in
                    # non-created measures, ai, r1...
                    vals['ai_fact'] = 0
                    vals['r1_fact'] = 0
                    vals['r2_fact'] = 0
                    vals['r3_fact'] = 0
                    vals['r4_fact'] = 0
                    self.write(cursor, uid, [original.meta['id']], vals)
            else:
                # Real and valid
                period = t.get_period_by_date(measure.date).code
                # Is adjusted if period is marked as adjusted
                if period in adjusted_periods:
                    logger.info('Meter {0} adjusting {1} with {2}'.format(
                        meter, measure.date, measure.measure
                    ))
                    vals['kind_fact'] = KIND_ADJUSTED
                else:
                    vals['kind_fact'] = KIND_REAL
                vals['valid_date'] = measure.meta['valid_date']
                self.write(cursor, uid, [measure.meta['id']], vals)


TmProfile()
