# -*- coding: utf-8 -*-
{
    "name": "Mòdul de Telemesura Règim Especial",
    "description": """
    * Perfilacio automatica al importar curves a regim especial desde telemesura
    * Preparació de curves per generar fitxers de regim especial (MAGRE, INMERE, MEDIDAS).""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_re",
        "giscedata_telemesura",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
