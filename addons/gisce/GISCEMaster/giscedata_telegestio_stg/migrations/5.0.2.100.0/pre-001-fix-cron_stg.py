# coding=utf-8
from oopgrade import DataMigration
from addons import get_module_resource
import pooler
import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Migrating STG schedule registers')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    logger.info(
        "Change old ir_cron register id from 'ir_cron_tg_reader_action' to "
        "'ir_cron_launch_stg_retry_requests'"
    )

    ir_model_obj = pool.get('ir.model.data')
    ir_ids = ir_model_obj.search(
        cursor, uid,
        [('module', '=', 'giscedata_telegestio_stg'),
         ('name', '=', 'ir_cron_tg_reader_action')]  # Old bad id
    )

    for ir_data in ir_model_obj.read(cursor, uid, ir_ids, ['res_id', 'name']):
        irmd_id = ir_data['id']
        logger.info(' * Renaming old ir_cron register {}'.format(irmd_id))
        ir_model_obj.write(
            cursor, uid, irmd_id, {'name': 'ir_cron_launch_stg_retry_requests'}
        )

    data_path = get_module_resource(
        'giscedata_telegestio_stg', 'telegestio_stg_data.xml'
    )
    data_xml = """
        <openerp>
            <data>
                <record forcecreate="True" id="ir_cron_pending_requests_for_tg_data" model="ir.cron">
                    <field name="name">Create STG retry requests</field>
                    <field name="user_id" ref="base.user_root"/>
                    <field name="interval_number">1</field>
                    <field name="interval_type">days</field>
                    <field name="numbercall">-1</field>
                    <field eval="False" name="doall"/>
                    <field eval="False" name="active"/>
                    <field name="nextcall" eval="'2019-01-01 23:00:00'"/>
                    <field eval="'giscedata.telegestio.stg'" name="model"/>
                    <field eval="'pending_requests_for_tg_data'" name="function"/>
                    <field eval="'()'" name="args"/>
                </record>
            </data>
        </openerp>
    """

    dm = DataMigration(data_xml, cursor, 'giscedata_telegestio_stg', {
        'ir.cron': ['model', 'function'],
        'res.config': ['name'],
    })
    dm.migrate()

    logger.info('ir cron migrated')


migrate = up
