# -*- coding: utf-8 -*-
{
    "name": "STG",
    "description": """
    Interact with PRIME
    """,
    "version": "0-dev",
    "author": "Miquel Isern Roca",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_telegestio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscedata_telegestio_stg_view.xml",
        "telegestio_stg_data.xml",
        "wizard/wizard_stg_requests_view.xml",
        "wizard/wizard_stg_dc_requests_view.xml",
        "wizard/wizard_stg_all_dc_requests_view.xml"
    ],
    "active": False,
    "installable": True
}
