from urllib import urlencode
from tools.translate import _
from osv import fields, osv
from datetime import datetime, date, time, timedelta
from tools import config
from dateutil.relativedelta import relativedelta
import netsvc
import requests
from giscedata_telegestio.telegestio import CNC_REPORTS
from oorq.decorators import job
import logging
import pooler

REPORT_TG_NAMES = {
    'instant-data': _('S01 - Instant Data'),
    'daily-incremental': _('S02 - Daily Incremental'),
    'monthly-billing': _('S04 - Monthly Billing'),
    'daily-absolute': _('S05 - Daily Absolute'),
    'meter-parameters': _('S06 - Meter Parameters'),
    'meter-events': _('S09 - Meter Events'),
    'cnc-parameters': _('S12 - Concentrator Parameters'),
    'cnc-events': _('S17 - Concentrator events'),
    'advanced-instant-data': _('S21 - Advanced Instant Data'),
    'contract-definition': _('S23 - Contract definition'),
    'current-billing': _('S27 - Current billing'),
}

ORDER_TG_NAMES = {
    'order-request': _('B11 - DC Order Request')
}

MODE = [('REP', _('Request report')),
        ('ORD', _('Send order'))]

B11_ORDERS = [('T01', _('T01 - Reboot DC')),
              ('T02', _('T02 - Cancel running reports')),
              ('T03', _('T03 - Synchronize meter time')),
              ('T04', _('T04 - Permanent remove of meters with Permanent Failure')),
              ('T05', _('T05 - Force DC time synchronisation')),
              # ('T06', _('T06 - Remove DC passwords')),
              ('T07', _('T07 - Force meter synchronisation')),
              ]
DATA_SOURCES = [('DCC', _('DC conditional')),
                ('DCF', _('DC Force')),
                ('MET', _('Meter Force'))]


def build_url_parameters(from_date=None, to_date=None, source=None):
    req_params = {}
    if from_date:
        from_date_ = str(from_date).translate(None, " /:-")
        req_params['from'] = from_date_
    if to_date:
        to_date_ = str(to_date).translate(None, " /:-")
        req_params['to'] = to_date_
    if source:
        req_params['source'] = source
    if req_params:
        return'?{}'.format(urlencode(req_params))
    else:
        return ''


def get_stg_report_name(report_code):
    if report_code == 'S04':
        return 'monthly-billing'
    elif report_code == 'S05':
        return 'daily-absolute'
    elif report_code == 'S02':
        return 'daily-incremental'
    else:
        return False


class GiscedataTelegestioStg(osv.osv):
    """Manages the operations of the STG"""
    _name = 'giscedata.telegestio.stg'
    _auto = False

    def pending_requests_for_tg_data(self, cursor, uid, r_date=None,
                                     report=None, context=None):
        """
        Checks for every TG concentrator on the system which meters lack data
        from the required reports.
        Once it has all meters and what reports they need, groups them by the
        best possible hours of availability.
        Lastly, the registers for the requests that have to be done to the STG
        are created with the necessary data for the request to be sent.
        :param cursor: DB cursor
        :param uid: User ID
        :param report: List of reports to be requested
        :param context: context param
        :return:
        """
        cnc_obj = self.pool.get('tg.concentrator')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        if not report:
            report = ['S02', 'S04', 'S05']
        if not isinstance(report, list):
            report = [report]
        if r_date is None:
            # Defaults calcultate for tomorrow
            r_date = (
                datetime.combine(date.today(), time.min) + timedelta(days=1)
            )
        if not isinstance(r_date, datetime):
            try:
                r_date = datetime.strptime(r_date, '%Y-%m-%d')
            except ValueError as exc:
                raise osv.except_osv(exc.message)
        cnc_ids = cnc_obj.search(cursor, uid, [])
        for cnc_id in cnc_ids:
            rep = cnc_obj.check_meters_for_tg_data_shortage(cursor, uid, cnc_id,
                                                            report=report,
                                                            context=context)
            if all(not value for value in rep.values()):
                # If the CNC have no data shortage skip it
                continue
            by_hours = meter_obj.group_meters_by_best_availability_hours(
                cursor, uid, rep, r_date, context=context)
            self.create_stg_request_registers(cursor, uid, cnc_id, by_hours,
                                              r_date, context=context)

    def create_stg_request_registers(self, cursor, uid, cnc_id, h_data, r_date,
                                     context=None):
        """
        Creates a request for every report required and at the hours and for the
         meters specified on h_data.
        :param cursor: DB cursor
        :param uid: User ID
        :param cnc_id: TG concentrator ID
        :param h_data: Dictionary with all reports to request containing another
        dictionary with every hour of the day and a list of meters for that hour
            {
                'S02': {'hour 1': [(meter_id1, tg_name1)],
                        'hour 2': [(meter_id2, tg_name2)],
                        ...,
                        }
                'S04': {}
            }
        :param r_date: Date of the day the request must be sent
        :param context: context param
        :return:
        """
        srr_obj = self.pool.get('tg.stg.request.register')
        srs_obj = self.pool.get('tg.stg.request.summary')
        conf_obj = self.pool.get('res.config')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        created = []
        r_meters = []
        base_url = config['stg_url']
        for report, hours in h_data.items():
            for hour, meters in hours.items():
                if not meters:
                    # These meters do no have availability data
                    continue
                r_date_ = datetime.replace(r_date, hour=hour)
                d_back = 0
                m_back = 0
                meter_ids = [meter[0] for meter in meters]
                meter_names = [meter[1] for meter in meters]
                str_meters = ','.join(meter_names)

                if report == 'S04':
                    m_back = int(conf_obj.get(cursor, uid,
                                 'tg_monthly_billings_retry_months', '1'))
                    tg_field = 'tg_last_read'
                elif report == 'S05':
                    d_back = int(conf_obj.get(cursor, uid,
                                 'tg_daily_billings_retry_days', '5'))
                    tg_field = 'tg_last_read'
                elif report == 'S02':
                    m_back = int(conf_obj.get(cursor, uid,
                                 'tg_hourly_profiles_retry_months', '1'))
                    tg_field = 'tg_last_profile'
                else:
                    break
                report_type = get_stg_report_name(report)
                tg_last = min(meter_obj.read(cursor, uid, meter_ids, [tg_field])
                              , key=lambda x: x[tg_field])[tg_field]
                if tg_last:
                    if tg_field == 'tg_last_profile':
                        date_format = '%Y-%m-%d %H:%M:%S'
                    else:
                        date_format = '%Y-%m-%d'
                    f_date = datetime.replace(datetime.strptime(
                        tg_last, date_format), hour=1)
                else:
                    f_date = datetime.replace(r_date_, hour=1) - relativedelta(
                        months=m_back) - relativedelta(days=d_back)
                req_params = build_url_parameters(
                    from_date=f_date, to_date=r_date_, source='DCC')
                report_url = '{}/{}/async/{}{}'.format(base_url, report_type,
                                                       str_meters,  req_params)
                params = {
                    'cnc_id': cnc_id,
                    'meter_ids': [(6, False, meter_ids)],
                    'report_type': report,
                    'request_date': r_date_,
                    'sync': False,
                    'origin': 'retry',
                    'date_from': f_date,
                    'date_to': r_date_,
                    'url_address': report_url,
                    'state': 'to_send'
                }
                srr_id = srr_obj.create(cursor, uid, params)
                created.append(srr_id)
                for meter_id in meter_ids:
                    if meter_id not in r_meters:
                        r_meters.append(meter_id)
        if r_meters and created:
            srs_id = srs_obj.create(cursor, uid, {
                'meter_ids_before': [(6, False, r_meters)],
                'request_ids': [(6, False, created)],
                'cnc_id': cnc_id,
                'date': r_date
            })
            srr_obj.write(cursor, uid, created, {
                'summary_id': srs_id,
            })

    def launch_stg_retry_requests(self, cursor, uid, context=None):
        """
        Searches for TgStgRequestRegister which are programmed to be sent at the
        current time. If there are any it sends a request each of them specify and modifies the
         register according to the TG concentrator response.
        :param cursor: DB cursor
        :param uid: User ID
        :param context: context param
        :return: True
        """
        srr_obj = self.pool.get('tg.stg.request.register')
        cnc_obj = self.pool.get('tg.concentrator')
        conf_obj = self.pool.get('res.config')
        logger = netsvc.Logger()
        logger.notifyChannel("STG Retries", netsvc.LOG_INFO,
                             "Launching pending requests")
        _hour = datetime.now().hour
        r_date = (datetime.combine(date.today(), time.min).replace(hour=_hour)
                  ).strftime('%Y-%m-%d %H:%M:%S')
        max_retries = int(conf_obj.get(
            cursor, uid, 'tg_max_retries_for_stg_request', 5))
        srr_ids = srr_obj.search(cursor, uid, [
            ('request_date', '=', r_date),
            ('origin', '=', 'retry'),
            ('state', 'in', ['to_send', 'send_error']),
            ('send_retries', '<', max_retries)
        ])
        if srr_ids:
            logger.notifyChannel("STG Retries", netsvc.LOG_INFO,
                                 "We have {} requests ready to send".format(
                                     len(srr_ids)))
            srrs = srr_obj.read(cursor, uid, srr_ids, ['url_address', 'cnc_id',
                                                       'send_retries'])
            sent_cncs = []
            db = pooler.get_db(cursor.dbname)
            for srr in srrs:
                if srr['cnc_id'] in sent_cncs:
                    continue
                tmp_cursor = db.cursor()
                sent_cncs.append(srr['cnc_id'])
                cnc = cnc_obj.read(cursor, uid, srr['cnc_id'][0], ['name'])
                cnc_req = [
                    srr_ for srr_ in srrs if srr_['cnc_id'][0] == cnc['id']]
                self.launch_cnc_stg_retry_requests(tmp_cursor, uid, cnc_req, cnc
                                                   , context=context)
                tmp_cursor.close()
        else:
            logger.notifyChannel("STG Retries", netsvc.LOG_INFO,
                                 "No requests pending right now")
            srr_error_ids = srr_obj.search(cursor, uid, [
                ('request_date', '=', r_date),
                ('origin', '=', 'retry'),
                ('state', '=', 'send_error'),
                ('send_retries', '>=', max_retries)
            ])
            srr_obj.write(cursor, uid, srr_error_ids, {
                'state': 'unanswered',
                'cnc_ack': False})
        self.update_sent_requests(cursor, uid, context=context)
        return True

    @job(queue='stg_retry_launcher', timeout=30)
    def launch_cnc_stg_retry_requests(self, cursor, uid, srrs, cnc,
                                      context=None):
        srr_obj = self.pool.get('tg.stg.request.register')
        cnc_obj = self.pool.get('tg.concentrator')
        logger = logging.getLogger('stg.retry.launcher')
        for srr in srrs:
            error = False
            cnc_status = cnc_obj.read(cursor, uid, srr['cnc_id'][0], ['status'])
            if cnc_status.get('status', 'down') != 'up':
                logger.info("CNC {} is down. Not sending the request")
                srr_obj.write(cursor, uid, srr['id'], {
                    'state': 'send_error',
                    'cnc_ack': False,
                    'send_retries': srr['send_retries'] + 1})
                continue
            try:
                r = requests.get(srr['url_address'], timeout=5)
            except requests.ReadTimeout as tout_exc:
                logger.info("Request for CNC {} failed. The message timed out"
                            "".format(cnc['name']))
                error = True
            if error or r.status_code != 200:
                logger.info("Request for CNC {} failed".format(cnc['name']))
                srr_obj.write(cursor, uid, srr['id'], {
                    'state': 'send_error',
                    'cnc_ack': False,
                    'send_retries': srr['send_retries'] + 1})
            else:
                logger.info("Request for CNC {} succeeded with ID: {}".format(
                    cnc['name'], r.headers['request_id']))
                srr_obj.write(cursor, uid, srr['id'], {
                    'state': 'sent',
                    'send_retries': srr['send_retries'] + 1,
                    'cnc_ack': True,
                    'request_id': r.headers['request_id']
                })
        return True

    def update_sent_requests(self, cursor, uid, context=None):
        srr_obj = self.pool.get('tg.stg.request.register')
        srs_obj = self.pool.get('tg.stg.request.summary')
        logger = netsvc.Logger()
        logger.notifyChannel("STG Retries", netsvc.LOG_INFO,
                             "Updating completed or failed requests")
        n_date = datetime.now()
        _hour = n_date.hour
        y_date = (datetime.combine(date.today(), time.min).replace(hour=_hour)
                  - relativedelta(days=2)).strftime('%Y-%m-%d %H:%M:%S')
        srr_ids = srr_obj.search(cursor, uid, [
            ('request_date', '>=', y_date),
            ('origin', '=', 'retry'),
            ('state', '=', 'completed')
        ])
        if srr_ids:
            logger.notifyChannel("STG Retries", netsvc.LOG_INFO,
                                 "We have {} completed requests to update"
                                 "".format(len(srr_ids)))
            srrs = srr_obj.read(cursor, uid, srr_ids, [
                'summary_id', 'meter_ids', 'request_date'])
            for srr in srrs:
                if not srr['summary_id']:
                    continue
                meters_ = srs_obj.read(cursor, uid, srr['summary_id'][0], [
                    'meter_ids_before', 'meter_ids_after'])
                meters_after = set(meters_['meter_ids_after'])
                meters_before = set(meters_['meter_ids_before'])
                srr_ok_ = set(srr['meter_ids']) - meters_after
                srs_ok_ = meters_before - meters_after
                srr_correct = 100 * len(srr_ok_) / len(srr['meter_ids'])
                srs_correct = 100 * len(srs_ok_) / len(meters_before)
                srr_obj.write(cursor, uid, srr['id'], {
                    'success_percentage': srr_correct})
                srs_obj.write(cursor, uid, srr['summary_id'][0], {
                    'success_percentage': srs_correct})
                if srr_correct < 100:
                    r_date = datetime.strptime(srr['request_date'],
                                               '%Y-%m-%d %H:%M:%S')
                    if r_date.day == n_date.day and r_date.hour == _hour \
                            and n_date.minute < 30:
                        srr_obj.write(cursor, uid, srr['id'], {
                            'state': 'to_send',
                            'send_retries': 0,
                            'cnc_ack': False,
                        })

        srr_ids = srr_obj.search(cursor, uid, [
            ('request_date', '<', y_date),
            ('origin', '=', 'retry'),
            ('state', 'in', ['sent', 'to_send']),
        ])
        if srr_ids:
            logger.notifyChannel("STG Retries", netsvc.LOG_INFO,
                                 "We have {} old unanswered requests to update"
                                 "".format(len(srr_ids)))
            srr_obj.write(cursor, uid, srr_ids, {'state': 'unanswered'})

GiscedataTelegestioStg()


class TgStgRequestSummary(osv.osv):
    """
    Implements a register for each request done to the STG
    """

    _name = 'tg.stg.request.summary'
    _order = 'date desc'

    def name_get(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if not ids:
            return []
        res = []
        f_to_read = ['cnc_id', 'date']
        for record in self.read(cursor, uid, ids, f_to_read, context=context):
            res += [(
                record['id'], '{} - {}'.format(record['cnc_id'][1], record['date'])
            )]
        return res

    _columns = {
        'create_uid': fields.many2one('res.users', 'Creator', required=True,
                                      readonly=True),
        'create_date': fields.datetime('Create date', required=True,
                                       readonly=True),
        'meter_ids_before': fields.many2many('giscedata.lectures.comptador',
                                             'stg_summary_meters_rel',
                                             'summary_id', 'meter_id',
                                             'Meters before retries',
                                             readonly=True),
        'meter_ids_after': fields.many2many('giscedata.lectures.comptador',
                                            'stg_summary_meters_after_rel',
                                            'summary_id', 'meter_id',
                                            'Meters after retries',
                                            readonly=True),
        'cnc_id': fields.many2one('tg.concentrator', 'Cnc id', required=True,
                                  readonly=True),
        'request_ids': fields.one2many('tg.stg.request.register', 'summary_id',
                                       'Request IDs', required=True,
                                       readonly=True),
        'tg_error_ids': fields.json('TG reader error IDs', readonly=False),
        'date': fields.datetime('Date of execution', required=True,
                                readonly=True),
        'success_percentage': fields.integer('Success %', readonly=True),
    }

    _defaults = {
        'success_percentage': lambda *a: 0,
    }

TgStgRequestSummary()


class StgSummaryMetersRel(osv.osv):
    _name = "stg.summary.meters.rel"
    _rec_name = "summary_id"
    _columns = {
        'summary_id': fields.many2one('tg.stg.request.summary', 'STG summary',
                                      ondelete='cascade'),
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter',
                                    ondelete='cascade')
    }
StgSummaryMetersRel()


class StgSummaryMetersAfterRel(osv.osv):
    _name = "stg.summary.meters.after.rel"
    _rec_name = "summary_id"
    _columns = {
        'summary_id': fields.many2one('tg.stg.request.summary', 'STG summary',
                                      ondelete='cascade'),
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter',
                                    ondelete='cascade')
    }
StgSummaryMetersAfterRel()


class TgStgRequestRegister(osv.osv):
    """
    Implements a register for each request done to the STG
    """

    _name = 'tg.stg.request.register'
    _order = 'request_date desc'

    def _count_meters(self, cursor, uid, ids, prop, unkown_one, context=None):
        res = {}
        meter_ids = self.read(cursor, uid, ids, ['meter_ids'])
        for meter in meter_ids:
            res[meter['id']] = str(len(meter['meter_ids']))
        return res

    _columns = {
        'request_id': fields.char('Request ID', size=20, readonly=True),
        'create_uid': fields.many2one('res.users', 'Creator', required=True,
                                      readonly=True),
        'create_date': fields.datetime('Create date', required=True,
                                       readonly=True),
        'meter_ids': fields.many2many('giscedata.lectures.comptador',
                                      'stg_requests_meters_rel', 'request_id',
                                      'meter_id', 'Meters', readonly=True),
        'cnc_id': fields.many2one('tg.concentrator', 'Cnc id', required=True,
                                  readonly=True),
        'tg_reader_ids': fields.many2many('tg.reader.register',
                                          'stg_requests_tg_reader_register_rel',
                                          'request_id', 'tg_reader_id',
                                          'Reader registers', readonly=True),
        'report_type': fields.char('Report', size=4, required=True,
                                   readonly=True),
        'request_date': fields.datetime('Date of request', required=True,
                                        readonly=True),
        'answer_date': fields.datetime('Date of answer', readonly=True),
        'sync': fields.boolean('Report sync', readonly=True),
        'origin': fields.selection([('wizard', 'STG wizard'),
                                    ('retry', 'Retries process')],
                                   'Request origin', readonly=True),
        'date_from': fields.datetime('Data date from', readonly=True),
        'date_to': fields.datetime('Data date to', readonly=True),
        'url_address': fields.text('Request URL', required=True, readonly=True),
        'cnc_ack': fields.boolean('Cnc ACK', readonly=True),
        'state': fields.selection([('to_send', 'To send'),
                                   ('sent', 'Request sent'),
                                   ('send_error', 'Error sending request'),
                                   ('answered', 'Answer received'),
                                   ('unanswered', 'Answer not received'),
                                   ('completed', 'Request completed')],
                                  'State', required=True, readonly=True),
        'send_retries': fields.integer('Sending retries', required=False),
        'summary_id': fields.many2one('tg.stg.request.summary', 'Summary ID',
                                      readonly=True),
        'success_percentage': fields.integer('Success %', readonly=True),
        'num_meters': fields.function(_count_meters, method=True, type="char",
                                      string='Meters amount', readonly=True),
    }

    _defaults = {
        'cnc_ack': lambda *a: False,
        'state': lambda *a: 'to_send',
        'send_retries': lambda *a: 0,
        'success_percentage': lambda *a: 0,
    }

TgStgRequestRegister()


class StgRequestsMetersRel(osv.osv):
    _name = "stg.requests.meters.rel"
    _rec_name = "request_id"
    _columns = {
        'request_id': fields.many2one('tg.stg.request.register', 'STG request',
                                      ondelete='cascade'),
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter',
                                    ondelete='cascade')
    }
StgRequestsMetersRel()


class StgRequestsTgReaderRegisterRel(osv.osv):
    _name = "stg.requests.tg.reader.register.rel"
    _rec_name = "request_id"
    _columns = {
        'request_id': fields.many2one('tg.stg.request.register', 'STG request',
                                      ondelete='cascade'),
        'tg_reader_id': fields.many2one('tg.reader.register', 'Tg reader '
                                        'register', ondelete='cascade')
    }
StgRequestsTgReaderRegisterRel()


class TgReader(osv.osv):
    '''Implements reader'''
    _name = 'tg.reader'
    _inherit = 'tg.reader'

    def insert_primestg(self, cursor, uid, tg_xml, context=None):
        '''insert function for values from a primestg lib xml'''
        if not context:
            context = {}
        srr_obj = self.pool.get('tg.stg.request.register')
        srs_obj = self.pool.get('tg.stg.request.summary')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        srr_id = []
        errors = False
        req_id = tg_xml.request_id
        if req_id != '0':
            srr_id = srr_obj.search(cursor, uid, [
                ('request_id', '=', req_id)])
        for cnc in tg_xml.concentrators:
            if tg_xml.report_type not in CNC_REPORTS:
                errors = True
                if cnc.meters:
                    for meter in cnc.meters:
                        meter_id = meter_obj.search(cursor, uid, [
                            ('meter_tg_name', '=', meter.name)])
                        if meter.errors:
                            error_id = self.create_file_reader_errors(
                                cursor, uid, cnc.name, meter, context=context)
                            if srr_id:
                                srs_id = srr_obj.read(
                                    cursor, uid, srr_id[0], ['summary_id'])[
                                    'summary_id']
                                if not srs_id:
                                    continue
                                error_ids = srs_obj.read(cursor, uid, srs_id[0],
                                                         ['tg_error_ids',
                                                          'meter_ids_after'])
                                if error_ids.get('tg_error_ids', False):
                                    error_ids.update(
                                        {error_id: meter.name})
                                else:
                                    error_ids = {error_id: meter.name}
                                params = {'tg_error_ids': error_ids}
                                if meter_id and meter_id[0] not in error_ids.get(
                                        'meter_ids_after', []):
                                    params.update(
                                        {'meter_ids_after': [(4, meter_id[0])]})
                                srs_obj.write(cursor, uid, srs_id[0], params)
                            continue
        context.update({'meter_errors': errors,
                        'srr_id': srr_id})
        return super(TgReader, self).insert_primestg(cursor, uid, tg_xml,
                                                     context=context)

    def reader(self, cursor, uid, ftp_ids=None, one_file=None, f_to_read=None,
               context=None):
        '''Connect to the ftp server and read all the files'''
        srr_obj = self.pool.get('tg.stg.request.register')
        result = super(TgReader, self).reader(
            cursor, uid, ftp_ids=ftp_ids, one_file=one_file, f_to_read=f_to_read
            , context=context)
        if result:
            current_date = datetime.now().strftime(
                '%Y-%m-%d %H:%M:%S')
            for srr_id, reg_ids in result.items():
                srr_obj.write(cursor, uid, srr_id, {
                    'answer_date': current_date,
                    'state': 'completed'
                })
                for reg_id in reg_ids:
                    srr_obj.write(cursor, uid, srr_id, {
                        'tg_reader_ids': [(4, reg_id)],
                    })

TgReader()
