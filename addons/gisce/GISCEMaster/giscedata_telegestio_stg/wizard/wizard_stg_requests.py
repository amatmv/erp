import StringIO
import zipfile
from oorq.decorators import job
from autoworker import AutoWorker
from oorq.oorq import JobsPool
from osv import osv, fields
from tools import config, logging
from datetime import datetime
from tools.translate import _
import base64
import requests
import operator
import giscedata_telegestio_stg.giscedata_telegestio_stg as stg
from base_extended.base_extended import NoDependency
from tqdm import tqdm


class ProgressJobsPool(JobsPool):
    def __init__(self, wizard):
        self.wizard = wizard
        super(ProgressJobsPool, self).__init__()

    @property
    def all_done(self):
        logger = logging.getLogger('openerp.ifo.progress')
        logger.info('Progress: {0}%'.format(self.progress
                                            ))
        self.wizard.write({'export_progress': self.progress})
        return super(ProgressJobsPool, self).all_done


class ActiveSTGRequest(osv.osv_memory):
    """
        Wizard to request reports fot meters from TG systems.
    """
    _name = 'wizard.stg.requests'

    SUPP_REPORTS = []
    SUPP_ORDERS = []

    def get_report_synchronicity(self, rtype):
        for report in self.SUPP_REPORTS:
            if report[0] == rtype:
                return report[1]
        return None

    def send_request(self, wizard, url, context=None):
        r = requests.get(url)
        if r.status_code != 200:
            return None
        else:
            if wizard.sync:
                return r.content
            else:
                return 'async'

    def build_requested_url(self, base_url, sync, rtype, register_name, cnc_info):
        if not sync:
            if 'cnc' in rtype:
                res = '{}/{}/{}/async'.format(
                    base_url,
                    cnc_info['name'],
                    rtype
                )
            else:
                res = '{}/{}/async/{}'.format(
                    base_url,
                    rtype,
                    register_name
                )
        else:
            if 'cnc' in rtype:
                res = '{}/{}/{}'.format(
                    base_url,
                    cnc_info['name'],
                    rtype
                )
            else:
                res = '{}/{}/{}'.format(
                    base_url,
                    rtype,
                    register_name
                )
        return res

    def _get_supported_reports(self, cursor, uid, context=None):
        res = []
        if not context.get('active_ids', False):
            return res
        url = '{}/supported_reports'.format(config['stg_url'])

        for report in eval(requests.get(url).text).items():
            if report[1][1] == 'reg_only' or report[1][1] == 'reg_n_cnc':
                res.append((report[0], stg.REPORT_TG_NAMES[report[0]]))
                self.SUPP_REPORTS.append((report[0], report[1][0]))
        res.sort(key=operator.itemgetter(1))
        return res

    def _get_supported_orders(self, cursor, uid, context=None):
        res = []
        if not context.get('active_ids', False):
            return res
        url = '{}/supported_orders'.format(config['stg_url'])

        for report in eval(requests.get(url).text).items():
            if report[1][1] == 'reg_only' or report[1][1] == 'reg_n_cnc':
                res.append((report[0], stg.ORDER_TG_NAMES[report[0]]))
                self.SUPP_ORDERS.append((report[0], report[1][0]))
        res.sort(key=operator.itemgetter(1))
        return res

    def onchange_mode(self, cursor, uid, ids, mode_selection, context=None):
        if mode_selection == 'REP':
            return {
                'value': {
                    'info': 'Report request might be synchronous or asyncrhonous. '
                            'Select a report.'
                }
            }
        else:
            sync = False
            return {
                'value': {
                    'sync': sync,
                    'info': 'Orders are always asynchronous, it will be executed'
                            ' as soon as possible after a succesfull connection'
                            ' check with the concentrator'
                }
            }


    def onchange_report(self, cursor, uid, ids, report_type, context=None):
        sync = False
        if self.get_report_synchronicity(report_type) == 'sync':
            sync = True

        return {
            'value': {
                'sync': sync,
                'info': 'The report selected is synchronous. It '
                        'won\'t be sent to the FTP, instead it will '
                        'be available to download from this very '
                        'wizard once its ready.' if sync
                else 'The report selected is asynchronous. It '
                     'will be uploaded to the FTP once its ready.'
            }
        }


    @job(queue='do_request')
    def do_request(self, cursor, uid, register_name, register_cnc_id, wiz_id, context):
        error = ''
        info = ''
        # Build url and send request
        cnc_obj = self.pool.get('tg.concentrator')
        wizard = self.browse(cursor, uid, wiz_id, context=context)
        cnc_info = cnc_obj.read(cursor, uid, register_cnc_id,
                                ['name', 'stg_ws_ip_address'])
        base_url = config['stg_url']
        req_params = stg.build_url_parameters(from_date=wizard.from_date,
                                              to_date=wizard.to_date,
                                              source=wizard.source)
        filename = ''
        sync = self.get_report_synchronicity(wizard.report_type)
        if sync == 'async':
            wizard.sync = False
        else:
            wizard.sync = True
            filename = '{}_{}_{}'.format(register_name,
                                         wizard.report_type,
                                         str(datetime.now()).translate(
                                             None, " :/-"),
                                         )
        report_url = self.build_requested_url(base_url, wizard.sync,
                                              wizard.report_type,
                                              register_name, cnc_info)
        if req_params:
            report_url = '{}{}'.format(report_url, req_params)
        res = self.send_request(wizard, report_url)

        # Store information for the user
        if res is None:
            error = register_name + str(
                _(' - Error requesting the report, DC %s not responding.\n' % (cnc_info.get('name'))))
        else:
            if sync == 'sync':
                info = register_name + str(
                    _(' - DC %s  responding. Requested report available to download.\n' % (cnc_info.get('name'))))
            else:
                info = register_name + str(
                    _(' - DC %s  responding. Requested report will be available at ftp.\n' % (cnc_info.get('name'))))
        return filename, res, error, info

    def action_request_report(self, cursor, uid, ids, context=None):
        errors = ''
        infos = ''
        if not context:
            context = {}

        register_obj = self.pool.get('giscedata.registrador')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        # Fill active_ids with register id if we execute the wizard from model giscedata.lectures.comptador
        # Keep context active_ids if we execute the wizard from model giscedata.registrador
        active_ids = []
        if context.get('from_model') == 'giscedata.registrador':
            active_ids = context.get('active_ids')
        elif context.get('from_model') == 'giscedata.lectures.comptador':
            register_info = meter_obj.read(cursor, uid, context.get('active_ids'), ['registrador_id'])
            for register in register_info:
                active_ids.append(register.get('registrador_id')[0])

        # Read registers and prepare zip file on memory
        register_list = register_obj.read(cursor, uid, active_ids, [])
        zip_ios = StringIO.StringIO()
        z = zipfile.ZipFile(zip_ios, 'w', compression=zipfile.ZIP_DEFLATED)

        # Fill pool with jobs that request the report to the concentrator
        wiz = self.browse(cursor, uid, ids, context)[0]
        wiz_j_pool = ProgressJobsPool(wiz)
        with NoDependency():
            for register in register_list:
                job = self.do_request(cursor, uid, register['name'], register['cnc_id'][0], ids[0],
                                      context)
                wiz_j_pool.add_job(job)

        # Start workers and wait till all jobs are done
        aw = AutoWorker(queue='do_request', default_result_ttl=24 * 3600)
        aw.work()
        wiz_j_pool.join()

        # Read results and build zip
        for job, values in tqdm(
                wiz_j_pool.results.items(), desc='Building ZIP'):
            filename, res, error, info = values
            if res:
                if res != 'async':
                    z.writestr(filename, res)
                infos = infos + info
            else:
                errors = errors + error
        z.close()
        zip_filename = '{}_{}.zip'.format(wiz.report_type, str(datetime.now()).translate(None, " :/-"))

        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({'file': base64.b64encode(zip_ios.getvalue()),
                      'filename': zip_filename,
                      'info': infos,
                      'error': errors})

    _columns = {
        'report_type': fields.selection(_get_supported_reports, 'Report'),
        'order_type': fields.selection(_get_supported_orders, 'Order'),
        'mode_selection': fields.selection(stg.MODE, 'Mode selection', size=2,
                                           help=_(
                                               'REP: Send a report request\n'
                                               'ORD: Send an order\n'
                                           )),
        'sync_reports': fields.boolean('Report only sync'),
        'from_date': fields.datetime('Date from'),
        'to_date': fields.datetime('Date to'),
        'source': fields.selection(stg.DATA_SOURCES, 'Source', size=3,
                                   help=_(
                                       'DCC: If data exists in DC deliver it, '
                                       'otherwise, ask the meter\n'
                                       'DCF: (Default) Deliver only data which '
                                       'is in the DC, if data is not there, '
                                       'consider data is missing\n'
                                       'MET: Force reading from meter'
                                   )),
        'sync': fields.boolean('Synchrony', help=_('Requested synchronous '
                                                   'reports aren\'t uploaded '
                                                   'to the FTP')),
        'filename': fields.char('Name', size=256),
        'file': fields.binary('File'),
        'export_progress': fields.float('Progress'),
        'info': fields.text('Info'),
        'error': fields.text('Errors'),
    }

    _defaults = {
        'sync': lambda *a: False,
        'to_date': lambda *a: datetime.strftime(datetime.now(),
                                                '%Y-%m-%d %H:%M:%S'),
        'export_progress': lambda *x: 0
    }


ActiveSTGRequest()


class ActiveSTGMeterRequest(osv.osv_memory):
    """
        Wizard to request reports for meters from TG systems.
    """
    _name = 'wizard.stg.meter.requests'
    _inherit = 'wizard.stg.requests'

    _columns = {
        'meter_id': fields.many2many('giscedata.lectures.comptador', 'comptador_wizard', 'id', 'id', 'Meter',
                                     required=True)
    }

    _defaults = {
        'meter_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')
    }


ActiveSTGMeterRequest()


class ActiveSTGRegisterRequest(osv.osv_memory):
    """
        Wizard to request reports for registers from TG systems.
    """
    _name = 'wizard.stg.register.requests'
    _inherit = 'wizard.stg.requests'

    _columns = {
        'register_id': fields.many2many('giscedata.registrador', 'registrador_wizard', 'id', 'id', 'Register',
                                        required=True)
    }

    _defaults = {
        'register_id': lambda cursor, uid, ids, context: context.get(
            'active_ids')
    }


ActiveSTGRegisterRequest()
