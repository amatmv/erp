from osv import osv, fields
from datetime import datetime
from tools.translate import _
from tools import config
import requests
import operator
import giscedata_telegestio_stg.giscedata_telegestio_stg as stg


class ActiveSTGAllDCRequest(osv.osv_memory):
    """
    Wizard to request reports from TG to every concentrator available.
    """

    _name = 'wizard.stg.all.dc.requests'

    def action_all_dc_request_report(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        cnc_obj = self.pool.get('tg.concentrator')
        wizard = self.browse(cursor, uid, ids[0], context=context)

        cnc_ids = cnc_obj.search(cursor, uid, [])
        cnc_info = cnc_obj.read(cursor, uid, cnc_ids,
                                ['name', 'stg_ws_ip_address'])
        base_url = config['stg_url']

        req_params = stg.build_url_parameters(from_date=wizard.from_date,
                                              to_date=wizard.to_date,
                                              source=wizard.source)

        cnc_with_error = []
        # Send a request to every dc
        for cnc in cnc_info:
            # Send to all registers on the dc
            if wizard.mode_selection == 'REP':
                report_url = '{}/{}/{}/async'.format(
                    base_url,
                    cnc['name'],
                    wizard.report_type,
                )
            else:
                if wizard.order_type == 'order-request':
                    report_url = '{}/{}/{}/{}/async'.format(
                        base_url,
                        cnc['name'],
                        wizard.order_type,
                        wizard.b11_order,
                    )
            if req_params:
                report_url = '{}{}'.format(report_url, req_params)
            r = requests.get(report_url)
            if r.status_code != 200:
                cnc_with_error.append(cnc['name'])

        if not cnc_with_error:
            if wizard.mode_selection == 'REP':
                wiz_info = _('The report has been requested to the {} '
                             'concentrator. It will be uploaded to the FTP server '
                             'once its ready.').format(cnc_info['name'])
            else:
                wiz_info = _('Order received by {} concentrator.').format(cnc_info['name'])
        else:
            wiz_info = _('ERROR: These list of concentrators are not '
                         'responding correctly: ')
            for cnc in cnc_with_error:
                wiz_info = "{0}{1}{2}".format(wiz_info, cnc, '  ')

        wizard.write({'info': wiz_info}, context=context)

    def _get_supported_reports(self, cursor, uid, context=None):
        res = []
        if not context.get('active_ids', False):
            return res
        url = '{}/supported_reports'.format(config['stg_url'])

        for report in eval(requests.get(url).text).items():
            if report[1][0] == 'async' and report[1][1] == 'cnc_only' or \
                    report[1][0] == 'async' and report[1][1] == 'reg_n_cnc':
                res.append((report[0], stg.REPORT_TG_NAMES[report[0]]))
        res.sort(key = operator.itemgetter(1))
        return res

    def _get_supported_orders(self, cursor, uid, context=None):
        res = []
        if not context.get('active_ids', False):
            return res
        url = '{}/supported_orders'.format(config['stg_url'])

        for order in eval(requests.get(url).text).items():
            if order[1][0] == 'async' and order[1][1] == 'cnc_only' or \
                    order[1][0] == 'async' and order[1][1] == 'reg_n_cnc':
                res.append((order[0], stg.ORDER_TG_NAMES[order[0]]))
        res.sort(key=operator.itemgetter(1))
        return res

    def onchange_mode(self, cursor, uid, ids, mode_selection, context=None):
        if mode_selection == 'REP':
            return {
                'value': {
                    'info': 'Report request might be synchronous or asyncrhonous. '
                            'Select a report.'
                }
            }
        else:
            sync = False
            return {
                'value': {
                    'sync': sync,
                    'info': 'Orders are always asynchronous, it will be executed'
                            ' as soon as possible after a succesfull connection'
                            ' check with the concentrator'
                }
            }

    _columns = {
        'report_type': fields.selection(_get_supported_reports, 'Report'),
        'order_type': fields.selection(_get_supported_orders, 'Order'),
        'mode_selection': fields.selection(stg.MODE, 'Mode selection', size=2,
                                           help=_(
                                               'REP: Send a report request\n'
                                               'ORD: Send an order\n'
                                           )),
        'b11_order': fields.selection(stg.B11_ORDERS, 'B11 Order', size=2,
                                      help=_(
                                          'T01: Reboot of the DC\n'
                                          'T02: This implies that all running reports '
                                          '(due to scheduled tasks or STG report requests) '
                                          'are stopped or cancelled.\n'
                                          'T03: Synchronize meter time according to parameter '
                                          'TimeDev and TimeDevOver.\n'
                                          'T04: Perform a Clear of meters database in DC '
                                          'that are in Permanent Failure state.\n'
                                          'T05: Force time synchronisation in DC.\n'
                                          # 'T06: Clean meter passwords in DC database.\n'
                                          'T07: Force meter synchronisation.'
                                      )),
        'from_date': fields.datetime('Date from'),
        'to_date': fields.datetime('Date to'),
        'source': fields.selection(stg.DATA_SOURCES, 'Source',
                                   size=3, required=True,
                                   help=_(
                                       'DCC: If data exists in DC deliver it, '
                                       'otherwise, ask the meter\n'
                                       'DCF: (Default) Deliver only data which '
                                       'is in the DC, if data is not there, '
                                       'consider data is missing\n'
                                       'MET: Force reading from meter'
                                   )),
        'info': fields.text('Info'),
    }

    _defaults = {
        'to_date': lambda *a: datetime.strftime(datetime.now(),
                                                '%Y-%m-%d %H:%M:%S'),
        'info': lambda *a: _('It will request the selected report to every '
                             'meter.'),
    }


ActiveSTGAllDCRequest()
