# -*- coding: utf-8 -*-
{
    "name": "CNMC INF/DE/0066/14 Distribuïdora",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Generació de fitxer CSV amb el canvis de comercialitzadora durant l'any 2013
    segons CNMC INF/DE/0066/14 a partir de les modificacions contractuals per
    empreses distribuïdores
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa_distri",
        "giscedata_administracio_publica_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_inf_de_006614_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
