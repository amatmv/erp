# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_cnmc_infde006614_distri
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2015-03-30 13:59:41+0000\n"
"PO-Revision-Date: 2015-03-30 13:59:41+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_cnmc_infde006614_distri
#: field:wizard.cnmc.infde006614,name:0
msgid "Nom"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: model:ir.actions.act_window,name:giscedata_cnmc_infde006614_distri.action_wizard_inf_de_006614
#: model:ir.ui.menu,name:giscedata_cnmc_infde006614_distri.menu_wizard_inf_de_006614
msgid "Informe INF/DE/0066/14"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: field:wizard.cnmc.infde006614,state:0
msgid "Estat"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: view:wizard.cnmc.infde006614:0
msgid "Cancel·lar"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: view:wizard.cnmc.infde006614:0
msgid "Tancar"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: model:ir.module.module,description:giscedata_cnmc_infde006614_distri.module_meta_information
msgid "Aquest mòdul afegeix les següents funcionalitats:\n"
"  * Generació de fitxer CSV amb el canvis de comercialitzadora durant l'any 2013\n"
"    segons CNMC INF/DE/0066/14 a partir de les modificacions contractuals per\n"
"    empreses distribuïdores\n"
"  "
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: field:wizard.cnmc.infde006614,fitxer:0
msgid "Fitxer"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: model:ir.module.module,shortdesc:giscedata_cnmc_infde006614_distri.module_meta_information
msgid "CNMC INF/DE/0066/14 Distribuïdora"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: model:ir.model,name:giscedata_cnmc_infde006614_distri.model_wizard_cnmc_infde006614
msgid "wizard.cnmc.infde006614"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: view:wizard.cnmc.infde006614:0
msgid "Informes CUPS"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: field:wizard.cnmc.infde006614,any:0
msgid "Any"
msgstr ""

#. module: giscedata_cnmc_infde006614_distri
#: view:wizard.cnmc.infde006614:0
msgid "Exportar"
msgstr ""

