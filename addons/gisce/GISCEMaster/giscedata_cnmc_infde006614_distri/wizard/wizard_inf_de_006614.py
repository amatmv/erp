# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config
from datetime import date
import csv
import StringIO
import base64


class GiscedataCNMCinfde006614(osv.osv_memory):
    '''Assistent per generar el CSV de la INF/DE/0066/14 de la CNMC d'un any'''

    _name = 'wizard.cnmc.infde006614'

    def genera_informe_sql(self, cursor, uid, ids, context=None):
        '''Genera CSV'''
        if not context:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context=context)

        codi_r1 = self.pool.get('res.users').browse(
            cursor, uid, uid, context).company_id.codi_r1


        sql_file = 'canvis.sql'
        sql = open('%s/%s/sql/%s' % (config['addons_path'],
                                     'giscedata_cnmc_infde006614_distri',
                                     sql_file)).read()
        cursor.execute(sql, {'inici': date(wiz.any, 1, 1),
                             'fi': date(wiz.any, 12, 31)})

        filename = 'IVC_R1-%s_%s.csv' % (codi_r1, wiz.any)

        output = StringIO.StringIO()
        writer = csv.writer(output)

        for row in cursor.fetchall():
            writer.writerow(row[:4])

        fitxer = base64.b64encode(output.getvalue())

        wiz.write({'fitxer': fitxer, 'name': filename})

        output.close()
        return True

    _columns = {
        'state': fields.char('Estat', 10),
        'name': fields.char('Nom', size=256),
        'any': fields.integer('Any'),
        'fitxer': fields.binary('Fitxer')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'any': lambda *a: 2013,
    }

GiscedataCNMCinfde006614()