select 'R1-' || company.codi_r1,cups.name as cups , to_char(m1.data_inici, 'YYYYMMDD') as data_inici, comer.ref2,
  m1.comercialitzadora as comer_actual, m2.comercialitzadora as comer_ant, m1.name as mod_con
FROM giscedata_polissa_modcontractual AS m1
LEFT JOIN giscedata_polissa_modcontractual AS m2 on (m1.modcontractual_ant = m2.id and m1.comercialitzadora != m2.comercialitzadora)
LEFT JOIN giscedata_cups_ps AS cups ON cups.id=m1.cups
LEFT JOIN res_partner AS comer ON m1.comercialitzadora=comer.id
LEFT JOIN res_partner AS distri ON cups.distribuidora_id=distri.id
LEFT JOIN res_company AS company ON company.partner_id=distri.id
WHERE m1.data_inici>=%(inici)s and m1.data_inici<=%(fi)s
      and m2.comercialitzadora is not null
ORDER BY cups.name