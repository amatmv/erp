Documentació del mòdul per generar el fixer CSV segons CNMC INF/DE/0066/44 per distribuïdora
============================================================================================

===========
Introducció
===========

Per poder fer una auditoria de canvis de comercialitzadora, la CNMC demana un
CSV amb el format definit a INF/DE/0066/14

=========
Generació
=========

Per generar el fitxer, hem d'executar l'assistent que trobarem a
`Administració pública > CNMC > Informe INF/DE/0066/14`

.. image:: _static/menu.png


Només caldrà introduïr l'any del qual es vol fer l'informe, que per defecte ja
és 2013 i prèmer el botó **Exportar**.

Des de **Fitxer** es podrà obrir el CSV i descarregar-lo a disc

.. image:: _static/assistent.png

-----------------
Dades utilitzades
-----------------

Per omplir el fitxer es tenen en compte tots els canvis de comercialitzadora
registrats com a modificació contractual amb data de inici dins l'any escollit.
Per fer-ho es comprova que la modificació anterior té una comercialitzadora
diferent a la que s'està analitzant.

Així doncs, el fitxer generat serà correcte si els canvis de comercialitzadora
es registren com a modificació contractual de la pòlissa correctament.

+-----------------------------+-----------------------------------------------+
| **CODIGO_INFORMANTE**       | Camp `Codi R1` de la companyia                |
+-----------------------------+-----------------------------------------------+
| **CODIGO_CUPS**             | CUPS                                          |
+-----------------------------+-----------------------------------------------+
| **FECHA_ACTIVACION**        | Data inici de la modificació contractual nova |
+-----------------------------+-----------------------------------------------+
| **CODIGO_COMERCIALIZADORA** | Camp `Ref2` amb el codi R2                    |
+-----------------------------+-----------------------------------------------+


.. note::
   :name: notar2

   Codi R2 (R2-xxx) de la CNMC gestionable en el camp `Ref2` de la fitxa de
   l'empresa comercialitzadora accessible des del menú `Empreses`