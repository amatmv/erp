# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    """Migration to 2.21.0
    """
    pool = pooler.get_pool(cursor.dbname)
    cups_obj = pool.get('giscedata.cups.ps')
    uid = 1
    logger = netsvc.Logger()
    ids = cups_obj.search(cursor, uid, [('id_poblacio', '!=', False)],
                          context={'active_test': False})
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         "Updating %s CUPS" % len(ids))
    for cups in cups_obj.read(cursor, uid, ids, ['pnp']):
        cups_obj.write(cursor, uid, [cups['id']], {'pnp': cups['pnp']},
                       context={'sync': False, 'log': False})
    logger.notifyChannel('migration', netsvc.LOG_INFO, "Done")
