# -*- coding: utf-8 -*-

"""Executem el backup de les dades
"""
import time

import netsvc
import pooler

from osv import osv

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor \
     import GisceUpgradeMigratoor, log

def migrate(cursor, installed_version):
    """Cridem a la llibreria de migració per aquest model
    """
    gm = GisceUpgradeMigratoor('giscedata_cups', cursor)
    log('Filling columns')
    uid = 1
    char_mapping = {
        'poligon': ('cpo', 4),
        'parcela': ('cpa', 4),
        'tipovia': ('tv', False),
        'carrer': ('nv', False),
        'numero': ('pnp', 4),
        'pis': ('pt', 4),
        'porta': ('pu', 4),
        'cp': ('dp', 4),
    }
    pool = pooler.get_pool(cursor.dbname)
    conf = pool.get('res.config')
    conf.set(cursor, uid, 'check_cups', '0')
    cups_obj = pool.get('giscedata.cups.ps')
    context = {'active_test': False}
    log('Copying data')
    cups_ids = cups_obj.search(cursor, uid, [], context=context)
    for cups in cups_obj.read(cursor, uid, cups_ids, ['ref_catastral']):
        if cups['ref_catastral']:
            cups_o = cups_obj.browse(cursor, uid, cups['id'])
            try:
                log('Llegint dades del cadastre del CUPS %s' % (cups_o.name,))
                cups_o.update_dir_from_ref()
                updated = True
            except Exception, e:
                print(e)
                log("No s'ha pogut llegir les dades del cadastre",
                    level=netsvc.LOG_WARNING)

        cups = cups_obj.read(cursor, uid, cups['id'], ['name'] +
                             char_mapping.keys())
        log('Copiant dades del CUPS %s' % (cups['name'],),
            level=netsvc.LOG_DEBUG)
        vals = {}
        mfields = gm.get_local_fields('giscedata_cups_ps')
        for old in char_mapping:
            if old not in mfields:
                continue
            new_name = char_mapping[old][0]
            new_size = char_mapping[old][1]
            if not new_size:
                sql = """UPDATE giscedata_cups_ps SET %s = %s
                      WHERE id = %s""" % (new_name, old, cups['id'])
            if new_size:
                sql = """UPDATE giscedata_cups_ps SET %s = 
                      substring(%s from 1 for %s)
                      WHERE id = %s""" % (new_name, old, new_size, cups['id'])
            cursor.execute(sql)
        res = cups_obj._direccio(cursor, uid, [cups['id']], 'direccio', False)
        cursor.execute("""UPDATE giscedata_cups_ps set direccio = %s
        WHERE id = %s""", (res[cups['id']], cups['id']))
    conf.set(cursor, uid, 'check_cups', '1')

