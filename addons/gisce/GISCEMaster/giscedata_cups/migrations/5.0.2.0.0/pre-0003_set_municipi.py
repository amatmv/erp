# -*- coding: utf-8 -*-

import pooler
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log


def migrate(cursor, installed_version):
    uid = 1
    imd_obj = pooler.get_pool(cursor.dbname).get('ir.model.data')
    id_desconegut = imd_obj.read(cursor, uid,
                                 [imd_obj._get_id(cursor, uid, 'base_extended',
                                                  'ine_99999')], ['res_id']
                     )[0]['res_id']
    cursor.execute("""update giscedata_cups_ps set id_municipi = %s
                      where id_municipi is null""", (id_desconegut,))
    log("Actualitzats %s CUPS a municipi desconegut." % cursor.rowcount)
