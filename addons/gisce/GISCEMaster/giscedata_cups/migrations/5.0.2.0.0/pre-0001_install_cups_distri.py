# -*- coding: utf-8 -*-

"""Executem el backup de les dades
"""

import netsvc
import pooler

from osv import osv

def migrate(cursor, installed_version):
    """Cridem a la llibreria de migració per aquest model
    """

    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)
    pool = pooler.get_pool(cursor.dbname)
    module_obj = pool.get('ir.module.module')
    search_params = [('name', '=', 'giscedata_cups_distri')]
    module_id = module_obj.search(cursor, 1, search_params)
    if module_id:
        module_obj.button_install(cursor, 1, module_id)
    else:
        osv.except_osv('Error', "No s'ha trobat el mòdul "
                                "giscedata_cups_distri")

