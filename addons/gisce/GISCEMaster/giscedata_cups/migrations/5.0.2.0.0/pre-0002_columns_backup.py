# -*- coding: utf-8 -*-

"""Executem el backup de les dades
"""

import netsvc
import pooler

from osv import osv

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor

def migrate(cursor, installed_version):
    """Cridem a la llibreria de migració per aquest model
    """

    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Backing up columns')
    mig = GisceUpgradeMigratoor('giscedata_cups', cursor)
    mig.backup(suffix='v4')

