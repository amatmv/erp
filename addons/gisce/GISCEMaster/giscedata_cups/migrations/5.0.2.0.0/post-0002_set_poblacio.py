# -*- coding: utf-8 -*-

import pooler
from fuzzywuzzy import process
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log


def migrate(cursor, installed_version):
    log("Migrating poblacions...")
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    pob_obj = pool.get('res.poblacio')
    cups_obj = pool.get('giscedata.cups.ps')
    conf = pool.get('res.config')
    conf.set(cursor, uid, 'check_cups', '0')
    existing_pobs = {}
    muni_desc = pool.get('res.municipi').search(cursor, uid, [
        ('ine', '=', '99999')
    ])[0]
    for pob in pob_obj.read(cursor,
                            uid,
                            pob_obj.search(cursor, uid, []),
                            ['name']):
        existing_pobs[pob['name']] = pob['id']

    cursor.execute("select distinct poblacio, id_municipi from "
                   "giscedata_cups_ps")
    pobs = [(a[0], a[1] or muni_desc) for a in cursor.fetchall()]
    for pob, muni in pobs:
        res = process.extractOne(pob, existing_pobs.keys()) or ('', 0)
        log(u"Població: %s Match %s amb %s" % (pob, res[1], res[0]))
        if res and res[1] >= 95:
            id_poblacio = existing_pobs[res[0]]
        else:
            if pob in existing_pobs:
                id_poblacio = existing_pobs[pob]
            else:
                id_poblacio = pob_obj.create(cursor, uid, {'name': pob,
                                                       'municipi_id': muni})
            log(u"Creem nova població: %s (id:%s)" % (pob, id_poblacio))
            existing_pobs[pob] = id_poblacio
        cursor.execute("select id from giscedata_cups_ps where poblacio "
                       "= %s", (pob,))
        cups_ids = [a[0] for a in cursor.fetchall()]
        cups_obj.write(cursor, uid, cups_ids, {'id_poblacio': id_poblacio})
    conf.set(cursor, uid, 'check_cups', '1')
