# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    logger = netsvc.Logger()
    logger.notifyChannel('migrations', netsvc.LOG_INFO,
                         "Migrant el camp planta del giscedata_cups_ps")

    cursor.execute("""select count(*) as taulacupsv4
                      from information_schema.tables where
                      table_name like 'giscedata_cups_ps_v4';""")
    taulacupsv4 = cursor.dictfetchall()[0]['taulacupsv4']
    if taulacupsv4:
        cursor.execute("""update giscedata_cups_ps set pt = trim(v4.pis)
                          from giscedata_cups_ps as v5,
                          giscedata_cups_ps_v4 as v4
                          where v5.pt != v4.pis and v4.pis is not null
                          and v5.pt like substring(trim(v4.pis)
                          from 1 for 4);""")
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                                u"realitzat correctament")
    else:
        cursor.execute("""select exists(select column_name
                          from INFORMATION_SCHEMA.columns
                          where table_name='giscedata_cups_ps'
                          and column_name = 'pis_v4');""")
        existeix = cursor.dictfetchall()[0]['?column?']

        if existeix:
            cursor.execute("""update giscedata_cups_ps set pt = trim(pis_v4)
                              where pt != pis_v4 and pis_v4 is not null
                              and pt like substring(trim(pis_v4)
                              from 1 for 4);""")
            logger.notifyChannel('migration', netsvc.LOG_INFO,
                                    u"Realitzat correctament")
        else:
            logger.notifyChannel('migration', netsvc.LOG_INFO,
                                    u"No s'ha trobat el camp pis_v4 a la taula "
                                    u"giscedata_cups_ps")