# -*- coding: utf-8 -*-

import netsvc
import pooler

def migrate(cursor, installed_version):
    """Migration to 2.19.0
    """
    pool = pooler.get_pool(cursor.dbname)
    cups_obj = pool.get('giscedata.cups.ps')
    uid = 1
    logger = netsvc.Logger()
    
    #Update cups manager group with the same users in base.group_user
    group_obj = pool.get('res.groups')
    model_data_obj = pool.get('ir.model.data')
    #Search cups manager group id
    search_params = [('module', '=', 'giscedata_cups'),
                     ('name', '=', 'group_cups_manager')]
    model_data_ids = model_data_obj.search(cursor, uid, search_params)
    group_cups_id = model_data_obj.read(cursor, uid,
                                        model_data_ids)[0]['res_id']
    #Search group_user id
    search_params = [('module', '=', 'base'),
                     ('name', '=', 'group_user')]
    model_data_ids = model_data_obj.search(cursor, uid, search_params)
    group_base_id = model_data_obj.read(cursor, uid,
                                        model_data_ids)[0]['res_id']
    #Assign users from base_users to cups_manager group
    base_users = group_obj.read(cursor, uid, group_base_id, ['users'])['users']
    group_obj.write(cursor, uid, group_cups_id,
                    {'users': [(6, 0, base_users)]})
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')