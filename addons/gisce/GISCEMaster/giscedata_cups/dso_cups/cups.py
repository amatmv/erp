# -*- coding: utf-8 -*-

REE_CODES_TRANSLATION = {
    # Solanar -> Distribucion cantabrico
    '0463': '0026',
    '0492': '0026',
    # Bakaikuko Argia -> Electra Saltea
    '0584': '0198',
    # Electra del Jallas -> Fenosa
    '0390': '0022'
}

CUPS_CODES_TRANSLATION = {
    '0021': [
        {
            'codi': [
                'ES0021000018487060YW0F',
                'ES0021000002134366HD0F',
                'ES0021000015610630VK0F',
                'ES0021000015610710KD0F',
             ],
            'dso': '0172'
        }
    ]
}


def get_dso(cups):
    distri = cups[2:6]
    for c in CUPS_CODES_TRANSLATION.get(distri, []):
        if cups in c['codi']:
            return c['dso']
    return REE_CODES_TRANSLATION.get(distri, distri)
