# -*- coding: utf-8 -*-
"""Classes pel mòdul giscedata_cups (General)."""
from dso_cups.cups import get_dso
from osv import osv, fields
from osv.orm import OnlyFieldsConstraint
from tools import cache
from tools.translate import _
from datetime import datetime
import re
import netsvc
import logging

logger = netsvc.Logger()


class GiscedataCupsPs(osv.osv):
    """Classe d'un CUPS (Punt de servei)."""

    _name = 'giscedata.cups.ps'
    _cups_checksum_table = 'TRWAGMYFPDXBNJZSQVHLCKE'

    @cache(timeout=600)
    def whereiam(self, cursor, uid, context=None):
        '''retorna si estem a distri o a comer depenent de si el
            cups pertany a la nostra companyia o no'''

        user_obj = self.pool.get('res.users')

        cups_id = self.search(
            cursor, uid, [('distribuidora_id', '!=', '')], limit=1,
            context=context
        )[0]

        cups = self.browse(cursor, uid, cups_id)
        user = user_obj.browse(cursor, uid, uid)

        distri_id = cups.distribuidora_id.id
        comp_id = user.company_id.partner_id.id

        if distri_id == comp_id:
            return 'distri'
        return 'comer'

    def gen_checksum(self, cursor, uid, cupsname):
        """Calcula el checksum d'un CUPS."""
        try:
            rest0 = int(cupsname) % 529
        except ValueError:
            raise osv.except_osv("Error", "El CUPS no és vàlid.")
        coficient = int(rest0 / 23)
        rest1 = rest0 % 23

        checksum = '%s%s' % (self._cups_checksum_table[coficient],
                             self._cups_checksum_table[rest1])
        return checksum

    def check_cups_code(self, cursor, uid, cupsname):
        tmp = cupsname[2:18]
        checksum = self.gen_checksum(cursor, uid, tmp)
        check = 'ES%s%s' % (tmp, checksum)
        # Només comprovem els primers 20 dígits del CUPS els dos últims
        # del final no surten del checksum
        if check != cupsname[:20]:
            return False
        return True

    def check_cups(self, cursor, uid, ids):
        """Comprova si un CUPS és correcte."""
        config = self.pool.get('res.config')
        try:
            check_cups = int(config.get(cursor, uid, 'check_cups', 1))
        except ValueError:
            check_cups = True
        if not check_cups:
            return True
        for cups in self.read(cursor, uid, ids, ['name']):
            if not self.check_cups_code(cursor, uid, cups['name']):
                msg = 'CUPS incorrecte: {0}'.format(cups['name'])
                logger.notifyChannel('check_cups', netsvc.LOG_INFO, msg)
                return False
        return True

    def check_unique_cups(self, cursor, uid, ids):
        """
        Comprova que el CUPS sigui unic sense mirar la terminacio

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Affected ids
        :return: Boolean
        """

        conf_obj = self.pool.get('res.config')
        cups_length = int(conf_obj.get(cursor, uid, 'check_cups_length', 20))

        for cups in self.read(cursor, uid, ids, ['name']):
            search_params = [('name', 'like', cups['name'][:cups_length])]
            # We can't use cups_exists() here because we already have the CUPS
            # in the database, so it will allways exist.
            # That's also why we use the > 1
            if self.search_count(cursor, uid, search_params) > 1:
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    msg = "El CUPS {} ja existeix".format(cups["name"])
                    sentry.client.captureMessage(msg, level=logging.WARNING)
                return False
        return True

    def cups_exists(self, cursor, uid, cups_name):
        """Comprova si el CUPS passat existeix a la base de dades sense
        mirar la terminacio"""
        search_params = [('name', 'like', cups_name[:20])]
        return bool(self.search_count(cursor, uid, search_params))

    def delete_dir_ref(self, cursor, uid, ids, context=None):
        """Elimina els camps emplenats segons la referència cadastral.
        """
        vals = {}
        for obj_field in self.fields_get(cursor, uid):
            if obj_field.startswith('catas_'):
                vals[obj_field] = False
        self.write(cursor, uid, ids, vals)
        return True

    def update_dir_from_ref(self, cursor, uid, ids, context=None):
        """Busca la direcció mitjançant la referència cadastral."""
        url_req = "/ovcservweb/OVCSWLocalizacionRC/OVCCallejeroCodigos.asmx/"
        url_req += "Consulta_DNPRC_Codigos"
        for cups in self.browse(cursor, uid, ids, context):
            res = {'value': {}, 'domain': {}}
            if (not cups.id_municipi or not cups.id_provincia
                or not cups.ref_catastral):
                raise osv.except_osv(_('Error'),
            _("S'han d'omplir els camps Provincia, Municipi i Ref. Cadastral"))
            import httplib
            import urllib
            import xml.dom.minidom
            from xml.dom.minidom import Node
            import StringIO
            headers = {"Content-type": "application/x-www-form-urlencoded",
                       "Accept": "text/plain"}
            params = urllib.urlencode([
                            ('CodigoProvincia', cups.id_provincia.code),
                            ('CodigoMunicipio', ''),
                            ('CodigoMunicipioINE', cups.id_municipi.ine[-3:]),
                            ('RC', cups.ref_catastral)], doseq=False)
            conn = httplib.HTTPConnection('ovc.catastro.meh.es:80')
            # Per posar debug
            conn.set_debuglevel(0)
            conn.request("POST", url_req, params, headers)
            resposta = conn.getresponse()
            resposta2 = resposta.read()
            respota_xml = StringIO.StringIO(resposta2)
            doc = xml.dom.minidom.parse(respota_xml)
            resultats = doc.getElementsByTagName("cudnp")

            if not len(resultats) == 1:
                raise osv.except_osv(_("Error"),
                                     _("No s'ha trobat cap resultat."))

            # TODO: Passar a xpath per no recorrer tantes vegades el bucle
            for node in doc.getElementsByTagName("dt"):
                for attr in node.childNodes:
                    if (attr.nodeType == Node.ELEMENT_NODE
                        and attr.tagName in ('np', 'nm')):
                        key = 'catas_%s' % attr.tagName
                        value = attr.childNodes[0].data
                        res['value'].update({key: value})

            for node in doc.getElementsByTagName("dir"):
                for attr in node.childNodes:
                    if (attr.nodeType == Node.ELEMENT_NODE
                        and attr.tagName in ('cv', 'tv', 'nv', 'pnp')):
                        key = 'catas_%s' % attr.tagName
                        value = attr.childNodes[0].data

                        res['value'].update({key: value})
            for node in doc.getElementsByTagName("loint"):
                for attr in node.childNodes:
                    if (attr.nodeType == Node.ELEMENT_NODE
                        and attr.tagName in ('es', 'pt', 'pu', 'bq')):
                        key = 'catas_%s' % attr.tagName
                        value = attr.childNodes[0].data
                        res['value'].update({key: value})

            for node in doc.getElementsByTagName("cpp"):
                for attr in node.childNodes:
                    if (attr.nodeType == Node.ELEMENT_NODE
                        and attr.tagName in ('cpo', 'cpa')):
                        key = 'catas_%s' % attr.tagName
                        value = attr.childNodes[0].data
                        res['value'].update({key: value})

            # Si es poligon/parcel·la eliminem lo anterior
            if ('catas_cpo' in res['value'].keys()
                or 'catas_cpa' in res['value'].keys()):
                for k in ('cv', 'tv', 'nv', 'pnp', 'es', 'pt', 'pu'):
                    if 'catas_%s' % k in res['value']:
                        del res['value']['catas_%s' % k]

            for node in doc.getElementsByTagName("dp"):
                key = 'catas_%s' % node.tagName
                value = node.childNodes[0].data
                res['value'].update({key: value})

            ldt = doc.getElementsByTagName("ldt")
            if len(ldt):
                res['value'].update({'catas_ldt': ldt[0].childNodes[0].data})

            conn.close()
            cups.write(res['value'])

    def copy_to_alternative(self, cursor, uid, ids, context=None):
        """Copia la direcció del cadastre a l'alternativa."""
        fields_to_read = ['catas_tv', 'catas_nv', 'catas_pnp', 'catas_es',
                  'catas_pt', 'catas_pu', 'catas_cpo', 'catas_cpa', 'catas_dp', 'catas_bq']
        tipovia = self.pool.get('res.tipovia')
        for cups in self.read(cursor, uid, ids, fields_to_read, context):
            cups_id = cups['id']
            del cups['id']
            tv_id = tipovia.search(cursor, uid,
                                   [('abr', '=', cups['catas_tv'])])
            if len(tv_id):
                cups['catas_tv'] = tipovia.browse(cursor, uid, tv_id)[0].id
            else:
                cups['catas_tv'] = False
            vals = {}
            for key, value in cups.items():
                vals[key.replace('catas_', '')] = value
            self.write(cursor, uid, cups_id, vals, context)

    def copy_from_principal(self, cursor, uid, ids, context=None):
        """Copia la direcció del principal a l'actual."""
        fields_to_read = ['tv', 'nv', 'pnp', 'es', 'pt', 'bq',
                          'pu', 'cpo', 'cpa', 'aclarador']
        for cups in self.read(cursor, uid, ids, fields_to_read, context):
            cups_id = cups['id']
            del cups['id']
            vals = {}
            for key, value in cups.items():
                if isinstance(value, (list, tuple)):
                    value = value[0]
                vals['act_%s' % key] = value
            self.write(cursor, uid, cups_id, vals, context)

    # TODO: Funció on_change_municipi per actualitzar el camp provincia
    def _get_cups_ids(self, cursor, uid, ids, context=None):
        return ids

    def _store_changes(self, cursor, uid, cups_id, field_name,
                      value, context=None):
        '''store in notes changes in field'''
        if not context:
            context = {}
        if not context.get('log', True):
            return True
        if isinstance(cups_id, (list, tuple)):
            cups_id = cups_id[0]
        user_obj = self.pool.get('res.users')
        today = datetime.strftime(datetime.now(), '%d-%m-%Y %H:%M')
        user = user_obj.browse(cursor, uid, uid)
        field_data = self.fields_get(cursor, uid, [field_name])
        field_desc = field_data[field_name]['string']
        cups_values = self.read(cursor, uid, cups_id, [field_name,
                                                       'notes'])
        # If no changes, do not do anything
        if value == cups_values[field_name]:
            return True
        notes = cups_values['notes'] or ''
        notes += _(u'***Cambios detectados***\n')
        notes += _(u'Fecha: %s, Usuario: %s (%s)\n' % (
            today, user.name, user.login))
        notes += u'• %s: %s → %s\n\n' % (field_desc,
                                      cups_values[field_name],
                                      value)
        super(GiscedataCupsPs, self).write(cursor, uid, [cups_id],
                                             {'notes': notes}, {'sync': False})
        return True

    def get_direccio_fields(self, cursor, uid, field_name, context=None):
        '''return fields for constructing direccio depending on field_name'''
        fields = {}
        if field_name == 'direccio':
            fields['fields_normal'] = ['nv', 'pnp', 'es', 'pt', 'pu', 'bq',
                                       'aclarador', 'dp']
            fields['fields_poligon'] = ['cpo', 'cpa']
            fields['fields_all'] = ['tv', 'id_municipi', 'id_poblacio']
            fields['field_tv'] = 'tv'
        return fields

    def _direccio(self, cursor, uid, ids, field_name, arg, context=None):
        """Genera un text amb l'adreça completa.
        """
        res = {}
        all_fields = self.get_direccio_fields(cursor, uid, field_name,
                                              context=context)
        fields_normal = all_fields['fields_normal']
        fields_poligon = all_fields['fields_poligon']
        fields_all = all_fields['fields_all']
        field_tv = all_fields['field_tv']
        for cups in self.read(cursor, uid, ids,
                              fields_normal + fields_poligon + fields_all,
                              context):
            vals = []
            if cups[field_tv]:
                tv_ = self.pool.get('res.tipovia').browse(cursor, uid,
                                                          cups[field_tv][0])
                vals.append(tv_.abr)
            for field in fields_poligon:
                if cups.get(field, False):
                    vals.append(cups[field])

            for field in fields_normal:
                if cups.get(field, False):
                    vals.append(cups[field])

            vals.append('(%s)' % (cups['id_poblacio']
                                  and cups['id_poblacio'][1]  or
                                  cups['id_municipi'][1]))
            res[cups['id']] = ' '.join(vals)
            self._store_changes(cursor, uid, cups['id'],
                                field_name, ' '.join(vals),
                                context=context)
        return res

    def get_modcontractual_intervals(self, cursor, uid, cups_id, data_inici,
                                     data_final, context=None):
        """
        Obté tots els intervals de la pòlissa amb la data de tall i les
        modificacions.

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param cups_id: Id of the CUPS to get the intervals
        :type cups_id: int
        :param data_inici: Start date to get the intervals
        :param data_final: End date to get the intervals
        :param context: Open ERP context
        :type context: dict
        :return: Intervals
        :rtype: list of dict
        """
        if isinstance(cups_id, (list, tuple)):
            cups_id = cups_id[0]
        if not context:
            context = {}
        context.update({'active_test': False})
        modcontractual_obj = self.pool.get('giscedata.polissa.modcontractual')
        dates_de_tall = {}
        search_params = [
            ('cups', '=', cups_id),
            '|',
            '|',
            '&',
            ('data_inici', '<=', data_inici),
            ('data_final', '>=', data_final),
            '&',
            ('data_inici', '>=', data_inici),
            ('data_inici', '<=', data_final),
            '&',
            ('data_final', '>=', data_inici),
            ('data_final', '<=', data_final),
        ]
        modc_ids = modcontractual_obj.search(cursor, uid, search_params,
                                             context=context,
                                             order="data_inici asc")
        modsf = []
        changes = {}
        dates = {}
        if len(modc_ids) > 1:
            # Per defecte qualsevol canvi és una nou interval
            ffields = context.get('ffields',
                                  self.fields_get(cursor, uid).keys())
            diffs = {}
            fields_to_read = ffields + ['data_inici', 'data_final']
            for modc in sorted(modcontractual_obj.read(cursor, uid, modc_ids,
                                                fields_to_read),
                               key=lambda k: k['data_inici']):
                # Si és la primera l'afegim
                if not diffs:
                    modsf.append(modc['id'])
                    dates.setdefault(modc['id'],
                                     (modc['data_inici'], modc['data_final']))
                    data_inici = modc['data_inici']
                for ffield in ffields:
                    # Comprovem si tenim inicialitzat:
                    if diffs.get(ffield, False):
                        if diffs.get(ffield, False) != modc.get(ffield, False):
                            if modc['id'] not in modsf:
                                modsf.append(modc['id'])
                            # Posem el camp a les modificacions
                            changes.setdefault(modc['id'], [])
                            changes[modc['id']].append(ffield)
                    diffs[ffield] = modc.get(ffield, False)
                if modc['id'] in changes:
                    # Si hi ha canvis afegim la nova modificació a les dates
                    dates[modc['id']] = (modc['data_inici'], modc['data_final'])
                else:
                    # Si no hi ha canvis agafem les dates de l'última
                    data_inici1, data_final1 = dates[modsf[-1]]
                    dates[modsf[-1]] = (min(data_inici1, modc['data_inici']),
                                         max(data_final1, modc['data_final']))
            modc_ids = modsf[:]
        for modc in sorted(modcontractual_obj.read(cursor, uid, modc_ids,
                                                   ['data_inici', 'data_final'],
                                                   context),
                           key=lambda k: k['data_inici']):
            data_tall = max(data_inici, modc['data_inici'])
            dates_de_tall[data_tall] = {'id': modc['id'],
                                        'changes': changes.get(modc['id'], []),
                                        'dates': dates.get(modc['id'],
                                                           (modc['data_inici'],
                                                            modc['data_final']))
                                        }
        return dates_de_tall

    def clean_name(self, cursor, uid, name, context=None):
        '''Cleans all space characters including
        carriage returns from name'''

        clean_name = re.sub('[\s]', '', name)
        return clean_name.upper()

    def create(self, cursor, uid, vals, context=None):

        if 'name' in vals:
            vals['name'] = self.clean_name(cursor, uid, vals['name'])
            ref = get_dso(vals['name'])
            partner_obj = self.pool.get('res.partner')
            search_params = [('ref', '=', ref)]
            partner_id = partner_obj.search(cursor, uid, search_params)
            if partner_id and not vals.get("distribuidora_id"):
                vals['distribuidora_id'] = partner_id[0]

        return super(GiscedataCupsPs,
                     self).create(cursor, uid, vals, context=context)

    def write(self, cursor, uid, ids, vals, context=None):

        if 'name' in vals:
            vals['name'] = self.clean_name(cursor, uid, vals['name'])

        return super(GiscedataCupsPs,
                     self).write(cursor, uid, ids, vals, context=context)

    _columns = {
        'name': fields.char('CUPS', size=25, required=True),
        'distribuidora_id': fields.many2one('res.partner', 'Distribuidora'),
        'active': fields.boolean('Actiu'),
        'id_municipi': fields.many2one('res.municipi', 'Municipi',
                                       required=True),
        'id_provincia': fields.related('id_municipi', 'state', type='many2one',
                                       relation='res.country.state',
                                       string="Provincia", store=False,
                                       readonly=True),
        'aclarador': fields.char('Aclarador', size=256),
        'id_poblacio': fields.many2one('res.poblacio', 'Població'),
        'catas_cv': fields.char('Código Via (c)', size=10, readonly=True),
        'catas_tv': fields.char('Tipus Via (c)', size=10, readonly=True),
        'catas_nv': fields.char('Carrer (c)', size=256, readonly=True),
        'catas_pnp': fields.char('Número (c)', size=4, readonly=True),
        'catas_bq': fields.char('Bloc (c)', size=4, readonly=True),
        'catas_es': fields.char('Escala (c)', size=4, readonly=True),
        'catas_pt': fields.char('Planta (c)', size=4, readonly=True),
        'catas_pu': fields.char('Porta (c)', size=4, readonly=True),
        'catas_cpo': fields.char('Poligon (c)', size=4, readonly=True),
        'catas_cpa': fields.char('Parcel·la (c)', size=4, readonly=True),
        'catas_ldt': fields.char('Domicili tributari (c)', size=256,
                                 readonly=True),
        'catas_dp': fields.char('Codi Postal (c)', size=5, readonly=True),
        'catas_np': fields.char('Provincia (c)', size=256, readonly=True),
        'catas_nm': fields.char('Municipi (c)', size=256, readonly=True),
        'ref_catastral': fields.char('Ref Cadastral (c)', size=20),
        'tv': fields.many2one('res.tipovia', 'Tipus Via'),
        'nv': fields.char('Carrer', size=256),
        'pnp': fields.char('Número', size=9),
        'bq': fields.char('Bloc', size=4),
        'es': fields.char('Escala', size=4),
        'pt': fields.char('Planta', size=12),
        'pu': fields.char('Porta', size=4),
        'cpo': fields.char('Poligon', size=4),
        'cpa': fields.char('Parcel·la', size=4),
        'dp': fields.char('Codi Postal', size=5),
        'direccio': fields.function(_direccio, method=True,
                                    string="Direcció Principal",
                                    store={'giscedata.cups.ps': (_get_cups_ids,
                                           ['tv', 'nv', 'pnp', 'es', 'pt',
                                            'pu', 'cpo', 'cpa', 'dp',
                                            'id_municipi', 'id_poblacio',
                                            'aclarador'], 10)},
                                            type='char', size=256),
        'notes': fields.text('Canvis', readonly=True),
        'potencia_adscrita': fields.float("Potència adscrita (kW)",
                                          readonly=False)
    }

    _defaults = {
        'active': lambda *a: 1,
    }

    _constraints = [
        OnlyFieldsConstraint(check_cups, "El codi del CUPS no és correcte.", ["name"]),
        OnlyFieldsConstraint(check_unique_cups, "Aquest Codi de CUPS ja existeix", ["name"]),
    ]

GiscedataCupsPs()
