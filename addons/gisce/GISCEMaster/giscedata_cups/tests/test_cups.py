# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import expect, raise_error
from osv.orm import ValidateException


class CUPSTest(testing.OOTestCase):
    """Testing Base CUPS
    """

    cups_values = {
        'name': 'ES9990',
        'id_municipi': 1
    }

    cups_term = {
        'name': 'ES0291000000000001DN{0}',
        'id_municipi': 1
    }

    def test_error_inserting_cups(self):
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        with Transaction().start(self.database) as txn:
            expect(lambda: cups_obj.create(
                txn.cursor, txn.user, self.cups_values)).to(
                    raise_error(ValidateException, u'warning -- ValidateError\n\nError '
                                            u'occurred while validating the '
                                            u'field(s) CUPS: El codi del CUPS '
                                            u'no és correcte.')
            )

    def test_no_error_inserting_cups_config(self):
        config_obj = self.openerp.pool.get('res.config')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        with Transaction().start(self.database) as txn:
            config_obj.set(txn.cursor, txn.user, 'check_cups', 0)
            cups_obj.create(txn.cursor, txn.user, self.cups_values)

    def test_error_cups_unic(self):
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')

        with Transaction().start(self.database) as txn:
            cups_0F = self.cups_term.copy()
            cups_1P = self.cups_term.copy()

            cups_0F['name'] = cups_0F['name'].format('0F')
            cups_1P['name'] = cups_1P['name'].format('1P')

            cups_obj.create(txn.cursor, txn.user, cups_0F)

            expect(
                lambda: cups_obj.create(txn.cursor, txn.user, cups_1P)
            ).to(
                raise_error(
                    ValidateException,
                    u'warning -- ValidateError\n\nError occurred while'
                    u' validating the field(s) CUPS: Aquest Codi de '
                    u'CUPS ja existeix'
                )
            )

    def test_unique_cups_works_with_short_cups(self):
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        res_config_obj = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            res_config_obj.set(txn.cursor, txn.user, 'check_cups', 0)

            cups = self.cups_term.copy()
            cups_wrong_check = self.cups_term.copy()

            cups['name'] = cups['name'].format('')
            cups_wrong_check['name'] = cups_wrong_check['name'].format('')
            cups_wrong_check['name'] = cups_wrong_check['name'][:18] + 'WC'

            cups_obj.create(txn.cursor, txn.user, cups_wrong_check)
            # This second one shouldn't raise an exception because it's a
            # different CUPS
            cups_obj.create(txn.cursor, txn.user, cups)
