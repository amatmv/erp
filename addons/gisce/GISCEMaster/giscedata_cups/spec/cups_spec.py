from expects import *

from giscedata_cups.dso_cups.cups import get_dso

with description("Checking get_dso function"):
    with it("Checking 0463 is 0026"):
        dso = get_dso('ES046355CCCCCCCCCCEENT')
        expect(dso).to(equal('0026'))

    with it("Checking 0492 is 0026"):
        dso = get_dso('ES049255CCCCCCCCCCEENT')
        expect(dso).to(equal('0026'))

    with it("Checking 0584 is 0198"):
        dso = get_dso('ES058455CCCCCCCCCCEENT')
        expect(dso).to(equal('0198'))

    with it("Checking ES0021000018487060YW0F is translated into 0172"):
        dso = get_dso('ES0021000018487060YW0F')
        expect(dso).to(equal('0172'))

    with it("Checking ES0021XXXXXXXXXXXXXXXX is NOT translated into 0172"):
        dso = get_dso('ES0021XXXXXXXXXXXXXXXX')
        expect(dso).not_to(equal('0172'))

    with it("Checking ES1234XXXXXXXXXXXXXXXX is not translated"):
        dso = get_dso('ES1234XXXXXXXXXXXXXXXX')
        expect(dso).to(equal('1234'))
