# -*- coding: utf-8 -*-
{
    "name": "GISCE Data CUPS",
    "description": """Codi Universal de Punt de Provisionament""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CUPS",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_cups_demo.xml"
    ],
    "update_xml":[
        "giscedata_cups_view.xml",
        "giscedata_cups_data.xml",
        "security/giscedata_cups_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
