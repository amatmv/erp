from osv import osv, fields

class crm_case(osv.osv):
    _name = "crm.case"
    _inherit = "crm.case"

    def _ref2_id(self, cr, uid, ids, field_name, ar, context):
        res = {}
        for case in self.browse(cr,uid, ids):
            if case.ref2:
                model,res[case.id] = case.ref2.split(',')
            else:
                res[case.id] = ''
        return res


    _columns = {
      'create_uid': fields.many2one('res.users', 'Usuari'),
      'ref2_id': fields.function(_ref2_id, method=True, type='char', string='Ref2 Id'),
    }

    def incidencia(self, cr, uid, ids, *args):
        refs = self.read(cr, uid, ids, ['ref', 'ref2'])
        refs = refs[0]
        for name in ['ref', 'ref2']:
            if refs[name]:
                model,id = refs[name].split(',')
                id = int(id)
                if model == 'crm.incidencia' and id > 0:
                    i = self.pool.get('crm.incidencia').browse(cr, uid, id)
                    return {
                      'id': i.id or '',
                      'descripcio': i.descripcio or '',
                      'operaris': i.operaris or '',
                      'n_comptador': i.n_comptador or '',
                      'h_laborables': i.hores_laborables or '',
                      'h_n_laborables': i.hores_no_laborables or '',
                      'dies_festius': i.dies_festius or '',
                      'hora_avis': i.hora_avis or '',
                      'hora_normalitzacio': i.hora_normalitzacio or '',
                      'num_calser': i.num_calser or '',
                      'avaria_client': i.avaria_client or '',
                      'avaria_propia': i.avaria_propia or '',
                      'avaria_tercers': i.avaria_tercers or '',
                      'agents_atmosferics': i.agents_atmosferics or '',
                      'parte': i.parte or '',
                      'n_parte': i.n_parte or '',
                      'demanat_afectacions': i.demanat_afectacions or '',
                      'observacions': i.observacions or '',
                      'categoria': i.categ_id and i.categ_id.name or ''
                    }
        return {
                'id': '',
                'descripcio': '',
                'operaris': '',
                'n_comptador': '',
                'h_laborables': '',
                'h_n_laborables': '',
                'dies_festius': '',
                'hora_avis': '',
                'hora_normalitzacio': '',
                'num_calser': '',
                'avaria_client': '',
                'avaria_propia': '',
                'avaria_tercers': '',
                'agents_atmosferics': '',
                'parte': '',
                'n_parte': '',
                'demanat_afectacions':'',
                'observacions': '',
                'categoria': ''
        }

    def polissa(self, cr, uid, ids, *args):
        refs = self.read(cr, uid, ids, ['ref', 'ref2'])
        refs = refs[0]
        for name in ['ref', 'ref2']:
            if refs[name]:
                model,id = refs[name].split(',')
                id = int(id)
                if model == 'giscedata.polissa' and id > 0:
                    polissa = self.pool.get('giscedata.polissa').browse(cr, uid, id)
                    return {
                      'name': polissa.name or '',
                      'codi': polissa.codi or '',
                      'municipi': polissa.cups_municipi or '',
                      'poblacio': polissa.cups_poblacio or '',
                      'numero': polissa.cups_numero or '',
                      'escala': polissa.cups_escala or '',
                      'carrer': polissa.cups_carrer or '',
                      'cups': polissa.cups.name or '',
                      'n_comptador': polissa.n_comptador or '',
                      'et': polissa.et or '',
                      'linia': polissa.linea or '',
                    }
        return {
                'name': '',
                'codi': '',
                'municipi': '',
                'poblacio': '',
                'numero': '',
                'escala': '',
                'carrer': '',
                'cups': '',
                'n_comptador': '',
                'et': '',
                'linia': '',
                }

crm_case()
