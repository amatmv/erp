# -*- coding: utf-8 -*-
{
    "name": "CRM Extra reports",
    "description": """Extra reports for CRM""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm_multicompany",
        "crm_extended"
    ],
    "init_xml":[
        "data.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "crm_extrareports_report.xml"
    ],
    "active": False,
    "installable": True
}
