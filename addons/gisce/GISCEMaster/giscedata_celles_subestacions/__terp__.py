# -*- coding: utf-8 -*-
{
    "name": "Cel·les i elements de tall (Subestacions)",
    "description": """
    * Afegeix funcionalitat per subestacions
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_celles",
        "giscedata_cts_subestacions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_celles_view.xml"
    ],
    "active": False,
    "installable": True
}
