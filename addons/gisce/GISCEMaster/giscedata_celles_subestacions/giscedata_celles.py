# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

INSTALLACIONS_RELE = [
    ('giscedata.cts.subestacions.posicio', 'Posició'),
    ('giscedata.celles.cella', 'Cel.la'),
]


class GiscedataCellesCella(osv.osv):
    _name = 'giscedata.celles.cella'
    _inherit = 'giscedata.celles.cella'

    def _get_cella_id(self, cursor, uid, ids, context=None):
        """
        Function that givest the celles to update the tensio

        :param cursor: Database cursor
        :param uid: User ids
        :type uid: int
        :param ids: Celles ids
        :type ids: list of int
        :param context: OpenERP context
        :type context: dicts
        :return: List of ids to update
        :rtype: list
        """
        return ids

    def _get_celles_from_pos(self, cursor, uid, ids, context=None):
        """
        Function that search all the celles linked with the affected posicions.

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User identifier
        :type       uid:        integer
        :param      ids:        Affected posicions ids
        :type       ids:        integer list
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    All the celles ids linked by the affected posicions
        :rtype:     integer list
        """

        celles_obj = self.pool.get('giscedata.celles.cella')

        return celles_obj.search(cursor, uid, [('posicio_id', 'in', ids)])

    def _fnct_get_reles(self, cr, uid, ids, field_name, args, context=None):
        """
        Function that find the linked reles of the celles.

        :param      cr:         Database cursor
        :type       cr:         cursor
        :param      uid:        User id
        :type       uid:        integer
        :param      ids:        Afected ids
        :type       ids:        integer list
        :param      field_name: Unused
        :param      args:       Unused
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    Dictionari with celles ids as a key and a list of reles ids
                    as a value for each key
        """
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = {i: [] for i in ids}
        reles_obj = self.pool.get('giscedata.celles.rele')

        for cela_id in ids:
            search_params = [
                ('installacio',
                 '=',
                 'giscedata.celles.cella,{}'.format(cela_id))
            ]
            res[cela_id] = reles_obj.search(cr, uid, search_params)
        return res


    _columns = {
        'posicio_id': fields.many2one('giscedata.cts.subestacions.posicio',
                                      'Circuit'),
        'subestacio_id': fields.related('posicio_id', 'subestacio_id',
                                        relation='giscedata.cts.subestacions',
                                        type='many2one',
                                        readonly=True,
                                        string='Subestació'),
        'tensio': fields.related('posicio_id', 'tensio',
                                 relation='giscedata.tensions.tensio',
                                 type='many2one',
                                 readonly=True,
                                 store={
                                     'giscedata.cts.subestacions.posicio': (
                                         _get_celles_from_pos,
                                         ['tensio'], 10),
                                     'giscedata.celles.cella': (
                                         _get_cella_id,
                                         ['posicio_id'], 10),
                                 },
                                 string='Tensió'),
        'reles': fields.function(
            _fnct_get_reles,
            relation='giscedata.celles.rele',
            type='one2many',
            method=True,
            string='Relés',
        )
    }

GiscedataCellesCella()


class GiscedataCellesRele(osv.osv):

    _name = 'giscedata.celles.rele'
    _inherit = 'giscedata.celles.rele'

    def _installacio_name(self, cursor, uid, ids, field_name, arg,
                          context=None):
        """
        Function that build the composed name of the installacio where the cella
        is linked.

        :param      cursor: Database cursor
        :type       cursor: cursor
        :param      uid:    User id
        :type       uid:    integer
        :param      ids:    Afected ids
        :type       ids:    list
        :param      field_name: Unused
        :param      arg:        Unused
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    Dictionari with reles ids as a key and the values of the
                    installation composed name related to each id.
        """
        res = dict.fromkeys(ids, False)
        # Obtenim el llistat així, perquè les obtindrem traduides segons el
        # context
        selection = dict(
            self.fields_get(cursor, uid, ['installacio'],
                            context=context)['installacio']['selection']
        )
        for rele in self.read(cursor, uid, ids, ['installacio']):
            if rele['installacio']:
                obj, obj_id = rele['installacio'].split(',')
                selection_name = selection[obj]
                obj = self.pool.get(obj)
                obj_id = int(obj_id)
                if obj_id == 0:
                    deleted_element_msg = _(
                        "Instal·lació eliminada, s'ha de reasignar l'element"
                    )
                    res[rele['id']] = "{}: {}".format(
                        selection_name, deleted_element_msg
                    )
                else:
                    obj_id = int(obj_id)
                    res[rele['id']] = "{}: {}".format(
                        selection_name, obj.name_get(cursor, uid, obj_id)[0][1]
                    )
        return res

    def _installacio_name_search(self, cursor, uid, obj, name, args,
                                 context=None):
        """
        Function that build the domain of installation search

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        integer
        :param      obj:        Unused
        :param      name:       Unused
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    list with the domain filter inside.
        """
        instalacions_rele_search_fields = {
            'giscedata.cts.subestacions.posicio': 'alies',
            'giscedata.celles.cella': 'descripcio'
        }

        search_value = args[0][2]
        installacio_refs = []
        for orm_model_name, user_model_name in INSTALLACIONS_RELE:
            obj = self.pool.get(orm_model_name)
            second_field = instalacions_rele_search_fields[orm_model_name]
            model_search_domain = ['|', ('name', 'ilike', search_value), (second_field, 'ilike', search_value)]

            model_ids = obj.search(cursor, uid, model_search_domain, context=context)

            for model_id in model_ids:
                installacio_refs.append("{},{}".format(orm_model_name, model_id))

        return [('installacio', 'in', installacio_refs)]

    def _centre_subestacio_name_posicio(self, cursor, uid, posicio_id, context=None):
        """
        Obtains the name of the 'subestación' related to the 'posición'.
        :param posicio_id: <giscedata.cts.subestacions.posicio> id.
        :type posicio_id: long
        :param context:
        :return: 'subestacion' name.
        :rtype: str
        """
        posicio_o = self.pool.get('giscedata.cts.subestacions.posicio')
        posicio_v = posicio_o.read(
            cursor, uid, posicio_id, ['subestacio_id'], context=context
        )
        centre_subestacio_name = posicio_v['subestacio_id']
        if centre_subestacio_name:
            centre_subestacio_o = self.pool.get('giscedata.cts.subestacions')
            centre_subestacio_id = int(centre_subestacio_name[0])
            centre_subestacio_v = centre_subestacio_o.read(
                cursor, uid, centre_subestacio_id, ['descripcio'],
                context=context
            )
            centre_subestacio_name = centre_subestacio_v['descripcio']

        return centre_subestacio_name

    def _centre_subestacio_name_cella(self, cursor, uid, cella_id, context=None):
        """
        Obtains the name of the 'instalación' related to the 'celda'.
        :param cella_id: <giscedata.celles.cella> id.
        :type cella_id: long
        :param context:
        :return: 'instalacion' name.
        :rtype: str
        """
        cella_o = self.pool.get('giscedata.celles.cella')
        cella_v = cella_o.read(
            cursor, uid, cella_id, ['installacio'], context=context
        )
        centre_subestacio_name = cella_v['installacio']
        if centre_subestacio_name:
            model_name, model_id = centre_subestacio_name.split(',')
            model_o = self.pool.get(model_name)
            model_v = model_o.read(
                cursor, uid, int(model_id), ['descripcio'], context=context
            )
            centre_subestacio_name = model_v['descripcio']

        return centre_subestacio_name

    def _centre_subestacio_name(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Obtains the name of the 'centro' or 'subestación' related to the 'relé'.
        :param field_name:
        :param arg:
        :param context:
        :return: Dictionary where the kye is the rele id and the value is the
        name of the the 'centro' or 'subestación'.
        :rtype: {}
        """
        if not isinstance(ids, list):
            ids = [ids]

        res = dict.fromkeys(ids, '')

        rele_vs = self.read(cursor, uid, ids, ['installacio'], context=context)

        for rele_v in rele_vs:
            rele_id = rele_v['id']
            model_name, model_id = rele_v['installacio'].split(',')

            if model_name == 'giscedata.cts.subestacions.posicio':
                res[rele_id] = self._centre_subestacio_name_posicio(
                    cursor, uid, int(model_id), context=context
                )
            elif model_name == 'giscedata.celles.cella':
                res[rele_id] = self._centre_subestacio_name_cella(
                    cursor, uid, int(model_id), context=context
                )

        return res

    def _centre_subestacio_name_search(self, cursor, uid, obj, name, args, context=None):
        search_value = args[0][2]
        instalacions_rele_search_fields = {
            'giscedata.cts.subestacions.posicio': (
                'giscedata.cts.subestacions', 'subestacio_id'
            ),
            'giscedata.celles.cella': (
                'giscedata.cts.subestacions', 'subestacio_id'
            )
        }
        installacio_refs = []
        for orm_model_name, user_model_name in INSTALLACIONS_RELE:
            end_model_name = instalacions_rele_search_fields[orm_model_name][0]
            end_model_o = self.pool.get(end_model_name)
            dmn = [('descripcio', 'ilike', search_value)]
            end_model_ids = end_model_o.search(cursor, uid, dmn, context=context)

            model_o = self.pool.get(orm_model_name)
            model_field = instalacions_rele_search_fields[orm_model_name][1]
            dmn = [(model_field, 'in', end_model_ids)]
            model_ids = model_o.search(cursor, uid, dmn, context=context)

            for model_id in model_ids:
                installacio_refs.append(
                    "{},{}".format(orm_model_name, model_id))

        return [('installacio', 'in', installacio_refs)]

    _columns = {
        'installacio': fields.reference('Instal·lació', INSTALLACIONS_RELE, 150,
                                        required=True, select=True),
        'installacio_name': fields.function(
            _installacio_name,
            fnct_search=_installacio_name_search,
            type='char',
            method=True,
            string='Nom instal·lació',
            size=256,
        ),
        'centre_subestacio_name': fields.function(
            _centre_subestacio_name,
            fnct_search=_centre_subestacio_name_search,
            type='char',
            method=True,
            string='Nom centre/subestació',
            size=128
        )
    }

GiscedataCellesRele()


class GiscedataCtsSubestacionsPosicio(osv.osv):

    _name = 'giscedata.cts.subestacions.posicio'
    _inherit = 'giscedata.cts.subestacions.posicio'

    def _fnct_get_reles(self, cr, uid, ids, field_name, args, context=None):
        """
        Function that find the linked reles of the posicions

        :param      cr:         Database cursor
        :type       cr:         cursor
        :param      uid:        User id
        :type       uid:        integer
        :param      ids:        Afected ids
        :type       ids:        integer list
        :param      field_name: Unused
        :param      args:       Unused
        :param      context:    OpenERP context
        :type       context:    dict
        :return:    Dictionari with posicio ids as a key and a list of reles ids
                    as a value for each key
        """

        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = {i: [] for i in ids}
        reles_obj = self.pool.get('giscedata.celles.rele')

        for pos_id in ids:
            search_params = [
                ('installacio',
                 '=',
                 'giscedata.cts.subestacions.posicio,{}'.format(pos_id))
            ]
            res[pos_id] = reles_obj.search(cr, uid, search_params)

        return res

    _columns = {
        'reles': fields.function(
            _fnct_get_reles,
            relation='giscedata.celles.rele',
            type='one2many',
            method=True,
            string='Relés'
        )
    }

GiscedataCtsSubestacionsPosicio()