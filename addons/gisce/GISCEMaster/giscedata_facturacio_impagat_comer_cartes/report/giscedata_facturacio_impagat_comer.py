# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.requeriment',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_impagat_comer_cartes/report/carta_requeriment.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.segona.carta.requeriment',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_impagat_comer_cartes/report/carta_requeriment.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.tall.bo.social',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_impagat_comer_cartes/report/carta_requeriment.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.facturacio.factura.carta.tall.normal',
    'giscedata.facturacio.factura',
    'giscedata_facturacio_impagat_comer_cartes/report/carta_tall.mako',
    parser=report_sxw.rml_parse
)
