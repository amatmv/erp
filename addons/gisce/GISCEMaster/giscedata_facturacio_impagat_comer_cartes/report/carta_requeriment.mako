## -*- coding: utf-8 -*-
<%
    import babel
    from datetime import datetime, timedelta
    from account_invoice_base.report.utils import localize_period
    from account_invoice_base.account_invoice import amount_grouped

    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')
    ncarta = context['carta']
    if ncarta == 1:
        tipo_carta = 'req1'
    elif ncarta == 2:
        tipo_carta = 'req2'
    else:
        tipo_carta = 'corte_bs'

    phone = company.partner_id.address[0].phone or ''
    email = company.partner_id.address[0].email or ''
    web = company.partner_id.website or ''
    company_address = company.partner_id
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>Carta de Tall</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_impagat_comer_cartes/report/estils_carta_requeriment.css"/>
        <style>
            @font-face {
                font-family: "Lato";
                src: url("${assets_path}/fonts/Lato/Lato-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        </style>
        <%block name="custom_css"/>
    </head>
    <body>
        %for factura in objects:
            <%
                data_inici = report_obj.get_start_day(
                    cursor, uid, factura.invoice_id.id, tipo_carta, context=context
                )[factura.invoice_id.id]
                locale = user.context_lang
                if ncarta == 1:
                    data_pago = data_inici + timedelta(days=10)
                elif ncarta == 2:
                    data_pago = data_inici + timedelta(days=45)
                else:
                    data_pago = data_inici + timedelta(days=10)
                    pending_obj = pool.get('account.invoice.pending.state.process')
                    bono_social_obj = pool.get('account.invoice.pending.state')
                    imd_obj = pool.get('ir.model.data')
                    tall_process = imd_obj.get_object_reference(
                        cursor, uid, 'giscedata_facturacio_comer_bono_social',
                        'bono_social_pending_state_process')[1]
                    avis_state = imd_obj.get_object_reference(
                        cursor, uid, 'giscedata_facturacio_comer_bono_social',
                        'carta_avis_tall_pending_state')[1]
                    avis_tipus = bono_social_obj.read(cursor, uid, avis_state, ['pending_days', 'pending_days_type'])
                    pending_days = avis_tipus['pending_days']
                    pending_type = avis_tipus['pending_days_type']
                    data_tall = pending_obj.get_cutoff_day(cursor, uid, tall_process, start_day=data_inici,
                        delta_days=pending_days, delta_type=pending_type, context={'sector': 'energia'})
            %>
            <div id="carta">
                <table class="header">
                    <colgroup>
                        <col style="width: 25%"/>
                        <col style="width: 40%"/>
                        <col style="width: 30%"/>
                    </colgroup>
                    <tr>
                        <td>
                            <img alt="logo" class="logo" src="data:image/jpeg;base64,${company.logo}"/>
                        </td>
                        <td>
                            <h1>
                                ${company.name} <br/>
                            </h1>
                            <div id="subtitle">${company.rml_footer2}</div>
                        </td>
                        <td id="partner_address">
                            ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''} <br/>
                            ${company_address.address[0].zip or ''} ${company_address.city or ''} <br/>
                            Telf: ${phone} <br/>
                            Web: <a href="${web}" style="color: inherit; text-decoration: none;">${web}</a> <br/>
                            E-mail: <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>
                        </td>
                    </tr>
                </table>
                <div class="address">
                    <div>
                        <div class="left-40 ">
                            <h3> ${_(u"Dades del contracte")} </h3>
                        </div>
                        <div class="right-45">
                            <h3> ${_(u"Dades de notificació")} </h3>
                        </div>
                    </div>
                    <div class="left-40 contact_info">
                        <div class="">
                            <table>
                                <tr>
                                    <td>
                                        <strong>${_(u"Titular:")}</strong> ${factura.polissa_id.titular.name}
                                    </td>
                                </tr>
                               <tr>
                                    <td>
                                        <strong>NIF:</strong> ${factura.polissa_id.titular.vat.replace('ES','')}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>CUPS:</strong> ${factura.cups_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.cups_id.direccio or ''}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="right-45 contact_info">
                        <div class="">
                            <table>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.address_contact_id.zip or ''} ${factura.address_contact_id.city or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${factura.polissa_id.name}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <%
                    city = company.partner_id.city.capitalize() or ''
                %>
                <div style="clear:both;"></div>
                <h3 class="center">
                    %if ncarta == 1:
                        ${_(u"Primera carta d'avís d'impagament")}
                    %elif ncarta == 2:
                        ${_(u"Segona carta d'avís d'impagament")}
                    %else:
                        ${_(u"Avís de suspensió de subministrament")}
                    %endif
                </h3>
                <p>
                    ${(city + ", ")} ${babel.dates.format_datetime(data_inici, "d LLLL 'de' YYYY,", locale = locale)} <br/>
                    ${_(u"Benvolgut client,")}
                </p>
                <div id="resum" class="box-border">
                    <p>
                        ${_(u"Mitjançant la present, se li requereix el pagament de les quantitats degudes en concepte de consum d'energia elèctrica.")}
                        <br/>
                        ${_(u"A dia d'avui, no ha satisfet el pagament de la ")}
                        <b>${_(u"factura número")} ${factura.number}</b>
                        ${_(u"emesa amb data ")} ${factura.date_invoice}${_(u", que ascendeix a un")}
                        <b>
                            ${_(u"import de ")}
                            ${formatLang(amount_grouped(factura), monetary=True, digits=2)} €
                        </b>
                        <br/>
                        ${_(u"En cas de no abonar-se la quantitat deguda,")}
                        %if ncarta in [1, 2]:
                             ${_(u"en un termini de 2 mesos a partir de la notificació del present requeriment,")}
                            <b>
                                ${_(u"l'empresa distribuïdora podrà suspendre el seu subministrament d'electricitat.")}
                                <%block name="pagament_data"></%block>
                            </b>
                        %else:
                            ${_(u"a partir del dia")} ${data_tall.strftime('%d/%m/%Y')}
                            ${_(u"l'empresa distribuïdora podrà suspendre el seu subministrament d'electricitat.")}
                            <br/>
                            <%block name="pagament_data_tall"></%block>
                        %endif
                    </p>
                </div>

                <%block name="forma_pagament"/>

                <p>
                    ${_(u"Sense perjudici de l'anterior, si vostè compleix els requisits per a ser consumidor vulnerable, pot sol·licitar a una de les empreses comercialitzadores de referència acollir-se al bo social, que suposa un descompte sobre el preu voluntari per al petit consumidor (PVPC).")}
                    <br/>
                    ${_(u"El canvi de modalitat en el contracte per passar a PVPC, sempre que no es modifiquin els paràmetres recollits en el contracte d'accés de tercers a la xarxa, es durà a terme sense cap tipus de penalització ni cost addicional.")}
                    <br/>
                    ${_(u"Sempre que s’hagin acreditat els requisits per a ser consumidor vulnerable, una vegada acollit al PVPC, el termini perquè el seu subministrament d’electricitat pugui ser suspès, de no haver estat abonada la quantitat deguda, passarà a ser de")}
                    <b>${_(u"4 mesos")}</b>
                    ${_(u"(contats sempre des de la recepció del present requeriment fefaent de pagament).")}
                    <br/>
                    ${_(u"L'enllaç a la pàgina web de la CNMC on trobarà les dades necessàries per contactar amb la comercialitzadora de referència és la següent:")}
                </p>
                <p>
                    <a href="https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico#listados">https://www.cnmc.es/ambitos-de-actuacion/energia/mercado-electrico#listados</a>
                </p>
                <p>
                    ${_(u"Addicionalment, si vostè compleix els requisits per a ser vulnerable sever, pot posar-se en contacte amb els serveis socials del municipi i comunitat autònoma on resideix, per tal que l’informin sobre la possibilitat d’atendre el pagament del seu subministrament d’electricitat.")}
                    <br/>
                    ${_(u"Els requisits per a ser consumidor vulnerable estan recollits en l’article 3 del Reial Decret 897/2017, de 6 d’octubre, pel qual es regula la figura del consumidor vulnerable, el bo social i altres mesures de protecció pels consumidors domèstics d’energia elèctrica.")}
                %if ncarta == 1:
                        ${_(u"Els pot trobar al següent enllaç:")}
                    </p>
                    <p>
                        <a href="https://www.boe.es/diario_boe/txt.php?id=BOE-A-2017-11505">https://www.boe.es/diario_boe/txt.php?id=BOE-A-2017-11505</a>
                %else:
                        ${_(u"Li adjuntem un extracte juntament amb aquest document.")}
                %endif
                </p>
                <p>
                    ${_(u"Atentament.")} <br/>
                    ${company.name}
                </p>
            </div>
            %if ncarta != 1:
                <p style="page-break-after:always"></p>
                <div id="boe">
                    <i>
                        ${_(u"Reial decret 897/2017, de 6 d'octubre, pel qual es regula la figura del consumidor vulnerable, el bo social i altres mesures de protecció per als consumidors domèstics d'energia elèctrica.")}
                    </i>
                    <b>${_(u"Article 3. Definició de consumidor vulnerable.")}</b>
                    <ol>
                        <li>${_(u"Als efectes d’aquest reial decret i altra normativa d’aplicació, tindrà la consideració de consumidor vulnerable el titular d’un punt de subministrament d’electricitat en el seu habitatge habitual que, essent persona física, estigui acollit al preu voluntari pel petit consumidor (PVPC) i compleixi la resta de requisits del present article.")}</li>
                        <li>${_(u"Per tal que un consumidor d’energia elèctrica pugui ser considerat consumidor vulnerable, haurà de complir algun dels següents requisits:")}
                            <ol type="a">
                                <li>${_(u"Que la seva renda o, en cas de formar part d’una unitat familiar, la renda conjunta anual de la unitat familiar a la qual pertanyi sigui igual o inferior:")}
                                    <ul>
                                        <li>${_(u"a 1,5 vegades l’Indicador Públic de Renda d’Efectes Múltiples (IPREM) de 14 pagues, en el cas de que no formi part d’una unitat familiar o no hi hagi cap menor a la unitat familiar;")}</li>
                                        <li>${_(u"a 2 vegades l’índex IPREM de 14 pagues, en el cas que hi hagi un menor a la unitat familiar;")}</li>
                                        <li>${_(u"a 2,5 vegades l’índex IPREM de 14 pagues, en el cas que hi hagi dos menors a la unitat familiar.")}</li>
                                    </ul>
                                    ${_(u"A aquests efectes, es considera unitat familiar a la constituïda d’acord el que s’estableix a la Llei 35/2006, de 28 de novembre, de l’Impost sobre la Renda de les Persones Físiques i de modificació parcial de les lleis dels Impostos sobre Societats, sobre la Renda de no Residents i sobre el Patrimoni.")}</li>
                                <li>${_(u"Estar en possessió del títol de família nombrosa.")}</li>
                                <li>${_(u"Que el propi consumidor i, en el cas de formar part d’una unitat familiar, tots els membres de la mateixa que tinguin ingressos, siguin pensionistes del Sistema de la Seguretat Social per jubilació o incapacitat permanent, percebent la quantia mínima vigent en cada moment per aquestes classes de pensió, i no percebin altres ingressos.")}</li>
                            </ol>
                        </li>
                        <li>${_(u"Els multiplicadors de renda respecte de l’índex IPREM de 14 pagues establerts a  l’apartat 2.a) s’incrementarà, en cada cas, a 0,5, sempre que concorri alguna de les següents circumstàncies especials:")}
                            <ol type="a">
                                <li>${_(u"Que el consumidor o algun dels membres de la unitat familiar tingui discapacitat reconeguda igual o superior al 33%.")}</li>
                                <li>${_(u"Que el consumidor o algun de los membres de la unitat familiar acrediti la situació de violència de gènere, d’acord l’establert a la legislació vigent.")}</li>
                                <li>${_(u"Que el consumidor o algun dels membres de la unitat familiar tingui la condició de víctima de terrorisme, d’acord l’establert a la legislació vigent.")}</li>
                            </ol>
                        </li>
                        <li>${_(u"Quan, complint els requisits anteriors, el consumidor i, en el seu cas, la unitat familiar a la que pertanyi, tinguin una renda anual inferior o igual al 50% dels llindars establerts a l’apartat 2.a), incrementats en el seu cas d’acord al que es disposa a l’apartat 3, el consumidor serà considerat vulnerable sever. Així mateix, també serà considerat vulnerable sever quan el consumidor, i, en el seu cas, la unitat familiar a la que pertanyi, tinguin una renda anual inferior o igual a una vegada el IPREM a 14 pagues o dos vegades el mateix, en el cas de que es trobi en la situació de l’apartat 2.c) o 2.b), respectivament.")}</li>
                        <li>${_(u"En tot cas, per tal que un consumidor sigui considerat vulnerable haurà d’acreditar el compliment dels requisits recollits en el present article en els termes que s’estableixin per ordre del Ministre d’Energia, Turisme i Agenda Digital.")}</li>
                    </ol>
                </div>
            %endif

            % if factura != objects[-1]:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
