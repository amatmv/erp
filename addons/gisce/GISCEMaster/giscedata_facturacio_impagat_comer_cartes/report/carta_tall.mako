## -*- coding: utf-8 -*-
<%
    import babel
    from babel.numbers import format_currency
    from datetime import datetime, timedelta
    from bankbarcode.cuaderno57 import Recibo507
    from giscedata_facturacio_impagat.giscedata_facturacio import find_B1
    from workalendar.europe.spain import Spain

    report_obj = pool.get('giscedata.facturacio.carta.requeriment.report')
    imd_obj = pool.get('ir.model.data')
    account_invoice_obj = pool.get('account.invoice')
    ncarta = context['carta']

    def obtenir_dia_pagament(date_due):
        origin_date = datetime.strptime(date_due, '%Y-%m-%d')
        calendar = Spain()
        new_date = calendar.add_working_days(origin_date, delta=-1)
        return new_date

    phone = company.partner_id.address[0].phone or company.partner_id.address[0].mobile or ''
    email = company.partner_id.address[0].email or ''
    web = company.partner_id.website or ''
    company_address = company.partner_id

    poblacio = company_address.address[0].id_poblacio.name or ''
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <title>${_(u"Carta d'Avís de Pagament")}</title>
        <link rel="stylesheet" href="${addons_path}/giscedata_facturacio_impagat_comer_cartes/report/estil_carta_tall.css"/>
    </head>
    <body>
        %for invoice in objects:
            <%
                b1_ids = find_B1(cursor, uid, invoice.id, context={'only_cuts': 1})
            %>
            % if not b1_ids:
                % if len(objects) == 1:
                    <div id="contingut">
                        <h1>${_(u"No s'ha detectat cap tall per a la factura")} ${invoice.number or ''}</h1>
                    </div>
                % endif
                <%
                    continue
                %>
            %else:
                <%
                    setLang(invoice.lang_partner)
                    data_inici = report_obj.get_start_day(cursor, uid, invoice.id, ncarta, context=context)[invoice.id]
                    b1_01_obj = pool.get('giscedata.switching.b1.01')
                    swh_ids = b1_01_obj.search(cursor, uid, [('sw_id', '=', b1_ids[0])], limit=1)
                    swh_init = b1_01_obj.browse(cursor, uid, swh_ids[0])
                    date_due = datetime.strptime(swh_init.data_accio, '%Y-%m-%d')
                    fecha_pago = obtenir_dia_pagament(swh_init.data_accio).strftime('%Y-%m-%d')
                %>
            %endif
            <table class="header">
                <colgroup>
                    <col style="width: 25%"/>
                    <col style="width: 40%"/>
                    <col style="width: 30%"/>
                </colgroup>
                <tr>
                    <td>
                        <img alt="logo" class="logo" src="data:image/jpeg;base64,${company.logo}" height="75px"/>
                    </td>
                    <td>
                        <h1>
                            ${company.name} <br/>
                        </h1>
                        <div id="subtitle">${company.rml_footer2}</div>
                    </td>
                    <td id="partner_address">
                        ${company_address.address[0].street or ''} ${company_address.address[0].street2 or ''} <br/>
                        ${company_address.address[0].zip or ''} ${poblacio} (${company_address.address[0].state_id.name or ''})<br/>
                        Telf: ${phone} <br/>
                        %if web:
                            Web: <a href="${web}" style="color: inherit; text-decoration: none;">${web}</a> <br/>
                        %endif
                        %if email:
                            E-mail: <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>
                        %endif
                    </td>
                </tr>
            </table>
            <div class="address">
                <table>
                    <colgroup>
                        <col style="width: 55%"/>
                        <col style="width: 45%"/>
                    </colgroup>
                    <tr>
                        <td>
                            <table class="address">
                                <tr>
                                    <td>
                                        <u><b>${_(u"Dades del subministrament")}</b></u>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>${_(u"Núm. Contracte")}</b> ${invoice.polissa_id.name}
                                    </td>
                                </tr>
                                <%
                                    state_cups = ''
                                    if invoice.cups_id.id_provincia.name:
                                        state_cups = '(' + invoice.cups_id.id_provincia.name + ')'
                                %>
                                <tr>
                                    <td>
                                        ${invoice.address_invoice_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.nv or ''} ${invoice.cups_id.pnp or ''} ${invoice.cups_id.es or ''} ${invoice.cups_id.pt or ''} ${invoice.cups_id.pu or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.cpo or ''} ${invoice.cups_id.cpa or ''} ${invoice.cups_id.dp or ''} ${invoice.cups_id.id_poblacio.name or ''} ${state_cups}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.cups_id.aclarador or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <p id="referencia">${_(u"CUPS:")} ${invoice.cups_id.name}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <%
                                state_contact = ''
                                if invoice.address_contact_id.state_id.name:
                                    state_contact = '(' + invoice.address_contact_id.state_id.name + ')'
                            %>
                            <table class="address">
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.name}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.street or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.street2 or ''}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${invoice.address_contact_id.zip or ''} ${invoice.address_contact_id.city or ''}  ${state_contact}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <h2>${_(u"AVÍS DE TALL DEL SERVEI ELÈCTRIC")}</h2>
            <div class="">
                <table>
                    <colgroup>
                        <col style="width: 60%"/>
                        <col style="width: 40%"/>
                    </colgroup>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <h3>${_(u"ÚLTIM DIA DE PAGAMENT:")}
                                % if date_due != 'False':
                                    ${formatLang(fecha_pago, date=True)}
                                % endif
                            </h3>
                        </td>
                    </tr>
                </table>
            </div>
            ${poblacio}, ${babel.dates.format_datetime(data_inici, "d LLLL 'de' YYYY,", locale='es_ES')}<br/>
            ${_(u"Benvolgut client,")}<br/>
            <div class="condicions">
	    	${_(u"Per la present li comuniquem que tenim constància de que la següent factura del subministrament elèctric ha sigut RETORNADA pel seu Banc o Caixa d'Estalvis i que a continuació li detallem.")}
            ${_(u"(Preguem que, si la devolució es deguda a un error o cancel·lació de la seva domiciliació bancària, es posi en contacte amb nosaltres per actualitzar les seves dades bancàries)")}
            </div>
            <br/>
            ${_(u"DETALL DE LA FACTURA:")}
            <div class="minheight100 border margin_bottom_10">
                <table class="cut_invoice_details">
                    <tr>
                        <td>
                            ${_(u'Ref. rebut')}
                        </td>
                        <td>
                            ${_(u'Període facturat')}
                        </td>
                        <td>
                            ${_(u'Total fact.')}
                        </td>
                        <td>
                            ${_(u'Pagat')}
                        </td>
                        <td>
                            ${_(u'Pendent')}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${invoice.number}
                        </td>
                        <td>
                            ${formatLang(invoice.data_inici, date=True)} - ${formatLang(invoice.data_final, date=True)}
                        </td>
                        <td>
                            ${format_currency(float(invoice.amount_total), 'EUR', locale='es_ES')}
                        </td>
                        <%
                            pagat = invoice.amount_total - invoice.residual
                        %>
                        <td>
                            ${format_currency(float(pagat), 'EUR', locale='es_ES')}
                        </td>
                        <td>
                            ${format_currency(float(invoice.residual), 'EUR', locale='es_ES')}
                        </td>
                    </tr>
                </table>
                <br/>
                <div id="total">
                    TOTAL: ${format_currency(float(invoice.amount_total), 'EUR', locale='es_ES')}
                </div>
            </div>
            <div class="condicions">
	    	${_(u"Per tot el descrit, i d'acord amb el R.D 1955/2000, art 84 punt 1 i la legislació posterior del pagament i suspensió del subministrament d'energia Elèctrica, se li concedeix un termini determinat, per a que procedeixi a la cancel·lació integra del deute.")}
            </div>
            ${self.compte_bancari(company.partner_id)}
            <div class="condicions">
                ${_(u"Una vegada efectuat el pagament en el termini senyalat, vostè s'haurà de posar en contacte amb nosaltres per a acreditar el pagament.")} <br/>
                % if phone or email:
                    ${_(u"Es pot posar en contacte amb nosaltres via:")}
                    <ul>
                        % if phone:
                            <li>
                                ${_(u"Telèfon:")} <a href="tel:${phone}" style="color: inherit; text-decoration: none;">${phone}</a>.
                            </li>
                        % endif
                        %if email:
                            <li>
                                ${_(u"Correu electrònic:")} <a href="mailto:${email}" style="color: inherit; text-decoration: none;">${email}</a>.
                            </li>
                        %endif
                    </ul>
                % endif

                ${_(u"Un cop passat el termini sense haver efectuat el pagament procedirem al tall del subministrament elèctric que es durà a terme durant l'horari habitual de treball.")}
            </div>
            <br/>
            <table class="invoice_details">
                <tr>
                    <td>
                        ${_(u"DATA DEL TALL DEL SUBMINISTRAMENT ELÈCTRIC:")}
                    </td>
                    <td>
                        ${date_due.strftime('%d/%m/%Y')}
                    </td>
                </tr>
            </table>
            ${_(u"Atentament,")} <br/>
            ${company.name}
            % if invoice != objects[-1]:
                <p style="page-break-after:always"></p>
            % endif
        %endfor
    </body>
</html>
<%def name="compte_bancari(partner)"/>