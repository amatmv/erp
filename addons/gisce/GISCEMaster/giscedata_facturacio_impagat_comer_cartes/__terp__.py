# -*- coding: utf-8 -*-
{
    "name": "Impagaments Reports (comer)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Reports de cartes de bono social (avis 1, avis 2 i tall)
  * Report Carta Tall normal
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_facturacio_impagat_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_impagat_comer_report.xml"
    ],
    "active": False,
    "installable": True
}
