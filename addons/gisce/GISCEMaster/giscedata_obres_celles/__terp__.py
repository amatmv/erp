# -*- coding: utf-8 -*-
{
    "name": "Obres per cel·les i elements de tall",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a les cel·les i elements de tall
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_celles",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_celles_view.xml"
    ],
    "active": False,
    "installable": True
}
