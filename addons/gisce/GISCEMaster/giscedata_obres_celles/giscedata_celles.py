# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCellesCella(osv.osv):
    _name = 'giscedata.celles.cella'
    _inherit = 'giscedata.celles.cella'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_cella_rel',
            'cella_id',
            'obra_id',
            'Obres'
        )
    }

GiscedataCellesCella()