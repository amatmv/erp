# -*- coding: utf-8 -*-
{
    "name": "Index Circuits",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index giscedata_circuits (AT)
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "giscedata_circuits",
        "giscedata_at_index",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
