from base_index.base_index import BaseIndex


class GiscedataAtTram(BaseIndex):
    """
    Index for giscedata.at.tram
    """
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _index_fields = {
        "circuit.name": True,
        "circuit.alies": True
    }


GiscedataAtTram()
