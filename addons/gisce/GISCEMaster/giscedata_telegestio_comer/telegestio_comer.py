# -*- coding: utf-8 -*-

import os
from osv import osv, fields
import netsvc
import pooler
import time
from tools.translate import _
from tools import config
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from mongodb_backend import osv_mongodb
from mongodb_backend.mongodb2 import mdbpool
import socket
import re
from oorq.decorators import job
from six import itervalues
from cchloader.file import CchFile, PackedCchFile
from cchloader.backends import get_backend

from cchloader.compress import is_compressed_file


_CCH_MODULES = {
}

MEASURE_SOURCE = [
    (1, 'Medida real válida'),
    (2, 'Medida perfilada a partir de saldo ATR'),
    (3, 'Medida ajustada a un saldo ATR real'),
    (4, 'Medida perfilada a partir de autolectura'),
    (5, 'Medida perfilada a partir de estimación'),
    (6, 'Medida perfilada a partir de estimación por factor de utilización')
]

MEASURE_SOURCE_P1_F1 = [
    (1, 'Medidas firmes en configuración principal'),
    (2, 'Medidas firmes en configuración redundante'),
    (3, 'Medidas firmes de equipos de medida en configuración comprobante'),
    (4, 'Medidas provisionales en configuración principal'),
    (5, 'Medidas provisionales en configuración redundante'),
    (6, 'Medidas provisionales en configuración comprobante'),
    (7, 'Estimación del energía horaria basada en el histórico del punto de '
        'medida principal modulado con su saldo'),
    (8, 'Estimación de energía con perfil plano a partir de cierres de ATR'),
    (9, 'Estimación basada en histórico del punto de medida principal (sin '
        'datos de saldo o de cierre de ATR)'),
    (10, 'Estimación técnicamente justificada tras incidencia en el equipo '
         'de medida'),
    (11, 'Estimación de energía horaria realizada basada en un factor de '
         'utilización del 33%'),
    (22, 'Estimación que penaliza para clientes tipo 1 y 2'),

]



def register(tag, enabled, file_syntax, file_folder, file_date, model_cls):
    _CCH_MODULES[tag] = {
        'type': tag,
        'enabled': enabled,
        'file_syntax': file_syntax,
        'file_folder': file_folder,
        'file_date': file_date,
        'model': model_cls
    }


class TgComerProvider(osv.osv):

    _name = 'tg.comer.provider'

    _columns = {
        'name': fields.char('Description', size=50),
        'enabled': fields.boolean('Enabled'),
        'distribuidora': fields.many2one('res.partner', 'Utility'),
        'sftp': fields.many2one('tg.sftp', 'Utility sftp service'),
        'f5d_enabled': fields.boolean('F5D Enabled'),
        'f5d_syntax': fields.char('F5D filename regex', size=255),
        'f5d_dir': fields.char('F5D folder path', size=255),
        'f5d_date': fields.datetime('F5D access date'),
        'p5d_enabled': fields.boolean('P5D enabled'),
        'p5d_syntax': fields.char('P5D filename regex', size=255),
        'p5d_dir': fields.char('P5D folder path', size=255),
        'p5d_date': fields.datetime('P5D access date'),
        'cchcons_enabled': fields.boolean('CCHCONS Enabled'),
        'cchcons_syntax': fields.char('CCHCONS filename regex', size=255),
        'cchcons_dir': fields.char('CCHCONS folder path', size=255),
        'cchcons_date': fields.datetime('CCHCONS access date'),
        'rf5d_enabled': fields.boolean('RF5D Enabled'),
        'rf5d_syntax': fields.char('RF5D filename regex', size=255),
        'rf5d_dir': fields.char('RF5D folder path', size=255),
        'rf5d_date': fields.datetime('RF5D access date'),
        'f1_enabled': fields.boolean('F1 Enabled'),
        'f1_syntax': fields.char('F1 filename regex', size=255),
        'f1_dir': fields.char('F1 folder path', size=255),
        'f1_date': fields.datetime('F1 access date'),
        'p1_enabled': fields.boolean('P1 Enabled'),
        'p1_syntax': fields.char('P1 filename regex', size=255),
        'p1_dir': fields.char('P1 folder path', size=255),
        'p1_date': fields.datetime('P1 access date'),
        'a5d_enabled': fields.boolean('A5D Enabled'),
        'a5d_syntax': fields.char('A5D filename regex', size=255),
        'a5d_dir': fields.char('A5D folder path', size=255),
        'a5d_date': fields.datetime('A5D access date'),
        'b5d_enabled': fields.boolean('B5D Enabled'),
        'b5d_syntax': fields.char('B5D filename regex', size=255),
        'b5d_dir': fields.char('B5D folder path', size=255),
        'b5d_date': fields.datetime('B5D access date')
    }

    _defaults = {
        'enabled': lambda *a: True,
        'f5d_enabled': lambda *a: True,
        'f5d_syntax': lambda *a: 'F5D_',
        'f5d_dir': lambda *a: '/',
        'p5d_enabled': lambda *a: True,
        'p5d_syntax': lambda *a: 'P5D_',
        'p5d_dir': lambda *a: '/',
        'cchcons_enabled': lambda *a: False,
        'rf5d_enabled': lambda *a: False,
        'f1_enabled': lambda *a: False,
        'f1_syntax': lambda *a: 'F1_',
        'f1_dir': lambda *a: '/',
        'p1_enabled': lambda *a: False,
        'p1_syntax': lambda *a: 'P1_',
        'p1_dir': lambda *a: '/',
        'a5d_enabled': lambda *a: False,
        'a5d_syntax': lambda *a: 'A5D_',
        'a5d_dir': lambda *a: '/',
        'b5d_enabled': lambda *a: False,
        'b5d_syntax': lambda *a: 'B5D_',
        'b5d_dir': lambda *a: '/',
    }

TgComerProvider()


class TgCurveBaseClass(object):

    def get_curve_cups(self, cursor, uid, cups_name):

        mdbcolname = self.pool.get(self._name)._table
        tg_mdb = mdbpool.get_collection(mdbcolname)

        res = tg_mdb.find_one(
            {
                'name': {'$regex': '^{}'.format(cups_name[:20])}
            }
        )
        return res and res.get('name', False) or cups_name

    def get_aggregated_curve(self, cursor, uid, cups, start, end, aggr='monthly'):
        mdbcolname = self.pool.get(self._name)._table
        tg_mdb = mdbpool.get_collection(mdbcolname)

        assert aggr in ('monthly', 'daily')

        if isinstance(start, basestring):
            start = datetime.strptime(start, '%Y-%m-%d')
        start = start.replace(hour=1)

        if isinstance(end, basestring):
            end = datetime.strptime(end, '%Y-%m-%d')
        end += timedelta(days=1)
        end = end.replace(hour=0)

        pipeline = [
            {
                "$match": {
                    "$and": [
                        {"name": cups,},
                        {"datetime": {"$lte": end, "$gte": start}}
                    ]
                }
            },
            {
                "$group": {
                    "_id": {
                        "month": {"$month": "$datetime"},
                        "year": {"$year": "$datetime"}
                    },
                    "active": {"$sum": "$ai"},
                    "reactive": {"$sum": "$r1"},
                    "year": {"$max": {"$year": "$datetime"}},
                    "month": {"$max": {"$month": "$datetime"}}
                }
            },
            {
                "$sort": {
                    "year": 1,
                    "month": 1
                }
            }
        ]

        if aggr == 'daily':
            pipeline[1]["$group"]["_id"]["day"] = {"$dayOfMonth": "$datetime"}
            pipeline[1]["$group"]["day"] = {"$max": {"$dayOfMonth": "$datetime"}}
            pipeline[2]["$sort"]["day"] = 1

        res = tg_mdb.aggregate(pipeline)
        return res.get('result', [])

    def get_curve(self, cursor, uid, cups, start, end, context=None):
        mdbcolname = self.pool.get(self._name)._table
        tg_mdb = mdbpool.get_collection(mdbcolname)

        if isinstance(start, basestring):
            start = datetime.strptime(start, '%Y-%m-%d')
        start = start.replace(hour=1)

        if isinstance(end, basestring):
            end = datetime.strptime(end, '%Y-%m-%d')
        end += timedelta(days=1)
        end = end.replace(hour=0)

        res = tg_mdb.find({
            "$and": [
                {"name": cups},
                {"datetime": {"$lte": end, "$gte": start}}
            ]
        }, {"_id": False, "datetime": True, "ai": True, "r1": True, "source": 1})
        return list(res)


class TgCchfact(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.cchfact'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgCchfact,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Season'),
        'ai': fields.float('Activa entrante'),
        # it must be "ae", but cchload load as "ao"
        'ao': fields.float('Active saliente'),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'source': fields.selection(MEASURE_SOURCE, 'Origen'),
        'validated': fields.selection([(0, 'Provisional'),
                                       (1, 'Firme')]),
        'bill': fields.char('Código de factura', size=26)

    }

TgCchfact()
register(
    'cchfact', 'f5d_enabled', 'f5d_syntax', 'f5d_dir', 'f5d_date', TgCchfact
)


class TgCchval(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.cchval'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgCchval,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Season'),
        'ai': fields.float('Activa entrante'),
        # it must be "ae", but cchload load as "ao"
        'ao': fields.float('Active saliente'),

    }

TgCchval()
register('cchval', 'p5d_enabled', 'p5d_syntax', 'p5d_dir', 'p5d_date', TgCchval)


class TgCchcons(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.cchcons'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgCchcons,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'ai': fields.float('Activa entrante'),
        'source': fields.selection(MEASURE_SOURCE, 'Origin')
    }

TgCchcons()


class TgF1(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.f1'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgF1,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True, select=1),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Season'),
        'measure_type': fields.integer('Tipo medida'),
        'ai': fields.float('Activa entrante'),
        'ao': fields.float('Activa saliente'),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'reserve1': fields.float('Reserva 1'),
        'reserve2': fields.float('Reserva 2'),

        'source': fields.selection(MEASURE_SOURCE_P1_F1, 'Origen'),
        'validated': fields.selection([(0, 'Provisional'),
                                       (1, 'Firme')],
                                      'Indicador firmeza'),
    }

TgF1()
register('f1', 'f1_enabled', 'f1_syntax', 'f1_dir', 'f1_date', TgF1)


class TgP1(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.p1'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgP1,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True, select=1),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Season'),
        'measure_type': fields.integer('Tipo medida'),
        'ai': fields.float('Activa entrante'),
        'aiquality': fields.integer('Calidad activa entrante'),
        'aivalid': fields.selection([(0, 'Activa entrante inválida'),
                                     (1, 'Activa entrante Válida')],
                                    'Activa entrante válida'),
        'ao': fields.float('Activa saliente'),
        'aoquality': fields.integer('Calidad activa saliente'),
        'aovalid': fields.selection([(0, 'Activa saliente inválida'),
                                     (1, 'Activa saliente válida')],
                                    'Activa saliente válida'),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r1quality': fields.integer('Calidad reactiva 1'),
        'r1valid': fields.selection([(0, 'r1 inválida'),
                                     (1, 'r1 válida')],
                                    'R1 válida'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r2quality': fields.integer('Calidad reactiva 2'),
        'r2valid': fields.selection([(0, 'r2 inválida'),
                                     (1, 'r2 válida')],
                                    'R2 válida'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r3quality': fields.integer('Calidad reactiva 3'),
        'r3valid': fields.selection([(0, 'r3 inválida'),
                                     (1, 'r3 válida')],
                                    'R3 válida'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'r4quality': fields.integer('Calidad reactiva 4'),
        'r4valid': fields.selection([(0, 'r4 inválida'),
                                     (1, 'r4 válida')],
                                    'R4 válida'),
        'reserve1': fields.float('Reserva 1'),
        'res1quality': fields.integer('Calidad reserva1 1'),
        'res1valid': fields.selection([(0, 'Reserva1 inválida'),
                                       (1, 'Reserva1 válida')],
                                      'Reserva 1 válida'),
        'reserve2': fields.float('Reserva 2'),
        'res2quality': fields.integer('Calidad reserva 2'),
        'res2valid': fields.selection([(0, 'Reserva2 inválida'),
                                       (1, 'Reserva2 válida')],
                                      'Reserva 2 válida'),
        'source': fields.selection(MEASURE_SOURCE_P1_F1, 'Origen'),
        'validated': fields.selection([(0, 'Provisional'),
                                       (1, 'Firme')],
                                      'Indicador firmeza'),
    }

TgP1()
register('p1', 'p1_enabled', 'p1_syntax', 'p1_dir', 'p1_date', TgP1)


class TgCchGenNetaBeta(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.cch_gennetabeta'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgCchGenNetaBeta,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Season'),
        'ai': fields.float('Activa entrante'),
        'ae': fields.float('Active saliente'),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'source': fields.selection(MEASURE_SOURCE, 'Origen'),
        'validated': fields.selection([(0, 'Provisional'),
                                       (1, 'Firme')]),
        'bill': fields.char('Código de factura', size=26)

    }


TgCchGenNetaBeta()
register(
    'cch_gennetabeta', 'b5d_enabled', 'b5d_syntax', 'b5d_dir', 'b5d_date', TgCchGenNetaBeta
)


class TgCchAutoCons(osv_mongodb.osv_mongodb, TgCurveBaseClass):

    _name = 'tg.cch_autocons'
    _order = 'datetime desc'

    def search(self, cursor, uid, args, offset=0, limit=0, order=None,
               context=None, count=False):
        '''Exact match for name.
        In mongodb, even when an index exists, not exact
        searches for fields scan all documents in collection
        making it extremely slow. Making name exact search
        reduces dramatically the amount of documents scanned'''

        new_args = []
        for arg in args:
            if not isinstance(arg, (list, tuple)):
                new_args.append(arg)
                continue
            field, operator, value = arg
            if field == 'name' and operator.lower().endswith('like'):
                operator = '='
            new_args.append((field, operator, value))
        return super(TgCchAutoCons,
                     self).search(cursor, uid, new_args,
                                  offset=offset, limit=limit,
                                  order=order, context=context,
                                  count=count)

    _columns = {
        'name': fields.char('CUPS', size=22, required=True),
        'datetime': fields.datetime('Fecha y hora medida'),
        'create_at': fields.datetime('Create datetime'),
        'update_at': fields.datetime('Update datetime'),
        'season': fields.selection([(0, 'Invierno'),
                                    (1, 'Verano')], 'Season'),
        'ai': fields.float('Activa entrante'),
        'ae': fields.float('Active saliente'),
        'r1': fields.float('Reactiva cuadrante 1'),
        'r2': fields.float('Reactiva cuadrante 2'),
        'r3': fields.float('Reactiva cuadrante 3'),
        'r4': fields.float('Reactiva cuadrante 4'),
        'source': fields.selection(MEASURE_SOURCE, 'Origen'),
        'validated': fields.selection([(0, 'Provisional'),
                                       (1, 'Firme')]),
        'bill': fields.char('Código de factura', size=26)

    }


TgCchAutoCons()
register(
    'cch_autocons', 'a5d_enabled', 'a5d_syntax', 'a5d_dir', 'a5d_date', TgCchAutoCons
)


def get_uri():
    return 'erp{0}'.format(mdbpool.uri)


class TgComerReader(osv.osv_memory, TgCurveBaseClass):
    '''Implements reader'''

    _name = 'tg.comer.reader'
    _auto = False

    def reader(self, cursor, uid, provider_ids=None, context=None):
        '''Connect to the profile provider and read all the files'''

        provider_obj = self.pool.get('tg.comer.provider')
        partner_obj = self.pool.get('res.partner')
        sftp_obj = self.pool.get('tg.sftp')
        logger = netsvc.Logger()
        cfg_obj = self.pool.get('res.config')

        tg_comer_cch_dir = cfg_obj.get(cursor, uid, 'tg_comer_cch_dir', 0)
        if not tg_comer_cch_dir:
            raise osv.except_osv(_(u'TG settings error'),
                _(u"Configuration variable missing: tg_comer_cch_dir"))

        if not os.path.isdir(tg_comer_cch_dir):
            raise osv.except_osv(_(u'TG settings error'),
                _(u"Data folder missing: %s") % (tg_comer_cch_dir))

        if not os.access(tg_comer_cch_dir, os.W_OK):
            raise osv.except_osv(_(u'TG settings error'),
                _(u"Data folder not writable: %s") % (tg_comer_cch_dir))

        if not provider_ids:
            provider_ids = provider_obj.search(
                cursor, uid, [('enabled', '=', True)]
            )
        for provider_id in provider_ids:
            fields_to_read = [
                'name',
                'enabled',
                'sftp',
            ]
            for module in itervalues(_CCH_MODULES):
                # Read only the registered modules
                fields_to_read.append(module['enabled'])
                fields_to_read.append(module['file_syntax'])
                fields_to_read.append(module['file_folder'])
                fields_to_read.append(module['file_date'])

            provider = provider_obj.read(
                cursor, uid, provider_id, fields_to_read
            )
            if not provider['enabled']:
                logger.notifyChannel(
                    "TG Reader", netsvc.LOG_INFO,
                    "Provider disabled: %s ..." % provider['name']
                )
                continue
            dirs_to_read = []
            try:
                # Get required sftp server info
                sftp_id = provider.get('sftp', [None])[0]
                sftpcon = sftp_obj.login(cursor, uid, sftp_id)
                sftp_url = sftp_obj.read(cursor, uid, sftp_id, ['host'])['host']
                dirs_to_read = sftpcon.get_read_dirs(cursor, uid)
            except Exception as exc:
                logger.notifyChannel(
                    "TG Reader", netsvc.LOG_ERROR,
                    "Failed connecting to {provider[name]}: {exc}".format(
                        **locals()
                    )
                )
                continue

            reg_obj = self.pool.get('tg.comer.reader.register')
            newest_load = None
            search_vals = [('server_from', '=', sftp_url),
                           ('state', '!=', 'error')]
            reg_ids = reg_obj.search(cursor, uid, search_vals)
            if reg_ids:
                newest_load = reg_obj.read(cursor, uid, reg_ids[0], ['name',
                                           'state', 'date_load'])

            # All files from a provider
            file_list = []
            supported_syntax = []
            supported_date = {}
            update_date = {}
            for module in itervalues(_CCH_MODULES):
                try:
                    if provider[module['enabled']]:  # Add only if enabled
                        if newest_load:
                            last_read = newest_load['date_load']
                        elif provider[module['file_date']]:
                            last_read = datetime.strptime(
                                provider[module['file_date']],
                                '%Y-%m-%d %H:%M:%S')
                        else:
                            last_read = datetime.now() - relativedelta(years=4)
                        supported_syntax.append(
                            (module['type'],
                             provider[module['enabled']],
                             provider[module['file_syntax']],
                             provider[module['file_folder']],
                             last_read
                             ))
                        supported_date[module['type']] = module['file_date']
                        update_date[module['type']] = datetime.strptime(
                            provider[module['file_date']], '%Y-%m-%d %H:%M:%S')
                except Exception as exc:
                    logger.notifyChannel(
                        "TG Reader", netsvc.LOG_ERROR,
                        "Failed getting info of {} from provider {}".format(
                            module['type'], provider['name'])
                    )
                    continue
            logger.notifyChannel(
                "TG Reader", netsvc.LOG_INFO,
                "Listing files from FTP server"
            )
            for fdir in dirs_to_read:
                aux_file_list = sftpcon.list_files(
                    cursor, uid, fdir, recursive=True,
                    supported_syntax=supported_syntax
                )
                file_list.extend(aux_file_list)
            logger.notifyChannel(
                "TG Reader", netsvc.LOG_INFO,
                "Found {} files on the FTP server.".format(len(file_list))
            )
            # Filter files between files to read from local or files to
            # download from sftp
            files_to_read, files_to_load = self.cch_file_check(cursor, uid,
                                                               file_list,
                                                               sftp_url)
            # Download all possible files
            if files_to_load:
                downloaded_files = self.cch_file_downloader(cursor, uid,
                                                            files_to_load,
                                                            tg_comer_cch_dir,
                                                            sftp_url)
                # Join files to read with the recently downloaded files
                files_to_read.extend(downloaded_files)
            sftpcon.close(cursor, uid)

            for file_to_read in sorted(files_to_read,
                                       key=lambda t: t['remote_file']):
                self.cch_loader(cursor, uid, file_to_read, tg_comer_cch_dir,
                                sftp_url)
                if file_to_read['remote_date'] > update_date[file_to_read[
                                                             'type']]:
                    update_date[file_to_read['type']] = file_to_read[
                        'remote_date']
            write_values = {field: update_date[_type]
                            for _type, field in supported_date.iteritems()
                            }
            provider_obj.write(cursor, uid, provider_id, write_values)

    def cch_file_check(self, cursor, uid, file_list, sftp_url):
        '''
            Filters all files in sftp server. Classifies the files in
            files_to_read and files_to_load.

        :param file_list: List of all files in the sftp dir
        :param sftp_url: Url to the sftp server
        :return: files_to_read: Files which had been properly downloaded from
                                sftp and already are on local dir
                 files_to_load: Files which couldn't be downloaded from sftp
                                and all new files recently added to the sftp
        '''
        reg_obj = self.pool.get('tg.comer.reader.register')
        logger = netsvc.Logger()
        search_vals = [('server_from', '=', sftp_url)]
        reg_ids = reg_obj.search(cursor, uid, search_vals)
        reg_vals = reg_obj.read(cursor, uid, reg_ids, ['name', 'state'])
        registers = [x['name'] for x in reg_vals]

        files_to_read = []
        files_to_load = []

        for remote_file, _type, db_date, remote_date, remote_mdate in file_list:

            logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                 "Checking status of {} file".format(
                                     remote_file)
                                 )
            remote_file_name = os.path.basename(remote_file)

            if remote_file_name in registers:
                state = (item for item in reg_vals if item['name'] ==
                         remote_file_name).next()['state']
                if state == 'uploaded':
                    files_to_read.append({'remote_file': remote_file,
                                          'type': _type, 'state': state,
                                          'remote_date': remote_mdate})
                elif state == 'error':
                    files_to_load.append({'remote_file': remote_file,
                                          'type': _type, 'state': state,
                                          'remote_date': remote_mdate})
            else:
                files_to_load.append({'remote_file': remote_file,
                                      'type': _type, 'state': 'error',
                                      'remote_date': remote_mdate})
        return files_to_read, files_to_load

    def cch_file_downloader(self, cursor, uid, files_to_load,
                            tg_comer_cch_dir, sftp_url):
        '''
            Tries to download all the files in the "files_to_load" list from
            the sftp server
        :param files_to_load: List of files required to be downloaded from
        sftp server
        :param tg_comer_cch_dir: Local path to save the downloaded files
        :param sftp_url: Url to the sftp server
        :return: List of all files, indicating if its download was successful
                 or not
        '''
        sftp_obj = self.pool.get('tg.sftp')
        logger = netsvc.Logger()
        downloaded_files = []
        for new_file in files_to_load:
            # first try downloading the file
            try:
                logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                     "Downloading %s..." % new_file['remote_file'])
                remote_file_name = os.path.basename(new_file['remote_file'])
                local_file = os.path.join(
                    tg_comer_cch_dir, new_file['type'],
                    remote_file_name
                )
                sftp_obj.get_file(cursor, uid, new_file['remote_file'],
                                  local_file)
                # Change state to properly uploaded
                new_file['state'] = 'uploaded'
                new_file['local_file'] = local_file
            except Exception as exc:
                logger.notifyChannel("TG Reader", netsvc.LOG_ERROR, exc)
                # Leave the state as an error
                new_file['state'] = 'error'
            finally:
                downloaded_files.append(new_file)
        # Update the results of the download to mongo
        self.update_mongo_register(cursor, uid, downloaded_files, sftp_url)
        return downloaded_files

    def update_mongo_register(self, cursor, uid, downloaded_files, sftp_url):
        '''
            Update or create the registers in mongo "tg.comer.reader.register"
            for every file downloaded
        :param downloaded_files: List of every file downloaded
        :param sftp_url: Url to the sftp server
        :return:
        '''
        logger = netsvc.Logger()
        reg_obj = self.pool.get('tg.comer.reader.register')
        db = pooler.get_db_only(cursor.dbname)
        search_vals = [('server_from', '=', sftp_url)]
        reg_ids = reg_obj.search(cursor, uid, search_vals)
        reg_vals = reg_obj.read(cursor, uid, reg_ids, ['name'])
        mongo_file_names = [x['name'] for x in reg_vals]
        for down_file in downloaded_files:
            try:
                now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                remote_file_name = os.path.basename(down_file['remote_file'])
                if remote_file_name in mongo_file_names:
                    tmp_cr = db.cursor()
                    mongo_id = (item for item in reg_vals if item['name'] ==
                                remote_file_name).next()['id']
                    vals = {'date_load': now,
                            'state': down_file['state'],
                            }
                    reg_obj.write(tmp_cr, uid, [mongo_id], vals)
                    tmp_cr.commit()
                else:
                    tmp_cr = db.cursor()
                    vals = {'name': remote_file_name,
                            'date_load': now,
                            'server_from': sftp_url,
                            'state': down_file['state'],
                            'create_date': now,
                            'date_read': False,
                            }
                    reg_obj.create(tmp_cr, uid, vals)
                    tmp_cr.commit()
            except Exception as exc:
                logger.notifyChannel(
                    "TG Reader", netsvc.LOG_INFO,
                    "Error registering %s upload state..." % remote_file_name)
                tmp_cr.rollback()
            finally:
                tmp_cr.close()

    # Function to queue up
    @job(queue='cch_loader', timeout=10800)
    def cch_loader(self, cursor, uid, file_to_read, tg_comer_cch_dir, sftp_url):
        '''
            Tries to read and load every line of the "file_to_read" to the
            mongo registers
        :param file_to_read: File to read and load to mongo
        :param tg_comer_cch_dir: Path to local file
        :param sftp_url: Url to sftp server
        :return:
        '''
        reg_obj = self.pool.get('tg.comer.reader.register')
        search_vals = [('server_from', '=', sftp_url)]

        logger = netsvc.Logger()

        reg_ids = reg_obj.search(cursor, uid, search_vals)
        reg_vals = reg_obj.read(cursor, uid, reg_ids, ['name'])
        remote_file_name = os.path.basename(file_to_read['remote_file'])
        # Make sure the file has been properly downloaded
        if file_to_read['state'] == 'uploaded':
            try:

                if 'local_file' in file_to_read:
                    local_file = file_to_read['local_file']
                else:
                    local_file = os.path.join(tg_comer_cch_dir,
                                              file_to_read['type'],
                                              remote_file_name
                                              )
                logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                     "Loading %s..." % local_file)
                uri_backend = get_uri()
                backend = get_backend(uri_backend)
                with backend(uri_backend) as bnd:
                    if is_compressed_file(local_file):
                        with PackedCchFile(local_file) as psf:
                            for cch_file in psf:
                                for line in cch_file:
                                    if not line:
                                        continue
                                    try:
                                        bnd.insert(line)
                                    except Exception as exc:
                                        logger.notifyChannel(
                                            "TG Reader",
                                            netsvc.LOG_WARNING,
                                            exc
                                        )
                                        continue
                    else:
                        with CchFile(local_file) as sf:
                            for line in sf:
                                if not line:
                                    continue
                                try:
                                    bnd.insert(line)
                                except Exception as exc:
                                    logger.notifyChannel(
                                        "TG Reader",
                                        netsvc.LOG_WARNING,
                                        exc
                                    )
                                    continue
                logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                     "Finished reading %s..." % local_file)
                mongo_id = (item for item in reg_vals if item['name'] ==
                            remote_file_name).next()['id']
                logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                     "Mongo id is: %s" % mongo_id)
                now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                vals = {'date_read': now,
                        'state': 'readed'}
                logger.notifyChannel("TG Reader", netsvc.LOG_INFO,
                                     "Writing on file register. State: {} "
                                     "Date: {}".format(vals['state'],
                                                       vals['date_read']))
                reg_obj.write(cursor, uid, [mongo_id], vals)

            except Exception as exc:
                logger.notifyChannel("TG Reader", netsvc.LOG_ERROR, exc)


TgComerReader()


class TgComerReaderRegister(osv_mongodb.osv_mongodb):

    _name = 'tg.comer.reader.register'
    _order = 'date_load desc'

    _columns = {
        'name': fields.char('File name', size=100, readonly=True),
        'create_date': fields.datetime('Creation date', readonly=True),
        'date_load': fields.datetime('Date load', readonly=True),
        'date_read': fields.datetime('Date readed'),
        'server_from': fields.char('Server', size=100, readonly=True),
        'state': fields.selection([('readed', 'Readed'),
                                   ('error', 'Error'),
                                   ('uploaded', 'Uploaded')],
                                  'State', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'uploaded',
    }

TgComerReaderRegister()
