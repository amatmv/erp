# coding=utf-8
import logging
from tqdm import tqdm
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    tg_comer_obj = pool.get('tg.comer.reader.register')
    tg_sftp_obj = pool.get('tg.sftp')

    tg_sftp_ids = tg_sftp_obj.search(cursor, uid, [])
    for sftp_id in tqdm(tg_sftp_ids):
        tg_comer_ids = tg_comer_obj.search(cursor, uid, [('server_from', '=', {'id': sftp_id})])
        host = tg_sftp_obj.read(cursor, uid, sftp_id, ['host'])['host']
        if tg_comer_ids:
            tg_comer_obj.write(cursor, uid, tg_comer_ids, {'server_from': host})
        logger.info('Updated %s records for server %s', len(tg_comer_ids), host)



migrate = up
