# -*- coding: utf-8 -*-
{
    "name": "Telegestio comercialitzadora",
    "description": """
    Management of utility measurements (retailer view)
    """,
    "version": "0-dev",
    "author": "SomEnergia",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "mongodb_backend",
        "infraestructura",
        "giscedata_lectures_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "telegestio_comer_data.xml",
        "wizard/wizard_show_cchcurve_view.xml",
        "telegestio_comer_view.xml",
        "sftp_view.xml",
        "wizard/wizard_load_meter_curve_view.xml",
        "security/telegestio_comer_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
