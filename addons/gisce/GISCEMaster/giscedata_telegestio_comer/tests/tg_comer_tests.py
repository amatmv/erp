import unittest
import os
import random
from datetime import datetime
from addons import get_module_resource
import string
from destral import testing
from destral.transaction import Transaction
from expects import *
from mock import patch, Mock
from cchloader.file import CchFile
from cchloader.backends import get_backend


class CchLoaderTests(testing.OOTestCase):

    def setUp(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.f5d_path = get_module_resource(
                'giscedata_telegestio_comer', 'tests', 'data',
                'F5D_0021_0762_20170714.0'
            )
            self.mongo_reg = self.create_mongo_registers(cursor, uid)

    def tearDown(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.unlink_tg_models(cursor, uid)

    def unlink_tg_models(self, cursor, uid):
        for model_name in ['tg.comer.reader', 'tg.comer.reader.register',
                           'tg.cchfact', 'tg.cchval', 'tg.cchcons']:
            cch_obj = self.openerp.pool.get(model_name)
            del_ids = cch_obj.search(cursor, uid, [])
            cch_obj.unlink(cursor, uid, del_ids)

    def create_mongo_registers(self, cursor, uid):
        register_obj = self.openerp.pool.get('tg.comer.reader.register')
        names = []
        res = []
        for _ in range(10):
            file_name = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))
            path = '/test/path/file/'
            now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
            vals = {'name': file_name,
                    'date_load': now,
                    'server_from': 'test_server_url',
                    'state': random.choice(['uploaded', 'error']),
                    }
            _type = random.choice(['cchfact', 'cchval'])
            names.append({'name': vals['name'], 'type': _type, 'state': vals[
                'state'], 'remote_date': now, 'remote_mdate': now})
            res.append((vals['name'], _type, now, now, now))
            register_obj.create(cursor, uid, vals)
        return res, names

    def test_file_check(self):
        reader_obj = self.openerp.pool.get('tg.comer.reader')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            result = self.mongo_reg[0]
            mongo_names = self.mongo_reg[1]

            files_to_read, files_to_load = reader_obj.cch_file_check(cursor,
                                           uid, result, 'test_server_url')

            expect(len(files_to_read) + len(files_to_load)).to(equal(10))
            for item in mongo_names:
                if item['state'] == 'error':
                    expect(self.file_in_list(item['name'], files_to_load)
                           ).to(be_true)
                else:
                    expect(self.file_in_list(item['name'], files_to_read)
                           ).to(be_true)

    def file_in_list(self, file_name, file_list):
        for file in file_list:
            if file_name == file['remote_file']:
                return True
        return False

    def test_mongo_register_updater(self):
        reader_obj = self.openerp.pool.get('tg.comer.reader')
        reg_obj = self.openerp.pool.get('tg.comer.reader.register')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            d_files = []
            mongo_names = self.mongo_reg[1]
            for item in mongo_names:
                d_files.append({'remote_file': item['name'], 'type': item[
                    'type'], 'state': item['state']})
            # Add extra file to create a new register in mongo
            d_files.append({'remote_file': 'test_file_name', 'type':
                            'test_type', 'state': 'test_state'})
            reader_obj.update_mongo_register(cursor, uid, d_files,
                                             'test_server_url')

            search_vals = [('server_from', '=', 'test_server_url')]
            reg_ids = reg_obj.search(cursor, uid, search_vals)
            reg_vals = reg_obj.read(cursor, uid, reg_ids, ['name'])
            mongo_file_names = [x['name'] for x in reg_vals]
            expect('test_file_name' in mongo_file_names).to(be_true)

    def test_cch_file_downloader(self):
        reader_obj = self.openerp.pool.get('tg.comer.reader')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            with patch(
                    "giscedata_telegestio_comer.sftp.TgSftp.get_file") as \
                    tg_sftp:
                tg_sftp.start()
                tg_sftp.return_value = ['', '']

                d_files = []
                mongo_names = self.mongo_reg[1]
                for item in mongo_names:
                    d_files.append({'remote_file': item['name'], 'type': item[
                        'type'], 'state': item['state']})
                res = reader_obj.cch_file_downloader(cursor, uid, d_files,
                                                     'cch/dir/',
                                                     'test_server_url')
                for item in res:
                    l_path = '{0}{1}{2}{3}'.format('cch/dir/', item['type'],
                                                   '/', os.path.basename(item[
                                                    'remote_file']))
                    expect(item['state']).to(equal('uploaded'))
                    expect(item['local_file']).to(equal(l_path))

    def test_cch_loader(self):
        reader_obj = self.openerp.pool.get('tg.comer.reader')
        reg_obj = self.openerp.pool.get('tg.comer.reader.register')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            files_to_read = self.mongo_reg[1]
            r_files = []
            for item in files_to_read:
                r_files.append({'remote_file': item['name'], 'type': item[
                    'type'], 'state': 'uploaded'})
            r_files[0]['local_file'] = get_module_resource(
                'giscedata_telegestio_comer', 'tests', 'data',
                'F5D_0316_0631_20170309.tar.gz')
            reader_obj.cch_loader(cursor, uid, r_files[0], 'cch/dir/',
                                  'test_server_url')

            search_vals = [('server_from', '=', 'test_server_url'),
                           ('name', '=', r_files[0]['remote_file'])]
            reg_ids = reg_obj.search(cursor, uid, search_vals)
            reg_vals = reg_obj.read(cursor, uid, reg_ids, ['name', 'state'])
            expect(len(reg_vals)).to(equal(1))
            expect(reg_vals[0]['name']).to(equal(r_files[0]['remote_file']))
            expect(reg_vals[0]['state']).to(equal('readed'))
            self.unlink_tg_models(cursor, uid)

    def test_cch_loader_wrong_f5d_fields(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cls = self.openerp.pool.get('tg.comer.reader')
            tg_cchfact_obj = self.openerp.pool.get('tg.cchfact')
            reg_obj = self.openerp.pool.get('tg.comer.reader.register')

            now = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
            vals = {'name': 'F5D_0021_0762_20170714.0',
                    'date_load': now,
                    'server_from': 'url_test',
                    'state': 'uploaded',
                    }
            file_to_read = {
                'state': 'uploaded',
                'type': 'tests/data/',
                'remote_file': '/F5D_0021_0762_20170714.0',
                'local_file': self.f5d_path
            }
            reg_obj.create(cursor, uid, vals)
            cls.cch_loader(cursor, uid, file_to_read, '/', 'url_test')

            cchf_ids = tg_cchfact_obj.search(cursor, uid, [])
            expect(len(cchf_ids)).to(equal(96))
            self.unlink_tg_models(cursor, uid)
