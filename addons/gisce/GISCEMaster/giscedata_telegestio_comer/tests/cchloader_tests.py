# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction

from expects import *


class CCHLoaderTest(testing.OOTestCase):

    def setUp(self):
        self.load_curves()

    def tearDown(self):
        self.remove_curves()

    def load_curves(self):
        self.cch_data = {
            'tg.cchfact': {
                "name": "ES1234000000000004JS0F",
                "datetime": "2017-06-10 10:00:00",
                "season": 0,
                "ai": 35,
                "ao": 0,
                "r1": 0,
                "r2": 0,
                "r3": 0,
                "r4": 0,
                "source": 1,
                "validated": True,
                "create_at": "2017-07-01 10:00:00",
                "update_at": "2017-07-10 10:00:00",
                "bill": "123456789",
            },
            'tg.cchval': {
                "name": "ES1234000000000004JS0F",
                "datetime": "2017-06-10 10:00:00",
                "season": 1,
                "ai": 9000,
                "ao": 0,
                "create_at": "2017-07-01 10:00:00",
                "update_at": "2017-07-10 10:00:00",
            },
            'tg.p1': {
                "name": "ES1234000000000004JS0F",
                "measure_type": 11,
                "datetime": "2017-06-10 10:00:00",
                "season": 0,
                "ai": 0,
                "aiquality": 0,
                "ao": 0,
                "aoquality": 0,
                "source": 1,
                "r1": 0,
                "r1quality": 0,
                "r2": 0,
                "r2quality": 0,
                "r3": 0,
                "r3quality": 0,
                "r4": 1,
                "r4quality": 0,
                "reserve1": 0,
                "res1quality": 128,
                "reserve2": 0,
                "res2quality": 128,
                "source": 1,
                "validated": False,
                "create_at": "2017-07-01 10:00:00",
                "update_at": "2017-07-10 10:00:00",
            },
            'tg.f1': {
                "name": "ES1234000000000004JS0F",
                "measure_type": 11,
                "datetime": "2017-06-10 10:00:00",
                "season": 0,
                "ai": 15,
                "ao": 0,
                "r1": 0,
                "r2": 0,
                "r3": 0,
                "r4": 4,
                "reserve1": 0,
                "reserve2": 0,
                "source": 1,
                "validated": False,
                "create_at": "2017-07-01 10:00:00",
                "update_at": "2017-07-10 10:00:00",
            }
        }
        with Transaction().start(self.database) as txn:
            for cchmodel in self.cch_data.keys():
                self.openerp.pool.get(cchmodel).create(
                    txn.cursor, txn.user, self.cch_data[cchmodel]
                )

    def remove_curves(self):
        with Transaction().start(self.database) as txn:
            for cchmodel in self.cch_data.keys():
                cls = self.openerp.pool.get(cchmodel)
                cch_ids = cls.search(
                    txn.cursor, txn.user,
                    [('name', '=', 'ES1234000000000004JS0F')]
                )
                cls.unlink(txn.cursor, txn.user, cch_ids)

    def test_get_curve_cups_same_cups_suffix(self):
        with Transaction().start(self.database) as txn:
            cups = 'ES1234000000000004JS0F'
            for cchmodel in self.cch_data.keys():
                cls = self.openerp.pool.get(cchmodel)
                cups_name = cls.get_curve_cups(
                    txn.cursor, txn.user, cups
                )
                # gets same correct cups in db
                expect(cups_name).to(equal('ES1234000000000004JS0F'))

    def test_get_curve_cups_distinct_cups_suffix(self):
        with Transaction().start(self.database) as txn:
            cups = 'ES1234000000000004JS1P'
            for cchmodel in self.cch_data.keys():
                cls = self.openerp.pool.get(cchmodel)
                cups_name = cls.get_curve_cups(
                    txn.cursor, txn.user, cups
                )
                # gets cups with correct suffix in db
                expect(cups_name).to(equal('ES1234000000000004JS0F'))

    def test_get_curve_cups_inexistent_cups(self):
        with Transaction().start(self.database) as txn:
            cups = 'ES1234000000001234KA0F'
            for cchmodel in self.cch_data.keys():
                cls = self.openerp.pool.get(cchmodel)
                cups_name = cls.get_curve_cups(
                    txn.cursor, txn.user, cups
                )
                # gets cups required if it doesn't exists
                expect(cups_name).to(equal('ES1234000000001234KA0F'))