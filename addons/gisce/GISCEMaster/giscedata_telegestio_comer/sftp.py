# -*- coding: utf-8 -*-

import os
from datetime import datetime
from pysftp import Connection, CnOpts
from pysftp import ConnectionException, CredentialException, SSHException, AuthenticationException
from osv import osv, fields
from tools.translate import _
import socket
from StringIO import StringIO
import re


class TgSftp(osv.osv):

    _name = 'tg.sftp'
    connection = None

    def login(self, cursor, uid, server_id, context=None):
        '''login to a sftp server
        returns a connection to the server'''

        self.server_id = server_id
        if isinstance(server_id, (list, tuple)):
            self.server_id = server_id[0]

        cnopts = CnOpts()
        cnopts.hostkeys = None  # disable host key checking.

        server = self.browse(cursor, uid, self.server_id)
        params = {
            'host': server.host,
            'port': server.port,
            'username': server.user,
            'password': server.password,
            'private_key': server.private_key,
            'private_key_pass': server.private_key_pass,
            'cnopts': cnopts
        }
        try:
            self.connection = Connection(**params)
        except (ConnectionException, CredentialException, SSHException, AuthenticationException), e:
            raise osv.except_osv(_('Error!'),
                                 _('Connection error: %s') % (e))
        return self

    def close(self, cursor, uid, context=None):
        '''close connection to a sftp server'''
        return self.connection.close()

    def list_files(self, cursor, uid, path='/', recursive=False, supported_syntax=[], context=None):
        '''return list of files'''

        connection = self.connection

        file_list = []
        try:
            if recursive:
                for _type, enabled, syntax, folder, adate in supported_syntax:
                    if not enabled:
                        continue
                    remote_path = os.path.join(path, folder)

                    def _valid_file(filename):
                        if syntax:
                            m = re.match(syntax, os.path.basename(filename), re.IGNORECASE)
                            if m and m.groups():
                                _adate = datetime.strptime(m.groups(0)[-1], '%Y%m%d')
                                attrs = connection.lstat(filename)
                                _mdate = datetime.fromtimestamp(attrs.st_mtime)
                                file_list.append((filename,
                                                  _type,
                                                  adate,
                                                  _adate,
                                                  _mdate))

                    def _valid_dir(path):
                        pass

                    def _valid_unknown(path):
                        pass

                    connection.walktree(remote_path, _valid_file, _valid_dir, _valid_unknown, recurse=recursive)
        except Exception, e:
            raise osv.except_osv(_('Error!'),
                                 _('Directory listing error: %s') % (e))
        return file_list

    def get_file(self, cursor, uid, remote_path, local_path, context=None):
        '''return list of files to read'''
        try:
            res = self.connection.get(remote_path, local_path)
        except Exception, e:
            raise osv.except_osv(_('Error!'),
                                 _('File downloading error: %s') % (e))
        return res

    def read_file(self, cursor, uid, path, context=None):
        '''returns the content of a file as string'''
        output = StringIO()
        self.connection.getfo(path, output)
        return output.getvalue()

    def move_file(self, cursor, uid, path, origin_path, dest_path, context=None):
        '''Moves a file from origin_path to dest_path
        inside the ftp server. If dest_path do not exists
        it will be created'''
        filename = path.split('/')[-1]
        try:
            self.connection.cwd(dest_path)
        except Exception, e:
            self.connection.mkdir(dest_path)
        try:
            self.connection.rename('%s%s' % (origin_path, filename),
                                   '%s%s' % (dest_path, filename))
        except Exception, e:
            raise osv.except_osv(_('Error!'),
                                 _('Move file error: %s') % (e))
        return True

    def get_root_dir(self, cursor, uid, context=None):
        server = self.read(cursor, uid, self.server_id, ['root_dir'])

        return server['root_dir'] or '/'

    def get_read_dirs(self, cursor, uid, context=None):
        server = self.read(cursor, uid, self.server_id, ['read_dir'])

        dir_list = server['read_dir'].split(';') or ['/']

        return dir_list

    def _ip_address_valid(self, address):
        '''validates an ipv4 address'''
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return True

    def _hostname_valid(self, hostname):
        if len(hostname) > 255:
            return False
        if hostname[-1] == ".":
            hostname = hostname[:-1] # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)

        return all(allowed.match(x) for x in hostname.split("."))

    def _valid_host(self, cursor, uid, ids):
        '''validate hostname'''
        for server in self.browse(cursor, uid, ids):
            try:
                if socket.gethostbyname(server.host) == server.host:
                    if not self._ip_address_valid(server.host):
                        return False
                else:
                    if not self._hostname_valid(server.host):
                        return False
            except socket.error:
                return False
        return True

    def _valid_dir_name(self, dirname):
        '''checks dirname'''
        not_allowed_chars = '[,*?<>:\'"|]'
        if re.search(not_allowed_chars, dirname):
            return False
        else:
            return True

    def _path_valid(self, cursor, uid, ids):
        '''validates a SFTP directory path'''
        for server in self.browse(cursor, uid, ids):
            rdir = server.root_dir
            if not self._valid_dir_name(rdir):
                return False
            if not rdir or rdir[-1] != "/" or rdir[0] != "/":
                return False
            # semi-colon separated directories list
            r_dirs = server.read_dir.split(';')
            for rdir in r_dirs:
                if not self._valid_dir_name(rdir):
                    return False
                if not rdir or rdir[-1] != "/" or rdir[0] != "/":
                    return False
        return True

    _columns = {
        'name': fields.char('Description', size=150, required=True),
        'host': fields.char('Host', size=150, required=True),
        'port': fields.integer('Port'),
        'user': fields.char('User', size=100),
        'password': fields.char('Password', size=50),
        'private_key_binary': fields.binary('Private key file binary'),
        'private_key': fields.char('Private key file name', size=200),
        'private_key_pass': fields.char('Private key password', size=200),
        'root_dir': fields.char('Root dir', size=20),
        'read_dir': fields.char('Read dir(s)', size=255,
                                help='semi-colon(;) separated dir list'),
    }

    _defaults = {
        'port': lambda *a: 22,
        'root_dir': lambda *a: '/',
        'read_dir': lambda *a: '/',
    }

    _constraints = [(_valid_host,
                     _(u'Host not valid'),
                     ['host']),
                    (_path_valid,
                     _(u"FTP directory not valid. Invalid chars ',', '*', '?',"
                       u" '<' ,'>' ,':' ,'\"', '|' in dir name or FTP "
                       u'directories must be enclosed by slashes ("/"). i.e '
                       u'"/root/dir/"'),
                     ['root_dir', 'read_dir']),
                    ]

TgSftp()
