from cchloader.backends import register, MongoDBBackend, urlparse
from mongodb_backend.mongodb2 import mdbpool
import datetime


class ERPMongoDBBackend(MongoDBBackend):

    def __init__(self, uri):
        self.uri = mdbpool.uri
        self.config = urlparse(self.uri)
        self.connection = mdbpool.connection
        self.db = mdbpool.get_db()

    def insert_cch(self, cch):

        collection = cch.collection
        document = cch.backend_data

        counter = self.db['counters'].find_one_and_update(
                    {'_id': collection},
                    {'$inc': {'counter': 1}},
                    upsert=True)

        id = int(counter['counter'])

        update_time = datetime.datetime.now()
        document.update({
            'id': id,
            'update_at': update_time,
        })

        res = self.db[collection].update_many(
            {
                'name': document['name'],
                'datetime': document['datetime'],
                'season': document['season'],
            },
            {
                '$set': document,
                '$setOnInsert': {
                    'create_at': update_time,
                }
            },
            upsert=True,
        )

        return id


register("erpmongodb", ERPMongoDBBackend)
