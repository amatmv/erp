# -*- coding: utf-8 -*-
import zipfile
import base64
import csv
import time
import StringIO
from tools.translate import _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from osv import osv, fields
import os
import tempfile

from addons.giscedata_telegestio_comer.telegestio_comer import get_uri

from cchloader.file import CchFile, PackedCchFile
from cchloader.backends import get_backend

from cchloader.compress import is_compressed_file

from cchloader.parsers.parser import ParserNotFoundException


class WizardLoadMeterCurve(osv.osv_memory):
    """Assistent per convertir curves a fitxers F1.
    """
    _name = 'wizard.load.meter.curve'

    def _default_info(self, cursor, uid, context=None):
        info = _('Aquest assistent carregarà les curves d\'un comptador '
                 'a partir d\'un fitxer de corba.')

        return info

    def get_curve_name(self, document):
        """Gets CUPS from parsed line"""
        for key in document.keys():
            if key not in ['orig']:
                return key

    def get_cups(self, document):
        """Gets CUPS from parsed line"""
        curve_name = self.get_curve_name(document)
        return document.get(curve_name).data['name']

    def get_parser_name(self, sf):
        return sf.parser.__class__.__name__

    def load(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        wiz_id = ids[0]

        wiz = self.browse(cursor, uid, wiz_id)

        filename = wiz.filename
        file_content = base64.decodestring(str(wiz.file))

        processed_cups = []
        packed = False
        parser_name = ''

        try:
            # tempcchfile = tempfile.NamedTemporaryFile(prefix=filename)
            tempcchfile = open('/tmp/{0}'.format(filename), 'wb')
            tempcchfile.write(file_content)
            tempcchfile.flush()
            # Parse file
            uri = get_uri()
            backend = get_backend(uri)
            with backend(uri) as bnd:
                if is_compressed_file(tempcchfile.name):
                    with PackedCchFile(tempcchfile.name) as packed_sf:
                        packed = True
                        parser_name = self.get_parser_name(packed_sf)
                        for sf in packed_sf:
                            for line in sf:
                                if not line:
                                    continue
                                bnd.insert(line)
                                cups = self.get_cups(line)
                                if cups not in processed_cups:
                                    processed_cups.append(cups)
                else:
                    with CchFile(tempcchfile.name) as sf:
                        parser_name = self.get_parser_name(sf)
                        for line in sf:
                            if not line:
                                continue
                            bnd.insert(line)
                            cups = self.get_cups(line)
                            if cups not in processed_cups:
                                processed_cups.append(cups)

            info_txt_tmpl = _(
                u"Processats fitxer {0} format {1}{2} amb {3} CUPS:\n* {4}"
            )
            cups_list_txt = (processed_cups and "\n* ".join(processed_cups)
                             or _(u"WARNING: No s'ha carregat cap corba"))

            info = info_txt_tmpl.format(
                filename,
                parser_name,
                packed and _(u' comprimit') or '',
                len(processed_cups),
                cups_list_txt
            )
        except ParserNotFoundException, e:
            info = _(u"ERROR: No s'ha pogut processar el fitxer perquè no té "
                     u"un format conegut. Assegureu-vos que tan el nom del "
                     u"fitxer com el CSV tenen un dels formats esperats")
        finally:
            tempcchfile.close()
            os.remove(tempcchfile.name)

        wiz.write({'state': 'done', 'info': info})

        return


    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End'),
                                   ('on', 'On')], 'Estat'),
        'info': fields.text(_('Informació'), readonly=True),
        'meter_id': fields.many2one('giscedata.lectures.comptador',
                                    'Meter', required=True),
        'cups_name': fields.char('CUPS', size=22),
        'file': fields.binary('Fitxer'),
        'filename': fields.char("Nom Fitxer", size=256),
    }

    _defaults = {

        'state': lambda *a: 'init',
        'info': _default_info,
    }

WizardLoadMeterCurve()
