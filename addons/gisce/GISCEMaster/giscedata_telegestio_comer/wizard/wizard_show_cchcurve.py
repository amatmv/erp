# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import time
from datetime import datetime
from mongodb_backend.mongodb2 import mdbpool
import re


class WizardShowReadTG(osv.osv_memory):

    _name = 'wizard.show.cchcurve'

    def default_curve(self, cursor, uid, context=None):
        curve_name = context.get('cch', '')
        if curve_name:
            curve_model = 'tg.{0}'.format(curve_name)
        else:
            curve_model = context.get('curve_model', 'tg.cchval')
        return curve_model

    def action_show_cchcurve(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0], context=context)
        meter = wizard.meter_id
        # Build full name from serial if present
        cups_name = meter.polissa.cups.name

        cch_cups_name = self.pool.get(wizard.curve).get_curve_cups(
            cursor, uid, cups_name
        )

        domain = [('name', '=', cch_cups_name)]

        cch_name = wizard.curve.split('.')[1].upper()
        vals_view = {
            'name': '{0} TG {1}'.format(cups_name, cch_name),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': wizard.curve,
            'limit': 80,
            'type': 'ir.actions.act_window',
            'domain': domain,
        }

        return vals_view

    _columns = {
        'meter_id': fields.many2one('giscedata.lectures.comptador', 'Meter',
                                    required=True),
        'curve': fields.selection([
            ('tg.f1', 'F1'), ('tg.p1', 'P1'), ('tg.cchfact', 'F5D'),
            ('tg.cchval', 'P5D'), ('tg.cchcons', 'CCHCONS'),
            ('tg.cch_autocons', 'A5D'), ('tg.cch_gennetabeta', 'B5D')],
            'Origen',
            required=True),
    }

    _defaults = {
        'curve': default_curve
    }

WizardShowReadTG()
