#!/usr/bin/python
# coding=utf-8

from erppeek import Client
from tqdm import tqdm
import click


@click.command()
@click.option('--host', default=False, help='ERP host')
@click.option('--database', default=False, help='ERP host')
@click.option('--port', default='8069', help='ERP port', type=click.INT)
@click.option('--user', default='admin', help='ERP user')
@click.option('--password', default=None, help='ERP password')
def main(**kwargs):

    client = Client('{}:{}'.format(kwargs['host'], kwargs['port']), kwargs['database'],
                    kwargs['user'], kwargs['password'])

    tg_comer_obj = client.model('tg.comer.reader.register')
    tg_sftp_obj = client.model('tg.sftp')

    for sftp_id in tqdm(tg_sftp_obj.search()):
        host = tg_sftp_obj.read(sftp_id, ['host'])['host']
        temp_ids = tg_comer_obj.search([('server_from', '=', {'id': sftp_id})])
        if temp_ids:
            tg_comer_obj.write(temp_ids, {'server_from': host})


if __name__ == '__main__':
    main()
