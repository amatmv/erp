# -*- encoding: utf-8 -*-

from osv import osv, fields
from osv.expression import OOQuery

MAPPING_NOTI_STATES = {
    'entregado': {},
    'enviat': {},
    'perdida': {},
    'no_entrega': {}
}


class AccountInvoicePendingState(osv.osv):

    _name = 'account.invoice.pending.state'
    _inherit = 'account.invoice.pending.state'

    def search_data_name_by_id(self, cursor, uid, state_id, context=None):
        imd_obj = self.pool.get('ir.model.data')
        res = False
        q = OOQuery(imd_obj, cursor, uid)
        sql = q.select(['name']).where([
            ('model', '=', 'account.invoice.pending.state'),
            ('res_id', '=', state_id),
        ])
        cursor.execute(*sql)
        for result in cursor.dictfetchall():
            res = result['name']
            break

        return res

    def notification_delivered(self, cursor, uid, invoice_id,
                               origen_pending_state_id, context=None):

        return self.notification_generic(
            cursor, uid, invoice_id, origen_pending_state_id, 'entregado',
            context=context
        )

    def notification_lost(self, cursor, uid, invoice_id,
                          origen_pending_state_id, context=None):

        return self.notification_generic(
            cursor, uid, invoice_id, origen_pending_state_id, 'perdida',
            context=context
        )

    def notification_sent(self, cursor, uid, invoice_id,
                          origen_pending_state_id, context=None):

        return self.notification_generic(
            cursor, uid, invoice_id, origen_pending_state_id, 'enviat',
            context=context
        )

    def notification_not_delivered(self, cursor, uid, invoice_id,
                                   origen_pending_state_id, context=None):

        return self.notification_generic(
            cursor, uid, invoice_id, origen_pending_state_id, 'no_entrega',
            context=context
        )

    def notification_generic(self, cursor, uid, invoice_id,
                             origen_pending_state_id, tipo_noti, context=None):

        inv_obj = self.pool.get('account.invoice')

        inv_state = inv_obj.read(
            cursor, uid, invoice_id, ['state'], context=context
        )['state']

        if inv_state == 'paid':
            pending_state_obj = self.pool.get('account.invoice.pending.state')
            proces_id = pending_state_obj.read(
                cursor, uid, ['process_id'], context=context
            )['process_id'][0]

            correct_state = pending_state_obj.search(
                cursor, uid,
                [
                    ('process_id', '=', proces_id),
                    ('weight', '=', 0)
                ]
            )

            return correct_state

        pend_name = self.search_data_name_by_id(
            cursor, uid, origen_pending_state_id, context=context
        )

        pend_dest_name = MAPPING_NOTI_STATES[tipo_noti].get(pend_name, False)

        if not pend_dest_name:
            return False

        pend_dest_id = self.search_destination_pending_by_name(cursor, uid, pend_dest_name)

        inv_obj.set_pending(
            cursor, uid, [invoice_id], pend_dest_id, context=context
        )

        return pend_dest_id

    def search_destination_pending_by_name(self, cursor, uid, pend_dest_name):

        return super(
            AccountInvoicePendingState, self
        ).search_destination_pending_by_name(
            cursor, uid, pend_dest_name
        )


AccountInvoicePendingState()