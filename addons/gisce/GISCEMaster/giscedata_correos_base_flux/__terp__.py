# -*- coding: utf-8 -*-
{
    "name": "Giscedata correos Base Flujos",
    "description": """
    Modulo base con las funcionalidades genericas
    de flujo para el sistema de correos para funcionar con
    qualquier flujo
    """,
    "version": "0-dev",
    "author": "Gisce",
    "category": "Polissa",
    "depends":[
        "base",
        "base_extended",
        "giscedata_facturacio",
        "giscedata_polissa",
        "account",
        "account_invoice_pending",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}