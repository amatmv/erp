# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Enero 2018",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 314 - 22/12/2017.
 ETU/1282/2017
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20180101_data.xml"
    ],
    "active": False,
    "installable": True
}
