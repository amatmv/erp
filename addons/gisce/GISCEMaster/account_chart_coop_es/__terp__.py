# -*- coding: utf-8 -*-
{
    "name": "Account Chart Coop",
    "description": """
    Mòdulo para crear las cuentas para cooperativas
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Account",
    "depends":[
        "base",
        "l10n_chart_ES",
        "account",
        "account_chart_update"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "account_chart_coop_es_data.xml"
    ],
    "active": False,
    "installable": True
}
