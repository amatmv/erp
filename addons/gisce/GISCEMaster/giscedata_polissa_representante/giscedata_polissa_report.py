# -*- coding: utf-8 -*-
from osv import osv


class PolissaReport(osv.osv):
    _auto = False
    _name = "giscedata.polissa.report"
    _inherit = "giscedata.polissa.report"

    def representante(self, cursor, uid, polissa, translate_call):
        """
        Obtains the HTML code that adds the representante in the contract report
        :param cursor: DB Cursor
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param polissa: <giscedata.polissa> browse record.
        :type polissa: orm.browse_record
        :param translate_call: function that translates the report strings
        :return:
        """
        mako_path = "giscedata_polissa_representante/report/contracte.mako"
        variables = {
            '_': translate_call,
            'polissa': polissa
        }
        html = self.render_mako_code(mako_path, variables)

        return html


PolissaReport()
