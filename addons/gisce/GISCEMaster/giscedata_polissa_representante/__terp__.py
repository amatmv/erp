# -*- coding: utf-8 -*-
{
  "name": "Polissa representante",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Campo representante para la polissa
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      "partner_representante",
      "giscedata_polissa"
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
    "giscedata_polissa_view.xml",
  ],
  "active": False,
  "installable": True
}
