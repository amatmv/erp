from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'representante_id': fields.many2one(
            'res.partner', 'Representante', readonly=True,
             states={
                 'esborrany':[('readonly', False)],
                 'validar': [('readonly', False)],
                 'modcontractual': [('readonly', False)]
            })
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'representante_id': fields.many2one('res.partner', 'Representante')
    }

GiscedataPolissaModcontractual()
