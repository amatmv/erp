# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad ITC 3353/2010",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Pagos por capacidad ITC 3353/2010
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_pagos_capacidad"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_pagos_capacidad_2010_data.xml"
    ],
    "active": False,
    "installable": True
}
