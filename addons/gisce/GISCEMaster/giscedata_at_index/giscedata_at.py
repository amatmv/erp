from base_index.base_index import BaseIndex


class GiscedataAtLinia(BaseIndex):
    """
    Index for giscedata.at.linia
    """
    _name = 'giscedata.at.linia'
    _inherit = 'giscedata.at.linia'

    _index_title = '{obj.name}'
    _index_summary = '{obj.name} - {obj.origen} - {obj.final}'

    _index_fields = {
        'name': True,
        'descripcio': True,
        'origen': True,
        'final': True,
        'municipi.name': True,
        'tensio': True,
        "expedients_string": True
    }


GiscedataAtLinia()


class GiscedataAtTram(BaseIndex):
    """
    Index for giscedata.at.tram
    """
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _index_title = '{obj.name}'
    _index_summary = '{obj.name} - {obj.origen} - {obj.final}'

    _index_fields = {
        'cini': True,
        'linia.name': True,
        'linia.descripcio': True,
        'linia.origen': True,
        'linia.final': True,
        'linia.municipi.name': True,
        'linia.tensio': True,
        'data_pm': True,
        'name': True,
        'origen': True,
        'final': True,
        'cable.name': True,
        'cable.seccio': True,
        'cable.material.name': True,
        'expedients': True,
    }


GiscedataAtTram()


class GiscedataAtSuport(BaseIndex):
    """
    Index for giscedata.at.suport
    """
    _name = 'giscedata.at.suport'
    _inherit = 'giscedata.at.suport'

    _index_title = '{obj.name}'
    _index_summary = '{obj.name} - {obj.linia.name}'

    _index_fields = {
        'name': True,
        'linia.name': True,
        'poste.name': True,
        'poste.altura': True,
        'poste.esforc': True,
    }


GiscedataAtSuport()
