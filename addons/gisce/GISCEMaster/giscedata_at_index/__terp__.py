# -*- coding: utf-8 -*-
{
    "name": "Index LAT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index giscedata_at
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscedata_at",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
