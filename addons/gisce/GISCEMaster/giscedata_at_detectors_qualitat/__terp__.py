# -*- coding: utf-8 -*-
{
    "name": "GISCE Detectors Qualitat",
    "description": """Detectors Qualitat""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Alta Tensió",
    "depends":[
        "base_extended",
        "giscedata_at_detectors",
        "giscedata_qualitat"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_qualitat_view.xml"
    ],
    "active": False,
    "installable": True
}
