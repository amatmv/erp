# -*- coding: utf-8 -*-
from osv import osv, fields

class giscedata_qualitat_span(osv.osv):
    _inherit = 'giscedata.qualitat.span'
    _name = 'giscedata.qualitat.span'

    _columns = {
        'detectors_ids': fields.many2many('giscedata.at.detectors',
                                          'giscedata_at_span_detectors',
                                          'span_id', 'detector_id',
                                          'Detectors'),
    }

giscedata_qualitat_span()