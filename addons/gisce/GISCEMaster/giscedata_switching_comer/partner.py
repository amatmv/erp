# coding=utf-8
from osv import osv


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def search_unactive_sips(self, cursor, uid, ids, who='titular', context=None):
        if context is None:
            context = {}
        assert who in ('pagador', 'titular')
        polissa_obj = self.pool.get('giscedata.polissa')
        owned_contracts = []
        for partner_id in ids:
            pol_ids = polissa_obj.search(cursor, uid, [
                (who, '=', partner_id),
                ('no_cessio_sips', '=', 'unactive')
            ])
            owned_contracts = owned_contracts + pol_ids
        return owned_contracts

    def request_no_cession_sips(self, cursor, uid, ids, who='titular', context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        owned_contracts = self.search_unactive_sips(cursor, uid, ids, who, context=context)
        return polissa_obj.request_no_cession_sips(cursor, uid, owned_contracts)


ResPartner()
