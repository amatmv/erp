# -*- coding: utf-8 -*-
"""
Copiar el cos dels missatges de les plantilles "antigues" a les noves
"""
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('migration')
    logger.info('Updating switching actualitzar_tarifa_comer')
    update_query = "UPDATE giscedata_switching SET actualitzar_tarifa_comer = True;"
    cursor.execute(update_query)

migrate = up
