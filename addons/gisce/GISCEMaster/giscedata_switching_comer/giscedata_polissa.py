# -*- coding: utf-8 -*-
from osv import osv, fields
import logging
from datetime import datetime


logger = logging.getLogger('openerp.' + __name__)


class GiscedataPolissa(osv.osv):
    """Pòlissa (Comercialitzadora)."""
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'
    _description = "Polissa d'un Client"

    def _get_distri_phone(
            self, cursor, uid, ids, field_name, arg, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        pa_obj = self.pool.get('res.partner.address')
        partner_obj = self.pool.get('res.partner')
        vals = self.read(
            cursor, uid, ids, ['distribuidora', 'cups'], context=context)
        res = {}
        for v in vals:
            pol_id = v['id']
            distri_id = v['distribuidora'][0]
            cups_id = v['cups'][0]
            try:
                partner_id = sw_obj.partner_map_id(cursor, uid, cups_id, distri_id)
            except Exception as exc:
                res[pol_id] = ''
                logger.error('Error obtenint telefon distri: {}'.format(exc))
                continue
            second_partner_id = partner_obj.browse(cursor, uid, distri_id)
            second_telf = (
                    second_partner_id['address']
                    and second_partner_id['address'][0].phone or ''
            )
            if partner_id:
                pa_ids = pa_obj.search(cursor, uid, [
                    ('partner_id', '=', partner_id)
                ])
                if pa_ids:
                    address = pa_obj.read(
                        cursor, uid, [pa_ids[0]], ['phone']
                    )
                    telf = address and address[0]['phone'] or ''
                    res[pol_id] = telf or second_telf
                else:
                    res[pol_id] = second_telf
            else:
                res[pol_id] = second_telf
        return res

    def request_no_cession_sips(self, cursor, uid, ids, context=None):
        """
        Request a no cession in the SIPS file (base method)
        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        imd_obj = self.pool.get('ir.model.data')
        r1_wizard = self.pool.get('wizard.subtype.r1')
        cfg = self.pool.get('res.config')

        subtipus_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_subtipus_reclamacio',
            'subtipus_reclamacio_038'
        )[1]

        cas_ids = []
        for polissa_id in ids:
            ctx = context.copy()
            ctx['polissa_id'] = polissa_id

            values = {
                'tipus': '01',
                'subtipus_id': subtipus_id,
                'polissa_id': polissa_id,
                'comentaris': cfg.get(cursor, uid, 'sw_r1_038_default_comment')

            }
            wiz_id = r1_wizard.create(cursor, uid, values, context=ctx)
            result = r1_wizard.action_create_r1_case(cursor, uid, [wiz_id])
            cas_ids.append(result['domain'][0][2])

        self.write(cursor, uid, ids, {
            'no_cessio_sips': 'requested',
            'no_cessio_sips_data': datetime.now().strftime('%Y-%m-%d')
        })
        return cas_ids

    _columns = {
        'distri_phone': fields.function(
            _get_distri_phone, method=True,
            type='string', string='Telèfon distribuidora'
        )
    }


GiscedataPolissa()
