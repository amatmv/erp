# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from osv import osv, fields
from tools.translate import _
from ast import literal_eval
import netsvc
from datetime import datetime, timedelta
from giscedata_switching.giscedata_switching_validacions_lectures import ValidadorLecturesATR
from giscedata_facturacio_comer.giscedata_polissa import _modes_facturacio

POTENCIES_TRIFASIQUES = [5.196, 6.928, 10.392, 13.856]


class GiscedataSwitching(osv.osv):
    _name = 'giscedata.switching'
    _inherit = "giscedata.switching"

    def escull_tarifa_comer(self, cursor, uid, ids, tarifa_atr,
                            context=None):

        if context is None:
            context = {}

        sw_id = ids
        if isinstance(ids, (tuple, list)):
            sw_id = ids[0]

        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        polissa_obj = self.pool.get('giscedata.polissa')

        sw_fields = ['tarifa_comer_id', 'name', 'cups_polissa_id',
                     'actualitzar_tarifa_comer']
        sw_vals = self.read(cursor, uid, sw_id, sw_fields, context)

        polissa_id = sw_vals['cups_polissa_id'][0]
        polissa_info = polissa_obj.read(
            cursor, uid, polissa_id, ['tarifa', 'llista_preu'], context=context
        )

        tarifa = tarifa_obj.browse(cursor, uid, tarifa_atr)

        if (not sw_vals.get('actualitzar_tarifa_comer')
            and polissa_info.get('llista_preu')
            and tarifa.id == polissa_info.get('tarifa')[0]):
            return polissa_info['llista_preu'][0]

        llistes_preus = [l for l in tarifa.llistes_preus_comptatibles
                         if l.type == 'sale']
        if not llistes_preus:
            #ERROR, No hem trobat llista de preus
            info = _(u"No hem trobat llista de preus per la tarifa ATR"
                     u" '%s' del cas '%s'") % (tarifa.name, sw_vals['name'])
            raise osv.except_osv('Error', info)

        # Si arribem a aquest punt significa que hem d'escollir la tarifa o
        # perque ho hem seleccionat o perque la tarifa ATR del contracte es
        # diferent a la del cas ATR
        if sw_vals.get('tarifa_comer_id'):
            tarifa_comer_id = sw_vals['tarifa_comer_id'][0]
            if tarifa_comer_id not in [lp.id for lp in llistes_preus]:
                #ERROR, La llista de preus no és compatible
                tarifa_comer_name = sw_vals['tarifa_comer_id'][1]
                info_tmpl = _(u"La llista de preus '{0}' no és compatible "
                              u"amb la tarifa ATR '{1}' del cas '{2}'")
                info = info_tmpl.format(
                    tarifa_comer_name, tarifa.name, sw_vals['name']
                )
                raise osv.except_osv('Error', info)

            return tarifa_comer_id

        # funció per escollir la tarifa única (si es pot)
        llista_preus = polissa_obj.escull_llista_preus(
            cursor, uid, polissa_id, llistes_preus, context
        )

        if not llista_preus:
            #ERROR, No hem trobat una tarifa única
            tarifes_comer = [t.name for t in llistes_preus]
            txt = _(u"No hem pogut escollir una tarifa automàticament"
                    u" per la tarifa ATR '%s' del cas '%s' perquè hem"
                    u" trobat %d tarifes: %s")
            info = txt % (tarifa.name, sw_vals['name'], len(llistes_preus),
                          tarifes_comer)
            raise osv.except_osv('Error', info)

        return llista_preus.id

    _columns = {
        'tarifa_comer_id': fields.many2one('product.pricelist',
                                           'Tarifa Comercialitzadora',
                                           help=u"Tarifa a aplicar si hi ha "
                                                u"canvi de tarifa d'accés"),
        'mode_facturacio': fields.selection(
            _modes_facturacio, u'Mode facturació',),
        'actualitzar_tarifa_comer': fields.boolean(
            "Actualitzar llista de preus",
            help=u"Si està marcat al activar el cas ATR s'actualitzarà la "
                 u"tarifa de comercializadora del contracte amb la tarifa "
                 u"associada al cas ATR"
        ),
        'actualitzar_mode_facturacio': fields.boolean(
            "Actualitzar mode facturacio",
            help=u"Si està marcat al activar el cas ATR s'actualitzarà el "
                 u"mode de facturacio del contracte amb el mode"
                 u"associat al cas ATR"
        )
    }

    _defaults = {
        'tarifa_comer_id': lambda *a: False,
        'actualitzar_tarifa_comer': lambda *a: False,
    }

GiscedataSwitching()


class GiscedataSwitchingPuntMesura(osv.osv):

    _name = 'giscedata.switching.pm'
    _inherit = "giscedata.switching.pm"

    def polissa_read_on_date(self, cursor, uid, polissa, date, context=None):
        '''Returns polissa data on date'''
        if not context:
            context = {}

        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        polissa_vals = polissa.read(['tarifa', 'potencia'],
                                    context={'date': date})

        tarifaatr = tarifa_obj.read(cursor, uid,
                                    polissa_vals[0]['tarifa'][0],
                                    ['codi_ocsum'])['codi_ocsum']
        potencia = polissa_vals[0]['potencia']

        return {'tarifaatr': tarifaatr, 'potencia': potencia}

    def can_load_measures(self, cursor, uid, process, step, sw=None, context=None):
        ''' Checks if current process and step can be evaluated '''
        if not context:
            context = {}

        res_config = self.pool.get('res.config')

        if sw:
            # Check if we ignore the distri when loading measures
            conf = res_config.get(cursor, uid, 'sw_distris_ignore_load_measures', "[]")
            ignored_distris = literal_eval(conf)
            distri = sw.cups_id and sw.cups_id.distribuidora_id.ref
            if distri in ignored_distris:
                return False

        conf = res_config.get(cursor, uid, 'sw_load_measures_steps', False)

        if not conf:
            return True
        else:
            conf = literal_eval(conf)
            if not conf:
                return False
            else:
                case = "{0}-{1}".format(process, step)
                return case in conf

    def activa_pm_a_polissa(self, cursor, uid, ids, context=None):
        ''' Activem i o creem el comptador i l'associem a la pólissa
            Si s'escau, també podem carregar les lectures'''

        logger = netsvc.Logger()

        if not context:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = dict([(i, {'comptadors': []}) for i in ids])

        compta_obj = self.pool.get('giscedata.lectures.comptador')
        aparell_obj = self.pool.get('giscedata.switching.aparell')

        for id_pm in ids:
            pm = self.browse(cursor, uid, id_pm)
            step_header = pm.header_ids[0]
            sw = step_header.sw_id
            polissa = sw.cups_polissa_id

            # calculem el pas
            pas_obj = self.pool.get(sw.step_id.get_step_model())
            pas_id = pas_obj.search(cursor, uid,
                                    [('sw_id', '=', sw.id)])[0]

            pas = pas_obj.browse(cursor, uid, pas_id)

            comptadors_afegits = []
            comptadors_afegits_names = []
            comptadors_baixa = []
            lectures_afegits = 0
            maximetres_afegits = 0
            errors = []

            #busquem la tarifa de la polissa per la data_alta
            data_alta = pas.data_activacio
            try:
                polissa_vals = self.polissa_read_on_date(cursor, uid, polissa,
                                                         data_alta, context)
            except Exception, e:
                txt = _(u"Polissa %s no existia el dia %s: %s")
                logger.notifyChannel('comptadors_atr', netsvc.LOG_INFO,
                                     txt % (polissa.name, data_alta,
                                            e.value))
                errors.append(txt % (polissa.
                                     name, data_alta, e.value))
                continue
            tarifaatr_actual = polissa_vals['tarifaatr']
            potencia_actual = polissa_vals['potencia']

            # Intentem crear tots els comptadors:
            for aparell in pm.aparell_ids:
                tarifaatr = tarifaatr_actual
                potencia = potencia_actual
                # Només activem el comptadors que que són de tipus
                # o tenen lectures:
                tipus_activar = ['CA', 'CC', 'CG', 'P']
                if aparell.tipus not in tipus_activar:
                    # Mirarem si tenen lectures
                    if not aparell.mesura_ids:
                        continue
                num_serie = aparell.num_serie

                search_vals = [('name', '=', num_serie),
                               ('polissa', '=', polissa.id)]
                compta_id = compta_obj.search(cursor, uid, search_vals,
                                              context={'active_test': False})
                # mirem si el mateix número de comptador (traien els zeros)
                # existeix
                if not compta_id:
                    for meter in polissa.comptadors:
                        try:
                            num_serie_int = int(num_serie)
                            meter_name = int(meter.name)
                        except:
                            meter_name = False
                            num_serie_int = False

                        if meter_name and meter_name == num_serie_int:
                            compta_id = [meter.id]
                            break

                if compta_id:
                    compta_id = compta_id[0]
                else:
                    # Si la pólissa ja té un comptador de alta,
                    # el donem de baixa
                    data_baixa = datetime.strptime(data_alta, "%Y-%m-%d") - timedelta(days=1)
                    data_baixa = data_baixa.strftime("%Y-%m-%d")
                    actual_meters = [c for c in polissa.comptadors
                                     if c.name != num_serie and c.active]
                    for actual_meter in actual_meters:
                        vals = {'active': False,
                                'data_baixa': data_baixa}
                        actual_meter.write(vals)
                        comptadors_baixa.append(actual_meter.name)
                    # Creem el comptador
                    gir = '1' + '0' * aparell.enters
                    # MAX pg int is 2147483648 (9 zeros)
                    if len(gir) > 9:
                        gir = '1' + '0' * 9
                    # Propietat '1': Distribuidora és de l'empresa ('empresa')
                    # la resta 'client'
                    propietat = (aparell.propietat in '1' and 'empresa'
                                 or 'client')
                    vals = {'name': num_serie,
                            'polissa': polissa.id,
                            'giro': gir,
                            'data_alta': data_alta,
                            'active': False,
                            'propietat': propietat,
                            'lloguer': False}

                    lloguer_txt = ''
                    if propietat == 'empresa':
                        lloguer_vals = aparell_obj.calcula_lloguer_comptador(
                            cursor, uid, aparell.id, tarifaatr=tarifaatr,
                            potencia=potencia)
                        if lloguer_vals:
                            vals.update(lloguer_vals)
                            lloguer_txt = (" (%s€/dia)" %
                                           lloguer_vals['preu_lloguer'])

                    compta_id = compta_obj.create(cursor, uid, vals)
                    if compta_id:
                        txt = num_serie + lloguer_txt
                        comptadors_afegits_names.append(num_serie)
                        comptadors_afegits.append(u"%s" % txt)
                # Si tenim comptador, podem carregar les lectures
                if compta_id:
                    compta_vals = compta_obj.read(cursor, uid, compta_id,
                                                  ['data_alta', 'data_baixa',
                                                   'active', 'name'])
                    # Si el moviment és donar de baixa (DX) , donem de baixa
                    # el comptador i les lectures les posem del dia anterior
                    if aparell.tipus_moviment in ['DX']:
                        ok = compta_obj.write(cursor, uid, compta_id,
                                              {'active': False,
                                               'data_baixa': data_alta})
                        dt_format = '%Y-%m-%d'
                        data_alta = (datetime.strptime(data_alta, dt_format)
                                     - timedelta(days=1)).strftime(dt_format)
                        # Tornem a calcular la tarifaatr per la nova data
                        try:
                            polissa_vals = self.polissa_read_on_date(
                                cursor, uid, polissa, data_alta, context)
                        except Exception, e:
                            txt = _(u"Polissa %s no existia el dia %s: %s")
                            logger.notifyChannel(
                                'comptadors_atr', netsvc.LOG_INFO,
                                txt % (polissa.name, data_alta, e.value))
                            errors.append(
                                txt % (polissa.name, data_alta, e.value))
                            continue
                        tarifaatr = polissa_vals['tarifaatr']
                        if ok:
                            comptadors_baixa.append(num_serie)
                    elif not compta_vals['active'] and aparell.mesura_ids:
                        compta_obj.write(cursor, uid, compta_id,
                                         {'active': True,
                                          'data_baixa': False})

                    proces = step_header.sw_id.proces_id.name
                    pas = step_header.sw_id.step_id.name
                    if self.can_load_measures(cursor, uid, proces, pas, sw=sw):
                        lect_res = aparell_obj.carrega_lectures_a_comptador(
                            cursor, uid, aparell.id, data_alta, compta_id,
                            tarifaatr, sw_id=sw.id, context=context)
                        lectures_afegits += lect_res['lectures']
                        maximetres_afegits += lect_res['maxims']

                        if 'errors' in lect_res:
                            errors += lect_res['errors']

            res_config = self.pool.get('res.config')
            conf = eval(res_config.get(cursor, uid, 'sw_dont_load_meters_without_readings', "[]"))
            if sw.proces_id.name in conf and lectures_afegits <= 0 and maximetres_afegits <= 0:
                # Si no s'han afegit lectures, desfer el canvi de contador.
                polissa = polissa.browse()[0]
                data_baixa = datetime.today().strftime("%Y-%m-%d")
                for comptador in polissa.comptadors:
                    if comptador.name in comptadors_afegits_names:
                        comptador.write({'active': False, 'data_baixa': data_baixa})
                        comptador.unlink()
                    elif comptador.name in comptadors_baixa:
                        comptador.write({'active': True, 'data_baixa': False})

            txt_tpl = _(u"[%s] %s [%s] -> S'han creat els comptadors '%s', %s "
                        u"lectures i %s maxímetres")
            valors = (sw.id, sw.name, sw.step_id.name,
                      ','.join(comptadors_afegits), lectures_afegits,
                      maximetres_afegits)
            txt = txt_tpl % valors

            logger.notifyChannel('comptadors_atr', netsvc.LOG_INFO, txt)

            res[id_pm] = {'comptadors': comptadors_afegits,
                          'baixes': comptadors_baixa,
                          'lectures': lectures_afegits,
                          'maximetres': maximetres_afegits,
                          'errors': errors,
                          }
        return res

GiscedataSwitchingPuntMesura()


class GiscedataSwitchingAparell(osv.osv):

    _name = 'giscedata.switching.aparell'
    _inherit = "giscedata.switching.aparell"

    def calcula_lloguer_comptador(self, cursor, uid, ids, tarifaatr=None,
                                  potencia=None, context=None):

        return {}

    def carrega_lectures_a_comptador(self, cursor, uid, ids, data_lectura,
                                     comptador_id=None, tarifaATR=None,
                                     sw_id=None, context=None):
        """
        Agafa les lectures de l'aparell i les carrega al comptador que
        correspongui
        """
        logger = netsvc.Logger()

        if not context:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = {'lectures': 0, 'maxims': 0, 'errors': []}

        if not comptador_id or not tarifaATR:
            return res

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        origen_comer_obj = self.pool.get('giscedata.lectures.origen_comer')
        conf_obj = self.pool.get('res.config')

        origen_comer_atr = origen_comer_obj.search(cursor, uid,
                                                   [('codi', '=', 'AT')])[0]
        conf_id = 'sw_carrega_lectures_a_pool'
        lect_a_pool = bool(int(conf_obj.get(cursor, uid, conf_id, '1')))

        for aparell_id in ids:
            # validem comptador
            apa_vals = self.read(cursor, uid, aparell_id, ['num_serie'])
            meter_vals = meter_obj.read(cursor, uid, comptador_id,
                                        ['name', 'active', 'data_baixa'])
            # Si són numèrics, poden tenir zero's i ho tenim en compte al
            # comparar
            try:
                num_serie_int = int(apa_vals['num_serie'])
                meter_name = int(meter_vals['name'])
            except:
                num_serie_int = False
                meter_name = False

            if (meter_vals['name'] != apa_vals['num_serie']
                and num_serie_int != meter_name):
                # El comptador no és correcte
                txt = (_(u"Aquest aparell '%s' no es correspon amb el "
                         u"comptador '%s'.") % (apa_vals['num_serie'],
                                                meter_vals['name']))
                res['errors'].append(txt)
                logger.notifyChannel('lectures_atr', netsvc.LOG_WARNING,
                                     txt)
                continue
            lectures_vals = self.get_lectures_vals(cursor, uid, aparell_id,
                                                   comptador_id, tarifaATR)
            # Actualitzem origen i data de lectura
            update_vals = {'origen_comer_id': (origen_comer_atr,)}
            for l in lectures_vals['lectures']:
                l.update(update_vals)
                if not l.get('name'):
                    l.update({'name': data_lectura})
            for l in lectures_vals['maximetres']:
                l.update(update_vals)
                if not l.get('name'):
                    l.update({'name': data_lectura})

            validator = ValidadorLecturesATR()
            correct_lect = []
            correct_max = []
            measures_to_validate = validator.agrupar_lectures_per_data(
                lectures_vals['lectures']+lectures_vals['maximetres']
            )
            for date in measures_to_validate:
                try:
                    res_lect = validator.validate(self.pool, cursor, uid, measures_to_validate[date], sw_id, context=context)
                    if not res_lect.valid:
                        res['errors'].append(res_lect.error_msg)
                    else:
                        for measure in measures_to_validate[date]:
                            if measure in lectures_vals['lectures']:
                                correct_lect.append(measure)
                            else:
                                correct_max.append(measure)

                except Exception, e:
                    res['errors'].append(e.message)

            try:
                # si tenim lectures hem d'activar el comptador
                # si no té data_de baixa
                if not meter_vals['active'] and not meter_vals['data_baixa']:
                    meter_obj.write(cursor, uid, comptador_id, {'active': 1})

                found_errors = res['errors']
                res = meter_obj.inserta_lectures(cursor, uid,
                                                 correct_lect,
                                                 correct_max,
                                                 pool=lect_a_pool)
                if not res.get('errors'):
                    res['errors'] = []
                res['errors'].extend(found_errors)

            except Exception, e:
                # El comptador no és correcte
                txt = (_(u"No em pogut insertar les lectures al comptador"
                         u" %s: %s") % (meter_vals['name'], e))
                res['errors'].append(txt)
                logger.notifyChannel('lectures_atr', netsvc.LOG_WARNING,
                                     txt)

        return res

GiscedataSwitchingAparell()
