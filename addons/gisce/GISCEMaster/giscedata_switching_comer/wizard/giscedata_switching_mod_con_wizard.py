# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv
from giscedata_facturacio_comer.giscedata_polissa import _modes_facturacio


class GiscedataSwitchingModConWizard(osv.osv_memory):
    _name = 'giscedata.switching.mod.con.wizard'
    _inherit = 'giscedata.switching.mod.con.wizard'

    def wrapper_get_modes_facturacio(self, cursor, uid, context=None):
        return _modes_facturacio(self, cursor, uid, context=context)

GiscedataSwitchingModConWizard()
