# coding=utf-8
from osv import osv, fields


class WizardPartnerNoCessionSIPS(osv.osv_memory):
    _name = 'wizard.partner.no.cession.sips'
    _columns = {
        'numero': fields.integer('Contractes a sol·licitar', readonly=True),
        'partner_id': fields.many2one('res.partner', 'Titular', required=True)
    }

    def request(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        wiz.partner_id.request_no_cession_sips(context=context)
        return {'type': 'ir.actions.act_window_close'}

    def _default_numero(self, cursor, uid, context=None):
        if context is None:
            context = {}
        active_id = context.get('active_id')
        if active_id:
            polissa_obj = self.pool.get('giscedata.polissa')
            return polissa_obj.search_count(cursor, uid, [
                ('titular.id', '=', active_id),
                ('no_cessio_sips', '=', 'unactive')
            ])
        else:
            return 0

    def _default_partner_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('active_id', False)

    _defaults = {
        'numero': _default_numero,
        'partner_id': _default_partner_id
    }


WizardPartnerNoCessionSIPS()
