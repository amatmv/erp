# -*- coding: utf-8 -*-
{
    "name": "Switching Comer",
    "description": """Switching per Comercialitzadores
    Processos útils i exclusius per la gestió de switching (gestió ATR)
    per a empreses comercialitzadores""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_switching",
        "giscedata_lectures_comer",
        "giscedata_lectures_pool",
        "giscedata_facturacio_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_partner_no_cession_sips_view.xml",
        "giscedata_switching_data.xml",
        "giscedata_switching_view.xml",
        "giscedata_polissa_view.xml",
        "partner_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
