# -*- coding: utf-8 -*-

from destral.transaction import Transaction
from addons import get_module_resource
from giscedata_switching.tests import TestSwitchingImport
from datetime import datetime
from destral.patch import PatchNewCursors


class TestValidateMeasures(TestSwitchingImport):

    def prepare_a3_for_activation(self, cursor, uid, txn):
        a302_xml_path = get_module_resource('giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        with open(a302_xml_path, 'r') as f:
            a302_xml = f.read()

        self.switch(txn, 'comer')
        # create step 01
        contract_id = self.get_contract_id(txn)

        contract_obj = self.openerp.pool.get('giscedata.polissa')
        contract = contract_obj.browse(cursor, uid, contract_id)

        # Create payment mode for contract titular
        mode_obj = self.openerp.pool.get('payment.mode')
        journal_obj = self.openerp.pool.get('account.journal')
        payment_type_obj = self.openerp.pool.get('payment.type')
        bank_obj = self.openerp.pool.get('res.partner.bank')
        bank_id = bank_obj.search(cursor, uid, [('partner_id', '=', contract.pagador.id)])[0]
        contract.write({'bank': bank_id})
        type_id = payment_type_obj.search(cursor, uid, [])[0]
        mode_obj.create(cursor, uid, {
            'partner_id': uid,
            'require_bank_account': True,
            'tipo': 'sepa19',
            'sepa_creditor_code': "ES10000B22350466",
            'journal': journal_obj.search(cursor, uid, [])[0],
            'name': "test mode",
            'bank_id': bank_id,
            'type': type_id,
            'sufijo': 000,
        })

        # Create request link (field ref) for polissa
        rl_obj = self.openerp.pool.get('res.request.link')
        rl_obj.create(cursor, uid, {
            'priority': 30, 'object': u'giscedata.polissa',
            'name': u'Polissa'
        })

        # Create lot facturacio
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        lot_ids = lot_obj.search(cursor, uid, [])
        lot_obj.write(cursor, uid, [lot_ids[0]], {
            'data_inici': '2016-10-01',
            'data_final': '2016-10-31',
            'name': '10/2016'
        })

        # Create pricelist version for tarifa electricidad
        pricelist_ver_obj = self.openerp.pool.get('product.pricelist.version')
        pricelist_ids = pricelist_ver_obj.search(cursor, uid, [])
        if len(pricelist_ids) > 1:
            for pricelist_id in pricelist_ids[1:]:
                pricelist_ver_obj.unlink(cursor, uid, [pricelist_id])
        pricelist_ver_obj.write(
            cursor, uid, [pricelist_ids[0]], {
                'pricelist_id': contract.llista_preu.id,
                'date_start': '2016-08-01',
                'date_end': False
        })

        # Write tarifa 2.0 DHA to contract
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        tarifa_ids = tarifa_obj.search(cursor, uid,
                                       [('name', '=', '2.0DHA')])
        contract.write({'tarifa': tarifa_ids[0]})

        step_id = self.create_case_and_step(
            cursor, uid, contract_id, 'A3', '01'
        )
        step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        a3 = step_obj.browse(cursor, uid, step_id)
        a3 = sw_obj.browse(cursor, uid, a3.sw_id.id)
        codi_sollicitud = a3.codi_sollicitud

        # change the codi sol.licitud of a302.xml
        a302_xml = a302_xml.replace(
            "<CodigoDeSolicitud>201412111009",
            "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
        )
        # import step 02
        sw_obj.importar_xml(cursor, uid, a302_xml, 'a302_new.xml')
        return codi_sollicitud, contract_id

    def test_load_a3_05_valid_measures_corrrect(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')

            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])

            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == "0000539522"][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 4)
            self.assertEqual(len(meter.pool_lectures_pot), 1)
            # Check we have loaded all correct periods
            lectures = meter.pool_lectures
            la1, la2, lr1, lr2 = None, None, None, None
            for l in lectures:
                if l.periode.name == 'P1':
                    if l.tipus == 'A':
                        la1 = l
                    else:
                        lr1 = l
                elif l.periode.name == 'P2':
                    if l.tipus == 'A':
                        la2 = l
                    else:
                        lr2 = l
            self.assertTrue(la1)
            self.assertTrue(la2)
            self.assertTrue(lr1)
            self.assertTrue(lr2)
            self.assertEqual(la1.name, "2016-08-20")
            self.assertEqual(la2.name, "2016-08-20")
            self.assertEqual(lr1.name, "2016-08-20")
            self.assertEqual(lr2.name, "2016-08-20")
            self.assertEqual(la1.lectura, 100)
            self.assertEqual(la2.lectura, 200)
            self.assertEqual(lr1.lectura, 10)
            self.assertEqual(lr2.lectura, 20)
            lpot = meter.pool_lectures_pot[0]
            self.assertEqual(lpot.name, "2016-08-20")
            self.assertEqual(lpot.lectura, 40)

    def test_load_a3_05_valid_measures_corrrect_with_p0(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305_P0.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')

            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)

            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])

            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == "0000539522"][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 4)
            self.assertEqual(len(meter.pool_lectures_pot), 1)
            # Check we have loaded all correct periods
            lectures = meter.pool_lectures
            la1, la2, lr1, lr2 = None, None, None, None
            for l in lectures:
                if l.periode.name == 'P1':
                    if l.tipus == 'A':
                        la1 = l
                    else:
                        lr1 = l
                elif l.periode.name == 'P2':
                    if l.tipus == 'A':
                        la2 = l
                    else:
                        lr2 = l
            self.assertTrue(la1)
            self.assertTrue(la2)
            self.assertTrue(lr1)
            self.assertTrue(lr2)
            self.assertEqual(la1.name, "2016-08-20")
            self.assertEqual(la2.name, "2016-08-20")
            self.assertEqual(lr1.name, "2016-08-20")
            self.assertEqual(lr2.name, "2016-08-20")
            self.assertEqual(la1.lectura, 100)
            self.assertEqual(la2.lectura, 200)
            self.assertEqual(lr1.lectura, 10)
            self.assertEqual(lr2.lectura, 20)
            lpot = meter.pool_lectures_pot[0]
            self.assertEqual(lpot.name, "2016-08-20")
            self.assertEqual(lpot.lectura, 40)

    def test_load_a3_05_valid_measures_incorrrect_check_dates(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change lectures date to make them incorrect
            a305_xml = a305_xml.replace(
                "<FechaLecturaFirme>2016-08-20</FechaLecturaFirme>",
                "<FechaLecturaFirme>2016-08-19</FechaLecturaFirme>"
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')

            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)

            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])

            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == "0000539522"][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 0)
            self.assertEqual(len(meter.pool_lectures_pot), 0)

    def test_load_a3_05_valid_measures_incorrrect_check_origin(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change lectures origin to make them incorrect
            a305_xml = a305_xml.replace(
                "<Procedencia>20</Procedencia>",
                "<Procedencia>40</Procedencia>"
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')

            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])

            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == "0000539522"][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 0)
            self.assertEqual(len(meter.pool_lectures_pot), 0)

    def test_load_a3_05_valid_measures_incorrrect_check_periodes(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change lectures period to make them incorrect
            a305_xml = a305_xml.replace(
                "<Periodo>22</Periodo>",
                "<Periodo>10</Periodo>"
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])

            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == "0000539522"][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 0)
            self.assertEqual(len(meter.pool_lectures_pot), 0)

    def test_load_a3_05_valid_measures_ignore_distri(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)

            # Change config var to ignore the distri
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'sw_distris_ignore_load_measures')]
            )[0]

            conf_obj.write(cursor, uid, var_id, {'value': "['1234']"})
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])

            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == "0000539522"][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 0)
            self.assertEqual(len(meter.pool_lectures_pot), 0)

    def test_load_m1_05_valid_measures_corrrect(self):
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        tarif_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            val_ids = val_obj.search(cursor, uid, [],context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True})

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            # Prepare contract
            vals = {
                'data_alta': '2017-11-17',
                'data_baixa': False,
                'potencia': 8.050,
                'facturacio_distri': 1,
                'data_ultima_lectura': False,
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '00',
                'contract_type': '01',
                'lot_facturacio': False
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                for l in comptador.pool_lectures:
                    l.unlink(context={})
                for lp in comptador.pool_lectures_pot:
                    lp.unlink(context={})

            comptador = contract.comptadors[0]

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            dalta = datetime.strptime(contract.data_alta, "%Y-%m-%d")
            vals = {
                'name': dalta,
                'periode': periode_id,
                'lectura': 7000,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            # Prepare tarifa comer for 2.0DHA
            tarif_20dha_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            tarif = tarif_obj.browse(cursor, uid, tarif_20dha_id)
            tarif.llistes_preus_comptatibles[0].write({
                'type': 'sale'
            })

            # Prepare M1 to be activated
            self.switch(txn, 'comer')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )
            m1 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m1.sw_id.id)
            codi_sollicitud = m1.codi_sollicitud

            # Prepare step 02
            m102_xml_path = get_module_resource('giscedata_switching', 'tests', 'fixtures', 'm102_new.xml')
            with open(m102_xml_path, 'r') as f:
                m102_xml = f.read()
            m102_xml = m102_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(cursor, uid, m102_xml, 'm102_new.xml')

            # Prepare step 05
            m105_xml_path = get_module_resource('giscedata_switching_comer', 'tests', 'fixtures', 'm105_tarif_change.xml')
            with open(m105_xml_path, 'r') as f:
                m105_xml = f.read()
            m105_xml = m105_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            m105_xml = m105_xml.replace(
                "<NumeroSerie>0000926781</NumeroSerie>",
                "<NumeroSerie>{0}</NumeroSerie>".format(contract.comptador)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, m105_xml, 'm105.xml')

            # Activate M105
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])
            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == contract.comptador][0]
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 3)
            self.assertEqual(len(meter.pool_lectures_pot), 0)
            # Check we have loaded all correct periods
            la10, la21, la22 = None, None, None
            for l in meter.pool_lectures:
                if l.periode.tarifa.name == '2.0DHA' and l.periode.name == 'P1':
                    la21 = l
                elif l.periode.tarifa.name == '2.0DHA' and l.periode.name == 'P2':
                    la22 = l
                elif l.periode.tarifa.name == '2.0A':
                    la10 = l
            self.assertTrue(la21)
            self.assertTrue(la22)
            self.assertTrue(la10)
            self.assertEqual(la21.lectura, 4380.00)
            self.assertEqual(la22.lectura, 3064.00)
            self.assertEqual(la10.lectura, 7444.00)
            self.assertEqual(la21.name, '2017-12-29')
            self.assertEqual(la22.name, '2017-12-29')
            self.assertEqual(la10.name, '2017-12-29')

    def test_load_m1_05_incorrect_check_giro(self):
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        lectura_pol_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
        step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        tarif_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            val_ids = val_obj.search(cursor, uid, [],context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True})

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            # Prepare contract
            vals = {
                'data_alta': '2017-11-17',
                'data_baixa': False,
                'potencia': 8.050,
                'facturacio_distri': 1,
                'data_ultima_lectura': False,
                'facturacio': 1,
                'facturacio_potencia': 'icp',
                'tg': '1',
                'agree_tensio': 'E0',
                'agree_dh': 'E1',
                'agree_tipus': '05',
                'agree_tarifa': '2A',
                'autoconsumo': '00',
                'contract_type': '01',
                'lot_facturacio': False
            }
            contract_obj.write(cursor, uid, contract_id, vals)
            contract_obj.send_signal(cursor, uid, [contract_id], [
                'validar', 'contracte'
            ])

            contract = contract_obj.browse(cursor, uid, contract_id)

            # Delete all lectures from meter
            for comptador in contract.comptadors:
                for l in comptador.lectures:
                    l.unlink(context={})
                for lp in comptador.lectures_pot:
                    lp.unlink(context={})
                for l in comptador.pool_lectures:
                    l.unlink(context={})
                for lp in comptador.pool_lectures_pot:
                    lp.unlink(context={})

            comptador = contract.comptadors[0]

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            periode2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHA_new'
            )[1]
            comptador_id = comptador.id

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            dalta = datetime.strptime(contract.data_alta, "%Y-%m-%d")
            vals = {
                'name': dalta,
                'periode': periode_id,
                'lectura': 7000,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }
            lectura_obj.create(cursor, uid, vals)

            # Creem una lectura amb un valor superior a les que haurien de
            # entrar. Saltará la validació del giro i no entraran
            vals.update({
                'name': dalta,
                'lectura': 9000,
                'periode': periode2_id
            })
            lectura_pol_obj.create(cursor, uid, vals)

            # Prepare tarifa comer for 2.0DHA
            tarif_20dha_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            tarif = tarif_obj.browse(cursor, uid, tarif_20dha_id)
            tarif.llistes_preus_comptatibles[0].write({
                'type': 'sale'
            })

            # Prepare M1 to be activated
            self.switch(txn, 'comer')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )
            m1 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m1.sw_id.id)
            codi_sollicitud = m1.codi_sollicitud

            # Prepare step 02
            m102_xml_path = get_module_resource('giscedata_switching', 'tests', 'fixtures', 'm102_new.xml')
            with open(m102_xml_path, 'r') as f:
                m102_xml = f.read()
            m102_xml = m102_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(cursor, uid, m102_xml, 'm102_new.xml')

            # Prepare step 05
            m105_xml_path = get_module_resource('giscedata_switching_comer', 'tests', 'fixtures', 'm105_tarif_change.xml')
            with open(m105_xml_path, 'r') as f:
                m105_xml = f.read()
            m105_xml = m105_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            m105_xml = m105_xml.replace(
                "<NumeroSerie>0000926781</NumeroSerie>",
                "<NumeroSerie>{0}</NumeroSerie>".format(contract.comptador)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, m105_xml, 'm105.xml')

            # Activate M105
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])
            self.assertEqual(result[0], "OK")
            # Check that lectures have been loaded
            contract = contract_obj.browse(cursor, uid, contract_id)
            meter = [m for m in contract.comptadors if m.name == contract.comptador][0]
            self.assertTrue(meter.active)
            # només ha d'haver-hi les dos lectures que hem creat al inici del test
            self.assertEqual(len(meter.pool_lectures), 2)
            self.assertEqual(len(meter.pool_lectures_pot), 0)
