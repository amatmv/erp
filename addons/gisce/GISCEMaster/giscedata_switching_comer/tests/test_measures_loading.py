# -*- coding: utf-8 -*-
import unittest

from destral import testing
from destral.transaction import Transaction
from expects import expect, raise_error
from osv.orm import except_orm
from addons import get_module_resource
import time
from tools.misc import cache


class TestMeasuresLoading(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_measures_loading_all(self):
        """Aquest test comprova que en qualsevol cas ATR es carreguen les
        lectures al comptador"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        sw_obj = pool.get('giscedata.switching.pm')
        res_config = pool.get('res.config')

        # Especificar valor '' a res_config

        res_config.set(cursor, uid, 'sw_load_measures_steps', '')

        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'A3', '01'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'C1', '05'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'R1', '01'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'W1', '01'),
            True
        )

    def test_measures_not_loading_any(self):
        """Aquest test comprova que en cap cas ATR es carreguin les lectures
        al comptador"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        sw_obj = pool.get('giscedata.switching.pm')
        res_config = pool.get('res.config')

        # Especificar valor '' a res_config

        res_config.set(cursor, uid, 'sw_load_measures_steps', '[]')

        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'A3', '01'),
            False
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'C1', '05'),
            False
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'R1', '01'),
            False
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'W1', '01'),
            False
        )

    def test_measures_loading_A3_only(self):
        """Aquest test comprova que només es carreguen lectures
        en el cas de que el codi del procés sigui A3 i el pas sigui 01"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        sw_obj = pool.get('giscedata.switching.pm')
        res_config = pool.get('res.config')

        # Especificar valor '' a res_config

        res_config.set(cursor, uid, 'sw_load_measures_steps', "['A3-01']")

        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'A3', '01'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'C1', '05'),
            False
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'R1', '01'),
            False
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'W1', '01'),
            False
        )

    def test_measures_loading_multiple_processes(self):
        """Aquest test comprova que només es carreguen lectures
        en el cas de que el codi del procés sigui A3 i el pas sigui 01,
        procés C1 i pas 01 o procés R1 i pas 01"""
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        sw_obj = pool.get('giscedata.switching.pm')
        res_config = pool.get('res.config')

        # Especificar valor '' a res_config

        res_config.set(
            cursor, uid, 'sw_load_measures_steps',
            "['A3-01', 'C1-05', 'R1-01']"
        )

        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'A3', '01'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'C1', '05'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'R1', '01'),
            True
        )
        self.assertEqual(
            sw_obj.can_load_measures(cursor, uid, 'W1', '01'),
            False
        )
