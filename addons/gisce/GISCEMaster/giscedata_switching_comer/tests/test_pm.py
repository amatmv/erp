# -*- coding: utf-8 -*-
import unittest

from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from datetime import datetime
from giscedata_switching.tests.common_tests import TestSwitchingImport


class TestPM(TestSwitchingImport):

    def setUp(self):
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def test_activa_pm_a_polissa(self):
        m1_02_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'm102.xml')
        m1_05_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'm105.xml')

        uid = self.txn.user
        cursor = self.txn.cursor
        pool = self.openerp.pool
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = pool.get("giscedata.polissa")
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        meter_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'comptador_0001'
        )[1]
        meter = meter_obj.browse(cursor, uid, meter_id)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        m101_obj = self.openerp.pool.get('giscedata.switching.m1.01')

        self.switch(self.txn, 'comer')
        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', "polissa_0001"
        )[1]
        polissa_obj.write(cursor, uid, contract_id, {'data_baixa': '02-01-2018'})
        self.activar_polissa_CUPS(self.txn)
        self.crear_modcon(self.txn, 8, '2017-01-01', '2018-01-01')

        # Creates step M1-01
        step_id = self.create_case_and_step(cursor, uid, contract_id, 'M1', '01')
        m101 = m101_obj.browse(cursor, uid, step_id)
        sw_obj.write(
            cursor, uid, m101.sw_id.id, {'codi_sollicitud': '201704120009'}
        )
        # El creem ara perque la data sigui posterior a la posada al m101
        with open(m1_02_xml_path, 'r') as f:
            m1_02_xml = f.read()
            data_new = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            m1_02_xml = m1_02_xml.replace("2014-04-16T23:13:37", data_new)
        # M1-02 step loading
        sw_obj.importar_xml(cursor, uid, m1_02_xml, 'm102.xml')

        # El creem ara perque la data sigui posterior a la posada al r102
        with open(m1_05_xml_path, 'r') as f:
            m1_05_xml = f.read()
            data_nw = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
            m1_05_xml = m1_05_xml.replace("2017-04-12T11:30:07", data_nw)
            m1_05_xml = m1_05_xml.replace("B63011077", meter.name)
        # M1-05 step loading
        sw_obj.importar_xml(cursor, uid, m1_05_xml, 'm105.xml')

        res = sw_obj.search(cursor, uid, [
            ('proces_id.name', '=', 'M1'),
            ('step_id.name', '=', '05'),
            ('codi_sollicitud', '=', '201704120009')
        ])
        m1 = sw_obj.browse(cursor, uid, res[0])
        m105 = sw_obj.get_pas(cursor, uid, m1)

        pm_obj = pool.get("giscedata.switching.pm")
        pm_obj.activa_pm_a_polissa(cursor, uid, [x.id for x in m105.pm_ids])
        polissa = polissa_obj.browse(cursor, uid, contract_id)
        new_meter = polissa.comptadors[0]
        self.assertEqual(new_meter.name, meter.name)
        self.assertEqual(new_meter.data_baixa, '2017-01-02')
        self.assertFalse(new_meter.active)
