# -*- coding: utf-8 -*-
from __future__ import absolute_import
from destral.transaction import Transaction
from giscedata_switching.tests.common_tests import TestSwitchingImport
from datetime import datetime, timedelta
from destral.patch import PatchNewCursors


class TestSwitchingWizards(TestSwitchingImport):

    def setUp(self):
        self.openerp.install_module('giscedata_facturacio_switching')
        return super(TestSwitchingWizards, self).setUp()

    def test_wizard_mod_con_M1_owner_change_L(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])
            contract.write({'data_ultima_lectura': '2017-01-01'})

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            contract_obj.write(cursor, uid, new_contract_id, {
                'cups': contract.cups.id,
                'data_baixa': False
            })
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'L',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id
            })
            wiz.genera_casos_atr()
            contract = contract_obj.browse(cursor, uid, contract_id)
            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'L')
            self.assertEqual(sw.get_pas().data_accio, '2017-01-02')
            self.assertFalse(contract.facturacio_suspesa)

    def test_wizard_mod_con_M1_owner_change_A(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.wkf_activa()
            contract.write({'data_ultima_lectura': '2017-01-01', 'lot_facturacio': lot_id})

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            contract_obj.write(cursor, uid, new_contract_id,
                               {'cups': contract.cups.id})
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'A',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id
            })
            wiz.genera_casos_atr()
            contract = contract_obj.browse(cursor, uid, contract_id)
            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertEqual(sw.get_pas().data_accio, '2017-01-01')
            self.assertTrue(contract.lot_facturacio)
            self.assertTrue(contract.facturacio_suspesa)

    def test_wizard_mod_con_M1_owner_change_A_lects(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.wkf_activa()
            contract.write({'data_ultima_lectura': '2017-01-01', 'lot_facturacio': lot_id})

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            contract_obj.write(cursor, uid, new_contract_id,
                               {'cups': contract.cups.id})
            contract_obj.generar_periodes_potencia(cursor, uid,
                                                   [new_contract_id])

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'A',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id, 'measure_date': '2017-05-04',
                'check_introduir_lectures': True, 'measure_ae_p1': 9000,
                'measure_pm_p1': 9000, 'send_pm_measures': True
            })
            wiz.genera_casos_atr()
            contract = contract_obj.browse(cursor, uid, contract_id)
            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertEqual(sw.get_pas().data_accio, '2017-05-04')
            self.assertTrue(contract.lot_facturacio)
            self.assertTrue(contract.facturacio_suspesa)

    def test_wizard_mod_con_M1_atr_change(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.wkf_activa()
            contract.write({'data_ultima_lectura': '2017-01-01'})

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({'change_adm': False, 'activacio_cicle': 'A', 'change_retail_tariff': False, 'retail_tariff': False, 'skip_validations': True})
            wiz.genera_casos_atr()
            contract = contract_obj.browse(cursor, uid, contract_id)
            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertTrue(sw.ref)
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertFalse(sw.actualitzar_tarifa_comer)
            self.assertFalse(contract.facturacio_suspesa)

    def test_wizard_mod_con_M1_atr_change_retail_tarif(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            imd_obj = self.openerp.pool.get('ir.model.data')
            tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
            list_obj = self.openerp.pool.get('product.pricelist')
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            llista_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad_pre_iet_843_2012'
            )[1]
            lids = [l.id for l in contract.tarifa.llistes_preus_comptatibles]
            lids = lids+[llista_id]
            list_obj.write(cursor, uid, lids, {'type': 'sale'})
            contract.tarifa.write({
                'llistes_preus_comptatibles': [(6, 0, lids+[llista_id])]
            })
            contract.write({
                'data_ultima_lectura': '2017-01-01',
            })
            contract.wkf_activa()

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({'change_adm': False, 'activacio_cicle': 'A',
                       'change_retail_tariff': True, 'retail_tariff': llista_id,
                       'skip_validations': True})
            wiz.genera_casos_atr()

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertTrue(sw.ref)
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertEqual(sw.tarifa_comer_id.id, llista_id)

            # Check escull tarifa returns atr tarif when
            # actualitzar_tarifa_comer is True
            self.assertTrue(sw.actualitzar_tarifa_comer)
            tarif_lb = sw.get_pas().marca_medida_bt == 'S'
            tarifa_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid,
                                                         sw.get_pas().tarifaATR,
                                                         tarifa_lb=tarif_lb)
            tarif = sw.escull_tarifa_comer(tarifa_id)
            self.assertEqual(tarif, sw.tarifa_comer_id.id)
            # Check escull tarifa returns contract tarif if
            # actualitzar_tarifa_comer is False
            sw.write({'actualitzar_tarifa_comer': False})
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertFalse(sw.actualitzar_tarifa_comer)
            tarif_lb = sw.get_pas().marca_medida_bt == 'S'
            tarifa_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid,
                                                         sw.get_pas().tarifaATR,
                                                         tarifa_lb=tarif_lb)
            tarif = sw.escull_tarifa_comer(tarifa_id)
            self.assertEqual(tarif, contract.llista_preu.id)

    def test_wizard_mod_con_M1_adm_create_contract(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            # Cambiem el valor de la potencia del primer periode perque sigui
            # diferent a la potencia contractada. Al crera el nou contracte
            # hauria de mantenir aquest valor en comptes de utilitzar el de la
            # potencia contractada.
            contract.potencies_periode[0].write({"potencia": 0.29})
            new_contract_id = self.get_contract_id(txn, xml_id="polissa_0005")
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            imd_obj = self.openerp.pool.get('ir.model.data')
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]
            new_contract.write({
                'cups': contract.cups.id,
                'tarifa': 2,
                'facturacio_potencia': 'icp',
                'titular': other_id,
                'pagador': another_id
            })

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            # We have a new contract with the same cups and we are in an owner
            # change so it should use info of new contrtact
            wiz.write({'change_adm': True, 'change_atr': False})
            # We simulate the onchange_type
            vals = wiz.onchange_type(
                False, True, wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False, "M1"
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.proces, 'M1')
            self.assertEqual(wiz.tariff, new_contract.tarifa.codi_ocsum)
            self.assertEqual(wiz.owner, new_contract.pagador)
            self.assertEqual(wiz.owner_pre, new_contract.pagador)

            self.assertEqual(wiz.vat, new_contract.pagador.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, new_contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, new_contract.direccio_pagament.id)
            self.assertEqual(wiz.generate_new_contract, "exists")

            # We change "generate_new_contract" to create, the values should
            # change to the old contract ones
            wiz.write({"generate_new_contract": "create"})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            vals = wiz.onchange_new_contact(
                wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False, "M1"
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.tariff, contract.tarifa.codi_ocsum)
            self.assertEqual(wiz.owner, contract.pagador)
            self.assertEqual(wiz.owner_pre, contract.pagador)
            self.assertEqual(wiz.vat, contract.pagador.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, contract.direccio_pagament.id)

            # Now we create the atr case and this should generate a new
            # contract with the new values we have written
            wiz.write({
                'owner': other_id,
                'pagador': another_id
            })
            wiz.genera_casos_atr()
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.state, 'end')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('ref', 'like', 'giscedata.polissa')
            ])
            self.assertEqual(len(res), 1)

            # Check the vals of the new contract
            sw = sw_obj.browse(cursor, uid, res[0])
            pol_obj = self.openerp.pool.get("giscedata.polissa")
            pol = pol_obj.browse(cursor, uid, int(sw.ref.split(",")[1]))
            self.assertEqual(pol.titular, wiz.owner)
            self.assertEqual(pol.cnae, wiz.cnae)
            self.assertEqual(pol.facturacio_potencia, 'icp' if wiz.power_invoicing == '1' else 'max')
            self.assertEqual(pol.pagador, wiz.pagador)
            self.assertEqual(pol.pagador_sel, 'altre_p')
            self.assertEqual(pol.direccio_pagament, wiz.direccio_pagament)
            self.assertEqual(pol.notificacio, 'titular')
            self.assertEqual(pol.direccio_notificacio, wiz.direccio_notificacio)
            self.assertEqual(pol.bank.id, vals['value']['bank'])
            self.assertEqual(pol.tipo_pago, wiz.tipo_pago)
            self.assertEqual(pol.payment_mode_id, wiz.payment_mode_id)
            self.assertEqual(
                datetime.strptime(pol.data_firma_contracte, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d'),
                datetime.today().strftime("%Y-%m-%d")
            )
            self.assertEqual(pol.potencies_periode[0].potencia, 0.29)
            self.assertFalse(pol.facturacio_suspesa)
            self.assertEqual(pol.ref_dist, contract.ref_dist)


class TestOwnerChangeActivation(TestSwitchingImport):

    def setUp(self):
        self.openerp.install_module('giscedata_facturacio_switching')
        return super(TestOwnerChangeActivation, self).setUp()

    def test_activation_owner_change_L(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])
            contract.write({'data_ultima_lectura': '2017-01-01'})

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            contract_obj.write(cursor, uid, new_contract_id, {
                'cups': contract.cups.id,
                'data_baixa': False
            })
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'L',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id
            })
            wiz.genera_casos_atr()

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'L')
            self.assertEqual(sw.get_pas().data_accio, '2017-01-02')

            # Create step 2 and test activation
            step_id = self.create_step(cursor, uid, sw, 'M1', '02')
            self.assertTrue(step_id)
            # Activate changes
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            contract_obj.write(cursor, uid, contract_id, {'data_ultima_lectura': '2017-02-01'})
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            # Check everything is OK
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.state, 'done')
            self.assertEqual(old_contract.data_ultima_lectura, '2017-02-01')
            self.assertEqual(old_contract.data_baixa, old_contract.data_ultima_lectura)
            self.assertEqual(old_contract.state, 'baixa')
            self.assertFalse(old_contract.lot_facturacio)
            self.assertFalse(old_contract.facturacio_suspesa)
            self.assertEqual(new_contract.data_alta, '2017-02-02')
            self.assertEqual(new_contract.state, 'activa')
            self.assertEqual(new_contract.lot_facturacio.id, lot_id)

    def test_activation_owner_change_A(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
            lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
            lectpot_pool_obj = self.openerp.pool.get('giscedata.lectures.potencia.pool')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)

            compt2_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_lectures', 'comptador_0004')[1]
            compt_obj.write(cursor, uid, compt2_id, {'polissa': contract_id, 'data_baixa': False, 'active': True})

            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])
            contract.write({'data_ultima_lectura': '2017-01-01'})

            for meter in contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.write({'data_alta': contract.data_alta})

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2016-12-30', 'lectura': 0,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-01', 'lectura': 1000,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-04', 'lectura': 2000,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-06', 'lectura': 3000,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lectpot_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-06', 'lectura': 500,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )

            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-06', 'lectura': 100,
                    'comptador': contract.comptadors[1].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            new_contract.write({'cups': contract.cups.id, 'data_baixa': False})
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])
            for meter in new_contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.unlink(context={})

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'A',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id
            })
            wiz.genera_casos_atr()

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertEqual(sw.get_pas().data_accio, '2017-01-01')
            self.assertTrue(old_contract.lot_facturacio)
            self.assertTrue(old_contract.facturacio_suspesa)

            # Create step 2 and test activation
            step_id = self.create_step(cursor, uid, sw, 'M1', '02')
            self.assertTrue(step_id)
            # Activate changes
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            # Check everything is OK
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.state, 'done')
            self.assertEqual(old_contract.data_ultima_lectura, '2017-01-01')
            self.assertEqual(old_contract.data_baixa, old_contract.data_ultima_lectura)
            self.assertEqual(old_contract.state, 'baixa')
            self.assertTrue(old_contract.lot_facturacio)
            self.assertFalse(old_contract.facturacio_suspesa)
            self.assertEqual(new_contract.data_alta, '2017-01-02')
            self.assertEqual(new_contract.state, 'activa')
            self.assertEqual(new_contract.lot_facturacio.id, lot_id)
            # Check old meters
            self.assertEqual(len(old_contract.comptadors), 2)
            meter = old_contract.comptadors[0]
            for meter in old_contract.comptadors:
                if meter.name == 'B63011077':
                    break
            self.assertEqual(meter.name, 'B63011077')
            self.assertEqual(meter.data_baixa, '2017-01-01')
            self.assertFalse(meter.active)
            self.assertEqual(len(meter.pool_lectures), 2)
            self.assertEqual(meter.pool_lectures[1].name, '2016-12-30')
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-01')
            self.assertEqual(len(meter.pool_lectures_pot), 0)
            # Check new meters
            self.assertEqual(len(new_contract.comptadors), 2)
            for meter in new_contract.comptadors:
                if meter.name == 'B63011077':
                    break
            self.assertEqual(meter.name, 'B63011077')
            self.assertEqual(meter.data_alta, '2017-01-02')
            self.assertFalse(meter.data_baixa)
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 3)
            # By now initial date is the same day than activation, will change
            # to the day before activation
            self.assertEqual(meter.pool_lectures[2].name, '2017-01-01')
            self.assertEqual(meter.pool_lectures[1].name, '2017-01-04')
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-06')
            self.assertEqual(meter.pool_lectures[0].lectura, 3000)
            self.assertEqual(len(meter.pool_lectures_pot), 1)
            for meter in new_contract.comptadors:
                if meter.name == '42553686':
                    break
            self.assertEqual(meter.name, '42553686')
            self.assertEqual(meter.data_alta, '2017-01-02')
            self.assertFalse(meter.data_baixa)
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 1)
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-06')
            self.assertEqual(meter.pool_lectures[0].lectura, 100)
            self.assertEqual(len(meter.pool_lectures_pot), 0)

    def test_activation_owner_change_A_lects(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
            lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
            lectpot_pool_obj = self.openerp.pool.get('giscedata.lectures.potencia.pool')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)

            compt2_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_lectures', 'comptador_0004')[1]
            compt_obj.write(cursor, uid, compt2_id, {'polissa': contract_id, 'data_baixa': False, 'active': True})

            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])
            contract.write({'data_ultima_lectura': '2017-01-01'})

            primer = True
            for meter in contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.write({'data_alta': contract.data_alta})
                if not primer:
                    meter.write({
                        'data_baixa': contract.data_alta, 'active': False
                    })
                    meter.unlink()
                primer = False

            mname = contract.comptadors[0].name

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2016-12-30', 'lectura': 0,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-01', 'lectura': 1000,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            new_contract.write({'cups': contract.cups.id, 'data_baixa': False})
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])
            for meter in new_contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.unlink(context={})

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'A',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id,
                # Fill values to create lects on day 2017-01-04.
                # The activation will be done the day after
                'measure_date': '2017-01-04',
                'check_introduir_lectures': True,
                'measure_ae_p1': 2000,
                'measure_pm_p1': 20,
                'send_pm_measures': True,
            })
            wiz.genera_casos_atr()

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertEqual(sw.get_pas().data_accio, '2017-01-04')
            self.assertTrue(old_contract.lot_facturacio)
            self.assertTrue(old_contract.facturacio_suspesa)
            # The measure must be loaded to the pool
            self.assertEqual(old_contract.comptadors[0].pool_lectures[0].name, '2017-01-04')
            self.assertEqual(old_contract.comptadors[0].pool_lectures[0].lectura, 2000)

            # Create step 2 and test activation
            step_id = self.create_step(cursor, uid, sw, 'M1', '02')
            self.assertTrue(step_id)
            # Activate changes
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            # Check everything is OK
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.state, 'done')
            self.assertEqual(old_contract.data_ultima_lectura, '2017-01-01')
            self.assertEqual(old_contract.data_baixa, '2017-01-04')
            self.assertEqual(old_contract.state, 'baixa')
            self.assertTrue(old_contract.lot_facturacio)
            self.assertFalse(old_contract.facturacio_suspesa)
            self.assertEqual(new_contract.data_alta, '2017-01-05')
            self.assertEqual(new_contract.state, 'activa')
            self.assertEqual(new_contract.lot_facturacio.id, lot_id)
            # Check old meters
            self.assertEqual(len(old_contract.comptadors), 1)
            meter = old_contract.comptadors[0]
            for meter in old_contract.comptadors:
                if meter.name == mname:
                    break
            self.assertEqual(meter.name, mname)
            self.assertEqual(meter.data_baixa, '2017-01-04')
            self.assertFalse(meter.active)
            self.assertEqual(len(meter.pool_lectures), 3)
            self.assertEqual(meter.pool_lectures[2].name, '2016-12-30')
            self.assertEqual(meter.pool_lectures[1].name, '2017-01-01')
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-04')
            self.assertEqual(len(meter.pool_lectures_pot), 1)
            # Check new meters
            self.assertEqual(len(new_contract.comptadors), 1)
            for meter in new_contract.comptadors:
                if meter.name == mname:
                    break
            self.assertEqual(meter.name, mname)
            self.assertEqual(meter.data_alta, '2017-01-05')
            self.assertFalse(meter.data_baixa)
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 1)
            # By now initial date is the same day than activation, it'll change
            # to the day before activation
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-04')
            self.assertEqual(meter.pool_lectures[0].lectura, 2000)
            self.assertEqual(len(meter.pool_lectures_pot), 1)

    def test_owner_change_A_incorrect_lects(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
            lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
            lectpot_pool_obj = self.openerp.pool.get('giscedata.lectures.potencia.pool')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)

            compt2_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_lectures', 'comptador_0004')[1]
            compt_obj.write(cursor, uid, compt2_id, {'polissa': contract_id, 'data_baixa': False, 'active': True})

            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])
            contract.write({'data_ultima_lectura': '2017-01-01'})

            for meter in contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.write({'data_alta': contract.data_alta})
            contract.comptadors[-1].write({
                'data_baixa': contract.data_alta, 'active': False
            })
            mname = contract.comptadors[0].name

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2016-12-30', 'lectura': 0,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-01', 'lectura': 2500,
                    'comptador': contract.comptadors[0].id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0002')
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            new_contract.write({'cups': contract.cups.id, 'data_baixa': False})
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])
            for meter in new_contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.unlink(context={})

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'A',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id,
                # Fill values to create lects on day 2017-01-04.
                # The activation will be done the day after
                'measure_date': '2017-01-04',
                'check_introduir_lectures': True,
                'measure_ae_p1': 2000,
                'measure_pm_p1': 20,
                'send_pm_measures': True,
            })
            res = wiz.genera_casos_atr()

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 0)

    def test_activation_owner_change_subrogacy_NO_new_contract(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_subrogacio_new_contract')])
            conf_obj.write(cursor, uid, config_id, {'value': '0'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            partner_obj = self.openerp.pool.get("res.partner")
            imd_obj = self.openerp.pool.get('ir.model.data')
            bank_obj = self.openerp.pool.get("res.partner.bank")
            payment_type_obj = self.openerp.pool.get("payment.type")
            group_payment_obj = self.openerp.pool.get("payment.mode")

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])

            # Busquem un partner que no sigui l'actual, sera el nou titular
            new_titular_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_11'
            )[1]
            new_titular = partner_obj.browse(cursor, uid, new_titular_id)
            new_titular.write({"vat": "ES00000000T"})
            new_titular = partner_obj.browse(cursor, uid, new_titular_id)
            # Una direccio de un partner diferent al nou titular
            new_pagador = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_15'
            )[1]
            new_direccio_pagament = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_14'
            )[1]
            new_direccio_notificacio = new_direccio_pagament
            # Obtenir un nou compte bancari i tipus de pagament
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]
            new_bank_id = imd_obj.get_object_reference(
                cursor, uid, 'base_iban', 'res_partner_bank_iban_0001'
            )[1]
            bank_obj.write(cursor, uid, new_bank_id, {'partner_id': new_pagador})
            new_type = payment_type_obj.create(cursor, uid, {"code": "SWDEMO", "name": "SW DEMO"})
            new_group = group_payment_obj.create(cursor, uid, {
                'name': 'Mode of Payment',
                'bank_id': new_bank_id,
                'journal': journal_id,
                'type': new_type,
                "code": "AA",
                'require_bank_account': False
            })

            # Ara que ja tenim totes les dades del nou titular, podem crear el cas
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'L',
                'change_retail_tariff': False, 'retail_tariff': False,
                'owner_change_type': 'S',
                'direccio_pagament': new_direccio_pagament,
                'direccio_notificacio': new_direccio_notificacio,
                'bank': new_bank_id, 'payment_mode_id': new_group,
                'tipo_pago': new_type, 'owner': new_titular_id
            })
            wiz.genera_casos_atr(context={'pol_id': contract_id})
            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )
            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            pas01 = sw.get_pas()
            # Create step 2 and test activation
            step_id = self.create_step(cursor, uid, sw, 'M1', '02')
            self.assertTrue(step_id)
            # Activate changes
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            sw.get_pas().write({"data_activacio": "2016-05-01"})
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            # Check everything is OK
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.state, 'done')
            self.assertEqual(old_contract.titular.vat, "ES"+pas01.codi_document)
            self.assertEqual(old_contract.direccio_pagament.id, pas01.direccio_pagament.id)
            self.assertEqual(old_contract.direccio_notificacio.id, pas01.direccio_notificacio.id)
            self.assertEqual(old_contract.bank.id, pas01.bank.id)
            self.assertEqual(old_contract.tipo_pago.id, pas01.tipo_pago.id)
            actdate = sw.get_pas().data_activacio
            actdate = datetime.strptime(actdate, "%Y-%m-%d") + timedelta(days=1)
            actdate = actdate.strftime("%Y-%m-%d")
            self.assertEqual(old_contract.modcontractual_activa.data_inici, actdate)

    def test_activation_tarpot_and_owner(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
            lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
            lectpot_pool_obj = self.openerp.pool.get('giscedata.lectures.potencia.pool')
            lot_obj = self.openerp.pool.get("giscedata.facturacio.lot")
            lot_ids = lot_obj.search(cursor, uid, [('state', '=', 'open'), ('data_final', '<=', '2017-02-01')])
            lot_obj.write(cursor, uid, lot_ids, {'state': 'tancat'})
            lot_id = lot_obj.create(cursor, uid, {
                'name': 'lot_test',
                'data_inici': '2017-02-01',
                'data_final': '2017-03-01',
                'state': 'obert',
            })

            contract_id = self.get_contract_id(txn)

            compt2_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_lectures', 'comptador_0004')[1]
            compt_obj.write(cursor, uid, compt2_id, {'polissa': contract_id, 'data_baixa': False, 'active': True})

            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])
            contract.write({'data_ultima_lectura': '2017-01-01'})

            data_activacio_05 = '2017-01-02'

            for meter in contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.write({'data_alta': contract.data_alta, 'active': True})
            contract = contract_obj.browse(cursor, uid, contract_id)
            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            # Refresh browse
            contract = contract.browse()[0]
            meter = contract.comptadors[0]
            for meter in contract.comptadors:
                if meter.name == u'42553686':
                    break
            comptador_id = meter.id

            meter = contract.comptadors[0]
            for meter in contract.comptadors:
                if meter.name != u'42553686':
                    break
            comptador2_id = meter.id

            # Create lects
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2016-12-30', 'lectura': 0,
                    'comptador': comptador_id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-01', 'lectura': 1000,
                    'comptador': comptador_id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-04', 'lectura': 2000,
                    'comptador': comptador_id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-06', 'lectura': 3000,
                    'comptador': comptador_id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )
            lectpot_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-06', 'lectura': 500,
                    'comptador': comptador_id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )

            lect_pool_obj.create(
                cursor, uid, {
                    'name': '2017-01-06', 'lectura': 100,
                    'comptador': comptador2_id,
                    'tipus': 'A', 'periode': periode_id, 'origen_id': origen_id,
                }
            )

            new_contract_id = self.get_contract_id(txn, xml_id='polissa_0004')
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            new_contract.write(
                {'cups': contract.cups.id,
                 'data_baixa': False,
                 }
            )
            contract_obj.generar_periodes_potencia(cursor, uid, [new_contract_id])
            for meter in new_contract.comptadors:
                for lect in meter.lectures:
                    lect.unlink(context={})
                for lect in meter.lectures_pot:
                    lect.unlink(context={})
                for lect in meter.pool_lectures:
                    lect.unlink(context={})
                for lect in meter.pool_lectures_pot:
                    lect.unlink(context={})
                meter.unlink(context={})

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': True, 'change_adm': True, 'activacio_cicle': 'A',
                'change_retail_tariff': False, 'retail_tariff': False,
                'new_contract': new_contract_id
            })

            powers_tariff_update = {
                'power_p1': 7.000,
                'power_p2': 7.000,
                'power_p3': 7.000,
                'power_p4': 7.000,
                'power_p5': 7.000,
                'power_p6': 7.000,
                'tariff': '004',

            }

            wiz.write(powers_tariff_update)
            wiz.genera_casos_atr()

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            self.assertEqual(sw.ref, 'giscedata.polissa,{}'.format(new_contract_id))
            self.assertEqual(sw.get_pas().activacio_cicle, 'A')
            self.assertEqual(sw.get_pas().data_accio, '2017-01-01')
            self.assertTrue(old_contract.lot_facturacio)
            self.assertTrue(old_contract.facturacio_suspesa)

            # Create step 05 and test activation
            step_id = self.create_step(cursor, uid, sw, 'M1', '02')
            self.assertTrue(step_id)

            step_id = self.create_step(cursor, uid, sw, 'M1', '05')
            self.assertTrue(step_id)
            step = sw.get_pas()
            m1_05_id = step.id

            step.write({'data_activacio': data_activacio_05})

            m101_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            pas01 = m101_obj.browse(
                cursor, uid,
                m101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )

            # Activate changes
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            # self.create_pricelist(cursor, uid)
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            # Check everything is OK
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            self.assertEqual(sw.state, 'done')
            self.assertEqual(old_contract.data_ultima_lectura, '2017-01-01')
            self.assertEqual(old_contract.data_baixa, old_contract.data_ultima_lectura)
            self.assertEqual(old_contract.state, 'baixa')
            self.assertTrue(old_contract.lot_facturacio)
            self.assertFalse(old_contract.facturacio_suspesa)
            self.assertEqual(new_contract.data_alta, '2017-01-02')
            self.assertEqual(new_contract.state, 'activa')
            self.assertEqual(new_contract.lot_facturacio.id, lot_id)
            # Check old meters

            self.assertEqual(len(old_contract.comptadors), 2)
            meter = old_contract.comptadors[0]
            for meter in old_contract.comptadors:
                if meter.name == u'42553686':
                    break
            self.assertEqual(meter.name, u'42553686')
            self.assertEqual(meter.data_baixa, '2017-01-01')
            self.assertFalse(meter.active)
            self.assertEqual(len(meter.pool_lectures), 2)
            self.assertEqual(meter.pool_lectures[1].name, '2016-12-30')
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-01')
            self.assertEqual(len(meter.pool_lectures_pot), 0)
            # Check new meters
            self.assertEqual(len(new_contract.comptadors), 2)
            for meter in new_contract.comptadors:
                if meter.name == u'42553686':
                    break
            self.assertEqual(meter.name, u'42553686')
            self.assertEqual(meter.data_alta, '2017-01-02')
            self.assertFalse(meter.data_baixa)
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 3)
            # By now initial date is the same day than activation, will change
            # to the day before activation
            self.assertEqual(meter.pool_lectures[2].name, '2017-01-01')
            self.assertEqual(meter.pool_lectures[1].name, '2017-01-04')
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-06')
            self.assertEqual(meter.pool_lectures[0].lectura, 3000)
            self.assertEqual(len(meter.pool_lectures_pot), 1)
            for meter in new_contract.comptadors:
                if meter.name == 'B63011077':
                    break
            self.assertEqual(meter.name, 'B63011077')
            self.assertEqual(meter.data_alta, '2017-01-02')
            self.assertFalse(meter.data_baixa)
            self.assertTrue(meter.active)
            self.assertEqual(len(meter.pool_lectures), 1)
            self.assertEqual(meter.pool_lectures[0].name, '2017-01-06')
            self.assertEqual(meter.pool_lectures[0].lectura, 100)
            self.assertEqual(len(meter.pool_lectures_pot), 0)
