# -*- coding: utf-8 -*-
from destral.transaction import Transaction
from addons import get_module_resource
from giscedata_switching_comer.tests import TestValidateMeasures
from destral.patch import PatchNewCursors


class TestA3(TestValidateMeasures):

    def test_activate_a3(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)
            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])
            self.assertEqual(result[0], "OK")
            # Check contract is active
            contract = contract_obj.browse(cursor, uid, contract_id)
            pas05 = sw_obj.get_pas(cursor, uid, sw_ids)
            self.assertEqual(contract.state, "activa")
            self.assertEqual(contract.data_alta, pas05.data_activacio)
            self.assertEqual(contract.potencies_periode[0].potencia*1000, pas05.pot_ids[0].potencia)
            self.assertEqual(contract.tarifa.codi_ocsum, pas05.tarifaATR)
            self.assertTrue(contract.lot_facturacio)

    def test_activate_a3_compte_bancari_erroni(self):
        a305_xml_path = get_module_resource(
            'giscedata_switching_comer', 'tests', 'fixtures', 'a305.xml')
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            val_obj = self.openerp.pool.get('giscedata.switching.validacio.lectures')
            val_ids = val_obj.search(cursor, uid, [], context={'active_test': False})
            val_obj.write(cursor, uid, val_ids, {'active': True, 'show_message': False})
            codi_sollicitud, contract_id = self.prepare_a3_for_activation(cursor, uid, txn)

            # Posem un bank que no pertany al pagador
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.bank.write({'partner_id': 1})

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            with PatchNewCursors():
                sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(sw_ids), 1)
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])
            self.assertEqual(result[0], "ERROR")
            self.assertIn("te un compte bancari asignat", result[1])
            # Check contract is not active
            contract = contract_obj.browse(cursor, uid, contract_id)
            self.assertEqual(contract.state, "esborrany")
            contract.bank.write({'partner_id': contract.pagador.id})
            # Ara l'activem i anira be
            with PatchNewCursors():
                result = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_ids[0])
            self.assertEqual(result[0], "OK")
            # Check contract is active
            contract = contract_obj.browse(cursor, uid, contract_id)
            pas05 = sw_obj.get_pas(cursor, uid, sw_ids)
            self.assertEqual(contract.state, "activa")
            self.assertEqual(contract.data_alta, pas05.data_activacio)
            self.assertEqual(contract.potencies_periode[0].potencia*1000, pas05.pot_ids[0].potencia)
            self.assertEqual(contract.tarifa.codi_ocsum, pas05.tarifaATR)
            self.assertTrue(contract.lot_facturacio)
