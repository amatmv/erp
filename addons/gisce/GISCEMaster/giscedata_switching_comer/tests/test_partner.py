# coding=utf-8
from destral import testing
import mock
from giscedata_switching_comer import giscedata_polissa


class TestPartnerNoCession(testing.OOTestCaseWithCursor):
    @mock.patch.object(giscedata_polissa.GiscedataPolissa, 'request_no_cession_sips')
    def test_calling_no_cession_on_parter_calls_all_owned_contracts(self, mock_method):
        cursor = self.txn.cursor
        uid = self.txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        polisses_ids = polissa_obj.search(cursor, uid, [
            ('titular.id', '=', partner_id)
        ])
        partner = partner_obj.browse(cursor, uid, partner_id)
        pol_ids = partner.search_unactive_sips()
        polissa_obj.request_no_cession_sips(cursor, uid, pol_ids)
        mock_method.assert_called_with(cursor, uid, polisses_ids)
