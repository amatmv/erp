# coding=utf-8
from destral import testing
from datetime import datetime


class TestR1038NoCesioSIPS(testing.OOTestCaseWithCursor):

    def fix_codes(self, polissa):
        from tools.misc import cache

        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = self.txn.cursor
        uid = self.txn.user

        codes = {'distri': '1234', 'comer': '4321'}

        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]

        polissa.write({'comercialitzadora': partner_id})
        polissa.comercialitzadora.write({'ref': codes.pop('comer')})
        polissa.distribuidora.write({'ref': codes.values()[0]})
        polissa.cups.write({
            'distribuidora_id': polissa.distribuidora.id
        })
        cache.clean_caches_for_db(cursor.dbname)

    def test_calling_request_no_cession_sips_creates_r1_and_updates_status(self):
        cursor = self.txn.cursor
        uid = self.txn.user

        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        cfg = self.openerp.pool.get('res.config')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = pol_obj.browse(cursor, uid, polissa_id)
        self.fix_codes(pol)

        self.assertEqual(pol.no_cessio_sips, 'unactive')

        r1_ids = sw_obj.search(cursor, uid, [
            ('cups_polissa_id', '=', polissa_id),
            ('proces_id.name', '=', 'R1')
        ])
        self.assertEqual(len(r1_ids), 0)

        pol.request_no_cession_sips()

        r1_ids = sw_obj.search(cursor, uid, [
            ('cups_polissa_id', '=', polissa_id),
            ('proces_id.name', '=', 'R1')
        ])
        self.assertEqual(len(r1_ids), 1)
        r1 = sw_obj.browse(cursor, uid, r1_ids[0])
        pas = r1.step_ids[0].get_pas()

        self.assertEqual(
            pas.comentaris,
            cfg.get(cursor, uid, 'sw_r1_038_default_comment')
        )

        subtipus_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_subtipus_reclamacio',
            'subtipus_reclamacio_038'
        )[1]

        self.assertEqual(pas.tipus, '01')
        self.assertEqual(pas.subtipus_id.id, subtipus_id)

        pol = pol_obj.browse(cursor, uid, polissa_id)
        self.assertEqual(pol.no_cessio_sips, 'requested')
        self.assertEqual(
            pol.no_cessio_sips_data,
            datetime.now().strftime('%Y-%m-%d')
        )
