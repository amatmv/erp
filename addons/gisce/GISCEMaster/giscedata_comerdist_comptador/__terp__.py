# -*- coding: utf-8 -*-
{
    "name": "Comerdist (comptadors)",
    "description": """
    This module provide :
      * Sincronització de comptadors.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
