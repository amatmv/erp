# -*- coding: utf-8 -*-
from osv import osv
from giscedata_comerdist import Sync, OOOPPool
from tools.translate import _
from datetime import datetime

class GiscedataLecturesComptador(osv.osv):
    """Comtpador per sincronitzar.
    """

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for comptador in self.browse(cursor, uid, ids, context):
            if comptador.polissa and \
            comptador.polissa.comercialitzadora and \
            comptador.polissa.comercialitzadora.id in partner_ids and \
            comptador.get_config().user_local.id != uid:
                return True
            else:
                return False

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for comptador in self.browse(cursor, uid, ids, context):
            partner_id = comptador.polissa.comercialitzadora.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            config = config_obj.browse(cursor, uid, config_ids[0])
            return config

    def update_lloguer(self, cursor, uid, comptador,
                       proxy, sync, context=None):
        '''Write rent price and uom'''

        r_uom_obj = proxy.ProductUom
        uom_obj = self.pool.get('product.uom')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        # Implicated uoms
        uos = comptador.property_uom_id
        uom = comptador.product_lloguer_id.uom_id
        rent_price = comptador.product_lloguer_id.standard_price

        if not uos.factor % 365 or not uos.factor % 366:
            today = datetime.now().strftime('%Y-%m-%d')
            new_uos_id = facturador_obj.get_uoms_leaps(cursor, uid,
                                                       uos.id, today)
        if new_uos_id != uos.id:
            uos = uom_obj.browse(cursor, uid, new_uos_id)

        # Search for remote uos
        search_params = [('name', '=', uos.name),
                         ('factor', '=', uos.factor)]
        r_uos_id = r_uom_obj.search(search_params)[0]

        rent_price = uom_obj._compute_price(cursor, uid, uom.id,
                                            rent_price, uos.id)
        r_comptador = sync.get_object(comptador.id, active_test=False,
                                      double_test=True)
        r_comptador.write({'preu_lloguer': rent_price,
                           'uom_id': r_uos_id})

        return True

    def create(self, cursor, uid, vals, context=None):
        """Mètode create per la sincronització.
        """
        # Creem el comptador en local
        res_id = super(GiscedataLecturesComptador, self).create(cursor, uid,
                                                                vals, context)
        comptador = self.browse(cursor, uid, res_id, context)
        # Comprovem si s'ha de sincronitzar
        if comptador.must_be_synched(context):
            config_id = comptador.get_config().id
            ooop = OOOPPool.get_ooop(cursor, uid, config_id)
            sync = Sync(cursor, uid, ooop, self._name, config=config_id)
            sync.sync(comptador.id)
            if comptador.lloguer:
                self.update_lloguer(cursor, uid, comptador, ooop,
                                    sync, context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode write per la sincronització.
        """
        res = super(GiscedataLecturesComptador, self).write(cursor, uid, ids,
                                                            vals, context)
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for comptador in self.browse(cursor, uid, ids, context):
            if comptador.must_be_synched(context):
                config_id = comptador.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                # S'ha de tenir en compte quan és una pòlissa que torna a la
                # nostre comer. Després l'active_test i el double_test no ens
                # funciona ja que sempre ens retornarà el comptador antic.
                r_comptador = sync.get_object(
                    comptador.id, active_test=False, double_test=True
                )
                if not r_comptador:
                    sync.sync(comptador.id)
                    r_comptador = sync.get_object(
                        comptador.id, active_test=False, double_test=True
                    )
                r_polissa = r_comptador.polissa
                # Aquest cas la pòlissa remota ja està de baixa amb una data
                # diferent que la actual
                if (r_polissa.data_baixa and
                    r_polissa.data_baixa != comptador.polissa.data_baixa):
                    r_comptador = sync.sync(comptador.id)
                    r_comptador.write({
                        'data_alta': r_comptador.polissa.data_alta
                    })
                    if comptador.lloguer:
                        self.update_lloguer(cursor, uid, comptador, ooop,
                                            sync, context)
                else:
                    if comptador.lloguer:
                        self.update_lloguer(cursor, uid, comptador, ooop,
                                            sync, context)
                    sync.sync(comptador.id, vals, active_test=False,
                              double_test=True)
                if comptador.lloguer:
                    self.update_lloguer(cursor, uid, comptador, ooop,
                                        sync, context)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        """Mètode unlink per la sincronització.
        """
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for comptador in self.browse(cursor, uid, ids, context):
            if comptador.must_be_synched(context):
                config_id = comptador.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(comptador.id, active_test=False,
                                      double_test=True)
                if obj:
                    obj.unlink()
        res = super(GiscedataLecturesComptador, self).unlink(cursor, uid, ids,
                                                             context)
        return res

    def renombrar(self, cursor, uid, comptador_id, new_name, context=None):

        if not context:
            context = {}

        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context=context)

        #Per escriure en remot, hem d'activar la sincronitzacio
        context.update({'sync': True})
        #Escribim en remot el nou nom si ho hem de fer
        if comptador.must_be_synched():
            #Obtenim el valor del comptador remot
            config_id = comptador.get_config().id
            ooop = OOOPPool.get_ooop(cursor, uid, config_id)
            sync = Sync(cursor, uid, ooop,
                        'giscedata.lectures.comptador', config=config_id)
            comptador_remot = sync.get_object(comptador.id)
            if not comptador_remot:
                raise osv.except_osv(_(u"Error"),
                                     _(u"No s'ha pogut trobar el comptador "
                                       u"remot"))
            comptador_remot.renombrar(new_name)
        #Per escriure en local, desactivem la sincronitzacio
        context.update({'sync': False})
        return super(GiscedataLecturesComptador,
                     self).renombrar(cursor, uid, comptador_id,
                                     new_name, context)

GiscedataLecturesComptador()
