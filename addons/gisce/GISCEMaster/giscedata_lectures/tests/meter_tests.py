# -*- coding: utf-8 -*-

from osv.orm import ValidateException
from destral import testing
from destral.transaction import Transaction
from expects import *
from datetime import datetime, timedelta

class TestGiscedataComptador(testing.OOTestCase):

    def prepare_test(self, txn):
        self.pool = self.openerp.pool

        polissa_obj = self.pool.get('giscedata.polissa')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        imd_obj = self.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        # Reference meter
        self.comptador_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'comptador_0001'
        )[1]

        # active contract
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        self.polissa_id = polissa_id

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        # draft contract
        polissa_draft_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0006'
        )[1]
        self.polissa_draft_id = polissa_draft_id

        # other active contract
        polissa_other_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0002'
        )[1]

        self.polissa_other_id = polissa_other_id
        polissa_obj.send_signal(cursor, uid, [polissa_other_id], [
            'validar', 'contracte'
        ])

        comptador = comptador_obj.browse(cursor, uid, self.comptador_id)
        self.meter_dict = {
            'name': comptador.name,
            'polissa': self.polissa_draft_id,
            'data_alta': comptador.data_alta,
            'meter_type': comptador.meter_type
        }

    def test_contraint_unique_meter_draft_contract(self):
        comptador_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user
            self.prepare_test(txn)

            # New meter same name, draft contract
            meter_data = self.meter_dict.copy()

            comptador_obj.create(
                cursor, uid, meter_data
            )

    def test_contraint_unique_meter_active_contract(self):
        comptador_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user
            self.prepare_test(txn)

            # New meter same name, draft contract
            meter_data = self.meter_dict.copy()

            # New meter same name, active contract,
            meter_data['polissa'] = self.polissa_other_id

            expect(
                lambda: self.assertRaises(
                    ValidateException,
                    comptador_obj.create(cursor, uid, meter_data)
                )
            ).to(
                    raise_error(
                        ValidateException,
                        u"warning -- ValidateError\n\nError occurred while "
                        u"validating the field(s) Nº de sèrie: Error: "
                        u"Ja existeix un comptador actiu amb aquest "
                        u"numero de serie",
                    )
            )


    def test_contraint_unique_meter_active_contract_diferent_type(self):
        comptador_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        with Transaction().start(self.database) as txn:

            cursor = txn.cursor
            uid = txn.user
            self.prepare_test(txn)

            meter_data = self.meter_dict.copy()

            # New meter same name, active contract, diferent type
            meter_data['meter_type'] = 'G'
            comptador_obj.create(cursor, uid, meter_data)
