# -*- coding: utf-8 -*-
import time
from osv import osv, fields


class GiscedataCupsPs(osv.osv):
    """Model de Punt de servei (CUPS)."""

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def _polissa_polissa_search(self, cursor, uid, obj, name, args, context):
        """Permet buscar un CUPS segons el número de pòlissa."""
        if not len(args):
            return []
        else:
            pol_ids = self.pool.get('giscedata.polissa').search(cursor, uid,[
                ('name', args[0][1], args[0][2]),
                ('cups', '!=', False)])
            if not pol_ids:
                return [('id', '=', '0')]
            else:
                ids = []
                for polissa in self.pool.get('giscedata.polissa').read(
                        cursor, uid, pol_ids, ['cups']):
                    ids.append(polissa['cups'][0])
                return [('id', 'in', ids)]

    def _polissa_ncomptador(self, cursor, uid, ids, field_name, arg, context):
        """Número de comptador del CUPS."""
        res = {}
        conta_obj = self.pool.get('giscedata.lectures.comptador')

        search_params = [('id', 'in', ids)]
        cups_ids = self.search(cursor, uid, search_params)
        for cups in self.browse(cursor, uid, cups_ids):
            polissa = cups.polissa_polissa
            comptadors = polissa and polissa.comptadors_actius(
                time.strftime('%Y-%m-%d')) or []
            if comptadors:
                comptador = conta_obj.read(cursor, uid, comptadors[0], ['name'])
                res[cups.id] = comptador['name']
            else:
                res[cups.id] = False
        return res

    def _polissa_ncomptador_search(self, cursor, uid, obj, name, args, context):
        """Permet buscar un CUPS segons el número de comptador."""
        conta_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        ids = []
        if not args:
            return ids
        else:
            search_params = [('name', args[0][1], args[0][2])]
            conta_id = conta_obj.search(cursor, uid, search_params)
            if conta_id:
                polissa_id = conta_obj.read(cursor, uid, conta_id,
                                            ['polissa'])[0]['polissa'][0]
                for polissa in polissa_obj.browse(cursor, uid, [polissa_id]):
                    ids.append(polissa.cups.id)
            return [('id', 'in', ids)]

    _columns = {
        'polissa_comptador': fields.function(
            _polissa_ncomptador,
            fnct_search=_polissa_ncomptador_search,
            method=True, type='char',
            string='Nº de comptador', size=60),
    }


GiscedataCupsPs()
