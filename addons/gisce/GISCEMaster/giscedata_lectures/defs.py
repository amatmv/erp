# -*- coding: utf-8 -*-
from switching.defs import TABLA_106
# Definició de variables del mòdul
CODIS_LECTURES_ESTIMADES = ['40', '99']
PROPIETAT_COMPTADOR_SELECTION  = [('empresa', 'Empresa'),
                                  ('client', 'Client')]
MOTIUS_AJUST = TABLA_106
MOTIUS_AJUST += [('98', 'Autoconsumo')]
