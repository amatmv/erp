# coding=utf-8
import logging
from tqdm import tqdm
from oopgrade.oopgrade import column_exists, add_columns, drop_columns

COLUMNS = [
    ('lectura_exporta', 'integer'),
    ('ajust_exporta', 'integer'),
    ('generacio', 'integer'),
]


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    columns_to_add = []

    for col in COLUMNS:
        field = col[0]
        if column_exists(cursor, 'giscedata_lectures_lectura', field):
            logger.info('Column {} already exists. Passing'.format(field))
        else:
            columns_to_add.append(col)

    if columns_to_add:
        add_columns(cursor, {
            'giscedata_lectures_lectura': columns_to_add
        })

    for col in COLUMNS:
        field = col[0]
        logger.info('Updating all {} column to 0'.format(field))
        cursor.execute("SELECT count(id) from giscedata_lectures_lectura where {} is null".format(field))
        total = cursor.fetchone()[0]
        with tqdm(total=total) as pbar:
            while total:
                cursor.execute(
                    "UPDATE giscedata_lectures_lectura SET {} = 0 "
                    "WHERE id in (SELECT id from giscedata_lectures_lectura where "
                    "{} is null LIMIT 10000)".format(field, field)
                )
                pbar.update(cursor.rowcount)
                cursor.execute(
                    "SELECT count(id) from giscedata_lectures_lectura where {} is null".format(field)
                )
                total = cursor.fetchone()[0]

        logger.info('Updating {} column'.format(field))

def down(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    for col in COLUMNS:
        field = col[0]
        logger.info('Dropping column {} from giscedata_lectures_lectura'.format(field))
        drop_columns(cursor, [('giscedata_lectures_lectura', field)])

migrate = up
