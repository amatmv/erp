# -*- coding: utf-8 -*-

import pooler
import netsvc

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log

def migrate(cursor, installed_version):
    """Neteja de lectures
    """
    uid = 1
    pool = pooler.get_pool(cursor.dbname)

    # netejem lectures sense comptador
    log('Netejant lectures sense comptador')
    cursor.execute("DELETE FROM giscedata_lectures_lectura "
                   "WHERE comptador IS NULL")

    # evitar creació constraint consum_positiu
    imd_obj = pool.get('ir.model.data')
    imd_id = imd_obj.read(cursor, uid,
                          imd_obj._get_id(cursor, uid, 'giscedata_polissa',
                                          'p1_e_tarifa_20A_new'), ['res_id'])
    imd_id = imd_id['res_id']
    cursor.execute("ALTER TABLE giscedata_lectures_lectura "
                   "ADD COLUMN consum integer")
    cursor.execute("INSERT INTO giscedata_lectures_lectura "
                   "(periode,lectura,tipus,hora,name,consum) "
                   "VALUES (%s, 0, 'A', '12:00:00', '2000-01-01', -1)" % imd_id)

