# -*- coding: utf-8 -*-
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log
import pooler

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    comp_obj = pool.get('giscedata.lectures.comptador')
    c_ids = comp_obj.search(cursor, uid, ['|',
                                           ('lloguer', '=', 0),
                                           ('lloguer', '=', False)],
                             context={'active_test': False})
    comp_obj.write(cursor, uid, c_ids, {'propietat': 'client'})
    c_ids = comp_obj.search(cursor, uid, [('lloguer', '=', 1)],
                             context={'active_test': False})
    comp_obj.write(cursor, uid, c_ids, {'propietat': 'empresa'})

    cursor.execute("""ALTER TABLE giscedata_factura
    DROP constraint giscedata_factura_tax_fkey""")
    cursor.execute("""ALTER TABLE giscedata_factura
    ADD constraint giscedata_factura_tax_fkey FOREIGN KEY (tax)
    REFERENCES account_tax(id) ON DELETE restrict""")

    # Busquem comptadors que les dates estiguin pa'llá...
    cursor.execute("""SELECT
      c.id,
      c.name,
      c.data_alta,
      min(f.data_inici) as data_inici,
      min(f.data_final) as data_final
    FROM
      giscedata_lectures_comptador c,
      giscedata_factura f
    WHERE
      f.comptador = c.id
    GROUP BY
      c.name,
      c.id,
      c.data_alta
    HAVING data_alta > min(f.data_final)""")
    for comptador in cursor.dictfetchall():
        comp_obj.write(cursor, uid, [comptador['id']],
                        {'data_alta': comptador['data_inici']})
        log("Aplicant data d'alta %s al comptador %s (aniga: %s)"
            % (comptador['data_inici'], comptador['name'],
               comptador['data_alta']))
    log("Assignem origen a totes les lectures que no en tingui")
    cursor.execute("UPDATE giscedata_lectures_lectura "
                   "SET "
                   "origen_id = (select id from giscedata_lectures_origen "
                   "where codi = '20') WHERE origen_id is null")
