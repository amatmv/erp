# -*- coding: utf-8 -*-

from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import GisceUpgradeMigratoor, log
import netsvc
import pooler

def migrate(cursor, installed_version):
    """Canvis a executar relatius al mòdul base
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Migration from %s'
                         % installed_version)

    # backup
    mig = GisceUpgradeMigratoor('giscedata_lectures', cursor)
    mig.backup(suffix='v4')
    # De v4 a v5 els comptadors de telegestió han canviat el codi de l'xml
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    imd_obj = pool.get('ir.model.data')
    renom = [
        ('alq_conta_domestico_tele', 'alq_conta_tele'),
        ('alq_conta_domestico_tele_product_template',
         'alq_conta_tele_product_template')
    ]
    for old, new in renom:
        imd_id = imd_obj.search(cursor, uid, [('name', '=', old)])
        log("Renombrant %s -> %s (id:%s)" % (old, new, imd_id[0]))
        imd_obj.write(cursor, uid, imd_id, {'name': new})
    mig.pre_xml()
