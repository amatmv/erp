# -*- coding: utf-8 -*-

import wizard
import pooler
import time

def _init(self, cr, uid, data, context=None):
    if data.get('model', '') == 'giscedata.lectures.comptador':
        comptador_id = data['id']
    else:
        comptador_id = False
    data['comptador'] = 0
    data['n_comptadors'] = 0
    data['comptadors'] = []
    if data.has_key('form'):
        if data['form'].has_key('data'):
            data_lec = data['form']['data']
            data['form'] = {'data': data_lec}
        else:
            data['form'] = {}
    return {'comptador_id': comptador_id}

_init_form = """<?xml version="1.0"?>
<form string="Introducción lecturas por contador">
  <field name="comptador_id" colspan="4" width="200" context="{'tree_view_ref': 'giscedata_lectures.view_comptador_intro_lectures_tree'}"/>
</form>"""

_init_fields = {
  'comptador_id': {'string': 'Contador', 'type': 'many2one', 'relation': 'giscedata.lectures.comptador', 'required': True}
}

def _check_comptadors(self, cr, uid, data, context=None):
    data['comptadors'].append(data['form']['comptador_id'])
    data['n_comptadors'] += 1
    return {}

def _check_comptadors_next_state(self, cr, uid, data, context=None):
    if data['comptador'] >= data['n_comptadors']:
        return 'init'
    else:
        return 'init_lectures'

def _init_lectures(self, cr, uid, data, context=None):
    comptador_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.comptador')
    comptador = comptador_obj.browse(cr, uid, data['comptadors'][data['comptador']])
    data['comptador'] += 1
    if not data['form'].has_key('data'):
        data_form = time.strftime('%Y-%m-%d')
    else:
        data_form = data['form']['data']
    return {
      'client': comptador.polissa.titular.id,
      'polissa': comptador.polissa.id,
      'tarifa': comptador.polissa.tarifa.id,
      'comptador': comptador.name,
      'data': data_form,
      'comptador_id': comptador.id,
    }

_init_lectures_form = """<?xml version="1.0"?>
<form string="Introducción lecturas">
  <field name="client" colspan="4" />
  <field name="polissa" />
  <field name="tarifa" />
  <field name="comptador" />
  <field name="comptador_id" invisible="1" />
  <field name="origen_id" required="1" colspan="4"/>
  <newline />
  <field name="data" />
</form>"""

_init_lectures_fields = {
  'client': {'string': 'Cliente', 
             'type': 'many2one', 
             'relation': 'res.partner', 
             'readonly': True},
  'polissa': {'string': 'Póliza', 
              'type': 'many2one', 
              'relation': 'giscedata.polissa', 
              'readonly': True},
  'tarifa': {'string': 'Tarifa', 
             'type': 'many2one', 
             'relation': 'giscedata.polissa.tarifa', 
             'readonly': True},
  'comptador': {'string': 'Contador', 
                'type': 'char', 
                'size': 64, 
                'readonly': True},
  'comptador_id': {'string': 'Comptador ID', 
                   'type': 'integer'},
  'data': {'string': 'Data', 
           'type': 'date'},
  'origen_id': {'string': 'Origen', 
                'type': 'many2one', 
                'relation': 'giscedata.lectures.origen', 
                'required': True}
}

def _check_intro_dades_pot(self, cr, uid, data, context=None):
    data['periode_p'] = 0
    data['n_periodes_p'] = 0
    data['periodes_p'] = []
    tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
    tarifa = tarifa_obj.browse(cr, uid, data['form']['tarifa'])
    for periode in tarifa.periodes:
        if periode.tipus == 'tp':
            data['periodes_p'].append(periode.id)
            data['n_periodes_p'] += 1
    return {}

def _save_pot(self, cr, uid, data, context=None):
    lectura_p_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.potencia')
    vals = {
      'name': data['form']['data'],
      'periode': data['form']['periode'],
      'lectura': data['form']['maximetre'],
      'comptador': data['form']['comptador_id'],
      'origen_id': data['form']['origen_id'],
    }
    if data['form']['exces'] > 0:
        vals['exces'] = data['form']['exces']
    lectura_p_obj.create(cr, uid, vals)

    return {}

def _check_intro_dades_pot_next_state(self, cr, uid, data, context=None):
    if data['periode_p'] >= data['n_periodes_p']:
        return 'check_intro_dades'
    else:
        return 'init_intro_dades_pot'


def _init_intro_dades_pot(self, cr, uid, data, context=None):
    comptador_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.comptador')
    comptador = comptador_obj.browse(cr, uid, data['form']['comptador_id'])
    periode_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa.periodes')
    periode = periode_obj.browse(cr, uid, data['periodes_p'][data['periode_p']])

    # Amaguem l'exces si no és una tarifa del tipus 6_1
    if not periode.tarifa.name.startswith('6.'):
        self.states['init_intro_dades_pot']['result']['fields']['exces']['readonly'] = True
    else:
        self.states['init_intro_dades_pot']['result']['fields']['exces']['readonly'] = False

    data['periode_p'] += 1
    return {
      'client': comptador.polissa.titular.id,
      'polissa': comptador.polissa.id,
      'tarifa': comptador.polissa.tarifa.id,
      'comptador': comptador.name,
      'comptador_id': comptador.id,
      'data': data['form']['data'],
      'periode': periode.id,
      'maximetre': 0.0,
      'exces': 0.0,
    }

_init_intro_dades_pot_form = """<?xml version="1.0"?>
<form string="Introducción lecturas">
  <field name="client" colspan="4" />
  <field name="polissa" />
  <field name="tarifa" />
  <field name="comptador" />
  <field name="comptador_id" invisible="1" />
  <newline />
  <field name="data" readonly="1"/>
  <field name="periode" readonly="1" colspan="4"/>
  <separator string="Maxímetro" colspan="4" />
  <field name="maximetre" />
  <field name="exces" />
  <group colspan="4">
    <label string="Solo rellenar el campo exceso para tarifas que el exceso el de el contador (6.N)" />
  </group>
</form>"""

_init_intro_dades_pot_fields = {
  'client': {'string': 'Cliente', 'type': 'many2one', 'relation': 'res.partner', 'readonly': True},
  'polissa': {'string': 'Póliza', 'type': 'many2one', 'relation': 'giscedata.polissa', 'readonly': True},
  'tarifa': {'string': 'Tarifa', 'type': 'many2one', 'relation': 'giscedata.polissa.tarifa', 'readonly': True},
  'comptador': {'string': 'Contador', 'type': 'char', 'size': 64, 'readonly': True},
  'data': {'string': 'Data', 'type': 'date'},
  'periode': {'string': 'Periodo', 'type': 'many2one', 'relation': 'giscedata.polissa.tarifa.periodes', 'readonly': True},
  'comptador_id': {'string': 'Comptador ID', 'type': 'integer'},
  'maximetre': {'string': 'Potencia', 'type': 'float', 'required': True},
  'exces': {'string': 'Exceso', 'type': 'float'},
}


def _check_intro_dades(self, cr, uid, data, context=None):
    data['periode'] = 0
    data['n_periodes'] = 0
    data['periodes'] = []
    tarifa_obj = pooler.get_pool(cr.dbname).get('giscedata.polissa.tarifa')
    tarifa = tarifa_obj.browse(cr, uid, data['form']['tarifa'])
    for periode in tarifa.periodes:
        if periode.tipus == 'te':
            data['periodes'].append(periode.id)
            data['n_periodes'] += 1
    return {}

def _check_intro_dades_next_state(self, cr, uid, data, context=None):
    if data['periode'] >= data['n_periodes']:
        return 'check_comptadors_next_state'
    else:
        return 'init_intro_dades'


def _init_intro_dades(self, cursor, uid, data, context=None):
    comptador_obj = pooler.get_pool(cursor.dbname).get('giscedata.lectures.comptador')
    comptador = comptador_obj.browse(cursor, uid, data['form']['comptador_id'])
    periode_obj = pooler.get_pool(cursor.dbname).get('giscedata.polissa.tarifa.periodes')
    periode = periode_obj.browse(cursor, uid, data['periodes'][data['periode']])

    lectura_obj = pooler.get_pool(cursor.dbname).get('giscedata.lectures.lectura')
    search_params = [
        ('comptador.id', '=', comptador.id),
        ('name', '<', data['form']['data']),
        ('periode.id', '=', periode.id)
    ]
    search_activa = search_params[:]
    search_activa.append(('tipus', '=', 'A'))
    lectura_ids = lectura_obj.search(cursor, uid, search_activa, limit=1,
                                     order="name desc", context=context)
    if lectura_ids:
        lectura = lectura_obj.read(cursor, uid, lectura_ids, ['lectura', 'name'],
                                  context)[0]
        activa = lectura['lectura']
        data_activa = lectura['name']
    else:
        activa = 0
        data_activa = False

    search_reactiva = search_params[:]
    search_reactiva.append(('tipus', '=', 'R'))
    lectura_ids = lectura_obj.search(cursor, uid, search_reactiva, limit=1,
                                     order="name desc", context=context)
    if lectura_ids:
        lectura = lectura_obj.read(cursor, uid, lectura_ids,
                                    ['lectura', 'name'], context)[0]
        reactiva = lectura['lectura']
        data_reactiva = lectura['name']
    else:
        reactiva = 0
        data_reactiva = False

    data['periode'] += 1
    return {
      'client': comptador.polissa.titular.id,
      'polissa': comptador.polissa.id,
      'tarifa': comptador.polissa.tarifa.id,
      'comptador': comptador.name,
      'comptador_id': comptador.id,
      'data': data['form']['data'],
      'periode': periode.id,
      'data_activa_ant': data_activa,
      'activa_ant': activa,
      'data_activa': data['form']['data'],
      'activa': 0.0,
      'data_reactiva': data['form']['data'],
      'reactiva': 0.0,
      'reactiva_ant': reactiva,
      'data_reactiva_ant': data_reactiva,
    }

_init_intro_dades_form = """<?xml version="1.0"?>
<form string="Introducción lecturas">
  <field name="client" colspan="4" />
  <field name="polissa" />
  <field name="tarifa" />
  <field name="comptador" />
  <field name="comptador_id" invisible="1" />
  <newline />
  <field name="data" readonly="1"/>
  <field name="periode" readonly="1" colspan="4"/>
  <separator string="Activa" colspan="4" />
  <field name="data_activa_ant" nolabel="1" colspan="2" readonly="1" />
  <field name="data_activa" nolabel="1" colspan="2" readonly="1"/>
  <field name="activa_ant" string="Anterior"/>
  <field name="activa" string="Actual"/>
  <separator string="Reactiva" colspan="4" />
  <field name="data_reactiva_ant" nolabel="1" colspan="2" readonly="1"/>
  <field name="data_reactiva" nolabel="1" colspan="2" readonly="1"/>
  <field name="reactiva_ant" string="Anterior"/>
  <field name="reactiva" string="Actual"/>
</form>"""

_init_intro_dades_fields = {
  'client': {'string': 'Cliente', 'type': 'many2one', 'relation': 'res.partner', 'readonly': True},
  'polissa': {'string': 'Póliza', 'type': 'many2one', 'relation': 'giscedata.polissa', 'readonly': True},
  'tarifa': {'string': 'Tarifa', 'type': 'many2one', 'relation': 'giscedata.polissa.tarifa', 'readonly': True},
  'comptador': {'string': 'Contador', 'type': 'char', 'size': 64, 'readonly': True},
  'data': {'string': 'Data', 'type': 'date'},
  'periode': {'string': 'Periodo', 'type': 'many2one', 'relation': 'giscedata.polissa.tarifa.periodes', 'readonly': True},
  'comptador_id': {'string': 'Comptador ID', 'type': 'integer'},
  'data_activa_ant': {'string': 'Data', 'type': 'date'},
  'activa_ant': {'string': 'Activa ant.', 'type': 'integer', 'readonly': True},
  'data_activa': {'string': 'Data', 'type': 'date'},
  'activa': {'string': 'Activa', 'type': 'integer', 'required': True},
  'data_reactiva_ant': {'string': 'Data', 'type': 'date'},
  'reactiva_ant': {'string': 'Reactiva ant.', 'type': 'integer', 'readonly': True},
  'data_reactiva': {'string': 'Data', 'type': 'date'},
  'reactiva': {'string': 'Reactiva', 'type': 'integer', 'required': True},
}

def _save_ene(self, cr, uid, data, context=None):
    lectura_obj = pooler.get_pool(cr.dbname).get('giscedata.lectures.lectura')
    vals = {
      'name': data['form']['data'],
      'periode': data['form']['periode'],
      'lectura': data['form']['activa'],
      'tipus': 'A',
      'comptador': data['form']['comptador_id'],
      'origen_id': data['form']['origen_id'],
    }
    lectura_obj.create(cr, uid, vals)

    vals = {
      'name': data['form']['data'],
      'periode': data['form']['periode'],
      'lectura': data['form']['reactiva'],
      'tipus': 'R',
      'comptador': data['form']['comptador_id'],
      'origen_id': data['form']['origen_id'],
    }
    lectura_obj.create(cr, uid, vals)
    return {}


class giscedata_lectures_introduir_lectures_comptador(wizard.interface):

    states = {
      'init': {
        'actions': [_init],
        'result': {'type': 'form', 'arch': _init_form, 'fields': _init_fields,  'state': [('end', 'Tancar', 'gtk-cancel'), ('check_comptadors', 'Continuar', 'gtk-go-forward')]},
      },
      'check_comptadors': {
        'actions': [_check_comptadors],
        'result': {'type': 'state', 'state': 'check_comptadors_next_state'}
      },
      'check_comptadors_next_state': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _check_comptadors_next_state}
      },
      'init_lectures': {
        'actions': [_init_lectures],
        'result': {'type': 'form', 'arch': _init_lectures_form, 'fields': _init_lectures_fields, 'state': [('end', 'Tancar', 'gtk-cancel'), ('check_intro_dades_pot', 'Introducir datos', 'gtk-edit'), ('check_comptadors_next_state', 'Siguiente contador', 'gtk-go-forward')]}
      },
      'check_intro_dades_pot_next_state': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _check_intro_dades_pot_next_state}
      },
      'check_intro_dades_pot': {
        'actions': [_check_intro_dades_pot],
        'result': {'type': 'state', 'state': 'check_intro_dades_pot_next_state'}
      },
      'init_intro_dades_pot': {
        'actions': [_init_intro_dades_pot],
        'result': {'type': 'form', 'arch': _init_intro_dades_pot_form, 'fields': _init_intro_dades_pot_fields, 'state': [('end', 'Tancar', 'gtk-cancel'), ('save_pot', 'Guardar y siguiente', 'gtk-save'), ('check_intro_dades_pot_next_state', 'Siguiente', 'gtk-go-forward')]}
      },
      'save_pot': {
        'actions': [_save_pot],
        'result': {'type': 'state', 'state': 'check_intro_dades_pot_next_state'}
      },
      'check_intro_dades_next_state': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _check_intro_dades_next_state}
      },
      'check_intro_dades': {
        'actions': [_check_intro_dades],
        'result': {'type': 'state', 'state': 'check_intro_dades_next_state'}
      },
      'init_intro_dades': {
        'actions': [_init_intro_dades],
        'result': {'type': 'form', 'arch': _init_intro_dades_form, 'fields': _init_intro_dades_fields, 'state': [('end', 'Tancar', 'gtk-cancel'), ('save_ene', 'Guardar y siguiente', 'gtk-save'), ('check_intro_dades_next_state', 'Siguiente', 'gtk-go-forward')]}
      },
      'save_ene': {
        'actions': [_save_ene],
        'result': {'type': 'state', 'state': 'check_intro_dades_next_state'},
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      }

    }


giscedata_lectures_introduir_lectures_comptador('giscedata.lectures.introduir.lectures.comptador')
