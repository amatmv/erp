# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields


class WizardRenombrarComptador(osv.osv_memory):

    _name = 'wizard.renombrar.comptador'

    def action_renombrar(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        wizard = self.browse(cursor, uid, ids[0])
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        comptador_obj.renombrar(cursor, uid, wizard.comptador_id.id,
                                wizard.new_name, context=context)

        wizard.write({'state': 'end'})

    _columns = {
        'comptador_id': fields.many2one('giscedata.lectures.comptador',
                                       'Comptador', required=True),
        'new_name': fields.char('Nº de sèrie nou', size=20,
                               required=True, readonly=False),
        'state': fields.selection([('init', 'Init'),
                                  ('end', 'End')], 'Estat',
                                 select=True, readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardRenombrarComptador()
