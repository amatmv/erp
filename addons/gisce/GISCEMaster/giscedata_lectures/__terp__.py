# -*- coding: utf-8 -*-
{
    "name": "Lectura de comptadors (Base)",
    "description": """Lectures dels comptadors""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "product",
        "stock",
        "infraestructura"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_lectures_demo.xml"
    ],
    "update_xml":[
        "giscedata_lectures_data.xml",
        "giscedata_lectures_wizard.xml",
        "giscedata_lectures_view.xml",
        "giscedata_polissa_view.xml",
        "giscedata_cups_view.xml",
        "wizard/wizard_renombrar_comptador_view.xml",
        "giscedata_lectures_report.xml",
        "ir.model.access.csv",
        "security/giscedata_lectures_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
