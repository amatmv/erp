# -*- coding: utf-8 -*-
"""Models per la presa de lectures
"""
from __future__ import absolute_import

from osv import osv, fields
from tools.translate import _
import time
from giscedata_lectures.defs import *
from addons import get_module_resource
from osv.expression import OOQuery
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


def recalc_consums(lectures, giro):
    """Funció per tal d'assegurar que el consum i la generació
    de lectures està ben calculat.
    """
    for periode in lectures.keys():
        lectura = lectures[periode]
        if not lectura['anterior']:
            # Si no hi ha lectura anterior és que és una lectura inicial!
            # Consum 0
            consum = 0
            generacio = 0
        else:
            consum = ((lectura['actual'].get('lectura', 0)
                      - lectura['anterior'].get('lectura', 0))
                      + lectura['actual'].get('ajust', 0))
            generacio = ((lectura['actual'].get('lectura_exporta', 0)
                         - lectura['anterior'].get('lectura_exporta', 0))
                         + lectura['actual'].get('ajust_exporta', 0))
        if consum < 0:
            consum += giro
        if generacio < 0:
            generacio += giro
        lectura['actual']['consum'] = consum
        lectura['actual']['generacio'] = generacio
    return lectures

# TODO: Posar restriccions només una lectura per periode


class GiscedataLecturesIncidenciesCategoria(osv.osv):
    """Categories d'incidències de lectura
    """

    _name = 'giscedata.lectures.incidencies.categoria'

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        if not len(ids):
            return res
        for i in self.read(cursor, uid, ids, ['name', 'descripcio'], context):
            res.append((i['id'], '%s - %s' % (i['name'], i['descripcio'])))
        return res

    _columns = {
      'name': fields.char('Codi', size=10, required=True),
      'descripcio': fields.char('Descripció', size=255, required=True),
    }
GiscedataLecturesIncidenciesCategoria()


class GiscedataLecturesIncidenciesIncidencia(osv.osv):
    """Incidències de lectures.
    """

    _name = 'giscedata.lectures.incidencies.incidencia'

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        if not len(ids):
            return res
        for i in self.read(cursor, uid, ids, ['name', 'descripcio'], context):
            res.append((i['id'], '%s - %s' % (i['name'], i['descripcio'])))
        return res

    _columns = {
      'name': fields.char('Codi', size=10, required=True),
      'descripcio': fields.char('Descripció', size=255, required=True),
      'categoria_id': fields.many2one('giscedata.lectures.incidencies.'
                                      'categoria',
                                      'Categoria', required=True)
    }

GiscedataLecturesIncidenciesIncidencia()


class GiscedataLecturesComptador(osv.osv):
    """Comptadors
    """

    _METER_TYPE = [
        (u'PF', u'Punt frontera'),
        (u'G', u'Generació'),
        (u'C', u'Consum'),
    ]

    _name = 'giscedata.lectures.comptador'

    def data_ultima_lectura(self, cursor, uid, comptador_id, tarifa_id,
                            context=None):
        """Retorna la data de l'útlima lectura segons la tarifa que s'indiqui.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, list) or isinstance(comptador_id, tuple):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context)
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        periodes_ids = periodes_obj.search(cursor, uid, search_params,
                                           context=context)
        search_params = [
            ('periode.id', 'in', periodes_ids),
            ('comptador.id', '=', comptador.id)
        ]

        if context.get('fins_lectura_fact', False):
            search_params.append(('name', '<=', context['fins_lectura_fact']))

        context.update({'active_test': False})
        lectures_ids = lectures_obj.search(cursor, uid, search_params, limit=1,
                                           order="name desc", context=context)
        if lectures_ids:
            data_ultima_lectura = lectures_obj.read(cursor, uid, lectures_ids,
                                                    ['name'])[0]['name']
            return data_ultima_lectura
        return False

    def get_generacio_per_facturar(self, cursor, uid, comptador_id, tarifa_id, periodes=None, context=None):
        """Calcula la generacio pendent per facturar.
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx['get_generacio'] = True
        return self.get_consum_per_facturar(cursor, uid, comptador_id, tarifa_id, periodes=periodes, context=ctx)

    def get_consum_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                tipus='A', periodes=None, context=None):
        """Calcula el consum pendent per facturar.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context)
        cdata_alta = datetime.strptime(comptador.polissa.data_alta, "%Y-%m-%d")
        cdata_alta -= timedelta(days=1)
        cdata_alta = cdata_alta.strftime("%Y-%m-%d")
        ult_lectura_fact = max(comptador.polissa.data_ultima_lectura,
                               comptador.polissa.data_ultima_lectura_estimada,
                               cdata_alta)
        ult_lectura_fact = context.get('ult_lectura_fact', ult_lectura_fact)
        if context.get("get_generacio"):
            return comptador.get_generacio(tarifa_id, ult_lectura_fact, tipus, True, periodes=periodes, context=context)
        else:
            return comptador.get_consum(tarifa_id, ult_lectura_fact, tipus, True, periodes=periodes, context=context)

    def get_consum(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   tipus='A', per_facturar=False, periodes=None, context=None):
        """Calcula el consum.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        consums = {}
        lectures = self.get_lectures(cursor, uid, comptador_id, tarifa_id,
                                     from_date, tipus, per_facturar, periodes,
                                     context)
        for periode in lectures.keys():
            consums[periode] = lectures[periode]['actual']['consum']
        return consums

    def get_generacio(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   tipus='A', per_facturar=False, periodes=None, context=None):
        """Calcula la generacio.

        En el cas que la lectura a facturar sigui real.
        """
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        generacions = {}
        lectures = self.get_lectures(cursor, uid, comptador_id, tarifa_id,
                                     from_date, tipus, per_facturar, periodes,
                                     context)
        for periode in lectures.keys():
            generacions[periode] = lectures[periode]['actual']['generacio']
        return generacions

    def get_lectures_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                  tipus='A', periodes=None, context=None):
        """Retorna les lectures pendents de facturar.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context)
        cdata_alta = datetime.strptime(comptador.polissa.data_alta, "%Y-%m-%d")
        cdata_alta -= timedelta(days=1)
        cdata_alta = cdata_alta.strftime("%Y-%m-%d")
        ult_lectura_fact = max(comptador.polissa.data_ultima_lectura,
                               comptador.polissa.data_ultima_lectura_estimada,
                               cdata_alta)
        ult_lectura_fact = context.get('ult_lectura_fact', ult_lectura_fact)
        return comptador.get_lectures(tarifa_id, ult_lectura_fact, tipus, True,
                                      periodes, context)

    def get_maximetres_per_data(self, cursor, uid, comptador_id,
                                data_inici, data_final, context=None):
        """Retorna els maxímetres entre les dates aportades.
        """
        l_pot_obj = self.pool.get('giscedata.lectures.potencia')
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        if not context:
            context = {}

        # For 1 day invoices:
        # Maximeters are searched with init_date not included and final_date included.
        # This works well except in 1 day invoices where init_date is the same than final_date.
        # So we will substract 1 day to init_date in those cases when searching for maximeters.
        if data_inici == data_final:
            data_inici = (
                datetime.strptime(data_inici, '%Y-%m-%d') - timedelta(days=1)
            ).strftime('%Y-%m-%d')

        context.update({'active_test': False})
        search_params = [
            ('comptador.id', '=', comptador_id),
            ('name', '>', data_inici),
            ('name', '<=', data_final)
        ]
        max_ids = l_pot_obj.search(cursor, uid, search_params, context=context)

        return max_ids

    def get_maximetres_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                    data_inici, data_final, context=None):
        """Retorna els maxímetres per facturar.
        """
        maxs = {}
        l_pot_obj = self.pool.get('giscedata.lectures.potencia')

        max_ids = self.get_maximetres_per_data(cursor, uid, comptador_id,
                                               data_inici, data_final, context=context)

        for maxim in l_pot_obj.browse(cursor, uid, max_ids):
            if (not maxs.get(maxim.periode.name) or
               maxim.lectura >= maxs.get(maxim.periode.name)['maximetre']):
                maxs[maxim.periode.name] = {
                    'maximetre': maxim.lectura,
                    'data': maxim.name
                }

        return maxs

    def get_excesos_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                 data_inici, data_final, context=None):
        """Retorna els excesos per una 6.X.
        """
        res_excs = {}
        l_pot_obj = self.pool.get('giscedata.lectures.potencia')
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        if not context:
            context = {}

        # For 1 day invoices:
        # excesos are searched with init_date not included and final_date included.
        # This works well except in 1 day invoices where init_date is the same than final_date.
        # So we will substract 1 day to init_date in those cases when searching for excesos.
        if data_inici == data_final:
            data_inici = (
                datetime.strptime(data_inici, '%Y-%m-%d') - timedelta(days=1)
            ).strftime('%Y-%m-%d')

        context.update({'active_test': False})
        search_params = [
            ('comptador.id', '=', comptador_id),
            ('name', '>', data_inici),
            ('name', '<=', data_final)
        ]
        exc_ids = l_pot_obj.search(cursor, uid, search_params, context=context)
        for exc in l_pot_obj.browse(cursor, uid, exc_ids):
            res_excs[exc.periode.name] = max(exc.exces,
                                             res_excs.get(exc.periode.name, 0))
        return res_excs

    def get_lectures(self, cursor, uid, comptador_id, tarifa_id, from_date,
                    tipus='A', per_facturar=False, periodes=None, context=None):
        """Busca les lectures actuals i anteriors.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id)
        lectures = {}
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        if not periodes:
            ctx = context.copy()
            ctx.update({'active_test': False})
            periodes_ids = periodes_obj.search(cursor, uid, search_params,
                                               context=ctx)
        else:
            periodes_ids = periodes[:]
        operator = '='
        if per_facturar:
            operator = '>'
        search_params = [
            ('comptador.id', '=', comptador_id),
            ('periode.id', 'in', periodes_ids),
            ('name', operator, from_date),
            ('tipus', '=', tipus),
        ]
        # Si el comptador té data de baixa la lectura no pot ser més gran
        # que la data de baixa
        if comptador.data_baixa:
            search_params += [('name', '<=', comptador.data_baixa)]
        # Especial per les rectificatives, li podem dir fins a quina pot
        # arribar.
        if 'fins_lectura_fact' in context:
            search_params.extend([
                ('name', '<=', context['fins_lectura_fact'])
            ])
        lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                           context={'active_test': False})
        # Si es per facturar cerquem si hi ha darreres lectures
        # per tindre-les en compte encara que hi hagin lectures posteriors
        # dins el mateix periode
        if per_facturar and not context.get('from_perfil', False):
            search_params.append(('origen_id.codi', '=', 'DR'))
            lectures_dl_ids = lectures_obj.search(cursor, uid, search_params,
                                                  context={'active_test': False}
                                                  )
            if lectures_dl_ids:
                lectures_ids = lectures_dl_ids

        # En el cas que no hi hagi lectures ho inicialitzem pel validador
        if not lectures_ids:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {}
                lectures[periode.name]['actual'] = {}
                lectures[periode.name]['anterior'] = {}
        for lectura in lectures_obj.browse(cursor, uid, lectures_ids, context):
            periode = lectura.periode.name
            # Si ja hem assignat la lectura per aqueset periode passem
            if periode in lectures:
                continue
            lectures[periode] = {}
            lectures[periode]['actual'] = lectures_obj.read(cursor, uid,
                                                            lectura.id)
            # Té el mateix siginificat que False pq evalua així, però podem
            # fet .get(key, default)
            lectures[periode]['anterior'] = {}
            # posem l'order per defecte (no facturar)
            order = "name desc"
            search_params = [
                ('comptador.id', '=', comptador_id),
                ('periode.id', '=', lectura.periode.id),
                ('name', '<', lectura.name),
                ('tipus', '=', tipus)
            ]
            # Si és per facturar mirem les lectures des d'on volem facturar
            # pel cas que tingui més d'una lectura en el mateix periode.
            # L'order = "name asc" ens assegura que vindran en el bon ordre
            if per_facturar:
                search_params.append(('name', '>=', from_date))
                order = "name asc"
            lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                               order=order,
                                               context={'active_test': False})
            if lectures_ids:
                lectures[periode]['anterior'] = lectures_obj.read(
                    cursor, uid, lectures_ids[0]
                )
                # En el cas que hi hagi més d'una lectura intermitja sumem
                # els consums
                for idx in range(1, len(lectures_ids)):
                    l_tmp = lectures_obj.browse(cursor, uid, lectures_ids[idx])
                    lectures[periode]['actual']['consum'] += l_tmp.consum
                    lectures[periode]['actual']['generacio'] += l_tmp.generacio
        return recalc_consums(lectures, comptador.giro)

    def get_lectures_month_per_facturar(self, cursor, uid, comptador_id, tarifa_id,
                                tipus='A', periodes=None, context=None):
        """Retorna les lectures pendents de facturar.
        """
        if not context:
            context = {}
        if isinstance(comptador_id, tuple) or isinstance(comptador_id, list):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context)
        cdata_alta = datetime.strptime(comptador.polissa.data_alta, "%Y-%m-%d")
        cdata_alta -= timedelta(days=1)
        cdata_alta = cdata_alta.strftime("%Y-%m-%d")
        ult_lectura_fact = max(comptador.polissa.data_ultima_lectura,
                               comptador.polissa.data_ultima_lectura_estimada,
                               cdata_alta)
        ult_lectura_fact = context.get('ult_lectura_fact', ult_lectura_fact)
        return comptador.get_lectures_month(
            tarifa_id, ult_lectura_fact, tipus, True,periodes, context
        )

    def get_lectures_month(self, cursor, uid, comptador_id, tarifa_id, from_date,
                   tipus='A', per_facturar=False, periodes=None, context=None):
        """Retorna tots els tancaments mensuals entre from_date i to_date
        """
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')

        if not context:
            context = {}
        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id)
        lectures = {}
        
        search_params = [
            ('tarifa.id', '=', tarifa_id),
            ('tipus', '=', 'te'),
        ]
        #Search periodes if not passed as a parameter
        if not periodes:
            ctx = context.copy()
            ctx.update({'active_test': False})
            periodes_ids = periodes_obj.search(cursor, uid, search_params,
                                           context=ctx)
        else:
            periodes_ids = periodes[:]
        #Define search params for lectures
        operator = '='
        if per_facturar:
            operator = '>'
        search_params = [
            ('comptador', '=', comptador_id),
            ('periode', 'in', periodes_ids),
            ('name', operator, from_date),
            ('tipus', '=', tipus),
        ]
        # Si el comptador té data de baixa la lectura no pot ser més gran
        # que la data de baixa
        if comptador.data_baixa:
            search_params += [('name', '<=', comptador.data_baixa)]
        # Especial per les rectificatives, li podem dir fins a quina pot
        # arribar.
        if 'fins_lectura_fact' in context:
            search_params.extend([
                ('name', '<=', context['fins_lectura_fact'])
            ])
        lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                           limit=1,
                                           context={'active_test': False})
        # Si es per facturar cerquem si hi ha darreres lectures
        # per tindre-les en compte encara que hi hagin lectures posteriors
        # dins el mateix periode
        if per_facturar:
            search_params.append(('origen_id.codi', '=', 'DR'))
            lectures_dl_ids = lectures_obj.search(cursor, uid, search_params,
                                                  limit=1,
                                           context={'active_test': False})
            if lectures_dl_ids:
                lectures_ids = lectures_dl_ids
        # En el cas que no hi hagi lectures ho inicialitzem pel validador
        if not lectures_ids:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {}
                lectures[periode.name]['actual'] = {}
                lectures[periode.name]['anterior'] = {}
            return [recalc_consums(lectures, comptador.giro)]
        
        to_date = lectures_obj.read(cursor, uid, lectures_ids[0],
                                    ['name'])['name']

        # Cerquem totes les dates de lectures entre from_date i to_date
        search_date = max(from_date, comptador.data_alta)
        search_params = [
            ('comptador', '=', comptador_id),
            ('periode', 'in', periodes_ids),
            ('name', '>=', search_date),
            ('name', '<=', to_date),
            ('tipus', '=', tipus),
            ]
        ctx = context.copy()
        ctx.update({'active_test': False})
        all_lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                                context=ctx)
        month_dates = sorted(list(set([x['name'] for x in
                                       lectures_obj.read(cursor, uid,
                                                         all_lectures_ids)])),
                             reverse=True)
        totes = []
        # Comencem per la primera data
        date = month_dates[0]
        while date != month_dates[-1]:
            search_params = [
                ('comptador', '=', comptador_id),
                ('periode', 'in', periodes_ids),
                ('name', '=', date),
                ('tipus', '=', tipus),
                ]
            lectures_ids = lectures_obj.search(cursor, uid, search_params,
                                               context={'active_test': False})
            lectures = {}
            for lectura in lectures_obj.browse(cursor, uid, lectures_ids, context):
                periode = lectura.periode.name
                date_lectura = lectura.name
                # Si ja hem assignat la lectura per aqueset periode passem
                if periode in lectures:
                    continue
                lectures[periode] = {}
                lectures[periode]['actual'] = lectures_obj.read(cursor, uid,
                                                                lectura.id)
                # Té el mateix siginificat que False pq evalua així, però podem
                # fet .get(key, default)
                lectures[periode]['anterior'] = {}
                # posem l'order per defecte (no facturar)
                order = "name desc"
                found = False
                while not found:
                    search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('periode.id', '=', lectura.periode.id),
                        ('name', '<', lectura.name),
                        ('tipus', '=', tipus)
                    ]
                    # Si és per facturar mirem les lectures des d'on volem
                    # facturar pel cas que tingui més d'una lectura en el
                    # mateix periode.
                    # L'order = "name asc" ens assegura que vindran en el
                    # bon ordre.
                    if per_facturar:
                        # Mirem quin es el tancament anterior
                        index = month_dates.index(date_lectura) + 1
                        date_lectura = month_dates[index]
                        search_params.append(('name', '=', date_lectura))
                        order = "name asc"
                    lectures_ant_ids = lectures_obj.search(cursor, uid,
                                               search_params,
                                               order=order,
                                               context={'active_test': False})
                    if lectures_ant_ids or index == len(month_dates):
                        found = True
                if lectures_ant_ids:
                    lectures[periode]['anterior'] = lectures_obj.read(cursor,
                                                        uid,
                                                        lectures_ant_ids[0])
            date = date_lectura
            totes.append(recalc_consums(lectures, comptador.giro))
        # Check if we are returning something
        if not totes:
            for periode in periodes_obj.browse(cursor, uid, periodes_ids):
                lectures[periode.name] = {}
                lectures[periode.name]['actual'] = {}
                lectures[periode.name]['anterior'] = {}
            return [recalc_consums(lectures, comptador.giro)]
        return totes

    def get_by_type(self, cursor, uid, start, end, agree_tipus, context=None):
        """Get the meters by type in a range of dates
        """
        sql_file = get_module_resource(
            'giscedata_lectures', 'sql', 'meters_by_type.sql'
        )
        with open(sql_file, 'r') as f:
            sql = f.read()

        cursor.execute(sql, {
            'start': start, 'end': end, 'agree_tipus': agree_tipus
        })
        return [x[0] for x in cursor.fetchall()]

    def are_dates_correct(self, cursor, uid, compt_id, start, end,
                          context=None):
        if context is None:
            context = {}

        if start > end:
            # If we start after the end it will be incorrect
            return False

        compt_vals = self.read(
            cursor, uid, compt_id, ['data_alta', 'data_baixa']
        )

        if compt_vals.get('data_alta'):
            alta_compt = datetime.strptime(compt_vals['data_alta'], "%Y-%m-%d")
            alta_compt -= timedelta(days=1)
            alta_compt = alta_compt.strftime("%Y-%m-%d")
            if start < alta_compt:
                return False

        if compt_vals.get('data_baixa') and compt_vals['data_baixa'] < end:
            return False

        return True

    def _default_polissa(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('polissa_id', False)

    def search_by_name(self, cursor, uid, meter_name, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})
        meter_ids = self.search(
            cursor, uid, [('name', '=', meter_name)], context=ctx
        )
        if meter_ids:
            return meter_ids

        # If we didn't find a counter it may be because one of the names has
        # more or less '0' at the start.
        # For example, meters '0000123' and '123' should be considered the same
        table_name = self._name.replace('.', '_')
        meter_regex = '0+' + meter_name.lstrip('0') + '$'
        sql = "SELECT id FROM {0} WHERE name ~ '{1}'".format(
            table_name, meter_regex
        )

        cursor.execute(sql)
        meter_search = cursor.fetchall()
        return [meter[0] for meter in meter_search]

    def search_with_contract(self, cursor, uid, meter_name, polissa,
                             context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx.update({'active_test': False})

        polissa_obj = self.pool.get('giscedata.polissa')
        distri_obj = self.pool.get('res.partner')

        distri_id = polissa_obj.read(cursor, uid, polissa, ['distribuidora'])
        if distri_id and distri_id['distribuidora']:
            distri_id = distri_id['distribuidora'][0]

        distri_ref = distri_obj.read(cursor, uid, distri_id, ['ref'])['ref']

        if distri_ref == '0022':
            numeric_name = ''.join(c for c in meter_name if c.isdigit())
            numeric_name = str(int(numeric_name))
            search_params = [('name', 'like', numeric_name), ('polissa', '=', polissa), ('active', '=', 1)]
            meter_ids = self.search(
                cursor, uid, search_params, order='data_alta desc', limit=1, context=ctx
            )
        else:
            search_params = [('name', '=ilike', meter_name), ('polissa', '=', polissa)]
            meter_ids = self.search(
                cursor, uid, search_params, context=ctx
            )

        if meter_ids:
            return meter_ids

        # If we didn't find a counter it may be because one of the names has
        # more or less '0' at the start.
        # For example, meters '0000123' and '123' should be considered the same
        table_name = self._name.replace('.', '_')
        meter_regex = '0*' + meter_name.lstrip('0')
        sql = "SELECT id FROM {0} WHERE name ~ '{1}' AND polissa = {2}".format(
            table_name, meter_regex, polissa
        )

        cursor.execute(sql)
        meter_search = cursor.fetchall()
        return [meter[0] for meter in meter_search]

    def create(self, cursor, uid, vals, context=None):
        if 'name' in vals:
            vals['name'] = vals['name'].strip()
        res_id = super(GiscedataLecturesComptador,
                       self).create(cursor, uid, vals, context)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        if 'name' in vals:
            vals['name'] = vals['name'].strip()
        return super(GiscedataLecturesComptador,
                     self).write(cursor, uid, ids, vals, context)

    def unlink(self, cursor, uid, ids, context=None):
        '''if a meter has reads, check if we can unlink it
        It depends on the config variable unlink_meter_with_reads'''

        config = self.pool.get('res.config')
        unlink_meter = int(config.get(cursor, uid,
                           'unlink_meter_with_reads', '0'))
        for comptador in self.browse(cursor, uid, ids):
            if not unlink_meter and comptador.lectures:
                raise osv.except_osv('Error',
                                     _(u"No es poden esborrar comptadors "
                                       u"amb lectures d'energia"))
            if not unlink_meter and comptador.lectures_pot:
                raise osv.except_osv('Error',
                                     _(u"No es poden esborrar comptadors "
                                       u"amb lectures de potència"))

        return super(GiscedataLecturesComptador,
                     self).unlink(cursor, uid, ids, context=context)

    def renombrar(self, cursor, uid, comptador_id, new_name, context=None):
        if not context:
            context = {}

        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]
        comptador = self.browse(cursor, uid, comptador_id, context=context)
        vals = {'name': new_name}
        if 'serial' in self.fields_get_keys(cursor, uid):
            stock_lot_obj = self.pool.get('stock.production.lot')
            search_params = [('name', '=', new_name)]
            lot_id = stock_lot_obj.search(cursor, uid, search_params)
            if lot_id:
                vals.update({'serial': lot_id[0]})
            else:
                vals.update({'serial': None})

        comptador.write(vals, context=context)
        return True

    def get_meter_on_date(self, cursor, uid, serial, date_read, context=None):
        '''returns id of the meter with serial'''

        if not context:
            context = {}

        # first of all, search only active meters in date_end
        # We can only find as much as one, because there is a constraint
        # that does not allow two meters with same serial being active
        search_params = [
            ('name', '=', serial),
            ('polissa.state', 'not in', CONTRACT_IGNORED_STATES),
            ('data_alta', '<=', date_read),
            # Avoids meters registered prior to policy register date.
            ('polissa.data_alta', '<=', date_read)
        ]
        from osv.expression import OOQuery
        q = OOQuery(self, cursor, uid)
        sql = q.select(['id']).where(search_params)
        cursor.execute(*sql)
        for res in cursor.fetchall():
            return res[0]

        # Search no active meters if no results.
        context.update({'active_test': False})
        # Update search params
        search_params.extend([('data_baixa', '>=', date_read),
                              ('data_alta', '<=', date_read)])
        q = OOQuery(self, cursor, uid)
        sql = q.select(['id'], limit=1, only_active=False).where(search_params)
        cursor.execute(*sql)
        for res in cursor.fetchall():
            return res[0]

        if context.get('validate', False):
            # Allow one day before data_alta if no results yet.
            context.update({'active_test': True})
            # Update search params
            try:
                day_before_read = datetime.strptime(
                    date_read, '%Y-%m-%d %H:%M:%S') + relativedelta(days=1)
            except ValueError as e:
                day_before_read = datetime.strptime(
                    date_read, '%Y-%m-%d') + relativedelta(days=1)
            except TypeError as e:
                day_before_read = date_read
            search_params = [
                ('name', '=', serial),
                ('polissa.state', 'not in', CONTRACT_IGNORED_STATES),
                ('data_alta', '<=', day_before_read),
                # Avoids meters registered prior to policy register date.
                ('polissa.data_alta', '<=', day_before_read)
            ]
            q = OOQuery(self, cursor, uid)
            sql = q.select(['id'], limit=1, only_active=False).where(
                search_params)
            cursor.execute(*sql)
            for res in cursor.fetchall():
                return res[0]

        return False

    _columns = {
        'name': fields.char('Nº de sèrie', size=32, required=True),
        'lloguer': fields.boolean('Lloguer'),
        'polissa': fields.many2one('giscedata.polissa', 'Polissa', required=True,
                                   select="1", ondelete='cascade'),
        'lectures': fields.one2many('giscedata.lectures.lectura', 'comptador',
                                    'Lectures'),
        'lectures_pot': fields.one2many('giscedata.lectures.potencia',
                                        'comptador', 'Lectures Potència'),
        'giro': fields.integer('Gir'),
        'active': fields.boolean('Actiu'),
        'data_alta': fields.date('Data Alta', select="1"),
        'data_baixa': fields.date('Data Baixa', select="1"),
        'propietat': fields.selection(PROPIETAT_COMPTADOR_SELECTION,
                                      "Propietat de"),
        'meter_type': fields.selection(_METER_TYPE, 'Tipus de comptador',
                                       required=True),
        'policy_tariff': fields.related(
            'polissa',
            'tarifa',
            readonly=True,
            type='many2one',
            relation="giscedata.polissa.tarifa",
            string='Tarifa Contracte'
        ),
        'policy_owner': fields.related(
            'polissa',
            'titular',
            readonly=True,
            type='many2one',
            relation="res.partner",
            string='Titular Contracte'
        ),
    }

    _defaults = {
      'lloguer': lambda *a: 0,
      'giro': lambda *a: 0,
      'active': lambda *a: 1,
      'data_alta': lambda *a: time.strftime('%Y-%m-%d'),
      'propietat': lambda *a: 'empresa',
      'polissa': _default_polissa,
      'meter_type': lambda *a: u'PF',
    }

    _order = 'data_alta desc'

    _sql_constraints = [
        ('data_baixa_alta', 'CHECK (data_baixa >= data_alta)',
         _("La data de baixa del comptador no pot ser menor a la data d'alta")
         ),
    ]

    def _check_comptador_actiu_unic(self, cursor, uid, ids):
        '''No mes pot haver-hi un comptador actiu
        amb el mateix numero de serie en polisses actives'''
        q = OOQuery(self, cursor, uid)
        sql = q.select(['name', 'meter_type', 'polissa.distribuidora']).where(
            [('id', 'in', ids)]
        )
        cursor.execute(*sql)
        for comptador in cursor.dictfetchall():
            search_params = [
                ('name', '=', comptador['name']),
                ('polissa.state', '!=', 'esborrany'),
                ('active', '=', True),
                ('polissa.distribuidora', '=', comptador['polissa.distribuidora']),
                ('meter_type', '=', comptador['meter_type'])
            ]
            q = OOQuery(self, cursor, uid)
            sql = q.select(['id']).where(
                search_params
            )
            cursor.execute(*sql)
            if cursor.rowcount > 1:
                return False
        return True

    def _check_lloguer_required(self, cursor, uid, ids):
        '''Si el comptador es d'empresa ha de tindre lloguer'''
        cfg_obj = self.pool.get('res.config')
        if not int(cfg_obj.get(cursor, uid, 'lloguer_required', 0)):
            return True
        for comptador in self.browse(cursor, uid, ids):
            if (comptador.propietat == 'empresa'
                and not comptador.lloguer):
                return False
        return True

    _constraints = [
        (_check_comptador_actiu_unic,
         _(u'Error: Ja existeix un comptador actiu amb aquest '
           u'numero de serie'),
         ['name']),
        (_check_lloguer_required,
         _(u"Un comptador d'empresa ha de tindre un lloguer associat"),
         ['propietat', 'lloguer'])
                    
    ]

GiscedataLecturesComptador()


class GiscedataLecturesOrigen(osv.osv):
    """Orígens de lectures
    """

    _name = 'giscedata.lectures.origen'

    _columns = {
        'name': fields.char('Nom', size=64, required=True, translate=True),
        'codi': fields.char('Codi', size=2, required=True),
        'subcodi': fields.char('Subcodi', size=2, required=True),
        'active': fields.boolean('Actiu', readonly=True),
    }

    _order = "codi asc, subcodi asc"

    _defaults = {
        'subcodi': lambda *a: '00',
        'active': lambda *a: True,
    }

GiscedataLecturesOrigen()


class GiscedataLecturesLectura(osv.osv):
    """Lectures d'energia
    """

    _name = 'giscedata.lectures.lectura'

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        force_trigger = context.get('force_trigger', False)
        if ('lectura' in vals
            or 'ajust' in vals
            or 'lectura_exporta' in vals
            or 'ajust_exporta' in vals) and not force_trigger:

            fields_to_check = ['lectura', 'ajust', 'lectura_exporta',
                               'ajust_exporta']
            trigger = {True: [], False: []}
            # tests all "lectures"
            for lec in self.read(cursor, uid, ids, fields_to_check):
                # for each field, test if is equals to
                # tests checked fields for the "lectura"
                for field in fields_to_check:
                    if field in vals and lec[field] != vals[field]:
                        trigger[True].append(lec['id'])
                        break
                    else:
                        trigger[False].append(lec['id'])

            # We write only to the triggered ids
            for trg, ids_trg in trigger.items():
                if not ids_trg:
                    continue
                context['trigger'] = trg
                res = super(GiscedataLecturesLectura,
                            self).write(cursor, uid, ids_trg, vals, context)
        else:
            res = super(GiscedataLecturesLectura,
                        self).write(cursor, uid, ids, vals, context)
        return res

    def create(self, cursor, uid, vals, context=None):
        """Sobreescrivim el create per cridar el trigger.
        """
        res_id = super(GiscedataLecturesLectura, self).create(cursor, uid,
                                                              vals, context)
        if 'lectura' in vals:
            consum = self._consum(
                cursor, uid, [res_id], 'consum', None,context
            )[res_id]
            self.write(cursor, uid, [res_id], {'consum': consum}, context)
        if 'lectura_exporta' in vals:
            generacio = self._generacio(
                cursor, uid, [res_id], 'generacio', None, context
            )[res_id]
            self.write(cursor, uid, [res_id], {'generacio': generacio}, context)
        return res_id

    def create_lectura_sortint(self, cursor, uid, lect_vals, context=None):
        """
        Crea una lectura nova amb els valors de lectura_exporta i ajust_exporta" i calcula el valor de 'lectura' i
        'ajust' a partir de la ultima lectura existent.

        Si ens arriven "lectura" i "ajust" pero cap  "lectura_exporta" i "ajust_exporta" es presuposa que
        aquestes dades de lectura i ajust son de ecxportacio.
        """
        if context is None:
            context = {}

        new_vals = lect_vals.copy()

        if "lectura" in new_vals and "lectura_exporta" not in new_vals:
            new_vals['lectura_exporta'] = new_vals.get("lectura")
            del new_vals['lectura']
        if "ajust" in new_vals and "ajust_exporta" not in new_vals:
            new_vals['ajust_exporta'] = new_vals.get("ajust")
            del new_vals['ajust']

        if "lectura" not in new_vals:
            # Busquem la lectura anterior per posar el seu valor com a lectura.
            search_params = [
                ('comptador.id', '=', new_vals['comptador']),
                ('name', '<', new_vals['name']),
                ('periode.id', '=', new_vals['periode']),
                ('tipus', '=', new_vals['tipus']),
            ]
            lectura_ids = self.search(cursor, uid, search_params, limit=1, order='name desc', context=context)
            if len(lectura_ids):
                previous_vals = self.read(cursor, uid, lectura_ids[0], ['lectura', 'ajust', 'motiu_ajust'])
                if previous_vals['ajust'] != 0:
                    new_vals['ajust'] = previous_vals['ajust']
                    new_vals['motiu_ajust'] = previous_vals['motiu_ajust']
                new_vals['lectura'] = previous_vals['lectura']
            else:
                new_vals['lectura'] = 0.0

        return self.create(cursor, uid, new_vals, context=context)

    def afegir_lectura_sortint_a_lectura(self, cursor, uid, lect_id, lect_vals, context=None):
        """
        Modifica una lectura existent.

        Si ens arriven "lectura" i "ajust" pero cap  "lectura_exporta" i "ajust_exporta" es presuposa que
        aquestes dades de lectura i ajust son de ecxportacio.
        """
        if context is None:
            context = {}

        new_vals = lect_vals.copy()

        if "lectura" in new_vals and "lectura_exporta" not in new_vals:
            new_vals['lectura_exporta'] = new_vals.get("lectura")
            del new_vals['lectura']
        if "ajust" in new_vals and "ajust_exporta" not in new_vals:
            new_vals['ajust_exporta'] = new_vals.get("ajust")
            del new_vals['ajust']

        return self.write(cursor, uid, lect_id, new_vals, context=context)

    def unlink(self, cursor, uid, ids, context=None):
        """Sobreescrivim el unlink per cridar el trigger del consum.

        Cal buscar la lectura posterior a la què s'esborra i cridar el trigger
        del consum per a aquesta lectura trobada.
        """
        if context is None:
            context = {}
        lect_recalc = []
        # busquem les lectures a recalcular
        for lectura in self.browse(cursor, uid, ids, context):
            search_params = [
                ('comptador.id', '=', lectura.comptador.id),
                ('name', '>', lectura.name),
                ('periode.id', '=', lectura.periode.id),
                ('tipus', '=', lectura.tipus),
            ]
            lectura_ids = self.search(cursor, uid, search_params, limit=1,
                                      order='name asc', context=context)
            if lectura_ids:
                lect_recalc.append(lectura_ids[0])
        # esborrem les lectures
        super(GiscedataLecturesLectura, self).unlink(cursor, uid, ids, context)
        # recalculem
        ctx = context.copy()
        ctx.update({'force_trigger': True})
        if 'trigger' in ctx:
            del ctx['trigger']
        for lectura_post in self.read(cursor, uid, lect_recalc,
                                      ['lectura', 'lectura_exporta'], context):
            self.write(cursor, uid, [lectura_post['id']],
                       {'lectura': lectura_post['lectura'],
                        'lectura_exporta': lectura_post['lectura_exporta']},
                       context=ctx)

    def _trg_consum(self, cursor, uid, ids, context=None):
        """Trigger que es cridarà per tal de tornar a calcular els consums.
        """
        if not context:
            context = {}
        if not context.get('trigger', True):
            return []
        context.update({'active_test': False})
        ids_to_call = []
        for lectura in self.browse(cursor, uid, ids, context):
            search_params = [
                ('comptador.id', '=', lectura.comptador.id),
                ('name', '>=', lectura.name),
                ('periode.id', '=', lectura.periode.id),
                ('tipus', '=', lectura.tipus)
            ]
            found_ids = self.search(cursor, uid, search_params,
                                    context=context)
            ids_to_call.extend(found_ids)
        # Uniq ids
        res = {}
        for fid in ids_to_call:
            res[fid] = 1
        return res.keys()

    def _consum(self, cursor, uid, ids, field_name, arg, context=None):
        """Calcula el consum i el guardarà a la base de dades.
        """
        res = {}
        for lectura in self.browse(cursor, uid, ids, context):
            from_date = lectura.name
            tipus = lectura.tipus
            tarifa_id = lectura.periode.tarifa.id
            periodes = [lectura.periode.id]
            consum_periode = lectura.comptador.get_consum(tarifa_id, from_date,
                                                          tipus,
                                                          periodes=periodes,
                                                          context=context)
            res[lectura.id] = consum_periode[lectura.periode.name]
        return res

    def _generacio(self, cursor, uid, ids, field_name, arg, context=None):
        """Calcula la generacio i la guardarà a la base de dades.
        """
        res = {}
        for lectura in self.browse(cursor, uid, ids, context):
            from_date = lectura.name
            tipus = lectura.tipus
            tarifa_id = lectura.periode.tarifa.id
            periodes = [lectura.periode.id]
            generacio_periode = lectura.comptador.get_generacio(
                tarifa_id, from_date, tipus, periodes=periodes, context=context
            )
            res[lectura.id] = generacio_periode[lectura.periode.name]
        return res

    def _auto_init(self, cr, context={}):
        if context is None:
            context = {}
        res = super(GiscedataLecturesLectura, self)._auto_init(cr, context)
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscedata_lectures_cnt'")
        if not cr.fetchone():
            cr.execute('CREATE INDEX idx_giscedata_lectures_cnt '
                       'ON giscedata_lectures_lectura (comptador, name, tipus)')
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscedata_lectures_lectura_ntpc'")
        if not cr.fetchone():
            cr.execute(
                'CREATE INDEX idx_giscedata_lectures_lectura_ntpc '
                'ON giscedata_lectures_lectura (name,tipus,periode,comptador)'
            )
        return res

    _columns = {
        'name': fields.date('Data', required=True, select=True),
        'periode': fields.many2one(
            'giscedata.polissa.tarifa.periodes', 'Periode', required=True
        ),
        'lectura': fields.integer('Lectura', required=True, select=True),
        'consum': fields.function(
            _consum, method=True, string="Consum", type="float", digits=(16, 3),
            store={
                'giscedata.lectures.lectura': (
                    _trg_consum, ['lectura', 'ajust'], 10
                )
            }
        ),
        'tipus': fields.selection(
            [('A', 'Activa'), ('R', 'Reactiva')],
            'Tipus', required=True, select=1
        ),
        'comptador': fields.many2one(
            'giscedata.lectures.comptador', 'Comptador',
            required=True, ondelete='cascade'
        ),
        'incidencia_id': fields.many2one(
            'giscedata.lectures.incidencies.incidencia', 'Incidència',
            ondelete='restrict'
        ),
        'observacions': fields.text('Observacions'),
        'origen_id': fields.many2one(
            'giscedata.lectures.origen', 'Orígen', required=True
        ),
        'motiu_ajust': fields.selection(MOTIUS_AJUST, 'Motiu Ajust'),
        'ajust': fields.float('Ajust', digits=(16, 3)),
        # Lectures generació/exportació
        'lectura_exporta': fields.integer('Lectura Exporta', required=True, select=True),
        'ajust_exporta': fields.float('Ajust Exporta', digits=(16, 3)),
        'generacio': fields.function(
            _generacio, method=True, string="Generació", type="float", digits=(16, 3),
            store={
                'giscedata.lectures.lectura': (
                    _trg_consum,
                    ['lectura_exporta', 'ajust_exporta'], 10
                )
            }
        ),
    }

    def _default_comptador(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('comptador_id', False)

    _defaults = {
      'name': lambda *a: time.strftime('%Y-%m-%d'),
      'comptador': _default_comptador,
      'lectura_exporta': lambda *a: 0,
      'ajust': lambda *a: 0,
      'ajust_exporta': lambda *a: 0,
    }

    _order = "name desc, periode asc, tipus asc"

    _sql_constraints = [('consum_positiu', 'CHECK (consum >= 0)',
                         _(u'El consum ha de ser positiu.')),
                        ('generacio_positiva', 'CHECK (generacio>= 0)',
                         _(u'El consum ha de ser positiu.')),
                        ('lectures_duplicades',
                         'UNIQUE(name, comptador, periode, tipus)',
                         _(u"No es pot entrar més d'una lectura per data, "
                           u"comptador, període i tipus"))]

    def _check_datalectura(self, cursor, uid, ids):
        '''No es poden introduir lectures posteriors a la data
        de baixa de un comptador ni anteriors a la data d'alta-1'''
        cfg_obj = self.pool.get('res.config')
        for lectura in self.browse(cursor, uid, ids):
            if not lectura.comptador.active and\
               lectura.name > lectura.comptador.data_baixa:
                return False
            data_tall = cfg_obj.get(cursor, uid, 'data_lectura_constraint',
                                    '2013-01-01')
            if lectura.comptador.data_alta:
                alta_compt = datetime.strptime(lectura.comptador.data_alta, "%Y-%m-%d")
                alta_compt -= timedelta(days=1)
                alta_compt = alta_compt.strftime("%Y-%m-%d")
                if lectura.name >= data_tall and lectura.name < alta_compt:
                    return False
        return True

    _constraints = [
        (_check_datalectura,
         _(u'Error: No es poden introduir lectures posteriors a '
           u'la data de baixa del comptador o dates anteriors al dia anterior a'
           u' la data d\'alta del comptador'),
         ['name']),
    ]

GiscedataLecturesLectura()


class GiscedataLecturesPotencia(osv.osv):
    """Lectures de potència.
    """

    _name = 'giscedata.lectures.potencia'

    _columns = {
        'name': fields.date('Data', required=True, select=True),
        'periode': fields.many2one(
            'giscedata.polissa.tarifa.periodes', 'Periode', required=True
        ),
        'lectura': fields.float('Lectura', digits=(16, 3)),
        'comptador': fields.many2one(
            'giscedata.lectures.comptador', 'Comptador', ondelete='cascade'
        ),
        'incidencia_id': fields.many2one(
            'giscedata.lectures.incidencies.incidencia', 'Incidència',
            ondelete='restrict'
        ),
        'exces': fields.float('Excés', digits=(16, 3)),
        'observacions': fields.text('Observacions'),
    }

    def _auto_init(self, cr, context={}):
        if context is None:
            context = {}
        res = super(GiscedataLecturesPotencia, self)._auto_init(cr, context)
        cr.execute(
            "SELECT indexname "
            "FROM pg_indexes "
            "WHERE indexname = 'idx_giscedata_lectures_potencia_nc'")
        if not cr.fetchone():
            cr.execute('CREATE INDEX idx_giscedata_lectures_potencia_nc '
                       'ON giscedata_lectures_potencia (name,comptador)')
        return res

    def _default_comptador(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('comptador_id', False)

    _defaults = {
      'name': lambda *a: time.strftime('%Y-%m-%d'),
      'comptador': _default_comptador,
    }

    _order = "name desc"

    def _check_datalectura(self, cursor, uid, ids):
        '''No es poden introduir lectures posteriors a la data
        de baixa de un comptador'''
        for lectura in self.browse(cursor, uid, ids):
            if not lectura.comptador.active and\
               lectura.name > lectura.comptador.data_baixa:
                return False
        return True

    _constraints = [
        (_check_datalectura,
         _(u'Error: No es poden introduir lectures posteriors a la '
           u'data de baixa del comptador'),
         ['comptador']),
    ]

GiscedataLecturesPotencia()
