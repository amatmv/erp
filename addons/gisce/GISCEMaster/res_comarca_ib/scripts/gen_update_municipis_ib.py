#!/usr/bin/env python
# -*- coding: utf-8 -*-

import optparse
import csv
import sys

parser = optparse.OptionParser(version="[GISCE ERP] Migrations")

group = optparse.OptionGroup(parser, "File Related")
group.add_option("-f", "--csv", dest="csv_file", help="specify the cvs file")
group.add_option("--delimiter", dest="delimiter", help="specify the delimiter for cvs file")
parser.add_option_group(group)

options = optparse.Values()
options.delimiter = ','
parser.parse_args(values=options)

csv_file = options.csv_file
delimiter = options.delimiter

CONV_ID_CODI = {
    'ib_00': '070000', # Mallorca
    'ib_01': '077000', # Menorca
    'ib_02': '078000', # Pitiüses (Eivissa i Formentera)
 }

xml = xml = """      <record model="res.municipi" id="base_extended.%s">
          <field search="[('codi','=','%s')]" model='res.comarca' name='comarca'/>
      </record>
"""

sys.stdout.write("""<?xml version="1.0" encoding="UTF-8"?>
<openerp>
    <data>
""")
reader = csv.reader(open(csv_file, "r"), delimiter=delimiter)
lines = []
for row in reader:
    lines.append(row)

for row in lines:
    municipi_id = row[3]
    comarca_id = CONV_ID_CODI[row[1]]

    sys.stdout.write(xml % (municipi_id, comarca_id))
sys.stdout.write("""    </data>
</openerp>
""")
sys.stdout.flush()
