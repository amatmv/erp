# -*- coding: utf-8 -*-
{
    "name": "GISCE Comarques Illes Balears",
    "description": """
Comarcalitzem les illes Balears com si cada illa fos una comarca. Així podem
categoritzar els municipis i les entitats relacionades per illes, com si fossin
comarques
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "res_comarca_ib_data.xml",
        "res_municipi_ib_data.xml"
    ],
    "active": False,
    "installable": True
}
