# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCts(osv.osv):
    """Afegim el camp de manteniments als CTS.
    """
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
        'manteniments': fields.one2many('giscedata.cts.manteniment', 'name',
                                        'Manteniments'),
    }

GiscedataCts()
