# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCtsManteniment(osv.osv):
    """Manteniment intern de CTS.
    """
    _name = 'giscedata.cts.manteniment'
    _description = __doc__

    def _fnct_ct_descripcio(self, cursor, uid, ids, field_name, args,
                            context=None):
        res = {}
        for manteniment in self.browse(cursor, uid, ids, context):
            res[manteniment.id] = manteniment.name.descripcio
        return res

    _columns = {
        'name': fields.many2one('giscedata.cts', 'CT', required=True,
                                ondelete='restrict'),
        'ct_descripcio': fields.function(_fnct_ct_descripcio, type='char',
                                         method=True, size=60,
                                         string="CT Descripció"),
        'descripcio': fields.char('Descripció', size=256, required=True),
        'data_desde': fields.date('Data desde'),
        'data_fins': fields.date('Data fins'),
        'treballs': fields.one2many('giscedata.cts.manteniment.treball',
                                    'manteniment', 'Treballs'),
        'observacions': fields.text('Observacions')
    }

GiscedataCtsManteniment()


class GiscedataCtsMantenimentTreball(osv.osv):
    """Treball dins un manteniment de CT.
    """
    _name = 'giscedata.cts.manteniment.treball'
    _description = __doc__

    def _fnct_imatge_book(self, cursor, uid, ids, field_name, args,
                          context=None):
        res = {}
        for treball in self.read(cursor, uid, ids, ['imatge']):
            res[treball['id']] = treball['imatge'] and 1 or 0
        return res

    _columns = {
        'name': fields.char(u'Descripció', size=256, required=True),
        'manteniment': fields.many2one('giscedata.cts.manteniment',
                                       'Manteniment', required=True,
                                       ondelete='cascade'),
        'situacio': fields.char(u'Situació', size=64, required=True),
        'responsable': fields.many2one('res.users', 'Responsable',
                                       required=True),
        'data_realitzat': fields.date('Data realització'),
        'verificat': fields.boolean('Verificat'),
        'imatge': fields.binary('Imatge'),
        'imatge_bool': fields.function(_fnct_imatge_book, type='boolean',
                                       method=True, string="Imatge"),
        'observacions': fields.text('Observacions')
    }

    _defaults = {
        'verificat': lambda *a: 0
    }

GiscedataCtsMantenimentTreball()
