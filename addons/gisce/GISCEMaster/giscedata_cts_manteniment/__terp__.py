# -*- coding: utf-8 -*-
{
    "name": "Manteniment Intern CTS",
    "description": """
    This module provide :
      * Manteniment intern dels CTS
    """,
    "version": "0-dev",
    "author": "GISCe",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cts"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cts_manteniment_view.xml",
        "giscedata_cts_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
