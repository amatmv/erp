# -*- coding: utf-8 -*-

from osv import osv


class WizardSwitchingMultiClose(osv.osv_memory):
    _name = 'wizard.switching.multi.close'
    _inherit = 'wizard.crm.multi.close'

    def _default_atr_output_text(self, cursor, uid, context=None):
        if not context:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')

        sw_ids = context.get('active_ids', [])
        case_ids = [x['case_id'][0] for x in sw_obj.read(cursor, uid, sw_ids)]
        ctx = context.copy()
        ctx.update({
            'active_ids': case_ids
        })

        return self._default_output_text(cursor, uid, context=ctx)

    def perform_atr_close(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')

        sw_ids = context.get('active_ids', [])
        case_ids = [x['case_id'][0] for x in sw_obj.read(cursor, uid, sw_ids)]

        ctx = context.copy()
        ctx.update({
            'active_ids': case_ids
        })
        self.perform_close(cursor, uid, ids, context=ctx)
        return {}

    _defaults = {
        'result': _default_atr_output_text
    }


WizardSwitchingMultiClose()
