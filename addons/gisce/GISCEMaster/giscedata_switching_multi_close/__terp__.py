# -*- coding: utf-8 -*-
{
    "name": "Switching Multi Close",
    "description": """Permet tancar casos en massa""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CRM",
    "depends":[
        "crm_multi_close",
        "giscedata_switching"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_switching_multi_close_view.xml",
        "wizard/wizard_switching_multi_change_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
