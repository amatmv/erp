# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients Municpi",
    "description": """Afegeix el camp 'municipi' a un expedient per tal de poder filtrar els expedients per municipi""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended",
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_municipi_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
