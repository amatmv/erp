# -*- coding: utf-8 -*-
{
    "name": "Suministros no cortables",
    "description": """Módulo para gestionar el ciclo de impagos para 
    suministros no cortables y la integración con SICER y correos
    Art.52 P.4: https://www.boe.es/boe/dias/2013/12/27/pdfs/BOE-A-2013-13645.pdf
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        'giscedata_correos_base_flux',
        'giscedata_facturacio_impagat_comer_nocutoff'
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_correos_suministros_nocutoff_data.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}