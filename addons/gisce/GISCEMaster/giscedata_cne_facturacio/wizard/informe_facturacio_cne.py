# -*- coding: utf-8 -*-

import wizard
import pooler
import csv
import StringIO

def _escollir_any(self, cr, uid, data, context=None):
  return {}

_escollir_any_form = """<?xml version="1.0"?>
<form string="Generar Informe Facturación CNE" col="2">
  <field name="year" />
</form>"""

_escollir_any_fields = {
    'year': {'string': 'Año de liquidación', 'type': 'many2one', 'relation': 'giscedata.facturacio.liquidacio.year', 'required': True}
}

def _check_exists(self, cr, uid, data, context=None):
  informe_obj = pooler.get_pool(cr.dbname).get('giscedata.cne.facturacio')
  form = data['form']
  informe_ids = informe_obj.search(cr, uid, [('liquidacio_year_id.id', '=', form['year'])])
  if informe_ids:
    data['del_informes'] = informe_ids
    return 'ask_overwrite'
  else:
    return 'generar_informe'
  
_ask_overwrite_form = """<?xml version="1.0"?>
<form string="Generar Informe Facturación CNE" col="2">
  <label colspan="4" string="Ya existe un informe para el año seleccionado. Desea reemplazarlo?" />
</form>"""

_ask_overwrite_fields = {}


def _generar_informe(self, cr, uid, data, context=None):
  form = data['form']
  liquidacio_year_obj = pooler.get_pool(cr.dbname).get('giscedata.facturacio.liquidacio.year')
  informe_obj = pooler.get_pool(cr.dbname).get('giscedata.cne.facturacio')
  informe_linia_obj = pooler.get_pool(cr.dbname).get('giscedata.cne.facturacio.linia')
  factura_obj = pooler.get_pool(cr.dbname).get('giscedata.factura')
  # 1. Eliminem qualsevol informe que ja existeix per l'any seleccionat
  if data.get('del_informes', False):
    informe_obj.unlink(cr, uid, data['del_informes'])
  liquidacio_year = liquidacio_year_obj.browse(cr, uid, form['year'])
  # Per any busquem totes les factures del periode i les processem

  results = {}
  for periode in liquidacio_year.period_ids:
    # Busquem totes les factures associades a aquest periode
    factura_ids = factura_obj.search(cr, uid, [('periode.id', '=', periode.id)])
    if not results.has_key(periode.id):
      results[periode.id] = {}
    for factura in factura_obj.browse(cr, uid, factura_ids):
      if not results[factura.periode.id].has_key(factura.comercialitzadora.id):
        results[factura.periode.id][factura.comercialitzadora.id] = {'energia': 0, 'euros': 0.00}
      for linia in factura.linies:
        # Calculem els Euros
        if linia.tipus not in ('lloguer', 'iee'):
          results[factura.periode.id][factura.comercialitzadora.id]['euros'] += linia.subtotal
        # Calculem l'energia
        if linia.tipus == 'energia':
          results[factura.periode.id][factura.comercialitzadora.id]['energia'] += int(round(linia.quantitat)) # Arrodonim a enters per casos de medida en baixa
  # 2.Processem els resultats emmagatzemats
  # 2.1 Creem el nou informe
  vals = {'name': liquidacio_year.name, 'liquidacio_year_id': liquidacio_year.id}
  data['informe_id'] = informe_obj.create(cr, uid, vals)
  # 2.2 Creem les línies
  for k,v in results.items():
    for k2,v2 in v.items():
      linia_vals = {
        'facturacio_cne_id': data['informe_id'],
        'comercialitzadora_id': k2,
        'periode_id': k,
        'energia': v2['energia'],
        'euros': v2['euros']
      }
      informe_linia_obj.create(cr, uid, linia_vals)
  return {}

def _print(self, cr, uid, data, context=None):
  return {'ids': [data.get('informe_id', data.get('del_informes', 0))]}

def _mostrar_tab(self, cr, uid, data, context=None):
  action = {
    'domain': "[('id','=', %i)]" % data['informe_id'],
    'view_type': 'form',
    'view_mode': 'tree,form',
    'res_model': 'giscedata.cne.facturacio',
    'view_id': False,
    'type': 'ir.actions.act_window'
  }
  return action
  
class informe_facturacio_cne(wizard.interface):

  states = {
      'init': {
          'actions': [],
          'result': {'type': 'state', 'state': 'escollir_any'}
      },
      'escollir_any': {
          'actions': [_escollir_any],
          'result': {'type': 'form', 'arch': _escollir_any_form, 'fields': _escollir_any_fields, 'state': [('check_exists', 'Siguiente'), ('end', 'Cancelar')]}
      },
      'check_exists': {
          'actions': [],
          'result': {'type': 'choice', 'next_state': _check_exists}
      },
      'ask_overwrite': {
          'actions': [],
          'result': {'type': 'form', 'arch': _ask_overwrite_form, 'fields': _ask_overwrite_fields, 'state': [('generar_informe', 'Sí'), ('print', 'No')]}
      },
      'generar_informe': {
          'actions': [_generar_informe],
          'result': {'type': 'state', 'state': 'print'}
      },
      'print': {
          'actions': [_print],
          'result': {'type': 'print', 'report': 'giscedata.cne.facturacio.informe', 'get_id_from_action':True, 'state':'end'}
      },
      'end': {
          'actions': [],
          'result': {'type': 'state', 'state': 'end'}
      }
  }
  
informe_facturacio_cne('giscedata.cne.facturacio.informe')