# -*- coding: utf-8 -*-
from datetime import datetime

from dateutil.relativedelta import relativedelta

from osv import osv, fields


class giscedata_facturacio_liquidacio_year(osv.osv):
    _name = "giscedata.facturacio.liquidacio.year"
    _description = "Año de liquidación de Energía"
    _columns = {
      'name': fields.char('Año liquidacion', size=64, required=True),
      'code': fields.char('Código', size=6, required=True),
      'date_start': fields.date('Fecha inicio', required=True),
      'date_stop': fields.date('Fecha final', required=True),
      'period_ids': fields.one2many('giscedata.facturacio.liquidacio.period', 'year_id', 'Periodos'),
      'state': fields.selection([('draft','Draft'), ('done','Done')], 'State', redonly=True),
    }

    _defaults = {
      'state': lambda *a: 'draft',
    }
    _order = "date_start"

    #def create_period3(self,cr, uid, ids, context={}):
    #  return self.create_period(cr, uid, ids, context, 3)

    def create_period(self,cr, uid, ids, context={}, interval=1):
        for fy in self.browse(cr, uid, ids, context):
            ds = datetime.strptime(fy.date_start, '%Y-%m-%d')
            while ds.strftime('%Y-%m-%d')<fy.date_stop:
                de = ds + relativedelta(months=interval, days=-1)
                self.pool.get('giscedata.facturacio.liquidacio.period').create(cr, uid, {
                  'name': de.strftime('%m/%Y'),
                  'code': de.strftime('%m/%Y'),
                  'date_start': ds.strftime('%Y-%m-%d'),
                  'date_stop': de.strftime('%Y-%m-%d'),
                  'year_id': fy.id,
                })
                ds = ds + relativedelta(months=interval)
        return True

    def find(self, cr, uid, dt=None, exception=True, context={}):
        if not dt:
            dt = time.strftime('%Y-%m-%d')
        ids = self.search(cr, uid, [('date_start', '<=', dt), ('date_stop', '>=', dt)])
        if not ids:
            if exception:
                raise osv.except_osv('Error !', 'No fiscal year defined for this date !\nPlease create one.')
            else:
                return False
        return ids[0]
giscedata_facturacio_liquidacio_year()

class giscedata_facturacio_liquidacio_period(osv.osv):
    _name = "giscedata.facturacio.liquidacio.period"
    _description = "Periodo de liquidación de energia"
    _columns = {
      'name': fields.char('Nombre', size=64, required=True),
      'code': fields.char('Código', size=12),
      'date_start': fields.date('Fecha inicio', required=True, states={'done':[('readonly',True)]}),
      'date_stop': fields.date('Fecha final', required=True, states={'done':[('readonly',True)]}),
      'year_id': fields.many2one('giscedata.facturacio.liquidacio.year', 'Año de liquidación', required=True, states={'done':[('readonly',True)]}, select=True),
      'state': fields.selection([('draft','Draft'), ('done','Done')], 'State', readonly=True)
    }
    _defaults = {
      'state': lambda *a: 'draft',
    }
    _order = "date_start"
    def next(self, cr, uid, period, step, context={}):
        ids = self.search(cr, uid, [('date_start','>',period.date_start)])
        if len(ids)>=step:
            return ids[step-1]
        return False

    def find(self, cr, uid, dt=None, context={}):
        if not dt:
            dt = time.strftime('%Y-%m-%d')
#CHECKME: shouldn't we check the state of the period?
        ids = self.search(cr, uid, [('date_start','<=',dt),('date_stop','>=',dt)])
        if not ids:
            raise osv.except_osv('Error !', 'No hay un periodo definido por la fecha %s !\nPor favor cree un año de liquidacion con los periodos correspondientes.' % (dt,))
        return ids
giscedata_facturacio_liquidacio_period()

class giscedata_cne_facturacio(osv.osv):

    _name = 'giscedata.cne.facturacio'

    _columns = {
        'name': fields.char('Año', size=4, required=True),
        'liquidacio_year_id': fields.many2one('giscedata.facturacio.liquidacio.year', 'Año liquidación', required=True, ondelete='restrict'),
        'total_energia_1_semestre': fields.integer('Energía 1er Semestre', required=True),
        'total_euros_1_semestre': fields.float('Euros 1er Semestre', digits=(16,2), required=True),
        'total_energia_2_semestre': fields.integer('Energía 2o Semestre', required=True),
        'total_euros_2_semestre': fields.float('Euros 2o Semestre', digits=(16,2), required=True),
        'linia_ids': fields.one2many('giscedata.cne.facturacio.linia', 'facturacio_cne_id', 'Periodos'),
    }

    _defaults = {
        'total_energia_1_semestre': lambda *a: 0,
        'total_euros_1_semestre': lambda *a: 0.00,
        'total_energia_2_semestre': lambda *a: 0,
        'total_euros_2_semestre': lambda *a: 0.00,
    }

    _sql_constraints = [   ('name_uniq', 'unique (name)', 'Solo puede haber un informe por año.'),    ]

giscedata_cne_facturacio()

class giscedata_cne_facturacio_linia(osv.osv):

    _name = 'giscedata.cne.facturacio.linia'

    # TODO: Funció name_get() que retorni un combinat de comercialtizadora i peridode de liquidació

    _columns  = {
        'facturacio_cne_id': fields.many2one('giscedata.cne.facturacio', 'Any', required=True, ondelete='cascade'),
        'comercialitzadora_id': fields.many2one('res.partner', 'Comercializadora', required=True, ondelete='restrict'),
        'periode_id': fields.many2one('giscedata.facturacio.liquidacio.period', 'Periodo', required=True, ondelete='restrict'),
        'energia': fields.integer('Energia', required=True),
        'euros': fields.float('Euros', digits=(16,2), required=True)
    }

    _defaults = {
        'energia': lambda *a: 0,
        'euros': lambda *a: 0.00,
    }

    _sql_constraints = [   ('comercialitzadora_periode_uniq', 'unique (comercialitzadora_id, periode_id)', 'Esta comercilizadora ya existe en este periodo.'),    ]

giscedata_cne_facturacio_linia()
