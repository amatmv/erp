SELECT
  informe_linia.energia AS informe_linia_quantitat,
  comercialitzadora.name AS comercialitzadora_name,
  to_char(periode.date_stop, 'MM/YYYY') AS periode_date_stop,
  informe_linia.create_date AS informe_linia_create_date,
  'Kwh' AS tipus,
  informe_linia.facturacio_cne_id AS informe_id
FROM
  giscedata_cne_facturacio_linia informe_linia
  LEFT JOIN res_partner comercialitzadora ON informe_linia.comercialitzadora_id = comercialitzadora.id
  LEFT JOIN giscedata_facturacio_liquidacio_period periode ON informe_linia.periode_id = periode.id
UNION
SELECT
  informe_linia.euros AS informe_linia_quantitat,
  comercialitzadora.name AS comercialitzadora_name,
  to_char(periode.date_stop, 'MM/YYYY') AS periode_date_stop,
  informe_linia.create_date AS informe_linia_create_date,
  '€' AS tipus,
  informe_linia.facturacio_cne_id AS informe_id
FROM
  giscedata_cne_facturacio_linia informe_linia
  LEFT JOIN res_partner comercialitzadora ON informe_linia.comercialitzadora_id = comercialitzadora.id
  LEFT JOIN giscedata_facturacio_liquidacio_period periode ON informe_linia.periode_id = periode.id
