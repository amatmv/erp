# -*- coding: utf-8 -*-
{
    "name": "Informes de facturació per la CNE",
    "description": """
    This module provide :
      * Informe de facturación por comercializadora.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_administracio_publica_cne",
        "giscedata_lectures",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cne_facturacio_wizard.xml",
        "giscedata_cne_facturacio_report.xml",
        "ir.model.access.csv",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
