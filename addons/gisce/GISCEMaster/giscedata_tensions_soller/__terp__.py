# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Tensions (Sóller)",
    "description": """
Afegeix les següents tensions
    * 150                150    BT
    * 3x150              150    BT
    * 2x133/230          230    BT
    * 3x230              230    BT
    * 3x8860/15000     15000    AT
    * 3x38106/66000    66000    AT
    * 230+TR150          150    BT
    * 3x230/400+TR150    150    BT
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tensions_data.xml"
    ],
    "active": False,
    "installable": True
}
