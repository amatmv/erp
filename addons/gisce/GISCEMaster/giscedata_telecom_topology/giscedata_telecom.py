# -*- coding: utf-8 -*-
"""
giscedata_telecom.py

Fitxer amb classes comunes per telecomunicacions.

"""

from osv import fields, osv

class GiscedataTelecomNode(osv.osv):
    """Classe per representar nodes de telecomunicacions.
    
    Tenen un codi, una referència geogràfica, un CT associat i si soprten
    telecomandament o no.
    """
    _name = 'giscedata.telecom.node'
    _description = 'Node de telecomunicacions'

    def name_get(self, cursor, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for node in self.browse(cursor, uid, ids):
            res.append((node.id, "%s" % (node.codi,)))
        return res

    _columns = {
        # un camp o fer servir el del CT?
        'name': fields.char('Codi', size=16, required=True), 
        'georef': fields.char('GeoRef', size=128, required=True),
        'telemando': fields.boolean('Telemando'),
        'ct': fields.many2one('giscedata.cts', 'CT'),
        'observacions': fields.text('Observacions'),
    }
GiscedataTelecomNode()

class GiscedataTelecomEquipsCategoria(osv.osv):
    """Classe per representar les categories d'equips de telecomunicacions.
    
    """
    _name = 'giscedata.telecom.equips.categoria'
    _description = "Categoria d'equips de telecomunicacions"
    _columns = {
        'codi': fields.char('Codi', size=16, required=True),
        'name': fields.char('Nom', size=255, required=True),
    }
GiscedataTelecomEquipsCategoria()

class GiscedataTelecomPortConnector(osv.osv):
    """Classe per representar els connectors disponibles a un port d'un equip.
    """
    _name = 'giscedata.telecom.port.connector'
    _description = 'Connectors disponibles pels ports'
    _columns = {
        'codi': fields.char('Codi', size=16, required=True),
        'name': fields.char('Nom', size=255, required=True),
    }
GiscedataTelecomPortConnector()

class GiscedataTelecomCatalegPorts(osv.osv):
    """Classe per representar el catàleg de ports de comunicacions.
    """
    _name = 'giscedata.telecom.cataleg.ports'
    _description = 'Catàleg de ports de comunicacions'
    _inherits = {'product.product': 'product_id'}

    def name_get(self, cursor, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for port in self.browse(cursor, uid, ids):
            res.append((port.id, "%s - %s" % (port.tipus, port.velocitat)))
        return res

    _columns = {
        'tipus': fields.selection([('E', 'Elèctric'), ('O', 'Òptic'), 
                                   ('R','Ràdio')], 'Tipus', required=True),
        'connector': fields.many2one('giscedata.telecom.port.connector', 
                                     'Connector', required=True, 
                                     ondelete='restrict'),
        'velocitat': fields.integer('bps'), # de moment ho deixem a bps... 
                                            # ja veurem quins rangs maneguen
        'product_id': fields.many2one('product.product', 'Producte', 
                                      ondelete='restrict', required=True),
    }
    
    _defaults = {
        'type': lambda *a: 'product',
        'procure_method': lambda *a: 'make_to_stock',
        'standard_price': lambda *a: 0.000000,
    }
GiscedataTelecomCatalegPorts()

class GiscedataTelecomCatalegEquips(osv.osv):
    """Classe per representar el catàleg d'equips de telecomunicacions.
    """
    _name = 'giscedata.telecom.cataleg.equips'
    _description = "Catàleg d'equips de comunicacions"
    _inherits = {'product.product': 'product_id'}
    
    def _get_n_ports(self, cursor, uid, ids, field_name, arg, context):
        """Mètode per obtenir el nombre de ports de l'equip.
        """
        cataleg = self.pool.get('giscedata.telecom.cataleg.equips')
        res = {}
        for _id in ids:
            cat = cataleg.browse(cursor, uid, _id)
            res[_id] = len(cat.ports)
        return res

    def name_get(self, cursor, uid, ids, context={}):
        """Mètode per composar el name del model
        """
        if not len(ids):
            return []
        res = []
        for cequip in self.browse(cursor, uid, ids):
            res.append((cequip.id, "%s - %s" % (
                        cequip.product_id.product_tmpl_id.name, cequip.model)))
        return res

    _columns = {
        'categoria': fields.many2one('giscedata.telecom.equips.categoria', 
                                     'Categoria'),
        'marca': fields.many2one('res.partner', 'Fabricant'),
        'model': fields.char('Model', size=128),
        'ports': fields.one2many('giscedata.telecom.port', 'equip_id', 'Ports'),
        'num_ports': fields.function(_get_n_ports, type='integer', 
                                     obj='giscedata.telecom.cataleg.equips', 
                                     method=True, string='N Ports'),
        'product_id': fields.many2one('product.product', 'Producte', 
                                      ondelete='restrict', required=True),
    }
    
    _defaults = {
        'type': lambda *a: 'product',
        'procure_method': lambda *a: 'make_to_stock',
        'standard_price': lambda *a: 0.000000,
    }

GiscedataTelecomCatalegEquips()

class GiscedataTelecomPort(osv.osv):
    """Classe per representar un port de telecomunicacions.
    """
    _name = 'giscedata.telecom.port'
    _description = 'Port de comunicacions'
    _columns = {
        'name': fields.char('Codi port', size=16, required=True),
        'model': fields.many2one('giscedata.telecom.cataleg.ports', 'Model', 
                                 required=True),
        'equip_id': fields.many2one('giscedata.telecom.cataleg.equips', 
                                    'Equip', required=True),
    }
GiscedataTelecomPort()

class GiscedataTelecomEquip(osv.osv):
    """Classe per representar un equip de telecomunicacions.
    """
    _name = 'giscedata.telecom.equip'
    _description = 'Equips de telecomunicacions'

    def name_get(self, cursor, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for equip in self.browse(cursor, uid, ids):
            res.append((equip.id, "%s (%s)" % (equip.model.name, equip.ip)))
        return res

    _columns = {
        'node': fields.many2one('giscedata.telecom.node', 'Node', 
                                required=True),
        'data_instalacio': fields.date('Data instal.lació', required=True),
        'ip': fields.char('Adreça IP', size=15, required=True),
        'model': fields.many2one('giscedata.telecom.cataleg.equips', 'Model', 
                                 required=True),
    }
GiscedataTelecomEquip()

