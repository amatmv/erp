# -*- coding: utf-8 -*-
"""
giscedata_telecom_rf.py

Aquest fitxer conté els models relacionats amb les telecomunicacions RF.

"""

from osv import fields, osv

class GiscedataTelecomMastil(osv.osv):
    """Classe per representar un màstil d'antena
    """
    _name = 'giscedata.telecom.mastil'
    _description = 'Màstils'
    
    def name_get(self, cursor, uid, ids, context={}):
        if not len(ids):
            return []
        res = []
        for mastil in self.browse(cursor, uid, ids):
            res.append((mastil.id, 
                        "MASTIL %sm @ %s" % (mastil.longitud, mastil.geocode)))
        return res
    
    _columns = {
        'longitud': fields.integer('Longitud (m)', required=True),
        'geocode': fields.char('Geocode', size=128, required=True), 
    }
GiscedataTelecomMastil()

class GiscedataTelecomCatalegAntenes(osv.osv):
    """Classe per representar el catàleg d'antenes.
    """
    _name = 'giscedata.telecom.cataleg.antenes'
    _description = "Catàleg d'antenes"
    _inherits = {'product.product': 'product_id'}
    
    _tipus_antenes = (
        ('DI', 'Dipolo'),
        ('PA', 'Panell'),
        ('PR', 'Parabòlica'),
    )
    _columns = {
        'tipus': fields.selection(_tipus_antenes, 'Tipus', required=True),
        'descripcio': fields.char('Descripció', size=256),
        'ganancia': fields.integer('Ganància (dB)'),
        'fabricant': fields.many2one('res.partner', 
                                     'Fabricant', required=True),
        'model': fields.char('Model', size=128),
        'frequencies': fields.char('Freqüències', size=128),
        'product_id': fields.many2one('product.product', 'Producte', 
                                      ondelete='restrict'),
    }
    
    _defaults = {
        'type': lambda *a: 'product',
        'procure_method': lambda *a: 'make_to_stock',
        'standard_price': lambda *a: 0.000000,
    }
    
GiscedataTelecomCatalegAntenes()

class GiscedataTelecomAntena(osv.osv):
    """Classe per representar una antena.
    """
    _name = 'giscedata.telecom.antena'
    _description = 'Antena de RF'
    _columns = {
        'name': fields.char('Codi', size=128, required=True),
        'model_id': fields.many2one('giscedata.telecom.cataleg.antenes', 
                                    'Model', required=True),
        'mastil_id': fields.many2one('giscedata.telecom.mastil', 'Mastil',
                                     required=True),
        'data_instalacio': fields.date('Data instal.lació'),
    }
GiscedataTelecomAntena()

class GiscedataTelecomRenllac(osv.osv):
    """Classe per representar un radioenllaç.
    """
    _name = 'giscedata.telecom.renllac'
    _description = 'Radioenllaç'
    _tipus_renllac = (
        ('PAP', 'Punt-a-Punt'),
        ('PMP', 'Punt-MultiPunt'),
    )
    _columns = {
        'frequencia': fields.float('Freqüència (MHz)', required=True),
        'amplebanda': fields.float('Ample de banda (MHz)', required=True),
        'canal': fields.integer('Canal', required=True),
        'tipus': fields.selection(_tipus_renllac, 'Tipus radioenllaç', 
                                  required=True),
        'observacions': fields.text('Observacions'),
        'antenes': fields.one2many('giscedata.telecom.antena.renllac', 
                                   'renllac_id', 'Antenes'),
    }
GiscedataTelecomRenllac()

class GiscedataTelecomAntenaRenllac(osv.osv):
    """Classe per representar les antenes que pertanyen a un radioenllaç.
    """
    _name = 'giscedata.telecom.antena.renllac'
    _description = "Antenes a un radioenllaç"
    _funcions_antena = (
        ('O', 'Orígen'),
        ('B', 'Base'),
        ('D', 'Destí'),
    )
    _columns = {
        'antena_id': fields.many2one('giscedata.telecom.antena', 'Antena', 
                                     required=True),
        'renllac_id': fields.many2one('giscedata.telecom.renllac', 'R. Enllaç',
                                      required=True),
        'funcio': fields.selection(_funcions_antena, 'Funció', required=True),
    }
GiscedataTelecomAntenaRenllac()
