# -*- coding: utf-8 -*-
"""
giscedata_telecom_fo.py 

Aquest fitxer conté els diferents models de telecomunicacions de fibra òptica.

"""

from osv import fields, osv

class GiscedataTelecomFibra(osv.osv):
    """Classe per les fibres dels cables
    """
    _name = 'giscedata.telecom.fibra'
    _description = 'Fibra òptica'
    _colors_fibra = [
        (1, 'verd'),
        (2, 'taronja'),
        (3, 'blau'),
        (4, 'groc'),
        (5, 'gris'),
        (6, 'lila'),
        (7, 'marró'),
        (8, 'blanc'),
    ]
    _columns = {
        'color': fields.integer('Color fibra', selection=_colors_fibra, 
                                widget='selection', required=True),
        'name': fields.integer('Número de fibra', required=True),
    }
GiscedataTelecomFibra()

class GiscedataTelecomCatalegCables(osv.osv):
    """Classe pels cables de FO
    """
    _name = 'giscedata.telecom.cataleg.cables'
    _description = 'Catàleg de cables de FO'
    _inherits = {'product.product': 'product_id'}
    
    def _get_n_fibres(self, cursor, uid, ids, field_name, arg, context):
        """Mètode per obtenir el nombre de fibres del cable
        """
        cataleg = self.pool.get('giscedata.telecom.cataleg.cables')
        res = {}
        for _id in ids:
            cat = cataleg.browse(cursor, uid, _id)
            res[_id] = len(cat.fibres)
        return res

    _columns = {
        'codi': fields.char('Codi', size=16, required=True),
        'tipus': fields.char('Tipus', size=16, required=True),
        'fabricant': fields.many2one('res.partner', 
                                     'Fabricant'),
        'model': fields.char('Model', size=128),
        'n_fibres': fields.function(_get_n_fibres, type='integer', 
                                    obj='giscedata.telecom.cataleg.cables', 
                                    method=True, string='N Fibres'),
        'coberta': fields.selection([('AL','Alumini'), ('PET', 'Polietilé')], 
                                    'Tipus de coberta'),
        'fibres': fields.many2many('giscedata.telecom.fibra', 
                                   'giscedata_telecom_cataleg_cables_fibres', 
                                   'cable_id', 'fibra_id', 'Fibres'),
        'product_id': fields.many2one('product.product', 'Producte', 
                                      ondelete='restrict', required=True),
    }
    
    _defaults = {
        'type': lambda *a: 'product',
        'procure_method': lambda *a: 'make_to_stock',
        'standard_price': lambda *a: 0.000000,
    }
    
GiscedataTelecomCatalegCables()

class GiscedataTelecomLinia(osv.osv):
    """Classe per les línies de FO
    """
    _name = 'giscedata.telecom.linia'
    _description = 'Línia de FO'
    _columns = {
        'name': fields.char('Nom', size=128, required=True),
    }
GiscedataTelecomLinia()

class GiscedataTelecomTram(osv.osv):
    """Classe pels trams de línies de FO
    """
    _name = 'giscedata.telecom.tram'
    _description = 'Tram de cable de FO'
    _columns = {
        'name': fields.char('Nom del tram', size=128, required=True),
        'origen': fields.char('Orígen', size=128, required=True),
        'final': fields.char('Final', size=128, required=True),
        'cable_id': fields.many2one('giscedata.telecom.cataleg.cables', 
                                    'Tipus de cable', required=True),
        'linia_id': fields.many2one('giscedata.telecom.linia', 'Línia',
                                    required=True),
    }
GiscedataTelecomTram()

class GiscedataTelecomRepartidorTipus(osv.osv):
    """Classe pels tipus de repartidors de FO
    """
    _name = 'giscedata.telecom.repartidor.tipus'
    _description = 'Tipus de Repartidors'
    _columns = {
        'name': fields.char('Nom', size=128, required=True),
        'codi': fields.char('Tipus', size=16, required=True),
    }
GiscedataTelecomRepartidorTipus()

class GiscedataTelecomCatalegRepartidors(osv.osv):
    """Classe pel catàleg de repartidors de FO
    """
    _name = 'giscedata.telecom.cataleg.repartidors'
    _description = 'Catàleg de repartidors FO'
    _inherits = {'product.product': 'product_id'}
    
    _columns = {
        'model': fields.char('Model', size=128, required=True),
        'capacitat': fields.integer('Capacitat'),
        'adaptadors': fields.char('Tipus adaptadors', size=16),
        'dimensions': fields.char('Dimensions', size=32),
        'format': fields.char('Format', size=128),
        'tipus': fields.many2one('giscedata.telecom.repartidor.tipus', 'Tipus', 
                                 required=True),
        'product_id': fields.many2one('product.product', 'Producte', 
                                      ondelete='restrict'),
    }
    
    _defaults = {
        'type': lambda *a: 'product',
        'procure_method': lambda *a: 'make_to_stock',
        'standard_price': lambda *a: 0.000000,
    }
GiscedataTelecomCatalegRepartidors()

class GiscedataTelecomRepartidor(osv.osv):
    """Classe pels repartidors de FO
    """
    _name = 'giscedata.telecom.repartidor'
    _description = 'Repartidor FO'
    _columns = {
        'name': fields.char('Codi', size=16, required=True),
        'node': fields.many2one('giscedata.telecom.node', 'Node', 
                                required=True),
        'data': fields.date('Data instal.lació'),
        'instalador': fields.char('Instalador', size=128),
        'state': fields.selection((('on', 'En servei'), 
                                   ('off', 'Fora de servei')), 'Estat', 
                                   required=True),
        'model': fields.many2one('giscedata.telecom.cataleg.repartidors', 
                                 'Model', required=True),
    }
    _defaults = {
        'state': lambda x: 'on',
    }
GiscedataTelecomRepartidor()

