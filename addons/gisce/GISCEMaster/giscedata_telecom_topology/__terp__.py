# -*- coding: utf-8 -*-
{
    "name": "GISCE Telecom",
    "description": """Mòdul per gestionar actius de telecomunicacions i topologia""",
    "version": "0-dev",
    "author": "GISCE Enginyeria, SL",
    "category": "",
    "depends":[
        "base",
        "base_extended",
        "giscedata_cts",
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_telecom_data.xml",
        "giscedata_telecom_view.xml"
    ],
    "active": False,
    "installable": True
}
