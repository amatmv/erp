# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'firmant_id': fields.many2one('res.partner', string=u'Persona firmant'),
        'firmant_vat': fields.related('firmant_id', 'vat',
                                       type='char', size=32,
                                       string=u'Nº Document firmant',
                                       readonly=True),
    }

GiscedataPolissa()
