# -*- coding: utf-8 -*-
{
    "name": "giscedata_polissa_persona_firmant",
    "description": """
        Afegir la persona firmant al contracte
        """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_polissa_view.xml"
    ],
    "active": False,
    "installable": True
}
