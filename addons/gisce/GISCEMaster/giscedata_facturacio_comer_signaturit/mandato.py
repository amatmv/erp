from osv import osv
from datetime import datetime


class PaymentMandate(osv.osv):
    _name = 'payment.mandate'
    _inherit = 'payment.mandate'

    def make_bank_account_mod_con(self, cursor, uid, res_id, context=None):
        if context is None:
            context = {}
        pol_obj = self.pool.get('giscedata.polissa')
        doc_obj = self.pool.get('giscedata.signatura.documents')
        wizard_account_obj = self.pool.get('wizard.bank.account.change')

        document_id = context.get('signature_document_id', None)
        mandato = self.browse(cursor, uid, res_id, context=context)
        if document_id:
            document = doc_obj.browse(cursor, uid, document_id)
        else:
            raise osv.except_osv('Documento no valido proceder manualmente')

        resource, resource_id = mandato.reference.split(',')
        if resource == 'giscedata.polissa':
            polissa_id = int(resource_id)
            values = document.process_id.data['process_data']
            values['digital_sign'] = False
            values.pop('id')
            polissa_state = pol_obj.read(cursor, uid, polissa_id, ['state'])
            if polissa_state['state'] != 'esborrany':
                ctx = {
                    'active_id': polissa_id,
                    'use_mandate_id': mandato.id
                }
                wiz_id = wizard_account_obj.create(
                    cursor, uid, values, context=context)
                wizard_account_obj.action_bank_account_change(
                    cursor, uid, [wiz_id], context=ctx)
                state = wizard_account_obj.read(
                    cursor, uid, wiz_id, ['state'])[0]['state']
                if state == 'conf':
                    ctx['modcon_type'] = 'current'
                    wizard_account_obj.action_bank_account_change_confirm(
                        cursor, uid, [wiz_id], context=ctx)

        return True

    def process_signature_callback(self, cursor, uid, res_id, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        res = super(PaymentMandate, self).process_signature_callback(
            cursor, uid, res_id, context=context
        )
        if process_data:
            method = process_data.get('callback_method', False)
            if method == 'bank_account_change':
                self.make_bank_account_mod_con(
                    cursor, uid, res_id, context=context)

        return res


PaymentMandate()
