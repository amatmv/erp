# coding=utf-8
from osv import osv, fields
from tools.translate import _


class WizardBankAccountChange(osv.osv_memory):
    """
    Modificamos el asistente para cambiar el comportamiento de la siguiente
    forma:
        El asistente no crearà la modificación contractual sino que solo
        creará el mandato. Cuando el mandato sea firmado se creará la
        modificación contractual.
    """
    _name = 'wizard.bank.account.change'
    _inherit = 'wizard.bank.account.change'

    def on_change_digital_sign(self, cursor, uid, ids, digital_sign, context=None):
        if context is None:
            context = {}
        template = {'domain': {}, 'value': {}, 'warning': {}}
        if digital_sign:
            template['value'].update({'print_mandate': 0})

            address_obj = self.pool.get('res.partner.address')
            pol_obj = self.pool.get('giscedata.polissa')
            pol_id = context.get('active_id', False)
            if pol_id:
                titular = pol_obj.read(
                    cursor, uid, pol_id, ['titular']
                )['titular'][0]
                # Busco la direcció de notificació.
                search_params = [
                    ('type', '=', 'contact'),
                    ('partner_id', '=', titular)
                ]
                address_id = address_obj.search(
                    cursor, uid, search_params, limit=1, context=context)
                if address_id:
                    email = address_obj.read(cursor, uid, address_id[0], ['email'])
                    if email:
                        template['value'].update({'email': email['email']})
        return template

    def action_bank_account_change_confirm(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        mandate_obj = self.pool.get('payment.mandate')
        signature_obj = self.pool.get('giscedata.signatura.process')
        pol_obj = self.pool.get('giscedata.polissa')
        imd_obj = self.pool.get('ir.model.data')
        bank_obj = self.pool.get('res.partner.bank')
        attach_obj = self.pool.get('ir.attachment')

        mandate_report_id = imd_obj.get_object_reference(
            cursor, uid, 'l10n_ES_remesas', 'report_mandato_generico'
        )[1]

        wiz = self.browse(cursor, uid, ids, context=context)[0]

        if wiz.digital_sign:
            if not wiz.email:
                raise osv.except_osv(
                    _('Error!'),
                    _(u"Se necesita una dirección con correo electrónico "
                      u"donde enviar el documento a firmar.")
                )

            # Mirar si polissa té compte bancari
            polissa_id = context.get('active_id', False)
            pol_bank_fields = pol_obj.read(
                cursor, uid, polissa_id, ['bank', 'tipo_pago', 'payment_mode_id'])
            pol_bank = pol_bank_fields['bank']

            if not pol_bank:
                # Creo en el contracte l'iban final
                iban = wiz.account_iban.replace(" ", "")
                bank_new_id = bank_obj._find_or_create_bank(
                    cursor, uid, wiz.pagador.id, iban, wiz.account_owner,
                    wiz.owner_address, context=context
                )
                params = {
                    'bank': bank_new_id,
                }

                if wiz.payment_mode:
                    params.update({
                        'payment_mode_id': wiz.payment_mode.id,
                        'tipo_pago': wiz.payment_mode.type.id
                    })
                pol_obj.write(cursor, uid, polissa_id, params, context=context)

            mandate_reference = "giscedata.polissa,{}".format(polissa_id)
            mandate_new_id = mandate_obj._create_mandate(
                cursor, uid, wiz.change_date, mandate_reference, wiz.mandate_scheme,
                context=context
            )

            if not pol_bank:
                params = {
                    'bank': False,
                    'payment_mode_id': pol_bank_fields['payment_mode_id'][0],
                    'tipo_pago': pol_bank_fields['tipo_pago'][0]
                }
                pol_obj.write(cursor, uid, polissa_id, params, context=context)

            mandate_obj.write(cursor, uid, [mandate_new_id], {
                'date': False,
                'signed': 0,
                'debtor_iban': wiz.account_iban.replace(" ", "")
            })
            data = {
                'callback_method': 'bank_account_change',
                'process_data': wiz.read(load='_classic_write')[0]
            }
            mandate_categ = attach_obj.get_category_for(
                cursor, uid, 'mandato', context=context)
            process_id = signature_obj.create(cursor, uid,  {
                'subject': 'Cambio cuenta corriente',
                'body': '',
                'data': data,
                'delivery_type': wiz.delivery_type,
                'recipients': [
                    (0, 0, {
                        'partner_address_id': wiz.owner_address.id,
                        'name': wiz.owner_address.name,
                        'email': wiz.email,
                        'phone': wiz.owner_address.mobile
                    })
                ],
                'files': [
                    (0, 0, {
                        'model': 'payment.mandate,{}'.format(mandate_new_id),
                        'report_id': mandate_report_id,
                        'category_id': mandate_categ
                    })
                ]

            })
            signature_obj.start(cursor, uid, [process_id], context)
            return {
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.signatura.process',
                'type': 'ir.actions.act_window',
                'domain': [('id', 'in', [process_id])]
            }
        else:
            return super(WizardBankAccountChange, self).action_bank_account_change_confirm(
                cursor, uid, ids, context
            )

    _columns = {
        'digital_sign': fields.boolean('Firma digital'),
        'delivery_type': fields.selection([
            ('url', 'Código QR'),
            ('email', 'Email')
        ], 'Forma de envío'),
        'email': fields.char('E-mail', size=256)
    }

    _defaults = {
        'delivery_type': lambda *a: 'email',
    }


WizardBankAccountChange()
