# -*- coding: utf-8 -*-
{
  "name": "Facturació comer (Signaturit)",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Cambio de cuenta bancària con Signaturit
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      "l10n_ES_remesas_signaturit",
      "giscedata_signatura_documents_signaturit",
      "giscedata_facturacio_comer"
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
    "wizard/wizard_bank_account_change_view.xml",
    "giscedata_signatura_documents_data.xml"
  ],
  "active": False,
  "installable": True
}
