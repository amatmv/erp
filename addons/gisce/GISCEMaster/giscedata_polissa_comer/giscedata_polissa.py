# -*- coding: utf-8 -*-
"""Mòdul giscedata_polissa (comercialitzadora)."""
from osv import osv, fields
from tools.translate import _
from tools.safe_eval import safe_eval as eval


def get_polissa_ids(self, cursor, uid, partner_ids, context=None):
    pol_obj = self.pool.get('giscedata.polissa')
    if not isinstance(partner_ids, list):
        partner_ids = [partner_ids]
    return pol_obj.search(
        cursor, uid, [('titular', 'in', partner_ids)], context=context)

STORE_PROC_BONOSOCIAL_FIELD = {
    'giscedata.polissa': (
        lambda self, cursor, uid, ids, context=None: ids,
        ['potencia', 'tipus_vivenda', 'cnae', 'titular'], 20),
    'res.partner': (get_polissa_ids, ['vat'], 20),
}


class GiscedataPolissa(osv.osv):
    """Pòlissa (Comercialitzadora)."""
    
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'
    _description = "Polissa d'un Client"

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'ref_dist': False
        }
        default.update(default_values)
        res_id = super(
            GiscedataPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def default_comercialitzadora(self, cursor, uid, context=None):
        """Obté el valor per defecte de la comercialitzadora.
        """
        user = self.pool.get('res.users').browse(cursor, uid, uid, context)
        return user.company_id.partner_id.id

    def _aplicacion_bono(self, cursor, uid, ids, field_name, arg, context):
        res_obj = self.pool.get('res.partner')
        if not isinstance(ids, list):
            ids = [ids]
        res = dict.fromkeys(ids, 0)

        from osv.expression import OOQuery
        q = OOQuery(self, cursor, uid)
        sql = q.select(['id', 'titular.vat']).where([
            ('tipus_vivenda', '=', 'habitual'),
            ('cnae.name', '=', '9820'),
            ('potencia', '<=', 10),
            ('id', 'in', ids)
        ])
        cursor.execute(*sql)
        for polissa in cursor.dictfetchall():
            if not res_obj.is_enterprise_vat(polissa['titular.vat']):
                res[polissa['id']] = 1
        return res

    def _check_cnae_and_vat_corrects(self, cursor, uid, ids):
        res_obj = self.pool.get('res.partner')
        conf_obj = self.pool.get('res.config')

        constraint_enabled = conf_obj.get(
            cursor, uid, 'cnae_constrain_enabled', '0')

        if eval(constraint_enabled):
            for con in self.browse(cursor, uid, ids):
                if con.cnae.name == '9820' and res_obj.is_enterprise_vat(
                        con.titular.vat):
                    return False
        return True

    _columns = {
        'name': fields.char('Contracte', size=60, readonly=True, select=True,
                            states={'esborrany':[('readonly', False)]}),
        'ref_dist': fields.char('Referència distribuidora', size=60),
        'distribuidora': fields.many2one('res.partner', 'Distribuidora',
                                    domain=[('supplier', '=', 1)],
                                    readonly=True,
                                    states={
                                        'esborrany':[('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                    }),
        # Aquest camp és especial per la sincronització, té un valor constant
        'comercialitzadora': fields.many2one('res.partner', 'Comercialitzadora',
                                             readonly=True, required=True),
        'facturacio_distri': fields.selection([(1, 'Mensual'), (2, 'Bimestral')],
                                              u'Facturació de distribuïdora',
                                              readonly=True,
                                              states={'esborrany': [('readonly',
                                                                     False)],
                                                      'validar': [('readonly',
                                                                    False)],
                                                      'modcontractual': [
                                                          ('readonly', False)],
                                                      },
                                              help=u"Periodicitat de "
                                                   u"facturació de"
                                                   u"distribuidora"),
        'bono_social_disponible': fields.function(
            _aplicacion_bono, method=True,
            string="Bo social disponible",
            type='boolean',
            store=STORE_PROC_BONOSOCIAL_FIELD,
        )
    }

    _defaults = {
        'comercialitzadora': default_comercialitzadora,
    }

    _constraints = [
        (_check_cnae_and_vat_corrects,
         _("Les pòlisses amb CNAE 9820 no poden tenir un VAT d'empresa"),
         ['cnae']),
    ]

GiscedataPolissa()


class GiscedataPolissaModContractual(osv.osv):
    """Modificació contractual (comercialitzadora)."""
    
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'distribuidora': fields.many2one('res.partner', 'Distribuidora',
                                    domain=[('supplier', '=', 1)],
                                    required=True),
        'ref_dist': fields.char('Referència distribuidora', size=60),
        'facturacio_distri': fields.selection([(1, 'Mensual'),
                                               (2, 'Bimestral')],
                                              u'Facturació de distribuïdora',
                                              help=u"Periodicitat de "
                                                   u"facturació de "
                                                   u"distribuidora"),
        'bono_social_disponible': fields.boolean('Bo social disponible'),

    }
    
GiscedataPolissaModContractual()

