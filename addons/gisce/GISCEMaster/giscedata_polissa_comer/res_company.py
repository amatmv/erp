# -*- coding: utf-8 -*-
from osv import osv, fields


class ResCompany(osv.osv):

    _name = 'res.company'
    _inherit = 'res.company'

    _columns = {
      'codi_r2': fields.char('Codi R2', size=4,
                             help='Codi R2 de la comercialitzadora segons la '
                                  'CNMC'),
    }

ResCompany()