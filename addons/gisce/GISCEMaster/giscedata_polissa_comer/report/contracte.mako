    <%inherit file="/giscedata_polissa/report/contracte.mako"/>
    <%block name="custom_css" >
        <link rel="stylesheet"
          href="${addons_path}/giscedata_polissa/report/stylesheet_generic_contract.css"/>
    </%block >
    <%block name="info_lloguer">
        <td style="width: 250px;">
            <span class="label">${_("Lloguer equip de mesura:")}</span>
            <span class="field">${_("Segons regulació")}</span>
        </td>

    </%block>
    <%def name="llista_preus(polissa)">
        <%
            import calendar
            from giscedata_facturacio.report.utils import get_atr_price

            potencies = polissa.potencies_periode
            if potencies:
                periodes = []
                for i in range(0, 6):
                    if i < len(potencies):
                        periode = potencies[i]
                    else:
                        periode = False
                    periodes.append((i+1, periode))
            ctx = {'date': False}
            if potencies:
                ctx['sense_agrupar'] = True
                periodes_energia = sorted(polissa.tarifa.get_periodes(context=ctx).keys())
                periodes_potencia = sorted(polissa.tarifa.get_periodes('tp', context=ctx).keys())
                if periodes:
                    if data_final:
                        data_llista_preus = min(datetime.strptime(data_final, '%Y-%m-%d'), datetime.today())
                        ctx['date'] = data_llista_preus
                    data_i = data_inici and datetime.strptime(polissa.modcontractual_activa.data_inici, '%Y-%m-%d')
                    if data_i and calendar.isleap(data_i.year):
                        dies = 366
                    else:
                        dies = 365
            %>

            %if potencies:
                <div class="seccio">
                    ${_(u"PREUS CONTRACTATS:")}
                    <table class="data_table collapse">
                        <colgroup>
                            <col style="width:30%"/>
                            <col style="width:10%"/>
                            <col style="width:10%"/>
                            <col style="width:10%"/>
                            <col style="width:10%"/>
                            <col style="width:10%"/>
                            <col style="width:10%"/>
                        </colgroup>
                        <thead>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <span class="label">P1</span>
                                </td>
                                <td>
                                    <span class="label">P2</span>
                                </td>
                                <td>
                                    <span class="label">P3</span>
                                </td>
                                <td>
                                    <span class="label">P4</span>
                                </td>
                                <td>
                                    <span class="label">P5</span>
                                </td>
                                <td>
                                    <span class="label">P6</span>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    ${_(u"Preu del terme d'Energia €/kWh:")}
                                </td>
                                %for p in periodes_energia:
                                    <td style="width: 60px;" class="">
                                        <span class="">${formatLang(get_atr_price(cursor, uid, polissa, p, 'te', ctx)[0], digits=6)}</span>
                                    </td>
                                %endfor
                                %if len(periodes_energia) < 6:
                                    %for p in range(0, 6-len(periodes_energia)):
                                        <td style="width: 60px;" class="">
                                            &nbsp;
                                        </td>
                                    %endfor
                                %endif
                            </tr>
                            <tr>
                                <td>
                                    ${_(u"Preu del terme de Potència €/kW/any:")}
                                </td>
                                %for p in periodes_potencia:
                                    <td style="width: 60px;" class="">
                                        <span class="">${formatLang(get_atr_price(cursor, uid, polissa, p, 'tp', ctx)[0] * 365, digits=6)}</span>
                                    </td>
                                %endfor
                                %if len(periodes_potencia) < 6:
                                    %for p in range(0, 6-len(periodes_potencia)):
                                        <td style="width: 60px;" class="">
                                            &nbsp;
                                        </td>
                                    %endfor
                                %endif
                            </tr>
                        </tbody>
                    </table>
                </div>
            %endif
    </%def>
