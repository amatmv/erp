SELECT
  polissa.name AS polissa_name,
  contracte.name AS contracte_name,
  titular.name AS titular_name,
  titular.vat AS titular_vat,
  cnae.name AS cnae_name,
  cnae.descripcio AS cnae_descripcio,
  notificacio_address.street AS notificacio_address_street,
  notificacio_address.city AS notificacio_address_city,
  notificacio_address.zip AS notificacio_address_zip,
  notificacio_address.phone AS notificacio_address_phone,
  notificacio_address.fax AS notificacio_address_fax,
  notificacio_address.mobile AS notificacio_address_mobile,
  notificacio_address.email AS notificacio_address_email,
  notificacio_state.name AS notificacio_state_name,
  notificacio_country.name AS notificacio_country_name,
  invoice_address.name AS invoice_address_name,
  invoice_address.street AS invoice_address_street,
  invoice_address.city AS invoice_address_city,
  invoice_address.zip AS invoice_address_zip,
  invoice_address.phone AS invoice_address_phone,
  invoice_address.fax AS invoice_address_fax,
  invoice_address.mobile AS invoice_address_mobile,
  invoice_address.email AS invoice_address_email,
  invoice_state.name AS invoice_state_name,
  invoice_country.name AS invoice_country_name,
  cups.tv AS cups_tv,
  cups.nv AS cups_nv,
  cups.pnp AS cups_pnp,
  cups.es AS cups_es,
  cups.pt AS cups_pt,
  cups.pu AS cups_pu,
  cups.cpo AS cups_cpo,
  cups.cpa AS cups_cpa,
  cups.dp AS cups_dp,
  cups.localitat AS cups_localitat,
  cups.name AS cups_name,
  cups.catas_ref AS cups_catas_ref,
  distribuidora.name AS distribuidora_name,
  cups_municipi.name AS cups_municipi_name,
  cups_state.name AS cups_state_name,
  contracte.tensio AS contracte_tensio,
  tarifa_acces.name AS tarifa_acces_name,
  contracte.potencies_periode AS contracte_potencies_periode,
  tarifa_comer.name AS tarifa_comer_name,
  bank.acc_number AS bank_acc_number,
  bank.owner_name AS bank_owner_name,
  comercialitzadora.name AS comercialitzadora_name,
  contracte.data_inici AS contracte_data_inici,
  contracte.id AS contracte_id
FROM
  giscedata_polissa_modcontractual contracte
  LEFT JOIN giscedata_polissa polissa ON contracte.polissa_id = polissa.id
  LEFT JOIN res_partner titular ON contracte.titular = titular.id
  LEFT JOIN res_partner_address notificacio_address ON contracte.direccio_notificacio = notificacio_address.id
  LEFT JOIN res_country_state notificacio_state ON notificacio_address.state_id = notificacio_state.id
  LEFT JOIN res_country notificacio_country ON notificacio_address.country_id = notificacio_country.id
  LEFT JOIN giscemisc_cnae cnae ON contracte.cnae = cnae.id
  LEFT JOIN res_partner_address invoice_address ON contracte.direccio_pagament = invoice_address.id
  LEFT JOIN res_country_state invoice_state ON invoice_address.state_id = invoice_state.id
  LEFT JOIN res_country invoice_country ON invoice_address.country_id = invoice_country.id
  LEFT JOIN giscedata_cups_ps cups ON contracte.cups = cups.id
  LEFT JOIN res_partner distribuidora ON cups.distribuidora = distribuidora.id
  LEFT JOIN res_municipi cups_municipi ON cups.id_municipi = cups_municipi.id
  LEFT JOIN res_country_state cups_state ON cups_municipi.state = cups_state.id
  LEFT JOIN giscedata_polissa_tarifa tarifa_acces ON contracte.tarifa_acces = tarifa_acces.id
  LEFT JOIN product_pricelist tarifa_comer ON contracte.llista_preu = tarifa_comer.id
  LEFT JOIN res_partner_bank bank ON contracte.bank = bank.id
  LEFT JOIN res_users res_users ON contracte.create_uid = res_users.id
  LEFT JOIN res_company res_company ON res_users.company_id = res_company.id
  LEFT JOIN res_partner comercialitzadora ON res_company.partner_id = comercialitzadora.id
LIMIT 1
