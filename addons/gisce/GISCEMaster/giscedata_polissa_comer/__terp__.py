# -*- coding: utf-8 -*-
{
    "name": "Polisses dels clients (Comercialitzadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_cups_comer",
        "account_payment_extension"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_polissa_comer_demo.xml"
    ],
    "update_xml":[
        "giscedata_polissa_comer_data.xml",
        "giscedata_polissa_view.xml",
        "res_company_view.xml",
        "giscedata_polissa_comer_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
