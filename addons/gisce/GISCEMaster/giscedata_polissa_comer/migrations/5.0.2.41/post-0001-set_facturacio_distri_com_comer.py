# -*- coding: utf-8 -*-

import netsvc


def migrate(cursor, installed_version):
    """Assignem per defecte facturació de distribuidara igual que
    comercialitzadora."""
    logger = netsvc.Logger()

    try:
        cursor.execute("UPDATE giscedata_polissa SET facturacio_distri=facturacio")
        cursor.execute("UPDATE giscedata_polissa_modcontractual SET facturacio_distri=facturacio")
    except Exception, e:
        msg = (u"ERROR actualitzant 'giscedata_polissa.facturacio_distri'"
               u"o 'giscedata_polissa_modcontractual.facturacio_distri'"
               u": %s" % e)
        logger.notifyChannel('migration', netsvc.LOG_WARNING, msg)
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         "Totes les Polisses i MOdificacions Contractuals"
                         "facturacio_distri igual que facturació")
