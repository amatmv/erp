# -*- coding: utf-8 -*-

import netsvc
from destral import testing
from destral.transaction import Transaction
import datetime


class TestsPolissaBonDisponibleField(testing.OOTestCase):

    def test_field_function_work_when_changing_values(self):
        """ This test check if field ckeck and unckeck with conditions:
                Field is checked if:
                    - owner is not enterprise (constrain)
                    - vivenda habitual
                    - contracted power under 10 KW
                    - CNAE 9820
        """
        with Transaction().start(self.database) as txn:
            pool = self.openerp.pool
            cursor = txn.cursor
            uid = txn.user

            polissa_obj = pool.get('giscedata.polissa')
            imd_obj = pool.get('ir.model.data')

            # Importing contract 1

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            bono_value = polissa_obj.read(
                cursor, uid, polissa_id, ['bono_social_disponible']
            )['bono_social_disponible']

            self.assertFalse(bono_value)

            partner = polissa_obj.read(
                cursor, uid, polissa_id, ['titular']
            )['titular'][0]

            # Set vat as no enterprise
            res_obj = pool.get('res.partner')
            res_obj.write(cursor, uid, partner, {'vat': 'ES71895522Y'})

            cnae_9820 = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_cnae', 'cnae_9820'
            )[1]

            # Here current owner vat is enterprise and power is under 10 kW
            polissa_obj.write(
                cursor, uid, polissa_id,
                {'cnae': cnae_9820, 'tipus_vivenda': 'habitual'}
            )

            bono_value = polissa_obj.read(
                cursor, uid, polissa_id, ['bono_social_disponible']
            )['bono_social_disponible']

            self.assertTrue(bono_value)

            cnae_no = imd_obj.get_object_reference(
                cursor, uid, 'giscemisc_cnae', 'cnae_99'
            )[1]
            polissa_obj.write(cursor, uid, polissa_id, {'cnae': cnae_no})

            bono_value = polissa_obj.read(
                cursor, uid, polissa_id, ['bono_social_disponible']
            )['bono_social_disponible']

            self.assertFalse(bono_value)
