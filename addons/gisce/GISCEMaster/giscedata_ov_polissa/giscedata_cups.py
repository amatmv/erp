# -*- coding: utf-8 -*-
from osv import osv


class GiscedataCupsPs(osv.osv):

    _inherit = 'giscedata.cups.ps'

    def is_cups_ov_contractable(self, cursor, uid, cups_name, context=None):
        """
        Aquesta funció serveix per saber des de l'oficina virtual si el cups
        compleix les condicions per poder donar d'alta un contracte.
        Mira que cap altre contracte el tingui en Alta i que cap procés ATR
        crític l'estigui utilitzant.
        :param cups_name: Valor del cups en string 'ES1618210323670632JF0F'
        :param context: No necessita res
        :return: True si el cups és contractable
        """
        if context is None:
            context = {}
        cups_id = self.search(cursor, uid, [('name', '=', cups_name)])
        if not cups_id:
            return True
        cups_id = cups_id[0]
        swi_obj = self.pool.get('giscedata.switching')
        processes = ['C1', 'C2', 'A3']
        no_process = not swi_obj.get_casos_actiu_cups_process(
            cursor, uid, cups_name, processes, context=context
        )
        no_active_contract = not self._get_polissa(cursor, uid, cups_id)
        return no_process and no_active_contract


GiscedataCupsPs()
