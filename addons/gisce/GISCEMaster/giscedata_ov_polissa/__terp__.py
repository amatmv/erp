# -*- coding: utf-8 -*-
{
  "name": "Contracts (OV)",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * OV Modifications for contracts
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "Master",
  "depends": [
      "giscedata_switching",
      "giscedata_polissa",
      "giscedata_ov"
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      "giscedata_ov_view.xml",
      "security/ir.model.access.csv"
  ],
  "active": False,
  "installable": True
}
