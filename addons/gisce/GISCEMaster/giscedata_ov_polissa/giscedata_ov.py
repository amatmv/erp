# coding=utf-8
from osv import osv, fields
from osv.orm import browse_record
from tools.translate import _


class OvUsersContracts(osv.osv):
    _name = 'ov.users.contracts'
    _columns = {
        'contract_id': fields.many2one(
            'giscedata.polissa', 'Contract', required=True, ondelete='cascade'
        ),
        'user_id': fields.many2one(
            'ov.users', 'User', required=True, ondelete='cascade'
        )
    }

OvUsersContracts()


class OvUsers(osv.osv):
    _name = 'ov.users'
    _inherit = 'ov.users'

    def serialize(self, cursor, uid, user, context=None):
        if context is None:
            context = {}
        if not isinstance(user, (browse_record, int)):
            raise ValueError('Invalid user parameter')
        if not isinstance(user, browse_record):
            user = self.browse(cursor, uid, user, context)
        data = super(OvUsers, self).serialize(cursor, uid, user, context)
        data.update({
            'allowed_contracts_mode': user.allowed_contracts_mode,
            'allowed_contracts_ids': [x.id for x in user.allowed_contracts_ids]
        })
        return data

    def asign_allowed_contracts_ids(self, cursor, uid, ids, contract_ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        overwrite = context.get("overwrite_allowed_contracts_ids")

        for ov_info in self.read(cursor, uid, ids, ['allowed_contracts_mode', 'allowed_contracts_ids']):

            if ov_info['allowed_contracts_mode'] != 'restricted':
                raise osv.except_osv("Error", _(u"No es poden asignar contractes a un usuari de OV sense mode 'restricted'"))

            old_contracts = []
            if not overwrite:
                old_contracts = ov_info['allowed_contracts_ids']

            new_contracts = contract_ids + old_contracts
            self.write(cursor, uid, [ov_info['id']], {'allowed_contracts_ids': [(6, 0, new_contracts)]})

        return True

    _columns = {
        'allowed_contracts_mode': fields.selection([
            ('all', 'All'),
            ('restricted', 'Restricted')
        ], 'Contracts mode'),
        'allowed_contracts_ids': fields.many2many(
            'giscedata.polissa', 'ov_users_contracts', 'user_id', 'contract_id',
            'Allowd contracts'
        )
    }

    _defaults = {
        'allowed_contracts_mode': lambda *a: 'all'
    }

OvUsers()
