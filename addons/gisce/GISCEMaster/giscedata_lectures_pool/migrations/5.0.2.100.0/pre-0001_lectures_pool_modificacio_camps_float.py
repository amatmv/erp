# coding=utf-8
import logging
from tqdm import tqdm
from oopgrade.oopgrade import column_exists, add_columns, drop_columns

COLUMNS = [
    ('ajust', 'NUMERIC(16, 3)'),
    ('ajust_exporta', 'NUMERIC(16, 3)'),
    ('consum', 'NUMERIC(16, 3)'),
    ('generacio', 'NUMERIC(16, 3)'),
]


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    columns_to_copy = []
    for col in tqdm(COLUMNS):
        field = col[0]
        if column_exists(cursor, 'giscedata_lectures_lectura_pool', field):
            cursor.execute('ALTER TABLE giscedata_lectures_lectura_pool RENAME COLUMN {field} TO {field}_int'
                           .format(field=field))
            logger.info('Column {field} already exists. Renaming it to {field}_int'
                        ' and creating a new decimal column'.format(field=field))
            columns_to_copy.append(col)
        else:
            logger.info('Column {} not found. Cannot copy its value to new float column'.format(field))

    if columns_to_copy:
        add_columns(cursor, {
            'giscedata_lectures_lectura_pool': columns_to_copy
        })
        logger.info('Copying integer columns to the new float ones'.format(field))
        cursor.execute('UPDATE giscedata_lectures_lectura_pool SET {columns}'
                       .format(columns=','.join(['{field} = {field}_int'.format(field=x[0]) for x in COLUMNS])))

        logger.info('Copied columns'.format(field))


def down(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    for col in COLUMNS:
        field = col[0]
        logger.info('Dropping column {} from giscedata_lectures_lectura_pool'
                    ' and renaming integer one'.format(field))
        drop_columns(cursor, [('giscedata_lectures_lectura_pool', field)])
        cursor.execute('ALTER TABLE giscedata_lectures_lectura_pool RENAME COLUMN {field}_int TO {field}'
                       .format(field=field))


migrate = up
