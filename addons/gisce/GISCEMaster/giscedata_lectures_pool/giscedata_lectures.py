# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from oorq.decorators import job
from tools.translate import _
from tools import config

import pooler
import netsvc

from datetime import datetime, timedelta
from oorq.decorators import split_job
from oorq.oorq import JobsPool
from osconf import config_from_environment


class PoolLecturesLectura(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.lectures.lectura.pool'
    _inherit = 'giscedata.lectures.lectura'

    def _trg_consum(self, cursor, uid, ids, context=None):

        return ids


    def _consum(self, cursor, uid, ids, field_name, arg, context=None):

        return dict([(i, False) for i in ids])

    def _generacio(self, cursor, uid, ids, field_name, arg, context=None):

        return dict([(i, False) for i in ids])

    _columns = {
        'consum': fields.function(_consum, method=True, string="Consum",
                                  type="float", digits=(16, 3),
                                  store={'giscedata.lectures.lectura.pool': (
                                         _trg_consum, ['lectura'], 10)}),
        'generacio': fields.function(_generacio, method=True, string="Generacio",
                                     type="float", digits=(16, 3),
                                     store={'giscedata.lectures.lectura.pool': (_trg_consum, ['lectura_exporta'], 10)})
    }


PoolLecturesLectura()


class PoolLecturesPotencia(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.lectures.potencia.pool'
    _inherit = 'giscedata.lectures.potencia'

PoolLecturesPotencia()


class PoolLecturesComptador(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_lectura_estimada(self, cursor, uid, ids, data, context=None):
        return []

    def dates_limit(self, cursor, uid, comptador_id, context=None):
        """
        Busquem els dies limit
        """
        conf_obj = self.pool.get('res.config')
        min_dies = int(conf_obj.get(cursor, uid,
                                    'pool_min_periode_entre_lectures', '0'))
        llindar_estimacio = int(conf_obj.get(cursor, uid,
                                             'pool_llindar_estimacio_lectures',
                                             '1000'))
        return min_dies, llindar_estimacio

    def select_better_origen(self, cursor, uid, lectures):
        """ De totes les lectures, escull les millors en funció de l'origen.
            Pot rebre una llista de lectures provinent d'un read o els ids
            de les lectures
            Retorna la data de la lectura(es) que es consideren vàlides
        """
        if not lectures:
            return []
        pool_lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        if isinstance(lectures[0], int):
            lectures = pool_lect_obj.read(cursor, uid, lectures)

        # diferents dates
        dates = list(set([l['name'] for l in lectures]))

        return dates

    def inserta_lectures(self, cursor, uid, lectures, maximetres,
                         pool=False, prefix_observacions='',
                         context=None):
        dies_lect = []
        lect_carregades = 0
        max_carregades = 0

        res = {'lectures': lect_carregades, 'maxims': max_carregades,
               'dies': dies_lect}

        if pool:
            # Carreguem les lectures al Pool de lectures
            lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
            max_obj = self.pool.get('giscedata.lectures.potencia.pool')
        else:
            # Carreguem les lectures al les lectures de facturació directament
            lect_obj = self.pool.get('giscedata.lectures.lectura')
            max_obj = self.pool.get('giscedata.lectures.potencia')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        for lect in lectures:
            observacions = prefix_observacions + (lect['observacions'] or '')
            # protecció gir de comptador
            id_comptador = lect['comptador'][0]
            meter_vals = meter_obj.read(cursor, uid, id_comptador, ['giro'])

            gir = int(meter_vals['giro'] or 0)
            lectura = lect['lectura']
            lectura_exporta = lect.get('lectura_exporta', 0)
            #Validem que la lectura sigui "coherent" amb el gir
            if gir <= lectura:
                gir = int('1' + '0' * len(str(lectura or 0)))
                meter_vals['giro'] = gir
                meter_obj.write(cursor, uid, id_comptador, {'giro': gir})
            if gir:
                lectura %= gir

            vals = {'comptador': id_comptador,
                    'name': lect['name'],
                    'periode': lect['periode'][0],
                    'tipus': lect['tipus'][0],
                    'lectura': lectura,
                    'lectura_exporta': lectura_exporta,
                    'observacions': observacions,
                    'origen_comer_id': lect['origen_comer_id'][0],
                    'origen_id': lect['origen_id'][0],
                    'ajust': lect.get('ajust', 0),
                    'ajust_exporta': lect.get('ajust_exporta', 0),
                    'motiu_ajust': lect.get('motiu_ajust', False),
                    }
            incidencia_id = (
                lect['incidencia_id'] and lect['incidencia_id'][0] or False
            )
            if incidencia_id:
                vals.update({'incidencia_id': incidencia_id})
            # comprovem que no hi és
            search_vals = [('comptador', '=', vals['comptador']),
                           ('name', '=', vals['name']),
                           ('periode', '=', vals['periode']),
                           ('tipus', '=', vals['tipus'])]
            existing_ids = lect_obj.search(cursor, uid, search_vals)
            if not existing_ids:
                dies_lect += [vals['name']]
                lect_carregades += 1
                lect_obj.create(cursor, uid, vals)
            else:
                # Si ja existeix, mirarem que no estiguem intentant carregar una lectura exporta.
                # Es possible que la lectura que ja existeix tingui el valor de la lectura normal emplenat pero no el de
                # la lectura exporta. En aquest cas escriurem el valor de lectura_exporta a la lectura existent.
                # Tambe pot ser el cas invers: tenim una lectra amb nomes el lectura_exporta emplenat. En aquest cas
                # emplenariem la lectura normal.
                lectura_actuals = lect_obj.read(cursor, uid, existing_ids, [])
                for lectura_actual in lectura_actuals:

                    vals_to_update = {
                        'observacions': (lect.get('observacions') or '') + "\n" + (lectura_actual['observacions'] or '').strip(),
                    }
                    if not lectura_actual.get('motiu_ajust') and lect.get('motiu_ajust'):
                        vals_to_update['motiu_ajust'] = lect['motiu_ajust']

                    if not lectura_actual['lectura_exporta'] and not lectura_actual['ajust_exporta'] and lectura_actual['lectura'] == lect.get('lectura') and lectura_actual['ajust'] == lect.get('ajust', 0):
                        vals_to_update.update({
                            'lectura_exporta': lect.get('lectura_exporta'),
                            'ajust_exporta': lect.get('ajust_exporta', 0)
                        })
                        lect_obj.write(cursor, uid, [lectura_actual['id']], vals_to_update, context=context)

                    elif not lectura_actual['lectura'] and not lectura_actual['ajust'] and lectura_actual['lectura_exporta'] == lect.get('lectura_exporta') and lectura_actual['ajust_exporta'] == lect.get('ajust_exporta', 0):
                        vals_to_update.update({
                            'lectura': lect.get('lectura'),
                            'ajusta': lect.get('ajust')
                        })
                        lect_obj.write(cursor, uid, [lectura_actual['id']], vals_to_update, context=context)

        for maxim in maximetres:
            observacions = prefix_observacions + (maxim['observacions']
                                            or '')
            vals = {'comptador': maxim['comptador'][0],
                    'name': maxim['name'],
                    'periode': maxim['periode'][0],
                    'exces': maxim['exces'],
                    'lectura': maxim['lectura'],
                    'observacions': observacions,
                    'origen_comer_id': maxim['origen_comer_id'][0],
                    }
            incidencia_id = (maxim['incidencia_id']
                             and maxim['incidencia_id'][0]
                             or False)
            if incidencia_id:
                vals.update({'incidencia_id': incidencia_id})
            #comprovem que no hi és
            search_vals = [('comptador', '=', vals['comptador']),
                           ('name', '=', vals['name']),
                           ('periode', '=', vals['periode']),
                           ]
            if not max_obj.search(cursor, uid, search_vals):
                max_carregades += 1
                max_obj.create(cursor, uid, vals)

        res.update({'lectures': lect_carregades, 'maxims': max_carregades,
                    'dies': list(set(dies_lect))})

        return res

    @split_job(queue='import_xml', result_ttl=24 * 3600)
    def get_lectures_from_pool(self, cursor, uid, ids, for_date=None,
                               context=None):
        """Busca les lectures del pool a carregar segons un seguit de criteris
           establerts. De moment, totes les que faltin fins a la data passada"""

        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        logger = netsvc.Logger()

        polissa_obj = self.pool.get('giscedata.polissa')
        lect_obj = self.pool.get('giscedata.lectures.lectura')
        pool_lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        pool_max_obj = self.pool.get('giscedata.lectures.potencia.pool')
        conf_obj = self.pool.get('res.config')

        # Controla si carreguem lectures noves o no (1:no, 0:si)
        if context.get('allow_new_measures', False):
            no_new_lect = False
        else:
            no_new_lect = int(conf_obj.get(
                cursor, uid, 'pool_no_lectures_noves_per_facturar', '0')
            )

        if not for_date:
            for_date = datetime.today()
        else:
            for_date = datetime.strptime(for_date, '%Y-%m-%d')

        res = {'num_comptadors': len(ids), 'estimades': 0, 'carregades': 0,
               'for_date': for_date.strftime('%Y-%m-%d'), 'errors': 0,
               'comptadors': dict([(c, '') for c in ids])}

        comptadors_dades = self.read(cursor, uid, ids, ['polissa', 'name',
                                                        'data_baixa', 'active'])
        processats = 0
        for comptador_dades in comptadors_dades:
            db = pooler.get_db_only(cursor.dbname)
            nou_cr = db.cursor()
            try:
                comptador_id = comptador_dades['id']
                res['comptadors'][comptador_id] = ('%s: ' %
                                                   comptador_dades['name'])
                polissa_id = comptador_dades['polissa'][0]
                polissa_dades = polissa_obj.read(nou_cr, uid, polissa_id,
                                                 ['tarifa',
                                                  'data_ultima_lectura',
                                                  'data_alta', 'data_baixa',
                                                  'state', 'name'])

                min_dies, llindar_estimacio = self.dates_limit(
                    cursor, uid, comptador_id, context=context)

                if polissa_dades['state'] == 'esborrany':
                    txt = _(u'Polissa %s en esborrany') % polissa_dades['name']
                    res['comptadors'][comptador_id] += txt
                    continue

                dalta = datetime.strptime(polissa_dades['data_alta'], "%Y-%m-%d")
                dalta = dalta - timedelta(days=1)
                dalta = dalta.strftime("%Y-%m-%d")
                data_ult_lect_facturada = (
                    context.get('data_ultima_lectura', False) or
                    polissa_dades['data_ultima_lectura'] or
                    dalta
                )

                """Cerquem lectures de com a mínim min_dies dies posteriors a
                    la última lectura"""
                inici_cerca = (datetime.strptime(data_ult_lect_facturada,
                                                 '%Y-%m-%d')
                               + timedelta(days=min_dies)).strftime('%Y-%m-%d')

                """A partir de llindar_estimacio dies des de la última lectura
                   de la tarifa actual, es generarà una lectura estimada"""
                inici_estimacio = (datetime.strptime(data_ult_lect_facturada,
                                                     '%Y-%m-%d') +
                                   timedelta(days=llindar_estimacio)
                                   ).strftime('%Y-%m-%d')

                if inici_cerca > for_date.strftime('%Y-%m-%d'):
                    #Encara no cal carregar cap lectura
                    txt = _(u'Encara no han passat el mínim de dies (%s) des '
                            u'de la última lectura facturada' % min_dies)
                    res['comptadors'][comptador_id] += txt
                    continue

                # Mirem les modificacions contractuals per veure si tenim algun
                # canvi de tarifa.
                modcons = polissa_obj.get_modcontractual_intervals(
                    cursor, uid, polissa_id, data_ult_lect_facturada, for_date,
                    context={'ffields': ['tarifa', 'potencia']})

                if no_new_lect and len(modcons) < 2:
                    # Mirem si tenim lectures de facturació no facturades i si
                    # en tenim, no carreguem lectures. Aquesta comprovació
                    # nomes la podem fer si no hi ha canvi de tarifa, en un
                    # canvi de tarifa interessa carregar lectures si o si.
                    search_params = [
                        ('comptador.id', '=', comptador_id),
                        ('name', '>', data_ult_lect_facturada)
                    ]
                    lect_ids = lect_obj.search(nou_cr, uid, search_params,
                                               context={'active_test': False})
                    if lect_ids:
                        txt = _(u"S'han trobat %s lectures no facturades i "
                                u"no es permet carregar-ne de noves" %
                                len(lect_ids))
                        res['comptadors'][comptador_id] += txt
                        continue

                inici_carrega = inici_cerca
                if modcons > 1:
                    #Busquem totes les tarifes de l'interval
                    for modcon in modcons.items():
                        '''On acaba la modificació contractual'''
                        data_tarifa = modcon[1]['dates'][1]
                        # Quan creem les lectures, buscarem a partir d'aquesta
                        # data
                        inici_carrega = min(data_tarifa, inici_carrega)

                search_params = [
                    ('comptador.id', '=', comptador_id),
                    ('name', '>=', inici_carrega),
                    ('name', '<=', for_date.strftime('%Y-%m-%d')),
                    ('name', '<=', inici_estimacio),
                ]

                lect_ids = pool_lect_obj.search(nou_cr, uid, search_params,
                                                context={'active_test': False})

                lectures = pool_lect_obj.read(nou_cr, uid, lect_ids, [])
                lect_dins_interval = [l for l in lectures
                                      if (inici_cerca <= l['name'] <=
                                          inici_estimacio)]
                dates_ok_carga = self.select_better_origen(nou_cr, uid,
                                                     lect_dins_interval)

                dates_dins_interval = [d for d in dates_ok_carga
                                       if inici_cerca <= d <= inici_estimacio]

                # Agafem totes les lectures excepte les extretes de dins
                # l'interval de càrrega
                dates_ok = [l['name'] for l in lectures
                            if l['name'] < inici_cerca]
                dates_ok += dates_ok_carga

                ''' Si no trobem lectura i ja hem passat la data limit
                    d'estimació inici_estimacio, estimem la lectura '''
                lectures_estimades = []
                if (not dates_dins_interval
                        and polissa_dades['state'] not in ['baixa']
                        and comptador_dades['active']):

                    if for_date.strftime('%Y-%m-%d') >= inici_estimacio:
                        '''Estimem lectura'''
                        # Cerquem qualsevo[l lectura encara que estigui en el
                        # futur (fins a for_date)
                        lectures_estimades = self.get_lectura_estimada(cursor,
                            uid, comptador_id, for_date.strftime('%Y-%m-%d'))

                res['estimades'] += len(lectures_estimades)

                lectures = [l for l in lectures if l['name'] in dates_ok]
                lectures += lectures_estimades
                # Ordenem per dates. No faria falta però és més òptim
                sorted(lectures, key=lambda lectura: lectura['name'])
                #Els maxímetres (si en tenim)
                search_params = [
                    ('comptador.id', '=', comptador_id),
                    ('name', 'in', dates_ok),
                ]
                max_ids = pool_max_obj.search(nou_cr, uid, search_params,
                                              context={'active_test': False})
                maxims = pool_max_obj.read(nou_cr, uid, max_ids, [])
                #Ara ja podem crear les lectures
                prefix = u"From Pool."
                ins_res = self.inserta_lectures(nou_cr, uid, lectures, maxims,
                                                prefix_observacions=prefix)
                res['carregades'] += ins_res['lectures']

                if ins_res['lectures'] > 0:
                    txt_tmpl = _(u"Carregades %s lectures i %s maxímetres pels "
                                 u"dies '%s' (%s estimades)")
                    txt = txt_tmpl % (ins_res['lectures'], ins_res['maxims'],
                                      ','.join(list(set(ins_res['dies']))),
                                      len(lectures_estimades))
                else:
                    txt = _(u"No s'han carregat lectures")
                res['comptadors'][comptador_id] += txt
                nou_cr.commit()
            except Exception, e:
                txt_tmpl = _(u"Error carregant lectures a '%s': %s")
                txt = txt_tmpl % (comptador_dades['name'], str(e))
                res['comptadors'][comptador_id] += txt
                res['errors'] += 1
                nou_cr.rollback()
            finally:
                nou_cr.close()
                processats += 1
                marcador = "[%d/%d] " % (processats, len(res['comptadors']))
                logger.notifyChannel('carrega_lectures', netsvc.LOG_INFO,
                                     marcador + res['comptadors'][comptador_id])
        return res

    def processa_resultat_carrega(self, cursor, uid, ids, dades):

        if not dades:
            return ''

        txt_head = _(u'Data processat: %(for_date)s\n'
                     u'Processats %(num_comptadors)d comptadors\n'
                     u'Errors: %(errors)d\n'
                     u'Carregades: %(carregades)d\n'
                     u'Lectures estimades: %(estimades)d/%(num_comptadors)d\n'
                     u'Detall:\n\n') % dades

        detall = ''
        for comptador in dades['comptadors'].items():
            detall_tmp = _(u"* ['%s']: %s\n") % comptador
            detall += detall_tmp

        return txt_head + detall

    def _cronjob_lectures_load(self, cursor, uid, data=None, context=None):

        if not context:
            context = {}
        if not data:
            data = {}

        emails_to = filter(lambda a: bool(a),
                           map(str.strip, data.get('emails_to', '').split(',')))
        requests_to = filter(lambda a: bool(a),
                             map(str.strip,
                                 data.get('requests_to', '').split(',')))

        logger = netsvc.Logger()

        lot_obj = self.pool.get('giscedata.facturacio.lot')
        polissa_obj = self.pool.get('giscedata.polissa')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        conf_obj = self.pool.get('res.config')

        #Busquem el lot actual
        lot_id = lot_obj.search(cursor, uid, [('state', '=', 'obert')])[0]
        lot_dades = lot_obj.read(cursor, uid, lot_id,
                                 ['data_inici', 'data_final', 'name',
                                  'contracte_lot_ids'])
        # Totes les pólisses dins el lot amb data_ultima_lectura abans de
        # min_dies o sense data última factura
        min_dies = int(conf_obj.get(cursor, uid,
                                    'pool_min_periode_entre_lectures', '1'))
        limit = (datetime.today() - timedelta(days=min_dies)).strftime("%Y-%m-%d")
        polisses_ids = polissa_obj.search(cursor, uid,
                                          [('lot_facturacio', '=', lot_id),
                                           ('state', 'not in', ['esborrany']),
                                           ],
                                           context={'active_test': False})

        search_vals = [('polissa', 'in', polisses_ids)]
        comptadors_ids = self.search(cursor, uid, search_vals,
                                     context={'active_test': False})

        logger.notifyChannel('carrega_lectures', netsvc.LOG_INFO,
                             (u'Carregant lectures per %s comptadors' %
                              len(comptadors_ids)))
        async_mode = config_from_environment('OORQ').get('async', True)
        if async_mode:
            j_pool = JobsPool()
            jobs = self.get_lectures_from_pool(cursor, uid, comptadors_ids)
            for j in jobs:
                j_pool.add_job(j)
            j_pool.join()
            res = {}
        else:
            res = self.get_lectures_from_pool(
                cursor, uid, comptadors_ids
            ).result
        txt = self.processa_resultat_carrega(cursor, uid, comptadors_ids, res)

        subject_tmpl = _(u"Càrrega de lectures pel lot '%s' pel dia %s")
        subject = subject_tmpl % (lot_dades['name'], res['for_date'])

        message = subject + '\n'
        message += ('=' * len(subject)) + '\n\n'
        message += txt

        logger.notifyChannel('carrega_lectures', netsvc.LOG_INFO, txt)

        modcon_obj.send_action_message(cursor, uid, emails_to, requests_to,
                                       subject, message)

        return True

    _columns = {
        'pool_lectures': fields.one2many('giscedata.lectures.lectura.pool',
                                         'comptador', u'Pool Lectures'),
        'pool_lectures_pot': fields.one2many('giscedata.lectures.potencia.pool',
                                             'comptador',
                                             u'Pool Lectures Potència'),
    }

PoolLecturesComptador()
