# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _


class GiscedataFacturacioContracteLot(osv.osv):

    _name = "giscedata.facturacio.contracte_lot"
    _inherit = "giscedata.facturacio.contracte_lot"

    def get_text_validate_polissa_error(self, cursor, uid, clot_id, context=None):

        res = super(GiscedataFacturacioContracteLot, self).get_text_validate_polissa_error(
            cursor, uid, clot_id, context=context
        )

        polissa_o = self.pool.get('giscedata.polissa')
        lectura_pool_obj = self.pool.get('giscedata.lectures.lectura.pool')

        clot_v = self.read(cursor, uid, clot_id, ['polissa_id'], context=context)
        polissa_id = clot_v['polissa_id'][0]

        comptador_actiu_id = polissa_o.get_ultim_comptador(
            cursor, uid, [polissa_id], context=context
        )[polissa_id]
        no_value = '-'
        ultima_lectura_pool_id = False

        if comptador_actiu_id:
            ultima_lectura_pool_id = lectura_pool_obj.search(
                cursor, uid, [('comptador', '=', comptador_actiu_id)], limit=1,
                order="name desc", context=context
            )

        ultima_lectura_pool_vals = {}
        ultima_lectura_pool_origen_comer = no_value

        if ultima_lectura_pool_id:
            params = ['name', 'origen_comer_id']
            ultima_lectura_pool_vals = lectura_pool_obj.read(
                cursor, uid, ultima_lectura_pool_id, params,
                context=context
            )[0]
            ultima_lectura_pool_origen_comer = ultima_lectura_pool_vals['origen_comer_id'][1]

        ultima_lectura_pool_f1 = _(
            "Última lectura de pool amb origen {0}: {1}"
        ).format(
            ultima_lectura_pool_origen_comer,
            ultima_lectura_pool_vals.get('name', no_value)
        )

        res = '\n'.join([res, ultima_lectura_pool_f1])

        return res


GiscedataFacturacioContracteLot()
