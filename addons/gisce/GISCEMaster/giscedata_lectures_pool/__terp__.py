# -*- coding: utf-8 -*-
{
    "name": "Pool de Lectures",
    "description": """
    Mòdul que permet crear lectures en el pool que després es poden carregar en
    el comptador real per facturar. Ens permetria coses com:
      * Autolectures sense interferir en facturació
      * Carregar F1 i Q1 amb validació més senzilla i sense interferir amb les
        lectures de facturació
      * Crear algoritmes de càrrega de lectures (reals o estimades) a partir de
        les lectures del pool i crear un procés de càrrega de lectures quan es
        factura per lots
    """,
    "version": "0-dev",
    "author": "Gisce-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_lectures",
        "giscedata_lectures_comer",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": ["giscedata_lectures_pool_demo.xml"],
    "update_xml":[
        "giscedata_lectures_data.xml",
        "wizard/giscedata_lectures_pool_wizard.xml",
        "giscedata_lectures_view.xml",
        "security/ir.model.access.csv",
        "giscedata_lectures_pool_cronjobs.xml"
    ],
    "active": False,
    "installable": True
}
