# -*- coding: utf-8 -*-

from osv import osv, fields, orm


class GiscedataPolissa(osv.osv):
    """" Amplicació amb el progres de càrrega de lectures des del pool"""

    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    def get_comptadors(self, cursor, uid, ids, context=None):
        """ Retorna els comptadors associats a la/les pólisses"""
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        search_vals = [('polissa', 'in', ids)]
        comptadors_ids = comptador_obj.search(cursor, uid, search_vals,
                                              context={'active_test': False})

        return comptadors_ids

GiscedataPolissa()