# -*- coding: utf-8 -*-

from destral import testing
from destral.patch import PatchNewCursors
from destral.transaction import Transaction
from expects import *

from ..defs import *

from datetime import datetime
from dateutil.relativedelta import relativedelta


class TestGiscedataLectures(testing.OOTestCase):

    def prepare_contract_for_test_pool(self, cursor, uid, data_ultima_lectura='2017-01-01'):
        contract_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        self.data_ultima_lectura = data_ultima_lectura

        # Preparem i activem un contracte
        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', "polissa_0001"
        )[1]
        self.contract = contract_obj.browse(cursor, uid, contract_id)
        self.contract.write({
            'data_alta': '2016-01-01', 'data_ultima_lectura': data_ultima_lectura
        })
        self.contract.send_signal(['validar', 'contracte'])

        # Només ens interesa treballar amb un comptador per tant
        # desactivem els altres si n'hi ha
        for meter in self.contract.comptadors:
            for lect in meter.lectures:
                lect.unlink(context={})
            for lect in meter.lectures_pot:
                lect.unlink(context={})
            for lect in meter.pool_lectures:
                lect.unlink(context={})
            for lect in meter.pool_lectures_pot:
                lect.unlink(context={})
            meter.write({
                'data_alta': self.contract.data_alta,
                'data_baixa': self.contract.data_alta, 'active': False
            })
        self.comptador_actiu = self.contract.comptadors[0]
        self.comptador_actiu.write({
            'data_baixa': False, 'active': True
        })

        # Hi posem lectures a pool i a facturació <= que data_ultima_lectura
        self.periode_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
        )[1]
        self.origen_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen10'
        )[1]
        lect_pool_obj.create(
            cursor, uid, {
                'name': data_ultima_lectura, 'lectura': 1000,
                'comptador': self.comptador_actiu.id,
                'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
            }
        )
        lect_obj.create(
            cursor, uid, {
                'name': data_ultima_lectura, 'lectura': 1000,
                'comptador': self.comptador_actiu.id,
                'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
            }
        )
        self.contract = contract_obj.browse(cursor, uid, contract_id)
        self.comptador_actiu = self.comptador_actiu.browse()[0]

    def crear_modcon(self, txn, ini, fi, tariff_id):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def test_inserta_lectures_copies_ajust_value(self):
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        compt_pool_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            (periode_name, periode_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )
            (comptador_name, comptador_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )

            (origen_name, origen_id) = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )

            vals = {
                'name': '2016-01-01',
                'periode': (periode_id, periode_name),
                'lectura': 10,
                'lectura_exporta': 20,
                'tipus': 'A',
                'comptador': (comptador_id, comptador_name),
                'observacions': '',
                'origen_comer_id': [None],
                'origen_id': (origen_id, origen_name),
                'ajust': 5,
                'ajust_exporta': 8,
                'motiu_ajust': MOTIUS_AJUST[0][0],
                'incidencia_id': False,
            }

            res = compt_pool_obj.inserta_lectures(cursor, uid, [vals], [])
            assert res['lectures'] == 1
            lectura_id = lectura_obj.search(
                cursor, uid, [
                    ('name', '=', '2016-01-01'),
                    ('periode', '=', periode_id),
                    ('lectura', '=', 10),
                    ('lectura_exporta', '=', 20),
                    ('tipus', '=', 'A'),
                    ('comptador', '=', comptador_id),
                    ('observacions', '=', ''),
                    ('origen_id', '=', origen_id),
                    ('ajust', '=', 5),
                    ('ajust_exporta', '=', 8),
                    ('motiu_ajust', '=', MOTIUS_AJUST[0][0]),
                ]
            )
            assert len(lectura_id) == 1
            lectura = lectura_obj.browse(cursor, uid, lectura_id[0])

            assert lectura.lectura == 10
            assert lectura.ajust == 5
            assert lectura.lectura_exporta == 20
            assert lectura.ajust_exporta == 8
            assert lectura.motiu_ajust == MOTIUS_AJUST[0][0]

    def test_creating_giscedata_lectures_lectura_pool_has_default_ajust_0(self):
        lectura_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            comptador_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            vals = {
                'name': '2016-01-01',
                'periode': periode_id,
                'lectura': 10,
                'tipus': 'A',
                'comptador': comptador_id,
                'observacions': '',
                'origen_id': origen_id,
            }

            lectura_id = lectura_obj.create(cursor, uid, vals)
            lectura = lectura_obj.browse(cursor, uid, lectura_id)

            assert lectura.lectura_exporta == 0
            assert lectura.lectura == 10

            assert lectura.ajust == 0
            assert lectura.ajust_exporta == 0

            assert not lectura.motiu_ajust

    def test_carregar_lectures_de_pool(self):

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            with PatchNewCursors():

                # Obtenim el comptador_id en el que carregarem les
                # lectures de pool
                imd_obj = self.openerp.pool.get('ir.model.data')
                comptador_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_lectures', 'comptador_0001'
                )[1]

                comptador_obj = self.openerp.pool.get(
                    'giscedata.lectures.comptador')
                polissa_id = comptador_obj.read(
                    cursor, uid, comptador_id, ['polissa'])['polissa'][0]
                polisses_obj = self.openerp.pool.get('giscedata.polissa')
                polisses_obj.write(cursor, uid, polissa_id, {'data_ultima_lectura': False})
                polisses_obj.send_signal(
                    cursor, uid, [polissa_id], ['validar', 'contracte']
                )

                # Eliminem les lectures de facturacio posteriors a la lectura de pool
                lectura_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_lectures_pool', 'lectura_pool_0001'
                )[1]
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                data_lectura_pool = lect_pool_obj.read(cursor, uid, lectura_id, ['name'])['name']
                for l in comptador_obj.browse(cursor, uid, comptador_id).lectures:
                    if l.name >= data_lectura_pool:
                        l.unlink(context={})

                # Obtenim la data de l'última lectura del comptador
                lectures = comptador_obj.browse(
                    cursor, uid, comptador_id).lectures
                data_ultima_lectura_inicial = sorted(
                    lectures, key=lambda l: l['name'], reverse=True)[0]['name']

                # Data de càrrega de les lectures
                data_carrega = (datetime.strptime(
                    data_ultima_lectura_inicial, '%Y-%m-%d'
                ).date() + relativedelta(months=2)).strftime('%Y-%m-%d')

                # Carreguem les lectures de pool
                comptador_obj.get_lectures_from_pool(
                    cursor, uid, [comptador_id], data_carrega, context={
                        'allow_new_measures': True,
                        'data_ultima_lectura': data_ultima_lectura_inicial
                    }
                )

                # Obtenim la data de la lectura del pool de lectures
                lectura_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_lectures_pool',
                    'lectura_pool_0001'
                )[1]
                lect_pool_obj = self.openerp.pool.get(
                    'giscedata.lectures.lectura.pool')
                data_lectura_pool = lect_pool_obj.read(
                    cursor, uid, lectura_id, ['name'])['name']

                # Obtenim les lectures del comptador
                lectures = comptador_obj.browse(
                    cursor, uid, comptador_id).lectures
                data_nova_lectura = sorted(
                    lectures, key=lambda l: l['name'], reverse=True)[0]['name']

                # Comprovem que les dues dates són iguals
                expect(data_nova_lectura).to(equal(data_lectura_pool))
                # Comprovem que la data de la nova lectura és més gran que
                # la data de l'última lectura inicial
                expect(data_nova_lectura).to(
                    be_above(data_ultima_lectura_inicial))

    def test_lpool_no_carrega_anteriors_a_ultima_facturada(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                wiz_obj = self.openerp.pool.get('giscedata.lectures.pool.wizard')

                # Preparem i activem un contracte
                self.prepare_contract_for_test_pool(cursor, uid)

                # En posem de mes anteriors a pool que no s'haurien de
                # carregar perque a facturació n'hi ha de mes recents carregades
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2016-12-01', 'lectura': 0,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
                    }
                )
                ctx = {
                    'model': 'giscedata.lectures.comptador',
                    'active_ids': [self.comptador_actiu.id],
                    'data_ultima_lectura': self.data_ultima_lectura
                }
                self.assertEqual(len(self.comptador_actiu.lectures), 1)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 2)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 1)

    def test_lpool_no_carrega_posteriors_a_ultima_facturada_amb_pool_no_lectures_noves_per_facturar_activat(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                conf_obj = self.openerp.pool.get("res.config")
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                wiz_obj = self.openerp.pool.get('giscedata.lectures.pool.wizard')

                # Preparem i activem un contracte
                self.prepare_contract_for_test_pool(cursor, uid)

                # Activem variable que impedeix carrega de pool quan n'hi ha de no
                # facturades a facturacio
                config_id = conf_obj.search(cursor, uid, [('name', '=', 'pool_no_lectures_noves_per_facturar')])
                conf_obj.write(cursor, uid, config_id, {'value': '1'})

                # En posem a pool que s'haurien de carregar perque son posteriors
                # a data_ultima_lectura
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 2000,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                ctx = {
                    'model': 'giscedata.lectures.comptador',
                    'active_ids': [self.comptador_actiu.id],
                    'data_ultima_lectura': self.data_ultima_lectura
                }
                self.assertEqual(len(self.comptador_actiu.lectures), 1)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 2)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 2)

                # En posem de mes posteriors a pool que no s'haurien de carregar
                # perque hi ha lectures a facturacio no facturades
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-16', 'lectura': 2000,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id,
                        'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 2)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 3)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 2)

    def test_lpool_carrega_lectures_tarifa_nova_i_antigues(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                conf_obj = self.openerp.pool.get("res.config")
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                wiz_obj = self.openerp.pool.get('giscedata.lectures.pool.wizard')
                imd_obj = self.openerp.pool.get('ir.model.data')

                # Preparem i activem un contracte
                self.prepare_contract_for_test_pool(cursor, uid)
                # Fem una modcon a dia 2017-01-16 de canvi de tarifa
                tarifa_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
                )[1]
                self.crear_modcon(txn, "2017-01-16", "2019-01-01", tarifa_id)

                # Activem variable que impedeix carrega de pool quan n'hi ha de no
                # facturades a facturacio
                config_id = conf_obj.search(cursor, uid, [('name', '=', 'pool_no_lectures_noves_per_facturar')])
                conf_obj.write(cursor, uid, config_id, {'value': '1'})

                # En posem a pool per la tarifa antiga que es carregaran.
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 2000,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                ctx = {
                    'model': 'giscedata.lectures.comptador',
                    'active_ids': [self.comptador_actiu.id],
                    'data_ultima_lectura': self.data_ultima_lectura
                }
                self.assertEqual(len(self.comptador_actiu.lectures), 1)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 2)

                # En posem a pool per la nova tarifa que també s'haurien de carregar
                periode2_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHA_new'
                )[1]
                periode3_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHA_new'
                )[1]
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 1500,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': periode2_id,
                        'origen_id': self.origen_id,
                    }
                )
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 500,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': periode3_id,
                        'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 4)

    def test_lpool_carrega_lectures_tarifa_nova_amb_antigues_ja_carregades(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                conf_obj = self.openerp.pool.get("res.config")
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                wiz_obj = self.openerp.pool.get('giscedata.lectures.pool.wizard')
                imd_obj = self.openerp.pool.get('ir.model.data')

                # Preparem i activem un contracte
                self.prepare_contract_for_test_pool(cursor, uid)
                # Fem una modcon a dia 2017-01-16 de canvi de tarifa
                tarifa_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
                )[1]
                self.crear_modcon(txn, "2017-01-16", "2019-01-01", tarifa_id)

                # Activem variable que impedeix carrega de pool quan n'hi ha de no
                # facturades a facturacio
                config_id = conf_obj.search(cursor, uid, [('name', '=', 'pool_no_lectures_noves_per_facturar')])
                conf_obj.write(cursor, uid, config_id, {'value': '1'})

                # En posem a pool per la tarifa antiga que es carregaran.
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 2000,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                ctx = {
                    'model': 'giscedata.lectures.comptador',
                    'active_ids': [self.comptador_actiu.id],
                    'data_ultima_lectura': self.data_ultima_lectura
                }
                self.assertEqual(len(self.comptador_actiu.lectures), 1)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 2)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 2)

                # En posem a pool per la nova tarifa que també s'haurien de carregar
                periode2_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHA_new'
                )[1]
                periode3_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHA_new'
                )[1]
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 1500,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': periode2_id,
                        'origen_id': self.origen_id,
                    }
                )
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 500,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': periode3_id,
                        'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 2)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 4)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 4)

    def test_lpool_carrega_lectures_tarifa_antiga_amb_noves_ja_carregades(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                conf_obj = self.openerp.pool.get("res.config")
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                wiz_obj = self.openerp.pool.get('giscedata.lectures.pool.wizard')
                imd_obj = self.openerp.pool.get('ir.model.data')

                # Preparem i activem un contracte
                self.prepare_contract_for_test_pool(cursor, uid)
                # Fem una modcon a dia 2017-01-16 de canvi de tarifa
                tarifa_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
                )[1]
                self.crear_modcon(txn, "2017-01-16", "2019-01-01", tarifa_id)

                # Activem variable que impedeix carrega de pool quan n'hi ha de no
                # facturades a facturacio
                config_id = conf_obj.search(cursor, uid, [('name', '=', 'pool_no_lectures_noves_per_facturar')])
                conf_obj.write(cursor, uid, config_id, {'value': '1'})

                # En posem a pool per la nova tarifa que s'haurien de carregar
                periode2_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20DHA_new'
                )[1]
                periode3_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_polissa', 'p2_e_tarifa_20DHA_new'
                )[1]
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 1500,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': periode2_id,
                        'origen_id': self.origen_id,
                    }
                )
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 500,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': periode3_id,
                        'origen_id': self.origen_id,
                    }
                )

                self.comptador_actiu = self.comptador_actiu.browse()[0]
                ctx = {
                    'model': 'giscedata.lectures.comptador',
                    'active_ids': [self.comptador_actiu.id],
                    'data_ultima_lectura': self.data_ultima_lectura
                }
                self.assertEqual(len(self.comptador_actiu.lectures), 1)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 3)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 3)

                # En posem a pool per la tarifa antiga que tambe es carregaran.
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 2000,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
                    }
                )
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 3)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 4)
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 4)

    def test_lpool_carrega_lectures_exporta_amb_lectures_normals_ja_carregades(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                conf_obj = self.openerp.pool.get("res.config")
                lect_pool_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
                wiz_obj = self.openerp.pool.get('giscedata.lectures.pool.wizard')
                imd_obj = self.openerp.pool.get('ir.model.data')

                # Preparem i activem un contracte
                self.prepare_contract_for_test_pool(cursor, uid)

                # Activem variable que impedeix carrega de pool quan n'hi ha de no
                # facturades a facturacio
                config_id = conf_obj.search(cursor, uid, [('name', '=', 'pool_no_lectures_noves_per_facturar')])
                conf_obj.write(cursor, uid, config_id, {'value': '1'})

                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 1)
                self.assertEqual(len(self.comptador_actiu.pool_lectures), 1)

                # Modifiquem la lectura que ja existeix cambiant el valor de la lectura exporta
                # i en creem una de mes posterior
                self.comptador_actiu.pool_lectures[0].write({
                    'lectura_exporta': 150,
                    'ajust_exporta': -50
                })
                lect_pool_obj.create(
                    cursor, uid, {
                        'name': '2017-01-15', 'lectura': 2000, 'lectura_exporta': 200,
                        'comptador': self.comptador_actiu.id,
                        'tipus': 'A', 'periode': self.periode_id, 'origen_id': self.origen_id,
                    }
                )
                # Carreguem de pool. Ha de modificar la original perque tenia lectura_exporta a 0 i ha de carregar
                # la nova
                ctx = {
                    'model': 'giscedata.lectures.comptador',
                    'active_ids': [self.comptador_actiu.id],
                    'data_ultima_lectura': self.data_ultima_lectura
                }
                wiz_id = wiz_obj.create(cursor, uid, {}, context=ctx)
                wiz_obj.action_carrega_lectures(cursor, uid, [wiz_id], context=ctx)
                self.comptador_actiu = self.comptador_actiu.browse()[0]
                self.assertEqual(len(self.comptador_actiu.lectures), 2)
                self.assertEqual(self.comptador_actiu.lectures[1].lectura, 1000)
                self.assertEqual(self.comptador_actiu.lectures[1].ajust, 0)
                self.assertEqual(self.comptador_actiu.lectures[1].lectura_exporta, 150)
                self.assertEqual(self.comptador_actiu.lectures[1].ajust_exporta, -50)
                self.assertEqual(self.comptador_actiu.lectures[0].lectura, 2000)
                self.assertEqual(self.comptador_actiu.lectures[0].ajust, 0)
                self.assertEqual(self.comptador_actiu.lectures[0].lectura_exporta, 200)
                self.assertEqual(self.comptador_actiu.lectures[0].ajust_exporta, 0)
                # Ara canviem el valor de pool de la lectura i tornem a carregar de pool. Com que ara el valor de la
                # lectura_exporta no es 0, no s'hauria de actualitzar res a les lectures de facturacio
