from destral import testing
from destral.transaction import Transaction
from expects import *


class TgPrefixTests(testing.OOTestCase):

    def setUp(self):
        self.meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        self.carct_obj = self.openerp.pool.get('product.caracteristica')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.meter_id = self.meter_obj.search(cursor, uid, [('name', '=', '36301516')])

    def test_using_default_code(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # With Serial
            res_conf_obj = self.openerp.pool.get('res.config')
            res_conf_obj.set(cursor, uid, 'tg_comptador_prefix_origen', '0')
            meter = self.meter_obj.browse(cursor, uid, self.meter_id[0])
            prefix = meter.get_tg_prefix()
            expect(prefix).to(equal('ZIV'))
            # Without Serial
            meter.write({'serial': False})
            prefix = meter.get_tg_prefix()
            expect(prefix).to(equal(''))

    def test_using_characteristic_value(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            res_conf_obj = self.openerp.pool.get('res.config')
            res_conf_obj.set(cursor, uid, 'tg_comptador_prefix_origen', '1')
            meter = self.meter_obj.browse(cursor, uid, self.meter_id[0])
            # With Serial
            self.carct_obj.create(cursor, uid, {
                'name': 'tg_prefix',
                'value': 'ZIV',
                'product_id': meter.serial.product_id.id
            })
            prefix = meter.get_tg_prefix()
            expect(prefix).to(equal('ZIV'))
            # Without Serial
            meter.write({'serial': False})
            prefix = meter.get_tg_prefix()
            expect(prefix).to(equal(''))
