# coding=utf-8

import logging
from oopgrade.oopgrade import column_exists, add_columns, drop_columns


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Create new characteristics of tg_prefix for the existing meter'
                'products')
    query = """
            INSERT INTO product_caracteristica
        (product_id, name, value, description)
        (
          SELECT prod.id as id, 'tg_prefix' as name, prod.default_code as value, 'INTERNAL - TG Prefix to build Meter TG Name' as descr
          FROM giscedata_lectures_comptador AS compt
          LEFT JOIN stock_production_lot AS lot ON compt.serial = lot.id
          LEFT JOIN product_product AS prod ON lot.product_id = prod.id
          LEFT JOIN product_template AS temp ON temp.id = prod.product_tmpl_id
          LEFT JOIN product_caracteristica AS caract ON prod.id = caract.product_id
          LEFT JOIN product_category AS categ ON temp.categ_id = categ.id
          WHERE compt.serial IS NOT NULL
          AND categ.name = 'Contadors'
          GROUP BY prod.id, prod.default_code
          HAVING prod.id NOT IN (
            SELECT DISTINCT prod.id
            FROM product_caracteristica AS caract
            LEFT JOIN product_product AS prod ON caract.product_id = prod.id
            WHERE caract.name ilike 'tg_prefix'
          )
        )
    """
    cursor.execute(query)

migrate = up
