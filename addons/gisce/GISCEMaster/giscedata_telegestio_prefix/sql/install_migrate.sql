-- Check existing caracteristiques for tg_prefix
SELECT prod.default_code, prod.id, caract.name, caract.value
FROM product_caracteristica AS caract
LEFT JOIN product_product AS prod ON caract.product_id = prod.id
WHERE caract.name ilike 'tg_prefix';

-- Create característiques from product.default_code
INSERT INTO product_caracteristica
(product_id, name, value, description)
(
  SELECT prod.id as id, 'tg_prefix' as name, prod.default_code as value, 'INTERNAL - TG Prefix to build Meter TG Name' as descr
  FROM giscedata_lectures_comptador AS compt
  LEFT JOIN stock_production_lot AS lot ON compt.serial = lot.id
  LEFT JOIN product_product AS prod ON lot.product_id = prod.id
  LEFT JOIN product_caracteristica AS caract ON prod.id = caract.product_id
  WHERE compt.serial IS NOT NULL
  GROUP BY prod.id, prod.default_code
  HAVING prod.id NOT IN (
    SELECT DISTINCT prod.id
    FROM product_caracteristica AS caract
    LEFT JOIN product_product AS prod ON caract.product_id = prod.id
    WHERE caract.name ilike 'tg_prefix'
  )
);