# -*- coding: utf-8 -*-
from osv import osv
from osv.expression import OOQuery


class TgLecturesComptador(osv.osv):

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_tg_prefix(self, cursor, uid, meter_id, context=None):
        conf_obj = self.pool.get('res.config')
        caract_obj = self.pool.get('product.caracteristica')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        if isinstance(meter_id, (list, tuple)):
            meter_id = meter_id[0]
        # meter = self.browse(cursor, uid, meter_id, context=context)
        q = OOQuery(meter_obj, cursor, uid)
        sql = q.select(['serial.product_id'], only_active=False).where([
            ('id', '=', meter_id)])
        cursor.execute(*sql)
        prod_id = cursor.fetchone()
        if not prod_id:
            return ''
        prod_id = prod_id[0]
        origin = int(conf_obj.get(cursor, uid, 'tg_comptador_prefix_origen', '0'))
        if not origin or not prod_id:
            return super(TgLecturesComptador, self).get_tg_prefix(
                cursor, uid, meter_id, context=context
            )
        q2 = OOQuery(caract_obj, cursor, uid)
        sql = q2.select(['value']).where([
            ('product_id', '=', prod_id),
            ('name', '=', 'tg_prefix'),
        ])
        cursor.execute(*sql)
        prefix = cursor.fetchone()
        return prefix[0] if prefix else ''

    def get_search_meter_tg_params(self, cursor, uid, tg_name, context=None):
        if context is None:
            context = {}
        conf_obj = self.pool.get('res.config')
        caract_obj = self.pool.get('product.caracteristica')
        extra_params = super(
            TgLecturesComptador, self).get_search_meter_tg_params(
                cursor, uid, tg_name, context)
        # Only if variable is activated
        origin = int(
            conf_obj.get(cursor, uid, 'tg_comptador_prefix_origen', '0'))
        if not origin:
            return extra_params
        len_codi = int(conf_obj.get(cursor, uid, 'tg_clean_serial_chars', '5'))
        meter_product_code = tg_name[0:len_codi]
        # Remove search by serial's product's default_code parameter
        # ADD Serial's Product id in product list with code as caracteristica
        q = OOQuery(caract_obj, cursor, uid)
        sql = q.select(['product_id']).where([
            ('name', '=', 'tg_prefix'),
            ('value', 'ilike', meter_product_code)
        ])
        cursor.execute(*sql)
        product_ids = cursor.fetchall()
        if context.get('target', False) == 'product':
            if ('default_code', '=', meter_product_code) in extra_params:
                extra_params.remove(('default_code', '=', meter_product_code))
            prefix_search = [('id', 'in', product_ids)]
        else:  # Target is the meter
            if ('serial.product_id.default_code', '=', meter_product_code) in \
                    extra_params:
                extra_params.remove(
                    ('serial.product_id.default_code', '=', meter_product_code))
            prefix_search = [('serial.product_id', 'in', product_ids)]
        return extra_params + prefix_search

    def provide_tg_prefix(self, cursor, uid, prod_id, prefix):
        caract_obj = self.pool.get('product.caracteristica')
        caract_obj.create(cursor, uid, {'product_id': prod_id,
                                        'name': prefix})
        return True

TgLecturesComptador()
