# -*- coding: utf-8 -*-
{
    "name": "STG",
    "description": """
    Set and get the prefix code for telemanaged meters
    """,
    "version": "0-dev",
    "author": "Miquel Isern Roca",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_lectures_telegestio",
        "product_caracteristiques",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_telegestio_prefix_data.xml",
    ],
    "active": False,
    "installable": True
}
