# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from osv import osv
from tools.translate import _


class ResPartner(osv.osv):

    _name = 'res.partner'
    _inherit = 'res.partner'

    def is_blacklisted(self, cursor, uid, ids, context=None):
        """
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <res.partner> ids
        :type ids: long | list
        :param context: OpenERP context.
        :type context: dict
        :return: whether the partner is blacklisted or not
        :rtype: bool
        """
        if context is None:
            context = {}

        if isinstance(ids, (tuple, list)):
            partner_id = ids[0]
        else:
            partner_id = ids

        imd_obj = self.pool.get('ir.model.data')

        __, categ_id = imd_obj.get_object_reference(
            cursor, uid, 'partner_blacklist', 'categ_no_contratar'
        )

        sql = """
            SELECT partner_id
            FROM res_partner_category_rel
            WHERE partner_id = %(titular)s
            AND category_id = %(categ)s;
        """
        params = {
            'titular': partner_id,
            'categ': categ_id
        }
        cursor.execute(sql, params)
        return bool(cursor.rowcount)

    def can_contract(self, cursor, uid, ids, context=None):
        """
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <res.partner> ids
        :type ids: long | list
        :param context: OpenERP context.
        :type context: dict
        :return: whether the partner can open new contracts of not and the
        description of the reason because it can't
        :rtype: bool, str
        """
        if context is None:
            context = {}

        if isinstance(ids, (tuple, list)):
            partner_id = ids[0]
        else:
            partner_id = ids

        can_contract, reason = super(ResPartner, self).can_contract(
            cursor, uid, partner_id, context=context)
        if can_contract:
            can_contract = not self.is_blacklisted(cursor, uid, partner_id,
                                                   context=context)
            reason = _('El cliente no puede activar nuevos contratos porqué '
                       'tiene asignado la categoría de "No puede contratar".')

        return can_contract, reason


ResPartner()
