# -*- encoding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from osv import osv
from osv.orm import OnlyFieldsConstraint
from tools.translate import _


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _cnt_is_blacklisted(self, cursor, uid, ids):
        """
        :return: False if the partner has any contract blacklisted.
        """

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        conf_obj = self.pool.get('res.config')
        condition = conf_obj.get(cursor, uid, 'check_partner_can_contract')
        if condition == 'crear':
            partner_obj = self.pool.get('res.partner')

            q = self.q(cursor, uid)
            res = q.read(['titular']).where([('id', 'in', ids)])

            for contract_values in res:
                if partner_obj.is_blacklisted(
                        cursor, uid, contract_values['titular']
                ):
                    return False

        return True

    _constraints = [
        OnlyFieldsConstraint(
            _cnt_is_blacklisted,
            _('Error: no se puede asignar el titular porqué está en la '
              'lista negra.'),
            ['titular']
        )
    ]


GiscedataPolissa()
