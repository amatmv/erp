# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from osv.orm import ValidateException
from osv.osv import except_osv
from tools.translate import _

from giscedata_facturacio_impagat.tests import AbstractTestContractCreationWithDebt
from datetime import date


class TestBlacklistedContractActivation(AbstractTestContractCreationWithDebt):

    def setUp(self):
        super(TestBlacklistedContractActivation, self).setUp()

        self.activation_error_msg = _(
            'El cliente no puede activar nuevos contratos porqué tiene asignado'
            ' la categoría de "No puede contratar".'
        )
        __, self.categ_no_contratar = self.imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user, 'partner_blacklist',
            'categ_no_contratar'
        )

    def test_check_blacklist_contract_creation(self):
        """
        Tests that a new contract can't be **created** when the partner has a
        blacklisted contract.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        vals, _ = self.contract_obj.copy_data(
            cursor, uid, self.demo_contract_01)

        self.partner_obj.write(cursor, uid, [vals['titular']], {
            'category_id': [(6, 0, [self.categ_no_contratar])]
        })

        vals.update({
            'data_alta': date.today(),
            'data_firma_contracte': date.today(),
        })

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'crear'}
        )

        with self.assertRaises(ValidateException):
            self.contract_obj.create(cursor, uid, vals)

    def test_check_blacklist_contract_validation(self):
        """
        Tests that a new contract can't be **activated** when the partner has a
        blacklisted contract.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        _, demo_contract_01 = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )

        vals, _ = self.contract_obj.copy_data(cursor, uid,
                                              self.demo_contract_01)

        self.partner_obj.write(cursor, uid, [vals['titular']], {
            'category_id': [(6, 0, [self.categ_no_contratar])]
        })

        vals.update({
            'data_alta': date.today(),
            'data_firma_contracte': date.today()
        })

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'validar'}
        )

        new_contract_id = self.contract_obj.create(cursor, uid, vals)

        with self.assertRaises(except_osv):
            self.contract_obj.send_signal(cursor, uid, [new_contract_id], [
                'validar', 'contracte'
            ])

    def test_check_blacklist_on_contract_c101_creation(self):
        """
        Tests that a new contract can't be created through C1-01 step generation
        when the partner has a blacklisted contract.
        """

        cursor = self.txn.cursor
        uid = self.txn.user

        self.switch(self.txn, 'comer', other_id_name='res_partner_asus')

        asus_partner = self.imd_obj.get_object_reference(
            cursor, uid, 'base',  'res_partner_asus'
        )[1]

        self.partner_obj.write(cursor, uid, asus_partner, {
            'vat': 'ES06938188P'
        })

        self.contract_obj.write(cursor, uid, self.demo_contract_01, {
            'titular': asus_partner,
        })

        self.partner_obj.write(cursor, uid, asus_partner, {
            'category_id': [(6, 0, [self.categ_no_contratar])]
        })

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'ATR'}
        )

        c1_wizard_obj = self.pool.get('giscedata.switching.c101.wizard')

        ctx = {'contract_id': self.demo_contract_01}
        wiz_id = c1_wizard_obj.create(cursor, uid, {}, context=ctx)

        c1_wizard_obj.genera_casos_atr(cursor, uid, [wiz_id], context=ctx)

        res_info = c1_wizard_obj.read(cursor, uid, [wiz_id],
                                      ['info'])[0]['info']
        self.assertIn(self.activation_error_msg, res_info)

        created_cases = self.sw_obj.search(cursor, uid, [
            ('cups_polissa_id', '=', self.demo_contract_01),
            ('proces_id.name', '=', 'C1')
        ])

        self.assertEqual(len(created_cases), 0)

    def test_check_blacklist_on_contract_a301_creation(self):
        """
        Tests that a new contract can't be created through A3-01 step generation
        when the partner has a blacklisted contract.
        """
        cursor = self.txn.cursor
        uid = self.txn.user

        self.switch(self.txn, 'comer', other_id_name='res_partner_asus')

        asus_partner = self.imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_asus'
        )[1]

        self.partner_obj.write(cursor, uid, asus_partner, {
            'vat': 'ES06938188P'
        })

        self.contract_obj.write(cursor, uid, self.demo_contract_01, {
            'titular': asus_partner,
        })

        self.partner_obj.write(cursor, uid, asus_partner, {
            'category_id': [(6, 0, [self.categ_no_contratar])]
        })

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'ATR'}
        )

        wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')

        wiz_id = wiz_obj.create(
            cursor, uid, {}, context={'cas': 'A3', 'pol_id': self.demo_contract_01})

        context = {
            'pol_id': self.demo_contract_01, 'cas': 'A3', 'active_ids': [1],
            'active_id': False, 'contract_id': self.demo_contract_01
        }

        wiz_obj.write(cursor, uid, [wiz_id], {
            'owner': 4, 'change_atr': False, 'change_adm': True
        })

        wiz_obj.genera_casos_atr(cursor, uid, [wiz_id], context=context)

        created_cases = self.sw_obj.search(cursor, uid, [
            ('cups_polissa_id', '=', self.demo_contract_01),
            ('proces_id.name', '=', 'ATR')
        ])

        res_info = wiz_obj.read(cursor, uid, wiz_id, ['info'])[0]['info']

        self.assertIn(self.activation_error_msg, res_info)

        self.assertEqual(len(created_cases), 0)
