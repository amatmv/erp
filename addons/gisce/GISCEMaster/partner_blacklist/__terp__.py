# -*- coding: utf-8 -*-
{
    "name": "Lista negra para clientes",
    "description": """
	    Añade la nueva categoría de contratos "No puede contratar" y permite usarla para bloquear contrataciones
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_facturacio_impagat",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_polissa_data.xml",
    ],
    "active": False,
    "installable": True
}
