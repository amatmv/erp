# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from tools.translate import _
from tools import config

import pooler
import netsvc

from datetime import datetime, timedelta
from calendar import monthrange

import giscedata_lectures_estimacio_helpers as estima_helper


class EstimacioLecturesComptador(osv.osv):
    """" Comptador """
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def dates_limit(self, cursor, uid, comptador_id, context=None):
        """Retorna les dates limits en funció de dades del comptador """
        if context is None:
            context = {}
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')

        comptador_vals = comptador_obj.read(cursor, uid, comptador_id,
                                            ['polissa', 'active', 'data_baixa',
                                             'lectures'])
        polissa_vals = polissa_obj.read(cursor, uid,
                                        comptador_vals['polissa'][0],
                                        ['es_pot_estimar'])

        # Els comptadors de baixa han de tenir última lectura
        # Els comptadors que no tenen lectures, han de tenir una primera lectura
        # Les polisses que no es poden estimar, han de tenir totes les lectures
        # La resta, fa cas dels llindars
        if (not comptador_vals['active']
            or not comptador_vals['lectures']
            or not polissa_vals['es_pot_estimar']):
            min_dies = 0
            if not comptador_vals['lectures']:
                # Totes les lectures, fins i tot la de alta!
                min_dies = 0
            llindar_estimacio = 1000
        else:
            min_dies, llindar_estimacio = super(
                EstimacioLecturesComptador, self).dates_limit(cursor, uid,
                                                              comptador_id)
            llindar_estimacio = context.get(
                'llindar_estimacio', llindar_estimacio)

        return min_dies, llindar_estimacio

    def select_better_origen(self, cursor, uid, lectures):
        if not lectures:
            return []

        pool_lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        if isinstance(lectures[0], int):
            lectures = pool_lect_obj.read(cursor, uid, lectures)

        return list(set([l['name'] for l in lectures]))

    def get_lectura_estimada(self, cursor, uid, comptador_id, data=None,
                             dies_marge=None, context=None):
        """ Estimació de lectures
            * Retorna una llista de lectures estimades pel comptador
            * Utilitza el consum diari del últims 14 mesos per calcular lectura
            * Calculem les lectures estimades per la data passada
        """
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        lectura_obj = self.pool.get('giscedata.lectures.lectura')
        lecturapool_obj = self.pool.get('giscedata.lectures.lectura.pool')
        origencomer_obj = self.pool.get('giscedata.lectures.origen_comer')
        origen_obj = self.pool.get('giscedata.lectures.origen')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        cups_obj = self.pool.get('giscedata.cups.ps')
        conf_obj = self.pool.get('res.config')

        info_estimacio_lectura = _(u"Informació d'estimació de lectura:\n")

        if isinstance(comptador_id, (list, tuple)):
            comptador_id = comptador_id[0]

        comptador_dades = comptador_obj.read(cursor, uid, comptador_id,
                                             ['polissa', 'giro'])
        polissa_id = comptador_dades['polissa'][0]

        pol_dades = polissa_obj.read(cursor, uid, polissa_id,
                                     ['comptadors', 'data_ultima_lectura',
                                      'tarifa', 'data_alta', 'cups'])

        # períodes (energia) de la tarifa actual
        periode_ids = tarifa_obj.read(cursor, uid, pol_dades['tarifa'][0],
                                      ['periodes'])['periodes']

        if not data:
            data = pol_dades['data_ultima_lectura'] or pol_dades['data_alta']
        data_ult_lectura = (context.get('data_ultima_lectura', False)
                            or pol_dades['data_ultima_lectura'])
        # Algunes pólisses no les estimem mai (filtre de pólisses)
        if not polissa_obj.es_estimable(cursor, uid, polissa_id, data):
            return []

        #Busquem orígens distri
        # autolectura
        origen_autolectura_id = origen_obj.search(cursor, uid,
                                                  [('codi', '=', '50')])[0]
        #estimada
        origen_estimada_id = origen_obj.search(cursor, uid,
                                               [('codi', '=', '40')])[0]

        # Busquem orígens comer
        origen_comer_estimada_id = origencomer_obj.search(cursor, uid,
                                                          [('codi', '=',
                                                            'ES')])[0]

        # Busquem la lectura de referència (inicial per afegir-hi el consum)
        # Han de ser de la mateixa tarifa, no les podem estimar
        # 1) Seran les autolectures o lectures de distribuidora del pool
        # 2) Per si hi ha una lectura manual a facturació, també ho mirarem
        # Obs: En un futur (potser) descartarem les estimades de distribuïdora

        search_vals = [('comptador', '=', comptador_id),
                       ('periode', 'in', periode_ids),
                       ('name', '<=', data)]

        lect_fields = ['name', 'origen_id', 'origen_comer_id']

        #1) de lectures del pool
        lectures = lecturapool_obj.search(cursor, uid, search_vals,
                                          order='name DESC')

        lectura_pool_dades = None
        lectura_recent = None
        if lectures:
            lectura_pool_dades = lecturapool_obj.read(cursor, uid, lectures[0],
                                                      lect_fields)
            # Si hi ha una lectura de distribuïdora després de l'última lectura
            # (no autolectura) el marge de dies des de l'última lectura serà de
            # max_dies per poder moure la finestra i poder adaptarnos als canvis
            # de data de lectura de la distribuïdora.
            lectures_dades = lecturapool_obj.read(cursor, uid, lectures,
                                                  lect_fields)
            lectura_recent = [l for l in lectures_dades
                              if (l['name'] > data_ult_lectura
                                  and (l['origen_id'][0]
                                       != origen_autolectura_id))
                              ]
            # Si és una lectura Estimada de distribuïdora, busquem una lectura
            # millor , p.e. Autolectura
            if lectura_pool_dades['origen_id'][0] == origen_estimada_id:
                lects = [l for l in lectures_dades
                         if (l['name'] > data_ult_lectura
                             and l['origen_id'][0] != origen_estimada_id)]
                if lects:
                    lectura_pool_dades = lects[0]

        #2) de lectures de facturació
        lectura = lectura_obj.search(cursor, uid, search_vals, limit=1,
                                     order='name DESC')

        lectura_fact_dades = None
        if lectura:
            lectura_fact_dades = lectura_obj.read(cursor, uid, lectura[0],
                                                  lect_fields)

        if not lectura_fact_dades and not lectura_pool_dades:
            # No tenim cap lectura per fer la estimació
            return []

        if (lectura_fact_dades and
                (not lectura_pool_dades or
                 (lectura_fact_dades['name'] > lectura_pool_dades['name']))):
            lectura_inici_obj = lectura_obj
            lectura_inici = lectura_fact_dades
            pool_lectura = _(u"Facturació")
        else:
            lectura_inici_obj = lecturapool_obj
            lectura_inici = lectura_pool_dades
            pool_lectura = _(u"Pool")

        info_estimacio_lectura += (_(u"Lectura inici: %s (de %s) %s\n") %
                                   (lectura_inici['name'], pool_lectura,
                                    lectura_inici['origen_id'][1]))

        data_ult_lectura = data_ult_lectura or pol_dades['data_alta']
        data_ult_lectura = datetime.strptime(data_ult_lectura, '%Y-%m-%d')

        #dies del mes data ultima lectura
        if not dies_marge:
            dies_marge = monthrange(data_ult_lectura.year,
                                    data_ult_lectura.month)[1]

            # per ajustar en cas de canvi de data lectura de distribuïdora
            if lectura_recent:
                dies_marge = int(conf_obj.get(cursor, uid,
                                 'pool_llindar_estimacio_lectures', '1'))
                info_estimacio_lectura += (_(u"Tenim una lectura recent: %s\n")
                                           % lectura_recent[0]['name'])

        data_lectura_estimada = (data_ult_lectura + timedelta(days=dies_marge))
        data_inici = lectura_inici['name']
        dies_a_estimar = (data_lectura_estimada
                          - datetime.strptime(data_inici, '%Y-%m-%d')).days

        info_estimacio_lectura += (_(u"Data lectura estimada: %s "
                                     u"(%s + %s dies)\n") %
                                   (data_lectura_estimada.strftime('%Y-%m-%d'),
                                    data_ult_lectura.strftime('%Y-%m-%d'),
                                    dies_marge))
        #Busquem el consum diari de mitjana dels últims 14 mesos (426 dies)
        consums_diaris = estima_helper.consum_diari(cursor, uid, polissa_id,
                                                    426, data_ref=data)

        info_estimacio_lectura += (_(u"Consums diaris: %s (14 mesos)\n") %
                                   consums_diaris)

        # If not P0, and there's more than 1 measure
        if not consums_diaris['P0']:
            # 0 consum protection
            # Only when there's only one measure group
            search_fact_vals = [('name', '<=', data),
                                ('comptador', '=', comptador_id),
                                ('tipus', '=', 'A'),
                                ('periode', '=', periode_ids[0])]

            num_lectures = lectura_obj.search_count(cursor, uid,
                                                    search_fact_vals,)
            if num_lectures < 2:
                # Only when there's less than 1 measure
                consums_diaris = estima_helper.consum_diari_cups_anual(
                    cursor, uid, polissa_id, data_ref=data)

            if consums_diaris['P0']:
                cups_list = ['conany_kwh', 'conany_data', 'conany_origen']
                cups_vals = cups_obj.read(cursor, uid, pol_dades['cups'][0],
                                          cups_list)
                cups_vals.update({'cups': pol_dades['cups'][1],
                                  'consum': consums_diaris})
                info_estimacio_lectura += (_(u"Consum anual "
                                             u"'%(conany_origen)s' al CUPS "
                                             u"'%(cups)s' de "
                                             u"%(conany_kwh)s kWh el dia "
                                             u"%(conany_data)s: "
                                             u"%(consum)s \n") % cups_vals)

        # El consum que hem d'afegir a cada període
        consums = dict([(p, int(cd * dies_a_estimar))
                        for p, cd in consums_diaris.items()])

        info_estimacio_lectura += (_(u"Consums estimats: %s (%s dies)\n") %
                                   (consums, dies_a_estimar))

        #Agafem les lectures del dia de referència (data_inici)
        search_vals = [('name', '=', data_inici),
                       ('comptador', '=', comptador_id),
                       ('tipus', '=', 'A')]
        lectures_ids = lectura_inici_obj.search(cursor, uid, search_vals)

        lectures = lectura_inici_obj.read(cursor, uid, lectures_ids, [])
        # Creem les noves lectures afegint el consum a les lectures trobades
        if not lectures:
            # No hem trobat lectures de la tarifa en qüestió en el dia indicat
            return []
        res = []
        # Protecció segons consum de factura anterior
        # En el cas que ens passem, rebaixem proporcionalment el consum de
        # tots els períodes
        maxim_consum_per = float(conf_obj.get(cursor, uid,
                                              'estimacio_max_factura_anterior',
                                              '1.0'))
        # Quan estimem molts dies, no tenim en compte la limitació
        if maxim_consum_per > 0 and dies_marge < 40:
            consum_ultima = polissa_obj.consum_ultima_factura(cursor, uid,
                                                              polissa_id)
            if (consum_ultima
                    and (consum_ultima * maxim_consum_per) < consums['P0']):
                p0_orig = consums['P0']
                proporcio = (consum_ultima * maxim_consum_per) / consums['P0']
                consums = dict([(k, int(c * proporcio))
                                for k, c in consums.items()])
                info = _(u"Limitació sobre factura (%s x %.2f%% < %s) "
                         u"anterior: %s x %.02f -> %s\n")
                info_estimacio_lectura += (
                    info % (consum_ultima, maxim_consum_per * 100 , p0_orig,
                            p0_orig, proporcio, consums['P0']))
                info_estimacio_lectura += (
                    _(u"Consums estimats (limitats): %s\n") % consums)

        for lectura in lectures:
            per_vals = periode_obj.read(cursor, uid, lectura['periode'][0],
                                        ['name'])
            periode = per_vals['name']
            # tenim en compte possibles lectures negatives quan tenim
            # Autolectures que cauen dins el marge [min_dies..inici_estimacio]
            # Vigilem gir de comptador
            lectura_estimada = lectura['lectura'] + consums[periode]
            if comptador_dades['giro']:
                lectura_estimada %= comptador_dades['giro']

            data = data_lectura_estimada.strftime('%Y-%m-%d')
            vals = {'name': data,
                    'periode': (per_vals['id'],),
                    'comptador': lectura['comptador'],
                    'tipus': 'A',
                    'origen_id': (origen_estimada_id,),
                    'origen_comer_id': (origen_comer_estimada_id,),
                    'lectura': lectura_estimada,
                    'observacions': info_estimacio_lectura,
                    'incidencia_id': False,
                    }
            res.append(vals)

        return res

EstimacioLecturesComptador()