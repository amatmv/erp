# -*- coding: utf-8 -*-
{
    "name": "Estimació de Lectures",
    "description": """
    Mòdul que permet la creació de lectures estimades segons lectures anteriors.
    """,
    "version": "0-dev",
    "author": "Gisce-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_lectures",
        "giscedata_lectures_comer",
        "giscedata_lectures_pool",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_lectures_estimacio_data.xml",
        "giscedata_polissa_view.xml",
        "giscedata_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
