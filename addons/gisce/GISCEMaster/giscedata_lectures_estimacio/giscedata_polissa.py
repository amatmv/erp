# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from oorq.decorators import job
from tools.translate import _
from tools import config
from tools.misc import cache

import pooler
import netsvc

from datetime import datetime, timedelta
from dateutil.parser import parse as parse_date

import giscedata_lectures_estimacio_helpers as estima_helper

#error de com a molt de 3 dies
DIES_SEGONS_MESOS = {
    #14 mesos: error de com a molt de +/- 3 dies
    'consum_diari_14': 365 + 61,
}


class GiscedataPolissa(osv.osv):
    """" Lectures del pool de lectures """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_consum_anual_lectures(self, cursor, uid, polissa_id, context=None):
        """Calculem el consum anual a partir del consum diari"""

        if isinstance(polissa_id, (tuple, list)):
            polissa_id = polissa_id[0]

        # More than 4 months old contracts only
        limit = datetime.today() - timedelta(122)

        contract_vals = self.read(
            cursor, uid, polissa_id, ['data_alta'], context=context
        )

        if limit < parse_date(contract_vals['data_alta']):
            return False

        consum_diari_historic = self.consum_diari(
            cursor, uid, polissa_id, context=context
        )

        if consum_diari_historic:
            return int(consum_diari_historic['P0'] * 365)

        return False

    def consum_diari(self, cursor, uid, polissa_id,
                     dies=DIES_SEGONS_MESOS['consum_diari_14'], data_ref=None,
                     context=None):
        """Calculem el consum diari a 14 mesos"""
        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        consum_diari = estima_helper.consum_diari(cursor, uid, polissa_id,
                                                  dies, data_ref)

        return consum_diari

    @cache(timeout=600)
    def get_som_no_estimables(self, cursor, uid, val=[]):
        """Retorna les pòlisses de som no estimables. Útil pel període
           d'implantació"""
        conf_obj = self.pool.get('res.config')
        polissa_obj = self.pool.get('giscedata.polissa')
        max_id_polissa = int(conf_obj.get(cursor, uid,
                              'som_max_polissa_a_estimar', '0'))

        res = []

        if max_id_polissa:
            res = polissa_obj.search(cursor, uid, [('id', '>', max_id_polissa)])
        return res

    def es_estimable(self, cursor, uid, polissa_id, data=None, context=None):
        """ Decidim si una pólissa es pot estimar en funció de diferents
            criteris, p.e. tarifa, facturació potència, etc..."""
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        if polissa_id in self.get_som_no_estimables(cursor, uid):
            return False

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        if not data:
            data = datetime.today().strftime('%Y-%m-%d')

        pol_vals = polissa_obj.read(cursor, uid, polissa_id,
                                    ['facturacio_potencia', 'estat'
                                     'data_ultima_lectura', 'data_alta',
                                     'tarifa', 'no_estimable'])
        if pol_vals['no_estimable']:
            return False

        if pol_vals['data_alta'] and pol_vals['data_alta'] < data:
            try:
                pol_vals = polissa_obj.read(cursor, uid, polissa_id,
                                            ['facturacio_potencia', 'estat'
                                             'data_ultima_lectura', 'data_alta',
                                             'tarifa'],
                                            context={'date': data})
            except:
                #Per si no hi ha modcon en la data passada (no s'ha renovat)
                pass

        # Facturació per maxímetre no estimem
        if pol_vals['facturacio_potencia'] == 'max':
            return False
        # Algunes 3.0 encara no estan amb facturació per maxímetre
        per_tarifa = tarifa_obj.get_periodes_producte(cursor, uid,
                                                      pol_vals['tarifa'][0],
                                                      'te')
        if len(per_tarifa) > 3:
            return False

        return True

    def _ff_estimable(self, cursor, uid, ids, field, arg, context=None):
        res = dict([(i, False) for i in ids])

        for id in ids:
            res[id] = self.es_estimable(cursor, uid, id)

        return res

    def consum_ultima_factura(self, cursor, uid, polissa_id, context=None):
        """ Mirem el consum de la última factura"""
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        search_vals = [('polissa_id', '=', polissa_id),
                       ('journal_id', 'ilike', '%ENERGIA%'),
                       ('type', '=', 'out_invoice')]
        factura_ids = factura_obj.search(cursor, uid, search_vals,
                                         order='date_invoice DESC')
        if not factura_ids:
            return None

        vals = factura_obj.read(cursor, uid, factura_ids[0], ['energia_kwh'])
        return vals['energia_kwh']

    def check_sobreestimacio_se01(self, cursor, uid, polissa_id, context=None):
        """
        Detecta quan tenim una lectura estimada que es superior a la següent
        lectura peró tenim lectures posteriors a pool que son superiors a
        l'estimada.
        """
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['origen'] = '40'
        pol_obj = self.pool.get("giscedata.polissa")
        return pol_obj.check_sobreautolectura_sa01(cursor, uid, polissa_id, ctx)

    def fix_sobreestimacio_se01(self, cursor, uid, contract_id, context=None):
        """
        Soluciona el cas que d'una lectura sobrestimada (Ls) que
        * La lectura següent (Ls+1) és inferior
        * La lectura posterior a pool (Ls+2, Ls+3...Ls+jump) és superior

        Copia la lectura Ls a Ls+1

        :param polissa_id: Contract to fix
        :param jumps: Number of measures in pool from last invoiced to search
                      for
        :param context:
        :return: True
        """

        if context is None:
            context = {}
        ctx = context.copy()
        ctx['origen'] = '40'
        pol_obj = self.pool.get("giscedata.polissa")
        return pol_obj.fix_sobreautolectura_sa01(cursor, uid, contract_id, ctx)


    _columns = {
        'es_pot_estimar': fields.function(_ff_estimable, string='Es pot estimar',
                                          type='boolean', method=True),
        'no_estimable': fields.boolean('No estimable'),
        'observacions_estimacio': fields.text('Observacions estimació'),
    }

    _defaults = {
        'no_estimable': lambda *a: True,
    }

GiscedataPolissa()
