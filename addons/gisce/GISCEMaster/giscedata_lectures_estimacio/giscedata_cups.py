# -*- coding: utf-8 -*-
"""Classes pel mòdul giscedata_cups (General)."""
from osv import osv, fields
from tools.translate import _
from datetime import datetime, timedelta
import netsvc

from operator import itemgetter


class GiscedataCupsPs(osv.osv):
    """Classe d'un CUPS (Punt de servei)."""

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _ORIGENS_CONANY = [
        ('lectures', _(u'Històric de lectures')),
        ('manual', _(u'Manual')),
    ]

    def get_fonts_consums_anuals(self, cursor, uid, context=None):
        ''' Retorna llista amb funcions per calcular consum anual
        {'origen': origens['index'],'model': model, 'funcio': func ,
         'priority': enter}
        '''
        llista = []
        vals = {'priority': 10,
                'model': 'giscedata.polissa',
                'func': 'get_consum_anual_lectures',
                'origen': 'lectures'}
        llista.append(vals)

        return llista

    def omple_consum_anual_xmlrpc(self, cursor, uid, ids, context=None):
        res = self.omple_consum_anual(cursor, uid, ids, context=context)
        return dict([(str(k), v) for k, v in res.items()])

    def omple_consum_anual(self, cursor, uid, ids, context=None):
        """ Omple els camps de consum anual en en funció de les funcions
        registrades"""

        logger = netsvc.Logger()

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = {}.fromkeys(ids, False)
        pol_obj = self.pool.get('giscedata.polissa')

        llista = sorted(self.get_fonts_consums_anuals(cursor, uid),
                        key=itemgetter('priority'))
        origens = dict(self._columns['conany_origen'].selection)

        for cups_id in ids:
            pol_id = False
            # busquem la pòlissa actual (si en té)
            search_vals = [('cups', '=', cups_id), ('state', '=', 'activa')]
            pol_ids = pol_obj.search(cursor, uid, search_vals, context=context)
            if pol_ids:
                pol_id = pol_ids[0]
            consum = False
            for func in llista:
                #ID of model
                if func['model'] == 'giscedata.polissa':
                    if not pol_id:
                        continue
                    else:
                        func_id = pol_id
                else:
                    func_id = cups_id

                l_func = getattr(self.pool.get(func['model']), func['func'])
                consum = l_func(cursor, uid, func_id, context=context)
                if consum is False or not func['origen'] in origens:
                    continue

                res[cups_id] = consum
                vals = {'conany_kwh': consum,
                        'conany_origen': func['origen'],
                        'conany_data': datetime.today().strftime('%Y-%m-%d')}
                if self.write(cursor, uid, cups_id, vals):
                    cups_vals = self.read(cursor, uid, cups_id, ['name'])
                    txt = _(u"Consum anual per %s (%s): %d kWh des de '%s'")
                    logger.notifyChannel(
                        'conany', netsvc.LOG_INFO,
                        txt % (cups_vals['name'], cups_id, consum,
                               origens[func['origen']]))
                    break

        return res

    _columns = {
        'conany_kwh': fields.integer('Consum Anual',
                                     help=u'Consum anual segons origen '
                                          u'seleccionat. Consum de l\'any '
                                          u'anterior a la data mostrada.'),
        'conany_data': fields.date('Data càlcul Consum Anual',
                                   help=u'Data del consum anual calculat. El '
                                        u'consum és entre aquesta data i un '
                                        u'any enrera d\'aquesta data'),
        'conany_origen': fields.selection(_ORIGENS_CONANY, 'Origen',
                                          help=u'Quan calculat, quina és la '
                                               u'font de les dades.'),
    }

    _default = {
        'conany_origen': lambda *a: 'manual',
    }

GiscedataCupsPs()
