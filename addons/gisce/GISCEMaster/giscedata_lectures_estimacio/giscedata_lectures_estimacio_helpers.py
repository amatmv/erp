# -*- coding: utf-8 -*-

# Funcions compartides per diferents processos d'estimacio de lectures

import pooler
from tools.translate import _
from osv import osv, fields, orm

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import math


# matriu de normalització de períodes segons tarifes
# m[from][to] = (P1, P2, P3)
# Només quan no coincideixin els períodes

NORM_PERIODES_MATRIX = {'2.0DHA': (0.4, 0.6, 0),
                        '2.1DHA': (0.35, 0.65, 0),
                        '2.0DHS': (0.5, 0.25, 0.25),
                        '2.1DHS': (0.5, 0.25, 0.25),
                        }


# matriu segons número de períodes (P0, P2, P3)(P1, P2, P3)
# P1 sempre serà P0 - P2 - P3
NORM_MATRIX = (((1, 1, 1), (0, 0, 0), (0, 0, 0)),
               ((1, 1, 1), (0, 1, 1), (0, 0, 0)),
               ((1, 1, 1), (0, 1, 0), (0, 0, 1)),
               )

def consum_diari(cursor, uid, polissa_id, dies, data_ref=None, context=None):
    """Calcula el consum diari d'una polissa amb lectures des de
       n dies enrera"""

    pool = pooler.get_pool(cursor.dbname)

    pol_obj = pool.get('giscedata.polissa')
    comptador_obj = pool.get('giscedata.lectures.comptador')
    tarifa_obj = pool.get('giscedata.polissa.tarifa')

    pol_vals = pol_obj.read(cursor, uid, polissa_id,
                            ['data_ultima_lectura'])

    if not data_ref:
        #Calculem la data inici segons el període (en dies) seleccionat des de
        # la última lectura facturada
        data_ref = pol_vals['data_ultima_lectura']

    # Mirem els períodes de la tarifa actual (o data_ref)
    tarifa_actual = pol_obj.read(cursor, uid, polissa_id, ['tarifa'],
                                 context={'date': data_ref})['tarifa']

    per_tarifa = tarifa_obj.get_periodes_producte(cursor, uid,
                                                  tarifa_actual[0], 'te').keys()
    # 3.0 només tenim en compte 3 períodes
    num_per = min(len(per_tarifa), 3)

    consums_diaris = dict([('P%d' % p, {'dies': 0, 'consum': 0})
                           for p in range(1, num_per + 1)])
    consums_diaris.update({'P0': {'dies': 0, 'consum': 0}})

    data_inici = (datetime.strptime(data_ref, '%Y-%m-%d')
                  - timedelta(days=dies)).strftime('%Y-%m-%d')

    # Busquem totes les modcons amb canvi de comptador
    ctx = {'ffields': ['tarifa', 'comptador']}
    modcons = pol_obj.get_modcontractual_intervals(cursor, uid, polissa_id,
                                                   data_inici, data_ref,
                                                   context=ctx)
    total_dies = 0
    trossos = []

    for (data, valors) in modcons.items():
        pol_dades = pol_obj.read(cursor, uid, polissa_id,
                                 ['comptadors', 'tarifa'],
                                 context={'date': data})
        for comptador_id in pol_dades['comptadors']:
            num_per_tros = 0
            tarifa_id = pol_dades['tarifa'][0]

            lectures = comptador_obj.get_lectures(cursor, uid, comptador_id,
                                                  tarifa_id,
                                                  max(data_inici, data),
                                                  per_facturar=True
                                                  )

            consums = {'P0': 0, 'dies': 0}

            for (periode, valor) in lectures.items():
                if not valor['actual'] or not valor['anterior']:
                    #No hi ha lectures que ens dins els 14 mesos
                    continue
                consum = (valor['actual']['lectura'] -
                          valor['anterior']['lectura'])
                # Controlem el gir de comptador
                if consum < 0:
                    gir = comptador_obj.read(cursor, uid, comptador_id,
                                             ['giro'])['giro']
                    consum += gir or 0
                # Només 3 períodes a 3.0
                if int(periode[-1:]) > 3:
                    periode = "P%d" % (int(periode[-1:]) - 3)

                if consums.get(periode, False):
                    consums[periode] += consum
                else:
                    consums[periode] = consum

                if not consums['dies']:
                    num_dies = (datetime.strptime(valor['actual']['name'],
                                                  '%Y-%m-%d') -
                                datetime.strptime(valor['anterior']['name'],
                                                  '%Y-%m-%d')).days
                    consums['dies'] = num_dies
                consums['P0'] += consum

            if not consums['P0']:
                #Periode sense consum, no cal entrar
                continue

            for p in ['P1', 'P2', 'P3']:
                if consums.get(p):
                    consums[p + 'perc'] = ((consums[p] * 1.0) / consums['P0'])
                    num_per_tros += 1

            consums['num_per'] = num_per_tros

            # Normalitzem segons tarifa actual (1, 2 o 3 períodes)
            consums['P0n'] = consums['P0']
            P2 = consums.get('P2', 0)
            P3 = consums.get('P3', 0)
            consums['P2n'] = ((NORM_MATRIX[num_per - 1][1][1] and P2) +
                              (NORM_MATRIX[num_per - 1][1][2] and P3))
            consums['P3n'] = ((NORM_MATRIX[num_per - 1][2][1] and P2) +
                              (NORM_MATRIX[num_per - 1][2][2] and P3))
            consums['P1n'] = (consums['P0n'] - consums['P2n'] - consums['P3n'])

            # Afegim dies només si tenim algun període (P2 o P3)
            if ((('P2' in consums) and NORM_MATRIX[num_per - 1][1][1]) or
               (('P3' in consums) and NORM_MATRIX[num_per - 1][1][2])):
                consums_diaris['P2']['dies'] += consums['dies']
            # Afegim dies només si tenim algun període (P2 o P3)
            if ((('P2' in consums) and NORM_MATRIX[num_per - 1][2][1]) or
               (('P3' in consums) and NORM_MATRIX[num_per - 1][2][2])):
                consums_diaris['P3']['dies'] += consums['dies']

            # sempre tenim P1 i P0
            consums_diaris['P0']['dies'] += consums['dies']
            consums_diaris['P1']['dies'] += consums['dies']

            for p in consums_diaris.keys():
                consums_diaris[p]['consum'] += consums['%sn' % p]

            trossos.append(consums)
            total_dies += consums['dies']

    # No tenim lectures per fer la estimació
    if not consums_diaris['P0']['consum']:
        return dict([(k, 0) for k, v in consums_diaris.items()])

    # consum diari amb períodes agregats
    dies_unics = True
    for c in consums_diaris.values():
        if c['dies'] != total_dies:
            dies_unics = False
            break
    if dies_unics:
        # quan tots els períodes duren els mateixos dies,
        # les mitges són correctes
        res = dict([(k, (v['consum'] * 1.0) / v['dies'])
                    for k, v in consums_diaris.items()])
    else:
        # proporció de P0 segons període
        # busquem el tros on hi hagi els períodes de la tarifa actual i mirem
        # les proporcions
        trossos_ok = [t for t in trossos if t['num_per'] == num_per]
        if not trossos_ok:
            perc = NORM_PERIODES_MATRIX[tarifa_actual[1]]
            tros_ok = {'P1perc': perc[0],
                       'P2perc': perc[1],
                       'P3perc': perc[2]}
        else:
            tros_ok = sorted(trossos_ok, key=lambda a: 'dies')[-1]

        p0 = ((consums_diaris['P0']['consum'] * 1.0)
              / consums_diaris['P0']['dies'])
        res = dict([(k, p0 * tros_ok["%sperc" % k])
                    for k in consums_diaris.keys() if k != 'P0'])
        res.update({'P0': p0})

    return res

def consum_diari_cups_anual(cursor, uid, polissa_id, data_ref=None,
                            context=None):
    """ Consum diari per períodes segons consum anual emmagatzemat a cups"""
    pool = pooler.get_pool(cursor.dbname)

    pol_obj = pool.get('giscedata.polissa')
    tarifa_obj = pool.get('giscedata.polissa.tarifa')
    cups_obj = pool.get('giscedata.cups.ps')

    # Mirem el consum anual del cups assignat a la pólissa
    pol_dades = pol_obj.read(cursor, uid, polissa_id, ['cups', 'tarifa',
                                                       'data_ultima_lectura',])

    if not data_ref:
        #Calculem la data inici segons el període (en dies) seleccionat des de
        # la última lectura facturada
        data_ref = pol_dades['data_ultima_lectura']

    # Mirem els períodes de la tarifa actual (o data_ref)
    tarifa_actual = pol_obj.read(cursor, uid, polissa_id, ['tarifa'],
                                 context={'date': data_ref})['tarifa']

    per_tarifa = tarifa_obj.get_periodes_producte(cursor, uid,
                                                  tarifa_actual[0], 'te').keys()
    # 3.0 només tenim en compte 3 períodes
    num_per = min(len(per_tarifa), 3)

    consums_diaris = dict([('P%d' % p, 0) for p in range(1, num_per + 1)])
    consums_diaris.update({'P0': 0})

    cups_list = ['conany_kwh', 'conany_data', 'conany_origen']
    cups_dades = cups_obj.read(cursor, uid, pol_dades['cups'][0],
                               cups_list)

    if not cups_dades['conany_data'] or not cups_dades['conany_kwh']:
        return consums_diaris

    consum_anual = cups_dades['conany_kwh']

    tarifa_name = tarifa_actual[1]
    if tarifa_name in NORM_PERIODES_MATRIX.keys():
            perc = NORM_PERIODES_MATRIX[tarifa_name]
            tros_ok = {'P1': perc[0],
                       'P2': perc[1],
                       'P3': perc[2]}
            for per in consums_diaris.keys():
                if per != 'P0':
                    consums_diaris[per] = consum_anual * tros_ok[per]
            consums_diaris['P0'] = sum(consums_diaris.values())
    else:
        consums_diaris['P0'] = consums_diaris['P1'] = consum_anual

    res = dict([(k, v * 1.0 / 365) for k, v in consums_diaris.items()])

    return res
