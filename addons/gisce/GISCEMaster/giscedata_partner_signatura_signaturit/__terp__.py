# -*- coding: utf-8 -*-
{
    "name": "Signatura digital dels clients (Signaturit)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_signatura_documents_signaturit",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "giscedata_partner_signatura_wizard_view.xml"
    ],
    "active": False,
    "installable": True
}
