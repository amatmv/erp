# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Julio 2010",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 158 - 30/06/2010.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20100701_data.xml"
    ],
    "active": False,
    "installable": True
}
