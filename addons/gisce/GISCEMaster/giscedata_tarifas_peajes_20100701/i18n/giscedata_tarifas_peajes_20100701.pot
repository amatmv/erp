# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_tarifas_peajes_20100701
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-04-25 16:10:53+0000\n"
"PO-Revision-Date: 2012-04-25 16:10:53+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_tarifas_peajes_20100701
#: constraint:product.pricelist.item:0
msgid "Error ! You cannot assign the Main Pricelist as Other Pricelist in PriceList Item!"
msgstr ""

#. module: giscedata_tarifas_peajes_20100701
#: model:product.pricelist.version,name:giscedata_tarifas_peajes_20100701.boe_158_2010
msgid "BOE núm. 158 - 30/06/2010"
msgstr ""

#. module: giscedata_tarifas_peajes_20100701
#: model:ir.module.module,description:giscedata_tarifas_peajes_20100701.module_meta_information
msgid "\n"
"Actualització de les tarifes de peatges segons el BOE nº 158 - 30/06/2010.\n"
""
msgstr ""

#. module: giscedata_tarifas_peajes_20100701
#: model:ir.module.module,shortdesc:giscedata_tarifas_peajes_20100701.module_meta_information
msgid "Tarifas Peajes Julio 2010"
msgstr ""

#. module: giscedata_tarifas_peajes_20100701
#: constraint:product.pricelist.version:0
msgid "You cannot have 2 pricelist versions that overlap!"
msgstr ""

