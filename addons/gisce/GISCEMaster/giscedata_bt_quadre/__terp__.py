# -*- coding: utf-8 -*-
{
    "name": "GISCE Quadre Baixa Tensió",
    "description": """Quadre de Baixa Tensió""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base_extended",
        "giscedata_bt",
        "giscedata_cts",
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [
        "giscedata_bt_quadre_demo.xml"
    ],
    "update_xml":[
        "security/ir.model.access.csv",
        "giscedata_bt_quadre_view.xml",
        "giscedata_bt_quadre_data.xml"
    ],
    "active": False,
    "installable": True
}
