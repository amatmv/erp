# -*- coding: utf-8 -*-
from osv import osv, fields

# --------------IMPORTANT-----------------
# This model is suposed to implement all, the quadre bt and the quadre bt
# element, but now it will just implement the quadre bt element.
# ToDo: Implement the model GiscedataBtQuadre


class GiscedataBtQuadreElementTipus(osv.osv):

    _name = 'giscedata.bt.quadre.element.tipus'
    _description = 'Tipus d\'elements del Quadre de BT'

    _columns = {
        'name': fields.char('Tipus', size=32, required=True),
        'code': fields.char('Codi', size=32, required=True),
        'description': fields.char('Descripció', size=256)
    }

    _defaults = {}

    _order = "name, id"


GiscedataBtQuadreElementTipus()


class GiscedataBtQuadreElement(osv.osv):
    _name = 'giscedata.bt.quadre.element'
    _description = 'Elements del Quadre de BT'
    _rec_name = 'codi'

    _columns = {
        'codi': fields.char('Codi', size=64, required=True, select=1),
        'blockname': fields.many2one(
            'giscedata.bt.quadre.element.tipus', 'Tipus', required=True,
            select=1
        ),
        'sortida_bt': fields.integer('Sortida BT', select=1),
        'intensitat': fields.float('Intensitat'),
        'ct_id': fields.many2one('giscedata.cts', 'CT', select=1),
        'trafo_id': fields.many2one(
            'giscedata.transformador.trafo', 'Transformador', select=1
        ),
        "active": fields.boolean("Actiu")
    }

    _defaults = {
        'active': lambda *a: 1,
    }


GiscedataBtQuadreElement()
