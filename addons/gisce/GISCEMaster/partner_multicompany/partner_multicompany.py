from osv import osv, fields

class res_partner(osv.osv):
	_name = "res.partner"
	_inherit = "res.partner"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company'),
	}
	_defaults = {
		'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
res_partner()
