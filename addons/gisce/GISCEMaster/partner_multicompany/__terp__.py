# -*- coding: utf-8 -*-
{
    "name": "Partner multicompany",
    "description": """Multi-company support for partner""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "base"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "partner_multicompany_view.xml"
    ],
    "active": False,
    "installable": True
}
