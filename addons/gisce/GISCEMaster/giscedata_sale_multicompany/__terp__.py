# -*- coding: utf-8 -*-
{
    "name": "GISCE Sale Multicompany",
    "description": """Extensió del mòdul sale base que implementa la multicompanyia.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscedata_sale",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "sale_view.xml",
    ],
    "active": False,
    "installable": True
}
