# -*- encoding: utf-8 -*-

from osv import fields
from osv import osv
from osv.expression import OOQuery


class sale_order(osv.osv):

    _name = 'sale.order'
    _inherit = 'sale.order'

    def _default_company(self, cursor, uid, context=None):
        res = context.get('company_id', False)
        if not res:
            user_obj = self.pool.get('res.users')
            columns = ['company_id']
            dmn = [('id', '=', uid)]
            q = OOQuery(user_obj, cursor, uid).read(columns).where(dmn)
            res = q[0][columns[0]]
        return res

    _columns = {
        'company_id': fields.many2one('res.company', 'Company'),
    }

    _defaults = {
        'company_id': _default_company,
    }


sale_order()
