# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime, timedelta
from dateutil.rrule import rrule, DAILY

TECHNOLOGY_TYPE = {
    'tg': ('smmweb', 'prime'),
    'tm': ('electronic', 'telemeasure')
}

REGULARIZER_TYPE = "G"
COMPLEMENTARY_TYPE = "C"


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    @staticmethod
    def get_unique_months_from_invoice(di, df):
        """
        Get unique months between two dates
        :param di: str init date LIKE 'YYYY-MM-DD'
        :param df: str end date LIKE 'YYYY-MM-DD'
        :return: months LIKE ['YYYY-MM']
        """
        first_month = datetime.strptime(di, '%Y-%m-%d')
        last_month = datetime.strptime(df, '%Y-%m-%d')

        months = list(set([dt.strftime("%Y-%m") for dt in
                           rrule(freq=DAILY, dtstart=first_month,
                                 until=last_month)]))
        return months

    def check_profilable(self, cursor, uid, fact_id, context=None):
        if isinstance(fact_id, (list, tuple)):
            fact_id = fact_id[0]
        return self.check_profilable_distri(cursor, uid, fact_id, context)

    def get_automatic_profile(self, cursor, uid, context=None):
        """
        Get from res_config
        :return: profile_on_invoice_open value or False if not exist
        """
        res_config = self.pool.get('res.config')
        automatic_profile = int(res_config.get(
            cursor, uid, 'profile_on_invoice_open', '1'
        ))

        return automatic_profile

    def create_profiles_contract_invoice_batch(self, cursor, uid, inv_id,
                                               context=None):
        """
        Create contract_profiling_batch obj
        Create profiling_batch_invoice obj
        """
        if context is None:
            context = {}

        # Write invoice_batch & invoice_contract
        contracte_lot_obj = self.pool.get('giscedata.profiles.contracte.lot')
        factura_lot_obj = self.pool.get('giscedata.profiles.factura.lot')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        lot_obj = self.pool.get('giscedata.profiles.lot')
        pol_obj = self.pool.get('giscedata.polissa')

        invoice = self.read(cursor, uid, inv_id, [
            'polissa_tg', 'polissa_id', 'data_inici', 'data_final', 'type',
            'comptadors'
        ])
        #todo: Can an invoice have meters with different technology?
        compt_id = invoice['comptadors'][0]
        technology_type = compt_obj.read(
            cursor, uid, compt_id, ['technology_type']
        )['technology_type']

        try:
            pol_data = pol_obj.read(
                cursor, uid, invoice['polissa_id'][0],
                ['agree_tipus'], context={'date': invoice['data_inici']}
            )
        except:
            pol_data = pol_obj.read(
                cursor, uid, invoice['polissa_id'][0],
                ['agree_tipus']
            )

        agree_type = pol_data['agree_tipus']

        if agree_type >= '03':
            # Create Contract-Batch & Invoice-Batch
            months = self.get_unique_months_from_invoice(
                invoice['data_inici'], invoice['data_final']
            )
            for month in months:
                di = month + '-01'
                lot_id = lot_obj.search(cursor, uid, [('data_inici', '=', di)])
                try:
                    cl_id = contracte_lot_obj.search(cursor, uid, [
                        ('polissa_id', '=', invoice['polissa_id'][0]),
                        ('lot_id', '=', lot_id[0]), ('state', '=', 'draft')
                    ])

                    # Preventive delete
                    if cl_id:
                        contracte_lot_obj.unlink(cursor, uid, cl_id)
                except:
                    # Batch doesn't exist
                    continue

                try:
                    invoice_id = factura_lot_obj.search(cursor, uid, [
                        ('factura_id', '=', invoice['id']),
                        ('lot_id', '=', lot_id[0]),
                    ])

                    # Preventive delete
                    if invoice_id:
                        factura_lot_obj.unlink(cursor, uid, invoice_id)
                except:
                    # Batch doesn't exist
                    continue

                contract_vals = {
                    'polissa_id': invoice['polissa_id'][0],
                    'lot_id': lot_id[0],
                    'state': 'draft'
                }
                if agree_type == '05':
                    if invoice['polissa_tg'] == '1':
                        inv_type = 'tg'
                    else:
                        inv_type = 'perfil'
                elif agree_type in ('03', '04'):
                    if technology_type in TECHNOLOGY_TYPE['tm']:
                        inv_type = 'tm'
                    elif technology_type in TECHNOLOGY_TYPE['tg']:
                        inv_type = 'tg'
                    else:
                        inv_type = 'perfil'

                invoice_vals = {
                    'factura_id': invoice['id'],
                    'lot_id': lot_id[0],
                    'type': inv_type,
                    'state': 'draft'
                }
                contracte_lot_obj.create(cursor, uid, contract_vals, context)
                factura_lot_obj.create(cursor, uid, invoice_vals, context)

    def check_profilable_distri(self, cursor, uid, inv_id, context=None):
        """
        Check if a invoice is profilable in distri module
        :param inv_id: factura_id
        :return: bool
        """
        account_invoice_obj = self.pool.get('account.invoice')
        pol_obj = self.pool.get('giscedata.polissa')
        read_params = [
            'invoice_id', 'tipo_factura', 'type', 'polissa_id', 'polissa_tg',
            'data_inici', 'tipo_rectificadora'
        ]

        # Check Profilable
        invoice = self.read(cursor, uid, inv_id, read_params)
        account_invoice = account_invoice_obj.browse(
            cursor, uid, invoice['invoice_id'][0]
        )
        invoice_type = account_invoice.journal_id.code

        # - Only profile energy invoices
        if invoice['tipo_factura'] != '01' or not invoice_type.startswith(
                "ENERGIA"):
            return False

        # - Only profile out_invoices or out_refunds
        if invoice['type'] in ['in_invoice', 'in_refund']:
            return False

        # - Can't profile regularizer invoices
        if invoice['tipo_rectificadora'] == REGULARIZER_TYPE:
            return False

        # - Can't profile complementary invoices
        if invoice['tipo_rectificadora'] == COMPLEMENTARY_TYPE:
            return False

        # - Can't profile a TG contract
        if invoice['polissa_tg'] == '1':
            return False

        # - Profile invoices with dates
        if 'data_inici' not in invoice.keys() or not invoice['data_inici']:
            return False

        pol_data = pol_obj.read(
            cursor, uid, invoice['polissa_id'][0],
            ['agree_tipus'], context={'date': invoice['data_inici']}
        )
        agree_type = pol_data['agree_tipus']

        # - Profile REE type 05
        if agree_type < '05':
            return False

        return True

    def check_inv_contract_batch(self, cursor, uid, inv_id, context=None):
        """
        Check if a contract/invoice batch can be created in distri module
        :param inv_id: factura_id
        :return: bool
        """
        account_invoice_obj = self.pool.get('account.invoice')
        imd_obj = self.pool.get('ir.model.data')
        read_params = [
            'invoice_id', 'tipo_factura', 'type', 'data_inici', 'llista_preu',
            'potencia'
        ]

        # Check create contract_invoice batch
        invoice = self.read(cursor, uid, inv_id, read_params)
        account_invoice = account_invoice_obj.browse(
            cursor, uid, invoice['invoice_id'][0]
        )
        invoice_type = account_invoice.journal_id.code

        # - Only energy invoices
        if invoice['tipo_factura'] != '01' or not invoice_type.startswith(
                "ENERGIA"):
            return False

        # - Only out_invoices or out_refunds
        if invoice['type'] in ['in_invoice', 'in_refund']:
            return False

        # - Invoices with dates and number
        if 'data_inici' not in invoice.keys() or not invoice['data_inici']:
            return False

        # - Not 1 and 2 REE types
        if invoice['potencia'] > 450:
            return False

        # - No Generation invoices RE & RE12
        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_distri',
            'pricelist_tarifas_generacion'
        )[1]
        if invoice['llista_preu'][0] == pricelist_id:
            return False

        return True

    def invoice_open(self, cursor, uid, ids, context=None):
        """
        Workflow: check_automatic_profile -> check_profilable -> profile ->
        create profiles_contract_inv_batch
        """

        invoice_is_open = super(GiscedataFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context)

        if self.get_automatic_profile(cursor, uid, context):
            for inv_id in ids:
                if invoice_is_open:
                    # Check create invoice contract batch
                    if self.check_inv_contract_batch(
                            cursor, uid, inv_id, context
                    ):
                        self.create_profiles_contract_invoice_batch(
                            cursor, uid, inv_id, context
                        )
                    # Check profilable
                    if self.check_profilable_distri(
                            cursor, uid, inv_id, context
                    ):
                        self.encua_perfilacio(cursor, uid, [inv_id], context)

        return invoice_is_open


GiscedataFacturacioFactura()
