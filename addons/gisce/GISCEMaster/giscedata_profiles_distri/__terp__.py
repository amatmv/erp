# -*- coding: utf-8 -*-
{
  "name": "Perfilació distribuidora",
  "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Transformar lectures en perfils a Distribuidora.
""",
  "version": "0-dev",
  "author": "GISCE",
  "category": "GISCEMaster",
  "depends": [
      "giscedata_profiles",
      "giscedata_facturacio_distri_tg"
  ],
  "init_xml": [],
  "demo_xml": [],
  "update_xml": [
      "giscedata_facturacio_view.xml",
      "security/ir.model.access.csv",
  ],
  "active": False,
  "installable": True
}
