# -*- coding: utf-8 -*-
{
    "name": "Comercialitzadores Elèctriques",
    "description": """
  Carrega les comercialitzadores com a partnes i les hi assigna la categoria de 'Empresa Comercialitzadora' (COMER)
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_comercialitzadores_data.xml"
    ],
    "active": False,
    "installable": True
}
