SELECT 'R2-' || company.codi_r2, cups.name, to_char(m1.data_inici, 'YYYYMMDD')
FROM giscedata_polissa_modcontractual AS m1
LEFT JOIN giscedata_polissa AS p ON p.id=m1.polissa_id
LEFT JOIN giscedata_cups_ps AS cups ON cups.id=m1.cups
LEFT JOIN res_company AS company ON company.partner_id=p.comercialitzadora
WHERE m1.tipus='alta'
AND m1.data_inici>=%(inici)s and m1.data_inici<=%(fi)s
ORDER BY cups.name