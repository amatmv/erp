# -*- encoding: utf-8 -*-

from osv import fields
from osv import osv
from osv.expression import OOQuery


class purchase_order(osv.osv):

    _name = 'purchase.order'
    _inherit = 'purchase.order'

    def _default_company(self, cursor, uid, context=None):
        res = context.get('company_id', False)
        if not res:
            user_obj = self.pool.get('res.users')
            columns = ['company_id.id']
            dmn = [('id', '=', uid)]
            user_query = OOQuery(user_obj, cursor, uid).select(columns).where(dmn)
            cursor.execute(*user_query)
            res = cursor.dictfetchall()[0][columns[0]]
        return res

    _columns = {
        'company_id': fields.many2one('res.company', 'Companyia'),
    }

    _defaults = {
        'company_id': _default_company,
    }


purchase_order()
