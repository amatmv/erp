# -*- coding: utf-8 -*-
{
    "name": "GISCE Purchase Multicompany",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscedata_purchase",
        "c2c_webkit_report",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "purchase_view.xml",
    ],
    "active": False,
    "installable": True
}
