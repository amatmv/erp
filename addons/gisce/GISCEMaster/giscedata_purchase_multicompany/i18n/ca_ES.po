# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-02-26 13:44\n"
"PO-Revision-Date: 2019-02-26 13:45+0000\n"
"Last-Translator: <>\n"
"Language-Team: Catalan (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_purchase_multicompany
#: model:ir.actions.act_window,name:giscedata_purchase_multicompany.action_purchase_orders_by_company
#: model:ir.actions.act_window,name:giscedata_purchase_multicompany.action_purchase_orders_by_company_2
#: model:ir.ui.menu,name:giscedata_purchase_multicompany.menu_purchase_multicompany_test
msgid "Ordres de compra per companyia"
msgstr "Ordres de compra per companyia"

#. module: giscedata_purchase_multicompany
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscedata_purchase_multicompany
#: field:purchase.order,company_id:0
msgid "Company"
msgstr "Company"

#. module: giscedata_purchase_multicompany
#: model:ir.module.module,shortdesc:giscedata_purchase_multicompany.module_meta_information
msgid "GISCE Purchase Multicompany"
msgstr "GISCE Purchase Multicompany"

#. module: giscedata_purchase_multicompany
#: view:res.company:0
msgid "Companyies"
msgstr "Companyies"

#. module: giscedata_purchase_multicompany
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr "Invalid model name in the action definition."
