# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2016) Balears",
    "description": """
Actualitza els valors de perfilació per l'any 2016 a Balears
""",
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_perfils_2016"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_mm_2016_data.xml"
    ],
    "active": False,
    "installable": True
}
