# -*- coding: utf-8 -*-
import base64
import random
from datetime import datetime
import multiprocessing
import os
import subprocess
import tempfile
import netsvc
import zipfile
from tempfile import SpooledTemporaryFile

from osv import osv, fields
from tools import config
from tools.translate import _


forms_without_r1 = [
    '14', '13C', '13bis', '12bis', '12', '9'
]


class WizardCircular42015(osv.osv_memory):
    _name = 'wizard.circular.4_2015'

    def add_status(self, cursor, uid, ids, status, context=None):
        if not context:
            context = None
        wiz = self.browse(cursor, uid, ids[0], context=context)
        if wiz.status:
            status = wiz.status + '\n' + status
        wiz.write({'status': status})

    def get_script_params(self, cursor, uid, ids, context=None):
        """
        Generates a list of arguments to call cnmc command

        :type cursor: Database cursor
        :type uid: User id
        :param uid: int
        :type ids: Wizard id
        :param ids: list(int), int
        :type context: OpenERP context
        :param context: dict
        :rtype: list(str)
        """

        if not context:
            context = {}
        user_obj = self.pool.get('res.users')
        user = user_obj.read(cursor, uid, uid, ['login', 'password'])
        wiz = self.browse(cursor, uid, ids[0], context=context)
        uri = 'http'
        if config.get('secure'):
            uri += 's'
        uri += '://localhost'
        args = {
            'server': uri,
            'port': config.get('port'),
            'user': user['login'],
            'password': user['password'],
            'database': cursor.dbname
        }
        if wiz.num_proc != '*':
            args['num-proc'] = int(wiz.num_proc)
        return args

    def run_script(self, cursor, uid, ids, script_args, context=None):
        if not context:
            context = {}
        env = os.environ.copy()
        sentry_dsn = config.get('sentry_dsn')
        if sentry_dsn:
            env['SENTRY_DSN'] = sentry_dsn
        env['PYTHONPATH'] = ':'.join([
            config['root_path'], config['addons_path']
        ])
        for k, v in config.options.iteritems():
            env['OPENERP_%s' % k.upper()] = str(v)

        # Afegir la crida al log
        logger = netsvc.Logger()
        logger.notifyChannel(
            'server', netsvc.LOG_INFO,
            'script executed: {}'.format(' '.join(map(str, script_args)))
        )
        retcode = subprocess.call(script_args, env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _("No s'ha pogut generar l'informe correctament. "
                  "Codi: %s") % retcode
            )
        return retcode

    def _get_sql(self, cursor, uid, filename, context=None):
        """
        Returns the SQL query

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param filename: Name of the SQL fiel
        :type filename: str
        :param context: OpenERP context
        :type context: dict
        :return: The text of the SQL file
        :rtype: str
        """

        if context is None:
            context = {}

        addons_path = config['addons_path']
        url_sql = os.path.join(
            addons_path,
            "giscedata_cnmc_circular_4_2015/sql/",
            filename
        )
        with open(url_sql, 'r') as f:
            query = f.read()
        return query

    def actualitzar_energia(self, cursor, uid, ids, context=None):
        """
        Updates the CNMC stats of the CUPS

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Id of the wizard
        :type ids: list [int]
        :param context: OpenERP context
        :type context: dict
        :return: True
        :rtype: bool
        """

        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        status = False

        if wiz.te_facturacio:
            sql_params = {
                "year": str(wiz.year)
            }

            # Posa a 0 el numero de lectures
            update_query = self._get_sql(
                cursor, uid, "reiniciar_lectures_cups.sql"
                )
            cursor.execute(update_query)

            # Calculates the field cnmc_numero_lectures
            lectures_query = self._get_sql(
                cursor, uid, "recalcular_lectures_cnmc_cups.sql"
                )
            cursor.execute(lectures_query, sql_params)

            # Calculates the field cne_anual_activa and cnmc_potencia_facturada
            if not wiz.prorratejar:
                valors_query = self._get_sql(
                    cursor, uid, "recalcular_valors_cnmc_cups.sql"
                )
            else:
                first_day_year = "{}-01-01".format(str(wiz.year))
                last_day_year = "{}-12-31".format(str(wiz.year))
                sql_params['first_day_year'] = first_day_year
                sql_params['last_day_year'] = last_day_year
                valors_query = self._get_sql(
                    cursor, uid, "recalcular_valors_prorratejats_cnmc_cups.sql"
                )
            cursor.execute(valors_query, sql_params)

            # Calculates the field cne_anual_reactiva
            reactiva_query = self._get_sql(
                cursor, uid, "recalcular_lect_reactiva_cnmc_cups.sql"
                )
            cursor.execute(reactiva_query, sql_params)
            status = True
        elif wiz.file_in_cnmc:
            filename = tempfile.mkstemp()[1]
            data = base64.decodestring(wiz.file_in_cnmc)
            with open(filename, 'w') as f:
                f.write(data)
            args = wiz.get_script_params()
            args.update({'file-input': filename})

            args = (
                ['cnmc'] +
                ['update_cnmc_stats'] +
                ['--%s=%s' % (k, v) for k, v in args.items() if v] +
                ['--no-interactive']
            )
            wiz.run_script(args, context=context)
            wiz.write({'file_in_cnmc': False})
            os.unlink(filename)
            status = True
        if status:
            wiz.add_status(
                _(u"Valors actualitzats correctament per l'any %s") % wiz.year
            )
        return True

    def actualitzar_cinis(self, cursor, uid, ids, context=None):
        if not context:
            context = None
        wiz = self.browse(cursor, uid, ids[0], context=context)
        if wiz.file_in_cinis:
            filename = tempfile.mkstemp()[1]
            data = base64.decodestring(wiz.file_in_cinis)
            with open(filename, 'w') as f:
                f.write(data)
            args = wiz.get_script_params()
            args.update({'file-input': filename})
            args = (
                ['cnmc'] +
                ['update_cinis_comptador'] +
                ['--%s=%s' % (k, v) for k, v in args.items() if v] +
                ['--no-interactive']
            )
            wiz.run_script(args, context=context)
            wiz.write({'file_in_cinis': False})
            os.unlink(filename)
            wiz.add_status(_(u"CINIS de comptadors actualitzats correctament"))
        return True

    def action_gen_formulari(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        args = wiz.get_script_params()
        output = tempfile.mkstemp()[1]
        args.update({
            'year': wiz.year, 'output': output
        })
        if wiz.type not in forms_without_r1:
            args.update({
                'codi-r1': wiz.codi_r1
            })
        args = (
            ['cnmc'] +
            ['cir_4_2015_f%s' % str.lower(wiz.type)] +
            ['--%s=%s' % (k, v) for k, v in args.items() if v] +
            ['--no-interactive']
        )
        
        if wiz.reducir_cups:
            args.append("--reducir-cups")
        if wiz.type == '12bis':
            if wiz.doslmesp:
                args = args + ['--doslmesp']
            else:
                args = args + ['--no-doselemesp']

        if wiz.type == '12bis':
            if wiz.fia:
                args = args + ['--fiabilitat']
            else:
                args = args + ['--no-fiabilitat']

        if wiz.type == '13C':
            if wiz.f13c_only_auto:
                args = args + ['--only-int-auto']
            else:
                args = args + ['--all-int']

        if wiz.type == '9':
            if wiz.f9_alternative_format:
                args = args + ['--alternative']

        if wiz.type == '1':
            args = args + ["--zona_qualitat={}".format(wiz.zona_qualitat)]
            if not wiz.last_comptador:
                args = args + ['--allow-cna']
            else:
                args = args + ['--no-allow-cna']

        if wiz.type == '10':
            args[1] = 'cir_4_2015_f10at'
            wiz.run_script(args, context=context)
            at = open(output).read()
            args[1] = 'cir_4_2015_f10bt'
            wiz.run_script(args, context=context)
            bt = open(output).read()
            result = base64.b64encode(at+bt)
        else:
            wiz.run_script(args, context=context)
            result = base64.b64encode(open(output).read())
        filename = 'CIR4_2015_%s_R1-%s_%s.txt' % (wiz.type, wiz.codi_r1,
                                                  wiz.year)
        if len(result) <= 0:
            status = _(
                u"No s'ha generat el fitxer de l'informe %s ja que no s'han "
                u"trobat registres coincidents amb els paràmetres de cerca. "
                u"Per mes informació reviseu la documentació."
            ) % filename
        else:
            status = _(
                u"El fitxer de l'informe %s s'ha generat correctament"
            ) % filename
        wiz.write({
            'name': filename, 'file_out_cnmc': result, 'state': 'done',
            'status': status
        })

    def action_check_files(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        state = 'init'
        try:
            generated = False
            while not generated:
                if not os.path.exists("/tmp/validation_files"):
                    os.mkdir("/tmp/validation_files")
                folder = random.randint(1, 1000000)
                if not os.path.exists("/tmp/validation_files/%s" % folder):
                    os.mkdir("/tmp/validation_files/%s" % folder)
                    generated = True

            base64_zip = wiz.files_to_check
            decoded_base64 = base64.b64decode(base64_zip)
            directory = "/tmp/validation_files/%s" % folder
            with SpooledTemporaryFile() as tmp:
                tmp.write(decoded_base64)
                archive = zipfile.ZipFile(tmp, 'r')
                for f in archive.filelist:
                    content = archive.read(f.filename)
                    url = "/tmp/validation_files/{}/{}".format(
                        folder,
                        f.filename
                    )
                    with open(url, 'w') as newFile:
                        newFile.write(content)
            args = (
                ['cnmc'] +
                ['validate_files'] +
                ['--dir=%s' % directory] +
                ['--lang="%s"' % context['lang']]
            )
            wiz.run_script(args, context=context)
            results = open("%s/resultats.txt" % directory).read()
            result = base64.b64encode(results)
            state = 'checked'
            info = _(u"Validació completada.")
            wiz.write({
                'name': 'resultats.txt',
                'file_out_checker': result
            })
        except:
            state = 'checked'
            info = _(u"ERROR: el format del fitxer ha de ser .zip")

        wiz.write({
            'state': state,
            'info': info
        })

    def _te_facturacio(self, cursor, uid, ids, field_name, args, context=None):
        module_obj = self.pool.get('ir.module.module')
        cou = module_obj.search_count(cursor, uid, [
            ('name', '=', 'giscedata_facturacio'),
            ('state', '=', 'installed')
        ])
        return dict.fromkeys(ids, bool(cou))

    def _default_te_facturacio(self, cursor, uid, context=None):
        fact = self._te_facturacio(cursor, uid, [1], '', None, context=context)
        return fact[1]

    def _num_proc(self, cursor, uid, context=None):
        return (
            [('*', 'Automàtic')] +
            [(str(x), ('%s proc.') % x)
                for x in xrange(1, multiprocessing.cpu_count() + 2)]
        )

    def _default_r1(self, cursor, uid, context=None):
        ''' Gets R1 code from company if defined '''
        if not context:
            context = {}

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref2

        if r1 and r1.startswith('R1-'):
            r1 = r1[3:]

        return r1 or ''

    def _default_info(self, cursor, uid, context=None):
        informacio = _(u"Seleccioni un fitxer .zip amb els fitxers de la "
                       u"circular. Aquest assistent analitzarà per tal de "
                       u"trobar errors.")
        return informacio

    _columns = {
        'type': fields.selection(
            [
                ('1', 'F1'), ('1bis', 'F1Bis'), ('9', 'F9'),
                ('10at', 'F10 AT'), ('10bt', 'F10 BT'), ('11', 'F11'),
                ('12', 'F12'), ('12bis', 'F12Bis'), ('13', 'F13'),
                ('13bis', 'F13Bis'), ('13C', 'F13C'), ('14', 'F14'),
                ('15', 'F15'), ('16', 'F16'), ('20', 'F20')
            ],
            'Formulari',
            required=True
        ),
        'codi_r1': fields.char('Codi R1', size=3, required=True),
        'state': fields.char('Estat', size=16),
        'doslmesp': fields.boolean(
            '2L+P',
            help=u"Generar l'F12Bis amb elements de 2l+p"
        ),
        'fia': fields.boolean(
            'FIA',
            help=u"Generar l'F12Bis amb elements de fiabilitat"
        ),
        'name': fields.char('Nom del fitxer', size=64),
        'year': fields.integer(
            'Any del càlcul',
            size=4,
            required=True,
            help=(
                u"Any pel qual es vol generar l'informe, normalment és "
                u"l'any anterior al vigent."
            )
        ),
        'recalcular_camps_cnmc': fields.boolean(
            'Actulitzar valors CNMC CUPS',
            help=u"Torna a actualitzar els camps d'estadística CNMC del CUPS"
        ),
        'te_facturacio': fields.function(
            _te_facturacio,
            type='boolean',
            method=True,
            string='Te facturació'
        ),
        'num_proc': fields.selection(
            _num_proc,
            'Num. Processadors',
            required=True,
            help=(
                "Número de processos a utilitzar per generar el fitxer."
                "Com més processos, més ràpid, però utilitza més recursos."
                "Automàticament utilitza un procés per processador"
            )
        ),
        'file_in_cnmc': fields.binary(
            'CUPS CSV',
            help=(u"Fitxer CSV amb les dades CNMC per CUPS "
                  u"l'any anterior en KWh:\nESXXXXXXXXXX;activa;reactiva"
                  u"potencia_facturada;numero_lecturas")
        ),
        'file_in_cinis': fields.binary(
            'Comptadors CSV',
            help=(
                "Fitxer CSV amb els CINIS dels comptadors "
                "El format és:\nNúmero de comptador;CINI"
            )
        ),
        'file_out_cnmc': fields.binary('Resultat'),
        'file_out_checker': fields.binary('Resultat'),
        'files_to_check': fields.binary(
            'Fitxers a comprovar', size=256,
            help=u'Fitxer .zip amb els formularis '
                 u'generats a validar.'
        ),
        'info': fields.text(_('Informació'), readonly=True),
        'status': fields.text('Estatus'),
        'f13c_only_auto': fields.boolean(
            'Pos. amb Int. Auto.',
            help="Generar l'F13C incloent només Posicions amb Interruptor Auto."
        ),
        'f9_alternative_format': fields.boolean(
            "Format alternatiu",
            help="Generar l'F9 amb una linia per a cada coordenada dels trams"
        ),
        "reducir_cups": fields.boolean(
            "Reduir CUPS a 20 carácters",
            help="Redueix els CUPS A 20 carácters"
        ),
        "prorratejar": fields.boolean(
            "Prorratejar energia activa",
            help=(
                "Al marcar aquesta opció l'energia activa es prorrateja en "
                "aquelles factures on el seu periode de lectura, l'any de la "
                "data d'inici es diferent a l'any de la data final."
            )
        ),
        "zona_qualitat": fields.selection(
            [("ct", "CT"), ("municipi", "Municipi")],
            "Zona qualitat", required=True,
            help="Criterio usado para la zona de calidad:\n"
                 "CT: se usará la zona de calidad del CT donde está el CUPS.\n"
                 "Municipio: se usará la zona de calidad del municipio donde "
                 "está el CUPS"
        ),
        "last_comptador": fields.boolean(
            "Últim Equip de Mesura",
            help="Al marcar aquesta opció s'utilitzarà sempre l'últim comptador"
                 " associat al CUPS per a calcular el camp 'Equipo de Mesura'"
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'doslmesp': lambda *a: True,
        'fia': lambda *a: True,
        'type': lambda *a: '1',
        'year': lambda *a: datetime.now().year - 1,
        'recalcular_camps_cnmc': lambda *a: 0,
        'te_facturacio': _default_te_facturacio,
        'num_proc': lambda *a: '*',
        'codi_r1': _default_r1,
        'info': _default_info,
        'f13c_only_auto': lambda *a: False,
        'prorratejar': lambda *a: False,
        'zona_qualitat': lambda *a: "ct",
        'last_comptador': lambda *a: False
    }


WizardCircular42015()
