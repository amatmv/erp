# -*- coding: utf-8 -*-
{
    "name": "CNMC Circular 4/2015",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Generació dels formularis de la circular 4/2015
    - F1
    - F1 bis
    - F9
    - F10
    - F11
    - F12
    - F12 bis
    - F13
    - F13 bis
    - F13 C
    - F14
    - F15
    - F20
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_administracio_publica_cne",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscedata_lectures_distri",
        "giscedata_cups_distri",
        "giscedata_at",
        "giscedata_bt",
        "giscedata_celles",
        "giscedata_tensions",
        "giscegis_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_circular_4_2015_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
