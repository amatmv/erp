UPDATE giscedata_cups_ps
SET cne_anual_reactiva = energia.reactiva
FROM(
  SELECT c.id as cups_id, COALESCE(SUM(l.consum),0.0) AS reactiva
  FROM giscedata_cups_ps c
  LEFT JOIN giscedata_polissa p ON (p.cups = c.id)
  LEFT JOIN giscedata_lectures_comptador co ON (co.polissa = p.id)
  LEFT JOIN giscedata_lectures_lectura l ON (l.comptador = co.id AND l.tipus = 'R' AND to_char(l.name,'YYYY') = %(year)s)
  WHERE
   (
          to_char(p.data_alta, 'YYYY') = %(year)s
          OR
          to_char(p.data_baixa,'YYYY') = %(year)s
          OR
          p.data_baixa IS NULL
   )
   AND p.state NOT IN ('esborrany','validar','cancelada')
  GROUP BY c.id
) energia
WHERE energia.cups_id = id
