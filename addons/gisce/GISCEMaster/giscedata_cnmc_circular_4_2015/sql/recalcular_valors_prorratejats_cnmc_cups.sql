UPDATE giscedata_cups_ps c
SET cne_anual_activa = round(
        (energia.activa - energia.activa_past_year - energia.activa_next_year)::NUMERIC,
        3
    ),
    cnmc_potencia_facturada = energia.potencia_facturada
FROM (
    SELECT
        c.id AS id ,
        c.name AS cups ,
        COALESCE(
            SUM(
                il.quantity * (fl.tipus='energia')::INT * (
                    CASE
                        WHEN i.type='out_refund' THEN -1
                        ELSE 1
                    END
                )
            ),
            0.0
        ) AS activa,
        COALESCE(
            (
                SELECT
                    COALESCE(
                        (SUM(
                            il.quantity * (fl.tipus = 'energia') :: INT * (
                                CASE
                                    WHEN i.type = 'out_refund' THEN -1
                                    ELSE 1
                                END
                               )
                            ) / (f.data_final-f.data_inici+1)::FLOAT
                        ) * (%(first_day_year)s::date-f.data_inici)::FLOAT,
                        0.0
                    ) AS activa_past_year
                FROM giscedata_cups_ps c1
                LEFT JOIN giscedata_facturacio_factura f
                    ON (f.cups_id=c1.id
                    AND (
                        to_char(f.data_final, 'YYYY') = %(year)s
                        OR to_char(f.data_inici, 'YYYY') = %(year)s
                    ))
                LEFT JOIN account_invoice i ON (i.id=f.invoice_id)
                LEFT JOIN giscedata_facturacio_factura_linia fl
                    ON (fl.factura_id=f.id
                    AND fl.tipus IN ('energia','reactiva'))
                LEFT JOIN account_invoice_line il ON (fl.invoice_line_id=il.id)
                WHERE to_char(f.data_inici, 'YYYY')::INT < 2018
                AND to_char(f.data_final, 'YYYY')::INT = 2018
                AND c1.id = c.id
                GROUP BY c1.id,c1.name, f.data_final, f.data_inici
            ),
            0.0
        ) AS activa_past_year,
        COALESCE(
            (
                SELECT
                    COALESCE(
                        (SUM(
                            il.quantity * (fl.tipus = 'energia') :: INT * (
                                CASE
                                    WHEN i.type = 'out_refund' THEN -1
                                    ELSE 1
                                END
                               )
                            ) / (f.data_final-f.data_inici+1)::FLOAT
                        ) * (f.data_final-%(last_day_year)s::date)::FLOAT,
                        0.0
                    ) AS activa_next_year
                FROM giscedata_cups_ps c1
                LEFT JOIN giscedata_facturacio_factura f
                    ON (f.cups_id=c1.id
                    AND (
                        to_char(f.data_final, 'YYYY') = %(year)s
                        OR to_char(f.data_inici, 'YYYY') = %(year)s)
                    )
                LEFT JOIN account_invoice i ON (i.id=f.invoice_id)
                LEFT JOIN giscedata_facturacio_factura_linia fl
                    ON (fl.factura_id=f.id
                    AND fl.tipus IN ('energia','reactiva'))
                LEFT JOIN account_invoice_line il ON (fl.invoice_line_id=il.id)
                WHERE to_char(f.data_inici, 'YYYY')::INT = (%(year)s)::INT
                AND to_char(f.data_final, 'YYYY')::INT > (%(year)s)::INT
                AND c1.id = c.id
                GROUP BY c1.id,c.name, f.data_final, f.data_inici
            ),
            0.0
        ) AS activa_next_year,
        COALESCE(
            (
                SELECT
                    il1.quantity
                FROM account_invoice_line il1
                INNER JOIN giscedata_facturacio_factura_linia gfl
                    ON gfl.invoice_line_id = il1.id
                INNER JOIN account_invoice i ON i.id = il1.invoice_id
                INNER JOIN giscedata_facturacio_factura gf
                    ON gf.invoice_id = i.id
                WHERE
                    gfl.tipus = 'potencia'
                    AND to_char(gf.data_final, 'YYYY') = %(year)s
                    AND gf.cups_id = c.id
                    AND gf.tipo_rectificadora IN ('N', 'R', 'RA')
                    AND i.refund_by_id IS NULL
                    ORDER BY gf.data_final DESC, il1.quantity DESC LIMIT 1
            ),
            0.0
        ) AS potencia_facturada
    FROM giscedata_cups_ps c
    LEFT JOIN giscedata_facturacio_factura f
        ON (f.cups_id=c.id
        AND (
                to_char(f.data_final, 'YYYY') = %(year)s
                OR to_char(f.data_inici, 'YYYY') = %(year)s)
        )
    LEFT JOIN account_invoice i ON (i.id=f.invoice_id)
    LEFT JOIN giscedata_facturacio_factura_linia fl
        ON (fl.factura_id=f.id AND fl.tipus IN ('energia','reactiva'))
    LEFT JOIN account_invoice_line il ON (fl.invoice_line_id=il.id)
    GROUP BY c.id,c.name
) AS energia
WHERE energia.id=c.id