UPDATE giscedata_cups_ps c
SET cnmc_numero_lectures = lectures.num_lec
FROM (
       SELECT cups.name,SUM(lectures.num_lec) AS num_lec
       FROM (
              SELECT comptador_id,count(*) AS num_lec
              FROM (
                     SELECT
                       comptador_id,
                       lectures.data_actual
                     FROM giscedata_facturacio_lectures_energia AS lectures
                     WHERE tipus='activa' AND
                       to_char(lectures.data_actual,'YYYY') = %(year)s
                     GROUP BY comptador_id,lectures.data_actual
                   ) AS compt_lect
              GROUP BY comptador_id
            ) AS lectures
         LEFT JOIN giscedata_lectures_comptador AS comptador ON (comptador.id= lectures.comptador_id)
         LEFT JOIN giscedata_polissa AS polissa ON (polissa.id = comptador.polissa)
         LEFT JOIN giscedata_cups_ps AS cups ON (cups.id = polissa.cups)
       GROUP by cups.name
       ORDER BY num_lec DESC
) AS lectures
WHERE lectures.name = c.name;