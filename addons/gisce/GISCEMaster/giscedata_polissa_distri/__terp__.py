# -*- coding: utf-8 -*-
{
    "name": "Pòlisses dels clients (Distribuidora)",
    "description": """Afegeix les polisses pels clients""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_cups_distri",
        "stock"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_polissa_distri_demo.xml"
    ],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "giscedata_cups_view.xml",
        "giscedata_polissa_data.xml",
        "giscedata_polissa_report.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
