# -*- coding: utf-8 -*-
"""Modificacions pel mòdul de CUPS des del giscedata_polissa
(distribuidora)."""
# pylint: disable-msg=W0613
from __future__ import absolute_import
from osv import osv, fields
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES


class GiscedataCupsEscomesa(osv.osv):
    """Escumesa."""
    _name = "giscedata.cups.escomesa"
    _inherit = "giscedata.cups.escomesa"

    def _polisses_actives(self, cursor, uid, ids, name, arg, context=None):
        """Retorna el llistat de pòlisses actives d'una escomesa."""
        polissa_obj = self.pool.get('giscedata.polissa')
        res = {}
        for escomesa in self.browse(cursor, uid, ids, context):
            # les pòlisses de baixa ja no surten perquè active=False
            search_params = [
                ('escomesa', '=', escomesa.name),
                ('state', 'not in', CONTRACT_IGNORED_STATES),
            ]
            res[escomesa.id] = polissa_obj.search(cursor, uid, search_params)
        return res

    def _potencia_polisses(self, cursor, uid, ids, name, arg, context=None):
        """Sumatori de la potencia d'una escomesa."""
        res = {}
        for escomesa in self.browse(cursor, uid, ids, context):
            pot = 0
            for polissa in escomesa.polisses_actives:
                pot += polissa.potencia
            res[escomesa.id] = pot
        return res

    _columns = {
      'polisses_actives': fields.function(_polisses_actives, type='one2many',
                                          relation='giscedata.polissa',
                                          method=True,
                                          string='Polisses actives'),
      'potencia_polisses': fields.function(_potencia_polisses, type='float',
                                           method=True,
                                           string='Potència de las polissas'),
    }
GiscedataCupsEscomesa()
