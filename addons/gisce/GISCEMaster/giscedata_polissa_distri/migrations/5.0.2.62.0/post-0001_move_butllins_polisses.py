import logging


def migrate(cursor, installed_version):
    logger = logging.getLogger('openerp.migration.' + __name__)
    logger.info('giscedata_polisssa_distri found we have to migrate.')

    cursor.execute("SELECT COUNT(*) FROM information_schema.columns "
                   "WHERE table_name='giscedata_butlleti' AND "
                   "column_name='cups_id'")

    res = cursor.fetchall()
    if not res[0][0]:
        logger.info('creating giscedata_butlleti.cups_id column.')
        cursor.execute("""
        ALTER TABLE giscedata_butlleti ADD COLUMN cups_id INTEGER REFERENCES
        giscedata_cups_ps on DELETE CASCADE
        """)
    else:
        logger.info('Found giscedata_butlleti.cups_id column.')

    cursor.execute("""
    UPDATE giscedata_butlleti
        SET cups_id = b.cups_id
    FROM (
        SELECT
          b.id as id,
          b.polissa_id as polissa_id,
          b.name as name,
          p.cups as cups_id
          FROM giscedata_butlleti b
          LEFT JOIN giscedata_polissa p on (b.polissa_id = p.id)
        )
        AS b
    WHERE giscedata_butlleti.id = b.id
    """)
    return True
