# -*- coding: utf-8 -*-

import pooler
from addons.gisceupgrade_migratoor.gisceupgrade_migratoor import log

def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    pol_obj = pool.get('giscedata.polissa')
    pol_ids = pol_obj.search(cursor, uid, [('distribuidora', '=', False)],
                             context={'active_test': False})
    if pol_ids:
        log("Assignant distribuidora a %s pòlisses" % len(pol_ids))
        distri = pol_obj.default_distribuidora(cursor, uid)
        pol_obj.write(cursor, uid, pol_ids, {'distribuidora': distri})
    # Assignem la comercialitzadora a la modcontractual
    pol_ids = pol_obj.search(cursor, uid, [], context={'active_test': False})
    log("Assignant comercialitzadora a %s pòlisses i a les seves "
        "modificacions contractuals" % len(pol_ids))
    mc_obj = pool.get('giscedata.polissa.modcontractual')
    for pol in pol_obj.read(cursor, uid, pol_ids, ['name', 'active', 'tarifa',
                                                  'modcontractuals_ids']):
        if not pol['name'] or not pol['active'] or not pol['tarifa']:
            continue
        vals = mc_obj.get_vals_from_polissa(cursor, uid, pol['id'])
        #mod_ids = [a.id for a in pol.modcontractuals_ids]
        mc_obj.write(cursor, uid, pol['modcontractuals_ids'], vals)