# -*- coding: utf-8 -*-
"""Modificacions al mòdul base (res_partner)."""
from osv import osv, fields

class ResPartner(osv.osv):
    _name = "res.partner"
    _inherit = "res.partner"

    def _polisses_addresses_search(self, cursor, uid, obj, name, args,
                                   context=None):
        if not len(args):
            return []
        else:
            cursor.execute("""select pa.id from giscedata_polissa p, 
            giscedata_cups_ps c, res_partner pa where p.cups = c.id and 
            p.baixa = False and p.titular = pa.id and (
            c.carrer || ' ' || c.numero || ' ' || c.escala || ' ' || case 
            when c.pis is null then '' else c.pis end || ' ' || case 
            when c.porta is null then '' 
            else c.porta end || ' ' || c.poblacio) 
            ilike %s""", ('%'+args[0][2].replace(' ', '%')+'%',));
            res = cursor.fetchall()
            if not len(res):
                return [('id', '=', '0')]
            return [('id', 'in', map(lambda x: x[0], res))]

    def _polisses_addresses(self, cursor, uid, ids, name, arg, context=None):
        res = {}
        for partner in self.browse(cursor, uid, ids, context):
            text = ''
            for polissa in partner.polisses:
                text += "%s\n" % (polissa.cups_direccio)
            res[partner.id] = text[0:-1]
        return res


    _columns = {
      'polisses': fields.one2many('giscedata.polissa', 'titular',
                                  'Polisses'),
      'polisses_addresses': fields.function(_polisses_addresses, type='text',
                                        size='255',
                                        fnct_search=_polisses_addresses_search,
                                        method=True,
                                        string='Adreces de pòlisses'),
    }

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        default.update({
          'polisses': [],
          'polisses_addresses': [],
        })
        super(ResPartner, self).copy(cr, uid, id, default, context)

ResPartner()
