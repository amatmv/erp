    <%inherit file="/giscedata_polissa/report/contracte.mako"/>
    <%block name="custom_css" >
        <link rel="stylesheet"
          href="${addons_path}/giscedata_polissa/report/stylesheet_generic_contract.css"/>
    </%block >
    <%block name="info_titular">
        ${_("DADES DEL TITULAR")}
        <br>
        %for polissa in objects:
            <div>
                <table style="margin-top: 5px;">
                    <tr>
                        <td style="width: 500px;">
                            <span class="label">${_("Nom:")}</span>
                            <span class="field">${polissa.titular.name}</span>
                        </td>
                        <td style="width: 250px;">
                            <span class="label">${_("NIF/CIF:")}</span>
                            <span class="field">${(polissa.titular.vat.replace('ES', '') or '')}</span>
                        </td>
                    </tr>
                </table>
            </div>
        %endfor

    </%block>