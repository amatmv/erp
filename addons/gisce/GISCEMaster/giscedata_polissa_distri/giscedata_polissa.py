# -*- coding: utf-8 -*-
"""Mòdul giscedata_polissa (distribuidora)."""

from osv import osv, fields
from gestionatr.defs import TABLA_116

SOCIALBONUS_TYPES = TABLA_116

class GiscedataTallMotiu(osv.osv):

    _name = 'giscedata.polissa.motiu.tall'

    _columns = {
        'name': fields.char('Motiu', size=240),
    }

GiscedataTallMotiu()


class GiscedataPolissa(osv.osv):
    """Pòlissa (distribuidora)."""

    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'
    _description = "Polissa d'un Client"

    def copy_data(self, cursor, uid, id, default=None, context=None):
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'ref_comer': False
        }
        default.update(default_values)
        res_id = super(
            GiscedataPolissa, self).copy_data(cursor, uid, id, default, context)
        return res_id

    def default_distribuidora(self, cursor, uid, context=None):
        """Obtenim el valor per defecte de distribuidora.
        """
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        return user.company_id.partner_id.id

    def onchange_comercialitzadora(self, cursor, uid, ids, comercialitzadora,
                                   context=None):
        """Funció per quan canviem la comercialitzadora.

        Hem de buscar l'última modificació contractual per aquesta pòlissa i
        comercialitzadora. Si en trobem una agafem el camp ref_comer si no en
        trobem camp ho posarem a Null (False).
        """
        vals = {'domain': {}, 'value': {}, 'warning': {}}
        if not ids or not comercialitzadora:
            vals['value'].update({'ref_comer': False})
            return vals
        modcontractual_obj = self.pool.get('giscedata.polissa.modcontractual')
        # Busquem l'útlima modificació contractual que tingui referència
        for polissa in self.browse(cursor, uid, ids, context):
            ref_comer = False
            search_params = [
                ('polissa_id.id', '=', polissa.id),
                ('comercialitzadora.id', '=', comercialitzadora),
                ('ref_comer', '!=', False)
            ]
            mod_ids = modcontractual_obj.search(cursor, uid, search_params,
                                                order="data_final desc",
                                                limit=1,
                                                context={'active_test': False})
            if mod_ids:
                ref_comer = modcontractual_obj.browse(cursor, uid,
                                                      mod_ids[0]).ref_comer
            vals['value'].update({'ref_comer': ref_comer})
            return vals

    def _get_polissa_from_cups(self, cursor, uid, ids, context=None):
        '''return polissa associated with cups in ids'''
        polissa_obj = self.pool.get('giscedata.polissa')
        search_params = [('cups', 'in', ids)]
        return list(set(polissa_obj.search(cursor, uid, search_params)))

    def _get_escomesa(self, cursor, uid, ids, name, arg, context=None):
        '''return escomesa name associated to cups in polissa ids'''
        res = {}
        for polissa in self.browse(cursor, uid, ids):
            res[polissa.id] = (polissa.cups
                               and polissa.cups.id_escomesa
                               and polissa.cups.id_escomesa.name
                               or '')
        return res

    _columns = {
        'codi': fields.char('Codi Intern', size=60, readonly=True,
                            states={'esborrany': [('readonly', False)],
                                    'validar': [('readonly', False)]}),
        'ref_comer': fields.char('Referència comercialitzadora', size=60),
        'comercialitzadora': fields.many2one('res.partner',
                                    'Comercialitzadora',
                                    domain=[('supplier', '=', 1)],
                                    readonly=True,
                                    states={
                                        'esborrany': [('readonly', False)],
                                        'validar': [('readonly', False),
                                                    ('required', True)],
                                        'modcontractual': [('readonly', False),
                                                           ('required', True)]
                                    }),
        'distribuidora': fields.many2one('res.partner', 'Distribuidora',
                                         readonly=True, required=True),
        'escomesa': fields.function(_get_escomesa, type='char', method=True,
                               string="Escomesa", size=50, readonly=True,
                               store={'giscedata.polissa':
                                      (lambda self, cr, uid, ids, c=None: ids,
                                       ['cups'], 20),
                                      'giscedata.cups.ps':
                                      (_get_polissa_from_cups,
                                       ['id_escomesa'], 20)}),
        'ordre': fields.related('cups', 'ordre', type='char',
                               relation='giscedata.cups.ps',
                               string="Ordre", store=False, readonly=True,
                               size=256),
        'zona': fields.related('cups', 'zona', type='char',
                               relation='giscedata.cups.ps',
                               string="Zona", store=False, readonly=True,
                               size=256),
        'et': fields.related('cups', 'et', type='char',
                            relation='giscedata.cups.ps',
                            string="ET", store=False, readonly=True,
                            size=256),
        'linia': fields.related('cups', 'linia', type='char',
                               relation='giscedata.cups.ps',
                               string="Línia", store=False, readonly=True,
                               size=256),
        'cedules': fields.one2many('giscedata.cedula', 'polissa_id',
                                   'Cèdules'),
        'icps': fields.one2many(
            'giscedata.polissa.icp', 'polissa_id', 'ICPs',
            context={'active_test': False},
        ),
        'motiu_tall_id': fields.many2one('giscedata.polissa.motiu.tall',
                                        'Motiu de tall'),
        'titular': fields.many2one('res.partner', 'Titular', readonly=True,
                                   states={'esborrany': [('readonly', False)],
                                           'validar': [('readonly', False),
                                                       ('required', True)],
                                           'modcontractual': [
                                               ('readonly', False),
                                               ('required', True)]
                                           }, size=40),
        'bono_social': fields.selection(SOCIALBONUS_TYPES,
                                        'Bo social', required=False,
                                        readonly=True,
                                        states={
                                            'esborrany': [('readonly', False)],
                                            'modcontractual': [
                                                ('readonly', False)
                                            ],
                                            'validar': [('readonly', False)]
                                        }),
    }

    _defaults = {
        'distribuidora': default_distribuidora,
        'bono_social': lambda *a: '0'
    }


GiscedataPolissa()


class GiscedataPolissaModContractual(osv.osv):
    """Modificació contractual (distribuidora)."""

    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    _columns = {
        'comercialitzadora': fields.many2one('res.partner',
                                    'Comercialitzadora',
                                    domain=[('supplier', '=', 1)],
                                    required=True),
        'ref_comer': fields.char('Referència comercialitzadora', size=60),
        'bono_social': fields.selection(SOCIALBONUS_TYPES,
                                        'Bo social', required=False,
                                        readonly=True),

    }
    _defaults = {
        'bono_social': lambda *a: '0'
    }
GiscedataPolissaModContractual()


class GiscedataPolissaIcp(osv.osv):
    """ICP d'una pòlissa.
    """
    _name = 'giscedata.polissa.icp'
    _order = 'data_alta desc'

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa',
                                      required=True, select=True),
        'name': fields.char('Nº de sèrie', size=64,
                            required=True, select=True),
        'propietat': fields.selection([('empresa', 'Empresa'),
                                       ('client', 'Client')], 'Propietat'),
        'intensitat': fields.float('Intensitat'),
        'data_alta': fields.date('Data alta'),
        'data_verificacio': fields.date('Data de verificació'),
        'serial': fields.many2one("stock.production.lot",
                                  "Nº de sèrie (magatzem)", select=True),
        'product_id': fields.related('serial', 'product_id', type='many2one',
                                     relation='product.product', store=True,
                                     readonly=True, string="Producte"),
        'observacions': fields.text('Observacions'),
        'active': fields.boolean('Activo'),
    }

    _defaults = {
        'intensitat': lambda *a: 0.0,
        'propietat': lambda *a: 'empresa',
        'active': lambda *a: True,
    }

GiscedataPolissaIcp()


class GiscedataCedula(osv.osv):
    """Cèdula d'habitabilitat"""
    _name = "giscedata.cedula"
    _description = __doc__

    def _get_polissa(self, cursor, uid, context=None):

        if not context:
            context = {}
        return context.get('polissa_id', False)

    _columns = {
        'name': fields.char('Nº Cèdula', size=32, required=True,
                            readonly=False),
        'data_inici': fields.date('Data inici', required=True),
        'data_final': fields.date('Data final', required=True),
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True, ondelete='cascade'),
    }

    _defaults = {
        'polissa_id': _get_polissa,
    }

GiscedataCedula()
