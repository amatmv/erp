# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataTalaCodi(osv.osv):
    _name = 'giscedata.tala.codi'
    _inherit = 'giscedata.tala.codi'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_tala_codi_rel',
            'tala_codi_id',
            'obra_id',
            'Obres'
        )
    }

GiscedataTalaCodi()