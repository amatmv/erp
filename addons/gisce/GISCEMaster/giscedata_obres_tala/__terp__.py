# -*- coding: utf-8 -*-
{
    "name": "Obres per tala",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a les codis de tala
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_tala",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tala_view.xml"
    ],
    "active": False,
    "installable": True
}
