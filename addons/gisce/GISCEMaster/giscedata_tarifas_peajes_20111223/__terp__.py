# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes 23 desembre 2011",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 236 - 30/09/2011.
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20111223_data.xml"
    ],
    "active": False,
    "installable": True
}
