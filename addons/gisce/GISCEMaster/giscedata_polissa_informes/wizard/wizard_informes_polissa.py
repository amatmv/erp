# -*- coding: utf-8 -*-

from osv import osv, fields
import time


class WizardInformesPolissa(osv.osv_memory):

    _name = "wizard.informes.polissa"
    
    _get_informe = [('report_wizard_informes_polissa_altes','Altes'),
                    ('report_wizard_informes_polissa_baixes','Baixes'),
                   ]

    _get_with_lot_sel = [('with', 'Amb lot'),
                         ('without', 'Sense lot'),
                         ('both', 'Amb i sense lot')] 

    def imprimir(self, cr, uid, ids, context=None):
        
        wizard = self.browse(cr, uid, ids[0])

        datas = {'form':{'data_inici':wizard.data_inici,
                         'data_final':wizard.data_final,
                         'lot_facturacio': wizard.with_lot,
                        }
                }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': wizard.informe,
            'datas': datas,
        }

    _columns = {
        'data_inici': fields.date('Des de', required=True),
        'data_final': fields.date('Fins', required=True),
        'with_lot': fields.selection(_get_with_lot_sel,
                                     'Mostrar pòlisses',
                                     required=True,
                                     help=(u"Es pot filtrar per pòlisses "
                                          u"amb lot de facturació o sense ell "
                                          u"o treure tots els resultats sense filtre")),
        'informe': fields.selection(_get_informe, 'Informe', required=True),
                
    }

    _defaults = {
        'with_lot': lambda *a: 'with',
    }
    
WizardInformesPolissa()
