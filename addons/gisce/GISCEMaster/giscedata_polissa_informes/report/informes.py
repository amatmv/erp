# -*- coding: utf-8 -*-

import jasper_reports

def informes(cursor, uid, ids, data, context):
    
    return {
        'parameters': {'data_inici':data['form']['data_inici'],
                       'data_final':data['form']['data_final'],
                       'lot_facturacio': data['form']['lot_facturacio'] },
    }

jasper_reports.report_jasper(
   'report.report_wizard_informes_polissa_altes',
   'wizard.informes.polissa',
   parser=informes
)

jasper_reports.report_jasper(
   'report.report_wizard_informes_polissa_baixes',
   'wizard.informes.polissa',
   parser=informes
)

