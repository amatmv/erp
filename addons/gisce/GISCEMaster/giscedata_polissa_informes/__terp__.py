# -*- coding: utf-8 -*-
{
    "name": "Informes per pòlisses",
    "description": """Informes de pòlisses
    * Informe d'altes i baixes agrupat per periodicitat de facturació""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "jasper_reports"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_informes_report.xml",
        "wizard/wizard_informes_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
