# -*- coding: utf-8 -*-
{
    "name": "Comerdist CUPS (Comercialitzadora)",
    "description": """
    This module provide :
      * Sincronització de CUPS.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist",
        "giscedata_cups",
        "giscedata_comerdist_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
