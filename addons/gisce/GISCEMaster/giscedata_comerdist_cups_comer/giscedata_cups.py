# -*- coding: utf-8 -*-
from osv import osv, fields
from xml import dom

class GiscedataCupsPs(osv.osv):
    """Cups pel mòdul de sincronització.
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def _ff_propi(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna si aquest CUPS ve de la sincronització o no.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        res = {}
        for cups in self.read(cursor, uid, ids, ['distribuidora_id'], context):
            if cups['distribuidora_id'][0] in partner_ids:
                res[cups['id']] = 1
            else:
                res[cups['id']] = 0
        return res

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        result = super(GiscedataCupsPs,
                       self).fields_view_get(cursor, uid,view_id, view_type,
                                             context, toolbar)
        if view_type != 'form':
            return result
        mydom = dom.minidom.parseString(unicode(result['arch'], 'utf-8').encode('utf-8'))
        for field in mydom.getElementsByTagName('field'):
            if field.getAttribute('attrs'):
                attrs = eval(field.getAttribute('attrs'))
            else:
                attrs = {'readonly': []}
            for key, value in attrs.items():
                field_name = field.getAttribute('name')
                if (field_name in self._columns
                    and hasattr(self._columns[field_name], 'propi')
                    and getattr(self._columns[field_name], 'propi') == 'no'):
                    continue
                if key == 'readonly':
                    value.append(('propi', '=', 1))
                field.setAttribute('attrs', str(attrs))
        result['arch'] = mydom.toxml()
        return result

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for cups in self.browse(cursor, uid, ids, context):
            partner_id = cups.distribuidora_id.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            config = config_obj.browse(cursor, uid, config_ids[0])
            return config


    _columns = {
        'propi': fields.function(_ff_propi, type='boolean', method=True),
    }

GiscedataCupsPs()
