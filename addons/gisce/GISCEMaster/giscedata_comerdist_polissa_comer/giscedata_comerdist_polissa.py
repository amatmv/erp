# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):
    """Pòlissa per sincronitzar.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si aquest objecte s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for modcont in self.browse(cursor, uid, ids, context):
            if modcont.distribuidora and \
            modcont.distribuidora.id in partner_ids and \
            modcont.get_config().user_local.id != uid:
                return True
            else:
                return False

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for polissa in self.browse(cursor, uid, ids, context):
            partner_id = polissa.distribuidora.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            if config_ids:
                config = config_obj.browse(cursor, uid, config_ids[0])
                return config
            else:
                return False

    def _ff_propi(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna si aquest CUPS ve de la sincronització o no.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        res = {}
        for cups in self.read(cursor, uid, ids, ['distribuidora'], context):
            if cups['distribuidora'] and \
                cups['distribuidora'][0] in partner_ids:
                res[cups['id']] = 1
            else:
                res[cups['id']] = 0
        return res

    def cnd_baixa(self, cursor, uid, ids):

        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        for polissa in self.browse(cursor, uid, ids):
            if polissa.propi:
                #Si es propi i trobem comptadors actius vol dir
                #que es un canvi de comercialitzadora
                #i no hem donat de baixa el comptador a distri
                #donem de baixa el comptador actiu
                #amb la data de baixa de la polissa
                if polissa.comptadors:
                    if polissa.comptadors[0].active:
                        polissa.comptadors[0].write(
                                    {'data_baixa': polissa.data_baixa,
                                     'active': 0})
        #Una volta hem donat de baixa els comptadors
        #ja podem fer les comprovacions
        res = super(GiscedataPolissa, self).cnd_baixa(cursor, uid, ids)

        return res

    def wkf_activa(self, cursor, uid, ids):

        for polissa in self.browse(cursor, uid, ids):
            if polissa.propi and\
                not polissa.modcontractual_activa.active:
                # Això és que venim d'una reactivació
                # reactivem el darrer comptador
                if polissa.comptadors:
                    polissa.comptadors[0].write({'data_baixa': None,
                                             'active': True})
        res = super(GiscedataPolissa, self).wkf_activa(cursor, uid, ids)

        return res

    _columns = {
        'propi': fields.function(_ff_propi, type='boolean', method=True),
    }

GiscedataPolissa()


class GiscedataPolissaModcontractual(osv.osv):
    """Modificacions contractuals per sincronitzar.
    """
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si aquest objecte s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for modcont in self.browse(cursor, uid, ids, context):
            if modcont.distribuidora and \
            modcont.distribuidora.id in partner_ids and \
            modcont.get_config().user_local.id != uid:
                return True
            else:
                return False

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for modcont in self.browse(cursor, uid, ids, context):
            partner_id = modcont.distribuidora.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            if config_ids:
                config = config_obj.browse(cursor, uid, config_ids[0])
                return config
            else:
                return False

GiscedataPolissaModcontractual()
