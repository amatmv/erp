# -*- coding: utf-8 -*-
from osv import osv, fields
from giscedata_comerdist import OOOPPool
from tools.translate import _

class GiscedataPolissaObtenirCUPS(osv.osv_memory):
    """Assistent per l'obtenció d'un CUPS.
    """
    _name = 'giscedata.polissa.obtenir_cups'

    _state_selection = [
        ('init', 'Petició'),
        ('xmlrpc_info', 'Informació del CUPS'),
        ('xmlrpc_ok', 'CUPS creat'),
    ]

    def default_state(self, cursor, uid, context=None):
        """Posem el camp state per defecte a init.
        """
        return 'init'

    def action_cups_search(self, cursor, uid, ids, context=None):
        """Busquem el CUPS remot.
        """
        wizard = self.browse(cursor, uid, ids[0], context)
        cups_obj = self.pool.get('giscedata.cups.ps')
        if cups_obj.search_count(cursor, uid, [('name', '=', wizard.name)],
                                 context={'active_test': False}):
            raise osv.except_osv('Error', 'Aquest CUPS ja existeix localment.')
        config_id = wizard.distribuidora.id
        ooop = OOOPPool.get_ooop(cursor, uid, config_id)
        results = ooop.GiscedataCupsPs.browse([('name', '=', wizard.name)])
        if len(results):
            cups = results[0]
            remote_cups_id = cups.id
            informacio = _(u'CUPS: %s\n' % cups.name)
            informacio += _(u'Direcció: %s' % cups.direccio)
        else:
            remote_cups_id = False
            informacio = _(u'No hi ha cap CUPS amb el codi %s' % wizard.name)
        wizard.write({'remote_cups_id': remote_cups_id,
                      'info': informacio,
                      'state': 'xmlrpc_info'})

    def assing_distribuidora(self, cursor, uid, ids, context=None):
        """Assigna la distribuidora local al CUPS.
        """
        wizard = self.browse(cursor, uid, ids[0], context)
        if wizard.local_cups_id:
            distribuidora_id = wizard.distribuidora.partner_id.id
            cups_obj = self.pool.get('giscedata.cups.ps')
            cups_obj.write(cursor, uid, [wizard.local_cups_id],
                           {'distribuidora_id': distribuidora_id})

    def action_cups_create(self, cursor, uid, ids, context=None):
        """Creem el CUPS local.
        """
        wizard = self.browse(cursor, uid, ids[0], context)
        config_id = wizard.distribuidora.id
        ooop = OOOPPool.get_ooop(cursor, uid, config_id)
        results = ooop.GiscedataComerdistConfig.browse([('dbname', '=',
                                                       cursor.dbname)])
        if len(results):
            config_id = results[0].id
            cups = ooop.GiscedataCupsPs.get(wizard.remote_cups_id)
            local_id = cups.create_en_comer(config_id)
            wizard.write({'local_cups_id': local_id,
                          'state': 'xmlrpc_ok',
                          'info': _(u'CUPS creat correctament')})
        else:
            raise Exception("No s'ha trobat la config remota.")
            return wizard.action_close()

    def action_cups_open(self, cursor, uid, ids, context=None):
        """Obrim el formulari del CUPS.
        """
        self.assing_distribuidora(cursor, uid, ids, context)
        wizard = self.browse(cursor, uid, ids[0], context)
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.cups.ps',
            'domain': [('id', '=', wizard.local_cups_id)]
        }

    def action_close2(self, cursor, uid, ids, context=None):
        """Tanquem la finstra.
        """
        if ids:
            self.assing_distribuidora(cursor, uid, ids, context)
        return {
            'type': 'ir.actions.act_window_close',
        }

    def action_close(self, cursor, uid, ids, context=None):
        """Tanquem la finstra.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    _columns = {
        'name': fields.char('CUPS', size=25, required=True),
        'distribuidora': fields.many2one('giscedata.comerdist.config', 'Distribuidora',
                                required=True),
        'info': fields.text('Informació'),
        'remote_cups_id': fields.integer('ID remot'),
        'local_cups_id': fields.integer('ID Local'),
        'state': fields.selection(_state_selection, 'Estat', required=True)
    }

    _defaults = {
        'state': default_state,
    }

GiscedataPolissaObtenirCUPS()
