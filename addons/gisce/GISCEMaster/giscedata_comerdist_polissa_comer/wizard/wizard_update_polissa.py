# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from giscedata_comerdist import Sync, OOOPPool
from datetime import datetime


class WizardComerdistUpdatePolissa(osv.osv_memory):

    _name = 'wizard.comerdist.update.polissa'

    def action_update(self, cursor, uid, ids, context=None):
        '''Update comer polissa with data from distri. Only
        technic values like power or meter'''

        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        tensio_obj = self.pool.get('giscedata.tensions.tensio')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        wizard = self.browse(cursor, uid, ids[0])

        polissa = wizard.polissa_id

        # Only polissa in esborrany state
        if polissa.state != 'esborrany':
            raise osv.except_osv('Error',
                                 _(u"Tan sols es poden actualitzar pòlisses "
                                   u"en estat esborrany"))

        # Get remote polissa
        if not polissa.distribuidora:
            raise osv.except_osv('Error',
                                 _(u"No es pot actualitzar una pòlissa "
                                   u"sense distribuidora "))

        config_id = polissa.get_config().id
        ooop = OOOPPool.get_ooop(cursor, uid, config_id)
        sync = Sync(cursor, uid, ooop, 'giscedata.polissa', config=config_id)
        obj = sync.get_object(polissa.id)
        if not obj:
            raise osv.except_osv('Error',
                                 _(u"No s'ha pogut trobar la pòlissa "
                                   u"a distribuidora "))

        # Get vals
        vals = {'potencia': obj.potencia,
                'tensio': obj.tensio,
                'facturacio': obj.facturacio,
                'facturacio_potencia': obj.facturacio_potencia,
                }

        # Search for tarifa acces
        tarifa_name = obj.tarifa.name
        search_params = [('name', '=', tarifa_name)]
        tarifa_ids = tarifa_obj.search(cursor, uid, search_params)
        if tarifa_ids:
            vals.update({'tarifa': tarifa_ids[0]})

        # Search for tensio
        tensio_name = obj.tensio_normalitzada.name
        search_params = [('name', '=', tensio_name)]
        tensio_ids = tensio_obj.search(cursor, uid, search_params)
        if tensio_ids:
            vals.update({'tensio_normalitzada': tensio_ids[0]})

        polissa.write(vals)

        # Delete all meters as admin
        meter_ids = [meter.id for meter in polissa.comptadors]
        meter_obj.unlink(cursor, 1, meter_ids)

        # Create a copy of the last remote meter
        r_meter = obj.comptadors[0]

        meter_vals = {'lloguer': r_meter.lloguer,
                      'name': r_meter.name,
                      'polissa': polissa.id,
                      'data_alta': r_meter.data_alta,
                      'giro': r_meter.giro,
                      'propietat': r_meter.propietat,
                      'descripcio_lloguer': r_meter.product_lloguer_id.name,
                      'preu_lloguer': r_meter.product_lloguer_id.standard_price
                      }
        # update measure units
        if r_meter.lloguer:
            lloguer_data = self.update_lloguer(cursor, uid, r_meter, context)
            meter_vals.update({'preu_lloguer': lloguer_data['price'],
                               'uom_id': lloguer_data['uom_id']})

        meter_obj.create(cursor, uid, meter_vals)

        return {}

    def update_lloguer(self, cursor, uid, r_meter, context=None):
        '''Write rent price and uom'''

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        uom_obj = self.pool.get('product.uom')
        #remote variables
        r_uos = r_meter.property_uom_id
        r_uom = r_meter.product_lloguer_id.uom_id
        r_rent_price = r_meter.product_lloguer_id.standard_price

        #local variables
        search_params = [('name', '=', r_uos.name),
                         ('factor', '=', r_uos.factor)]
        uos_id = uom_obj.search(cursor, uid, search_params)[0]
        uos = uom_obj.browse(cursor, uid, uos_id)

        search_params = [('name', '=', r_uom.name),
                         ('factor', '=', r_uom.factor)]
        uom_id = uom_obj.search(cursor, uid, search_params)[0]
        uom = uom_obj.browse(cursor, uid, uom_id)

        new_uos_id = uos_id
        if not uos.factor % 365 or not uos.factor % 366:
            today = datetime.now().strftime('%Y-%m-%d')
            new_uos_id = facturador_obj.get_uoms_leaps(cursor, uid,
                                                       uos.id, today)
        if new_uos_id != uos_id:
            uos = uom_obj.browse(cursor, uid, new_uos_id)

        rent_price = uom_obj._compute_price(cursor, uid, uom.id,
                                            r_rent_price, uos.id)

        return {'price': rent_price, 'uom_id': uos.id}


    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True)
    }


WizardComerdistUpdatePolissa()
