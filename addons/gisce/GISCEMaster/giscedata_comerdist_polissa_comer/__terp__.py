# -*- coding: utf-8 -*-
{
    "name": "Comerdist pòlissa (comercialitzadora)",
    "description": """
    This module provide :
      * Part específica de sincronització de pòlisses en la comercialitzadora.
    """,
    "version": "0-dev",
    "author": "GISCe",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist_polissa",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_obtenir_cups_view.xml",
        "wizard/wizard_update_polissa_view.xml",
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
