# -*- coding: utf-8 -*-
{
    "name": "GISCE Data CUPS (Comercialitzadora)",
    "description": """Codi Universal de Punt de Provisionament""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "CUPS",
    "depends":[
        "base",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_cups_comer_demo.xml"
    ],
    "update_xml":[
        "giscedata_cups_comer_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
