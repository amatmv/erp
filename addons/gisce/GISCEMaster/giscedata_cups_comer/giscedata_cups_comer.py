# -*- coding: utf-8 -*-
"""Classes pel mòdul giscedata_cups (Comercialitzadora)."""
from osv import osv, fields
from tools.translate import _

class GiscedataCupsPs(osv.osv):
    """Classe d'un CUPS (Punt de servei)."""
    
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def whereiam(self, cursor, uid, context=None):
        '''retorna si estem a distri o a comer depenent de si el
            cups pertany a la nostra companyia o no'''
        return 'comer'
    
    _columns = {
        'distribuidora_id': fields.many2one('res.partner', 'Distribuidora',
                                            required=False)
    } 
    
GiscedataCupsPs()
