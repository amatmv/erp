# -*- coding: utf-8 -*-
{
    "name": "Comerdist Partner (comer)",
    "description": """
    This module provide :
      * Sync of partners and partner address
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa_comer",
        "giscedata_comerdist_partner"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
