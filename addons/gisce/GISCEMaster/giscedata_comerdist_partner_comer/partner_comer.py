# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config


class ResPartnerComerdistDist(osv.osv):
    """sync partners
    """
    _name = 'res.partner'
    _inherit = 'res.partner'

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        _queryfile = ("%s/giscedata_comerdist_partner_comer/sql/"
                      "query_partner_in_polissa.sql") %  config['addons_path']
        _query = open(_queryfile).read()
        for partner in self.browse(cursor, uid, ids, context):
            cursor.execute(_query, (partner.id, partner.id, partner.id,))
            comer_ids = cursor.fetchall()
            if not comer_ids:
                return False
            search_params = [('partner_id.id', 'in', comer_ids)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            if config_ids:
                sync_config = config_obj.browse(cursor, uid, config_ids[0])
                return sync_config
            return False

ResPartnerComerdistDist()


class ResPartnerAddressComerdistDist(osv.osv):
    """sync partners address
    """
    _name = 'res.partner.address'
    _inherit = 'res.partner.address'

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        _queryfile = ("%s/giscedata_comerdist_partner_comer/sql/"
                      "query_partner_in_polissa.sql") %  config['addons_path']
        _query = open(_queryfile).read()
        for address in self.browse(cursor, uid, ids, context):
            partner_id = address.partner_id and address.partner_id.id or 0
            cursor.execute(_query, (partner_id, partner_id, partner_id,))
            distri_ids = cursor.fetchall()
            if not distri_ids:
                return False
            search_params = [('partner_id.id', 'in', distri_ids)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            if config_ids:
                sync_config = config_obj.browse(cursor, uid, config_ids[0])
                return sync_config
            return False

ResPartnerAddressComerdistDist()