# -*- encoding: utf-8 -*-
from __future__ import absolute_import
import re
from collections import namedtuple
from datetime import datetime

from giscedata_facturacio.giscedata_facturacio \
    import ALL_RECTIFICATIVE_INVOICE_TYPES
from gestionatr.output.messages import sw_f1 as new_f1
from osv import osv
from switching.output.messages import facturacio as f1
from tools.translate import _


class GiscedataFacturacioFactura(osv.osv):
    """Customització per distribució.
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def export_f1(self, cursor, uid, ids, context=None):
        """Retorna la factura conceptes XML F1"""
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        fac = self.browse(cursor, uid, ids, context)
        if not fac:
            return None
        #Regex per agafar tots els tipus contractació
        if not re.match('^CONTRATACION', fac.journal_id.code):
            return super(GiscedataFacturacioFactura, self).export_f1(
                cursor, uid, ids, context)
        else:
            #Capçalera
            codi_r1 = fac.company_id.cups_code
            msg = f1.MensajeFacturacion()
            partner_ref = fac.partner_id.ref or "P%s" % fac.partner_id.id
            msg.set_agente(partner_ref)
            cod_sol = "%s%s" % (partner_ref, fac.number)
            cabecera = f1.Cabecera()
            cabecera.feed({
                'ree_emisora': codi_r1,
                'ree_destino': partner_ref,
                'proceso': 'F1',
                'paso': '01',
                'solicitud': cod_sol,
                'secuencia': '01',
                'codigo': fac.cups_id.name.ljust(22),
                'fecha': fac.date_invoice,
            })

            cli_id = fac.partner_id.vat
            cifnif = 'NI'
            if cli_id[0].isalnum():
                cifnif = 'CI'
            cliente = f1.Cliente()
            cliente.feed({
                'identificador': fac.partner_id.vat,
                'cifnif': cifnif,
            })

            #Direcció de subministre
            dirsuministro = f1.DireccionSuministro()
            dirsuministro.feed({
                'dirsuministro': fac.cups_id.direccio,
                'cups': fac.cups_id.name,
                'municipio': fac.cups_id.id_municipi.ine,
            })

            #Datos generales factura
            cif = fac.company_id.partner_id.vat
            datosgrl = f1.DatosGeneralesFactura()
            dades_grl = {
                'numero': fac.number,
                'tipo': fac.tipo_factura,
                'rectificadora': fac.tipo_rectificadora,
                'fecha': fac.date_invoice,
                'cif': cif[:2] == 'ES' and cif[2:] or cif,
                'codigo': fac.number,
                'obs': '. ',
                'importe': fac.amount_total,
                'saldo': fac.residual,
                'saldocobro': fac.residual,
            }
            if fac.tipo_rectificadora in ALL_RECTIFICATIVE_INVOICE_TYPES:
                dades_grl.update({'ref': fac.ref.number})
            datosgrl.feed(dades_grl)

            #Datos generales otras facturas
            datosotras = f1.DatosGeneralesOtrasFacturas()
            datosotras.feed({
                'direccion': dirsuministro,
                'cliente': cliente,
                'contrato': fac.polissa_id.name,
                'datosgrles': datosgrl,
                'linea': '1'})

            concepto = []
            for linia in fac.linia_ids:
                concepte_f1_obj = self.pool.get('facturacio.concepte.f1')
                concepte_f1 = '14'
                if concepte_f1_obj is not None:
                    if linia.product_id:
                        categ_id = linia.product_id.categ_id.id
                        concepte_f1 = concepte_f1_obj.get_concepte(cursor, uid,
                                                                   categ_id)
                        if not concepte_f1:
                            concepte_f1 = '14'
                #Concepte
                concepte = f1.Concepto()
                concepte.feed({
                    'tipoconcepto': concepte_f1,
                    'unidadesconcepto': linia.quantity,
                    'importeunidadconcept': linia.price_unit,
                    'importetotalconcept': linia.price_subtotal
                })
                concepto.append(concepte)

            #Concepte de IVA
            iva = []
            tax_obj = self.pool.get('account.tax')
            for tax in fac.tax_line:
                if 'IVA' in tax.name:
                    tax_id = tax_obj.search(cursor, uid,
                                            [('name', '=', tax.name)],
                                            context={'active_test': False})
                    if tax_id:
                        _tax = tax_obj.browse(cursor, uid, tax_id[0])
                    else:
                        msg = _(u"No s'ha pogut identificar la taxa %s"
                                u"") % tax.name
                        raise osv.except_osv('Error', msg)
                    niva = f1.IVA()
                    pcent = _tax.amount * 100
                    niva.feed({
                        'base': tax.base_amount,
                        'porcentaje': pcent,
                        'importe': tax.amount,
                    })
                    # s'ordenen decreixentment pel valor de la taxa
                    _pos = len(iva)
                    for pos, val in enumerate(iva):
                        if pcent > float(val.porcentaje.get_value()):
                            _pos = pos
                            break
                    iva.insert(_pos, niva)

            #Concepte de IVAIGICReducido
            ivaigicreducido = f1.IVAIGICReducido()
            ivaigicreducido.feed({
                'base': 0,
                'porcentaje': 0,
                'importe': 0,
            })

            # Factura de Conceptes
            otrasfacturas = f1.OtrasFacturas()
            otrasfacturas.feed({
                'datosotras': datosotras,
                'concepto': concepto,
                'iva': iva,
                'ivaigic': ivaigicreducido,
            })

            # Obtenim el número de compte
            banks = fac.company_id.partner_id.bank_ids
            if banks:
                # per defecte, el primer que hi hagi
                bank = banks[0]
                # si en té algun de 'default' el pillem
                for bank_ in banks:
                    if bank_.default_bank:
                        bank = bank_
                        break
            else:
                bank = namedtuple('Bank', ['iban'])
                bank.iban = ''

            registrofin = f1.RegistroFin()
            registrofin.feed({
                'importe': fac.amount_total,
                'sfacturacion': fac.residual,
                'scobro': fac.residual,
                'totalrec': 1,
                'tipomoneda': '02',
                'fvalor': fac.date_invoice,
                'flimite': fac.date_due,
                'idremesa': '0',
                'iban': bank.iban
            })

            msg.feed({
                'cabecera': cabecera,
                'facturas': [otrasfacturas, registrofin],
            })
            msg.build_tree()

            #Cal que la factura tingui una data de factura i un número
            date_invoice = datetime.strptime(fac.date_invoice, '%Y-%m-%d')
            # netejem el número de factura
            fnum = ''.join([x for x in fac.number if x.isalnum()])
            fname = u'F1_%s_%s_%s_%s.xml' % (cursor.dbname,
                                             date_invoice.strftime('%Y%m'),
                                             partner_ref, fnum)
            return fname, str(msg)

    def generate_general_data_others(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        general_data_others = new_f1.DatosGeneralesOtrasFacturas()
        supply_address = self.generate_supply_address(
            cursor, uid, fact, context
        )
        client = self.generate_client(
            cursor, uid, fact, context
        )
        general_invoice_data = self.generate_general_invoice_data(
            cursor, uid, fact, context
        )

        general_data_others.feed(
            {
                'direccion_suministro': supply_address,
                'cliente': client,
                'cod_contrato': fact.polissa_id.name,
                'datos_generales_factura': general_invoice_data,
                'fecha_boe': fact.date_boe,
            }
        )

        return general_data_others

    def generate_atr_invoices(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        if re.match('^CONTRATACION', fact.journal_id.code):
            # If the invoice is of type CONTRACTACION we don't return an atr
            # invoice. If it's not, we return the default atr invoice

            return None

        return super(
            GiscedataFacturacioFactura, self
        ).generate_atr_invoices(
            cursor, uid, fact, context
        )

    def generate_other_invoices(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        if not re.match('^CONTRATACION', fact.journal_id.code):
            # If the invoice is not of type CONTRACTACION we return the default
            # generate_other_invoices. If it is, we return the correct values

            return super(
                GiscedataFacturacioFactura, self
            ).generate_other_invoices(
                cursor, uid, fact, context
            )

        other_invoices = new_f1.OtrasFacturas()
        general_data_others = self.generate_general_data_others(
            cursor, uid, fact, context
        )
        repercussible_concept = self.generate_repercussible_concepts(
            cursor, uid, fact, context
        )
        iva = self.generate_iva(cursor, uid, fact, context)
        reduced_iva = self.generate_reduced_iva(cursor, uid, fact, context)

        other_invoices.feed(
            {
                'datos_generales_otras_facturas': general_data_others,
                'concepto_repercutible': repercussible_concept,
                'iva': iva,
                'iva_reducido': reduced_iva,
            }
        )

        return other_invoices

    def validate_journal_is_supported(self, cursor, uid, fact, context=None):
        if context is None:
            context = {}

        supported_here = fact.journal_id.code.startswith('CONTRATACION')

        supported_inherited = super(
            GiscedataFacturacioFactura, self
        ).validate_journal_is_supported(
            cursor, uid, fact, context
        )

        return supported_here or supported_inherited

GiscedataFacturacioFactura()