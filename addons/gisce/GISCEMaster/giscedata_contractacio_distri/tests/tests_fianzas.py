# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta
from json import loads


class TestsWizardFianzas(testing.OOTestCase):

    def create_fianza(self, cursor, uid, polissa_id, deposit, date_move):
        move_line_obj = self.openerp.pool.get('account.move.line')
        imd_obj = self.openerp.pool.get('ir.model.data')
        period_obj = self.openerp.pool.get('account.period')
        cfg_obj = self.openerp.pool.get('res.config')
        product_obj = self.openerp.pool.get('product.product')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        product_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_contractacio_distri', 'fianza_fianza'
        )[1]
        journal_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'facturacio_journal_energia'
        )[1]
        acc_id = product_obj.get_account(cursor, uid, product_id, 'income')
        period_id = period_obj.find(cursor, uid, date_move)[0]
        ml_id = move_line_obj.create(cursor, uid, {
            'product_id': product_id,
            'account_id': acc_id,
            'period_id': period_id,
            'date': date_move,
            'journal_id': journal_id,
            'name': 'Fianza recibida póliza %s' % polissa.name,
            'credit': deposit,
            'ref': polissa.name,
            'partner_id': polissa.titular.id
        })
        cfg_obj.set(cursor, uid, 'deposit_product_id', product_id)
        return ml_id

    def test_wizard_fianzas(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        conf_obj = self.openerp.pool.get('res.config')
        wiz_obj = self.openerp.pool.get('wizard.informes.fianzas')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri', 'fianza_fianza'
            )[1]

            conf_obj.set(cursor, uid, 'deposit_product_id', product_id)

            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol_2_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            date_move = datetime.now().strftime('%Y-%m-%d')

            fianze_line = [
                self.create_fianza(cursor, uid, pol_id, 42.0, date_move),
                self.create_fianza(cursor, uid, pol_2_id, 194.4, date_move)
            ]

            wiz_id = wiz_obj.create(cursor, uid, {'informe_type': 'complet'})

            wiz_obj.write_wizard_json_datas(cursor, uid, [wiz_id])

            data_fianzas = wiz_obj.read(
                cursor, uid, wiz_id, ['recopiled_info']
            )[0]['recopiled_info']

            data_fianzas = loads(data_fianzas)

            self.assertEqual(sorted(data_fianzas['fianzas_ids']), fianze_line)
