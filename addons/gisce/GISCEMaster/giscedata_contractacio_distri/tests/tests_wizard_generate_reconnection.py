# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from addons import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta


class TestWizardGenerateReconnectionInvoice(testing.OOTestCase):

    def setUp(self):
        self.openerp.install_module(
            'tarifas_contratacion_20091231'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20180101'
        )
        self.openerp.install_module(
            'giscedata_tarifas_peajes_20190101'
        )
        self.txn = Transaction().start(self.database)
        cursor = self.txn.cursor
        uid = self.txn.user

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        contractacio_concept_obj = self.openerp.pool.get('contractacio.distri.concept')
        prod_obj = self.openerp.pool.get('product.product')
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_contractacio_distri', 'pricelist_tarifas_contratacion'
        )[1]

        pol_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte', 'impagament', 'tallar',
        ])

        self.polissa_id = polissa_id

        type_c = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_contractacio_distri',
            'contractacio_type_generic'
        )[1]

        prod_category_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_contractacio_distri',
            'categ_derechos_reconexion'
        )[1]

        prod_id = prod_obj.search(
            cursor, uid, [('categ_id', '=', prod_category_id)]
        )
        # Create product if doesn't exit into category

        if not prod_id:
            vals = {
                "name": 'Reenganche',
                "categ_id": prod_category_id,
                "type": 'service',
                "procure_method": "make_to_stock",
                "supply_method": 'buy',
                "standard_price": 18.09,
                "list_price": 18.09,
                "uom_id": 1,
                "uom_po_id": 1,
                "pricelist_sale": pricelist_id
            }
            prod_id = prod_obj.create(cursor, uid, vals)

        concept_id = contractacio_concept_obj.search(
            cursor, uid, [('code', '=', 'enganche')])
        # Create concept if it does't exist
        if not concept_id:
            vals = {
                'name': 'Derechos de enganche / Actuaciones en equipos',
                'code': 'enganche',
                'type_id': type_c,
            }
            concept_id = contractacio_concept_obj.create(cursor, uid, vals)

    def tearDown(self):
        self.txn.stop()

    def test_reconnect_with_reconnection_invoice(self):
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscedata.facturacio.factura')
        wizard_obj = pool.get('wizard.generate.reconnection.invoice')
        pol_obj = pool.get('giscedata.polissa')

        vals = {
            'polissa_id': self.polissa_id,
            'invoicing': True
        }

        context = {
            'active_id': self.polissa_id
        }

        state = pol_obj.read(cursor, uid, self.polissa_id, ['state'])['state']
        self.assertEqual(state, 'tall')

        wiz_id = wizard_obj.create(cursor, uid, vals, context=context)
        wizard = wizard_obj.browse(cursor, uid, wiz_id)

        self.assertFalse(wizard.factura_id)
        wizard.go_invoicing_state()
        wizard.generate_reconnection()
        wiz_res = wizard_obj.read(cursor, uid, wiz_id)[0]

        self.assertTrue(wiz_res['factura_id'])
        state = pol_obj.read(cursor, uid, self.polissa_id, ['state'])['state']
        self.assertEqual(state, 'activa')

    def test_reconnect_without_reconnection_invoice(self):
        pool = self.openerp.pool
        cursor = self.txn.cursor
        uid = self.txn.user
        factura_obj = pool.get('giscedata.facturacio.factura')
        wizard_obj = pool.get('wizard.generate.reconnection.invoice')
        pol_obj = pool.get('giscedata.polissa')

        vals = {
            'polissa_id': self.polissa_id,
        }
        context = {
            'active_id': self.polissa_id
        }

        wiz_id = wizard_obj.create(cursor, uid, vals, context=context)
        wizard = wizard_obj.browse(cursor, uid, wiz_id)

        state = pol_obj.read(cursor, uid, self.polissa_id, ['state'])['state']
        self.assertEqual(state, 'tall')
        wizard.generate_reconnection()

        state = pol_obj.read(cursor, uid, self.polissa_id, ['state'])['state']
        self.assertEqual(state, 'activa')

