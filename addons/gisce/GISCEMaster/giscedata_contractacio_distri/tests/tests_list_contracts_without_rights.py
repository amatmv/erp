# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta


class TestsListContractsNoRights(testing.OOTestCase):

    def clean_rights(self, cursor, uid):
        contractacio_obj = self.openerp.pool.get('contractacio.distri')
        c_ids = contractacio_obj.search(cursor, uid, [])
        contractacio_obj.unlink(cursor, uid, c_ids)

    def test_list_all_contracts_with_no_rights(self):
        # Scenario: all contracts has no rights
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # delete all contractacio.distri
            self.clean_rights(cursor, uid)
            all_pol_ids_no_rights = pol_obj.search(cursor, uid, [])
            filter_search = pol_obj._search_fact_sense_drets(cursor, uid, obj=pol_obj, name='facturacio_sense_drets', args=[['facturacio_sense_drets', '=', True]],  context={})
            self.assertEqual(
                filter_search, [('id', 'in', all_pol_ids_no_rights)]
            )

            dict_pols = pol_obj._ff_fact_sense_drets(
                cursor, uid, all_pol_ids_no_rights, field_name='facturacio_sense_drets', args=None, context={}
            )
            self.assertEqual(
                dict_pols, dict((p_id, True) for p_id in all_pol_ids_no_rights)
            )

    def test_list_contracts_with_no_rights_one_has_these(self):
        # Scenario: all contracts has no rights less one (Polissa 1)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.clean_rights(cursor, uid)
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            contractacio_obj = self.openerp.pool.get('contractacio.distri')
            imd_obj = self.openerp.pool.get('ir.model.data')

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            contractacio_values = {
                'polissa_id': polissa_id,
                'name': 'Enganche',
                'date': datetime.now().strftime('%Y-%m-%d'),
                'factura_comer': False,
            }
            ctx = {'polissa_id': polissa_id}

            contractacio_id = contractacio_obj.create(
                cursor, uid, contractacio_values, context=ctx
            )

            all_pol_ids_no_rights = pol_obj.search(cursor, uid, [])

            args_par = [['facturacio_sense_drets', '=', True]]

            filter_search = (
                pol_obj._search_fact_sense_drets(cursor, uid, obj=pol_obj,
                                                 name='facturacio_sense_drets',
                                                 args=args_par,
                                                 context=ctx
                                                 )
            )
            self.assertEqual(
                filter_search,
                [('id', 'in', list(set(all_pol_ids_no_rights[:])
                                   - set([polissa_id])))
                 ]
            )

            dict_pols = pol_obj._ff_fact_sense_drets(
                cursor, uid, all_pol_ids_no_rights,
                field_name='facturacio_sense_drets', args=None, context={}
            )

            expected_dict = dict((p_id, True) for p_id in all_pol_ids_no_rights)
            expected_dict.update({polissa_id: False})
            self.assertEqual(dict_pols, expected_dict)

    def test_list_all_contracts_with_no_rights_limit_date_2018_03_01(self):
        # Scenario: all contracts has no rights
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.clean_rights(cursor, uid)

            pol_4 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0004'
            )[1]

            pol_obj.write(cursor, uid, pol_4, {'data_alta': '2018-04-13'})

            all_pol_ids_no_rights = pol_obj.search(
                cursor, uid, [('id', '!=', pol_4)]
            )
            filter_search = pol_obj._search_fact_sense_drets(cursor, uid, obj=pol_obj, name='facturacio_sense_drets', args=[['facturacio_sense_drets', '=', True]],  context={})
            self.assertEqual(
                filter_search, [('id', 'in', all_pol_ids_no_rights)]
            )

            dict_pols = pol_obj._ff_fact_sense_drets(
                cursor, uid, all_pol_ids_no_rights, field_name='facturacio_sense_drets', args=None, context={}
            )
            self.assertEqual(
                dict_pols, dict((p_id, True) for p_id in all_pol_ids_no_rights)
            )