# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
from datetime import datetime
from dateutil.relativedelta import relativedelta


def assertXmlEqual(self, got, want):
    from lxml.doctestcompare import LXMLOutputChecker
    from doctest import Example

    checker = LXMLOutputChecker()
    if checker.check_output(want, got, 0):
        return
    message = checker.output_difference(Example("", want), got, 0)
    raise AssertionError(message)

testing.OOTestCase.assertXmlEqual = assertXmlEqual
testing.OOTestCase.__str__ = testing.OOTestCase.id


class TestsExportF1(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscedata_tarifas_peajes_20160101'),
                ], limit=1, order='name DESC'
            )[0]
            newest_name = module_obj.read(
                cursor, uid, newest_id, ['name']
            )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestsExportF1, self).setUp()

    def activar_polissa(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def activar_polissa_48(self, txn, polissa_ref='polissa_0001'):
        cursor = txn.cursor
        uid = txn.user
        today = datetime.now()
        year = today.year
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', polissa_ref
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        final_date = datetime.strptime(
            polissa.modcontractual_activa.data_final, '%Y-%m-%d'
        )
        current_year = final_date.year
        deltayears = (year - current_year)
        if deltayears >= 0:
            final_date = final_date + relativedelta(years=deltayears)
            if final_date < today:
                final_date = final_date + relativedelta(years=1)
            if final_date.year == today.year and final_date.month == today.month:
                final_date = final_date + relativedelta(years=1)
            polissa.modcontractual_activa.write(
                {'data_final': final_date.strftime('%Y-%m-%d')}
            )

    def add_taxes_to_periods(self, txn):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.openerp.pool.get('giscedata.polissa.tarifa.periodes')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        tax_ids = tax_obj.search(
            cursor, uid, [
                ('name', '=', 'IVA 21%'),
            ]
        )

        product_ids = []
        for tarifa_id in tarifa_obj.search(cursor, uid, []):
            periode_ids = tarifa_obj.read(
                cursor, uid, tarifa_id, ['periodes']
            )['periodes']
            for periode_vals in periode_obj.read(cursor, uid, periode_ids):
                product_ids.append(periode_vals['product_id'][0])
                if periode_vals['product_reactiva_id']:
                    product_ids.append(periode_vals['product_reactiva_id'][0])
                if periode_vals['product_exces_pot_id']:
                    product_ids.append(periode_vals['product_exces_pot_id'][0])

        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, tax_ids)]
            }
        )

    def add_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA {}%'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                '|',
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'like', 'DENBT'),
                ('default_code', 'like', 'DVBT'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, iva_id)]
            }
        )

    def set_taxes_to_invoice_lines(self, txn):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA 21%'
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        for linia_id in linia_obj.search(cursor, uid, []):
            linia_vals = linia_obj.read(
                cursor, uid, linia_id, ['product_id', 'tipus']
            )
            if linia_vals.get('product_id', False):
                product_vals = product_obj.read(
                    cursor, uid, linia_vals['product_id'][0], ['taxes_id']
                )
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, product_vals['taxes_id'])
                        ]
                    }
                )
            elif linia_vals.get('tipus', '') == 'lloguer':
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, iva_id)
                        ]
                    }
                )

    def test_exportation_returns_correct_xml_on_atr_invoice(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def test_exportation_returns_correct_xml_on_others_invoice(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_contractacio_distri', 'tests',
            'fixtures', 'other_invoice_f1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.activar_polissa(txn)
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'factura_conceptes_0001'
            )[1]
            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]

            self.assertXmlEqual(generated_f1, expected_f1)

    def create_invoice_extra_line_48(self, txn, fact_id, dades_linia):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        extraline_obj = pool.get('giscedata.facturacio.extra')

        tax_obj = self.openerp.pool.get('account.tax')

        tax_ids = tax_obj.search(
            cursor, uid, [
                ('name', '=', 'IVA 21%'),
            ]
        )
        polissa_id = dades_linia['polissa_id']

        product_obj = pool.get('product.product')
        product_etu35 = product_obj.search(
            cursor, uid, [('default_code', '=', 'SETU35')]
        )[0]
        product = product_obj.browse(cursor, uid, product_etu35)
        if product.product_tmpl_id.property_account_income:
            account = product.product_tmpl_id.property_account_income.id
        else:
            account = product.categ_id.property_account_income_categ.id
        diari_obj = pool.get('account.journal')
        diari_id = diari_obj.search(cursor, uid, [('code', '=', 'ENERGIA')])[0]
        termes = dades_linia['terms']
        extraline_vals = {
            'account_id': account,
            'journal_ids': [[6, 0, [diari_id]]],
            'product_id': product_etu35,
            'price_unit': dades_linia['total'],
            'name': product.description,
            'date_line_from': '2013-01-01',
            'date_line_to': '2013-12-31',
            'date_from': '2017-07-01',
            'date_to': '2018-09-01',
            'polissa_id': polissa_id,
            'term': termes,
            'taxs_id': [(6, 0, tax_ids)],
            'notes': 'EMPTY',
        }

        return extraline_obj.create(cursor, uid, extraline_vals)

    def add_extra_line_concept_48(self, txn, fact_id, values):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool

        self.create_invoice_extra_line_48(txn, fact_id, values)
        extraline_obj = pool.get('giscedata.facturacio.extra')
        extraline_obj.compute_extra_lines(cursor, uid, [fact_id])

    def test_exportation_concept_48_1_term(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1_concept_48_1_term.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id, {'name': '0A0B0C1/'})

            self.activar_polissa_48(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            # Change values to recreate process
            factura_obj.write(cursor, uid, [fact_id], {
                'date_invoice': '2017-08-31',
                'data_inici': '2017-08-01',
                'data_final': '2017-08-31'
            })

            values = {'terms': 1, 'total': 1.54, 'polissa_id': polissa_id}
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)
            self.add_extra_line_concept_48(txn, fact_id, values)

            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]
            self.assertXmlEqual(generated_f1, expected_f1)

    def test_exportation_concept_48_12_term(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1_concept_48_12_terms.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id, {'name': '0A0B0C1/'})

            self.activar_polissa_48(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            # Change values to recreate process
            factura_obj.write(cursor, uid, [fact_id], {
                'date_invoice': '2017-08-31',
                'data_inici': '2017-08-01',
                'data_final': '2017-08-31'
            })

            values = {'terms': 12, 'total': 18.48, 'polissa_id': polissa_id}
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)
            self.add_extra_line_concept_48(txn, fact_id, values)

            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]
            self.assertXmlEqual(generated_f1, expected_f1)

    def test_exportation_concept_48_6_term(self):
        factura_obj = self.openerp.pool.get(
            'giscedata.facturacio.factura'
        )
        invoice_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_distri', 'tests',
            'fixtures', 'basic_f1_concept_48_6_terms.xml'
        )
        with open(f1_xml_path, 'r') as f:
            expected_f1 = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.write(cursor, uid, polissa_id, {'name': '0A0B0C1/'})

            self.activar_polissa_48(txn)

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0008'
            )[1]

            # Change values to recreate process
            factura_obj.write(cursor, uid, [fact_id], {
                'date_invoice': '2017-08-31',
                'data_inici': '2017-08-01',
                'data_final': '2017-08-31'
            })

            values = {'terms': 6, 'total': 18.48, 'polissa_id': polissa_id}
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)
            self.add_extra_line_concept_48(txn, fact_id, values)

            invoice_id = factura_obj.read(
                cursor, uid, fact_id, ['invoice_id']
            )['invoice_id'][0]
            invoice_obj.button_reset_taxes(cursor, uid, [invoice_id])

            fact = factura_obj.browse(cursor, uid, fact_id)

            fact.partner_id.write({'ref': '0002'})
            iban_banks = [
                bank
                for bank in fact.company_id.partner_id.bank_ids
                if bank.iban
            ]
            iban_banks[0].write({'default_bank': True})

            generated_f1 = factura_obj.export_new_f1(
                cursor, uid, fact_id, context=None
            )[1]
            self.assertXmlEqual(generated_f1, expected_f1)
