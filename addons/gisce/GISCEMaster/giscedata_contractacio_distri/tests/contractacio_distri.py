# -*- encoding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction

from expects import *


class TestContractacio(testing.OOTestCase):

    def open_contract(self, cursor, uid, contract_ref, tariff_type='BT'):
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', contract_ref
        )[1]

        vals = {'llista_preu': pricelist_id, }

        if tariff_type == 'AT':
            fare_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_31A'
            )[1]
            vals['tarifa'] = fare_id

        polissa_obj.write(cursor, uid, contract_id, vals)

        polissa_obj.send_signal(cursor, uid, [contract_id], [
            'validar', 'contracte'
        ])

        return True

    def test_fianza_1year_bt(self):
        condis_obj = self.openerp.pool.get('contractacio.distri')
        imd_obj = self.openerp.pool.get('ir.model.data')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )

        self.openerp.install_module(
            'tarifas_contratacion_20091231'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.open_contract(cursor, uid, 'polissa_0001')
            condis_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'contractacio_distri_0001'
            )[1]

            product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'fianza_fianza'
            )[1]

            res = condis_obj.compute_fianza(cursor, uid, [condis_id])

            # Calc
            # Prices 2017 (2.0A)
            # Energy: 3.450 kW * 50 horas * 0.044027 = 7,5946575
            # Power: 3.450 kW * (38.043426 / 12) = 10,937484975
            # Total: Power + Energy = 18,532142475 -> 18,53
            expect(res).to(be_a(dict))
            expect(res[condis_id]).to(be_a(dict))
            expect(round(res[condis_id]['price'], 2)).to(equal(18.53))
            expect(res[condis_id]['product_id']).to(equal(product_id))

    def test_fianza_1year_at(self):
        condis_obj = self.openerp.pool.get('contractacio.distri')
        imd_obj = self.openerp.pool.get('ir.model.data')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )

        self.openerp.install_module(
            'tarifas_contratacion_20091231'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.open_contract(cursor, uid, 'polissa_0001', 'AT')
            condis_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'contractacio_distri_0001'
            )[1]

            product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'fianza_fianza'
            )[1]

            res = condis_obj.compute_fianza(cursor, uid, [condis_id])

            # Calc
            # Prices 2017 (3.1A) P2
            # Energy: 3.450 kW * 24 * 31 * 0.4  * 0.012754 = 13,09478688
            # Power: 3.450 kW * (36.490689 / 12) = 10,491073088
            # Total: Power + Energy = 23,585859967
            expect(res).to(be_a(dict))
            expect(res[condis_id]).to(be_a(dict))
            expect(round(res[condis_id]['price'], 2)).to(equal(23.59))
            expect(res[condis_id]['product_id']).to(equal(product_id))

    def test_fianza_historico(self):
        condis_obj = self.openerp.pool.get('contractacio.distri')
        imd_obj = self.openerp.pool.get('ir.model.data')
        config_obj = self.openerp.pool.get('res.config')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )

        self.openerp.install_module(
            'tarifas_contratacion_20091231'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.open_contract(cursor, uid, 'polissa_0001')
            condis_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'contractacio_distri_0001'
            )[1]

            product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'fianza_fianza_historico'
            )[1]

            calc_fiance = condis_obj.get_historic_fiance(cursor)

            config_obj.set(cursor, uid, 'fiance_historic_value', '0.0')

            res = condis_obj.compute_fianza_historic(cursor, uid, [condis_id])

            expect(res).to(be_a(dict))
            expect(res[condis_id]).to(be_a(dict))
            expect(
                round(res[condis_id]['price'], 2)
            ).to(equal(round(float(calc_fiance), 2)))
            expect(res[condis_id]['product_id']).to(equal(product_id))

            config_obj.set(cursor, uid, 'fiance_historic_value', '25.67')

            res = condis_obj.compute_fianza_historic(cursor, uid, [condis_id])

            expect(res).to(be_a(dict))
            expect(res[condis_id]).to(be_a(dict))
            expect(round(res[condis_id]['price'], 2)).to(equal(25.67))
            expect(res[condis_id]['product_id']).to(equal(product_id))


    def test_calc_fianza(self):
        condis_obj = self.openerp.pool.get('contractacio.distri')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor

            res = condis_obj.get_historic_fiance(cursor)

            expect(res).to(be_a(float))

    def test_fianza_50_days_bt_con_factor_1_8(self):
        condis_obj = self.openerp.pool.get('contractacio.distri')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )

        self.openerp.install_module(
            'tarifas_contratacion_20091231'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol_vals_mazinger_z = {
                'contract_type': '02'  # Eventual medido x_factor = 1.8
            }

            pol_obj.write(cursor, uid, contract_id, pol_vals_mazinger_z)

            self.open_contract(cursor, uid, 'polissa_0001')
            condis_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'contractacio_distri_0002'
            )[1]

            product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'fianza_fianza'
            )[1]

            res = condis_obj.compute_fianza(cursor, uid, [condis_id])

            # Calc
            # Prices 2017 (2.0A) Contrato eventual medido factor -> 1.8
            # Energy: 4,6 kW * 50 horas * 0.044027 = 10.12621
            # Power: 4.6 kW * (38.043426 / 12) * x_factor(1.8) = 26.24996394
            # Total: Power + Energy = 36.376174
            expect(res).to(be_a(dict))
            expect(res[condis_id]).to(be_a(dict))
            expect(round(res[condis_id]['price'], 6)).to(equal(36.376174))
            expect(res[condis_id]['product_id']).to(equal(product_id))

    def test_fianza_50_days_at_con_factor_1_8(self):
        condis_obj = self.openerp.pool.get('contractacio.distri')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        self.openerp.install_module(
            'giscedata_tarifas_peajes_20170101'
        )

        self.openerp.install_module(
            'tarifas_contratacion_20091231'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            contract_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            pol_vals_mazinger_z = {
                'contract_type': '02'  # Eventual medido x_factor = 1.8
            }

            pol_obj.write(cursor, uid, contract_id, pol_vals_mazinger_z)

            self.open_contract(cursor, uid, 'polissa_0001', 'AT')
            condis_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'contractacio_distri_0002'
            )[1]

            product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_contractacio_distri',
                'fianza_fianza'
            )[1]

            res = condis_obj.compute_fianza(cursor, uid, [condis_id])

            # Calc
            # Prices 2017 (2.0A) P1
            # Energy: 4.6 kW * 24 * 31 * 0.4  * 0.012754 = 17,45971584
            # Power: 4.6 kW * (36.490689 / 12) * x_factor(1.8) = 25,17857541
            # Total: Power + Energy = 42,638291
            expect(res).to(be_a(dict))
            expect(res[condis_id]).to(be_a(dict))
            expect(round(res[condis_id]['price'], 6)).to(equal(42.638291))
            expect(res[condis_id]['product_id']).to(equal(product_id))
