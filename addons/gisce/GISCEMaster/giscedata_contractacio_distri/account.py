# *-* coding: utf-8 *-*
from osv import osv, fields


class AccountMoveLine(osv.osv):
    _name = 'account.move.line'
    _inherit = 'account.move.line'

    def get_fianzas_by_period(self, cursor, uid, start_date, end_date,
                              product_id=None, context=None):
        if context is None:
            context = {}

        search_params = [('date', '>=', start_date), ('date', '<=', end_date)]

        if product_id:
            search_params.append(('product_id', '=', product_id))

        fianzas_ids = self.search(cursor, uid, search_params, context=context)

        return fianzas_ids

    def get_related_contracts_from_move_lines(self, cursor, uid, ids, context=None):
        """

        :param cursor:
        :param uid:
        :param ids:
        :param context:
        :return: List
        """

        # Get a list of related contract ids without duplicated ids
        select_polissa_ids_from_move_lines = """
        SELECT array_agg(distinct p.id) AS polissa_ids 
            FROM account_move_line mv
            INNER JOIN giscedata_polissa p ON (mv.ref = p.name)
            WHERE
                mv.id in %s
        """
        cursor.execute(select_polissa_ids_from_move_lines, (tuple(ids),))

        polissa_ids = cursor.dictfetchall()

        return polissa_ids[0]['polissa_ids']

    def get_acomulat_positiu_negatiu_from_contract_and_product(self, cursor, uid, product_id, contract_name, positive=False, context=None):

        query_select_acomulat_contracte_linies = """
        SELECT SUM((mv.debit - mv.credit)) AS acomulat FROM account_move_line AS mv
        WHERE mv.product_id = %s AND mv.ref = %s and (mv.debit - mv.credit) %s 0 
        """
        operator = '>' if positive else '<'

        cursor.execute(
            query_select_acomulat_contracte_linies,
            (product_id, contract_name, operator)
        )

        acomulat = cursor.dictfetchall()

        if acomulat:
            return acomulat['acomulat']

        return 0.0

    def get_acomulat_positiu_negatiu_from_contract_and_product_to_date(self, cursor, uid, product_id, contract_ids, final_date, context=None):

        if not isinstance(contract_ids, (tuple, list)):
            contract_ids = (contract_ids, )

        query_select_acomulat_contracte_linies = """
        SELECT mv.ref, SUM((mv.debit - mv.credit)) AS deposit FROM account_move_line AS mv 
        RIGHT JOIN giscedata_polissa AS p ON (mv.ref = p.name)
        WHERE mv.product_id = %s AND p.id in %s and mv.date <= %s
        GROUP BY mv.ref  
        """

        cursor.execute(
            query_select_acomulat_contracte_linies,
            (product_id, tuple(contract_ids), final_date)
        )

        acomulat = cursor.dictfetchall()

        if acomulat:
            return acomulat

        return 0.0

AccountMoveLine()