# -*- encoding: utf-8 -*-

from osv import osv
from osv import fields


class GiscedataFacturacioConcepteSwitching(osv.osv):

    _name = 'facturacio.concepte.f1'

    def get_concepte(self, cursor, uid, categ_id, context=None):

        search_params = [('categ_id', '=', categ_id)]

        concept_ids = self.search(cursor, uid, search_params)
        if concept_ids:
            concept = self.read(cursor, uid, concept_ids, ['name'])[0]['name']
            return concept
        return False

    _columns = {
        'name': fields.char('Concepte', size=4),
        'categ_id': fields.many2one('product.category',
                                    'Categoria de producte'),
    }

GiscedataFacturacioConcepteSwitching()
