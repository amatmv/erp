# -*- coding: utf-8 -*-
from osv import fields, osv
from datetime import datetime
from tools.translate import _
import netsvc


class GenerateReconnectionInvoice(osv.osv_memory):

    _name = 'wizard.generate.reconnection.invoice'

    # Default fields methods

    def _get_polissa(self, cursor, uid, context=None):
        """Get polissa id from context: active_id"""
        if not context:
            context = {}
        return context.get('active_id', False)

    def _get_current_date(self, cursor, uid, context=None):
        """Return current date in str"""
        return datetime.now().strftime('%Y-%m-%d')

    def _default_info(self, cursor, uid, context=None):
        """Return info about wizard usage"""
        if not context:
            context = {}
        return _(u'Es procedira a la reconexió del client de la pólissa '
                 u'{0}.\n' +
                 u'Si el camp generar factura esta marcat es '
                 u'generara una factura de reconnexió.'
                 ).format(self.get_polissa_name(
                    cursor, uid, context.get('active_id'), context=context)
        )
    # Onchange

    def onchange_invoicing(self, cursor, uid, ids, invoicing, context=None):
        """Change state in function of invoicing checkbox"""
        if invoicing:
            return {'value': {
                'state': 'inv'}}
        else:
            return {'value': {
                'state': 'init'}}

    # Helpful functions

    def get_polissa_name(self, cursor, uid, pol_id, context=None):
        """Returns the name of current contract"""
        pol_obj = self.pool.get('giscedata.polissa')

        if pol_obj:
            pol = pol_obj.read(cursor, uid, pol_id, ['name'], context=context)
            return pol['name']
        else:
            return False

    def write_message_summary(self, cursor, uid, ids, message, context=None):
        """Write :param: message on summary"""
        if message:
            self.write(cursor, uid, ids, {'summary': message}, context=context)

    def write_summary(self, cursor, uid, ids, factura=None, context=None):
        """Depending of wizard state, write determinate message on summary"""
        if not context:
            context = {}
        # factura is object returned by browse in facturacio_factura
        header_message = _(u'Resum de reconnexió per la pòlissa amb ID {0} i '
                           u'NOM {1}:\n')
        succes_message = _(u'S\'ha reconnectat correctament, la pòlissa {0} '
                           u's\'ha tornat a activar\n')
        invoiced_message = _(u'S\'ha generat una factura de reconnexió amb '
                             u'id {0}\n')
        failure_message = _(u'No s\'ha pogut reconectar la pòlissa {0} '
                            u'degut a que {1}\n')
        sum_fields = self.read(cursor, uid, ids,
                               ['summary_state', 'summary', 'polissa_id'],
                               context=context)[0]
        pol_name = self.get_polissa_name(
            cursor, uid, sum_fields['polissa_id'], context=context)

        if sum_fields['summary_state'] == 'buit':
            message = header_message.format(sum_fields['polissa_id'], pol_name)

        elif sum_fields['summary_state'] == 'invoiced':
            if factura:
                message = sum_fields['summary'] + \
                    succes_message.format(pol_name) + \
                    invoiced_message.format(factura)
            else:
                message = sum_fields['summary'] + failure_message.format(
                    pol_name, _(u'no s\'ha pogut generar la factura de '
                                u'reconnexio')
                )

        elif sum_fields['summary_state'] == 'noti':
            message = sum_fields['summary'] + succes_message.format(pol_name)
        else:
            # Fail state
            message = sum_fields['summary'] + failure_message.format(
                        pol_name, context.get('failure_message', '')
            )
        self.write_message_summary(cursor, uid, ids, message,
                                   context=context)

    # Logic

    def check_contract_cutting_state(self, cursor, uid, ids, context=None):
        """Returns True if current contract is in cutting state"""
        pol_id = self.read(
            cursor, uid, ids, ['polissa_id'], context=context)[0]['polissa_id']

        if not pol_id:
            return False

        pol_obj = self.pool.get('giscedata.polissa')

        return pol_obj.read(cursor, uid, pol_id, ['state'], context=context
                            )['state'] in ('tall', )

    def check_exist_reconnection_invoice(self, cursor, uid, ids, context=None):
        """
        Check if someone invoice(factura) of current contract has lines
        with reconnection products, it means that there exist a reconnection

        :param cursor: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Wizard ids
        :type ids: list, int
        :param context: OpenERP context
        :type context: dict
        :return: If contract has a reconnection invoice
        :rtype: bool
        """

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fact_line_obj = self.pool.get('giscedata.facturacio.factura.linia')
        product_obj = self.pool.get('product.product')

        polissa_id = self.read(cursor, uid, ids, ['polissa_id'],
                               context=context)[0]['polissa_id']

        if polissa_id:
            # List of reconnection products ids
            reconn_prod_ids = product_obj.search(
                cursor, uid, [('default_code', 'like', 'DEN%')], context=context)

            search_params = [('polissa_id', '=', polissa_id)]
            # ALL Factures of contract
            polissa_factures_ids = fact_obj.search(
                cursor, uid, search_params, context=context
            )
            if polissa_factures_ids and reconn_prod_ids:
                search_params = [
                    ('factura_id', 'in', polissa_factures_ids),
                    ('product_id', 'in', reconn_prod_ids)
                ]

                fact_lines_prod = fact_line_obj.search(
                    cursor, uid, search_params, context=context)
                if fact_lines_prod:
                    return True
        return False

    def activar_polissa(self, cursor, uid, polissa_id, context=None):
        wf_service = netsvc.LocalService('workflow')
        wf_service.trg_validate(uid, 'giscedata.polissa', polissa_id, 'activar',
                                cursor)
        return True

    def generate_reconnection(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        pol_obj = self.pool.get('giscedata.polissa')
        self.write_summary(cursor, uid, ids, context=context)
        factura = None
        if self.check_contract_cutting_state(cursor, uid, ids, context=context):

            #if not self.check_exist_reconnection_invoice(cursor, uid, ids, context=context):
            recon_fields = self.read(cursor, uid, ids,
                                     ['state', 'invoice_date', 'polissa_id']
                                     , context=context)[0]
            if recon_fields:
                if recon_fields['state'] == 'inv':
                # Generar Factura
                    factura = pol_obj.create_reconnection_invoice(
                        cursor, uid,
                        recon_fields['polissa_id'],
                        recon_fields['invoice_date'],
                        context=context
                    )
                    if factura:
                        self.write(cursor, uid, ids,
                                   {'summary_state': 'invoiced',
                                    'factura_id': factura},
                                   context=context)
                        activated = self.activar_polissa(
                            cursor, uid,
                            polissa_id=recon_fields['polissa_id'],
                            context=context
                        )
                    else:
                        self.write(cursor, uid, ids,
                                   {'summary_state': 'fail'},
                                   context=context)
                        context['failure_message'] = _(
                            u'no s\'ha pogut generar la factura')
                else:
                    activated = self.activar_polissa(
                        cursor, uid, polissa_id=recon_fields['polissa_id'],
                        context=context
                    )
                    if activated:
                        self.write(cursor, uid, ids,
                                   {'summary_state': 'noti'},
                                   context=context)
                        return {}
                        # self.close_wizard(cursor, uid, ids, context=context)
                    else:
                        self.write(cursor, uid, ids,
                                   {'summary_state': 'fail'},
                                   context=context)
                        context['failure_message'] = _(
                            u'a fallat a l\'hora d\'activar la polissa')
            else:
                self.write(cursor, uid, ids,
                           {'summary_state': 'fail'},
                           context=context)
                context['failure_message'] = _(
                    u'a que els camps de l\'assistent no son valids')
            #else:
            #    self.write(cursor, uid, ids, {'summary_state': 'fail'})
            #    context['failure_message'] = _(
            #        u'ja existeix una factura de reconnexió')
        else:
            self.write(cursor, uid, ids, {'summary_state': 'fail'})
            context['failure_message'] = _(u'la pòlissa no esta en estat de '
                                           u'tall')
        self.write_summary(cursor, uid, ids, factura=factura, context=context)
        self.go_end_state(cursor, uid, ids, context=context)

    # Buttons
    def show_factura(self, cursor, uid, ids, context=None):
        """
        Retornem les factures creades per repassar-les
        """
        factures_ids = self.read(
            cursor, uid, ids, ['factura_id'], context=context)[0]['factura_id']

        return {
            'domain': "[('id','in', %s)]" % str([factures_ids]),
            'name': _('Factures generades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'type': 'ir.actions.act_window'
        }

    def go_init_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'init'}, context=context)

    def go_invoicing_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'inv'}, context=context)

    def go_end_state(self, cursor, uid, ids, context=None):
        self.write(cursor, uid, ids, {'state': 'end'}, context=context)

    def close_wizard(self, cursor, uid, ids, context=None):
        return {}

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Polissa',
                                      required=True, select=True),
        'invoice_date': fields.date('Data de facturació'),
        'info': fields.text('Descripció', readonly=True),
        'state': fields.selection(
            [('init', 'Init'), ('end', 'End'), ('inv', 'Inv')], 'Estat'),
        'summary': fields.text('Resum'),
        'summary_state': fields.selection(
            [('buit', 'Buit'), ('invoiced', 'Factura generada amb reconnexió'),
             ('noti', 'Factura no generada però amb reconnexió'),
             ('fail', 'No reconnectat')
             ], 'Estat'),
        'invoicing': fields.boolean('Generar factura de reconnexió?'),
        'factura_id': fields.integer('Factura de reconnexió')
    }

    _defaults = {
        'polissa_id': _get_polissa,
        'invoice_date': _get_current_date,
        'info': _default_info,
        'state': lambda *a: 'init',
        'summary': lambda *a: '',
        'summary_state': lambda *a: 'buit',
        'invoicing': lambda *a: False,
        'factura_id': lambda *a: False
    }

GenerateReconnectionInvoice()
