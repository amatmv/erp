# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
import time


class ContractacioDistriCompute(osv.osv_memory):
    '''Compute concepts'''

    _name = 'contractacio.distri.compute'

    _columns = {
        'obra': fields.float('Obra', digits=(16, 2)),
        'desp': fields.many2one('product.product',
                               'Desplaçament', required=False),
    }

    def fields_get(self, cursor, uid, fields=None,
                   context=None, read_access=True):
        '''dynamically add fields'''

        result = super(ContractacioDistriCompute, self).\
                        fields_get(cursor, uid, fields, context)
        contractacio_obj = self.pool.get('contractacio.distri')
        contractacio_id = context.get('active_ids', [])
        if not contractacio_id:
            return result
        contractacio_id = contractacio_id[0]
        contractacio = contractacio_obj.browse(cursor, uid, contractacio_id)

        dic = {}
        for concept in contractacio.concept_ids:
            dic[concept.id] = concept.code
        if 'obra' in dic.values():
            result['obra'] = {'string': 'Quantitat',
                              'type': 'float',
                              'digits': '(16, 2)'}
        if 'desp' in dic.values():
            result['desp'] = {'string': 'Desplaçament',
                              'type': 'many2one',
                              'relation': 'product.product'}

        return result

    def fields_view_get(self, cursor, uid, view_id=None,
                        view_type='form', context=None, toolbar=False):
        '''dynamically add fields'''
        if context is None:
            context = {}

        result = super(ContractacioDistriCompute, self).\
                        fields_view_get(cursor, uid, view_id,
                                        view_type, context=context,
                                        toolbar=toolbar)

        contractacio_obj = self.pool.get('contractacio.distri')
        contractacio_id = context.get('active_ids', [])
        if not contractacio_id:
            return result
        contractacio_id = contractacio_id[0]
        contractacio = contractacio_obj.browse(cursor, uid, contractacio_id)
        fields = {}
        dic = {}
        for concept in contractacio.concept_ids:
            dic[concept.id] = concept.code

        xml = '''<?xml version="1.0"?>\n'''\
              '''<%s string="Dades">\n\t''' % (view_type,)

        if 'obra' in dic.values():
            #si hi ma de obra hem de afegir els
            #camps de quantitat i desplacament
            xml += '''<separator string="Ma d'obra" colspan="4"/>'''
            xml += '''<field name="obra"/>\n'''
        if 'desp' in dic.values():
            xml += '''<separator string="Desplaçament" colspan="4"/>'''
            xml += '''<field name="desp"/>\n'''
        if 'obra' not in dic.values() and 'desp' not in dic.values():
            xml += '''<label string="No fan falta dades extra '''\
                   '''per fer els càlculs" colspan="4" rowspan="4"/>'''

        xml += '''<group colspan="4" col="2">'''
        xml += '''<button special="cancel"
        string="Cancel·lar" icon="gtk-cancel"/>'''
        xml += '''<button name="action_compute" string="Calcular"
        type="object" icon="gtk-go-forward"/>'''
        xml += '''</group>'''

        xml += '''</%s>''' % (view_type,)

        result['arch'] = xml
        result['fields'] = self.fields_get(cursor, uid, fields, context)
        return result

    def action_compute(self, cursor, uid, ids, context=None):
        '''compute lines from concepts'''

        wizard = self.browse(cursor, uid, ids[0])
        contractacio_obj = self.pool.get('contractacio.distri')
        contractacio_line_obj = self.pool.get('contractacio.distri.line')
        contractacio_id = context.get('active_ids', [])[0]
        contractacio = contractacio_obj.browse(cursor, uid, contractacio_id)

        #Only compute if not invoices done yet
        if contractacio.factures_ids:
            raise osv.except_osv(_('Error'),
                                 _(u"Ja hi han factures emeses "
                                   u"d\'aquesta contractació"))
            return {}
        #Remove auto lines
        for line in contractacio.line_ids:
            if line.auto:
                contractacio_line_obj.unlink(cursor, uid, line.id, context)

        for concept in contractacio.concept_ids:
            product_id = None
            quantity = 1
            if concept.code in ['extensio', 'acces']:
                #This concepts compute using pot_res
                quantity = contractacio.pot_res
            elif concept.code == 'local':
                #This concept compute using pot_new
                quantity = contractacio.pot_new
            elif concept.code == 'obra':
                quantity = wizard.obra
            elif concept.code == 'desp':
                product_id = wizard.desp.id
                quantity = 1
            else:
                #The rest, quantity = 1
                quantity = 1
            price = contractacio.get_price(concept.code, product_id=product_id)
            vals = {'product_id': price[contractacio_id]['product_id'],
                    'contractacio_id': contractacio_id,
                    'quantity': quantity,
                    'price_unit': price[contractacio_id]['price'],
                    'amount_untaxed': price[contractacio_id]['price']\
                                         * quantity,
                    'type_id': concept.type_id.id,
                    }
            contractacio_line_obj.create(cursor, uid, vals)

        return {}

ContractacioDistriCompute()
