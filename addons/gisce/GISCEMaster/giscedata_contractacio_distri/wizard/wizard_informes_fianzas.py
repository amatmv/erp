# -*- coding: utf-8 -*-
from osv import fields, osv
from datetime import date
from tools.translate import _
from json import dumps, loads
from giscemisc_informes.giscemisc_informes import Informe


TYPE_INFORMES = [('complet', 'Complet')]


class WizardInformesFianzas(osv.osv_memory):

    _name = 'wizard.informes.fianzas'

    def _default_start_date(self, cursor, uid, context=None):
        first_day_of_current_year = date(date.today().year, 1, 1)
        return first_day_of_current_year.strftime("%Y-%m-%d")

    def _default_end_date(self, cursor, uid, context=None):
        last_day_of_current_year = date(date.today().year, 12, 31)
        return last_day_of_current_year.strftime("%Y-%m-%d")

    def _default_info(self, cursor, uid, context=None):
        info = _(u'Asistent per generar informes relacionats '
                 u'amb les fiances i arrendametns:\n{}'
                 )
        informes_type = ''

        for i in TYPE_INFORMES:
            informes_type = '{}\n- {}'.format(informes_type, i[1])

        return info.format(informes_type)

    def _get_fianzas(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        conf_obj = self.pool.get('res.config')
        account_move_line_obj = self.pool.get('account.move.line')

        period = self.read(
            cursor, uid, ids, ['start_date', 'end_date'], context=context
        )[0]

        start_date = period['start_date']
        end_date = period['end_date']

        product_fianza = int(conf_obj.get(cursor, uid, 'deposit_product_id', 0))

        if not product_fianza:
            raise osv.except_osv(
                'ERROR',
                _(u'Cal configurar la variable de configuració '
                  u'deposit_product_id amb el producte de fiances')
            )

        lin_fianzas_ids = account_move_line_obj.get_fianzas_by_period(
            cursor, uid, start_date, end_date, product_fianza, context=context
        )

        if not lin_fianzas_ids:
            raise osv.except_osv(
                'ERROR',
                _(
                    u'No s\'han trobat linies de fiances per el periode '
                    u'{} - {}'
                ).format(start_date, end_date)
            )

        return lin_fianzas_ids

    def get_type_informe_info(self, cursor, uid, ids, context=None):

        informe_to_generate = self.read(
            cursor, uid, ids, ['recopiled_info', 'informe_type'],
            context=context
        )[0]

        fianzas_ids = loads(informe_to_generate['recopiled_info'])['fianzas_ids']
        n_rows = len(fianzas_ids)

        info = _(u'S\'han trobat {} linies per el periode contable seleccionat').format(n_rows)

        if informe_to_generate['informe_type'] == 'complet':
            specific_info = _(u'Es generara un informe '
                              u'amb tots els camps de les fiances'
                              )
            info = u'{}\n{}'.format(specific_info, info)

        return info

    def get_wizard_json_datas(self, cursor, uid, ids, context=None):
        """
        Metodo para recopilar la información necesaria para los informes,
        facimente extendible para escribir mas datos en el json

        :return: diccionario con los datos que se escribiran en el json en
        la funcion write_wizard_json_datas
        """
        mv_line_obj = self.pool.get('account.move.line')
        pol_obj = self.pool.get('giscedata.polissa')
        conf_obj = self.pool.get('res.config')

        product_fianza = int(conf_obj.get(cursor, uid, 'deposit_product_id', 0))

        if context is None:
            context = {}

        fianzas_ids = self._get_fianzas(cursor, uid, ids, context=context)

        polissa_ids = mv_line_obj.get_related_contracts_from_move_lines(
            cursor, uid, fianzas_ids, context=context
        )

        data_final_periode = self.read(
            cursor, uid, ids, ['end_date'], context=context
        )[0]['end_date']

        deposits = mv_line_obj.get_acomulat_positiu_negatiu_from_contract_and_product_to_date(
            cursor, uid, product_fianza, polissa_ids, data_final_periode,
            context=context
        )

        deposits_with_contract_name_as_key = {
            item['ref']: item for item in deposits
        }

        wizard_datas = {'fianzas_ids': fianzas_ids,
                        'diposits': deposits_with_contract_name_as_key}

        return wizard_datas

    def write_wizard_json_datas(self, cursor, uid, ids, context=None):
        """
        Metodo para escribir la información recopilada por la funcion
        get_wizard_json_datas en el campo json 'recopiled_info' del asistente

        """
        if context is None:
            context = {}

        datas = self.get_wizard_json_datas(cursor, uid, ids, context=context)

        wizard_write_dict = {
            'recopiled_info': dumps(datas),

        }

        self.write(cursor, uid, ids, wizard_write_dict, context=context)

        info_format_type = self.get_type_informe_info(
            cursor, uid, ids, context=context
        )

        wizard_write_dict = {
            'state': 'rec',
            'info': info_format_type
        }

        self.write(cursor, uid, ids, wizard_write_dict, context=context)

    def generate_informe(self, cursor, uid, ids, context=None):
        informe_to_generate = self.read(
            cursor, uid, ids, ['recopiled_info', 'informe_type'],
            context=context
        )[0]

        fianzas_ids = loads(informe_to_generate['recopiled_info'])['fianzas_ids']
        informe = informe_to_generate['informe_type']

        if informe == 'complet':
            return True

        return False

    _columns = {
        'start_date': fields.date(
            'Data inici',
            required=True, help=u'Data inicial per acotar la cerca'
        ),
        'end_date': fields.date(
            'Data final', required=True, help=u'Data final per acotar la cerca'
        ),
        'recopiled_info': fields.json('Datos de uso para el asistente'),
        'info': fields.text('Informació', readonly=True),
        'state': fields.selection(
            [('init', 'Inici'), ('rec', 'Recopilació'), ('end', 'Fi')], 'Estat'
        ),
        'informe_type': fields.selection(TYPE_INFORMES, 'Informe a generar'),
        'generated_informe': fields.binary('Informe generat'),
        'informe_name': fields.char('Nom informe', size=20)

    }

    _defaults = {
        'start_date': _default_start_date,
        'end_date': _default_end_date,
        'state': lambda *a: 'init',
        'info': _default_info

    }


WizardInformesFianzas()
