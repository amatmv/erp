# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) Joan M. Grande <jgrande@el-gas.es>
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from tools.translate import _
from datetime import date, datetime
from addons import get_module_resource
from libfacturacioatr.tarifes import CONTRACT_TYPE_POWER_FACTOR, is_temporada_alta


class ContractacioDistri(osv.osv):

    _name = 'contractacio.distri'

    def get_historic_fiance(self, cursor, uid=None, ids=None):
        today = datetime.today()

        sql_path = get_module_resource(
            'giscedata_contractacio_distri', 'sql/historic_fiance.sql'
        )

        with open(sql_path, 'rw') as sqlfile:
            sql = sqlfile.read()

        date_start = '{0}-01-01'.format(today.year - 1)
        date_end = '{0}-12-31'.format(today.year - 1)

        cursor.execute(sql, (date_start, date_end, date_start, date_end))
        res = cursor.fetchall()
        return float(res[0][0]) or 25.0

    def check_prefianza(self, cursor, uid, ids, context=None):
        '''Do som checks before compute fianza'''

        for contractacio in self.browse(cursor, uid, ids):
            #Contract date is used for computing
            if not contractacio.date:
                raise osv.except_osv(_(u'Error'),
                            _(u'Contract date missing'))
            #Tarifa and pricelist must be present
            if not contractacio.polissa_id.tarifa:
                raise osv.except_osv(_(u'Error'),
                            _(u'Access fare is not present'))
            if not contractacio.polissa_id.llista_preu:
                raise osv.except_osv(_(u'Error'),
                            _(u'Pricelist is not present'))

        return True

    def compute_fianza(self, cursor, uid, ids, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        product_obj = self.pool.get('product.product')
        conf_o = self.pool.get("res.config")
        obras_like_eventuales = int(conf_o.get(cursor, uid, "fianza_obras_like_eventuales", "1"))

        if not context:
            context = {}

        #Do some checks
        self.check_prefianza(cursor, uid, ids, context)

        #Fianza product is the one with code FIANZA
        search_params = [('default_code', '=', 'FIANZA')]
        product_id = product_obj.search(cursor, uid, search_params)[0]

        compute = {
            '2.0A': ['P1'],
            '2.0DHA': ['P1', 'P2'],
            '2.0DHS': ['P1', 'P2', 'P3'],
            '2.1A': ['P1'],
            '2.1DHA': ['P1', 'P2'],
            '2.1DHS': ['P1', 'P2', 'P3'],
            '3.0A': ['P2'],
            '3.1A': ['P2'],
            '3.1A LB': ['P2'],
            '6.1': ['P3'],
            '6.1A': ['P3'],
            '6.1B': ['P3'],
            '6.2': ['P3']
        }

        res = 0
        result = {}
        for contractacio in self.browse(cursor, uid, ids):
            context['date'] = contractacio.date
            polissa = contractacio.polissa_id
            contract_type = polissa.contract_type  # TABLA_9
            if contract_type == '07' and obras_like_eventuales:
                contract_type = '02'
            # Factor multiplicador segun tipo de contrato eventual, socorro...
            x_factor = CONTRACT_TYPE_POWER_FACTOR.get(contract_type, [1.0, 1.0])
            if polissa.data_firma_contracte:
                check_date = datetime.strptime(polissa.data_firma_contracte[:10], "%Y-%m-%d")
            elif polissa.data_alta:
                check_date = datetime.strptime(polissa.data_alta, "%Y-%m-%d")
            else:
                check_date = datetime.today()
            if is_temporada_alta(check_date):
                x_factor = x_factor[0]
            else:
                x_factor = x_factor[1]

            tarifa = polissa.llista_preu
            # compute potencia
            search_params = [('tarifa', '=', polissa.tarifa.id),
                         ('name', 'in', compute[polissa.tarifa.name]),
                         ('tipus', '=', 'tp')]
            periodes_pot_ids = periode_obj.search(
                cursor, uid, search_params
            )
            for period in periode_obj.browse(cursor, uid, periodes_pot_ids):
                pot_price = tarifa.price_get(
                    period.product_id.id, 1, polissa.titular.id,
                    context=context
                )[tarifa.id]
                res += (pot_price / 12) * contractacio.pot_new * x_factor
            # Compute 50 kWh
            search_params = [('tarifa', '=', polissa.tarifa.id),
                         ('name', 'in', compute[polissa.tarifa.name]),
                         ('tipus', '=', 'te')]
            periodes_ener_ids = periode_obj.search(
                cursor, uid, search_params
            )
            for period in periode_obj.browse(cursor, uid, periodes_ener_ids):
                ene_price = tarifa.price_get(
                    period.product_id.id, 1, polissa.titular.id,
                    context=context
                )[tarifa.id]
                if polissa.tarifa.tipus == 'BT':
                    # Distribute the 50 kw equally for each period
                    if contract_type in ('02', '03', '09'):
                        kw = (30 * 8) / len(periodes_ener_ids)
                    else:
                        kw = 50 / len(periodes_ener_ids)
                else:
                    kw = (31 * 24 * 0.4) / len(periodes_ener_ids)
                res += contractacio.pot_new * kw * ene_price
            result[contractacio.id] = {'price': res, 'product_id': product_id}

        return result

    def compute_fianza_historic(self, cursor, uid, ids, context=None):
        '''
        Computes "Fianza" for old retailers by historic info
        '''
        product_obj = self.pool.get('product.product')
        conf_obj = self.pool.get('res.config')

        try:
            calc_fiance = float(
                conf_obj.get(cursor, uid, 'fiance_historic_value', 0.0)
            )
        except Exception as e:
            calc_fiance = 0.0

        if calc_fiance:
            price = calc_fiance
        else:
            price = self.get_historic_fiance(cursor)
        # Fianza historic product is the one with code FIANZA_HISTORICO
        search_params = [('default_code', '=', 'FIANZA_HISTORICO')]
        product_id = product_obj.search(cursor, uid, search_params)[0]

        return dict([(id, {'price': price, 'product_id': product_id})
                     for id in ids]
                    )

    def get_price(self, cursor, uid, ids, concept,
                                product_id=None, context=None):
        '''returns dict with price amb product_id'''

        if not context:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        product_obj = self.pool.get('product.product')
        pricelist_obj = self.pool.get('product.pricelist')

        #The pricelist name to use is 'TARIFAS CONTRATACION'
        search_params = [('name', '=', 'TARIFAS CONTRATACION')]
        pricelist_ids = pricelist_obj.search(cursor, uid, search_params)
        if pricelist_ids:
            pricelist = pricelist_obj.browse(cursor, uid, pricelist_ids[0])
        else:
            raise osv.except_osv(_('Error'),
                                 _(u"Cannot find TARIFAS CONTRATACION "
                                   u"price list"))

        _map_concept = {'extensio': ['DEBT', 'DEAT0', 'DEAT1', 'DEAT2'],
                      'acces': ['DABT', 'DAAT0', 'DAAT1', 'DAAT2'],
                      'enganche': ['DENBT', 'DENAT0', 'DENAT1', 'DENAT2'],
                      'verificacio': ['DVBT', 'DVAT0', 'DVAT1', 'DVAT2'],
                      'actuacio': ['DAMICBT', 'DAMICAT0',
                                   'DAMICAT1', 'DAMICAT2'],
                      'vequip': ['DVCT', 'DVCM'],
                      'vicp': ['DVICP'],
                      'complocal': ['COMPL'],
                      'obra': ['MO'],
                      'desp': product_id,
                      'sup_inst_ced_ct': ['DSBT', 'DSCT', 'DSAT0', 'DSAT1'],
                      'sup_inst_ced_lat': ['DSBT', 'DSLAT', 'DSAT0', 'DSAT1']
                     }

        res = {}
        for contractacio in self.browse(cursor, uid, ids, context):
            #Update context to call price_get
            context.update({'date': contractacio.date})
            if concept in ('extensio', 'acces', 'sup_inst_ced_lat',
                           'sup_inst_ced_ct', 'enganche', 'verificacio',
                           'actuacio'):
                if (contractacio.polissa_id.tarifa.tipus == 'BT'):
                    search_params = [('default_code', '=',
                                      _map_concept[concept][0])]
                else:
                    tensio = contractacio.polissa_id.tensio
                    if (tensio >= 0) and (tensio <= 36000):
                        search_params = [('default_code', '=',
                                          _map_concept[concept][1])]
                    elif (tensio > 36000) and (tensio <= 72500):
                        search_params = [('default_code', '=',
                                          _map_concept[concept][2])]
                    else:
                        search_params = [('default_code', '=',
                                          _map_concept[concept][3])]
            elif concept == 'vequip':
                tensio = contractacio.polissa_id.tensio_normalitzada.name
                if tensio.startswith('3x'):
                    #tensio trifasica
                    search_params = [('default_code', '=',
                                      _map_concept[concept][0])]
                else:
                    #tensio monofasica
                    search_params = [('default_code', '=',
                                      _map_concept[concept][1])]
            elif concept in ('vicp', 'complocal', 'obra'):
                search_params = [('default_code', '=',
                                  _map_concept[concept][0])]
            if concept == 'fianza':
                #fianza has a special computation
                fianza_res = contractacio.compute_fianza()[contractacio.id]
                res[contractacio.id] = fianza_res
            elif concept == 'fianza_his':
                #fianza has a special computation
                fianza_res = contractacio.compute_fianza_historic()[contractacio.id]
                res[contractacio.id] = fianza_res
            elif concept == 'desp':
                #desp takes directly product_id
                price = pricelist.price_get(product_id, 1,
                                        context=context)[pricelist.id]
                res[contractacio.id] = {'price': price,
                                        'product_id': product_id}
            else:
                product_id = product_obj.search(cursor, uid,
                                                    search_params,
                                                    order='id desc',
                                                    limit=1)[0]
                price = pricelist.price_get(product_id, 1,
                                        context=context)[pricelist.id]
                res[contractacio.id] = {'price': price,
                                        'product_id': product_id}
        return res

    def get_types(self, cursor, uid, contractacio, context=None):
        '''distinct type of lines in this contractacio'''

        res = {}
        for line in contractacio.line_ids:
            res[line.type_id.id] = line.type_id.journal_id.id
        return res

    def get_product_taxes(self, cursor, uid, product_id, context=None):
        '''dict with taxes of a product'''

        product_obj = self.pool.get('product.product')
        if isinstance(product_id, (list, tuple)):
            product_id = product_id[0]
        res = {}
        for tax in product_obj.browse(cursor, uid, product_id).taxes_id:
            res[tax.id] = True
        return res.keys()

    def check_prefacturar(self, cursor, uid, ids, context=None):
        '''some checks before invoicing'''

        for contractacio in self.browse(cursor, uid, ids, context):
            #Check if we already have invoices
            if contractacio.factures_ids:
                raise osv.except_osv(_(u'Error'),
                                     _(u"You already have invoices done"))
            #Check if we have pagador and its address
            if contractacio.factura_comer:
                if not contractacio.comer_id\
                    or not contractacio.comer_address_id:
                    raise osv.except_osv(_(u'Error'),
                                         _(u'Comer invoice data missing'))
            else:
                if not contractacio.partner_id\
                    or not contractacio.invoice_address_id:
                    raise osv.except_osv(_(u'Error'),
                                         _(u'Invoice data missing'))

        return True

    def action_facturar_giscedata(self, cursor, uid, ids, context=None):
        '''create invoices'''

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura_linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        invoice_line_obj = self.pool.get('account.invoice.line')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        date_invoice = time.strftime('%Y-%m-%d')
        date_boe = facturador_obj.get_data_boe(cursor, uid)

        #Do some checks before
        self.check_prefacturar(cursor, uid, ids, context)

        for contractacio in self.browse(cursor, uid, ids):
            type_ids = self.get_types(cursor, uid, contractacio)
            factures_ids = {}
            fiscal_partner = contractacio.partner_id
            polissa = contractacio.polissa_id
            # If factura_comer partner_id and invoice_address_id change
            if contractacio.factura_comer:
                fiscal_partner = contractacio.comer_id
            for type in type_ids:
                #For each type, an invoice is created
                vals = factura_obj.onchange_polissa(
                    cursor, uid, [],
                    contractacio.polissa_id.id, 'out_invoice',
                    context=context
                )['value']

                payment_term_id = vals.get('payment_term', False)
                date_due = factura_obj.onchange_payment_term_date_invoice(
                    cursor, uid, [], payment_term_id, date_invoice
                )
                date_due = date_due.get('value', {})
                date_due = date_due.get('date_due', date_invoice)

                vals.update({
                    'date_invoice': date_invoice,
                    'date_due': date_due,
                    'polissa_id': polissa.id,
                    'type': 'out_invoice',
                    'journal_id': type_ids[type],
                    'partner_id': fiscal_partner.id,
                    'address_invoice_id': contractacio.invoice_address_id.id,
                    'account_id': fiscal_partner.property_account_receivable.id,
                    'tarifa_acces_id': polissa.tarifa.id,
                    'llista_preu': polissa.llista_preu.id,
                    'date_boe': date_boe,
                    'facturacio': 1,
                    'cups_id': polissa.cups.id,
                    'potencia': polissa.potencia,
                    'tipo_factura': '04',
                })
                #If factura_comer partner_id and invoice_address_id change
                if contractacio.factura_comer:
                    vals.update({
                        'address_invoice_id': contractacio.comer_address_id.id,
                        'account_id': fiscal_partner.property_account_receivable.id,
                        'fiscal_position': fiscal_partner.property_account_position.id,
                    })
                factures_ids[type] = factura_obj.create(cursor, uid,
                                                        vals, context)
            #Una volta generades les factures, anem creant les linies
            for line in contractacio.line_ids:
                line_vals = {
                    'factura_id': factures_ids[line.type_id.id],
                    'product_id': line.product_id.id,
                    'quantity': line.quantity,
                    'tipus': 'altres',
                    'multi': 1,
                }
                changes = invoice_line_obj.product_id_change(
                    cursor, uid, [],
                    line.product_id.id,
                    line.product_id.uom_id.id,
                    line.quantity,
                    partner_id=fiscal_partner.id,
                    fposition_id=fiscal_partner.property_account_position.id
                )['value']
                line_vals.update(changes)
                #Update the name
                line_vals['name'] = line.product_id.product_tmpl_id.name
                line_vals['price_unit_multi'] = line.price_unit
                tax_ids = self.get_product_taxes(cursor, uid,
                                                 line.product_id.id)
                line_vals['invoice_line_tax_id'] = [(6, 0, tax_ids)]
                factura_linia_obj.create(cursor, uid, line_vals, context)
        #After creating the lines, compute taxes in the invoice
        factura_obj.button_reset_taxes(cursor, uid, factures_ids.values())

        #Create the relation between the invoices and the contractacio
        contractacio.write({'factures_ids': [(6, 0, factures_ids.values())]})

        return contractacio.action_show_factures()

    def get_change_id(self, cursor, uid, ids, changes, context=None):
        '''return id of the changes in changes'''
        change_obj = self.pool.get('contractacio.distri.change')

        ids = []
        for change in changes:
            search_params = [('code', '=', change)]
            change_id = change_obj.search(cursor, uid, search_params)
            if change_id:
                ids.append(change_id[0])
        return ids

    def action_get_changes(self, cursor, uid, ids, context=None):
        '''tensio, titular i potencia changes
        between polissa_ant_ids and polissa_id'''

        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        contractacio = self.browse(cursor, uid, ids)
        polissa_obj = self.pool.get('giscedata.polissa')

        changes = []

        if not contractacio.polissa_ant_ids:
            changes.append('NUEVO')
        else:
            pol_act = contractacio.polissa_id
            for pol_ant in contractacio.polissa_ant_ids:
                if pol_act.titular != pol_ant.titular:
                    changes.append('TITULAR')
                if pol_act.potencia != pol_ant.potencia:
                    changes.append('POTENCIA')
                if pol_act.tensio_normalitzada != pol_ant.tensio_normalitzada:
                    changes.append('TENSION')
        #remove duplicates
        changes = list(set(changes))
        change_ids = contractacio.get_change_id(changes)
        contractacio.write({'change_ids': [(6, 0, change_ids)]})

    def action_show_factures(self, cursor, uid, ids, context=None):

        contractacio_id = ids[0]
        contractacio = self.browse(cursor, uid, contractacio_id)

        factures = [fact.id for fact in contractacio.factures_ids]

        vals = {
            'name': 'Factures creades',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }
        vals['domain'] = [('id', 'in', factures)]

        return vals

    def action_report(self, cursor, uid, ids, context=None):
        '''print report'''

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'report_informe_contractacio_distri',
        }

    def action_case(self, cursor, uid, ids, context=None):
        '''create case'''

        crm_case_obj = self.pool.get('crm.case')
        crm_section_obj = self.pool.get('crm.case.section')

        #OG section
        search_params = [('code', '=', 'OG')]
        section_id = crm_section_obj.search(cursor, uid, search_params)[0]

        for contractacio in self.browse(cursor, uid, ids):
            vals = {'name': contractacio.changes_str,
                    'description': contractacio.name,
                    'section_id': section_id,
                    'polissa_id': contractacio.polissa_id.id,
                   }
            if contractacio.instal_id:
                vals['partner_id'] = contractacio.instal_id.id
            if contractacio.instal_address_id:
                vals['partner_address_id'] = contractacio.instal_address_id.id
            case_id = crm_case_obj.create(cursor, uid, vals, context)

        vals = {
            'name': 'Ordre creada',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'crm.case',
            'view_id': False,
            'type': 'ir.actions.act_window',
        }
        vals['domain'] = [('id', '=', case_id)]

        return vals

    def _get_partner(self, cursor, uid, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = context.get('polissa_id', False)
        if polissa_id:
            return polissa_obj.browse(cursor, uid,
                                      polissa_id).titular.id

        return False

    def _get_comer(self, cursor, uid, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = context.get('polissa_id', False)
        if polissa_id:
            return polissa_obj.browse(cursor, uid,
                                      polissa_id).pagador.id

        return False

    def _get_comer_address(self, cursor, uid, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = context.get('polissa_id', False)
        if polissa_id:
            return polissa_obj.browse(cursor, uid,
                                      polissa_id).direccio_pagament.id

        return False

    def _get_polissa(self, cursor, uid, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = context.get('polissa_id', False)
        if polissa_id:
            return polissa_id

        return False

    def _get_default_pot(self, cursor, uid, context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa_id = context.get('polissa_id', False)
        if polissa_id:
            return polissa_obj.browse(cursor, uid,
                                      polissa_id).potencia

        return 0

    def _get_potencia(self, cursor, uid, polissa_id=None,
                      polissa_ant_ids=[], context=None):

        polissa_obj = self.pool.get('giscedata.polissa')
        if not context:
            context = {}
        if 'polissa_id' in context:
            polissa_id = context.get('polissa_id', False)
        if not polissa_id:
            res = {}
            return res

        pol_new = polissa_obj.browse(cursor, uid, polissa_id)
        polisses_ant = polissa_obj.browse(cursor, uid, polissa_ant_ids)
        pot_new = pol_new.potencia or 0
        pot_ant = 0
        for pol_ant in polisses_ant:
            pot_ant += pol_ant.potencia or 0
        res = {'pot_new': pot_new,
               'pot_ant': pot_ant,
               'pot_res': pot_new - pot_ant}

        return res

    def onchange_potencia(self, cursor, uid, ids, auto,
                          polissa_id, polissa_ant_ids,
                          pot_new, pot_ant,
                          context=None):

        if not context:
            context = {}
        res = {'value': {}, 'domain': {}, 'warning': {}}
        #if not auto computing of values,
        #only return pot_res
        if not auto:
            res['value'].update({'pot_res': pot_new - (pot_ant or 0)})
            return res

        res['value'].update(self._get_potencia(cursor, uid,
                                               polissa_id,
                                               polissa_ant_ids[0][2],
                                               context=context))
        return res

    def _get_polissa_ant_str(self, cursor, uid, ids, name, arg, context=None):
        '''string with the polisses anteriors'''

        res = {}
        for contractacio in self.browse(cursor, uid, ids):
            pol_str = ''
            for polissa in contractacio.polissa_ant_ids:
                pol_str += '%s, ' % (polissa.name)
            res[contractacio.id] = pol_str

        return res

    _columns = {
        'name': fields.char('Nom', size=64, required=True, readonly=False),
        'polissa_id': fields.many2one('giscedata.polissa',
                                      'Pòlissa', required=True),
        'polissa_ant_ids': fields.many2many('giscedata.polissa',
                                            'contractacio_polissa_ant_ref',
                                            'contractacio_id', 'polissa_id',
                                            'Pòlisses anteriors'),
        'polissa_ant_str': fields.function(_get_polissa_ant_str, method=True,
                                           type='char', size=200,
                                           string='Pòlisses anteriors'),
        'ref': fields.char('Ref', size=64,
                            required=False, readonly=False),
        'partner_id': fields.many2one('res.partner', 'Titular',
                                     required=False),
        'invoice_address_id': fields.many2one('res.partner.address',
                                              'Adreça factura',
                                              required=False),
        'date': fields.date('Data', help=u"Aquesta data s\'utilitzarà \
                                per fer els càlculs de preus", required=True),
        'pot_auto': fields.boolean('Auto',
                                help=u"Marqui aquesta opció per "
                                     u"calcular les potències  "
                                     u"automàticament segons la "
                                     u"pòlissa actual i les anteriors"),
        'pot_new': fields.float('Potència', digits=(16, 3)),
        'pot_ant': fields.float('Potència anterior', digits=(16, 3)),
        'pot_res': fields.float('Potència resultant', digits=(16, 3)),
        'observacions': fields.text('Observacions'),
        'factura_comer': fields.boolean('Facturar a comercialitzadora',
                                       required=False),
        'instal_id': fields.many2one('res.partner', 'Instal·lador',
                                          required=False,),
        'instal_address_id': fields.many2one('res.partner.address',
                                             'Contacte Inst.', required=False),
        'comer_id': fields.many2one('res.partner', 'Comercialitzadora',
                                          required=False,),
        'comer_address_id': fields.many2one('res.partner.address',
                                             'Contacte Comer', required=False),

    }

    _defaults = {
        'pot_auto': lambda *a: True,
        'pot_new': _get_default_pot,
        'pot_ant': lambda *a: 0,
        'factura_comer': lambda *a: False,
        'partner_id': _get_partner,
        'polissa_id': _get_polissa,
        'comer_id': _get_comer,
        'comer_address_id': _get_comer_address,

    }
ContractacioDistri()


class ContractacioDistriConceptType(osv.osv):

    _name = 'contractacio.distri.concept.type'

    _columns = {
        'name': fields.char('Name', size=64, required=True, readonly=False),
        'journal_id': fields.many2one('account.journal',
                                      'Invoice journal', required=True),

    }

ContractacioDistriConceptType()


class ContractacioDistriConcept(osv.osv):
    '''Different concepts for invoicing'''

    _name = 'contractacio.distri.concept'

    _columns = {
        'name': fields.char('Name', size=64, required=True, readonly=False),
        'code': fields.char('Code', size=20, required=True, readonly=False),
        'type_id': fields.many2one('contractacio.distri.concept.type',
                                   'Type', required=True),

    }

ContractacioDistriConcept()


class ContractacioDistriChange(osv.osv):
    '''Different changes that can take place'''

    _name = 'contractacio.distri.change'

    _columns = {
        'name': fields.char('Name', size=64, required=True, readonly=False),
        'code': fields.char('Code', size=20, required=True, readonly=False),
    }

ContractacioDistriChange()


class ContractacioDistriLine(osv.osv):

    _name = 'contractacio.distri.line'

    def on_change_product(self, cursor, uid, ids, product_id, quantity,
                          data, context=None):
        """Compute total if product is changed"""
        if not context:
            context = {}
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not product_id:
            return res
        if not data:
            raise osv.except_osv(_(u'Atenció'),
                        _(u'Esculli una data per la nova contractació'))
        prod_obj = self.pool.get('product.product')
        ir_md = self.pool.get('ir.model.data')
        ir_md_id = ir_md._get_id(cursor, uid, 'giscedata_contractacio_distri',
                                              'pricelist_tarifas_contratacion')
        pl_id = ir_md.read(cursor, uid, ir_md_id, ['res_id'])['res_id']

        price_obj = self.pool.get('product.pricelist')
        price_unit = price_obj.price_get(cursor, uid, [pl_id], product_id,
                                       quantity, context={'date': data})[pl_id]
        amount_untaxed = quantity * price_unit
        res['value'].update({
                'price_unit': price_unit,
                'amount_untaxed': amount_untaxed})
        amount_tax = 0
        prod = prod_obj.browse(cursor, uid, product_id, context)
        for tax in prod.taxes_id:
            amount_tax = amount_tax + round(amount_untaxed * tax.amount, 2)
        res['value'].update({
                'amount_tax': amount_tax,
                'amount_total': amount_untaxed + amount_tax})
        return res

    def on_change_price(self, cursor, uid, ids, quantity,
                        price_unit, context=None):
        """Compute total if quantity or price_unit changed"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if quantity and price_unit:
            res['value'].update({'amount_untaxed': quantity * price_unit})
        return res

    def _get_tax_line(self, cursor, uid, ids, name, arg, context=None):
        tax_obj = self.pool.get('account.tax')
        res = {}
        for line in self.browse(cursor, uid, ids, context):
            res[line.id] = {'amount_tax': 0,
                            'amount_total': 0}
            for tax in line.product_id.taxes_id:
                res[line.id]['amount_tax'] = res[line.id]['amount_tax'] + \
                                    round(line.amount_untaxed * tax.amount, 2)
            res[line.id]['amount_total'] = line.amount_untaxed + \
                                            res[line.id]['amount_tax']

        return res

    _columns = {
        'auto': fields.boolean('Auto', required=False,
                              help=u'Uncheck this box '
                                   u'if you are entering a manual concept',),
        'product_id': fields.many2one('product.product',
                                      'Product', required=False),
        'contractacio_id': fields.many2one('contractacio.distri',
                                          'Contractacio', required=True,
                                          ondelete='cascade'),
        'quantity': fields.float('Quantity', digits=(16, 3)),
        'price_unit': fields.float('Price', digits=(16, 6)),
        'amount_untaxed': fields.float('Subtotal', digits=(16, 2)),
        'type_id': fields.many2one('contractacio.distri.concept.type',
                                    'Type', required=True),
        'amount_tax': fields.function(_get_tax_line, method=True, type='float',
                                  digits=(16, 2), string='Tax',
                                  store={'contractacio.distri.line':\
                                (lambda self, cursor, uid, ids, c=None: ids,
                                ['quantity', 'price_unit', 'total'], 20),
                                         },
                                  multi='taxes'),
        'amount_total': fields.function(_get_tax_line, method=True,
                                        type='float',
                                       digits=(16, 2), string='Total',
                                       store={'contractacio.distri.line':\
                                (lambda self, cursor, uid, ids, c=None: ids,
                                ['quantity', 'price_unit', 'total'], 20),
                                         },
                                   multi='taxes'),

    }

    _defaults = {
        'auto': lambda *a: True,
        'quantity': lambda *a: 1,
        'price_unit': lambda *a: 0,

    }
ContractacioDistriLine()


class ContractacioDistri2(osv.osv):

    _name = 'contractacio.distri'
    _inherit = 'contractacio.distri'

    def _get_changes_str(self, cursor, uid, ids, name, arg, context=None):
        '''string with the changes'''

        res = {}
        for contractacio in self.browse(cursor, uid, ids):
            changes_str = ''
            for change in contractacio.change_ids:
                changes_str += '%s, ' % (change.name.upper())
            res[contractacio.id] = changes_str

        return res

    _columns = {
        'line_ids': fields.one2many('contractacio.distri.line',
                                   'contractacio_id',
                                   'Lines', required=False),
        'concept_ids': fields.many2many('contractacio.distri.concept',
                                       'contractacio_distri_concept_rel',
                                       'contractacio_id',
                                       'concept_id', 'Concepts'),
        'change_ids': fields.many2many('contractacio.distri.change',
                                       'contractacio_distri_change_rel',
                                       'contractacio_id',
                                       'change_id', 'Changes'),
        'factures_ids': fields.many2many('giscedata.facturacio.factura',
                                         'contractacio_distri_factura_rel',
                                         'contractacio_id',
                                         'factura_id', 'Invoices',
                                         readonly=True),
        'changes_str': fields.function(_get_changes_str, method=True,
                                       type='char', size=200,
                                       string='Changes'),

    }

ContractacioDistri2()
