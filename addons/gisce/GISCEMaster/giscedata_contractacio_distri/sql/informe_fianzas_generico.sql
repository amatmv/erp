SELECT
    fianzas.ref AS "Número de contrato",
    '' AS "Tipo de registro", -- Tipo de registro A, M, B


    CASE WHEN polissa.state = 'baixa' THEN TO_CHAR(polissa.data_baixa, 'DD/MM/YYYY')
    WHEN modcon.tipus = 'alta' THEN TO_CHAR(polissa.data_alta, 'DD/MM/YYYY')
    ELSE TO_CHAR(fianzas.date, 'DD/MM/YYYY') END AS "Fecha de movimiento", -- Fecha del movimiento A-> fecha_alta_contrato, B-> Fecha_baja_contrato, M-> fecha_modificacion
    fianzas.date AS "Fecha fianza",

    -- + Información del inmueble
    CASE WHEN (cups.ref_catastral is null or cups.ref_catastral = '') THEN 'S' ELSE 'N' END
    AS "Indicador Referencia Catastral propia", -- S o N depende de si hay referencia catastral


    CASE WHEN (cups.ref_catastral is null or cups.ref_catastral = '') THEN 'N'
    -- WHEN asdfasfasf THEN 'R'
    ELSE 'U' END
    AS "Tipo de referencia catastral", --(U)rbano, (R)ustico o (N)o ref catastro


    cups.ref_catastral AS "Referencia catastral", -- En la base de datos a veces es nulo aveces es un string vacio


    CASE WHEN (cups.ref_catastral is null or cups.ref_catastral = '') THEN 100
    ELSE NULL END
    AS "participación en la superficie del inmueble principal", -- Solamente cuando Indicador es N

    CASE WHEN (cups.ref_catastral is null or cups.ref_catastral = '') THEN 1
    ELSE NULL END
    AS "Número de orden", -- Solamente cuando Indicador es N


    tipovia.codi AS "Tipo de vía",

    cups.nv AS "Nombre de la vía pública",

    cups.direccio AS "Calle completa suministro",


    CASE WHEN (cups.pnp IS NULL OR cups.pnp = '' OR cups.pnp = 'S/N') THEN 'SN'
    -- WHEN asdfasfasf THEN 'KM'
    ELSE 'NUM' END
    AS "Tipo de numeración", -- NUM si es un numero KM si es kilometro SN si no se especifica

    CASE WHEN (cups.pnp IS NULL OR cups.pnp = '' OR cups.pnp = 'S/N') THEN NULL
    ELSE cups.pnp END AS "Número/Kilómetro", -- Numero

    -- Opcionales
    '' AS "Calificador de numeración",
    '' AS "Bloque",
    '' AS "Letra del portal",
    '' AS "Escalera",
    '' AS "Piso",
    '' AS "Puerta",
    '' AS "Local",

    provincia.code AS "Provincia", -- Ine 2 digitos
    RIGHT(municipi.ine, 3) AS "Municipio", -- Ine 3 digitos
    municipi.name AS "Municipio Nombre",
    CONCAT(municipi.ine, municipi.dc) AS "Localidad", --110125 ine 6 digitos
    cups.dp AS "Código postal",

    -- Arrendatari
    REPLACE(titular.vat, 'ES', '') AS "NIF del Arrendatario",
    titular.vat AS "NIF Completo",
    titular.name AS "Apellidos, Nombre o Razón Social del Arrendatario",

    -- Opcionals
    '' AS "Tipo de vía Arrendatario",
    '' AS "Nombre de la vía pública Arrendatario",
    '' AS "Tipo de numeración Arrendatario",
    '' AS "Número/Kilómetro Arrendatario",
    '' AS "Calificador de numeración Arrendatario",
    '' AS "Bloque Arrendatario",
    '' AS "Letra del portal Arrendatario",
    '' AS "Escalera Arrendatario",
    '' AS "Piso Arrendatario",
    '' AS "Puerta Arrendatario",
    '' AS "Pais Arrendatario",
    '' AS "Provincia Arrendatario",
    '' AS "Municipio Arrendatario",
    '' AS "Localidad Arrendatario",
    '' AS "Código postal Arrendatario",
    '' AS "Número de teléfono Arrendatario",
    '' AS "Dirección e-mail Arrendatario",


    -- Fianza
    (fianzas.debit - fianzas.credit) AS "Importe de la fianza",

    -- Arrendamiento suministro electrico
    tarifa.name AS "Tarifa",
    modcon.potencia AS "Potencia"

        FROM account_move_line AS fianzas -- Realmente son lineas, se filtran las fianzas en el asistente
        INNER JOIN giscedata_polissa AS polissa ON (fianzas.ref = polissa.name)
        INNER JOIN giscedata_polissa_modcontractual AS modcon ON
            (polissa.id = modcon.polissa_id
            and
            (
                (modcon.data_inici <= fianzas.date and modcon.data_final >= fianzas.date)
                or
                (polissa.state = 'baixa'
                 and fianzas.date > modcon.data_final
                 and polissa.modcontractual_activa = modcon.id
                 )
            )
            )
        INNER JOIN giscedata_cups_ps AS cups ON (modcon.cups = cups.id) -- mod or polissa?

        INNER JOIN res_tipovia AS tipovia ON (cups.tv = tipovia.id)

        INNER JOIN res_municipi AS municipi ON (cups.id_municipi = municipi.id)
        INNER JOIN res_country_state AS provincia ON (municipi.state = provincia.id)

        INNER JOIN res_partner AS titular ON (modcon.titular = titular.id)

        INNER JOIN giscedata_polissa_tarifa AS tarifa ON (modcon.tarifa = tarifa.id)

        WHERE fianzas.id in %(fianzas_ids)s and modcon.id is not null order by fianzas.ref