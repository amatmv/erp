SELECT (
-- Facturació diària
SELECT (SUM(i.amount_total*(CASE WHEN i.type like '%%_refund' THEN -1 ELSE 1 END))/365)
FROM account_invoice i
LEFT JOIN account_journal j on j.id=i.journal_id
WHERE j.code like 'ENERGIA%%' AND date_invoice between %s AND %s
) / (
-- Total CUPS facturats
SELECT COUNT(DISTINCT cups_id)
FROM giscedata_facturacio_factura f
LEFT JOIN account_invoice i ON i.id=f.invoice_id
WHERE i.date_invoice between %s AND %s
) * 30 AS fianza;