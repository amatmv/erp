# coding=utf-8
import logging

import pooler


TO_DISABLE = [
    ('giscedata_contractes_acces', 'categ_drets_escomesa'),
    ('giscedata_facturacio_conceptes', 'categ_drets_escomesa')
]


def migrate(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migrations')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    product_obj = pool.get('product.product')
    imd = pool.get('ir.model.data')
    for module, categ in TO_DISABLE:
        cursor.execute("""select count(*) from
                          ir_module_module
                          where state='uninstalled'
                          and name=%s""", [module])
        count = cursor.dictfetchone()
        if count:
            continue
        res = imd.get_object_reference(cursor, uid, module, categ)
        if res:
            category_id = res[1]
            products_ids = product_obj.search(cursor, uid, [
                ('categ_id', 'child_of', [category_id])
            ])
            logger.info("Disabling {0} from {1}-{2}".format(
                len(products_ids), module, categ)
            )
            product_obj.write(cursor, uid, products_ids, {'active': False})

up = migrate
