# -*- coding: utf-8 -*-
{
    "name": "Contractació distribució",
    "description": """
Contractacio de distribució:
    * Compute concepts
    * Invoicing journal per concept
    * Contract summary report
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Polissa",
    "depends":[
        "giscedata_polissa",
        "giscedata_facturacio_distri"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_facturacio_demo.xml",
        "giscedata_contractacio_distri_demo.xml"
    ],
    "update_xml":[
        "security/contractacio_distri_security.xml",
        "contractacio_distri_sequence.xml",
        "contractacio_distri_data.xml",
        "contractacio_distri_switching_data.xml",
        "contractacio_distri_report.xml",
        "wizard/wizard_generate_reconnection_invoice_view.xml",
        "wizard/contractacio_distri_compute_view.xml",
        "wizard/wizard_config_taxes_view.xml",
        "wizard/wizard_informes_fianzas_view.xml",
        "giscedata_polissa_view.xml",
        "contractacio_distri_view.xml",
        "security/ir.model.access.csv",
        "giscedata_facturacio_view.xml"
    ],
    "active": False,
    "installable": True
}
