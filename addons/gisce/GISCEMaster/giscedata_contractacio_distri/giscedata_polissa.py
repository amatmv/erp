# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    d$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from osv import osv
from osv import fields
import time
from tools.translate import _
from copy import copy


class ContractacioDistriPolissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    def copy(self, cursor, uid, id, default=None, context=None):
        """don't copy contractacions when copying a polissa"""

        default.update({'contractacio_id': []})
        res_id = super(ContractacioDistriPolissa, self).copy(cursor,
                                                             uid, id, default,
                                                             context)
        return res_id

    def get_product_reconnection(self, cursor, uid, context=None):
        """Returns product in reconnection category if exist only one product
        into them"""
        prod_obj = self.pool.get('product.product')
        imd_obj = self.pool.get('ir.model.data')

        prod_category_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_contractacio_distri',
            'categ_derechos_reconexion'
        )[1]

        recon_products_ids = prod_obj.search(
            cursor, uid, [('categ_id', '=', prod_category_id)], context=context
        )

        if not len(recon_products_ids):
            raise osv.except_osv(
                'Error',
                _(u'No existeix el producte reconnexió a la base de dades')
            )
        elif len(recon_products_ids) == 1:
            return recon_products_ids[0]
        else:
            raise osv.except_osv(
                'Error',
                _(u'No existeix un únic producte reconnexió a la base de dades')
            )
        return False

    def create_reconnection_invoice(self, cursor, uid, polissa_id, date_inv,
                                    quantitat=2, context=None):
        if context is None:
            context = {}
        contractacio_obj = self.pool.get('contractacio.distri')
        contractacio_line_obj = self.pool.get('contractacio.distri.line')
        contractacio_concept_obj = self.pool.get('contractacio.distri.concept')
        imd_obj = self.pool.get('ir.model.data')

        contractacio_values = {
            'polissa_id': polissa_id,
            'name': 'Reconnexió amb data ' + date_inv,
            'date': date_inv,
            'factura_comer': True,
        }
        ctx = context.copy()
        ctx.update({'polissa_id': polissa_id})
        contractacio_id = contractacio_obj.create(
            cursor, uid, contractacio_values, context=ctx)
        if contractacio_id:
            # Get contractació object by browse
            contractacio = contractacio_obj.browse(
                cursor, uid, contractacio_id, context=context
            )
            prod_id = self.get_product_reconnection(
                cursor, uid, context=context)
            if prod_id:
                type_c = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_contractacio_distri',
                    'contractacio_type_generic'
                )[1]
                if type_c:
                    concept_id = contractacio_concept_obj.search(
                        cursor, uid, [('code', '=', 'enganche')],
                        context=context)
                    if concept_id:
                        price = contractacio.get_price('enganche',
                                                       product_id=prod_id)

                        contractacio_line_values = {
                            'contractacio_id': contractacio_id,
                            'type_id': type_c,
                            'product_id': prod_id,
                            'quantity': quantitat,
                            'price_unit': price[contractacio_id]['price'],
                            'amount_untaxed':
                                price[contractacio_id]['price'] * quantitat

                        }
                        con_lin_id = contractacio_line_obj.create(
                            cursor, uid, contractacio_line_values,
                            context=context)
                        if con_lin_id:
                            contractacio.action_facturar_giscedata()
                            contractacio = contractacio_obj.browse(
                                cursor, uid, contractacio_id, context=context
                            )
                            len_fact_lis = len(contractacio.factures_ids)
                            if len_fact_lis:
                                return contractacio.factures_ids[len_fact_lis-1].id
        return False

    def _ff_fact_sense_drets(self, cursor, uid, ids, field_name, args,
                             context=None):

        """ Marquem un contracte com a sense drets:
                * La polissa no te cap contractació
        """
        con_obj = self.pool.get('contractacio.distri')
        res = dict.fromkeys(ids, False)

        contractacions_pol_ids = con_obj.search_reader(
            cursor, uid, [], ['polissa_id'],
            context=context
        )
        list_con_pol_ids = [con['polissa_id'] for con in contractacions_pol_ids]
        pol_no_contracts = set(ids) - set(list_con_pol_ids)

        res.update(dict((p_id, True) for p_id in pol_no_contracts))

        return res

    def _search_fact_sense_drets(self, cursor, uid, obj, name, args,
                                 context=None):
        """
        Funció de cerca de contractes sense facturació de drets
        """
        con_obj = self.pool.get('contractacio.distri')
        pol_obj = self.pool.get('giscedata.polissa')
        res_obj = self.pool.get('res.config')

        filter_date_contractacions = res_obj.get(
            cursor, uid, 'filter_date_contractacions', '2018-03-01'
        )

        filter_date = ('data_alta', '<=', filter_date_contractacions)

        contr_pol_ids = con_obj.search_reader(cursor, uid, [], ['polissa_id'], context=context)
        contra_pol_ids = [con['polissa_id'] for con in contr_pol_ids]

        pol_no_contractacions = pol_obj.search(
            cursor, uid, [('id', 'not in', contra_pol_ids), filter_date], context=context)

        # return Domain
        return [('id', 'in', pol_no_contractacions)]

    _columns = {
        'contractacio_ids': fields.one2many('contractacio.distri',
                                          'polissa_id', 'Contractacio',
                                          required=False),
        'facturacio_sense_drets': fields.function(_ff_fact_sense_drets,
                                                  method=True, type='boolean',
                                                  string='Facturació '
                                                         'sense drets',
                                                  fnct_search=_search_fact_sense_drets,
                                                  readonly=True),
    }

ContractacioDistriPolissa()
