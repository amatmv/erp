# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Revisions BT Multicompany",
    "description": """Revisions """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi companyy",
    "depends":[
        "giscedata_revisions_bt"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_revisions_bt_multicompany_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
