# -*- coding: utf-8 -*-
from osv import osv, fields
from tools import config


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def _get_polissa_from_invoice_with_proformas(self, cursor, uid, ids,
                                                 context=None):
        """Returns ids of polissa in invoice passed as ids"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')
        # Search factura associated to invoices in ids
        query = '''
            SELECT f.id
            FROM giscedata_facturacio_factura f
            INNER JOIN account_invoice i
            ON i.id = f.invoice_id
            WHERE i.id in %s
            AND i.type in ('out_invoice', 'out_refund')
            AND i.state in ('open', 'paid', 'proforma2', 'grouped_proforma')
            '''
        cursor.execute(query, (tuple(ids),))
        factura_ids = [x[0] for x in cursor.fetchall()]
        vals = factura_obj.read(cursor, uid, factura_ids, ['polissa_id'])
        return [val['polissa_id'][0] for val in vals if val['polissa_id']]

    def _ff_darrera_lectura_proforma(self, cursor, uid, ids, field_name, arg,
                                     context=None):
        '''Calcula la data de la darrera lectura facturada'''

        multipoint_obj = self.pool.get('giscedata.polissa.multipunt')

        # We set the default value to False
        res = dict.fromkeys(ids, False)

        in_proforma_multipoint = multipoint_obj.get_profroma_polisses(
            cursor, uid, context=context
        )
        if in_proforma_multipoint:
            # If there are no polisses assigned to a multipoint with agrupation
            # the result of super is already correct, so there's no point in
            # doing this
            query_file = (u"%s/giscedata_facturacio_comer_multipunt/sql/"
                          u"darrera_lectura_facturada.sql"
                          % config['addons_path'])
            query = open(query_file).read()

            cursor.execute(query, (tuple(in_proforma_multipoint), tuple(ids), ))
            # We set all the ones we have found to the correct value
            res.update(dict([(x[0], x[1]) for x in cursor.fetchall()]))
        return res

    def _ff_darrera_lectura_proforma_inv(self, cursor, uid, ids, name, value,
                                         fnct_inv_arg, context=None):
        return self._ff_darrera_lectura_inv(
            cursor, uid, ids, name, value, fnct_inv_arg, context
        )

    _store_darrera_lectura_proforma = {
        'account.invoice': (
            _get_polissa_from_invoice_with_proformas, ['state'], 20
        )
    }

    _columns = {
        'data_ultima_lectura': fields.function(
            _ff_darrera_lectura_proforma,
            fnct_inv=_ff_darrera_lectura_proforma_inv,
            method=True, type='date', string=u'Data última real facturada',
            store=_store_darrera_lectura_proforma
        ),
    }
GiscedataPolissa()
