# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _
import netsvc

class WizardPassarAgrupadaAProforma(osv.osv_memory):

    _name = 'wizard.passar.agrupada.a.proforma'

    def _filtrate_invoices(self, cursor, uid, invoice_ids, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        return [
            inv_vals['id']
            for inv_vals in fact_obj.read(cursor, uid, invoice_ids, ['state'])
            if inv_vals['state'] == 'grouped_proforma'
        ]

    def action_passar_a_agrupada(self, cursor, uid, id_wiz, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        if isinstance(id_wiz, (tuple, list)):
            id_wiz = id_wiz[0]

        invoice_ids = self._filtrate_invoices(
            cursor, uid, context.get('active_ids', []), context
        )

        info = _(u"Les següents factures es passaran a proforma: {0}").format(
            ', '.join(
                [
                    inv_vals['number']
                    for inv_vals in fact_obj.read(
                        cursor, uid, invoice_ids, ['number']
                    )
                ]
            )
        )

        return self.write(
            cursor, uid, [id_wiz], {
                'state': 'conf',
                'info': info,
            }
        )

    def action_confirmar(self, cursor, uid, id_wiz, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        wf_service = netsvc.LocalService("workflow")

        if isinstance(id_wiz, (tuple, list)):
            id_wiz = id_wiz[0]

        factura_ids = self._filtrate_invoices(
            cursor, uid, context.get('active_ids', []), context
        )

        base_ids = fact_obj._get_base_ids(cursor, uid, factura_ids, context)

        for inv_id in base_ids:
            wf_service.trg_validate(
                uid, 'account.invoice', inv_id, 'invoice_proforma2', cursor
            )

        return self.write(cursor, uid, [id_wiz], {'state': 'end'})

    _columns = {
        'state': fields.selection(
            [('init', 'Inici'), ('conf', 'Confirmacio'), ('end', 'Fi')],
            'Estat'
        ),
        'info': fields.text('Info', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init'
    }
WizardPassarAgrupadaAProforma()
