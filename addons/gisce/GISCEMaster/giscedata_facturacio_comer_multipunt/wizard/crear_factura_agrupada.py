# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

class WizardCrearFacturaAgrupada(osv.osv_memory):

    _name = 'wizard.crear.factura.agrupada'

    def get_missing_polisses_text(self, cursor, uid, multipunt_ids,
                                  invoice_all_pending, start_date, end_date,
                                  invoice_rect_types, context=None):
        if context is None:
            context = {}

        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')
        polissa_obj = self.pool.get('giscedata.polissa')

        missing_polisses_text = ''

        if invoice_all_pending:
            # If we want to invoice all we don't want to filter by start or end
            # date, so we set them to None
            start_date = None
            end_date = None

        for multipunt_id in multipunt_ids:
            fact_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multipunt_id, start_date, end_date,
                invoice_rect_types, context
            )

            multipunt_name = multipunt_obj.read(
                cursor, uid, multipunt_id, ['name']
            )['name']

            missing_polisses_ids = multipunt_obj.get_missing_polisses(
                cursor, uid, multipunt_id, fact_ids, context=context
            )

            if missing_polisses_ids:
                missing_polisses_vals = polissa_obj.read(
                    cursor, uid, missing_polisses_ids, ['name']
                )
                missing_polisses_names = ', '.join(
                    [
                        polissa_vals['name']
                        for polissa_vals in missing_polisses_vals
                    ]
                )

                missing_polisses_text += _(
                    'Pel multipunt {0} ens falten les '
                    'factures de les polisses [{1}]\n'
                ).format(multipunt_name, missing_polisses_names)
            else:
                missing_polisses_text += _(
                    'Pel multipunt {0} tenim factures per totes les polisses\n'
                ).format(multipunt_name)

        return missing_polisses_text

    def get_agrupated_invoices_text(self, cursor, uid, multipunt_ids,
                                    invoice_all_pending, start_date, end_date,
                                    invoice_rect_types, context=None):
        if context is None:
            context = {}

        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        agrupated_invoices_text = ''

        if invoice_all_pending:
            # If we want to invoice all we don't want to filter by start or end
            # date, so we set them to None
            start_date = None
            end_date = None

        for multipunt_id in multipunt_ids:
            fact_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multipunt_id, start_date, end_date,
                invoice_rect_types, context
            )

            multipunt_name = multipunt_obj.read(
                cursor, uid, multipunt_id, ['name']
            )['name']

            if fact_ids:
                agrupated_fact_vals = fact_obj.read(
                    cursor, uid, fact_ids, ['number']
                )
                agrupated_fact_names = ', '.join(
                    [
                        fact_vals['number'] for fact_vals in agrupated_fact_vals
                    ]
                )

                agrupated_invoices_text += _(
                    'Pel multipunt {0} agruparem les factures [{1}]\n'
                ).format(multipunt_name, agrupated_fact_names)
            else:
                agrupated_invoices_text += _(
                    'Pel multipunt {0} no tenim factures a agrupar en el '
                    'periode seleccionat\n'
                ).format(multipunt_name)

        return agrupated_invoices_text

    @staticmethod
    def get_inclueded_types(facturar_normals, facturar_rectificadores,
                            facturar_anuladores, facturar_substituidores):
        factura_rect_types = []
        if facturar_normals:
            factura_rect_types.append('N')
        if facturar_rectificadores:
            factura_rect_types.append('R')
        if facturar_anuladores:
            factura_rect_types.append('A')
        if facturar_substituidores:
            factura_rect_types.append('B')

        return factura_rect_types

    def create_agrupated_invoice(self, cursor, uid, id_wiz, context=None):
        if context is None:
            context = {}

        if isinstance(id_wiz, (tuple, list)):
            id_wiz = id_wiz[0]

        inv_obj = self.pool.get('account.invoice')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')

        wizard = self.browse(cursor, uid, id_wiz)

        start_date = None
        end_date = None
        if not wizard.invoice_all_pending:
            start_date = wizard.start_date
            end_date = wizard.end_date

        result = ''
        grouping_invoices = []

        invoice_rect_types = self.get_inclueded_types(
            wizard.facturar_normals, wizard.facturar_rectificadores,
            wizard.facturar_anuladores, wizard.facturar_substituidores
        )

        for multipunt_id in context.get('active_ids', []):
            fact_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multipunt_id, start_date, end_date,
                invoice_rect_types, context
            )

            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, fact_ids, multipunt_id, context
            )
            grouping_invoices.append(inv_id)

            multipunt_name = multipunt_obj.read(
                cursor, uid, multipunt_id, ['name']
            )['name']
            factures_vals = fact_obj.read(cursor, uid, fact_ids, ['number'])
            factures_names = ', '.join(
                [
                    fact_vals['number'] for fact_vals in factures_vals
                ]
            )

            result += _(
                'S\'ha creat una factura agrupada pel multipunt {0} '
                'a partir de les factures {1}'
            ).format(multipunt_name, factures_names)

        self.write(
            cursor, uid, [id_wiz], {
                'state': 'end',
                'result': result,
                'grouping_invoices': grouping_invoices
            }
        )

        return True

    def view_created_invoices(self, cursor, uid, id_wiz, context=None):
        if context is None:
            context = {}

        if isinstance(id_wiz, (tuple, list)):
            id_wiz = id_wiz[0]

        imd_obj = self.pool.get('ir.model.data')

        tree_view_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'invoice_tree'
        )
        form_view_id = imd_obj.get_object_reference(
            cursor, uid, 'account', 'invoice_form'
        )

        wizard = self.browse(cursor, uid, id_wiz)

        return {
            'domain': "[('id', 'in', {0})]".format(wizard.grouping_invoices),
            'name': _('Factures Agrupadores'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'account.invoice',
            'type': 'ir.actions.act_window',
            'view_id': (tree_view_id[1], tree_view_id[0]),
        }

    def onchange_update_invoices_to_group(
            self, cursor, uid, ids, invoice_all_pending, start_date, end_date,
            facturar_normals, facturar_rectificadores, facturar_anuladores,
            facturar_substituidores, context=None
    ):
        res = {'value': {}, 'domain': {}, 'warning': {}}

        res['value']['missing_polisses'] = self.get_missing_polisses_text(
            cursor, uid, context.get('active_ids', []),
            invoice_all_pending, start_date, end_date,
            self.get_inclueded_types(
                facturar_normals, facturar_rectificadores, facturar_anuladores,
                facturar_substituidores
            ),
            context
        )
        res['value']['agrupated_invoices'] = self.get_agrupated_invoices_text(
            cursor, uid, context.get('active_ids', []),
            invoice_all_pending, start_date, end_date,
            self.get_inclueded_types(
                facturar_normals, facturar_rectificadores, facturar_anuladores,
                facturar_substituidores
            ),
            context
        )

        return res

    def default_missing_polisses(self, cursor, uid, context=None):
        if context is None:
            context = {}

        return self.get_missing_polisses_text(
            cursor, uid, context.get('active_ids', []),
            self._defaults.get('invoice_all_pending', False),
            self._defaults.get('start_date', None),
            self._defaults.get('end_date', None),
            self.get_inclueded_types(
                self._defaults.get('facturar_normals', True),
                self._defaults.get('facturar_rectificadores', True),
                self._defaults.get('facturar_anuladores', True),
                self._defaults.get('facturar_substituidores', True)
            ),
            context
        )

    def default_agrupated_invoices(self, cursor, uid, context=None):
        if context is None:
            context = {}

        return self.get_agrupated_invoices_text(
            cursor, uid, context.get('active_ids', []),
            self._defaults.get('invoice_all_pending', False),
            self._defaults.get('start_date', None),
            self._defaults.get('end_date', None),
            self.get_inclueded_types(
                self._defaults.get('facturar_normals', True),
                self._defaults.get('facturar_rectificadores', True),
                self._defaults.get('facturar_anuladores', True),
                self._defaults.get('facturar_substituidores', True)
            ),
            context
        )

    _columns = {
        'state': fields.selection([('start', 'Inici'), ('end', 'Fi')], 'Estat'),
        'invoice_all_pending': fields.boolean('Facturar totes les pendents'),
        'start_date': fields.date('Data d\'inici'),
        'end_date': fields.date('Data de fi'),
        'missing_polisses': fields.text(
            'Polisses que no s\'inclouran', readonly=True
        ),
        'agrupated_invoices': fields.text(
            'Factues que s\'agruparan', readonly=True
        ),
        'result': fields.text('Resultat', readonly=True),
        'grouping_invoices': fields.json('Factures agrupadores'),
        'facturar_normals': fields.boolean('Facturar Normals'),
        'facturar_rectificadores': fields.boolean('Facturar Rectificadores'),
        'facturar_anuladores': fields.boolean('Facturar Anuladoras'),
        'facturar_substituidores': fields.boolean(
            'Facturar Anuladoras con Sustituyente'
        ),
    }

    _defaults = {
        'state': lambda *a: 'start',
        'missing_polisses': default_missing_polisses,
        'agrupated_invoices': default_agrupated_invoices,
        'facturar_normals': lambda *a: True,
        'facturar_rectificadores': lambda *a: True,
        'facturar_anuladores': lambda *a: True,
        'facturar_substituidores': lambda *a: True,
    }

WizardCrearFacturaAgrupada()
