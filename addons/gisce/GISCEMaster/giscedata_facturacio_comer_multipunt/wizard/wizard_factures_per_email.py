# -*- coding: utf-8 -*-

from osv import osv
from addons import get_module_resource


class WizardFacturesPerEmail(osv.osv_memory):
    _name = 'wizard.factures.per.email'
    _inherit = 'wizard.factures.per.email'

    def get_query_path(self, cursor, uid, query_file):
        conf_obj = self.pool.get('res.config')
        use_adr_contact = int(
            conf_obj.get(cursor, uid, 'fact_use_adr_contact_email', '0')
        )
        query_suffix = ''
        if use_adr_contact:
            query_suffix = '_contact'
        query_file = "{}{}.sql".format(query_file, query_suffix)
        res = get_module_resource(
            'giscedata_facturacio_comer_multipunt', 'sql', query_file)

        if res:
            return res

        return super(WizardFacturesPerEmail, self).get_query_path(
            cursor, uid, query_file
        )
WizardFacturesPerEmail()
