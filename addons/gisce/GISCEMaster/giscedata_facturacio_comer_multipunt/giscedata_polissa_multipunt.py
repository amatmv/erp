# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissaMultipunt(osv.osv):
    _name = 'giscedata.polissa.multipunt'
    _inherit = 'giscedata.polissa.multipunt'

    _columns = {
        'is_proforma': fields.boolean('Es Pro-forma'),
        'proforma_sequence': fields.many2one('ir.sequence', u'Seqüència'),
        'payment_type_id': fields.many2one('payment.type', 'Tipus pagament')
    }

    _defaults = {
        'is_proforma': lambda *a: False,
    }

    def get_profroma_polisses(self, cursor, uid, context=None):
        if not context:
            context = {}

        prof_mult_ids = self.search(
            cursor, uid, [('is_proforma', '=', True)]
        )
        prof_mult_vals = self.read(cursor, uid, prof_mult_ids, ['polisses'])

        proforma_poli = []
        for mult_vals in prof_mult_vals:
            proforma_poli += mult_vals['polisses']

        return proforma_poli

    def get_pending_proformas(self, cursor, uid, multipunt_id, start_date=None,
                              end_date=None, invoice_rect_types=None,
                              context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        polissa_ids = self.read(
            cursor, uid, multipunt_id, ['polisses']
        )['polisses']

        search_params = [
            ('polissa_id', 'in', polissa_ids),
            ('state', '=', 'proforma2'),
        ]
        if start_date:
            search_params.append(('date_invoice', '>=', start_date))
        if end_date:
            search_params.append(('date_invoice', '<=', end_date))
        if invoice_rect_types:
            search_params.append(
                ('tipo_rectificadora', 'in', invoice_rect_types)
            )

        return fact_obj.search(cursor, uid, search_params)

    def get_missing_polisses(self, cursor, uid, multipunt_id, prof_ids,
                             context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        multipunt_polissa_ids = self.read(
            cursor, uid, multipunt_id, ['polisses']
        )['polisses']

        proforma_polissa_ids = set()

        for prof_vals in fact_obj.read(cursor, uid, prof_ids, ['polissa_id']):
            proforma_polissa_ids.add(prof_vals['polissa_id'][0])

        return list(set(multipunt_polissa_ids) - proforma_polissa_ids)
GiscedataPolissaMultipunt()
