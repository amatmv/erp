 SELECT
      SUBSTRING(c.name,0,21) AS "CUPS"
      ,pol.name as "Contrato"
      ,replace(tit.name,'"', '') as "Nombre cliente"
      ,replace(tit.vat, 'ES','') as "NIF/CIF"
     , COALESCE(replace(c.direccio,'"', ''), '') as "Dirección"
     , i.number AS "Núm. Factura"
     , i.date_invoice AS "Fecha factura"
     , t.name AS "Tarifa de acceso"
     ,CASE WHEN (
          select count(*) as total
          from giscedata_facturacio_lectures_energia fle
          inner join giscedata_lectures_origen lo on fle.origen_id = lo.id
          where fle.factura_id = f.id and lo.codi = '40'

        ) = 0 THEN 'Real'
        ELSE 'Estimada'
        END as origen
     , CASE WHEN f.tipo_rectificadora='A' THEN 'Anuladora'
    WHEN f.tipo_rectificadora='B' THEN 'Anuladora+Rectificadora'
    WHEN f.tipo_rectificadora='R' THEN 'Rectificadora'
    WHEN f.tipo_rectificadora='RA' THEN 'Rectificadora sin Anuladora'
    WHEN f.tipo_rectificadora='BRA' THEN 'Anuladora (ficticia)'
    ELSE 'Normal'
    END AS "tipo factura"
     , f.data_inici AS "Fecha inicial"
     , f.data_final AS "Fecha final"
     , (f.data_final -f.data_inici) + 1 as "num. dias"
     , CASE
         WHEN i.type IN ('in_refund', 'out_refund') THEN -COALESCE(ene.energia, 0)
         ELSE COALESCE(ene.energia, 0)
     END AS energia_kW
     , CASE
         WHEN i.type IN ('in_refund', 'out_refund') THEN -COALESCE(f.total_lloguers, 0)
         ELSE COALESCE(f.total_lloguers, 0)
     END AS alquiler
     , COALESCE(altres.altres, 0) AS "otros conceptos"
     , COALESCE(garantia.garantia, 0) AS "fianza"
     , CASE
         WHEN i.type IN ('in_refund', 'out_refund') THEN -COALESCE(it_iese.base,0)
         ELSE COALESCE(it_iese.base,0)
     END AS "Base IESE"
     , CASE
         WHEN i.type IN ('in_refund', 'out_refund') THEN -COALESCE(i.amount_untaxed, 0)
         ELSE COALESCE(i.amount_untaxed, 0)
     END AS Base
     ,polm.name as mulitpunto

 FROM giscedata_facturacio_factura f
 LEFT JOIN giscedata_cups_ps c ON (f.cups_id=c.id)
 LEFT JOIN giscedata_polissa pol ON (f.polissa_id=pol.id)
 LEFT JOIN account_invoice i ON (i.id=f.invoice_id)
 LEFT JOIN giscedata_polissa_tarifa t ON (t.id=f.tarifa_acces_id)
 LEFT JOIN (
 SELECT lf.factura_id AS factura_id,SUM(li.quantity) AS energia
 FROM giscedata_facturacio_factura_linia lf
 LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
 WHERE lf.tipus='energia' AND lf.isdiscount = False
 GROUP BY lf.factura_id
 ) AS ene ON (ene.factura_id=f.id)

 LEFT JOIN (
 SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS altres
 FROM giscedata_facturacio_factura_linia lf
 LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
 JOIN product_product p ON (li.product_id=p.id)
 WHERE lf.tipus='altres' and p.default_code!='CON06'
 GROUP BY lf.factura_id
 ) AS altres ON (altres.factura_id=f.id)

 LEFT JOIN (
 SELECT lf.factura_id AS factura_id,SUM(li.price_subtotal) AS garantia
 FROM giscedata_facturacio_factura_linia lf
 LEFT JOIN account_invoice_line li ON (li.id=lf.invoice_line_id)
 JOIN product_product p ON (li.product_id=p.id)
 WHERE lf.tipus='altres' and p.default_code ='CON06'
 GROUP BY lf.factura_id
 ) AS garantia ON (altres.factura_id=f.id)

 LEFT JOIN account_invoice_tax it_21 ON (it_21.invoice_id=i.id AND (it_21.name LIKE 'IVA 21%%' OR it_21.name LIKE '21%%%% IVA%%'))
 LEFT JOIN account_invoice_tax it_iese ON (it_iese.invoice_id=i.id AND it_iese.name LIKE '%%electricidad%%')
 LEFT JOIN res_partner tit on (tit.id = i.partner_id)
 LEFT JOIN payment_type pt on (pol.tipo_pago = pt.id)
 LEFT JOIN giscedata_polissa_multipunt polm on (polm.id = pol.multipunt_id)
 INNER JOIN giscedata_facturacio_multipunt_invoices fmi on (fmi.factura_id = f.id)
 WHERE
     fmi.invoice_id = %s
 ORDER BY CUPS,i.number