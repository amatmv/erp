# -*- coding: utf-8 -*-
from datetime import datetime

import netsvc
from osv import osv, orm, fields



class GiscedataFacturacioMultipuntInvoices(osv.osv):
    """Relació entre facturas multipunto i giscedata.facturacio factures
    """
    _name = 'giscedata.facturacio.multipunt.invoices'

    _sql_constraints = [
        ('gd_facturacio_mulitpunt_invoices_uniq',
         'unique(factura_id)',
         "No pot haver-hi dos vegades una factura d'energia "
         "agrupada en mulitpunt"),
    ]

    def create_relation_from_ids(
            self, cursor, uid, invoice_id, factures_ids, context=None):
        if context is None:
            context = {}

        for fact_id in factures_ids:
            self.create(
                cursor, uid, {'invoice_id': invoice_id, 'factura_id': fact_id}
            )

    _columns = {
        'invoice_id': fields.many2one(
            'account.invoice', 'Factura mulitpunto',
            required=True, ondelete='restrict'
        ),
        'factura_id': fields.many2one(
            'giscedata.facturacio.factura', 'Factura proforma', required=True,
            ondelete='restrict'
        )
    }

GiscedataFacturacioMultipuntInvoices()


class GiscedataFacturacioFactura(osv.osv):
    """Classe per importar factures en XML
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def proform_invoice_open(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        seq_obj = self.pool.get('ir.sequence')
        wf_service = netsvc.LocalService("workflow")
        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        cfg_obj = self.pool.get('res.config')

        # Revisem si hem de modificar la data de la factura
        change_date_invoice = int(
            cfg_obj.get(cursor, uid, 'fact_change_date_invoice', '0')
        )
        if change_date_invoice:
            current_date = datetime.now().strftime('%Y-%m-%d')
            self.write(cursor, uid, ids, {'date_invoice': current_date})

        for factura in self.browse(cursor, uid, ids, context):
            # Assign the numbers from the sequence to the invoice
            polissa = factura.polissa_id
            multipunt = polissa.multipunt_id
            if multipunt.proforma_sequence:
                seq_id = multipunt.proforma_sequence.id
                if not factura.number:
                    factura.write(
                        {'number': seq_obj.get_id(cursor, uid, seq_id)}
                    )

            # Move from lot and change last invoiced date
            te_lot = factura.lot_facturacio
            es_rectificadora = factura.tipo_rectificadora != 'N'
            es_a_client = factura.type == 'out_invoice'
            if not te_lot or es_rectificadora or not es_a_client:
                # Pot ser que una factura de liquidació no s'hagi associat
                # a un lot de facturació
                continue
            search_params = [
                ('polissa_id.id', '=', factura.polissa_id.id),
                ('lot_id.id', '=', factura.lot_facturacio.id)
            ]
            clot_ids = clot_obj.search(
                cursor, uid, search_params, context={'active_test': False}
            )
            if clot_ids:
                clot_obj.wkf_finalitzat(cursor, uid, clot_ids, context)
            lot_polissa = factura.polissa_id.lot_facturacio
            if lot_polissa and lot_polissa == factura.lot_facturacio:
                data_final = factura.lot_facturacio.data_final
                factura.polissa_id.assignar_seguent_lot(
                    {'data_final': data_final}
                )

        base_ids = self._get_base_ids(cursor, uid, ids, context)
        for fact_id in base_ids:
            wf_service.trg_validate(
                uid, 'account.invoice', fact_id, 'invoice_proforma2', cursor
            )

        return True

    def is_in_proforma_multipoint(self, cursor, uid, fact_id, context=None):
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')

        inv_vals = self.read(cursor, uid, fact_id, ['polissa_id'], context)

        polissa_vals = polissa_obj.read(
            cursor, uid, inv_vals['polissa_id'], ['multipunt_id'], context
        )
        if polissa_vals['multipunt_id']:
            is_proforma = multipunt_obj.read(
                cursor, uid, polissa_vals['multipunt_id'], ['is_proforma']
            )['is_proforma']
            if is_proforma:
                return True

        return False

    def invoice_open(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')

        invoice_vals = self.read(
            cursor, uid, ids, ['polissa_id', 'type'], context
        )

        proforma_poli = multipunt_obj.get_profroma_polisses(
            cursor, uid, context=context
        )

        proforma_inv = []
        no_proforma_inv = []
        for inv_vals in invoice_vals:
            # It is done like this because it seems like it would be more
            # efficient. In case we wanted to check everything for each invoice
            # we could use the method is_in_proforma_multipoint
            out_type = 'out' in inv_vals['type']
            for_prof_poli = inv_vals['polissa_id'][0] in proforma_poli

            if out_type and for_prof_poli:
                proforma_inv.append(inv_vals['id'])
            else:
                no_proforma_inv.append(inv_vals['id'])

        res = super(
            GiscedataFacturacioFactura, self
        ).invoice_open(cursor, uid, no_proforma_inv, context=context)

        self.proform_invoice_open(cursor, uid, proforma_inv, context=context)

        return res


GiscedataFacturacioFactura()


class GiscedataFacturacioExtra(osv.osv):
    _name = 'giscedata.facturacio.extra'
    _inherit = 'giscedata.facturacio.extra'

    def get_states_invoiced(self, cursor, uid, context=None):
        res = super(
            GiscedataFacturacioExtra, self).get_states_invoiced(cursor, uid)
        res.append('grouped_proforma')
        return res

    def get_states_not_invoiced(self, cursor, uid, context=None):
        res = super(
            GiscedataFacturacioExtra, self).get_states_not_invoiced(cursor, uid)
        if 'proforma2' in res:
            res = filter(lambda x: x != 'proforma2', res)
        return res


GiscedataFacturacioExtra()
