# -*- coding: utf-8 -*-
{
    "name": "Factures multipunt",
    "description": """Afegeix la opcio de crear factures agrupadores per les factures d'un multipunt""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa_comer_multipunt",
        "custom_search"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_polissa_multipunt_demo.xml"
    ],
    "update_xml":[
        "giscedata_polissa_multipunt_data.xml",
        "giscedata_polissa_multipunt_view.xml",
        "security/ir.model.access.csv",
        "wizard/crear_factura_agrupada_view.xml",
        "custom_searches_proforma.xml",
        "wizard/passar_agrupada_a_proforma_view.xml"
    ],
    "active": False,
    "installable": True
}
