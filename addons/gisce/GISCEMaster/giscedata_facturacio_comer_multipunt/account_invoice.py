# -*- coding: utf-8 -*-
from osv import osv, orm, fields
from account.invoice import INVOICE_STATE_SELECTION
from addons.giscedata_facturacio.defs import TIPO_RECTIFICADORA_SELECTION, SIGN
import netsvc
from tools.translate import _
from datetime import datetime


INVOICE_STATE_SELECTION.append(
    ('grouped_proforma', 'Proforma Agrupada')
)


class AccountInvoice(osv.osv):
    _name = 'account.invoice'
    _inherit = 'account.invoice'

    def create_invoice_for_multipoint(self, cursor, uid, multipunt_id,
                                      context=None):
        if context is None:
            context = {}

        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')
        journal_obj = self.pool.get('account.journal')
        res_config = self.pool.get('res.config')

        journal_code = res_config.get(
            cursor, uid, 'grouped_invoice_journal', ''
        )
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', journal_code)], limit=1
        )[0]

        multipunt_vals = multipunt_obj.read(
            cursor, uid, multipunt_id, [
                'name',
                'partner_address_id',
                'partner_id',
                'bank_id',
                'contact_address_id',
                'payment_type_id'
            ]
        )

        inv_vals = self.onchange_partner_id(
            cursor, uid, [], 'out_invoice', multipunt_vals['partner_id'][0],
            partner_bank=multipunt_vals['bank_id'][0]
        )['value']

        payment_type = None
        if multipunt_vals['payment_type_id']:
            payment_type = multipunt_vals['payment_type_id'][0]

        inv_vals.update(
            {
                'origin': None,
                'comment': None,
                'number': False,
                'journal_id': journal_id,
                'address_invoice_id': multipunt_vals['partner_address_id'][0],
                'partner_id': multipunt_vals['partner_id'][0],
                'address_contact_id': multipunt_vals['contact_address_id'][0],
                'reference_type': u'none',
                'state': 'draft',
                'partner_bank': multipunt_vals['bank_id'][0],
                'type': 'out_invoice',
                'name': multipunt_vals['name'],
                'payment_type': payment_type,
                'date_invoice': datetime.now()
            }
        )
        return self.create(cursor, uid, inv_vals)

    def create_grouped_invoice(self, cursor, uid, proforma_ids, multipunt_id,
                               context=None):
        if context is None:
            context = {}

        wf_service = netsvc.LocalService("workflow")

        multipunt_obj = self.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fact_multi_obj = self.pool.get('giscedata.facturacio.multipunt.invoices')
        linia_fact_obj = self.pool.get('giscedata.facturacio.factura.linia')
        line_inv_obj = self.pool.get('account.invoice.line')

        rectif_types = dict(TIPO_RECTIFICADORA_SELECTION)
        line_types = dict(linia_fact_obj._tipus_selection(cursor, uid, context))

        ignore_on_line_name = ['N']

        read_params = ['number', 'tipo_rectificadora', 'linia_ids',
                       'date_invoice']
        fact_reads = fact_obj.read(
            cursor, uid, proforma_ids, read_params, context
        )

        invoice_id = self.create_invoice_for_multipoint(
            cursor, uid, multipunt_id, context
        )

        multipunt_name = multipunt_obj.read(
            cursor, uid, multipunt_id, ['name']
        )['name']

        comment = _(
            u'Això és la factura agrupadora per al multipunt {0} '
            u'de les factures [{1}]'
        ).format(
            multipunt_name,
            ', '.join([str(fact['number']) for fact in fact_reads])
        )
        self.write(cursor, uid, invoice_id, {'comment': comment})

        for fact_vals in fact_reads:
            sign = SIGN[fact_vals['tipo_rectificadora']]
            for linia_fact_id in fact_vals['linia_ids']:
                linia_fact_vals = linia_fact_obj.read(
                    cursor, uid, linia_fact_id, ['invoice_line_id', 'tipus'],
                    context
                )
                line_inv_id = linia_fact_vals['invoice_line_id'][0]

                line_data = line_inv_obj.copy_data(
                    cursor, uid, line_inv_id, context
                )[0]

                line_data.update({'invoice_id': invoice_id})

                old_name = line_data['name']

                line_name = '{0} - {1} - {2}'.format(
                    fact_vals['number'], old_name,
                    line_types[linia_fact_vals['tipus']]
                )
                rect_type = fact_vals['tipo_rectificadora']
                if rect_type not in ignore_on_line_name:
                    fact_rect_type = rectif_types[rect_type]
                    line_name += ' - {0}'.format(fact_rect_type)
                line_data['name'] = line_name
                line_data['quantity'] = line_data['quantity'] * sign

                line_inv_id = line_inv_obj.create(cursor, uid, line_data)

        self.button_reset_taxes(cursor, uid, [invoice_id], context)
        # wf_service.trg_validate(
        #     uid, 'account.invoice', invoice_id, 'invoice_open', cursor
        # )

        date_invoice = self.read(
            cursor, uid, invoice_id, ['date_invoice']
        )['date_invoice']

        for proforma in fact_reads:
            date_invoice_proforma = proforma['date_invoice']
            fact_obj.write(
                cursor, uid, proforma['id'],
                {'origin_date_invoice': date_invoice_proforma}
            )

        fact_obj.write(
            cursor, uid, proforma_ids, {'date_invoice': date_invoice}
        )

        fact_multi_obj.create_relation_from_ids(
            cursor, uid, invoice_id, proforma_ids, context=context
        )

        base_ids = fact_obj._get_base_ids(cursor, uid, proforma_ids, context)
        for fact_id in base_ids:
            wf_service.trg_validate(
                uid, 'account.invoice', fact_id, 'invoice_group', cursor
            )


        return invoice_id
AccountInvoice()
