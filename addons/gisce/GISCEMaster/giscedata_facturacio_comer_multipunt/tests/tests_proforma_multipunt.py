# -*- coding: utf-8 -*-
import unittest
from destral import testing
from destral.transaction import Transaction
from expects import *
from addons.giscedata_facturacio.defs import TIPO_RECTIFICADORA_SELECTION, SIGN


class TestInvoicesInProformaMultipunt(testing.OOTestCase):
    def test_opening_invoice_in_proforma_multipoint_goes_to_proforma(self):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            factura_obj.invoice_open(cursor, uid, [factura_id])

            factura = factura_obj.browse(cursor, uid, factura_id)
            expect(factura.state).to(equal('proforma2'))

    def test_opening_invoice_in_not_proforma_multipoint_goes_to_open(self):
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            multipoint_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]
            multipunt_obj.write(
                cursor, uid, multipoint_id, {'is_proforma': False}
            )

            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            factura_obj.invoice_open(cursor, uid, [factura_id])

            factura = factura_obj.browse(cursor, uid, factura_id)
            expect(factura.state).to(equal('open'))

    def test_opening_regular_invoice_goes_to_open(self):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Set a correct account_id
            account_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'a_recv'
            )[1]
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'factura_0001'
            )[1]
            lin_fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'linia_factura_0001'
            )[1]
            factura_obj.write(
                cursor, uid, factura_id, {'account_id': account_id}
            )
            linia_obj.write(
                cursor, uid, lin_fact_id, {'account_id': account_id}
            )

            # Open the invoice
            factura_obj.invoice_open(cursor, uid, [factura_id])

            factura = factura_obj.browse(cursor, uid, factura_id)
            expect(factura.state).to(equal('open'))

    def test_multiple_openings_give_diffrent_numbers(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            fact_vals = fact_obj.read(cursor, uid, inv_to_open, ['number'])
            fact_names = {fact['number'] for fact in fact_vals}
            expect(len(fact_names)).to(equal(4))


class TestProformaMultipoint(testing.OOTestCase):
    def test_get_proforma_polisses(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_multipunt_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'polissa_multipunt_0001'
            )[1]
            polissa_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa',
                'polissa_0001'
            )[1]

            multipunt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            prof_poli_ids = multipunt_obj.get_profroma_polisses(cursor, uid)
            expect(set(prof_poli_ids)).to(equal({polissa_multipunt_0001}))

            polissa_obj.write(
                cursor, uid, polissa_0001, {'multipunt_id': multipunt_id}
            )

            new_prof_poli_ids = multipunt_obj.get_profroma_polisses(cursor, uid)
            expect(set(new_prof_poli_ids)).to(
                equal(
                    {polissa_multipunt_0001, polissa_0001}
                )
            )

    def test_get_pending_proformas(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            fact_obj.invoice_open(
                cursor, uid, [fact_0001, fact_0002, fact_0003, fact_0004]
            )

            proformas_1_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            proformas_2_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date='2016-03-15', end_date=None
            )
            proformas_3_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date='2016-05-15'
            )
            proformas_4_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date='2016-03-15',
                end_date='2016-05-15'
            )

            expect(set(proformas_1_ids)).to(
                equal({fact_0001, fact_0002, fact_0003, fact_0004})
            )
            expect(set(proformas_2_ids)).to(
                equal({fact_0002, fact_0003, fact_0004})
            )
            expect(set(proformas_3_ids)).to(
                equal({fact_0001, fact_0002, fact_0003})
            )
            expect(set(proformas_4_ids)).to(
                equal({fact_0002, fact_0003})
            )

    def test_get_missing_polisses(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            polissa_multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'polissa_multipunt_0001'
            )[1]
            polissa_no_multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa',
                'polissa_0001'
            )[1]

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            # Initially we are missing the polissa since invoices are on draft
            initial_prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            initial_missing_polisses = multipunt_obj.get_missing_polisses(
                cursor, uid, multi_id, initial_prof_ids
            )
            expect(set(initial_missing_polisses)).to(equal({polissa_multi_id}))

            # We now open the invoices (which, since they are related to a
            # polissa that is in a multipoint with pro-forma, makes them go to
            # proforma2). This will make them to be considered when we do the
            # get_pending_profromas and will make the polissa to be included
            fact_obj.invoice_open(
                cursor, uid, [fact_0001, fact_0002, fact_0003, fact_0004]
            )

            opened_prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            opened_missing_polisses = multipunt_obj.get_missing_polisses(
                cursor, uid, multi_id, opened_prof_ids
            )
            expect(set(opened_missing_polisses)).to(equal(set()))

            # We now add a new polissa to the multipoint, making it so that we
            # are missing a polissa once again
            polissa_obj.write(
                cursor, uid, polissa_no_multi_id, {'multipunt_id': multi_id}
            )
            added_prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            added_missing_polisses = multipunt_obj.get_missing_polisses(
                cursor, uid, multi_id, added_prof_ids
            )
            expect(set(added_missing_polisses)).to(equal({polissa_no_multi_id}))


class TestCreateGroupingInvoice(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscedata_tarifas_peajes_20160101'),
                ], limit=1, order='name DESC'
            )[0]
            newest_name = module_obj.read(
                cursor, uid, newest_id, ['name']
            )['name']

        if newest_name:
            self.openerp.install_module(newest_name)
        return super(TestCreateGroupingInvoice, self).setUp()

    def add_taxes_to_periods(self, txn):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.openerp.pool.get('giscedata.polissa.tarifa.periodes')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        tax_ids = tax_obj.search(
            cursor, uid, [
                '|',
                ('name', '=', 'IVA 21%'),
                ('name', '=', 'Impuesto especial sobre la electricidad')
            ]
        )
        
        product_ids = []
        for tarifa_id in tarifa_obj.search(cursor, uid, []):
            periode_ids = tarifa_obj.read(
                cursor, uid, tarifa_id, ['periodes']
            )['periodes']
            for periode_vals in periode_obj.read(cursor, uid, periode_ids):
                product_ids.append(periode_vals['product_id'][0])
                if periode_vals['product_reactiva_id']:
                    product_ids.append(periode_vals['product_reactiva_id'][0])
                if periode_vals['product_exces_pot_id']:
                    product_ids.append(periode_vals['product_exces_pot_id'][0])

        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, tax_ids)]
            }
        )

    def add_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA {}%'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def set_taxes_to_invoice_lines(self, txn):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = 'IVA 21%'
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        for linia_id in linia_obj.search(cursor, uid, []):
            linia_vals = linia_obj.read(cursor, uid, linia_id, ['product_id', 'tipus'])
            if linia_vals.get('product_id', False):
                product_vals = product_obj.read(
                    cursor, uid, linia_vals['product_id'][0], ['taxes_id']
                )
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, product_vals['taxes_id'])
                        ]
                    }
                )
            elif linia_vals.get('tipus', '') == 'lloguer':
                linia_obj.write(
                    cursor, uid, linia_id, {
                        'invoice_line_tax_id': [
                            (6, 0, iva_id)
                        ]
                    }
                )

    def test_grouping_invoice_uses_multipoint_values(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')
        fact_mulit = self.openerp.pool.get('giscedata.facturacio.multipunt.invoices')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            address_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_address'
            )[1]
            bank_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_bank'
            )[1]

            multipunt_obj.write(
                cursor, uid, multi_id, {
                    'partner_address_id': address_id,
                    'contact_address_id': address_id,
                    'bank_id': bank_id,
                }
            )

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )

            invoice = inv_obj.browse(cursor, uid, inv_id)
            expect(invoice.address_invoice_id.id).to(equal(address_id))
            expect(invoice.address_contact_id.id).to(equal(address_id))
            expect(invoice.partner_bank.id).to(equal(bank_id))
            mulit_lines = fact_mulit.search(
                cursor, uid, [('invoice_id', '=', inv_id)]
            )
            mulit_lines_vals = fact_mulit.read(
                cursor, uid, mulit_lines, ['factura_id']
            )
            mulit_lines_fact_ids = [x['factura_id'][0] for x in
                                    mulit_lines_vals]
            expect(len(prof_ids)).to(equal(len(mulit_lines)))
            expect(prof_ids).to(equal(mulit_lines_fact_ids))

    def test_grouped_invoices_are_set_to_grouped(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_mulit = self.openerp.pool.get(
            'giscedata.facturacio.multipunt.invoices')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )
            mulit_lines = fact_mulit.search(
                cursor, uid, [('invoice_id', '=', inv_id)]
            )
            mulit_lines_vals = fact_mulit.read(
                cursor, uid, mulit_lines, ['factura_id']
            )
            mulit_lines_fact_ids = [x['factura_id'][0] for x in
                                    mulit_lines_vals]
            expect(len(prof_ids)).to(equal(len(mulit_lines)))
            expect(prof_ids).to(equal(mulit_lines_fact_ids))
            expect(set(inv_to_open)).to(equal(set(prof_ids)))
            factures = fact_obj.browse(cursor, uid, prof_ids)
            for fact in factures:
                expect(fact.state).to(equal('grouped_proforma'))

    def test_grouping_invoice_duplicate_fails(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_mulit = self.openerp.pool.get(
            'giscedata.facturacio.multipunt.invoices')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )
            mulit_lines = fact_mulit.search(
                cursor, uid, [('invoice_id', '=', inv_id)]
            )
            mulit_lines_vals = fact_mulit.read(
                cursor, uid, mulit_lines, ['factura_id']
            )
            mulit_lines_fact_ids = [x['factura_id'][0] for x in
                                    mulit_lines_vals]
            expect(len(prof_ids)).to(equal(len(mulit_lines)))
            expect(prof_ids).to(equal(mulit_lines_fact_ids))
            expect(set(inv_to_open)).to(equal(set(prof_ids)))
            factures = fact_obj.browse(cursor, uid, prof_ids)
            for fact in factures:
                expect(fact.state).to(equal('grouped_proforma'))

            def fail_func():
                inv_id = inv_obj.create_grouped_invoice(
                    cursor, uid, prof_ids, multi_id
                )
            # Fail when try to create duplicate group invoices
            expect(fail_func).to(raise_error)




    def test_taxes_are_calculated(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_mulit = self.openerp.pool.get(
            'giscedata.facturacio.multipunt.invoices')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )
            invoice = inv_obj.browse(cursor, uid, inv_id)
            taxes = {}
            for tax in invoice.tax_line:
                taxes[tax.name] = tax.tax_amount
            expect(set(taxes.keys())).to(
                equal({'IVA 21%', 'Impuesto especial sobre la electricidad'})
            )
            base_iese = sum(
                [
                    line.price_subtotal
                    for line in invoice.invoice_line
                    if 'Ene' in line.name or 'Pot' in line.name
                ]
            )
            amount_iese = base_iese * 1.051130 * 0.04864

            base_iva = amount_iese + sum(
                [
                    line.price_subtotal
                    for line in invoice.invoice_line
                ]
            )
            amount_iva = base_iva * 0.21
            expect(
                round(taxes['Impuesto especial sobre la electricidad'], 2)
            ).to(
                equal(round(amount_iese, 2))
            )
            expect(round(taxes['IVA 21%'], 2)).to(
                equal(round(amount_iva, 2))
            )
            expect(round(invoice.amount_total, 2)).to(
                equal(round(base_iva + amount_iva, 2))
            )
            mulit_lines = fact_mulit.search(
                cursor, uid, [('invoice_id', '=', inv_id)]
            )
            mulit_lines_vals = fact_mulit.read(
                cursor, uid, mulit_lines, ['factura_id']
            )
            mulit_lines_fact_ids = [x['factura_id'][0] for x in
                                    mulit_lines_vals]
            expect(len(prof_ids)).to(equal(len(mulit_lines)))
            expect(prof_ids).to(equal(mulit_lines_fact_ids))

    def test_numbers_match_between_grouping_and_grouped_invoices(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_mulit = self.openerp.pool.get(
            'giscedata.facturacio.multipunt.invoices')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.button_reset_taxes(cursor, uid, inv_to_open)

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )
            invoice = inv_obj.browse(cursor, uid, inv_id)

            facts = fact_obj.browse(cursor, uid, inv_to_open)
            suma_totals = sum(
                [
                    fact.amount_total for fact in facts
                ]
            )
            taxes = {}
            for fact in facts:
                for tax in fact.tax_line:
                    taxes[tax.name] = taxes.get(tax.name, 0) + tax.tax_amount
            # In one side we are calculating the sum of lines (grouping invoice)
            # and in the other we are calculating the sums of the total values,
            # which are already rounded (here in the test), so it's possible
            # that there will be a difference of 1 cent
            expect(abs(round(invoice.amount_total - suma_totals, 2))).to(
                be_below_or_equal(0.01)
            )
            for tax in invoice.tax_line:
                expect(
                    abs(round(tax.tax_amount - taxes.get(tax.name, 0), 2))
                ).to(
                    be_below_or_equal(0.01)
                )
            mulit_lines = fact_mulit.search(
                cursor, uid, [('invoice_id', '=', inv_id)]
            )
            mulit_lines_vals = fact_mulit.read(
                cursor, uid, mulit_lines, ['factura_id']
            )
            mulit_lines_fact_ids = [x['factura_id'][0] for x in mulit_lines_vals]
            expect(len(prof_ids)).to(equal(len(mulit_lines)))
            expect(prof_ids).to(equal(mulit_lines_fact_ids))

    def test_lines_on_grouped_invoice_have_correct_sign(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_mulit = self.openerp.pool.get(
            'giscedata.facturacio.multipunt.invoices')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        rectif_types = dict(TIPO_RECTIFICADORA_SELECTION)

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            fact_obj.write(cursor, uid, fact_0001, {'tipo_rectificadora': 'N'})
            fact_obj.write(cursor, uid, fact_0002, {'tipo_rectificadora': 'A'})
            fact_obj.write(cursor, uid, fact_0003, {'tipo_rectificadora': 'B'})
            fact_obj.write(cursor, uid, fact_0004, {'tipo_rectificadora': 'R'})

            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )
            invoice = inv_obj.browse(cursor, uid, inv_id)
            invoice_types = {
                fact['number']: fact['tipo_rectificadora']
                for fact in fact_obj.read(cursor, uid, inv_to_open)
            }
            for line in invoice.invoice_line:
                ori_inv_type = invoice_types[line.name.split(' ')[0]]
                if ori_inv_type not in ['N']:
                    # We don't add Normal to the lines that come from a
                    # Normal invoice
                    expect(line.name).to(contain(rectif_types[ori_inv_type]))
                value = line.price_subtotal * SIGN[ori_inv_type]
                expect(value).to(equal(abs(value)))
            mulit_lines = fact_mulit.search(
                cursor, uid, [('invoice_id', '=', inv_id)]
            )
            expect(len(prof_ids)).to(equal(len(mulit_lines)))

    def test_original_date_proform_on_multipoint(self):
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fact_mulit = self.openerp.pool.get(
            'giscedata.facturacio.multipunt.invoices')
        inv_obj = self.openerp.pool.get('account.invoice')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]
            multi_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'multipunt_0001'
            )[1]

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]
            date_list = {}

            for fact in inv_to_open:
                date_list[fact] = (fact_obj.read(
                    cursor, uid, fact, ['date_invoice']
                )['date_invoice'])

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            prof_ids = multipunt_obj.get_pending_proformas(
                cursor, uid, multi_id, start_date=None, end_date=None
            )
            inv_id = inv_obj.create_grouped_invoice(
                cursor, uid, prof_ids, multi_id
            )
            factures = fact_obj.browse(cursor, uid, prof_ids)
            for fact in factures:
                self.assertEqual(fact.origin_date_invoice, date_list[fact.id])

    @unittest.skip(
        'This is currently done manualy by setting the polissa to postal, so '
        'this test will always fail until it is done automaticly in some way'
        '(an update to the test may be needed then)'
    )
    def test_proform_invoices_are_allways_sent_on_postal(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        multipunt_obj = self.openerp.pool.get('giscedata.polissa.multipunt')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            res_config.set(
                cursor, uid, 'grouped_invoice_journal', 'ENERGIA'
            )
            self.add_taxes_to_periods(txn)
            self.add_iva_to_concepts(txn)
            self.set_taxes_to_invoice_lines(txn)

            fact_0001 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0001'
            )[1]
            fact_0002 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0002'
            )[1]
            fact_0003 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0003'
            )[1]
            fact_0004 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'factura_multipunt_0004'
            )[1]

            polissa_multipunt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer_multipunt',
                'polissa_multipunt_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_multipunt_id, {'enviament': 'email'}
            )
            polissa_obj.send_signal(
                cursor, uid, [polissa_multipunt_id], ['validar', 'contracte']
            )

            inv_to_open = [fact_0001, fact_0002, fact_0003, fact_0004]

            fact_obj.invoice_open(cursor, uid, inv_to_open)

            for proforma_inv in fact_obj.browse(cursor, uid, inv_to_open):
                expect(proforma_inv.per_enviar).to(equal('postal'))
