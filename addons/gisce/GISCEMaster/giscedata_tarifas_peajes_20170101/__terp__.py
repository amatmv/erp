# -*- coding: utf-8 -*-
{
    "name": "Tarifas Peajes Enero 2017",
    "description": """
Actualització de les tarifes de peatges segons el BOE nº 314 - 23/12/2016.
 ETU/1976/2016
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_polissa",
        "giscedata_lectures",
        "product",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_peajes_20170101_data.xml"
    ],
    "active": False,
    "installable": True
}
