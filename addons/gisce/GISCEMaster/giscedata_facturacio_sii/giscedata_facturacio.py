# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioFactura(osv.osv):

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def copy_data(self, cr, uid, id, default=None, context=None):
        if default is None:
            default = {}
        res, x = super(GiscedataFacturacioFactura, self).copy_data(
            cr, uid, id, default, context
        )
        res.update({
            'sii_sent': False,
            'sii_state': 'NoEnviado',
            'sii_registered': False
        })
        return res, x


GiscedataFacturacioFactura()
