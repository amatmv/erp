# -*- coding: utf-8 -*-
{
    "name": "Módulo para la Librería de Suministro Inmediato de Información para facturas de electricidad",
    "description": """Este módulo añade las siguientes funcionalidades:
  * Añade los campos necesarios para la utilización de la librería SII
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_facturacio",
        "l10n_ES_aeat_sii",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
