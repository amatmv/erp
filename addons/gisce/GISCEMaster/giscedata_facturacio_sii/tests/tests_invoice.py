# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction


class TestAccountInvoiceSII(testing.OOTestCase):
    def test_duplicate_invoice_fields_sii(self):
        """ Avoid copying SII fields on giscedata.facturacio.factura"""
        pool = self.openerp.pool
        factura_obj = pool.get('giscedata.facturacio.factura')
        imd_obj = pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # Change the invoice SII fields to non default values
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]

            factura_obj.write(cursor, uid, factura_id, {
                'sii_state': 'Correcto',
                'sii_sent': True,
                'sii_registered': True,
            })

            # Duplicate the invoice
            new_factura_id = factura_obj.copy(cursor, uid, factura_id)

            # Expected values on a new invoice
            expected_values = {
                'sii_state': 'NoEnviado',
                'sii_sent': False,
                'sii_registered': False
            }

            # Read the expected values from the new invoice
            factura_data = factura_obj.read(
                cursor, uid, new_factura_id, expected_values.keys())
            factura_data.pop('id')

            # Expect the values to have the default ones
            self.assertDictEqual(factura_data, expected_values)
