# -*- coding: utf-8 -*-
from osv import fields,osv
import pooler

IESE_XML_IDS = [
    ('giscedata_polissa', 'categ_t20A_new'),
    ('giscedata_polissa', 'categ_t20DHA_new'),
    ('giscedata_polissa', 'categ_t20DHS'),
    ('giscedata_polissa', 'categ_t20A'),
    ('giscedata_polissa', 'categ_t20DHA'),
    ('giscedata_polissa', 'categ_t21DHS'),
    ('giscedata_polissa', 'categ_t30A'),
    ('giscedata_polissa', 'categ_t31A'),
    ('giscedata_polissa', 'categ_t31A_LB'),
    ('giscedata_polissa', 'categ_t61'),
    ('giscedata_polissa', 'categ_t61a'),
    ('giscedata_polissa', 'categ_t61b'),
    ('giscedata_polissa', 'categ_t62'),
    ('giscedata_polissa', 'categ_t63'),
    ('giscedata_polissa', 'categ_t64'),
    ('giscedata_polissa', 'categ_t65'),
    ('giscedata_lectures', 'categ_alq_conta')
]

class config_taxes_facturacio_distri(osv.osv_memory):
    _name = 'config.taxes.facturacio.distri'
    _inherit = 'config.taxes.facturacio.distri'

    _columns = {
        'iese_venda': fields.many2one(
            'account.tax',
            'IESE',
            domain=[('type_tax_use', '=', 'sale')]
        ),
    }
    
    def action_set(self, cr, uid, ids, context=None):

        res = super(config_taxes_facturacio_distri,
                    self).action_set(cr, uid, ids, context)
        
        config = self.browse(cr, uid, ids[0], context)
        # Per cada periode de les tarifes hi apliquem aquests impostos
        product_obj = self.pool.get('product.product')
        categ_ids = []
        for xml_module, xml_id in IESE_XML_IDS:
            md_obj = pooler.get_pool(cr.dbname).get('ir.model.data')
            md_id = md_obj.search(cr, uid, [('module', '=', xml_module),
                                            ('name', '=', xml_id)])
            categ_ids.append(md_obj.browse(cr, uid, md_id[0]).res_id)
        vals = {
            'taxes_id': [(4, config.iese_venda.id)],
        }
        search_params = [('categ_id', 'child_of', categ_ids)]
        productes_ids = product_obj.search(cr, uid, search_params)
        product_obj.write(cr, uid, productes_ids, vals)
                
        return res
        
config_taxes_facturacio_distri()
