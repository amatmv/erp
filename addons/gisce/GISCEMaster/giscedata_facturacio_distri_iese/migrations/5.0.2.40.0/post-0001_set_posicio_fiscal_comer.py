# -*- coding: utf-8 -*-
import pooler
import netsvc


def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel("migration", level, msg)


def migrate(cursor, installed_version):
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    # Busquem totes les factures que tinguin IESE
    cursor.execute("SELECT distinct t.invoice_id FROM account_invoice_tax t "
                   "INNER JOIN account_invoice i ON t.invoice_id = i.id "
                   "WHERE t.name = 'Impuesto especial sobre la electricidad' "
                   "and i.type in ('out_invoice', 'out_refund')")
    inv_ids = [x[0] for x in cursor.fetchall()]
    # Les factures amb IESE no s'han de tocar, ja que tenen el règim correcte
    cursor.execute("SELECT i.id,i.fiscal_position FROM account_invoice i "
                   "INNER JOIN account_journal j ON i.journal_id = j.id "
                   "WHERE i.id NOT IN %s AND i.type IN "
                   "('out_invoice', 'out_refund') "
                   "AND j.code ilike 'ENERGIA%%' ORDER BY i.create_date ASC",
                   (tuple(inv_ids + [-1]), ))
    result = cursor.fetchall()

    invoice_obj = pool.get('account.invoice')
    partner_obj = pool.get('res.partner')
    imd_obj = pool.get('ir.model.data')
    xml_id_mapping = [
        ('fp_nacional_2009', 'fp_nacional_2009_comer'),
        ('fp_nacional_2010', 'fp_nacional_2010_comer'),
        ('fp_nacional_2012', 'fp_nacional_2012_comer')
    ]
    mapping = {}
    fp_nacional_2012_comer_id = False
    for old, new in xml_id_mapping:
        imd_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio', old)
        old_res_id = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
        imd_id = imd_obj._get_id(cursor, uid, 'giscedata_facturacio_distri',
                                 new)
        new_res_id = imd_obj.read(cursor, uid, imd_id, ['res_id'])['res_id']
        mapping[old_res_id] = new_res_id
        if new == 'fp_nacional_2012_comer':
            fp_nacional_2012_comer_id = new_res_id
        log("%s (%s) => %s (%s)" % (old, old_res_id, new,new_res_id))

    # Fem una agrupació per posició fiscal per actualitzar totes les factures
    # de cop
    updates = {}
    for invoice_id, fiscal_position in result:
        updates.setdefault(fiscal_position, [])
        updates[fiscal_position].append(invoice_id)

    partners = []

    for fiscal_position, invoice_ids in updates.items():
        if not fiscal_position in mapping:
            log("No s'ha trobat el mapping per la posicio fiscal %s" % fiscal_position,
                netsvc.LOG_WARNING)
            continue
        log("S'han trobat %s factures per la posició fiscal %s a actualizar "
            "per %s" % (len(invoice_ids), fiscal_position,
                        mapping[fiscal_position]))
        partners += [
            x['partner_id'][0]
                for x in invoice_obj.read(cursor, uid, invoice_ids, ['partner_id'])
        ]

        cursor.execute("UPDATE account_invoice SET fiscal_position = %s "
                       "WHERE id in %s", (mapping[fiscal_position],
                                          tuple(invoice_ids)))

    partners = list(set(partners))
    log("Trobats %s partners a actualitzar" % len(partners))
    # Deshabilitem les constraints
    constraints = partner_obj._constraints[:]
    partner_obj._constraints = []
    for partner in partner_obj.read(cursor, uid, partners, ['name', 'ref']):
        log("Actualitzant %s (%s)" % (partner['name'], partner['ref']))
        partner_obj.write(cursor, uid, [partner['id']], {
            'property_account_position': fp_nacional_2012_comer_id
        }, context={'sync': False})
    partner_obj._constraints = constraints