# -*- coding: utf-8 -*-
{
    "name": "Facturació IESE (Distribuïdora)",
    "description": """Permet la facturació directe a client afegint l'impost IESE
  i posicions fiscals""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_facturacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_data.xml",
        "wizard/config_taxes_facturacio_distri_view.xml"
    ],
    "active": False,
    "installable": True
}
