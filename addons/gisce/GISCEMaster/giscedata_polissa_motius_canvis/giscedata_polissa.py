# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

from motius_canvis_base.giscedata_polissa import tipus_canvis


class GiscedataPolissaMotiuCanvi(osv.osv):

    _name = 'giscedata.polissa.motiu.canvi'

    _columns = {
        'motiu_id': fields.many2one(
            'giscedata.motiu.canvi', u'Motiu Canvi', required=True
        ),
        'tipus_motiu': fields.selection(
            tipus_canvis, 'Tipus canvi', required=True, select=1
        ),
        'observacions':  fields.text(u"Observacions"),
        'polissa_id': fields.many2one('giscedata.polissa', u'Pólissa'),
    }


GiscedataPolissaMotiuCanvi()


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def wkf_cancelar(self, cursor, uid, ids):
        if isinstance(ids, list):
            ids = ids[0]

        motiu_canvi_obj = self.pool.get('giscedata.polissa.motiu.canvi')
        polissa_vals = self.read(
            cursor, uid, ids, ['motius_canvis_ids']
        )
        motius_canvis_vals = motiu_canvi_obj.read(
            cursor, uid, polissa_vals['motius_canvis_ids'], ['tipus_motiu']
        )
        can_be_canceled = False
        for motiu_canvi_vals in motius_canvis_vals:
            can_be_canceled = motiu_canvi_vals['tipus_motiu'] == 'cancelacio'
            if can_be_canceled:
                break

        if can_be_canceled:
            return super(GiscedataPolissa, self).wkf_cancelar(
                cursor, uid, ids
            )
        else:
            raise osv.except_osv(
                _('Error'), _(u'No es pot cancel·lar la pólissa ja que no té '
                              u'un motiu de cancel·lació.')
            )

    def _check_repeated_types(self, cursor, uid, ids, context=None):
        if isinstance(ids, list):
            ids = ids[0]
        motiu_canvi_obj = self.pool.get('giscedata.polissa.motiu.canvi')
        polissa_vals = self.read(
            cursor, uid, ids, ['motius_canvis_ids'], context=context
        )
        motius_canvis_vals = motiu_canvi_obj.read(
            cursor, uid, polissa_vals['motius_canvis_ids'], ['tipus_motiu'],
            context=context
        )
        elems = [motiu['tipus_motiu'] for motiu in motius_canvis_vals]

        return len(elems) == len(set(elems))

    def _get_polissa_origen(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for pol_info in self.read(cursor, uid, ids, ['polissa_anterior_id'], context=context):
            if pol_info['polissa_anterior_id']:
                res[pol_info['id']] = self.calc_polissa_origen(
                    cursor, uid, pol_info['polissa_anterior_id'][0],
                    context=context
                )
            else:
                res[pol_info['id']] = False
        return res

    def calc_polissa_origen(self, cursor, uid, polissa_id, context=None):
        pol_ant = self.read(
            cursor, uid, polissa_id, ['polissa_anterior_id'], context=context
        )['polissa_anterior_id']
        if not pol_ant or (pol_ant and pol_ant[0] == polissa_id):
            return polissa_id
        return self.calc_polissa_origen(cursor, uid, pol_ant[0], context=context)

    _columns = {
        'motius_canvis_ids': fields.one2many(
            'giscedata.polissa.motiu.canvi', 'polissa_id', 'Motius Canvis'
        ),
        'polissa_anterior_id': fields.many2one("giscedata.polissa",
                                               u"Pólissa Anterior",
                                               domain=[
                                                   ('state', '!=', 'cancelada')
                                                       ]
                                               ),
        'polissa_origen_id': fields.function(
            _get_polissa_origen, method=True, type='many2one',
            string=u"Pòlissa Original", relation='giscedata.polissa', store={
                'giscedata.polissa': (
                    lambda self, cursor, uid, ids, c=None: ids,
                    ['polissa_anterior_id'], 10
                )
            })
    }

    _constraints = [
        (_check_repeated_types,
         _(u'Hi ha dos motius de canvis amb el mateix tipus de canvi.'),
         ['motius_canvis_ids'])
    ]
    

GiscedataPolissa()
