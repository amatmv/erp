# -*- coding: utf-8 -*-
{
    "name": "Motius de alta, baixa i tall per pólisses",
    "description": """Afageix els motius de alta, baixa i tall per a les pólisses""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_switching",
        "motius_canvis_base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
