# -*- coding: utf-8 -*-

from giscedata_switching.tests.common_tests import TestSwitchingImport

from destral.transaction import Transaction


class TestSwitchingM1(TestSwitchingImport):

    def test_wizard_mod_con_M1_adm_create_contract_set_polissa_anterior(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)

            imd_obj = self.openerp.pool.get('ir.model.data')
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]

            new_contract_id = self.get_contract_id(txn, xml_id="polissa_0005")
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)

            new_contract.write({'cups': contract.cups.id})

            ctx = {'cas': 'M1', 'pol_id': contract_id, 'proces': 'M1'}

            wiz_id = wiz_obj.create(
                cursor, uid,
                {
                    "generate_new_contract": "exists",
                    'owner': other_id,
                    'pagador': another_id,
                    'retail_tariff': False,
                    'new_contract': new_contract_id
                }, context=ctx
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            # We have a new contract with the same cups and we are in an owner
            # change so it should use info of new contrtact
            wiz.write({'change_adm': True, 'change_atr': False})
            # We simulate the onchange_type
            vals = wiz.onchange_type(
                False, True, wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False, 'M1'
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.proces, 'M1')

            self.assertEqual(wiz.generate_new_contract, "exists")

            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            vals = wiz.onchange_new_contact(
                wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False, 'M1'
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.tariff, contract.tarifa.codi_ocsum)
            self.assertEqual(wiz.owner, contract.pagador)
            self.assertEqual(wiz.owner_pre, contract.pagador)
            self.assertEqual(wiz.vat, contract.pagador.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, contract.direccio_pagament.id)

            # Now we create the atr case and this should generate a new
            # contract with the new values we have written

            wiz.genera_casos_atr(context=ctx)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.state, 'end')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('ref', 'like', 'giscedata.polissa')
            ])
            self.assertEqual(len(res), 1)

            # Check the vals of the new contract
            sw = sw_obj.browse(cursor, uid, res[0])
            pol_obj = self.openerp.pool.get("giscedata.polissa")
            pol = pol_obj.browse(cursor, uid, int(sw.ref.split(",")[1]))

            self.assertNotEqual(pol.id, contract_id)
            self.assertEqual(pol.polissa_anterior_id.id, contract_id)
