# -*- coding: utf-8 -*-
from osv import osv


class GiscedataSwitchingM1_01(osv.osv):
    """Classe pel pas 01
    """
    _name = 'giscedata.switching.m1.01'
    _inherit = 'giscedata.switching.m1.01'

    def config_step(self, cursor, uid, ids, vals, context=None):
        pol_obj = self.pool.get('giscedata.polissa')

        res = super(GiscedataSwitchingM1_01, self).config_step(
            cursor, uid, ids, vals, context=context
        )
        change_type = vals['change_type']

        if change_type == 'owner':
            pas = self.browse(cursor, uid, ids, context=context)
            polissa_anterior_id = pas.sw_id.cups_polissa_id.id
            polissa_nova = pas.sw_id.ref
            polissa_nova_id = int(polissa_nova.split(',')[1]) if polissa_nova else False

            if polissa_nova_id:
                pol_obj.write(
                    cursor, uid, polissa_nova_id,
                    {'polissa_anterior_id': polissa_anterior_id},
                    context=context
                )

        return res

GiscedataSwitchingM1_01()
