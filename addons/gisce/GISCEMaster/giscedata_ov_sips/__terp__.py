# -*- coding: utf-8 -*-
{
    "name": "Módul Comercial Oficina Virtual",
    "description": """
    Módul complementari per a configuració extra de la pólissa a l'Oficina Virtual
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_ov_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
