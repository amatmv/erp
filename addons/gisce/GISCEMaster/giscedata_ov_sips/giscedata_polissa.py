# -*- coding: utf-8 -*-

from osv import osv


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def configurar_contracte_ov(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        sips_obj = self.pool.get('giscedata.cnmc.sips.comer.ps')
        try:
            sips_obj.import_and_update_contract_sips_data(
                cursor, uid, ids, context=context
            )
        except Exception as e:
            sentry = self.pool.get('sentry.setup')
            if sentry is not None:
                sentry.client.captureException()

        return super(GiscedataPolissa, self).configurar_contracte_ov(
            cursor, uid, ids, context=context
        )


GiscedataPolissa()
