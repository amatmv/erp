# -*- coding: utf-8 -*-
{
    "name": "Index per obres per Trams AT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Indexat per obres a Trams AT
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscedata_at",
        "giscedata_obres_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
