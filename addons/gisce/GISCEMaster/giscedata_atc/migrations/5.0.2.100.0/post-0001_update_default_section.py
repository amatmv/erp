# coding=utf-8

import logging
import pooler


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info(
        "Asignar seccio per defecta als subtipus de reclamacio sense seccio per defecte"
    )

    sql = """
        UPDATE giscedata_subtipus_reclamacio SET default_section = (SELECT id FROM crm_case_section WHERE code = 'ATC') WHERE default_section is Null;
    """
    cursor.execute(sql)


def down(cursor, installed_version):
    pass


migrate = up
