# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from addons import get_module_resource
# from giscedata_switching.tests.common_tests import TestSwitchingImport
from datetime import datetime, timedelta
import base64
from workdays import *


REPORT = """comercializadorRemitente,distribuidorRemitente,sector,agenteReclamante,ruta,codigoReclamacion,subtipoReclamacion,cups,totalcups,codigoTarifaPeaje,provincia,codigoComercializador,codigoDistribuidor,necesarioDistribuidor,canal,fechaReclamacion,estadoCerrado,resultado,agenteAccionPendiente,tiempoResolucion,tiempoComercializador,tiempoDistribuidor,tiempoCliente,tiempoOtros
,1234,E,01,2,1,009,ES1234000000000001JN0F,1,001,01,4321,1234,1,03,2018-11-16,1,02,,12.0,8.0,0.0,4.0,0.0
,1234,E,01,2,2,009,ES1234000000000001JN0F,1,001,01,4321,1234,1,02,2018-11-16,0,,10,,,,,
,1234,E,06,3,3,009,ES1234000000000001JN0F,1,001,01,4321,1234,1,03,2018-11-16,1,02,,12.0,8.0,0.0,4.0,0.0
"""


class TestATC(testing.OOTestCase):

    def _test_atc_report(self):

        with Transaction().start(self.database) as txn:
            canal_obj = self.openerp.pool.get('res.partner.canal')
            pol_obj = self.openerp.pool.get("giscedata.polissa")
            case_obj = self.openerp.pool.get('crm.case')
            atc_obj = self.openerp.pool.get('giscedata.atc')
            cursor = txn.cursor
            uid = txn.user
            self.switch(txn, 'distri')
            pol_id = self.get_contract_id(txn, xml_id='polissa_0001')
            # Configure code of the comer of polissa
            pol = pol_obj.browse(cursor, uid, pol_id)
            pol.comercialitzadora.write({'ref': 4321})

            test_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            test_date2 = (datetime.now() + timedelta(days=150)).strftime("%Y-%m-%d %H:%M:%S")
            # Create closed 2 atc cases
            atc1 = self._create_atc_case_client(cursor, uid, "Case 1", pol_id, test_date)
            atc2 = self._create_atc_case_client(cursor, uid, "Case 2", pol_id, test_date, {
                'canal_id': canal_obj.search(cursor, uid, [('cnmc_code', '=', '02')])[0]
            })
            # Create a atc case and left it opened
            atc3 = self._create_atc_case_client(cursor, uid, "Case 3", pol_id, test_date, {
                'reclamante': "06"
            })
            case_obj.case_open(cursor, uid, [atc3])
            atc = atc_obj.browse(cursor, uid, atc3)
            atc.log_ids[0].write({'date': test_date})
            # Create an atc case from another quarter
            self._create_atc_case_client(cursor, uid, "Case 4", pol_id, test_date2, {
                'date': test_date2
            })

            # Call the wizard
            wiz_obj = self.openerp.pool.get("wizard.generate.atc.report")
            wiz_id = wiz_obj.create(cursor, uid, {})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            if datetime.now().month in [1, 2, 3]:
                trimestre = '1'
            elif datetime.now().month in [4, 5, 6]:
                trimestre = '2'
            elif datetime.now().month in [7, 8, 9]:
                trimestre = '3'
            else:
                trimestre = '4'
            wiz.write({
                'trimestre': trimestre,
                'year': datetime.now().year
            })
            wiz.build_atc_report()
            report = base64.b64decode(wiz.report)
            self.assertEqual(report.replace("\r", ""), REPORT.format(datetime.now().strftime("%Y-%m-%d"), atc1, atc2, atc3))

    def _create_atc_case_client(self, cursor, uid, name, pol_id, work_date=None, extra_vals=None):
        if not work_date:
            work_date = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
        atc_obj = self.openerp.pool.get('giscedata.atc')
        case_obj = self.openerp.pool.get('crm.case')
        tracking_obj = self.openerp.pool.get("crm.time.tracking")
        canal_obj = self.openerp.pool.get('res.partner.canal')

        client_track_id = tracking_obj.search(cursor, uid, [('code', '=', '0')])[0]
        distri_track_id = tracking_obj.search(cursor, uid, [('code', '=', '2')])[0]

        canal_id = canal_obj.search(cursor, uid, [('cnmc_code', '=', '03')])[0]

        # Create and open ATC case
        vals = {
            'name': name,
            'reclamante': '01',  # client
            'agent_actual': '10',  # distri
            'time_tracking_id': distri_track_id,
            'polissa_id': pol_id,
            'canal_id': canal_id,
            'date': work_date
        }
        vals.update(atc_obj.onchange_polissa_id(cursor, uid, [], pol_id)['value'])
        if extra_vals:
            vals.update(extra_vals)
        atc_id = atc_obj.create(cursor, uid, vals)
        atc_obj.case_open(cursor, uid, [atc_id])

        # Set the date of the first log -10 days
        atc = atc_obj.browse(cursor, uid, atc_id)
        test_date = datetime.strptime(work_date, "%Y-%m-%d %H:%M:%S")
        atc.log_ids[0].write({'date': test_date - timedelta(days=10)})

        # Change agent to client and pas to pending
        atc_obj.write(cursor, uid, atc_id, {
            'agent_actual': '01',
            'time_tracking_id': client_track_id
        })
        atc_obj.case_pending(cursor, uid, [atc_id])
        # Set the date of the second log -5 days
        atc = atc_obj.browse(cursor, uid, atc_id)
        atc.log_ids[0].write({'date': test_date - timedelta(days=5)})

        # Change agent to distri and pas to open
        atc_obj.write(cursor, uid, atc_id, {
            'agent_actual': '10',
            'time_tracking_id': distri_track_id
        })
        atc_obj.case_open(cursor, uid, [atc_id])
        atc = atc_obj.browse(cursor, uid, atc_id)
        atc.log_ids[0].write({'date': test_date})
        # Close the case
        atc_obj.case_close(cursor, uid, [atc_id])
        atc = atc_obj.browse(cursor, uid, atc_id)
        atc.log_ids[0].write({'date': test_date})
        return atc_id

    def test_wizard_create_atc_from_polissa(self):

        with Transaction().start(self.database) as txn:
            pol_obj = self.openerp.pool.get("giscedata.polissa")
            atc_obj = self.openerp.pool.get('giscedata.atc')
            imd_obj = self.openerp.pool.get('ir.model.data')
            track_obj = self.openerp.pool.get("crm.time.tracking")
            wiz_o = self.openerp.pool.get("wizard.create.atc.from.polissa")
            cursor = txn.cursor
            uid = txn.user

            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', "polissa_0001"
            )[1]
            whereiam = self.openerp.pool.get('giscedata.cups.ps').whereiam(cursor, uid, {})
            if whereiam == "comer":
                tracking_id = track_obj.search(cursor, uid, [('code', '=', '0')])[0]
            else:
                tracking_id = track_obj.search(cursor, uid, [('code', '=', '1')])[0]

            # Creem un cas a data d'avui
            wiz_id = wiz_o.create(cursor, uid,
                                  {'subtipus_id': 1, 'section_id': 3, 'no_responsible': True},
                                  {'active_ids': [pol_id]})

            wiz_o.create_atc_case(cursor, uid, [wiz_id], {'active_ids': [pol_id]})
            wiz = wiz_o.browse(cursor, uid, wiz_id)
            casos_ids = wiz.generated_cases
            cas = atc_obj.browse(cursor, uid, casos_ids[0])
            pol = pol_obj.browse(cursor, uid, pol_id)
            self.assertEqual(cas.name, wiz.name)
            self.assertEqual(cas.date, wiz.date)
            self.assertEqual(cas.user_id.id, False)
            self.assertIn(cas.subtipus_id.desc, cas.subtipus_desc)
            self.assertEqual(cas.resultat, False)
            self.assertEqual(cas.has_process, False)
            self.assertEqual(cas.cups_id.id, pol.cups.id)
            self.assertEqual(cas.canal_id.id, wiz.canal_id.id)
            self.assertEqual(cas.polissa_id.id, pol.id)
            self.assertEqual(cas.partner_id.id, pol.titular.id)
            self.assertEqual(cas.provincia.id, pol.cups.id_provincia.id)
            self.assertEqual(cas.email_from, "info@camptocamp.com")
            self.assertEqual(cas.state, 'draft')
            self.assertEqual(cas.time_tracking_id.id, tracking_id)
            track_info = cas.get_time_tracked()[cas.id]
            self.assertEqual(sum(track_info.values()), 0.0)

            # Ara en creem un en que la dataes anterior, per tant es crearà
            # tracking id imputant a la comer
            avui = datetime.today()
            minus5days = (avui-timedelta(days=5))
            dies_laborables = networkdays(minus5days, avui)-1
            minus5days_str = minus5days.strftime("%Y-%m-%d")+" 00:00:00"
            wiz_id = wiz_o.create(cursor, uid, {'subtipus_id': 1, 'section_id': 3, 'date': minus5days_str}, {'active_ids': [pol_id]})
            wiz_o.create_atc_case(cursor, uid, [wiz_id], {'active_ids': [pol_id]})
            wiz = wiz_o.browse(cursor, uid, wiz_id)
            casos_ids = wiz.generated_cases
            cas = atc_obj.browse(cursor, uid, casos_ids[0])
            pol = pol_obj.browse(cursor, uid, pol_id)
            self.assertEqual(cas.name, wiz.name)
            self.assertEqual(cas.date, wiz.date)
            self.assertNotEqual(cas.user_id.id, False)
            self.assertIn(cas.subtipus_id.desc, cas.subtipus_desc)
            self.assertEqual(cas.resultat, False)
            self.assertEqual(cas.has_process, False)
            self.assertEqual(cas.cups_id.id, pol.cups.id)
            self.assertEqual(cas.canal_id.id, wiz.canal_id.id)
            self.assertEqual(cas.polissa_id.id, pol.id)
            self.assertEqual(cas.partner_id.id, pol.titular.id)
            self.assertEqual(cas.provincia.id, pol.cups.id_provincia.id)
            self.assertEqual(cas.email_from, "info@camptocamp.com")
            self.assertEqual(cas.state, 'draft')
            self.assertEqual(cas.time_tracking_id.id, tracking_id)
            track_info = cas.get_time_tracked()[cas.id]
            self.assertEqual(sum(track_info.values()), dies_laborables)
            self.assertEqual(track_info[tracking_id], dies_laborables)

    def test_wizard_retipificar_atc(self):

        with Transaction().start(self.database) as txn:
            imd_obj = self.openerp.pool.get('ir.model.data')
            atc_o = self.openerp.pool.get("giscedata.atc")
            wiz_o = self.openerp.pool.get("wizard.retipificar.atc")
            cursor = txn.cursor
            uid = txn.user

            pol_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', "polissa_0001"
            )[1]

            # Creem uns casos
            atc_id = self._create_atc_case_client(cursor, uid, "Case 1", pol_id, extra_vals={'subtipus_id': 1})
            atc2_id = self._create_atc_case_client(cursor, uid, "Case 2", pol_id, extra_vals={'subtipus_id': 2})
            atc = atc_o.browse(cursor, uid, atc_id)
            atc2 = atc_o.browse(cursor, uid, atc2_id)
            self.assertEqual(atc.subtipus_id.id, 1)
            self.assertEqual(atc2.subtipus_id.id, 2)
            # Els retipifiquem
            wiz_id = wiz_o.create(cursor, uid, {'subtipus_id': 3, 'comments': "TEST"}, {'active_ids': [atc_id, atc2_id]})
            wiz_o.retipificar_atc_case(cursor, uid, wiz_id, {'active_ids': [atc_id, atc2_id]})
            wiz = wiz_o.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.generated_cases, [atc_id, atc2_id])
            atc = atc_o.browse(cursor, uid, atc_id)
            atc2 = atc_o.browse(cursor, uid, atc2_id)
            self.assertEqual(atc.subtipus_id.id, 3)
            self.assertEqual(atc2.subtipus_id.id, 3)
            self.assertEqual(atc.history_line[0].description, u'TEST')
            self.assertEqual(atc.history_line[1].description, u'Retipificat de 001 a 003')
            self.assertEqual(atc2.history_line[0].description, u'TEST')
            self.assertEqual(atc2.history_line[1].description, u'Retipificat de 002 a 003')
