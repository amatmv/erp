# -*- coding: utf-8 -*-
from osv import osv, fields
from osv.orm import browse_record
from gestionatr.defs import *
from datetime import datetime, timedelta
from tools.translate import _

TABLA_RECLAMANTE = TABLA_83 + [('10', u'Distribuidor')]

TABLA_RESULTADOS = TABLA_80[0:3]

NOT_ALLOWED_SUBTIPUS = ['036', '038', '039', '040', '041', '042', '043',
                        '044', '045', '046', '048', '056', '066', '067']


def get_quarter(date):
    return ((date.month - 1) // 3) + 1


class ResPartnerCanal(osv.osv):

    _name = "res.partner.canal"
    _inherit = "res.partner.canal"

    _columns = {
        'cnmc_code': fields.char(_('Codi'), size=2),
    }
    _defaults = {
        'active': lambda *a: 1,
    }

ResPartnerCanal()


class GiscedataAtc(osv.OsvInherits):

    _name = "giscedata.atc"
    _inherits = {"crm.case": "crm_id"}

    @staticmethod
    def get_tipus(cursor, uid):
        return TABLA_81

    def get_subtipus(self, cursor, uid, tipus):
        st_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = st_obj.q(cursor, uid).read(['name', 'desc']).where([
            ('type', '=', tipus),
            ('sector', 'in', ('t', 'e'))
        ])
        return subtipus

    def calc_temps_total_tracking_id(self, cursor, uid, atc_id, tracking_id, context=None):
        lg_obj = self.pool.get('crm.case.log')
        cas_id = self.read(cursor, uid, atc_id, ['crm_id'])['crm_id'][0]
        return round(lg_obj.calc_time_same_tracking(cursor, uid, cas_id, tracking_id), 1)

    def get_time_tracked(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list,tuple)):
            ids = [ids]
        track_obj = self.pool.get('crm.time.tracking')
        aviable_agents = track_obj.search(cursor, uid, [])
        res = {}
        for atc_id in ids:
            res[atc_id] = {}
            for agent in aviable_agents:
                res[atc_id][agent] = self.calc_temps_total_tracking_id(cursor, uid, atc_id, agent, context=None)
        return res

    def add_time_tracked(self, cursor, uid, ids, time_spend, tracking_id=None, context=None):
        """
        :param cursor:
        :param uid:
        :param ids:
        :param time_spend: numero de dies gastats (float)
        :param tracking_id:
        :param context:
        :return:
        """
        if context is None:
            context = {}
        if not isinstance(ids, (list,tuple)):
            ids = [ids]

        if not tracking_id:
            track_obj = self.pool.get("crm.time.tracking")
            whereiam = self.pool.get('giscedata.cups.ps').whereiam(cursor, uid, context)
            if whereiam == "comer":
                tracking_id = track_obj.search(cursor, uid, [('code', '=', '0')])[0]
            else:
                tracking_id = track_obj.search(cursor, uid, [('code', '=', '1')])[0]

        avui = datetime.today()
        inici_time_spent = avui - timedelta(days=time_spend)
        lg_obj = self.pool.get('crm.case.log')
        res = {}
        for atc_info in self.read(cursor, uid, ids, ['crm_id', 'canal_id', 'section_id']):
            vals = {
                'case_id': atc_info['crm_id'][0],
                'canal_id': atc_info['canal_id'][0],
                'section_id': atc_info['section_id'][0],
                'user_id': uid,
                'date': inici_time_spent.strftime("%Y-%m-%d"),
                'time_tracking_id': tracking_id,
            }
            lid1 = lg_obj.create(cursor, uid, vals)
            vals['date'] = avui.strftime("%Y-%m-%d")
            lid2 = lg_obj.create(cursor, uid, vals)
            res[atc_info['id']] = [lid1, lid2]
        return res

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id, email=False):
        """Al canviar polissa s'actualitza 'partner_id', 'provincia' i si és possible
        'partner_address_id'"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        polissa_obj = self.pool.get("giscedata.polissa")
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        provincia = polissa.cups.id_provincia.id
        res['value'].update({'provincia': provincia})
        partner_id = polissa.titular.id
        res['value'].update({'partner_id': partner_id})
        if partner_id:
            addr = self.pool.get('res.partner').address_get(
                cursor, uid, [partner_id], ['contact'])
            data = {'partner_address_id': addr['contact']}
            if addr['contact'] and not email:
                data['email_from'] = self.pool.get(
                    'res.partner.address').browse(
                    cursor, uid, addr['contact']).email
            res['value'].update(data)
        return res

    def onchange_partner_id(self, cursor, uid, ids, partner_id, email=False):
        """Al canviar partner_id s'actualitza 'polissa_id', 'provincia',
        'total_cups' i si és possible 'partner_address_id'"""
        if not partner_id:
            return {'value': {'partner_address_id': False, 'polissa_id': False}}
        res = {'value': {}, 'domain': {}, 'warning': {}}
        partner_obj = self.pool.get("res.partner")
        partner = partner_obj.browse(cursor, uid, partner_id)
        polissa_id = ""
        provincia = ""
        if partner.polisses and len(partner.polisses) > 0:
            polissa = partner.polisses[0]
            polissa_id = polissa.id
            if polissa.cups:
                provincia = polissa.cups.id_provincia.id
            else:
                res['value'].update({'total_cups': 0})
        else:
            res['value'].update({'total_cups': 0})
        res['value'].update({'provincia': provincia})
        res['value'].update({'polissa_id': polissa_id})

        addr = self.pool.get('res.partner').address_get(
            cursor, uid, [partner_id], ['contact'])
        data = {'partner_address_id': addr['contact']}
        if addr['contact'] and not email:
            data['email_from'] = self.pool.get('res.partner.address').browse(
                cursor, uid, addr['contact']).email
        res['value'].update(data)
        return res

    def onchange_tracking_id(self, cursor, uid, ids, time_tracking_id):
        """Al canviar 'time_tracking_id' s'actualitza 'agent_actual'"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tracking_obj = self.pool.get("crm.time.tracking")
        tracking = tracking_obj.read(cursor, uid, time_tracking_id, ['code'])
        if tracking['code'] == '0':
            data = {'agent_actual': '06'}  # Comer
        elif tracking['code'] == '1':
            data = {'agent_actual': '10'}  # Distri
        elif tracking['code'] == '2':
            data = {'agent_actual': '01'}  # Client
        else:
            data = {'agent_actual': '02'}  # Other
        res['value'].update(data)
        return res

    def onchange_agent_actual(self, cursor, uid, ids, agent_actual):
        """Al canviar 'agent_actual' s'actualitza 'time_tracking_id'"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tracking_obj = self.pool.get("crm.time.tracking")
        if agent_actual == '06':  # Comer
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '0')])
        elif agent_actual == '10':  # Distri
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '1')])
        elif agent_actual == '01':  # Titular
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '2')])
        else:  # Others
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '3')])
        res['value'].update({'time_tracking_id': tracking[0]})
        return res

    def _get_atc_section(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        sec_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_atc', 'atc_section')
        return sec_id[1]

    def _get_whereiam(self, cursor, uid, context=None):
        cups_obj = self.pool.get('giscedata.cups.ps')
        where = cups_obj.whereiam(cursor, uid)
        if where == 'distri':
            return '10'
        else:  # == 'comer'
            return '06'

    def _get_whereiam_time_tracking(self, cursor, uid, context=None):
        cups_obj = self.pool.get('giscedata.cups.ps')
        where = cups_obj.whereiam(cursor, uid)
        tracking_obj = self.pool.get("crm.time.tracking")
        if where == 'distri':
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '1')])
        else:  # == 'comer'
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '0')])
        return tracking[0]

    def _get_default_subtipus(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        return imd_obj.get_object_reference(
            cursor, uid, 'giscedata_subtipus_reclamacio',
            'subtipus_reclamacio_009'
        )[1]

    def case_log(self, cr, uid, ids,context={}, email=False, *args):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        infos = self.read(cr, uid, ids, ['crm_id', 'canal_id'])
        cids = [c['crm_id'][0] for c in infos]
        res = self.pool.get("crm.case").case_log(cr, uid, cids, context, email, args)
        for info in infos:
            self.write(cr, uid, info['id'], {'canal_id': info['canal_id'] and info['canal_id'][0] or False})
        return res

    def case_log_reply(self, cr, uid, ids,context={}, email=False, *args):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        infos = self.read(cr, uid, ids, ['crm_id', 'canal_id'])
        cids = [c['crm_id'][0] for c in infos]
        res = self.pool.get("crm.case").case_log_reply(cr, uid, cids, context, email, args)
        for info in infos:
            self.write(cr, uid, info['id'], {'canal_id': info['canal_id'] and info['canal_id'][0] or False})
        return res

    def case_open(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            atc_data['crm_id'][0]
            for atc_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        return crm_obj.case_open(cr, uid, crm_ids, args)

    def case_close(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            atc_data['crm_id'][0]
            for atc_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        return crm_obj.case_close(cr, uid, crm_ids, args)

    def case_cancel(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            atc_data['crm_id'][0]
            for atc_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        return crm_obj.case_cancel(cr, uid, crm_ids, args)

    def case_pending(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            atc_data['crm_id'][0]
            for atc_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        return crm_obj.case_pending(cr, uid, crm_ids, args)

    def case_reset(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            atc_data['crm_id'][0]
            for atc_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        return crm_obj.case_reset(cr, uid, crm_ids, args)

    def case_escalate(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')
        crm_ids = [
            atc_data['crm_id'][0]
            for atc_data in self.read(cr, uid, ids, ['crm_id'])
        ]
        return crm_obj.case_escalate(cr, uid, crm_ids, args)

    def _ff_get_cups_id(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for pol_info in self.read(cursor, uid, ids, ['polissa_id'], context=context):
            polissa_obj = self.pool.get('giscedata.polissa')
            res[pol_info['id']] = False
            if pol_info['polissa_id']:
                cups_id = polissa_obj.read(cursor, uid, pol_info['polissa_id'][0], ['cups'])['cups']
                if cups_id:
                    res[pol_info['id']] = cups_id[0]
        return res

    def _ff_get_subtype_desc(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        for subtype_info in self.read(cursor, uid, ids, ['subtipus_id'], context=context):
            subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
            res[subtype_info['id']] = False
            if subtype_info['subtipus_id']:
                subtype_desc = "{} - {}".format(
                    subtype_info['subtipus_id'][1],
                    subtipus_obj.read(cursor, uid, subtype_info['subtipus_id'][0], ['desc'])['desc']
                )
                if subtype_desc:
                    if len(subtype_desc) > 128:
                        subtype_desc = subtype_desc[:128]
                    res[subtype_info['id']] = subtype_desc
        return res

    _columns = {
        'crm_id': fields.many2one('crm.case', required=True),
        'total_cups': fields.integer(_(u'Número total afectats')),
        'resultat': fields.selection(TABLA_RESULTADOS, _("Resultat")),
        'reclamante': fields.selection(TABLA_RECLAMANTE, _("Reclamant"),
                                       required=True, select=1),
        'agent_actual': fields.selection(TABLA_RECLAMANTE, _("Agent Actual"),
                                         help=_(u'Agent que té accions pendents necessàries per tancar el cas.'),
                                         required=True, select=1),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipus de reclamació", required=True),
        'subtipus_desc': fields.function(_ff_get_subtype_desc, method=True,
                                        string=u"Descripció subtipus", type='char',
                                        size=128, readonly=True),
        'provincia': fields.many2one('res.country.state', _(u'Província'),
                                     required=True),
        'not_allowed_subtipus': fields.char(u"Subtipus no utilitzats", size=300),
        'cups_id': fields.function(_ff_get_cups_id, method=True, type='many2one',
            string=u"CUPS", relation='giscedata.cups.ps', readonly=True)
    }

    _defaults = {
        'total_cups': lambda *a: 1,
        'reclamante': lambda *a: '01',
        'resultat': lambda *a: False,
        'subtipus_id': _get_default_subtipus,
        'section_id': _get_atc_section,
        'agent_actual': _get_whereiam,
        'time_tracking_id': _get_whereiam_time_tracking,
        'not_allowed_subtipus': lambda *a: str(NOT_ALLOWED_SUBTIPUS),
    }

GiscedataAtc()
