# -*- coding: utf-8 -*-
from osv import osv, fields
from gestionatr.defs import *
from datetime import datetime
import json
from tools.translate import _


class WizardRetipificarATC(osv.osv_memory):

    _name = "wizard.retipificar.atc"

    def retipificar_atc_case(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        wizard = self.browse(cursor, uid, ids[0], context)
        atc_obj = self.pool.get('giscedata.atc')
        casos_ids = []
        retip_str = _(u"Retipificat de {0} a {1}")
        for atc_info in atc_obj.read(cursor, uid, context.get('active_ids', []), ['subtipus_id']):
            atc_id = atc_info['id']
            old_subtipus = atc_info['subtipus_id'] and atc_info['subtipus_id'][1] or False
            vals = {'subtipus_id': wizard.subtipus_id.id}
            atc_obj.write(cursor, uid, atc_id, vals, context)
            casos_ids.append(atc_id)
            if old_subtipus:
                atc_obj.case_log(cursor, uid, atc_id, context=context)
                atc_obj.write(cursor, uid, atc_id, {'description': retip_str.format(old_subtipus, wizard.subtipus_id.name)}, context=context)
            if wizard.comments:
                atc_obj.case_log(cursor, uid, atc_id, context=context)
                atc_obj.write(cursor, uid, atc_id, {'description': wizard.comments}, context=context)
            if old_subtipus or wizard.comments:
                atc_obj.case_log(cursor, uid, atc_id, context=context)

        wizard.write({'state': 'done', 'generated_cases': casos_ids})
        return True

    def action_casos(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_ids = wiz.generated_cases
        return {
            'domain': "[('id','in', {0})]".format(str(casos_ids)),
            'name': _('Casos Retipificats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.atc',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio", u"Nou Tipus", required=True),
        'state': fields.char('State', size=16),
        'generated_cases': fields.json(_('Casos generats')),
        'comments': fields.text("Comentaris")
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardRetipificarATC()
