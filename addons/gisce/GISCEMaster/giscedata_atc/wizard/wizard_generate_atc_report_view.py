# -*- coding: utf-8 -*-
from osv import osv, fields
import StringIO
import csv
import base64
from datetime import datetime
from giscedata_atc.giscedata_atc import get_quarter, NOT_ALLOWED_SUBTIPUS
from tools.translate import _

ATC_REPORT_FIELDS = [
    'comercializadorRemitente',
    'distribuidorRemitente',
    'sector',
    'agenteReclamante',
    'ruta',
    'codigoReclamacion',
    'subtipoReclamacion',
    'cups',
    'totalcups',
    'codigoTarifaPeaje',
    'provincia',
    'codigoComercializador',
    'codigoDistribuidor',
    'necesarioDistribuidor',
    'canal',
    'fechaReclamacion',
    'estadoCerrado',
    'resultado',
    'agenteAccionPendiente',
    'tiempoResolucion',
    'tiempoComercializador',
    'tiempoDistribuidor',
    'tiempoCliente',
    'tiempoOtros',
]


def get_first_quarter_month(trimestre):
    return ((trimestre - 1) * 3) + 1


def get_last_quarter_month(trimestre):
    return ((trimestre - 1) * 3) + 3


class GenerateAtcReport(osv.osv_memory):

    _name = "wizard.generate.atc.report"

    def build_atc_report(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context=context)
        today = datetime.now().strftime('%Y-%m-%d')
        trimestre = wizard.trimestre
        year = wizard.year
        name = "{0}_electricidad_reclamaciones_T{1}{2}.csv".format(today, trimestre, year)
        report = self.build_report_csv(cursor, uid,
                                       int(wizard.trimestre), wizard.year)
        wizard.write({
            'report': base64.b64encode(report),
            'buscat': True,
            'filename_report': name
        })
        return True

    def whereiam(self, cursor, uid):
        cups_obj = self.pool.get('giscedata.cups.ps')
        return cups_obj.whereiam(cursor, uid)

    def obtenir_ref_empresa(self, cursor, uid):
        user_obj = self.pool.get('res.users')
        return user_obj.browse(cursor, uid, uid).company_id.partner_id.ref

    def get_ruta(self, cursor, uid, case_id):
        where = self.whereiam(cursor, uid)
        if where == 'distri':
            atc_obj = self.pool.get("giscedata.atc")
            atc_info = atc_obj.read(cursor, uid, case_id, ["reclamante"])
            if atc_info["reclamante"] == "06":
                return '3'
            else:
                return '2'
        elif where == 'comer':
            return '1'
        return '0'

    def get_CUPS(self, cursor, uid, polissa):
        if polissa.cups:
            return polissa.cups.name
        return "SIN_CUPS"

    def get_tarifa(self, cursor, uid, polissa):
        if polissa.tarifa:
            return polissa.tarifa.codi_ocsum
        return ""

    def get_comer(self, cursor, uid, polissa):
        if polissa:
            return polissa.comercialitzadora.ref
        return ""

    def get_distri(self, cursor, uid, polissa):
        where = self.whereiam(cursor, uid)
        if where == 'comer':
            if polissa:
                return polissa.distribuidora.ref
        else:
            return self.obtenir_ref_empresa(cursor, uid)
        return ""

    def check_necessita_distri(self, cursor, uid, cas_id, atc_id):
        ruta = self.get_ruta(cursor, uid, atc_id)
        if ruta in ['2', '3']:
            return '1'
        log_obj = self.pool.get("crm.case.log")
        log_ids = log_obj.search(cursor, uid,
                                 [('case_id', '=', cas_id),
                                  ('time_tracking_id.code', '=', '1')])
        if log_ids:
            return '1'
        return '0'

    def get_data(self, cursor, uid, date):
        date_object = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        return date_object.strftime("%Y-%m-%d")

    def check_reclamacio_tancada(self, cursor, uid, cas):
        if cas.state != 'done':
            return '0'
        log_obj = self.pool.get("crm.case.log")
        log_id = log_obj.search(cursor, uid, [('case_id', '=', cas.id)],
                                order='date desc', limit=1)
        if log_id:
            last_log = log_obj.browse(cursor, uid, log_id[0])
            data_ini = datetime.strptime(cas.date, '%Y-%m-%d %H:%M:%S')
            data_tanc = datetime.strptime(last_log.date, '%Y-%m-%d %H:%M:%S')
            if data_ini.year == data_tanc.year:
                if get_quarter(data_tanc) - \
                        get_quarter(data_ini) <= 1:
                    return '1'
                return '0'
            elif data_tanc.year - data_ini.year == 1:
                if get_quarter(data_tanc) == 1 and \
                                get_quarter(data_ini) == 4:
                    return '1'
                return '0'
        return '0'

    def calc_temps_total_tracking_id(self, cursor, uid, cas, codi_track):
        track_obj = self.pool.get('crm.time.tracking')
        tr_id = track_obj.search(cursor, uid, [('code', '=', codi_track)])[0]
        lg_obj = self.pool.get('crm.case.log')
        return round(lg_obj.calc_time_same_tracking(cursor, uid, cas.id, tr_id), 1)

    def calc_temps_total_sw_comer(self, cursor, uid, sw_id, context=None):
        r103_obj = self.pool.get("giscedata.switching.r1.03")
        r104_obj = self.pool.get("giscedata.switching.r1.04")
        r105_obj = self.pool.get("giscedata.switching.r1.05")
        header_obj = self.pool.get("giscedata.switching.step.header")

        header_ids = header_obj.search(cursor, uid, [("sw_id", "=", sw_id)])

        # get r103 create date
        r103_id = r103_obj.search(cursor, uid, [('header_id', 'in', header_ids)])
        if not len(r103_id):
            return 0
        r103_info = r103_obj.read(cursor, uid, r103_id[0], ['header_id'])
        r103_header = r103_info['header_id'][0]
        header_inf = header_obj.read(cursor, uid, r103_header, ['date_created'])
        r103_date = datetime.strptime(header_inf['date_created'], '%Y-%m-%d %H:%M:%S')

        # get r104 create date
        r104_id = r104_obj.search(cursor, uid, [('header_id', 'in', header_ids)])
        if not len(r104_id):
            # get r105 create date
            r105_id = r105_obj.search(cursor, uid, [('header_id', 'in', header_ids)])
            if not len(r105_id):
                return 0
            r1_info = r105_obj.read(cursor, uid, r105_id[0], ['header_id'])
        else:
            r1_info = r104_obj.read(cursor, uid, r104_id[0], ['header_id'])
        r1_header = r1_info['header_id'][0]
        header_inf = header_obj.read(cursor, uid, r1_header, ['date_created'])
        r1_date = datetime.strptime(header_inf['date_created'], '%Y-%m-%d %H:%M:%S')

        # calc days between dates
        return round(round((r1_date - r103_date).days, 1), 1)

    def calc_temps_total_sw_distri(self, cursor, uid, sw_id, context=None):
        r101_obj = self.pool.get("giscedata.switching.r1.01")
        r105_obj = self.pool.get("giscedata.switching.r1.05")
        header_obj = self.pool.get("giscedata.switching.step.header")

        header_ids = header_obj.search(cursor, uid, [("sw_id", "=", sw_id)])
        # get r101 create date
        r101_id = r101_obj.search(cursor, uid, [('header_id', 'in', header_ids)])
        if not len(r101_id):
            return 0
        r101_info = r101_obj.read(cursor, uid, r101_id[0], ['header_id'])
        r101_header = r101_info['header_id'][0]
        header_inf = header_obj.read(cursor, uid, r101_header, ['date_created'])
        r101_date = datetime.strptime(header_inf['date_created'], '%Y-%m-%d %H:%M:%S')

        # get r105 create date
        r105_id = r105_obj.search(cursor, uid, [('header_id', 'in', header_ids)])
        if not len(r105_id):
            return 0
        r105_info = r105_obj.read(cursor, uid, r105_id[0], ['header_id'])
        r105_header = r105_info['header_id'][0]
        header_inf = header_obj.read(cursor, uid, r105_header, ['date_created'])
        r105_date = datetime.strptime(header_inf['date_created'], '%Y-%m-%d %H:%M:%S')

        # calc days between dates
        time_comer = self.calc_temps_total_sw_comer(cursor, uid, sw_id)
        return round(round((r105_date - r101_date).days, 1) - time_comer, 1)

    def build_report_csv(self, cursor, uid, trimestre, year):
        output_report = StringIO.StringIO()
        writer_r = csv.writer(output_report, quoting=csv.QUOTE_MINIMAL)
        # write the fields names in the first row
        writer_r.writerow(ATC_REPORT_FIELDS)

        # search all atc cases in 'trimestre' and 'year'
        data_min = "{1}-{0}-01 00:00:00".format(get_first_quarter_month(trimestre), year)
        month = get_last_quarter_month(trimestre)
        if month != 12:
            data_max = "{1}-{0}-01 00:00:00".format(month + 1, year)
        else:
            data_max = "{0}-01-01 00:00:00".format(year + 1)
        atc_obj = self.pool.get('giscedata.atc')
        atc_ids = atc_obj.search(cursor, uid, [('state', 'not in', ('draft', 'cancel')),
                                               ('date', '>=', data_min),
                                               ('date', '<', data_max)])
        for atc_id in atc_ids:
            # write a row for each atc case
            writer_r = self.add_atc_case_csv(cursor, uid, writer_r, atc_id)

        if self.whereiam(cursor, uid) == 'distri':
            sw_obj = self.pool.get('giscedata.switching')
            # if no sw_obj the module giscedata_switching is not installed so
            # we can't have R1 cases
            if sw_obj:
                # Agafar els R1 que tinguin tipo_reclamante la comer
                # i que tinguin un pas 02 ja fet
                sw_ids = sw_obj.search(cursor, uid, [
                    ('proces_id.name', '=', 'R1'),
                    ('create_date', '>=', data_min),
                    ('create_date', '<', data_max),
                    ('step_id.name', '>=', '02'),
                ])
                for sw_id in sw_ids:
                    # Afafem el pas 01 per mirar si el reclamant es Comer
                    r1_obj = self.pool.get('giscedata.switching.r1.01')
                    r1_id = r1_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
                    r1 = r1_obj.browse(cursor, uid, r1_id[0])
                    if r1.tipus_reclamant == '06' \
                            and r1.subtipus_id.name not in NOT_ALLOWED_SUBTIPUS:
                        writer_r = self.add_r1_case(cursor, uid, writer_r,
                                                    sw_id, r1)
        value = output_report.getvalue()
        output_report.close()
        return value

    def add_r1_case(self, cursor, uid, writer_report, sw_id, pas):
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id)
        # If we don't have contract we won't inform this R1 because
        # it will generate an incorrect report
        if not sw.cups_polissa_id:
            return writer_report

        # comercializadorRemitente and distribuidorRemitente
        comercializadorRemitente = ""
        distribuidorRemitente = self.obtenir_ref_empresa(cursor, uid)

        # sector, agenteReclamante, ruta, codigoReclamacion i subtipoReclamacion
        sector = 'E'
        agenteReclamante = '06'  # comer
        ruta = '3'  # comer -> distri
        if agenteReclamante == '06' and ruta != "3":
            return writer_report
        codigoReclamacion = sw.case_id.id
        subtipoReclamacion = str(pas.subtipus_id.name).zfill(3)
        if subtipoReclamacion in ["015", "037", "047", "049"] and ruta != "3":
            return writer_report
        if not pas.subtipus_id.circular or pas.subtipus_id.sector == "g":
            return writer_report

        # cups and totalCUPS
        cups = self.get_CUPS(cursor, uid, sw.cups_polissa_id)
        totalcups = 1

        # codigoTarifaPeaje and provincia
        codigoTarifaPeaje = self.get_tarifa(cursor, uid, sw.cups_polissa_id)
        provincia = sw.cups_id.id_provincia.code

        # codigoComercializador, codigoDistribuidor and necesarioDistribuidor
        codigoComercializador = self.get_comer(cursor, uid, sw.cups_polissa_id)
        codigoDistribuidor = self.get_distri(cursor, uid, sw.cups_polissa_id)
        necesarioDistribuidor = 1

        # canal, fechaReclamacion
        canal = '05'  # ficheros intercambio
        fechaReclamacion = self.get_data(cursor, uid, sw.create_date)
        if canal == "05" and ruta in ["1", "2"]:
            return writer_report

        # estadoCerrado, resultado and agenteAccionPendiente
        estadoCerrado = self.check_reclamacio_tancada(cursor, uid, sw.case_id)
        resultado = ""
        agenteAccionPendiente = ""
        if estadoCerrado == '1':
            if sw.rebuig:  # No gestionable perque hi ha rebuig pas 02
                resultado = '03'
            else:  # Altrament mirar resultat pas 05
                # Si el cas està tancat i no és rebuig, el pas serà 05
                pas05 = sw.get_pas()
                resultado = pas05.resultat
        else:
            if sw.step_id.name in ['03']:
                agenteAccionPendiente = '06'  # Comer
            else:
                agenteAccionPendiente = '10'  # Distri

        # tiempoResolucion, tiempoComercializador, tiempoDistribuidor,
        # tiempoCliente and tiempoOtros
        tResolucion = ""
        tComercializador = ""
        tDistribuidor = ""
        tCliente = ""
        tOtros = ""
        if estadoCerrado == '1':
            tComercializador = self.calc_temps_total_sw_comer(
                cursor, uid, sw.id)
            tDistribuidor = self.calc_temps_total_sw_distri(
                cursor, uid, sw.id)
            tCliente = 0
            tOtros = 0
            tResolucion = round(tComercializador + tDistribuidor + tCliente + tOtros, 1)

        writer_report.writerow([
            comercializadorRemitente,
            distribuidorRemitente,
            sector,
            agenteReclamante,
            ruta,
            codigoReclamacion,
            subtipoReclamacion,
            cups,
            totalcups,
            codigoTarifaPeaje,
            provincia,
            codigoComercializador,
            codigoDistribuidor,
            necesarioDistribuidor,
            canal,
            fechaReclamacion,
            estadoCerrado,
            resultado,
            agenteAccionPendiente,
            tResolucion,
            tComercializador,
            tDistribuidor,
            tCliente,
            tOtros
        ])
        return writer_report

    def add_atc_case_csv(self, cursor, uid, writer_report, atc_id):
        atc_obj = self.pool.get('giscedata.atc')
        atc_case = atc_obj.browse(cursor, uid, atc_id)

        # comercializadorRemitente and distribuidorRemitente
        comercializadorRemitente = ""
        distribuidorRemitente = ""
        if self.whereiam(cursor, uid) == "distri":
            distribuidorRemitente = self.obtenir_ref_empresa(cursor, uid)
        else:
            comercializadorRemitente = self.obtenir_ref_empresa(cursor, uid)

        # sector, agenteReclamante, ruta, codigoReclamacion i subtipoReclamacion
        sector = 'E'
        agenteReclamante = atc_case.reclamante
        ruta = self.get_ruta(cursor, uid, atc_id)
        if agenteReclamante == '06' and ruta != "3":
            return writer_report
        codigoReclamacion = atc_case.id
        subtipoReclamacion = atc_case.subtipus_id.name
        if not atc_case.subtipus_id.circular or atc_case.subtipus_id.sector == "g":
            return writer_report
        if subtipoReclamacion in ["015", "037", "047", "049"] and ruta != "3":
            return writer_report

        # cups and totalCUPS
        cups = self.get_CUPS(cursor, uid, atc_case.polissa_id)
        totalcups = atc_case.total_cups

        # codigoTarifaPeaje and provincia
        codigoTarifaPeaje = self.get_tarifa(cursor, uid, atc_case.polissa_id)
        provincia = atc_case.provincia.code

        # codigoComercializador, codigoDistribuidor and necesarioDistribuidor
        codigoComercializador = self.get_comer(cursor, uid, atc_case.polissa_id)
        codigoDistribuidor = self.get_distri(cursor, uid, atc_case.polissa_id)
        necesarioDistribuidor = self.check_necessita_distri(
            cursor, uid, atc_case.crm_id.id, atc_case.id)

        # canal, fechaReclamacion
        canal = atc_case.canal_id.cnmc_code
        if canal == "05" and ruta in ["1", "2"]:
            return writer_report
        fechaReclamacion = self.get_data(cursor, uid, atc_case.date)

        # estadoCerrado, resultado and agenteAccionPendiente
        estadoCerrado = self.check_reclamacio_tancada(cursor, uid,
                                                      atc_case.crm_id)
        resultado = ""
        agenteAccionPendiente = ""
        if estadoCerrado == '1':
            resultado = atc_case.resultat
        if estadoCerrado == '0':
            agenteAccionPendiente = atc_case.agent_actual

        # tiempoResolucion, tiempoComercializador, tiempoDistribuidor,
        # tiempoCliente and tiempoOtros
        tResolucion = ""
        tComercializador = ""
        tDistribuidor = ""
        tCliente = ""
        tOtros = ""
        if estadoCerrado == '1':
            tComercializador = self.calc_temps_total_tracking_id(
                cursor, uid, atc_case.crm_id, '0')
            tDistribuidor = self.calc_temps_total_tracking_id(
                cursor, uid, atc_case.crm_id, '1')
            tCliente = self.calc_temps_total_tracking_id(
                cursor, uid, atc_case.crm_id, '2')
            tOtros = self.calc_temps_total_tracking_id(
                cursor, uid, atc_case.crm_id, '3')
            tResolucion = round(tComercializador + tDistribuidor + tCliente + tOtros, 1)

        writer_report.writerow([
            comercializadorRemitente,
            distribuidorRemitente,
            sector,
            agenteReclamante,
            ruta,
            codigoReclamacion,
            subtipoReclamacion,
            cups,
            totalcups,
            codigoTarifaPeaje,
            provincia,
            codigoComercializador,
            codigoDistribuidor,
            necesarioDistribuidor,
            canal,
            fechaReclamacion,
            estadoCerrado,
            resultado,
            agenteAccionPendiente,
            tResolucion,
            tComercializador,
            tDistribuidor,
            tCliente,
            tOtros
        ])
        return writer_report

    _columns = {
        'trimestre': fields.selection(
            [('1', '1'), ('2', '2'), ('3', '3'), ('4', '4')],
            required=True, string=_("Trimestre")),
        'year': fields.integer(size=4, required=True, string=_("Any")),
        'report': fields.binary(_('Resultat')),
        'buscat': fields.boolean(),
        'filename_report': fields.char(_('Nom fitxer exportat'), size=256),
    }

    _defaults = {
        'buscat': lambda *a: False,
        'year': lambda *a: datetime.now().year,
        'trimestre': lambda *a: str(get_quarter(datetime.now()))
    }

GenerateAtcReport()
