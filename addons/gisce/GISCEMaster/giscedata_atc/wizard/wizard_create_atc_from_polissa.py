# -*- coding: utf-8 -*-
from osv import osv, fields
from gestionatr.defs import *
from datetime import datetime
import json
from tools.translate import _


class WizardCreateAtc(osv.osv_memory):

    _name = "wizard.create.atc.from.polissa"

    def create_atc_case(self, cursor, uid, ids, context=None):
        ''' Generates the cases from invoices'''
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        atc_obj = self.pool.get('giscedata.atc')
        polissa_obj = self.pool.get("giscedata.polissa")
        partner_obj = self.pool.get('res.partner')
        adres_obj = self.pool.get('res.partner.address')

        wizard_date = datetime.strptime(wizard.date, "%Y-%m-%d %H:%M:%S")
        avui = datetime.today()
        if wizard_date > avui:
            raise osv.except_osv(_(u"Error Usuari"), _(u"No es poden crear casos de atenció al client amb una data a futur!"))

        casos_ids = []
        for polissa_id in context.get('active_ids', []):
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            partner_id = polissa.titular.id
            addr = partner_obj.address_get(cursor, uid, [partner_id], ['contact'])
            email = ''
            if addr['contact']:
                email = adres_obj.read(cursor, uid, addr['contact'], ['email'])
                email = email['email']

            vals = {
                'name': wizard.name,
                'date': wizard.date,
                'canal_id': wizard.canal_id.id,
                'subtipus_id': wizard.subtipus_id.id,
                'polissa_id': polissa.id,
                'partner_id': partner_id,
                'provincia': polissa.cups.id_provincia.id,
                'partner_address_id': addr['contact'] or False,
                'email_from': email,
                'description': wizard.comments
            }
            if wizard.section_id:
                vals.update({
                    'section_id': wizard.section_id.id,
                })
            if wizard.canal_id.cnmc_code == '05':
                vals.update({
                    'reclamante': '06',
                })
                
            atc_id = atc_obj.create(cursor, uid, vals, context)

            if wizard.no_responsible:
                atc_obj.write(cursor, uid, atc_id, {'user_id': False})

            atc_obj.case_log(cursor, uid, [atc_id], context=context)
            if wizard.open_case:
                atc_obj.case_open(cursor, uid, [atc_id])

            casos_ids.append(atc_id)

            # contabilitzem aquest temps a la nostre banda
            time_spend = (avui-wizard_date).days
            lcreated = atc_obj.add_time_tracked(cursor, uid, atc_id, time_spend, context=context)

        wizard.write({'state': 'done', 'generated_cases': casos_ids})
        return True

    def action_casos(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_ids = wiz.generated_cases
        return {
            'domain': "[('id','in', {0})]".format(str(casos_ids)),
            'name': _('Casos CAC creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.atc',
            'type': 'ir.actions.act_window'
        }

    def _get_default_chanel(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        sec_id = imd_obj.get_object_reference(
            cursor, uid, 'l10n_ES_partner_data', 'canal_directo')
        return sec_id[1]

    def _generate_default_name(self, cursor, uid, context=None):
        polissa_id = context.get('active_id', None)
        if polissa_id is None:
            return ''
        polissa_obj = self.pool.get("giscedata.polissa")
        polissa = polissa_obj.browse(cursor, uid, polissa_id)

        partner_name = polissa.titular.name
        partner_code = polissa.titular.vat
        if partner_code:
            partner_code = "({0})".format(partner_code)
        else:
            partner_code = ''
        msg = _(u"Atenció al client {0} {1}".format(partner_name, partner_code))
        return msg

    def onchange_subtipus(self, cursor, uid, ids, subtipus_id):
        res = False

        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        seccio = subtipus_obj.read(cursor, uid, subtipus_id, ['default_section'])['default_section']
        if seccio:
            res = seccio[0]
        return {'value': {'section_id': res},
                'domain': {},
                'warning': {}
                }

    def _ff_get_canal_code(self, cursor, uid, ids, field_name, arg, context):
        res = {}
        canal_obj = self.pool.get('res.partner.canal')
        for canal_info in self.read(cursor, uid, ids, ['canal_id'], context=context):
            res[canal_info['id']] = False
            if canal_info['canal_id']:
                canal_code = canal_obj.read(cursor, uid, canal_info['canal_id'], ['cnmc_code'])['cnmc_code']
                if canal_code:
                    res[canal_info['id']] = canal_code
        return res

    _columns = {
        'name': fields.char(_(u'Descripció'), size=128, required=True),
        'date': fields.datetime(_('Data'), required=True),
        'canal_id': fields.many2one('res.partner.canal', 'Canal',
                                    required=True),
        'canal_code': fields.function(_ff_get_canal_code, method=True,
                                        string=u"Codi del canal", type='char',
                                        size=2, readonly=True),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                        u"Tipus", required=True),
        'section_id': fields.many2one('crm.case.section', 'Secció', required=True),
        'state': fields.char('State', size=16),
        'generated_cases': fields.json(_('Casos generats')),
        'comments': fields.text("Comentaris"),
        'open_case': fields.boolean(u'Passar a "obert" els casos generats'),
        'no_responsible': fields.boolean(u'Marcar per no assignar responsable')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'date': lambda *a: datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
        'canal_id': _get_default_chanel,
        'name': _generate_default_name,
    }

WizardCreateAtc()
