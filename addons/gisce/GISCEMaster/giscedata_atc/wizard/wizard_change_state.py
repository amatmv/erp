# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
from crm.crm import AVAILABLE_STATES
from giscedata_atc.giscedata_atc import TABLA_RECLAMANTE, TABLA_RESULTADOS
from osv.osv import except_osv


class WizardAtcMultiChange(osv.osv_memory):
    _name = 'wizard.change.state.atc'

    def onchange_tracking_id(self, cursor, uid, ids, time_tracking_id):
        """Al canviar 'time_tracking_id' s'actualitza 'agent_actual'"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tracking_obj = self.pool.get("crm.time.tracking")
        tracking = tracking_obj.read(cursor, uid, time_tracking_id, ['code'])
        if tracking['code'] == '0':
            data = {'agent_actual': '06'}  # Comer
        elif tracking['code'] == '1':
            data = {'agent_actual': '10'}  # Distri
        elif tracking['code'] == '2':
            data = {'agent_actual': '01'}  # Client
        else:
            data = {'agent_actual': '02'}  # Other
        res['value'].update(data)
        return res

    def onchange_agent_actual(self, cursor, uid, ids, agent_actual):
        """Al canviar 'agent_actual' s'actualitza 'time_tracking_id'"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tracking_obj = self.pool.get("crm.time.tracking")
        if agent_actual == '06':  # Comer
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '0')])
        elif agent_actual == '10':  # Distri
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '1')])
        elif agent_actual == '01':  # Titular
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '2')])
        else:  # Others
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '3')])
        res['value'].update({'time_tracking_id': tracking[0]})
        return res

    def perform_change(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        case_ids = context.get('active_ids', [])
        case_obj = self.pool.get("giscedata.atc")
        wizard = self.browse(cursor, uid, ids[0])
        new_state = wizard.new_state
        old_responsibles = case_obj.read(cursor, uid, case_ids, ['user_id'])

        if new_state == 'draft':
            new_state = 'reset'
        elif new_state == 'done':
            new_state = 'close'

        change_state = getattr(case_obj, "case_{0}".format(new_state))
        change_state(cursor, uid, case_ids, context)

        case_obj.write(cursor, uid, case_ids, {
            'time_tracking_id': wizard.time_tracking_id.id,
            'agent_actual': wizard.agent_actual,
            'resultat': wizard.resultat
        })

        for user in old_responsibles:
            if not user['user_id']:
                case_obj.write(cursor, uid, user['id'], {'user_id': False})
            else:
                case_obj.write(cursor, uid, user['id'], {'user_id': user['user_id'][0]})
        return {}

    _columns = {
        'new_state': fields.selection(
            selection=AVAILABLE_STATES,
            string='Estat Nou', required=True
        ),
        'time_tracking_id': fields.many2one(
            'crm.time.tracking', _(u"Imputació de temps"),
            help=_(u"Agent a qui se l'hi imputarà el temps gastat en el nou "
                   u"estat.")
        ),
        'agent_actual': fields.selection(
            TABLA_RECLAMANTE, _("Agent Actual"),
            help=_(u'Agent que té accions pendents necessàries per tancar el '
                   u'cas.'),
            required=True
        ),
        'resultat': fields.selection(TABLA_RESULTADOS, _("Resultat")),
    }

    _defaults = {
        'resultat': lambda *a: False,
    }

WizardAtcMultiChange()