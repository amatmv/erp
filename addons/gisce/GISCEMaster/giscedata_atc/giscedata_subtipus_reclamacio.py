# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataSwitchingSubtipus(osv.osv):
    ''' Tabla 82 From CNMC Gestión ATR documentation'''

    _name = 'giscedata.subtipus.reclamacio'
    _inherit = 'giscedata.subtipus.reclamacio'

    def _get_default_section(self, cursor, uid, context=None):
        section_id = self.pool.get("crm.case.section").search(
            cursor, uid, [('code', '=', 'ATC')]
        )
        if len(section_id):
            return section_id[0]
        return None

    _columns = {
        'default_section': fields.many2one('crm.case.section', 'Secció per defecte')
    }

    _defaults = {
        'default_section': _get_default_section,
    }


GiscedataSwitchingSubtipus()
