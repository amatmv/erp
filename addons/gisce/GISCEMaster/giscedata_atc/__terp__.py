# -*- coding: utf-8 -*-
{
    "name": "GISCE Atenció al Client",
    "description": """Atenció al client en reclamacions.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "crm",
        "giscedata_subtipus_reclamacio",
        "giscedata_polissa_crm",
        "crm_timetracking",
        "l10n_ES_partner_data"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_atc_data.xml",
        "giscedata_subtipus_reclamacio_view.xml",
        "wizard/wizard_generate_atc_report_view.xml",
        "wizard/wizard_create_atc_from_polissa.xml",
        "wizard/wizard_change_state_view.xml",
        "wizard/wizard_retipificar_atc.xml",
        "giscedata_atc_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
