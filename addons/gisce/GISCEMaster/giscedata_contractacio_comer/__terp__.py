# -*- coding: utf-8 -*-
{
    "name": "Contractació comercialitzadora",
    "description": """
Contractació de comercialitzadora:
    * Invoicing journal CONCEPTOS
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "Facturacio",
    "depends": [
        "giscedata_facturacio_comer",
        "giscedata_facturacio_switching",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_contractacio_comer_sequence.xml",
        "giscedata_contractacio_comer_data.xml",
    ],
    "active": False,
    "installable": True
}
