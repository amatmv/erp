# -*- coding: utf-8 -*-

from osv import osv


class GiscedataFacturacioSwitchingHelper(osv.osv):

    _name = 'giscedata.facturacio.switching.helper'
    _inherit = 'giscedata.facturacio.switching.helper'

    def get_extra_line_journals(self, cursor, uid, context=None):

        res = super(GiscedataFacturacioSwitchingHelper, self).get_extra_line_journals(
            cursor, uid, context
        )
        imd_obj = self.pool.get('ir.model.data')
        contractacio_journal_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_contractacio_comer',
            'journal_contractacio_distri'
        )[1]
        res.append(contractacio_journal_id)

        return res


GiscedataFacturacioSwitchingHelper()
