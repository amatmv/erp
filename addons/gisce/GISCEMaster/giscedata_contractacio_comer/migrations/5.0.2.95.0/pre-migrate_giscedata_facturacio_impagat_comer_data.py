# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')
    logger.info('''
        Updating module\'s contractacio journal models that are defined in
        giscedata_facturacio_impagat_comer and are related with
        giscedata_contractacio_comer
    ''')
    cursor.execute("""
        UPDATE ir_model_data
        SET module = 'giscedata_contractacio_comer'
        WHERE module = 'giscedata_facturacio_impagat_comer'
        AND name IN (
            'journal_contractacio_distri',
            'journal_contractacio_distri_a',
            'journal_contractacio_distri_b',
            'journal_contractacio_distri_r',
            'seq_type_contractacio_distri',
            'seq_contractacio_distri',
            'seq_type_contractacio_distri_ab',
            'seq_contractacio_distri_ab',
            'seq_type_contractacio_distri_r',
            'seq_contractacio_distri_r'
        )
    """)
    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass

migrate = up