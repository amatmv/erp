# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def configurar_contracte_ov(self, cursor, uid, ids, context=None):
        hr_employee_obj = self.pool.get('hr.employee')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for pid in ids:
            codi_promocional = self.read(cursor, uid, pid, ['codigo_promocional'])['codigo_promocional']
            if codi_promocional:
                search_params = [('codi_ov', '=', codi_promocional)]
                empleat = hr_employee_obj.search(cursor, uid, search_params, context)
                if empleat:
                    self.write(cursor, uid, pid, {'comercial_id': empleat[0]})
                    info = hr_employee_obj.read(cursor, uid, empleat[0], ['user_id'])
                    if info['user_id']:
                        self.write(cursor, uid, pid, {'user_id': info['user_id'][0]})
        return super(GiscedataPolissa, self).configurar_contracte_ov(
            cursor, uid, ids, context=context
        )

    _columns = {
        'codigo_promocional': fields.char('Codi promocional', size=20),
    }


GiscedataPolissa()
