# -*- coding: utf-8 -*-
from osv import osv, fields


class HrEmployee(osv.osv):
    _name = 'hr.employee'
    _inherit = 'hr.employee'

    def actualitzar_contractes_ov_all_comercials(self, cursor, uid, ids, context=None):
        cids = self.search(cursor, uid, [])
        self.actualitzar_contractes_ov_asignats(cursor, uid, cids, context=context)
        return True

    def actualitzar_contractes_ov_asignats(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        ctx = context.copy()
        ctx['overwrite_allowed_contracts_ids'] = True

        updates_ids = []
        for emp_info in self.read(cursor, uid, ids, ['ov_user']):
            emp_id = emp_info['id']
            ov_id = emp_info['ov_user']
            if ov_id:
                contractes_ids = self.get_asigned_contracts_to_comercial(cursor, uid, emp_id, context=ctx)
                self.pool.get('ov.users').asign_allowed_contracts_ids(cursor, uid, ov_id[0], contractes_ids, context=ctx)
        return updates_ids

    _columns = {
        'codi_ov': fields.char('Codi Comercial', size=20),
        'ov_user': fields.many2one('ov.users', "Usuari de Oficina Virtual")
    }


HrEmployee()
