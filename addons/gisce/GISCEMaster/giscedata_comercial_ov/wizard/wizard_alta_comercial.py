# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from tools.translate import _
from osv import osv, fields
import base64


class WizardAltaComercial(osv.osv_memory):

    _inherit = 'wizard.alta.comercial'

    def accions_adicionals(self, cursor, uid, wizard, part_id, address_id, user_id, pw_id, emp_id, context=None):
        res = super(WizardAltaComercial, self).accions_adicionals(cursor, uid, wizard, part_id, address_id, user_id, pw_id, emp_id, context=context)
        if wizard.create_ov_user:
            ov_id = self.create_ov_user(cursor, uid, part_id, emp_id, wizard, context=context)
            res += _(" * S'ha creat l'usuari de oficina virtual '{0}' i s'ha enviat un email a '{1}' amb els detalls.\n").format(wizard.ov_username, wizard.ov_email)
        return res

    def create_ov_user(self, cursor, uid, partner_id, emp_id, wizard, context=None):
        if context is None:
            context = {}

        partner_o = self.pool.get("res.partner")
        emp_o = self.pool.get('hr.employee')

        for field in ["ov_username", "ov_password", "ov_email"]:
            if not wizard[field]:
                raise osv.except_osv(_(u"Error Usuari"), _(u"Falta emplenar el camp {}").format(field))

        ov_id = partner_o.enable_ov_user(cursor, uid, [partner_id], wizard.ov_username, wizard.ov_email, password=wizard.ov_password, context=context)

        ctx = context.copy()
        self.pool.get('ov.users').write(cursor, uid, [ov_id], {'allowed_contracts_mode': 'restricted'}, context=ctx)

        emp_o.write(cursor, uid, emp_id, {'ov_user': ov_id})
        emp_o.actualitzar_contractes_ov_asignats(cursor, uid, emp_id, context=context)

        return ov_id

    def action_pantalla_adreca(self, cursor, uid, ids, context=None):
        super(WizardAltaComercial, self).action_pantalla_adreca(cursor, uid, ids, context)
        wizard = self.browse(cursor, uid, ids[0], context=context)
        if wizard.create_ov_user:
            wizard.write({
                'state': 'crear_ov'
            })

    def action_pantalla_partner(self, cursor, uid, ids, context=None):
        super(WizardAltaComercial, self).action_pantalla_partner(cursor, uid, ids, context)
        wizard = self.browse(cursor, uid, ids[0], context=context)
        if wizard.create_ov_user and wizard.state == 'crear_usuari':
            wizard.write({
                'state': 'crear_ov'
            })

    def action_pantalla_ant_ov(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({
            'state': 'crear_adreca'
        })

    def action_pantalla_seg_ov(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({
            'state': 'crear_usuari'
        })

    def _estats_disponibles(self, cursor, uid, context=None):
        res = super(WizardAltaComercial, self)._estats_disponibles(cursor, uid, context)
        res.append(('crear_ov', "Creacio Usuari OV"))
        return res

    _columns = {
        'state': fields.selection(_estats_disponibles, "Estat"),
        # Dades Wizard
        'create_ov_user': fields.boolean("Pot entrar a la Oficina Virtual (crear usuari de ov?)"),
        'ov_username': fields.char('Login OV', size=64),
        'ov_password': fields.char('Password OV', size=64),
        'ov_email': fields.char('Email OV', size=64),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardAltaComercial()
