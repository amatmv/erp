# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from tools.translate import _
from osv import osv, fields
import base64


class WizardActualitzarContractesOv(osv.osv_memory):

    _name = 'wizard.actualitzar.contractes.ov'

    def action_actualitzar_contractes_ov(self, cursor, uid, ids=None, context=None):
        if context is None:
            context = {}

        if context.get("from_comercials"):
            cids = context.get("from_comercials", [])
        else:
            cids = self.pool.get('hr.employee').search(cursor, uid, [])

        wizard = self.browse(cursor, uid, ids[0], context=context)
        try:
            self.pool.get('hr.employee').actualitzar_contractes_ov_asignats(cursor, uid, cids, context=context)
            wizard.write({
                'state': "end",
            })
        except osv.except_osv, e:
            raise e
        except Exception, e:
            raise osv.except_osv(_(u"Atenció!"), _(u"Hi ha hagut algun problema durant el procés. Missatge d'error:\n\n{0}").format(e.message))

    _columns = {
        'state': fields.char("Estat", size=16),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }


WizardActualitzarContractesOv()
