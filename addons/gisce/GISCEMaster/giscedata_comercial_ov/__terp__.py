# -*- coding: utf-8 -*-
{
    "name": "Módul Comercial Oficina Virtual",
    "description": """
    Módul complementari per a la gestió de comercials. Crea la relació entre els
    comercials i el codi promocional de l'Oficina Virtual
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_comercial",
        "giscedata_ov_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        'giscedata_polissa_view.xml',
        'wizard/wizard_alta_comercial_view.xml',
        'wizard/wizard_actualitzar_contractes_ov_view.xml',
        'hr_employee_view.xml',
        'giscedata_comercial_data.xml',
    ],
    "active": False,
    "installable": True
}
