from base_index.base_index import BaseIndex


class GiscedataAtTram(BaseIndex):
    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    _index_fields = {
        'circuit.name': True,
    }


GiscedataAtTram()