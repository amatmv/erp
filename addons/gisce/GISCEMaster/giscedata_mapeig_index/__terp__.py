# -*- coding: utf-8 -*-
{
    "name": "Index Mapeig",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Camp circuit a index giscedata_at
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_at_index",
        "giscedata_cts_index",
        "giscedata_mapeig"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
