# -*- coding: utf-8 -*-
from base_index.base_index import BaseIndex


class GiscedataCts(BaseIndex):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _index_fields = {
        'circuit.name': True,
    }


GiscedataCts()