SELECT
	cups.id as id,
    polissa.id as polissa_id,
    cups.name as cups,
    cups.direccio as direccion_del_suministro,
    municipi.name AS poblacion_del_suministro,
    coalesce(cups.dp, '00000') as codigo_postal_del_suministro,
    provincia.name as provincia_del_suministro,
    distribuidora.name as empresa_distribuidora,
    companyia.codi_r1 as codigo_empresa_distribuidora,
    titular.vat as documento_identidad,
    CASE
        WHEN substring(titular.vat,1,2) = 'ES' and substring(titular.vat,3,1) in ('0','1','2','3','4','5','6','7','8','9')
          THEN 'DNI'
        WHEN substring(titular.vat,1,2) = 'ES' and substring(titular.vat,3,1) in ('X','Y')
        	THEN 'NIE'
        WHEN substring(titular.vat,1,2) = 'PS'
        	THEN 'PASAPORTE'
        ELSE 'CIF'
    END AS tipo_documento_identidad,
    CASE
        WHEN substring(titular.vat,1,2) = 'ES' and substring(titular.vat,3,1) in ('0','1','2','3','4','5','6','7','8','9','X','Y')
        THEN 'Persona Física'
        WHEN substring(titular.vat,1,2) <> 'ES'
        THEN 'Persona Física'
        ELSE 'Persona Jurídica'
    END AS identificador,
    titular.name as nombre_y_apellidos_denominacion_social,
    titular_address.street as domicilio_del_titular,
    titular_address.city as poblacion_del_titular,
    titular_address.zip as codigo_postal_del_titular,
    titular_provincia.name as provincia_del_titular,
    CASE
        WHEN (substring(titular.vat,1,2) = 'ES' and substring(titular.vat,3,1) in ('0','1','2','3','4','5','6','7','8','9','X','Y'))
              or substring(titular.vat,1,2) <> 'ES'
           THEN
              CASE
                 WHEN polissa.tipus_vivenda = 'habitual'
                   THEN 'Vivienda habitual'
                 WHEN polissa.tipus_vivenda = 'no_habitual'
                   THEN 'No vivienda habitual'
              END
           ELSE
               ''
        END as tipo_vivienda,
            CASE
              WHEN polissa.state = 'impagament'
              THEN 'Baja por impago'
              ELSE 'Al corriente de pago'
            END as impagos,
    'no disponible' as deposito_de_garantia,
    'Si' as autorizacion_cesion_de_datos,
    cups_alta.data_alta as fecha_alta,
    tarifa.name as tarifa,
    coalesce(tarifa.cof, '') as tipo_coeficiente,
    polissa.tensio AS tension,
    coalesce(butlleti.pot_max_admisible, 0) AS potencia_maxima_autorizada,
    coalesce(butlleti.pot_installada, 0) AS potencia_maxima_autorizada_acta_puesta_en_marcha,
    polissa.agree_tipus::int as tipo_de_punto_de_medida,
    CASE
             WHEN comptador.tg_icp_active
             THEN 'ICP instalado' 
	 ELSE 
                CASE
                  WHEN comptador_product.default_code
                       SIMILAR TO  '%ALQ1(1|2|3)%'
                  THEN 'ICP instalado'
		ELSE 'ICP no instalado'
	 END
            END AS icp_instalado,
            'P' || substring(tarifa.cof, '.$') AS tipo_perfil_de_consumo,
    CASE
      WHEN polissa.potencia <= 15
      THEN
        CASE
          WHEN tarifa.name LIKE '%DH%'
          THEN 'Si'
          ELSE 'No'
          END
	ELSE 'DH' || substring(polissa.agree_dh,2,1)::varchar
    END AS discriminacion_horaria,
    polissa.potencia AS derechos_de_acceso_reconocidos,
    coalesce(butlleti.pot_max_admisible,0) AS derechos_de_extension_reconocidos,
    CASE
      WHEN comptador.lloguer
      THEN 'Empresa distribuidora'
      ELSE 'Titular del punto de suministro'
    END AS propiedad_equipo_medida,
    CASE
    	WHEN comptador.tg_icp_active and comptador.lloguer
    		THEN 'Empresa distribuidora'
    	WHEN comptador.tg_icp_active and not comptador.lloguer
    		THEN 'Titular del punto de suministro'
    ELSE ''
        END AS propiedad_del_icp,
            substring(modcontractual.potencies_periode,
                      'P1: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_1,
            substring(modcontractual.potencies_periode,
                      'P2: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_2,
            substring(modcontractual.potencies_periode,
                      'P3: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_3,
            substring(modcontractual.potencies_periode,
                      'P4: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_4,
            substring(modcontractual.potencies_periode,
                      'P5: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_5,
            substring(modcontractual.potencies_periode,
                      'P6: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_6,
            substring(modcontractual.potencies_periode,
                      'P7: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_7,
            substring(modcontractual.potencies_periode,
                      'P8: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_8,
            substring(modcontractual.potencies_periode,
                      'P9: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_9,
            substring(modcontractual.potencies_periode,
                      'P10: ([0-9.]*)')::numeric(16, 3) as potencia_periodo_10,
            modcontractual.data_inici AS fecha_ultimo_movimiento_de_contratacion,
            canvi_comer.data_canvi AS fecha_ultimo_cambio_comercializador,
            butlleti.data_vigencia AS fecha_limite_derechos_de_extension,
            polissa.data_ultima_lectura AS fecha_ultima_lectura
FROM
    giscedata_cups_ps cups
LEFT JOIN res_municipi municipi
ON (cups.id_municipi = municipi.id)
LEFT JOIN res_country_state provincia
ON (municipi.state = provincia.id)
LEFT JOIN res_partner distribuidora
ON (cups.distribuidora_id = distribuidora.id)
LEFT JOIN res_company companyia
ON (companyia.partner_id = distribuidora.id)
LEFT JOIN giscedata_polissa polissa
ON (polissa.cups = cups.id
    and polissa.state not in ('cancelada','validar', 'baixa', 'esborrany'))
LEFT JOIN res_partner titular ON (polissa.titular = titular.id)
LEFT JOIN (
	select p.id,COALESCE(
        (SELECT MAX(id) FROM res_partner_address
        WHERE partner_id=p.id AND type='contact' AND p.active),
        (SELECT MAX(id) FROM res_partner_address
        WHERE partner_id=p.id AND type='default' AND p.active),
        (SELECT id FROM res_partner_address
        WHERE partner_id=p.id
        AND type IS NULL AND p.active LIMIT 1)) AS dir
	from res_partner p
) AS direccion_titular ON (titular.id=direccion_titular.id)
LEFT JOIN res_partner_address titular_address
ON (titular_address.id = direccion_titular.dir)
LEFT JOIN res_country_state titular_provincia
ON (titular_provincia.id = titular_address.state_id)
LEFT JOIN giscedata_polissa_tarifa tarifa
ON (polissa.tarifa = tarifa.id)
LEFT JOIN (
	SELECT p.cups as id,MIN(data_alta) as data_alta
	FROM giscedata_polissa p
	GROUP BY p.cups
) AS cups_alta ON (cups_alta.id=cups.id)
LEFT JOIN (
  SELECT
    polissa_id,
    MAX(data) AS data
  FROM giscedata_butlleti
  GROUP BY polissa_id
) AS butlleti_data
ON (butlleti_data.polissa_id = polissa.id)
LEFT JOIN giscedata_butlleti AS butlleti
ON (butlleti.polissa_id = polissa.id) AND butlleti.data=butlleti_data.data
LEFT JOIN giscedata_polissa_icp AS icp
ON (icp.polissa_id = polissa.id)
LEFT JOIN giscedata_lectures_comptador comptador
ON (comptador.polissa = polissa.id and comptador.active
    and comptador.data_baixa is null)
LEFT JOIN giscedata_polissa_modcontractual modcontractual
ON (modcontractual.id = polissa.modcontractual_activa)
LEFT JOIN (SELECT m1.polissa_id,
       max(m1.data_inici) as data_canvi,
       max(m1.comercialitzadora) as comer_nova,
       max(m2.comercialitzadora) as comer_vella
	   FROM giscedata_polissa_modcontractual m1
       INNER JOIN giscedata_polissa_modcontractual m2
       ON (m1.modcontractual_ant = m2.id
           and m1.comercialitzadora != m2.comercialitzadora)
       GROUP BY m1.polissa_id
) AS canvi_comer ON (canvi_comer.polissa_id = polissa.id)
LEFT JOIN product_product comptador_product
ON (comptador.product_lloguer_id = comptador_product.id)
WHERE cups.active
and polissa.state not in ('esborrany', 'cancelada', 'validar', 'baixa')
and (not titular.lopd_active and polissa.state != 'impagament')
ORDER BY cups.name
