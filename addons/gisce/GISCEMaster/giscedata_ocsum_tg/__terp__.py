# -*- coding: utf-8 -*-
{
    "name": "OCSUM",
    "description": """
    This module provide :
        * Exportació CSV per la OCSUM adaptat a telegestió
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_ocsum",
        "giscedata_lectures_telegestio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
