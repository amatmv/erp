# -*- coding: utf-8 -*-
from osv import osv, fields

class GiscedataOcsumCUPS(osv.osv):

    _name = 'giscedata.ocsum.cups'
    _inherit = 'giscedata.ocsum.cups'

    _module = 'giscedata_ocsum_tg'

GiscedataOcsumCUPS()