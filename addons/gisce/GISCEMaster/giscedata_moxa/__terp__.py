# -*- coding: utf-8 -*-
{
    "name": "Moxa model",
    "description": """
    Moxa model
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "infraestructura",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "moxa_view.xml",
        "security/moxa_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
