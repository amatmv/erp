from osv import fields, osv


class GiscedataMoxa(osv.osv):

    _name = 'giscedata.moxa'

    _columns = {
        'name': fields.char('Name', size=100, required=True),
        'manufacturer': fields.char('Manufacturer', size=50),
        'model': fields.char('Model', size=50),
        'serial_number': fields.char('Serial Number', size=100),
        'ip_address': fields.char('IP Address', size=15, required=True),
        'port': fields.char('Port', size=5, required=True),
    }


GiscedataMoxa()