# -*- coding: utf-8 -*-
from osv import fields, osv
from tools.translate import _
from json import dumps, loads
from addons import get_module_resource
from giscemisc_informes.giscemisc_informes import Informe
from giscedata_contractacio_distri.wizard.wizard_informes_fianzas import TYPE_INFORMES
from StringIO import StringIO
from pandas import ExcelWriter, DataFrame
from base64 import b64encode
# from re import compile, IGNORECASE, sub
import numpy as np

TYPE_INFORMES.append(('incasol', 'INCASOL'))


class WizardInformesFianzas(osv.osv_memory):
    _name = 'wizard.informes.fianzas'
    _inherit = 'wizard.informes.fianzas'

    select_incasol = ["Tipo de registro", "Número de contrato",
                      "Fecha fianza", "Importe de la fianza",
                      "Tipo de vía", "Calle completa suministro",
                      "Número Carrer", "Escalera", "Piso", "Puerta",
                      "Código postal", "Municipio Nombre",
                      "Altres Dades Identificatives",
                      "Referencia catastral", "NIF Completo",
                      "Apellidos, Nombre o Razón Social del Arrendatario",
                      "Nom de l'abonat", "Id. Error Fiança", "Descripció Error"
                      ]

    header_sheet_columns = [
        "Número de concert", "Número de Liquidació", "Data de Liquidació",
        "Número Altes", "Números Baixes", "Import Altes", "Import Baixes",
        "Fitxer Errors"
    ]

    rename_header_incasol = {
        "Tipo de registro": "Tipus fiança",
        "Número de contrato": "Número Referència Contracte",
        "Fecha fianza": "Data fiança",
        "Importe de la fianza": "Import Fiança",
        "Tipo de vía": "Tipus Carrer",
        "Calle completa suministro": "Nom Carrer",
        "Escalera": "Escala",
        "Piso": "Pis",
        "Puerta": "Porta",
        "Código postal": "Codi Postal",
        "Municipio Nombre": "Municipi Finca",
        "Referencia catastral": "Referència Cadastral",
        "NIF Completo": "NIF de l'abonat",
        "Apellidos, Nombre o Razón Social del Arrendatario": "Cognoms/Raó social abonat",
    }

    def get_type_informe_info(self, cursor, uid, ids, context=None):
        info = super(WizardInformesFianzas, self).get_type_informe_info(
            cursor, uid, ids, context=context
        )

        informe_to_generate = self.read(
            cursor, uid, ids, ['informe_type'],
            context=context
        )[0]['informe_type']

        if informe_to_generate == 'incasol':
            specific_info = _(u'Es generara el fitxer de fiances amb el format '
                              u'd\'INCASOL '
                              )
            info = u'{}\n{}'.format(specific_info, info)

        return info

    def generate_informe(self, cursor, uid, ids, context=None):
        partner_obj = self.pool.get('res.partner')
        mv_line_obj = self.pool.get('account.move.line')
        conf_obj = self.pool.get('res.config')
        product_fianza = int(conf_obj.get(cursor, uid, 'deposit_product_id', 0))
        res = super(WizardInformesFianzas, self).generate_informe(
            cursor, uid, ids, context=context
        )

        if not res:
            informe_to_generate = self.read(
                cursor, uid, ids, ['recopiled_info', 'informe_type'],
                context=context
            )[0]

            json_datas_recopiled_inf = loads(
                informe_to_generate['recopiled_info']
            )

            fianzas_ids = json_datas_recopiled_inf['fianzas_ids']

            contract_diposits = json_datas_recopiled_inf['diposits']

            informe = informe_to_generate['informe_type']

            if informe == 'incasol':
                sql_name = 'informe_fianzas_generico.sql'
                sql_path = get_module_resource(
                    'giscedata_contractacio_distri', 'sql', sql_name
                )
                query = open(sql_path).read()

                params = {
                    'fianzas_ids': tuple(fianzas_ids),
                }

                cursor.execute(query, params)
                res = cursor.dictfetchall()

                informe = Informe(
                    data_dict=res, name='informe_incasol',
                    type='xls', columns=self.select_incasol,
                    rename_columns=self.rename_header_incasol, fill_na=None
                )

                # Let's process df
                process_df = informe.df

                # Create column with diposit asociated to contract
                # Use dict contract_diposits to do that
                # {contract_name: {deposit: float, }}

                # process_df['diposit_contracte'] = process_df["Número Referència Contracte"].apply(
                #     lambda x: contract_diposits[x]['deposit']
                # )

                # Procesamos los contratos con fianzas del periodo seleccionado
                # en el asistente

                # Result data frame, take renamed correct headers
                # from query renamed dataframe

                res_dataframe_dicts_list = []
                for contract_name in contract_diposits.keys():
                    # Buscamos las lineas positivas y negativas del periodo
                    # seleccionado para cada contrato con lineas de fianza

                    # Las lineas positivas indican retorno de dinero
                    # comer -> cliente
                    positives = process_df.loc[
                        (process_df["Import Fiança"] > 0)
                        &
                        (process_df["Número Referència Contracte"] == contract_name)
                    ]

                    # Las lineas negativas indican pago de dinero
                    # Cliente -> Comer
                    negativas = process_df.loc[
                        (process_df["Import Fiança"] < 0)
                        &
                        (process_df["Número Referència Contracte"] == contract_name)
                    ]

                    # Obtenim el diposit associat al contracte
                    # get_wizard_json_datas
                    diposit_pol = contract_diposits[contract_name]['deposit']

                    import_total_negatives_periode = 0
                    import_total_positives_periode = 0

                    if not negativas.empty:
                        import_total_negatives_periode = negativas["Import Fiança"].sum()

                    if not positives.empty:
                        import_total_positives_periode = positives["Import Fiança"].sum()

                    if diposit_pol == 0:
                        # Si 0 tenemos una baja

                        # Miramos si alta este año
                        if import_total_positives_periode and np.absolute(import_total_negatives_periode) == import_total_positives_periode:
                            # Linea d'alta
                            data_alta = negativas["Data fiança"].min()
                            min_row = negativas.loc[negativas["Data fiança"] == data_alta].iloc[0]
                            res_dataframe_dicts_list.append(
                                {
                                 "Tipus fiança": "FI",
                                 "Número Referència Contracte": contract_name,
                                 "Data fiança": data_alta.replace('/', '-'),
                                 "Import Fiança": np.absolute(import_total_negatives_periode),
                                 "Tipus Carrer": "C.",
                                 "Nom Carrer": min_row["Nom Carrer"],
                                 "Número Carrer": min_row["Número Carrer"],
                                 "Escala": min_row["Escala"],
                                 "Pis": min_row["Pis"],
                                 "Porta": min_row["Porta"],
                                 "Codi Postal": min_row["Codi Postal"],
                                 "Municipi Finca": min_row["Municipi Finca"],
                                 "Altres Dades Identificatives": None,
                                 "Referència Cadastral": min_row["Referència Cadastral"],
                                 "NIF de l'abonat": min_row["NIF de l'abonat"].replace('ES', ''),
                                 "Cognoms/Raó social abonat": min_row["NIF de l'abonat"],
                                 "Nom de l'abonat": min_row["Nom de l'abonat"],
                                 "Id. Error Fiança": None,
                                 "Descripció Error": None

                                 }
                            )

                            # Linea de baixa
                            data_baixa = positives["Data fiança"].max()
                            max_row = positives.loc[
                                positives["Data fiança"] == data_baixa].iloc[0]
                            res_dataframe_dicts_list.append(
                                {
                                    "Tipus fiança": "CA",
                                    "Número Referència Contracte": contract_name,
                                    "Data fiança": data_baixa.replace('/', '-'),
                                    "Import Fiança": -import_total_positives_periode,
                                    "Tipus Carrer": "C.",
                                    "Nom Carrer": max_row["Nom Carrer"],
                                    "Número Carrer": max_row["Número Carrer"],
                                    "Escala": max_row["Escala"],
                                    "Pis": max_row["Pis"],
                                    "Porta": max_row["Porta"],
                                    "Codi Postal": max_row["Codi Postal"],
                                    "Municipi Finca": max_row["Municipi Finca"],
                                    "Altres Dades Identificatives": None,
                                    "Referència Cadastral": max_row[
                                        "Referència Cadastral"],
                                    "NIF de l'abonat": max_row[
                                        "NIF de l'abonat"],
                                    "Cognoms/Raó social abonat": max_row[
                                        "NIF de l'abonat"].replace('ES', ''),
                                    "Nom de l'abonat": max_row[
                                        "Nom de l'abonat"],
                                    "Id. Error Fiança": None,
                                    "Descripció Error": None

                                }
                            )
                        else:
                            # TODO revisar condicion else o hacer elif
                            # Buscar anterior
                            # alta realizada en año anterior,
                            # solamente linea de baja con acomulado

                            acomulat_positiu = mv_line_obj.get_acomulat_positiu_negatiu_from_contract_and_product(
                                cursor, uid, product_fianza, contract_name,
                                positive=True, context=context
                            )

                            # Linea d'alta
                            data_baixa = positives["Data fiança"].max()
                            max_row = positives.loc[
                                positives["Data fiança"] == data_baixa].iloc[0]
                            res_dataframe_dicts_list.append(
                                {
                                    "Tipus fiança": "CA",
                                    "Número Referència Contracte": contract_name,
                                    "Data fiança": data_baixa.replace('/', '-'),
                                    "Import Fiança": -acomulat_positiu,
                                    "Tipus Carrer": "C.",
                                    "Nom Carrer": max_row["Nom Carrer"],
                                    "Número Carrer": max_row["Número Carrer"],
                                    "Escala": max_row["Escala"],
                                    "Pis": max_row["Pis"],
                                    "Porta": max_row["Porta"],
                                    "Codi Postal": max_row["Codi Postal"],
                                    "Municipi Finca": max_row["Municipi Finca"],
                                    "Altres Dades Identificatives": None,
                                    "Referència Cadastral": max_row[
                                        "Referència Cadastral"],
                                    "NIF de l'abonat": max_row[
                                        "NIF de l'abonat"].replace('ES', ''),
                                    "Cognoms/Raó social abonat": max_row[
                                        "NIF de l'abonat"],
                                    "Nom de l'abonat": max_row[
                                        "Nom de l'abonat"],
                                    "Id. Error Fiança": None,
                                    "Descripció Error": None

                                }
                            )
                    else:
                        # Si NO 0, deberia ser negativo
                        # (positivo no tendria mucho sentido)
                        # No tenemos baja pero puede haber altas
                        if not negativas.empty: # TODO CASO EN QUE SE SUBA FIANZA OTRO FICHERO
                            data_alta = negativas["Data fiança"].min()
                            min_row = negativas.loc[
                                negativas["Data fiança"] == data_alta].iloc[0]
                            res_dataframe_dicts_list.append(
                                {
                                    "Tipus fiança": "FI",
                                    "Número Referència Contracte": contract_name,
                                    "Data fiança": data_alta.replace('/', '-'),
                                    "Import Fiança": np.absolute(import_total_negatives_periode),
                                    "Tipus Carrer": "C.",
                                    "Nom Carrer": min_row["Nom Carrer"],
                                    "Número Carrer": min_row["Número Carrer"],
                                    "Escala": min_row["Escala"],
                                    "Pis": min_row["Pis"],
                                    "Porta": min_row["Porta"],
                                    "Codi Postal": min_row["Codi Postal"],
                                    "Municipi Finca": min_row["Municipi Finca"],
                                    "Altres Dades Identificatives": None,
                                    "Referència Cadastral": min_row[
                                        "Referència Cadastral"],
                                    "NIF de l'abonat": min_row["NIF de l'abonat"].replace('ES', ''),
                                    "Cognoms/Raó social abonat": min_row[
                                        "NIF de l'abonat"],
                                    "Nom de l'abonat": min_row["Nom de l'abonat"],
                                    "Id. Error Fiança": None,
                                    "Descripció Error": None

                                }
                            )

                res_dataframe = DataFrame(
                    data=res_dataframe_dicts_list,
                    columns=list(process_df)
                )

                # Lets generate first sheet from generated data frame process_df
                cout_df = res_dataframe.groupby(
                    'Tipus fiança'
                )['Import Fiança'].agg(['sum', 'count']).reset_index()

                altes = cout_df[cout_df['Tipus fiança'] == 'FI']
                baixes = cout_df[cout_df['Tipus fiança'] == 'CA']

                data_head_dict = [
                    {
                        "Número Altes": 0 if altes.empty else altes['count'].iloc[0],
                        "Números Baixes": 0 if baixes.empty else baixes['count'].iloc[0],
                        "Import Altes": 0.0 if altes.empty else altes['sum'].iloc[0],
                        "Import Baixes": 0.0 if baixes.empty else baixes['sum'].iloc[0],
                    }
                ]

                header_sheet_df = DataFrame(
                    data=data_head_dict, columns=self.header_sheet_columns
                )

                generated_file = StringIO()
                writer = ExcelWriter(generated_file, engine='xlsxwriter')

                header_sheet_df.to_excel(
                    writer, float_format='%.2f', index=None,
                    encoding='utf-8-sig', sheet_name='Capçalera'
                )

                res_dataframe.to_excel(
                    writer, float_format='%.2f', index=None,
                    encoding='utf-8-sig', sheet_name='Moviments'
                )

                writer.close()
                gen_file = b64encode(generated_file.getvalue())
                generated_file.close()

                self.write(cursor, uid, ids,
                           {'state': 'end',
                            'generated_informe': gen_file,
                            'informe_name': informe.file_name
                            }, context=context
                           )

    def validator_fichero_arrendamientos_incasol(self, cursor, uid, ids, informe, context=None):
        """
        Method to validate format, to avoid problems with date base
        :param ids: ids of wizard
        :param informe: Python object -> contains dataframe
        :return: True if valid format else show errors
        """
        errors = {}
        file_df = informe.df
        for index, row in file_df.iterrows():
            contract = row["Número Referència Contracte"]

            if len(str(row["Nom Carrer"])) > 50:
                if not errors.get(contract, False):
                    errors[contract] = []
                errors[contract].append(
                    'Direccio del punt de suministre massa llarga > 50'
                )

            if len(str(row["Nom de l'abonat"])) > 15:
                if not errors.get(contract, False):
                    errors[contract] = []
                errors[contract].append('Nom abonat massa llarg > 10')

            if len(str(row["Cognoms/Raó social abonat"])) > 50:
                if not errors.get(contract, False):
                    errors[contract] = []
                errors[contract].append('Cognom/Rao Social massa llarg > 50')

            if len(str(row["Código postal"])) != 5:
                if not errors.get(contract, False):
                    errors[contract] = []
                errors[contract].append('Codi postal no valid')

        return errors

WizardInformesFianzas()
