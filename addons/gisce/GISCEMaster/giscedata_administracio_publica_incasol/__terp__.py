# -*- coding: utf-8 -*-
{
    "name": "Administració pública INCASOL",
    "description": """Informes fianzas INCASOL""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_contractacio_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}