# -*- coding: utf-8 -*-
from osv import osv


class GiscedataBtElement(osv.osv):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    def unlink(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        tala_bt = self.pool.get('giscedata.tala.bt.tram')
        to_unlink = tala_bt.search(cursor, uid, [
            ('name', 'in', ids),
            ('done', '!=', 1)
        ])
        if to_unlink:
            tala_bt.unlink(cursor, uid, to_unlink)
        return super(GiscedataBtElement, self).unlink(cursor, uid, ids, context)

GiscedataBtElement()