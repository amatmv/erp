<%
    from datetime import datetime
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
    	<style type="text/css">
        ${css}
            body{
                font-family: helvetica;
                font-size: 11px;
                margin-right: 50px;
                margin-left: 50px;
            }
            table{
                font-size: 10px;
                font-weight: bold;
                width: 40%;
                border-collapse: collapse;
                border: none;
            }
            table td{
                padding: 5px;
            }
            table td.fosc{
                background-color: #BFBFBF;
            }
            table td.clar{
                background-color: #D8D8D8;
            }
            #capcalera{
                width: 100%;
                background-color: #BFBFBF;
                height: 20px;
            }
            #titol{
                position: relative;
            	width: 100%;
            	background-color: #D8D8D8;
                font-weight: bold;
            }
            #continguts{
                position: relative;
                top: 40px;
            }
            #dades_capcalera{
                padding-left: 3px;
                float: left;
                font-weight: bold;
                font-size: 14px;
            }
            #data{
                padding-right: 3px;
                float: right;
                font-weight: bold;
                font-size: 14px;
            }
        </style>
    </head>
    <body>
        <div id="capcalera">
            <div id="dades_capcalera">
                %if data['informe'] == 'longitud_bt':
                    Tala línies BT - Trimestre ${objects[0].trimestre.name}
                %else:
                    Tala línies AT/MT - Trimestre ${objects[0].trimestre.name}
                %endif
            </div>
            <div id="data">
                Data informe: ${datetime.now().strftime("%d-%m-%Y")}
            </div>
        </div>
    	<div id="titol">
            <p>
                %if data['informe'] == 'longitud_at':
        		    Compliment Pla trienal de tala i poda en AT i MT [%]
                %elif data['informe'] == 'longitud_bt':
                    Compliment Pla trienal de tala i poda en BT [%]
                % elif data['informe'] == 'tram':
                    Nº trams pendents d'un trimestre
                % elif data['informe'] == 'empresa':
                    Nº trams pendents per contracta
                % elif data['informe'] == 'critic':
                    Nº trams crítics per trimestre
                %endif
            </p>
    	</div>
        <div id="continguts" align="center">
            %if data['pc_longitud'] == 0 and data['informe'] != 'empresa':
                No s'han trobat trams sense talar en el trimestre ${objects[0].trimestre.name}
            %else:
                <table>
                    %if data['informe'] == 'longitud_at' or data['informe'] == 'longitud_bt':
                        <tr>
                            <td class="fosc">Longitud pendent</td>
                            <td class="fosc">Longitud total</td>
                            <td class="fosc">Trams sense talar</td>
                        </tr>
                        <tr align="center">
                            <td class="clar">${round(data['longitud_pendent']/1000, 2)} Km</td>
                            <td class="clar">${round(data['longitud_total']/1000, 2)} Km</td>
                            <td class="clar">${round(data['pc_longitud'], 2)} %</td>
                        </tr>
                        </tr>
                    %elif data['informe'] == 'tram' or data['informe'] == 'critic':
                        <tr align="center">
                            <td class="fosc">Trams pendents</td>
                            <td class="fosc">Trams totals</td>
                            <td class="fosc">Trams pendents [%]</td>
                        </tr>
                        <tr align="center">
                            <td class="clar">${data['num_pendents']}</td>
                            <td class="clar">${data['trams_total']}</td>
                            <td class="clar">${round((data['num_pendents']*100)/data['trams_total'], 2)} %</td>
                        </tr>
                    %elif data['informe'] == 'empresa':
                        <tr>
                               <td class="fosc">Empresa</td>
                               <td class="fosc">Trams pendents</td>
                        </tr>
                        %for elem in data['empreses']:
                            <tr>
                                <td class="clar">${elem}</td>
                                <td class="clar" align="center">${data['empreses'][elem]}</td>
                            </tr>
                        %endfor
                        %for elem in data['empreses_sense_pendents']:
                            <tr>
                                <td class="clar">${elem}</td>
                                <td class="clar" align="center">${data['empreses_sense_pendents'][elem]}</td>
                            </tr>
                        %endfor
                    %endif
                </table>
                %if data['informe'] == 'longitud_bt':
                    <br>
                    <br>
                    <table>
                        <tr>
                            <td class="fosc">CT's fets</td>
                            <td class="fosc">CT's pendents</td>
                            <td class="fosc">CT's sense talar [%]</td>
                        </tr>
                        <tr align="center">
                            <td class="clar">${data['bt_fet']}</td>
                            <td class="clar">${data['bt_pendent']}</td>
                            <td class="clar">${round((data['bt_pendent']*100)/data['bt_total'], 2)} %</td>
                        </tr>
                    </table>
                %endif
            %endif
            <br><br><br>
            <hr>
        </div>
    </body>
</html>
