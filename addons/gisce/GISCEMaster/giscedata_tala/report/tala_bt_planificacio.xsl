<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="grup-comarca" match="tala_bt" use="comarca_nom"/>
  <xsl:key name="grup-trimestre" match="trimestre" use="trimestre_nom"/>

  <xsl:template match="/">
    <document>
      <template bottomMargin="10mm" pageSize="(297mm,190mm)" rightMargin="10mm" topMargin="10mm">
        <pageTemplate id="main">
          <frame height="175mm" id="first" width="277mm" x1="10mm" y1="10mm"/>
        </pageTemplate>
      </template>
      <stylesheet> 
        <paraStyle fontName="Helvetica-Bold" fontSize="8" alignment="center" name="titol"/>
        <paraStyle fontName="Helvetica-Bold" fontSize="7" alignment="center" name="titol_2"/>
        <paraStyle fontName="Helvetica-Bold" fontSize="6" alignment="center" name="header"/>
        <paraStyle fontName="Helvetica" fontSize="6" name="data"/>
        <blockTableStyle id="header">
	      <blockBackground colorName="lightgrey" start="0,0" stop="-1,2"/>
	      <lineStyle kind="GRID" colorName="lightgrey" start="0,3" stop="-1,-1"/>
	      <!-- a partir d'aqui els formats de les capcaleres -->
          <blockFont name="Helvetica-Bold" size="6" start="0,0" stop="-1,2"/>
	      <blockAlignment value="CENTER" start="6,0" stop="6,0"/>
          <blockAlignment value="RIGHT" start="8,1" stop="8,1"/>
          <blockAlignment value="CENTER" start="0,2" stop="-1,2"/>
          <blockAlignment value="LEFT" start="4,2" stop="4,2"/>
          <blockAlignment value="LEFT" start="10,2" stop="10,2"/>
	      <!-- a partir d'aqui les linies a les capcaleres -->
	      <lineStyle kind="LINEBEFORE" start="10,0" stop="10,1" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="8,0" stop="8,0" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="5,0" stop="5,0" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="5,1" stop="5,1" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="7,1" stop="8,1" colorName="black"/>
          <lineStyle kind="LINEBEFORE" start="0,0" stop="0,1" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="2,1" stop="2,1" colorName="black"/>
	      <lineStyle kind="LINEABOVE" start="0,0" stop="-1,0" colorName="black"/>
	      <lineStyle kind="LINEBELOW" start="0,0" stop="7,0" colorName="black"/>
	      <lineStyle kind="LINEAFTER" start="11,0" stop="12,2" colorName="black"/>
	      <lineStyle kind="GRID" start="0,2" stop="6,2" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="10,0" stop="11,2" colorName="black"/>
	      <lineStyle kind="LINEBELOW" start="7,2" stop="11,2" colorName="black"/>
	      <lineStyle kind="LINEBEFORE" start="8,2" stop="9,2" colorName="black"/>
	      <lineStyle kind="LINEABOVE" start="8,2" stop="9,2" colorName="black"/>
	      <!-- a partir d'aqui les files de dades -->
          <blockFont name="Helvetica" size="6" start="0,3" stop="-1,-1"/>
	      <blockAlignment value="CENTER" start="2,3" stop="2,-1"/><!-- CT nom -->
	      <blockAlignment value="CENTER" start="3,3" stop="3,-1"/><!-- CT nom -->
	      <blockAlignment value="CENTER" start="8,3" stop="9,-1"/><!-- dates -->
	      <blockAlignment value="RIGHT" start="0,3" stop="1,-1"/><!-- id ct, tensio -->
	      <blockAlignment value="RIGHT" start="4,3" stop="4,-1"/><!-- INE Municipi -->
	      <blockAlignment value="RIGHT" start="7,3" stop="7,-1"/><!-- longitud -->
	      <blockValign value="TOP" start="0,3" stop="-1,-1"/>-->
        </blockTableStyle>
      </stylesheet>
      <story>
        <xsl:apply-templates select="arrel"/>
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="arrel">
    <xsl:for-each select="trimestre[count(. | key('grup-trimestre', trimestre_nom)[1]) = 1]">
      <xsl:variable name="trimestre_nom" select="trimestre_nom" />
      <spacer length="20"/>
      <para style="titol" t="1"><xsl:value-of select="//corporate-header/corporation/rml_header1" /> - Planificació de tala de línies BT - Trimestre <xsl:value-of select="trimestre_nom"/><!-- * Pàgina <pageNumber/> de <getName id="LASTPAGENO" default="-999"/>--></para>
      <xsl:for-each select="//arrel/trimestre/tala_bt[count(. | key('grup-comarca', comarca_nom)[1]) = 1]">
        <xsl:variable name="comarca_nom" select="comarca_nom" />
        <xsl:if test="count(//arrel/trimestre[tala_bt/comarca_nom=$comarca_nom and trimestre_nom=$trimestre_nom]) &gt; 0">
          <spacer length="20"/>
          <blockTable colWidths="25cm">
            <tr>
              <td><para style="titol_2" alignment="left">Comarca: <xsl:value-of select="comarca_nom"/></para></td>
            </tr>
          </blockTable>
          <blockTable colWidths="11mm,11mm,15mm,35mm,10mm,10mm,40mm,10mm,15mm,15mm,12mm,65mm" repeatRows="3" style="header">
            <tr>
              <td t="1"></td>
              <td t="1"></td>
	          <td t="1">Línies elèctriques</td>
	          <td t="1"></td>
	          <td t="1"></td>
	          <td t="1"></td>
	          <td t="1">Zona d'actuació</td>
	          <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
            </tr>
            <tr>
              <td t="1">Instal.lació</td>
	          <td t="1"></td>
	          <td t="1"></td>
              <td t="1">Punt d'inici</td>
	          <td t="1"></td>
	          <td t="1"></td>
	          <td t="1">Municipi</td>
              <td t="1"></td>
              <td t="1">Època</td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
            </tr>
            <tr>
              <td t="1">Codi</td>
	          <td t="1"><para style="header">Tensió (V)</para></td>
	          <td t="1"><para style="header">CT</para></td>
	          <td t="1"><para style="header">Coordenades</para></td>
	          <td t="1"><para style="header">Núm. plànol</para></td>
	          <td t="1"><para style="header">Codi INE</para></td>
	          <td t="1"><para style="header">Nom</para></td>
	          <td t="1"><para style="header">Long. (m)</para></td>
	          <td t="1"><para style="header">Inici</para></td>
	          <td t="1"><para style="header">Final</para></td>
	          <td t="1"><para style="header">Tipus treballs</para></td>
	          <td t="1"><para style="header">Observacions</para></td>
            </tr>
            <xsl:for-each select="//arrel/trimestre/tala_bt[comarca_nom=$comarca_nom]">
            <xsl:if test="../trimestre_nom=$trimestre_nom">
              <tr>
                <td><xsl:value-of select="ct_id"/></td>
                <td><xsl:value-of select="ct_tensio"/></td>
                <td><xsl:value-of select="ct_nom"/></td>
                <td><xsl:value-of select="ct_coordenades"/></td>
                <td><!-- numero de planol --></td>
                <td><xsl:value-of select="municipi_ine"/></td>
                <td><para style="data"><xsl:value-of select="municipi_nom"/></para></td>
                <td><xsl:value-of select="format-number(ct_longitud_trams, '0')"/></td>
                <td><xsl:value-of select="inici_previst"/></td>
                <td><xsl:value-of select="final_previst"/></td>
                <td><xsl:value-of select="tipus_treball"/></td>
                <td><para style="data"><xsl:if test="observacions!=0"><xsl:value-of select="observacions"/></xsl:if></para></td>
              </tr>
                <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
              </xsl:if>
            </xsl:for-each>
          </blockTable>
          <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
        </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
