<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="grup-comarca" match="tala_at" use="comarca_nom"/>
  <xsl:key name="grup-trimestre" match="trimestre" use="trimestre_nom"/>

  <xsl:template match="/">
    <document>
      <template bottomMargin="10mm" pageSize="(297mm,190mm)" rightMargin="10mm" topMargin="10mm">
        <pageTemplate id="main">
          <frame height="175mm" id="first" width="277mm" x1="10mm" y1="10mm"/>
        </pageTemplate>
      </template>
      <stylesheet>
      <paraStyle fontName="Helvetica-Bold" fontSize="8" alignment="center" name="titol"/>
      <paraStyle fontName="Helvetica-Bold" fontSize="7" alignment="center" name="titol_2"/>
      <paraStyle fontName="Helvetica" fontSize="6" name="data"/>
      <blockTableStyle id="header">
        <!-- aquesta linia es per debugging per els bordes de la capcalera :) -->
        <!--lineStyle kind="GRID" colorName="grey" /-->
        <lineStyle kind="GRID" colorName="lightgrey" start="0,3" stop="-1,-1"/>
        <blockBackground colorName="lightgrey" start="0,0" stop="-1,2"/>
        <!-- a partir d'aqui els formats de les capcaleres -->
        <blockFont name="Helvetica-Bold" size="6" start="0,0" stop="-1,2"/>
        <blockAlignment value="CENTER" start="2,1" stop="3,1"/><!-- segona linia inici i final linia -->
        <blockAlignment value="CENTER" start="8,1" stop="9,1"/><!-- segona linia long linia i numero planol -->
        <blockAlignment value="RIGHT" start="10,1" stop="10,1"/><!-- segona linia epoca -->
        <blockAlignment value="CENTER" start="12,1" stop="12,1"/><!-- segona linia tipus treballs -->
        <blockAlignment value="CENTER" start="0,2" stop="-1,2"/><!-- ultima linia de capcaleres -->
        <!-- a partir d'aqui les linies a les capcaleres -->
        <lineStyle kind="LINEABOVE" start="0,0" stop="-1,0" colorName="black"/>
        <lineStyle kind="LINEBELOW" start="0,0" stop="9,0" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="4,0" stop="4,1" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="8,1" stop="8,1" colorName="black"/>
        <lineStyle kind="GRID" start="2,1" stop="3,2" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="6,1" stop="6,1" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="0,0" stop="0,1" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="9,1" stop="10,2" colorName="black"/>
        <lineStyle kind="LINEABOVE" start="0,0" stop="0,1" colorName="black"/>
        <lineStyle kind="LINEABOVE" start="1,1" stop="1,1" colorName="black"/>
        <lineStyle kind="GRID" start="0,2" stop="7,2" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="10,0" stop="10,0" colorName="black"/>
        <lineStyle kind="GRID" start="10,2" stop="11,2" colorName="black"/>
        <lineStyle kind="LINEBEFORE" start="12,0" stop="13,2" colorName="black"/>
        <lineStyle kind="LINEAFTER" start="13,0" stop="13,2" colorName="black"/>
        <lineStyle kind="LINEBELOW" start="0,2" stop="-1,2" colorName="black"/>
        <!-- a partir d'aqui les files de dades -->
        <blockFont name="Helvetica" size="6" start="0,3" stop="-1,-1"/>
        <blockAlignment value="CENTER" start="10,3" stop="11,-1"/><!-- dates -->
        <blockAlignment value="RIGHT" start="0,3" stop="1,-1"/><!-- id linia, tensio -->
        <blockAlignment value="RIGHT" start="4,3" stop="4,-1"/><!-- INE Municipi -->
        <blockAlignment value="RIGHT" start="8,3" stop="8,-1"/><!-- longitud -->
        <blockValign value="TOP" start="0,3" stop="-1,-1"/>
      </blockTableStyle>
      </stylesheet>
      <story>
        <xsl:apply-templates select="arrel"/>
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="arrel">
    <xsl:for-each select="trimestre[count(. | key('grup-trimestre', trimestre_nom)[1]) = 1]">
      <xsl:variable name="trimestre_nom" select="trimestre_nom" />
      <spacer length="20"/>
      <para style="titol" t="1"><xsl:value-of select="//corporate-header/corporation/rml_header1" /> - Planificació de tala de línies AT - Trimestre <xsl:value-of select="trimestre_nom"/><!-- * Pàgina <pageNumber/> de <getName id="LASTPAGENO" default="-999"/>--></para>
      <xsl:for-each select="//arrel/trimestre/tala_at[count(. | key('grup-comarca', comarca_nom)[1]) = 1]">
        <xsl:variable name="comarca_nom" select="comarca_nom" />
        <xsl:if test="count(//arrel/trimestre[tala_at/comarca_nom=$comarca_nom and trimestre_nom=$trimestre_nom]) &gt; 0">
          <spacer length="20"/> 
          <blockTable colWidths="28.5cm">
            <tr>
              <td><para style="titol_2" alignment="left">Comarca: <xsl:value-of select="comarca_nom"/></para></td>
            </tr>
          </blockTable>
          <blockTable colWidths="8mm,8mm,30mm,30mm,12mm,30mm,30mm,30mm,8mm,7mm,13mm,13mm,10mm,55mm" repeatRows="3" style="header">
            <tr>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1">Línia elèctrica</td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1">Zona d'actuació</td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1"></td>
            </tr>
            <tr>
              <td t="1">Instal.lació</td>
              <td t="1"></td>
              <td t="1">Punt d'inici</td>
              <td t="1">Punt final</td>
              <td t="1"></td>
              <td t="1">Municipi</td>
              <td t="1"></td>
              <td t="1"></td>
              <td t="1">Long.</td>
              <td t="1">Núm.</td>
              <td t="1">Època</td>
              <td t="1"></td>
              <td t="1">Tipus</td>
              <td t="1"></td>
            </tr>
            <tr>
              <td t="1">Codi</td>
              <td t="1">kV</td>
              <td t="1">Centre distribució</td>
              <td t="1">Centre distribució</td>
              <td t="1">Codi INE</td>
              <td t="1">Nom</td>
              <td t="1">Punt d'inici</td>
              <td t="1">Punt final</td>
              <td t="1">(m)</td>
              <td t="1">plànol</td>
              <td t="1">Inici</td>
              <td t="1">Final</td>
              <td t="1">treballs</td>
              <td t="1">Observacions</td>
            </tr>
            <xsl:for-each select="//arrel/trimestre/tala_at[comarca_nom=$comarca_nom]">
              <xsl:if test="../trimestre_nom=$trimestre_nom">
                <tr>
                  <td><xsl:value-of select="linia_at"/></td>
                  <td><xsl:value-of select="linia_tensio div 1000"/></td>
                  <td><para style="data"><xsl:value-of select="linia_origen"/></para></td>
                  <td><para style="data"><xsl:value-of select="linia_final"/></para></td>
                  <td><xsl:value-of select="municipi_ine"/></td>
                  <td><para style="data"><xsl:value-of select="municipi_nom"/></para></td>
                  <td><para style="data"><xsl:value-of select="zona_actuacio_inici"/></para></td>
                  <td><para style="data"><xsl:value-of select="zona_actuacio_final"/></para></td>
                  <td><xsl:value-of select="format-number(linia_longitud, '0')"/></td>
                  <td><!-- numero de planol --></td>
                  <td><xsl:value-of select="inici_previst"/></td>
                  <td><xsl:value-of select="final_previst"/></td>
                  <td><xsl:value-of select="tipus_treball"/></td>
                  <td><para style="data"><xsl:if test="observacions!=0"><xsl:value-of select="observacions"/></xsl:if></para></td>
                </tr>
                <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
              </xsl:if>
            </xsl:for-each>
          </blockTable>
          <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
        </xsl:if>
      </xsl:for-each>
      <xsl:if test="not(position() = last())"><nextPage/></xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>