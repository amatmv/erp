# -*- coding: utf-8 -*-
{
    "name": "GISCE Tala línies AT i BT",
    "description": """Tala
$Id$""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Tala",
    "depends":[
        "base",
        "base_extended",
        "giscedata_trieni",
        "giscedata_at",
        "giscedata_cts",
        "giscedata_bt",
        "c2c_webkit_report",
        "giscegis_catastre"
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_tala_demo.xml"
    ],
    "update_xml":[
        "giscedata_tala_view.xml",
        "giscedata_tala_at_view.xml",
        "giscedata_tala_bt_view.xml",
        "wizard/wizard_giscedata_tala_report_trim_view.xml",
        "giscedata_tala_wizard.xml",
        "giscedata_tala_report.xml",
        "wizard/wizard_reparteix_costos_codi_tala_view.xml",
        "wizard/wizard_giscedata_tala_omple_trienni.xml",
        "security/giscedata_tala_security.xml",
        "security/ir.model.access.csv",
        "giscegis_catastre_parceles_at_trams.xml"
    ],
    "active": False,
    "installable": True
}
