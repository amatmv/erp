# -*- coding: utf-8 -*-
"""Assigna el valor cups_id.name a tots els  cups_input de la base de dades.
"""

from oopgrade import oopgrade
import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()

    if not installed_version:
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Start of the script to restore longitud for '
                         'done trams')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Doing it for gd_tala_at_tram')
    cursor.execute('UPDATE giscedata_tala_at_tram '
                   'SET longitud=longitud_pre '
                   'WHERE done=TRUE')
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Successfully changed!')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Doing it for gd_tala_bt_tram')
    cursor.execute('UPDATE giscedata_tala_bt_tram '
                   'SET longitud=longitud_pre '
                   'WHERE done=TRUE')
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Successfully changed!')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Correctly migrated!!')


def down(cursor, installed_version):
    pass
    # We don't need to do anything here

migrate = up
