# -*- coding: utf-8 -*-
"""Assigna el valor cups_id.name a tots els  cups_input de la base de dades.
"""

from oopgrade import oopgrade
import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()

    if not installed_version:
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Start of the script to change longitud colum name'
                         'to save it')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Doing it for gd_tala_at_tram')
    oopgrade.rename_columns(cursor, {
        'giscedata_tala_at_tram': [
            ('longitud', 'longitud_pre')
        ]
    })
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Successfully changed!')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Doing it for gd_tala_bt_tram')
    oopgrade.rename_columns(cursor, {
        'giscedata_tala_bt_tram': [
            ('longitud', 'longitud_pre')
        ]
    })
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Successfully changed!')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Correctly migrated!!')


def down(cursor, installed_version):
    logger = netsvc.Logger()

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Start of the script to restore longitud values')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Doing it for gd_tala_at_tram')
    if oopgrade.column_exists(cursor, 'giscedata_tala_at_tram', 'longitud_pre'):
        oopgrade.drop_columns(cursor, [('giscedata_tala_at_tram', 'longitud')])
        oopgrade.rename_columns(cursor, {
            'giscedata_tala_at_tram': [
                ('longitud_pre', 'longitud')
            ]
        })
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             'Successfully changed!')
    else:
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             'We don\'t have the column longitud_pre to do it')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Doing it for gd_tala_bt_tram')
    if oopgrade.column_exists(cursor, 'giscedata_tala_bt_tram', 'longitud_pre'):
        oopgrade.drop_columns(cursor, [('giscedata_tala_bt_tram', 'longitud')])
        oopgrade.rename_columns(cursor, {
            'giscedata_tala_bt_tram': [
                ('longitud_pre', 'longitud')
            ]
        })
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             'Successfully changed!')
    else:
        logger.notifyChannel('migration', netsvc.LOG_INFO,
                             'We don\'t have the column longitud_pre to do it')

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Correctly migrated!!')

migrate = up
