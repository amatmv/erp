# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataAtTram(osv.osv):

    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"

    _columns = {
        'codi_tala': fields.many2one('giscedata.tala.codi', u'Codi de tala')
    }


GiscedataAtTram()