# *-* coding: utf-8 *-*

import wizard

def _selection_trimestre(self, cr, uid, context={}):
    cr.execute('select gta.trimestre as id_trimestre, gt.name as nom_trimestre from giscedata_tala_at gta left join giscedata_trieni gt on gta.trimestre = gt.id group by id_trimestre, nom_trimestre order by cast(substring(gt.name from 3 for 4) || substring(gt.name from 1 for 1) as int)')
    #return = [(a[0], a[1]) for a in cr.fetchall()]
    return [(a[1], a[1]) for a in cr.fetchall()] # retornem el "nom" del trimestre com a clau, igualment ens cal tornar a buscar els id's despres per al BETWEEN

_init_form = """<?xml version="1.0"?>
<form string="Informe planificació tala AT" col="1">
  <field name="trimestre_inicial" />
  <field name="trimestre_final" />
</form>
"""

_init_fields = {
  'trimestre_inicial' : {'string':'Trimestre inicial', 'type':'selection', 'selection': _selection_trimestre },
  'trimestre_final' : {'string':'Trimestre final', 'type':'selection', 'selection': _selection_trimestre },
}


def _print(self, cr, uid, data, context={}):
    ids = []
    inici = int(data['form']['trimestre_inicial'][0:1]) + 10 * int(data['form']['trimestre_inicial'][2:6])
    final = int(data['form']['trimestre_final'][0:1]) + 10 * int(data['form']['trimestre_final'][2:6])
    if inici > final:
        inici, final = final, inici
    cr.execute("select id from giscedata_trieni where cast(substring(name from 1 for 1) as int) + 10 * cast(substring(name from 3 for 4) as int) between %d and %d", (inici, final,))
    return { 'ids': [ a[0] for a in cr.fetchall() ] }

class wizard_giscedata_tala_report_at(wizard.interface):
    states = {
        'init': {
          'actions': [],
          'result': {
              'type': 'form',
              'arch': _init_form,
              'fields': _init_fields,
              'state': [
                  ('end', 'Cancelar', 'gtk-cancel'),
                  ('print', 'Imprimir', 'gtk-print')
                  ]
              }
        },
        'print': {
          'actions': [_print],
          'result': {
              'type': 'print',
              'report': 'giscedata.tala.at.planificacio',
              'get_id_from_action': True,
              'state': 'end'
              }
        },
        'end': {
          'actions': [],
          'result': {
              'type': 'state',
              'state': 'end'
              }
        }
    }


wizard_giscedata_tala_report_at('giscedata.tala.report.at')
