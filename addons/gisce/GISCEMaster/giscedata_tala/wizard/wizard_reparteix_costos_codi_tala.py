# -*- coding: utf-8 -*-
from osv import osv, fields, orm

from tools.translate import _


class GiscedataTalaReparteixCostosWizard(osv.osv_memory):

    _name = "wizard.reparteix.costos.codi.tala"

    def reparteix_costos(self, cursor, uid, ids, context=None):

        if not context:
            context = {}

        codi_obj = self.pool.get('giscedata.tala.codi')
        wiz = self.browse(cursor, uid, ids[0])

        ids = context.get('active_ids', False)
        if not ids:
            wiz.write({'state': 'done',
                       'info': _(u"No s'han trobat codis")})
            return False

        codi_obj.reparteix(cursor, uid, ids, context=context)

        codis = codi_obj.read(cursor, uid, ids, ['codi'])
        txt = _(u"S'han repartit els costos dels següents codis de tala: %s")

        info = txt % ', '.join([c['codi'] for c in codis])

        wiz.write({'state': 'done', 'info': info})

        return True

    def _get_info(self, cursor, uid, context=None):
        '''Omple info'''
        codi_obj = self.pool.get('giscedata.tala.codi')

        if not context:
            context = {}

        ids = context.get('active_ids', False)

        codis = codi_obj.read(cursor, uid, ids, ['codi'])

        txt = _(u"Repartirem els costos dels següents codis de tala: %s")

        return txt % ', '.join([c['codi'] for c in codis])

    _columns = {
        'state': fields.char('Estat', size=10),
        'info': fields.text('Info'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_info,
    }

GiscedataTalaReparteixCostosWizard()