# *-* coding: utf-8 *-*
from datetime import datetime, timedelta
from collections import Counter

import netsvc
from osv import osv, fields


logger = netsvc.Logger()


INFORME_SELECTION = [
    ('longitud_at', '[AT] Tala i poda (% realitzat)'),
    ('longitud_bt', '[BT] Tala i poda (% realitzat)'),
    ('tram', '[AT] Trams pendents per període'),
    ('empresa', '[AT] Trams pendents per contracta'),
    ('critic', '[AT] Trams pendents fora periode (crítics)')
]


def log(missatge):
    logger.notifyChannel('Informes-Trimestrals', netsvc.LOG_INFO, missatge)


class WizardGiscedataTalaReportTrim(osv.osv_memory):
    _name = 'wizard.giscedata.tala.report.trim'

    def generar_informe_tala_poda(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        trams_obj = self.pool.get('giscedata.tala.at.tram')
        c = Counter()
        if wiz.informe.endswith('_bt'):
            trams_obj = self.pool.get('giscedata.tala.bt.tram')
            ct_bt_obj = self.pool.get('giscedata.tala.bt')
            search_params_ct = [
                ('trimestre.id', '=', wiz.trimestre.id)
            ]
            ct_bt_ids = ct_bt_obj.search(cursor, uid, search_params_ct)
            c['bt_fet'] = 0
            c['bt_pendent'] = 0
            for ct in ct_bt_obj.read(cursor, uid, ct_bt_ids):
                c['bt_total'] += 1
                if ct['perc_talat'] == 100:
                    c['bt_fet'] += 1
                else:
                    c['bt_pendent'] += 1

        c['empreses'] = Counter()
        c['empreses_sense_pendents'] = Counter()
        search_params = [
            ('tasca.trimestre.id', '=', wiz.trimestre.id),
        ]
        if wiz.informe == 'critic':
            data_limit = (
                datetime.now() - timedelta(days=wiz.dies)
            ).strftime('%Y-%m-%d')
            search_params += [
                ('tasca.data_final', '<', data_limit)
            ]
        trams_ids = trams_obj.search(cursor, uid, search_params)
        for tram in trams_obj.read(cursor, uid, trams_ids):
            c['longitud_total'] += tram['longitud']
            c['trams_total'] += 1
            if tram['codi_tala_tram'] != 'NO' and not tram['done']:
                c['longitud_pendent'] += tram['longitud']
                c['num_pendents'] += 1
                if tram['empresa']:
                    empresa = tram['empresa'][1]
                else:
                    empresa = 'Sense empresa'
                c['empreses'][empresa] += 1
            else:
                # Afegim empreses que ja hagin talat
                if tram['empresa']:
                    empresa = tram['empresa'][1]
                    if empresa not in c['empreses']:
                        c['empreses_sense_pendents'][empresa] = 0
        try:
            c['pc_longitud'] = (
                c['longitud_pendent'] / c['longitud_total']
            ) * 100
        except ZeroDivisionError:
            c['pc_longitud'] = 0

        c['informe'] = wiz.informe
        datas = dict(c)
        datas['empreses'] = dict(datas['empreses'])
        datas['empreses_sense_pendents'] = dict(
                datas['empreses_sense_pendents']
        )
        return datas

    def generar_informe(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0], context=context)
        informe_map = dict(INFORME_SELECTION)
        log("Generant informe: {0}".format(informe_map.get(wiz.informe, '')))
        dades = self.generar_informe_tala_poda(cursor, uid, ids, context)
        dades['informe'] = wiz.informe
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'giscedata.tala.trim',
            'datas': dades
        }

    _columns = {
        'trimestre': fields.many2one(
                'giscedata.trieni',
                'Trimestre',
                required=True
        ),
        'informe': fields.selection(INFORME_SELECTION, 'Informe', required=True),
        'dies': fields.integer(
            'Dies',
            size=3
        )
    }

    def _default_trimestre(self, cursor, uid, context=None):
        if context is None:
            context = {}
        trim_obj = self.pool.get('giscedata.trieni')
        now = datetime.now().strftime('%Y-%m-%d')
        trim_ids = trim_obj.search(cursor, uid, [
            ('data_inici', '<=', now),
            ('data_final', '>=', now)
        ])
        if trim_ids:
            return trim_ids[0]
        else:
            return False

    _defaults = {
        'trimestre': _default_trimestre
    }

WizardGiscedataTalaReportTrim()
