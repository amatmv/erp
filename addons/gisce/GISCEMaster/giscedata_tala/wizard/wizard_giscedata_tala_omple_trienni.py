# -*- coding: utf-8 -*-
import time
import pooler

from osv import osv, fields, orm


class GiscedataTalaOmpleTrienniWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.tala.omple.trienni.wizard'

    def _get_trimestres(self, cursor, uid, context=None):
        """Triennis disponibles"""
        if not context:
            context = None
        trienni_obj = self.pool.get('giscedata.trieni')
        trimestres_vals = trienni_obj.search_reader(cursor, uid,
                                                    [('trieni', '>', 0)],
                                                    ['name'],
                                                    order='anyy desc')

        return [(v['id'], v['name']) for v in trimestres_vals]

    def _get_default_trimestre(self, cursor, uid, context=None):
        """Agafa el trimestre actual"""
        if not context:
            context = None
        trienni_obj = self.pool.get('giscedata.trieni')
        avui = time.strftime('%Y-%m-%d', time.gmtime())
        search_param = [('data_final', '>', avui)]
        trienni_id = trienni_obj.search(cursor, uid, search_param,
                                        order='data_inici ASC')

        return trienni_id[0]

    def _get_fi_trienni(self, trimestre_inicial):
        trim, anyy = trimestre_inicial.split('/')
        #Calculem el trimestre final afegint 3 anys i agafant l'anterior
        anyy_nou = int(anyy) + 3
        trim_nou = ((int(trim) - 2) % 4) + 1

        nom_trim_nou = "%s/%d" % (trim_nou, anyy_nou)

        return nom_trim_nou


    def onchange_trimestre(self, cursor, uid, ids, trimestre, context=None):
        """Calcula el trimestre final"""
        if not context:
            context = None
        res = {}

        trienni_obj = self.pool.get('giscedata.trieni')
        trim_val = trienni_obj.read(cursor, uid, trimestre, ['name'],
                                    context=context)

        nom_trim_nou = self._get_fi_trienni(trim_val['name'])

        # Validem que existeixi
        num_trim = trienni_obj.search_count(cursor, uid,
                                            [('name', '=', nom_trim_nou)],
                                            context=context)

        if not num_trim:
            miss = (u'El trimestre \'%s\' no està generat. Genera el trienni '
                    u'següent o no es crearan les tales corresponents als '
                    u'trimestres que no estan generats' % nom_trim_nou)
            res['warning'] = {'title': u'Trimestre inexistent',
                              'message': miss}
            nom_trim_nou += " *warn*"

        res['value'] = {'trimestre_final': nom_trim_nou}

        return res

    def omple_trienni(self, cursor, uid, ids, context=None):
        """Omplim els trienni seleccionat. Agafem el trimestre escollit i omplim
        tot el trienni amb les tales del trienni anterior. Si el trimestre no
        existeix no importem les tales"""
        if not context:
            context = None

        res = {}
        tala_at_obj = self.pool.get('giscedata.tala.at')
        tala_bt_obj = self.pool.get('giscedata.tala.bt')
        trim_obj = self.pool.get('giscedata.trieni')

        wizard = self.browse(cursor, uid, ids[0], context)
        trim_ini = wizard.trimestre
        # busquem les tales del trimestre anterior (-3 anys)
        trim_ini_name = trim_obj.read(cursor, uid, trim_ini, ['name'])['name']
        trim, anyy = trim_ini_name.split('/')

        trim_ant_ini = "%s/%d" % (trim, int(anyy) - 3)
        trim_ant_fi = self._get_fi_trienni(trim_ant_ini)

        data_ini = "%d-%02d-%02d" % (int(anyy) - 3, (int(trim) - 1) * 3 + 1, 1)
        trim, anyy = trim_ant_fi.split('/')
        data_fi = "%d-%02d-%02d" % (int(anyy), (int(trim) - 1) * 3 + 1, 15)

        trim_ant_ids = trim_obj.search(cursor, uid,
                                       [('data_inici', '>=', data_ini),
                                        ('data_inici', '<=', data_fi)])

        triennis = {}
        trim_noex = {}
        tala_ex = []

        for tri in trim_obj.browse(cursor, uid, trim_ant_ids, context):
            trim_str, anyy_str = tri.name.split('/')
            trim_name = "%s/%d" % (trim_str, int(anyy_str) + 3)
            trim_ids = trim_obj.search(cursor, uid, [('name', '=', trim_name)])
            triennis[tri.id] = trim_obj.browse(cursor, uid, trim_ids)[0]

        #Ho fem per AT i BT
        for obj in [tala_at_obj, tala_bt_obj]:

            # Busquem les tales
            tales_ids = obj.search(cursor, uid,
                                   [('trimestre', 'in', trim_ant_ids)],
                                   context=context)

            omple_zona_actuacio = hasattr(obj, 'fill_zona_actuacio')


            for tala in obj.browse(cursor, uid, tales_ids, context=context):
                # a triennis[] anem guardant els trienis per eficiència
                next_trim = triennis.get(tala.trimestre.id, False)
                if not next_trim:
                    #El trimestre no està creat, ens saltem la tala
                    tales_noex = trim_noex.get(tala.trimestre, [])
                    trim_noex.update({tala.trimestre.id: tales_noex + tala.id})
                    continue

                anyy, mes, dia = tala.data_inici.split('-')
                nova_data_inici = "%d-%s-%s" % (int(anyy) + 3, mes, dia)
                anyy, mes, dia = tala.data_final.split('-')
                nova_data_final = "%d-%s-%s" % (int(anyy) + 3, mes, dia)

                # si ja existeix, ens la saltem
                if obj.search_count(cursor, uid,
                                    [('name', '=', tala.name.id),
                                     ('trimestre', '=', next_trim.id)],
                                    context=context):
                    tala_ex.append((tala.name.name, next_trim.name))
                    continue

                trams = []

                for tram in tala.trams:
                    if not tram.name.baixa and tram.name.active:
                        trams.append((0, 0, {'name': tram.name.id,
                                             'done': 0,
                                             'longitud': tram.longitud_cad}))

                vals = {'name': tala.name.id,
                        'trimestre': next_trim.id,
                        'tipus_treball': tala.tipus_treball,
                        'data_inici': nova_data_inici,
                        'data_final': nova_data_final,
                        'trams': trams
                        }
                nova_tala_id = obj.create(cursor, uid, vals, context=context)

                # Omplim zones d'actuació (només AT)
                if omple_zona_actuacio:
                    obj.fill_zona_actuacio(cursor, uid, [nova_tala_id], None)

        # Es pot afegir info a l'acabar el wizard
        # * A 'trim_noex' tenim els trimestres que no s'han generat perquè
        #   encara no estaven creats. S'hi passen els ids de tales
        # * A 'tala_ex' tenim les tales que no s'han generat perquè ja estaven
        #   creades

        wizard.write({'state': 'done'})
        return True

    _columns = {
        'state': fields.char('State', size=16),
        'trimestre': fields.selection(_get_trimestres, string='Trimestre Inicial',
                                    help=u'Primer trimestre a partir que '
                                         u'omplirem. Agafarà sempre les dades '
                                         u'del trimestre anterior (3 anys)'),
        'trimestre_final': fields.char("Trimestre Final", size="13",
                                       help=u"Últim trimestre que s'omplirà. "
                                            u"Si no existeix, avisa (*warn*)"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'trimestre': _get_default_trimestre,
    }

GiscedataTalaOmpleTrienniWizard()
