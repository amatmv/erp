# -*- coding: utf-8 -*-

from osv import osv, fields
import datetime


class GiscedataTalaCodi(osv.osv):

    _name = "giscedata.tala.codi"
    _description = "Codis de tala per poder repartir costos"

    def name_get(self, cursor, uid, ids, context=None):
        """Retorna el name"""
        res = []

        t_vals = self.read(cursor, uid, ids, ['codi'])

        return [(t['id'], t['codi']) for t in t_vals]

    def reparteix(self, cursor, uid, ids, context=None):
        """Reparteix el cost en funció de la longitud dels trams associats al
        codi de tala"""
        if not context:
            context = {}

        trams_obj = self.pool.get('giscedata.tala.at.tram')

        for codi_vals in self.read(cursor, uid, ids, ['codi', 'cost']):
            longituds = {}

            cost_model = codi_vals['cost']

            tram_ids = trams_obj.search(cursor, uid,
                                        [('codi_tala_tram', '=',
                                          codi_vals['codi'])])
            tram_vals = trams_obj.read(cursor, uid, tram_ids, ['longitud'])
            longituds.update(dict([(t['id'], t['longitud'])
                                    for t in tram_vals]))

            #repartim
            longitud = sum([t for t in longituds.values()])
            for tram_id, tram_longitud in longituds.items():
                cost = ((tram_longitud or 0.0) * cost_model / longitud)
                trams_obj.write(cursor, uid, tram_id, {'cost': cost})

        return True

    _columns = {
        'codi': fields.char(u'Codi de tala', size=8),
        'cost': fields.float(u'Cost', digits=(8, 2)),
    }

    _defaults = {
        'cost': lambda *a: 0.0,
    }

GiscedataTalaCodi()


# extenem el model giscedata.trieni perque volem
# accedir des del trienni a les tasques de tala AT
class giscedata_trieni(osv.osv):
    _name = "giscedata.trieni"
    _inherit = "giscedata.trieni"
    _columns = {
        'tala_at': fields.one2many('giscedata.tala.at', 'trimestre', 'Tala línies AT'),
        'tala_bt': fields.one2many('giscedata.tala.bt', 'trimestre', 'Tala línies BT'),
    }
giscedata_trieni()


# extenem el model giscedata.at.linia perque volem
# accedir des dels trams AT a les tasques de tala AT
class giscedata_at_linia(osv.osv):
    _name = "giscedata.at.linia"
    _inherit = "giscedata.at.linia"
    _columns = {
		'tala_at': fields.one2many('giscedata.tala.at', 'name', 'Tasques de tala'),
    }
giscedata_at_linia()

# extenem el model giscedata.at.tram perque volem
# accedir des dels trams AT a les tasques de tala AT
class giscedata_at_linia(osv.osv):
    _name = "giscedata.at.tram"
    _inherit = "giscedata.at.tram"
    _columns = {
        'tala_at': fields.one2many('giscedata.tala.at.tram', 'name',
                                   'Tasques de tala'),
    }
giscedata_at_linia()


# extenem el model giscedata.cts perque volem
# accedir des dels CTs a les tasques de tala BT
class giscedata_cts(osv.osv):
    _name = "giscedata.cts"
    _inherit = "giscedata.cts"
    _columns = {
        'tala_bt': fields.one2many('giscedata.tala.bt', 'name', 'Tasques de tala'),
    }
giscedata_cts()


#########
# MODEL #
#########
class giscedata_tala_at(osv.osv):
    _name = "giscedata.tala.at"
    _description = "Planificació de tasques de tala per línies AT"

    def name_get(self, cursor, uid, ids, context={}):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        res = []

        for tala in self.browse(cursor, uid, ids):
            name = '%s - %s' % (tala.linia_at_nom, tala.trimestre.name)
            res.append((tala.id, name))

        return res

    def name_search(self, cursor, uid, name, args=None, operator='ilike',
                    context=None, limit=80):
        ''' Funció de cerca de nom '''
        if not args:
            args = []
        if not context:
            context = {}

        res = super(giscedata_tala_at, self).name_search(
            cursor, uid, name, args, operator, context, limit
        )

        if not res:
            search_list = ['|', ('linia_at_nom', operator, name),
                           ('trimestre.name', operator, name)]
            ids = self.search(cursor, uid, search_list, limit=limit)
            res = self.name_get(cursor, uid, ids, context=context)
        return res

    ###################################################
    # funcions que es fan servir per als camps funcio #
    ###################################################
    def _get_nom_linia_at(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_at in self.browse(cr, uid, ids):
            res[tala_at.id] = tala_at.name.descripcio
        return res

    def _search_by_nom_linia_at(self, cr, uid, obj, name, args, context=None):
        linies = self.pool.get('giscedata.at.linia').search(cr, uid, [('descripcio', args[0][1], args[0][2])])
        if not len(linies):
            return [('id', '=', '0')]
        else:
            ids = []
            for linia in self.pool.get('giscedata.at.linia').browse(cr, uid, linies):
                for tala in linia.tala_at:
                    ids.append(tala.id)
            return [('id', 'in', ids)]

    def _te_observacions(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_at in self.browse(cr, uid, ids):
            res[tala_at.id] = tala_at.observacions and len(tala_at.observacions)>0
        return res

    def _resum_observacions(self, cr, uid, ids, field_name, arg, context):
        LIMIT_RESUM_OBSERVACIONS = 50
        res = {}
        for tala_at in self.browse(cr, uid, ids):
            if tala_at.observacions:
                text = tala_at.observacions[0:LIMIT_RESUM_OBSERVACIONS]
                if len(tala_at.observacions) > LIMIT_RESUM_OBSERVACIONS:
                    text = text + "..."
            else:
                text = ''
            res[tala_at.id] = text
        return res

    def _longitud_trams(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_at in self.browse(cr, uid, ids):
            longitud = 0.0
            for tala_at_tram in tala_at.trams:
                longitud += tala_at_tram.longitud
            res[tala_at.id] = longitud
        return res

    def _ff_perc_talat(self, cursor, uid, ids, field_name, arg, context):
        ''' % talat de la tala segons longitud total i longitud dels trams.
            Utilitza la longitud dels trams guardada'''
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        res = {}.fromkeys(ids, 0)

        tram_obj = self.pool.get('giscedata.tala.at.tram')

        for tala_vals in self.read(cursor, uid, ids, ['trams', 'longitud']):
            long_total = long_talat = 0.0
            for tram_vals in tram_obj.read(cursor, uid, tala_vals['trams'],
                                           ['longitud', 'done']):
                long_total += tram_vals['longitud']
                long_talat += tram_vals['done'] and tram_vals['longitud'] or 0.0
            perc = round((long_talat / (long_total or 1.0)) * 100, 2)
            res[tala_vals['id']] = perc

        return res

    def _ff_incidencia(self, cursor, uid, ids, field_name, arg, context=None):
        ''' Si aquesta tala té algun tram amb incidència '''
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        tram_obj = self.pool.get('giscedata.tala.at.tram')

        res = {}.fromkeys(ids, False)
        for tala_vals in self.read(cursor, uid, ids, ['trams']):
            for tram_vals in tram_obj.read(cursor, uid, tala_vals['trams'],
                                           ['incidencia']):
                if tram_vals['incidencia']:
                    res[tala_vals['id']] = True
                    break

        return res

    def _ff_empresa(self, cursor, uid, ids, field_name, arg, context=None):
        """Agafa la empresa de la primera tala"""
        res = {}.fromkeys(ids, False)
        tram_obj = self.pool.get('giscedata.tala.at.tram')

        for tala_id in ids:
            trams_vals = self.read(cursor, uid, tala_id, ['trams'])
            if trams_vals:
                for tram_val in tram_obj.read(cursor, uid, trams_vals['trams'],
                                              ['empresa']):
                    if tram_val['empresa']:
                        res[tala_id] = tram_val['empresa']
                        break
        return res

    def _ff_search_empresa(self, cursor, uid, obj, name, args, context=None):
        """Busca tales que continguin trams de la empresa seleccionada"""
        tram_obj = self.pool.get('giscedata.tala.at.tram')

        tram_ids = tram_obj.search(cursor, uid, [('empresa', args[0][1],
                                                 args[0][2])])

        tram_vals = tram_obj.read(cursor, uid, tram_ids, ['tasca'])

        return [('id', 'in', [t['tasca'][0] for t in tram_vals])]

    def _ff_cost(self, cursor, uid, ids, field_name, arg, context=None):
        """Cost agregat de tots els trams"""
        res = {}.fromkeys(ids, False)
        tram_obj = self.pool.get('giscedata.tala.at.tram')

        for tala_id in ids:
            tala_vals = self.read(cursor, uid, tala_id, ['trams'])
            if tala_vals:
                tram_vals = tram_obj.read(cursor, uid, tala_vals['trams'],
                                          ['cost'])
                res[tala_id] = sum([t['cost'] for t in tram_vals])

        return res

    ###################
    # camps del model #
    ###################
    _columns = {
        'name':               fields.many2one('giscedata.at.linia', u'Línia AT', required=True),
        'trimestre':          fields.many2one('giscedata.trieni', 'Trimestre', required=True),
        'data_inici':         fields.date(u"Data d'inici planificada"),
        'data_final':         fields.date(u"Data de finalització planificada"),
        'tipus_treball':      fields.char('Treballs a realitzar', size=5, help=u"Una o més lletres entre A, B, C, D, E, F, G, H; Per exemple 'ABC' o 'BH'"),
        'trams':              fields.one2many('giscedata.tala.at.tram', 'tasca', 'Trams'),
        'linia_at_nom':       fields.function(_get_nom_linia_at, fnct_search=_search_by_nom_linia_at, method=True, type='char', size=255, string="Nom línia AT"),
        'empresa':            fields.function(_ff_empresa, type='many2one',
                                              relation='res.partner',
                                              fnct_search=_ff_search_empresa,
                                              string=u'Empresa contractada',
                                              method=True,
                                              help=u"S'agafa la associada al "
                                                   u"primer tram ple de la "
                                                   u"tala"),
        'cost':               fields.function(_ff_cost, type='float',
                                              string=u'Cost contractació',
                                              method=True, digits=(8, 2)),
        'observacions':       fields.text("Observacions"),
        'te_observacions':    fields.function(_te_observacions, method=True, type='boolean', string=u'Te observacions?'),
        'resum_observacions': fields.function(_resum_observacions, method=True, type='string', string='Observacions'),
        'longitud_trams':     fields.function(_longitud_trams, method=True, type='float', string='Longitud trams', help="Longitud dels trams associats a aquesta tasca"),
        'zona_actuacio_inici': fields.char(u"Zona d'actuació inicial", size=120),
        'zona_actuacio_final': fields.char(u"Zona d'actuació final", size=120),
        'incidencia': fields.function(_ff_incidencia, method=True,
                                      type='boolean', string=u'Incidències',
                                      help=u'Marcat si hi té algun tram amb '
                                           u'incidència de tala',),
        'perc_talat': fields.function(_ff_perc_talat, method=True, type='float',
                                      string=u'% talat'),
        'supervisor': fields.many2one('res.users', u'Tècnic Supervisor',
                                      help=u'Tècnic propi que supervisa els'
                                           u'treballs. Ha de ser un usuari de '
                                           u'l\'ERP'),
    }

    _sql_constraints = [('tipus_treball_valid_values', "CHECK (tipus_treball ~* '^a?b?c?d?e?f?g?h?$')", "El camp 'treballs a realitzar' ha de tenir les lletres que calguin entre aquestes: { A, B, C, D, E, F, G, H } i aquestes han d'estar en ordre alfabètic, sense espais ni comes ni cap altre caracter, per exemple 'AB', 'H', 'CFH'; També pot estar buit.")]

    _order = "trimestre asc, name asc"

    ##########################################################
    # funcions que es fan servir des d'accions de les vistes #
    ##########################################################
    def afegir_tots_els_trams(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for tala in self.browse(cr, uid, ids):
            trams = []
            trams_actuals = []
            for tram in tala.trams:
                trams_actuals.append(tram.name.id)
            for tram in tala.name.trams:
                if tram.id not in trams_actuals:
                    if tram.tipus == 1: # 1: aeri, 2:subterrani
                        codi_tala = tram.codi_tala.codi
                        trams.append((0, 0, {'name': tram.id, 'done': 0,
                                             'codi_tala_tram': codi_tala,
                                             }))
            self.write(cr, uid, [tala.id], {'trams': trams})
        return True

    def marcar_tots_els_trams_talats(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for tala in self.browse(cr, uid, ids):
            for tram in tala.trams:
                if not tram.done:
                    tram.write({'done': True,
                                'data_execucio': datetime.date.today()})
        return True

    def fill_zona_actuacio(self, cr, uid, ids, context=None):
        """Emplena els camps "zona d'actuació" amb l'origen i final de la
        línia AT"""
        if context is None:
            context = {}

        for tala in self.browse(cr, uid, ids):
            a = tala.zona_actuacio_inici
            if(not a or len(a)==0):
                a = tala.name.origen
            b = tala.zona_actuacio_final
            if(not b or len(b)==0):
                b = tala.name.final
            tala.write({'zona_actuacio_inici': a, 'zona_actuacio_final': b})
        return True

giscedata_tala_at()
################
# FI DEL MODEL #
################


#########
# MODEL #
#########
class giscedata_tala_at_tram(osv.osv):
    _name = "giscedata.tala.at.tram"
    _description = "Planificació de tasques de tala per trams de línies AT"

    def _get_nom_tram_at(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_at_tram in self.browse(cr, uid, ids):
            res[tala_at_tram.id] = "{0} - {1}".format(
                tala_at_tram.name.origen, tala_at_tram.name.final
            )
        return res

    def _te_observacions(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_at_tram in self.browse(cr, uid, ids):
            res[tala_at_tram.id] = tala_at_tram.observacions and len(tala_at_tram.observacions)>0
        return res

    def _resum_observacions(self, cr, uid, ids, field_name, arg, context):
        LIMIT_RESUM_OBSERVACIONS = 50
        res = {}
        for tala_at_tram in self.browse(cr, uid, ids):
            if tala_at_tram.observacions:
                text = tala_at_tram.observacions[0:LIMIT_RESUM_OBSERVACIONS]
                if len(tala_at_tram.observacions) > LIMIT_RESUM_OBSERVACIONS:
                    text = text + "..."
            else:
                text = ''
            res[tala_at_tram.id] = text
        return res

    def _get_longitud(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = {}
        for tala_at_tram in self.browse(cr, uid, ids):
            res[tala_at_tram.id] = tala_at_tram.name.longitud_cad

        return res

    def _get_ids_from_self(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        return ids

    def _get_ids_from_at(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        tram_tala_at_obj = self.pool.get('giscedata.tala.at.tram')

        return tram_tala_at_obj.search(
            cr, uid, [('name', 'in', ids), ('done', '=', False)]
        )

    def _get_parceles_by_tram(self, cursor, uid, ids, name, arg, context=None):

        ret = {}
        ids_tram = []
        map_tram_tala = {}
        for id in ids:
            a = self.read(cursor, uid, id, ['name'])
            ids_tram.append(a['name'][0])
            map_tram_tala[a['name'][0]] = id

        if len(ids_tram) == 0:
            return ret

        sql = """
         SELECT tram_id, parcela_id 
         FROM giscedata_parcela_tram_rel
         WHERE tram_id in %s;
        """

        cursor.execute(sql, (tuple(ids_tram),))

        for res in cursor.fetchall():
            tram = map_tram_tala[res[0]]
            parcela = res[1]
            ret.setdefault(tram, []).append(parcela)
        return ret

    _columns = {
        'name':               fields.many2one('giscedata.at.tram',
                                              u'Tram de línia AT',
                                              required=True),
        'tasca':              fields.many2one('giscedata.tala.at',
                                              'Tasca de tala',
                                              required=True),
        'done':               fields.boolean('Talat?'),
        'data_execucio':      fields.date(u"Data d'execució"),
        'observacions':       fields.text("Observacions"),
        'tram_at_nom':        fields.function(_get_nom_tram_at, method=True,
                                              type='string',
                                              string='Nom tram AT'),
        'te_observacions':    fields.function(_te_observacions, method=True,
                                              type='boolean',
                                              string=u'Te observacions?'),
        'resum_observacions': fields.function(_resum_observacions, method=True,
                                              type='string',
                                              string='Observacions'),
        'longitud': fields.function(_get_longitud, method=True,
                                    type='float', digits=(10, 2),
                                    string='Longitud',
                                    store=
                                    {'giscedata.at.tram':
                                         (_get_ids_from_at,
                                          ['longitud_cad'], 10),
                                     'giscedata.tala.at.tram':
                                         (_get_ids_from_self, ['name'], 10),
                                     }),
        'incidencia':         fields.boolean(u'Incidència',
                                             help=u'Marcat si hi ha hagut una '
                                                  u'incidència durant la '
                                                  u'realització de les tasques '
                                                  u'de tala. Els detalls es'
                                                  u' poden veure al camp de '
                                                  u'les observacions.'),
        'codi_tala_tram': fields.char(u'Codi de tala', size=10,
                                      help=u'Codi de tala per referència'),
        'plano': fields.char(u'Plano', size=32, help=u'Referència al Plano on '
                                                     u'hi ha el tram'),
        'empresa': fields.many2one('res.partner', u'Empresa contractada',
                                    domain="[('supplier', '=' ,True)]",
                                    help=u"Només empreses marcades com a "
                                         u"proveïdor. Si una empresa no "
                                         u"apareix, assegura't que és "
                                         u"proveïdor"),
        'cost': fields.float(u'Cost contractació', digits=(8, 2)),
        "parceles": fields.function(_get_parceles_by_tram, type="many2many", method=True, relation="giscegis.catastre.parceles", string="Parcel·les afectades"),
    }

    _defaults = {
        'done': lambda *a: 0,
        'incidencia': lambda *a: 0,
    }

    _order = "tasca asc, name asc"

    _sql_constraints = [('done_if_execution_date', 'CHECK (data_execucio IS NULL AND NOT done OR data_execucio IS NOT NULL AND done)', "Si per a un tram s'entra la data d'execució s'ha de marcar com a executat, i si no es marca com executat no es pot entrar una data d'execució.")]

giscedata_tala_at_tram()
################
# FI DEL MODEL #
################


#########
# MODEL #
#########
class giscedata_tala_bt(osv.osv):
    _name = "giscedata.tala.bt"
    _description = "Planificació de tasques de tala per línies BT"

    def name_get(self, cursor, uid, ids, context={}):
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        res = []

        for tala in self.browse(cursor, uid, ids):
            name = '%s - %s' % (tala.name.name, tala.trimestre.name)
            res.append((tala.id, name))

        return res

    def name_search(self, cursor, uid, name, args=None, operator='ilike',
                    context=None, limit=80):
        ''' Funció de cerca de nom '''
        if not args:
            args = []
        if not context:
            context = {}

        ids = []

        if not ids:
            search_list = ['|', ('name.name', operator, name),
                           ('trimestre.name', operator, name)]
            ids = self.search(cursor, uid, search_list, limit=limit)

        return self.name_get(cursor, uid, ids, context=context)


    ###################################################
    # funcions que es fan servir per als camps funcio #
    ###################################################
    def _te_observacions(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            res[tala_bt.id] = tala_bt.observacions and len(tala_bt.observacions)>0
        return res

    def _resum_observacions(self, cr, uid, ids, field_name, arg, context):
        LIMIT_RESUM_OBSERVACIONS = 50
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            if tala_bt.observacions:
                text = tala_bt.observacions[0:LIMIT_RESUM_OBSERVACIONS]
                if len(tala_bt.observacions) > LIMIT_RESUM_OBSERVACIONS:
                    text = text + "..."
            else:
                text = ''
            res[tala_bt.id] = text
        return res

    def _longitud_linies(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            res[tala_bt.id] = tala_bt.name.longitud_bt
        return res

    def _longitud_trams(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            longitud = 0.0
            for tala_bt_tram in tala_bt.trams:
                longitud += tala_bt_tram.longitud
            res[tala_bt.id] = longitud
        return res

    def _tensio_ficticia(self, cr, uid, ids, field_name, arg, context):
        """Aquesta funció agafa els 3 primers caracters del camp "giscedata_cts.tensio_s" i
        els intenta interpretar com enters; Si s'aconsegueix un enter de 3 digits, es retorna
        aquest valor. En qualsevol altre cas es retorna "". El camp "giscedata_cts.tensio"
        es una cadena de caracters, i sembla que es costum habitual que els clients entrin
        una llista de tensions (en Volts) separades pel caracter "/" (forward slash), per
        exemple "420/380/220"."""
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            ts = tala_bt.name.tensio_s
            if ts and len(ts)>=3:
                ts = int(ts[0:3]) # els tres primers caracters del camp (p.ex. "420/380/220")
            else:
                ts = "" # si no aconseguim que el parse "tingui exit" deixem això que hauria de cridar l'atenció quan es miri l'informe.
                print "La tensió de secundari al CT %s no te el format que s'espera a l'informe de tala BT: " % tala_bt.name.name
            res[tala_bt.id] = ts
        return res

    def _coordenades_ct(self, cr, uid, ids, field_name, arg, context):
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            # busquem els giscegis_blocs_ctat que tenen el camp ct l'id que tenim en aquest tala_bt.name.id
            # hauriem d'aconseguir exactament zero o un id's -- si ve mes d'un agafarem el primer -- assertions anyone??
            ids_giscegis_blocs_ctat = self.pool.get('giscegis.blocs.ctat').search(cr, uid, [('ct', '=', tala_bt.name.id)])
            giscegis_blocs_ctat = self.pool.get('giscegis.blocs.ctat').browse(cr, uid, ids_giscegis_blocs_ctat)
            if len(giscegis_blocs_ctat)>0:
                giscegis_bloc_ctat = giscegis_blocs_ctat[0] # ara tenim l'id de l'element giscegis_blocs_ctat
                vertex = giscegis_bloc_ctat.vertex
                res[tala_bt.id] = "%s;%s" % (vertex.x, vertex.y)
            else:
                res[tala_bt.id] = "N/D" # no tenim aquesta informació!
        return res

    def afegir_tots_els_trams(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        for tala in self.browse(cr, uid, ids):
            trams = []
            trams_actuals = []
            for tram in tala.trams:
                trams_actuals.append(tram.name.id)
            for tram in tala.name.elements_bt:
                if tram.id not in trams_actuals:
                    if tram.tipus_linia and tram.tipus_linia.name[0] == 'A':
                        if tram.cable and tram.cable.name.startswith('EMBARR'):
                            continue
                        codi_tala = tala.codi_tala.codi
                        trams.append((0, 0, {'name': tram.id, 'done': 0,
                                             'codi_tala_tram': codi_tala,
                                             }))
            self.write(cr, uid, [tala.id], {'trams': trams})
        return True

    def marcar_tots_els_trams_talats(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        for tala in self.browse(cr, uid, ids):
            for tram in tala.trams:
                if not tram.done:
                    tram.write({'done': 1,
                                'data_execucio': datetime.date.today()}
                               )
        return True

    def _ff_perc_talat(self, cursor, uid, ids, field_name, arg, context):
        ''' % talat de la tala segons longitud total i longitud dels trams.
            Utilitza la longitud dels trams guardada'''
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        res = {}.fromkeys(ids, 0)

        tram_obj = self.pool.get('giscedata.tala.bt.tram')

        for tala_vals in self.read(cursor, uid, ids, ['trams', 'longitud']):
            long_total = long_talat = 0.0
            for tram_vals in tram_obj.read(cursor, uid, tala_vals['trams'],
                                           ['longitud', 'done']):
                long_total += tram_vals['longitud']
                long_talat += tram_vals['done'] and tram_vals['longitud'] or 0.0
            perc = round((long_talat / (long_total or 1.0)) * 100, 2)
            res[tala_vals['id']] = perc

        return res


    def _ff_incidencia(self, cursor, uid, ids, field_name, arg, context=None):
        ''' Si aquesta tala té algun tram amb incidència '''
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        tram_obj = self.pool.get('giscedata.tala.bt.tram')

        res = {}.fromkeys(ids, False)
        for tala_vals in self.read(cursor, uid, ids, ['trams']):
            for tram_vals in tram_obj.read(cursor, uid, tala_vals['trams'],
                                           ['incidencia']):
                if tram_vals['incidencia']:
                    res[tala_vals['id']] = True
                    break

        return res

    def _ff_empresa(self, cursor, uid, ids, field_name, arg, context=None):
        """Agafa la empresa de la primera tala"""
        res = {}.fromkeys(ids, False)
        tram_obj = self.pool.get('giscedata.tala.bt.tram')

        for tala_id in ids:
            trams_vals = self.read(cursor, uid, tala_id, ['trams'])
            if trams_vals:
                for tram_val in tram_obj.read(cursor, uid, trams_vals['trams'],
                                              ['empresa']):
                    if tram_val['empresa']:
                        res[tala_id] = tram_val['empresa']
                        break
        return res

    def _ff_search_empresa(self, cursor, uid, obj, name, args, context=None):
        """Busca tales que continguin trams de la empresa seleccionada"""
        tram_obj = self.pool.get('giscedata.tala.bt.tram')

        tram_ids = tram_obj.search(cursor, uid, [('empresa', args[0][1],
                                                 args[0][2])])

        tram_vals = tram_obj.read(cursor, uid, tram_ids, ['tasca'])

        return [('id', 'in', [t['tasca'][0] for t in tram_vals])]

    def _ff_cost(self, cursor, uid, ids, field_name, arg, context=None):
        """Cost agregat de tots els trams"""
        res = {}.fromkeys(ids, False)
        tram_obj = self.pool.get('giscedata.tala.bt.tram')

        for tala_id in ids:
            tala_vals = self.read(cursor, uid, tala_id, ['trams'])
            if tala_vals:
                tram_vals = tram_obj.read(cursor, uid, tala_vals['trams'],
                                          ['cost'])
                res[tala_id] = sum([t['cost'] for t in tram_vals])

        return res

    ###################
    # camps del model #
    ###################
    _columns = {
        'name':               fields.many2one('giscedata.cts', 'CT',
                                              required=True),
        'trimestre':          fields.many2one('giscedata.trieni', 'Trimestre',
                                              required=True),
        'data_inici':         fields.date("Data d'inici planificada"),
        'data_final':         fields.date(u"Data de finalització planificada"),
        'tipus_treball':      fields.char('Treballs a realitzar', size=5,
                                          help=u"Una o més lletres entre A, B,"
                                               u" C, D, E, F, G, H; Per "
                                               u"exemple 'ABC' o 'BH'"),
        'empresa':            fields.function(_ff_empresa, type='many2one',
                                              relation='res.partner',
                                              fnct_search=_ff_search_empresa,
                                              string=u'Empresa contractada',
                                              method=True,
                                              help=u"S'agafa la associada al "
                                                   u"primer tram ple de la "
                                                   u"tala"),
        'cost':               fields.float(u'Cost contractació', digits=(8, 2)),
        'done':               fields.boolean(u'Talat?'),
        'data_execucio':      fields.date(u"Data d'execució"),
        'trams':              fields.one2many('giscedata.tala.bt.tram',
                                              'tasca', 'Trams'),
        'observacions':       fields.text("Observacions"),
        'te_observacions':    fields.function(_te_observacions, method=True,
                                              type='boolean',
                                              string=u'Te observacions?'),
        'resum_observacions': fields.function(_resum_observacions, method=True,
                                              type='string',
                                              string='Observacions'),
        'longitud_linies':    fields.function(_longitud_linies, method=True,
                                              type='float',
                                              string=u'Longitud línies',
                                              help=u"Longitud de les línies "
                                                   u"associades a aquest CT"),
        'longitud_trams':     fields.function(_longitud_trams, method=True,
                                              type='float',
                                              string='Longitud trams',
                                              help=u"Longitud dels trams "
                                                   u"associats a "
                                                   u"aquesta tasca"),
        'tensio_ficticia':    fields.function(_tensio_ficticia, method=True,
                                              type='float',
                                              string='HIDE ME PLEASE!'),
        'coordenades':        fields.function(_coordenades_ct, method=True,
                                              type='float',
                                              string='Coordenades del CT'),
        'incidencia': fields.function(_ff_incidencia, method=True,
                                      type='boolean', string=u'Incidències',
                                      help=u'Marcat si hi té algun tram amb '
                                           u'incidència de tala',),
        'perc_talat': fields.function(_ff_perc_talat, method=True, type='float',
                                      string=u'% talat'),
        'supervisor': fields.many2one('res.users', u'Tècnic Supervisor',
                                      help=u'Tècnic propi que supervisa els '
                                           u'treballs. Ha de ser un usuari de '
                                           u'l\'ERP'),
        'codi_tala': fields.many2one('giscedata.tala.codi', u'Codi de tala'),
    }

    _defaults = {'cost': lambda *a: 0.0}

    _sql_constraints = [
        ('tipus_treball_valid_values',
         "CHECK (tipus_treball ~* '^a?b?c?d?e?f?g?h?$')",
         "El camp 'treballs a realitzar' ha de tenir les lletres que calguin "
         "entre aquestes: { A, B, C, D, E, F, G, H } i aquestes han d'estar en "
         "ordre alfabètic, sense espais ni comes ni cap altre caracter, per "
         "exemple 'AB', 'H', 'CFH'; També pot estar buit."),
        ('done_if_execution_date',
         'CHECK ((data_execucio IS NULL AND NOT done) '
         '  OR (data_execucio IS NOT NULL AND done))',
         "Si per a un CT s'entra la data d'execució s'ha de marcar com a "
         "executat, i si no es marca com executat no es pot entrar una "
         "data d'execució."),
    ]

    _order = "trimestre asc, name asc"

giscedata_tala_bt()
################
# FI DEL MODEL #
################


class GiscedataTalaBtTram(osv.osv):
    _name = "giscedata.tala.bt.tram"
    _description = "Planificació de tasques de tala per trams de línies BT"

    def _get_nom_tram_bt(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = {}
        for bt_tram in self.browse(cr, uid, ids):
            vals = []
            if bt_tram.name.cable:
                vals.append(bt_tram.name.cable.name)
            if bt_tram.name.situacio:
                vals.append(bt_tram.name.situacio)
            res[bt_tram.id] = ' - '.join([str(x).strip() for x in vals])
        return res

    def _te_observacions(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            res[tala_bt.id] = bool(tala_bt.observacions)
        return res

    def _resum_observacions(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        LIMIT_RESUM_OBSERVACIONS = 50
        res = {}
        for tala_bt in self.browse(cr, uid, ids):
            if tala_bt.observacions:
                text = tala_bt.observacions[0:LIMIT_RESUM_OBSERVACIONS]
                if len(tala_bt.observacions) > LIMIT_RESUM_OBSERVACIONS:
                    text += "..."
            else:
                text = ''
            res[tala_bt.id] = text
        return res

    def _get_longitud(self, cr, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        res = {}
        for tala_bt_tram in self.browse(cr, uid, ids):
            res[tala_bt_tram.id] = tala_bt_tram.name.longitud_cad

        return res

    def _get_ids_from_self(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        return ids

    def _get_ids_from_bt(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        tram_tala_bt_obj = self.pool.get('giscedata.tala.bt.tram')

        return tram_tala_bt_obj.search(
            cr, uid, [('name', 'in', ids), ('done', '=', False)]
        )

    _columns = {
        'name':               fields.many2one('giscedata.bt.element',
                                              u'Tram de línia BT',
                                              required=True),
        'tasca':              fields.many2one('giscedata.tala.bt',
                                              'Tasca de tala', required=True),
        'done':               fields.boolean(u'Talat?'),
        'data_execucio':      fields.date(u"Data d'execució"),
        'tram_bt_nom':        fields.function(_get_nom_tram_bt, method=True,
                                              type='string',
                                              string=u'Descripció tram BT'),
        'observacions':       fields.text("Observacions"),
        'te_observacions':    fields.function(_te_observacions, method=True,
                                              type='boolean',
                                              string=u'Te observacions?'),
        'resum_observacions': fields.function(_resum_observacions, method=True,
                                              type='string',
                                              string='Observacions'),
        'longitud': fields.function(_get_longitud, method=True, type='float',
                                    string='Longitud',
                                    store=
                                    {'giscedata.bt.element':
                                         (_get_ids_from_bt,
                                          ['longitud_cad'], 10),
                                     'giscedata.tala.bt.tram':
                                         (_get_ids_from_self, ['name'], 10),
                                     }),
        'incidencia': fields.boolean(u'Incidència',
                                     help=u'Marcat si hi ha hagut una '
                                          u'incidència durant la '
                                          u'realització de les tasques '
                                          u'de tala. Els detalls es'
                                          u' poden veure al camp de '
                                          u'les observacions.'),
        'codi_tala_tram': fields.char(u'Codi de tala', size=10,
                                      help=u'Codi de tala per referència'),
        'plano': fields.char(u'Plano', size=32, help=u'Referència al Plano on '
                                                     u'hi ha el tram'),
        'empresa': fields.many2one('res.partner', u'Empresa contractada',
                                    domain="[('supplier', '=' ,True)]",
                                    help=u"Només empreses marcades com a "
                                         u"proveïdor. Si una empresa no "
                                         u"apareix, assegura't que és "
                                         u"proveïdor"),
    }

    _defaults = {
        'done': lambda *a: 0,
        'incidencia': lambda *a: 0,
    }

    _order = "tasca asc, name asc"

    _sql_constraints = [
        ('done_if_execution_date',

         'CHECK (data_execucio IS NULL AND NOT done '
         'OR data_execucio IS NOT NULL AND done)',

         "Si per a un tram s'entra la data d'execució s'ha de marcar com a "
         "executat, i si no es marca com executat no es pot entrar una data "
         "d'execució.")
    ]

GiscedataTalaBtTram()
