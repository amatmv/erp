# -*- coding: utf-8 -*-

from destral import testing
from destral.transaction import Transaction


class BaseTests(testing.OOTestCase):
    def test_changing_tram_lenght_updates_tala_lenght_for_at(self):
        tram_obj = self.openerp.pool.get('giscedata.at.tram')
        tala_obj = self.openerp.pool.get('giscedata.tala.at')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tram_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_at', 'at_tram_0001'
            )[1]
            tram = tram_obj.browse(cursor, uid, tram_id)

            tala_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tala', 'tala_at_0001'
            )[1]
            tala_obj.afegir_tots_els_trams(cursor, uid, tala_id)
            tala = tala_obj.browse(cursor, uid, tala_id)
            # Tala should only have one tram or test would need to be recoded
            self.assertEqual(len(tala.trams), 56, "El número de trams no coincideix")
            tram_tala = tala.trams[0]

            self.assertEqual(tram.longitud_cad * len(tala.trams), tala.longitud_trams,
                             "La longitud dels trams AT no coincideix")
            self.assertEqual(tram.longitud_cad, tram_tala.longitud,
                             "La longitut del tram AT de tala no és igual. tram:{} tram_tala:{}".format(
                                 tram.longitud_cad, tram_tala.longitud))

            longitut_original = tram.longitud_cad
            nova_longitud = tram.longitud_cad + 50

            tram_obj.write(
                cursor, uid, tram_id, {'longitud_cad': nova_longitud}
            )
            tram = tram_obj.browse(cursor, uid, tram_id)
            tala = tala_obj.browse(cursor, uid, tala_id)
            tram_tala = tala.trams[0]

            self.assertEqual(tram.longitud_cad, nova_longitud,
                             "El número de trams no coincideix amb la nova longitud tram:{} nova_longitut:{}".format(tram.longitud_cad, nova_longitud))

            assert tram.longitud_cad + (longitut_original*(len(tala.trams)-1)) == tala.longitud_trams
            self.assertEqual(tram.longitud_cad, nova_longitud,
                             "El número de trams no coincideix amb el tram_tala.longitud tram:{} tram_tala.longitud:{}".format(
                                 tram.longitud_cad, tram_tala.longitud))

    def test_changing_tram_lenght_updates_tala_lenght_for_bt(self):
        tram_obj = self.openerp.pool.get('giscedata.bt.element')
        tala_obj = self.openerp.pool.get('giscedata.tala.bt')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tram_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cts', 'ct1'
            )[1]
            tram = tram_obj.browse(cursor, uid, tram_id)

            tala_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tala', 'tala_bt_0001'
            )[1]
            tala_obj.afegir_tots_els_trams(cursor, uid, tala_id)
            tala = tala_obj.browse(cursor, uid, tala_id)
            a = tala.trams
            # Tala should only have one tram or test would need to be recoded
            self.assertEqual(len(tala.trams), 0, "El número de trams BT no coincideix tala.trams:{} num: 0".format(len(tala.trams)))
            if len(tala.trams) == 0:
                self.assertEqual(tram.longitud_cad, tala.longitud_trams,
                                 "La longitut de trams BT de tala no és igual. tram:{} tala_longitud:{}".format(
                                     tram.longitud_cad, tala.longitud_trams))
            else:
                tram_tala = tala.trams[0]
                # assert tram.longitud_cad == tala.longitud_trams
                self.assertEqual(tram.longitud_cad, tram_tala.longitud, "La longitut del tram BT de tala no és igual. tram:{} tram_tala:{}".format(tram.longitud_cad, tram_tala.longitud))
                # assert tram.longitud_cad == tram_tala.longitud

                nova_longitud = tram.longitud_cad + 50

                tram_obj.write(
                    cursor, uid, tram_id, {'longitud_cad': nova_longitud}
                )
                tram = tram_obj.browse(cursor, uid, tram_id)
                tala = tala_obj.browse(cursor, uid, tala_id)
                tram_tala = tala.trams[0]

                assert tram.longitud_cad == nova_longitud
                assert tram.longitud_cad == tala.longitud_trams
                assert tram.longitud_cad == tram_tala.longitud

    def test_change_tram_len_for_done_tala_doesnt_update_tala_len_at(self):
        tram_obj = self.openerp.pool.get('giscedata.at.tram')
        tala_obj = self.openerp.pool.get('giscedata.tala.at')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tram_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_at', 'at_tram_0001'
            )[1]
            tram = tram_obj.browse(cursor, uid, tram_id)

            tala_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tala', 'tala_at_0001'
            )[1]
            tala_obj.afegir_tots_els_trams(cursor, uid, tala_id)
            tala = tala_obj.browse(cursor, uid, tala_id)
            a = tala.trams
            # Tala should only have one tram or test would need to be recoded
            self.assertEqual(len(tala.trams), 56, "El número de trams no coincideix")
            tram_tala = tala.trams[0]
            tram_tala.write({'done': True, 'data_execucio': '2016/10/20'})

            self.assertEqual(tram.longitud_cad * len(tala.trams), tala.longitud_trams, "La longitud dels trams AT no coincideix")
            # assert tram.longitud_cad == tala.longitud_trams
            self.assertEqual(tram.longitud_cad, tram_tala.longitud, "La longitut del tram AT de tala no és igual. tram:{} tram_tala:{}".format(tram.longitud_cad, tram_tala.longitud))
            # assert tram.longitud_cad == tram_tala.longitud

            nova_longitud = tram.longitud_cad + 50

            tram_obj.write(
                cursor, uid, tram_id, {'longitud_cad': nova_longitud}
            )
            tram = tram_obj.browse(cursor, uid, tram_id)
            new_tala = tala_obj.browse(cursor, uid, tala_id)
            new_tram_tala = tala.trams[0]

            # We check that despite changing the tram's lenght the other
            # lengths don't change (because it's already done)
            assert tram.longitud_cad == nova_longitud
            assert tala.longitud_trams == new_tala.longitud_trams
            assert tram_tala.longitud == new_tram_tala.longitud

    def test_change_tram_len_for_done_tala_doesnt_update_tala_len_bt(self):
        tram_obj = self.openerp.pool.get('giscedata.bt.element')
        tala_obj = self.openerp.pool.get('giscedata.tala.bt')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tram_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cts', 'ct1'
            )[1]
            tram = tram_obj.browse(cursor, uid, tram_id)

            tala_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tala', 'tala_bt_0001'
            )[1]
            tala_obj.afegir_tots_els_trams(cursor, uid, tala_id)
            tala = tala_obj.browse(cursor, uid, tala_id)
            a = tala.trams
            # Tala should only have one tram or test would need to be recoded
            self.assertEqual(len(tala.trams), 0, "El número de trams no coincideix")
            if len(tala.trams) >0:
                tram_tala = tala.trams[0]
                tram_tala.write({'done': True, 'data_execucio': '2016/10/20'})
                tala.write({'done': True, 'data_execucio': '2016/10/20'})

                assert tram.longitud_cad == tala.longitud_trams
                assert tram.longitud_cad == tram_tala.longitud

                nova_longitud = tram.longitud_cad + 50

                tram_obj.write(
                    cursor, uid, tram_id, {'longitud_cad': nova_longitud}
                )
                tram = tram_obj.browse(cursor, uid, tram_id)
                new_tala = tala_obj.browse(cursor, uid, tala_id)
                new_tram_tala = tala.trams[0]

                # We check that despite changing the tram's lenght the other
                # lengths don't change (because it's already done)
                assert tram.longitud_cad == nova_longitud
                assert tala.longitud_trams == new_tala.longitud_trams
                assert tram_tala.longitud == new_tram_tala.longitud
