# -*- coding: utf-8 -*-
import unittest
from expects import expect, contain
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from tools import cache


class TestImporting(testing.OOTestCase):
    def switch(self, txn, where):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        other_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_agrolait'
        )[1]
        another_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        codes = {'distri': '1234', 'comer': '4321'}
        partner_obj.write(cursor, uid, [partner_id], {
            'ref': codes.pop(where)
        })
        partner_obj.write(cursor, uid, [other_id], {
            'ref': codes.values()[0]
        })
        partner_obj.write(cursor, uid, [another_id], {
            'ref': '5555'
        })
        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_01'
        )[1]
        distri_ids = {'distri': partner_id, 'comer': other_id}
        cups_obj.write(cursor, uid, [cups_id], {
            'distribuidora_id': distri_ids[where]
        })
        cache.clean_caches_for_db(cursor.dbname)

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def crear_modcon(self, txn, potencia, ini, fi):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    @unittest.skip("Won't work due to cursor change in from_xml")
    def test_f1_from_xml_imports_adjustments(self):
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lect_compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
        lect_ene_obj = self.openerp.pool.get(
            'giscedata.facturacio.lectures.energia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests',
            'fixtures', 'F1_ajuste.xml'
        )
        with open(f1_xml_path, 'r') as f:
            f1_xml = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cursor.execute(
                "insert into "
                "giscedata_polissa_tarifa_pricelist(tarifa_id, pricelist_id) "
                "select "
                "    a.id, "
                "    ( "
                "      select id from product_pricelist "
                "      where name = 'TARIFAS ELECTRICIDAD'"
                "    ) "
                "from ( "
                "    select DISTINCT id "
                "    from giscedata_polissa_tarifa "
                ") a "
            )

            self.switch(txn, 'comer')
            self.activar_polissa_CUPS(txn)
            self.crear_modcon(txn, 10, '2017-01-01', '2020-12-31')

            llista_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            partner_obj.write(
                cursor, uid, other_id, {
                    'property_product_pricelist_purchase': llista_id
                }
            )

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            res = fact_obj.from_xml(
                cursor, uid, f1_xml, 'F1_ajuste.xml', '1234'
            )

            search_params = [
                ('polissa.cups.name', '=', u'ES1234000000000001JN0F'),
            ]
            lect_compt_id = lect_compt_obj.search(cursor, uid, search_params)

            lect_ids = lect_obj.search(
                cursor, uid, [('comptador', 'in', lect_compt_id)]
            )

            assert lect_ids
            params = ['ajust', 'motiu_ajust']
            for lectura in lect_obj.read(cursor, uid, lect_ids, params):
                assert lectura['ajust'] == 678
                assert lectura['motiu_ajust'] == '99'

            lect_ene_ids = lect_ene_obj.search(
                cursor, uid, [('comptador_id', 'in', lect_compt_id)]
            )

            assert lect_ene_ids
            params = ['ajust', 'motiu_ajust']
            for lectura in lect_obj.read(cursor, uid, lect_ene_ids, params):
                assert lectura['ajust'] == 678
                assert lectura['motiu_ajust'] == '99'

    @unittest.skip("Won't work due to cursor change in from_xml")
    def test_f1_from_xml_imports_ajusts_to_0_by_default(self):
        lot_obj = self.openerp.pool.get('giscedata.facturacio.lot')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        lect_compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures', 'F1.xml'
        )
        with open(f1_xml_path, 'r') as f:
            f1_xml = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cursor.execute(
                "insert into "
                "giscedata_polissa_tarifa_pricelist(tarifa_id, pricelist_id) "
                "select "
                "    a.id, "
                "    ( "
                "      select id from product_pricelist "
                "      where name = 'TARIFAS ELECTRICIDAD'"
                "    ) "
                "from ( "
                "    select DISTINCT id "
                "    from giscedata_polissa_tarifa "
                ") a "
            )

            self.switch(txn, 'comer')
            self.activar_polissa_CUPS(txn)
            self.crear_modcon(txn, 10, '2017-01-01', '2020-12-31')

            llista_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            partner_obj.write(
                cursor, uid, other_id, {
                    'property_product_pricelist_purchase': llista_id
                }
            )

            lot_obj.crear_lots_mensuals(cursor, uid, 2017)

            res = fact_obj.from_xml(
                cursor, uid, f1_xml, 'F1_ajuste.xml', '1234'
            )

            search_params = [
                ('polissa.cups.name', '=', u'ES1234000000000001JN0F'),
            ]
            lect_compt_id = lect_compt_obj.search(cursor, uid, search_params)

            lect_ids = lect_obj.search(
                cursor, uid, [('comptador', 'in', lect_compt_id)]
            )

            assert lect_ids
            params = ['ajust', 'motiu_ajust']
            for lectura in lect_obj.read(cursor, uid, lect_ids, params):
                assert lectura['ajust'] == 0
                assert not lectura['motiu_ajust']


class TestsFacturesValidation(testing.OOTestCase):
    def setUp(self):
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        line_obj = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        warn_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.warning.template'
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        ctx = {'active_test': False}

        # We make sure that all warnings are active
        warn_ids = warn_obj.search(
            cursor, uid, [], context=ctx
        )
        warn_obj.write(cursor, uid, warn_ids, {'active': True})

    def tearDown(self):
        self.txn.stop()

    def test_validation_FC01_pass(self):
        vali_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.validator'
        )
        warn_obj = self.openerp.pool.get(
            'giscedata.facturacio.validation.warning'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0006'
        )[1]

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).not_to(contain('FC01'))

    def test_validation_FC01(self):
        vali_obj = self.openerp.pool.get('giscedata.facturacio.validation.validator')
        warn_obj = self.openerp.pool.get('giscedata.facturacio.validation.warning')
        factura_o = self.openerp.pool.get('giscedata.facturacio.factura')
        linia_o = self.openerp.pool.get('giscedata.facturacio.factura.linia')
        f1_o = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
        f1_extra_o = self.openerp.pool.get('giscedata.facturacio.importacio.linia.extra')
        extra_o = self.openerp.pool.get('giscedata.facturacio.extra')
        imd_obj = self.openerp.pool.get('ir.model.data')
        cursor = self.txn.cursor
        uid = self.txn.user

        fact_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_facturacio', 'factura_0006')[1]
        line_id = linia_o.search(cursor, uid, [('factura_id', '=', fact_id)])[0]
        f1_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_facturacio_switching', 'line_01_f1_import_01')[1]
        extra_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_facturacio', 'facturacio_extraline_01')[1]

        # Vinculem la linia extra a la linia de factura
        extra_o.write(cursor, uid, extra_id, {'factura_ids': [(6, 0, [fact_id])], 'factura_linia_ids': [(6, 0, [line_id])]})
        # Vinculem la linia extra al F1
        f1_extra_o.create(cursor, uid, {'extra_id': extra_id, 'linia_id': f1_id})
        # Marquem el F1 com a tipus "Complementari"
        f1_o.write(cursor, uid, f1_id, {'type_factura': 'C'})

        warning_ids = vali_obj.validate_invoice(cursor, uid, fact_id)

        warning_vals = warn_obj.read(
            cursor, uid, warning_ids,
            ['name', 'warning_template_id', 'factura_id']
        )

        warning_names = [warn['name'] for warn in warning_vals]
        expect(warning_names).to(contain('FC01'))
