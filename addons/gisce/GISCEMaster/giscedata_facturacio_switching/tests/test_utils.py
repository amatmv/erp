# -*- coding: utf-8 -*-
from __future__ import absolute_import
import unittest

from expects import *
from addons import get_module_resource
from ..giscedata_facturacio_switching_utils import (
    get_cups_from_xml, get_filename
)


class TestGiscedataFacturacioSwitching(unittest.TestCase):

    def test_get_cups_from_xml(self):
        valid_cups_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_utils.xml'
        )
        with open(valid_cups_xml_path, 'r') as f:
            valid_cups_xml = f.read()

        cups = get_cups_from_xml(valid_cups_xml)
        origin_cups = 'ES0291000000000023XB0F'
        expect(origin_cups).to(equal(cups))

    def test_get_none_not_cups_from_xml(self):
        invalid_cups_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_utils_no_cups.xml'
        )
        with open(invalid_cups_xml_path, 'r') as f:
            invalid_cups_xml = f.read()

        cups = get_cups_from_xml(invalid_cups_xml)
        expect(cups).to(be_none)

    def test_get_filename_without_unicode_values(self):
        bad_filename = 'FILE_WITH_BAD_CODE\xd6.xml'
        good_filename = u'FILE_WITH_BAD_CODE.xml'
        expect(good_filename).to(equal(get_filename(bad_filename)))

    def test_get_filename_unicode(self):
        bad_filename = u'FILE_WITH_BAD_CODE.xml'
        good_filename = u'FILE_WITH_BAD_CODE.xml'
        expect(good_filename).to(equal(get_filename(bad_filename)))

    def test_get_filename_str(self):
        bad_filename = 'FILE_WITH_BAD_CODE.xml'
        good_filename = u'FILE_WITH_BAD_CODE.xml'
        expect(good_filename).to(equal(get_filename(bad_filename)))
