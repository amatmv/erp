# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource
from giscedata_switching.tests import TestSwitchingImport


class TestWizardGenerateR1FromF1(TestSwitchingImport):

    def test_generate_r1_from_erroneous_f1(self):
        self.openerp.install_module('giscedata_switching')
        with Transaction().start(self.database) as txn:
            imd_obj = self.openerp.pool.get('ir.model.data')
            lin_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            crm_obj = self.openerp.pool.get('crm.case')
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            pol_01_id = imd_obj.get_object_reference(cursor, uid,
                                                          'giscedata_polissa',
                                                          'polissa_0001')[1]
            pol_cups_01_id = pol_obj.read(
                cursor, uid, pol_01_id, ['cups']
            )['cups'][0]

            f1_id = imd_obj.get_object_reference(cursor, uid,
                                                 'giscedata_facturacio_switching',
                                                 'line_01_f1_import_01')[1]
            lin_obj.write(
                cursor, uid, f1_id,
                {'critical_info': 'It was an error',
                 'cups_id': pol_cups_01_id
                 }
            )

            wiz_obj = self.openerp.pool.get('wizard.r1.from.f1.erroni')

            context = {'active_ids': [f1_id]}
            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)

            wiz = wiz_obj.browse(cursor, uid, wiz_id, context=context)

            wiz.action_create_atr_cases(context=context)

            generated_case = eval(wiz_obj.read(
                cursor, uid, wiz_id, ['generated_cases'], context=context
            )[0]['generated_cases'])

            self.assertTrue(generated_case)

            case = sw_obj.browse(cursor, uid, generated_case[0], context=context)

            self.assertTrue(case.id)


