# -*- coding: utf-8 -*-
import unittest
import base64

from destral import testing
from destral.transaction import Transaction
from expects import *
from osv.osv import except_osv
from addons import get_module_resource
from gestionatr.input.messages import F1
from datetime import datetime, timedelta
from giscedata_polissa.tests.utils import crear_modcon


class TestImportF1(testing.OOTestCase):
    def setUp(self):
        module_obj = self.openerp.pool.get('ir.module.module')

        newest_name = None

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            newest_id = module_obj.search(
                cursor, uid, [
                    ('name', 'like', 'giscedata_tarifas_peajes_20160101'),
                    ('state', '!=', 'installed')
                ], limit=1, order='name DESC'
            )
            if newest_id:
                newest_name = module_obj.read(
                    cursor, uid, newest_id[0], ['name']
                )['name']

        if newest_name:
            self.openerp.install_module(newest_name)

        return super(TestImportF1, self).setUp()

    def activar_polissa(self, txn, polissa_ref):
        """Activa una polissa i obté el seu id."""
        cursor = txn.cursor
        uid = txn.user

        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', polissa_ref
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        return polissa_id

    def test_ignore_reference_is_false_by_default(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            distri_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]

            vals = {
                'name': 'Import Name',
                'importacio_id': import_id,
                'distribuidora_id': distri_id,
            }
            line_id = line_obj.create(cursor, uid, vals)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['ignore_reference']).to(equal(False))

    def test_f1_process_phase_1_sets_correctly_fields(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        f1_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_correct.xml'
        )

        with open(f1_xml_path, 'r') as f:
            xml_file = f.read()
        xml_file = xml_file.replace(
            '<IdRemesa>0</IdRemesa>', '<IdRemesa>123456789</IdRemesa>',
        )
        xml_file = xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0021</CodigoREEEmpresaEmisora>'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            expected_values = {
                'remesa_id': '123456789',
                'total_rebuts': 1,
                'import_remesa': 44.84,
            }
            user_obj = self.openerp.pool.get('res.users')
            user_browse = user_obj.browse(cursor, uid, uid)
            user_browse.company_id.partner_id.write({'ref': '4321'})
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            for field, value in expected_values.items():
                self.assertEquals(line_vals[field], value)

    def test_f1_with_reference_invoice_dont_import_by_default(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_rectificadora.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            user_obj = self.openerp.pool.get('res.users')
            user_browse = user_obj.browse(cursor, uid, uid)
            user_browse.company_id.partner_id.write({'ref': '4321'})
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('007'))

    def test_f1_with_reference_invoice_dont_import_when_no_code(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        f_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_rectificadora.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace(
                "<CodigoFacturaRectificadaAnulada>20150918030012928   </CodigoFacturaRectificadaAnulada>",
                ""
            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # First we import a corerct F1 to have an invoice in the db
            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))

            # Delete origin from new invoice
            factura = f_obj.browse(cursor, uid, line.liniafactura_id[0].factura_id.id)
            factura.write({'origin': False})
            factura = f_obj.browse(cursor, uid,line.liniafactura_id[0].factura_id.id)
            self.assertFalse(factura.origin)

            # Now we import a rectificative without ref code
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('007'))

    def test_f1_with_reference_invoice_imports_on_set_ignore_true(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        temp_err_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_rectificadora.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )
            line_obj.write(cursor, uid, line_id, {'ignore_reference': True})

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )
            address_obj.create(cursor, uid, {'partner_id': distri_id})

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            user_obj = self.openerp.pool.get('res.users')
            user_browse = user_obj.browse(cursor, uid, uid)
            user_browse.company_id.partner_id.write({'ref': '4321'})
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))

            error = error_obj.read(
                cursor, uid, line_vals['error_ids'], ['error_template_id']
            )
            template_ids = [
                err_vals['error_template_id'][0]
                for err_vals in error
            ]
            error_templates = [
                {'phase': temp_err_vals['phase'], 'code': temp_err_vals['code']}
                for temp_err_vals in temp_err_obj.read(
                    cursor, uid, template_ids, ['phase', 'code']
                )
            ]

            expect(error_templates).to(contain({'phase': '2', 'code': '012'}))

    def test_f1_with_rectifyer_creates_bra(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        fact_line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_rectifier.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read().replace(
                '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
                '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer',
                'facturacio_journal_energia_prov'
            )[1]

            factura_ori_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_dso_0001'
            )[1]
            factura_obj.write(
                cursor, uid, factura_ori_id, {
                    'origin': '99999',
                    'journal_id': journal_id,
                    'partner_id': 23
                }
            )

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['liniafactura_id']).to(have_len(2))

            invoice_types = [
                fact_vals['rectificative_type']
                for fact_vals in fact_line_obj.read(
                    cursor, uid, line_vals['liniafactura_id']
                )
            ]

            expect(
                sorted(invoice_types)
            ).to(
                equal(sorted(['RA', 'BRA']))
            )

    def test_f1_with_rectifier_creates_bra_with_payment_order_id(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        fact_line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )
        factura_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_rectifier.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read().replace(
                '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
                '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer',
                'facturacio_journal_energia_prov'
            )[1]

            factura_ori_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_dso_0001'
            )[1]
            remesa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_remeses', 'remesa_0001'
            )[1]
            factura_obj.write(
                cursor, uid, factura_ori_id, {
                    'origin': '99999',
                    'journal_id': journal_id,
                    'payment_order_id': remesa_id,
                    'partner_id': 23
                }
            )

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['liniafactura_id']).to(have_len(2))

            invoice_types = []
            for fact in fact_line_obj.read(
                    cursor, uid, line_vals['liniafactura_id']):
                invoice_types.append(fact['rectificative_type'])
                if fact['rectificative_type'] == 'BRA':
                    bra_id = fact['factura_id'][0]
                if fact['rectificative_type'] == 'RA':
                    ra_id = fact['factura_id'][0]

            # Expect to create a RA and a BRA from the import
            expect(
                sorted(invoice_types)
            ).to(
                equal(sorted(['RA', 'BRA']))
            )
            bra_inv = factura_obj.browse(cursor, uid, bra_id)
            ra_inv = factura_obj.browse(cursor, uid, ra_id)

            # Expect the BRA to have the same payment_order_id than the RA
            expect(
                bra_inv.payment_order_id.id
            ).to(equal(
                ra_inv.payment_order_id.id
            ))

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        polissa.modcontractual_activa.write({'data_final': '3000-01-01'})
        polissa.write({'data_baixa': None})
        user_obj = self.openerp.pool.get('res.users')
        user_browse = user_obj.browse(cursor, uid, uid)
        user_browse.company_id.partner_id.write({'ref': '4321'})

    def clean_hash_lines(self, txn):
        cursor = txn.cursor
        uid = txn.user
        line_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
        lids = line_obj.search(cursor, uid, [])
        line_obj.write(cursor, uid, lids, {"md5_hash": "123456789"})

    def crear_modcon(self, txn, potencia, ini, fi, tariff_id=None, extra_fields=None):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        if extra_fields and isinstance(extra_fields, dict):
            polissa_obj.write(cursor, uid, polissa_id, extra_fields)

        if tariff_id:
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tariff_id})

        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def add_supported_iva_to_periods(self, txn):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.openerp.pool.get('giscedata.polissa.tarifa.periodes')
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', '21% IVA Soportado (operaciones corrientes)')
            ]
        )

        product_ids = []
        for tarifa_id in tarifa_obj.search(cursor, uid, []):
            periode_ids = tarifa_obj.read(
                cursor, uid, tarifa_id, ['periodes']
            )['periodes']
            for periode_vals in periode_obj.read(cursor, uid, periode_ids):
                product_ids.append(periode_vals['product_id'][0])
                if periode_vals['product_reactiva_id']:
                    product_ids.append(periode_vals['product_reactiva_id'][0])
                if periode_vals['product_exces_pot_id']:
                    product_ids.append(periode_vals['product_exces_pot_id'][0])

        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def add_supported_iva_to_concepts(self, txn, iva='21'):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        name = '{}% IVA Soportado (operaciones corrientes)'.format(iva)
        iva_id = tax_obj.search(
            cursor, uid, [
                ('name', '=', name)
            ]
        )

        no_iva_concepts = ['CON06', 'CON40', 'CON41', 'CON42']
        product_ids = product_obj.search(
            cursor, uid, [
                '|',
                ('default_code', 'like', 'CON'),
                ('default_code', 'like', 'ALQ'),
                ('default_code', 'not in', no_iva_concepts)
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'supplier_taxes_id': [(6, 0, iva_id)]
            }
        )

    def add_sell_taxes_to_suplemento_territorial(self, txn):
        tax_obj = self.openerp.pool.get('account.tax')
        product_obj = self.openerp.pool.get('product.product')

        cursor = txn.cursor
        uid = txn.user

        tax_ids = tax_obj.search(
            cursor, uid, [
                '|',
                ('name', '=', 'IVA 21%'),
                ('name', '=', 'Impuesto especial sobre la electricidad')
            ]
        )

        product_ids = product_obj.search(
            cursor, uid, [
                ('default_code', 'in', ('SETU35', 'TEC271'))
            ]
        )
        product_obj.write(
            cursor, uid, product_ids, {
                'taxes_id': [(6, 0, tax_ids)]
            }
        )

    def prepare_for_phase_3(self, txn, context=None):
        if not context:
            context = {}
        polissa_obj = self.openerp.pool.get(
            'giscedata.polissa'
        )
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        version_obj = self.openerp.pool.get('product.pricelist.version')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')
        pot_obj = self.openerp.pool.get(
            "giscedata.polissa.potencia.contractada.periode")

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            context.get("test_xml", 'test_correct.xml')
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )

        conceptes_xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_conceptes.xml'
        )

        with open(conceptes_xml_path, 'r') as f:
            self.conceptes_xml_file = f.read()

        cursor = txn.cursor
        uid = txn.user

        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_01'
        )[1]
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        import_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
        )[1]
        distri_id = partner_obj.search(
            cursor, uid, [('ref', '=', '0031')]
        )[0]
        pricelist_id = pricelist_obj.search(
            cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
        )[0]
        versions = version_obj.search(cursor, uid, [('pricelist_id', '=', pricelist_id)], order="date_end desc", limit=1)
        version_obj.write(cursor, uid, versions[-1], {'date_end': None})
        self.line_id = line_obj.create_from_xml(
            cursor, uid, import_id, 'Import Name', self.xml_file
        )
        line_obj.write(cursor, uid, self.line_id, {'cups_id': cups_id})
        pvals = {
            'cups': cups_id,
            'state': 'activa',
            'distribuidora': distri_id,
            'facturacio_potencia': 'icp',
            'potencia': 5.75,
        }
        if context.get("tarifa"):
            pvals['tarifa'] = context.get("tarifa")
        if context.get("extra_vals"):
            pvals.update(context.get("extra_vals"))

        polissa_obj.write(
            cursor, uid, polissa_id, pvals
        )
        pol_pots = polissa_obj.read(cursor, uid, polissa_id, ['potencies_periode'])['potencies_periode']
        pot_obj.write(cursor, uid, pol_pots, {'potencia': 5.750})
        cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
        address_obj.create(cursor, uid, {'partner_id': distri_id})
        partner_obj.write(
            cursor, uid, distri_id, {
                'property_product_pricelist_purchase': pricelist_id
            }
        )

        self.activar_polissa_CUPS(txn)

        self.add_supported_iva_to_periods(txn)
        self.add_supported_iva_to_concepts(txn)
        self.add_sell_taxes_to_suplemento_territorial(txn)
        self.clean_hash_lines(txn)


    def test_for_validate_line_in_phase_3(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        err_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            compt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]

            # We set the giro of the meter so that it does not create a warning
            compt_obj.write(cursor, uid, compt_id, {'giro': 1000000})

            # We deactivate error 15 because this one allways returns
            err_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching',
                'error_phase_3_code_015'
            )[1]
            err_obj.write(cursor, uid, err_id, {'active': False})

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            expect(error_ids).to(equal([]))
            expect(has_critical).to(equal(False))

    def test_validation_3001(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '001', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3002(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            # We change the counter to one that shouldn't exist
            xml_file = self.xml_file.replace('B63011077', 'B99999999')
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '002', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

    @unittest.skip(reason='This is now handled by gestionatr')
    def test_validation_3003(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'test_double_meter.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '003', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3004(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_reactiva_separada.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '004', 'level': 'information'})
            )

    def test_validation_3006(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # polissa_01.tg = '1'
            xml_file = self.xml_file.replace(
                '<IndicativoCurvaCarga>01</IndicativoCurvaCarga>',
                '<IndicativoCurvaCarga>03</IndicativoCurvaCarga>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '006', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

    def validation_test_3006_update_cch_core(self, tg_fact, tg_pol, exp_tg_pol):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        err_templ = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            err_templ_id = err_templ.search(
                cursor, uid, [('phase', '=', '3'), ('code', '=', '006')]
            )
            err_templ.write(
                cursor, uid, err_templ_id,
                {'parameters': "{\"modify_cch\": 1}"}
            )

            # polissa_01.tg = '1' → '2'
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(cursor, uid, polissa_id, {'tg': tg_pol})
            self.activar_polissa(txn, 'polissa_0001')
            self.prepare_for_phase_3(txn)
            xml_file = self.xml_file.replace(
                '<IndicativoCurvaCarga>01</IndicativoCurvaCarga>',
                '<IndicativoCurvaCarga>{}</IndicativoCurvaCarga>'.format(tg_fact)
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            validator_obj.validate_f1(cursor, uid, self.line_id, f1, phase=3)

            polissa_tg = polissa_obj.read(
                cursor, uid, [polissa_id], ['tg']
            )[0]['tg']
            # check if telegestio field were modified
            self.assertEqual(polissa_tg, exp_tg_pol)

    def test_validation_3006_update_cch(self):
        # combinatorial logic according to #7070
        # case a
        self.validation_test_3006_update_cch_core('01', '1', '1')
        # case b
        self.validation_test_3006_update_cch_core('01', '2', '1')
        # case c
        self.validation_test_3006_update_cch_core('01', '2', '1')
        # case d -- for now it won't be tested because
        # we need to determinate the final telegestio state
        # self.validation_test_3006_update_cch_core('02', '1', '?')
        # case e
        self.validation_test_3006_update_cch_core('02', '2', '2')
        # case f
        self.validation_test_3006_update_cch_core('02', '3', '3')
        # the next cases (g,h,i) makes no sense because they are for AT.
        # for now, we'll 'ignore' the tg field so we'll leave as it was set.
        # case g
        self.validation_test_3006_update_cch_core('03', '1', '1')
        # case h
        self.validation_test_3006_update_cch_core('03', '2', '2')
        # case i
        self.validation_test_3006_update_cch_core('03', '3', '3')

    def test_validation_3007(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        lectures_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            line = line_obj.browse(cursor, uid, self.line_id)

            periode_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]
            distri_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_partner'
            )[1]
            compt_id = line.cups_id.polissa_polissa.comptadors[0].id
            origen_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'origen10'
            )[1]

            lectures_obj.create(
                cursor, uid, {
                    'periode': periode_id,
                    'name': '2016-01-31',
                    'origen_comer_id': distri_id,
                    'comptador': compt_id,
                    'origen_id': origen_id,
                    'lectura': 12345,
                    'tipus': 'A',
                }
            )

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '007', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3008(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Creem una modcon per poder importar sense errors
            self.crear_modcon(txn, 6.0, '2017-01-01', '2018-01-01')

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_missing_period.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '008', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3008_doesnt_pop_on_3_period_30A(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            tariff_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_30A'
            )[1]

            line = line_obj.browse(cursor, uid, self.line_id)
            line.cups_id.polissa_polissa.write({'tarifa': tariff_id})
            line.cups_id.polissa_polissa.modcontractual_activa.write(
                {'tarifa': tariff_id}
            )

            # Creem una modcon per poder importar sense errors
            self.crear_modcon(txn, 6.0, '2017-01-01', '2018-01-01')

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_30A_with_3_periods.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to_not(
                contain({'code': '008', 'level': 'critical'})
            )

    def test_validation_3008_reactiva_separada_not_error(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            # Creem una modcon per poder importar sense errors
            self.crear_modcon(txn, 6.0, '2017-01-01', '2018-01-01')

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_reactiva_separada.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to_not(
                contain({'code': '008', 'level': 'critical'})
            )

    def test_validation_3008_no_falla_dh_2_periodes_potencia(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'F1_dh_2_periods_pot.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscedata.facturacio.importacio.linia.lectures'
            )

            lectures_f1_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id)]
            )
            self.assertTrue(lectures_f1_ids)

    def test_validation_3009(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<ConsumoCalculado>416.00</ConsumoCalculado>',
                '<ConsumoCalculado>-416.00</ConsumoCalculado>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml(validate=False)

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '009', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3010(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_ajuste_1.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '010', 'level': 'information'})
            )

    def test_validation_3011(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<Lectura>4510.00</Lectura>',
                '<Lectura>3439.00</Lectura>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '011', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3013(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<Procedencia>30</Procedencia>',
                '<Procedencia>99</Procedencia>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '013', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3014(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<NumeroRuedasEnteras>6</NumeroRuedasEnteras>',
                '<NumeroRuedasEnteras>1</NumeroRuedasEnteras>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '014', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3016(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<Lectura>4510.00</Lectura>',
                '<Lectura>4000.00</Lectura>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '016', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3017(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Creem una modcon per poder importar sense errors
            self.crear_modcon(txn, 6.0, '2017-01-01', '2018-01-01')

            xml_file = self.xml_file.replace(
                '<TarifaATRFact>001</TarifaATRFact>',
                '<TarifaATRFact>003</TarifaATRFact>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '017', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3018(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<ModoControlPotencia>1</ModoControlPotencia>',
                '<ModoControlPotencia>2</ModoControlPotencia>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '018', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3019(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            tariff_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_31A_LB'
            )[1]

            line = line_obj.browse(cursor, uid, self.line_id)
            line.cups_id.polissa_polissa.write({'tarifa': tariff_id})

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '019', 'level': 'warning'})
            )

    def test_validation_3020(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'test_autoconsume.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '020', 'level': 'information'})
            )

    @unittest.skip(reason='Field is obligatory now')
    def test_validation_3021(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.conceptes_xml_file.replace(
                '<UnidadesConcepto>1</UnidadesConcepto>', ''
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '021', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    @unittest.skip(reason='Field is obligatory now')
    def test_validation_3022(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.conceptes_xml_file.replace(
                '<ImporteUnidadConcepto>9.04</ImporteUnidadConcepto>', ''
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '022', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    @unittest.skip(reason='Field is obligatory now')
    def test_validation_3023(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.conceptes_xml_file.replace(
                '<ImporteTotalConcepto>9.04</ImporteTotalConcepto>',
                '<ImporteTotalConcepto>123.456</ImporteTotalConcepto>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '023', 'level': 'information'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3024(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.conceptes_xml_file.replace(
                '<ConceptoRepercutible>04</ConceptoRepercutible>',
                '<ConceptoRepercutible>00</ConceptoRepercutible>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml(validate=False)

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '024', 'level': 'information'})
            )

    def test_validation_3025(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            compt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]
            compt_obj.write(
                cursor, uid, compt_id, {
                    'active': False,
                    'data_baixa': '2016-02-10'
                }
            )
            compt_obj.create(
                cursor, uid, {
                    'name': 'B12345678',
                    'polissa': polissa_id,
                    'giro': 10000,
                    'data_alta': '2016-02-11',
                }
            )

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '025', 'level': 'warning'})
            )
            # expect(has_critical).to(equal(True))

    def test_validation_3025_alta_correcta(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            compt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]
            # Alta del comptador el mateix dia que la pólissa, les lectures
            # seran del dia anterior
            compt = compt_obj.browse(cursor, uid, compt_id)
            for l in compt.lectures:
                l.unlink(context={})
            for lp in compt.lectures_pot:
                lp.unlink(context={})
            compt.write({
                'active': True,
                'data_alta': '2016-01-01'
            })

            xml_file = self.xml_file.replace(
                '<Fecha>2016-01-31</Fecha>',
                '<Fecha>2015-12-31</Fecha>'
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                if error_code == {'code': '025', 'level': 'warning'}:
                    error_codes.append(error_code)

            self.assertEqual(len(error_codes), 0)

    def test_validation_3025_alta_incorrecta(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            compt_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'comptador_0001'
            )[1]
            # Alta del comptador el mateix dia que la pólissa, les lectures
            # seran dos dies anteriors (incorrecte)
            compt = compt_obj.browse(cursor, uid, compt_id)
            for l in compt.lectures:
                l.unlink(context={})
            for lp in compt.lectures_pot:
                lp.unlink(context={})
            compt.write({
                'active': True,
                'data_alta': '2016-01-01'
            })

            xml_file = self.xml_file.replace(
                '<Fecha>2016-01-31</Fecha>',
                '<Fecha>2015-12-30</Fecha>'
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                if error_code == {'code': '025', 'level': 'warning'}:
                    error_codes.append(error_code)

            self.assertEqual(len(error_codes), 1)

    def test_validation_3026(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            tariff_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20A'  # It's 2.1A
            )[1]

            self.crear_modcon(
                txn, 12, '2016-02-10', '2016-05-02', tariff_id=tariff_id
            )

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '026', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3026_doesnt_pop_on_power_change(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            self.crear_modcon(txn, 8.5, '2016-02-10', '2016-05-02')

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to_not(
                contain({'code': '026', 'level': 'critical'})
            )

    def test_validation_3027(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)
            # Creem una modcon per poder importar sense errors
            self.crear_modcon(txn, 6.0, '2017-01-01', '2018-01-01')

            xml_file = self.xml_file.replace(
                '<TarifaATRFact>001</TarifaATRFact>',
                '<TarifaATRFact>011</TarifaATRFact>'
            )
            xml_file = xml_file.replace(
                '<MarcaMedidaConPerdidas>N</MarcaMedidaConPerdidas>',
                '<MarcaMedidaConPerdidas>S</MarcaMedidaConPerdidas>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '027', 'level': 'warning'})
            )

    def test_validation_3028_a(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<ImporteTotalTerminoPotencia>17.33</',
                '<ImporteTotalTerminoPotencia>17.34</'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-A]'))

    def test_validation_3028_b(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<ImporteTotalEnergiaActiva>18.32</',
                '<ImporteTotalEnergiaActiva>18.31</'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-B]'))

    def test_validation_3028_c(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_reactiva_ok.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            xml_file = xml_file.replace(
                '<ImporteTotalEnergiaReactiva>0.83</',
                '<ImporteTotalEnergiaReactiva>0.84</'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-C]'))

    def test_validation_3028_d(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        tarif_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            tarif_id = tarif_obj.search(cursor, uid, [('name', '=', '6.1A')])[0]
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(cursor, uid, polissa_id, {'tarifa': tarif_id})

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'F1_excesso_potencia.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            xml_file = xml_file.replace(
                '<ImporteTotalExcesos>2565.27</',
                '<ImporteTotalExcesos>2565.26</'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-D]'))

    def test_validation_3028_e(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<BaseImponible>36.16</BaseImponible>',
                '<BaseImponible>36.17</BaseImponible>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-E]'))

    def test_validation_3028_f(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<Importe>7.5900</Importe>', '<Importe>7.6000</Importe>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-F]'))

    def test_validation_3028_g(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'test_extra_iva.xml'
            )
            with open(xml_path, 'r') as f:
                xml_file = f.read()
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(True))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-G]'))

    def test_validation_3028_h(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            self.add_supported_iva_to_concepts(txn, '18')

            # We adapt the number so that we don't have more errors
            xml_file = self.xml_file.replace(
                '<BaseImponible>35.5600</BaseImponible>',
                '<BaseImponible>35.0500</BaseImponible>'
            )
            xml_file = xml_file.replace(
                '<Importe>7.4700</Importe>',
                '<Importe>7.3600</Importe>'
            )
            xml_file = xml_file.replace(
                '<ImporteTotalFactura>43.03</ImporteTotalFactura>',
                '<ImporteTotalFactura>43.01</ImporteTotalFactura>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-H]'))


    def test_validation_3028_i(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            # We adapt the number so that we don't have more errors
            xml_file = self.xml_file.replace(
                '<ImporteTotalFactura>43.75</ImporteTotalFactura>',
                '<ImporteTotalFactura>43.74</ImporteTotalFactura>'
            )
            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            errors_028 = []
            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

                if error_id['code'] == '028':
                    errors_028.append(error_id['error_id'])

            expect(error_codes).to(
                contain({'code': '028', 'level': 'warning'})
            )
            expect(has_critical).to(equal(False))

            error_subcategories = [
                err_vals['message'].split(' ')[0]
                for err_vals in error_obj.read(
                    cursor, uid, errors_028, ['message']
                )
            ]
            expect(error_subcategories).to(contain('[3028-I]'))

    def test_validation_3029(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<Fecha>2016-02-19</Fecha>',
                '<Fecha>2016-01-31</Fecha>'
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '029', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3030(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<NumeroSerie>B63011077</NumeroSerie>',
                '<NumeroSerie>B99999999</NumeroSerie>'
            )

            old_compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(
                cursor, uid, old_compt_id, {
                    'data_alta': '2016-02-01'
                }
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '030', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3031(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<PrecioDia>0.017586</PrecioDia>',
                '<PrecioDia>10.017586</PrecioDia>'
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '031', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3032(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<PotenciaContratada>5750</PotenciaContratada>',
                '<PotenciaContratada>2750</PotenciaContratada>'
            )

            xml_file = xml_file.replace(
                '<PotenciaAFacturar>5750</PotenciaAFacturar>',
                '<PotenciaAFacturar>2750</PotenciaAFacturar>'
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '032', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

            # Test modcontractual (F1 with 2.750 until 29/02/2016)
            # contract with 2.750 from 29/02/2016)
            imd_obj = self.openerp.pool.get('ir.model.data')
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            crear_modcon(
                self.openerp.pool, cursor, uid, polissa_id,
                {'potencia': 2.750}, '2016-04-01', '2017-03-31'
            )
            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '032', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

    def test_validation_3032_two_diff_pots(self):
        """
        Es poden enviar 2 terminoPotencia amb 2 potencies diferents perqué hi ha
        hagut un canvi. Cada terme te les seves dates i s'han d'utilitzar
        aquelles dates per comparar amb les modcons del contracte.

        Aquest test comprova que un cas correcte s'importi correctament.
        """
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_dos_term_pot.xml'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            with open(xml_path, 'r') as f:
                xml_file = f.read()

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            # Primer ha de donar error perque noi tenim cap modcon el dis 11/02
            # que canvii la potencia a 3000
            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '032', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))

            # Ara creem la modcon el 11/02 i no ha de donar error
            imd_obj = self.openerp.pool.get('ir.model.data')
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            crear_modcon(
                self.openerp.pool, cursor, uid, polissa_id,
                {'potencia': 3.000}, '2016-02-11', '2017-03-31'
            )
            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).to_not(
                contain({'code': '032', 'level': 'critical'})
            )
            expect(has_critical).to(equal(False))

    def test_validation_3032_cas_endesa(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_file = self.xml_file.replace(
                '<PotenciaContratada>5750</PotenciaContratada>',
                '<PotenciaContratada>2750</PotenciaContratada>'
            )

            f1 = F1(xml_file, 'F1')
            f1.parse_xml()

            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).not_to(
                contain({'code': '032', 'level': 'critical'})
            )

            # Test modcontractual (F1 with 2.750 until 29/02/2016)
            # contract with 2.750 from 29/02/2016)
            imd_obj = self.openerp.pool.get('ir.model.data')
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            crear_modcon(
                self.openerp.pool, cursor, uid, polissa_id,
                {'potencia': 2.750}, '2016-04-01', '2017-03-31'
            )
            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_id')
                error_codes.append(error_code)

            expect(error_codes).not_to(
                contain({'code': '032', 'level': 'critical'})
            )

    def test_validation_3033(self):
        validator_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.validator'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        mf_obj = self.openerp.pool.get('giscedata.polissa.mode.facturacio')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            from giscedata_facturacio.giscedata_polissa import INTERVAL_INVOICING_FIELDS

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            pol = polissa_obj.browse(cursor, uid, polissa_id)

            mf_obj.create(cursor, uid, {
                'code': 'new_mode',
                'name': 'NEW MODE',
                'compatible_price_lists': [(4, pol.llista_preu.id)]
            })

            INTERVAL_INVOICING_FIELDS += ['mode_facturacio']

            self.prepare_for_phase_3(txn)

            self.crear_modcon(
                txn, 12, '2016-02-10', '2016-05-02', extra_fields={'mode_facturacio': 'new_mode'}
            )

            f1 = F1(self.xml_file, 'F1')
            f1.parse_xml()



            error_ids, has_critical = validator_obj.validate_f1(
                cursor, uid, self.line_id, f1, phase=3
            )

            error_codes = []
            error_id = None
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code_id = error_code.pop('error_id')
                if error_code['code'] == '033':
                    error_id = error_code_id
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'code': '033', 'level': 'critical'})
            )
            expect(has_critical).to(equal(True))
            self.assertIsNotNone(error_id)

            fact_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')

            line = fact_obj.browse(cursor, uid, self.line_id)

            result_error = validator_obj.check_mode_facturacio(
                cursor, uid, line, f1, params={'new_modcon_date': 'end'}
            )
            values = {
                'data_canvi': '2016-02-10',
                'mode_facturacio': 'new_mode',
                'data_nova': '2016-03-01'
            }
            self.assertGreater(len(result_error), 0)
            self.assertDictEqual(values, result_error[0])

    def test_validation_3034(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        error_tmp_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            error_tmp_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'error_phase_3_code_034'
            )[1]
            error_tmp_obj.write(cursor, uid, error_tmp_id, {'level': 'critical'})

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_3034.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(line_vals['import_phase']).to(equal(30))

            error_codes = []
            error_id = None
            for error_id in line_vals['error_ids']:
                error_code = error_obj.read(cursor, uid, error_id, ['name', 'level'])
                if error_code['name'] == '3034':
                    error_id = error_code['id']
                error_code.pop('id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'name': '3034', 'level': 'critical'})
            )
            self.assertIsNotNone(error_id)

    def test_validation_3035(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        error_tmp_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            error_tmp_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'error_phase_3_code_035'
            )[1]
            error_tmp_obj.write(cursor, uid, error_tmp_id, {'level': 'critical'})

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_3034.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(line_vals['import_phase']).to(equal(30))

            error_codes = []
            error_id = None
            for error_id in line_vals['error_ids']:
                error_code = error_obj.read(cursor, uid, error_id, ['name', 'level'])
                if error_code['name'] == '3035':
                    error_id = error_code['id']
                error_code.pop('id')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'name': '3035', 'level': 'critical'})
            )
            self.assertIsNotNone(error_id)

    def test_validation_3035_ok(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        error_tmp_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            error_tmp_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'error_phase_3_code_035'
            )[1]
            error_tmp_obj.write(cursor, uid, error_tmp_id, {'level': 'critical'})

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_3035_ok.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))

            error_codes = []
            error_id = None
            for error_id in line_vals['error_ids']:
                error_code = error_obj.read(cursor, uid, error_id, ['name', 'level'])
                if error_code['name'] == '3035':
                    error_id = error_code['id']
                error_code.pop('id')
                error_codes.append(error_code)

            expect(error_codes).to_not(
                contain({'name': '3035', 'level': 'critical'})
            )
            self.assertIsNotNone(error_id)

    def test_validation_3035_complementaries(self):
        """
        Tests that the validation is OK when the F1 to import is a Rectifier or
        Nullifier of a C or G invoice
        """
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        error_tmp_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        journal_obj = self.openerp.pool.get('account.journal')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0031')]
            )[0]

            journal_id = journal_obj.search(
                cursor, uid, [('code', 'like', 'CENERGIA')]
            )[0]

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0003'
            )[1]

            params = {
                'origin_date_invoice': '2016-07-01',
                'origin': '20150918030012928',
                'partner_id': distri_id,
                'tipo_rectificadora': 'C',
                'journal_id': journal_id,
            }
            fact_obj.write(cursor, uid, [fact_id], params)

            error_tmp_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching',
                'error_phase_3_code_035'
            )[1]
            error_tmp_obj.write(cursor, uid, error_tmp_id,
                                {'level': 'critical'})

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'F1_anuladora_C.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))

            error_codes = []
            error_id = None
            for error_id in line_vals['error_ids']:
                error_code = error_obj.read(cursor, uid, error_id,
                                            ['name', 'level'])
                if error_code['name'] == '3035':
                    error_id = error_code['id']
                error_code.pop('id')
                error_codes.append(error_code)

            expect(error_codes).to_not(
                contain({'name': '3035', 'level': 'critical'})
            )
            self.assertIsNotNone(error_id)

    def test_full_import_works(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['lectures_processades']).to(equal(True))
            expect(line_vals['import_phase']).to(equal(40))
            imp_lect_obj = self.openerp.pool.get(
                'giscedata.facturacio.importacio.linia.lectures'
            )

            lectures_f1_ids = imp_lect_obj.search(
                cursor, uid, [('linia_id', '=', line_id)]
            )
            self.assertTrue(lectures_f1_ids)

    def check_linies_factura(self, factura, linies_vals):
        for linia_vals in linies_vals:
            found = True
            for l in factura.linia_ids:
                found = True
                for camp, valor in linia_vals.items():
                    if eval("l.{0}".format(camp)) != valor:
                        found = False
                        break
                if found:
                    break
            self.assertTrue(found, msg="No s'ha trobat la linia de la factura amb dades {0}".format(linia_vals))
        return True

    def check_lectures_factura(self, factura, lectures_vals):
        for linia_vals in lectures_vals:
            found = True
            for l in factura.lectures_energia_ids:
                found = True
                for camp, valor in linia_vals.items():
                    if eval("l.{0}".format(camp)) != valor:
                        found = False
                        break
                if found:
                    break
            self.assertTrue(found, msg="No s'ha trobat la lectura de la factura amb dades {0}".format(linia_vals))
        return True

    def check_lectures_pool_polissa(self, polissa, lectures_vals):
        for linia_vals in lectures_vals:
            lectures_pool = []
            for c in polissa.comptadors:
                lectures_pool.extend(c.pool_lectures)
            found = True
            for l in lectures_pool:
                found = True
                for camp, valor in linia_vals.items():
                    if eval("l.{0}".format(camp)) != valor:
                        found = False
                        break
                if found:
                    break

            self.assertTrue(found, msg="No s'ha trobat la lectura pool amb dades {0}".format(linia_vals))
        return True

    def test_nou_autoconsum_31_quadra_energia_consums(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_31_quadra_energia_consums.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 31.0},
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4190.0, 'ajust': 10.0, 'motiu_ajust': '99', 'comptador': "141244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4190, 'ajust': 10, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99', 'comptador.name': "141244861"},
            ])

    def test_nou_autoconsum_31_no_quadra_energia_consums(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_31_no_quadra_energia_consums.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 31.0},
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 5000.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 5000, 'ajust': -800, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
            ])

    def test_nou_autoconsum_31_dos_comptadors_no_quadra_energia_consums_20A(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            compt_o = self.openerp.pool.get("giscedata.lectures.comptador")
            context = {"test_xml": 'test_nou_autoconsum_31_dos_comptadors_no_quadra_energia_consums_20A.xml'}
            self.prepare_for_phase_3(txn, context=context)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            polissa.comptadors[0].write({'name': "941244861", 'data_baixa': None, 'active': True, 'data_alta': "2016-01-01"})
            aux = {
                'name': '141244861',
                'data_alta': '2016-01-01',
                'active': True,
                'polissa': polissa.id
            }
            compt_o.create(cursor, uid, aux)
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 31.0},
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4800.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0A (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4200.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "941244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4800, 'ajust': -700, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "941244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4200, 'ajust': -100, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "941244861"},
            ])

    def test_nou_autoconsum_31_dos_comptadors_no_quadra_energia_consums_20DHA(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            compt_o = self.openerp.pool.get("giscedata.lectures.comptador")
            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_31_dos_comptadors_no_quadra_energia_consums_20DHA.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            polissa.comptadors[0].write({'name': "941244861", 'data_baixa': None, 'active': True, 'data_alta': "2016-01-01"})
            aux = {
                'name': '141244861',
                'data_alta': '2016-01-01',
                'active': True,
                'polissa': polissa.id
            }
            compt_o.create(cursor, uid, aux)
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 31.0},
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4200.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4050.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "941244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 100.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "941244861", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4200, 'ajust': -175, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 500, 'ajust': -325, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "941244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4050, 'ajust': -25, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "941244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "941244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 75, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "941244861"},
            ])

    def test_nou_autoconsum_41_20A(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_41_20A.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 10.0, 'motiu_ajust': '99', 'comptador': "141244861", "name": "2.0A (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 0.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "155385244", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4240, 'ajust': -40, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 20, 'ajust_exporta': -15, 'motiu_ajust': '98', 'comptador.name': "155385244"},
            ])

    def test_nou_autoconsum_41_20A_un_sol_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            context = {"test_xml": 'test_nou_autoconsum_41_20A_un_sol_comptador.xml'}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 10.0, 'motiu_ajust': '99', 'comptador': "141244861", "name": "2.0A (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 0.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0A (P1)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0A','lectura': 4240, 'ajust': -40, 'lectura_exporta': 20, 'ajust_exporta': -15, 'motiu_ajust': '98', 'comptador.name': "141244861"},
            ])

    def test_nou_autoconsum_41_20DHA(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "155385244", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 10.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "155385244", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4240, 'ajust': -190, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 500, 'ajust': -250, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 120, 'ajust_exporta': -5, 'motiu_ajust': '98', 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 20, 'ajust_exporta': -2, 'motiu_ajust': '98', 'comptador.name': "155385244"},
            ])

    def test_nou_autoconsum_41_20DHA_un_sol_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA_un_sol_comptador.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 4000.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 10.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4000, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4240, 'ajust': -190, 'lectura_exporta': 120, 'ajust_exporta': -5, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 500, 'ajust': -250, 'lectura_exporta': 20, 'ajust_exporta': -2, 'motiu_ajust': '98', 'comptador.name': "141244861"},
            ])

    def test_nou_autoconsum_41_20DHA_canvi_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA_canvi_comptador.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20", 'lect_anterior': 4000.0, 'lect_actual': 4200.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20", 'lect_anterior': 100.0, 'lect_actual': 400.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31", 'lect_anterior': 4200.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "241244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31", 'lect_anterior': 400.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "241244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 100.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "155385244", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-31", 'lect_anterior': 10.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "155385244", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4000, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4200, 'ajust': -175, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 400, 'ajust': -225, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4200, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4240, 'ajust': -15, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 400, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 500, 'ajust': -25, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '98', 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 120, 'ajust_exporta': -5, 'motiu_ajust': '98', 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "155385244"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 20, 'ajust_exporta': -2, 'motiu_ajust': '98', 'comptador.name': "155385244"},
            ])

    def test_nou_autoconsum_41_20DHA_un_sol_comptador_canvi_comptador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            context = {"test_xml": 'test_nou_autoconsum_41_20DHA_un_sol_comptador_canvi_comptador.xml', 'tarifa': tarif_id}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_linies_factura(factura, [
                {'product_id.code': 'ACACP1', 'price_subtotal': 0.0, 'quantity': 5.0},
                {'product_id.code': 'ACACP2', 'price_subtotal': 0.0, 'quantity': 2.0},
                {'product_id.code': 'ACGNP1', 'price_subtotal': 0.0, 'quantity': 20.0},
                {'product_id.code': 'ACGNP2', 'price_subtotal': 0.0, 'quantity': 10.0},
                {'product_id.code': 'ACEXP1', 'price_subtotal': 0.0, 'quantity': 15.0},
                {'product_id.code': 'ACEXP2', 'price_subtotal': 0.0, 'quantity': 8.0},
                {'product_id.code': 'ACTIPO', 'price_subtotal': 0.0, 'quantity': 41.0},
                {'product_id.code': 'ACCR', 'price_subtotal': 0.0, 'quantity': 1.0}
            ])
            self.check_lectures_factura(factura, [
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20", 'lect_anterior': 4000.0, 'lect_actual': 4200.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20", 'lect_anterior': 100.0, 'lect_actual': 400.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31", 'lect_anterior': 4200.0, 'lect_actual': 4240.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "241244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AE', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31", 'lect_anterior': 400.0, 'lect_actual': 500.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "241244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20", 'lect_anterior': 100.0, 'lect_actual': 110.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-07-31", 'data_actual': "2019-08-20", 'lect_anterior': 10.0, 'lect_actual': 15.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "141244861", "name": "2.0DHA (P2)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31", 'lect_anterior': 110.0, 'lect_actual': 120.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "241244861", "name": "2.0DHA (P1)"},
                {'magnitud': 'AS', 'tipus': 'activa', 'data_anterior': "2019-08-20", 'data_actual': "2019-08-31", 'lect_anterior': 15.0, 'lect_actual': 20.0, 'ajust': 0.0, 'motiu_ajust': False, 'comptador': "241244861", "name": "2.0DHA (P2)"},
            ])
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4000, 'ajust': 0, 'lectura_exporta': 100, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4200, 'ajust': -175, 'lectura_exporta': 110, 'ajust_exporta': -3, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 100, 'ajust': 0, 'lectura_exporta': 10, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 400, 'ajust': -225, 'lectura_exporta': 15, 'ajust_exporta': -1, 'motiu_ajust': '98', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4200, 'ajust': 0, 'lectura_exporta': 110, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 4240, 'ajust': -15, 'lectura_exporta': 120, 'ajust_exporta': -2, 'motiu_ajust': '98', 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-08-20", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 400, 'ajust': 0, 'lectura_exporta': 15, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "241244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 500, 'ajust': -25, 'lectura_exporta': 20, 'ajust_exporta': -1, 'motiu_ajust': '98', 'comptador.name': "241244861"},
            ])

    def test_tanto_alzado_balancejat(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            extrav = {
                'contract_type': '09',
                'expected_consumption': 200
            }
            context = {"test_xml": 'test_tanto_alzado_balancejat.xml', 'tarifa': tarif_id, 'extra_vals': extrav}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 50, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99', 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 150, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99', 'comptador.name': "141244861"},
            ])

    def test_tanto_alzado_balancejat_30A(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_30A'
            )[1]
            extrav = {
                'contract_type': '09',
                'expected_consumption': 200,
                'facturacio_potencia': 'max',
                'potencia': 43.6
            }
            context = {"test_xml": 'test_tanto_alzado_balancejat_30A.xml', 'tarifa': tarif_id, 'extra_vals': extrav}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 11.12)
            self.assertEqual(factura.check_total, 33.81)
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-11-24", 'periode.name': 'P1', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-24", 'periode.name': 'P2', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-24", 'periode.name': 'P3', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-24", 'periode.name': 'P4', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-24", 'periode.name': 'P5', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-24", 'periode.name': 'P6', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-25", 'periode.name': 'P1', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 43, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99', 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-25", 'periode.name': 'P2', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 130, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99', 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-25", 'periode.name': 'P3', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 87, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': '99', 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-25", 'periode.name': 'P4', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-25", 'periode.name': 'P5', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
                {'tipus': 'A', 'name': "2019-11-25", 'periode.name': 'P6', 'periode.tarifa.name': '3.0A','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "CZZ028384"},
            ])

    def test_tanto_alzado_balancejat_no_fa_falta(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            extrav = {
                'contract_type': '09',
                'expected_consumption': 200
            }
            context = {"test_xml": 'test_tanto_alzado_balancejat_no_fa_falta.xml', 'tarifa': tarif_id, 'extra_vals': extrav}
            self.prepare_for_phase_3(txn, context=context)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            polissa_id = imd_obj.get_object_reference(cursor, uid, 'giscedata_polissa', 'polissa_0001')[1]
            f1 = line_obj.browse(cursor, uid, line_id)
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            factura = f1.liniafactura_id[0].factura_id

            # Comprovem que el F1 es correcte
            expect(f1.state).to(equal('valid'))
            expect(f1.lectures_processades).to(equal(True))
            expect(f1.import_phase).to(equal(40))
            # Comprovem que la factura es correcte
            self.assertEqual(factura.amount_untaxed, 27.94)
            self.assertEqual(factura.check_total, 33.81)
            self.check_lectures_pool_polissa(polissa, [
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P1', 'periode.tarifa.name': '2.0DHA','lectura': 50, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-07-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 0, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
                {'tipus': 'A', 'name': "2019-08-31", 'periode.name': 'P2', 'periode.tarifa.name': '2.0DHA','lectura': 150, 'ajust': 0, 'lectura_exporta': 0, 'ajust_exporta': 0, 'motiu_ajust': False, 'comptador.name': "141244861"},
            ])

    def test_full_import_changes_contract_ref(self):
        line_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )
            line_obj.process_line_sync(cursor, uid, line_id)

            cups_id = line_obj.read(cursor, uid, line_id, ['cups_id'])['cups_id']
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')
            polissa_id = cups_obj.read(cursor, uid, cups_id[0], ['polissa_polissa'])['polissa_polissa']
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            pol_dades = pol_obj.read(cursor, uid, polissa_id[0], ['ref_dist'])['ref_dist']
            self.assertEqual(pol_dades, '11111')

    def test_invoice_activation_date_due_remains(self):
        pool = self.openerp.pool
        fact_obj = pool.get('giscedata.facturacio.factura')
        period_obj = pool.get('account.period')
        line_obj = pool.get('giscedata.facturacio.importacio.linia')
        line_fact_obj = pool.get('giscedata.facturacio.importacio.linia.factura')

        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_ids = line_fact_obj.search(cursor, uid, [('linia_id', '=', line_id)])

            fact_id = line_fact_obj.read(cursor, uid, line_ids[0], ['factura_id'])['factura_id']
            fact_id = fact_id[0]
            fact_obj.write(cursor, uid, fact_id, {'payment_term': 1})
            fact_data = fact_obj.read(cursor, uid, fact_id, ['date_due', 'invoice_id'])
            expect(fact_data['date_due']).to(equal('2016-03-30'))

            period_id = period_obj.search(cursor, uid, [], limit=1)[0]
            fact_obj.write(cursor, uid, fact_id, {'period_id': period_id})
            fact_obj.button_reset_taxes(cursor, uid, [fact_id])
            fact_obj.invoice_open(cursor, uid, [fact_id])

            fact_data = fact_obj.read(cursor, uid, fact_id, ['date_due', 'invoice_id'])
            expect(fact_data['date_due']).to(equal('2016-03-30'))

    def test_repeated_meter_import_works(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            cups_obj = self.openerp.pool.get('giscedata.cups.ps')
            cups_id = cups_obj.search(cursor, uid, [('name', '=', 'ES1234000000000001JN0F')])

            cups = cups_obj.browse(cursor, uid, cups_id[0])
            meters = cups.polissa_polissa.comptadors
            meters[0].write({'name': 'REPEB63011077'})

            prev_num_meters = len(meters)

            line_obj.process_line_sync(cursor, uid, line_id)

            cups = cups.browse()[0]
            new_num_meters = len(cups.polissa_polissa.comptadors)

            self.assertEqual(prev_num_meters, new_num_meters)

    def test_repeated_meter_distri_fenosa(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            cups_obj = self.openerp.pool.get('giscedata.cups.ps')
            cups_id = cups_obj.search(cursor, uid, [('name', '=', 'ES1234000000000001JN0F')])
            cups = cups_obj.browse(cursor, uid, cups_id[0])

            distribuidora_obj = self.openerp.pool.get('res.partner')
            distribuidora_obj.write(cursor, uid, cups.distribuidora_id.id, {'ref': '0022'})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'test_correct.xml'
            )

            with open(xml_path, 'r') as f:
                xml_file = f.read()
                xml_file = xml_file.replace(
                    '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
                    '<CodigoREEEmpresaEmisora>0022</CodigoREEEmpresaEmisora>'
                )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups = cups.browse()[0]

            meters = cups.polissa_polissa.comptadors
            meters[0].write({'name': '63011077'})

            prev_num_meters = len(meters)

            line_obj.process_line_sync(cursor, uid, line_id)

            cups = cups.browse()[0]
            new_num_meters = len(cups.polissa_polissa.comptadors)

            line_state = line_obj.read(cursor, uid, line_id, ['state'])['state']

            self.assertEqual(line_state, 'valid')
            self.assertEqual(prev_num_meters, new_num_meters)

    def test_we_import_extra_lines_correctly(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            xml_path = get_module_resource(
                'giscedata_facturacio_switching', 'tests', 'fixtures',
                'test_suplemento_territorial.xml'
            )

            with open(xml_path, 'r') as f:
                xml_file = f.read()
            xml_file = xml_file.replace(
                '<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>',
                '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
            )

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.browse(cursor, uid, line_id)
            expect(line_vals.state).to(equal('valid'))
            expect(line_vals.import_phase).to(equal(40))

            expect(len(line_vals.liniaextra_id)).to(equal(1))

            extra_line = line_vals.liniaextra_id[0]

            expect(len(extra_line.tax_ids)).to(equal(2))
            expect(
                {tax.name for tax in extra_line.tax_ids}
            ).to(
                equal(
                    {'IVA 21%', 'Impuesto especial sobre la electricidad'}
                )
            )

    def test_new_meter_is_created(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            xml_file = self.xml_file.replace(
                '<NumeroSerie>B63011077</NumeroSerie>',
                '<NumeroSerie>B99999999</NumeroSerie>'
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            old_compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(
                cursor, uid, old_compt_id, {
                    'active': False,
                    'data_baixa': '2015-02-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            old_compt_vals = compt_obj.read(cursor, uid, old_compt_id)
            expect(old_compt_vals['active']).to(equal(False))
            expect(old_compt_vals['data_alta']).to(equal('2015-01-01'))
            expect(old_compt_vals['data_baixa']).to(equal('2015-02-01'))

            new_compt_id = compt_obj.search_by_name(cursor, uid, 'B99999999')[0]
            new_compt_vals = compt_obj.read(cursor, uid, new_compt_id)
            expect(new_compt_vals['name']).to(equal('B99999999'))
            expect(new_compt_vals['active']).to(equal(True))
            expect(new_compt_vals['data_alta']).to(equal('2016-02-01'))
            expect(new_compt_vals['data_baixa']).to(equal(False))
            expect(new_compt_vals['giro']).to(equal(1000000))
            expect(new_compt_vals['preu_lloguer']).to(equal(0.017586))

    def test_active_meters_are_deactivated_on_change(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            xml_file = self.xml_file.replace(
                '<NumeroSerie>B63011077</NumeroSerie>',
                '<NumeroSerie>B99999999</NumeroSerie>'
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            old_compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            old_compt_vals = compt_obj.read(cursor, uid, old_compt_id)
            expect(old_compt_vals['active']).to(equal(False))
            expect(old_compt_vals['data_alta']).to(equal('2015-01-01'))
            expect(old_compt_vals['data_baixa']).to(equal('2016-01-31'))

            new_compt_id = compt_obj.search_by_name(cursor, uid, 'B99999999')[0]
            new_compt_vals = compt_obj.read(cursor, uid, new_compt_id)
            expect(new_compt_vals['name']).to(equal('B99999999'))
            expect(new_compt_vals['active']).to(equal(True))
            expect(new_compt_vals['data_alta']).to(equal('2016-02-01'))
            expect(new_compt_vals['data_baixa']).to(equal(False))
            expect(new_compt_vals['giro']).to(equal(1000000))
            expect(new_compt_vals['preu_lloguer']).to(equal(0.017586))

    def test_error_is_generated_on_conflict(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            xml_file = self.xml_file.replace(
                '<NumeroSerie>B63011077</NumeroSerie>',
                '<NumeroSerie>B99999999</NumeroSerie>'
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            old_compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(
                cursor, uid, old_compt_id, {
                    'active': True,
                    'data_alta': '2016-02-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(30))

            error_ids = error_obj.read(cursor, uid, line_vals['error_ids'])

            error_codes = []
            for error_id in error_ids:
                error_code = error_id.copy()
                error_code.pop('error_template_id')
                error_code.pop('id')
                error_code.pop('line_id')
                error_code.pop('message')
                error_codes.append(error_code)

            expect(error_codes).to(
                contain({'name': '3030', 'level': 'critical', 'active': True})
            )

    def test_activation_date_is_upated_on_import(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(
                cursor, uid, compt_id, {
                    'data_alta': '2017-01-01'
                }
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            compt_vals = compt_obj.read(cursor, uid, compt_id)
            expect(compt_vals['active']).to(equal(True))
            expect(compt_vals['data_alta']).to(equal('2016-02-01'))
            expect(compt_vals['data_baixa']).to(equal(False))
            expect(compt_vals['name']).to(equal('B63011077'))
            expect(compt_vals['preu_lloguer']).to(equal(0.017586))

    def test_baixa_date_is_upated_on_import(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')
        lect_obj = self.openerp.pool.get("giscedata.lectures.lectura")
        lect_pot_obj = self.openerp.pool.get("giscedata.lectures.potencia")
        lect_pool_obj = self.openerp.pool.get("giscedata.lectures.lectura.pool")
        lect_pool_pot_obj = self.openerp.pool.get("giscedata.lectures.potencia.pool")

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(
                cursor, uid, compt_id, {
                    'data_baixa': '2017-01-01',
                    'active': False
                }
            )

            # Delete lects after 2016-02-19
            params = [('comptador', '=', compt_id), ('name', '>=', '2016-02-19')]
            ctx = {'active_test': False}
            for obj in [lect_obj, lect_pot_obj, lect_pool_obj, lect_pool_pot_obj]:
                obj_ids = obj.search(cursor, uid, params, context=ctx)
                obj.unlink(cursor, uid, obj_ids, ctx)

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            compt_vals = compt_obj.read(cursor, uid, compt_id)
            expect(compt_vals['active']).to(equal(False))
            expect(compt_vals['data_alta']).to(equal('2015-01-01'))
            expect(compt_vals['data_baixa']).to(equal('2016-02-19'))
            expect(compt_vals['name']).to(equal('B63011077'))
            expect(compt_vals['preu_lloguer']).to(equal(0.017586))

    def test_meter_is_created_when_none_exists(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')
        res_config = self.openerp.pool.get('res.config')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            polissa_2 = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]

            compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(cursor, uid, [compt_id], {'polissa': polissa_2})

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            new_compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_vals = compt_obj.read(cursor, uid, new_compt_id)
            expect(compt_vals['active']).to(equal(True))
            expect(compt_vals['data_alta']).to(equal('2016-02-01'))
            expect(compt_vals['data_baixa']).to(equal(False))
            expect(compt_vals['name']).to(equal('B63011077'))
            expect(compt_vals['giro']).to(equal(1000000))
            expect(compt_vals['preu_lloguer']).to(equal(0.017586))

    def test_new_meters_can_be_imported_before_currently_active_ones(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            xml_file = self.xml_file.replace(
                '<NumeroSerie>B63011077</NumeroSerie>',
                '<NumeroSerie>B99999999</NumeroSerie>'
            )

            post_compt_id = compt_obj.search_by_name(cursor, uid, 'B63011077')[0]
            compt_obj.write(
                cursor, uid, post_compt_id, {
                    'data_alta': '2017-01-01'
                }
            )

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)

            post_compt_vals = compt_obj.read(cursor, uid, post_compt_id)
            expect(post_compt_vals['active']).to(equal(True))
            expect(post_compt_vals['data_alta']).to(equal('2017-01-01'))
            expect(post_compt_vals['data_baixa']).to(equal(False))

            pre_compt_id = compt_obj.search_by_name(cursor, uid, 'B99999999')[0]
            pre_compt_vals = compt_obj.read(cursor, uid, pre_compt_id)
            expect(pre_compt_vals['name']).to(equal('B99999999'))
            expect(pre_compt_vals['active']).to(equal(False))
            expect(pre_compt_vals['data_alta']).to(equal('2016-02-01'))
            expect(pre_compt_vals['data_baixa']).to(equal('2016-12-31'))
            expect(pre_compt_vals['giro']).to(equal(1000000))
            expect(pre_compt_vals['preu_lloguer']).to(equal(0.017586))
            
    def test_incorrect_new_f1_return_case_001(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')

        # We get the xml directly from the 'fixtures' directory to get the
        # new version
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures', 'old',
            'test_correct.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        # But we edit it to make the xml format incorrect
        xml_file = xml_file.replace(
            'CodigoFiscalFactura', 'ThisFieldIsIncorrect'
        )
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )
            address_obj.create(cursor, uid, {'partner_id': distri_id})

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(10))

            expect(len(line_vals['error_ids'])).to(equal(1))
            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])

            expect(error.error_template_id.phase).to(equal('1'))
            expect(error.error_template_id.code).to(equal('001'))

    def test_old_f1_return_special_case(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')

        # We get the xml from the 'old' directory to get the old version
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures', 'old',
            'test_correct.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )
            address_obj.create(cursor, uid, {'partner_id': distri_id})

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(10))

            expect(len(line_vals['error_ids'])).to(equal(1))
            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])

            expect(error.error_template_id.phase).to(equal('1'))
            expect(error.error_template_id.code).to(equal('009'))

    def test_simultanious_rent_meters(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn, context={"test_xml": "test_simultanious_rents.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['import_phase']).to(equal(40))
            # Check if meter rent price is correct
            line = line_obj.browse(cursor, uid, line_id)
            comptador = line.cups_id.polissa_polissa.comptadors[0]
            self.assertEqual(comptador.preu_lloguer, 0.017586)

    def test_extra_line_negative_quantity(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn, context={
                "test_xml": "test_extra_line_negative_quantity.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['import_phase']).to(equal(40))
            # Check if extralines are correctlly created
            line = line_obj.browse(cursor, uid, line_id)
            extralines = line.liniaextra_id
            self.assertEqual(extralines[0].quantity, 1)
            self.assertEqual(extralines[0].price_unit, 12.06)
            self.assertEqual(extralines[1].quantity, 1)
            self.assertEqual(extralines[1].price_unit, -12.06)

    def test_extra_line_fiance_negative_quantity(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn, context={
                "test_xml": "F1_test_indemnitzacio_unitat_negativa_preu_positiu.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['import_phase']).to(equal(40))
            # Check if extralines are correctlly created
            line = line_obj.browse(cursor, uid, line_id)
            extralines = line.liniaextra_id
            self.assertEqual(extralines[0].quantity, 1)
            self.assertEqual(extralines[0].price_unit, -30.05)

    def test_ignore_reactive(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.prepare_for_phase_3(txn, context={
                "test_xml": "test_ignore_reactive.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('valid'))
            expect(line_vals['import_phase']).to(equal(40))

    def test_ignore_totalitzador(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            tarif_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'tarifa_20DHA_new'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {'tarifa': tarif_id}
            )

            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            comptador = polissa.comptadors[0]
            for l in comptador.lectures:
                l.unlink(context={})
            for lp in comptador.lectures_pot:
                lp.unlink(context={})
            comptador.write({'lectures': [(6, 0, [])],
                             'lectures_pot': [(6, 0, [])]})

            self.prepare_for_phase_3(txn, context={"test_xml": "test_ignore_totalitzador.xml"})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            # Check lectures
            self.assertEqual(
                len(line.liniafactura_id[0].factura_id.lectures_energia_ids), 2
            )

    def test_f1_conceptes_correcte(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pot_obj = self.openerp.pool.get("giscedata.polissa.potencia.contractada.periode")
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            "F1_conceptes.xml"
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>0024</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )
        self.xml_file = self.xml_file.replace(
            'ES0031406165417006PF0F', 'ES1234000000000001JN0F'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0031')]
            )[0]
            pricelist_id = pricelist_obj.search(
                cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
            )[0]

            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'cups': cups_id,
                    'state': 'activa',
                    'distribuidora': distri_id,
                    'facturacio_potencia': 'icp',
                    'potencia': 5.75,
                }
            )
            pol_pots = polissa_obj.read(
                cursor, uid, polissa_id, ['potencies_periode']
            )['potencies_periode']
            pot_obj.write(cursor, uid, pol_pots, {'potencia': 5.750})
            cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
            address_obj.create(cursor, uid, {'partner_id': distri_id})
            partner_obj.write(
                cursor, uid, distri_id,
                {'property_product_pricelist_purchase': pricelist_id}
            )

            self.activar_polissa_CUPS(txn)
            self.add_supported_iva_to_periods(txn)
            self.add_supported_iva_to_concepts(txn)
            self.add_sell_taxes_to_suplemento_territorial(txn)

            dalta = polissa_obj.read(cursor, uid, polissa_id, ['data_alta'])['data_alta']
            self.xml_file = self.xml_file.replace('2015-11-30', dalta)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            self.assertEqual(len(line.liniafactura_id), 1)
            self.assertEqual(line.liniafactura_id[0].factura_id.amount_total, 10.94)
            self.assertEqual(line.liniafactura_id[0].factura_id.check_total, 10.94)

    def test_f1_conceptes_correcte_dia_abans_alta(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pot_obj = self.openerp.pool.get(
            "giscedata.polissa.potencia.contractada.periode")
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            "F1_conceptes.xml"
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>0024</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )
        self.xml_file = self.xml_file.replace(
            'ES0031406165417006PF0F', 'ES1234000000000001JN0F'
        )

        self.openerp.install_module('giscedata_tarifas_peajes_20150101')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0031')]
            )[0]
            pricelist_id = pricelist_obj.search(
                cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
            )[0]

            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'cups': cups_id,
                    'state': 'activa',
                    'distribuidora': distri_id,
                    'facturacio_potencia': 'icp',
                    'potencia': 5.75,
                }
            )
            pol_pots = polissa_obj.read(
                cursor, uid, polissa_id, ['potencies_periode']
            )['potencies_periode']
            pot_obj.write(cursor, uid, pol_pots, {'potencia': 5.750})
            cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
            address_obj.create(cursor, uid, {'partner_id': distri_id})
            partner_obj.write(
                cursor, uid, distri_id,
                {'property_product_pricelist_purchase': pricelist_id}
            )

            self.activar_polissa_CUPS(txn)
            self.add_supported_iva_to_periods(txn)
            self.add_supported_iva_to_concepts(txn)
            self.add_sell_taxes_to_suplemento_territorial(txn)

            dalta = polissa_obj.read(cursor, uid, polissa_id, ['data_alta'])['data_alta']
            dalta = datetime.strptime(dalta, "%Y-%m-%d") - timedelta(days=1)
            dalta = dalta.strftime("%Y-%m-%d")
            self.xml_file = self.xml_file.replace('2015-11-30', dalta)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            self.assertEqual(len(line.liniafactura_id), 1)
            self.assertEqual(line.liniafactura_id[0].factura_id.amount_total, 10.94)
            self.assertEqual(line.liniafactura_id[0].factura_id.check_total, 10.94)

    def test_f1_conceptes_correcte_daia_polissa_llunyana(self):
        # it tests the following scenario:
        #            pol_id_closed                         pol_id_ok
        # |2015-01-01-------------2015-05-28|.|2016-01-01---------2016-05-28|. *
        #                                                                      ^
        #                                   the invoice date is 2016-11-30 ----|
        # so the test will pass if it founds the pol_id_ok.
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pot_obj = self.openerp.pool.get(
            "giscedata.polissa.potencia.contractada.periode")
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            "F1_conceptes.xml"
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>0024</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )
        self.xml_file = self.xml_file.replace(
            'ES0031406165417006PF0F', 'ES1234000000000001JN0F'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]

            # data alta polissa = 2016-01-01
            pol_id_ok = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            pol_id_closed = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0002'
            )[1]
            pol_id_conflict_with_pol_id_ok = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0006'
            )[1]
            polissa_obj.write(
                cursor, uid, pol_id_conflict_with_pol_id_ok, {
                    'data_alta': "1900-01-01"
                }
            )
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0031')]
            )[0]
            pricelist_id = pricelist_obj.search(
                cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
            )[0]

            # first of all, we need to set the oldest contract
            polissa_obj.write(
                cursor, uid, pol_id_closed, {
                    'data_alta': "2015-01-01",
                    'data_baixa': "2015-05-28"
                }
            )
            # and close it
            polissa_obj.send_signal(cursor, uid, [pol_id_closed], 'baixa')
            # in order to link the same cups of the newest contract
            polissa_obj.write(
                cursor, uid, pol_id_closed, {
                    'cups': cups_id,
                }
            )

            pol_pots = polissa_obj.read(
                cursor, uid, pol_id_ok, ['potencies_periode']
            )['potencies_periode']
            pot_obj.write(cursor, uid, pol_pots, {'potencia': 5.750})
            cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
            address_obj.create(cursor, uid, {'partner_id': distri_id})
            partner_obj.write(
                cursor, uid, distri_id,
                {'property_product_pricelist_purchase': pricelist_id}
            )

            # and then set the newest contract
            polissa_obj.write(
                cursor, uid, pol_id_ok, {
                    'cups': cups_id,
                    'state': 'activa',
                    'distribuidora': distri_id,
                    'facturacio_potencia': 'icp',
                    'potencia': 5.75
                }
            )
            self.activar_polissa_CUPS(txn)
            # and after activate it, set the end date in order to avoid the old
            # behavior and use the new one.
            polissa_obj.write(
                cursor, uid, pol_id_ok, {
                    'data_baixa': "2016-05-28"
                }
            )
            self.add_supported_iva_to_periods(txn)
            self.add_supported_iva_to_concepts(txn)
            self.add_sell_taxes_to_suplemento_territorial(txn)

            # and finally, set the invoice date some months after the last
            # contract.
            self.xml_file = self.xml_file.replace('2015-11-30', '2016-11-30')

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            self.assertEqual(len(line.liniafactura_id), 1)
            self.assertEqual(line.liniafactura_id[0].factura_id.amount_total, 10.94)

    def test_f1_conceptes_preu_client_diferent_preu_cost(self):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        pricelist_obj = self.openerp.pool.get('product.pricelist')
        pl_version_obj = self.openerp.pool.get('product.pricelist.version')
        pl_item_obj = self.openerp.pool.get('product.pricelist.item')
        pol_mf_obj = self.openerp.pool.get('giscedata.polissa.mode.facturacio')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        address_obj = self.openerp.pool.get('res.partner.address')
        imd_obj = self.openerp.pool.get('ir.model.data')
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        pot_obj = self.openerp.pool.get("giscedata.polissa.potencia.contractada.periode")
        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            "F1_conceptes.xml"
        )

        with open(xml_path, 'r') as f:
            self.xml_file = f.read()
        self.xml_file = self.xml_file.replace(
            '<CodigoREEEmpresaEmisora>0024</CodigoREEEmpresaEmisora>',
            '<CodigoREEEmpresaEmisora>0031</CodigoREEEmpresaEmisora>'
        )
        self.xml_file = self.xml_file.replace(
            'ES0031406165417006PF0F', 'ES1234000000000001JN0F'
        )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            concept_04_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_comer', 'concepte_04'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0031')]
            )[0]
            tarifas_electricidad_pl_id = pricelist_obj.search(
                cursor, uid, [('name', '=', 'TARIFAS ELECTRICIDAD')]
            )[0]

            # Create pricelist version with product value
            pricelist_concepts_id = pricelist_obj.create(cursor, uid, {
                'name': 'TARIFA PRECIOS CONCEPTOS',
                'type': 'sale',
            })

            pl_version_concept_id = pl_version_obj.create(cursor, uid, {
                'name': 'VERSION TARIFA PRECIOS CONCEPTOS',
                'pricelist_id': pricelist_concepts_id,
                'date_start': '2015-01-01',
                'date_end': False,
            })
            pl_item_obj.create(cursor, uid, {
                'name': 'Concepto 04 precio cliente',
                'product_id': concept_04_id,
                'price_version_id': pl_version_concept_id,
                'sequence': 5,
                'base_price': 3311.22,
                'base': -3,  # Pricelist item price
            })
            pl_item_obj.create(cursor, uid, {
                'name': 'Otros',
                'product_id': False,
                'price_version_id': pl_version_concept_id,
                'sequence': 99,
                'base': -1,  # Other pricelist
                'base_pricelist_id': tarifas_electricidad_pl_id
            })

            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            polissa.tarifa.write({
                'llistes_preus_comptatibles': [(4, pricelist_concepts_id)]
            })
            mode_id = pol_mf_obj.search(cursor, uid, [
                ('code', '=', polissa.mode_facturacio)], limit=1)[0]
            pol_mf_obj.write(cursor, uid, [mode_id], {
                'compatible_price_lists': [(4, pricelist_concepts_id)]
            })
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'cups': cups_id,
                    'state': 'activa',
                    'distribuidora': distri_id,
                    'facturacio_potencia': 'icp',
                    'potencia': 5.75,
                    'llista_preu': pricelist_concepts_id
                }
            )
            pol_pots = polissa_obj.read(
                cursor, uid, polissa_id, ['potencies_periode']
            )['potencies_periode']
            pot_obj.write(cursor, uid, pol_pots, {'potencia': 5.750})
            cups_obj.write(cursor, uid, cups_id, {'distribuidora_id': distri_id})
            address_obj.create(cursor, uid, {'partner_id': distri_id})
            partner_obj.write(
                cursor, uid, distri_id,
                {'property_product_pricelist_purchase': tarifas_electricidad_pl_id}
            )

            self.activar_polissa_CUPS(txn)
            self.add_supported_iva_to_periods(txn)
            self.add_supported_iva_to_concepts(txn)
            self.add_sell_taxes_to_suplemento_territorial(txn)

            dalta = polissa_obj.read(cursor, uid, polissa_id, ['data_alta'])['data_alta']
            self.xml_file = self.xml_file.replace('2015-11-30', dalta)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            # Check is imported correctlly
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))
            self.assertEqual(len(line.liniafactura_id), 1)
            self.assertEqual(line.liniafactura_id[0].factura_id.amount_total, 10.94)
            self.assertEqual(line.liniafactura_id[0].factura_id.check_total, 10.94)
            extralines = line.liniaextra_id
            self.assertEqual(extralines[0].quantity, 1)
            self.assertEqual(extralines[0].price_unit, 3311.22)

    def test_validation_2001_fail(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_rectificadora.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0003'
            )[1]

            params = {
                'origin_date_invoice': '2016-07-01',
                'origin': '20160122100000024',
                'partner_id': distri_id
            }
            fact_obj.write(cursor, uid, [fact_id], params)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            user_obj = self.openerp.pool.get('res.users')
            user_browse = user_obj.browse(cursor, uid, uid)
            user_browse.company_id.partner_id.write({'ref': '4321'})
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('001'))

    def test_validation_2001_success(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_rectificadora.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0003'
            )[1]

            params = {
                'origin_date_invoice': '2015-07-01',
                'origin': '20160122100000024',
                'partner_id': distri_id
            }
            fact_obj.write(cursor, uid, [fact_id], params)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching',
                'f1_import_01'
            )[1]

            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            user_obj = self.openerp.pool.get('res.users')
            user_browse = user_obj.browse(cursor, uid, uid)
            user_browse.company_id.partner_id.write({'ref': '4321'})
            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(20))
            expect(line_vals['lectures_processades']).to(equal(False))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid,
                                     line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('2'))
            expect(error.error_template_id.code).to(equal('007'))

    def test_f1_error_if_same_dates(self):
        line_obj = self.openerp.pool.get(
            'giscedata.facturacio.importacio.linia'
        )
        error_obj = self.openerp.pool.get(
            'giscedata.facturacio.switching.error'
        )
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        f_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'F1_rectificadora.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace(
                "<FechaHastaFactura>2015-09-10</FechaHastaFactura>",
                "<FechaHastaFactura>2015-07-16</FechaHastaFactura>"
            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            # First we import a corerct F1 to have an invoice in the db
            self.prepare_for_phase_3(txn)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', self.xml_file
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line = line_obj.browse(cursor, uid, line_id)
            expect(line.state).to(equal('valid'))
            expect(line.import_phase).to(equal(40))

            # Delete origin from new invoice
            factura = f_obj.browse(cursor, uid,
                                   line.liniafactura_id[0].factura_id.id)
            factura.write({'origin': False})
            factura = f_obj.browse(cursor, uid,
                                   line.liniafactura_id[0].factura_id.id)
            self.assertFalse(factura.origin)

            # Now we import a rectificative without ref code
            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching', 'f1_import_01'
            )[1]
            line_id = line_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            line_obj.process_line_sync(cursor, uid, line_id)
            line_vals = line_obj.read(cursor, uid, line_id)
            expect(line_vals['state']).to(equal('erroni'))
            expect(line_vals['import_phase']).to(equal(10))
            expect(len(line_vals['error_ids'])).to(equal(1))

            error = error_obj.browse(cursor, uid, line_vals['error_ids'][0])
            expect(error.error_template_id.phase).to(equal('1'))
            expect(error.error_template_id.code).to(equal('010'))

    def test_import_xml_fail_phase_1_error_1006(self):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
        error_obj = self.openerp.pool.get('giscedata.facturacio.switching.error')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        user_obj = self.openerp.pool.get('res.users')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_utils.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace(
                "<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>",
                "<CodigoREEEmpresaEmisora>0021</CodigoREEEmpresaEmisora>"
            )
            xml_file = xml_file.replace(
                "<CUPS>ES0291000000000023XB0F</CUPS>",
                "<CUPS>ES1234000000000001JN0F</CUPS>"
            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            user_browse = user_obj.browse(cursor, uid, uid)
            partner_id = user_browse.company_id.partner_id.id
            partner_obj.write(cursor, uid, [partner_id], {'ref': '4321'})

            distri_id = partner_obj.search(
                cursor, uid, [('ref', '=', '0021')]
            )[0]

            fact_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0003'
            )[1]

            params = {
                'origin_date_invoice': '2015-07-01',
                'origin': '20160122100000024',
                'partner_id': distri_id
            }
            fact_obj.write(cursor, uid, [fact_id], params)

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching',
                'f1_import_01'
            )[1]

            line_id = linia_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_01'
            )[1]
            cups_obj.write(
                cursor, uid, cups_id, {'distribuidora_id': distri_id}
            )

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(
                cursor, uid, polissa_id, {
                    'data_alta': '2015-01-01',
                    'data_baixa': '2016-01-01'
                }
            )

            linia_obj.process_line_sync(cursor, uid, line_id)

            line_id2 = linia_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            linia_obj.process_line_sync(cursor, uid, line_id2)

            errors = error_obj.search(
                cursor, uid, [('line_id', '=', line_id2)]
            )
            expect(len(errors)).to(equal(1))
            error = error_obj.browse(cursor, uid, errors[0])
            expect(error.name).to(equal("1006"))
            expect(error.message).to(equal(
                "Aquest fitxer XML ja s'ha processat en els següents IDs: "
                "{} i són en fases més avançades del procés.".format([line_id])
            ))

    def test_import_xml_fail_phase_1_error_1066(self):
        linia_obj = self.openerp.pool.get('giscedata.facturacio.importacio.linia')
        error_obj = self.openerp.pool.get('giscedata.facturacio.switching.error')
        partner_obj = self.openerp.pool.get('res.partner')
        imd_obj = self.openerp.pool.get('ir.model.data')
        user_obj = self.openerp.pool.get('res.users')

        xml_path = get_module_resource(
            'giscedata_facturacio_switching', 'tests', 'fixtures',
            'test_utils.xml'
        )

        with open(xml_path, 'r') as f:
            xml_file = f.read()
            xml_file = xml_file.replace(
                "<CodigoREEEmpresaEmisora>1234</CodigoREEEmpresaEmisora>",
                "<CodigoREEEmpresaEmisora>0021</CodigoREEEmpresaEmisora>"

            )

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            user_browse = user_obj.browse(cursor, uid, uid)
            partner_id = user_browse.company_id.partner_id.id
            partner_obj.write(cursor, uid, [partner_id], {'ref': '4321'})

            import_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_switching',
                'f1_import_01'
            )[1]

            line_id = linia_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            line_id2 = linia_obj.create_from_xml(
                cursor, uid, import_id, 'Import Name', xml_file
            )

            linia_obj.process_line_sync(cursor, uid, line_id)

            errors = error_obj.search(
                cursor, uid, [('line_id', '=', line_id)]
            )
            expect(len(errors)).to(equal(1))
            error = error_obj.browse(cursor, uid, errors[0])
            expect(error.name).to(equal("1066"))
            expect(error.message).to(equal(
                "Aquest fitxer XML ja s'ha processat en els següents IDs: {}, "
                "que també estàn en fase 1. És possible que el fitxer estigui "
                "duplicat al ZIP d'entrada.".format([line_id2, line_id])
            ))
