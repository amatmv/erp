# -*- coding: utf-8 -*-
import base64
import hashlib
import zipfile
import logging
import re
from datetime import datetime

import pooler
from osv import osv, fields
from tools.translate import _
from oorq.decorators import job
from giscedata_facturacio_switching_utils import (
    get_remeses_from_zip, get_remesa_from_xml, get_cups_from_xml,
    get_header_from_xml
)
from osv.expression import OOQuery
from sql.aggregate import Count
from gestionatr.defs import TABLA_102, TABLA_106

from gestionatr.input.messages import F1, message
from gestionatr.input.messages.F1 import CODIS_AUTOCONSUM
from switching.input.messages import F1 as OldF1, message as old_message

_IMPORT_PHASES = [
    (10, _(u'1 - Càrrega XML')),
    (20, _(u'2 - Creació Factura')),
    (30, _(u'3 - Validació dades')),
    (40, _(u'4 - Càrrega de dades')),
    (50, _(u'5 - Gestionat manualment')),
]


def get_md5(data):
    cleaned_data = data.replace(" ", "")
    cleaned_data = cleaned_data.replace("\n", "")
    cleaned_data = cleaned_data.replace("\t", "")
    md5 = hashlib.md5(cleaned_data)
    return md5.hexdigest()


class GiscedataFacturacioImportacio(osv.osv):
    """Agrupació d'importacions"""

    _name = 'giscedata.facturacio.importacio'
    _description = "Agrupació d'importacions"

    def unlink(self, cursor, uid, ids, context=None):
        """S'esborren les importacions que no contenen factures
           obertes. En el cas de tenir factures obertes, s'esborren
           únicament les línies de d'importació sense factures obertes
           i aquestes factures"""
        lin = self.pool.get('giscedata.facturacio.importacio.linia')
        for iid in ids:
            ivals = self.read(cursor, uid, iid, ['linia_ids'])
            if lin.unlink(cursor, uid, ivals['linia_ids']):
                super(GiscedataFacturacioImportacio, self).unlink(cursor, uid,
                                                        iid, context=context)

    def _ff_check_status(self, cursor, uid, ids, field_name, arg,
                         context=None):
        if context is None:
            context = {}
        res = {}
        lin_pool = self.pool.get('giscedata.facturacio.importacio.linia')
        linfact_pool = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura')
        fact_obj = self.pool.get(
            'giscedata.facturacio.factura'
        )
        for i_id in ids:
            # Ids of the lines in the import
            linies = lin_pool.search(
                cursor, uid, [('importacio_id', '=', i_id)], context=context)
            # Total number of lines in the import
            n_linies = len(linies)
            # Total number of already processed lines
            n_linies_proc = lin_pool.search_count(
                cursor, uid, [
                    ('importacio_id', '=', i_id), ('state', '!=', False)
                ]
            )
            # Total number of correct lines
            n_linies_ok = lin_pool.search_count(
                cursor, uid, [
                    ('importacio_id', '=', i_id), ('state', '=', 'valid')
                ]
            )

            linfact_ids = linfact_pool.search(
                cursor, uid, [('linia_id', 'in', linies)]
            )

            # Total number of created invoices
            fact_count = len(linfact_ids)

            linfact_data = linfact_pool.read(
                cursor, uid, linfact_ids, ['factura_id']
            )

            fact_ids = [x['factura_id'][0] for x in linfact_data]

            fact_data = fact_obj.read(
                cursor, uid, fact_ids, ['tipo_rectificadora']
            )

            resum = {}
            for fact in fact_data:
                tipus_rect = fact['tipo_rectificadora']
                if not resum.get(tipus_rect, False):
                    resum[fact['tipo_rectificadora']] = 0
                resum[fact['tipo_rectificadora']] += 1

            # Create resum text
            digest_text = ''
            types_dict = dict(TABLA_102)
            for key, value in resum.items():
                title = types_dict.get(key[0], key[0])
                digest_text += '{0}: {1}\n'.format(title, value)

            complet = (n_linies_ok == n_linies)

            if n_linies:
                progress = float(n_linies_proc)/n_linies * 100
                progress_importation = float(n_linies_ok)/n_linies * 100
            else:
                # If we don't have any line to import we have imported all
                progress = 100
                progress_importation = 100
            res[i_id] = {
                'importat_ok': complet,
                'num_xml': n_linies,
                'num_fact_creades': fact_count,
                'info': u"{0} XML importats, {1} factures creades".format(
                    n_linies_proc, fact_count
                ),
                'digest': digest_text,
                'progres': progress,
                'progres_importation': progress_importation,
            }
        return res

    def _ff_opened(self, cursor, uid, ids, field_name, arg, context=None):
        self_obj = self.pool.get('giscedata.facturacio.importacio')
        self_querier = OOQuery(self_obj, cursor, uid)
        # linies que estan ja han passat l'estat 10
        linia_20_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        linia_20_querier = OOQuery(linia_20_obj, cursor, uid)

        # linies que estan ja han passat l'estat 20, factures creades
        linia_30_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )
        linia_30_querier = OOQuery(linia_30_obj, cursor, uid)

        res = {}

        for importacio_id in ids:
            # Linies importades → total XML de la carrega
            search_params = [
                ('importacio_id', '=', importacio_id)
            ]
            linia_20_query = linia_20_querier.select(
                ['id', 'state']
            ).where(
                search_params
            )
            cursor.execute(*linia_20_query)
            linies_20_vals = cursor.dictfetchall()

            # Valid lines
            linies_20_ids_valides = []
            # XML no processat
            n_unprocessed_lines = 0
            # XML no generat correctament
            n_invalid_lines = 0
            for linia in linies_20_vals:
                if linia['state']:
                    if linia['state'] == 'valid':
                        linies_20_ids_valides.append(linia['id'])
                    else:
                        n_invalid_lines += 1
                else:
                    n_unprocessed_lines += 1

            n_linies_30 = 0
            if linies_20_ids_valides:
                search_params = [
                    ('linia_id', 'in', linies_20_ids_valides),
                    ('factura_id.invoice_id.state', 'in', ['open', 'paid'])
                ]
                linia_30_query = linia_30_querier.select(
                    [Count('id')],
                    order_by=None
                ).where(
                    search_params
                )
                cursor.execute(*linia_30_query)

                # Total number of created invoices
                n_linies_30 = cursor.fetchall()[0][0]

            res[importacio_id] = 0.0
            total_invoices = n_linies_30 + n_unprocessed_lines + n_invalid_lines
            if total_invoices > 0:
                res[importacio_id] = float(n_linies_30) / total_invoices * 100
        return res

    def _get_importacio_ids(self, cursor, uid, ids, context=None):
        l_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        i_id = []
        for a in l_obj.read(cursor, uid, ids, ['importacio_id']):
            if a['importacio_id']:
                i_id.append(a['importacio_id'][0])
        return i_id

    _STORE_PROC_AGREE_FIELDS = {
        'giscedata.facturacio.importacio.linia': (_get_importacio_ids,
                              ['state'], 20),
    }

    def _ff_trobar_polissa(self, cursor, uid, ids, field_name, arg,
                            context=None):
        """Aquest camp funció no es mostrarà en cap formulari.
           No ha d'actualitzar cap id.
        """
        return dict([(i, False) for i in ids])

    def _trobar_polissa(self, cursor, uid, ids, field, arg, context=None):
        """Retorna els ids dels lots d'importació amb factures de
           a la pòlissa especificada
        """
        if not context:
            context = {}
        poli = self.pool.get('giscedata.polissa')
        _poli = arg[0][2]
        p_id = poli.search(cursor, uid, [('name', '=', _poli)],
                                            context={'active_test': False})
        if not p_id:
            return [('id', 'in', [])]
        journal_obj = self.pool.get('account.journal')
        journal_code = 'CENERGIA'
        jid = journal_obj.search(cursor, uid, [('code', '=', journal_code)],
                                 limit=1)
        if not jid:
            return [('id', 'in', [])]
        fact = self.pool.get('giscedata.facturacio.factura')
        search_params = [('journal_id', '=', jid[0]),
                         ('polissa_id', '=', p_id[0])]
        ids = fact.search(cursor, uid, search_params,
                                            context={'active_test': False})
        vals = fact.read(cursor, uid, ids, ['importacio_id'])
        i_id = [i['importacio_id'][0] for i in vals]
        return [('id', 'in', i_id)]

    def _ff_data_carrega(self, cursor, uid, ids, field_name, arg,
                         context=None):
        "Data de càrrega del fitxer"
        if not context:
            context = {}
        res = {}
        dates = self.perm_read(cursor, uid, ids)
        res = dict([(d['id'], d['create_date'].split('.')[0]) for d in dates])

        return res

    def create_payment_orders_from_file(self, cursor, uid, import_file,
                                        context=None):
        if context is None:
            context = {}
        remeses = []
        if isinstance(import_file, zipfile.ZipFile):
            remeses = get_remeses_from_zip(import_file)
        else:
            remeses.append(get_remesa_from_xml(import_file))
        fswh_obj = self.pool.get('giscedata.facturacio.switching.helper')
        fswconfig_obj = self.pool.get('giscedata.facturacio.switching.config')
        partner_obj = self.pool.get('res.partner')
        # generate's payment orders (remeses)
        for remesa in remeses:
            # gets emissor reference
            try:
                po_emisor_id = fswconfig_obj.get_emissor(
                    cursor, uid, remesa['emisor'], context
                )
            except message.except_f1, e:
                # The DSO with this code is not found
                continue

            if po_emisor_id:
                po_emisor = partner_obj.read(
                    cursor, uid, po_emisor_id, ['ref']
                )['ref']
                if po_emisor:
                    fswh_obj.get_payment_order(
                        cursor, uid, po_emisor, remesa, context=context
                    )
        return True

    def process_import(self, cursor, uid, import_id, context=None):
        """ Process the lines of an import. If the error parameter is True,
        the method will only import those lines that are in state:'error'. """
        if context is None:
            context = {}
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        search_params = [('importacio_id', '=', import_id)]

        if context.get('import_errors', False):
            search_params += [
                '|',
                ('state', '=', False),
                ('state', '=', 'erroni')
            ]

        lines_ids = line_obj.search(
            cursor, uid, search_params
        )

        # Llegir els cups_text de lines_ids
        lines_vals = sorted(
            line_obj.read(
                cursor, uid, lines_ids, ['cups_text', 'codi_sollicitud'],
                context=context
            ), key=lambda k: k['codi_sollicitud']
        )

        # Group line_ids by cups_text
        cups_dict = {}

        for linia in lines_vals:
            fac_data = cups_dict.setdefault(linia['cups_text'], {})
            fac_data.setdefault('line_ids', []).append(linia['id'])

        # Format {CUP_TEXT_1: {line_ids: [line1, line2]}, CUP_TEXT_2...}

        db = pooler.get_db_only(cursor.dbname)

        for cups_t in cups_dict:
            try:
                tmp_cr = db.cursor()
                line_obj.process_line(
                    tmp_cr, uid, cups_dict[cups_t]['line_ids'], context=context
                )
                tmp_cr.commit()
            except Exception, e:
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()
                tmp_cr.rollback()
            finally:
                tmp_cr.close()


    def update_state(self, cursor, uid, import_id, context=None):
        # We don't need to update anything since it's now a field function
        return True

        if context is None:
            context = {}
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        search_params = [
            ('importacio_id', '=', import_id), ('state', '!=', False)
        ]
        lines_processed_ids = line_obj.search(cursor, uid, search_params)
        search_params = [
            ('importacio_id', '=', import_id)
        ]
        lines_ids = line_obj.search(cursor, uid, search_params)
        progress = float(len(lines_processed_ids))/len(lines_ids) * 100
        # Actualitzar el lot d'importació
        i_num = self.read(cursor, uid, import_id,
                                ['num_xml', 'num_fact_creades'], context)
        i_info = (_(u"%d XML importats, %d factures creades") %
                            (i_num['num_xml'], i_num['num_fact_creades']))
        i_vals = {'info': i_info, 'progres': progress}
        self.write(cursor, uid, [import_id], i_vals)
        return True

    def importat_ok_search(self, cursor, uid, obj, name, args, context):
        cursor.execute("SELECT DISTINCT importacio_id from giscedata_facturacio_importacio_linia where state != 'valid'")
        incorrect_ids = cursor.fetchall()
        incorrect_ids = [x[0] for x in incorrect_ids]
        operator = 'not in' if args[0][2] == 1 else 'in'
        return [('id', operator, incorrect_ids)]

    _columns = {
        'info': fields.function(
            _ff_check_status, string='Informació', type='text', multi='proc',
            method=True
        ),
        'digest': fields.function(
            _ff_check_status, string='Resum', type='text', multi='proc',
            method=True
        ),
        'name': fields.char('Nom Fitxer', size=1024, required=True),
        'linia_ids': fields.one2many(
                            'giscedata.facturacio.importacio.linia',
                            'importacio_id', 'Linies', ondelete='cascade'),
        'progres': fields.function(
            _ff_check_status, string='Progrés', readonly=True, method=True,
            type='float', multi='proc'
        ),
        'progres_importation': fields.function(
            _ff_check_status, string='Finalitzats', readonly=True, method=True,
            type='float', multi='proc'
        ),
        'progres_obertes': fields.function(
            _ff_opened, string='Oberts', method=True, type='float'
        ),
        'importat_ok': fields.function(_ff_check_status, method=True,
                                       string="Sense errors", type='boolean',
                                       multi='proc', fnct_search=importat_ok_search),
        'num_xml': fields.function(_ff_check_status, method=True,
                                       string="Total XMLs", type='integer',
                                       multi='proc'),
        'num_fact_creades': fields.function(_ff_check_status, method=True,
                                       string="Total factures creades",
                                       type='integer',
                                       multi='proc'),
        'partner_id': fields.many2one('res.partner', 'Empresa emisora',
                                      required=False),
        'hash': fields.char('Hash', size=32, requiered=True, readonly=True),
        'ff_polissa': fields.function(_ff_trobar_polissa, type='char',
                                      size='30', string='Contracte',
                                      fnct_search=_trobar_polissa,
                                      method=True),
        'data_carrega': fields.function(_ff_data_carrega, type='datetime',
                                        string='Data Càrrega', method=True)
    }

    _defaults = {
        'progres': lambda *a: 0,
    }

    _order = "id desc"

GiscedataFacturacioImportacio()

class LineProcessException(Exception):
    pass


class GiscedataFacturacioImportacioLinia(osv.osv):
    """Agrupació d'importacions"""

    _name = 'giscedata.facturacio.importacio.linia'
    _description = "Llistat de fitxers importats"

    def unlink(self, cursor, uid, ids, context=None):
        """S'esborren les línies d'importació que no contenen:
           + Factures de proveïdor obertes
           + Extres vinculats a factures de client
           Retorna:
           + True si esborra totes les línies
           + False en cas contrari
        """
        if not context:
            context = {}
        imp = self.pool.get('giscedata.facturacio.importacio')
        lf = self.pool.get('giscedata.facturacio.importacio.linia.factura')
        fact = self.pool.get('giscedata.facturacio.factura')
        sum_fact = 0
        u_imp = []
        u_linia = []
        u_linfac = []
        u_fact = []
        for lid in ids:
            lvals = self.read(cursor, uid, lid, ['importacio_id',
                                                  'liniafactura_id'])
            # comprovar si la factura associada a la línia és oberta
            obert = False
            if lvals and lvals['liniafactura_id']:
                lfvals = lf.read(cursor, uid, lvals['liniafactura_id'],
                                 ['factura_id'])
                for val in lfvals:
                    fid = val['factura_id'][0]
                    f_estat = fact.read(cursor, uid, fid, ['state'])['state']
                    if f_estat != 'open':
                        u_fact.append(fid)
                        u_linfac.append(val['id'])
                        continue
                    obert = True
                if obert:
                    continue
            if (lvals['importacio_id']
                and lvals['importacio_id'][0] not in u_imp):
                u_imp.append(lvals['importacio_id'][0])
            u_linia.append(lid)
            self.unlink_lectures(cursor, uid, lid, context)
            self.unlink_facturacio_extra(cursor, uid, lid, context)
            self.unlink_adjunts(cursor,  uid, lid, context)
        fact.unlink(cursor, uid, u_fact, context=context)
        super(GiscedataFacturacioImportacioLinia, self).unlink(cursor, uid,
                                                     u_linia, context=context)
        # actualitzar info de les importacions
        for iid in u_imp:
            ivals = imp.read(cursor, uid, iid,
                                        ['num_xml', 'num_fact_creades'])
            i_info = ("%d XML importats, %d factures creades" %
                                 (ivals['num_xml'], ivals['num_fact_creades']))
            imp.write(cursor, uid, iid, {'info': i_info})
        return u_linia == ids

    def unlink_adjunts(self, cursor, uid, lid, context=None):
        """Eliminar els adjunts vinculats a la línia
        """
        if not context:
            context = {}
        att = self.pool.get('ir.attachment')
        att_ids = att.search(cursor, uid,
                [('res_id','=',lid),
                 ('res_model','=','giscedata.facturacio.importacio.linia')])
        if att_ids:
            att.unlink(cursor, uid, att_ids)

    def unlink_lectures(self, cursor, uid, lid, context=None):
        """Eliminar les lectures de comptador associades a la línia a través 
           de giscedata.facturacio.importacio.lectures_lectura/potencia
        """
        if not context:
            context = {}
        lin_lect = self.pool.get(
                    'giscedata.facturacio.importacio.linia.lecturaenergia')
        lin_pot = self.pool.get(
                    'giscedata.facturacio.importacio.linia.lecturapotencia')
        lect_lect = self.pool.get('giscedata.lectures.lectura.pool')
        lect_pot = self.pool.get('giscedata.lectures.potencia.pool')
        lin_lect_id = lin_lect.search(cursor, uid, [('linia_id', '=', lid)])
        lin_pot_id = lin_pot.search(cursor, uid, [('linia_id', '=', lid)])
        if lin_lect_id:
            lect_id_obj = lin_lect.read(cursor, uid, lin_lect_id, ['lectura_id'])
            lect_ids = [x['lectura_id'][0] for x in lect_id_obj if x['lectura_id']]
            if lect_ids:
                lect_lect.unlink(cursor, uid, lect_ids, context)
        if lin_pot_id:
            pot_id_obj = lin_pot.read(cursor, uid, lin_pot_id, ['lectura_id'])
            pot_ids = [x['lectura_id'][0] for x in pot_id_obj if x['lectura_id']]
            if pot_ids:
                lect_pot.unlink(cursor, uid, pot_ids, context)

    def unlink_factures(self, cursor, uid, lid, context=None):
        """Eliminar factures associades a la linia lid"""
        lin_fact = self.pool.get(
                    'giscedata.facturacio.importacio.linia.factura')
        fact = self.pool.get('giscedata.facturacio.factura')

        lin_fact_id = lin_fact.search(cursor, uid, [('linia_id', '=', lid)])
        fact_id = lin_fact.read(cursor, uid, lin_fact_id, ['factura_id'],
                                                                     context)
        ids = [x['factura_id'][0] for x in fact_id]
        if ids:
            fact.unlink(cursor, uid, ids, context)

    def unlink_extra(self, cursor, uid, lid, context=None):
        """Eliminiar linies Extra associades a la linia lid"""
        lin_extra = self.pool.get(
                    'giscedata.facturacio.importacio.linia.extra')
        extra = self.pool.get('giscedata.facturacio.extra')
        lin_extra_id = lin_extra.search(cursor, uid, [('linia_id', '=', lid)])
        extra_id = lin_extra.read(cursor, uid, lin_extra_id, ['extra_id'],
                                                                       context)
        ids = [x['extra_id'][0] for x in extra_id]
        if ids:
            extra.unlink(cursor, uid, ids, context)

    def unlink_facturacio_extra(self, cursor, uid, lid, context=None):
        """Eliminiar les línies de giscedata.facturacio.extra associades a
           la línia a través de giscedata.facturacio.importacio.linia.extra
        """
        if not context:
            context = {}
        lin_ext = self.pool.get(
                    'giscedata.facturacio.importacio.linia.extra')
        ext = self.pool.get('giscedata.facturacio.extra')
        lin_ext_id = lin_ext.search(cursor, uid, [('linia_id', '=', lid)])
        if lin_ext_id:
            lin_ext_obj = lin_ext.read(cursor, uid, lin_ext_id, ['extra_id'])
            ext_ids = [x['extra_id'][0] for x in lin_ext_obj]
            ext.unlink(cursor, uid, ext_ids, context)

    _states_selection = [
        ('erroni', 'Error en la importació'),
        ('valid', 'Importat correctament'),
        ('incident', 'Incidents en la importació'),
    ]

    def recreate_checks(self, cursor, uid, line_id, imported_invoices,
                        context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        for inv_id in imported_invoices:
            inv_vals = fact_obj.read(
                cursor, uid, inv_id, ['comment']
            )
            comments = inv_vals['comment'].split('\n')
            tipus = ''
            for comment in comments:
                if '*******' in comment:
                    tipus = comment.split()[1].lower()
                if 'Subt XML' in comment:
                    split_com = comment.split()

                    total_xml = float(split_com[2])
                    total_imp = float(split_com[7])
                    diff = float(split_com[10])
                    if abs(diff) > 0.01:
                        error_obj.create_error_from_code(
                            cursor, uid, line_id, '2', '006',
                            {
                                'tipus': tipus,
                                'total_xml': total_xml,
                                'total_imp': total_imp
                            },
                            context=context
                        )

    def check_invoice_exists(self, cursor, uid, line_id, fact, context=None):
        """
        Checks if exists an the same invoice (by origin) and partner (DSO)
        :param line_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the invoice
        :param context: OpenERP context
        :return: True if not found, False (exception) if found
        """
        inv_obj = self.pool.get('giscedata.facturacio.factura')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        partner_obj = self.pool.get('res.partner')
        liniafact_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )

        line_vals = self.read(
            cursor, uid, line_id, ['distribuidora_id', 'liniafactura_id'],
            context=context
        )
        distribuidora_id = line_vals['distribuidora_id'][0]
        inv_number = fact.datos_factura.codigo_fiscal_factura
        fact_data = datetime.strptime(
            fact.datos_factura.fecha_factura, '%Y-%m-%d'
        )
        inv_ids = inv_obj.get_in_invoice_by_origin(
            cursor, uid, inv_number, distribuidora_id, year=str(fact_data.year),
            context=context
        )
        if len(inv_ids):
            imported_invoices = []
            for liniafact_id in line_vals['liniafactura_id']:
                imported_invoices.append(
                    liniafact_obj.read(
                        cursor, uid, liniafact_id, ['factura_id']
                    )['factura_id'][0]
                )

            # All the inv ids should have already been imported with this line
            # If any of the invoices hasn't been imported in this line, we need
            # to raise an error
            raise_error = False
            for inv_id in inv_ids:
                if inv_id not in imported_invoices:
                    raise_error = True

            # We recreate the checks that we had already created before but have
            # been deleted on reimport
            self.recreate_checks(
                cursor, uid, line_id, imported_invoices, context=context
            )

            if raise_error:
                distri_vals = partner_obj.read(
                    cursor, uid, distribuidora_id, ['ref']
                )

                vals = {
                    'distribuidora': distri_vals['ref'],
                    'num_factura': inv_number
                }
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '2', '001', vals, context=context
                )
                raise LineProcessException('STOP Procces')

    def set_phase_manual(self, cursor, uid, line_ids, context=None):
        self.set_phase(cursor, uid, line_ids, 50, context=context)
        amb_fact = []
        sense_fact = []
        for linfo in self.read(cursor, uid, line_ids, ['liniafactura_id'], context):
            if len(linfo.get('liniafactura_id')) > 0:
                amb_fact.append(linfo['id'])
            else:
                sense_fact.append(linfo['id'])
        self.write(cursor, uid, amb_fact, {'state': 'valid'})
        self.write(cursor, uid, sense_fact, {'state': 'erroni'})

    def set_phase(self, cursor, uid, line_ids, phase, context=None):
        """
        Sets the import_phase of the line(s) to phase
        :param cursor: Database cursor
        :param uid: User's id
        :param context: OpenERP context
        :param line_id: Id(s) of the line(s) to update
        :param phase: Phase we want to set the line(s)
        """
        if context is None:
            context = {}

        if isinstance(line_ids, (int, long)):
            line_ids = [line_ids]

        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        self.write(cursor, uid, line_ids, {'import_phase': phase}, context)

        phase_error = str(phase * 100)

        # Clean errors when reimport
        error_ids = error_obj.search(
            cursor, uid, [
                ('name', '>=', phase_error),
                ('line_id', 'in', line_ids)
            ]
        )
        error_obj.unlink(cursor, uid, error_ids, context)

    @job(queue='import_xml')
    def process_line(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}
        logger = logging.getLogger(
            'openerp.{}.process_file'.format(__name__)
        )
        if isinstance(line_id, (int, long)):
            line_id = [line_id]

        for line in line_id:
            self.process_line_sync(cursor, uid, line, context=context)
            logger.info('Processed line id {0}'.format(line))

    def validate_phase_1(self, cursor, uid, line_id, context=None):
        """
        Validates F1 xml. if OK, returns F1 switching object to be used in next
        steps
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :return: F1 switching object
        """
        if context is None:
            context = {}
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        try:
            data = self.get_xml_from_adjunt(
                cursor, uid, line_id, context=context)
            f1_xml = F1(data, 'F1')
            f1_xml.parse_xml()
        except Exception as e:
            try:
                data = self.get_xml_from_adjunt(
                    cursor, uid, line_id, context=context
                )

                old_f1_xml = OldF1(data, 'F1')
                old_f1_xml.parse_xml()

                # If it parses correctly in the old format it means it's the
                # old format
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '009', {}, context=context
                )
                raise LineProcessException('STOP Procces')
            except LineProcessException, e:
                raise e
            except old_message.except_f1:
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '001', {'exception': e.value},
                    context=context
                )
                raise LineProcessException('STOP Procces')
            except Exception as e:
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '001', {'exception': str(e)},
                    context=context
                )
                raise LineProcessException('STOP Procces')
        return f1_xml

    def read_lectures_from_xml(self, cursor, uid, line_id, f1_xml, context=None):
        lecturas_f1_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.lectures'
        )
        if not f1_xml:
            return
        lect_remove_ids = lecturas_f1_obj.search(
            cursor, uid, [('linia_id', '=', line_id)], context=context
        )
        if lect_remove_ids:
            lecturas_f1_obj.unlink(
                cursor, uid, lect_remove_ids, context=context)
        factures_atr = f1_xml.facturas_atr
        if factures_atr:
            for factura_atr in factures_atr:
                medidas = factura_atr.medidas
                for medida in medidas:
                    for modelo in medida.modelos_aparatos:
                        num_serie = modelo.numero_serie
                        for integrador in modelo.integradores:
                            fecha_desde = integrador.lectura_desde.fecha
                            lectura_desde = integrador.lectura_desde.lectura
                            origen_desde = integrador.lectura_desde.procedencia
                            fecha_hasta = integrador.lectura_hasta.fecha
                            lectura_hasta = integrador.lectura_hasta.lectura
                            origen_hasta = integrador.lectura_hasta.procedencia
                            magnitud = integrador.magnitud
                            periodo = integrador.codigo_periodo
                            ajuste = False
                            cod_mot_description = ''
                            if integrador.ajuste:
                                ajuste = integrador.ajuste.ajuste_por_integrador or False
                                motivo = integrador.ajuste.codigo_motivo or ''
                                rao = dict(TABLA_106)[motivo] or ''
                                cod_mot_description = '{0}-{1}'.format(
                                    motivo, rao
                                )
                            vals = {
                                'magnitud': magnitud,
                                'lectura_desde': lectura_desde,
                                'lectura_actual': lectura_hasta,
                                'fecha_desde': fecha_desde,
                                'fecha_actual': fecha_hasta,
                                'origen_desde': origen_desde,
                                'origen_actual': origen_hasta,
                                'comptador': num_serie,
                                'periode': periodo,
                                'linia_id': line_id,
                                'ajust': ajuste,
                                'motiu': cod_mot_description
                            }
                            lecturas_f1_obj.create(
                                cursor, uid, vals, context=context
                            )

    def check_dates_not_equals(self, cursor, uid, line_id, context=None):
        """
        Check if start and end dates of invoice are not the same,if it is the
        case raise a codified error 10
        :return: True if validation pass
        """
        vals = self.read(
            cursor, uid, line_id,
            ['fecha_factura_desde', 'fecha_factura_hasta', 'tipo_factura_f1'],
            context=context
        )

        fecha_desde = vals['fecha_factura_desde']
        fecha_hasta = vals['fecha_factura_hasta']

        if vals['tipo_factura_f1'] == 'atr':
            if fecha_desde == fecha_hasta:
                error_obj = self.pool.get('giscedata.facturacio.switching.error')
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '010',
                    {'data_i': fecha_desde,
                     'data_f': fecha_hasta
                     }
                )
                raise LineProcessException('STOP Process')

        return True

    def process_phase_1(self, cursor, uid, line_id, context=None):
        """
        Phase 1 of F1 processing. XML loading, payment order creation and
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :return: F1 switching object
        """
        if context is None:
            context = {}

        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        self.set_phase(cursor, uid, line_id, 10, context)

        # Clean errors when reimport
        line_vals = self.read(cursor, uid, line_id, ['error_ids'])
        for error_id in line_vals['error_ids']:
            error_obj.unlink(cursor, uid, error_id, context)

        f1_xml = self.validate_phase_1(cursor, uid, line_id, context=context)
        self.read_lectures_from_xml(cursor, uid, line_id, f1_xml, context=context)
        self.update_header_info(cursor, uid, line_id, context=context)
        self.update_remesa_info(cursor, uid, line_id, context=context)
        self.check_processed(cursor, uid, line_id, context=context)
        self.check_cups_distri(cursor, uid, line_id, context=context)
        self.check_dates_not_equals(cursor, uid, line_id, context=context)
        return f1_xml

    def process_all_invoice_phase_2(self, cursor, uid, line_id, fact_xml,
                                    context=None):
        """
        Processes an individual invoice from F1 of either type
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The switching version of the invoice we are importing
        :return:
        """
        imp_lin_obj = self.pool.get('giscedata.facturacio.importacio.linia')

        imp_lin_obj.check_invoice_exists(
            cursor, uid, line_id, fact_xml
        )

    def handle_bureaucratic_invoice(self, cursor, uid, file_id, invoice_id,
                                    fact_xml, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        factura_vals = fact_obj.read(
            cursor, uid, invoice_id, ['partner_id', 'origin']
        )

        total_factura = fact_xml.datos_factura.importe_total_factura
        file_vals = fact_obj.read(cursor, uid, file_id, ['cups_text'])

        anul_carregada_id = fact_obj.search(cursor, uid, [
            ('amount_total', '=', abs(total_factura)),
            ('invoice_id.partner_id.id', '=', factura_vals['partner_id']),
            ('cups_id.name', '=', file_vals['cups_text']),
            ('type', '=', 'in_refund')
        ], limit=1, context={'active_test': False})

        if anul_carregada_id:
            origin_anul = fact_obj.read(
                cursor, uid, anul_carregada_id[0], ['origin']
            )['origin']

            error_obj.create_error_from_code(
                cursor, uid, file_id, '2', '010',
                {'origen': factura_vals['origin'], 'anuladora': origin_anul},
                context=context
            )
        else:
            imp_total = total_factura
            error_obj.create_error_from_code(
                cursor, uid, file_id, '2', '011',
                {'origen': factura_vals['origin'], 'import': imp_total},
                context=context
            )

        fact_obj.unlink(cursor, uid, [invoice_id], context)
        return []

    def get_extra_lines(self, cursor, uid, invoice_id, fact_xml, line_id,
                        context=None):
        if context is None:
            context = {}

        fact_lin_obj = self.pool.get('giscedata.facturacio.factura.linia')
        product_obj = self.pool.get('product.product')
        pricelist_obj = self.pool.get('product.pricelist')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        config_obj = self.pool.get('res.config')
        polissa_obj = self.pool.get('giscedata.polissa')

        invoice_with_client_prices = int(
            config_obj.get(
                cursor, uid, 'invoice_from_C_with_client_prices', '1'
            )
        )

        extra_lines = []

        pricelist_sql = fact_obj.q(cursor, uid).select(
            ['polissa_id', 'data_final', 'origin_date_invoice', 'polissa_id.llista_preu'],
            only_active=False
        ).where([('id', '=', invoice_id)])
        cursor.execute(*pricelist_sql)
        fact_data = cursor.dictfetchall()[0]
        polissa_id = fact_data['polissa_id']
        invoice_date = (
            fact_data['data_final'] or fact_data['origin_date_invoice']
        )
        try:
            pricelist_id = polissa_obj.read(
                cursor, uid, polissa_id, ['llista_preu'],
                context={'date': invoice_date}
            )['llista_preu'][0]
        except osv.except_osv as e:
            pricelist_id = fact_data['polissa_id.llista_preu']

        if fact_xml.datos_factura.tipo_factura == 'C':
            subtotal_linies = fact_lin_obj.search(
                cursor, uid, [
                    ('factura_id', '=', invoice_id),
                    ('tipus', 'like', 'subtotal_xml')
                ]
            )

            SUBTOTAL_TYPES = {
                'subtotal_xml': 'altres',
                'subtotal_xml_ene': 'energia',
                'subtotal_xml_pow': 'potencia',
                'subtotal_xml_rea': 'reactiva',
                'subtotal_xml_exc': 'exces_potencia',
                'subtotal_xml_ren': 'lloguer',
                'subtotal_xml_oth': 'altres',
            }

            # The ones with a zero subtotal should not be invoiced
            zero_total_types = [
                SUBTOTAL_TYPES.get(sub_vals['tipus'], '')
                for sub_vals in fact_lin_obj.read(
                    cursor, uid, subtotal_linies, ['tipus', 'price_subtotal']
                )
                if sub_vals['price_subtotal'] == 0
            ]

            search_params = [
                ('factura_id', '=', invoice_id),
                ('tipus', 'not in', zero_total_types),
                ('product_id', '!=', False),
                ('uos_id', '!=', False),
            ]

            # If the invoice is of type C then all the lines that have a value
            # should become extra lines
            for line_id in fact_lin_obj.search(cursor, uid, search_params):
                line_vals = fact_lin_obj.read(
                    cursor, uid, line_id, ['product_id', 'quantity', 'tipus'],
                    context
                )
                if line_vals['product_id'] and line_vals['product_id'][1] in CODIS_AUTOCONSUM:
                    continue
                extra_line = {
                    'line_id': line_id,
                    'force_name': _(
                        u'Facturació Complementaria imputada per part de la '
                        u'Distribuïdora - {}'
                    ).format(line_vals['tipus'].title())
                }

                if invoice_with_client_prices:
                    product_id = line_vals['product_id']
                    product_qty = line_vals['quantity']
                    invoice_date = fact_obj.read(cursor, uid, invoice_id, ['data_final'])['data_final']
                    price = pricelist_obj.price_get(
                        cursor, uid, [pricelist_id], product_id[0], product_qty,
                        context={'date': invoice_date}
                    )[pricelist_id]
                    extra_line.update(
                        {
                            'force_price': price
                        }
                    )

                extra_lines.append(extra_line)
        else:
            other_lines_ids = fact_lin_obj.search(
                cursor, uid, [
                    ('tipus', '=', 'altres'),
                    ('factura_id', '=', invoice_id)
                ]
            )

            moti_fact = fact_xml.datos_factura.motivo_facturacion
            sin_base_imponible = fact_xml.sin_base_imponible()
            saldo_cobr = fact_xml.datos_factura.saldo_factura
            conceptes = set()
            for linia_fact_id in other_lines_ids:
                linia_vals = fact_lin_obj.read(
                    cursor, uid, linia_fact_id,
                    ['tipus', 'product_id', 'quantity', 'price_subtotal']
                )
                if linia_vals['product_id']:
                    categ = product_obj.read(cursor, uid, linia_vals['product_id'][0], ['categ_id'])['categ_id']
                    if categ[1] == u'Extra / Autoconsum':
                        continue
                extra_line = {
                    'line_id': linia_fact_id,
                }

                # Crear línea extra con precio de la lista de precios para el cliente
                product_id = linia_vals['product_id'][0]
                product_qty = linia_vals['quantity']
                price = pricelist_obj.price_get(
                    cursor, uid, [pricelist_id], product_id, product_qty,
                    context={
                        'date': invoice_date,
                        'pricelist_base_price': linia_vals['price_subtotal']
                    }
                )[pricelist_id]
                extra_line.update(
                    {
                        'force_price': price
                    }
                )
                extra_lines.append(extra_line)

                if moti_fact == '10' and sin_base_imponible and saldo_cobr < 0:
                    product_vals = product_obj.read(
                        cursor, uid, linia_vals['product_id'][0], ['code']
                    )
                    if product_vals['code'] == 'CON14':
                        conceptes.add(product_vals['code'])

            if len(conceptes) == 1:
                # Això deu ser la factura burocràtica d'ENDESA per tant
                # l'eliminem
                return self.handle_bureaucratic_invoice(
                    cursor, uid, line_id, invoice_id, fact_xml, context=context
                )

        return extra_lines

    def import_extra_lines(self, cursor, uid, invoice_id, fact_xml, line_id,
                           context=None):
        if context is None:
            context = {}

        fact_help_obj = self.pool.get('giscedata.facturacio.switching.helper')
        fact_lin_obj = self.pool.get('giscedata.facturacio.factura.linia')
        imp_line_extra_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.extra'
        )
        product_obj = self.pool.get('product.product')

        other_lines_ids = fact_lin_obj.search(
            cursor, uid, [
                ('tipus', '=', 'altres'),
                ('factura_id', '=', invoice_id)
            ]
        )

        line_name = self.read(cursor, uid, line_id, ['name'])['name']
        ctx = fact_help_obj.actualitzar_idioma_ctx(cursor, uid, invoice_id,
                                                   'out_invoice', context)
        extra_lines = self.get_extra_lines(
            cursor, uid, invoice_id, fact_xml, line_id, ctx
        )

        imp_line_extra_ids = []
        ctx = context.copy()
        ctx.update({
            'line_id': line_id,
        })
        extra_ids = fact_help_obj.create_invoicing_extra(
            cursor, uid, invoice_id, line_name, extra_lines, ctx
        )
        for extra_id in extra_ids:
            imp_line_extra_ids.append(
                imp_line_extra_obj.create(
                    cursor, uid, {
                        'linia_id': line_id,
                        'extra_id': extra_id
                    }
                )
            )

        return imp_line_extra_ids

    def generate_bra_invoice(self, cursor, uid, fact_xml, orig_invoice_id,
                             line_id, payment_order_id=False, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        imp_line_fact_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )
        ra_invoice_id = fact_obj.search(cursor, uid, [
            ('ref', '=', orig_invoice_id),
            ('tipo_rectificadora', '=', 'RA')
        ])[0]
        ra_date_invoice = fact_obj.read(
            cursor, uid, ra_invoice_id, ['date_invoice'])['date_invoice']
        ctx['data_factura'] = ra_date_invoice
        # If it's a RA we need to create the BRA
        bra_id = fact_obj.anullar(
            cursor, uid, [orig_invoice_id], 'BRA', context=ctx
        )
        bra_id = bra_id[0]
        # Set check_total equal to amount_total when invoice will open
        # it will be equal when invoice's type is 'in'

        amount_total = fact_obj.read(
            cursor, uid, bra_id, ['amount_total']
        )['amount_total']
        bra_vals = {
            'check_total': amount_total,
            'payment_order_id': payment_order_id
        }
        fact_obj.write(cursor, uid, bra_id, bra_vals)

        imp_bra_line_id = imp_line_fact_obj.create(
            cursor, uid, {
                'linia_id': line_id,
                'factura_id': bra_id,
            }, context
        )

        imp_line_extra_ids = self.import_extra_lines(
            cursor, uid, bra_id, fact_xml, line_id, context=context
        )
        return bra_id, imp_line_extra_ids

    def process_atr_invoice_phase_2(self, cursor, uid, line_id, fact_xml,
                                    data_limit_pagament, distri_id,
                                    payment_order_id, context=None):
        """
        Processes an individual ATR invoice from F1
        :param payment_order_id:
        :param distri_id:
        :param data_limit_pagament:
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The switching version of invoice we are importing
        :return:
        """
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        fact_help_obj = self.pool.get('giscedata.facturacio.switching.helper')
        imp_line_fact_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )

        invoice_id = fact_help_obj.import_invoice_atr(
            cursor, uid, line_id, fact_xml, data_limit_pagament, distri_id,
            payment_order_id, context=context
        )

        if invoice_id:
            imp_line_id = imp_line_fact_obj.create(
                cursor, uid, {
                    'linia_id': line_id,
                    'factura_id': invoice_id,
                }, context
            )

            fact_help_obj.create_invoice_lines(
                cursor, uid, line_id, invoice_id, fact_xml, context=context
            )

            imp_line_extra_ids = self.import_extra_lines(
                cursor, uid, invoice_id, fact_xml, line_id, context=context
            )

            tipo_factura = fact_xml.datos_factura.tipo_factura
            invoice_ref = fact_obj.read(
                cursor, uid, invoice_id, ['ref']
            )['ref']
            if tipo_factura == 'R' and invoice_ref:
                # If it's a RA we need to create the BRA
                bra_id, extra_ids = self.generate_bra_invoice(
                    cursor, uid, fact_xml, invoice_ref[0], line_id,
                    payment_order_id=payment_order_id, context=context
                )
                imp_line_extra_ids += extra_ids
            return imp_line_id, imp_line_extra_ids
        return None, None

    def process_other_invoice_phase_2(self, cursor, uid, line_id, fact_xml,
                                      data_limit_pagament, distri_id,
                                      payment_order_id, context=None):
        """
        Processes an individual Concept invoice from F1
        :param payment_order_id:
        :param distri_id:
        :param data_limit_pagament:
        :param context: OpenERP context
        :param uid: User identifier
        :param cursor: Database cursor
        :param line_id: Id of the line we are importing
        :param fact_xml: The switching version of the invoice we are importing
        :return:
        """
        fact_help_obj = self.pool.get('giscedata.facturacio.switching.helper')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        imp_line_fact_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )

        invoice_id = fact_help_obj.import_invoice_others(
            cursor, uid, line_id, fact_xml, data_limit_pagament, distri_id,
            payment_order_id, context=context
        )

        if invoice_id:
            imp_line_id = imp_line_fact_obj.create(
                cursor, uid, {
                    'linia_id': line_id,
                    'factura_id': invoice_id,
                }, context
            )

            fact_help_obj.create_invoice_lines(
                cursor, uid, line_id, invoice_id, fact_xml, context=context
            )

            imp_line_extra_ids = self.import_extra_lines(
                cursor, uid, invoice_id, fact_xml, line_id, context=context
            )

            tipo_factura = fact_xml.datos_factura.tipo_factura
            invoice_ref = fact_obj.read(
                cursor, uid, invoice_id, ['ref']
            )['ref']
            if tipo_factura == 'R' and invoice_ref:
                # If it's a RA we need to create the BRA
                # If it's a RA we need to create the BRA
                bra_id, extra_ids = self.generate_bra_invoice(
                    cursor, uid, fact_xml, invoice_ref[0], line_id,
                    payment_order_id=payment_order_id, context=context
                )
                imp_line_extra_ids += extra_ids
            return imp_line_id, imp_line_extra_ids
        return None, None

    def process_phase_2(self, cursor, uid, line_id, f1_xml, context=None):
        """
        Phase 2 of F1 processing. DSO invoice creation
        :param context:
        :param uid:
        :param cursor:
        :param line_id:
        :param f1_xml: F1 switching object
        :return:
        """
        fact_help_obj = self.pool.get('giscedata.facturacio.switching.helper')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        partner_obj = self.pool.get('res.partner')
        imp_line_fact_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )
        imp_extra_line_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.extra'
        )
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        extra_obj = self.pool.get('giscedata.facturacio.extra')

        self.set_phase(cursor, uid, line_id, 20, context)

        atr_invoices = f1_xml.facturas_atr
        other_invoices = f1_xml.otras_facturas
        all_invoices = atr_invoices + other_invoices

        line_vals = self.read(
            cursor, uid, line_id,
            ['distribuidora_id', 'liniafactura_id', 'liniaextra_id', 'cups_id'],
            context=context
        )
        cups_id = line_vals['cups_id'][0]
        distri_id = line_vals['distribuidora_id'][0]
        emissor = partner_obj.read(cursor, uid, distri_id, ['ref'])['ref']

        remesa_data = f1_xml.registro.get_remesa()

        # First of all, we unlink all the invoices we have imported which are
        # still in draft
        imp_line_fact_ids = imp_line_fact_obj.search(
            cursor, uid, [
                ('state', '=', 'draft'),
                ('id', 'in', line_vals['liniafactura_id'])
            ]
        )
        factura_ids = [
            fact['factura_id'][0] for fact in imp_line_fact_obj.read(
                cursor, uid, imp_line_fact_ids, ['factura_id']
            )
        ]
        fact_obj.unlink(cursor, uid, factura_ids)

        non_deletable_facts = imp_line_fact_obj.search(
            cursor, uid, [
                ('id', 'in', line_vals['liniafactura_id'])
            ]
        )
        if not non_deletable_facts:
            # If we don't delete the invioces we won't create the new invoice
            # lines so we shouldn't be deleting them
            deletable_imp_line_ids = imp_extra_line_obj.search(
                cursor, uid, [
                    ('id', 'in', line_vals['liniaextra_id']),
                    ('amount_invoiced', '=', 0),
                    ('total_amount_invoiced', '=', 0),
                ]
            )
            extra_line_ids = [
                line['extra_id'][0] for line in imp_extra_line_obj.read(
                    cursor, uid, deletable_imp_line_ids, ['extra_id']
                )
            ]
            extra_obj.unlink(cursor, uid, extra_line_ids)

            non_deletable_lines = imp_extra_line_obj.search(
                cursor, uid, [('id', 'in', line_vals['liniaextra_id'])]
            )
            if non_deletable_lines:
                error_obj.create_error_from_code(
                    cursor, uid, line_id, '2', '014',
                    {
                        'line_ids': ', '.join(map(str, non_deletable_lines)),
                    },
                    context=context
                )
                raise LineProcessException('STOP Process')

        invoice_numbers = []
        imported_invoices = []
        extra_lines = []

        try:
            payment_order_id = fact_help_obj.get_payment_order(
                cursor, uid, emissor, remesa_data, context=context
            )

            # Common part for ATR and others
            for invoice in all_invoices:
                origin_number = invoice.datos_factura.codigo_fiscal_factura
                if origin_number not in invoice_numbers:
                    invoice_numbers.append(origin_number)
                else:
                    error_obj.create_error_from_code(
                        cursor, uid, line_id, '2', '013',
                        {
                            'inv_number': origin_number,
                        },
                        context=context
                    )
                    raise LineProcessException('STOP Process')

                self.process_all_invoice_phase_2(
                    cursor, uid, line_id, invoice, context=context
                )

            # Only for ATR
            for invoice in atr_invoices:
                # actualitzem ref_distri de pólissa
                cups_obj = self.pool.get('giscedata.cups.ps')
                polissa_id = cups_obj.read(cursor, uid, cups_id, ['polissa_polissa'])['polissa_polissa']
                ref_distri = invoice.factura.DatosGeneralesFacturaATR.CodContrato.text
                if ref_distri and polissa_id:
                    pol_obj = self.pool.get('giscedata.polissa')
                    pol_ref_dist = pol_obj.read(cursor, uid, polissa_id[0], ['ref_dist'])['ref_dist']
                    if pol_ref_dist != ref_distri:
                        pol_obj.write(cursor, uid, polissa_id[0], {'ref_dist': ref_distri})

                imp_inv, extra_lines_it = self.process_atr_invoice_phase_2(
                    cursor, uid, line_id, invoice,
                    f1_xml.registro.fecha_limite_pago, distri_id,
                    payment_order_id, context=context
                )
                if imp_inv:
                    imported_invoices.append(imp_inv)
                    extra_lines += extra_lines_it

            # Only for Others
            for invoice in other_invoices:
                imp_inv, extra_lines_it = self.process_other_invoice_phase_2(
                    cursor, uid, line_id, invoice,
                    f1_xml.registro.fecha_limite_pago, distri_id,
                    payment_order_id, context=context
                )
                if imp_inv:
                    imported_invoices.append(imp_inv)
                    extra_lines += extra_lines_it

        except LineProcessException:
            raise  # If we get a LineProcessException we raise it directly
        except message.except_f1 as e:
            phase = '2'

            error_dic = {
                'UnidentifiablePeriode': {'code': '003', 'vals': e.values_dict},
                'JournalNotFound': {'code': '004', 'vals': {'diari': e.value}},
                'ContractNotFound': {'code': '005', 'vals': e.values_dict},
                'ReferenceInvoiceNotFound': {
                    'code': '007', 'vals': {'fact_ref': e.value}
                },
                'SwitchingConfigNotFound': {
                    'code': '008', 'vals': e.values_dict
                },
            }

            error_params = error_dic.get(e.name, {})
            error_code = error_params.get('code', '999')
            error_vals = error_params.get('vals', {'text': e.value})

            error_obj.create_error_from_code(
                cursor, uid, line_id, phase, error_code, error_vals,
                context=context
            )
            raise LineProcessException('STOP Procces')
        except osv.except_osv as e:
            phase = '2'
            error_code = '999'
            error_vals = {'text': e.value}
            if u"No s'ha trobat la configuració" in e.value:
                error_code = '008'
                error_vals = {'emissor': emissor}
            elif u"No s'ha configurat el mode de pagament" in e.value:
                pass
            error_obj.create_error_from_code(
                cursor, uid, line_id, phase, error_code, error_vals,
                context=context
            )
            raise LineProcessException('STOP Procces')
        except Exception as e:
            error_obj.create_error_from_code(
                cursor, uid, line_id, '2', '999', {'text': e},
                context=context
            )
            raise

    def process_phase_3(self, cursor, uid, line_id, f1_xml, context=None):
        if context is None:
            context = {}

        validator_obj = self.pool.get(
            'giscedata.facturacio.switching.validator'
        )

        self.set_phase(cursor, uid, line_id, 30, context)

        error_ids, has_critical = validator_obj.validate_f1(
            cursor, uid, line_id, f1_xml, phase=3, context=context
        )

        if has_critical:
            raise LineProcessException('STOP Procces')

        return error_ids

    def process_phase_4(self, cursor, uid, line_id, f1_xml, context=None):
        if context is None:
            context = {}

        fact_help_obj = self.pool.get('giscedata.facturacio.switching.helper')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        self.set_phase(cursor, uid, line_id, 40, context)

        try:
            fact_help_obj.update_meters(cursor, uid, line_id, f1_xml, context)

            ene_reads, pow_reads = fact_help_obj.import_readings(
                cursor, uid, line_id, f1_xml, context
            )
        except Exception as e:
            error_obj.create_error_from_code(
                cursor, uid, line_id, '4', '999', {'text': e},
                context=context
            )
            raise LineProcessException('STOP Procces')

        return ene_reads, pow_reads

    def process_line_sync(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}

        state = 'erroni'
        res = True
        actual_phase = self.read(
            cursor, uid, line_id, ['import_phase'])['import_phase']
        if not actual_phase:
            actual_phase = 10
        try:
            if actual_phase > 10:
                data = self.get_xml_from_adjunt(
                    cursor, uid, line_id, context=context)
                f1_xml = F1(data, 'F1')
                f1_xml.parse_xml()
            else:
                f1_xml = self.process_phase_1(
                    cursor, uid, line_id, context=context)
            if actual_phase <= 20:
                self.process_phase_2(
                    cursor, uid, line_id, f1_xml, context=context)
            if actual_phase <= 30:
                self.process_phase_3(
                    cursor, uid, line_id, f1_xml, context=context)
            if actual_phase <= 40:
                self.process_phase_4(
                    cursor, uid, line_id, f1_xml, context=context)
        except LineProcessException as e:
            state = 'erroni'
            res = False
        except Exception as e:
            state = 'erroni'
            res = False
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
        finally:
            has_critical = self.update_info(
                cursor, uid, line_id, context=context
            )

            # This shouldn't be necessary but for some reason some F1 end with
            # valid state while having errors
            if not has_critical:
                state = 'valid'

            self.write(cursor, uid, [line_id], {'state': state})
            # self.process_result(cursor, uid, line_id, res=state)

        return res

    def clean_linia_and_commit(self, cursor, uid, lid, context=None):
        """Eliminar lectures, factures, línies extra i comitar"""
        self.unlink_lectures(cursor, uid, lid)
        self.unlink_factures(cursor, uid, lid)
        self.unlink_extra(cursor, uid, lid)
        return True

    def importar_xml_f1(self, cursor, uid, line_id, context=None):
        """Reimportar un fitxer xml
           Copy from wizard
        """
        if context is None:
            context = {}

        att = self.pool.get('ir.attachment')
        f1 = self.pool.get('giscedata.facturacio.factura')
        importacio = self.pool.get('giscedata.facturacio.importacio')
        linia = self.pool.get('giscedata.facturacio.importacio.linia')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        cups_obj = self.pool.get('giscedata.cups.ps')
        partner = self.pool.get('res.partner')

        linval = linia.read(
            cursor, uid, line_id,
            ['importacio_id', 'attachment_id', 'distribuidora', 'cups_id'],
            context=context
        )
        importacio_id = linval['importacio_id'][0]
        context.update({'importacio_id': importacio_id})
        emisor_lot = linval['distribuidora']
        if not linval['attachment_id']:
            raise osv.except_osv(
                'Error',
                _("No s'ha trobat el fitxer adjunt")
            )
        att_ids = linval['attachment_id'][0]
        xml_obj = att.read(cursor, uid, att_ids, ['name'])
        fname = xml_obj['name']
        data = self.get_xml_from_adjunt(cursor, uid, line_id, context=context)

        try:
            f1_xml = F1(data, 'F1')
            f1_xml.parse_xml()
        except message.except_f1, e:
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '001', {'exception': e.value},
                context=context
            )
            raise LineProcessException('STOP Procces')

        # eliminem factures i lectures associades a la línia d'importació
        self.clean_linia_and_commit(cursor, uid, line_id, context)
        try:
            fact, emisor = f1.from_xml(cursor, uid, data, fname,
                                                 emisor_lot, context)
        except osv.except_osv, e:
            return e.value
        # vincular factura a la línia i lot
        if type(fact) != dict:
            linia.write(cursor, uid, line_id, {'state': 'erroni', 'info': fact})
            return fact
        # Parser final de les factures carregades
        res, l_info = self.parse_factures(cursor, uid, line_id, fact,
                                                    fname, context)
        self.process_result(
            cursor, uid, line_id, importacio_id,
            res=res, l_info=l_info, fact=fact, context=context
        )
        return 'Importat el fitxer %s' % fname


    @job(queue='process_result')
    def process_result(self, cursor, uid, line_id, importacio_id = None,
                       res=None, l_info=None, fact=None, context=None):
        if context is None:
            context = {}
        importacio = self.pool.get('giscedata.facturacio.importacio')
        line_vals = self.read(
            cursor, uid, line_id, ['importacio_id', 'info']
        )
        if not importacio_id:
            importacio_id = line_vals['importacio_id'][0]
        if fact:
            ref = fact[fact.keys()[0]][2]
            l_vals = {'state': res, 'info': l_info,
                      'codi_sollicitud': ref[:12], 'seq_sollicitud': ref[-2:]}
            self.write(cursor, uid, line_id, l_vals)
        # Actualitzar el lot d'importació
        importacio.update_state(cursor, uid, importacio_id, context=context)
        return True


    def parse_factures(self, cursor, uid, lid, factures, fitxer, context=None):
        """Parsejar el diccionari factures amb el format:
           - Factura errònia
               {<origen factura>: [False, <msg d'error>, <ref>, <b64>]}
           - Factura vàlida
               {<origen factura>:
                [True, <msg>, <ref>, [<id1>, <id2>], <lect_id>, <e_id>, <b64>]}
               L'estructura conté <id2> i <lect_id> únicament en cas
               d'existir divergència en els totals
               <lect_id> és una llista de tuples segons (<tipus>, <l_id>)
               <ref> = CodigoDeSolicitud | SecuencialDeSolicitud
               <e_id> llista d'ids del model giscedata.facturacio.extra
               <b64> xml en base64
        """
        if not context:
            context = {}
        lin_fact = self.pool.get(
                       'giscedata.facturacio.importacio.linia.factura')
        lin_lect = self.pool.get(
                    'giscedata.facturacio.importacio.linia.lecturaenergia')
        lin_pot = self.pool.get(
                    'giscedata.facturacio.importacio.linia.lecturapotencia')
        lin_ext = self.pool.get(
                    'giscedata.facturacio.importacio.linia.extra')
        info = []
        res = 'valid'
        for factura in factures:
            if factures[factura][0]:
                # En el cas de les rectificadores guardem la divergència per
                # saber la diferència entra la factura que rectifica però no
                # ho volem utiltizar. Per això comprovem si hi ha una segona
                # factura comparadora
                if len(factures[factura][3]) == 2 and factures[factura][3][1]:
                    if res == 'valid':
                        res = 'incident'
                    fid = factures[factura][3][1]
                    # únicament en el cas de ser divergents ens guardem els
                    # ids de les lectures de comptador per tal de disposar-les
                    # en cas de ser aprovades i mantenir l'històric.
                    # en cas de no aprovar-se la divergència s'esborraran.
                    for i in factures[factura][4]:
                        if i[0] in ['A', 'R']:
                            lin_lect.create(cursor, uid, {'linia_id': lid,
                                      'factura_id': fid, 'lectura_id': i[1]})
                        else:
                            lin_pot.create(cursor, uid, {'linia_id': lid,
                                      'factura_id': fid, 'lectura_id': i[1]})

                # es vincula les línies Extra per poder borrar-les si cal
                for i in factures[factura][5]:
                    lin_ext.create(cursor, uid, {'linia_id': lid,
                                                        'extra_id': i})

                for fid in factures[factura][3]:
                    if fid > 0:
                        lin_fact.create(cursor, uid, {
                            'linia_id': lid, 'factura_id': fid
                        })
            else:
                res = 'erroni'
            info.append(u'Factura %s: %s' % (factura,
                                            factures[factura][1]))
        info = u'\n'.join(info)
        return res, info

    def adjuntar_xml(self, cursor, uid, line_id, xml_data, fname, context=None):
        """Desa l'xml en l'erp"""
        attach_obj = self.pool.get('ir.attachment')
        if not context:
            context = {}

        desc = _(u"Fitxer adjunt: {0}".format(fname))
        md5_hash = get_md5(xml_data)
        vals = {
            'name': fname,
            'datas': base64.b64encode(xml_data),
            'datas_fname': fname,
            'res_model': 'giscedata.facturacio.importacio.linia',
            'res_id': line_id,
            'description': desc,
        }
        attch_id = attach_obj.create(cursor, uid, vals, context=context)
        vals = {'md5_hash': md5_hash, 'attachment_id': attch_id}
        self.write(cursor, uid, [line_id], vals)
        return attch_id

    def get_xml_from_adjunt(self, cursor, uid, line_id, context=None):
        """
        Reads XML from current attachment
        :param line_id: Id of line
        :return: xml_data (as string) of attachment
        """
        att_obj = self.pool.get('ir.attachment')

        line_vals = self.read(
            cursor, uid, line_id, ['attachment_id'], context=context
        )

        attachment_id = line_vals['attachment_id'][0]

        if not attachment_id:
            return ''

        b64_data = att_obj.read(
            cursor, uid, attachment_id, ['datas'], context=context
        )['datas']

        if not b64_data:
            return ''
        return base64.b64decode(b64_data)

    def change_attachment_id(self, cursor, uid, line_id, attachment_id):
        self.write(cursor, uid, [line_id], {'attachment_id': attachment_id})
        xml_data = self.get_xml_from_adjunt(cursor, uid, line_id)
        md5_hash = get_md5(xml_data)
        vals = {'md5_hash': md5_hash}
        self.write(cursor, uid, [line_id], vals)
        return True

    def get_header_dict(self, xml_data):
        """
        Returns dict to update line f1 header fields
        :param xml_data:
        :return:dict with values to update
        `giscedata.facturacio.importacio.line` model:
        {
            'cups_text': CUPS,
            'ree_source_code': SOURCE,
            'ree_destination_code': DESTINATION,
            'codi_sollicitud': SOLLICITUD code,
            'seq_sollicitud': SOLLICITUD sequence,
            'f1_date': DATE,
        }
        """
        header_f1 = get_header_from_xml(xml_data)

        invoice_number_text = re.findall(
            '<CodigoFiscalFactura>(.*)</CodigoFiscalFactura>', xml_data
        )

        vals = {
            'cups_text': header_f1['cups'],
            'ree_source_code': header_f1['codigo_ree_empresa_emisora'],
            'ree_destination_code': header_f1['codigo_ree_empresa_destino'],
            'codi_sollicitud': header_f1['codigo_solicitud'],
            'seq_sollicitud': header_f1['secuencia_solicitud'],
            'f1_date': header_f1['fecha_solicitud'],
            'invoice_number_text': ', '.join(invoice_number_text),
            'fecha_factura': header_f1['fecha_factura'],
            'tipo_factura_f1': header_f1['tipo_factura_f1'],
            'type_factura': header_f1['type_factura'],
            'fecha_factura_desde': header_f1['fecha_factura_desde'],
            'fecha_factura_hasta': header_f1['fecha_factura_hasta']
         }

        return vals

    def update_header_info(self, cursor, uid, line_id, context=None):
        """
        Updates header info from current attachment
        :param line_id:
        :return: result of update
        """
        imp_conf_obj = self.pool.get('giscedata.facturacio.switching.config')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        cups_obj = self.pool.get('giscedata.cups.ps')

        xml_data = self.get_xml_from_adjunt(
            cursor, uid, line_id, context=context
        )

        vals = self.get_header_dict(xml_data)

        # Emissor
        source_code = vals['ree_source_code']

        # Destinatari
        destination_code = vals['ree_destination_code']

        try:
            emissor_id = imp_conf_obj.get_emissor(cursor, uid, source_code)
            imp_conf_obj.check_destination_code(cursor, uid, destination_code)
            file_ids = imp_conf_obj.get_already_imported(cursor, uid, line_id)
            imp_conf_obj.check_already_imported(cursor, uid, file_ids)
            vals.update({'distribuidora_id': emissor_id})
        except message.except_f1 as e:
            phase = '1'
            error_code = '999'
            error_vals = {'text': e.value}
            if e.name == 'PartnerNotFound':
                error_code = '002'
                error_vals = {'ree_origin_code': source_code}
            elif e.name == 'MultiplePartnerForm':
                error_code = '003'
                error_vals = {'ree_origin_code': source_code}
            elif e.name == 'NotOurPartner':
                error_code = '004'
                error_vals = {'ree_destination_code': destination_code}
            elif e.name == 'F1AlreadyImportedNotInPhase1':
                error_code = '006'
                error_vals = {'fitxer_ids': file_ids['lines_006']}
            elif e.name == 'F1AlreadyImportedInPhase1':
                error_code = '066'
                error_vals = {'fitxer_ids': file_ids['lines_066']}
            else:
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()
            error_obj.create_error_from_code(
                cursor, uid, line_id, phase, error_code, error_vals
            )
            raise LineProcessException('STOP Procces')

        # CUPS
        ctx = context.copy()
        ctx.update({'active_test': False})
        cups = vals['cups_text'].strip().upper()
        cups_ids = cups_obj.search(
            cursor, uid, [('name', '=like', cups + '%')], context=ctx
        )

        if not cups_ids:
            # Si no hem trobat cap CUPS intentem buscar sense la terminacio
            if len(cups) > 20:
                cups_ids = cups_obj.search(
                    cursor, uid, [('name', '=like', cups[:20] + '%')],
                    context=ctx
                )

        if len(cups_ids) == 1:
            # Si hem trobat un sol CUPS, es aquest
            vals.update({'cups_id': cups_ids[0]})
        elif len(cups_ids) > 1:
            # Si hem trobat més d'un CUPS, no sabem quin triar
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '008', {'cups': cups}
            )
            raise LineProcessException('STOP Procces')
        else:
            # Si no hem trobat cap CUPS no podem continuar
            error_obj.create_error_from_code(
                cursor, uid, line_id, '1', '005', {'cups': cups}
            )
            raise LineProcessException('STOP Procces')

        return self.write(cursor, uid, [line_id], vals, context=context)

    @staticmethod
    def get_remesa_info_for_f1(xml_data, additional_fields=None):
        """
        Generates a dict containing the F1 fields related with payment orders.
        :param xml_data: XML data.
        :param additional_fields: Fields that may be added to the original ones.
        :return: a dict containing the value of the fields retrieved.
        """
        if additional_fields is None:
            additional_fields = []
        fields_to_return = [
            'IdRemesa', 'ImporteTotal', 'TotalRecibos'
        ] + additional_fields
        res = {}
        for field in fields_to_return:
            xml_field = re.findall('<{0}>(.*)</{0}>'.format(field), xml_data)
            if xml_field:
                res[field] = xml_field[0]
        return res

    def update_remesa_info(self, cursor, uid, line_id, context=None):
        """
        Update the fields related to payment_orders that exist in the F1 model
        with the information that comes in the F1 XML.
        The fields are: remesa_id, total_rebuts, import_remesa.
        :return: None
        """

        xml_data = self.get_xml_from_adjunt(
            cursor, uid, line_id, context=context
        )

        remesa_info = self.get_remesa_info_for_f1(xml_data)

        self.write(cursor, uid, line_id, {
            'remesa_id': remesa_info.get('IdRemesa', False),
            'total_rebuts': int(float(remesa_info.get('TotalRecibos', 0.0))),
            'import_remesa': float(remesa_info.get('ImporteTotal', False))
        })

    def update_info(self, cursor, uid, line_id, context=None):
        """
        Updates the field info of line_id based on the errors it has
        :param cursor: Database cursor
        :param uid: User's id
        :param line_id: Id of the line we are importing
        :param context: OpenERP's context
        :return Returns true if the line has critical info
        """
        info = self.get_errors_text(
            cursor, uid, line_id, context=context
        )
        critical_info = self.get_errors_text(
            cursor, uid, line_id, only_critical=True, context=context
        )
        self.write(
            cursor, uid, line_id, {
                'info': info,
                'critical_info': critical_info,
            }, context=context
        )

        return bool(critical_info)

    def check_processed(self, cursor, uid, line_id, context=None):
        if context is None:
            context = {}
        md5_hash = self.read(cursor, uid, line_id, ['md5_hash'])['md5_hash']
        search_params = [
            ('md5_hash', '=', md5_hash),
            ('id', '!=', line_id)
        ]
        lines_search = self.search(cursor, uid, search_params)
        if lines_search:
            error_obj = self.pool.get('giscedata.facturacio.switching.error')
            error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '006',
                    {'fitxer_ids': lines_search}
            )
            raise LineProcessException('STOP Process')
        return True

    def check_cups_distri(self, cursor, uid, line_id, context=None):
        """
        Checks if the given CUPS belongs to the given distri
        :param cursor:
        :param uid:
        :param line_id:
        :param context:
        :return: True when CUPS belongs to the given distri, raises
        LineProcessException if it doesn't
        """
        cups_obj = self.pool.get('giscedata.cups.ps')
        imp_conf_obj = self.pool.get('giscedata.facturacio.switching.config')
        partner_obj = self.pool.get('res.partner')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        vals = self.read(cursor, uid, line_id, ['cups_id', 'ree_source_code'])

        if vals['cups_id']:
            cups_id = vals['cups_id'][0]
            # CUPS DSO Validation
            cups_data = cups_obj.read(
                cursor, uid, cups_id, ['name', 'distribuidora_id'],
                context=context
            )
            emissor_id = imp_conf_obj.get_emissor(
                cursor, uid, vals['ree_source_code']
            )
            if cups_data['distribuidora_id'][0] != emissor_id:
                emissor_data = partner_obj.read(
                    cursor, uid, emissor_id, ['name']
                )
                cups_dso = cups_data['distribuidora_id'][1]
                emissor_dso = emissor_data['name']

                error_obj.create_error_from_code(
                    cursor, uid, line_id, '1', '007',
                    {
                        'cups': cups_data['name'],
                        'distri_cups': cups_dso,
                        'distri_f1': emissor_dso
                    }
                )
                raise LineProcessException('STOP Procces')
        return True

    def get_errors_text(self, cursor, uid, line_id, only_critical=False,
                        context=None):
        """
        Gets full errors text from line
        :param line_id:
        :return: text with error as
            [code]: message\n
            [code]: message\n
            [code]: ...\n
        """
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        critical_line_ids = error_obj.search(
            cursor, uid, [
                ('line_id', '=', line_id),
                ('level', '=', 'critical')
            ], context=context
        )

        texts = [
            error_obj.get_error_text(cursor, uid, l_id, context=context)
            for l_id in critical_line_ids
        ]

        if not only_critical:
            line_ids = error_obj.search(
                cursor, uid, [
                    ('line_id', '=', line_id),
                    ('level', '!=', 'critical')
                ], context=context
            )

            texts += [
                error_obj.get_error_text(cursor, uid, l_id, context=context)
                for l_id in line_ids
            ]

        text = '\n'.join(texts)
        return text

    def create_from_xml(self, cursor, uid, import_id, name, xml_data,
                        context=None):
        if context is None:
            context = {}
        vals = {
            'importacio_id': import_id,
            'name': name
        }
        
        header_dict = self.get_header_dict(xml_data)
        vals.update(header_dict)

        line_id = self.create(cursor, uid, vals)
        self.adjuntar_xml(cursor, uid, line_id, xml_data, name, context=context)
        return line_id

    def _ff_check_factures(self, cursor, uid, ids, field_name, arg,
                           context=None):
        res = {}
        for l_id in ids:
            lf = self.read(cursor, uid, l_id, ['liniafactura_id'], context)
            existeix = False
            if len(lf.get('liniafactura_id')) > 0:
                existeix = True
            res[l_id] = existeix
        return res

    def _ff_data_carrega(self, cursor, uid, ids, field_name, arg,
                         context=None):
        "Data de càrrega del fitxer"
        if not context:
            context = {}
        res = {}
        dates = self.perm_read(cursor, uid, ids)
        res = dict([(d['id'], d['create_date'].split('.')[0]) for d in dates])

        return res

    def _data_carrega_search(self, cursor, uid, ids, field, arg, context=None):
        """Cerca segons data creació"""
        if not context:
            context = {}
        search_vals = [('create_date', a[1], a[2]) for a in arg]
        trobats = self.search(cursor, uid, search_vals)
        return [('id', 'in', trobats)]

    def _ff_distribuidora(self, cursor, uid, ids, field_name, arg,
                          context=None):
        """Rertorna la distribuidora del fitxer"""
        if context is None:
            context = {}
        l_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        partner_obj = self.pool.get('res.partner')
        res = dict([(l, False) for l in ids])
        linies_vals = l_obj.read(
            cursor, uid, ids, ['distribuidora_id'], context=context
        )
        for linia_vals in linies_vals:
            l_id = linia_vals['id']
            if linia_vals['distribuidora_id']:
                res[l_id] = partner_obj.read(
                    cursor, uid, linia_vals['distribuidora_id'][0], ['ref']
                )['ref']
        return res

    def _distribuidora_search(self, cursor, uid, ids, field, arg, context=None):
        """Cerca per distribuidora"""
        if not context:
            return context
        search_vals = []
        for a in arg:
            search_vals.append(('distribuidora_id.ref', a[1], a[2]))
        i_ids = self.search(cursor, uid, search_vals)
        return [('id', 'in', i_ids)]

    def _buscar_f1_per_referencia(self, cursor, uid, obj, name, args,
                                  context=None):
        if context is None:
            context = {}

        if not args:
            return [('id', '=', 0)]

        value = args[0][2].split('-')

        if len(value) > 1:
            value = value[1]
        else:
            value = value[0]

        q = self.q(cursor, uid)
        f1s_ids = q.read(['id']).where([('remesa_id', 'like', value)])

        if f1s_ids:
            f1s_ids = [f1['id'] for f1 in f1s_ids]
            return [('id', 'in', f1s_ids)]
        else:
            return [('id', '=', 0)]

    def _referencies(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    _columns = {
        'name': fields.char('Fitxer', size=128),
        'state': fields.selection(_states_selection, 'Estat', size=32),
        'importacio_id': fields.many2one('giscedata.facturacio.importacio',
                                         'Importacio', required=True,
                                         select=True),
        'liniafactura_id': fields.one2many(
                              'giscedata.facturacio.importacio.linia.factura',
                              'linia_id', 'Factures', ondelete='cascade'),
        'liniaextra_id': fields.one2many(
            'giscedata.facturacio.importacio.linia.extra',
            'linia_id', 'Línies extres'
        ),
        'critical_info': fields.text('Info Critica', readonly=True, size=4000),
        'info': fields.text('Info', readonly=True, size=4000),
        'conte_factures': fields.function(_ff_check_factures, method=True,
                                       string="Conté factures", type='boolean'),

        # F1 header fields
        'codi_sollicitud': fields.char(
            'Codi sol·licitud', size=12, readonly=True
        ),
        'seq_sollicitud': fields.char(
            'Seqüencial de sol·licitud', size=2, readonly=True
        ),
        'cups_text': fields.char('F1 CUPS', size=22, readonly=True),
        'invoice_number_text': fields.char(
            'Numero Factura Origen', size=30, readonly=True
        ),
        'ree_source_code': fields.char(
            'Origen segons F1', size=4, readonly=True
        ),
        'ree_destination_code': fields.char(
            'Destí segons F1', size=4, readonly=True
        ),
        'f1_date': fields.char(
            'Data creacio F1', size=128, readonly=True
        ),

        'data_carrega': fields.function(_ff_data_carrega, type='datetime',
                                        string="Data Carrega", method=True,
                                        fnct_search=_data_carrega_search),
        'distribuidora': fields.function(_ff_distribuidora, type='char',
                                         string="Codi Distribuïdora",
                                         fnct_search=_distribuidora_search,
                                         method=True, size=4),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS afectat',
                                   required=False, size=22),
        'import_phase': fields.selection(
            _IMPORT_PHASES, string="Fase de càrrega", required=True
        ),
        'distribuidora_id': fields.many2one(
            'res.partner', 'Distribuïdora', readonly=True, required=False,
            select=True
        ),
        'md5_hash': fields.char('Hash', size=32, readonly=True),
        'attachment_id': fields.many2one(
            'ir.attachment', 'Adjunt utilitzat', readonly=True, select=True,
            ondelete='cascade'
        ),
        'ignore_reference': fields.boolean(
            'Forçar creacio sense necessitar factura de referencia'
        ),
        'lectures_processades': fields.boolean(
            "Lectures Processades",
            help="Indica si les lectures del F1 s'han carregat ja al contracte",
            select=2
        ),
        'tipo_factura_f1': fields.selection(
            [('atr', 'ATR'), ('otros', 'OTROS')], string="Tipo factura F1",
            help=u'Indica si es tracte d\'una '
                 u'factura ATR o d\'altres conceptes', readonly=True
        ),
        'type_factura': fields.char(
            'Tipus de factura', size=4, help=u'Indica de quin tipus de factura '
                                             u'es tracta (N, R...)',
            readonly=True
        ),
        'fecha_factura': fields.date('Data Factura', readonly=True),
        'fecha_factura_desde': fields.date('Inici periode factura',
                                           readonly=True),
        'fecha_factura_hasta': fields.date('Fi periode factura', readonly=True),
        'importacio_lectures_ids': fields.one2many(
            'giscedata.facturacio.importacio.linia.lectures', 'linia_id',
            'Lectures Importades XML'
        ),
        'buscar_remesa': fields.function(
            _referencies, type='char', string=u'Referència de la remesa',
            fnct_search=_buscar_f1_per_referencia, method=True, size=64
        ),
        'remesa_id': fields.char('Referència de la remesa', size=26),
        'total_rebuts': fields.integer('Nº de Rebuts'),
        'import_remesa': fields.float('Import Remesa', digits=(13, 2))
    }

    _defaults = {
        'import_phase': lambda *a: _IMPORT_PHASES[0][0],
        'ignore_reference': lambda *a: False,
        'lectures_processades': lambda *a: False,
    }

    _order = 'create_date desc, write_date desc'

GiscedataFacturacioImportacioLinia()


class GiscedataFacturacioImportacioLiniaFactura(osv.osv):
    """Model relacional linia-factura"""

    _name = 'giscedata.facturacio.importacio.linia.factura'
    _description = 'relació entre linies d\'importació i factures'
    _inherits = {'giscedata.facturacio.factura': 'factura_id'}

    _columns = {
        'linia_id': fields.many2one('giscedata.facturacio.importacio.linia',
                                   'Linia', requiered=True, select=1),
        'factura_id': fields.many2one('giscedata.facturacio.factura',
                                 'Factura', requiered=True, select=1,ondelete="cascade"),
    }

GiscedataFacturacioImportacioLiniaFactura()


class GiscedataFacturacioSwitchingConfig(osv.osv):
    """Configuració pels fitxers F1 segons l'emissor.
    """
    _name = 'giscedata.facturacio.switching.config'
    _rec_name = 'emissor'

    def get_emissor(self, cursor, uid, ree_code, context=None):
        """
        Gets origin partner from ree_code in switching_file. It gets partner_id
        with ree_code. If partner found has a parent partner, it returns it as
        origin partner. This functionality is for some DSO's like ENDESA and
        IBERDROLA that uses old DSO codes but its own CUPS prefix.
        :param ree_code:
        :return: origin DSO partner id
        """
        partner_obj = self.pool.get('res.partner')

        partner_ids = partner_obj.search(cursor, uid, [('ref', '=', ree_code)])

        if not partner_ids:
            raise message.except_f1(
                'PartnerNotFound',
                _("No s'ha trobat cap empresa amb codi {0}").format(ree_code)
            )
        elif len(partner_ids) > 1:
            raise message.except_f1(
                'MultiplePartnerFound',
                _("S'ha trobat més d'un empresa amb codi {0}").format(ree_code)
            )

        partner_id = partner_ids[0]

        parent_id = partner_obj.read(
            cursor, uid, partner_id, ['parent_id']
        )['parent_id']

        if parent_id:
            return parent_id[0]
        else:
            return partner_id

    def check_destination_code(self, cursor, uid, ree_destination_code):
        user_obj = self.pool.get('res.users')
        user_browse = user_obj.browse(cursor, uid, uid)

        user_partner = user_browse.company_id.partner_id.ref

        if user_partner != ree_destination_code:
            raise message.except_f1(
                'NotOurPartner',
                _("El destinatari d \'aquest fitxer no és la nostra comercialitzadora : {0}").format(ree_destination_code)
            )

    def get_already_imported(self, cursor, uid, line_id):
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        line_attrs = line_obj.read(cursor, uid, line_id, ['md5_hash'])
        line_006_ids = line_obj.search(cursor, uid, [('md5_hash', '=', line_attrs['md5_hash']), ('import_phase', '>', 10)])
        line_066_ids = line_obj.search(cursor, uid, [('md5_hash', '=', line_attrs['md5_hash']), ('import_phase', '=', 10)])

        return {'lines_006': line_006_ids, 'lines_066': line_066_ids}
    
    def check_already_imported(self, cursor, uid, file_ids):
        if file_ids['lines_006']:
            raise message.except_f1(
                'F1AlreadyImportedNotInPhase1',
                _("Aquest fitxer XML ja s'ha processat en els següents IDs: {0} "
                  "i són en fases més avançades del procés.").format(file_ids['lines_006'])
            )
        if len(file_ids['lines_066']) > 1:
            raise message.except_f1(
                'F1AlreadyImportedInPhase1',
                _("Aquest fitxer XML ja s'ha processat en els següents IDs: {0}, "
                  "que també estàn en fase 1. És possible que el fitxer estigui "
                  "duplicat al ZIP d'entrada.").format(file_ids['lines_066'])
            )

    def default_potencia(self, cursor, uid, context=None):
        """Retorna la unitat per defecte de potència.
        """
        uom_obj = self.pool.get('product.uom')
        uom_id = uom_obj.search(cursor, uid,
                                [('category_id.name', '=', 'POT ELEC'),
                                 ('factor', '=', 1)], limit=1)
        if uom_id:
            return uom_id[0]
        return False

    def default_energia(self, cursor, uid, context=None):
        """Retorna la unitat per defecte de potència.
        """
        uom_obj = self.pool.get('product.uom')
        uom_id = uom_obj.search(cursor, uid,
                                [('category_id.name', '=', 'ENEG ELEC'),
                                 ('factor', '=', 1)], limit=1)
        if uom_id:
            return uom_id[0]
        return False

    def default_lloguer(self, cursor, uid, context=None):
        """Retorna la unitat per defecte del lloguer.
        """
        uom_obj = self.pool.get('product.uom')
        uom_id = uom_obj.search(cursor, uid,
                                [('category_id.name', '=', 'ALQ ELEC'),
                                 ('factor', '=', 1)], limit=1)
        if uom_id:
            return uom_id[0]
        return False

    _columns = {
        'emissor': fields.many2one('res.partner', 'Emissor', required=True),
        'uom_potencia_f': fields.many2one('product.uom',
            u'Unitat de potència orígen', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'uom_potencia_t': fields.many2one('product.uom',
            u'Unitat de potència destí', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'uom_fact_max_f': fields.many2one('product.uom',
            u'Unitat de maxímetres orígen', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'uom_fact_max_t': fields.many2one('product.uom',
            u'Unitat de maxímetres destí', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'preu_potencia_f': fields.many2one('product.uom',
            u'Preu de potència orígen', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'preu_potencia_t': fields.many2one('product.uom',
            u'Preu de potència destí', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'uom_energia_f': fields.many2one('product.uom',
            u'Unitat d\'energia orígen', required=True,
            domain=[('category_id.name', '=', 'ENEG ELEC')]),
        'uom_energia_t': fields.many2one('product.uom',
            u'Unitat d\'energia destí', required=True,
            domain=[('category_id.name', '=', 'ENEG ELEC')]),
        'preu_energia_f': fields.many2one('product.uom',
            u'Preu d\'energia orígen', required=True,
            domain=[('category_id.name', '=', 'ENEG ELEC')]),
        'preu_energia_t': fields.many2one('product.uom',
            u'Preu d\'energia destí', required=True,
            domain=[('category_id.name', '=', 'ENEG ELEC')]),
        'preu_lloguer_f': fields.many2one('product.uom',
            u'Preu de lloguer orígen', required=True,
            domain=[('category_id.name', '=', 'ALQ ELEC')]),
        'preu_lloguer_t': fields.many2one('product.uom',
            u'Preu de lloguer destí', required=True,
            domain=[('category_id.name', '=', 'ALQ ELEC')]),
        'uom_lect_max_f': fields.many2one('product.uom',
            u'Unitat de maxímetres orígen', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'uom_lect_max_t': fields.many2one('product.uom',
            u'Unitat de maxímetres destí', required=True,
            domain=[('category_id.name', '=', 'POT ELEC')]),
        'uom_lect_energia_f': fields.many2one('product.uom',
            u'Unitat d\'energía orígen', required=True,
            domain=[('category_id.name', '=', 'ENEG ELEC')]),
        'uom_lect_energia_t': fields.many2one('product.uom',
            u'Unitat d\'energía destí', required=True,
            domain=[('category_id.name', '=', 'ENEG ELEC')]),
        # payment order info
        'create_payment_order': fields.boolean(
            u'Crea la Remesa de pagament',
            help=u"Crear una remesa de pagament amb el mateix nom que la "
                 u"que s'indica en el F1 i hi afegeix les factures generades"),
        'payment_mode_id': fields.many2one(
            'payment.mode', string=u'Mode de pagament',
            help=u"Mode de pagament per defecte de la remesa que es crearà"),
    }
    def get_config(self, cursor, uid, emissor, context=None):
        """Retorna un diccionari amb els valors de la config.
        """
        config_ids = self.search(cursor, uid, [('emissor.ref', '=', emissor)],
                                 context={'active_test': False})
        if not config_ids:
            # Si no troba la configuració genera una excepció
            raise osv.except_osv('Error',
                                 _(u"No s'ha trobat la configuració de "
                                 "switching per l'emisor: %s") % emissor)
        else:
            config = self.read(cursor, uid, config_ids[0])
            del config['emissor']
            del config['id']
            # Convertim els many2one (id, Name)
            non_tuple_fields = ['create_payment_order']
            for key in config:
                if key not in non_tuple_fields and config[key]:
                    config[key] = config[key][0]
        return config

    _defaults = {
        'uom_potencia_f': default_potencia,
        'uom_potencia_t': default_potencia,
        'preu_potencia_f': default_potencia,
        'preu_potencia_t': default_potencia,
        'uom_energia_f': default_energia,
        'uom_energia_t': default_energia,
        'preu_energia_f': default_energia,
        'preu_energia_t': default_energia,
        'preu_lloguer_f': default_lloguer,
        'preu_lloguer_t': default_lloguer,
        'uom_lect_max_f': default_potencia,
        'uom_lect_max_t': default_potencia,
        'uom_lect_energia_f': default_energia,
        'uom_lect_energia_t': default_energia,
        'create_payment_order': lambda *a: False,
    }

    _sql_constraints = [('emissor_uniq', 'unique (emissor)',
                         _(u'Ja existeix una configuració entrada per '
                           u'aquest emissor'))]

GiscedataFacturacioSwitchingConfig()


class GiscedataFacturacioImportacioLiniaLecturaenergia(osv.osv):
    """Model relacional linia-lecturaenergia per línies divergents"""

    _name = 'giscedata.facturacio.importacio.linia.lecturaenergia'
    _description = 'relació entre linies d\'importació i lectures d\'energia'

    _columns = {
        'linia_id': fields.many2one('giscedata.facturacio.importacio.linia',
                                   'Linia', requiered=True, ondelete="cascade"),
        'lectura_id': fields.many2one('giscedata.lectures.lectura.pool',
                                 'Lectura', requiered=True, ondelete="cascade"),
        'factura_id': fields.many2one('giscedata.facturacio.factura',
                                 'Factura', requiered=True, ondelete="cascade"),
    }

GiscedataFacturacioImportacioLiniaLecturaenergia()


class GiscedataFacturacioImportacioLiniaLecturapotencia(osv.osv):
    """Model relacional linia-lecturapotencia per línies divergents"""

    _name = 'giscedata.facturacio.importacio.linia.lecturapotencia'
    _description = 'relació entre linies d\'importació i lectures de potencia'

    _columns = {
        'linia_id': fields.many2one('giscedata.facturacio.importacio.linia',
                                   'Linia', requiered=True, ondelete="cascade"),
        'lectura_id': fields.many2one('giscedata.lectures.potencia.pool',
                                 'Lectura', requiered=True, ondelete="cascade"),
        'factura_id': fields.many2one('giscedata.facturacio.factura',
                                 'Factura', requiered=True, ondelete="cascade"),
    }

GiscedataFacturacioImportacioLiniaLecturapotencia()


class GiscedataFacturacioImportacioLiniaExtra(osv.osv):
    """Model relacional linia-facturacioextra"""

    _name = 'giscedata.facturacio.importacio.linia.extra'
    _description = 'relació entre linies d\'importació i lectures de potencia'
    _inherits = {'giscedata.facturacio.extra': 'extra_id'}

    _columns = {
        'linia_id': fields.many2one('giscedata.facturacio.importacio.linia',
                                   'Linia', requiered=True, ondelete="cascade"),
        'extra_id': fields.many2one('giscedata.facturacio.extra',
                                    'Extra', requiered=True, ondelete="cascade"),

    }

GiscedataFacturacioImportacioLiniaExtra()


class GiscedataFacturacioImportacioLiniaLectures(osv.osv):
    _name = 'giscedata.facturacio.importacio.linia.lectures'
    _description = 'Relacio entre linies (F1) i lectures'

    _columns = {
        'linia_id': fields.many2one('giscedata.facturacio.importacio.linia',
                                    'Linia', requiered=True,
                                    ondelete="cascade"),
        'magnitud': fields.char('Magnitud', size=2, readonly=True),
        'lectura_desde': fields.float('Lectura Inicial', readonly=True),
        'lectura_actual': fields.float('Lectura Actual', readonly=True),
        'fecha_desde': fields.date('Data Lectura Inicial', readonly=True),
        'fecha_actual': fields.date('Data Lectura Actual', readonly=True),
        'comptador': fields.char('Nº de sèrie', size=32, readonly=True),
        'origen_desde': fields.char('Origen Inicial', size=2, readonly=True),
        'origen_actual': fields.char('Origen Actual', size=2, readonly=True),
        'periode': fields.char('Període', size=16, readonly=True),
        'ajust': fields.float('Ajust Integrador', readonly=True),
        'motiu': fields.char('Motiu ajust', size=64, readonly=True),

    }
    _order = "comptador,magnitud,periode"
GiscedataFacturacioImportacioLiniaLectures()