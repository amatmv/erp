# -*- coding: utf-8 -*-

import netsvc

def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', level, msg)

def migrate(cursor, installed_version):
    """Fiquem les constraints en direcció a la nova taula"""
    log('Migration from %s' % installed_version)
    log('Creant constraints')
    cursor.execute("ALTER TABLE "
        "giscedata_facturacio_importacio_linia_lecturaenergia ADD FOREIGN KEY "
        "(lectura_id) REFERENCES giscedata_lectures_lectura_pool(id) "
        "ON DELETE cascade"
    )
    cursor.execute("ALTER TABLE "
        "giscedata_facturacio_importacio_linia_lecturapotencia ADD FOREIGN KEY "
        "(lectura_id) REFERENCES giscedata_lectures_potencia_pool(id) "
        "ON DELETE cascade"
    )