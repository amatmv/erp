# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import netsvc


def migrate(cursor, installed_version):
    """Migration to 2.31.0
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Updating default F1 '
                                                       'uom_fact_max '
                                                       'From F1 uom_potencia')
    cursor.execute("update giscedata_facturacio_switching_config "
                   "set "
                   "uom_fact_max_f=uom_potencia_f, "
                   "uom_fact_max_t=uom_potencia_t")
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')