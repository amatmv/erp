# -*- coding: utf-8 -*-
import netsvc


def migrate(cursor, installed_version):
    """Migration to 2.67.0
    """
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u'Updating default F1 "lectures processades" depending on import state:'
    )
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u' * Phase 4    :       Lectures processades = True'
    )
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u' * Phase 1, 2, 3 o 5: Lectures processades = False'
    )
    cursor.execute("update giscedata_facturacio_importacio_linia "
                   "SET lectures_processades = True "
                   "WHERE import_phase = 40 AND state = 'valid'")
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')

up = migrate
