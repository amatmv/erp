# -*- coding: utf-8 -*-
import netsvc


def migrate(cursor, installed_version):
    """Migration to 2.31.0
    """
    logger = netsvc.Logger()
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Updating default Q1 '
                                                       'UoMs to F1')
    cursor.execute("update giscedata_facturacio_switching_config set "
                   "uom_lect_energia_f=uom_energia_f,"
                   "uom_lect_energia_t=uom_energia_t,"
                   "uom_lect_max_f=uom_potencia_f,"
                   "uom_lect_max_t=uom_potencia_t")
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')