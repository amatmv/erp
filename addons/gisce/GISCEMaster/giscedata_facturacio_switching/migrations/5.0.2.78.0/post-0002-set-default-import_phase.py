# -*- coding: utf-8 -*-
import netsvc


def migrate(cursor, installed_version):
    """Migration to 2.67.0
    """
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u'Updating default F1 import phase depending on import state:'
    )
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u' * "valid"    : Phase 4 - "Càrrega de dades" (40)'
    )
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u' * "incident" : Phase 4 - "Càrrega de dades" (40)'
    )
    cursor.execute("update giscedata_facturacio_importacio_linia "
                   "SET import_phase = 40 "
                   "WHERE state in ('valid','incident')")
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u' * "erroni"   : Phase 1 - "Càrrega XML" (10)'
    )
    cursor.execute("update giscedata_facturacio_importacio_linia "
                   "SET import_phase = 10 "
                   "WHERE state in ('erroni')")
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')

up = migrate
