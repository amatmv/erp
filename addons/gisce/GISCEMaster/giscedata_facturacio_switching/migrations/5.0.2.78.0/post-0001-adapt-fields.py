# -*- coding: utf-8 -*-
import netsvc
from oopgrade import oopgrade

def migrate(cursor, installed_version):
    """Migration to 2.67.0
    """
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u'Adjusting fields:'
    )
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u' * "giscedata_facturacio_importacio_line.name" : LENGTH(1024)'
    )

    if oopgrade.column_exists(
            cursor, "giscedata_facturacio_importacio_linia", "name"):
        oopgrade.change_column_type(
            cursor,
            {"giscedata_facturacio_importacio_linia": [
                ("name", "VARCHAR(1024)")
            ]}
        )
    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')

up = migrate
