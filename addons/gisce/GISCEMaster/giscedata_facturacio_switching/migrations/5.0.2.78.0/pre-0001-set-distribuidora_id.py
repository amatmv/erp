# -*- coding: utf-8 -*-
import netsvc
from oopgrade import oopgrade


def migrate(cursor, installed_version):
    """Migration to 2.67.0
    """
    logger = netsvc.Logger()
    logger.notifyChannel(
        'migration', netsvc.LOG_INFO,
        u'Filling distribuidora_id and attachment_id from '
        u'giscedata_facturacio_importacio.partner_id and ir_attachment'
    )

    if not oopgrade.column_exists(
            cursor, 'giscedata_facturacio_importacio_linia', 'distribuidora_id'
    ):
        oopgrade.add_columns(
            cursor,
            {
                'giscedata_facturacio_importacio_linia': [
                    ('distribuidora_id', 'INTEGER NOT NULL DEFAULT 0'),
                    ('attachment_id', 'INTEGER NOT NULL DEFAULT 0')
                ]
            }
        )

    cursor.execute(
        "UPDATE giscedata_facturacio_importacio_linia "
        "SET distribuidora_id = COALESCE(i.partner_id, 1) ,"
        "    attachment_id = a.id "
        "FROM giscedata_facturacio_importacio i, ir_attachment a "
        "WHERE i.id=giscedata_facturacio_importacio_linia.importacio_id "
        "    AND a.res_model='giscedata.facturacio.importacio.linia' "
        "    AND a.res_id = giscedata_facturacio_importacio_linia.id "
    )

    logger.notifyChannel('migration', netsvc.LOG_INFO, 'Done.')

up = migrate
