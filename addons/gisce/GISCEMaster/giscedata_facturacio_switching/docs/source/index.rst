.. switching documentation master file, created by
   sphinx-quickstart on Fri Jan 20 16:42:27 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentació d’usuari del mòdul de switching de facturació
==========================================================


El mòdul de Switching permet, de moment, processar els fitxers XML de
facturació, ja sigui agrupats en un sol fitxer zip o individualment. Noves
funcionalitats s'aniran incorporant properament.

Aquest document descriu les funcionalitats del mòdul de Switching de facturació.


Lot d'importació i línies 
-------------------------

Denominem com a **lot d'importació** aquell conjunt de fitxers XML generats per
un mateix emisor i que ens convé tenir agrupats. El lot d'importació ens
permetrà importar el fitxer zip i/o xmls individualment. 

Cada un dels fitxers xml importats, ja sigui provinents d'un fitxer zip o
individualment, genera una **línia d'importació**. La línia d'importació
conté informació relacionada al fitxer xml importat i ens permet llistar les 
factures associades al fitxer xml o reimportar el fitxer en cas d'error.

El mòdul de switching de facturació  es troba en el submenú: Facturació >
Mercat Lliure > Factures Proveïdor > Importacions (veure `Figura 1`_). Aquest
desplegarà una vista amb el llistat de lots d'importació (veure `Figura 2`_).
El llistat ens mostra informació relacionada a cada lot d'importació com ara el
nom dels fitxers importats, l'empresa emisora i altre informació relacionada
amb el resultat.

També podem accedir al llistat de tots els fitxers XML individuals importats (o
línia d'importació) des del sub-menú *Fitxers F1 importats*

En prémer en un lot d'importació veurem el formulari del lot en qüestió (veure
`Figura 3`_). El formulari mostra juntament amb la informació del llistat,
una sèrie d'opcions a la dreta: Importar F1, Llistat de factures divergents,
Llistat de factures i Llistat de fitxers que es detallen a continuació.

* Importar F1:

    Ens permetrà importar un nou fitxer zip i associar-lo al lot d'importació. El
    widget que ens apareixerà es mostra en la `Figura 4`_. Aquest ens
    demanarà el nom de l'emprea emisora, i ens permetrà sel·leccionar el fitxer a
    importar.
    
    En el cas de voler importar un nou fitxer zip en un lot d'importació existent,
    l'empresa emisora haurà de coincidir amb l'empresa emisora associada a lot
    d'importació i que haurà quedat vinculada a partir de l'importació anterior.
    
    En prémer Importar, es realitzarà la importació en *background*. El que
    significa que podrem seguir treballant mentre l'ERP va important els xmls. En
    finalitzar el procés se'ns notificarà a través d'una nova sol·licitud (*Request*
    que es mostra abaix a la dreta de la `Figura 3`_).
    
    També podem anar prement el refrescar del formulari veure l'estat de la barra
    de progrés.
    
    En finalitzar la importació se'ns actualitzarà la informació del lot
    d'importació, mostrant en el cas que no hi hagin hagut incidents, el nombre de
    fitxers xmls importats i factures creades.

* Llistat de fitxers:

    Ens permet mostrar els fitxers xml que hi ha associats al lot d'importació
    després d'importar-los individualment o a través d'un fitxer zip. En prémer es
    mostra el llistat de línies d'improtació. Veure `Figura 5`_. El llistat
    ens mostra informació relacionada amb cadascun dels fitxers xml processats.
    
    En prémer en una línia d'importació podrem veure un resum de la informació
    de les factures generades en el cas d'haver-se importat de manera correcte.
    veure `Figura 6`_. Prement el botó **Llistar factures** podrem veure tota la 
    informació de les factures (veure `Figura 7`_).
    
    Si la importació ha estat errònia, el que veurem és el que es mostra en la
    `Figura 8`_. En aquest cas ens permet reimportar el fitxer xml
    individualment. Es pot veure el wizard que ens apareixerà en la 
    `Figura 9`_.

* Llistat de factures:

    Ens mostrarà totes les factures associades al lot d'importació, sigui quin
    sigui el seu estat (típicament obert o borrador).

* Llistat de factures divergents:

    Ens mostrarà aquelles factures que han resultat ser divergents respecte la
    facturada per l'ERP.  A continuació s'explicarà el concepte de divergència.

Accés a fitxers F1 d'un CUPS
----------------------------

Cada vegada que es processa una línia d'importació s'emmagatzema la informació
del CUPS al qual fa referència. Això ens permet accedir des d'un CUPS a tots els
fitxers F1 (o línies d'importació) que hi fan referència. Veure `Figura 14`_

Procediment d'importació
------------------------
    
En processar un fitxer xml, ja sigui provinent d'un zip o individualment, el
mòdul realitza les següents accions:

1. Facturar a partir de les lectures del fitxer xml.
2. Generar una factura a partir de les línies de factura del fitxer xml.
3. Comparar ambdues factures.

Del procés anterior poden resultar-ne les situacions següents:

* Mateix total en les dues factures:

    S'elimina la factura generada a partir de les lectures, quedant únicament la
    factura importada. Aquest última es deixa en estat **obert**. El formulari de
    la línia d'importació ens mostrarà un resum de les factures i ens donarà la
    opció de llistar-ne tota la informació.

* Divergència en els totals:

    En aquest cas es conserven les dues factures per un anàlisi posterior. Les
    dues factures es deixen amb estat de **borrador**. El formulari de la línia
    d'importació associada a l'xml ens mostrarà les factures i el valor de la 
    divergència en els totals. Veure `Figura 10`_. Tal i com s'observa,
    La factura realitzada per l'ERP a partir de les lectures de l'xml se li 
    assigna el mateix origen que la factura importada, juntament amb la 
    cadena de text '/comp'. Indicant que és la factura que s'usa per comparar. 
    L'altre factura, la importada a partir de les línies de factura, mostra en
    el camp divergència la resta dels totals.

    El formulari ens donarà la opció de reimportar el fitxer xml i llistar les
    factures (`Figura 11`_).

Aprovar factures divergents
---------------------------

Per tal d'acceptar la divergència que s'ha produït i donar la factura per
vàlida, cal visualitzar les factures divergents mitjançant el botó **LListat de
factures divergents** del formulari del lot d'importació. Inicialment es
mostraran totes les factures divergents del lot. Podem refinar la cerca i
mostrar únicament aquelles factures amb divergències dins un rang acceptable
com es mostra en l'exemple de la `Figura 12`_, que mostraria totes les
factures per aprovar amb divergència per sota d'un euro.

Tot seguit es pot prémer el botó de la barra d'eines **Action** que ens
desplegarà les accions disponibles (`Figura 13`_). Únicament serà
necessari prémer **Aprovar factura F1 (XML)**. Quedaran aprovades aquelles factures del
llistat que estiguin sel·leccionades, o bé totes si no se n'ha sel·leccionat
cap.


Figures
-------

.. _`Figura 1`:
.. figure::  ./img/importacions_widget.png
   :align:  center
   
   Figura 1: Ubicació del formulari d'importacions.

.. _`Figura 2`:
.. figure::  ./img/lot_tree.png
   :align:  center

   Figura 2: Llistat de lots d'importació.

.. _`Figura 3`:
.. figure:: ./img/lot_form.png
   :align: center

   Figura 3: Formulari d'un lot d'importació.

.. _`Figura 4`:
.. figure:: ./img/zip_wizard.png
   :align: center

   Figura 4: Importació d'un zip.

.. _`Figura 5`:
.. figure:: ./img/linies_tree.png
   :align: center

   Figura 5: Llistat de línies d'importació.

.. _`Figura 6`:
.. figure:: ./img/linies_form.png
   :align: center

   Figura 6: Formulari d'una línia d'importació correcte.

.. _`Figura 7`:
.. figure:: ./img/factura_tree.png
   :align: center

   Figura 7: Llistat de factures vinculades a la línia d'importació.

.. _`Figura 8`:
.. figure:: ./img/linies_form_error.png
   :align: center

   Figura 8: Formulari d'una línia d'importació errònia.

.. _`Figura 9`:
.. figure:: ./img/wizard_xml.png
   :align: center

   Figura 9: Importació d'un XML.

.. _`Figura 10`:
.. figure:: ./img/linies_form_div.png
   :align: center

   Figura 10: Formulari d'una línia d'importació amb divergències en els 
   totals.

.. _`Figura 11`:
.. figure:: ./img/fact_tree_div.png
   :align: center

   Figura 11: Llistat de factures divergents associades a la línia 
   d'importació.

.. _`Figura 12`:
.. figure:: ./img/fact_tree_div_2.png
   :align: center

   Figura 12: Llistat de factures divergents.

.. _`Figura 13`:
.. figure:: ./img/fact_aprovar.png
   :align: center

   Figura 13: Aprovar factures divergents

.. _`Figura 14`:
.. figure:: ./img/cups.png
   :align: center

   Figura 13: Accés a F1's des de CUPS