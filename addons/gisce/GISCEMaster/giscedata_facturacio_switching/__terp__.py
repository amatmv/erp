# -*- coding: utf-8 -*-
{
    "name": "Facturació switching",
    "description": """Facturació switching. 
        Per importar factures cal configurar correctament la tarifa
        de compra de les distribuidores a TARIFAS ELECTRICIDAD""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_facturacio_comer",
        "giscedata_lectures_switching",
        "giscedata_remeses",
        "attachments_tab",
        "giscedata_switching",
    ],
    "init_xml": [],
    "demo_xml":[
        "giscedata_facturacio_switching_demo.xml"
    ],
    "update_xml":[
        "wizard/wizard_canvi_fase.xml",
        "wizard/giscedata_facturacio_switching_wizard.xml",
        "giscedata_facturacio_switching_view.xml",
        "giscedata_facturacio_view.xml",
        "giscedata_facturacio_switching_error_view.xml",
        "giscedata_facturacio_validation_data.xml",
        "ir.model.access.csv",
        "giscedata_facturacio_data.xml",
        "giscedata_facturacio_switching_data.xml",
        "giscedata_facturacio_switching_error_data.xml",
        "giscedata_cups_ps_view.xml",
        "wizard/wizard_importacio_F1_wizard.xml",
        "wizard/wizard_reimportacio_F1.xml",
        "security/ir.model.access.csv",
        "wizard/wizard_generate_R1_from_F1_erroni.xml",
        "wizard/wizard_check_lectures_processades.xml"
    ],
    "active": False,
    "installable": True
}
