# -*- coding: utf-8 -*-
from __future__ import absolute_import

import zipfile
import base64
import StringIO
from os.path import basename
import traceback

from calendar import isleap
from datetime import datetime, timedelta

import pooler
from .giscedata_facturacio_switching import LineProcessException
from tools import config
from tools.translate import _
from osv import osv, orm, fields

from gestionatr.input.messages import F1, Q1, message, defs
from libfacturacioatr import tarifes
from gestionatr.helpers.funcions import *
from .giscedata_facturacio_switching_utils import (
    get_remeses_from_zip, get_emisor
)
from gestionatr.defs import REQUIRE_REFERENCE_TYPES
from addons.giscedata_facturacio.giscedata_facturacio import TARIFES
from addons.giscedata_facturacio.defs \
    import REFUND_RECTIFICATIVE_TYPES, RECTIFICATIVE_FROM_F1_TO_DB, SIGN
from gestionatr.input.messages.F1 \
    import agrupar_lectures_per_data, obtenir_data_inici_i_final
from giscedata_facturacio.giscedata_facturacio \
    import RECTIFYING_RECTIFICATIVE_INVOICE_TYPES, \
    ALL_RECTIFICATIVE_INVOICE_TYPES


class GiscedataFacturacioLot(osv.osv):
    """Funció d'ajuda per localitzar el lot"""

    _name = 'giscedata.facturacio.lot'
    _inherit = 'giscedata.facturacio.lot'

    def lot_per_data(self, cursor, uid, data, context=None):
        """Buscar el lot de facturació tal que contingui la data"""
        search_params = [('data_inici', '<=', data),
                         ('data_final', '>=', data)]
        lot = self.search(cursor, uid, search_params)
        if not lot:
            raise osv.except_osv('Error', _("No s'ha trobat el lot"))
        return lot[0]

GiscedataFacturacioLot()


class GiscedataFacturacioSwitchingHelper(osv.osv):
    """Funcions d'ajuda switching F1"""

    _name = 'giscedata.facturacio.switching.helper'

    def get_name_tarifa(self, cursor, uid, tarifa):
        search_params = [('codi_ocsum', '=', tarifa)]
        tarifa_pool = self.pool.get('giscedata.polissa.tarifa')
        tarifa_id = tarifa_pool.search(cursor, uid, search_params)
        obj = tarifa_pool.browse(cursor, uid, tarifa_id[0])
        return obj.name

    def importar_lectures_facturacio(self, cursor, uid, lect_data, factura_id,
                                     fact, context):
        lect = []
        for tipus in lect_data:
            for periode in lect_data[tipus]['final']:
                for i in range(len(lect_data[tipus]['final'][periode])):
                    vals_final = lect_data[tipus]['final'][periode][i][0]
                    if tipus in ['A', 'R']:
                        vals_inicial = lect_data[tipus]['inicial'][periode][i][0]
                        nom_tipus = 'activa' if  tipus == 'A' else 'reactiva'
                        lect_pool = self.pool.get(
                                       'giscedata.facturacio.lectures.energia')
                        nom_tarifa = self.get_name_tarifa(cursor, uid,
                                                     fact.codi_tarifa)
                        vals = {
                            'name': "%s (%s)" % (nom_tarifa, periode),
                            'lect_actual': vals_final['lectura'],
                            'consum': vals_final['consum'],
                            'lect_anterior': vals_inicial['lectura'],
                            'data_actual': vals_final['name'],
                            'data_anterior': vals_inicial['name'],
                            'factura_id': factura_id,
                            'comptador_id': vals_final['comptador'],
                            'magnitud': lect_data[tipus]['magnitud'],
                            'tipus': nom_tipus,
                            'origen_id': vals_final['origen_id'],
                            'origen_anterior_id': vals_inicial['origen_id']
                        }
                        ajust = vals_inicial.get('ajust', False)
                        motiu_ajust = vals_inicial.get('motiu_ajust', False)
                        if ajust and motiu_ajust:
                            vals.update({
                                'ajust': ajust,
                                'motiu_ajust': motiu_ajust,
                            })
                    elif tipus == 'M':
                        lect_pool = self.pool.get(
                                      'giscedata.facturacio.lectures.potencia')
                        q1_obj = self.pool.get(
                                        'giscedata.lectures.switching.helper')
                        pol_pool = self.pool.get('giscedata.polissa')
                        p_id = q1_obj.find_polissa(cursor, uid, fact.cups,
                                              fact.data_inici, fact.data_final)
                        p_obj = pol_pool.browse(cursor, uid, p_id)
                        vals = {
                            'name': periode,
                            'comptador_id': vals_final['comptador'],
                            'factura_id': factura_id,
                            'pot_contract': p_obj.potencia,
                            'pot_maximetre': vals_final['lectura'],
                            'exces': vals_final['exces'],
                        }
                    ids = lect_pool.create(cursor, uid, vals, context)
                    lect.append(ids)
        return ids

    def facturar_lectures(self, cursor, uid, polissa, fact, d_lect, context):
        """Facturar les lectures existents"""
        lot_obj = self.pool.get('giscedata.facturacio.lot')
        fact_obj = self.pool.get('giscedata.facturacio.facturador')
        f_obj = self.pool.get('giscedata.facturacio.factura')
        journal_obj = self.pool.get('account.journal')
        # Busquem el diari que toqui
        journal_code = 'CENERGIA'
        jid = journal_obj.search(cursor, uid, [('code', '=', journal_code)],
                                 limit=1)
        if not jid:
            msg = _(u"No s'ha trobat el diari corresponent")
            raise osv.except_osv('Error', msg)
        lot = lot_obj.lot_per_data(cursor, uid, fact.data_final)
        context.update({'type_invoice': 'in_invoice',
                        'data_factura': fact.data_factura,
                        'lot_id': lot,
                        'data_boe': fact.data_BOE,
                        'factura_manual': True,
                        'data_inici_factura': fact.data_inici,
                        'data_final_factura': fact.data_final,
                        'ult_lectura_fact': d_lect['inicial'],
                        'fins_lectura_fact': d_lect['final'],
                        'origin': str(fact.numero_factura) + '/comp',
                        'journal_id': jid[0]})
        factures = fact_obj.fact_via_lectures(cursor, uid, polissa, lot, context)
        f_obj.write(cursor, uid, factures, {'date_invoice': fact.data_factura})
        # TODO: Pensar si s'ha de sobreescriure la línia del lloguer segons el
        # que ens vingui de l'XML
        vals = {'importacio_id': context.get('importacio_id', False)}
        if not factures:
            msg = _(u"No s'ha pogut facturar a través de lectures")
            raise osv.except_osv('Error', msg)
        f_obj.write(cursor, uid, factures[0], vals)
        return factures[0]

    def get_subtotal_linia(self, cursor, uid, lid, context=None):
        """Retorna el subtotal de la línia de factura lid"""
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        val = linia_obj.read(cursor, uid, lid, ['price_subtotal'], context)
        return val['price_subtotal']

    def get_account_and_tax(self, cursor, uid, lid, context=None):
        """Retorna la compta comptable i els impostos de la línia per
        insertar-los a un altre.
        """
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        vals = linia_obj.read(cursor, uid, lid, ['invoice_line_tax_id',
                                                 'account_id'], context)
        del vals['id']
        vals['invoice_line_tax_id'] = [(6, 0,
                                        vals.get('invoice_line_tax_id', []))]
        vals['account_id'] = vals['account_id'][0]
        return vals

    def get_total_factura(self, cursor, uid, fid, context=None):
        """Retorna el total de la factura"""
        fact = self.pool.get('giscedata.facturacio.factura')
        total = fact.read(cursor, uid, fid, ['amount_total'], context)
        return total['amount_total']

    def get_factura_referencia(self, cursor, uid, fact_xml, journal_code, partner_id,
                               context=None):
        """Retorna l'id de la factura que anul·la o rectifica aquesta
        factura.
        """
        # Agafem el journal_code base, això vol dir la part esquerra del punt
        # ja que separem els d'anul·ladures/rectificadores amb JOURNAL_CODE.R
        journal_code = journal_code.split('.')[0]

        datos_factura = fact_xml.datos_factura
        if datos_factura.tipo_factura not in REQUIRE_REFERENCE_TYPES:
            return False
        f_obj = self.pool.get('giscedata.facturacio.factura')
        journal_obj = self.pool.get('account.journal')

        journal_ids = journal_obj.search(
            cursor, uid, [('code', 'ilike', journal_code)]
        )

        fid = f_obj.search(cursor, uid, [
            ('origin', '=', datos_factura.codigo_factura_rectificada_anulada),
            ('journal_id', 'in', journal_ids),
            ('partner_id', '=', partner_id)
        ])
        if not (fid and datos_factura.codigo_factura_rectificada_anulada):
            raise osv.except_osv(
                _(u"Error"),
                _(u"No s'ha trobat la factura de referència amb número %s") % (
                    datos_factura.codigo_factura_rectificada_anulada
                )
            )
        elif fid and len(fid) > 1:
            raise osv.except_osv(
                _(u"Error"),
                _(u"S'ha trobar més de una factura de referència amb número %s") % (
                    datos_factura.codigo_factura_rectificada_anulada
                )
            )
        return fid[0]

    def get_payment_order(self, cursor, uid, emissor, f1_data, context=None):
        """Returns a payment order id of name id_remesa.
        if not found it is created"""
        f1config_obj = self.pool.get('giscedata.facturacio.switching.config')
        payorder_obj = self.pool.get('payment.order')

        f1_config = f1config_obj.get_config(cursor, uid, emissor)
        if not f1_config.get('create_payment_order', False):
            return None

        payment_mode = f1_config.get('payment_mode_id', False)
        if not payment_mode:
            raise osv.except_osv(
                'Error',
                _(u"No s'ha configurat el mode de pagament per crear la "
                  u"remesa a l'empresa {0}").format(emissor))

        payment_order_name = '{0}-{1}'.format(emissor, f1_data['id_remesa'])
        search_vals = [('reference', '=', payment_order_name),
                       ('type', '=', 'payable')]

        payorder_ids = payorder_obj.search(cursor, uid, search_vals,
                                           context=context)

        if payorder_ids:
            return payorder_ids[0]
        else:
            vals_pm = {
                'reference': payment_order_name,
                'date_prefered': 'fixed',
                'date_planned': f1_data['fecha_valor_remesa'],
                'user_id': uid,
                'state': 'draft',
                'mode': payment_mode,
                'type': 'payable',
                'create_account_moves': 'direct-payment',
            }
            payment_order_id = payorder_obj.create(
                cursor, uid, vals_pm, context
            )
        return payment_order_id

    def aplicar_descompte(self, cursor, uid, lid):
        """Aplica un descompte del 100%.
        """
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        linia_obj.write(cursor, uid, lid, {'discount': 100})
        return True

    def escriure_resum_linies(self, cursor, uid, obj, factura_id, lid):
        """Escriurem el resum de les línies per poder fer un seguiment.
        """
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        lobj = self.pool.get('giscedata.facturacio.factura.linia')
        factura_vals = fact_obj.read(cursor, uid, factura_id, ['comment'])
        comment = factura_vals['comment'] or ''
        if comment:
            comment += '\n'
        comment += u"******* %s ******\n" % obj.tipus.title()
        subtotal = 0
        for l in lobj.browse(cursor, uid, lid):
            comment += u"  %s: %s %s * %s € * %s = %s €\n" % (l.name,
                l.quantity, l.uos_id.name, l.price_unit, l.multi,
                l.price_subtotal)
            subtotal += l.price_subtotal
        comment += '\n'
        comment += u"  Subt XML: %s € - Subt calc: %s € = %s €\n" % (obj.total,
            subtotal, obj.total - subtotal)
        fact_obj.write(cursor, uid, factura_id, {'comment': comment})
        return factura_id

    def write_lines_summary(self, cursor, uid, file_id, tipus, line_xml,
                            factura_id, line_ids, context=None):
        """
        Escriurem el resum de les línies per poder fer un seguiment.
        :param cursor:
        :param uid:
        :param file_id:
        :param line_xml:
        :param factura_id:
        :param line_ids:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        factura_vals = fact_obj.read(cursor, uid, factura_id, ['comment'])
        comment = factura_vals['comment'] or ''
        if comment:
            comment += '\n'
        comment += u"******* %s ******\n" % tipus.title()
        subtotal = 0
        read_params = [
            'name', 'quantity', 'uos_id', 'price_unit_multi', 'multi',
            'price_subtotal'
        ]

        for line_vals in linia_obj.read(cursor, uid, line_ids, read_params):
            comment += u"  {0}: {1} {2} * {3} € * {4} = {5} €\n".format(
                line_vals['name'], line_vals['quantity'],
                line_vals['uos_id'][1], line_vals['price_unit_multi'],
                line_vals['multi'], line_vals['price_subtotal']
            )
            subtotal += line_vals['price_subtotal']
        comment += '\n'
        comment += u"  Subt XML: %s € - Subt calc: %s € = %s €\n" % (
            line_xml['total'], subtotal, line_xml['total'] - subtotal
        )
        fact_obj.write(cursor, uid, factura_id, {'comment': comment})

        # If the invoice has an error of more than a cent between the sum of the
        # lines and the total it gives
        if abs(round(line_xml['total'] - subtotal, 2)) > 0.01:
            error_obj.create_error_from_code(
                cursor, uid, file_id, '2', '006',
                {
                    'tipus': tipus,
                    'total_xml': line_xml['total'],
                    'total_imp': subtotal
                },
                context=context
            )

        return factura_id

    def actualitzar_idioma_ctx(self, cursor, uid, factura_id, type_invoice,
                                                                     context):
        """Actulitza la clau 'lang' del context amb l'idioma del partner
           associat a la factura"""
        _factura = self.pool.get('giscedata.facturacio.factura')
        fact = _factura.browse(cursor, uid, factura_id, context)
        if type_invoice == 'in_invoice':
            lang = fact.partner_id.lang
        else:
            lang = fact.polissa_id.pagador.lang
        ctx = context.copy()
        ctx.update({'lang': lang})
        return ctx

    def crear_notes_extra(self, cursor, uid, f_id, fname, context=None):
        """Generar la descripció per a les notes de la línia extra"""
        if not context:
            context = {}
        _fact = self.pool.get('giscedata.facturacio.factura')
        notes = _(u"Creada desde la factura de proveïdor amb id: %d\n"
                  u"Fitxer XML: %s") % (f_id, fname)
        return notes

    def crear_extra_facturacio(self, cursor, uid, f_id, fname, r_lid,
                                                                context=None):
        """Crear una nova entrada a giscedata.facturacio.extra especificant els
           periodes entre els quals hi ha linies de refacturació"""
        if not context:
            context = {}
        _fact = self.pool.get('giscedata.facturacio.factura')
        _linia = self.pool.get('giscedata.facturacio.factura.linia')
        _extra = self.pool.get('giscedata.facturacio.extra')
        _journal = self.pool.get('account.journal')
        tax_obj = self.pool.get('account.tax')
        j_id = _journal.search(cursor, uid, [('code','=','ENERGIA')], limit=1)
        if not j_id:
            raise osv.except_osv('Error',
                                 "No s'ha trobat el diari 'ENERGIA'")
        fac = _fact.browse(cursor, uid, f_id)
        ctx = self.actualitzar_idioma_ctx(cursor, uid, f_id, 'out_invoice',
                                          context)
        lin = _linia.browse(cursor, uid, r_lid, context=ctx)
        # El text següent ha de traduïr-se segons l'idioma del partner i no
        # de l'usuari de l'erp
        eid = []
        sign = {'A': -1, 'B': -1, 'R': 1, 'N': 1}
        for linia in lin:
            product = linia.product_id
            _name = product.description_sale or product.description
            if codi_refact(linia.product_id.code):
                consum, total = parse_totals_refact(linia.name)
                _name = _name % (consum, total)
            price_unit = linia.price_unit_multi * sign[fac.tipo_rectificadora]
            vals = {'polissa_id': fac.polissa_id.id,
                    'date_from': linia.data_desde,
                    'date_to': linia.data_fins,
                    'date_line_from': linia.data_desde,
                    'date_line_to': linia.data_fins,
                    'name': _name,
                    'product_id': linia.product_id.id,
                    'quantity': linia.quantity,
                    'uos_id': linia.uos_id.id,
                    'price_unit': price_unit,
                    'term': 1,
                    'journal_ids': [(6, 0, j_id)],
                    'notes': self.crear_notes_extra(cursor, uid, f_id,
                                                                fname, context),
                   }
            # Canviem tots els impostos pels seus "equivalents" en venda
            taxes = []
            for tax in linia.invoice_line_tax_id:
                if tax.type_tax_use == 'purchase':
                    taxes += tax_obj.search(cursor, uid, [
                        ('type_tax_use', '=', 'sale'),
                        ('amount', '=', tax.amount)
                    ], limit=1)

            vals['tax_ids'] = [(6, 0, taxes)]

            eid.append(_extra.create(cursor, uid, vals, context=context))
        return eid

    def update_meters(self, cursor, uid, line_id, f1_xml, context=None):
        """
        Here we will handle everything related with updating meters while
        importing F1 files. We will do things like:
            -> Create unexisting meters
            -> Deactivate old meters
            -> Handle dates on active and deactivated meters
        :param cursor: Database cursor
        :param uid: User id
        :param line_id: The line_id that we are currently working with
        :param f1_xml: F1 file that we are currently importing, from the
        switching library
        :param context: OpenERP's context
        :return: -
        """
        if context is None:
            context = {}

        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        cups_obj = self.pool.get('giscedata.cups.ps')
        imd_obj = self.pool.get('ir.model.data')

        uom_alq_dia = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'uom_aql_elec_dia'
        )[1]

        line_vals = line_obj.read(cursor, uid, line_id, ['cups_id'])
        cups_id = line_vals['cups_id'][0]

        for fact_xml in f1_xml.facturas_atr:
            if fact_xml.datos_factura.tipo_factura not in ['C']:
                # If it's a C invoice then we shouldn't have readings to import
                start_date = datetime.strptime(
                    fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
                )
                start_date += timedelta(days=1)

                data_inici = start_date.strftime('%Y-%m-%d')

                end_date = datetime.strptime(
                    fact_xml.datos_factura.fecha_hasta_factura, '%Y-%m-%d'
                )

                num_dias_fact = (end_date - start_date).days + 1

                polissa_id = lect_helper.find_polissa_with_cups_id(
                    cursor, uid, cups_id, data_inici,
                    fact_xml.datos_factura.fecha_hasta_factura
                )

                comptadors_xml = fact_xml.get_comptadors()
                lloguers, total = fact_xml.get_info_lloguer()

                calc_total = sum([
                    llog.precio_dia * llog.numero_dias
                    for llog in lloguers
                ])

                num_compt = len(comptadors_xml)
                comptadors_autoconsum = [c.numero_serie for c in comptadors_xml if len(c.get_lectures_activa_sortint()) >= 1]
                for i, compt_xml in enumerate(comptadors_xml):
                    lectures = compt_xml.get_lectures()
                    dict_lect = agrupar_lectures_per_data(lectures)
                    start_date, end_date = obtenir_data_inici_i_final(dict_lect)

                    # We add 1 day to start_date because initial readings are
                    # always not included, so the meter must start 1 day after
                    # initial reading
                    start_date = datetime.strptime(start_date, "%Y-%m-%d")
                    start_date += timedelta(days=1)
                    start_date = start_date.strftime("%Y-%m-%d")
                    ctx = context.copy()
                    ctx['es_autoconsum'] = compt_xml.numero_serie in comptadors_autoconsum
                    ctx['comptadors_autoconsum'] = comptadors_autoconsum
                    compt_id = lect_helper.check_meter(
                        cursor, uid, compt_xml.numero_serie,
                        compt_xml.gir_comptador, polissa_id, start_date,
                        end_date, num_compt, fact_xml, ctx
                    )

                    if compt_id:
                        if num_dias_fact == 0:
                            preu = 0
                        elif round(total, 2) != round(calc_total, 2):
                            preu = total / num_dias_fact
                        else:
                            # Add to context if rents must be processed as
                            # "simultanious" rents. They are simultanious rents
                            # when the number of total rent days is greater
                            # than num_dias_fact and for each individual
                            # rent the number of days is equal to num_dias_fact
                            new_context = context.copy()
                            new_context.update({
                                "simultanious_rents":
                                    self.check_simultanious_rents(
                                        num_dias_fact, lloguers
                                    )
                            })
                            preu = lect_helper.calculate_price_from_rent_prices(
                                cursor, uid, comptadors_xml, lloguers, i,
                                new_context
                            )

                        compt_vals = compt_obj.read(
                            cursor, uid, compt_id, ['preu_lloguer', 'lloguer']
                        )

                        price_changed = preu != compt_vals['preu_lloguer']

                        # If it's currently on rent and the price on the XML is
                        # different from the one we have
                        new_rent_price = price_changed and compt_vals['lloguer']
                        # Or if it's currently as not on rent but they send us
                        # a price
                        to_set_to_rent = preu > 0 and not compt_vals['lloguer']
                        if new_rent_price or to_set_to_rent:
                            compt_obj.write(
                                cursor, uid, compt_id, {
                                    'preu_lloguer': preu,
                                    'lloguer': True,
                                    'uom_id': uom_alq_dia
                                }
                            )

    @staticmethod
    def check_simultanious_rents(num_dias_fact, lloguers_xml):
        equal_rent_days = False
        rent_days = len(lloguers_xml) and lloguers_xml[0].numero_dias
        for l in lloguers_xml:
            equal_rent_days = rent_days == l.numero_dias
            if not equal_rent_days:
                break
        return equal_rent_days and rent_days == num_dias_fact

    def import_invoicing_reading_energy(self, cursor, uid, factura_id,
                                        tariff_name, period, lect_vals,
                                        context=None):
        if context is None:
            context = {}

        inv_read_obj = self.pool.get('giscedata.facturacio.lectures.energia')

        tipus = lect_vals['final']['tipus']

        nom_tipus = 'activa' if tipus in ['A', 'S'] else 'reactiva'

        lect_final_vals = lect_vals['final']
        lect_inic_vals = lect_vals['inicial']
        vals = {
            'name': "%s (%s)" % (tariff_name, period),
            'lect_actual': lect_final_vals['lectura'],
            'consum': lect_final_vals['consum'],
            'lect_anterior': lect_inic_vals['lectura'],
            'data_actual': lect_final_vals['name'],
            'data_anterior': lect_inic_vals['name'],
            'factura_id': factura_id,
            'comptador_id': lect_final_vals['comptador'],
            'magnitud': lect_final_vals['magnitud'],
            'tipus': nom_tipus,
            'origen_id': lect_final_vals['origen_id'],
            'origen_anterior_id': lect_inic_vals['origen_id']
        }
        ajust = lect_final_vals.get('ajust', False)
        motiu_ajust = lect_final_vals.get('motiu_ajust', False)
        if ajust and motiu_ajust:
            vals.update(
                {
                    'ajust': ajust,
                    'motiu_ajust': motiu_ajust,
                }
            )

        inv_read_id = inv_read_obj.create(cursor, uid, vals, context)

        return inv_read_id

    def import_invoicing_reading_power(self, cursor, uid, factura_id, period,
                                       lect_vals, context=None):
        if context is None:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        inv_read_obj = self.pool.get('giscedata.facturacio.lectures.potencia')

        fact_vals = fact_obj.read(cursor, uid, factura_id, ['polissa_id'])
        polissa_vals = polissa_obj.read(
            cursor, uid, fact_vals['polissa_id'][0], ['potencia']
        )

        vals = {
            'name': period,
            'comptador_id': lect_vals['comptador'],
            'factura_id': factura_id,
            'pot_contract': polissa_vals['potencia'],
            'pot_maximetre': lect_vals['lectura'],
            'exces': lect_vals['exces'],
        }

        inv_read_id = inv_read_obj.create(cursor, uid, vals, context)

        return inv_read_id

    def importar_lectures_generacio_a_pool(self, cursor, uid, line_id, fact_xml, polissa_id, context=None):
        """
        Aquest metode, donat un F1 processa totes les lectures de generacio. Per cada lectura de generacio:
            1.  - Busca el comptador actiu al contracte. Si no existeix falla ja que s'hauria d'haver creat anteriorment.
            2.  - Busca si ja hi ha una lectura en aquella data.
            2.1 - Si ja n'hi ha una pero el seu valor de lectura_sortint es != 0 falla per divergencia de lectures
            2.2 - Si ja n'hi ha una pero estem en una rectificadora, es sobreescriuen els valor se la lectura_exporta
            2.3 - Si ja n'hi ha i el valor de lectura_sortint es 0 escriu els valors de lectura_sortint i ajust_sortint
            2.4 - Si no n'hi ha cap crea una nova lectura amb els valors de lectura_sortint i ajust_sortint corresponents.
                  Com a valors de lectura_entrant es posen els valors que tingui la lectura anterior.
        :return: ids lectures afectades/generades
        """
        if context is None:
            context = {}

        context.update({
            'date': fact_xml.datos_factura.fecha_hasta_factura,
            'from_f1': True,
        })

        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        cfg = self.pool.get('res.config')

        overwrite_atr = bool(int(cfg.get(cursor, uid, 'lect_sw_overwrite_atr_measure_when_found', '0')))
        polissa_vals = polissa_obj.read(cursor, uid, polissa_id, ['tarifa'], context)
        tarifa_vals = tarifa_obj.read(cursor, uid, polissa_vals['tarifa'][0], ['codi_ocsum'], context)
        rectificadora = fact_xml.datos_factura.tipo_factura == 'R'
        tariff_name = polissa_vals['tarifa'][1]
        ocsum_tarifa = tarifa_vals['codi_ocsum']
        en_baixa = 'LB' in tariff_name
        energy_read_ids = []

        for compt_xml, lectures_xml in fact_xml.get_lectures_activa_sortint(ajust_balancejat=True).iteritems():
            compt_id = compt_obj.search_with_contract(
                cursor, uid, compt_xml.numero_serie, polissa_id
            )[0]

            for lect_xml in lectures_xml:
                if lect_xml.ometre:
                    continue

                # We process active and reactive readings
                l1 = self.proces_lect_xml_energia_sortint(
                    cursor, uid, lect_xml, compt_id, ocsum_tarifa, en_baixa,
                    tariff_name, rectificadora, overwrite_atr,
                    context=context
                )
                energy_read_ids.extend(l1)
        return energy_read_ids

    def proces_lect_xml_energia_sortint(self, cursor, uid, lect_xml, compt_id, ocsum_tarifa, en_baixa, tariff_name, rectificadora, overwrite_atr, context=None):
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        energy_read_ids = []
        lect_vals = {'inicial': {}, 'final': {}}
        for lect_point in ['inicial', 'final']:
            ctx = context.copy()
            ctx['lect_point'] = lect_point
            # We process both initial and final readings
            lect_vals[lect_point] = lect_helper.vals_lectura(
                cursor, uid, lect_xml, lect_point, compt_id,
                tarifa=ocsum_tarifa, en_baixa=en_baixa,
                context=context
            )
            if lect_vals[lect_point]['magnitud'] == 'AS':
                lect_id = lect_helper.import_reading(
                    cursor, uid, lect_vals[lect_point],
                    rectificadora=rectificadora,
                    overwrite_atr=overwrite_atr, context=ctx
                )
                if lect_id:
                    energy_read_ids.append(lect_id)
        return energy_read_ids

    def import_readings_from_fact(self, cursor, uid, line_id, fact_xml,
                                  context=None):
        if context is None:
            context = {}

        context.update(
            {
                'date': fact_xml.datos_factura.fecha_hasta_factura,
                'from_f1': True,
            }
        )

        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        lin_fact_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        cfg = self.pool.get('res.config')

        line_vals = line_obj.read(
            cursor, uid, line_id, ['cups_id', 'liniafactura_id'], context
        )

        start_date = datetime.strptime(
            fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        data_inici = start_date.strftime('%Y-%m-%d')

        polissa_id = lect_helper.find_polissa_with_cups_id(
            cursor, uid, line_vals['cups_id'][0], data_inici,
            fact_xml.datos_factura.fecha_hasta_factura
        )
        polissa_vals = polissa_obj.read(
            cursor, uid, polissa_id, ['tarifa', 'contract_type'], context
        )
        tarifa_vals = tarifa_obj.read(
            cursor, uid, polissa_vals['tarifa'][0], ['codi_ocsum'], context
        )
        lin_fact_id = lin_fact_obj.search(
            cursor, uid, [
                ('id', 'in', line_vals['liniafactura_id']),
                ('origin', '=', fact_xml.datos_factura.codigo_fiscal_factura)
            ]
        )[0]

        overwrite_atr = bool(int(
            cfg.get(
                cursor, uid, 'lect_sw_overwrite_atr_measure_when_found', '0'
            )
        ))
        rectificadora = fact_xml.datos_factura.tipo_factura == 'R'
        tariff_name = polissa_vals['tarifa'][1]
        ocsum_tarifa = tarifa_vals['codi_ocsum']
        en_baixa = 'LB' in tariff_name
        factura_id = lin_fact_obj.read(
            cursor, uid, lin_fact_id, ['factura_id']
        )['factura_id'][0]

        energy_read_ids = []
        energy_fact_read_ids = []
        power_read_ids = []
        power_fact_read_ids = []

        # Unlink old lectures of invoice
        old_lect = fact_obj.read(cursor, uid, factura_id, ['lectures_energia_ids', 'lectures_potencia_ids'])
        ene_read_obj = self.pool.get('giscedata.facturacio.lectures.energia')
        pot_read_obj = self.pool.get('giscedata.facturacio.lectures.potencia')
        ene_read_obj.unlink(cursor, uid, old_lect['lectures_energia_ids'])
        pot_read_obj.unlink(cursor, uid, old_lect['lectures_potencia_ids'])
        hi_ha_autoconsum = fact_xml.te_autoconsum()
        tanto_alzado = polissa_vals['contract_type'] == '09'

        # Si el F1 te autoconsum calcularem els ajustos necessaris perque les lectures donguin el que s'ha de facturar
        lectures_amb_ajustos = {}
        if hi_ha_autoconsum or tanto_alzado:
            lectures_amb_ajustos = {}
            motiu_ajust = '98' if hi_ha_autoconsum else '99'
            for compt_xml, lectures in fact_xml.get_lectures_activa_entrant(ajust_balancejat=True, motiu_ajust=motiu_ajust).iteritems():
                for l in lectures:
                    lid = compt_xml.numero_serie + "_" + l.unique_name()
                    lectures_amb_ajustos[lid] = l.ajuste or None

        for compt_xml in fact_xml.get_comptadors():
            compt_id = compt_obj.search_with_contract(
                cursor, uid, compt_xml.numero_serie, polissa_id
            )[0]

            readed_periods = {}
            totalitzador_periods = []

            for lect_xml in compt_xml.get_lectures_energia():
                ctx = context.copy()
                if not lect_xml.ometre:
                    # We will process totalitzador periods at the end to be able to
                    # check if we need them
                    if lect_xml.codigo_periodo in ['10', '20', '80']:
                        totalitzador_periods.append(lect_xml)
                        continue

                    # Store readed periods to make the check about totalitzadors
                    if not readed_periods.get(lect_xml.magnitud):
                        readed_periods[lect_xml.magnitud] = []
                    readed_periods[lect_xml.magnitud].append(lect_xml.periode)

                    if hi_ha_autoconsum or tanto_alzado:
                        lid = compt_xml.numero_serie + "_" + lect_xml.unique_name()
                        if lectures_amb_ajustos.get(lid):
                            ctx['ajust_balancejat'] = lectures_amb_ajustos.get(lid)

                    # We process active and reactive readings
                    l1, l2 = self.proces_lect_xml_energia(
                        cursor, uid, lect_xml, compt_id, ocsum_tarifa, en_baixa,
                        factura_id, tariff_name, rectificadora, overwrite_atr,
                        context=ctx
                    )
                    energy_read_ids.extend(l1)
                    energy_fact_read_ids.extend(l2)

            # Now we process the totalitzador periods. If we don't have any
            # other period we load them
            for lect_xml in totalitzador_periods:
                ctx = context.copy()
                period = lect_xml.periode
                magnitud = lect_xml.magnitud
                if (not readed_periods.get(magnitud)
                    or readed_periods[magnitud] and period not in readed_periods[magnitud]):

                    if hi_ha_autoconsum or tanto_alzado:
                        lid = compt_xml.numero_serie + "_" + lect_xml.unique_name()
                        if lectures_amb_ajustos.get(lid):
                            ctx['ajust_balancejat'] = lectures_amb_ajustos.get(lid)

                    l1, l2 = self.proces_lect_xml_energia(
                        cursor, uid, lect_xml, compt_id, ocsum_tarifa, en_baixa,
                        factura_id, tariff_name, rectificadora, overwrite_atr,
                        context=ctx
                    )
                    energy_read_ids.extend(l1)
                    energy_fact_read_ids.extend(l2)

            power_reads = {}
            # power_reads has the format:
            # power_reads[reading_date][reading_period] = lect_vals
            # Where:
            #   reading_date is the date in format string as %Y-%m-%d
            #   reading_period is the period as PX, where X is the period number
            #   lect_vals is the vals of the readings of maximeter (M) and
            #               power excess (EP) joined. If we only have one of the
            #               types the other value will be 0
            readed_periods = {}
            totalitzador_periods = []
            for lect_xml in compt_xml.get_lectures(tipus=['M']):
                if not lect_xml.ometre:
                    if lect_xml.codigo_periodo in ['10', '20']:
                        totalitzador_periods.append(lect_xml)
                        continue

                    if not readed_periods.get(lect_xml.magnitud):
                        readed_periods[lect_xml.magnitud] = []
                    readed_periods[lect_xml.magnitud].append(lect_xml.periode)

                    lect_vals = lect_helper.vals_lectura(
                        cursor, uid, lect_xml, 'final', compt_id,
                        tarifa=ocsum_tarifa, en_baixa=en_baixa,
                        context=context
                    )

                    date = lect_xml.lectura_hasta.fecha
                    period = lect_xml.periode
                    power_reads.setdefault(date, {})
                    power_reads[date][period] = lect_vals

            # Porces totalitzadors for 'M'
            for lect_xml in totalitzador_periods:
                period = lect_xml.periode
                magnitud = lect_xml.magnitud
                if (not readed_periods.get(magnitud)
                    or readed_periods[magnitud] and period not in readed_periods[magnitud]):
                    lect_vals = lect_helper.vals_lectura(
                        cursor, uid, lect_xml, 'final', compt_id,
                        tarifa=ocsum_tarifa, en_baixa=en_baixa,
                        context=context
                    )

                    date = lect_xml.lectura_hasta.fecha
                    period = lect_xml.periode
                    power_reads.setdefault(date, {})
                    power_reads[date][period] = lect_vals

            readed_periods = {}
            totalitzador_periods = []
            for lect_xml in compt_xml.get_lectures(tipus=['EP']):
                if not lect_xml.ometre:
                    if lect_xml.codigo_periodo in ['10', '20']:
                        totalitzador_periods.append(lect_xml)
                        continue

                    if not readed_periods.get(lect_xml.magnitud):
                        readed_periods[lect_xml.magnitud] = []
                    readed_periods[lect_xml.magnitud].append(lect_xml.periode)

                    lect_vals = lect_helper.vals_lectura(
                        cursor, uid, lect_xml, 'final', compt_id,
                        tarifa=ocsum_tarifa, en_baixa=en_baixa,
                        context=context
                    )

                    date = lect_xml.lectura_hasta.fecha
                    period = lect_xml.periode
                    power_reads.setdefault(date, {})
                    if period not in power_reads[date]:
                        # If we don't have a power reading for this date and
                        # period we set the excess one with maximeter type
                        lect_vals['tipus'] = 'M'
                        power_reads[date][period] = lect_vals
                    else:
                        # If we do have a power reading for this date and period
                        #  we update the exces value
                        power_reads[date][period]['exces'] = lect_vals['exces']

            # Porces totalitzadors for 'EP'
            for lect_xml in totalitzador_periods:
                period = lect_xml.periode
                magnitud = lect_xml.magnitud
                if (not readed_periods.get(magnitud)
                    or readed_periods[magnitud] and period not in readed_periods[magnitud]):
                    lect_vals = lect_helper.vals_lectura(
                        cursor, uid, lect_xml, 'final', compt_id,
                        tarifa=ocsum_tarifa, en_baixa=en_baixa,
                        context=context
                    )

                    date = lect_xml.lectura_hasta.fecha
                    period = lect_xml.periode
                    power_reads.setdefault(date, {})
                    if period not in power_reads[date]:
                        # If we don't have a power reading for this date and
                        # period we set the excess one with maximeter type
                        lect_vals['tipus'] = 'M'
                        power_reads[date][period] = lect_vals
                    else:
                        # If we do have a power reading for this date and period
                        #  we update the exces value
                        power_reads[date][period]['exces'] = lect_vals['exces']

            for reads_by_period in power_reads.values():
                for period, power_read in reads_by_period.items():
                    lect_id = lect_helper.import_reading(
                        cursor, uid, power_read,
                        rectificadora=rectificadora,
                        overwrite_atr=overwrite_atr, context=context
                    )
                    power_read_ids.append(lect_id)

                    fact_lect_id = self.import_invoicing_reading_power(
                        cursor, uid, factura_id, period, power_read,
                        context=context
                    )
                    power_fact_read_ids.append(fact_lect_id)

        # Processar lectures de generacio
        generacio_lects = self.importar_lectures_generacio_a_pool(cursor, uid, line_id, fact_xml, polissa_id, context=context)
        energy_read_ids.extend(generacio_lects)
        energy_read_ids = list(set(energy_read_ids))

        # Write new lect.
        fact_obj.write(
            cursor, uid, factura_id, {
                'lectures_energia_ids': [(6, 0, energy_fact_read_ids)],
                'lectures_potencia_ids': [(6, 0, power_fact_read_ids)],
            }
        )

        # Mark the import line with "lectures_processades"
        line_obj.write(
            cursor, uid, line_id, {"lectures_processades": True}, context
        )

        return energy_read_ids, power_read_ids

    def proces_lect_xml_energia(self, cursor, uid, lect_xml, compt_id,
                                ocsum_tarifa, en_baixa, factura_id, tariff_name,
                                rectificadora, overwrite_atr, context=None):
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        energy_read_ids = []
        energy_fact_read_ids = []
        lect_vals = {'inicial': {}, 'final': {}}
        for lect_point in ['inicial', 'final']:
            ctx = context.copy()
            ctx['lect_point'] = lect_point
            # We process both initial and final readings
            lect_vals[lect_point] = lect_helper.vals_lectura(
                cursor, uid, lect_xml, lect_point, compt_id,
                tarifa=ocsum_tarifa, en_baixa=en_baixa,
                context=context
            )
            if lect_vals[lect_point]['magnitud'] != 'AS':  # Les lectures autoconsum les crearem al pool mes endevant
                lect_id = lect_helper.import_reading(
                    cursor, uid, lect_vals[lect_point],
                    rectificadora=rectificadora,
                    overwrite_atr=overwrite_atr, context=ctx
                )
                energy_read_ids.append(lect_id)
        fact_lect_id = self.import_invoicing_reading_energy(
            cursor, uid, factura_id, tariff_name, lect_xml.periode,
            lect_vals, context=context
        )
        energy_fact_read_ids.append(fact_lect_id)
        return energy_read_ids, energy_fact_read_ids

    def import_readings(self, cursor, uid, line_id, f1_xml, context=None):
        """
        Here we will create the readings in the database and make the necessary
        links to the other rows
        :param cursor: Database cursor
        :param uid: User id
        :param line_id: The line_id that we are currently working with
        :param f1_xml: F1 file that we are currently importing, from the
        switching library
        :param context: OpenERP's context
        :return: The ids of the energy and power readings that we have created
        """
        if context is None:
            context = {}

        energy_readings = []
        power_readings = []

        for fact_xml in f1_xml.facturas_atr:
            if fact_xml.datos_factura.tipo_factura not in ['C']:
                # If it's a type C invoice we shouldn't have readings to import
                it_ener_read, it_pow_read = self.import_readings_from_fact(
                    cursor, uid, line_id, fact_xml, context
                )
                energy_readings += it_ener_read
                power_readings += it_pow_read

        return energy_readings, power_readings

    def create_invoicing_extra(self, cursor, uid, fact_id, fact_name, ori_lines,
                               context=None):
        """Crear una nova entrada a giscedata.facturacio.extra especificant els
                   periodes entre els quals hi ha linies de refacturació"""
        extra_obj = self.pool.get('giscedata.facturacio.extra')

        extra_ids = []
        values = self.get_values_in_ext(
            cursor, uid, fact_id, fact_name, ori_lines, context
        )
        for vals in values:
            extra_ids.append(
                extra_obj.create(cursor, uid, vals, context=context)
            )
        return extra_ids

    def get_extra_line_journals(self, cursor, uid, context=None):
        if context is None:
            context = {}
        journal_obj = self.pool.get('account.journal')
        journal_ids = journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')], limit=1
        )
        return journal_ids

    def get_values_in_ext(self, cursor, uid, fact_id, fact_name, ori_lines,
                               context=None):
        if not context:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        linia_obj = self.pool.get('giscedata.facturacio.factura.linia')
        product_obj = self.pool.get('product.product')

        journal_ids = self.get_extra_line_journals(cursor, uid)
        if not journal_ids:
            raise message.except_f1('JournalNotFound', 'ENERGIA')

        fact_vals = fact_obj.read(
            cursor, uid, fact_id, ['tipo_rectificadora', 'polissa_id']
        )
        ctx = self.actualitzar_idioma_ctx(
            cursor, uid, fact_id, 'out_invoice', context
        )

        read_params = [
            'product_id',
            'name',
            'price_unit_multi',
            'data_desde',
            'data_fins',
            'quantity',
            'uos_id',
            'invoice_line_tax_id',
        ]

        # El text següent ha de traduïr-se segons l'idioma del partner i no
        # de l'usuari de l'erp
        extra_ids = []
        values = []
        for ori_line in ori_lines:
            linia = linia_obj.read(
                cursor, uid, ori_line['line_id'], read_params, context=ctx
            )

            product_id = None

            if 'force_product' in ori_line:
                product_id = ori_line['force_product']
            elif linia.get('product_id', False):
                product_id = linia['product_id'][0]
            product_fields_read = [
                'description', 'description_sale', 'code', 'taxes_id'
            ]

            product_vals = product_obj.read(
                cursor, uid, product_id, product_fields_read, context=ctx
            )
            extra_name = (
                product_vals['description_sale'] or product_vals['description']
            )

            if codi_refact(product_vals['code']):
                consum, total = parse_totals_refact(linia['name'])
                extra_name %= consum, total

            if ori_line.get('force_name', False):
                extra_name = ori_line['force_name']

            price_unit_multi = linia['price_unit_multi']
            sign_value = SIGN[fact_vals['tipo_rectificadora']]
            price_unit = price_unit_multi * sign_value

            if ori_line.get('force_price', False):
                price_unit = ori_line['force_price']

            notes = self.crear_notes_extra(
                cursor, uid, fact_id, fact_name, context
            )

            vals = {
                'polissa_id': fact_vals['polissa_id'][0],
                'date_from': linia['data_desde'],
                'date_to': linia['data_fins'],
                'date_line_from': linia['data_desde'],
                'date_line_to': linia['data_fins'],
                'name': extra_name,
                'product_id': product_id,
                'quantity': linia['quantity'],
                'uos_id': linia['uos_id'][0],
                'price_unit': price_unit,
                'term': 1,
                'journal_ids': [(6, 0, journal_ids)],
                'notes': notes,
            }

            # Agafem els impostos segons el producte
            vals['tax_ids'] = [(6, 0, product_vals['taxes_id'])]
            values.append(vals)

        return values

    def crear_linia_refacturacio(self, cursor, uid, factura_id, ref,
                                                                context=None):
        """Crear una línia de factura amb el contingut de la refacturacio
           o regularització ref"""
        if not context:
            context = {}
        context.update({'type_invoice': 'in_invoice'})
        _fact = self.pool.get('giscedata.facturacio.facturador')
        product_obj = self.pool.get('product.product')
        lid = 0
        info = ''
        refact = True
        if ref.__class__.__name__ == 'Refacturacio':
            nom_prod = nom_refact
        else:
            nom_prod = nom_reg_refact
            refact = False
        try:
            p_id = product_obj.search(cursor, uid,
                      [('default_code', '=', nom_prod(ref.tipus))])[0]
        except IndexError:
            info = _("No s'ha pogut identificar el producte %s") % \
                                                 nom_prod(ref.tipus)
            return lid, info
        # escriure les línies segons la llengua del partner
        ctx = self.actualitzar_idioma_ctx(cursor, uid, factura_id,
                                                        'in_invoice', context)
        prod = product_obj.browse(cursor, uid, p_id, context=ctx)
        vals = {}
        _name = prod.description
        if refact:
            _name = _name % (ref.consum_total, ref.import_total)
        vals.update({'force_price': ref.import_parcial,
                     'product_id': p_id,
                     'uos_id': prod.uom_id.id,
                     'quantity': 1,
                     'multi': 1,
                     'tipus': 'altres',
                     'name': _name,
                     'data_desde': ref.data_inici,
                     'data_fins': ref.data_final
                    })
        lid = _fact.crear_linia(cursor, uid, factura_id, vals, context)
        return lid, info

    def crear_linies_factura(self, cursor, uid, fact, factura_id, obj,
                                                            context=None):
        if not context:
            context = {}
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        f1config_obj = self.pool.get('giscedata.facturacio.switching.config')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')
        factura = factura_obj.browse(cursor, uid, factura_id)
        emissor = factura.partner_id.ref
        f1config = f1config_obj.get_config(cursor, uid, emissor)
        tipus = obj.tipus
        total = obj.total
        data = obj.get_data()
        info = ""
        if not data:
            return None, ''
        lid = []
        _fact = self.pool.get('giscedata.facturacio.facturador')
        if tipus == 'altres':
            for concepte in data:
                # Si l'import total es 0 no creem la línia en la factura
                if not concepte.total:
                    continue
                xml_id = 'concepte_%s' % concepte.codi
                prod = imd_obj._get_obj(cursor, uid,
                                        'giscedata_facturacio_comer', xml_id,
                                        context)
                # Si total té valor, hem de facturar el concepte, per tant,
                # quantity = 1.0
                quantity = concepte.quantitat or (concepte.total and 1.0)
                vals = {
                    'quantity': quantity,
                    'multi': 1,
                    'product_id': prod.id,
                    'name': prod.name,
                    'tipus': tipus,
                    'data_desde': factura.date_invoice,
                    'data_fins': factura.date_invoice
                }
                if not concepte.preu:
                    vals['force_price'] = concepte.total
                elif concepte.preu and concepte.total:
                    vals['force_price'] = concepte.preu
                    vals['quantity'] = float(concepte.total / concepte.preu)
                else:
                    vals['force_price'] = concepte.preu
                # Forcem a que sigui negatiu
                # Si el producte és negatiu i el valor no és negatiu el forcem
                # a negatiu
                if prod.list_price < 0 < vals['force_price']:
                    vals['force_price'] *= -1
                linia = _fact.crear_linies_altres(cursor, uid, factura_id,
                                                  vals, context)
                self.aplicar_descompte(cursor, uid, linia)
                lid.append(linia)
                vals = {
                    'data_desde': factura.date_invoice,
                    'data_fins': factura.date_invoice,
                    'name': 'Total %s' % prod.name,
                    'force_price': concepte.total,
                    'uos_id': prod.uom_id.id,
                    'quantity': 1,
                    'tipus': 'subtotal_xml',
                    'product_id': prod.id,
                    'multi': 1
                }
                if prod.list_price < 0 < vals['force_price']:
                    vals['force_price'] *= -1
                vals.update(
                    self.get_account_and_tax(cursor, uid, linia, context)
                )
                lid.append(
                    _fact.crear_linia(cursor, uid, factura_id, vals, context)
                )
            return lid, info
        elif tipus == 'lloguer':
            vals = {'data_desde': fact.data_inici,
                    'data_fins': fact.data_final,
                    'force_price': data.quantitat,
                    'uos_id': False,
                    'quantity': 1,
                    'multi': 1,
                    'tipus': tipus,
                    'name': _('Lloguer equip de mesura')}
            if data.quantitat != 0:
                lid.append(_fact.crear_linies_lloguer(cursor, uid, factura_id,
                                                                vals, context))
            return lid, info
        elif tipus in ('refacturacions', 'regularitzacions'):
            i_txt = []
            for ref in data:
                r_lid, r_inf = self.crear_linia_refacturacio(cursor, uid,
                                                      factura_id, ref, context)
                lid.append(r_lid)
                if r_inf and not r_inf in i_txt:
                    i_txt.append(r_inf)
            return lid, i_txt
        factor = None
        if tipus == 'energia':
            _tipus = 'te'
            multi = 1
        elif tipus == 'reactiva':
            _tipus = 'tr'
            multi = 1
        elif tipus == 'potencia':
            _tipus = 'tp'
            uom_pot = uom_obj.browse(cursor, uid, f1config['preu_potencia_t'])
            unitat = uom_pot.name.split('/')[1] if '/' in uom_pot.name else 'mes'
        elif tipus == 'exces_potencia':
            _tipus = 'ep'
            factor = tarifes.Tarifa61.factors_k
        search_params = [('codi_ocsum', '=', fact.codi_tarifa)]
        tarifa_pool = self.pool.get('giscedata.polissa.tarifa')
        tarifa_id = tarifa_pool.search(cursor, uid, search_params)
        productes = tarifa_pool.get_periodes_producte(cursor, uid, tarifa_id,
                                                        _tipus, context)
        subtotal = 0
        ctx = context.copy()
        for periode in data:
            name = periode.name
            if not productes.get(name, False):
                if not periode.quantitat or not periode.preu_unitat:
                    continue
                tarifa = defs.INFO_TARIFA[fact.codi_tarifa]['name']
                raise osv.except_osv('Error',
                        _("No es pot identificar el periode %s del tipus %s "
                          "en el llistat de periodes de la tarifa %s") % (
                            name, tipus[0].upper(), tarifa))
            if tipus == 'exces_potencia':
                if not periode.quantitat:
                    continue
                multi = factor[name]
            if tipus == 'potencia':
                multi = tarifes.repartir_potencia({periode.data_inici: 1},
                          periode.data_inici, periode.data_final, base=unitat)\
                                                            [periode.data_inici]
            vals = {'data_desde': periode.data_inici or fact.data_inici,
                    'data_fins': periode.data_final or fact.data_final,
                    'product_id': productes.get(name, False),
                    'quantity': periode.quantitat,
                    'multi': multi,
                    'tipus': tipus,
                    'name': name}
            if tipus != 'exces_potencia':
                vals.update({'force_price': periode.preu_unitat})
            # Factors de conversió!
            if tipus == 'energia':
                vals['quantity'] = uom_obj._compute_qty(cursor, uid,
                    f1config['uom_energia_f'],
                    periode.quantitat,
                    f1config['uom_energia_t']
                )
                vals['force_price'] = uom_obj._compute_price(cursor, uid,
                    f1config['preu_energia_f'],
                    periode.preu_unitat,
                    f1config['preu_energia_t']
                )
                vals['uos_id'] = f1config['preu_energia_t']
            if tipus == 'potencia':
                vals['quantity'] = uom_obj._compute_qty(cursor, uid,
                    f1config['uom_potencia_f'],
                    periode.quantitat,
                    f1config['uom_potencia_t']
                )
                vals['force_price'] = uom_obj._compute_price(cursor, uid,
                    f1config['preu_potencia_f'],
                    periode.preu_unitat,
                    f1config['preu_potencia_t']
                )
                vals['uos_id'] = f1config['preu_potencia_t']
            lid.append(_fact.crear_linia(cursor, uid, factura_id, vals, ctx))
            subtotal += self.get_subtotal_linia(cursor, uid, lid[-1])
        lid = list(set(lid))
        if round(subtotal, 2) != round(total, 2):
            self.escriure_resum_linies(cursor, uid, obj, factura_id, lid)
            info = _(u"Error subtotal línia factura en %s. "
                     u"XML: %s IMP: %s") % (obj.tipus, subtotal, total)
        self.aplicar_descompte(cursor, uid, lid)
        # Creem la línia fictícia amb el total
        # Agafem els valors dels impostos i comptes comptables de l'última
        # línia creada
        vals = {
            'data_desde': fact.data_inici,
            'data_fins': fact.data_final,
            'name': 'Total %s' % obj.tipus,
            'force_price': obj.total,
            'quantity': 1,
            'tipus': 'subtotal_xml',
            'product_id': False,
        }
        vals.update(self.get_account_and_tax(cursor, uid, lid[-1], ctx))
        lid.append(_fact.crear_linia(cursor, uid, factura_id, vals, ctx))
        return lid, info

    def create_invoice_atr(self, cursor, uid, file_id, fact,
                           data_limit_pagament, partner_id, payment_order_id,
                           context=None):
        """
        Creates an invoice for atributes
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the F1 file
        :param payment_order_id: Id of the payment order depending on the sender 
        and the date
        :param context: OpenERP context 
        :return: Returns the id of the created invoice or None if we didn't
        create it
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        vals = self.create_invoice_general(
            cursor, uid, fact, file_id, data_limit_pagament, partner_id,
            'CENERGIA', payment_order_id, context=context
        )

        start_date = datetime.strptime(
            fact.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        data_inici = start_date.strftime('%Y-%m-%d')

        vals.update({
            'data_inici': data_inici,
            'data_final': fact.datos_factura.fecha_hasta_factura,
            'date_boe': fact.datos_factura.fecha_boe,
        })

        fact_data = datetime.strptime(
            fact.datos_factura.fecha_factura, '%Y-%m-%d'
        )
        invoice_already_exists = fact_obj.get_in_invoice_by_origin(
            cursor, uid, fact.datos_factura.codigo_fiscal_factura, partner_id,
            year=str(fact_data.year), context=context
        )
        # If the invoice already exists we don't actually want to create it
        # (we have done everything up to now to create the necessary errors)
        if invoice_already_exists:
            return None

        fact_id = fact_obj.create(cursor, uid, vals, context=context)

        return fact_id

    def create_invoice_others(self, cursor, uid, file_id, fact,
                              data_limit_pagament, partner_id, payment_order_id,
                              context=None):
        """
        Creates an invoice for other
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the F1 file
        :param payment_order_id: Id of the payment order depending on the sender 
        and the date
        :param context: OpenERP context 
        :return: Returns the id of the created invoice or None if we didn't
        create it
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        vals = self.create_invoice_general(
            cursor, uid, fact, file_id, data_limit_pagament, partner_id, 'CCONCEPTES',
            payment_order_id, context=context
        )

        datos_factura = fact.datos_factura
        vals.update({
            'date_boe': datos_factura.fecha_boe or datos_factura.fecha_factura
        })

        fact_data = datetime.strptime(
            datos_factura.fecha_factura, '%Y-%m-%d'
        )
        invoice_already_exists = fact_obj.get_in_invoice_by_origin(
            cursor, uid, datos_factura.codigo_fiscal_factura, partner_id,
            year=str(fact_data.year), context=context
        )
        # If the invoice already exists we don't actually want to create it
        # (we have done everything up to now to create the necessary errors)
        if invoice_already_exists:
            return None

        fact_id = fact_obj.create(cursor, uid, vals, context=context)

        return fact_id

    def create_invoice_lines(self, cursor, uid, file_id, fact_id, fact_xml,
                             context=None):
        """
        Creates the lines of an ATR invoice
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the ids of the created invoice lines
        """
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        lines_by_type = fact_xml.get_linies_factura_by_type()
        line_ids = []
        for line_type, lines in lines_by_type.items():
            line_id = self.create_invoice_line_by_type(
                cursor, uid, line_type, file_id, fact_xml, fact_id, lines,
                context
            )
            if line_id:
                line_ids += line_id
        # Si la factura no té base imponible és que cap línia ha de tenir
        # impostos
        if fact_xml.sin_base_imponible():
            for linia in fact_obj.browse(cursor, uid, fact_id).linia_ids:
                linia.write({'invoice_line_tax_id': [(6, False, [])]})
        fact_obj.button_reset_taxes(cursor, uid, [fact_id], context)

        val_lectures = self.get_total_factura(cursor, uid, fact_id, context)
        rounded_invoice_total = round(
            fact_xml.datos_factura.importe_total_factura, 2
        )
        rounded_readings_total = round(val_lectures, 2)
        if abs(rounded_invoice_total) != abs(rounded_readings_total):
            error_values = {
                'total_importacio': rounded_readings_total,
                'total_xml': rounded_invoice_total
            }
            error_obj.create_error_from_code(
                cursor, uid, file_id, '2', '002', error_values,
                context=context
            )

        return line_ids

    def create_invoice_line_by_type(self, cursor, uid, tipus, file_id, fact_xml,
                                    fact_id, line_xml, context=None):
        """
        Creates a line of an invoice depending on the type of line it is
        :param tipus: Tipus de linia a crear
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        if tipus == 'altres':
            return self.create_invoice_line_others(
                cursor, uid, fact_xml, fact_id, line_xml, context=context
            )
        elif tipus == 'lloguer':
            return self.create_invoice_line_rent(
                cursor, uid, fact_xml, fact_id, line_xml, context=context
            )
        # elif tipus == 'refacturacions':
        #     return self.create_invoice_line_refacturation(
        #         cursor, uid, file_id, fact_id, line_xml, context=context
        #     )
        # elif tipus == 'regularitzacions':
        #     return self.create_invoice_line_regularitzation(
        #         cursor, uid, file_id, fact_id, line_xml, context=context
        #     )
        elif tipus == 'energia':
            return self.create_invoice_line_energy(
                cursor, uid, file_id, fact_xml, fact_id, line_xml,
                context=context
            )
        elif tipus == 'reactiva':
            return self.create_invoice_line_reactive(
                cursor, uid, file_id, fact_xml, fact_id, line_xml,
                context=context
            )
        elif tipus == 'potencia':
            return self.create_invoice_line_power(
                cursor, uid, file_id, fact_xml, fact_id, line_xml,
                context=context
            )
        elif tipus == 'exces_potencia':
            return self.create_invoice_line_power_excess(
                cursor, uid, file_id, fact_xml, fact_id, line_xml,
                context=context
            )

    def get_line_others_vals(
            self, cursor, uid, concepte, factura_vals, tipus, context=None):
        imd_obj = self.pool.get('ir.model.data')
        xml_id = 'concepte_%s' % concepte.concepto_repercutible
        prod = imd_obj._get_obj(
            cursor, uid, 'giscedata_facturacio_comer', xml_id, context
        )
        # Si total té valor, hem de facturar el concepte, per tant,
        # quantity = 1.0
        quantity = concepte.unidades or (concepte.importe and 1.0)
        vals = {
            'quantity': quantity,
            'multi': 1,
            'product_id': prod.id,
            'name': prod.name,
            'tipus': tipus,
            'data_desde': factura_vals['date_invoice'],
            'data_fins': factura_vals['date_invoice']
        }
        if not concepte.precio_unidad:
            vals['force_price'] = concepte.importe
        elif concepte.precio_unidad and concepte.importe:
            vals['force_price'] = concepte.precio_unidad
            vals['quantity'] = float(
                concepte.importe / concepte.precio_unidad
            )
        else:
            vals['force_price'] = concepte.precio_unidad
        # Forcem a que sigui negatiu
        if prod.list_price < 0 < vals['force_price']:
            vals['force_price'] *= -1
        # Quantity must be positive
        vals['quantity'] = abs(vals['quantity'])

        return vals


    def create_invoice_line_others(self, cursor, uid, fact_xml, fact_id,
                                   line_xml, context=None):
        """
        Creates a line of an invoice with type others
        :param line_xml: The switching's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        imd_obj = self.pool.get('ir.model.data')

        factura_vals = factura_obj.read(
            cursor, uid, fact_id, ['partner_id', 'date_invoice']
        )
        tipus = 'altres'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = []

        ctx = context.copy()
        for concepte in data:
            xml_id = 'concepte_%s' % concepte.concepto_repercutible
            prod = imd_obj._get_obj(
                cursor, uid, 'giscedata_facturacio_comer', xml_id, context
            )
            vals = self.get_line_others_vals(
                cursor, uid, concepte, factura_vals, tipus, context=context)

            ctx['pricelist_base_price'] = vals['force_price']
            linia_id = facturador_obj.crear_linies_altres(
                cursor, uid, fact_id, vals, context=ctx
            )
            self.aplicar_descompte(cursor, uid, linia_id)
            line_ids.append(linia_id)
            vals = {
                'data_desde': factura_vals['date_invoice'],
                'data_fins': factura_vals['date_invoice'],
                'name': 'Total %s' % prod.name,
                'force_price': concepte.importe,
                'uos_id': prod.uom_id.id,
                'quantity': 1,
                'tipus': 'subtotal_xml',
                'product_id': prod.id,
                'multi': 1
            }
            if prod.list_price < 0 < vals['force_price']:
                vals['force_price'] *= -1
            vals.update(
                self.get_account_and_tax(cursor, uid, linia_id, context)
            )
            line_ids.append(
                facturador_obj.crear_linia(
                    cursor, uid, fact_id, vals, context=ctx
                )
            )
        return line_ids

    def create_invoice_line_rent(self, cursor, uid, fact_xml, fact_id, line_xml,
                                 context=None):
        """
        Creates a line of an invoice with type rent
        :param line_xml: The switching's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        start_date = datetime.strptime(
            fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        date = start_date.strftime('%Y-%m-%d')
        if isleap(int(date.split('-')[0])):
            days_in_year = 366.0
            # uom_alq_dia = imd_obj.get_object_reference(
            #     cursor, uid, 'giscedata_facturacio',
            #     'uom_aql_elec_dia_traspas'
            # )[1]
        else:
            days_in_year = 365.0
            # uom_alq_dia = imd_obj.get_object_reference(
            #     cursor, uid, 'giscedata_facturacio', 'uom_aql_elec_dia'
            # )[1]
        uom_alq_mes = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'uom_aql_elec_mes'
        )[1]

        tipus = 'lloguer'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = []

        for llog in data:
            # TODO: Crear subtotal per lloguers

            price = llog.precio_dia  # €/dia
            price *= days_in_year    # €/year
            price /= 12              # €/month

            # Quantity in months
            quantity = llog.numero_dias / days_in_year * 12

            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            vals = {
                'data_desde': data_inici,
                'data_fins': fact_xml.datos_factura.fecha_hasta_factura,
                # TODO: Should use uom but units are incorrect
                'force_price': price,
                'uos_id': uom_alq_mes,
                'quantity': quantity,
                'multi': 1,
                'tipus': tipus,
                'name': _('Lloguer equip de mesura')
            }

            if llog.precio_dia != 0 and llog.numero_dias != 0:
                line_ids.append(
                    facturador_obj.crear_linies_lloguer(
                        cursor, uid, fact_id, vals, context
                    )
                )

        if line_ids:
            line_ids.append(
                self.create_subtotal_line(
                    cursor, uid, 'ren', fact_xml, line_xml, fact_id, line_ids
                )
            )

        return line_ids

    def create_invoice_line_refacturation(self, cursor, uid, file_id, fact_id,
                                          line_xml, context=None):
        """
        Creates a line of an invoice with type refacturation
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        return self.create_invoice_line_refact_or_regul(
            cursor, uid, file_id, fact_id, line_xml, context=context
        )

    def create_invoice_line_regularitzation(self, cursor, uid, file_id, fact_id,
                                            line_xml, context=None):
        """
        Creates a line of an invoice with type regularitzation
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        return self.create_invoice_line_refact_or_regul(
            cursor, uid, file_id, fact_id, line_xml, context=context
        )

    def create_invoice_line_refact_or_regul(self, cursor, uid, file_id, fact_id,
                                            line_xml, context=None):
        """
        Creates a line of an invoice with type refacturation or regularitzation
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        product_obj = self.pool.get('product.product')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')

        context.update({'type_invoice': 'in_invoice'})

        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = []

        for ref in data:
            if ref.__class__.__name__ == 'Refacturacio':
                nom_prod = nom_refact
                is_refact = True
            else:
                nom_prod = nom_reg_refact
                is_refact = False

            try:
                p_id = product_obj.search(
                    cursor, uid, [('default_code', '=', nom_prod(ref.tipus))]
                )[0]

                # escriure les línies segons la llengua del partner
                ctx = self.actualitzar_idioma_ctx(
                    cursor, uid, fact_id, 'in_invoice', context
                )
                prod_vals = product_obj.read(
                    cursor, uid, p_id, ['description', 'uom_id'], context=ctx
                )
                vals = {}
                line_name = prod_vals['description']
                if is_refact:
                    line_name %= ref.consum_total, ref.import_total
                vals.update({
                    'force_price': ref.import_parcial,
                    'product_id': p_id,
                    'uos_id': prod_vals['uom_id'][0],
                    'quantity': 1,
                    'multi': 1,
                    'tipus': 'altres',
                    'name': line_name,
                    'data_desde': ref.data_inici,
                    'data_fins': ref.data_final
                })
                line_ids.append(
                    facturador_obj.crear_linia(
                        cursor, uid, fact_id, vals, context
                    )
                )
            except IndexError:
                error_obj.create_error_from_code(
                    cursor, uid, file_id, '2', '009',
                    {'producte': nom_prod(ref.tipus)}, context=context
                )

        return line_ids

    def create_invoice_line_energy(self, cursor, uid, file_id, fact_xml,
                                   fact_id, line_xml, context=None):
        """
        Creates a line of an invoice with type energy
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        uom_kwatt_hour = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'uom_eneg_elec'
        )[1]

        tipus = 'energia'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = set()
        multi = 1

        tarifa_atr = fact_xml.datos_factura.tarifa_atr_fact

        is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, tarifa_atr, is_low
        )
        productes = tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'te', context
        )
        ctx = context.copy()
        for periode in data:
            name = periode.nombre
            if productes.get(name, False):
                vals = self._get_vals_for_energy_or_power_line(
                    periode, productes, fact_xml
                )
                vals.update({
                    'multi': multi,
                    'tipus': tipus,
                    'force_price': periode.precio
                })
                # No s'aplica cap factor de conversio perque tot esta en kWh
                vals['quantity'] = periode.cantidad
                vals['force_price'] = periode.precio
                vals['uos_id'] = uom_kwatt_hour

                line_id = facturador_obj.crear_linia(
                    cursor, uid, fact_id, vals, ctx
                )
                line_ids.add(line_id)
            else:
                if periode.cantidad and periode.precio:
                    tarifa = defs.INFO_TARIFA[tarifa_atr]['name']
                    raise message.except_f1(
                        'UnidentifiablePeriode',
                        'Cannot identify period in tariff\'s periode list',
                        {
                            'nom_periode': name,
                            'tipus_periode': tipus[0].upper(),
                            'nom_tarifa': tarifa
                        }
                    )

        line_ids = list(set(line_ids))
        self.write_lines_summary(
            cursor, uid, file_id, tipus, line_xml, fact_id, line_ids
        )

        line_ids.append(
            self.create_subtotal_line(
                cursor, uid, 'ene', fact_xml, line_xml, fact_id, line_ids
            )
        )

        return line_ids

    def create_invoice_line_reactive(self, cursor, uid, file_id, fact_xml,
                                     fact_id, line_xml, context=None):
        """
        Creates a line of an invoice with type reactive
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        tipus = 'reactiva'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = set()
        multi = 1

        tarifa_atr = fact_xml.datos_factura.tarifa_atr_fact

        is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, tarifa_atr, is_low
        )
        productes = tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'tr', context
        )
        total_of_correct_periods = self.calc_reactive_total(cursor, uid, line_xml, tarifa_id)
        ctx = context.copy()
        for periode in data:
            name = periode.nombre
            if productes.get(name, False):
                vals = self._get_vals_for_energy_or_power_line(
                    periode, productes, fact_xml
                )
                vals.update({
                    'multi': multi,
                    'tipus': tipus,
                    'force_price': periode.precio
                })

                line_id = facturador_obj.crear_linia(
                    cursor, uid, fact_id, vals, ctx
                )
                line_ids.add(line_id)
            else:
                if periode.cantidad and periode.precio:
                    # When we import reactive and the total is the sum of
                    # correct periods and current period is not expected we
                    # ignore it. This is to avoid errors when we recive reactive
                    # periods that we shouldn't recive but they are only
                    # "informative" so they are "correct"
                    if abs(total_of_correct_periods - line_xml['total']) > 0.1:
                        tarifa = defs.INFO_TARIFA[tarifa_atr]['name']
                        raise message.except_f1(
                            'UnidentifiablePeriode',
                            'Cannot identify period in tariff\'s periode list',
                            {
                                'nom_periode': name,
                                'tipus_periode': tipus[0].upper(),
                                'nom_tarifa': tarifa
                            }
                        )

        line_ids = list(set(line_ids))
        self.write_lines_summary(
            cursor, uid, file_id, tipus, line_xml, fact_id, line_ids
        )

        line_ids.append(
            self.create_subtotal_line(
                cursor, uid, 'rea', fact_xml, line_xml, fact_id, line_ids
            )
        )

        return line_ids

    def calc_reactive_total(self, cursor, uid, line_xml, tarifa_id, context=None):
        """
        :param line_xml:switching's version of the reactive line we are creating
        :return: total price (with 2 decimals) summing all correct reactive periods of tarifa_id
        """
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        prods = tarifa_obj.get_periodes_producte(cursor, uid, tarifa_id, 'tr', context)
        total = 0
        for periode in line_xml.get('lines', None):
            if prods.get(periode.nombre, False):
                total += periode.cantidad * periode.precio
        return round(total, 2)

    def create_invoice_line_power(self, cursor, uid, file_id, fact_xml, fact_id,
                                  line_xml, context=None):
        """
        Creates a line of an invoice with type power
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        f1config_obj = self.pool.get('giscedata.facturacio.switching.config')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        partner_obj = self.pool.get('res.partner')
        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        factura_vals = factura_obj.read(
            cursor, uid, fact_id, ['partner_id', 'date_invoice']
        )
        emissor = partner_obj.read(
            cursor, uid, factura_vals['partner_id'][0], ['ref']
        )['ref']
        try:
            f1config = f1config_obj.get_config(cursor, uid, emissor)
        except osv.except_osv as e:
            raise message.except_f1(
                'SwitchingConfigNotFound',
                'Switching configuration not found for {0}'.format(emissor),
                {'emissor': emissor}
            )
        tipus = 'potencia'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = set()

        uom_watt = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_wat'
        )[1]
        uom_kwatt = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'uom_pot_elec'
        )[1]

        start_date = datetime.strptime(
            fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        date = start_date.strftime('%Y-%m-%d')
        if isleap(int(date.split('-')[0])):
            uom_watt_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_wat_dia_traspas'
            )[1]
            uom_kwatt_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia_traspas'
            )[1]
        else:
            uom_watt_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_wat_dia'
            )[1]
            uom_kwatt_dia = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_dia'
            )[1]
        unitat = 'dia'

        is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, fact_xml.datos_factura.tarifa_atr_fact, is_low
        )
        productes = tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'tp', context
        )
        ctx = context.copy()
        for periode in data:
            name = periode.nombre
            if productes.get(name, False):
                pstart_date = datetime.strptime(periode.fecha_desde, '%Y-%m-%d')
                pstart_date += timedelta(days=1)
                pdate = start_date.strftime('%Y-%m-%d')
                multi = tarifes.repartir_potencia(
                    {pdate: 1}, pdate, periode.fecha_hasta, base=unitat
                )[pdate]
                vals = self._get_vals_for_energy_or_power_line(
                    periode, productes, fact_xml
                )
                vals.update({
                    'multi': multi,
                    'tipus': tipus,
                    'force_price': periode.precio
                })

                vals['quantity'] = uom_obj._compute_qty(
                    cursor, uid, uom_watt, periode.cantidad, uom_kwatt
                )
                vals['force_price'] = uom_obj._compute_price(
                    cursor, uid, uom_watt_dia, periode.precio, uom_kwatt_dia
                )
                vals['uos_id'] = uom_kwatt_dia

                line_id = facturador_obj.crear_linia(
                    cursor, uid, fact_id, vals, ctx
                )
                line_ids.add(line_id)
            else:
                if periode.cantidad and periode.precio:
                    tarifa_atr = fact_xml.datos_factura.tarifa_atr_fact
                    tarifa = defs.INFO_TARIFA[tarifa_atr]['name']
                    raise message.except_f1(
                        'UnidentifiablePeriode',
                        'Cannot identify period in tariff\'s periode list',
                        {
                            'nom_periode': name,
                            'tipus_periode': tipus[0].upper(),
                            'nom_tarifa': tarifa
                        }
                    )

        line_ids = list(set(line_ids))
        self.write_lines_summary(
            cursor, uid, file_id, tipus, line_xml, fact_id, line_ids
        )

        line_ids.append(
            self.create_subtotal_line(
                cursor, uid, 'pow', fact_xml, line_xml, fact_id, line_ids
            )
        )

        return line_ids

    def create_invoice_line_power_excess(self, cursor, uid, file_id, fact_xml,
                                         fact_id, line_xml, context=None):
        """
        Creates a line of an invoice with type power excess
        :param line_xml: The switching's version of the line we are creating
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created invoice line
        """
        if context is None:
            context = {}

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        tipus = 'exces_potencia'
        data = line_xml.get('lines', None)
        if not data:
            return None
        line_ids = set()
        factor = tarifes.Tarifa61.factors_k

        tarifa_atr = fact_xml.datos_factura.tarifa_atr_fact
        is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, tarifa_atr, is_low
        )
        productes = tarifa_obj.get_periodes_producte(
            cursor, uid, tarifa_id, 'ep', context
        )
        ctx = context.copy()
        for periode in data:
            name = periode.name
            if productes.get(name, False):
                if periode.valor_exceso_potencia:
                    multi = factor[name]

                    start_date = datetime.strptime(
                        fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
                    )
                    start_date += timedelta(days=1)

                    desde_factura = start_date.strftime('%Y-%m-%d')
                    hasta_factura = fact_xml.datos_factura.fecha_hasta_factura

                    vals = {
                        'data_desde': desde_factura,
                        'data_fins': hasta_factura,
                        'product_id': productes.get(name, False),
                        'quantity': 1,
                        'force_price': periode.valor_exceso_potencia,
                        'name': name,
                        'multi': multi,
                        'tipus': tipus,
                    }

                    line_id = facturador_obj.crear_linia(
                        cursor, uid, fact_id, vals, ctx
                    )
                    line_ids.add(line_id)
            else:
                if periode.valor_exceso_potencia:
                    tarifa = defs.INFO_TARIFA[tarifa_atr]['name']
                    raise message.except_f1(
                        'UnidentifiablePeriode',
                        'Cannot identify period in tariff\'s periode list',
                        {
                            'nom_periode': name,
                            'tipus_periode': tipus[0].upper(),
                            'nom_tarifa': tarifa
                        }
                    )

        line_ids = list(set(line_ids))
        if line_ids:
            self.write_lines_summary(
                cursor, uid, file_id, tipus, line_xml, fact_id, line_ids
            )

            line_ids.append(
                self.create_subtotal_line(
                    cursor, uid, 'exc', fact_xml, line_xml, fact_id, line_ids
                )
            )

        return line_ids

    @staticmethod
    def _get_vals_for_energy_or_power_line(periode_xml, productes, fact_xml):
        name = periode_xml.nombre

        start_date = datetime.strptime(
            fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        desde_factura = start_date.strftime('%Y-%m-%d')
        hasta_factura = fact_xml.datos_factura.fecha_hasta_factura

        return {
            'data_desde': periode_xml.fecha_desde or desde_factura,
            'data_fins': periode_xml.fecha_hasta or hasta_factura,
            'product_id': productes.get(name, False),
            'quantity': periode_xml.cantidad,
            'name': name
        }

    def create_subtotal_line(self, cursor, uid, subtotal_type, fact_xml,
                             line_xml, fact_id, line_ids, context=None):
        """
        Creates a line of an invoice with type subtotal_type (taking into
        account the subtotal_type passed) and the value of the total the
        line_xml.  Also sets all the line_ids descounts to 100.
        :param line_ids: Ids of the lines we are creating the subtotal from
        :param subtotal_type: Type of subtotal value we are creating
        :param line_xml: The switching's version of the line we are creating
        :param cursor: Database cursor
        :param uid: User's id
        :param fact_id: The id of the invoice where we are creating the lines
        (giscedata.facturacio.factura)
        :param fact_xml: The switching's version of the xml's invoice of the F1
        from which we are creating the lines
        :param context: OpenERP context
        :return: Returns the id of the created subtotal invoice line
        """
        if context is None:
            context = {}

        tipus_dict = {
            'ene': 'energia',
            'rea': 'reactiva',
            'pow': 'potencia',
            'exc': 'exceso_potencia',
            'ren': 'lloguer',
        }

        facturador_obj = self.pool.get('giscedata.facturacio.facturador')

        self.aplicar_descompte(cursor, uid, line_ids)
        # Creem la línia fictícia amb el total
        # Agafem els valors dels impostos i comptes comptables de l'última
        # línia creada
        start_date = datetime.strptime(
            fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        data_desde = start_date.strftime('%Y-%m-%d')

        vals = {
            'data_desde': data_desde,
            'data_fins': fact_xml.datos_factura.fecha_hasta_factura,
            'name': 'Total {0}'.format(tipus_dict.get(subtotal_type, '')),
            'force_price': line_xml['total'],
            'quantity': 1,
            'tipus': 'subtotal_xml_{0}'.format(subtotal_type),
            'product_id': False,
        }
        vals.update(
            self.get_account_and_tax(cursor, uid, line_ids[-1], context)
        )
        subtotal_id = facturador_obj.crear_linia(
            cursor, uid, fact_id, vals, context
        )

        return subtotal_id

    def create_invoice_general(self, cursor, uid, fact, file_id,
                               data_limit_pagament, partner_id, journal_code,
                               payment_order_id, context=None):
        """
        This function is used to create the values common in both a ATR and an
         Others invoice
        :param file_id: The id of the giscedata.facturacio.importacio.linia
        :param journal_code: Code of the account.journal we are using
        :param cursor: Database cursor
        :param uid: User's id
        :param fact: Switching's version of the xml invoice
        :param data_limit_pagament: Data limit de pagament especificada a l'xml
        :param partner_id: Id of the sender of the F1 file
        :param payment_order_id: Id of the payment order depending on the sender
        and the date
        :param context: OpenERP context
        :return: Returns the the values of the general invoice.
        """
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        journal_obj = self.pool.get('account.journal')

        line_vals = line_obj.read(
            cursor, uid, file_id,
            ['cups_id', 'cups_text', 'importacio_id', 'ignore_reference']
        )
        cups_id = line_vals['cups_id'][0]
        datos_factura = fact.datos_factura
        if hasattr(datos_factura, 'fecha_desde_factura'):
            start_date = datetime.strptime(
                datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
        else:
            data_inici = datos_factura.fecha_factura

        data_final = getattr(
            datos_factura, 'fecha_hasta_factura', datos_factura.fecha_factura
        )

        try:
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, cups_id, data_inici, data_final
            )
        except osv.except_osv as e:
            if fact.datos_factura.motivo_facturacion != '04':
                raise message.except_f1(
                    'ContractNotFound', e.message,
                    {
                        'cups': line_vals['cups_text'],
                        'start_date': data_inici,
                        'end_date': data_final
                    }
                )
            else:  # fact.datos_factura.motivo_facturacion == '04':
                # We will try to read polissa 1 day after given data_inici
                # because for some reason some distris send this kind of
                # invoices (04 = contratación) 1 day before activation
                data_inici = datetime.strptime(data_inici,
                                               "%Y-%m-%d") + timedelta(days=1)
                data_inici = data_inici.strftime("%Y-%m-%d")
                try:
                    polissa_id = lect_help_obj.find_polissa_with_cups_id(
                        cursor, uid, cups_id, data_inici, data_final
                    )
                except osv.except_osv as e:

                    cups_obj = self.pool.get('giscedata.cups.ps')
                    polissa_id = cups_obj.find_most_recent_polissa(
                        cursor, uid, cups_id, data_inici, context=context
                    )[cups_id]
                    if not polissa_id:
                        raise message.except_f1(
                            'ContractNotFound', e.message,
                            {
                                'cups': line_vals['cups_text'],
                                'start_date': data_inici,
                                'end_date': data_final
                            }
                        )

        if fact.datos_factura.tipo_factura not in REFUND_RECTIFICATIVE_TYPES:
            invoice_type = 'in_invoice'
        else:
            invoice_type = 'in_refund'
        context['type_invoice'] = invoice_type

        if fact.datos_factura.tipo_factura != 'N':
            journal_code += '.%s' % fact.datos_factura.tipo_factura
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', journal_code)], limit=1
        )
        if not journal_id:
            raise message.except_f1('JournalNotFound', journal_code)

        vals = fact_obj.onchange_polissa(
            cursor, uid, [], polissa_id, invoice_type, context=context
        )['value']
        vals.update(fact.get_create_invoice_params())

        # In contratacio (04) invoices we may have changed the date_invoice to
        # add 1 day, so we update it
        if fact.datos_factura.motivo_facturacion == '04':
            vals.update({'date_invoice': data_inici})

        vals['saldo'] = fact.datos_factura.saldo_factura
        # If tipo_rectificadora is in RECTIFICATIVE_FROM_F1_TO_DB we should
        # change it. If it's not there we should use the original one
        vals['tipo_rectificadora'] = RECTIFICATIVE_FROM_F1_TO_DB.get(
            vals['tipo_rectificadora'],
            vals['tipo_rectificadora']
        )

        # We must use the tarif from the F1, not the tarif from the contract
        if hasattr(fact.datos_factura, 'tarifa_atr_fact'):
            tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
            tarifa_atr = fact.datos_factura.tarifa_atr_fact
            is_low = fact.datos_factura.marca_medida_con_perdidas == 'S'
            tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
                cursor, uid, tarifa_atr, is_low
            )
            if tarifa_id:
                vals.update({'tarifa_acces_id': tarifa_id})

        # TODO: Add "origin_date_invoice"
        vals.update({
            'polissa_id': polissa_id,
            'type': invoice_type,
            'date_due': data_limit_pagament,
            'importacio_id': line_vals['importacio_id'][0],
            'partner_id': partner_id,
            'journal_id': journal_id[0],
        })
        ref = None
        try:
            ref = self.get_factura_referencia(
                cursor, uid, fact, journal_code, partner_id, context
            )

            vals.update({
                'ref': ref,
            })
        except osv.except_osv:
            fact_ref = fact.datos_factura.codigo_factura_rectificada_anulada
            if line_vals['ignore_reference']:
                # If ignore_reference is set to true we allow to continue the
                # import but we give a message saying we didn't find the
                # reference invoice
                err_vals = {'fact_ref': fact_ref}
                error_obj.create_error_from_code(
                    cursor, uid, file_id, '2', '012', err_vals, context=context
                )
            else:
                # If ignore_reference is not set to true we raise an error
                # (this is what we do by default)
                raise message.except_f1('ReferenceInvoiceNotFound', fact_ref)

        if payment_order_id:
            vals.update({'payment_order_id': payment_order_id})

        return vals

    def create_simulated_invoice(self, cursor, uid, fact_xml, tariff_name,
                                 polissa_id, compt_id, context=None):
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        pricelist_version_obj = self.pool.get('product.pricelist.version')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        uom_obj = self.pool.get('product.uom')

        if context is None:
            context = {}

        watts = uom_obj.search(cursor, uid, [('name', '=', 'W')])[0]
        k_watts = uom_obj.search(cursor, uid, [('name', '=', 'kW')])[0]

        tariff_class = TARIFES[tariff_name]

        # We initialitze all the values
        consums = {'activa': {}, 'reactiva': {}}
        maximetres = {}
        exc_pot = {}
        # Our start date (inclusive)
        start_date = fact_xml.datos_factura.fecha_desde_factura
        # Exclussive start date
        excl_start_date = datetime.strptime(start_date, '%Y-%m-%d')
        excl_start_date += timedelta(days=1)
        end_date = fact_xml.datos_factura.fecha_hasta_factura
        power_invoicing = fact_xml.get_info_facturacio_potencia()
        terminos_potencia = fact_xml.potencia.terminos_potencia

        # We get the latest terminos_potencia to get the last powers
        # TODO: Change this to handle different contracted powers in an invoice
        contr_powers = terminos_potencia[-1].get_contracted_periods_by_period()
        # We change the contracted powers from W to kW
        for period, cont_pow in contr_powers.items():
            contr_powers[period] = uom_obj._compute_qty(
                cursor, uid, watts, cont_pow, k_watts
            )
        versions = facturador_obj.versions_de_preus_ids(
            cursor, uid, polissa_id, excl_start_date.strftime('%Y-%m-%d'),
            end_date, context=context
        )

        powers, total_pow_fact = fact_xml.get_info_potencia()
        actives, total_act_fact = fact_xml.get_info_activa()
        reactives = []
        excesses = []
        for compt_xml in fact_xml.get_comptadors():
            reactives += compt_xml.get_lectures_reactiva()
            excesses += compt_xml.get_lectures(tipus='EP')

        for period in powers:
            maximetres[period.nombre] = lect_helper.set_uom_lectura(
                cursor, uid, period.potencia_max_demandada, 'M', compt_id,
                context={'from_f1': True}
            )

        for period in actives:
            consums['activa'][period.nombre] = lect_helper.set_uom_lectura(
                cursor, uid, period.cantidad, 'A', compt_id,
                context={'from_f1': True}
            )

        for reading in reactives:
            end_reading = reading.lectura_hasta.lectura
            start_reading = reading.lectura_desde.lectura
            total = end_reading - start_reading
            if reading.ajuste:
                total += reading.ajuste.ajuste_por_integrador
            consums['reactiva'][reading.periode] = lect_helper.set_uom_lectura(
                cursor, uid, total, 'R', compt_id, context={'from_f1': True}
            )

        for reading in excesses:
            end_reading = reading.lectura_hasta.lectura
            start_reading = reading.lectura_desde.lectura
            total = end_reading - start_reading
            if reading.ajuste:
                total += reading.ajuste.ajuste_por_integrador
            exc_pot[reading.periode] = lect_helper.set_uom_lectura(
                cursor, uid, total, 'EP', compt_id,
                context={'from_f1': True}
            )

        tariff = tariff_class(
            consums,  # Active and reactive consumes
            maximetres,  # Maximeter values
            excl_start_date, end_date,  # Start and end date
            excesos_potencia=exc_pot,
            facturacio_potencia=power_invoicing,
            # Power invoicing: maximetre('max') / ICP('icp')
            data_inici_periode=excl_start_date,
            data_final_periode=end_date,
            potencies_contractades=contr_powers,
            versions=versions,
            base='dia'
        )

        tariff.update_dates_consums(
            excl_start_date,
            end_date
        )

        if tariff_name == "2.0DHA":
            ctx = context.copy()
            # get_cofs_20DHA necessita la data a la clau 'date' per
            # trobar bé el "preu" (coeficients en aquest cas)
            cofs_20dha = {}
            for date_v in versions:
                pricelist_id = pricelist_version_obj.read(
                    cursor, uid, versions[date_v], ['pricelist_id']
                )['pricelist_id'][0]
                ctx.update(
                    {
                        'date': date_v,
                        'listprice_id': pricelist_id,
                    }
                )
                cofs_20dha[date_v] = tarifa_obj.get_cofs_20DHA(
                    cursor, uid, None, context=ctx
                )
            tariff.conf['cof_20DHA'] = cofs_20dha

        tariff.factura_energia()
        tariff.factura_reactiva()
        tariff.factura_potencia()

        return tariff

    def check_simulated_invoice_is_correct(self, cursor, uid, versions_ids,
                                           sim_inv, fact_xml, context=None):
        if context is None:
            context = {}

        totals_sim, taxes_sim = self.calculate_energy_totals_sim_inv(
            cursor, uid, fact_xml, sim_inv, versions_ids, context=context
        )
        conc_totals, conc_taxes = self.calculate_concept_totals_sim_inv(
            cursor, uid, fact_xml, context=context
        )
        for conc_tax in conc_taxes:
            if conc_tax in taxes_sim:
                taxes_sim[conc_tax]['base'] += conc_taxes[conc_tax]['base']
                taxes_sim[conc_tax]['imp'] += conc_taxes[conc_tax]['imp']
            else:
                taxes_sim[conc_tax] = conc_taxes[conc_tax]

        totals_taxes = {
            'iva': {}
        }
        for original_tax in taxes_sim:
            for tax_type in totals_taxes:
                if tax_type in original_tax.lower():
                    percentage = taxes_sim[original_tax]['perc'] * 100
                    totals_taxes['iva'][percentage] = taxes_sim[original_tax]

        errors = self.check_totals_are_correct(
            fact_xml, totals_sim, conc_totals, totals_taxes
        )

        return errors

    def calculate_energy_totals_sim_inv(self, cursor, uid, fact_xml, sim_inv,
                                        versions_ids, context=None):
        if context is None:
            context = {}

        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        pricelist_obj = self.pool.get('product.pricelist')
        pricelist_version_obj = self.pool.get('product.pricelist.version')
        product_obj = self.pool.get('product.product')
        tax_obj = self.pool.get('account.tax')

        tarifa_atr = fact_xml.datos_factura.tarifa_atr_fact
        is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, tarifa_atr, is_low
        )

        trad = {
            'potencia': 'tp',
            'activa': 'te',
            'reactiva': 'tr',
            'exces_potencia': 'ep',
        }

        # TODO: Implement checks divided by period. If you uncomment the lines
        # that modify totals_by_period_sim it will calculate the periods
        # totals_by_period_sim = {
        #     'potencia': {},
        #     'activa': {},
        #     'reactiva': {},
        #     'exces_potencia': {},
        # }
        totals_sim = {
            'potencia': 0,
            'activa': 0,
            'reactiva': 0,
            'exces_potencia': 0,
        }
        taxes_sim = {}

        for tipus, values_by_date in sim_inv.termes.items():
            if tipus in totals_sim and values_by_date:
                for date in values_by_date:
                    ctx = context.copy()
                    ctx['date'] = date
                    pricelist_version_id = versions_ids[date]
                    pricelist_id = pricelist_version_obj.read(
                        cursor, uid, pricelist_version_id, ['pricelist_id']
                    )['pricelist_id'][0]
                    periodes_producte = tarifa_obj.get_periodes_producte(
                        cursor, uid, tarifa_id, trad[tipus], context=ctx
                    )
                    for period, values in values_by_date[date].items():
                        # If periodes_producte doesn't have period we won't
                        # include it because we can't calulate the value
                        if periodes_producte.get(period, False):
                            # Calculate subtotal by period
                            if tipus == 'reactiva':
                                price = tarifes.calc_cosfi_price(
                                    values['cosfi']
                                )
                            else:
                                price = pricelist_obj.price_get(
                                    cursor, uid, [pricelist_id],
                                    periodes_producte[period], 1,
                                    context=ctx
                                )[pricelist_id]

                            quantity = values['quantity']
                            subtotal = values['multi'] * quantity * price

                            if tipus == 'potencia':
                                # If we are invoicing power we need to divide
                                # the result by the days of this year
                                if isleap(int(date.split('-')[0])):
                                    subtotal /= 366
                                else:
                                    subtotal /= 365

                            # totals_by_period_sim[tipus].setdefault(period, 0)
                            # totals_by_period_sim[tipus][period] += subtotal
                            totals_sim[tipus] += subtotal

                            # Calculate taxes by period
                            product_vals = product_obj.read(
                                cursor, uid, periodes_producte[period],
                                ['supplier_taxes_id']
                            )
                            tax_vals = tax_obj.read(
                                cursor, uid, product_vals['supplier_taxes_id'],
                                ['name', 'amount']
                            )
                            for tax in tax_vals:
                                taxes_sim.setdefault(
                                    tax['name'], {
                                        'base': 0,
                                        'perc': tax['amount'],
                                        'imp': 0
                                    }
                                )
                                imp = subtotal * tax['amount']
                                taxes_sim[tax['name']]['base'] += subtotal
                                taxes_sim[tax['name']]['imp'] += imp

        return totals_sim, taxes_sim

    def calculate_concept_totals_sim_inv(self, cursor, uid, fact_xml,
                                         context=None):
        if context is None:
            context = {}

        product_obj = self.pool.get('product.product')
        tax_obj = self.pool.get('account.tax')

        conc_totals = {}
        conc_taxes = {}

        concepts_xml, total_conc_xml = fact_xml.get_info_conceptes_repercutibles()
        for conc_xml in concepts_xml:
            code = 'CON{}'.format(conc_xml.concepto_repercutible)

            # We save the total of the concept
            conc_totals.setdefault(code, {'occ': 0, 'total': 0})
            conc_totals[code]['occ'] += 1
            conc_totals[code]['total'] += conc_xml.importe

            conc_id = product_obj.search(
                cursor, uid, [('default_code', '=', code)]
            )
            if conc_id:
                conc_id = conc_id[0]
                conc_vals = product_obj.read(
                    cursor, uid, conc_id, ['supplier_taxes_id']
                )
                tax_vals = tax_obj.read(
                    cursor, uid, conc_vals['supplier_taxes_id'],
                    ['name', 'amount']
                )
                for tax in tax_vals:
                    conc_taxes.setdefault(
                        tax['name'], {
                            'base': 0,
                            'perc': tax['amount'],
                            'imp': 0
                        }
                    )
                    imp = conc_xml.importe * tax['amount']
                    conc_taxes[tax['name']]['base'] += conc_xml.importe
                    conc_taxes[tax['name']]['imp'] += imp

        llogers_xml, total_llog = fact_xml.get_info_lloguer()
        for llog in llogers_xml:
            # FIXME: Here we use the default ALQ01 to calculate the taxes. All
            # ALQXX have the same taxes, so this is tecnicly correct, but it may
            # not actually be a ALQ01. If we have a way to know which one it is
            # we could change it here
            code = 'ALQ01'

            llog_price = llog.precio_dia * llog.numero_dias

            conc_totals.setdefault(
                code, {'occ': 0, 'total': 0}
            )
            conc_totals[code]['occ'] += 1
            conc_totals[code]['total'] += llog_price

            alq_id = product_obj.search(
                cursor, uid, [('default_code', '=', code)]
            )[0]

            conc_vals = product_obj.read(
                cursor, uid, alq_id, ['supplier_taxes_id']
            )
            tax_vals = tax_obj.read(
                cursor, uid, conc_vals['supplier_taxes_id'],
                ['name', 'amount']
            )
            for tax in tax_vals:
                conc_taxes.setdefault(
                    tax['name'], {
                        'base': 0,
                        'perc': tax['amount'],
                        'imp': 0
                    }
                )
                imp = llog_price * tax['amount']
                conc_taxes[tax['name']]['base'] += llog_price
                conc_taxes[tax['name']]['imp'] += imp
        return conc_totals, conc_taxes

    @staticmethod
    def check_totals_are_correct(fact_xml, totals_sim, conc_totals,
                                 total_taxes):
        errors = []

        powers, total_pow_fact = fact_xml.get_info_potencia()
        actives, total_act_fact = fact_xml.get_info_activa()
        reactives, total_react_fact = fact_xml.get_info_reactiva()
        excesses, total_exc_fact = fact_xml.get_info_exces()
        ivas = fact_xml.ivas
        total_fact = fact_xml.datos_factura.importe_total_factura

        if round(total_pow_fact, 2) != round(totals_sim['potencia'], 2):
            total_xml = round(total_pow_fact, 2)
            total_sim = round(totals_sim['potencia'], 2)
            errors.append(
                {
                    'text': _(
                        u'[3028-A] El subtotal de potencia en l\'XML ({0}€) '
                        u'divergeix amb el subtotal calculat ({1}€). La '
                        u'divergencia és de {2}€.'
                    ).format(
                        total_xml, total_sim, total_xml - total_sim
                    )
                }
            )
        if round(total_act_fact, 2) != round(totals_sim['activa'], 2):
            total_xml = round(total_act_fact, 2)
            total_sim = round(totals_sim['activa'], 2)
            errors.append(
                {
                    'text': _(
                        u'[3028-B] El subtotal d\'activa en l\'XML ({0}€) '
                        u'divergeix amb el subtotal calculat ({1}€). La '
                        u'divergencia és de {2}€.'
                    ).format(
                        total_xml, total_sim, total_xml - total_sim
                    )
                }
            )
        if round(total_react_fact, 2) != round(totals_sim['reactiva'], 2):
            total_xml = round(total_react_fact, 2)
            total_sim = round(totals_sim['reactiva'], 2)
            errors.append(
                {
                    'text': _(
                        u'[3028-C] El subtotal de reactiva en l\'XML ({0}€) '
                        u'divergeix amb el subtotal calculat ({1}€). La '
                        u'divergencia és de {2}€.'
                    ).format(
                        total_xml, total_sim, total_xml - total_sim
                    )
                }
            )
        if round(total_exc_fact, 2) != round(totals_sim['exces_potencia'], 2):
            total_xml = round(total_exc_fact, 2)
            total_sim = round(totals_sim['exces_potencia'], 2)
            errors.append(
                {
                    'text': _(
                        u'[3028-D] El subtotal d\'exces potencia en l\'XML '
                        u'({0}€) divergeix amb el subtotal calculat ({1}€). La '
                        u'divergencia és de {2}€.'
                    ).format(
                        total_xml, total_sim, total_xml - total_sim
                    )
                }
            )
        for iva in ivas:
            if iva.porcentaje in total_taxes['iva']:
                base = total_taxes['iva'][iva.porcentaje]['base']
                imp = total_taxes['iva'][iva.porcentaje]['imp']
                if round(iva.base, 2) != round(base, 2):
                    rounded_xml = round(iva.base, 2)
                    rounded_sim = round(base, 2)
                    errors.append(
                        {
                            'text': _(
                                u'[3028-E] La base de l\'IVA {0}% en l\'XML '
                                u'({1}€) divergeix amb el calculat ({2}€). La '
                                u'divergencia és de {3}€'
                            ).format(
                                iva.porcentaje, rounded_xml, rounded_sim,
                                rounded_xml - rounded_sim
                            )
                        }
                    )
                if round(iva.importe, 2) != round(imp, 2):
                    rounded_xml = round(iva.importe, 2)
                    rounded_sim = round(imp, 2)
                    errors.append(
                        {
                            'text': _(
                                u'[3028-F] El total de l\'IVA {0}% en l\'XML '
                                u'({1}€) divergeix amb el calculat ({2}€). La '
                                u'divergencia és de {3}€'
                            ).format(
                                iva.porcentaje, rounded_xml, rounded_sim,
                                rounded_xml - rounded_sim
                            )
                        }
                    )
                total_taxes['iva'][iva.porcentaje]['checked'] = True
            elif iva.importe:
                errors.append(
                    {
                        'text': _(
                            u'[3028-G] Segons les nostres simulacions cap '
                            u'concepte té un IVA {0}%, però aquest apareix al '
                            u'fitxer XML'
                        ).format(iva.porcentaje)
                    }
                )
        for tax_type in total_taxes:
            for tax_amount, tax_values in total_taxes[tax_type].items():
                if not tax_values.get('checked', False) and tax_values['imp']:
                    errors.append(
                        {
                            'text': _(
                                u'[3028-H] Segons les nostres simulacions '
                                u'hauriem de tenir un {0} {1}%, però aquest no '
                                u'apareix al fitxer XML'
                            ).format(tax_type, tax_amount)
                        }
                    )
        inv_total_sim = sum(totals_sim.values())
        for conc_values in conc_totals.values():
            inv_total_sim += conc_values['total']
        for values_by_type in total_taxes.values():
            for tax_values in values_by_type.values():
                inv_total_sim += tax_values['imp']
        if round(inv_total_sim, 2) != round(total_fact, 2):
            total_xml = round(total_fact, 2)
            total_sim = round(inv_total_sim, 2)
            errors.append(
                {
                    'text': _(
                        u'[3028-I] El total en l\'XML ({0}€) divergeix amb el '
                        u'total calculat ({1}€). La divergencia és de {2}€.'
                    ).format(
                        total_xml, total_sim, total_xml - total_sim
                    )
                }
            )
        return errors

    def simulate_invoice(self, cursor, uid, fact_xml, tariff_name, polissa_id,
                         compt_xml, context=None):
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')

        if context is None:
            context = {}

        context = context.copy()
        if fact_xml.datos_factura.tipo_factura in ('N', 'R'):
            context['type_invoice'] = 'in_invoice'
        else:
            context['type_invoice'] = 'in_refund'

        compt_id = compt_obj.search_with_contract(
            cursor, uid, compt_xml.numero_serie, polissa_id
        )
        if not compt_id:
            return []
        compt_id = compt_id[0]

        tariff = self.create_simulated_invoice(
            cursor, uid, fact_xml, tariff_name, polissa_id, compt_id,
            context=context
        )

        start_date = datetime.strptime(
            fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        start_date = start_date.strftime('%Y-%m-%d')
        end_date = fact_xml.datos_factura.fecha_hasta_factura
        versions_ids = facturador_obj.versions_de_preus_ids(
            cursor, uid, polissa_id, start_date, end_date,
            context=context
        )

        return self.check_simulated_invoice_is_correct(
            cursor, uid, versions_ids, tariff, fact_xml, context=context
        )

    def crear_factura(self, cursor, uid, polissa_id, fact, data, partner_id,
                      val1, remesa_id, context=None):
        """Crear la factura a partir de les línies de factura"""
        context.update({'type_invoice': 'in_invoice'})
        _fact = self.pool.get('giscedata.facturacio.factura')
        journal_obj = self.pool.get('account.journal')
        if fact.tipus_rectificadora in \
                        RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']:
            invoice_type = 'in_invoice'
        else:
            invoice_type = 'in_refund'
        if 'date' in context:
            # Evitem prendre els valors de la modificació contractual
            # en l'onchange_polissa que segueix
            del context['date']
        vals = _fact.onchange_polissa(cursor, uid, [], polissa_id,
                                         invoice_type, context)['value']
        # Busquem el diari que toqui
        if fact.tipus == 'FacturaATR':
            journal_code = 'CENERGIA'
        else:
            journal_code = 'CCONCEPTES'
        if fact.tipus_rectificadora != 'N':
            journal_code += '.%s' % fact.tipus_rectificadora
        jid = journal_obj.search(cursor, uid, [('code', '=', journal_code)],
                                 limit=1)
        if not jid:
            raise osv.except_osv('Error',
                                 "No s'ha trobat el diari corresponent")
        vals.update({'polissa_id': polissa_id,
                     'tipo_rectificadora': fact.tipus_rectificadora,
                     'tipo_factura': fact.tipus_factura,
                     'type': invoice_type,
                     'date_invoice': fact.data_factura,
                     'data_due': data,
                     'check_total': abs(fact.import_total_factura),
                     'origin': fact.numero_factura,
                     'origin_date_invoice': fact.data_factura,
                     'reference': fact.numero_factura,
                     'importacio_id': context.get('importacio_id', False),
                     'partner_id': partner_id,
                     'journal_id': jid[0],
                     'ref': self.get_factura_referencia(cursor, uid, fact,
                                                        journal_code, partner_id, context)
                     })
        if remesa_id:
            vals.update({'payment_order_id': remesa_id})
        if fact.tipus == 'FacturaATR':
            vals.update({
                'data_inici': fact.data_inici,
                'data_final': fact.data_final,
                'tipo_facturacion': fact.tipus_facturacio,
                'date_boe': fact.data_BOE,
            })
        elif fact.tipus == 'OtrasFacturas':
            vals.update({
                'date_boe': fact.data_factura
            })
        f_id = _fact.create(cursor, uid, vals, context=context)
        linies = fact.get_linies_factura()
        lids = []
        infos = []
        for obj in linies:
            lid, info = self.crear_linies_factura(cursor, uid, fact, f_id, obj,
                                                  context)
            if lid:
                lids += lid
            infos += [info]
        # Si la factura no té base imponible és que cap línia ha de tenir
        # impostos
        if fact.import_net == 0:
            for linia in _fact.browse(cursor, uid, f_id).linia_ids:
                linia.write({'invoice_line_tax_id': [(6, False, [])]})
        _fact.button_reset_taxes(cursor, uid, [f_id], context)
        val = self.get_total_factura(cursor, uid, f_id, context)
        if val1:
            vals = {'div_totals': val - val1}
            _fact.write(cursor, uid, f_id, vals)
        if round(abs(fact.import_total_factura), 2) != round(abs(val), 2):
            infos += [_("El total de la factura importada no coincideix amb "
                        "l'xml. Total XML: %0.2f, total factura importada: "
                        "%0.2f") % (round(fact.import_total_factura, 2),
                                    round(val, 2))]
        return f_id, infos

    def facturar_via_lectures(self, cursor, uid, lect_data, compt, fact,
                                                    polissa_id, context=None):
        """Facturar via lectures"""
        q1_obj = self.pool.get('giscedata.lectures.switching.helper')
        lect_id = q1_obj.importar_lectures_compt(cursor, uid, lect_data, context)
        fid = self.facturar_lectures(cursor, uid, polissa_id, fact, context)
        return fid, lect_id

    def importar_factura(self, cursor, uid, lectures, fact, polissa_id, val1,
                         data_pagament, partner_id, remesa_id, context=None):
        """Importar factura"""
        fid, info = self.crear_factura(
            cursor, uid, polissa_id, fact, data_pagament, partner_id, val1,
            remesa_id, context=context
        )
        for lect in lectures:
            self.importar_lectures_facturacio(cursor, uid, lect, fid, fact,
                                              context=context)
        return fid, info

    def import_invoice_atr(self, cursor, uid, file_id, fact,
                           data_limit_pagament, partner_id, payment_order_id,
                           context=None):
        if context is None:
            context = {}

        return self.create_invoice_atr(
            cursor, uid, file_id, fact, data_limit_pagament, partner_id,
            payment_order_id, context=context
        )

    def import_invoice_others(self, cursor, uid, file_id, fact,
                              data_limit_pagament, partner_id, payment_order_id,
                              context=None):
        if context is None:
            context = {}
        return self.create_invoice_others(
            cursor, uid, file_id, fact, data_limit_pagament, partner_id,
            payment_order_id, context=context
        )

    def check_existeix_factura(self, cursor, uid, fact, partner_id):
        """Comprovar si existeix una factura amb el mateix origen i emisor a
           la bd
           (DEPRECATED) TO BE DELETED
           """
        f_pool = self.pool.get('giscedata.facturacio.factura')
        # Comprovant per emisor i origen no cal comprovar el lot d'importació
        ch_val = [('origin', '=', fact.numero_factura),
                  ('invoice_id.partner_id', '=', partner_id)]
        if f_pool.search(cursor, uid, ch_val):
            p_obj = self.pool.get('res.partner')
            partner_ref = p_obj.read(cursor, uid, partner_id, ['ref'])['ref']
            raise osv.except_osv(
                'Error',
                _("Ja existeix una factura amb el mateix origen %s i emisor "
                  "%s") % (fact.numero_factura, partner_ref)
            )

    def check_dades_emisor(self, cursor, uid, emisor):
        """Comprovar que existeix un partner_address i la informació d'aquest
           per l'emisor especificat"""
        partner = self.pool.get('res.partner')
        address = self.pool.get('res.partner.address')
        ids = partner.search(cursor, uid, [('ref', '=', emisor)])
        if len(ids) > 1:
            raise osv.except_osv('Error', _("Existeix més d'un emisor amb el "\
                                            "codi %s") % emisor)
        a_ids = address.search(cursor, uid, [('partner_id', 'in', ids)])
        if not a_ids:
            raise osv.except_osv('Error', _("No existeix cap adreça de "\
                                 "contacte per l'empresa amb id %s") % ids[0])

    def check_refacturacio(self, cursor, uid, fact, f_id, context=None):
        """Afegeix la informació de refacturació de l'xml en cas d'existir-hi,
           a la factura f_id"""
        if not context:
            context = {}
        _fact = self.pool.get('giscedata.facturacio.factura')
        lid = []
        refact = False
        # linies de refacturació
        val, total = fact.get_info_refacturacions()
        for ref in val:
            nlid, info = self.crear_linia_refacturacio(cursor, uid, f_id, ref,
                                                                       context)
            lid.append(nlid)
        if lid:
            refact = True
            _fact.button_reset_taxes(cursor, uid, [f_id], context)
        return lid, refact

    def check_tarifa(self, cursor, uid, fact, polissa_id, context=None):
        """Validem que les tarifa F1 sigui la de la pólissa a la data d'inici
           de la factura"""
        if not context:
            ctxt = {}
        else:
            ctxt = context.copy()
        pol_obj = self.pool.get('giscedata.polissa')

        start_date = datetime.strptime(
            fact.datos_factura.fecha_desde_factura, '%Y-%m-%d'
        )
        start_date += timedelta(days=1)

        data_inici = start_date.strftime('%Y-%m-%d')
        data_final = fact.datos_factura.fecha_hasta_factura
        for data_fact in [data_inici, data_final]:
            ctxt.update({'date': data_fact})
            p_br = pol_obj.browse(cursor, uid, polissa_id, context=ctxt)
            if p_br.tarifa.codi_ocsum != fact.codi_tarifa:
                nom_tarifa_F1 = defs.INFO_TARIFA[fact.codi_tarifa]['name']
                msg = _(u"La tarifa (%s) de la pólissa %s no és la mateixa "
                        u"que la del F1 (%s) el dia %s. Comprovi que la tarifa "
                        u"de la pólissa és la correcte")
                raise osv.except_osv('Error', _(msg) % (p_br.tarifa.name,
                                                        p_br.name,
                                                        nom_tarifa_F1,
                                                        data_fact))
            # Check mode facturacio potència
            fact_pot = fact.info_facturacio_potencia()
            if p_br.facturacio_potencia != fact_pot:
                msg = _(
                    u"El mètode de facturació de potència ({0}) de la "
                    u"pólissa '{1}' no és el mateix que el del F1 ({2}) el "
                    u"dia '{3}'. Comprovi que la facturació de potència "
                    u"de la pólissa és la correcte")
                raise osv.except_osv(
                    'Error',
                    _(msg).format(
                        p_br.facturacio_potencia,
                        p_br.name,
                        fact_pot,
                        data_fact
                    )
                )
            if p_br.tarifa.codi_ocsum == '011':
                # checks 3.1A is LB or not in both sides, contract and F1
                f1_is_lb = fact.ind_mesura_baixa == 'S' and True or False
                contract_is_lb = ('LB' in p_br.tarifa.name)
                if f1_is_lb != contract_is_lb:
                    f1_fare = "3.1A{0}".format(
                        fact.ind_mesura_baixa and " LB" or ""
                    )
                    msg = _(u"Aquesta pòlissa (%s) té la tarifa %s i el F1 "
                            u"indica que hauria de ser %s el dia %s")
                    raise osv.except_osv('Error', _(msg) % (p_br.name,
                                                            p_br.tarifa.name,
                                                            f1_fare,
                                                            data_fact))

    def move_invoices_of_contract(self, cursor, uid, old_contract, new_contract, limit_date, context=None):
        fact_obj = self.pool.get("giscedata.facturacio.factura")
        moved_invoices = []
        # Factures energia
        f_ids = fact_obj.search(cursor, uid, [
            ('cups_id', '=', old_contract.cups.id),
            ('polissa_id', '=', old_contract.id),
            ('type', 'in', ['in_invoice', 'in_refund']),
            ('data_final', '>', limit_date),  # és la data de la ultima lectura
            ('tipo_factura', '=', '01')
        ])
        fact_obj.write(cursor, uid, f_ids, {'polissa_id': new_contract.id})
        moved_invoices.extend(f_ids)

        # Altres Factures
        f_ids = fact_obj.search(cursor, uid, [
            ('cups_id', '=', old_contract.cups.id),
            ('polissa_id', '=', old_contract.id),
            ('type', 'in', ['in_invoice', 'in_refund']),
            ('date_invoice', '>', limit_date),
            ('tipo_factura', '!=', '01')
        ])
        fact_obj.write(cursor, uid, f_ids, {'polissa_id': new_contract.id})

        # Linies Extres dels F1s
        imd_obj = self.pool.get('ir.model.data')
        try:
            devolucio_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_devolucions', "product_devolucions"
            )[1]
        except Exception, e:
            devolucio_id = False
        try:
            impagament_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_impagament', "product_unpaid_management"
            )[1]
        except Exception, e:
            impagament_id = False

        extr_line_obj = self.pool.get('giscedata.facturacio.extra')
        extr_limit_date = datetime.strptime(limit_date, "%Y-%m-%d")
        extr_limit_date = extr_limit_date - timedelta(days=30)
        extr_limit_date = extr_limit_date.strftime("%Y-%m-%d")
        search_filters = [
            ('polissa_id', '=', old_contract.id),
            ('date_to', '>', extr_limit_date),
            ('amount_pending', '!=', 0),
            # Linies Extra de suplements territorials no es mouen
            ('product_id.default_code', 'not in', ('SETU35', 'TEC271')),
            # Linies Extra "costes devolucion" no es mouen
            ('product_id', 'not in', [devolucio_id, impagament_id])
        ]
        # Moureles de contracte i canviar dates
        new_date_from = (
            datetime.strptime(limit_date, "%Y-%m-%d")+timedelta(days=1)
        ).strftime("%Y-%m-%d")
        new_date_to = (
            datetime.strptime(limit_date, "%Y-%m-%d")+timedelta(days=10)
        ).strftime("%Y-%m-%d")
        e_ids = extr_line_obj.search(cursor, uid, search_filters)
        extr_line_obj.write(cursor, uid, e_ids, {
            'polissa_id': new_contract.id,
            'date_from': new_date_from,
            'date_to': new_date_to
        })

        moved_invoices.extend(f_ids)

        return moved_invoices

    def get_non_factured_extra_lines(self, cursor, uid, contract_id, context=None):
        extr_line_obj = self.pool.get('giscedata.facturacio.extra')
        return extr_line_obj.search(cursor, uid, [
            ('polissa_id', '=', contract_id),
            ('amount_pending', '!=', 0)
        ])

GiscedataFacturacioSwitchingHelper()


class GiscedataFacturacioFactura(osv.osv):
    """Classe per importar factures en XML
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def from_xml(self, cursor, uid, xml, fname, emisor_lot, context=None):
        """Importa la factura de l'xml, factura via lectures i en compara els
           resultats.

           La primera variable de retorn, conté el missatge de l'excepció de
           la llibreria de switching en el cas que l'xml sigui invàlid o bé,
           en el cas de ser vàlid,  un diccionari amb els origens de les
           factures per clau i en el valor els resultats de la importació.
           L'estructura del diccionari és la següent:
           - Factura errònia
               {<origen factura>: [False, <msg d'error>, <ref>, <b64>]}
           - Factura vàlida
               {<origen factura>:
                [True, <msg>, <ref>, [<id1>, <id2>], <lect_id>, <e_id>, <b64>]}
               on <id1> és l'id de la factura importada i
                  <id2> l'id de la facturada a partir de les lectures de l'xml.
                  <lect_id> llista de tuples (<tipus>, <l_id>) on l_id és l'id
                            de lectures de comptador i tipus 'A', 'R' o 'M'.
                  <ref> = CodigoDeSolicitud | SecuencialDeSolicitud
                  <e_id> llista d'ids del model giscedata.facturacio.extra
                  <b64> xml en base64
               L'estructura conté <id2> únicament en cas d'existir divergència
               en els totals entre les factures importada i facturada.
        """
        if not context:
            context = {}
        # Activem el flag que venim a través d'un F1
        context['from_f1'] = True
        db = pooler.get_db_only(cursor.dbname)
        fact_pool = self.pool.get('giscedata.facturacio.factura')
        f1_obj = self.pool.get('giscedata.facturacio.switching.helper')
        q1_obj = self.pool.get('giscedata.lectures.switching.helper')
        try:
            f1_xml = F1(xml, 'F1')
            f1_xml.parse_xml()
            xml_b64 = base64.encodestring(xml)
            emisor = f1_xml.get_codi_emisor
            codi_cups_distri = f1_xml.get_codi[2:6]
            ref = '%s%s' % (f1_xml.codi_sollicitud, f1_xml.seq_sollicitud)
        except message.except_f1, e:
            return e.value, ''
        # comprovar que l'emisor es correspon amb el lot
        emisor = get_emisor(emisor, emisor_lot, codi_cups_distri)
        if not emisor:
            return _("L'Empresa emisora no correspon a l'emisor del lot"), ''
        partner_id = q1_obj.get_partner_id(cursor, uid, emisor)
        if not partner_id:
            raise osv.except_osv('Error', _("Empresa emisora amb codi "\
                                            "%s inexistent") % emisor)
        f1_obj.check_dades_emisor(cursor, uid, emisor)
        # Payment order data
        remesa_data = f1_xml.get_remesa()
        factures_xml = f1_xml.get_factures()
        res = {}
        for fact in factures_xml['OtrasFacturas']:
            infos = []
            cr = db.cursor()
            try:
                l_id = []
                # Payment order search/generation
                payment_order_id = f1_obj.get_payment_order(
                    cr, uid, emisor, remesa_data, context=context
                )
                polissa_id = q1_obj.find_polissa(cr, uid, fact.cups,
                                                 fact.data_factura,
                                                 fact.data_factura)
                if not polissa_id:
                    raise osv.except_osv('Error', _("No s'ha trobat la polissa"))
                q1_obj.check_emisor_polissa(cr, uid, polissa_id, emisor)
                f1_obj.check_existeix_factura(cr, uid, fact, partner_id[0])
                lect_total = []
                val1 = None
                fid2, info = f1_obj.importar_factura(
                    cr, uid, lect_total, fact, polissa_id, val1,
                    f1_xml.data_limit_pagament, partner_id[0], payment_order_id,
                    context=context
                )
                # Totes les linies creades del titpus altres són extra lines
                factura = fact_pool.browse(cr, uid, fid2, context)
                lin_extras = []
                conceptes = []
                for linia in factura.linia_ids:
                    if linia.tipus == 'altres':
                        lin_extras.append(linia.id)
                        if (fact.tipus_factura == '10'
                                and fact.import_net == 0
                                and fact.saldo_cobrament < 0):
                            if linia.product_id.code == 'CON14':
                                conceptes.append(linia.product_id.code)
                conceptes = set(conceptes)
                if len(conceptes) == 1:
                    # Això deu ser la factura burocràtica d'ENDESA
                    # l'eliminem
                    anul_carregada = fact_pool.search(cr, uid, [
                        ('amount_total', '=', abs(fact.import_total_factura)),
                        ('invoice_id.partner_id.id', '=', factura.partner_id.id),
                        ('cups_id.name', '=', fact.cups),
                        ('type', '=', 'in_refund')
                    ], limit=1, context={'active_test': False})
                    if anul_carregada:
                        anul_carregada = fact_pool.read(cr, uid,
                                                        anul_carregada[0],
                                                        ['origin'])['origin']
                    msg = _(u"CUPS %s Descartada per no complir condicions "
                            u"coherència.") % fact.cups
                    if anul_carregada:
                        msg += _(u"Te el mateix import que la factura "
                                 u"anul·ladora %s ja carregada") % anul_carregada
                    else:
                        msg += _(u"Probablement falta una factura anul·ladora "
                                 u"de import %s") % fact.import_total_factura
                    info = [msg]
                    fact_pool.unlink(cr, uid, [fid2], context)
                    res_ids = []
                    e_id = []
                else:
                    e_id = f1_obj.crear_extra_facturacio(cr, uid, fid2, fname,
                                                         lin_extras, context)
                    res_ids = [fid2]
                    val2 = f1_obj.get_total_factura(cr, uid, fid2, context)
                infos.extend(info)
                infos = '%s' % '\n'.join([
                    u'  * %s' % info for info in infos if info
                ])
                if infos:
                    res_ids.append(-1)
                infos = '\n%s' % infos
                res.update({str(fact.numero_factura):
                             [True, infos, ref, res_ids, l_id, e_id, xml_b64]})
                cr.commit()
            except (osv.except_osv, orm.except_orm, message.except_f1), e:
                cr.rollback()
                n_fact = fact and str(fact.numero_factura) or ''
                _info = e.value if isinstance(e.value, unicode) else \
                                                       unicode(e.value, 'utf8')
                res.update({n_fact: [False, _info, ref, xml_b64]})
            except Exception as exc:
                traceback.print_exc()
                cr.rollback()
                _info = exc[0] if isinstance(exc[0], unicode) else \
                                                       unicode(exc[0], 'utf8')
                res.update({str(fact.numero_factura):
                                    [False, _info, ref, xml_b64]})
            finally:
                cr.close()
        for fact in factures_xml['FacturaATR']:
            context['tipus_rectificadora'] = fact.tipus_rectificadora
            infos = []
            e_id = []
            cr = db.cursor()
            try:
                # Payment order search/generation
                payment_order_id = f1_obj.get_payment_order(
                    cr, uid, emisor, remesa_data, context=context
                )
                fid1 = fid2 = l_id = None
                polissa_id = q1_obj.find_polissa(cr, uid, fact.cups,
                                                 fact.data_inici,
                                                 fact.data_final)
                if not polissa_id:
                    raise osv.except_osv('Error',
                                         _("No s'ha trobat la polissa"))
                # actualitzem ref_distri de pólissa
                ref_distri = fact.contracte_distri
                if ref_distri:
                    pol_obj = self.pool.get('giscedata.polissa')
                    pol_dades = pol_obj.read(cr, uid, polissa_id,
                                             ['name', 'ref_dist'])
                    if pol_dades['ref_dist'] != ref_distri:
                        pol_obj.write(cr, uid, polissa_id,
                                      {'ref_dist': ref_distri})
                q1_obj.check_emisor_polissa(cr, uid, polissa_id, emisor)
                f1_obj.check_tarifa(cr, uid, fact, polissa_id, context=context)
                f1_obj.check_existeix_factura(cr, uid, fact, partner_id[0])
                comptadors = fact.get_comptadors()
                if comptadors:
                    dates_lect = {}
                    lect_total = []
                    context['num_comptadors'] = len(comptadors)
                    for cmpt in comptadors:
                        lectures = cmpt.get_lectures()
                        if not lectures:
                            raise osv.except_osv('Error',
                                                _("Comptador %s sense lectures") %
                                                cmpt.nom_comptador)
                        data_inici = lectures[0].data_lectura_inicial
                        dict_lect = Q1.agrupar_lectures_per_data(lectures)
                        data_inici_l, data_final_l = Q1.\
                                            obtenir_data_inici_i_final(dict_lect)

                        compt = q1_obj.check_comptador(cr, uid, cmpt.nom_comptador,
                                                       cmpt.gir_comptador,
                                                       polissa_id, data_inici_l,
                                                       data_final_l, fact, context)
                        if fact.tipus_rectificadora in \
                                        RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']:
                            check_lectures = True
                        else:
                            check_lectures = False
                        en_baixa = False
                        if (fact.codi_tarifa == '011'
                                and fact.ind_mesura_baixa == 'S'):
                            en_baixa = True
                        lect_data = q1_obj.processar_lectures(
                            cr, uid, lectures, compt, fact.codi_tarifa,
                            check=check_lectures, en_baixa=en_baixa,
                            context=context
                        )
                        if not lect_data:
                            msg = _(u"No s'han trobat lectures vàlides pel "
                                    u"comptador %s") % cmpt.nom_comptador
                            raise osv.except_osv('Error', msg)
                        else:
                            lect_total.append(lect_data)
                        if fact.tipus_rectificadora in \
                                RECTIFYING_RECTIFICATIVE_INVOICE_TYPES + ['N']:
                            l_id = q1_obj.importar_lectures_compt(cr, uid,
                                                                  lect_data,
                                                                  context)
                        dates_lect.update(q1_obj.cerca_dates_lect(lect_data,

                                                                  dates_lect))
                else:
                    dates_lect = {}
                    lect_total = []
                if fact.tipus_rectificadora == 'N':
                    fid1 = f1_obj.facturar_lectures(cr, uid, polissa_id, fact,
                                                    dates_lect, context)
                    lin_refact, refact = f1_obj.check_refacturacio(cr, uid,
                                                                   fact, fid1)
                    if refact:
                        e_id = f1_obj.crear_extra_facturacio(cr, uid, fid1,
                                                             fname, lin_refact,
                                                             context)
                    val1 = f1_obj.get_total_factura(cr, uid, fid1, context)
                elif fact.tipus_rectificadora in \
                        ALL_RECTIFICATIVE_INVOICE_TYPES:
                    fid1 = None
                    # Fem que val1 sigui el valor de l'original
                    fid = f1_obj.get_factura_referencia(cr, uid, fact,
                                                        'CENERGIA', partner_id, context)
                    val1 = f1_obj.get_total_factura(cr, uid, fid, context)
                # facturar important línies de factura
                fid2, info = f1_obj.importar_factura(
                    cr, uid, lect_total, fact, polissa_id, val1,
                    f1_xml.data_limit_pagament, partner_id[0], payment_order_id,
                    context=context
                )
                factura = fact_pool.browse(cr, uid, fid2, context)
                lin_extras = []
                for linia in factura.linia_ids:
                    if linia.tipus == 'altres':
                        lin_extras.append(linia.id)
                e_id = f1_obj.crear_extra_facturacio(cr, uid, fid2, fname,
                                                         lin_extras, context)
                val2 = f1_obj.get_total_factura(cr, uid, fid2, context)
                infos += [info]
                # Comparar els totals
                if round(val1, 2) == round(val2, 2):
                    if fid1:
                        fact_pool.unlink(cr, uid, [fid1], context)
                    infos += [u'Correcte']
                    res_ids = [fid2]
                else:
                    infos += [u'Divergència en els totals']
                    res_ids = [fid2, fid1]
                cr.commit()
                infos.extend(info)
                infos = '\n%s' % '\n'.join([
                    u'  * %s' % info for info in infos if info
                ])
                res.update({str(fact.numero_factura):
                             [True, infos, ref, res_ids, l_id, e_id, xml_b64]})
            except (osv.except_osv, orm.except_orm, message.except_f1), e:
                cr.rollback()
                n_fact = fact and str(fact.numero_factura) or ''
                _info = e.value if isinstance(e.value, unicode) else \
                                                       unicode(e.value, 'utf8')
                res.update({n_fact: [False, _info, ref, xml_b64]})
            except Exception as exc:
                traceback.print_exc()
                cr.rollback()
                _info = exc[0] if isinstance(exc[0], unicode) else \
                                                       unicode(exc[0], 'utf8')
                res.update({str(fact.numero_factura):
                                    [False, _info, ref, xml_b64]})
            finally:
                cr.close()
        return res, emisor

    def from_zip(self, cursor, uid, data, emisor_lot=None, context=None):
        """Processar un zip de fitxers xml.

           La funció retorna en la variable val el diccionari en
           que les claus són els noms dels fitxers que conté el
           zip i els valors el resultat de la funció from_xml()
        """
        imp = self.pool.get('giscedata.facturacio.importacio')
        fswh_obj = self.pool.get('giscedata.facturacio.switching.helper')
        cups_obj = self.pool.get('giscedata.cups.ps')
        if not context:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        _data = base64.decodestring(data)
        fileHandle = StringIO.StringIO(_data)
        try:
            zfile = zipfile.ZipFile(fileHandle, "r")
        except zipfile.BadZipfile, e:
            raise osv.except_osv('Error', 'El fitxer no es un fitxer zip')
        val = {}
        emisor = emisor_lot and emisor_lot or ''
        i_id = context.get('importacio_id')
        pb_total = len(zfile.infolist())

        # generate's payment orders (remeses)
        db = pooler.get_db_only(cursor.dbname)
        pbcr = db.cursor()
        remeses = get_remeses_from_zip(zfile, emisor)
        for remesa in remeses:
            po_emisor = remesa['emisor']
            if po_emisor:
                fswh_obj.get_payment_order(
                    pbcr, uid, po_emisor, remesa, context=context
                )
        pbcr.commit()
        pbcr.close()

        pbcr = db.cursor()
        try:
            for i, info in enumerate(zfile.infolist()):
                prog = i * 100.0 / pb_total
                imp.write(pbcr, uid, i_id, {'progres': prog})
                pbcr.commit()
                fname = info.filename
                xml = zfile.read(fname)
                cups_id = ''
                #Agafem el CUPS per registrar-lo a la bd
                try:
                    f1_xml = F1(xml, 'F1')
                    f1_xml.parse_xml()
                    cups = f1_xml.get_codi[:20]
                    cupses = cups_obj.search(cursor, uid, [('name', 'like', cups)],
                                             context={'active_test': False})
                    if cupses:
                        cups_id = cupses[0]
                except message.except_f1, e:
                    pass
                if isinstance(info.filename, str):
                    uni_fname = unicode(info.filename, errors='ignore')
                else:
                    uni_fname = info.filename
                res, e_fact = self.from_xml(cursor, uid, xml, uni_fname,
                                                        emisor, context)
                retorn = {'res': res, 'cups_id': cups_id}
                if not emisor:
                    emisor = e_fact
                val.update({basename(fname): retorn})
        finally:
            pbcr.close()
        return val, emisor

    def search(self, cursor, uid, args, offset=0, limit=None, order=None,
        context=None, count=False):
        for pos, arg in enumerate(args):
            if arg[0] == 'div_totals' and arg[1] in ('>=', '<=', '='):
                cursor.execute("select id from giscedata_facturacio_factura "
                               "where "
                               "@div_totals %s %%s" % arg[1], (arg[2],))
                ids = [a[0] for a in cursor.fetchall()] or [-1]
                args[pos] = ['id', 'in', ids]
        return super(GiscedataFacturacioFactura, self).search(cursor, uid,
            args, offset=offset, limit=limit, order=order, context=context,
            count=count)

    def invoice_open(self, cursor, uid, ids, context=None):
        """Adds invoice to payment order configured in payment_order_id"""

        # Check if configuration var is enabled
        # - If an invoice has f1 and not in phase 4-5 can be opened
        # - If the F1 is in phase 4 will check if F1 is valid to be opened
        fact_f1_obj = self.pool.get('giscedata.facturacio.importacio.linia.factura')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        cfg_obj = self.pool.get('res.config')

        f1_constraint = int(
            cfg_obj.get(cursor, uid, 'fact_open_f1_constraint', '0')
        )

        if f1_constraint:
            # Relation Table Factura(factura_id) - F1(linia_id)
            q = fact_f1_obj.q(cursor, uid)
            sql = q.select(['linia_id', 'linia_id.import_phase', 'linia_id.state']).where([
                ('factura_id', 'in', tuple(ids)),
                ('linia_id.import_phase', '!=', 50),
            ])
            cursor.execute(*sql)
            result = cursor.dictfetchall()
            result = [f1['linia_id'] for f1 in result
                      if f1['linia_id.import_phase'] != 40
                      or f1['linia_id.state'] != 'valid'
                      ]
            if result:
                raise osv.except_osv(_('Error'),
                                     _('No es pot obrir la factura ja que te un'
                                       ' F1 en fase 1-3 o un en fase 4 no valid'
                                       '\nF1 detectats: {0}'
                                       ).format(",".join(str(x) for x in result))
                                     )

        q = fact_f1_obj.q(cursor, uid)
        sql = q.select(['factura_id']).where([
            ('factura_id', 'in', tuple(ids)),
        ])
        cursor.execute(*sql)
        fact_ids = [id for (id,) in cursor.fetchall()]
        if fact_ids:
            fact_dates = fact_obj.read(cursor, uid, fact_ids, ['date_due'])

        res = super(
            GiscedataFacturacioFactura, self
        ).invoice_open(cursor, uid, ids, context=context)

        if fact_ids and fact_dates:
            for fact_date in fact_dates:
                fact_obj.write(cursor, uid, fact_date['id'], {'date_due': fact_date['date_due']})

        invoice_fields = ['payment_order_id', 'state']
        for invoice in self.read(cursor, uid, ids, invoice_fields,
                                 context=context):
            order_id = invoice.get('payment_order_id', False)
            invoice_state = invoice.get('state', False)
            if order_id and invoice_state in ['open']:
                invoice_id = invoice['id']
                self.afegeix_a_remesa(
                    cursor, uid, [invoice_id], order_id[0], context=context
                )

        return res

    def get_in_invoice_by_origin(
            self, cursor, uid, origin, partner_id, year=None, context=None
    ):
        """
        Returns provider invoice id of partner with selected origin
        :param origin:
        :param partner_id:
        :param context:
        :param year:
        :type year: str
        :return: return invoice ids or empty list
        """
        search_dict = [('origin', '=', origin),
                       ('partner_id', '=', partner_id),
                       ('type', 'in', ['in_invoice', 'in_refund'])
                       ]

        if year:
            ini = '{0}-01-01'.format(year)
            end = '{0}-12-31'.format(year)
            search_dict.append(('origin_date_invoice', '>=', ini))
            search_dict.append(('origin_date_invoice', '<=', end))

        invoice_ids = self.search(cursor, uid, search_dict, context=context)
        return invoice_ids

    def te_linies_facturacio_complementaria(self, cursor, uid, factura_id, context=None):
        if isinstance(factura_id, (list, tuple)):
            factura_id = factura_id[0]
        query = """
            SELECT linia_fac.id linia_factura, extra_rel.extra_id linia_extra, f1.id f1, f1.type_factura tipus_f1
            FROM giscedata_facturacio_factura_linia linia_fac
            INNER JOIN facturacio_extra_factura_linia_rel extra_rel ON extra_rel.factura_linia_id = linia_fac.id
            INNER JOIN giscedata_facturacio_importacio_linia_extra f1_to_extra_rel ON f1_to_extra_rel.extra_id=extra_rel.extra_id
            INNER JOIN giscedata_facturacio_importacio_linia f1 ON f1.id=f1_to_extra_rel.linia_id
            where linia_fac.factura_id = %s AND f1.type_factura='C';
        """ % (factura_id, )
        cursor.execute(query)
        res = cursor.dictfetchall()
        return res

    _columns = {
        'importacio_id': fields.many2one('giscedata.facturacio.importacio',
                                         'Importacio'),
        'div_totals': fields.float('Divergència',
                                   digits=(16, int(config['price_accuracy'])))
    }

GiscedataFacturacioFactura()
