# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from tools.translate import _
from datetime import datetime, timedelta


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_factures_f1_xml(self, cursor, uid, ids, context=None):
        """ Retorna els ids des línies de importació relacionats amb una
        pólissa"""
        res = []
        if not context:
            context = {}

        if isinstance(ids, list):
            ids = ids[0]

        ilf_o = self.pool.get('giscedata.facturacio.importacio.linia.factura')
        pol_o = self.pool.get('giscedata.polissa')

        il_o = self.pool.get('giscedata.facturacio.importacio.linia')

        ctx = context.copy()
        ctx.update({'active_text': False})
        lfactures = ilf_o.search(cursor, uid, [('polissa_id', '=', ids)], context=ctx)
        l_vals = ilf_o.read(cursor, uid, lfactures, ['linia_id'])
        res = [l['linia_id'][0] for l in l_vals]

        cups_id = pol_o.read(cursor, uid, ids, ['cups'])['cups'][0]
        if cups_id:
            res_cups = il_o.search(cursor, uid, [('cups_id', '=', cups_id)])
            res += res_cups
            res = list(set(res))

        return res

    def fix_lectures_modcon_mc02(self, cursor, uid, polissa_id, context=None):
        """
        Corregeix la situacio en la qual tenim una modcon en dia D sense lectures inicials al dia D-1 carregades pero
        si a pool i amb lectures >= al dia D carregades.

        Es carreguen de pool les lectures del dia -1 i a més es carreguen, si n'hi ha, les primeres lectures amb data > D.
        """
        if context is None:
            context = {}

        meter_name = self.check_lectures_modcon_mc02(cursor, uid, polissa_id, context)
        if not meter_name:
            raise osv.except_osv(_(u'No és una modificació contractual amb dates de lectures inicials incorrectes.'))

        pol_obj = self.pool.get("giscedata.polissa")
        lect_pool_obj = self.pool.get("giscedata.lectures.lectura.pool")

        polissa = pol_obj.browse(cursor, uid, polissa_id, context=context)
        dini_modcon = polissa.modcontractual_activa.data_inici
        dini_ant = (
            datetime.strptime(dini_modcon, "%Y-%m-%d") - timedelta(days=1)
        ).strftime("%Y-%m-%d")
        tarifa = polissa.tarifa
        periodes_ids = tarifa.get_periodes(tipus='te').values()

        # Busquem les lectures que s'han de carregar del pool
        l_ids = lect_pool_obj.search(cursor, uid, [
            ('comptador', '=', meter_name), ('name', '=', dini_ant), ('periode', 'in', periodes_ids)
        ])
        wiz_pool_obj = self.pool.get("wizard.copiar.lectura.pool.a.fact")
        for lid in l_ids:
            wiz_id = wiz_pool_obj.create(cursor, uid, {}, context={'active_id': lid})
            wiz_pool_obj.action_copia_lectura(cursor, uid, [wiz_id], context={'active_id': lid})

        # Finalment mirem si podem carregar alguna lectura posterior
        l_ids = lect_pool_obj.search(cursor, uid, [
            ('comptador', '=', meter_name), ('name', '>', dini_modcon), ('periode', 'in', periodes_ids)
        ])
        if len(l_ids):
            lectures_per_data = {}
            for linfo in lect_pool_obj.read(cursor, uid, l_ids, ['name']):
                lectures_per_data.setdefault(linfo['name'], [])
                lectures_per_data[linfo['name']].append(linfo['id'])

            mes_antiga = min(lectures_per_data.keys())
            for lid in lectures_per_data[mes_antiga]:
                wiz_id = wiz_pool_obj.create(cursor, uid, {}, context={'active_id': lid})
                wiz_pool_obj.action_copia_lectura(cursor, uid, [wiz_id], context={'active_id': lid})

        return True

GiscedataPolissa()
