# -*- coding: utf-8 -*-
import click
import logging
from erppeek import Client
from datetime import datetime
from progressbar import ProgressBar, ETA, Percentage, Bar
'''
Script que genera un fitxer SQL per actualitzar els origens de les lectures de les
factures de proveidor
'''


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)

query = '''
update giscedata_facturacio_lectures_energia
set origen_id = sub.origen_id
from (
  select l.name as l_data, l.origen_id
  from giscedata_facturacio_factura f, giscedata_facturacio_lectures_energia fl,
     giscedata_lectures_comptador c, giscedata_lectures_lectura l, giscedata_polissa_tarifa_periodes tp,
     product_product pp, product_template pt
  where
   fl.factura_id = f.id
   and (fl.comptador_id = c.id or fl.comptador = c.name)
   and l.comptador = c.id
   and l.periode = tp.id
   and tp.product_id=pp.id
   and pp.product_tmpl_id=pt.id
   and substr(upper(fl.tipus), 1, 1) = l.tipus
   and substr(fl.name, strpos(fl.name, 'P'), 2) = pt.name
   and l.name = fl.data_actual
   and l.tipus = 'A'
   and f.id = {fact_number}
) sub
where giscedata_facturacio_lectures_energia.data_actual=sub.l_data
      and giscedata_facturacio_lectures_energia.factura_id = {fact_number}
      and giscedata_facturacio_lectures_energia.tipus = 'activa'
      and giscedata_facturacio_lectures_energia.origen_id is null
;

update giscedata_facturacio_lectures_energia
set origen_anterior_id = sub.origen_id
from (
  select l.name as l_data, l.origen_id
  from giscedata_facturacio_factura f, giscedata_facturacio_lectures_energia fl,
     giscedata_lectures_comptador c, giscedata_lectures_lectura l, giscedata_polissa_tarifa_periodes tp,
     product_product pp, product_template pt
  where
   fl.factura_id = f.id
   and (fl.comptador_id = c.id or fl.comptador = c.name)
   and l.comptador = c.id
   and l.periode = tp.id
   and tp.product_id=pp.id
   and pp.product_tmpl_id=pt.id
   and substr(upper(fl.tipus), 1, 1) = l.tipus
   and substr(fl.name, strpos(fl.name, 'P'), 2) = pt.name
   and l.name = fl.data_anterior
   and l.tipus = 'A'
   and f.id = {fact_number}
) sub
where giscedata_facturacio_lectures_energia.data_anterior=sub.l_data
      and giscedata_facturacio_lectures_energia.factura_id = {fact_number}
      and giscedata_facturacio_lectures_energia.tipus = 'activa'
      and giscedata_facturacio_lectures_energia.origen_anterior_id is null
;

'''

def update_origins_in_invoices(c, output_file):
    logger = logging.getLogger('populate_origins_invoices')
    fact_obj = c.model('giscedata.facturacio.factura')
    fact_lect_obj = c.model('giscedata.facturacio.lectures.energia')
    search_params = [
        ('type', 'in', ['in_invoice', 'in_refund']),
        ('state', '!=', 'draft'),
    ]
    fact_ids = fact_obj.search(search_params)
    widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=len(fact_ids)).start()
    done = 0
    with open(output_file, 'w') as f:
        for fact_id in fact_ids:
            search_params = [
                '|',
                ('origen_id', '=', False),
                ('origen_anterior_id', '=', False),
                ('factura_id', '=', fact_id),
                ('tipus', '=', 'activa')
            ]
            lects_empty = fact_lect_obj.search(search_params)
            if lects_empty:
                f.write(query.format(fact_number=fact_id))
            done += 1
            pbar.update(done)
        pbar.finish()
    return True

@click.command()
@click.option('-s', '--server', default='localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=18069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('-o','--output', default='/tmp/populate_origins_invoices.sql',
                help='Path del fitxer de sortida. Per defecte /tmp/populate_origins_invoices.sql')
@click.option('--log-path', default='/tmp/populate_origins_invoices.log',
                help='Path del fitxer de log. Per defecte /tmp/populate_origins_invoices.log')
def main(**kwargs):

    log_file = kwargs['log_path']
    setup_log('populate_origins_invoices', log_file)
    logger = logging.getLogger('populate_origins_invoices')
    logger.info('#######Start now {0}'.format(datetime.today()))
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])

    update_origins_in_invoices(c, kwargs['output'])

if __name__ == '__main__':
    main()