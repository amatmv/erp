# -*- coding: utf-8 -*-
from datetime import datetime
import json

from osv import osv, fields, orm
from tools.translate import _
from tools.safe_eval import safe_eval as eval
#from ..giscedata_switching_r1 import get_type_subtype
from itertools import chain


class WizardGenerateR1FromF1Erroni(osv.osv_memory):
    """Wizard to generate R1 Cases from contracts.
    """
    _name = 'wizard.r1.from.f1.erroni'

    def get_line_ids(self, cursor, uid, context=None):
        return context.get('active_ids', [])

    def get_polissa_id(self, cursor, uid, cups_id, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['active_test'] = False
        pol_obj = self.pool.get('giscedata.polissa')
        search_params = [('cups', '=', cups_id)]

        pol_id = pol_obj.search(cursor, uid, search_params, context=ctx)

        ids_len = len(pol_id)

        if ids_len == 1:
            return pol_id
        elif ids_len == 0:
            return False
        else:
            search_params.append(('id', 'in', pol_id))
            search_params.append(('state', '=', 'activa'))
            pol_id = pol_obj.search(cursor, uid, search_params, context=ctx)
            ids_len = len(pol_id)
            if ids_len != 1:
                search_params.pop()
                search_params.append(('state', 'in', ('esborrany', 'validar')))
                pol_id = pol_obj.search(cursor, uid, search_params,
                                        context=ctx)
            if len(pol_id) == 1:
                return pol_id
        return False

    def create_atr_case(self, cursor, uid, line_id, context=None):
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        pol_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        reclama_obj = self.pool.get("giscedata.switching.reclamacio")
        sw_oj = self.pool.get("giscedata.switching")
        line_info = line_obj.read(
            cursor, uid, line_id,
            ['critical_info', 'cups_id', 'cups_text', 'invoice_number_text'],
            context=context
        )
        critical_info = line_info['critical_info'] or ""
        cups_id = line_info['cups_id']
        pol_id = False
        if not cups_id:
            cups_id = cups_obj.search(
                cursor, uid, [('name', '=', line_info['cups_text'])]
            )
        if cups_id:
            cups_id = cups_id[0]
            ctx = context.copy()
            ctx['search_non_active'] = True
            pol_id = sw_oj.trobar_polissa_w_cups(cursor, uid, cups_id, context=ctx)

        if pol_id:
            # Config vals to generate R1 case
            rec_vals = {
                'num_factura': line_info['invoice_number_text']
            }
            rec_id = reclama_obj.create(cursor, uid, rec_vals, context=context)
            r1_comments = context.get('comentaris', "") + "\n" + critical_info
            config_vals = {
                'type': '02',
                'subtype': '037',
                'comments': r1_comments[:4000],
                'objector': '06',
                'reclamacio_ids': [rec_id]
            }
            res = pol_obj.crear_cas_atr(
                cursor, uid, pol_id, 'R1', config_vals=config_vals,
                context=context
            )

            return res
        return False

    def action_create_atr_cases(self, cursor, uid, ids, context=None):
        """
        Creates a R1-01 step for every selected F1
        :param ids: wizard id
        :param context: 'proces' key with desired type/subtype in 'R1-ttss'
        format
        """
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context=context)
        context['comentaris'] = wiz.comentaris or ""
        line_ids = self.get_line_ids(cursor, uid, context=context)
        msg = _('Històric del procés:\n')
        json_cases = False
        if line_ids:
            generated_cases = []
            line_err = []
            for line in line_ids:
                res = self.create_atr_case(cursor, uid, line, context=context)
                if not res:
                    msg += (str(line) + _(': No s\'ha trobat cap '
                                          'contracte per aquesta linia\n')
                            )
                else:
                    msg += "%s: %s\n" % (res[0], res[1])
                    if res[2]:
                        generated_cases.append(res[2])
                    else:
                        line_err.append(line)
            if line_err:
                msg_aux = _('Llistat de linies per les que no s\'han '
                            'generat casos:\n')
                for err in line_err:
                    msg_aux += str(err) + '\n'
                msg = msg_aux + msg
            json_cases = json.dumps(generated_cases)
        else:
            msg = _('No s\'ha sel·leccionat cap F1')

        writable = {'state': 'end', 'info': msg}
        if json_cases:
            writable.update({'generated_cases': json_cases})
        self.write(cursor, uid, ids, writable, context=context)

    def _default_info(self, cursor, uid, context=None):
        lines = context.get('active_ids', [])
        if not len(lines):
            return _('No s\'ha sel·leccionat cap F1')

        msg = _(
            u'Es procedirà a la generació de casos R1 de {0} F1.\n\n'
        ).format(len(lines))

        for line_id in lines:
            cups_id = self.get_cups_of_line(cursor, uid, line_id, context)
            if not cups_id:
                continue

            pol_id = self.get_polissa_id(cursor, uid, cups_id, context=context)
            if not pol_id:
                continue

            r1_oberts = self.get_opened_r1_from_contract(cursor, uid, pol_id)
            if not r1_oberts:
                continue

            msg = _(
                u"{0}Atenció! El F1 id: {1} ja te casos R1 oberts ({2})\n"
            ).format(msg, line_id, r1_oberts)

        return msg

    def get_opened_r1_from_contract(self, cursor, uid, polissa_id, context=None):
        sw_obj = self.pool.get("giscedata.switching")
        proces_obj = self.pool.get("giscedata.switching.proces")
        proces_r1_id = proces_obj.search(cursor, uid, [('name', '=', 'R1')])[0]
        r1_oberts = sw_obj.search(
            cursor, uid, [('proces_id', '=', proces_r1_id),
                          ('cups_polissa_id', '=', polissa_id),
                          ('state', '!=', 'done')]
        )
        return r1_oberts

    def get_cups_of_line(self, cursor, uid, line_id, context=None):
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        cups_obj = self.pool.get('giscedata.cups.ps')
        line_info = line_obj.read(
            cursor, uid, line_id, ['cups_id', 'cups_text'], context=context
        )
        cups_id = line_info['cups_id']
        if not cups_id:
            cups_id = cups_obj.search(
                cursor, uid, [('name', '=', line_info['cups_text'])]
            )
        if cups_id:
            cups_id = cups_id[0]
        return cups_id

    def show_cases(self, cursor, uid, ids, context=None):
        """
        Retornem les factures creades per repassar-les
        """
        wiz = self.browse(cursor, uid, ids[0], context=context)
        cases_ids = wiz.generated_cases
        if eval(str(cases_ids)):
            casos_ids = json.loads(cases_ids)
            return {
                'domain': "[('id','in', %s)]" % str(casos_ids),
                'name': _('Casos R1 Creats'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.switching',
                'type': 'ir.actions.act_window'
            }
        else:
            raise osv.except_osv('Error', _('No s\'ha generat cap cas'))

    def create_cases_from_invoice_contracts(self, cursor, uid, ids, context=None):
        rel_fact_line_obj = self.pool.get(
            'giscedata.facturacio.importacio.linia.factura'
        )

        if context is None:
            context = {}

        lines_ids = context.get('active_ids', [])

        if not lines_ids:
            raise osv.except_osv('Error',
                                 _("No s'han seleccionat línies d'importació"))
        else:
            polissa_ids = []
            facts_dict = {}

            for line_id in lines_ids:
                cups_id = self.get_cups_of_line(cursor, uid, line_id, context)
                if not cups_id:
                    continue

                pol_id = self.get_polissa_id(cursor, uid, cups_id,
                                             context=context)[0]
                if not pol_id:
                    continue

                polissa_ids.append(pol_id)

                rels_ids = rel_fact_line_obj.search(
                    cursor, uid, [('linia_id', '=', line_id)], context=context
                )

                rels = rel_fact_line_obj.read(
                    cursor, uid, rels_ids, ['factura_id'], context=context
                )

                factures_ids = [rel['factura_id'][0] for rel in rels]

                if facts_dict.get(str(pol_id), False):
                    facts_dict[str(pol_id)] = list(
                        set(
                            chain.from_iterable(
                                [facts_dict[str(pol_id)], factures_ids]
                            )
                        )
                    )

                else:
                    facts_dict[str(pol_id)] = factures_ids
            polissa_ids = list(set(polissa_ids))

            if len(polissa_ids) > 1:
                return {
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'wizard.r101.from.multiple.contracts',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': "{{'contract_id': {0}, 'contract_ids': {1}, 'reclamacio_num_factura': {2}}}".format(
                        polissa_ids[0], polissa_ids, facts_dict
                    )
                }
            else:
                return {
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'wizard.create.r1',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': "{{'polissa_id': {0}, 'reclamacio_num_factura': {1}}}".format(
                        polissa_ids[0], facts_dict
                    )
                }

    _columns = {
        'state': fields.char('State', size=16),
        'info': fields.text('Historic'),
        'generated_cases': fields.text('Casos generats'),
        'comentaris': fields.text('Comentaris'),
        'info_comentaris': fields.text('Info. Comentaris', readonly=True),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'generated_cases': lambda *a: json.dumps([]),
        'info_comentaris': lambda *a: _("A més dels comentaris introduïts en "
                                        "aquest camp, també s'afegiran els "
                                        "missatges d'error de cada F1 als "
                                        "comentaris del R1.\n(Nota: la llargada"
                                        " màxima és de 4000 caracters)")
    }

WizardGenerateR1FromF1Erroni()
