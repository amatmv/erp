# -*- coding: utf-8 -*-
from __future__ import absolute_import
import base64
try:
    import cStringIO as StringIO
except:
    import StringIO
import zipfile
import logging
import time

import pooler
from tools.translate import _
from tools.misc import ustr
from tools import config
from osv import osv, fields, orm
from oorq.oorq import JobsPool, get_queue, set_hash_job
from oorq.tasks import execute
from ..giscedata_facturacio_switching_utils import get_filename


class ProgressBarJobsPool(JobsPool):

    def __init__(self, dbname, uid, import_id):
        self.dbname = dbname
        self.uid = uid
        self.import_id = import_id
        super(ProgressBarJobsPool, self).__init__()

    def join(self):
        self.joined = True
        # self.write_progress()
        while not self.all_done:
            # self.write_progress()
            time.sleep(1)

    def write_progress(self):
        if self.import_id:
            pool = pooler.get_pool(self.dbname)
            imp = pool.get('giscedata.facturacio.importacio')
            cr = pooler.get_db(self.dbname).cursor()
            try:
                imp.write(cr, self.uid, self.import_id, {
                    'progres': self.progress
                })
                cr.commit()
            except:
                cr.rollback()
            finally:
                cr.close()


class WizardImportacioF1(osv.osv_memory):

    _name = 'wizard.importacio.f1'

    _columns = {
        'file': fields.binary('Fitxer', required=True),
        'filename': fields.char('Nom', size=1024),
        'info': fields.text('Informació'),
        'state': fields.char('Estat', size=16),
    }

    def create_import_file(self, cursor, uid, filename, context=None):
        if context is None:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        import_obj = self.pool.get('giscedata.facturacio.importacio')
        import_id = None
        try:
            tmp_cr = db.cursor()
            values = {
                'name': get_filename(filename)
            }
            import_id = import_obj.create(tmp_cr, uid, values, context)
            tmp_cr.commit()
        except (osv.except_osv, orm.except_orm), e:
            error_msg = ustr(e.value)
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
            tmp_cr.rollback()
            raise orm.except_orm(u'Atenció', error_msg)
        finally:
            tmp_cr.close()
        return import_id

    def create_payment_orders(self, cursor, uid, ids, import_file, context=None):
        if context is None:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        import_obj = self.pool.get('giscedata.facturacio.importacio')
        try:
            tmp_cr = db.cursor()
            import_id = import_obj.create_payment_orders_from_file(
                tmp_cr, uid, import_file, context=context
            )
            tmp_cr.commit()
        except (osv.except_osv, orm.except_orm), e:
            error_msg = ustr(e.value)
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
            tmp_cr.rollback()
            raise orm.except_orm(u'Atenció', error_msg)
        finally:
            tmp_cr.close()
        return import_id

    def create_import_line(self, cursor, uid, import_id, filename, xml_data,
                           context=None):
        if context is None:
            context = {}
        logger = logging.getLogger(
            'openerp.{}.create_import_line'.format(__name__)
        )
        db = pooler.get_db_only(cursor.dbname)
        line_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        line_id = None
        try:
            tmp_cr = db.cursor()
            logger.info('Create line for file {0}'.format(
                filename
            ))
            line_id = line_obj.create_from_xml(
                tmp_cr, uid, import_id, filename, xml_data, context=context
            )
            logger.info('Created import line for file {0} with'.format(
                filename, line_id
            ))
            tmp_cr.commit()
        except Exception, e:
            sentry = self.pool.get('sentry.setup')
            if sentry:
                sentry.client.captureException()
            tmp_cr.rollback()
        finally:
            tmp_cr.close()
        return line_id

    def import_zip(self, cursor, uid, import_id, zfile, context=None):
        if context is None:
            context = {}
        cfg = self.pool.get('res.config')
        load_zip_by_queue = int(
            cfg.get(cursor, uid, 'fact_sw_load_zip_by_queue', '0')
        )
        zip_queue = cfg.get(cursor, uid, 'fact_sw_zip_queue', 'import_xml')
        if load_zip_by_queue:
            j_pool = ProgressBarJobsPool(cursor.dbname, uid, import_id)
        for info in zfile.infolist():
            if not info.file_size:
                continue
            #check is not a folder
            if not info.filename.endswith('/'):
                xml_data = zfile.read(info.filename)
                filename = get_filename(info.filename.split('/')[-1])
                if not load_zip_by_queue:
                    self.create_import_line(
                        cursor, uid, import_id, filename, xml_data, context=context
                    )
                else:
                    timeout = 3 * 3600
                    max_ttl = 24 * 3600
                    q = get_queue(zip_queue)
                    conf_attrs = dict(
                        [(attr, value) for attr, value in config.options.items()]
                    )
                    job_args = (
                        conf_attrs, cursor.dbname, uid, self._name,
                        'create_import_line', import_id, filename, xml_data,
                        context
                    )
                    job = q.enqueue(execute, result_ttl=max_ttl, ttl=max_ttl,
                                    timeout=timeout, args=job_args)
                    set_hash_job(job)
                    x = j_pool.add_job(job)
        if load_zip_by_queue:
            j_pool.join()

    def process_file(self, cursor, uid, import_id, context=None):
        logger = logging.getLogger(
            'openerp.{}.process_file'.format(__name__)
        )
        if context is None:
            context = {}
        import_obj = self.pool.get('giscedata.facturacio.importacio')
        logger.info('Process lines from import file {0}'.format(import_id))
        import_obj.process_import(cursor, uid, import_id, context=context)
        logger.info('End of Process lines from import file {0}'.format(
            import_id))

    def action_importar_f1(self, cursor, uid, ids, context=None):
        logger = logging.getLogger(
            'openerp.{}.action_importar_f1'.format(__name__)
        )
        if context is None:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        filename = ustr(wizard.filename)
        if not wizard.file:
            err_msg = _(u'Sel·leccioneu un fitxer')
            raise orm.except_orm(u'Atenció', err_msg)
        logger.info('Import new F1 file {0}'.format(
            filename
        ))
        import_id = self.create_import_file(
            cursor, uid, filename, context=context)
        logger.info('Create import F1 for file {0} with id: {1}'.format(
            filename, import_id
        ))

        data = base64.b64decode(wizard.file)
        fileHandle = StringIO.StringIO(data)
        is_xml = False
        try:
            import_file = zipfile.ZipFile(fileHandle, "r")
        except zipfile.BadZipfile, e:
            is_xml = True
            import_file = data

        if is_xml:
            self.create_import_line(
                cursor, uid, import_id, filename, import_file, context=context)
        else:
            self.import_zip(
                cursor, uid, import_id, import_file, context=context
            )
        self.create_payment_orders(
            cursor, uid, ids, import_file, context=context)
        self.process_file(cursor, uid, import_id, context=context)
        wiz_msg = _(u"Ha començat el procés d'importació en segon pla."
                    u" En finalitzar rebrà una request.")
        wizard.write({'state': 'done', 'info': wiz_msg})
        return True

    _defaults = {
        'state': lambda *a: 'init'
    }

WizardImportacioF1()

