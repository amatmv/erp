# -*- coding: utf-8 -*-

from osv import osv


class GiscedataFacturacioSwitchingLlistarFactures(osv.osv_memory):
    """Wizard per mostrar les factures des del form de linia"""
    _name = 'giscedata.facturacio.switching.llistar.factures'

    def action_get_factures(self, cursor, uid, ids, context=None):
        active_id = context['active_id']
        linia_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        linia_imp = linia_obj.browse(cursor, uid, active_id)
        factures = [lf.factura_id.id for lf in linia_imp.liniafactura_id]
        nom = len(linia_imp.name) > 15 and '%s...' % linia_imp.name[:15]\
                                                         or linia_imp.name
        return {
            'name': 'Factures creades de %s' % nom,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % str(tuple(factures)),
        }

GiscedataFacturacioSwitchingLlistarFactures()
