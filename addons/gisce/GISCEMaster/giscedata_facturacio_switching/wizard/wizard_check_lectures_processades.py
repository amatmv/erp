# -*- coding: utf-8 -*-

from osv import osv, fields


class WizardCheckLecturesProcessades(osv.osv_memory):

    def action_check_lectures_processades(self, cursor, uid, ids, context=None):
        linia_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        check_value = self.read(cursor, uid, ids, [])[0]['check_type']
        vals = {'lectures_processades': check_value}
        linia_obj.write(
            cursor, uid, context['active_ids'], vals, context=context
        )
        return {}

    _name = 'wizard.check.lectures.processades'

    _columns = {
        'check_type': fields.boolean('Lectures processades', required=True)
    }

    _defaults = {
        'check_type': True
    }


WizardCheckLecturesProcessades()
