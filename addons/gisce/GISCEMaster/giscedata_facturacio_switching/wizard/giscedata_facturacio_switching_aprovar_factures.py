# -*- coding: utf-8 -*-

from tools import config
from tools.translate import _

import pooler
from osv import osv, fields


class GiscedataFacturacioSwitchingAprovarFactures(osv.osv_memory):
    """Wizard per mostrar les factures des del form de linia"""
    _name = 'giscedata.facturacio.switching.aprovar.factura'

    def action_aprovar_factures(self, cursor, uid, ids, context=None):
        """Aprovem totes les factures que haguem filtrat (ids).
        """
        if context is None:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        wizard = self.browse(cursor, uid, ids[0])
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        linia_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        lf_obj = self.pool.get('giscedata.facturacio.importacio.linia.factura')
        imp_obj = self.pool.get('giscedata.facturacio.importacio')

        active_ids = context.get('active_ids', [])
        if context.get('model') == 'giscedata.facturacio.importacio':
            factures = []
            for linia in imp_obj.browse(cursor, uid, active_ids[0], context).linia_ids:
                for lf in linia.liniafactura_id:
                    factures.append(lf.factura_id)
        else:
            factures = factura_obj.browse(cursor, uid, active_ids)
        aprovades = []
        no_aprovades = []
        for factura in factures:
            if factura.tipo_rectificadora == 'BRA':
                continue
            # comprovar que la factura no és oberta
            if factura.state == 'open':
                no_aprovades.append(factura.id)
                continue
            if not factura.origin:
                no_aprovades.append(factura.id)
                continue
            # localitzar factura generada amb el mateix origen i emisor
            origen = factura.origin + '/comp'
            ch_vals = [('origin', '=', origen),
                       ('polissa_id.id', '=', factura.polissa_id.id)]
            f_fact = factura_obj.search(
                cursor, uid, ch_vals, context={'active_test': False}
            )
            # si es tracta d'un comparada no se'n fa cas
            if not f_fact and factura.origin[-5:] == '/comp':
                continue
            try:
                cr = db.cursor()
                #
                if factura.type == 'in_invoice':
                    check_total = factura.check_total
                    amount_total = factura.amount_total
                    if (wizard.open_1ct_diff_invoices and
                            check_total != amount_total and
                            abs(check_total - amount_total) <= 0.015):
                        # Accepts 1 ct diff invoices
                        factura_obj.write(
                            cr, uid, [factura.id],
                            {'check_total': factura.amount_total}
                        )
                    elif (wizard.open_negative_invoices and
                            check_total != amount_total and
                            abs(check_total) == abs(amount_total)):
                        # Accepts negative invoices
                        factura_obj.write(
                            cr, uid, [factura.id],
                            {'check_total': factura.amount_total}
                        )
                factura_obj.invoice_open(cr, uid, [factura.id])
                cr.commit()
                aprovades.append(factura.id)
            except Exception, e:
                cr.rollback()
                no_aprovades.append(factura.id)
                continue
            finally:
                cr.close()
            if f_fact:
                # esborrar les facturades amb el mateix origen
                lfid = lf_obj.search(cursor, uid, [('factura_id', 'in', f_fact)])
                lf_obj.unlink(cursor, uid, lfid)
                factura_obj.unlink(cursor, uid, f_fact)
            # setejar la línia de factura com a vàlida
            lf_id = lf_obj.search(cursor, uid, [('factura_id', '=', factura.id)])
            if not lf_id:
                continue
            l_id = lf_obj.read(cursor, uid, lf_id, ['linia_id'])[0]
            l_info = linia_obj.read(cursor, uid, l_id['linia_id'][0], ['info'])
            info = l_info['info'] + _(u' - Aprovat!')
            l_vals = {'state': 'valid', 'info': info}
            linia_obj.write(cursor, uid, l_id['linia_id'][0], l_vals)

        wizard.write({'num_factures_aprovades': len(aprovades),
                      'num_factures_no_aprovades': len(no_aprovades),
                      'fact_aprovades':
                                ', '.join(['%s' % x for x in aprovades]),
                      'fact_no_aprovades':
                                ', '.join(['%s' % x for x in no_aprovades]),
                      'state': 'end'})

    def action_get_factures(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        fact = wizard.fact_no_aprovades
        return {
            'name': _('Factures no aprovades'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.facturacio.factura',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % \
                            str(tuple([int(x) for x in fact.split(', ') if x])),
        }

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    _columns = {
        'num_factures_aprovades': fields.integer('Factures aprovades',
                                                 readonly=True),
        'num_factures_no_aprovades': fields.integer('Factures no aprovades',
                                                 readonly=True),
        'fact_aprovades': fields.text('Factures aprovades'),
        'fact_no_aprovades': fields.text('Factures no aprovades'),
        'open_1ct_diff_invoices': fields.boolean(
            u"Aprova amb error d'1 cèntim",
            help=u"Aprova la factura encara que la diferència entre el total "
                 u"i el residual sigui d'un cèntim"
        ),
        'open_negative_invoices': fields.boolean(
            u"Aprova factura negativa",
            help=u"Aprova la factura encara que sigui negativa"
        ),
        'state': fields.char('State', size=4),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'open_1ct_diff_invoices': lambda *a: False,
        'open_negative_invoices': lambda *a: False,
    }


GiscedataFacturacioSwitchingAprovarFactures()
