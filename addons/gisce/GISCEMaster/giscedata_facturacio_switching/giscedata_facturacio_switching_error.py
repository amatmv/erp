# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from gestionatr.input.messages.F1 \
    import agrupar_lectures_per_data, obtenir_data_inici_i_final
from enerdata.datetime.holidays import get_holidays
from osv import osv, fields
from gestionatr.helpers.funcions import PERIODES_AGRUPATS
from tools.translate import _
from tools import cache
from gestionatr.defs import TABLA_106, TABLA_110, CONTROL_POTENCIA
from addons.giscedata_facturacio.giscedata_facturacio import TARIFES
from addons.giscedata_facturacio.giscedata_polissa \
    import FACTURACIO_POTENCIA_SELECTION
from math import log10
import json

ERROR_LEVEL = [
    ('critical', _(u'Crític')),
    ('warning', _('Warnig')),
    ('information',_(u'Informatiu'))
]

class GiscedataFacturacioSwitchingErrorTemplate(osv.osv):

    _name = 'giscedata.facturacio.switching.error.template'

    _rec_name = 'code'

    @cache(30)
    def get_error(self, cursor, uid, phase, code, context=None):
        if context is None:
            context = {}
        ctx = context.copy()
        ctx['active_test'] = False

        error_data = None
        error_id = self.search(
            cursor, uid, [('code', '=', code), ('phase', '=', phase)],
            limit=1, context=ctx
        )
        if error_id:
            error_data = self.read(
                cursor, uid, error_id[0], ['description', 'level'],
                context=ctx
            )
        return error_data

    def _ff_desactivable(self, cursor, uid, ids, field_name, ar, context=None):
        if context is None:
            context = {}

        res = {}
        for error_vals in self.read(cursor, uid, ids, ['method', 'level']):
            # The validation will only be desactivable if it's not critical and
            # it is linked to a method
            res[error_vals['id']] = bool(
                error_vals['level'] != 'critical' and error_vals['method']
            )
        return res

    def _desactivable_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        if args[0][2]:
            domain = [
                ('level', '!=', 'critical'), ('method', '!=', False),
                ('method', '!=', '')
            ]
        else:
            domain = [
                '|',
                '|',
                ('level', '=', 'critical'),
                ('method', '=', False),
                ('method', '=', '')
            ]
        return domain

    def _ff_parameters(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}

        res = {}
        for import_vals in self.read(cursor, uid, ids, ['parameters']):
            res[import_vals['id']] = json.dumps(
                import_vals['parameters'], indent=4
            )
        return res

    def _fi_parameters(self, cursor, uid, ids, name, value, arg, context=None):
        if not context:
            context = {}

        try:
            parameters = json.loads(value)
            self.write(cursor, uid, ids, {'parameters': parameters})
        except ValueError as e:
            pass

    def check_correct_json(self, cursor, uid, ids, parameters_text):
        try:
            parameters = json.loads(parameters_text)
        except ValueError as e:
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres entrats no tenen un format '
                                 'correcte de JSON')
                }
            }
        if not isinstance(parameters, dict):
            return {
                'warning': {
                    'title': _(u'Atenció'),
                    'message': _('Els parametres han de ser un diccionari')
                }
            }
        return {}

    _columns = {
        'code': fields.char(
            'Codi', size=4, required=True, readonly=True
        ),
        'phase': fields.char(
            'Fase', size=10, required=True, readonly=True
        ),
        'level': fields.selection(
            ERROR_LEVEL, string='Nivell', required=True, readonly=True
        ),
        'description': fields.char(
            u'Descripció error', size=1024, required=True, readonly=True,
            translate=True
        ),
        'observation': fields.text(u'Observació', readonly=True),
        'method': fields.char(
            u'Mètode a executar per comprovar aquest warning', size=100,
            readonly=True
        ),
        'active': fields.boolean('Actiu', readonly=False),
        'desactivable': fields.function(
            _ff_desactivable, type='boolean', method=True,
            string="Es pot desactivar", fnct_search=_desactivable_search,
        ),
        'parameters': fields.json('Parametres', readonly=False),
        'parameters_text': fields.function(
            _ff_parameters, type='text', method=True, string='Parametres',
            fnct_inv=_fi_parameters, readonly=False
        ),
    }

    _defaults = {
        'active': lambda *a: True,
        'method': lambda *a: '',
        'description': lambda *a: '',
        'level': lambda *a: 'warning',
    }

GiscedataFacturacioSwitchingErrorTemplate()


class GiscedataFacturacioSwitchingError(osv.osv):

    def _get_level(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}

        templ_obj = self.pool.get(
            'giscedata.facturacio.switching.error.template'
        )
        res = {}
        for vals in self.read(cursor, uid, ids, ['error_template_id']):
            res[vals['id']] = templ_obj.read(
                cursor, uid, vals['error_template_id'][0], ['level'], context
            )['level']

        return res

    _name = 'giscedata.facturacio.switching.error'

    _columns = {
        'name': fields.char(
            'Codi error', size=14, required=True, readonly=True, select=True
        ),
        'message': fields.char(
            "Missatge d'error", size=2048, required=True, readonly=True
        ),
        'error_template_id': fields.many2one(
            'giscedata.facturacio.switching.error.template',
            string='Error plantilla', select=1, readonly=True
        ),
        'line_id': fields.many2one(
            'giscedata.facturacio.importacio.linia',
            string=u"Línia d'importació", select=1, readonly=True
        ),
        'active': fields.boolean('Actiu', readonly=True, select=1),
        'level': fields.related(
            'error_template_id', 'level', string=u"Nivell", type='selection',
            selection=ERROR_LEVEL, readonly=True
        ),
    }

    def get_error_text(self, cursor, uid, error_id, context=None):
        """
        Gets Error text from line errors
        :param line_id:
        :return: text with error as
        [code]: message
        """
        vals = self.read(cursor, uid, error_id, ['name', 'message', 'level'])
        txt = '[{0}] {1}'.format(vals['name'], vals['message'])

        if vals['level'] == 'critical':
            txt = u'* {0}'.format(txt)

        return txt

    def create_error_from_code(self, cursor, uid, line_id,
                               phase, code, values, context=None):
        if context is None:
            context = {}

        error_template = self.pool.get(
            'giscedata.facturacio.switching.error.template')

        ctx = context.copy()
        conf_obj = self.pool.get('res.config')
        force_lang = int(conf_obj.get(cursor, 1, 'f1_errors_force_lang', '1'))
        if force_lang:
            new_lang = self.get_lang_of_f1(cursor, uid, line_id, ctx)
            if new_lang:
                ctx['lang'] = new_lang

        error_temp = error_template.get_error(
            cursor, uid, phase, code, context=ctx)
        if error_temp:
            vals = {
                'name': '{0}{1}'.format(phase, code),
                'message': error_temp['description'].format(**values),
                'error_template_id': error_temp['id'],
                'line_id': line_id
            }
            error_id = self.create(cursor, uid, vals)

            return {
                'error_id': error_id,
                'level': error_temp['level'],
                'code': code
            }
        return False

    def get_lang_of_f1(self, cursor, uid, line_id, context=None):
        line_obj = self.pool.get("giscedata.facturacio.importacio.linia")
        partner = line_obj.read(cursor, uid, line_id, ['distribuidora_id'])
        if not partner['distribuidora_id']:
            imp_con_obj = self.pool.get('giscedata.facturacio.switching.config')
            code = line_obj.read(cursor, uid, line_id, ['ree_source_code'])
            if not code['ree_source_code']:
                return False
            try:
                partner = imp_con_obj.get_emissor(cursor, uid, code['ree_source_code'])
            except:
                pass
            finally:
                if not partner:
                    return False
        else:
            partner = partner['distribuidora_id'][0]

        res_partner_obj = self.pool.get("res.partner")
        info = res_partner_obj.read(
            cursor, uid, partner, {'lang'}, context
        )
        return info['lang']

    _defaults = {
        'active': lambda *a: True,
    }

GiscedataFacturacioSwitchingError()


class GiscedataFacturacioSwitchingValidator(osv.osv):
    _name = 'giscedata.facturacio.switching.validator'
    _desc = 'Validador de F1'

    def get_checks(self, cursor, uid, phase=None, context=None):
        if context is None:
            context = {}

        template_obj = self. pool.get(
            'giscedata.facturacio.switching.error.template'
        )

        search_parameters = [
            ('active', '=', True), ('method', '!=', False), ('method', '!=', '')
        ]
        if phase:
            search_parameters.append(('phase', '=', phase))

        template_ids = template_obj.search(
            cursor, uid, search_parameters, order='code'
        )

        return template_ids

    def run_check(self, cursor, uid, line_id, f1, phase, error_code,
                  context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.importacio.linia')
        template_obj = self.pool.get(
            'giscedata.facturacio.switching.error.template'
        )

        line = fact_obj.browse(cursor, uid, line_id)

        search_parameters = [
            ('active', '=', True),
            ('method', '!=', False),
            ('phase', '=', phase),
            ('code', '=', error_code)
        ]

        check_ids = template_obj.search(cursor, uid, search_parameters)

        return self.execute_checks(
            cursor, uid, line, f1, check_ids, phase, context
        )

    def validate_f1(self, cursor, uid, line_id, f1, phase, context=None):
        if context is None:
            context = {}

        fact_obj = self.pool.get('giscedata.facturacio.importacio.linia')

        line = fact_obj.browse(cursor, uid, line_id)

        check_ids = self.get_checks(
            cursor, uid, phase=phase, context=context
        )
        error_ids, has_critical = self.execute_checks(
            cursor, uid, line, f1, check_ids, phase, context
        )

        return error_ids, has_critical

    def execute_checks(self, cursor, uid, line, f1, template_ids, phase,
                       context=None):
        if context is None:
            context = {}

        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        template_obj = self.pool.get(
            'giscedata.facturacio.switching.error.template'
        )

        has_critical = False
        error_ids = []

        template_values = template_obj.read(
            cursor, uid, template_ids, ['code', 'method', 'level', 'parameters']
        )
        for template_vals in template_values:
            check_method = getattr(self, template_vals['method'], None)
            if check_method:
                try:
                    vals_list = check_method(
                        cursor, uid, line, f1, template_vals['parameters']
                    )
                    if vals_list:
                        for vals in vals_list:
                            error_ids.append(
                                error_obj.create_error_from_code(
                                    cursor, uid, line.id, phase,
                                    template_vals['code'], vals, context=context
                                )
                            )

                        if template_vals['level'] == 'critical':
                            has_critical = True
                except Exception as e:
                    error_ids.append(
                        error_obj.create_error_from_code(
                            cursor, uid, line.id, phase, '999',
                            {
                                'text': '[{0}] {1}'.format(
                                    template_vals['code'], e
                                )
                            }
                        )
                    )
                    has_critical = True
            else:
                # If we didn't find the method to be executed we add the error
                # that indicates that, which is 998

                # We say what are the check number and method that didn't work
                vals = {
                    'check_number': template_vals['code'],
                    'method': template_vals['method']
                }
                error_ids.append(
                    error_obj.create_error_from_code(
                        cursor, uid, line.id, phase, '998', vals,
                        context=context
                    )
                )

        return error_ids, has_critical

    def check_turns(self, cursor, uid, line, f1, parameters=None):
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = []
        for atr_inv in f1.facturas_atr:
            start_date = datetime.strptime(
                atr_inv.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                atr_inv.datos_factura.fecha_hasta_factura
            )

            for compt_xml in atr_inv.get_comptadors():
                compt_id = compt_obj.search_with_contract(
                    cursor, uid, compt_xml.numero_serie, polissa_id
                )

                if compt_id:
                    req_giro = max(
                        [
                            lect_xml.gir_comptador
                            for lect_xml in compt_xml.get_lectures()
                        ]
                    )

                    compt_vals = compt_obj.read(
                        cursor, uid, compt_id[0], ['giro']
                    )

                    if compt_vals['giro'] != req_giro:
                        errors.append(
                            {
                                'meter_name': compt_xml.numero_serie,
                                'database_turn': compt_vals['giro'],
                                'req_turn': req_giro,
                            }
                        )

        return errors

    def check_meter(self, cursor, uid, line, f1, parameters=None):
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = []
        for atr_inv in f1.facturas_atr:
            start_date = datetime.strptime(
                atr_inv.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                atr_inv.datos_factura.fecha_hasta_factura
            )

            for compt_xml in atr_inv.get_comptadors():
                compt_id = compt_obj.search_with_contract(
                    cursor, uid, compt_xml.numero_serie, polissa_id
                )

                if not compt_id:
                    errors.append({'meter_name': compt_xml.numero_serie})

        return errors

    def check_repeated_meters(self, cursor, uid, line, f1, parameters=None):
        errors = []
        for atr_inv in f1.facturas_atr:
            meters = []
            for compt_xml in atr_inv.get_comptadors():
                new_meter = compt_xml.numero_serie.lstrip('0')
                if new_meter in meters:
                    errors.append({'meter_name': compt_xml.numero_serie})
                else:
                    meters.append(new_meter)

        return errors

    def check_separed_meters(self, cursor, uid, line, f1, parameters=None):
        errors = []
        for atr_inv in f1.facturas_atr:
            for compt_xml in atr_inv.get_comptadors():
                if not compt_xml.get_lectures_activa():
                    errors.append(
                        {
                            'meter_name': compt_xml.numero_serie
                        }
                    )
        return errors

    def check_extra_active_meters(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')
        compt_obj = self.pool.get('giscedata.lectures.comptador')

        active_meters = []
        errors = []

        for atr_inv in f1.facturas_atr:
            start_date = datetime.strptime(
                atr_inv.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                atr_inv.datos_factura.fecha_hasta_factura
            )
            ctx = {'date': data_inici}
            comptadors = polissa_obj.read(
                cursor, uid, polissa_id, ['comptadors'], context=ctx
            )['comptadors']

            for compt in compt_obj.browse(cursor, uid, comptadors):
                if compt.active:
                    active_meters.append(compt)

            if len(active_meters) > 1:
                errors.append(
                    {
                        'meter_names': str(
                            [met.name for met in active_meters]
                        )
                    }
                )
        return errors

    def check_cch_type(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')

        no_tg_tariffs = [
            '3.1A', '3.1A LB', '6.1', '6.1A',
            '6.1B', '6.2', '6.3', '6.4', '6.5'
        ]

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                fact_xml.datos_factura.fecha_hasta_factura
            )

            ctx = {'date': data_inici}
            polissa_vals = polissa_obj.read(
                cursor, uid, polissa_id, ['tg', 'tarifa'], context=ctx
            )
            polissa_tg = polissa_vals['tg']
            tarifa_polissa = polissa_vals['tarifa'][1]

            fact_tg = fact_xml.datos_factura.indicativo_curva_carga

            expected = None
            if tarifa_polissa not in no_tg_tariffs:
                # If the contract can have TG
                if polissa_tg == '1':
                    # If the contract has TG
                    if fact_tg != '01':
                        expected = _('01: Acompanya corba de carrega')
                else:
                    # If the contract does not have TG
                    if fact_tg == '01':
                        if parameters['modify_cch'] == 1:
                            # Set polissa state as modcontractual
                            polissa_obj.send_signal(
                                cursor, uid, [polissa_id], 'modcontractual'
                            )
                            # Update telegestio field
                            params = {'tg': '1'}
                            polissa_obj.write(cursor, uid, [polissa_id], params)
                            # Modify de current modcontractual
                            wz_crear_contracte_obj = self.pool.get(
                                'giscedata.polissa.crear.contracte'
                            )
                            params = {'duracio': 'actual', 'accio': 'modificar'}

                            try:
                                wz_id = wz_crear_contracte_obj.create(
                                    cursor, uid, params,
                                    context={'active_id': polissa_id}
                                )
                            except Exception as e:
                                raise osv.except_osv(_(u"ERROR"), e.message)

                            wz_inst = wz_crear_contracte_obj.browse(
                                cursor, uid, wz_id
                            )
                            wz_inst.action_crear_contracte()

                        expected = '{0} o {1}'.format(
                            _('02: Perfilat'), _('03: No aplica')
                        )
            else:
                # If the contract can't have TG
                if fact_tg != '03':
                    expected = _('03: No aplica')

            if expected:
                xml_value = fact_tg + ': ' + dict(TABLA_110)[fact_tg]
                errors.append(
                    {
                        'expected_value': expected,
                        'xml_value': xml_value,
                    }
                )

        return errors

    def calculate_divergences_reading(self, cursor, uid, tariff_id, compt_id,
                                      periode, tipus, numero_serie,
                                      reading_xml, overwrite_atr):
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        origen_comer_obj = self.pool.get('giscedata.lectures.origen_comer')

        reading = lect_helper.get_reading_model_id(
            cursor, uid, compt_id, reading_xml.fecha, periode, tipus, tariff_id
        )
        if reading:
            lect_obj = self.pool.get(reading['model'])

            read_fields = ['lectura', 'origen_comer_id']
            if tipus == 'EP':
                read_fields.append('exces')

            lect_vals = lect_obj.read(
                cursor, uid, reading['id'], read_fields
            )

            lectura_bd = lect_vals['lectura']
            if tipus == 'EP':
                lectura_bd = lect_vals['exces']

            lectura_xml = lect_helper.set_uom_lectura(
                cursor, uid, reading_xml.lectura, tipus, compt_id,
                context={'from_f1': True}
            )
            if tipus in ['A', 'R']:
                # If they are of energy type they cannot have decimals, so
                # if they do we ignore them
                lectura_bd = int(lectura_bd)
                lectura_xml = int(lectura_xml)
            if lectura_bd != lectura_xml:
                origen_comer_code = origen_comer_obj.read(
                    cursor, uid, lect_vals['origen_comer_id'][0], ['codi']
                )['codi']
                bd_has_at_ori = origen_comer_code == 'AT'
                if not overwrite_atr or not bd_has_at_ori:
                    reading_date = reading_xml.fecha
                    return {
                        'meter_name': numero_serie,
                        'type': tipus,
                        'period': periode,
                        'reading_date': reading_date,
                        'reading_value': lectura_bd,
                        'f1_value': lectura_xml,
                    }

        return None

    def calculate_divergences_meter(self, cursor, uid, polissa_id, tariff_id,
                                    compt_xml, overwrite_atr):
        compt_obj = self.pool.get('giscedata.lectures.comptador')

        errors = []

        compt_id = compt_obj.search_with_contract(
            cursor, uid, compt_xml.numero_serie, polissa_id
        )
        if not compt_id:
            return []
        compt_id = compt_id[0]

        readed_periods = {}
        totalitzador_periods = []
        for lect_xml in compt_xml.get_lectures():
            # We will process totalitzador perdios at the end to be able to
            # check if we need them
            if lect_xml.codigo_periodo in ['10', '20', '80']:
                totalitzador_periods.append(lect_xml)
                continue

            # Store readed periods to make the check about totalitzadors
            if not readed_periods.get(lect_xml.magnitud):
                readed_periods[lect_xml.magnitud] = []
            readed_periods[lect_xml.magnitud].append(lect_xml.periode)

            # Check lect.
            errors.extend(
                self.process_lect_check(
                    cursor, uid, lect_xml, tariff_id, compt_id, compt_xml,
                    overwrite_atr
                )
            )

        # Proces totalitzador periods only if we need them
        for lect_xml in totalitzador_periods:
            period = lect_xml.periode
            magnitud = lect_xml.magnitud
            if (not readed_periods.get(magnitud)
                or readed_periods[magnitud] and period not in readed_periods[magnitud]):
                errors.extend(
                    self.process_lect_check(
                        cursor, uid, lect_xml, tariff_id, compt_id, compt_xml,
                        overwrite_atr
                    )
                )

        return errors

    def process_lect_check(self, cursor, uid, lect_xml, tariff_id, compt_id,
                           compt_xml, overwrite_atr):
        errors = []
        if lect_xml.magnitud in ('PM', 'EP'):
            # For maximeters or power excess we only check end readings
            readings_to_check = [lect_xml.lectura_hasta]
        else:
            # For start readings and end readings
            readings_to_check = [
                lect_xml.lectura_desde, lect_xml.lectura_hasta
            ]

        for reading in readings_to_check:
            error = self.calculate_divergences_reading(
                cursor, uid, tariff_id, compt_id, lect_xml.periode,
                lect_xml.tipus, compt_xml.numero_serie, reading,
                overwrite_atr
            )

            if error:
                errors.append(error)

        return errors

    def check_divergence(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        res_config = self.pool.get('res.config')

        overwrite_atr = bool(int(
            res_config.get(
                cursor, uid, 'lect_sw_overwrite_atr_measure_when_found', '0'
            )
        ))
    
        errors = []

        for atr_inv in f1.facturas_atr:
            start_date = datetime.strptime(
                atr_inv.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                atr_inv.datos_factura.fecha_hasta_factura
            )
            if atr_inv.datos_factura.tipo_factura in ['N', 'G', 'A']:
                is_low = atr_inv.datos_factura.marca_medida_con_perdidas == 'S'
                tariff_id = tariff_obj.get_tarifa_from_ocsum(
                    cursor, uid, atr_inv.datos_factura.tarifa_atr_fact, is_low
                )
    
                for compt_xml in atr_inv.get_comptadors():
                    errors += self.calculate_divergences_meter(
                        cursor, uid, polissa_id, tariff_id, compt_xml, 
                        overwrite_atr
                    )
    
        return errors

    def check_period_number(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')

        TYPES_TO_CHECK = ['A', 'R', 'M', 'EP']

        errors = []

        for fact_xml in f1.facturas_atr:
            data_inici = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            # We use inclusive dates but in the F1 the start date should be
            # exclusive, so we subtract a day.
            # This also makes num_dies calculation correct
            data_inici += timedelta(days=1)

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici.strftime('%Y-%m-%d'),
                fact_xml.datos_factura.fecha_hasta_factura
            )

            ctx = {'date': data_inici.strftime('%Y-%m-%d')}
            polissa_vals = polissa_obj.read(
                cursor, uid, polissa_id, ['tarifa', 'facturacio_potencia'],
                context=ctx
            )
            tariff_id = polissa_vals['tarifa'][0]

            tariff = tarifa_obj.browse(cursor, uid, tariff_id)
            active_periods = set(tariff.get_periodes(tipus='te'))
            reactive_periods = set(tariff.get_periodes(tipus='tr'))
            maximeter_periods = set(tariff.get_periodes(tipus='tp'))
            excess_periods = set(tariff.get_periodes(tipus='ep'))
            expected_periods = {}

            if active_periods:
                expected_periods['A'] = active_periods

            reactive_periods_required = not tariff.name.startswith('2')
            if reactive_periods and reactive_periods_required:
                expected_periods['R'] = reactive_periods

            maximeter_contract = polissa_vals['facturacio_potencia'] == 'max'
            is_6_tariff = tariff.name.startswith('6')
            max_periods_required = maximeter_contract and not is_6_tariff
            expected_periods['M'] = maximeter_periods

            excess_periods_required = tariff.name.startswith('6')
            if excess_periods and excess_periods_required:
                expected_periods['EP'] = maximeter_periods

            react_meters = []
            for error in self.check_separed_meters(cursor, uid, line, f1):
                react_meters.append(error['meter_name'])

            for compt_xml in fact_xml.get_comptadors():
                read = {}
                for lect_xml in compt_xml.get_lectures():
                    if lect_xml.periode:
                        # The readings are the old readings (or an empty set if
                        # we hadn't added any yet) plus the current one
                        read.setdefault(
                            lect_xml.tipus, set()
                        ).add(lect_xml.periode)

                if read.keys() == ['S']:
                    continue
                for tipus in TYPES_TO_CHECK:
                    if tipus != 'R' and compt_xml.numero_serie in react_meters:
                        continue
                    if tipus in expected_periods and tipus in read:
                        periods_read = read[tipus]

                        extras = sorted(periods_read - expected_periods[tipus])
                        missing = sorted(expected_periods[tipus] - periods_read)

                        # If we don't need 'M' periods, we only check that
                        # there are no extra periods to avoid future errors
                        # when trying to proces the lectures
                        if tipus == 'M' and not max_periods_required:
                            missing = []

                        if fact_xml.datos_factura.tarifa_atr_fact in ['003', '011']:
                            # If the tariff is a 3.0A or a 3.1A
                            if missing == ['P4', 'P5', 'P6']:
                                missing = []
                        if tipus == 'M' and fact_xml.datos_factura.tarifa_atr_fact in ['004']:
                            # If the tariff is a 2.0DHA
                            # Some distris send 2 periods of 'M' in the 2.0DHA,
                            # we ignore this error and later when loading
                            # measures we will use the greater.
                            if extras == ['P2']:
                                extras = []
                        if tipus == 'M' and fact_xml.datos_factura.tarifa_atr_fact in ['007']:
                            # If the tariff is a 2.0DHS
                            # Some distris send 3 periods of 'M' in the 2.0DHS,
                            # we ignore this error and later when loading
                            # measures we will use the greater.
                            if extras == ['P2', 'P3']:
                                extras = []

                        if extras or missing:
                            errors.append(
                                {
                                    'meter_name': compt_xml.numero_serie,
                                    'extras': '[' + ', '.join(extras) + ']',
                                    'missing': '[' + ', '.join(missing) + ']',
                                    'type': tipus,
                                }
                            )
                    elif tipus in expected_periods:
                        if tipus == 'M' and not max_periods_required:
                            continue
                        missing = '[' + ', '.join(expected_periods[tipus]) + ']'
                        errors.append(
                            {
                                'meter_name': compt_xml.numero_serie,
                                'extras': '[]',
                                'missing': missing,
                                'type': tipus,
                            }
                        )

        return errors

    def check_negative_consumes(self, curosr, uid, line, f1, parameters=None):
        errors = []

        for fact_xml in f1.facturas_atr:
            for compt_xml in fact_xml.get_comptadors():
                for lect_xml in compt_xml.get_lectures():
                    if lect_xml.consumo_calculado < 0:
                        errors.append(
                            {
                                'type': lect_xml.tipus,
                                'period': lect_xml.periode,
                                'meter_name': compt_xml.numero_serie,
                                'value': lect_xml.consumo_calculado
                            }
                        )
        return errors

    def check_adjust(self, cursor, uid, line, f1, parameters=None):
        errors = []

        for fact_xml in f1.facturas_atr:
            for compt_xml in fact_xml.get_comptadors():
                for lect_xml in compt_xml.get_lectures():
                    if lect_xml.ajuste:
                        ajust = lect_xml.ajuste

                        errors.append(
                            {
                                'type': lect_xml.tipus,
                                'period': lect_xml.periode,
                                'meter_name': compt_xml.numero_serie,
                                'adjust': ajust.ajuste_por_integrador,
                                'reason': dict(TABLA_106)[ajust.codigo_motivo],
                            }
                        )
        return errors

    def check_turn(self, cursor, uid, line, f1, parameters=None):
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        compt_obj = self.pool.get('giscedata.lectures.comptador')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')

            polissa_id = lect_helper.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                fact_xml.datos_factura.fecha_hasta_factura
            )

            for compt_xml in fact_xml.get_comptadors():
                compt_id = compt_obj.search_with_contract(
                    cursor, uid, compt_xml.numero_serie, polissa_id
                )
                if compt_id:
                    compt_id = compt_id[0]
                    for lect_xml in compt_xml.get_lectures_energia():
                        initial_value = lect_helper.set_uom_lectura(
                            cursor, uid, lect_xml.lectura_desde.lectura,
                            lect_xml.tipus, compt_id,
                            context={'from_f1': True}
                        )
                        end_value = lect_helper.set_uom_lectura(
                            cursor, uid, lect_xml.lectura_hasta.lectura,
                            lect_xml.tipus, compt_id,
                            context={'from_f1': True}
                        )
                        calculated_consume = end_value - initial_value
                        if calculated_consume < 0:
                            errors.append(
                                {
                                    'type': lect_xml.tipus,
                                    'end_date': lect_xml.lectura_hasta.fecha,
                                    'period': lect_xml.periode,
                                    'meter_name': compt_xml.numero_serie,
                                    'end_value': end_value,
                                    'start_date': lect_xml.lectura_desde.fecha,
                                    'start_value': initial_value,
                                    'consume': calculated_consume,
                                }
                            )
        return errors

    def check_sobreescritura_rectificadora(self, cursor, uid, line, f1, parameters=None):
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')

        errors = []

        for atr_inv in f1.facturas_atr:
            start_date = datetime.strptime(
                atr_inv.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_helper.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                atr_inv.datos_factura.fecha_hasta_factura
            )

            if atr_inv.datos_factura.tipo_factura == 'R':
                is_low = atr_inv.datos_factura.marca_medida_con_perdidas == 'S'
                tariff_id = tariff_obj.get_tarifa_from_ocsum(
                    cursor, uid, atr_inv.datos_factura.tarifa_atr_fact, is_low
                )

                for compt_xml in atr_inv.get_comptadors():
                    compt_id = compt_obj.search_with_contract(
                        cursor, uid, compt_xml.numero_serie, polissa_id
                    )
                    if compt_id:
                        compt_id = compt_id[0]
                        for lect_xml in compt_xml.get_lectures():
                            reading_xml = lect_xml.lectura_hasta

                            original_value = lect_helper.get_reading_model_id(
                                cursor, uid, compt_id, reading_xml.fecha,
                                lect_xml.periode, lect_xml.tipus, tariff_id
                            )
                            new_value = lect_helper.set_uom_lectura(
                                cursor, uid, reading_xml.lectura,
                                lect_xml.tipus, compt_id,
                                context={'from_f1': True}
                            )
                            errors.append(
                                {
                                    'type': lect_xml.tipus,
                                    'end_date': reading_xml.fecha,
                                    'period': lect_xml.periode,
                                    'meter_name': compt_xml.numero_serie,
                                    'original_value': original_value,
                                    'new_value': new_value,
                                }
                            )
        return errors

    def check_origen_is_99(self, cursor, uid, line, f1, parameters=None):
        errors = []

        for fact_xml in f1.facturas_atr:
            for compt_xml in fact_xml.get_comptadors():
                for lect_xml in compt_xml.get_lectures():
                    if lect_xml.lectura_hasta.procedencia == '99':
                        errors.append(
                            {
                                'type': lect_xml.tipus,
                                'period': lect_xml.periode,
                                'meter_name': compt_xml.numero_serie,
                            }
                        )
        return errors

    def check_f1_turn_is_enough(self, cursor, uid, line, f1, parameters=None):
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        compt_obj = self.pool.get('giscedata.lectures.comptador')

        errors = []

        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')

            polissa_id = lect_helper.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                fact_xml.datos_factura.fecha_hasta_factura
            )

            for compt_xml in fact_xml.get_comptadors():
                compt_id = compt_obj.search_with_contract(
                    cursor, uid, compt_xml.numero_serie, polissa_id
                )
                if compt_id:
                    compt_id = compt_id[0]
                    for lect_xml in compt_xml.get_lectures():
                        lectura_final = lect_helper.set_uom_lectura(
                            cursor, uid, lect_xml.lectura_hasta.lectura,
                            lect_xml.tipus, compt_id,
                            context={'from_f1': True}
                        )
                        if int(lectura_final) > 0:
                            req_length = int(log10(int(lectura_final))) + 1
                        else:
                            req_length = 1
                        req_gir = 10 ** req_length
                        if lect_xml.gir_comptador < req_gir:
                            errors.append(
                                {
                                    'type': lect_xml.tipus,
                                    'period': lect_xml.periode,
                                    'meter_name': compt_xml.numero_serie,
                                    'given_turn': lect_xml.gir_comptador,
                                    'reading': lectura_final,
                                    'required_turn': req_gir,
                                }
                            )
        return errors

    def check_importing_units(self, cursor, uid, line, f1, parameters=None):
        f1config_obj = self.pool.get('giscedata.facturacio.switching.config')
        product_uom = self.pool.get('product.uom')

        f1_config = None
        try:
            f1_config = f1config_obj.get_config(
                cursor, uid, line.distribuidora_id.ref
            )
        except:
            pass

        if not f1_config:
            return []

        units = {
            'power': product_uom.read(
                cursor, uid, f1_config['uom_potencia_f'], ['name']
            )['name'],
            'power_price': product_uom.read(
                cursor, uid, f1_config['preu_potencia_f'], ['name']
            )['name'],
            'maximeter': product_uom.read(
                cursor, uid, f1_config['uom_fact_max_f'], ['name']
            )['name'],
            'energy': product_uom.read(
                cursor, uid, f1_config['uom_energia_f'], ['name']
            )['name'],
            'energy_price': product_uom.read(
                cursor, uid, f1_config['preu_energia_f'], ['name']
            )['name'],
            'rent_price': product_uom.read(
                cursor, uid, f1_config['preu_lloguer_f'], ['name']
            )['name'],
        }
        for unit in units:
            units[unit] = '\t' + units[unit].ljust(4) + '\t'
        return [units]

    def get_impossible_consumes_by_meter(self, cursor, uid, polissa,
                                         compt_xml):
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')

        polissa_id = polissa.id
        contracted_power = {}
        for cont_pow in polissa.potencies_periode:
            contracted_power[cont_pow.periode_id.name] = cont_pow.potencia

        tariff = TARIFES[polissa.tarifa.name]
        aggregated_to = dict([(p[1], p[0]) for p in PERIODES_AGRUPATS])

        errors = []

        compt_id = compt_obj.search_with_contract(
            cursor, uid, compt_xml.numero_serie, polissa_id
        )

        if compt_id:
            compt_id = compt_id[0]
            for lect_xml in compt_xml.get_lectures(tipus=['A']):
                if lect_xml.periode:
                    inici = datetime.strptime(
                        lect_xml.lectura_desde.fecha, '%Y-%m-%d'
                    )
                    final = datetime.strptime(
                        lect_xml.lectura_hasta.fecha, '%Y-%m-%d'
                    )

                    holidays = get_holidays(inici.year)
                    holidays = holidays | get_holidays(final.year)

                    # We don't need this so we send empty containers
                    consumes = []
                    maximeters = {}

                    period = tariff(
                        consumes, maximeters, inici, final,
                        data_inici_periode=inici, data_final_periode=final
                    ).energy_periods.get(lect_xml.periode, None)
                    if period:
                        hours = period.get_hours_between_dates(
                            inici, final, holidays
                        )

                        periode = lect_xml.periode
                        if periode not in contracted_power:
                            periode = aggregated_to.get(periode, '-')
                        period_power = contracted_power.get(periode, 0)
                        maximum_consume = hours * period_power

                        lect_fin = lect_helper.set_uom_lectura(
                            cursor, uid, lect_xml.lectura_hasta.lectura,
                            lect_xml.tipus, compt_id,
                            context={'from_f1': True}
                        )
                        lect_ini = lect_helper.set_uom_lectura(
                            cursor, uid, lect_xml.lectura_desde.lectura,
                            lect_xml.tipus, compt_id,
                            context={'from_f1': True}
                        )
                        consume = lect_fin - lect_ini

                        if consume < 0:
                            giro = compt_obj.read(
                                cursor, uid, compt_id, ['giro']
                            )['giro']
                            consume += giro

                        if consume > maximum_consume:
                            reading_date = lect_xml.lectura_hasta.fecha

                            errors.append(
                                {
                                    'period': lect_xml.periode,
                                    'meter_name': compt_xml.numero_serie,
                                    'reading_date': reading_date,
                                    'consume': consume,
                                }
                            )
        return errors

    def check_impossible_consume(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                fact_xml.datos_factura.fecha_hasta_factura
            )

            polissa = polissa_obj.browse(cursor, uid, polissa_id)

            for compt_xml in fact_xml.get_comptadors():
                errors += self.get_impossible_consumes_by_meter(
                    cursor, uid, polissa, compt_xml
                )

        return errors

    def check_consum_incoherent(self, cursor, uid, line, f1, parameters=None):
        errors = []
        for fact_xml in f1.facturas_atr:
            if fact_xml.datos_factura.tipo_factura == "G":
                continue
            for compt_xml in fact_xml.get_comptadors():
                errors += self.check_impossible_consume_by_meter(
                    cursor, uid, compt_xml
                )

        return errors

    def check_impossible_consume_by_meter(self, cursor, uid, compt_xml):
        errors = []
        for lect_xml in compt_xml.get_lectures(tipus=['A', 'R']):
            if lect_xml.periode:
                consume = lect_xml.lectura_hasta.lectura - lect_xml.lectura_desde.lectura
                ajust = 0
                if lect_xml.ajuste:
                    consume += lect_xml.ajuste.ajuste_por_integrador
                    ajust = lect_xml.ajuste.ajuste_por_integrador
                if consume < 0:
                    giro = lect_xml.gir_comptador
                    consume += giro
                if round(consume, 2) != round(lect_xml.consumo_calculado, 2):
                    errors.append(
                        {
                            'period': lect_xml.periode,
                            'meter_name': compt_xml.numero_serie,
                            'reading_date': lect_xml.lectura_hasta.fecha,
                            'consum': lect_xml.consumo_calculado,
                            'lect1': lect_xml.lectura_hasta.lectura,
                            'lect2': lect_xml.lectura_desde.lectura,
                            'ajust': ajust,
                            'total_consum': consume
                        }
                    )
        return errors

    def check_consum_facturat_incoherent(self, cursor, uid, line, f1,
                                         parameters=None):
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        lect_helper = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')
        errors = []
        consums = []
        consums_facturats = []
        for fact_xml in f1.facturas_atr:
            # Les de lectura en baixa no cuadrarà el mesurat amb el facturat,
            # per tant no ho podem comprovar
            if fact_xml.datos_factura.marca_medida_con_perdidas == 'S':
                continue
            # Per les regularitzadores i complementaries tampoc
            if fact_xml.datos_factura.tipo_factura in ["G", "C"]:
                continue
            # Per les anuladores de regularitzadores i complementaries
            if fact_xml.datos_factura.tipo_factura in ('A', 'R'):
                refunded_origin = fact_xml.datos_factura.codigo_factura_rectificada_anulada
                fact_refunded = fact_obj.search(cursor, uid, [
                    ('origin', 'like', '%{}%'.format(refunded_origin)),
                    ('tipo_rectificadora', 'in', ('C', 'G')),
                ])
                if fact_refunded:
                    continue
            # Per autoconsum tampoc
            if fact_xml.te_autoconsum():
                continue
            # Per els eventuales a tanto alzado tampoc
            if fact_xml.datos_factura.fecha_desde_factura:
                start_date = datetime.strptime(fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d')
                start_date += timedelta(days=1)
                data_inici = start_date.strftime('%Y-%m-%d')
                polissa_id = lect_helper.find_polissa_with_cups_id(
                    cursor, uid, line.cups_id.id, data_inici, fact_xml.datos_factura.fecha_hasta_factura
                )
                context = {'date': fact_xml.datos_factura.fecha_hasta_factura, 'from_f1': True}
                polissa_vals = polissa_obj.read(cursor, uid, polissa_id, ['contract_type'], context)
                if polissa_vals['contract_type'] == '09':
                    continue
            for compt_xml in fact_xml.get_comptadors():
                consums += self.get_consume_by_meter(
                    cursor, uid, compt_xml
                )
            for ltype, line_xml in fact_xml.get_linies_factura_by_type().iteritems():
                consums_facturats += self.get_consume_by_periode_facturat(
                    cursor, uid, ltype, line_xml
                )

        # Ara comprovem que la suma total sigui iguala per els consums
        # informats i els calculats
        total_consums_facturats = 0.0
        for consums_facturat in consums_facturats:
            total_consums_facturats += consums_facturat['total_consum']
        total_consums = 0.0
        for consum in consums:
            total_consums += consum['total_consum']
        if round(total_consums, 2) != round(total_consums_facturats, 2):
            errors.append({
                "total_consums": total_consums,
                "total_consums_facturats": total_consums_facturats
            })

        return errors

    def get_consume_by_meter(self, cursor, uid, compt_xml):
        consums = []
        for lect_xml in compt_xml.get_lectures(tipus=['A']):
            if lect_xml.periode:
                consume = lect_xml.lectura_hasta.lectura - lect_xml.lectura_desde.lectura
                if lect_xml.ajuste:
                    consume += lect_xml.ajuste.ajuste_por_integrador
                if consume < 0:
                    consume += lect_xml.gir_comptador
                consums.append(
                    {
                        'period': lect_xml.periode,
                        'total_consum': consume,
                        'data_desde': lect_xml.lectura_hasta.fecha,
                        'data_fins': lect_xml.lectura_desde.fecha,
                    }
                )
        return consums

    def get_consume_by_periode_facturat(self, cursor, uid, ltype, line_xml):
        consums = []
        if ltype != "energia":
            return consums
        ldatas = line_xml.get('lines', None)
        if not ldatas:
            return consums
        for periode in ldatas:
            consums.append(
                {
                    'period': periode.nombre,
                    'total_consum': periode.cantidad,
                    'data_desde': periode.fecha_desde,
                    'data_fins': periode.fecha_hasta,
                }
            )
        return consums

    def check_correct_tariff(self, cursor, uid, line, f1, parameters=None):
        pol_obj = self.pool.get('giscedata.polissa')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )

            for date in [fecha_desde, fecha_hasta]:
                ctxt = {'date': date}
                polissa_vals = pol_obj.read(
                    cursor, uid, polissa_id, ['tarifa'], context=ctxt
                )

                db_tariff_id = polissa_vals['tarifa'][0]

                is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
                xml_tariff_id = tariff_obj.get_tarifa_from_ocsum(
                    cursor, uid, fact_xml.datos_factura.tarifa_atr_fact, is_low
                )
                if xml_tariff_id != db_tariff_id:
                    xml_tariff_name = tariff_obj.read(
                        cursor, uid, xml_tariff_id, ['name']
                    )['name']
                    db_tariff_name = tariff_obj.read(
                        cursor, uid, db_tariff_id, ['name']
                    )['name']
                    errors.append(
                        {
                            'xml_tariff': xml_tariff_name,
                            'db_tariff': db_tariff_name,
                            'date': date
                        }
                    )
        return errors

    def check_correct_power_invoicing(self, cursor, uid, line, f1, parameters=None):
        pol_obj = self.pool.get('giscedata.polissa')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )

            for date in [fecha_desde, fecha_hasta]:
                ctxt = {'date': date}
                db_power_inv = pol_obj.read(
                    cursor, uid, polissa_id, ['facturacio_potencia'],
                    context=ctxt
                )['facturacio_potencia']
                xml_power_inv = fact_xml.get_info_facturacio_potencia()
                if xml_power_inv != db_power_inv:
                    errors.append(
                        {
                            'xml_power_invoicing': xml_power_inv,
                            'db_power_invoicing': db_power_inv,
                            'date': date,
                        }
                    )
        return errors

    def check_reading_type(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )
            polissa = polissa_obj.browse(cursor, uid, polissa_id)

            tariff_name = polissa.tarifa.name.split(' ')[0]
            db_low = 'LB' in polissa.tarifa.name

            if polissa.tarifa.codi_ocsum == '011':
                datos_factura = fact_xml.datos_factura
                xml_low = datos_factura.marca_medida_con_perdidas == 'S'
                if xml_low != db_low:
                    xml_tariff = tariff_name
                    db_tariff = tariff_name
                    if xml_low:
                        xml_tariff += 'LB'
                    if db_low:
                        db_tariff += 'LB'
                    errors.append(
                        {
                            'xml_tariff': xml_tariff,
                            'db_tariff': db_tariff,
                        }
                    )
        return errors

    def check_autoconsume_concepts(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')

        autoconsume_concepts = ['43', '44', '45', '46']

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )

            ctx = {'date': fecha_hasta}
            polissa_vals = polissa_obj.read(
                cursor, uid, polissa_id, ['autoconsumo'], context=ctx
            )

            if polissa_vals['autoconsumo'] == '00':
                # If we don't have autoconsume
                conceptes, total = fact_xml.get_info_conceptes_repercutibles()
                for concepte_xml in conceptes:
                    codi = concepte_xml.concepto_repercutible
                    if codi in autoconsume_concepts:
                        errors.append(
                            {
                                'concept_type': codi
                            }
                        )

        return errors

    def check_unit_number(self, cursor, uid, line, f1, parameters=None):
        errors = []

        for fact_xml in f1.otras_facturas:
            conceptes, total = fact_xml.get_info_conceptes_repercutibles()
            for concepte_xml in conceptes:
                if not concepte_xml.unidades:
                    unit_number = 1
                    errors.append(
                        {
                            'concept': concepte_xml.codi,
                            'unit_number': unit_number
                        }
                    )

        return errors

    def check_unit_price(self, cursor, uid, line, f1, parameters=None):
        errors = []

        for fact_xml in f1.otras_facturas:
            conceptes, total = fact_xml.get_info_conceptes_repercutibles()
            for conc_xml in conceptes:
                if not conc_xml.precio_unidad:
                    unit_price = conc_xml.importe
                    if conc_xml.has_quantity:
                        unit_price = conc_xml.importe / conc_xml.unidades
                    errors.append(
                        {
                            'concept': conc_xml.concepto_repercutible,
                            'unit_price': unit_price
                        }
                    )

        return errors

    def check_total_price(self, cursor, uid, line, f1, parameters=None):
        errors = []

        for fact_xml in f1.otras_facturas:
            conceptes, total = fact_xml.get_info_conceptes_repercutibles()
            for concepte_xml in conceptes:
                if concepte_xml.precio_unidad and concepte_xml.unidades:
                    unit_number = concepte_xml.unidades
                    unit_price = concepte_xml.precio_unidad
                    calculated_price = unit_number * unit_price
                    if abs(calculated_price-concepte_xml.importe) > 0.01:
                        errors.append(
                            {
                                'concept': concepte_xml.concepto_repercutible,
                                'unit_number': unit_number,
                                'unit_price': unit_price,
                                'total_price': concepte_xml.importe,
                                'calculated_price': calculated_price,
                            }
                        )
        return errors

    def check_concept_exists(self, cursor, uid, line, f1, parameters=None):
        producte_obj = self.pool.get('product.product')

        errors = []
        conceptes = []
        for fact_xml in f1.facturas_atr:
            conc, total = fact_xml.get_info_conceptes_repercutibles()
            conceptes += conc

        for fact_xml in f1.otras_facturas:
            conc, total = fact_xml.get_info_conceptes_repercutibles()
            conceptes += conc

        for concepte_xml in conceptes:
            concept_exists = producte_obj.search_count(
                cursor, uid,
                [('default_code', 'like', concepte_xml.concepto_repercutible)]
            )
            if not concept_exists:
                errors.append(
                    {
                        'concept': concepte_xml.concepto_repercutible
                    }
                )
        return errors

    def check_dates_meter(self, cursor, uid, line, f1, parameters=None):
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = []

        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )

            for compt_xml in fact_xml.get_comptadors():
                compt_id = compt_obj.search_with_contract(
                    cursor, uid, compt_xml.numero_serie, polissa_id
                )
                if compt_id:
                    compt_id = compt_id[0]
                    for lect_xml in compt_xml.get_lectures():
                        correct_dates = compt_obj.are_dates_correct(
                            cursor, uid, compt_id,
                            lect_xml.lectura_desde.fecha,
                            lect_xml.lectura_hasta.fecha
                        )
                        if not correct_dates:
                            errors.append(
                                {
                                    'start_date': lect_xml.lectura_desde.fecha,
                                    'end_date': lect_xml.lectura_hasta.fecha,
                                    # 'type': lect_xml.tipus,
                                    # 'period': lect_xml.periode,
                                    'meter_name': compt_xml.numero_serie,
                                }
                            )
                            break
        return errors

    def check_dates_modcon(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')

        errors = []

        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )
            polissa = polissa_obj.browse(cursor, uid, polissa_id)

            for compt_xml in fact_xml.get_comptadors():
                for lect_xml in compt_xml.get_lectures():
                    start_reading = datetime.strptime(
                        fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
                    )
                    start_reading += timedelta(days=1)

                    desde_lectura = start_reading.strftime('%Y-%m-%d')

                    ctx = {
                        'ffields': ['facturacio_potencia', 'tarifa']
                    }

                    modcons = polissa.get_modcontractual_intervals(
                        desde_lectura, lect_xml.lectura_hasta.fecha, ctx
                    )
                    if len(modcons) != 1:
                        errors.append(
                            {
                                'start_date': lect_xml.lectura_desde.fecha,
                                'end_date': lect_xml.lectura_hasta.fecha,
                                # 'type': lect_xml.tipus,
                                # 'period': lect_xml.periode,
                                'meter_name': compt_xml.numero_serie,
                            }
                        )
                        break
        return errors

    def check_trafo(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        polissa_obj = self.pool.get('giscedata.polissa')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            fecha_desde = start_date.strftime('%Y-%m-%d')
            fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
            )

            ctx = {'date': fecha_hasta}
            trafo = polissa_obj.read(
                cursor, uid, polissa_id, ['trafo'], context=ctx
            )['trafo']

            is_tariff_31a = fact_xml.datos_factura.tarifa_atr_fact == '011'
            xml_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
            if is_tariff_31a and xml_low:
                # If XML says the tariff is "medida en baja"
                # (which only exists for tariff 3.1)
                if trafo == 0:
                    # And we don't have any value assigned to trafo
                    errors.append(
                        {
                            'xml_tariff': '3.1 LB',
                        }
                    )
        return errors

    def check_simulated_invoice(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        help_obj = self.pool.get('giscedata.facturacio.switching.helper')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')

        errors = []
        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                fact_xml.datos_factura.fecha_hasta_factura
            )

            is_low = fact_xml.datos_factura.marca_medida_con_perdidas == 'S'
            tariff_id = tariff_obj.get_tarifa_from_ocsum(
                cursor, uid, fact_xml.datos_factura.tarifa_atr_fact, is_low
            )
            tariff_name = tariff_obj.read(
                cursor, uid, tariff_id, ['name']
            )['name']

            for compt_xml in fact_xml.get_comptadors():
                errors += help_obj.simulate_invoice(
                    cursor, uid, fact_xml, tariff_name, polissa_id,
                    compt_xml
                )

        return errors

    def check_conflicting_readings(self, cursor, uid, line, f1, parameters=None):
        errors = []

        reads_by_meter = {}
        for fact_xml in f1.facturas_atr:
            for compt_xml in fact_xml.get_comptadors():
                reads_by_meter.setdefault(compt_xml.numero_serie, {})
                reads_on_meter = reads_by_meter[compt_xml.numero_serie]
                for lect_xml in compt_xml.get_lectures_energia():
                    # We can have repeated periods if one is a totalitzador.
                    # They will be ignored when loading ther reads to meters
                    # For tarifs 2.0A and 2.1A we check the totalitzadors
                    # because we will only have them
                    if (lect_xml.codigo_periodo in ['10', '20', '80'] and
                        fact_xml.datos_factura.tarifa_atr_fact not in ['001', '005']):
                        continue
                    tipus = lect_xml.tipus
                    period = lect_xml.periode

                    start_date = lect_xml.lectura_desde.fecha
                    start_value = str(lect_xml.lectura_desde.lectura)
                    reads_on_meter.setdefault(tipus, {})
                    reads_on_meter[tipus].setdefault(period, {})
                    reads_on_meter[tipus][period].setdefault(start_date, set())
                    reads_on_meter[tipus][period][start_date].add(start_value)

                    end_date = lect_xml.lectura_hasta.fecha
                    end_value = str(lect_xml.lectura_hasta.lectura)
                    reads_on_meter.setdefault(tipus, {})
                    reads_on_meter[tipus].setdefault(period, {})
                    reads_on_meter[tipus][period].setdefault(end_date, set())
                    reads_on_meter[tipus][period][end_date].add(end_value)

                for lect_xml in compt_xml.get_lectures(tipus=['M', 'EP']):
                    # We can have repeated periods if one is a totalitzador.
                    # They will be ignored when loading ther reads to meters
                    # For tarifs 2.0A and 2.1A we check the totalitzadors
                    # because we will only have them
                    if (lect_xml.codigo_periodo in ['10', '20', '80'] and
                        fact_xml.datos_factura.tarifa_atr_fact not in ['001', '005']):
                        continue
                    tipus = lect_xml.tipus
                    period = lect_xml.periode

                    end_date = lect_xml.lectura_hasta.fecha
                    end_value = str(lect_xml.lectura_hasta.lectura)
                    reads_on_meter.setdefault(tipus, {})
                    reads_on_meter[tipus].setdefault(period, {})
                    reads_on_meter[tipus][period].setdefault(end_date, set())
                    reads_on_meter[tipus][period][end_date].add(end_value)

        for meter_name, reads_by_type in reads_by_meter.items():
            for tipus, reads_by_period in reads_by_type.items():
                for period, reads_by_date in reads_by_period.items():
                    for read_date, readings in reads_by_date.items():
                        if len(readings) > 1:
                            errors.append(
                                {
                                    'number_readings': len(readings),
                                    'type': tipus,
                                    'date': read_date,
                                    'period': period,
                                    'meter_name': meter_name,
                                    'readings_values': ', '.join(readings),
                                }
                            )
        return errors

    def check_conflicting_meters(self, cursor, uid, line, f1, parameters=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')

        errors = []

        for fact_xml in f1.facturas_atr:
            start_date = datetime.strptime(
                fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
            )
            start_date += timedelta(days=1)

            data_inici = start_date.strftime('%Y-%m-%d')
            polissa_id = lect_help_obj.find_polissa_with_cups_id(
                cursor, uid, line.cups_id.id, data_inici,
                fact_xml.datos_factura.fecha_hasta_factura
            )
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            comptadors = polissa.comptadors

            for compt_xml in fact_xml.get_comptadors():
                compt_id = compt_obj.search_with_contract(
                    cursor, uid, compt_xml.numero_serie, polissa_id
                )
                if not compt_id:
                    lectures = compt_xml.get_lectures()
                    dict_lect = agrupar_lectures_per_data(lectures)
                    dates = obtenir_data_inici_i_final(dict_lect)
                    start_date, end_date = dates
                    for compt in comptadors:
                        if start_date <= compt.data_alta < end_date:
                            errors.append(
                                {
                                    'new_meter': compt_xml.numero_serie,
                                    'start_date': start_date,
                                    'end_date': end_date,
                                    'old_meter': compt.name,
                                }
                            )
        return errors

    def check_meter_price(self, cursor, uid, line, f1, params=None):
        lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')

        errors = []

        for fact_xml in f1.facturas_atr:
            comptadors_xml = fact_xml.get_comptadors()
            lloguers, total = fact_xml.get_info_lloguer()

            for i, compt_xml in enumerate(comptadors_xml):
                preu = lect_help_obj.calculate_price_from_rent_prices(
                    cursor, uid, comptadors_xml, lloguers, i
                )

                if not (params['min_value'] <= preu <= params['max_value']):
                    errors.append(
                        {
                            'meter': compt_xml.numero_serie,
                            'price': preu,
                            'uom': 'ALQ/dia',
                            'min_value': params['min_value'],
                            'max_value': params['max_value'],
                        }
                    )

        return errors

    def check_contract_power(self, cursor, uid, line, f1, params=None, context=None):
            if context is None:
                context = {}
            pol_obj = self.pool.get('giscedata.polissa')
            lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
            pot_obj = self.pool.get(
                "giscedata.polissa.potencia.contractada.periode")

            errors = []
            for fact_xml in f1.facturas_atr:
                if fact_xml.datos_factura.tipo_factura == 'C':
                    if not fact_xml.potencia:
                        continue
                    elif fact_xml.potencia.importe_total == 0:
                        continue

                start_date = datetime.strptime(
                    fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
                )
                start_date += timedelta(days=1)

                fecha_desde = start_date.strftime('%Y-%m-%d')
                fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

                polissa_id = lect_help_obj.find_polissa_with_cups_id(
                    cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
                )

                for xml_term in fact_xml.potencia.terminos_potencia:
                    start_date = datetime.strptime(
                        xml_term.fecha_desde, '%Y-%m-%d'
                    )
                    start_date += timedelta(days=1)
                    fecha_desde = start_date.strftime('%Y-%m-%d')
                    fecha_hasta = xml_term.fecha_hasta
                    for date in [fecha_desde, fecha_hasta]:
                        ctxt = {'date': date}
                        pots_dict = pol_obj.get_potencies_dict(
                            cursor, uid, polissa_id, ctxt
                        )
                        f1_diff_pots = []
                        polissa_diff_pots = []
                        f1_pots = xml_term.get_contracted_periods_by_period(
                            use_facturada=context.get("use_facturada", False)
                        )
                        for f1_period in f1_pots:
                            f1_pot = f1_pots.get(f1_period) / 1000.0
                            pol_pot = pots_dict.get(f1_period, False)
                            if pol_pot != f1_pot:
                                f1_diff_pots.append((f1_period, f1_pot))
                                polissa_diff_pots.append((f1_period, pol_pot))
                        if len(f1_diff_pots):
                            # [173303] Endesa va normalitzar la potència però
                            # per una sentència els van obigar a cobrar la
                            # potència que tenien contractades els clients.
                            # En aquests casos ens informen valors diferents als
                            # camps potencia "contractada" i "a facturar".
                            # Per "solucionar-ho" en aquests casos farem la
                            # comprovació amb la potencia a facturar,
                            # peró només si estem en ICP.
                            info = pol_obj.read(cursor, uid, polissa_id, ['distribuidora', 'facturacio_potencia'])
                            icp = info['facturacio_potencia'] == "icp"
                            distri_id = info['distribuidora'][0]
                            part_obj = self.pool.get("res.partner")
                            codi = part_obj.read(cursor, uid, distri_id, ['ref'])['ref']
                            if icp and not context.get("use_facturada", False):
                                ctx = context.copy()
                                ctx['use_facturada'] = True
                                return self.check_contract_power(cursor, uid, line, f1, params=params, context=ctx)
                            else:
                                errors.append(
                                    {
                                        'f1_pots': f1_diff_pots,
                                        'polissa_pots': polissa_diff_pots,
                                    }
                                )
            return errors

    def check_mode_facturacio(self, cursor, uid, line, f1, params=None):
            pol_obj = self.pool.get('giscedata.polissa')
            lect_help_obj = self.pool.get('giscedata.lectures.switching.helper')
            errors = []
            for fact_xml in f1.facturas_atr:
                start_date = datetime.strptime(
                    fact_xml.datos_factura.fecha_desde_factura, '%Y-%m-%d'
                )
                start_date += timedelta(days=1)

                fecha_desde = start_date.strftime('%Y-%m-%d')
                fecha_hasta = fact_xml.datos_factura.fecha_hasta_factura

                polissa_id = lect_help_obj.find_polissa_with_cups_id(
                    cursor, uid, line.cups_id.id, fecha_desde, fecha_hasta
                )

                modcons = pol_obj.get_modcontractual_intervals(
                    cursor, uid, polissa_id, fecha_desde, fecha_hasta,
                    context={'ffields': ['mode_facturacio']}
                )
                if len(modcons) > 1:
                    if params['new_modcon_date'] == 'end':
                        new_date = (datetime.strptime(
                            fact_xml.datos_factura.fecha_hasta_factura, '%Y-%m-%d'
                        ) + timedelta(days=1)).strftime('%Y-%m-%d')
                    else:
                        new_date = fecha_desde
                    mode_facturacio = ''
                    data_canvi = ''
                    for modcon in modcons:
                        if 'mode_facturacio' in modcons[modcon]['changes']:
                            data_canvi = modcon
                            pol = pol_obj.read(
                                cursor, uid, polissa_id, ['mode_facturacio'],
                                context={'date': data_canvi}
                            )
                            mode_facturacio = pol['mode_facturacio']

                    errors.append(
                        {
                            'data_canvi': data_canvi,
                            'mode_facturacio': mode_facturacio,
                            'data_nova': new_date
                        }
                    )
            return errors

GiscedataFacturacioSwitchingValidator()


class GiscedataFacturacioImportacioLinia(osv.osv):
    """Agrupació d'importacions"""

    _name = 'giscedata.facturacio.importacio.linia'
    _inherit = 'giscedata.facturacio.importacio.linia'

    _columns = {
        'error_ids': fields.one2many(
            'giscedata.facturacio.switching.error', 'line_id',
            string='Errors importacio'
        )
    }

    def unlink_errors(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, list):
            ids = [ids]
        error_obj = self.pool.get('giscedata.facturacio.switching.error')
        for line_error in self.read(cursor, uid, ids, ['error_ids']):
            error_obj.unlink(cursor, uid, line_error['error_ids'])

    def clean_linia_and_commit(self, cursor, uid, lid, context=None):
        """Eliminar lectures, factures, línies extra i comitar"""
        if context is None:
            context = {}
        self.unlink_errors(cursor, uid, lid, context=context)
        super(GiscedataFacturacioImportacioLinia, self).clean_linia_and_commit(
            cursor, uid, lid, context=context)

    def unlink(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        self.unlink_errors(
            cursor, uid, ids, context=context
        )
        return super(GiscedataFacturacioImportacioLinia, self).unlink(
            cursor, uid, ids, context=context)


GiscedataFacturacioImportacioLinia()
