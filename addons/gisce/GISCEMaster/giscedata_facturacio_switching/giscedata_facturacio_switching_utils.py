# -*- coding: utf-8 -*-
import re


def get_cups_from_xml(xml_data):
    cups = re.findall('<CUPS>(.*)</CUPS>', xml_data)
    if cups:
        return cups[0]
    else:
        return None


def get_header_from_xml(xml_data):
    res = {}
    cups = re.findall('<CUPS>(.*)</CUPS>', xml_data)
    res.setdefault('cups', False)
    if cups:
        res['cups'] = cups[0]

    emisor_f1 = re.findall(
        '<CodigoREEEmpresaEmisora>(.*)</CodigoREEEmpresaEmisora>',
        xml_data
    )
    res.setdefault('codigo_ree_empresa_emisora', False)
    if emisor_f1:
        res['codigo_ree_empresa_emisora'] = emisor_f1[0]

    destino_f1 = re.findall(
        '<CodigoREEEmpresaDestino>(.*)</CodigoREEEmpresaDestino>',
        xml_data
    )
    res.setdefault('codigo_ree_empresa_destino', False)
    if destino_f1:
        res['codigo_ree_empresa_destino'] = destino_f1[0]

    sol_f1 = re.findall(
        '<CodigoDeSolicitud>(.*)</CodigoDeSolicitud>',
        xml_data
    )
    res.setdefault('codigo_solicitud', False)
    if sol_f1:
        res['codigo_solicitud'] = sol_f1[0]

    fecha_f1 = re.findall(
        '<FechaSolicitud>(.*)</FechaSolicitud>',
        xml_data
    )
    res.setdefault('fecha_solicitud', False)
    if fecha_f1:
        res['fecha_solicitud'] = fecha_f1[0]

    codigo_proceso = re.findall(
        '<CodigoDelProceso>(.*)</CodigoDelProceso>',
        xml_data
    )
    res.setdefault('codigo_proceso', False)
    if codigo_proceso:
        res['codigo_proceso'] = codigo_proceso[0]

    sec_solicitud = re.findall(
        '<SecuencialDeSolicitud>(.*)</SecuencialDeSolicitud>',
        xml_data
    )
    res.setdefault('secuencia_solicitud', False)
    if sec_solicitud:
        res['secuencia_solicitud'] = sec_solicitud[0]

    res.setdefault('fecha_factura_desde', False)
    res.setdefault('fecha_factura_hasta', False)
    res.setdefault('type_factura', False)
    res.setdefault('fecha_factura', False)
    factura_atr = re.findall(
        '<FacturaATR>(.*)', xml_data
    )
    if factura_atr:
        res['tipo_factura_f1'] = 'atr'
        fecha_desde = re.findall(
            '<FechaDesdeFactura>(.*)</FechaDesdeFactura>', xml_data
        )
        if fecha_desde:
            res['fecha_factura_desde'] = fecha_desde[0]

        fecha_hasta = re.findall(
            '<FechaHastaFactura>(.*)</FechaHastaFactura>', xml_data
        )
        if fecha_hasta:
            res['fecha_factura_hasta'] = fecha_hasta[0]

    else:
        res['tipo_factura_f1'] = 'otros'

    # Tipo factura N/R..
    invoice_type = re.findall(
        '<TipoFactura>(.*)</TipoFactura>', xml_data
    )
    if invoice_type:
        res['type_factura'] = invoice_type[0]

    invoice_date = re.findall(
        '<FechaFactura>(.*)</FechaFactura>', xml_data
    )
    if invoice_date:
        res['fecha_factura'] = invoice_date[0]

    return res


def get_remesa_from_xml(xml_data, emisor_lot=''):
    id_remesa = re.findall('<IdRemesa>(.*)</IdRemesa>', xml_data)
    fecha_valor_remesa = re.findall(
        '<FechaLimitePago>(.*)</FechaLimitePago>', xml_data
    )
    emisor_f1 = re.findall(
        '<CodigoREEEmpresaEmisora>(.*)</CodigoREEEmpresaEmisora>',
        xml_data
    )
    cups = re.findall(u'<CUPS>(.*)</CUPS>', xml_data)
    if not id_remesa or not fecha_valor_remesa:
        return None

    if not cups or not emisor_f1:
        return None

    remesa_info = {
        'id_remesa': id_remesa[0],
        'fecha_valor_remesa': fecha_valor_remesa[0]
    }
    emisor = emisor_f1[0]
    remesa_info.update({'emisor': emisor})
    return remesa_info


def get_remeses_from_zip(zfile, emisor_lot=''):
    """
    :param zfile: ZIP handle
    :param emisor_lot: from code
    :return: payment orders dict list
    """
    remeses = []
    remeses_names = []
    for i, info in enumerate(zfile.infolist()):
        fname = info.filename
        xml_data = zfile.read(fname)
        remesa_info = get_remesa_from_xml(xml_data, emisor_lot)
        if not remesa_info:
            continue
        if remesa_info['id_remesa'] not in remeses_names:
            remeses.append(remesa_info)
            remeses_names.append(remesa_info['id_remesa'])
    return remeses


def get_emisor(emisor, emisor_lot, cups_distri):
    """
    Select
    :param emisor: from code in F1
    :param emisor_lot: from code in importation lot
    :param cups: CUPS distri code or CUPS affected
    :return: new emisor or False on error
    """
    if emisor != emisor_lot:
        # Provem de buscar-lo pel CUPS? alguns encara envíen emisors no
        # vàlids...
        if len(cups_distri) > 4:
            cups_distri = cups_distri[2:6]
        emisor = cups_distri
    if emisor != emisor_lot and emisor_lot != '':
        return False
    return emisor


def get_filename(filename):
    if isinstance(filename, str):
        return unicode(filename, errors='ignore')
    return filename