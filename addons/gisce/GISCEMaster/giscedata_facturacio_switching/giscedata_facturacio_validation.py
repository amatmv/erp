# -*- coding: utf-8 -*-
from osv import osv


class GiscedataFacturacioValidationValidator(osv.osv):
    _inherit = 'giscedata.facturacio.validation.validator'
    _name = 'giscedata.facturacio.validation.validator'

    def check_factura_amb_linies_complementaria(self, cursor, uid, fact, parameters):
        if fact.te_linies_facturacio_complementaria():
            return {}
        return None

GiscedataFacturacioValidationValidator()
