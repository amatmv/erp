# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from tools.translate import _


class GiscedataSwitchingHelpers(osv.osv):

    _name = 'giscedata.switching.helpers'
    _inherit = 'giscedata.switching.helpers'

    def tancar_reclamacio(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}

        sw_obj = self.pool.get("giscedata.switching")
        r101_obj = self.pool.get("giscedata.switching.r1.01")
        pas01_id = r101_obj.search(cursor, uid, [('sw_id', '=', sw_id)])[0]
        pas01_info = r101_obj.read(cursor, uid, pas01_id, ['subtipus_id'])
        if pas01_info['subtipus_id'][1] == "037":
            # Try to get the F1 with the invoice number of the R1
            r1_inf = r101_obj.read(cursor, uid, pas01_id, ['reclamacio_ids'])
            codi_atr = sw_obj.read(cursor, uid, sw_id, ['codi_sollicitud'])
            codi_atr = codi_atr['codi_sollicitud']
            if r1_inf['reclamacio_ids']:
                rec_obj = self.pool.get("giscedata.switching.reclamacio")
                f1_obj = self.pool.get("giscedata.facturacio.importacio.linia")
                rec_ids = r1_inf['reclamacio_ids']
                rec_infos = rec_obj.read(cursor, uid, rec_ids, ['num_factura'])
                for rec_info in rec_infos:
                    if rec_info['num_factura']:
                        orig = rec_info['num_factura']
                        f1_ids = f1_obj.search(
                            cursor, uid, [('invoice_number_text', '=', orig)]
                        )
                        inf = f1_obj.read(
                            cursor, uid, f1_ids,
                            ['import_phase', 'codi_sollicitud']
                        )
                        for f1_info in inf:
                            if f1_info['import_phase'] not in [40, 50]:
                                err_msg = _(
                                    u"No s'ha tancat la reclamació {0}: s'ha "
                                    u"trobat un F1 associat (codi sol.licitud "
                                    u"{1}) que no està en fase final (4 o 5)."
                                ).format(codi_atr, f1_info['codi_sollicitud'])
                                return "ERROR", err_msg
            # Tanca la reclamacio, el F1 ja està en un estat correcte (4 o 5)
            sw_obj.case_close(cursor, uid, sw_id, context)

        return super(GiscedataSwitchingHelpers, self).tancar_reclamacio(
            cursor, uid, sw_id, context=context
        )

GiscedataSwitchingHelpers()
