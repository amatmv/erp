# -*- coding: utf-8 -*-
{
    "name": "giscedata_carrer",
    "description": """Model pels carrers de les adreces.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE Master",
    "depends":[
        "base",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
