# -*- coding: utf-8 -*-
"""GiscedataCarrer
"""

# pylint: disable=E1101,W0223

from osv import osv, fields

class GiscedataCarrer(osv.osv):
    """Model pels carrers de les adreces.
    
    """
    _name = 'giscedata.carrer'
    _description = __doc__
    
    _columns = {
        'name':fields.char('Carrer', size=128, required=True),
        'poblacio_id':fields.many2one('res.poblacio', 'Població', 
                                      required=True),
    }

GiscedataCarrer()
