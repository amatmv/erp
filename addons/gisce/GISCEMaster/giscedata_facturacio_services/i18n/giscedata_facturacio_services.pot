# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 13:12\n"
"PO-Revision-Date: 2019-02-26 16:32+0000\n"
"Last-Translator: Jaume  Florez Valenzuela <jflorez@gisce.net>\n"
"Language-Team: Catalan (Spain) <erp@dev.gisce.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: ca_ES\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_facturacio_services
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr "Invalid XML for View Architecture!"

#. module: giscedata_facturacio_services
#: constraint:ir.model:0
msgid ""
"The Object name must start with x_ and not contain any special character !"
msgstr "The Object name must start with x_ and not contain any special character !"

#. module: giscedata_facturacio_services
#: field:giscedata.facturacio.services,data_inici:0
msgid "Data Inici"
msgstr "Data Inici"

#. module: giscedata_facturacio_services
#: field:giscedata.facturacio.services,polissa_id:0
msgid "Polissa"
msgstr "Polissa"

#. module: giscedata_facturacio_services
#: field:giscedata.facturacio.services,producte:0
msgid "Producte"
msgstr "Producte"

#. module: giscedata_facturacio_services
#: model:ir.module.module,shortdesc:giscedata_facturacio_services.module_meta_information
msgid "Serveis addicionals"
msgstr "Serveis addicionals"

#. module: giscedata_facturacio_services
#: field:giscedata.facturacio.services,llista_preus:0
msgid "Llista de Preus"
msgstr "Llista de Preus"

#. module: giscedata_facturacio_services
#: field:giscedata.facturacio.services,data_fi:0
msgid "Data Fi"
msgstr "Data Fi"

#. module: giscedata_facturacio_services
#: view:giscedata.facturacio.services:0
msgid "Servei"
msgstr "Servei"

#. module: giscedata_facturacio_services
#: model:ir.model,name:giscedata_facturacio_services.model_giscedata_facturacio_services
msgid "giscedata.facturacio.services"
msgstr "giscedata.facturacio.services"

#. module: giscedata_facturacio_services
#: view:giscedata.polissa:0 field:giscedata.polissa,buscar_servei:0
#: field:giscedata.polissa,serveis:0
msgid "Serveis Contractats"
msgstr "Serveis Contractats"
