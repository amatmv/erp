# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy_data(self, cursor, uid, ids, default=None, context=None):
        """
        Avoid to duplicate the services when the contract is duplicated.
        """
        if default is None:
            default = {}
        if context is None:
            context = {}
        default_values = {
            'serveis': False,
        }
        default.update(default_values)
        res_id = super(GiscedataPolissa, self).copy_data(
            cursor, uid, ids, default, context)
        return res_id

    def _search_services(self, cursor, uid, obj, name, args, context=None):
        """
        Returns the domain that will be used to find the contracts that have
        the services.
        :param cursor:
        :param uid:
        :param obj: the object over which we are searching
        :param name: name of the field on which we are searching
        :param args: domain that has been introduced to the filter. In this
        case, will have the format: [('buscar_servei', 'ilike', 'X')]
        :type args: list
        :param context:
        :return:
        """

        if context is None:
            context = {}

        if not args:
            return [('id', '=', 0)]
        serv_obj = self.pool.get('giscedata.facturacio.services')
        term_introduced = args[0][2]
        serv_ids = serv_obj.search(cursor, uid, [
            ('producte.name', 'ilike', term_introduced)
        ])

        if serv_ids:
            services_v = serv_obj.read(cursor, uid, serv_ids, ['polissa_id'])
            contract_ids = [
                serv['polissa_id'][0] for serv in services_v
            ]
            return [('id', 'in', contract_ids)]
        else:
            return [('id', '=', 0)]

    def _services(self, cursor, uid, ids, field_name, arg, context=None):
        return dict.fromkeys(ids, False)

    _columns = {
        'serveis': fields.one2many(
            'giscedata.facturacio.services', 'polissa_id',
            u'Serveis Contractats'
        ),
        'buscar_servei': fields.function(
            _services, type='char', string=u'Serveis Contractats',
            fnct_search=_search_services
        )
    }


GiscedataPolissa()
