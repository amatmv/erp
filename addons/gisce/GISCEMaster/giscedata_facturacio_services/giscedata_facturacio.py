# -*- encoding: utf-8 -*-

from osv import osv, fields
from libfacturacioatr import tarifes

from datetime import datetime, timedelta


class GiscedataFacturacioFacturador(osv.osv):

    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        """ Overwrite the method to include the service line if needed. """
        if not context:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        serv_obj = self.pool.get('giscedata.facturacio.services')
        fact_ids = super(
            GiscedataFacturacioFacturador, self
        ).fact_via_lectures(cursor, uid, polissa_id, lot_id, context)

        for fact in fact_obj.browse(cursor, uid, fact_ids, context):
            if fact.journal_id.code in ('ENERGIA', 'ENERGIA.R') and fact.polissa_id.serveis:
                serv_obj.add_service_line(cursor, uid, fact, context)

        fact_obj.button_reset_taxes(cursor, uid, fact_ids, context)
        return fact_ids


GiscedataFacturacioFacturador()


class GiscedataFacturacioServices(osv.osv):

    _name = 'giscedata.facturacio.services'

    def _get_service_desc(self, service):
        """
        Returns the description of the service that will be used as a 
        description of the invoice line.
        :param service: the service
        :type service: browse_record
        :return: The description
        :rtype: str
        """
        return service.llista_preus.name

    def _get_vals_linia(self, cursor, uid, service, fact, context=None):
        """
        Get the values of the invoice line that has to be created.
        :param cursor:
        :param uid:
        :param service: the service to apply
        :type service: browse_record
        :param fact: the invoice over which the service is applied
        :type fact: browse_record
        :param context:
        :return:
        """
        data_inici = max(fact.data_inici, service.data_inici)
        data_fi = min(fact.data_final, service.data_fi or '3000-01-01')

        pricelist_versions = service.llista_preus.version_id
        dates_versions = {
            version.date_start: version.id for version in pricelist_versions
        }
        periodes_versions = tarifes.repartir_potencia(
            dates_versions, data_inici, data_fi, base='dia'
        )
        dates = sorted(periodes_versions.keys())
        for i, period in enumerate(dates):
            data_desde = max(data_inici, period)
            data_fins = data_fi
            if i+1 < len(dates):
                # If there's another version period, we must set the end_date
                # to the day before this period starts.
                data_fins = (
                    datetime.strptime(dates[i+1], '%Y-%m-%d') -
                    timedelta(days=1)
                ).strftime('%Y-%m-%d')

            vals = {
                'uos_id': service.producte.uom_id.id,
                'multi': 1,
                'product_id': service.producte.id,
                'tipus': 'altres',
                'name': self._get_service_desc(service),
                'data_desde': data_desde,
                'data_fins': data_fins,
                'quantity': periodes_versions[period],
            }
            yield vals

    def add_service_line(self, cursor, uid, fact, context):
        """
        Creates a line for each service that applies to the invoice.
        :param cursor: 
        :param uid: 
        :param fact: the invoice
        :type fact: browse_record
        :param context: 
        :return: None
        """
        if context is None:
            context = {}
        facturador_obj = self.pool.get('giscedata.facturacio.facturador')
        for service in fact.polissa_id.serveis:
            for valors_de_linia in self._get_vals_linia(
                cursor, uid, service, fact, context
            ):
                facturador_obj.crear_linia(
                    cursor, uid, fact.id, valors_de_linia,
                    # We want the new invoice line to take our service pricelist
                    # instead of the invoice one.
                    context={'force_llista_preu_id': service.llista_preus.id}
                )

    _columns = {
        'polissa_id': fields.many2one(
            'giscedata.polissa', 'Polissa', required=True),
        'producte': fields.many2one(
            'product.product', 'Producte', required=True),
        'data_inici': fields.date('Data Inici', required=True),
        'data_fi': fields.date('Data Fi'),
        'llista_preus': fields.many2one(
            'product.pricelist', 'Llista de Preus')
    }

    _order = 'data_inici'


GiscedataFacturacioServices()
