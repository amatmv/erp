# -*- coding: utf-8 -*-
{
    "name": "Serveis addicionals",
    "description": "",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        'giscedata_serveis_contractats',
        'giscedata_facturacio',
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        'giscedata_polissa_view.xml',
        'security/ir.model.access.csv'
    ],
    "active": False,
    "installable": True
}
