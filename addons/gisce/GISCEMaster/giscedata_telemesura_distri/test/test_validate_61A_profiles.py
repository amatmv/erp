from destral import testing


class TestValidate61AProfiles(testing.OOTestCaseWithCursor):

    def get_meter(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        comptador_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures',
            'comptador_0001')[1]

        return comptador_id

    def activar_polissa_CUPS(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        pol_obj.send_signal(self.cursor, self.uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def crear_modcon(self, polissa_id, ini):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol = pol_obj.browse(self.cursor, self.uid, polissa_id)
        pol.send_signal(['modcontractual'])

        wz_crear_mc_obj = self.openerp.pool.get('giscedata.polissa.crear.contracte')
        ctx = {'active_id': polissa_id}
        params = {
            'duracio': 'actual',
            'accio': 'nou',
            'data_inici': ini,
            'observacions': 'Test observacions'
        }

        wz_id_mod = wz_crear_mc_obj.create(
            self.cursor, self.uid, params, ctx
        )
        wiz_mod = wz_crear_mc_obj.browse(self.cursor, self.uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            self.cursor, self.uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_final': res['value']['data_final']
        })
        wiz_mod.action_crear_contracte(ctx)

    def test_check_technology_meter(self):
        """
        Check if meter is on telemeasure or electronic technology
        """
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        wiz_obj = self.openerp.pool.get('wizard.validate.61A.profiles')

        meter_id = self.get_meter()
        meter_obj.write(
            self.cursor, self.uid, meter_id, {'technology_type': 'telemeasure'}
        )

        wiz_mod_id = wiz_obj.create(
            self.cursor, self.uid, {'state': 'init'}, context={'active_ids': [meter_id]}
        )
        wizard = wiz_obj.browse(self.cursor, self.uid, wiz_mod_id)

        expected = ['electronic', 'telemeasure']
        result = wizard.check_technology(meter_id)
        self.assertIn(result, expected)

    def test_check_tariff_meter(self):
        """
        Check if meter's policy is on tariff 6.1A
        """
        imd_obj = self.openerp.pool.get('ir.model.data')
        meter_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pot_obj = self.openerp.pool.get('giscedata.polissa.potencia.contractada.periode')
        tar_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        wiz_obj = self.openerp.pool.get('wizard.validate.61A.profiles')

        # get id and data_alta from meter's policy
        meter_id = self.get_meter()
        # pol_id = meter_obj.read(self.cursor, self.uid, meter_id, ['polissa'])['polissa'][0]
        pol_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol_data = pol_obj.read(self.cursor, self.uid, pol_id, [])
        data_alta = pol_data['data_alta']
        CUPS = pol_data['cups'][0]

        # change tariff to 6.1A
        tar_id = tar_obj.search(self.cursor, self.uid, [('name', '=', '6.1A')])[0]

        # change tariff to 6.1A
        # activate contract
        pvals = {
            'cups': CUPS,
            'state': 'activa',
            'potencia': 4.44,
            'tarifa': tar_id
        }

        pol_obj.write(
            self.cursor, self.uid, pol_id, pvals
        )

        pol_pots = pol_obj.read(self.cursor, self.uid, pol_id, ['potencies_periode'])['potencies_periode']
        pot_obj.write(self.cursor, self.uid, pol_pots, {'potencia': 4.44})

        # self.crear_modcon(pol_id, data_alta)

        self.activar_polissa_CUPS()

        wiz_mod_id = wiz_obj.create(
            self.cursor, self.uid, {'state': 'init'}, context={'active_ids': [meter_id]}
        )
        wizard = wiz_obj.browse(self.cursor, self.uid, wiz_mod_id)

        expected = '6.1A'
        result = wizard.check_tariff(meter_id, data_alta)
        self.assertEqual(result, expected)
