# -*- coding: utf-8 -*-
{
    "name": "Telemesura Distri",
    "description": """
    Modul de Telemesura a distribuidora
    - Afegeix generació de F1/P1
    """,
    "version": "0-dev",
    "author": "GISCE-TI",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_telemesures_distri",
        "giscedata_telemesura"
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "wizard/wizard_validate_61A_profiles_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
