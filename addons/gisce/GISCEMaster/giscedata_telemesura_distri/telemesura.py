from mongodb_backend import osv_mongodb


class TmProfile(osv_mongodb.osv_mongodb):

    _name = 'tm.profile'
    _inherit = 'tm.profile'

    CONVERSOR = 1

    def get_curve(self, cursor, uid, meter_id, date_from, date_to, cch_fact_type=True, valid=True):
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        if isinstance(meter_id, list):
            meter_id = meter_id[0]
        meter_name = meter_obj.read(cursor, uid, meter_id, ['name'])['name']

        search_params = [
            ('timestamp', '>=', date_from),
            ('timestamp', '<=', date_to),
            ('name', '=', meter_name),
            ('type', '=', 'p'),
            ('cch_fact', '=', cch_fact_type),
            ('valid', '=', valid)
        ]
        return self.search(cursor, uid, search_params, order='timestamp asc')


TmProfile()
