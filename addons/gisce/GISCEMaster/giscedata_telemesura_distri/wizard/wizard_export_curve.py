# -*- coding: utf-8 -*-
import time
from tools.translate import _
from osv import osv, fields
from osv.expression import OOQuery


class WizardExportCurve(osv.osv_memory):

    _name = 'wizard.export.curve'
    _inherit = 'wizard.export.curve'

    def get_f1_meters(self, cursor, uid, di, df, context=None):
        """
        Get the ids of telemesura meters type 1 and 2 (REE)
        """
        if context is None:
            context= {}

        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_ids = []

        where_params = [
            ('potencia', '>', 450),
            ('data_inici', '<=', df),
            ('data_final', '>=', di),
        ]

        selected_comers = context.get('selected_comers', [])
        if selected_comers:
            where_params.append(('comercialitzadora.ref', 'in', selected_comers))

        q = OOQuery(modcon_obj, cursor, uid)
        sql = q.select(
            ['polissa_id', 'data_inici', 'data_final'],
            only_active=False
        ).where(where_params)
        cursor.execute(*sql)
        modcons = cursor.dictfetchall()
        for modcon in modcons:
            qc = OOQuery(meter_obj, cursor, uid)
            sql = qc.select(['id'], only_active=False).where(
                [('polissa', '=', modcon['polissa_id']),
                 ('technology_type', 'in', ['telemeasure', 'electronic']),
                 ('data_alta', '<=', modcon['data_final']),
                 '|',
                 ('data_baixa', '>=', modcon['data_inici']),
                 ('data_baixa', '=', None)])
            cursor.execute(*sql)
            try:
                meter_ids.append(cursor.fetchone()[0])
            except:
                raise osv.except_osv(_(u"ERROR"), _(
                    u"No hi han comptadors"
                ))
        return meter_ids


WizardExportCurve()
