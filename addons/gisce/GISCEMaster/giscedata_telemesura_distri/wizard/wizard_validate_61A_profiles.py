# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from pytz import timezone
from datetime import datetime, timedelta
import logging

TIMEZONE = timezone('Europe/Madrid')


class WizardValidate61AProfiles(osv.osv_memory):
    """Wizard per validar els perfils amb tarifa 6.1A"""

    _name = 'wizard.validate.61A.profiles'

    def _default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}

        obj_lectura = self.pool.get('giscedata.lectures.comptador')
        comptadors_ids = context.get('active_ids', [])
        comptadors = obj_lectura.read(
            cursor,
            uid,
            comptadors_ids,
            ['name'],
            context=context
        )
        info = _("We will validate the curves for the meters: \n{0}").format(
            ', '.join([x['name'] for x in comptadors])
        )

        return info

    @staticmethod
    def patch_date_to_complete_period(start_date, end_date):
        """
        Get first and last tg.profile slot timestamp from a period.

        :param start_date: first date of the period LIKE 'YYYY-MM-DD'
        :param end_date: last date of the period LIKE 'YYYY-MM-DD'
        :return: two str date_periods LIKE 'YYYY-MM-DD HH:mm:ss'
        """

        di = "{0} 01:00:00".format(start_date)
        df = "{0} 00:00:00".format(
            (datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
             ).strftime("%Y-%m-%d")
        )

        return di, df

    def check_technology(self, cursor, uid, ids, meter_id, context=None):
        if context is None:
            context = {}
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_data = meter_obj.read(
            cursor, uid, meter_id, ['technology_type'],
            context=context
        )
        return meter_data['technology_type']

    def check_tariff(self, cursor, uid, ids, meter_id, date_read, context=None):
        if context is None:
            context = {}
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_obj = self.pool.get('tm.profile')

        meter_data = meter_obj.read(
            cursor, uid, meter_id, ['name'],
            context=context
        )
        serial = meter_data['name']

        return tm_obj.get_tariff_by_meter(cursor, uid, serial, date_read)

    def fix_61A(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        info_list = []
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_obj = self.pool.get('tm.profile')

        logger = logging.getLogger('openerp.{0}'.format(__name__))

        wizard = self.browse(cursor, uid, ids[0], context)
        active_ids = context.get('active_ids', [])

        date_start = wizard.date_from
        date_end = wizard.date_to

        for meter_id in active_ids:
            if self.check_technology(cursor, uid, ids, meter_id, context=context) in ['electronic', 'telemeasure']:
                if self.check_tariff(cursor, uid, ids, meter_id, date_end) == '6.1A':
                    meter = meter_obj.read(cursor, uid, meter_id, [], context=context)
                    di_ts, df_ts = self.patch_date_to_complete_period(date_start, date_end)
                    tm_ids = tm_obj.search(cursor, uid, [
                        ('timestamp', '>=', di_ts),
                        ('timestamp', '<=', df_ts),
                        ('name', '=', meter['name']),
                        ('type', '=', 'p'),
                        ('valid', '=', True),
                        ('cch_fact', '=', False)
                    ])
                    if not tm_ids:
                        tm_ids_check = tm_obj.search(cursor, uid, [
                            ('timestamp', '>=', di_ts),
                            ('timestamp', '<=', df_ts),
                            ('name', '=', meter['name']),
                            ('type', '=', 'p'),
                            ('valid', '=', True),
                            ('cch_fact', '=', True)
                        ])
                        if tm_ids_check:
                            msg = _('Meter {} already fixed.').format(meter['name'])
                            logger.info(msg)
                            info_list.append(msg)
                            continue
                        else:
                            msg = _('Unable to fix meter {}.').format(meter['name'])
                            logger.info(msg)
                            info_list.append(msg)
                            continue
                    res = tm_obj.read(cursor, uid, tm_ids, [
                        'timestamp', 'ai_fact', 'kind_fact', 'name', 'ai', 'ae', 'r1', 'r2', 'r3', 'r4',
                        'cch_bruta', 'cch_fact', 'valid_date'
                    ])
                    try:
                        tm_obj.fix_61A_curve(res)
                    except osv.except_osv:
                        msg = _('Incomplete profile in meter {}.').format(meter['name'])
                        logger.info(msg)
                        info_list.append(msg)
                        continue
                    for x in res:
                        id_profile = x.pop('id')
                        tm_obj.write(cursor, uid, [id_profile], x)
                    msg = _('Meter {} fixed.').format(meter['name'])
                    info_list.append(msg)

                else:
                    meter = meter_obj.read(cursor, uid, meter_id, [], context=context)
                    msg = _('Meter {} is not in a policy with 6.1A tariff.').format(meter['name'])
                    info_list.append(msg)

            else:
                # TODO: Possibility to use the same fix in TG meters
                meter = meter_obj.read(cursor, uid, meter_id, [], context=context)
                msg = _('Meter {} is not a TM meter.').format(meter['name'])
                info_list.append(msg)

        if info_list:
            wizard.write({'info': '\n'.join(info_list)})
        else:
            msg = _('No meters fixed.')
            wizard.write({'info': msg})
        wizard.write({'state': 'end'})

    _columns = {
        'state': fields.selection(
            [
                ('init', 'Start'),
                ('end', 'End')
            ],
            'State'
        ),
        'date_from': fields.date(
            'Date from', required=True,
            help='Full month, last day excluded. Example: 01/05/2019 to 01/06/2019'
        ),
        'date_to': fields.date(
            'Date to', required=True,
            help='Full month, last day excluded. Example: 01/05/2019 to 01/06/2019'
        ),
        'info': fields.text('Info'),

    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
    }


WizardValidate61AProfiles()
