# -*- coding: utf-8 -*-
import subprocess
import tempfile
import base64
import os
import netsvc
from tools import config

from osv import osv, fields
import tools
from tools.translate import _
from datetime import date

OPCIONS = [('lat', 'LAT CSV (1)'), ('lbt', 'BT CSV (2)'),
           ('sub', 'Subestacions CSV (3)'), ('pos', 'Posicions CSV (4)'),
           ('maq', 'Maquines CSV (5)'), ('desp', 'Despatx CSV (6)'),
           ('fia', 'Fiabilitat CSV (7)'), ('cts', 'CTS CSV (8)')]


class WizardGenerarCnmcResolucio4771(osv.osv_memory):
    """Wizard per generar els XML de CNMC de inventari"""
    _name = 'wizard.generar.cnmc.resolucio.4771'
    _max_hours = 10

    def action_exec_script(self, cursor, uid, ids, context=None):
        """Llença l'script per l'informe de l'inventari.
        """
        script_name = {'lat': ['res_4771_lat', 'LAT CSV'],
                       'lbt': ['res_4771_lbt', 'BT CSV'],
                       'sub': ['res_4771_sub', 'Subestacions CSV'],
                       'pos': ['res_4771_pos', 'Posicions CSV'],
                       'maq': ['res_4771_maq', 'Maquina CSV'],
                       'desp': ['res_4771_des', 'Despatx CSV'],
                       'fia': ['res_4771_fia', 'Fiabilitat CSV'],
                       'cts': ['res_4771_cts', 'CTS CSV']}

        wizard = self.browse(cursor, uid, ids[0], context)

        #Agafar l'script que seleccionem al tipus
        exe = script_name.get(wizard.tipus)[0]

        # Crido la llibreria de CNMC
        user = self.pool.get('res.users').browse(cursor, uid, uid)
        port = tools.config['port'] or 8069
        filename = tempfile.mkstemp()[1]
        args = ['cnmc', exe, '-d', cursor.dbname, '-p', port, '-u', user.login,
                '-w', user.password, '--no-interactive', '-o', filename,
                '-c', wizard.r1, '-y', wizard.anyo]

        # Afegir la crida al log
        logger = netsvc.Logger()
        logger.notifyChannel(
            'server', netsvc.LOG_INFO,
            'libcnmc executed: {}'.format(' '.join(map(str, args)))
        )

        # Carrega el fitxer CSV
        env = os.environ.copy()
        sentry_dsn = config.get('sentry_dsn')
        if sentry_dsn:
            env['SENTRY_DSN'] = sentry_dsn
        retcode = subprocess.call(map(str, args), env=env)
        if retcode:
            raise osv.except_osv(
                'Error',
                _('El procés de generar el fitxer ha fallat. Codi: %s')
                % retcode
            )

        tmpxmlfile = open(filename, 'r+')
        report = base64.b64encode(tmpxmlfile.read())
        filenom = ''
        if wizard.tipus == 'lat':
            filenom = 'Inventario_R1-%s_1.txt' % wizard.r1
        elif wizard.tipus == 'lbt':
            filenom = 'Inventario_R1-%s_2.txt' % wizard.r1
        elif wizard.tipus == 'sub':
            filenom = 'Inventario_R1-%s_3.txt' % wizard.r1
        elif wizard.tipus == 'pos':
            filenom = 'Inventario_R1-%s_4.txt' % wizard.r1
        elif wizard.tipus == 'maq':
            filenom = 'Inventario_R1-%s_5.txt' % wizard.r1
        elif wizard.tipus == 'desp':
            filenom = 'Inventario_R1-%s_6.txt' % wizard.r1
        elif wizard.tipus == 'fia':
            filenom = 'Inventario_R1-%s_7.txt' % wizard.r1
        elif wizard.tipus == 'cts':
            filenom = 'Inventario_R1-%s_8.txt' % wizard.r1

        wizard.write({'name': filenom, 'file': report, 'state': 'done'})
        os.unlink(filename)

    def action_another(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)

        wizard.write({'state': 'init'})

    def _default_r1(self, cursor, uid, context=None):
        ''' Gets R1 code from company if defined '''
        if not context:
            context = {}

        company_obj = self.pool.get('res.company')
        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref2

        if r1 and r1.startswith('R1-'):
            r1 = r1[3:]

        return r1 or ''

    _columns = {
        'state': fields.selection([('init', 'Init'),
                                   ('done', 'Done'), ],
                                  'State'),
        'name': fields.char('File name', size=64),
        'file': fields.binary('Fitxer generat'),
        'r1': fields.char('Codi R1', size=12,
                          help=u'Agafa automàticament el camp ref2 de la '
                               u'Empresa configurada a la Companyia'),
        'tipus': fields.selection(OPCIONS, 'Tipus', required=True),
        'anyo': fields.integer('Any', size=4),
        'csv_lat': fields.binary('Fitxer LAT'),
        'csv_lbt': fields.binary('Fitxer BT'),
        'csv_sub': fields.binary('Fitxer Subestacions'),
        'csv_pos': fields.binary('Fitxer Posicions'),
        'csv_maq': fields.binary('Fitxer Màquines'),
        'csv_desp': fields.binary('Fitxer Despatx'),
        'csv_fia': fields.binary('Fitxer Fiabilitat'),
        'csv_trans': fields.binary('Fitxer CTS')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'tipus': lambda *a: 'lat',
        'anyo': lambda *a: date.today().year - 1,
        'r1': _default_r1,
    }

WizardGenerarCnmcResolucio4771()
