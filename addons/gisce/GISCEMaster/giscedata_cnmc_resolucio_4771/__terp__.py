# -*- coding: utf-8 -*-
{
    "name": "GISCE CNMC 4771",
    "description": """Este modulo añade la funcionalidad para generar los informes de la CNMC 4771""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_extended_distri",
        "partner_address_tipovia",
        "giscedata_at",
        "giscedata_bt",
        "giscedata_despatx",
        "giscedata_administracio_publica_cne",
        "giscedata_cts",
        "giscedata_celles",
        "giscedata_transformadors",
        "giscedata_cts_subestacions",
        "giscedata_tensions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cnmc_resolucio_4771_view.xml",
        "wizard/wizard_generar_cnmc_resolucio_4771.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
