# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCts(osv.osv):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    def get_codi_instalacio_xmlrpc(self, cursor, uid, ids, context=None):
        res = self.get_codi_instalacio(cursor, uid, ids, context)

        return [(str(k), v) for k, v in res.items()]

    def get_codi_instalacio(self, cursor, uid, ids, context=None):
        res = {}

        ct_fields = ['id_installacio', 'transformadors', 'potencia',
                     'id_subtipus']

        ct_subtipus_obj = self.pool.get('giscedata.cts.subtipus')
        cne_tipus_obj = self.pool.get('giscedata.cne.ct.tipus')

        for ct in self.read(cursor, uid, ids, ct_fields):
            ct_id = ct['id']
            installacio_name = ct['id_installacio'][1]
            ct_potencia = ct['potencia']
            transformadors = ct['transformadors']
            ct_id_subtipus = int(ct['id_subtipus'][0])

            codi = 22
            #Si no es del tipus CT sera 000
            if installacio_name != 'CT':
                res[ct_id] = 'TI-000'
                continue
            #Si tinc dos transformadors dividirem la potencia per 2
            if len(transformadors) == 2:
                codi += 10
                potencia = int(ct_potencia) / 2
            else:
                potencia = int(ct_potencia)

            if 15 < potencia <= 25:
                codi += 1
            elif 25 < potencia <= 50:
                codi += 2
            elif 50 < potencia <= 100:
                codi += 3
            elif 100 < potencia <= 160:
                codi += 4
            elif 160 < potencia <= 250:
                codi += 5
            elif 250 < potencia <= 400:
                codi += 6
            elif 400 < potencia <= 630:
                codi += 7
            elif 630 < potencia <= 1000:
                codi += 8
            elif 1000 < potencia <= 1250 or potencia > 1250:
                codi += 9

            ct_sub_vals = ct_subtipus_obj.read(cursor, uid, ct_id_subtipus,
                                               ['categoria_cne'], context)

            if 'categoria_cne' in ct_sub_vals:
                cne_tipo_id = ct_sub_vals['categoria_cne'][0]
                cne_codi_vals = cne_tipus_obj.read(cursor, uid, cne_tipo_id,
                                                   ['codi'], context)
                cne_codi = cne_codi_vals['codi']

                if cne_codi == 'L':
                    codi += 20
                elif cne_codi == 'I':
                    codi += 40
                elif cne_codi == 'S':
                    codi += 46

            res[ct_id] = 'TI-%s' % ('%s' % codi).zfill(3)
        return res


GiscedataCts()