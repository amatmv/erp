# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscemiscCallejero(osv.osv):

    _name = 'giscemisc.callejero'

    _rec_name = 'calle'

    def name_get(self, cr, uid, ids, context=None):
        if not len(ids):
            return []
        res = [(r['id'], r['municipi'] and r['municipi'][1] + ' - ' + r['calle'] or '')
               for r in self.read(cr, uid, ids, ['municipi', 'calle'], context=context)]
        return res

    _columns = {
        'tipo_via': fields.many2one('res.tipovia', 'Tipo Via'),
        'municipi': fields.many2one('res.municipi', 'Municipio'),
        'calle': fields.char('Calle', size=100)
    }


GiscemiscCallejero()
