# -*- coding: utf-8 -*-
{
    "name": "Modulo de callejero",
    "description": """Gestion de callejero""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "partner_address_tipovia",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "security/giscedata_callejero_security.xml",
        "giscemisc_callejero_view.xml",
        "partner_extend_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
