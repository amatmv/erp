# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestsCallejeros(testing.OOTestCase):
    def test_create_callejero(self):
        self.openerp.install_module('giscedata_tipovia_catastro')
        imd_obj = self.openerp.pool.get('ir.model.data')
        callejero_obj = self.openerp.pool.get('giscemisc.callejero')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tv_calle_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tipovia_catastro', 'tipovia_CL'
            )[1]

            municipi_id = imd_obj.get_object_reference(
                cursor, uid, 'base_extended', 'ine_17079'
            )[1]

            new_callejero_id = callejero_obj.create(
                cursor, uid,
                {'tipo_via': tv_calle_id,
                 'municipi': municipi_id,
                 'calle': u'del Carme'
                 }
            )

            self.assertTrue(new_callejero_id)

            callejero = callejero_obj.read(cursor, uid, new_callejero_id, [])

            self.assertEqual(callejero['tipo_via'][1], u'CALLE')
            self.assertEqual(callejero['calle'], u'del Carme')
            self.assertEqual(callejero['municipi'][1], u'Girona')

    def test_onchange_callejero_actualiza_partner_address(self):
        self.openerp.install_module('giscedata_tipovia_catastro')

        imd_obj = self.openerp.pool.get('ir.model.data')
        callejero_obj = self.openerp.pool.get('giscemisc.callejero')
        addr_obj = self.openerp.pool.get('res.partner.address')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            tv_calle_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_tipovia_catastro', 'tipovia_CL'
            )[1]

            municipi_id = imd_obj.get_object_reference(
                cursor, uid, 'base_extended', 'ine_17079'
            )[1]

            new_callejero_id = callejero_obj.create(
                cursor, uid,
                {'tipo_via': tv_calle_id,
                 'municipi': municipi_id,
                 'calle': u'del Carme'
                 }
            )

            self.assertTrue(new_callejero_id)

            partner_addr_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_8'
            )[1]

            addr_obj.write(
                cursor, uid, [partner_addr_id],
                {'callejero_id': new_callejero_id}
            )

            res = addr_obj.onchange_callejero(
                cursor, uid, [partner_addr_id], new_callejero_id
            )

            addr_obj.write(cursor, uid, [partner_addr_id], res['value'])

            add_reads = addr_obj.read(cursor, uid, partner_addr_id, [])

            self.assertEqual(add_reads['city'], u'Girona')

            self.assertEqual(add_reads['tv'][0], tv_calle_id)

            self.assertEqual(add_reads['id_municipi'][0], municipi_id)

            self.assertEqual(add_reads['nv'], u'del Carme')

            self.assertEqual(add_reads['street'], u'CL. del Carme')

            self.assertEqual(add_reads['country_id'][1], u'España')

            self.assertEqual(add_reads['state_id'][1], u'Girona')
