# -*- coding: utf-8 -*-
{
    "name": "Index CUPS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index for CUPS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "base_index",
        "giscedata_cups",
        "giscegis_cups",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
