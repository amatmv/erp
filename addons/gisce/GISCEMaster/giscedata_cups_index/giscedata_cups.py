from base_index.base_index import BaseIndex
from osv import osv


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def write(self, cr, uid, ids, vals, context=None):
        """
        Overwrites the write function to re-index the element when the field
        `polissa_polissa` is

        :param cr: Database curs
        :param uid: User id
        :type uid: int
        :param ids: Afected ids
        :type ids: list(int)
        :param vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: list of afected ids
        :rtype: list(int)
        """
        if context is None:
            context = {}
        if 'cups' in vals or 'state' in vals:
            cups_obj = self.pool.get('giscedata.cups.ps')
            for data in self.read(cr, uid, ids, ["cups"], context):
                cups_id = data.get("cups", False)
                if cups_id:
                    cups_obj.index(cr, uid, cups_id[0], context)
        ids = super(GiscedataPolissa, self).write(cr, uid, ids, vals, context)
        return ids


GiscedataPolissa()


class GiscedataCupsPs(BaseIndex):
    """
    Module to index giscedata.cups.ps
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _index_title = '{obj.name} {obj.titular:.20}'
    _index_summary = '{obj.direccio}'

    _index_fields = {
        'name': True,
        'id_municipi.name': True,
        'polissa_polissa.name': True,
        'polissa_comptador': True,
        'direccio': True,
        'titular': True,
        'zona': True,
        'ordre': True,
        'et': True,
        'linia': lambda self, data: data and '{0} L{0}'.format(data) or '',
        'id_escomesa.name': True,
    }

    def get_schema(self, cursor, uid, item, context=None):
        """
        Gets the index schema

        :param cursor: Database cursor
        :param uid: User id
        :param item: item to index
        :param context: OpenERP context
        :return: Dict with the schema
        """

        lat, lon = self.get_lat_lon(cursor, uid, item, context=context)
        content = self.get_content(cursor, uid, item, context=context)
        if item.titular:
            title = self._index_title.format(obj=item)
            if len(item.titular) > 20:
                title = '{} ...'.format(title)
        else:
            title = '{obj.name}'.format(obj=item)

        return {
            'id': item.id,
            'path': self.get_path(cursor, uid, item.id, context=context),
            'title': title,
            'content': ' '.join(set(content)),
            'gistype': self._name,
            'summary': self._index_summary.format(obj=item),
            'lat': lat,
            'lon': lon
        }


GiscedataCupsPs()
