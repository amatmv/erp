# -*- coding: utf-8 -*-
{
    "name": "Partner extension show only suppliers",
    "description": """Show only partners that are also suppliers""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Generic Modules/Suppliers",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_supplier_view.xml"
    ],
    "active": False,
    "installable": True
}
