# -*- encoding: utf-8 -*-
from osv import osv, fields


class account_invoice(osv.osv):

    _name = "account.invoice"
    _inherit = "account.invoice"

    _columns = {
        'purchase_ids': fields.one2many(
            'purchase.order', 'invoice_id', 'Pedidos de compra'
        )
    }


account_invoice()
