# -*- coding: utf-8 -*-
{
    "name": "GISCE Purchase",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "purchase",
        "giscedata_stock",
        "c2c_webkit_report",
        "partner_address_tipovia",
        "mrp",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_purchase_report.xml",
        "purchase_view.xml",
        "product_view.xml",
        "invoice_view.xml",
        "product_data.xml",
    ],
    "active": False,
    "installable": True
}
