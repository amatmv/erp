# -*- encoding: utf-8 -*-

from osv import fields
from osv import osv
import time
from tools.translate import _


class purchase_order(osv.osv):

    _name = 'purchase.order'
    _inherit = 'purchase.order'

    def wkf_approve_order(self, cursor, uid, ids, context=None):
        """
        Sets the date approve of the purchase order.
        :param cursor:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <purchase.order> ids.
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: wkf_approve_order parent result.
        """
        res = super(purchase_order, self).wkf_approve_order(
            cursor, uid, ids, context=context
        )
        if res:
            self.write(cursor, uid, ids, {
                'date_approve': time.strftime('%Y-%m-%d %H:%M:%S'),
            })

        return res

    def wkf_confirm_order(self, cr, uid, ids, context=None):
        """
        Warns if the purchase order has no partner or partner address when
        confirming it.
        :param cr:
        :param uid: <res.users> id
        :type uid: long
        :param ids: <purchase.order> ids.
        :type ids: [long]
        :param context: OpenERP context
        :type context: {}
        :return: wkf_confirm_order parent result.
        """
        columns = ['partner_id', 'partner_address_id']
        for po in self.read(cr, uid, ids, columns, context=context):
            if not po['partner_id']:
                raise osv.except_osv(
                    _(u"Atención"),
                    _(u"Para poder confirmar el pedido de compra se necesita "
                      u"el proveedor")
                )
            if not po['partner_address_id']:
                raise osv.except_osv(
                    _(u"Atención"),
                    _(u"Para poder confirmar el pedido de compra se necesita "
                      u"la dirección del proveedor")
                )
        res = super(purchase_order, self).wkf_confirm_order(
            cr, uid, ids, context=context
        )
        if res:
            now = time.strftime('%Y-%m-%d %H:%M:%S')
            self.write(cr, uid, ids, {'date_confirm': now})

        return res

    _columns = {
        'date_approve': fields.datetime('Fecha de aprobación', readonly=True),
        'date_confirm': fields.datetime('Fecha de confirmación', readonly=True),
        'numero_oferta': fields.char('Número de oferta', size=64),
        'partner_id': fields.many2one(
            'res.partner', 'Proveedor', change_default=True, required=False,
            states={
                'confirmed': [('readonly', True)],
                'approved': [('readonly', True)],
                'done': [('readonly', True)]},
        ),
        'partner_address_id': fields.many2one(
            'res.partner.address', 'Dirección proveedor', required=False,
            states={
                'posted': [('readonly', True)]
            }
        ),
    }


purchase_order()
