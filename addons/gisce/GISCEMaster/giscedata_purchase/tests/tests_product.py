# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime, timedelta
import random


class TestsProductTemplate(testing.OOTestCase):
    def setUp(self):
        self.pool = self.openerp.pool

        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

        irmd_o = self.pool.get('ir.model.data')

        self.location_customers_id = irmd_o.get_object_reference(
            self.cursor, self.uid, 'stock', 'stock_location_customers'
        )[1]
        self.pricelist_0_id = irmd_o.get_object_reference(
            self.cursor, self.uid, 'product', 'list0'
        )[1]
        self.uom_unit_id = irmd_o.get_object_reference(
            self.cursor, self.uid, 'product', 'product_uom_unit'
        )[1]

    def tearDown(self):
        self.txn.stop()

    def test_last_purchase_price(self):
        """Checks if the last purchase price sets correctly when purchasing
        a product."""
        purchase_o = self.pool.get('purchase.order')
        purchase_line_o = self.pool.get('purchase.order.line')
        product_o = self.pool.get('product.product')

        # Busquem un producte random
        product_ids = product_o.search(self.cursor, self.uid, [])
        product_id = random.choice(product_ids)

        # Creem una comanda de compra
        purchase_cv = {
            'location_id': self.location_customers_id,
            'pricelist_id': self.pricelist_0_id,
        }
        purchase_id = purchase_o.create(self.cursor, self.uid, purchase_cv)

        # Escollim un preu de compra random
        price_unit = random.uniform(1, 1000)

        # Creem una linea de compra amb el producte random i el preu random i
        # l'associem a la comanda de compra anteriorment creada
        purchase_line_cv = {
            'order_id': purchase_id,
            'product_id': product_id,
            'price_unit': price_unit,
            'product_qty': 3,
            'name': 'POL-TEST',
            'product_uom': self.uom_unit_id,
            'date_planned': '2100-01-01'
        }
        purchase_id = purchase_line_o.create(self.cursor, self.uid, purchase_line_cv)

        # Comprovem que el valor del camp que guarda el preu d'última compra
        # del producte random es igual al preu random.
        purchase = purchase_o.browse(self.cursor, self.uid, purchase_id)

        for purchase_line in purchase.order_line:
            product = purchase_line.product_id
            self.assertEqual(product.last_purchase_price, price_unit)

    def test_last_purchase_price_no_purchase(self):
        purchase_line_o = self.pool.get('purchase.order.line')
        product_o = self.pool.get('product.product')

        # Busquem tots els productes els quals no hagin tingut cap compra
        product_ids = product_o.search(self.cursor, self.uid, [])

        purchase_line_ids = purchase_line_o.search(self.cursor, self.uid, [])
        purchase_line_vs = purchase_line_o.read(
            self.cursor, self.uid, purchase_line_ids, ['product_id']
        )
        product_purchased_ids = [x['product_id'][0] for x in purchase_line_vs]

        product_not_purchased_ids = set(product_ids) - set(product_purchased_ids)
        product_not_purchased_ids = list(product_not_purchased_ids)

        # Dels que no tenen cap compra, n'escollim un de random
        product_id = random.choice(product_not_purchased_ids)

        # Comprovem que el preu d'última compra és el mateix que el preu
        # estàndard
        product = product_o.browse(self.cursor, self.uid, product_id)

        self.assertEqual(product.last_purchase_price, product.standard_price)

    def test_historic_average_price(self):
        """Checks if the average price is computed correctly when purchasing
        a product"""
        product_o = self.pool.get('product.product')
        purchase_line_o = self.pool.get('purchase.order.line')
        purchase_o = self.pool.get('purchase.order')

        # Escollim un producte random
        product_ids = product_o.search(self.cursor, self.uid, [])
        product_id = random.choice(product_ids)

        # Busquem les compres del producte
        dmn = [('product_id', '=', product_id),
               ('order_id.state', '=', 'done')]
        purchase_line_ids = purchase_line_o.search(self.cursor, self.uid, dmn)
        purchase_line_f = ['price_subtotal', 'product_qty']
        purchase_line_vs = purchase_line_o.read(
            self.cursor, self.uid, purchase_line_ids, purchase_line_f
        )

        # Calculem el seu preu mig
        total_price = 0
        total_units = 0
        for purchase_line_v in purchase_line_vs:
            total_price += purchase_line_v['price_subtotal']
            total_units += purchase_line_v['product_qty']

        average_price = 0
        if total_units > 0:
            average_price = total_price / total_units

        # Creem X ordres de compra amb una linea de compra cadascuna amb el
        # producte random i un preu random.
        purchase_cv = {
            'location_id': self.location_customers_id,
            'pricelist_id': self.pricelist_0_id,
        }

        n_purchases = random.randint(2, 4)
        purchase_averages = [average_price] if average_price else []
        for i in range(2, n_purchases):
            purchase_id = purchase_o.create(self.cursor, self.uid, purchase_cv)

            price_unit = random.uniform(1, 1000)

            purchase_line_cv = {
                'order_id': purchase_id,
                'product_id': product_id,
                'price_unit': price_unit,
                'product_qty': i,
                'name': 'POL-TEST {}'.format(i),
                'product_uom': self.uom_unit_id,
                'date_planned': '2100-01-01'
            }
            purchase_line_o.create(self.cursor, self.uid, purchase_line_cv)

            purchase = purchase_o.browse(self.cursor, self.uid, purchase_id)
            for order_line in purchase.order_line:
                average = order_line.price_subtotal / order_line.product_qty
                purchase_averages.append(average)

            # Forcem l'estat de la comanda per forçar el trigger que recalcula
            # el preu mig
            purchase.write({'state': 'done'})

        # Calculem el preu mig de les compres fetes i de la compra original (si
        # es que n'hi havia).
        average = sum(purchase_averages) / len(purchase_averages)
        average = round(average, 6)

        # Comprovem que el valor del camp que conté el preu mig del producte
        # random és el mateix que el preu mig que hem generat
        product_f = ['historic_average_price']
        product_v = product_o.read(self.cursor, self.uid, product_id, product_f)

        self.assertEqual(product_v['historic_average_price'], average)

    def test_historic_average_price_in_range(self):
        """Checks if the average price is computed correctly when purchasing
        a product. It tests when the stock_average_price_cost_method_range is
        enabled"""
        product_o = self.pool.get('product.product')
        purchase_line_o = self.pool.get('purchase.order.line')
        purchase_o = self.pool.get('purchase.order')
        varconf_o = self.pool.get('res.config')

        # Indiquem que només volem tenir en compte les compres fetes en els
        # últims 2 dies
        dmn = [('name', '=', 'stock_average_price_cost_method_range')]
        varconf_id = varconf_o.search(self.cursor, self.uid, dmn)
        purchase_range = 2
        varconf_o.write(self.cursor, self.uid, varconf_id, {'value': purchase_range})

        # Escollim un producte random
        product_ids = product_o.search(self.cursor, self.uid, [])
        product_id = random.choice(product_ids)

        # Busquem les compres del producte
        dmn = [('product_id', '=', product_id),
               ('order_id.state', '=', 'done')]
        purchase_line_ids = purchase_line_o.search(self.cursor, self.uid, dmn)
        purchase_line_f = ['price_subtotal', 'product_qty']
        purchase_line_vs = purchase_line_o.read(
            self.cursor, self.uid, purchase_line_ids, purchase_line_f
        )

        # Calculem el seu preu mig
        total_price = 0
        total_units = 0
        for purchase_line_v in purchase_line_vs:
            total_price += purchase_line_v['price_subtotal']
            total_units += purchase_line_v['product_qty']

        average_price = 0
        if total_units > 0:
            average_price = total_price / total_units

        # Creem 4 ordres de compra amb una linea de compra cadascuna amb el
        # producte random i un preu random. 2 dins del rang i 2 fora del rang.
        purchase_cv = {
            'location_id': self.location_customers_id,
            'pricelist_id': self.pricelist_0_id,
        }

        purchase_averages = [average_price] if average_price else []
        for i in range(1, 5):
            purchase_id = purchase_o.create(self.cursor, self.uid, purchase_cv)

            price_unit = random.uniform(1, 1000)

            purchase_line_cv = {
                'order_id': purchase_id,
                'product_id': product_id,
                'price_unit': price_unit,
                'product_qty': i,
                'name': 'POL-TEST {}'.format(i),
                'product_uom': self.uom_unit_id,
                'date_planned': '2100-01-01',
            }
            purchase_line_o.create(self.cursor, self.uid, purchase_line_cv)

            purchase = purchase_o.browse(self.cursor, self.uid, purchase_id)

            if i <= purchase_range:
                # Només computem el preu mig si el numero de dies a restar
                # a partir d'avui de la comanda de compra es mes petit
                # que el rang proposat.
                for order_line in purchase.order_line:
                    average = order_line.price_subtotal / order_line.product_qty
                    purchase_averages.append(average)

            # Forcem l'estat de la comanda per forçar el trigger que recalcula
            # el preu mig i actualitzem la data de la comanda per deixar unes
            # dins del rang i altres fora.
            date_order = datetime.today() - timedelta(days=i)
            date_order = date_order.strftime("%Y-%m-%d")
            purchase.write({'state': 'done', 'date_order': date_order})

        # Calculem el preu mig de les compres fetes i de la compra original (si
        # es que n'hi havia).
        average = sum(purchase_averages) / len(purchase_averages)
        average = round(average, 6)

        # Comprovem que el valor del camp que conté el preu mig del producte
        # random és el mateix que el preu mig que hem generat
        product_f = ['historic_average_price']
        product_v = product_o.read(self.cursor, self.uid, product_id, product_f)

        self.assertEqual(product_v['historic_average_price'], average)
