# coding=utf-8
import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Fill standard user price if cost method is Standard price')

    q = """
        UPDATE product_template
        SET standard_user_price = standard_price
        WHERE cost_method = 'standard'
    """

    cursor.execute(q)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
