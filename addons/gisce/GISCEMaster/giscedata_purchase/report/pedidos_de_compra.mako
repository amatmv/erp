## coding=utf-8
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <link rel="stylesheet" href="${addons_path}/giscedata_purchase/report/pedidos_de_compra.css"/>
        ${self.custom_css()}
    </head>
    <body>
        <%
            n_order = 0
        %>
        %for order in objects:
            <%
                address_obj = pool.get('res.partner.address')
                purchase_order_obj = pool.get('purchase.order')

                enterprise = company

                if 'company_id' in purchase_order_obj.fields_get(cursor, uid).keys():
                    enterprise = order.company_id

                address_id = enterprise.partner_id.address_get(['invoice'])['invoice']
                address = address_obj.browse(cursor, uid, address_id)

                supplier = order.partner_id
                supplier_address_id = supplier.address_get()['default']
                supplier_address = address_obj.browse(cursor, uid, supplier_address_id)
                supplier_vat = supplier.vat
                if not supplier_vat:
                    supplier_vat = ""
                oferta = order.numero_oferta # per determinar el nom del nou camp
                supplier_zip = supplier_address.zip
                supplier_poblacion = supplier_address.id_poblacio.name
                supplier_provincia = supplier_address.state_id.name

                supplier_location = "{}, ".format(supplier_zip) if supplier_zip else ""
                supplier_location += supplier_poblacion if supplier_poblacion else ""
                supplier_location += " ({})".format(supplier_provincia) if supplier_provincia else ""

                destination_address = order.location_id.address_id
                dest_addr_street = destination_address.street if destination_address.street else ""
                destination_zip = destination_address.zip

                dest_addr = True
                try:
                    destination_poblacion = destination_address.id_poblacio.name
                    destination_provincia = destination_address.state_id.name
                    destination_country = destination_address.country_id.name
                except:
                    dest_addr = ""

                if dest_addr:
                    dest_addr = "{}, ".format(destination_zip) if destination_zip else ""
                    dest_addr += destination_poblacion if destination_poblacion else ""
                    dest_addr += " ({})".format(destination_provincia) if destination_provincia else ""
                    dest_addr += "\n{}".format(destination_country) if destination_country else ""

                seller_order_number = order.partner_ref if order.partner_ref else ""
            %>
## BEGIN    HEADER
            <table id="header">
                <tr>
                    <td id="company-logo" rowspan="4"><img class="logo" src="data:image/jpeg;base64,${enterprise.logo}"/></td>
                    <td>${enterprise.name}</td>
                </tr>
                <tr>
                    <td>${enterprise.partner_id.vat}</td>
                </tr>
                <tr>
                    <td>${address.street}</td>
                </tr>
                <tr>
                    <td>${address.zip}, ${address.id_poblacio.name} (${address.state_id.name})</td>
                </tr>
            </table>
## END      HEADER
            <table id="order-info">
                 <colgroup>
                   <col span="1" style="width: 50%;">
                   <col span="1" style="width: 50%;">
                </colgroup>
                <thead>
                    <tr>
                        <th id="order-info-header" colspan="2">${_("Pedido de compra")} - ${order.name}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="separator">
                            <table id="datos-de-compra">
                                <colgroup>
                                   <col span="1" style="width: 50%;">
                                   <col span="1" style="width: 50%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th colspan="2">Datos de compra</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>${_("Comprador")}:</td>
                                        <td>${user.name}</td>
                                    </tr>
                                    <tr>
                                        <td>${_("Nº de pedido")}: </td>
                                        <td>${order.name}</td>
                                    </tr>
                                    <tr>
                                        <td>${_("Nº de pedido vendedor")}:</td>
                                        <td>${seller_order_number}</td>
                                    </tr>
                                    % if oferta:
                                        <tr>
                                            <td>${_("Oferta")}: </td>
                                            <td>${oferta}</td>
                                        </tr>
                                    % endif
                                    <tr>
                                        <td>${_("Fecha de solicitud")}: </td>
                                        <td>${order.date_order}</td>
                                    </tr>
                                    <tr class="shipping-address">
                                        <td rowspan="2">${_("Dirección de entrega")}: </td>
                                        <td>${dest_addr_street}</td>
                                    </tr>
                                    <tr class="shipping-address">
                                        <td>${dest_addr}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table id="datos-del-vendedor">
                                <tr>
                                    <th>${_("Datos del proveedor")}</th>
                                </tr>
                                <tr>
                                    <td>${supplier.name}</td>
                                </tr>
                                <tr>
                                    <td>${supplier_vat}</td>
                                </tr>
                                <tr>
                                    <td>${supplier_address.street}</td>
                                </tr>
                                <tr>
                                    <td>${supplier_location}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table id="order-lines">
                                <colgroup>
                                   <col span="1" style="width: 64%;">
                                   <col span="1" style="width: 10%;">
                                   <col span="1" style="width: 13%;">
                                   <col span="1" style="width: 13%;">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="subtitles">${_("Descripción")}</th>
                                        <th class="align-right subtitles">${_("Cantidad")}</th>
                                        <th class="align-right subtitles">${_("Precio")} (€)</th>
                                        <th class="align-right subtitles">${_("Total")} (€)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    %for line in order.order_line:
                                        <%
                                            price_unit = formatLang(line.price_unit, digits=2)
                                            subtotal = formatLang(line.price_subtotal, digits=2)
                                        %>
                                        <tr class="row-values">
                                            <td id="descr">${line.name}</td>
                                            <td class="qty">${line.product_qty}</td>
                                            <td class="qty">${price_unit}</td>
                                            <td class="qty bold">${subtotal}</td>
                                        </tr>
                                    %endfor
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%
                                untaxed = formatLang(order.amount_untaxed, digits=2)
                                taxed = formatLang(order.amount_tax, digits=2)
                                total = formatLang(order.amount_total, digits=2)
                            %>
                            <table id="summary">
                            %if untaxed!=total:
                                <tr>
                                    <th>${_("Subtotal")}</th>
                                    <td>${untaxed} €</td>
                                </tr>
                                <tr>
                                    <th id="impuestos">${_("IVA (21%)")}</th>
                                    <td>${taxed} €</td>
                                </tr>
                            %endif
                                <tr>
                                    <th>${_("Total")}</th>
                                    <td class="bold">${total} €</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    %if order.notes:
                        <tr><td id="notes-header" class="bold" colspan="2">${_("Notas")}</td></tr>
                        <tr><td id="notes-body" colspan="2">${order.notes.replace('\n','<br/>')}</td></tr>
                    %endif
                </tbody>
            </table>
            <%
                n_order += 1
            %>
            %if n_order < len(objects):
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    </body>
</html>
<%def name="custom_css()"/>
