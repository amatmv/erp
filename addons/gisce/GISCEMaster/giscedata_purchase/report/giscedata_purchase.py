# -*- coding: utf-8 -*-

from c2c_webkit_report import webkit_report
from report import report_sxw


webkit_report.WebKitParser(
    'report.giscedata.purchase.pedidos_de_compra',
    'purchase.order',
    'giscedata_purchase/report/pedidos_de_compra.mako',
    parser=report_sxw.rml_parse
)

webkit_report.WebKitParser(
    'report.giscedata.purchase.solicitud.presupuesto',
    'purchase.order',
    'giscedata_purchase/report/solicitud_presupuesto.mako',
    parser=report_sxw.rml_parse
)
