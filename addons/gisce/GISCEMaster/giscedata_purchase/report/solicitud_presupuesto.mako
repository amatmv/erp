## coding=utf-8
<%
    address_o = pool.get('res.partner.address')
    purchase_order_o = pool.get('purchase.order')


    def format_address(res_partner_address):
        dest_addr_street = res_partner_address.street if res_partner_address.street else ""
        destination_zip = res_partner_address.zip

        destination_poblacion = res_partner_address.id_municipi.name
        destination_provincia = res_partner_address.state_id.name
        destination_country = res_partner_address.country_id.name

        res = "{}, ".format(destination_zip) if destination_zip else ""
        res += destination_poblacion if destination_poblacion else ""
        res += " ({})".format(destination_provincia) if destination_provincia else ""
        res += "</br>{}".format(destination_country) if destination_country else ""
        res = '{}</br>{}'.format(dest_addr_street, res)
        return res
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
        <style type="text/css">
            ${css}
        </style>
        <link rel="stylesheet" href="${addons_path}/giscedata_purchase/report/solicitud_presupuesto.css"/>
    </head>
    <body>
        %for order in objects:
            <%
                enterprise = company

                if 'company_id' in purchase_order_o.fields_get(cursor, uid).keys():
                    enterprise = order.company_id

                address_id = enterprise.partner_id.address_get(['invoice'])['invoice']
                address = address_o.browse(cursor, uid, address_id)
#------------------------------------------------------------------------------#
                supplier = order.partner_id
                if supplier:
                    supplier_address_id = supplier.address_get()['default']
                    supplier_address = address_o.browse(cursor, uid, supplier_address_id)
                    supplier_vat = supplier.vat
                    if not supplier_vat:
                        supplier_vat = ""
                    supplier_zip = supplier_address.zip
                    supplier_poblacion = supplier_address.id_municipi.name

                    supplier_provincia = supplier_address.state_id.name

                    supplier_location = "{}, ".format(supplier_zip) if supplier_zip else ""
                    supplier_location += supplier_poblacion if supplier_poblacion else ""
                    supplier_location += " ({})".format(supplier_provincia) if supplier_provincia else ""
#------------------------------------------------------------------------------#
                destination_address = order.dest_address_id
                if not destination_address:
                    destination_address = order.location_id.address_id if order.location_id else False
            %>
            <!-- HEADER -->
            <table id="header">
                <tr>
                    <td id="company-logo" rowspan="4"><img class="logo" src="data:image/jpeg;base64,${enterprise.logo}"/></td>
                    <td>${enterprise.name}</td>
                </tr>
                <tr>
                    <td>${enterprise.partner_id.vat}</td>
                </tr>
                <tr>
                    <td>${address.street}</td>
                </tr>
                <tr>
                    <td>${address.zip}, ${address.id_poblacio.name} (${address.state_id.name})</td>
                </tr>
            </table>
            <!-- END HEADER-->
            <!-- REPORT BODY -->
            <table id="order-info">
                 <colgroup>
                   <col span="1" style="width: 50%;">
                   <col span="1" style="width: 50%;">
                </colgroup>
                <thead>
                    <tr>
                        <th id="order-info-header" colspan="2">${_("Solicitud de presupuesto")} - ${order.name}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <% separator = 'id="budget-request-info-separator"' if supplier else "" %>
                        <td ${separator}>
                            <table id="budget-request">
                                <colgroup>
                                   <col span="1" style="width: 50%;">
                                   <col span="1" style="width: 50%;">
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td>${_("Comprador")}:</td>
                                        <td>${user.name}</td>
                                    </tr>
                                    <tr>
                                        <td>${_("Fecha de solicitud")}: </td>
                                        <td>${order.date_order}</td>
                                    </tr>
                                    %if destination_address:
                                        <tr>
                                            <td rowspan="2">${_("Dirección de entrega")}:</td>
                                            <td>${format_address(destination_address)}</td>
                                        </tr>
                                    %endif
                                </tbody>
                            </table>
                        </td>
                        %if supplier:
                            <td>
                                <table id="supplier-info">
                                    <tr>
                                        <th>${_("Datos del proveedor")}</th>
                                    </tr>
                                    <tr>
                                        <td>${supplier.name}</td>
                                    </tr>
                                    <tr>
                                        <td>${supplier_vat}</td>
                                    </tr>
                                    <tr>
                                        <td>${format_address(supplier_address)}</td>
                                    </tr>
                                </table>
                            </td>
                        %endif
                    </tr>
                    <tr>
                        <td colspan="2">
                            <!-- BUDGET REQUEST LINES -->
                            <table id="order-lines">
                                <colgroup>
                                   <col width="74%">
                                   <col width="15%">
                                   <col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th class="subtitles">${_("Descripción")}</th>
                                        <th class="center subtitles" colspan="2">${_("Cantidad")}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    %for line in order.order_line:
                                        <tr class="row-values">
                                            <td id="descr">${line.name}</td>
                                            <td class="qty">${line.product_qty}</td>
                                            <td>${line.product_uom.name}</td>
                                        </tr>
                                    %endfor
                                </tbody>
                            </table>
                            <!-- END BUDGET REQUEST LINES -->
                        </td>
                    </tr>
                    %if order.notes:
                        <tr><td id="notes-header" class="bold" colspan="2">${_("Notas")}</td></tr>
                        <tr><td id="notes-body" colspan="2">${order.notes.replace('\n','<br/>')}</td></tr>
                    %endif
                </tbody>
            </table>
            <!-- END REPORT BODY -->
            %if order != objects[-1]:
                <p style="page-break-after:always"></p>
            %endif
        %endfor
    </body>
</html>