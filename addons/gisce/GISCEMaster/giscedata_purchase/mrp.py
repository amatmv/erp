from osv import osv


class mrp_procurement(osv.osv):

    _name = "mrp.procurement"
    _inherit = "mrp.procurement"

    def check_buy(self, cursor, uid, ids):
        for procurement in self.browse(cursor, uid, ids):
            if procurement.product_id.product_tmpl_id.supply_method != 'buy':
                return False
        return True


mrp_procurement()
