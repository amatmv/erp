# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from osv.expression import OOQuery
from datetime import date, timedelta
from tools.translate import _
from tools import config


class product_template(osv.osv):
    _name = 'product.template'
    _inherit = 'product.template'

    def get_cost_methods(self, cursor, uid, context=None):
        """
        Obtains the available cost methods.
        :param cursor:
        :param uid:
        :type uid: long
        :param context: OpenERP context.
        :type context: dict
        :return: Two new cost methods: last purchase and historic average.
        :rtype: [()]
        """
        if context is None:
            context = {}
        methods = super(product_template, self).get_cost_methods(
            cursor, uid, context)
        methods += [
            ('last_purchase', _('Última compra')),
            ('historic_average', _('Precio medio según histórico')),
        ]
        return methods

    def on_change_cost_method(self, cursor, uid, ids, cost_method, last_purchase_price, historic_average_price, context=None):
        """
        Updates visually the standard price in function of the cost method.
        :param cursor:
        :param uid:
        :type uid: long
        :param ids: <product.template> ids.
        :type ids: [long]
        :param cost_method: product's cost method
        :type cost_method: str
        :param last_purchase_price: product's last purchase price
        :type last_purchase_price: float
        :param historic_average_price: product's historic average price
        :type historic_average_price: float
        :param context: OpenERP context.
        :return: The OpenERP structure that is returned by the onchange
        functions.
        :rtype: {str: {}}
        """
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not ids:
            return res

        # TODO when #9031 or #9029 is merged, then change the onchange method
        # to use reads instead of the client values.
        if cost_method == 'last_purchase':
            res['value']['standard_price'] = last_purchase_price

        elif cost_method == 'standard':
            product_tmpl_v = self.read(
                cursor, uid, ids, ['standard_user_price'], context=context
            )
            standard_price = product_tmpl_v['standard_user_price']
            res['value'] = {'standard_price': standard_price}

        elif cost_method == 'historic_average':
            res['value']['standard_price'] = historic_average_price

        return res

    def get_average_price(self, cursor, uid, ids, field_name, arg=None, context=None):
        """
        Obtains the average price based on the purchase order lines of the
        product. The stock_average_price_cost_method_range determines which
        purchase orders are going to be considered.
        :param cursor:
        :param uid:
        :type uid: long
        :param ids: <product.template> ids.
        :param field_name: Field name that triggered the function
        :type field_name: str
        :param arg: ?
        :type arg: ?
        :return: Dictionary where the key is the product template id and the
        value is the average price.
        :rtype: {str: float}
        """
        purchase_line_o = self.pool.get('purchase.order.line')
        product_o = self.pool.get('product.product')
        confvar_o = self.pool.get('res.config')

        product_id_map = {}

        for product_tmpl_id in ids:
            dmn = [('product_tmpl_id', '=', product_tmpl_id)]
            product_id = product_o.search(cursor, uid, dmn, context=context)
            if len(product_id) > 1:
                raise NotImplementedError
            if product_id:
                product_id_map[product_id[0]] = product_tmpl_id

        dmn = [
            ('product_id', 'in', product_id_map.keys()),
            ('order_id.state', '=', 'done')
        ]

        offset_days = confvar_o.get(
            cursor, uid, 'stock_average_price_cost_method_range', "0"
        )
        offset_days = int(offset_days)
        if offset_days > 0:
            minimum_date = date.today() - timedelta(days=offset_days)
            minimum_date = minimum_date.strftime('%Y-%m-%d')
            dmn.append(('order_id.date_order', '>=', minimum_date))

        purchase_line_ids = purchase_line_o.search(
            cursor, uid, dmn, context=context
        )
        purchase_line_f = ['price_subtotal', 'product_id', 'product_qty']
        purchase_line_vs = purchase_line_o.read(
            cursor, uid, purchase_line_ids, purchase_line_f, context=context
        )

        res = {}
        for purchase_line_v in purchase_line_vs:
            product_id = purchase_line_v['product_id'][0]
            product_tmpl_id = product_id_map[product_id]
            price_subtotal = purchase_line_v['price_subtotal']
            product_qty = purchase_line_v['product_qty']

            line_average = price_subtotal / product_qty

            if product_tmpl_id in res:
                res[product_tmpl_id].append(line_average)
            else:
                res[product_tmpl_id] = [line_average]

        for product_tmpl_id in res:
            averages = res[product_tmpl_id]
            res[product_tmpl_id] = sum(averages)/len(averages)

        product_tmpl_f = ['standard_price', 'cost_method']
        for product_tmpl_id in ids:
            product_tmpl_v = self.read(
                cursor, uid, product_tmpl_id, product_tmpl_f, context=context
            )

            if res.get(product_tmpl_id, 0) == 0:
                standard_price = product_tmpl_v['standard_price']
                res[product_tmpl_id] = standard_price

            if product_tmpl_v['cost_method'] == 'historic_average':
                product_template_wv = {
                    'standard_price': res[product_tmpl_id]
                }
                self.write(
                    cursor, uid, product_tmpl_id, product_template_wv,
                    context=context
                )

        return res

    def get_purchase_price(self, cursor, uid, ids, field_name, arg=None, context=None):
        """
        Obtains the product price in function of the last purchase line which
        purchase state is done or approved.
        :param cursor:
        :param uid:
        :type uid: long
        :param ids: <product.template> ids.
        :type ids: [long]
        :param field_name: Field name that triggered the function
        :type field_name: str
        :param arg:
        :param context:
        :return: Dictionary where the key is the product template id and the
        value is the last purchase price.
        :rtype: {str: float}
        """
        query = """
            SELECT pol.price_unit AS preu_compra, pt.cost_method AS metode_calcul_preu
            FROM purchase_order_line AS pol
            INNER JOIN product_product p ON pol.product_id = p.id
            INNER JOIN product_template pt ON p.product_tmpl_id = pt.id
            INNER JOIN purchase_order po ON pol.order_id = po.id
            WHERE pt.id = %s AND po.state in ('done', 'approved')
            ORDER BY po.date_approve DESC LIMIT 1;
        """
        vals = {}

        product_tmpl_vs = self.read(
            cursor, uid, ids, ['standard_user_price'], context=context
        )
        for product_tmpl_v in product_tmpl_vs:
            product_tmpl_id = product_tmpl_v['id']
            default_last_purchase_price = product_tmpl_v['standard_user_price']
            vals[product_tmpl_id] = default_last_purchase_price

        for product_template_id in ids:
            cursor.execute(query, (product_template_id,))
            product_template_v = cursor.dictfetchall()
            if product_template_v:
                product_template_v = product_template_v[0]
                vals.update({product_template_id: product_template_v['preu_compra']})
                if product_template_v['metode_calcul_preu'] == 'last_purchase':
                    product_template_wv = {
                        'standard_price': product_template_v['preu_compra']
                    }
                    self.write(
                        cursor, uid, product_template_id, product_template_wv,
                        context=context
                    )

        return vals

    def get_standard_user_price(self, cursor, uid, ids, field_name, arg=None, context=None):
        product_tmpl_f = ['standard_price']
        product_tmpl_vs = self.read(cursor, uid, ids, product_tmpl_f, context=context)
        res = {}
        for product_tmpl_v in product_tmpl_vs:
            product_tmpl_id = product_tmpl_v['id']
            standard_user_price = product_tmpl_v['standard_price']
            res[product_tmpl_id] = standard_user_price

        return res

    def _get_product_ids_to_update_function_prices_when_product_purchased(self, cursor, uid, ids, context=None):
        """
        Obtains the products which its last purchase price or historic average
        price needs to be updated.
        :param cursor:
        :param uid:
        :param uid: long
        :param ids: <purchase.order> ids
        :type ids: [long]
        :param context: OpenERP context
        :type context: dict
        :return: <product.template> ids
        :rtype: [long]
        """
        dmn = [('id', 'in', ids), ('state', 'in', ['done', 'approved'])]
        q = self.q(cursor, uid).select(['id']).where(dmn)
        cursor.execute(*q)
        ids = [purchase_order_v['id'] for purchase_order_v in cursor.dictfetchall()]

        product_template_ids = []

        if ids:
            purchase_order_line_o = self.pool.get('purchase.order.line')
            product_template_id = 'product_id.product_tmpl_id.id'
            cols = [product_template_id]
            dmn = [('order_id', 'in', ids)]
            q = OOQuery(purchase_order_line_o, cursor, uid).select(cols).where(dmn)
            cursor.execute(*q)
            q_res = cursor.dictfetchall()
            product_template_ids = [v[product_template_id] for v in q_res]
        return product_template_ids

    def _store_last_historic_average_price_product_price_changed(self, cursor, uid, ids, context=None):
        """
        Obtains the products which its last purchase price or historic average
        price needs to be updated.
        :param cursor:
        :param uid:
        :param uid: long
        :param ids: <product.template> ids
        :param context: OpenERP context
        :type context: dict
        :return: <product.template> ids
        :rtype: [long]
        """
        return ids

    def _store_last_historic_average_price_varconf_range_changed(self, cursor, uid, ids, context=None):
        """
        Obtains the products which its last purchase price or historic average
        price needs to be updated. It'll return all the products.
        :param cursor:
        :param uid: <res.users> id
        :param uid: long
        :param ids: <res.config> ids
        :param context: OpenERP context
        :type context: dict
        :return: <product.template> ids
        :rtype: [long]
        """

        dmn = [('name', '=', 'stock_average_price_cost_method_range')]
        confvar_id = self.search(cursor, uid, dmn, context=context)

        res = []
        if len(confvar_id) == 1:
            if confvar_id[0] in ids:
                product_tmpl_o = self.pool.get('product.template')
                res = product_tmpl_o.search(cursor, uid, [], context=context)
        else:
            raise NotImplementedError

        return res

    def _store_standard_user_price_standard_price_changed(self, cursor, uid, ids, context=None):
        dmn = [('id', 'in', ids), ('cost_method', '=', 'standard')]
        return self.search(cursor, uid, dmn, context=context)

    _STORE_LAST_PURCHASE_PRICE = {
        'purchase.order': (
            _get_product_ids_to_update_function_prices_when_product_purchased, ['date_approve'], 20
        ),
        'product.template': (
            _store_standard_user_price_standard_price_changed, ['standard_price'], 20
        )
    }

    _STORE_HISTORIC_AVERAGE_PRICE = {
        'purchase.order': (
            _get_product_ids_to_update_function_prices_when_product_purchased, ['state'], 10
        ),
        'product.template': (
            _store_last_historic_average_price_product_price_changed, ['standard_user_price'], 10
        ),
        'res.config':  (
            _store_last_historic_average_price_varconf_range_changed, ['value'], 10
        )
    }

    _STORE_STANDARD_USER_PRICE = {
        'product.template': (
            _store_standard_user_price_standard_price_changed, ['standard_price'], 10
        )
    }

    _columns = {
        'cost_method': fields.selection(
            get_cost_methods, 'Método de coste',
            required=True
        ),
        'standard_user_price': fields.function(
            get_standard_user_price, store=_STORE_STANDARD_USER_PRICE,
            digits=(16, int(config['price_accuracy'])),
            method=True, type='float', string='Precio definido por el usuario'
        ),
        'last_purchase_price': fields.function(
            get_purchase_price, store=_STORE_LAST_PURCHASE_PRICE,
            method=True, type='float', size=100, string='Precio de compra',
            digits=(16, int(config['price_accuracy'])),
        ),
        'historic_average_price': fields.function(
            get_average_price, store=_STORE_HISTORIC_AVERAGE_PRICE,
            method=True, type='float', size=100,
            string='Precio medio según histórico',
            digits=(16, int(config['price_accuracy'])),
        )
    }


product_template()


class product_product(osv.osv):
    _name = 'product.product'
    _inherit = 'product.product'

    def on_change_cost_method(self, cursor, uid, ids, product_tmpl_id, cost_method, last_purchase_price, historic_average_price, context=None):
        """
        Updates visually the standard price in function of the cost method.
        :param cursor:
        :param uid:
        :type uid: long
        :param ids: <product.product> ids.
        :type ids: [long]
        :param cost_method: product's cost method
        :type cost_method: str
        :param last_purchase_price: product's last purchase price
        :type last_purchase_price: float
        :param historic_average_price: product's historic average price
        :type historic_average_price: float
        :param context: OpenERP context.
        :return: The OpenERP structure that is returned by the onchange
        functions.
        :rtype: {str: {}}
        """
        product_template_o = self.pool.get('product.template')
        return product_template_o.on_change_cost_method(
            cursor, uid, product_tmpl_id, cost_method, last_purchase_price,
            historic_average_price, context=context
        )


product_product()
