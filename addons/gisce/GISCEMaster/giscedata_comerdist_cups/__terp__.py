# -*- coding: utf-8 -*-
{
    "name": "Comerdist (cups)",
    "description": """
    This module provide :
      * Sincronització de CUPS.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
