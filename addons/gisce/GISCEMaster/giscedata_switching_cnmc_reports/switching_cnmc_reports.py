# -*- coding: utf-8 -*-
from __future__ import absolute_import

from datetime import datetime
from osv import osv
from tools.translate import _
import StringIO
import csv

from .xml.comer_changes_xml import ComerChangesXml
from .xml.supply_points_xml import SupplyPointsXml
from .xml.defs import CNMC_TARIFA_ATR_VALUES, UNKNOWN_RATE

from giscedata_polissa.agree import agree_tipus


def get_agent_type(whereiam):
    """
    Returns the letter wich identifies if we are in distri or in comer
    @param whereiam:
    @return:
    """
    if whereiam == 'comer':
        return 'C'
    return 'D'


def get_cnmc_tarifa_atr(tarifa_atr):
    """
    Returns the CNMC code for a given ATR rate or a default value if
    we cannot find it
    @param tarifa_atr:
    @return:
    """
    try:
        return CNMC_TARIFA_ATR_VALUES[tarifa_atr]
    except KeyError:
        return CNMC_TARIFA_ATR_VALUES[UNKNOWN_RATE]


def get_cnmc_state_code(town):
    """
    This function returns the state code, if any, filled with 0's to the right
    @param town:
    @return:
    """
    if town and town.state:
        return town.state.code.ljust(5, '0')
    return '00000'  # No state found.


def get_cnmc_change_type(proces_code):
    """
    C3 are changes (process code of type C1 or C2) and C4
    are really new enters.
    @param proces_code:
    @return: string C3 or C4
    """
    if proces_code == 'A3':
        return 'C4'
    return 'C3'


def get_cnmc_point_type(point_type):
    """
    Returns the point type parsed in format cnmc: a int instead of a string
    @param point_type:
    @return:
    """
    return int(point_type)


def get_r1_code(partner):
    """
    :param partner: browse of a partner
    :return: ref2 field of the partner or of its parent
    """
    res = None
    if partner.ref2:
        res = partner.ref2
    elif partner.parent_id:
        res = get_r1_code(partner.parent_id)
    return res


def get_ingoing_code(switch):
    """
    Returns the ingoing comer code depending on the erp instance is running
    @param switch:
    @return:
    """
    if switch.whereiam == 'distri':
        return get_r1_code(switch.partner_id)
    return get_r1_code(switch.company_id)


def get_distri(pool, cursor, uid, switch):
    """
    :param switch:
    :return: distri of the atr case taken from step 01.
    If no step 01 it's taken from the CUPS.
    """
    model_01 = "giscedata.switching.{0}.01".format(switch.proces_id.name.lower())
    pas01_obj = pool.get(model_01)
    pas01_id = pas01_obj.search(cursor, uid, [('sw_id', '=', switch.id)])
    if pas01_id:
        pas01 = pas01_obj.browse(cursor, uid, pas01_id[0])
        return pas01.receptor_id
    return switch.cups_id.distribuidora_id


class SwitchingCnmcReport(osv.osv):
    _name = 'switching_cnmc_report'
    _auto = False

    date_format = '%d/%m/%Y'
    agent_type = None
    agent_code = None
    period = None

    def set_date_format_from_config(self, cursor, uid, context=None):
        """
        This methods sets the date format of the reports depending on
        the configuration of the erp.
        @param cursor:
        @param uid:
        @param context:
        @return:
        """
        str_lang = context.get('lang', 'es_ES')
        lang_obj = self.pool.get('res.lang')
        try:
            self.date_format = lang_obj.search_reader(
                cursor, uid, [('code', '=', str_lang)], ['date_format']
            )[0]['date_format']
        except KeyError:
            self.date_format = '%d/%m/%Y'

    def format_date(self, cursor, uid, date, context=None):
        return datetime.strftime(
            datetime.strptime(date, '%Y-%m-%d'), self.date_format
        )

    def set_period(self, cursor, uid, start_date, context=None):
        """
        This method sets the period in format YYYYMM which is mandatory
        to inform in the reports.
        @param cursor:
        @param uid:
        @param start_date:
        @param context:
        @return:
        """
        start_date_time = datetime.strptime(start_date, "%Y-%m-%d")
        self.period = "{}{}".format(
            start_date_time.year,
            str(start_date_time.month).zfill(2)  # leading 0 to months
        )

    def get_step_n(self, cursor, uid, sw, step_name, context=None):
        """
        Returns step nn object
        :param cursor:
        :param uid:
        :param sw: case object
        :param step_name: step name (nn)
        :param context:
        :return: step object (giscedata.switching.pp.ss)
        """
        # switch = process. Find step 1
        step_obj = self.pool.get(
            "giscedata.switching.{0}.{1}".format(
                sw.proces_id.name.lower(), step_name
            )
        )
        step_id = step_obj.search(
            cursor, uid, [('sw_id', '=', sw.id)]
        )[0]
        step = step_obj.browse(cursor, uid, step_id, context=context)
        return step

    def get_tariff_code(self, cursor, uid, obj, context=None):
        """
        Returns tariff code from step. It gets related contract tariff if
        available or step tariff otherwise. I may happens on A3 case not
        accepted in DSO
        :param cursor:
        :param uid:
        :param obj: switching obj (giscedata.switching or
            giscedata.switching.nn.nn step)
        :return: tariff CNMC code
        """
        if obj._name == 'giscedata.switching':
            sw = obj
            sw_step = None
        else:
            sw = obj.sw_id
            sw_step = obj
        tariff_name = ""
        try:
            tariff_name = sw.cups_polissa_id.tarifa.name
        except AttributeError, e:
            # Gets step 01
            if sw_step is None:
                sw_step = self.get_step_n(
                    cursor, uid, sw, '01', context=context
                )
            tariff_obj = self.pool.get('giscedata.polissa.tarifa')

            tariff_id = tariff_obj.get_tarifa_from_ocsum(
                cursor, uid, sw_step.tarifaATR, context
            )
            if tariff_id:
                tariff_name = tariff_obj.read(
                    cursor, uid, tariff_id, ['name'], context=context
                )['name']

        return tariff_name and get_cnmc_tarifa_atr(tariff_name) or ''

    def get_point_type(self, cursor, uid, obj, context=None):
        """
        Returns agree_tipo from step. It gets related contract agree_tipo field
        if available or step power otherwise. I may happens on A3 case not
        accepted in DSO
        :param cursor:
        :param uid:
        :param sw_step: switching step
        :param context:
        :return: point type CNMC code
        """
        if obj._name == 'giscedata.switching':
            sw = obj
            sw_step = None
        else:
            sw = obj.sw_id
            sw_step = obj
        agree_type = ""
        try:
            agree_type = sw.cups_polissa_id.agree_tipus
            if not agree_type:
                raise AttributeError()
        except AttributeError, e:
            # Gets step 01
            if sw_step is None:
                sw_step = self.get_step_n(
                    cursor, uid, sw, '01', context=context
                )
            pot = 0
            for p in sw_step.pot_ids:
                pot = max(p.potencia, pot)

            agree_type = agree_tipus(pot / 1000.0)

        return agree_type and get_cnmc_point_type(agree_type)

SwitchingCnmcReport()


class SwitchingComerChangesReport(osv.osv):
    _name = 'comer.changes.cnmc.report'
    _auto = False
    _inherit = 'switching_cnmc_report'

    def get_xml_name(self, cursor, uid, context=None):
        return "SI_{}_E_{}_01.xml".format(self.agent_code, self.period)

    def get_header_info(self, cursor, uid, context=None):
        return {
            'agent_code': self.agent_code,
            'market_type': 'E',  # Energy market always
            'agent_type': self.agent_type,
            'period': self.period
        }

    def create_report(self, cursor, uid, start_date, end_date, context=None):
        """
        Generates the comer changes xml file happened in a period of time
        @param cursor:
        @param uid:
        @param start_date: start of the period
        @param end_date: end of the period
        @param context:
        @return: a dict with two keys: xml contains the xml content in string
        format; is_valid is a boolean to know if the xml validates against
        its xsd validator.
        """
        switching_obj = self.pool.get('giscedata.switching')
        self.set_date_format_from_config(cursor, uid, context)

        models = ["giscedata.switching.c1.01", "giscedata.switching.c2.01",
                  "giscedata.switching.a3.01"]

        search_filter = [
            ('date_created', '>=', start_date),
            ('date_created', '<=', end_date)
        ]

        switching_ids = []
        for model in models:
            model_obj = self.pool.get(model)
            step_ids = model_obj.search(cursor, uid, search_filter)
            steps_info = model_obj.read(cursor, uid, step_ids, ['sw_id'])
            aux_ids = [x['sw_id'][0] for x in steps_info if x['sw_id'][0] not in switching_ids]
            switching_ids = switching_ids + aux_ids

        switches = switching_obj.browse(cursor, uid, switching_ids)

        switching_info = {}
        self.set_period(cursor, uid, start_date)

        for switch in switches:
            self.agent_code = get_r1_code(switch.company_id)
            self.agent_type = get_agent_type(switch.whereiam)

            state_code = get_cnmc_state_code(switch.cups_id.id_municipi)
            if not switch.cups_id and switch.whereiam == 'distri':
                distri_code = self.agent_code
            else:
                distri_code = get_r1_code(get_distri(self.pool, cursor, uid, switch))
            if not distri_code:
                raise osv.except_osv(
                    _('Error'),
                    _(
                        u"The XML file has not been generated because the r1 "
                        u"code (ref2 field) is not filled in "
                        u"partner {}".format(
                            get_distri(self.pool, cursor, uid, switch).name
                        )
                    )
                )
            ingoing_code = get_ingoing_code(switch)
            outgoing_code = (
                switch.proces_id.name != 'A3' and
                switch.comer_sortint_id and
                get_r1_code(switch.comer_sortint_id) or
                '0'
            )
            access_rate = self.get_tariff_code(
                cursor, uid, switch, context=context
            )
            change_type = get_cnmc_change_type(
                switch.proces_id.name
            )
            # switch = process
            point_type = self.get_point_type(
                cursor, uid, switch, context=context
            )

            switching_record = (
                state_code, distri_code, ingoing_code, outgoing_code,
                change_type, point_type, access_rate,
            )

            switching_info.setdefault(
                switching_record, {
                    'total': 0, 'total_rejected': 0, 'total_canceled': 0,
                    'accepted': [], 'rejected': [], 'activated': [],
                    'pending': [], 'activated_pending': []
                }
            )
            switching_info[switching_record]['total'] += 1

            if switch.rebuig:  # Rejected switching
                switching_info[switching_record]['total_rejected'] += 1

            canceled = self.is_switching_canceled(
                cursor, uid, switch, start_date, end_date
            )
            if canceled:
                switching_info[switching_record]['total_canceled'] += 1

        pending_switchings = self.get_pending_switchings(
            cursor, uid, start_date, end_date
        )
        accepted_rejected_switchings = self.get_switchings_by_state(
            cursor, uid, start_date, end_date
        )
        activated_pending_switchings = self.get_activated_pending_switchings(
            cursor, uid, start_date, end_date
        )
        activated_switchings = self.get_activated_switchings(
            cursor, uid, start_date, end_date
        )

        switching_info = self.update_switchings(
            cursor, uid, switching_info,
            pending_switchings, 'pending'
        )
        switching_info = self.update_switchings(
            cursor, uid, switching_info,
            accepted_rejected_switchings['rejected'], 'rejected'
        )
        switching_info = self.update_switchings(
            cursor, uid, switching_info,
            accepted_rejected_switchings['accepted'], 'accepted'
        )
        switching_info = self.update_switchings(
            cursor, uid, switching_info,
            activated_pending_switchings, 'activated_pending'
        )
        switching_info = self.update_switchings(
            cursor, uid, switching_info,
            activated_switchings, 'activated'
        )

        if len(switching_info) == 0:
            raise osv.except_osv(
                _('Error'),
                _(
                    u"The XML file has not been generated because there are no "
                    u"changes, rejections, acceptations or activations between "
                    u"{} and {}. You don't need to upload any XML file, it is "
                    u"enough to send an email to the CNMC explaining this "
                    u"situation. Remember to inform the agent code and the "
                    u"company's complete name."
                ).format(
                    self.format_date(cursor, uid, start_date, context),
                    self.format_date(cursor, uid, end_date, context)
                )
            )

        xml_changes_file = ComerChangesXml()
        xml_changes_file.header.feed(self.get_header_info(cursor, uid))
        for key, switch_group in switching_info.iteritems():
            xml_changes_file.add_request_done(
                key, switch_group
            )
        xml_changes_file.generate_xml()
        csv_report = self.generate_csv_report(cursor, uid, switching_info, context)
        return {
            "xml": str(xml_changes_file), "is_valid": xml_changes_file.is_valid,
            "csv": csv_report
        }

    def update_switchings(self, cursor, uid, switchings, new_switchings,
                          type_update, context=None):
        """
        Updates the main dict with the info changes with the related childs
        of accepted, rejected or activated switches
        @param cursor:
        @param uid:
        @param switchings: original dict
        @param new_switchings: possible new switches to update or create
        @param type_update: the kind of the new switchings we are trying to add
        @param context:
        @return: the dict updated
        """
        for key, new_switch_detail in new_switchings.iteritems():
            if key not in switchings.keys():
                # We create a new instance of switch:
                switchings[key] = {
                    'total': 0, 'total_rejected': 0, 'total_canceled': 0,
                    'accepted': [], 'rejected': [], 'activated': [],
                    'pending': [], 'activated_pending': []
                }
            switchings[key][type_update] = new_switch_detail

        return switchings

    def is_switching_canceled(self, cursor, uid, switch, start_date, end_date,
                              context=None):
        """
        Returns true if the given switching is canceled.
        @param cursor:
        @param uid:
        @param switch: switching instance
        @param start_date: string date in Big Endian format Y-M-D
        @param end_date: string date in Big Endian format Y-M-D
        @param context:
        @return: boolean
        """
        canceled_models = {
            'c1': 'giscedata.switching.c1.09',
            'c2': 'giscedata.switching.c2.09',
            'a3': 'giscedata.switching.a3.07',
        }

        canceled_model = canceled_models[switch.proces_id.name.lower()]
        canceled_switching = self.pool.get(canceled_model)
        search_filter = [
            ('header_id.sw_id', '=', switch.id),
            ('rebuig', '=', True),
            ('date_created', '>=', start_date),
            ('date_created', '<=', end_date)
        ]
        canceled_ids = canceled_switching.search(cursor, uid, search_filter)
        return canceled_ids and len(canceled_ids) > 0

    def get_switchings_by_state(self, cursor, uid, start_date, end_date):
        """
        Returns the accepted and rejected switchings detailed info.
        @param cursor:
        @param uid:
        @param start_date:  date as string in big endian format
        @param end_date: date as string in big endian format
        @return: dictionary
        """
        models = [
            ('giscedata.switching.c1.02', 'giscedata.switching.c1.04'),
            ('giscedata.switching.c2.02', 'giscedata.switching.c2.04'),
            ('giscedata.switching.a3.02', 'giscedata.switching.a3.04')
        ]
        search_filter = [
            ('date_created', '>=', start_date),
            ('date_created', '<=', end_date),
            ('rebuig', '=', False)
        ]
        search_filter_rej = [
            ('date_created', '>=', start_date),
            ('date_created', '<=', end_date),
            ('rebuig', '=', True)
        ]

        main_accepted = {}
        main_rejected = {}

        for model_name_02, model_name_04 in models:
            switch_02_obj = self.pool.get(model_name_02)
            switch_04_obj = self.pool.get(model_name_04)

            # Get rejected sw for step 04
            switch_rej04_ids = switch_04_obj.search(cursor, uid, search_filter_rej)
            sw_ids_rej04 = switch_04_obj.read(cursor, uid, switch_rej04_ids, ['sw_id'])
            sw_ids_rej04 = [aux['sw_id'][0] for aux in sw_ids_rej04]
            # Get rejected sw for step 02
            switch_rej02_ids = switch_02_obj.search(cursor, uid, search_filter_rej)
            sw_ids_rej02 = switch_02_obj.read(cursor, uid, switch_rej02_ids, ['sw_id'])
            sw_ids_rej02 = [aux['sw_id'][0] for aux in sw_ids_rej02]
            # All rejected sw between dates
            sw_ids_rej = sw_ids_rej02 + sw_ids_rej04

            # Get all accepted sw for step 04 that don't have been rejected (not in sw_ids_rej)
            search_filter_acc = search_filter + [('header_id.sw_id', 'not in', sw_ids_rej)]
            switching_ids = switch_02_obj.search(cursor, uid, search_filter_acc)

            switches02 = switch_02_obj.browse(cursor, uid, switching_ids + switch_rej02_ids)
            switches04 = switch_04_obj.browse(cursor, uid, switch_rej04_ids)
            for switch in switches02 + switches04:
                delay_type = "00"  # For now it always will be in time (00)

                self.agent_code = get_r1_code(switch.sw_id.company_id)
                self.agent_type = get_agent_type(switch.sw_id.whereiam)

                state_code = get_cnmc_state_code(switch.sw_id.cups_id.id_municipi)
                distri_code = get_r1_code(get_distri(self.pool, cursor, uid, switch.sw_id))
                if not distri_code:
                    raise osv.except_osv(
                        _('Error'),
                        _(
                            u"The XML file has not been generated because the r1 "
                            u"code (ref2 field) is not filled in "
                            u"partner {}".format(
                                get_distri(self.pool, cursor, uid, switch.sw_id).name
                            )
                        )
                    )
                ingoing_code = get_ingoing_code(switch.sw_id)
                outgoing_code = (
                    switch.sw_id.proces_id.name != 'A3' and
                    switch.sw_id.comer_sortint_id and
                    get_r1_code(switch.sw_id.comer_sortint_id) or
                    '0'
                )

                # switch = step
                access_rate = self.get_tariff_code(cursor, uid, switch.sw_id)
                point_type = self.get_point_type(cursor, uid, switch)

                change_type = get_cnmc_change_type(
                    switch.sw_id.proces_id.name
                )
                main_key = (
                    state_code, distri_code, ingoing_code, outgoing_code,
                    change_type, point_type, access_rate,
                )

                if switch.rebuig:
                    if not len(switch.rebuig_ids):
                        raise osv.except_osv(
                            _('Error'),
                            _(
                                u"The XML file has not been generated because "
                                u"the {0}-{1} ({2}) is a reject step but it "
                                u"doesn't have any reject code".format(
                                    switch.sw_id.proces_id.name,
                                    switch._nom_pas,
                                    switch.sw_id.codi_sollicitud
                                )
                            )
                        )
                    reason_id = switch.rebuig_ids[0]
                    # The key is composed by te reason and the delay:
                    key = (delay_type, reason_id.motiu_rebuig.name)

                    main_rejected.setdefault(main_key, {})
                    main_rejected[main_key].setdefault(key,
                                                       {'total': 0, 'days': 0})
                    main_rejected[main_key][key]['total'] += 1
                    datetime_start = datetime.strptime(
                        switch.sw_id.data_sollicitud, '%Y-%m-%d'
                    )
                    # Es fa el split per evitar agafar els microsegons en cas
                    # que ens arribin
                    datetime_stop = datetime.strptime(
                        switch.date_created.split(".")[0], '%Y-%m-%d %H:%M:%S'
                    )
                    main_rejected[main_key][key]['days'] += (
                        datetime_stop - datetime_start
                    ).days

                    # Store data used for the csv report
                    if not main_rejected[main_key][key].get('csv_data'):
                        main_rejected[main_key][key]['csv_data'] = []

                    csv_sw_info = {
                        'cups': switch.sw_id.cups_input,
                        'codi_sollicitud': switch.sw_id.codi_sollicitud,
                        'process': switch.sw_id.proces_id.name,
                        'step': switch._nom_pas,
                        'creation_date': switch.date_created,
                        'rejected': self.is_switching_rejected(
                            cursor, uid, switch.sw_id.id,
                            switch.sw_id.proces_id.name,
                            start_date, end_date
                        ),
                    }
                    main_rejected[main_key][key]['csv_data'].append(csv_sw_info)
                else:
                    main_accepted.setdefault(main_key, {})
                    main_accepted[main_key].setdefault(delay_type,
                                                       {'total': 0, 'days': 0})

                    main_accepted[main_key][delay_type]['total'] += 1
                    datetime_start = datetime.strptime(
                        switch.sw_id.data_sollicitud, '%Y-%m-%d'
                    )
                    # Es fa el split per evitar agafar els microsegons en cas
                    # que ens arribin
                    datetime_stop = datetime.strptime(
                        switch.date_created.split(".")[0], '%Y-%m-%d %H:%M:%S'
                    )
                    main_accepted[main_key][delay_type]['days'] += (
                        datetime_stop - datetime_start
                    ).days

                    # Store data used for the csv report
                    if not main_accepted[main_key][delay_type].get('csv_data'):
                        main_accepted[main_key][delay_type]['csv_data'] = []

                    csv_sw_info = {
                        'cups': switch.sw_id.cups_input,
                        'codi_sollicitud': switch.sw_id.codi_sollicitud,
                        'process': switch.sw_id.proces_id.name,
                        'step': switch._nom_pas,
                        'creation_date': switch.date_created,
                        'rejected': self.is_switching_rejected(
                            cursor, uid, switch.sw_id.id,
                            switch.sw_id.proces_id.name,
                            start_date, end_date
                        ),
                    }
                    main_accepted[main_key][delay_type]['csv_data'].append(csv_sw_info)

        switchings = {'accepted': {}, 'rejected': {}}
        for main_key, rejected in main_rejected.iteritems():
            switchings['rejected'].setdefault(main_key, [])
            for key, values in rejected.iteritems():
                switchings['rejected'][main_key].append(
                    {'delay_type': key[0], 'reason': key[1],
                     'total': values['total'],
                     # Only positive numbers
                     'avg_time': max(int(values['days'] / values['total']), 0),
                     'csv_data': values['csv_data']
                     }
                )
        for main_key, accepted in main_accepted.iteritems():
            switchings['accepted'].setdefault(main_key, [])
            for key, values in accepted.iteritems():
                switchings['accepted'][main_key].append(
                    {'delay_type': key, 'total': values['total'],
                     # Only positive numbers
                     'avg_time': max(int(values['days'] / values['total']), 0),
                     'csv_data': values['csv_data']
                     }
                )
        return switchings

    def get_activated_switchings(self, cursor, uid, start_date, end_date):
        """
        Return info about the activated switchings between start and end date
        @param cursor:
        @param uid:
        @param start_date: string date in big endian format
        @param end_date: string date in big endian format
        @return:
        """
        models = [
            'giscedata.switching.c1.05',
            'giscedata.switching.c2.05',
            'giscedata.switching.a3.05'
        ]
        search_filter = [
            ('data_activacio', '>=', start_date),
            ('data_activacio', '<=', end_date)
        ]
        main_activated = {}

        for model_name in models:
            switch_05_obj = self.pool.get(model_name)
            switch_02_obj = self.pool.get(model_name.replace('05', '02'))

            switch_05_ids = switch_05_obj.search(cursor, uid, search_filter)

            switches_05 = switch_05_obj.browse(cursor, uid, switch_05_ids)
            for switch_05 in switches_05:
                self.agent_code = get_r1_code(switch_05.sw_id.company_id)
                self.agent_type = get_agent_type(switch_05.sw_id.whereiam)

                state_code = get_cnmc_state_code(
                    switch_05.sw_id.cups_id.id_municipi)
                distri_code = get_r1_code(get_distri(self.pool, cursor, uid, switch_05.sw_id))
                if not distri_code:
                    raise osv.except_osv(
                        _('Error'),
                        _(
                            u"The XML file has not been generated because the r1 "
                            u"code (ref2 field) is not filled in "
                            u"partner {}".format(
                                get_distri(self.pool, cursor, uid, switch_05.sw_id).name
                            )
                        )
                    )
                ingoing_code = get_ingoing_code(switch_05.sw_id)
                outgoing_code = (
                    switch_05.sw_id.proces_id.name != 'A3' and
                    switch_05.sw_id.comer_sortint_id and
                    get_r1_code(switch_05.sw_id.comer_sortint_id) or
                    '0'
                )
                # switch = step
                access_rate = self.get_tariff_code(cursor, uid, switch_05.sw_id)

                change_type = get_cnmc_change_type(
                    switch_05.sw_id.proces_id.name
                )
                point_type = self.get_point_type(cursor, uid, switch_05)
                main_key = (
                    state_code, distri_code, ingoing_code, outgoing_code,
                    change_type, point_type, access_rate,
                )
                main_activated.setdefault(main_key, {})

                delay_type = "00"  # For now it always will be in time (00)

                main_activated[main_key].setdefault(
                    delay_type, {'total': 0, 'days': 0}
                )
                main_activated[main_key][delay_type]['total'] += 1

                # acceptation date needs to be taken from table 02:
                switch_02_ids = switch_02_obj.search(
                    cursor, uid, [('sw_id', '=', switch_05.sw_id.id)]
                )
                if not switch_02_ids:
                    # Some cases doesn't have a 02 step. Skip temporaly
                    continue
                switch_02_id = switch_02_ids[0]
                switch_02 = switch_02_obj.browse(cursor, uid, switch_02_id)
                # Es fa el split per evitar agafar els microsegons en cas
                # que ens arribin
                datetime_start = datetime.strptime(
                    switch_02.date_created.split(".")[0], '%Y-%m-%d %H:%M:%S'
                )
                datetime_stop = datetime.strptime(
                    switch_05.data_activacio, '%Y-%m-%d'
                )
                main_activated[main_key][delay_type]['days'] += (
                    datetime_stop - datetime_start
                ).days

                # Store data used for the csv report
                if not main_activated[main_key][delay_type].get('csv_data'):
                    main_activated[main_key][delay_type]['csv_data'] = []

                csv_sw_info = {
                    'cups': switch_05.sw_id.cups_input,
                    'codi_sollicitud': switch_05.sw_id.codi_sollicitud,
                    'process': switch_05.sw_id.proces_id.name,
                    'step': switch_05._nom_pas,
                    'creation_date': switch_05.date_created,
                    'rejected': self.is_switching_rejected(
                        cursor, uid, switch_05.sw_id.id,
                        switch_05.sw_id.proces_id.name,
                        start_date, end_date
                    ),
                }
                main_activated[main_key][delay_type]['csv_data'].append(csv_sw_info)

        activated_switchings = {}
        for main_key, activated in main_activated.iteritems():
            activated_switchings.setdefault(main_key, [])
            for key, totals in activated.iteritems():
                activated_switchings[main_key].append(
                    {
                        'delay_type': key, 'total': totals['total'],
                        # Only positive numbers
                        'avg_time': max(
                            int(totals['days'] / totals['total']),
                            0
                        ),
                        'csv_data': totals.get('csv_data', []),
                    }
                )

        return activated_switchings

    def get_pending_switchings(self, cursor, uid, start_date, end_date):
        """
        Return info about the pending switchings (sw without step 02 response)
        between start and end date
        @param cursor:
        @param uid:
        @param start_date: string date in big endian format
        @param end_date: string date in big endian format
        @return:
        """
        models = [
            ('giscedata.switching.c1.01', 'giscedata.switching.c1.02'),
            ('giscedata.switching.c2.01', 'giscedata.switching.c2.02'),
            ('giscedata.switching.a3.01', 'giscedata.switching.a3.02')
        ]

        search_filter = [
            ('header_id.date_created', '>=', start_date),
            ('header_id.date_created', '<=', end_date),
        ]

        main_pending = {}

        for model_name_01, model_name_02 in models:
            switch_01_obj = self.pool.get(model_name_01)
            switch_02_obj = self.pool.get(model_name_02)

            switch_02_ids = switch_02_obj.search(cursor, uid, search_filter)
            sw_ids = switch_02_obj.read(cursor, uid, switch_02_ids, ['sw_id'])
            sw_ids = [aux['sw_id'][0] for aux in sw_ids]

            search_filter_01 = search_filter + [('header_id.sw_id', 'not in', sw_ids)]
            switch_01_ids = switch_01_obj.search(cursor, uid, search_filter_01)

            for switch_01 in switch_01_obj.browse(cursor, uid, switch_01_ids):
                self.agent_code = get_r1_code(switch_01.sw_id.company_id)
                self.agent_type = get_agent_type(switch_01.sw_id.whereiam)

                state_code = get_cnmc_state_code(switch_01.sw_id.cups_id.id_municipi)
                distri_code = get_r1_code(get_distri(self.pool, cursor, uid, switch_01.sw_id))
                if not distri_code:
                    raise osv.except_osv(
                        _('Error'),
                        _(
                            u"The XML file has not been generated because the r1 "
                            u"code (ref2 field) is not filled in "
                            u"partner {}".format(
                                get_distri(self.pool, cursor, uid, switch_01.sw_id).name
                            )
                        )
                    )
                ingoing_code = get_ingoing_code(switch_01.sw_id)
                outgoing_code = (
                    switch_01.sw_id.proces_id.name != 'A3' and
                    switch_01.sw_id.comer_sortint_id and
                    get_r1_code(switch_01.sw_id.comer_sortint_id) or
                    '0'
                )
                # switch = step
                access_rate = self.get_tariff_code(cursor, uid, switch_01.sw_id)

                change_type = get_cnmc_change_type(
                    switch_01.sw_id.proces_id.name
                )
                point_type = self.get_point_type(cursor, uid, switch_01)
                main_key = (
                    state_code, distri_code, ingoing_code, outgoing_code,
                    change_type, point_type, access_rate,
                )
                main_pending.setdefault(main_key, {})

                delay_type = "00"  # For now it always will be in time (00)

                main_pending[main_key].setdefault(
                    delay_type, {'total': 0}
                )
                main_pending[main_key][delay_type]['total'] += 1

                # Store data used for the csv report
                if not main_pending[main_key][delay_type].get('csv_data'):
                    main_pending[main_key][delay_type]['csv_data'] = []

                csv_sw_info = {
                    'cups': switch_01.sw_id.cups_input,
                    'codi_sollicitud': switch_01.sw_id.codi_sollicitud,
                    'process': switch_01.sw_id.proces_id.name,
                    'step': switch_01._nom_pas,
                    'creation_date': switch_01.date_created,
                    'rejected': self.is_switching_rejected(
                        cursor, uid, switch_01.sw_id.id,
                        switch_01.sw_id.proces_id.name,
                        start_date, end_date
                    ),
                }
                main_pending[main_key][delay_type]['csv_data'].append(csv_sw_info)

        pending_switchings = {}
        for main_key, activated in main_pending.iteritems():
            pending_switchings.setdefault(main_key, [])
            for key, totals in activated.iteritems():
                pending_switchings[main_key].append(
                    {'delay_type': key, 'total': totals['total'],
                     'csv_data': totals['csv_data']
                     }
                )

        return pending_switchings

    def is_switching_rejected(self, cursor, uid, sw_id, proces, start_date, end_date, context=None):
        model02 = "giscedata.switching.{0}.02".format(proces.lower())
        model02_obj = self.pool.get(model02)

        model04 = "giscedata.switching.{0}.04".format(proces.lower())
        model04_obj = self.pool.get(model04)

        search_filter_rej = [
            ('date_created', '>=', start_date),
            ('date_created', '<=', end_date),
            ('rebuig', '=', True),
            ('sw_id', '=', sw_id)
        ]
        # Get rejected sw for step 02
        switch_rej02_ids = model02_obj.search(cursor, uid, search_filter_rej)
        # Get rejected sw for step 04
        switch_rej04_ids = model04_obj.search(cursor, uid, search_filter_rej)

        return len(switch_rej02_ids) or len(switch_rej04_ids)

    def get_activated_pending_switchings(self, cursor, uid, start_date, end_date):
        """
        Return info about the accepted but not activated switchings between
        start and end date
        @param cursor:
        @param uid:
        @param start_date: string date in big endian format
        @param end_date: string date in big endian format
        @return:
        """
        header_obj = self.pool.get("giscedata.switching.step.header")
        models = [
            'giscedata.switching.c1.02',
            'giscedata.switching.c2.02',
            'giscedata.switching.a3.02'
        ]

        search_filter_05 = [
            ('data_activacio', '>=', start_date),
            ('data_activacio', '<=', end_date)
        ]

        search_filter = [
            ('date_created', '>=', start_date),
            ('date_created', '<=', end_date),
        ]

        main_activated_pending = {}

        for model_name in models:
            switch_02_obj = self.pool.get(model_name)
            switch_04_obj = self.pool.get(model_name.replace('02', '04'))
            switch_05_obj = self.pool.get(model_name.replace('02', '05'))

            # Get sw_ids from steps 05
            switch_05_ids = switch_05_obj.search(cursor, uid, search_filter_05)
            info05 = switch_05_obj.read(cursor, uid, switch_05_ids, ['header_id'])
            info05 = header_obj.read(cursor, uid, [x['header_id'][0] for x in info05], ['sw_id'])
            sw_ids = [x['sw_id'][0] for x in info05]

            # Get sw_ids from steps 04 rejected
            search_filter_04 = search_filter + [
                ('sw_id', 'not in', sw_ids),
                ('rebuig', '=', True)
            ]
            switch_rej04_ids = switch_04_obj.search(cursor, uid, search_filter_04)
            sw_ids_rej04 = switch_04_obj.read(cursor, uid, switch_rej04_ids, ['sw_id'])
            sw_ids = sw_ids + [aux['sw_id'][0] for aux in sw_ids_rej04]

            search_filter_02 = search_filter + [
                ('sw_id', 'not in', sw_ids),
                ('rebuig', '=', False)
            ]
            switch_02_ids = switch_02_obj.search(cursor, uid, search_filter_02)

            switches_02 = switch_02_obj.browse(cursor, uid, switch_02_ids)
            for switch_02 in switches_02:
                self.agent_code = get_r1_code(switch_02.sw_id.company_id)
                self.agent_type = get_agent_type(switch_02.sw_id.whereiam)

                state_code = get_cnmc_state_code(
                    switch_02.sw_id.cups_id.id_municipi)
                distri_code = get_r1_code(get_distri(self.pool, cursor, uid, switch_02.sw_id))
                if not distri_code:
                    raise osv.except_osv(
                        _('Error'),
                        _(
                            u"The XML file has not been generated because the r1 "
                            u"code (ref2 field) is not filled in "
                            u"partner {}".format(
                                get_distri(self.pool, cursor, uid, switch_02.sw_id).name
                            )
                        )
                    )
                ingoing_code = get_ingoing_code(switch_02.sw_id)
                outgoing_code = (
                    switch_02.sw_id.proces_id.name != 'A3' and
                    switch_02.sw_id.comer_sortint_id and
                    get_r1_code(switch_02.sw_id.comer_sortint_id) or
                    '0'
                )
                # switch = step
                access_rate = self.get_tariff_code(cursor, uid, switch_02.sw_id)

                change_type = get_cnmc_change_type(
                    switch_02.sw_id.proces_id.name
                )
                point_type = self.get_point_type(cursor, uid, switch_02)
                main_key = (
                    state_code, distri_code, ingoing_code, outgoing_code,
                    change_type, point_type, access_rate,
                )
                main_activated_pending.setdefault(main_key, {})

                delay_type = "00"  # For now it always will be in time (00)

                main_activated_pending[main_key].setdefault(
                    delay_type, {'total': 0, 'days': 0}
                )
                main_activated_pending[main_key][delay_type]['total'] += 1

                # Store data used for the csv report
                if not main_activated_pending[main_key][delay_type].get('csv_data'):
                    main_activated_pending[main_key][delay_type]['csv_data'] = []

                csv_sw_info = {
                    'cups': switch_02.sw_id.cups_input,
                    'codi_sollicitud': switch_02.sw_id.codi_sollicitud,
                    'process': switch_02.sw_id.proces_id.name,
                    'step': switch_02._nom_pas,
                    'creation_date': switch_02.date_created,
                    'rejected': self.is_switching_rejected(
                        cursor, uid, switch_02.sw_id.id,
                        switch_02.sw_id.proces_id.name,
                        start_date, end_date
                    ),
                }
                main_activated_pending[main_key][delay_type]['csv_data'].append(csv_sw_info)

        activated_pending_switchings = {}
        for main_key, activated_pending in main_activated_pending.iteritems():
            activated_pending_switchings.setdefault(main_key, [])
            for key, totals in activated_pending.iteritems():
                activated_pending_switchings[main_key].append(
                    {
                        'delay_type': key, 'total': totals['total'],
                        'num_issues': 0,
                        'csv_data': totals['csv_data'],
                    }
                )

        return activated_pending_switchings

    def generate_csv_report(self, cursor, uid, switching_info, context=None):
        output_report = StringIO.StringIO()
        writer_report = csv.writer(output_report, quoting=csv.QUOTE_MINIMAL)
        csv_fields = [
            "state", "distributor", "ingoing_comer", "outgoing_comer",
            "point_type", "tariff", "cups", "process", "request_code", "step",
            "step_creation_date", "rejected"
        ]
        writed_cases = []
        writer_report.writerow(csv_fields)
        for key, switch_group in switching_info.iteritems():
            for request_list in [x for x in switch_group.values() if isinstance(x, list)]:
                for request in request_list:
                    for case_dict in request.get('csv_data', []):
                        if case_dict and case_dict['codi_sollicitud'] not in writed_cases:
                            writed_cases.append(case_dict['codi_sollicitud'])
                            common_fields = [
                                key[0], key[1], key[2], key[3], key[5], key[6],
                                case_dict['cups'], case_dict['process'],
                                case_dict['codi_sollicitud']
                            ]
                            for step_info in self.get_historic_of_case(cursor, uid, case_dict['codi_sollicitud'], context=context):
                                writer_report.writerow(
                                    common_fields + step_info +
                                    [case_dict['rejected']]
                                )
        value = output_report.getvalue()
        output_report.close()
        return value

    def get_historic_of_case(self, cursor, uid, codi_sol, context=None):
        """
        :param codi_sol: codi de solicitud de un cas atr
        :return: llista amb llistes [pas, data_creacio_pas]
        Si el pas es un 05, es posa la data_activacio en comptes de la de creacio
        """
        res = []
        sw_obj = self.pool.get("giscedata.switching")
        step_info_obj = self.pool.get("giscedata.switching.step.info")

        sw_id = sw_obj.search(cursor, uid, [('codi_sollicitud', '=', codi_sol)])
        sw_id = sw_id[0]

        step_info_ids = step_info_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
        steps = step_info_obj.read(cursor, uid, step_info_ids, ['pas_id'])
        for step in steps:
            model, pas_id = step['pas_id'].split(",")
            model_obj = self.pool.get(model)
            pas = model.split(".")[-1]
            if pas == '05':
                data_pas = model_obj.read(cursor, uid, int(pas_id), ["data_activacio"])["data_activacio"]
            else:
                data_pas = model_obj.read(cursor, uid, int(pas_id), ["date_created"])["date_created"]
            res.append([pas, data_pas])
        return res

SwitchingComerChangesReport()


class SwitchingSupplyPointsReport(osv.osv):

    _name = 'supply.points.cnmc.report'
    _auto = False
    _inherit = 'switching_cnmc_report'

    def get_header_info(self, cursor, uid, context=None):
        return {
            'agent_code': self.agent_code,
            'market_type': 'E',  # Energy market always
            'agent_type': 'D',  # It's distri always
            'period': self.period
        }

    def get_xml_name(self, cursor, uid, context=None):
        return "PS_{}_E_{}_01.xml".format(self.agent_code, self.period)

    def create_report(self, cursor, uid, start_date, end_date, context=None):
        """
        Generates de supply points report
        @param cursor:
        @param uid:
        @param start_date:
        @param end_date:
        @param context:
        @return: Returns a dictionary with two keys: 'xml' with the generated
        xml; 'is_valid' tells if the generated xml is valid according to its
        defined xsd.
        """
        self.set_period(cursor, uid, start_date)

        contract_mod_obj = self.pool.get('giscedata.polissa.modcontractual')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        # Avoid RE contracts
        re_ids = tariff_obj.search(cursor, uid, [('name', 'like', 'RE%')])

        search_filter = [
            ('data_inici', '<=', end_date),
            ('data_final', '>=', end_date),
            ('tarifa', 'not in', re_ids)
        ]

        contract_mod_ids = contract_mod_obj.search(
            cursor, uid, search_filter, context={'active_test': False}
        )

        supply_points = {}
        distri_code = None

        contract_mods = contract_mod_obj.browse(cursor, uid, contract_mod_ids)
        if len(contract_mods) == 0:
            raise osv.except_osv(
                _('Error'),
                _(
                    "The XML file has not been generated because there are no "
                    "contract amendments for the given date ({})"
                ).format(
                    self.format_date(cursor, uid, end_date, context)
                )
            )

        for contract_mod in contract_mods:
            distri_code = get_r1_code(contract_mod.cups.distribuidora_id)
            self.agent_code = distri_code
            comer_code = get_r1_code(contract_mod.comercialitzadora)
            rate = get_cnmc_tarifa_atr(contract_mod.tarifa.name)
            state = get_cnmc_state_code(contract_mod.cups.id_municipi)
            key = (distri_code, comer_code, rate, state)
            supply_points.setdefault(key, 0)
            supply_points[key] += 1
            if not comer_code:
                raise osv.except_osv(
                    _('Error'),
                    _(
                        "The XML file has not been generated because there are "
                        "no ref2 in retailer of contract {}. You may fill with "
                        "R2-000 if is a 'Direct agent (not retailer)'"
                    ).format(contract_mod.polissa_id.name)
                )
        xml = SupplyPointsXml()
        xml.header.feed(self.get_header_info(cursor, uid))
        for key, totals in supply_points.iteritems():
            xml.add_supply_point_detail(key, totals)
        xml.generate_xml()
        return {
            "xml": str(xml), "is_valid": xml.is_valid
        }
SwitchingSupplyPointsReport()
