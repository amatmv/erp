# -*- coding: utf-8 -*-
{
    "name": "CNMC Switching Reports",
    "description": """
    Allows to create monthly reports about switching requested by the CNMC.
    To use this module a user needs to be in of the "GISCEDATA ADMIN. PUB. CNE"
    groups.
  """,
    "version": "0-dev",
    "author": "Miquel Frontera",
    "category": "Other",
    "depends":[
        "giscedata_switching",
        "giscedata_administracio_publica_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_cnmc_xml_report_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
