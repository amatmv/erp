# -*- coding: utf-8 -*-
import base64

from osv import osv, fields
from tools.translate import _


class WizardCnmcXmlReport(osv.osv_memory):

    def _get_file_types(self, cursor, uid, context=None):
        """
        This method returns the files allowed to create depending on the
        instance of erp we are: distri or comer.
        @param cr:
        @param uid:
        @param ids:
        @param field_name:
        @param arg:
        @param context:
        @return:
        """
        file_types = [
            ('SI', _('Comer change file')),
        ]
        if self.whereiam(cursor, uid, context) == 'distri':
            file_types.append(('PS', _('Supply points file')))

        return file_types

    _name = 'wizard.cnmc.xml.report'

    _columns = {
        'file_type': fields.selection(_get_file_types, _('File type')),
        'filename': fields.char(_('File name'), size=64),
        'start_date': fields.date(_('Start date'), required=True),
        'end_date': fields.date(_('End date'), required=True),
        'state':  fields.char(_('State'), size=16),
        'file': fields.binary(_('Result')),
        'status': fields.char(_('Status'), size=100),
        'file_csv': fields.binary(_('Result CSV')),
        'filename_csv': fields.char(_('File name'), size=64),
    }

    _defaults = {
        'state': 'init',
        'file_type': 'SI',
        'status': ''
    }

    def whereiam(self, cursor, uid, context=None):
        """
        To know if we are on distri or comer.
        @param cursor:
        @param uid:
        @param context:
        @return: a string 'comer' or 'distri' depending on where we are.
        """
        cups_obj = self.pool.get('giscedata.cups.ps')

        return cups_obj.whereiam(cursor, uid, context)

    def action_generate_file(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        file_generator = self.get_file_generator(
            cursor, uid, wiz.file_type, context
        )

        result = file_generator.create_report(
            cursor, uid, wiz.start_date, wiz.end_date, context
        )
        file_name = file_generator.get_xml_name(cursor, uid, context)

        file_content = base64.b64encode(result['xml'])
        status = _("File {} has been generated correctly").format(file_name)
        if not result['is_valid']:
            status = _("File {} is NOT valid. Please, review it.").format(
                file_name
            )

        file_content_csv = ""
        if result.get("csv"):
            file_content_csv = base64.b64encode(result.get("csv"))

        wiz.write({
            'filename': file_name, 'file': file_content, 'state': 'done',
            'status': status,
            'filename_csv': file_name.split(".")[0]+".csv",
            'file_csv': file_content_csv
        }, context=context)

    def get_file_generator(self, cursor, uid, file_type, context=None):
        if file_type == 'SI':
            return self.pool.get('comer.changes.cnmc.report')
        elif file_type == 'PS':
            return self.pool.get('supply.points.cnmc.report')

        raise osv.except_osv(
                _('Error'),
                _(
                    u"Sorry, the file type '{}' you selected cannot"
                    u" be found"
                ).format(file_type)
            )

WizardCnmcXmlReport()
