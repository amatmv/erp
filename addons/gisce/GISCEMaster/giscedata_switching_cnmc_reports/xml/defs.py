# -*- coding: utf-8 -*-

# unknown rate:
UNKNOWN_RATE = 'unknown'

# keys => tarifaATR values of the openerp. Values are tarifaATR that wants CNMC
CNMC_TARIFA_ATR_VALUES = {
    "unknown": "0",
    "2.0A": "1",
    "2.1A": "1T",
    "2.0DHA": "2",
    "2.0DHS": "2S",
    "2.1DHA": "2T",
    "2.1DHS": "2V",
    "3.0A": "3",
    "3.1A": "4",
    "3.1A LB": "4",
    "6.1A": "5",
    "6.1B": "5T",
    "6.2": "6",
    "6.3": "7",
    "6.4": "8",
    "6.5": "9"
}
