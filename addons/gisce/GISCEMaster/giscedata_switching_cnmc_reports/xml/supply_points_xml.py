# -*- coding: utf-8 -*-
import StringIO
from lxml import etree

from tools import config
from libcomxml.core import XmlModel, XmlField


class SupplyPointsXml(XmlModel):

    name = "PuntosDeSuministro"
    namespace = "http://www.w3.org/2001/XMLSchema-instance"
    xsd_file = "PuntosDeSuministro_v1.0.xsd"
    is_valid = False
    _sort_order = ('header', 'supply_point_info')

    def __init__(self):
        # Escape braces, with more braces, to left one pair in the string:
        location_attribute = "{{{}}}noNamespaceSchemaLocation".format(
            self.namespace
        )

        # We need to generate in the root element the xsi attributes with
        # the right namespaces:
        self.data = XmlField(
            name=self.name, attributes={location_attribute: self.xsd_file}
        )
        self.header = HeaderElement()
        self.supply_point_info = SupplyPointsInfo()
        super(SupplyPointsXml, self).__init__(self.name, 'data')
        self._pretty_print = True

    def get_xsd_path(self):
        return "{}/giscedata_switching_cnmc_reports/xml/{}".format(
            config['addons_path'], self.xsd_file
        )

    def generate_xml(self):
        self.build_tree()
        if self.is_valid_xml():
            self.is_valid = True

    def is_valid_xml(self):
        xsd_file = open(self.get_xsd_path(), 'r')
        try:
            xml_schema = etree.XMLSchema(file=xsd_file)
            xml = etree.parse(StringIO.StringIO(str(self)))
            return xml_schema.validate(xml)
        finally:
            xsd_file.close()

    def add_supply_point_detail(self, info, totals):

        supply_detail = SupplyPointDetail()
        supply_detail.distributor.set_value(info[0])
        supply_detail.comer.set_value(info[1])
        supply_detail.access_rate_type.set_value(info[2])
        supply_detail.state.set_value(info[3])
        supply_detail.total_supply_points.set_value(totals)

        self.supply_point_info.supply_points_details.append(supply_detail)


class HeaderElement(XmlModel):
    _sort_order = ('agent_code', 'market_type', 'agent_type', 'period')

    def __init__(self):
        self.data = XmlField('Cabecera')
        self.agent_code = XmlField('CodigoAgente')
        self.market_type = XmlField('TipoMercado')
        self.agent_type = XmlField('TipoAgente')
        self.period = XmlField('Periodo')

        super(HeaderElement, self).__init__('Cabecera', 'data')


class SupplyPointsInfo(XmlModel):

    def __init__(self):
        self.data = XmlField('DatosPtosSuministro')
        self.supply_points_details = []

        super(SupplyPointsInfo, self).__init__('DatosPtosSuministro', 'data')


class SupplyPointDetail(XmlModel):
    _sort_order = (
        'state', 'distributor', 'comer', 'access_rate_type',
        'total_supply_points'
    )

    def __init__(self):
        self.data = XmlField('DetallePtosSuministro')
        self.state = XmlField('Provincia')
        self.distributor = XmlField('Distribuidor')
        self.comer = XmlField('Comercializador')
        self.access_rate_type = XmlField('TarifaATR')
        self.total_supply_points = XmlField('TotalPuntosDeSuministro')

        super(SupplyPointDetail, self).__init__('DetallePtosSuministro', 'data')
