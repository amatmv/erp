# -*- coding: utf-8 -*-
import StringIO
from lxml import etree

from tools import config
from libcomxml.core import XmlModel, XmlField


class ComerChangesXml(XmlModel):
    name = "MensajeSolicitudesRealizadas"
    namespace = 'http://www.w3.org/2001/XMLSchema-instance'
    xsd_file = 'SolicitudesRealizadas_v1.0.xsd'
    is_valid = False
    _sort_order = ('header', 'requests_done')

    def __init__(self):
        # Escape braces, with more braces, to left one pair in the string:
        location_attribute = "{{{}}}noNamespaceSchemaLocation".format(
            self.namespace
        )

        # We need to generate in the root element the xsi attributes with
        # the right namespaces:
        self.data = XmlField(
            name=self.name, attributes={location_attribute: self.xsd_file}
        )
        self.header = HeaderElement()
        self.requests_done = RequestsDone()
        super(ComerChangesXml, self).__init__(self.name, 'data')
        self._pretty_print = True

    def add_request_done(self, key, totals):
        """
        Fills the request data from a dictionary in a fixed
        format.
        @param key: it's a tuple with the values to group by the info
        @param totals: a dictionary with the totals calculated
        @return:
        """

        data_request = DataRequests()
        data_request.state.set_value(key[0])
        data_request.distributor.set_value(key[1])
        data_request.ingoing_comer.set_value(key[2])
        data_request.outgoing_comer.set_value(key[3])
        data_request.change_type.set_value(key[4])
        data_request.point_type.set_value(key[5])
        data_request.access_rate_type.set_value(key[6])
        # Need of a str cast because libComXMl considers 0 == None and
        # doesn't add tags with None value:
        data_request.total_sent.set_value(str(totals['total']))
        data_request.total_canceled.set_value(str(totals['total_canceled']))
        data_request.num_replacements.set_value("0")  # Reposicion
        data_request.num_unsubscribed_clients.set_value("0")  # Salientes
        data_request.num_clients_with_unpaid_bills.set_value("0")  # Impagados

        data_request.add_pending_requests(totals['pending'])
        data_request.add_accepted_requests(totals['accepted'])
        data_request.add_rejected_requests(totals['rejected'])
        data_request.add_activated_pending_requests(totals['activated_pending'])
        data_request.add_activated_requests(totals['activated'])

        self.requests_done.data_requests.append(data_request)

    def get_xsd_path(self):
        return "{}/giscedata_switching_cnmc_reports/xml/{}".format(
            config['addons_path'], self.xsd_file
        )

    def generate_xml(self):
        self.build_tree()
        if self.is_valid_xml():
            self.is_valid = True

    def is_valid_xml(self):
        xsd_file = open(self.get_xsd_path(), 'r')
        try:
            xml_schema = etree.XMLSchema(file=xsd_file)
            xml = etree.parse(StringIO.StringIO(str(self)))
            return xml_schema.validate(xml)
        finally:
            xsd_file.close()


class HeaderElement(XmlModel):

    _sort_order = ('agent_code', 'market_type', 'agent_type', 'period')

    def __init__(self):
        self.data = XmlField('Cabecera')
        self.agent_code = XmlField('CodigoAgente')
        self.market_type = XmlField('TipoMercado')
        self.agent_type = XmlField('TipoAgente')
        self.period = XmlField('Periodo')

        super(HeaderElement, self).__init__('Cabecera', 'data')


class RequestsDone(XmlModel):

    _sort_order = (
        'data_requests', 'activated_requests'
    )

    def __init__(self):
        self.data = XmlField('SolicitudesRealizadas')
        self.data_requests = []
        self.activated_requests = []
        super(RequestsDone, self).__init__('SolicitudesRealizadas', 'data')


class DataRequests(XmlModel):

    _sort_order = (
        'state', 'distributor', 'ingoing_comer', 'outgoing_comer',
        'change_type', 'point_type', 'access_rate_type', 'total_sent',
        'total_canceled', 'num_replacements', 'num_unsubscribed_clients',
        'num_clients_with_unpaid_bills', 'pending_requests',
        'accepted_requests', 'rejected_requests', 'activated_pending_requests',
        'activated_requests'
    )

    def __init__(self):
        self.data = XmlField('DatosSolicitudes')
        self.state = XmlField('Provincia')
        self.distributor = XmlField('Distribuidor')
        self.ingoing_comer = XmlField('Comer_entrante')
        self.outgoing_comer = XmlField('Comer_saliente')
        self.change_type = XmlField('TipoCambio')
        self.point_type = XmlField('TipoPunto')
        self.access_rate_type = XmlField('TarifaATR')
        self.total_sent = XmlField('TotalSolicitudesEnviadas')
        self.total_canceled = XmlField('SolicitudesAnuladas')
        self.num_replacements = XmlField('Reposiciones')
        self.num_unsubscribed_clients = XmlField('ClientesSalientes')
        self.num_clients_with_unpaid_bills = XmlField('NumImpagados')

        self.pending_requests = []
        self.accepted_requests = []
        self.rejected_requests = []
        self.activated_pending_requests = []
        self.activated_requests = []

        super(DataRequests, self).__init__('DatosSolicitudes', 'data', drop_empty=False)

    def add_pending_requests(self, pending_requests):
        for request in pending_requests:
            pending = PendingRequests()

            pending.delay_type.set_value(request['delay_type'])
            pending.num_pending_requests.set_value(request['total'])

            self.pending_requests.append(pending)

    def add_accepted_requests(self, accepted_requests):
        for request in accepted_requests:
            accepted = AcceptedRequests()

            accepted.delay_type.set_value(request['delay_type'])
            accepted.num_accepted_requests.set_value(request['total'])
            accepted.accepted_request_avg_time.set_value(
                str(request['avg_time'])
            )

            self.accepted_requests.append(accepted)

    def add_rejected_requests(self, rejected_requests):
        for request in rejected_requests:
            rejected = RejectedRequests()

            rejected.delay_type.set_value(request['delay_type'])
            rejected.reject_reason.set_value(request['reason'])
            rejected.num_rejected_requests.set_value(request['total'])
            rejected.rejected_request_avg_time.set_value(
                str(request['avg_time'])
            )

            self.rejected_requests.append(rejected)

    def add_activated_pending_requests(self, activated_pending_info):
        for activated_pending in activated_pending_info:
            activated_pending_request = ActivatedPendingRequests()

            activated_pending_request.delay_type.set_value(
                activated_pending['delay_type']
            )
            activated_pending_request.num_issues.set_value(
                str(activated_pending['num_issues'])
            )
            activated_pending_request.num_activated_pending_requests.set_value(
                activated_pending['total']
            )

            self.activated_pending_requests.append(activated_pending_request)

    def add_activated_requests(self, activated_info):
        for activated in activated_info:
            activated_request = ActivatedRequests()

            activated_request.delay_type.set_value(activated['delay_type'])
            activated_request.activated_request_avg_time.set_value(
                str(activated['avg_time'])
            )
            activated_request.num_activated_requests.set_value(
                activated['total']
            )
            activated_request.num_issues.set_value("0")

            self.activated_requests.append(activated_request)


class PendingRequests(XmlModel):
    _sort_order = (
        'delay_type', 'num_pending_requests'
    )

    def __init__(self):
        self.data = XmlField('DetallePendientesRespuesta')
        self.delay_type = XmlField('TipoRetraso')
        self.num_pending_requests = XmlField('NumSolicitudesPendientes')

        super(PendingRequests, self).__init__('DetallePendientesRespuesta', 'data')


class RejectedRequests(XmlModel):
    _sort_order = (
        'delay_type', 'rejected_request_avg_time', 'reject_reason',
        'num_rejected_requests'
    )

    def __init__(self):
        self.data = XmlField('DetalleRechazadas')
        self.delay_type = XmlField('TipoRetraso')
        self.rejected_request_avg_time = XmlField('TMSolicitudesRechazadas')
        self.reject_reason = XmlField('MotivoRechazo')
        self.num_rejected_requests = XmlField('NumSolicitudesRechazadas')

        super(RejectedRequests, self).__init__('DetalleRechazadas', 'data')


class AcceptedRequests(XmlModel):
    _sort_order = (
        'delay_type', 'accepted_request_avg_time', 'num_accepted_requests'
    )

    def __init__(self):
        self.data = XmlField('DetalleAceptadas')
        self.delay_type = XmlField('TipoRetraso')
        self.accepted_request_avg_time = XmlField('TMSolicitudesAceptadas')
        self.num_accepted_requests = XmlField('NumSolicitudesAceptadas')

        super(AcceptedRequests, self).__init__('DetalleAceptadas', 'data')


class ActivatedPendingRequests(XmlModel):
    _sort_order = (
        'delay_type', 'num_issues',
        'num_activated_pending_requests'
    )

    def __init__(self):
        self.data = XmlField('DetallePdteActivacion')
        self.delay_type = XmlField('TipoRetraso')
        self.num_issues = XmlField('NumIncidencias')
        self.num_activated_pending_requests = XmlField('NumSolicitudesPdteActivacion')

        super(ActivatedPendingRequests, self).__init__('DetallePdteActivacion', 'data')


class ActivatedRequests(XmlModel):
    _sort_order = (
        'delay_type', 'activated_request_avg_time', 'num_issues',
        'num_activated_requests'
    )

    def __init__(self):
        self.data = XmlField('DetalleActivadas')
        self.delay_type = XmlField('TipoRetraso')
        self.activated_request_avg_time = XmlField('TMActivacion')
        self.num_issues = XmlField('NumIncidencias')
        self.num_activated_requests = XmlField('NumSolicitudesActivadas')

        super(ActivatedRequests, self).__init__('DetalleActivadas', 'data')
