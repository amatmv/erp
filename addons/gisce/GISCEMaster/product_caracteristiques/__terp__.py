# -*- coding: utf-8 -*-
{
    "name": "product_caracteristiques",
    "description": """Afegeix una pestanya amb un llistat de característiques als diferents productes""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "product"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "product_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
