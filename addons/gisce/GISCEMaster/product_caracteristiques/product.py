# -*- coding: utf-8 -*-
"""Productes amb Característiques"""
from osv import fields, osv
from osv.orm import OnlyFieldsConstraint


class ProductCaracteristica(osv.osv):
    _name = 'product.caracteristica'

    def check_uniq_caract_name(self, cursor, uid, ids):
        for caract in self.read(cursor, uid, ids, ['product_id', 'name']):
            if self.search_count(cursor, uid, [
                ('name', '=', caract['name']),
                ('product_id', '=', caract['product_id'][0])
            ]) > 1:
                return False
        return True

    _columns = {
        'name': fields.char(size=100, string='Nom', required=True),
        'description': fields.text(string='Descripció'),
        'value': fields.char(size=100, string='Valor', required=True),
        'product_id': fields.many2one('product.product', string='Producte',
                                      required=True, ondelete='cascade')
    }

    _constraints = [
        OnlyFieldsConstraint(
            check_uniq_caract_name,
            "No es pot repetir la característica en un producte.",
            ["name"]),
    ]


ProductCaracteristica()


class ProductAmbCaracteristiques(osv.osv):
    _inherit = 'product.product'
    _name = 'product.product'

    _columns = {
        'extra_vals_ids': fields.one2many(
            'product.caracteristica', 'product_id', string='Caracteristiques'),
    }


ProductAmbCaracteristiques()
