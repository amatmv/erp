# -*- coding: utf-8 -*-
{
    "name": "GISCE BT multicompany",
    "description": """Multi-company support for BT""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi Company",
    "depends":[
        "giscedata_bt"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_bt_multicompany_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
