# -*- coding: utf-8 -*-
{
    "name": "GISCE CUPS extensió",
    "description": """Camp duplicador pel CUPS""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cups_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
