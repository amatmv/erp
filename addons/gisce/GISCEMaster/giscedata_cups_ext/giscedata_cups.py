# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataCupsPs(osv.osv):
    _name = "giscedata.cups.ps"
    _inherit = "giscedata.cups.ps"
    _columns = {
        'duplicador': fields.char('Duplicador', size=64)
    }

GiscedataCupsPs()
