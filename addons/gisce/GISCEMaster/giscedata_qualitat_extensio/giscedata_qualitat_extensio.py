# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from time import *
from tools.translate import _


class giscedata_qualitat_span(osv.osv):
    _inherit = 'giscedata.qualitat.span'
    _name = 'giscedata.qualitat.span'
    _tipus_tall = ((False, ''),
                ('m', _(u'Microtall')),
                ('a', _(u'Avaria')),
                ('r', _(u'Revisió')),
                ('e', _(u'Reparació')),
                ('p', _(u'Ampliació')),
                ('f', _(u'Reforma')))
    _columns = {
      'tipus_tall': fields.selection(_tipus_tall, 'Tipus de tall'),
      'descripcio': fields.text('Explicació avaria'),
      'ampers': fields.float('Ampers'),
      'ct': fields.many2one('giscedata.cts', 'CT'),
      'linia': fields.many2one('giscedata.at.linia', 'Línia AT'),
    }

giscedata_qualitat_span()


class giscedata_qualitat_span_report(osv.osv):
    _name = 'giscedata.qualitat.span.report'
    _description = 'Report de microtalls'
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'interruptor': fields.char('Interruptor', size=50, readonly=True),
      'linia': fields.char('Línia', size=50, readonly=True),
      'mes': fields.integer('Mes', readonly=True),
      'any': fields.integer('Any', readonly=True),
      'tipus': fields.char('Tipus', size=50, readonly=True),
      'causa': fields.char('Causa', size=50, readonly=True),
      'data': fields.datetime('Data', readonly=True),
    }

giscedata_qualitat_span_report()
