<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:gi="http://gisce.net/XSL/xpath-functions" version="1.0">
    <xsl:strip-space elements="microtalls span"/>
    <xsl:template match="/">
        <xsl:apply-templates select="microtalls"/>
    </xsl:template>
    <xsl:template name="for.loop">
        <xsl:param name="i"/>
        <xsl:param name="mark"/>
        <xsl:param name="count"/>
        <xsl:choose>
            <xsl:when test="$i = $mark">
                <td>x</td>
            </xsl:when>
            <xsl:otherwise>
                <td/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="$i &lt;= $count">
            <xsl:call-template name="for.loop">
                <xsl:with-param name="i">
                    <xsl:value-of select="$i + 1"/>
                </xsl:with-param>
                <xsl:with-param name="count">
                    <xsl:value-of select="$count"/>
                </xsl:with-param>
                <xsl:with-param name="mark">
                    <xsl:value-of select="$mark"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template name="talls.mes">
        <xsl:param name="mesActual"/>
        <xsl:param name="mesFinal"/>
        <xsl:if test="$mesActual &lt;= $mesFinal">
            <!-- fer la feina -->
            <xsl:if test="count(//microtalls/span[mes=$mesActual]) = 0">
                <tr>
                    <td/>
                </tr>
            </xsl:if>
            <xsl:for-each select="//microtalls/span[mes=$mesActual]">
                <tr>
                    <xsl:choose>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='ADRALL-1'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">1</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='ADRALL-2'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">2</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='Querforadat'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">3</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='LES TORRES'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">4</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='MARTINET'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">5</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='LA FARGA'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">6</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='OS DE CIVIS'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">7</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='ARSEGUEL'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">8</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='ARDATX'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">9</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='Aravell-Bellestar'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">10</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='Estana'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">11</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='104-ADRALL-1'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">12</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:when test="//microtalls/span[mes=$mesActual and linia='104-ADRALL-2'] = current()">
                            <xsl:call-template name="marcar-x">
                                <xsl:with-param name="index">13</xsl:with-param>
                            </xsl:call-template>
                        </xsl:when>
                        <xsl:otherwise>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                            <td/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <td>
                        <xsl:value-of select="tipus"/>
                    </td>
                    <td>
                        <xsl:value-of select="data"/>
                    </td>
                    <td>
                        <xsl:value-of select="causa"/>
                    </td>
                </tr>
            </xsl:for-each>
            <!-- i cridar la funci� un altre cop -->
            <xsl:call-template name="talls.mes">
                <xsl:with-param name="mesActual">
                    <xsl:value-of select="$mesActual + 1"/>
                </xsl:with-param>
                <xsl:with-param name="mesFinal">12</xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    <xsl:template name="marcar-x">
        <xsl:param name="index"/>
        <xsl:call-template name="for.loop">
            <xsl:with-param name="i">1</xsl:with-param>
            <xsl:with-param name="mark">
                <xsl:value-of select="($index)"/>
            </xsl:with-param>
            <xsl:with-param name="count">12</xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    <xsl:template match="microtalls">
        <document>
            <template pageSize="(297mm,19cm)">
                <pageTemplate id="main">
                    <frame id="frameTitol" x1="0cm" y1="16.5cm" width="29cm" height="2cm"/>
                    <frame id="frameMesos" x1="0cm" y1="0cm" width="35mm" height="17cm"/>
                    <frame id="frameTalls" x1="41mm" y1="cm" width="232mm" height="17cm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <paraStyle name="heading" fontSize="24" fontName="Helvetica-Bold" alignment="center"/>
                <blockTableStyle id="mesos">
                    <blockFont name="Helvetica-Bold" size="6"/>
                    <blockAlignment value="CENTER" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,-1"/>
                    <lineStyle kind="LINEBELOW" colorName="black" start="0,0" stop="-1,-1"/>
                    <blockBackground colorName="silver" start="0,0" stop="0,-1"/>
                </blockTableStyle>
                <blockTableStyle id="talls">
                    <blockAlignment value="CENTER" start="0,0" stop="-1,-1"/>
                    <blockFont name="Helvetica-Bold" size="6" start="0,0" stop="-1,0"/>
                    <blockFont name="Helvetica" size="6" start="0,1" stop="-1,-1"/>
                    <blockFont name="Helvetica-Bold" size="6" start="0,-1" stop="-1,-1"/>
                    <lineStyle kind="GRID" colorName="black" start="0,0" stop="-1,-1"/>
                    <blockBackground colorName="silver" start="0,0" stop="-1,0"/>
                </blockTableStyle>
            </stylesheet>
            <story>
                <para style="heading">Resum dels microtalls de l'any <xsl:value-of select="//microtalls/span[1]/any"/></para>
                <nextFrame id="frameMesos"/>
                <blockTable style="mesos" repeatRows="1">
                    <tr>
                        <td>Mes \ L�nia</td>
                    </tr>
                    <tr>
                        <td>Gener</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=1]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Febrer</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=2]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Mar�</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=3]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Abril</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=4]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Maig</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=5]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Juny</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=6]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Juliol</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=7]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Agost</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=8]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Setembre</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=9]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Octubre</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=10]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Novembre</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=11]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Desembre</td>
                    </tr>
                    <xsl:for-each select="//microtalls/span[mes=12]">
                        <xsl:if test="not(position() = 1)">
                            <tr>
                                <td/>
                            </tr>
                        </xsl:if>
                    </xsl:for-each>
                    <tr>
                        <td>Total</td>
                    </tr>
                </blockTable>
                <nextFrame id="frameTalls"/>
                <blockTable style="talls" id="taula" colWidths="1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,1.6cm,2cm,2.2cm,1.5cm">
                    <!-- <blockTable> -->
                    <tr>
                        <!-- fila titols -->
                        <td>ADRALL-1</td>
                        <td>ADRALL-2</td>
                        <td>Querforadat</td>
                        <td>LES TORRES</td>
                        <td>MARTINET</td>
                        <td>LA FARGA</td>
                        <td>OS CIVIS</td>
                        <td>ARS�GUEL</td>
                        <td>ARDATX</td>
                        <td>Aravell-Bellestar</td>
                        <td>Estana</td>
                        <td>104-ADRALL-1</td>
                        <td>104-ADRALL-2</td>
                        <td>Falta</td>
                        <td>Data</td>
                        <td>Causa</td>
                    </tr>
                    <xsl:call-template name="talls.mes">
                        <xsl:with-param name="mesActual">1</xsl:with-param>
                        <xsl:with-param name="mesFinal">12</xsl:with-param>
                    </xsl:call-template>
                    <tr>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ADRALL-1'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ADRALL-2'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='Querforadat'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='LES TORRES'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='MARTINET'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='LA FARGA'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='OS DE CIVIS'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ARSEGUEL'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='ARDATX'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='Aravell-Bellestar'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='Estana'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='104-ADRALL-1'])"/>
                        </td>
                        <td>
                            <xsl:value-of select="count(//microtalls/span[linia='104-ADRALL-2'])"/>
                        </td>
                        <td/>
                        <td/>
                        <td/>
                    </tr>
                </blockTable>
            </story>
        </document>
    </xsl:template>
</xsl:stylesheet>
