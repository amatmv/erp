# -*- coding: utf-8 -*-
{
    "name": "GISCE Qualitat (Extensió)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "giscedata_polissa",
        "giscedata_cts",
        "base_extended",
        "giscedata_qualitat",
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_qualitat_extensio_view.xml",
        "giscedata_qualitat_extensio_report.xml",
        "giscedata_qualitat_extensio_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
