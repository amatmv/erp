# -*- coding: utf-8 -*-
{
    "name": "Partner Técnic",
    "description": """Master Base models""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master Generic Modules",
    "depends":[
        "base"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "partner_tecnic_view.xml"
    ],
    "active": False,
    "installable": True
}
