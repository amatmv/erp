# -*- coding: iso-8859-1 -*-
from osv import osv,fields

class res_partner_tecnic_titolacio(osv.osv):
  _name = 'res.partner.tecnic.titolacio'
  _description = 'Titolació el técnic'
  _columns = {
    'name': fields.char('Titolació', size=100, required=True),
  }

  _defaults = {

  }

  _order = "name, id"
res_partner_tecnic_titolacio()

class res_partner_tecnic_colegi(osv.osv):
  _name = 'res.partner.tecnic.colegi'
  _description = 'Col·legi del técnic'
  _columns = {
    'name': fields.char('Col·legi', size=100, required=True),
  }

  _defaults = {

  }

  _order = "name, id"
res_partner_tecnic_colegi()

class res_partner_tecnic_organisme(osv.osv):
  _name = 'res.partner.tecnic.organisme'
  _description = 'Organisme del técnic'
  _columns = {
    'name': fields.char('Organisme', size=100, required=True),
  }

  _defaults = {

  }

  _order = "name, id"
res_partner_tecnic_organisme()

class res_partner(osv.osv):
  _inherit = 'res.partner'
  _name = 'res.partner'

  _columns = {
    'dni': fields.char('DNI', size=9),
    'titolacio': fields.many2one('res.partner.tecnic.titolacio', 'Titolació'),
    'colegi': fields.many2one('res.partner.tecnic.colegi', 'Colegi'),
    'ncolegi': fields.char('Número de Col·legiat', size=10),
    'organisme': fields.many2one('res.partner.tecnic.organisme', 'Organisme'),
  }
res_partner()
