# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Base",
    "description": """Master Base models""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master Generic Modules",
    "depends": [],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_base_view.xml"
    ],
    "active": False,
    "installable": True
}
