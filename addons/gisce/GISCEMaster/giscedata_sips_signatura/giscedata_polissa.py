# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv


class GiscedataPolissa(osv.osv):
    _name = "giscedata.polissa"
    _inherit = 'giscedata.polissa'

    def start_sips_cession(self, cursor, uid, pol_ids, cas_ids, addr_id, context=None):
        if not context:
            context = {}
        atr_obj = self.pool.get('giscedata.switching')
        if not isinstance(cas_ids, (list, tuple)):
            cas_ids = [cas_ids]
        for cas_id in cas_ids:
            cas_browse = atr_obj.browse(cursor, uid, cas_id, context=context)
            pas_browse = atr_obj.get_pas(cursor, uid, cas_browse,
                                         context=context)
            pas_browse.write({
                'enviament_pendent': context.get("enviament_pendent", False)
            })
            cas_browse.write({'state': context.get('state', 'pending')})

        return True

    def enviar_sips(self, cursor, uid, pol_id, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        cas_ids = process_data.get('cas_ids', False)
        sw_obj = self.pool.get('giscedata.switching')
        search_params = [
            ('cups_polissa_id', '=', pol_id),
            ('proces_id.name', '=', 'R1'),
            ('step_id.name', '=', '01')
        ]
        if cas_ids:
            search_params.append(('id', 'in', cas_ids))
        cas_id = sw_obj.search(cursor, uid, search_params)
        pas_browse = sw_obj.get_pas(cursor, uid, cas_id)
        pas_browse.write(
            {'enviament_pendent': context.get("enviament_pendent", True)})
        sw_obj.write(cursor, uid, cas_id, {'state': 'open'})
        return True

    def process_signature_callback(self, cursor, uid, cas_id, context=None):
        if not context:
            context = {}
        process_data = context.get('process_data', False)
        if process_data:
            method = process_data.get('callback_method', False)
            if method == 'enviar_sips':
                return self.enviar_sips(cursor, uid, cas_id, context=context)
        super(GiscedataPolissa, self).process_signature_callback(
            cursor, uid, cas_id, context=context
        )


GiscedataPolissa()
