# -*- coding: utf-8 -*-
{
    "name": "Signatura digital Cessión SIPS (Comercializadora)",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_switching_comer",
        "giscedata_signatura_documents",
    ],
    "init_xml": [],
    "demo_xml":[
    ],
    "update_xml":[
        "giscedata_sips_data.xml"
    ],
    "active": False,
    "installable": True
}
