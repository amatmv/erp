<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="llistat"/>
    </xsl:template>
    <xsl:template match="llistat">
        <document>
            <template pageSize="(297mm,19cm)" topMargin="1cm" bottomMargin="1cm" rightMargin="1cm">
                <pageTemplate id="main">
                    <frame id="first" x1="1cm" y1="1cm" width="277mm" height="17cm"/>
                    <pageGraphics>
                        <setFont name="Helvetica-Bold" size="7"/>
                        <drawString x="10mm" y="10mm" t="1">Segun Real Decreto 1955/2000 de 1 de diciembre y orden ECO797/2002 de 22 de marzo.</drawString>
                        <lines>
              10mm 9mm 287mm 10mm
            </lines>
                    </pageGraphics>
                </pageTemplate>
            </template>
            <stylesheet>
                <blockTableStyle id="titol">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica-Bold" size="20"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="25"/>
                </blockTableStyle>
                <blockTableStyle id="subtitol">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="14"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="19"/>
                </blockTableStyle>
                <blockTableStyle id="taula1">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica-Bold" size="10"/>
                </blockTableStyle>
                <blockTableStyle id="taula2">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" colorName="silver" start="3,0"/>
                    <blockFont name="Helvetica-Bold" size="10" start="0,0" stop="2,0"/>
                    <lineStyle kind="LINEABOVE" colorName="silver" start="0,0" stop="2,0"/>
                    <lineStyle kind="LINEBEFORE" colorName="silver" start="0,0" stop="2,0"/>
                </blockTableStyle>
                <blockTableStyle id="taula3">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="8"/>
                    <lineStyle kind="GRID" colorName="silver" start="3,0"/>
                    <lineStyle kind="LINEBEFORE" colorName="silver" start="0,0" stop="2,0"/>
                </blockTableStyle>
                <blockTableStyle id="taula_contingut">
                    <blockFont name="Helvetica" size="8"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockAlignment value="RIGHT" start="2,0"/>
                </blockTableStyle>
                <blockTableStyle id="taula_totals">
                    <blockFont name="Helvetica-Bold" size="8"/>
                    <lineStyle kind="GRID" colorName="silver" start="1,0"/>
                    <blockAlignment value="RIGHT" start="1,0"/>
                </blockTableStyle>
                <paraStyle name="text" fontName="Helvetica" fontSize="8"/>
            </stylesheet>
            <story>
                <xsl:apply-templates select="niepi" mode="story"/>
            </story>
        </document>
    </xsl:template>
    <xsl:template match="niepi" mode="story">
        <blockTable colWidths="290mm" style="titol">
            <tr t="1">
                <td t="1">QUALITAT ZONAL</td>
            </tr>
        </blockTable>
        <blockTable colWidths="290mm" style="subtitol">
            <tr t="1">
                <td>NIEPI PER MUNICIPIS (<xsl:value-of select="name"/>)</td>
            </tr>
        </blockTable>
        <spacer length="10"/>
        <blockTable colWidths="72mm,72mm,72mm,72mm" style="taula1">
            <tr t="1">
                <td t="1">Comunitat aut�noma:</td>
                <td/>
                <td t="1">Prov�ncia:</td>
                <td>
                    <xsl:value-of select="provincia"/>
                </td>
            </tr>
        </blockTable>
        <blockTable colWidths="40mm,50mm,20mm,60mm,120mm" style="taula2">
            <tr t="1">
                <td t="1">MUNICIPI</td>
                <td t="1">ZONA</td>
                <td t="1">POT�NCIA</td>
                <td t="1">PROGRAMAT</td>
                <td t="1">IMPREVIST</td>
            </tr>
        </blockTable>
        <blockTable colWidths="40mm,50mm,20mm,25mm,20mm,15mm,22mm,22mm,22mm,17mm,22mm,15mm" style="taula3">
            <tr t="1">
                <td/>
                <td/>
                <td/>
                <td t="1">TRANSPORT</td>
                <td t="1">DISTRIB.</td>
                <td t="1">TOTAL</td>
                <td t="1">GENERACI�</td>
                <td t="1">TRANSPORT</td>
                <td t="1">C. TERCERS</td>
                <td t="1">F. MAJOR</td>
                <td t="1">C. PROPIES</td>
                <td t="1">TOTAL</td>
            </tr>
        </blockTable>
        <blockTable colWidths="40mm,50mm,20mm,25mm,20mm,15mm,22mm,22mm,22mm,17mm,22mm,15mm" style="taula_contingut">
            <xsl:for-each select="municipis">
                <tr>
                    <td>
                        <para style="text">
                            <xsl:value-of select="municipi"/>
                        </para>
                    </td>
                    <td>
                        <xsl:value-of select="zona"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(potencia_installada div 1000, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(pr_transport, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(pr_distribucio, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number((pr_transport) + (pr_distribucio), '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(im_generacio, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(im_transport, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(im_tercers, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(im_major, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(im_propies, '0.0000')"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(im_generacio + im_transport + im_tercers + im_major + im_propies, '0.0000')"/>
                    </td>
                </tr>
            </xsl:for-each>
        </blockTable>
        <!--
        <blockTable colWidths="40mm,50mm,20mm,25mm,20mm,15mm,22mm,22mm,22mm,17mm,22mm,15mm" style="taula_totals">
          <tr>
            <td></td>
            <td t="1">TOTAL</td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/pi) div 1000, '0.0000')"/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/pr_transport) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000') "/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/pr_distribucio) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number((sum(key('municipis-per-provincia', provincia)/pr_transport) + sum(key('municipis-per-provincia', provincia)/pr_distribucio)) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/im_generacio) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/im_transport) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/im_tercers) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/im_major) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number(sum(key('municipis-per-provincia', provincia)/im_propies) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
            <td><xsl:value-of select="format-number((sum(key('municipis-per-provincia', provincia)/im_generacio)+sum(key('municipis-per-provincia', provincia)/im_transport)+sum(key('municipis-per-provincia', provincia)/im_tercers)+sum(key('municipis-per-provincia', provincia)/im_major)+sum(key('municipis-per-provincia', provincia)/im_propies)) div sum(key('municipis-per-provincia', provincia)/pi), '0.0000')"/></td>
          </tr>
        </blockTable>
        -->
        <spacer length="10"/>
    </xsl:template>
</xsl:stylesheet>
