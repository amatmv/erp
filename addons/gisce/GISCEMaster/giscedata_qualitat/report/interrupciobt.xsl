<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="llistat"/>
    </xsl:template>
    <xsl:template match="llistat">
        <document>
            <template pageSize="(19cm, 297mm)" topMargin="1cm" bottomMargin="1cm" rightMargin="1cm">
                <pageTemplate id="main">
                    <frame id="first" x1="1cm" y1="1cm" height="277mm" width="17cm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <blockTableStyle id="titol">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica-Bold" size="20"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="25"/>
                </blockTableStyle>
                <blockTableStyle id="subtitol">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="14"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="19"/>
                </blockTableStyle>
                <blockTableStyle id="taula1">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica-Bold" size="10"/>
                </blockTableStyle>
                <blockTableStyle id="taula2">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" colorName="silver" start="3,0"/>
                    <blockFont name="Helvetica-Bold" size="10" start="0,0" stop="2,0"/>
                    <lineStyle kind="LINEABOVE" colorName="silver" start="0,0" stop="2,0"/>
                    <lineStyle kind="LINEBEFORE" colorName="silver" start="0,0" stop="2,0"/>
                </blockTableStyle>
                <blockTableStyle id="taula3">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="8"/>
                    <lineStyle kind="GRID" colorName="silver" start="0,0"/>
                    <lineStyle kind="LINEBEFORE" colorName="silver" start="0,0" stop="2,0"/>
                </blockTableStyle>
                <blockTableStyle id="taula_contingut">
                    <blockFont name="Helvetica" size="8"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockAlignment value="RIGHT" start="2,0"/>
                </blockTableStyle>
                <paraStyle name="text" fontName="Helvetica" fontSize="8"/>
            </stylesheet>
            <story>
                <xsl:apply-templates select="interrupciobt" mode="story"/>
            </story>
        </document>
    </xsl:template>
    <xsl:template match="interrupciobt" mode="story">
        <blockTable colWidths="160mm" style="titol">
            <tr t="1">
                <td t="1">QUALITAT ZONAL</td>
            </tr>
        </blockTable>
        <blockTable colWidths="160mm" style="subtitol">
            <tr t="1">
                <td>INTERRUPCIONS BT PER MUNICIPIS (<xsl:value-of select="name"/>)</td>
            </tr>
        </blockTable>
        <spacer length="10"/>
        <blockTable colWidths="40mm,40mm,40mm,40mm" style="taula1">
            <tr t="1">
                <td t="1">Comunitat aut�noma:</td>
                <td/>
                <td t="1">Prov�ncia:</td>
                <td>
                    <xsl:value-of select="provincia"/>
                </td>
            </tr>
        </blockTable>
        <blockTable colWidths="50mm,35mm,25mm,25mm,25mm" style="taula3">
            <tr t="1">
                <td t="1">MUNICIPI</td>
                <td t="1">ZONA</td>
                <td t="1">IMPREVISTES</td>
                <td t="1">PROGRAMADES</td>
                <td t="1">TOTALS</td>
            </tr>
        </blockTable>
        <blockTable colWidths="50mm,35mm,25mm,25mm,25mm" style="taula_contingut">
            <xsl:for-each select="municipis">
                <tr>
                    <td>
                        <para style="text">
                            <xsl:value-of select="municipi"/>
                        </para>
                    </td>
                    <td>
                        <xsl:value-of select="zona"/>
                    </td>
                    <td>
                        <xsl:value-of select="n_imprevistes"/>
                    </td>
                    <td>
                        <xsl:value-of select="n_programades"/>
                    </td>
                    <td>
                        <xsl:value-of select="n_total"/>
                    </td>
                </tr>
            </xsl:for-each>
        </blockTable>
        <xsl:if test="not(position() = last())">
            <nextFrame id="first"/>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>
