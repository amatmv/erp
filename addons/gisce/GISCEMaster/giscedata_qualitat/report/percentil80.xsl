<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
    <xsl:template match="/">
        <xsl:apply-templates select="llistat"/>
    </xsl:template>
    <xsl:template match="llistat">
        <document>
            <template>
                <pageTemplate id="main">
                    <frame id="first" x1="1cm" y1="1cm" width="19cm" height="277mm"/>
                </pageTemplate>
            </template>
            <stylesheet>
                <blockTableStyle id="titol">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica-Bold" size="20"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="25"/>
                </blockTableStyle>
                <blockTableStyle id="subtitol">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="14"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="19"/>
                </blockTableStyle>
                <blockTableStyle id="subtitol2">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="12"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockLeading length="19"/>
                </blockTableStyle>
                <blockTableStyle id="taula1">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica-Bold" size="10"/>
                </blockTableStyle>
                <blockTableStyle id="taula2">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="10"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockBackground colorName="grey"/>
                </blockTableStyle>
                <blockTableStyle id="taula3">
                    <blockAlignment value="CENTER"/>
                    <blockFont name="Helvetica" size="8"/>
                </blockTableStyle>
                <blockTableStyle id="taula_contingut">
                    <blockFont name="Helvetica" size="8"/>
                    <lineStyle kind="GRID" colorName="silver"/>
                    <blockAlignment value="RIGHT" start="2,0"/>
                </blockTableStyle>
                <paraStyle name="text" fontName="Helvetica" fontSize="8"/>
            </stylesheet>
            <story>
                <xsl:apply-templates select="percentil" mode="story"/>
            </story>
        </document>
    </xsl:template>
    <xsl:template match="percentil" mode="story">
        <blockTable colWidths="170mm" style="titol">
            <tr t="1">
                <td t="1">QUALITAT ZONAL</td>
            </tr>
        </blockTable>
        <blockTable colWidths="170mm" style="subtitol">
            <tr t="1">
                <td t="1">TIEPI PER MUNICIPIS (<xsl:value-of select="any"/>)</td>
            </tr>
        </blockTable>

        <spacer length="10"/>
        <blockTable colWidths="170mm" style="taula1">
            <tr t="1">
                <td>Provincia de <xsl:value-of select="provincia"/></td>
            </tr>
        </blockTable>
        <blockTable colWidths="57mm,57mm,56mm" style="taula2">
            <tr t="1">
                <td t="1">MUNICIPI</td>
                <td t="1">ZONA</td>
                <td t="1">TIEPI</td>
            </tr>
        </blockTable>
        <blockTable colWidths="57mm,57mm,56mm" style="taula_contingut">
            <xsl:for-each select="municipis">
                <xsl:sort select="nom"/>
                <tr>
                    <td>
                        <xsl:value-of select="municipi"/>
                    </td>
                    <td>
                        <xsl:value-of select="zona"/>
                    </td>
                    <td>
                        <xsl:value-of select="format-number(tiepi, '0.0000')"/>
                    </td>
                </tr>
            </xsl:for-each>
        </blockTable>
        <spacer length="10"/>
        <blockTable colWidths="170mm" style="taula1">
            <tr t="1">
                <td t="1">PERCENTIL 80 DEL TIEPI</td>
            </tr>
        </blockTable>
        <blockTable colWidths="57mm,57mm,56mm" style="taula2">
            <tr t="1">
                <td t="1">MUNICIPI</td>
                <td t="1">ZONA</td>
                <td t="1">TIEPI</td>
            </tr>
        </blockTable>
        <blockTable colWidths="57mm,57mm,56mm" style="taula_contingut">
            <xsl:for-each select="municipis">
                <xsl:if test="percentil=1">
                    <tr>
                        <td>
                            <para style="text">
                                <xsl:value-of select="municipi"/>
                            </para>
                        </td>
                        <td>
                            <xsl:value-of select="zona"/>
                        </td>
                        <td>
                            <xsl:value-of select="format-number(tiepi, '0.0000')"/>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="percentil!=1">
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </blockTable>
    </xsl:template>
</xsl:stylesheet>
