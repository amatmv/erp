# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *
import calendar
from psycopg2.extensions import AsIs


_avis_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Es genererà la potència instal·lada del mes actual." />
</form>"""

_avis_fields = {}

_mes_form = """<?xml version="1.0"?>
<form string="Escollir el mes">
  <field name="date" />
  <field name="year" />
</form>"""

_mes_fields = {
  'date': {'string': 'Mes', 'type': 'selection', 'selection': [(1, "Gener"), (2, "Febrer"), (3, "Març"), (4, "Abril"), (5, "Maig"), (6, "Juny"), (7, "Juliol"), (8, "Agost"), (9, "Setembre"), (10, "Octubre"), (11, "Novembre"), (12, "Desembre")]  },
  'year': {'string': 'Any', 'type': 'char', 'size': 4, 'required': True},
}

def _mes(self, cr, uid, data, context={}):
    return {'date': datetime.now().month, 'year': (datetime.now()).strftime('%Y')}

_cosfi_form = """<?xml version="1.0"?>
<form string="Avís">
  <field name="cosfi" />
</form>"""

_cosfi_fields = {
  'cosfi': {'type': 'float', 'string': 'Cosfi', 'size': 60},
}


def _calc(self, cr, uid, data, context={}):
    mes = data['form']['date']
    pool = pooler.get_pool(cr.dbname)
    year = data['form']['year']
    dia = calendar.monthrange(int(year), int(mes))[1]
    cr.execute("select id from giscedata_qualitat_install_power_data where name = %s", ('%s-%s-%s 00:00:00' % (year, mes, dia),))
    if len(cr.fetchall()):
        cr.execute("delete from giscedata_qualitat_install_power_data where name = %s", ('%s-%s-%s 00:00:00' % (year, mes, dia),))
        cr.commit()
    id = pool.get('giscedata.qualitat.install_power.data').create(cr, uid, {'name': '%s-%s-%s 00:00:00' % (year, mes, dia), 'cosfi': data['form']['cosfi']})
    cr.execute(
        """
            select %s as install_power_data_id, 
                'T' as type,t.name,
                ct.name as code_ct,
                ct.id_municipi as codeine,
                t.potencia_nominal as power, 
                ct.zona_id as zone_ct_id 
            from 
                giscedata_transformador_trafo t, 
                giscedata_cts ct, 
                giscedata_transformador_estat e 
            where 
                t.id_estat = e.id and 
                e.codi =1 and 
                t.ct = ct.id and 
                t.tiepi = True
        """, (AsIs(id), ))
    for trafo in cr.dictfetchall():
        pool.get('giscedata.qualitat.install_power').create(cr, uid, trafo)
    cr.execute("""
    select 
        'C' as type,e.name, 
        p.potencia::float as power, 
        cups.et as code_ct, 
        cups.id_municipi as codeine 
    from 
        giscedata_polissa p, 
        giscedata_cups_ps cups, 
        giscedata_cups_blockname block, 
        giscedata_cups_escomesa e 
    where 
        p.cups = cups.id and 
        cups.id_escomesa = e.id and 
        e.blockname = block.id and 
        block.code = 1 and 
        p.state not in ('esborrany', 'pendent', 'baixa')
        """)
    # Ara completem les dades que ens falten:
    #  - el nom correcte del CT i la seva descripció
    #  - la zona del ct
    #  - dividir per cosfi
    for conta_at in cr.dictfetchall():
        if not None in conta_at.values():
            cr.execute("select name,descripcio,zona_id from giscedata_cts where name = %s", (conta_at['code_ct'],))
            ct = cr.dictfetchone()
            if ct:
                conta_at['code_ct'] = ct['name']
                conta_at['zone_ct_id'] = ct['zona_id']
                conta_at['install_power_data_id'] = id
                pool.get('giscedata.qualitat.install_power').create(cr, uid, conta_at)
        else:
            if data.has_key('error_None'):
                data['error_None'] += '\n%s' % (conta_at)
            else:
                data['error_None'] = '%s' % (conta_at)
    return {}


def _errors(self, cr, uid, data, context={}):
    if data.has_key('error_None'):
        return 'error'
    else:
        return 'end'

def _error(self, cr, uid, data, context={}):
    return {'error': data['error_None']}

_error_form = """<?xml version="1.0"?>
<form string="Error">
  <label colspan="4" string="Hi ha algun valor que no és correcte, no s'ha tingut en compte a l'hora de calcular la potència instal·lada" />
  <field name="error" nolabel="1"/>
</form>"""

_error_fields = {
  'error': {'type': 'text', 'string': 'Error'},
}

class wizard_qualitat_potencia_installada(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('mes', 'Continuar', 'gtk-go-forward')]}
      },
      'mes': {
        'actions': [_mes],
        'result': {'type': 'form', 'arch': _mes_form,'fields': _mes_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('cosfi', 'Continuar', 'gtk-go-forward')]}
      },
      'cosfi': {
          'actions': [],
        'result': {'type': 'form', 'arch': _cosfi_form,'fields': _cosfi_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
          'actions': [_calc],
          'result': { 'type' : 'state', 'state' : 'pre_end' },
      },
      'pre_end': {
        'actions': [],
        'result': {'type': 'choice', 'next_state': _errors}
      },
      'error': {
          'actions': [_error],
        'result': {'type': 'form', 'arch': _error_form,'fields': _error_fields, 'state':[('end', 'Tancar', 'gtk-close')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'}
      },
    }

wizard_qualitat_potencia_installada('giscedata.qualitat.potencia.installada')
