# -*- coding: utf-8 -*-
try:
    from collections import OrderedDict
except:
    from ordereddict import OrderedDict


from osv import osv, fields
from tools import config


class WizardQualitatNiepiQualitatIndi(osv.osv_memory):

    _name = 'wizard.qualitat.niepi.qualitat.indi'

    def action_qualitat_niepi_qualitat_indi(self, cursor, uid, ids,
                                            context=None):

        wizard = self.browse(cursor, uid, ids[0])
        obj_niepi = self.pool.get('giscedata.qualitat.individual.niepi')
        obj_niepilinia = self.pool.get('giscedata.qualitat.individual.niepi.'
                                       'linia')
        span_niepi_obj = self.pool.get(
            'giscedata.qualitat.individual.niepi.span')

        any_niepi = 0
        if wizard.year.isdigit():
            any_niepi = wizard.year
        else:
            wizard.write({'state': 'error'})

        #Buscar id de l'any
        id_any_niepi = obj_niepi.search(cursor, uid, [('name', '=',
                                                       any_niepi)])

        #Borrar la qualitat a la bbdd, si existeix borrar
        #fer amb objecte
        if id_any_niepi:
            obj_niepi.unlink(cursor, uid, id_any_niepi)

        #Crear les noves entrades de Qualitat Tiepi
        #Creo l'objecte de qualitatinditiepi amb l'any del wizard
        objt_id = obj_niepi.create(cursor, uid, {'name': any_niepi})

        #Query amb els camps que necessitem
        query_file = ('%s/giscedata_qualitat/wizard/sqls/niepi_qualitat.sql'
                      % config['addons_path'])
        query = open(query_file).read()

        anyinici = '%s-01-01' % any_niepi
        anyfinal = '%s-01-01' % (int(any_niepi) + 1)

        cursor.execute(query, (anyinici, anyfinal))

        res = cursor.fetchall()
        if not res:
            raise osv.except_osv(
                'warning',
                u"No hi ha incidències en l'any seleccionat: %s" % anyinici
            )
        else:
            #Per cada linia
            linies = OrderedDict()
            for linia in res:
                key = '%s_%s_%s_%s' % (linia[0], linia[1], linia[3], linia[2])
                try:
                    linia_id = linies[key]['id']
                except KeyError:
                    try:
                        old_key = next(reversed(linies))
                    except StopIteration:
                        old_key = False
                    linia_id = obj_niepilinia.create(cursor, uid, {
                        'year': objt_id,
                        'cups': linia[0],
                        'polissa': linia[1],
                        'tarifa': linia[2],
                        'zona': linia[3],
                        'limit_talls_zona': linia[4],
                        'tipus_tall': linia[5],
                        'numero_total_inter': 0,
                    })
                    linies.update({key: {'id': linia_id, 'total': 0}})
                    if old_key and key != old_key:
                        old_linia = linies[old_key]
                        total = old_linia['total']
                        old_linia_id = old_linia['id']
                        if total == 0:
                            obj_niepilinia.unlink(cursor, uid, old_linia_id)
                        else:
                            obj_niepilinia.write(cursor, uid, [old_linia_id],
                                                 {'numero_total_inter': total})

                if linia[7] != 1 and linia[9] == 7:
                    dret_descompte = 1
                else:
                    dret_descompte = 0

                vals = {
                    'linia': linia_id,
                    'nom_incidencia': linia[12],
                    'tipus_tall': linia[5],
                    'tipus_code': linia[7],
                    'causa_nom': linia[8],
                    'causa_code': linia[9],
                    'data_inici': linia[10],
                    'data_final': linia[11],
                    'duracio': linia[6],
                    'dret_descompte': dret_descompte
                }
                span_niepi_obj.create(cursor, uid, vals)
                if vals['tipus_code'] != 1:
                    linies[key]['total'] = linies[key]['total'] + 1
            # Actualitzem l'úlitma entrada
            try:
                old_key = next(reversed(linies))
            except StopIteration:
                old_key = False
            if old_key:
                old_linia = linies[old_key]
                total = old_linia['total']
                old_linia_id = old_linia['id']
                if total == 0:
                    obj_niepilinia.unlink(cursor, uid, old_linia_id)
                else:
                    obj_niepilinia.write(cursor, uid, [old_linia_id],
                                         {'numero_total_inter': total})
            wizard.write({'state': 'end'})

            return {'domain': [('id', '=', objt_id)],
                    'name': 'NIEPI Qualitat',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'giscedata.qualitat.individual.niepi',
                    'type': 'ir.actions.act_window',
                    'view_id': False
                    }

    _columns = {
        'year': fields.char('Any', size='10', required=True),
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End'), ],
                                  'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardQualitatNiepiQualitatIndi()