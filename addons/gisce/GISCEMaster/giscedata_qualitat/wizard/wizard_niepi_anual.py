# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *

_avis_form =  """<?xml version="1.0"?>
<form string="Avís">
  <label string="Per generar el NIEPI anual s'han d'haver generat tots els NIEPIs menuals, si ja estan generats premi 'Continuar', sino els haurà de generar abans de tornar a executar aquest assistent." colspan="2"/>
</form>"""

_avis_fields = {}


def _any_selection(self, cr, uid, context={}):
    cr.execute("select distinct to_char(name, 'YYYY') as year from giscedata_qualitat_install_power_data order by year desc")
    return [(a[0], a[0]) for a in cr.fetchall()]

_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any" colspan="2"/>
  <newline />
  <field name="any" required="1"/>
</form>"""

_any_fields = {
  'any': {'string': 'Any', 'type':'selection', 'selection': _any_selection },
}


def _calc(self, cr, uid, data, context={}):

    # Busquem totes les provincies
    cr.execute("""
  select distinct
          s.id,
          s.name as provincia
  from
          giscedata_qualitat_install_power p,
          giscedata_qualitat_install_power_data d,
          res_municipi m,
          res_country_state s
  where
          p.install_power_data_id = d.id
          and p.codeine = m.id
          and m.state = s.id
          and to_char(d.name, 'YYYY') = %s
    """, (data['form']['any'],))
    # Borrem les entrades del mes que volem escriure
    for provincia in cr.dictfetchall():

        # Borrem les entrades del mes que volem escriure
        cr.execute("delete from giscedata_qualitat_niepi_anual where name = %s and provincia = %s", (data['form']['any'], int(provincia['id'])))
        cr.commit()

        niepis = []
        # Creem el nou
        q_prov = pooler.get_pool(cr.dbname).get('giscedata.qualitat.niepi.anual')
        q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.niepi.anual.municipi')
        id_q = q_prov.create(cr, uid, {'name': data['form']['any'], 'provincia': provincia['id']})
        niepis.append(id_q)
        cr.execute("""
    select
            nmm.municipi as municipi,
            nmm.zona as zona,
            sum(nmm.pr_transport) as pr_transport,
            sum(nmm.pr_distribucio) as pr_distribucio,
            sum(nmm.im_generacio) as im_generacio,
            sum(nmm.im_transport) as im_transport,
            sum(nmm.im_tercers) as im_tercers,
            sum(nmm.im_major) as im_major,
            sum(nmm.im_propies) as im_propies
    from
            giscedata_qualitat_niepi_mensual_municipi nmm,
            giscedata_qualitat_niepi_mensual nm
    where
            nmm.niepi_id = nm.id
            and nm.provincia = %s
            and substring(nm.name from 4 for 4) = %s
    group by
            nmm.municipi,
            nmm.zona""", (provincia['id'], data['form']['any']))
        for niepi in cr.dictfetchall():
            cr.execute("""
      select
        sum(
          case
            when pi.type = 'C'
              then pi.power/d.cosfi
            when pi.type = 'T'
              then pi.power
          end
        ) as power
      from
        giscedata_qualitat_install_power pi,
        giscedata_qualitat_install_power_data d
      where
        pi.install_power_data_id = d.id
        and to_char(d.name, 'YYYY') = %s
        and  pi.codeine = %s
        and pi.zone_ct_id = %s
      group by
        d.name
      order by
        d.name desc""", (data['form']['any'], niepi['municipi'], niepi['zona']))

            pi = float("%.4f" % cr.fetchone()[0])

            niepi.update({'niepi_id': id_q})
            niepi.update({'potencia_installada': pi})
            q_obj.create(cr, uid, niepi)



    data['print_ids'] = niepis
    return {}

def _mostrar_tab(self, cr, uid, data, context={}):
    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, data['print_ids'])))+"])]",
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.qualitat.niepi.anual',
      'name': 'NIEPI Anual %s' % (data['form']['any']),
                  'view_id': False,
      'limit': len(data['print_ids']),
                  'type': 'ir.actions.act_window',
      'res_id': False,
      'auto_refresh': False,
    }
    return action


def _pre_end(self, cr, uid, data, context={}):
    return {}



class wizard_qualitat_niepi_anual(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('any', 'Continuar', 'gtk-go-forward')]}
      },
      'any': {
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
          'actions': [_calc],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
      },
      'pre_end': {
          'actions': [_pre_end],
          'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_qualitat_niepi_anual('giscedata.qualitat.niepi.anual')
