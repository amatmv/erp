# -*- coding: utf-8 -*-

import wizard
import pooler

_avis_form =  """<?xml version="1.0"?>
<form string="Avís">
  <label string="S'intentarà buscar l'id del CT segons el codi, pot ser que si s'han renombrat CTS no es pugui trobar el codi" colspan="2"/>
</form>"""

_avis_fields = {}


def _any_selection(self, cr, uid, context={}):
    cr.execute("select distinct to_char(end_date, 'YYYY') as year from giscedata_qualitat_span order by year desc")
    return [(a[0], a[0]) for a in cr.fetchall()]

_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any" colspan="2"/>
  <newline />
  <field name="date" required="1"/>
</form>"""

_any_fields = {
  'date': {'string': 'Any', 'type':'selection', 'selection': _any_selection },
}


def _calc(self, cr, uid, data, context={}):
    # Busquem tots els CTS
    n_updated = 0
    n_no_updated = 0
    cts_obj = pooler.get_pool(cr.dbname).get('giscedata.cts')
    cts_ids = cts_obj.search(cr, uid, [], context={'active_test': False})
    cts = {}
    if cts_ids:
        for ct in cts_obj.read(cr, uid, cts_ids, ['id', 'name']):
            cts[ct['name']] = ct['id']

    # Busquem els customers que estiguin en aquest any i que ct_id sigui null
    search_params = [
      ('ct_id', '=', False),
      ('span_id.end_date', '>=', '%s-01-01' % data['form']['date']),
      ('span_id.end_date', '<=', '%s-12-31' % data['form']['date']),
    ]

    customer_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.affected_customer')
    customer_ids = customer_obj.search(cr, uid, search_params)
    # Creem un diccionari on la clau és el codi del ct i conté tots els customer_ids que
    # s'han d'actualitzar fent servir després el diccionari cts[codi_ct] = ct_id
    updates = {}
    if customer_ids:
        for customer in customer_obj.read(cr, uid, customer_ids, ['id', 'code_ct']):
            code_ct = customer['code_ct']
            customer_id = customer['id']
            if not updates.has_key(code_ct):
                updates[code_ct] = []
            updates[code_ct] += [customer_id]

    # Actualitzem els CTS que coincideixin segons els codis
    for code_ct,customer_ids in updates.items():
        if cts.has_key(code_ct):
            vals = {'ct_id': cts[code_ct]}
            customer_obj.write(cr, uid, customer_ids, vals)
            n_updated += len(customer_ids)
        else:
            n_no_updated += len(customer_ids)

    return {'n_updated': n_updated, 'n_no_updated': n_no_updated}

_calc_form = """<?xml version="1.0"?>
<form string="Resultado">
  <label string="Resultat d'actualització de Ids de CT segons el codi" colspan="2" />
  <newline />
  <field name="n_updated" readonly="1" />
  <newline />
  <field name="n_no_updated" readonly="1" />
</form>"""

_calc_fields = {
  'n_updated': {'string': 'Actualitzats', 'type':'integer'},
  'n_no_updated': {'string': 'No actualitzats', 'type': 'integer'}
}


def _pre_end(self, cr, uid, data, context={}):
    return {}



class wizard_qualitat_update_ctid(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('any', 'Continuar', 'gtk-go-forward')]}
      },
      'any': {
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
        'actions': [_calc],
        'result': {'type': 'form', 'arch': _calc_form, 'fields': _calc_fields, 'state': [('pre_end', 'Ok', 'gtk-ok')]}
      },
      'pre_end': {
        'actions': [_pre_end],
        'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_qualitat_update_ctid('giscedata.qualitat.update.ctid')
