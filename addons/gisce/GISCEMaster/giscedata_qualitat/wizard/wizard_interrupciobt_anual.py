# -*- coding: utf-8 -*-

import wizard
import pooler

_avis_form =  """<?xml version="1.0"?>
<form string="Avís">
  <label string="Per generar les Interrupcions BT anuals s'han d'haver generat totes les Interrupcions BT menuals, si ja estan generats premi 'Continuar', sino els haurà de generar abans de tornar a executar aquest assistent." colspan="2"/>
</form>"""

_avis_fields = {}


def _any_selection(self, cr, uid, context={}):
    cr.execute("select distinct substring(name from 4 for 4) as year from giscedata_qualitat_interrupciobt_mensual order by year desc")
    return [(a[0], a[0]) for a in cr.fetchall()]

_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any" colspan="2"/>
  <newline />
  <field name="date" required="1"/>
</form>"""

_any_fields = {
  'date': {'string': 'Any', 'type':'selection', 'selection': _any_selection },
}


def _calc(self, cr, uid, data, context={}):

    # Array que utilitzarem per mostrar els models que hem creat a un nou tab.
    print_ids = []
    q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.interrupciobt.anual')

    # Busquem totes les provincies
    cr.execute("""
  select distinct
    s.id,
    s.name as provincia
  from
    giscedata_qualitat_install_power p,
    giscedata_qualitat_install_power_data d,
    res_municipi m,
    res_country_state s
  where
    p.install_power_data_id = d.id
    and p.codeine = m.id
    and m.state = s.id
    and to_char(d.name, 'YYYY') = %s
    """, (data['form']['date'],))
    # Borrem les entrades del mes que volem escriure
    for provincia in cr.dictfetchall():
        # Eliminem si ja existeix
        search_params = [
          ('name', '=', data['form']['date']),
          ('provincia', '=', provincia['id'])
        ]
        del_ids = q_obj.search(cr, uid, search_params)
        if del_ids:
            q_obj.unlink(cr, 1, del_ids)
        # Creem el nou
        q_data = {'name': data['form']['date'], 'provincia': provincia['id']}
        id_q = q_obj.create(cr, uid, q_data)
        print_ids.append(id_q)

        # Ara busquem totes les interrupcions BT mensuals i fem la suma
        cr.execute("""SELECT
      q.provincia as provincia,
      q_municipi.zona as zona,
      q_municipi.municipi as municipi,
      SUM(q_municipi.n_imprevistes) as n_imprevistes,
      SUM(q_municipi.n_programades) as n_programades,
      SUM(q_municipi.n_total) as n_total
    FROM
      giscedata_qualitat_interrupciobt_mensual_municipi q_municipi,
      giscedata_qualitat_interrupciobt_mensual q
    WHERE
      q_municipi.interrupciobt_id = q.id
      AND q.provincia = %s
      AND substring(q.name from 4 for 4) = %s
    GROUP BY
      provincia,
      zona,
      municipi""", (provincia['id'], data['form']['date']))
        poblacions = cr.dictfetchall()
        q_municipi_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.interrupciobt.anual.municipi')
        for poble in poblacions:
            # Tenim el mateix noms en les columnes que ens retorna la consulta que els camps
            # de la base de dades, només falta eliminar les columnes provincia
            # ja que en el model de la bbdd es troba a giscedata.qualitat.interrupciobt.anual
            # i assignar-hi l'id del periode
            vals = poble.copy()
            del vals['provincia']
            vals['interrupciobt_id'] = id_q
            q_municipi_obj.create(cr, uid, vals)

    data['print_ids'] = print_ids[:]
    return {}


    return {}

def _mostrar_tab(self, cr, uid, data, context={}):
    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, data['print_ids'])))+"])]",
      'view_type': 'form',
      'view_mode': 'tree,form',
      'res_model': 'giscedata.qualitat.interrupciobt.anual',
      'name': 'Interrupcions BT Anuals %s' % (data['form']['date']),
      'view_id': False,
      'limit': len(data['print_ids']),
      'type': 'ir.actions.act_window',
      'res_id': False,
      'auto_refresh': False,
    }
    return action


def _pre_end(self, cr, uid, data, context={}):
    return {}



class wizard_qualitat_interrupciobt_anual(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('any', 'Continuar', 'gtk-go-forward')]}
      },
      'any': {
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
        'actions': [_calc],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
      },
      'pre_end': {
        'actions': [_pre_end],
        'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_qualitat_interrupciobt_anual('giscedata.qualitat.interrupciobt.anual')
