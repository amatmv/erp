# -*- coding: iso-8859-1 -*-
import wizard
import pooler
import math
from datetime import *

_avis_form =  """<?xml version="1.0"?>
<form string="Avís">
  <label string="Per generar el PERCENTIL80 anual s'han d'haver generat el TIEPI anual, si ja està generat premi 'Continuar', sino l'haurà de generar abans de tornar a executar aquest assistent." colspan="2"/>
</form>"""

_avis_fields = {}

def _any_selection(self, cr, uid, context={}):
    cr.execute("select distinct to_char(begin_date, 'YYYY') as year from giscedata_qualitat_span order by year desc")
    return [(a[0], a[0]) for a in cr.fetchall()]

_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any" colspan="2"/>
  <newline />
  <field name="any" required="1"/>
</form>"""

_any_fields = {
  'any': {'string': 'Any', 'type':'selection', 'selection': _any_selection },
}


def _calc(self, cr, uid, data, context={}):

    # Busquem totes les provincies
    cr.execute("""
  select distinct
          s.id,
          s.name as provincia
  from
          giscedata_qualitat_install_power p,
          giscedata_qualitat_install_power_data d,
          res_municipi m,
          res_country_state s
  where
          p.install_power_data_id = d.id
          and p.codeine = m.id
          and m.state = s.id
          and to_char(d.name, 'YYYY') = %s
    """, (data['form']['any'],))

    for provincia in cr.dictfetchall():
        # Borrem les entrades del mes que volem escriure
        cr.execute("delete from giscedata_qualitat_percentil80 where name = %s and provincia = %s", (data['form']['any'], int(provincia['id'])))
        cr.commit()

        percentils = []
        # Creem el nou

        q_prov = pooler.get_pool(cr.dbname).get('giscedata.qualitat.percentil80')
        id_q = q_prov.create(cr, uid, {'name': data['form']['any'], 'provincia': provincia['id']})
        q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.percentil80.municipi')
        percentils.append(id_q)

        cr.execute("""
    select
            tam.municipi as municipi,
            tam.zona as zona,
            ( tam.im_generacio
            + tam.im_transport
            + tam.im_tercers
            + tam.im_major
            + tam.im_propies
            ) as tiepi
    from
            giscedata_qualitat_tiepi_anual_municipi tam,
            giscedata_qualitat_tiepi_anual ta
    where
            tam.tiepi_id = ta.id
            and ta.provincia = %s
            and ta.name = %s""", (provincia['id'], data['form']['any']))
        for tiepi in cr.dictfetchall():
            tiepi.update({'percentil_id': id_q})
            q_obj.create(cr, uid, tiepi)

    data['print_ids'] = percentils
    return {}

def _percentil(self, cr, uid, data, context={}):

    q_prov = pooler.get_pool(cr.dbname).get('giscedata.qualitat.percentil80')
    q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.percentil80.municipi')
    for percentil80 in q_prov.browse(cr, uid, data['print_ids']):
        cr.execute("select distinct zona from giscedata_qualitat_percentil80_municipi where percentil_id = %s", (int(percentil80.id),))
        for zona in cr.dictfetchall():
            cr.execute("select id,tiepi from giscedata_qualitat_percentil80_municipi where zona = %s and percentil_id = %s order by tiepi asc", (int(zona['zona']), int(percentil80.id)))
            pobles = cr.dictfetchall()
            pos = int(math.ceil(float(80/float(100/len(pobles))))) - 1
            q_obj.write(cr, uid, [pobles[pos]['id']], {'percentil80': True})
    return {}


def _mostrar_tab(self, cr, uid, data, context={}):
    action = {
      'domain': "[('id','in', ["+','.join(map(str,data['print_ids']))+"])]",
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.qualitat.percentil80',
      'name': 'Percentil 80 %s' % (data['form']['any']),
                  'view_id': False,
      'limit': len(data['print_ids']),
                  'type': 'ir.actions.act_window',
      'res_id': False,
      'auto_refresh': False,
    }
    return action



class wizard_qualitat_percentil80(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('any', 'Continuar', 'gtk-go-forward')]}
      },
      'any': {
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
          'actions': [_calc],
          'result': { 'type' : 'state', 'state' : 'percentil' },
      },
      'percentil': {
          'actions': [_percentil],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'end'}
      },
    }

wizard_qualitat_percentil80('giscedata.qualitat.percentil80')
