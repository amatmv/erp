# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *

_avis_form =  """<?xml version="1.0"?>
<form string="Avís">
  <label string="Per generar el TIEPI anual s'han d'haver generat tots els TIEPIs menuals, si ja estan generats premi 'Continuar', sino els haurà de generar abans de tornar a executar aquest assistent." colspan="2"/>
</form>"""

_avis_fields = {}


def _any_selection(self, cr, uid, context={}):
    cr.execute("select distinct to_char(name, 'YYYY') as year from giscedata_qualitat_install_power_data order by year desc")
    return [(a[0], a[0]) for a in cr.fetchall()]

_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any" colspan="2"/>
  <newline />
  <field name="any" required="1"/>
</form>"""

_any_fields = {
  'any': {'string': 'Any', 'type':'selection', 'selection': _any_selection },
}


def _calc(self, cr, uid, data, context={}):

    # Busquem totes les provincies
    cr.execute("""
  select distinct
          s.id,
          s.name as provincia
  from
          giscedata_qualitat_install_power p,
          giscedata_qualitat_install_power_data d,
          res_municipi m,
          res_country_state s
  where
          p.install_power_data_id = d.id
          and p.codeine = m.id
          and m.state = s.id
          and to_char(d.name, 'YYYY') = %s
    """, (data['form']['any'],))
    # Borrem les entrades del mes que volem escriure
    for provincia in cr.dictfetchall():

        # Borrem les entrades del mes que volem escriure
        cr.execute("delete from giscedata_qualitat_tiepi_anual where name = %s and provincia = %s", (data['form']['any'], int(provincia['id'])))
        cr.commit()

        tiepis = []
        # Creem el nou
        q_prov = pooler.get_pool(cr.dbname).get('giscedata.qualitat.tiepi.anual')
        q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.tiepi.anual.municipi')
        id_q = q_prov.create(cr, uid, {'name': data['form']['any'], 'provincia': provincia['id']})
        tiepis.append(id_q)
        cr.execute("""
    select
            tmm.municipi as municipi,
            tmm.zona as zona,
            sum(tmm.pr_transport) as pr_transport,
            sum(tmm.pr_distribucio) as pr_distribucio,
            sum(tmm.im_generacio) as im_generacio,
            sum(tmm.im_transport) as im_transport,
            sum(tmm.im_tercers) as im_tercers,
            sum(tmm.im_major) as im_major,
            sum(tmm.im_propies) as im_propies
    from
            giscedata_qualitat_tiepi_mensual_municipi tmm,
            giscedata_qualitat_tiepi_mensual tm
    where
            tmm.tiepi_id = tm.id
            and tm.provincia = %s
            and substring(tm.name from 4 for 4) = %s
    group by
            tmm.municipi,
            tmm.zona""", (provincia['id'], data['form']['any']))
        for tiepi in cr.dictfetchall():
            cr.execute("""
      select
        sum(
          case
            when pi.type = 'C'
              then pi.power/d.cosfi
            when pi.type = 'T'
              then pi.power
          end
        ) as power
      from
        giscedata_qualitat_install_power pi,
        giscedata_qualitat_install_power_data d
      where
        pi.install_power_data_id = d.id
        and to_char(d.name, 'YYYY') = %s
        and  pi.codeine = %s
        and pi.zone_ct_id = %s
      group by
        d.name
      order by
        d.name desc""", (data['form']['any'], tiepi['municipi'], tiepi['zona']))

            pi = float("%.4f" % cr.fetchone()[0])

            tiepi.update({'tiepi_id': id_q})
            tiepi.update({'potencia_installada': pi})
            q_obj.create(cr, uid, tiepi)



    data['print_ids'] = tiepis
    return {}

def _mostrar_tab(self, cr, uid, data, context={}):
    action = {
      'domain': "[('id','in', ["+','.join(map(str,map(int, data['print_ids'])))+"])]",
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.qualitat.tiepi.anual',
      'name': 'TIEPI Anual %s' % (data['form']['any']),
                  'view_id': False,
      'limit': len(data['print_ids']),
                  'type': 'ir.actions.act_window',
      'res_id': False,
      'auto_refresh': False,
    }
    return action


def _pre_end(self, cr, uid, data, context={}):
    return {}



class wizard_qualitat_tiepi_anual(wizard.interface):

    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _avis_form,'fields': _avis_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('any', 'Continuar', 'gtk-go-forward')]}
      },
      'any': {
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
          'actions': [_calc],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
      },
      'pre_end': {
          'actions': [_pre_end],
          'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_qualitat_tiepi_anual('giscedata.qualitat.tiepi.anual')
