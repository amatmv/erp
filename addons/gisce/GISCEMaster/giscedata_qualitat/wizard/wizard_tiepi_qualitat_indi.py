# -*- coding: utf-8 -*-
try:
    from collections import OrderedDict
except:
    from ordereddict import OrderedDict


from osv import osv, fields
from tools import config


class WizardQualitatTiepiQualitatIndi(osv.osv_memory):

    _name = 'wizard.qualitat.tiepi.qualitat.indi'

    def action_qualitat_tiepi_qualitat_indi(self, cursor, uid, ids,
                                            context=None):

        wizard = self.browse(cursor, uid, ids[0])
        obj_tiepi = self.pool.get('giscedata.qualitat.individual.tiepi')
        obj_tiepilinia = self.pool.get(
            'giscedata.qualitat.individual.tiepi.linia')
        span_tiepi_obj = self.pool.get(
            'giscedata.qualitat.individual.tiepi.span')

        any_tiepi = 0
        if wizard.year.isdigit():
            any_tiepi = wizard.year
        else:
            wizard.write({'state': 'error'})

        #Buscar id de l'any
        id_any_tiepi = obj_tiepi.search(cursor, uid, [('name', '=',
                                                       any_tiepi)])

        #Borrar la qualitat a la bbdd, si existeix borrar
        #fer amb objecte
        if id_any_tiepi:
            obj_tiepi.unlink(cursor, uid, id_any_tiepi)

        #Crear les noves entrades de Qualitat Tiepi
        #Creo l'objecte de qualitatinditiepi amb l'any del wizard
        objt_id = obj_tiepi.create(cursor, uid, {'name': any_tiepi})

        #Query amb els camps que necessitem
        query_file = ('%s/giscedata_qualitat/wizard/sqls/tiepi_qualitat.sql'
                      % config['addons_path'])
        query = open(query_file).read()

        anyinici = '%s-01-01' % any_tiepi
        anyfinal = '%s-01-01' % (int(any_tiepi) + 1)

        cursor.execute(query, (anyinici, anyfinal))

        res = cursor.fetchall()
        if not res:
            raise osv.except_osv(
                'warning',
                u"No hi ha incidències en l'any seleccionat: %s" % anyinici
            )
        else:
            linies = OrderedDict()
            #Per cada linia
            for linia in res:
                key = '%s_%s_%s_%s' % (linia[0], linia[1], linia[3], linia[2])
                try:
                    linia_id = linies[key]['id']
                except KeyError:
                    try:
                        old_key = next(reversed(linies))
                    except StopIteration:
                        old_key = False
                    linia_id = obj_tiepilinia.create(cursor, uid, {
                        'year': objt_id,
                        'cups': linia[0],
                        'polissa': linia[1],
                        'tarifa': linia[2],
                        'zona': linia[3],
                        'limit_temps_zona': linia[4],
                        'tipus_tall': linia[5],
                        'temps_total_inter': 0,
                    })
                    linies.update({key: {'id': linia_id, 'sum': 0}})
                    if old_key and key != old_key:
                        old_linia = linies[old_key]
                        suma = old_linia['sum']
                        old_linia_id = old_linia['id']
                        if suma == 0:
                            obj_tiepilinia.unlink(cursor, uid, old_linia_id)
                        else:
                            obj_tiepilinia.write(cursor, uid, [old_linia_id],
                                                 {'temps_total_inter': suma})

                if linia[7] != 1 and linia[9] == 7:
                    dret_descompte = 1
                else:
                    dret_descompte = 0

                vals = {
                    'linia': linia_id,
                    'nom_incidencia': linia[12],
                    'tipus_tall': linia[5],
                    'tipus_code': linia[7],
                    'causa_nom': linia[8],
                    'causa_code': linia[9],
                    'data_inici': linia[10],
                    'data_final': linia[11],
                    'duracio': linia[6],
                    'dret_descompte': dret_descompte
                }
                span_tiepi_obj.create(cursor, uid, vals)
                if vals['tipus_code'] != 1:
                    linies[key]['sum'] = linies[key]['sum'] + vals['duracio']
            # Actualitzem l'úlitma entrada
            try:
                old_key = next(reversed(linies))
            except StopIteration:
                old_key = False
            if old_key:
                old_linia = linies[old_key]
                suma = old_linia['sum']
                old_linia_id = old_linia['id']
                if suma == 0:
                    obj_tiepilinia.unlink(cursor, uid, old_linia_id)
                else:
                    obj_tiepilinia.write(cursor, uid, [old_linia_id],
                                         {'temps_total_inter': suma})

            wizard.write({'state': 'end'})

            return {'domain': [('id', '=', objt_id)],
                    'name': 'TIEPI Qualitat',
                    'view_type': 'form',
                    'view_mode': 'tree,form',
                    'res_model': 'giscedata.qualitat.individual.tiepi',
                    'type': 'ir.actions.act_window',
                    'view_id': False
                    }

    _columns = {
        'year': fields.char('Any', size='10', required=True),
        'state': fields.selection([('init', 'Init'),
                                   ('error', 'Error'),
                                   ('end', 'End'), ],
                                  'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardQualitatTiepiQualitatIndi()