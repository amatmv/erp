# -*- coding: iso-8859-1 -*-
import wizard
import pooler
from datetime import *

def _date_selection(self, cr, uid, context={}):
    cr.execute("select distinct to_char(begin_date, 'MM/YYYY'), to_char(begin_date, 'YYYYMM') as ts from giscedata_qualitat_span order by ts desc")
    return [(m[0], m[0]) for m in cr.fetchall()]


_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any i el mes" colspan="2"/>
  <field name="date" required="1"/>
</form>"""

_any_fields = {
  'date': {'string': 'Mes', 'type': 'selection', 'selection': _date_selection},
}

def _calc(self, cr, uid, data, context={}):

    # Busquem totes les provincies
    cr.execute("""
  select distinct
          s.id,
          s.name as provincia
  from
          giscedata_qualitat_install_power p,
          giscedata_qualitat_install_power_data d,
          res_municipi m,
          res_country_state s
  where
          p.install_power_data_id = d.id
          and p.codeine = m.id
          and m.state = s.id
          and to_char(d.name, 'MM/YYYY') = %s
    """, (data['form']['date'],))
    # Borrem les entrades del mes que volem escriure
    niepis = []
    for provincia in cr.dictfetchall():

        # Borrem les entrades del mes que volem escriure
        cr.execute("delete from giscedata_qualitat_niepi_mensual where name = %s and provincia = %s", (data['form']['date'], int(provincia['id'])))
        cr.commit()

        # Creem el nou
        q_prov = pooler.get_pool(cr.dbname).get('giscedata.qualitat.niepi.mensual')
        id_q = q_prov.create(cr, uid, {'name': data['form']['date'], 'provincia': provincia['id']})
        niepis.append(id_q)

        # Consulta per numerador (mes info a niepi_mensual.sql)
        cr.execute("""
      select
        data,
        municipi,
        codeine,
        zona,
        zona_id,
        sum(power) as pot_affect,
        cause
      from (
        select distinct
          i.id as incidence_id,
          inst.name as installation_id,
          case
            when inst.type = 'C'
              then inst.power/(
                select
                  cosfi
                from
                  giscedata_qualitat_install_power_data
                where
                  to_char(name, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')
                )
            when inst.type = 'T'
              then inst.power
          end as power,
          m.name as municipi,
          m.id as codeine,
          z.name as zona,
          z.id as zona_id,
          c.code as cause,
          to_char(s.begin_date, 'YYYY-12-01') as data
        from
          giscedata_qualitat_incidence i,
          giscedata_qualitat_span s,
          giscedata_qualitat_affected_installation inst,
          res_municipi m,
          giscedata_cts_zona z,
          giscedata_qualitat_cause c
        where
          m.state = %s
          and c.id = i.cause_id
          and z.id = inst.zone_ct_id
          and m.id = inst.codeine
          and inst.span_id = s.id
          and s.incidence_id = i.id
          and i.id in (
            select distinct
              s.incidence_id
            from
              giscedata_qualitat_incidence i,
              giscedata_qualitat_span s
            where
              s.incidence_id = i.id
              and to_char(begin_date, 'MM/YYYY') = %s
              and i.affected_means = 1
              and extract('epoch' from s.end_date - s.begin_date) > 180)
            order by
            incidence_id,
            installation_id
        ) as foo
      group by
        data,
        municipi,
        codeine,
        zona,
        zona_id,
        cause
      order by
        municipi asc
        """, (provincia['id'], data['form']['date'],))

        report_ids = []
        poblacions = cr.dictfetchall()
        q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.niepi.mensual.municipi')
        for poble in poblacions:
            vals = {}
            # Anem a buscar la potencia instal·lada per municipi, zona i mes
            cr.execute("""
        select
          sum(
            case
              when pi.type = 'C'
                then pi.power/d.cosfi
              when pi.type = 'T'
                then pi.power
            end
          ) as power
        from
          giscedata_qualitat_install_power pi,
          giscedata_qualitat_install_power_data d
        where
          pi.install_power_data_id = d.id
          and to_char(d.name, 'MM/YYYY') = %s
          and  pi.codeine = %s
          and pi.zone_ct_id = %s
        group by
          d.name
        order by
          d.name desc""", (data['form']['date'], poble['codeine'], poble['zona_id']))

            pi = float("%.4f" % cr.fetchone()[0])
            niepi = float("%.4f" % (poble['pot_affect']))/pi

            vals['municipi'] = poble['codeine']
            vals['niepi_id'] = id_q
            vals['zona'] = poble['zona_id']
            vals['potencia_installada'] = float("%.4f" % pi)
            vals['pr_transport'] = 0.0
            vals['pr_distribucio'] = 0.0
            vals['im_generacio'] = 0.0
            vals['im_transport'] = 0.0
            vals['im_tercers'] = 0.0
            vals['im_major'] = 0.0
            vals['im_propies'] = 0.0

            if poble['cause'] == 1:
                vals['pr_transport'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'pr_transport': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))
            elif poble['cause'] == 2:
                vals['pr_distribucio'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'pr_distribucio': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))
            elif poble['cause'] == 3:
                vals['im_generacio'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'im_generacio': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))
            elif poble['cause'] == 4:
                vals['im_transport'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'im_transport': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))
            elif poble['cause'] == 5:
                vals['im_tercers'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'im_tercers': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))
            elif poble['cause'] == 6:
                vals['im_major'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'im_major': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))
            elif poble['cause'] == 7:
                vals['im_propies'] = niepi
                id = q_obj.search(cr, uid, [('niepi_id', '=', vals['niepi_id']), ('municipi','=',vals['municipi']), ('zona', '=', vals['zona'])])
                if len(id):
                    q_obj.write(cr, uid, id, {'im_propies': niepi})
                else:
                    report_ids.append(int(q_obj.create(cr, uid, vals)))

        # Tots els pobles pels que no s'hagi entrat res
        cr.execute("""
    select
      z.id as zona,
      m.id as municipi,
      to_char(d.name, 'YYYY-12-01') as data,
      sum(
        case
          when p.type = 'C'
            then p.power/d.cosfi
          when p.type = 'T'
            then p.power
        end
      ) as power
    from
      giscedata_qualitat_install_power p,
      giscedata_qualitat_install_power_data d,
      res_municipi m,
      giscedata_cts_zona z
    where
      p.codeine = m.id
      and m.state = %s
      and p.zone_ct_id = z.id
      and p.install_power_data_id = d.id
      and to_char(d.name, 'MM/YYYY') = %s
    group by
      z.id,
      m.id,
      d.name""", (provincia['id'], data['form']['date'],))

        for pob in cr.dictfetchall():
            if not len(q_obj.search(cr, uid, [('niepi_id', '=', id_q), ('municipi', '=', pob['municipi']), ('zona', '=', pob['zona'])])):
                report_ids.append(int(q_obj.create(cr, uid, {'niepi_id': id_q, 'municipi': pob['municipi'], 'zona': pob['zona'], 'potencia_installada': float(pob['power'])})))

    data['print_ids'] = niepis
    return {}

def _mostrar_tab(self, cr, uid, data, context={}):
    action = {
      'domain': "[('id','in', ["+','.join(map(str,data['print_ids']))+"])]",
                  'view_type': 'form',
                  'view_mode': 'tree,form',
                  'res_model': 'giscedata.qualitat.niepi.mensual',
      'name': 'NIEPI Mensual %s' % (data['form']['date']),
                  'view_id': False,
      'limit': len(data['print_ids']),
                  'type': 'ir.actions.act_window',
      'res_id': False,
      'auto_refresh': False,
    }
    return action

def _pre_end(self, cr, uid, data, context={}):
    return {}



class wizard_qualitat_niepi_mensual(wizard.interface):
    states = {
      'init': {
          'actions': [],
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
          'actions': [_calc],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
      },
      'pre_end': {
          'actions': [_pre_end],
          'result': { 'type' : 'state', 'state' : 'end' },
      },
    }
wizard_qualitat_niepi_mensual('giscedata.qualitat.niepi.mensual')
