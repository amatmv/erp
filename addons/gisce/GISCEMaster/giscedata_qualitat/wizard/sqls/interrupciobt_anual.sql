-- CALCULEM LES INTERRUPCIONS BT MENSUALS

SELECT
  q.provincia as provincia,
  q_municipi.zona as zona,
  q_municipi.municipi as municipi,
  SUM(q_municipi.n_imprevistes) as n_imprevistes,
  SUM(q_municipi.n_programades) as n_programades,
  SUM(q_municipi.n_total) as n_total
FROM
  giscedata_qualitat_interrupciobt_mensual_municipi q_municipi,
  giscedata_qualitat_interrupciobt_mensual q
WHERE
  q_municipi.interrupciobt_id = q.id
  AND q.provincia = 82
  AND substring(q.name from 4 for 4) = '2010'
GROUP BY
  provincia,
  zona,
  municipi
;
