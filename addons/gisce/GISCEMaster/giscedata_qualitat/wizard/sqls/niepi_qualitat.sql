            select
                c.cups as CUPS,
                c.policy as Poliza,
                c.pricelist as Tarifa,
                czone.id as Zona_id,
              -- Si la tarifa es at agafar hores at
              case when (c.pricelist = '3.1' or c.pricelist like '6.') then
                czoneq.at_interrupcions
              else
                czoneq.bt_interrupcions
              end as limit_talls_zona,
                tip.name as tipus_tall,
                extract('epoch' from s.end_date - s.begin_date)
                as temps_total_inter,
                tip.code as tipus_code,
                cau.name as causa_nom,
                cau.code as causa_code,
                s.begin_date as data_inici_inter,
                s.end_date as data_final_inter,
                i.name as incidencia_nom
            from
                giscedata_qualitat_span s,
                giscedata_qualitat_affected_customer c,
                giscedata_qualitat_incidence i,
                giscedata_qualitat_cause cau,
                giscedata_qualitat_type tip,
                giscedata_cts_zona czone,
                giscedata_cts_zona_qualitat czoneq
            where
              s.id = c.span_id
              and s.incidence_id = i.id
                -- Dates del formulari d'entrada
              and s.begin_date >= %s
              and s.end_date < %s
                -- tipus i causa de la incidencia
              and i.type_id = tip.id and i.cause_id = cau.id
                -- tipus no pot ser Programada
              --and tip.code != 1
                -- Casos no poden ser Transport, Tercers, Força Major
              and cau.code not in (4, 5, 6)
              -- l'interval ha durat més de 3 minuts
              and extract('epoch' from s.end_date - s.begin_date) > 180
                -- la zona de la pòlissa
              and c.zone_ct_id = czone.id and czone.id = czoneq.zona_id
            group by c.cups, czone.id, czoneq.bt_hores,
            czoneq.bt_interrupcions, limit_talls_zona, s.type, tip.name,
            tip.code, cau.name, cau.code, s.begin_date, s.end_date,
            c.policy, c.pricelist,  temps_total_inter, i.name
            order by c.cups, czone.id, c.pricelist, tip.code desc
;