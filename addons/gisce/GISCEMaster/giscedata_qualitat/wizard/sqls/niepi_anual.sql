-- CONSULTA DEL NIEPI ANUAL
select 
	data, 
	provincia, 
	municipi, 
	codeine, 
	zona, 
	zona_id, 
	sum(power) as pot_affect, 
	cause 
from (
	select distinct  
		i.id as incidence_id, 
		inst.id as installation_id, 
		case 
			when inst.type = 'C' 
				then inst.power/(
					select 
						cosfi 
					from 
						giscedata_qualitat_install_power_data 
					where 
						to_char(name, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')
					) 
			when inst.type = 'T' 
				then inst.power 
		end as power, 
		m.name as municipi, 
		m.id as codeine, 
		prov.name as provincia, 
		z.name as zona, 
		z.id as zona_id, 
		c.code as cause, 
		to_char(s.begin_date, '01-01-YYYY') as data 
	from 
		giscedata_qualitat_incidence i, 
		giscedata_qualitat_span s, 
		giscedata_qualitat_affected_installation inst, 
		res_municipi m, 
		giscedata_cts_zona z, 
		giscedata_qualitat_cause c, 
		res_country_state prov 
	where 
		prov.id = m.state 
		and c.id = i.cause_id 
		and z.id = inst.zone_ct_id 
		and m.id = inst.codeine 
		and inst.span_id = s.id 
		and s.incidence_id = i.id 
		and i.id in (
			select distinct 
				s.incidence_id 
			from 
				giscedata_qualitat_incidence i, 
				giscedata_qualitat_span s 
			where 
				s.incidence_id = i.id 
				and to_char(begin_date, 'YYYY') = '2007' 
				and i.affected_means = 1 
				and to_char(s.end_date - s.begin_date, 'SSSS')::int > 180) 
			order by 
			incidence_id, 
			installation_id
	) as foo 
group by 
	data, 
	provincia, 
	municipi, 
	codeine, 
	zona, 
	zona_id, 
	cause 
order by 
	municipi asc
	
	
-- TOTS ELS POBLES SEGONS POTENCIA INSTAL·LADA

select 
	z.name as zona,
	m.name as municipi,
	s.name as provincia,
	to_char(d.name, 'YYYY-12-01') as data,
	sum(
		case
			when p.type = 'C'
				then p.power/d.cosfi
			when p.type = 'T'
				then p.power
		end
	) as power
from 
	giscedata_qualitat_install_power p,
	giscedata_qualitat_install_power_data d, 
	res_municipi m,
	res_country_state s,
	giscedata_cts_zona z
where 
	p.codeine = m.id
	and m.state = s.id
	and p.zone_ct_id = z.id
	and p.install_power_data_id = d.id
	and to_char(d.name, 'MM/YYYY') = (
		select 
			to_char(name, 'MM/YYYY')
		from
			giscedata_qualitat_install_power_data
		where
			to_char(name, 'YYYY') = '2007'
		order by
			name desc
		limit 1
	)
group by
	z.name,
	m.name,
	s.name,
	d.name
