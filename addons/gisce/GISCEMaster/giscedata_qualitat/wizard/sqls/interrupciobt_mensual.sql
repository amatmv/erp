-- CALCULEM LES INTERRUPCIONS BT MENSUALS

SELECT
  provincia,
  provincia_id,
  zona,
  zona_id,
  municipi,
  municipi_id,
  SUM(n_incidencies_imprevistes) as n_imprevistes,
  SUM(n_incidencies_programades) as n_programades,
  SUM(n_incidencies_totals) as n_totals
FROM (
    SELECT
        state.name as provincia,
        state.id as provincia_id,
        zona.name as zona,
        zona.id as zona_id,
        municipi.name as municipi,
        municipi.id as municipi_id,
        count(distinct incidence.id) as n_incidencies_imprevistes,
        0 as n_incidencies_programades,
        count(distinct incidence.id) as n_incidencies_totals
    FROM
        giscedata_qualitat_incidence incidence,
        giscedata_qualitat_type incidence_type,
        giscedata_qualitat_span span,
        giscedata_qualitat_affected_customer customer,
        res_municipi as municipi,
        res_country_state as state,
        giscedata_cts cts,
        giscedata_cts_zona zona
    WHERE
      incidence.affected_means = 2
      AND incidence.type_id = incidence_type.id
      AND incidence_type.code = 2
      AND span.incidence_id = incidence.id 
      AND to_char(span.begin_date, 'MM/YYYY') = '02/2010'
      AND EXTRACT('epoch' FROM span.end_date - span.begin_date) > 180
      AND customer.span_id = span.id
      AND customer.ct_id = cts.id
      AND municipi.id = cts.id_municipi
      AND  municipi.state = state.id
      AND state.id = 82
      AND customer.zone_ct_id = zona.id
    GROUP BY
      state.name,
      state.id,
      zona.name,
      zona.id,
      municipi.name,
      municipi.id
    
    UNION
    
    SELECT
        state.name as provincia,
        state.id as provincia_id,
        zona.name as zona,
        zona.id as zona_id,
        municipi.name as municipi,
        municipi.id as municipi_id,
        0 as n_incidencies_imprevistes,
        count(distinct incidence.id) as n_incidencies_programades,
        count(distinct incidence.id) as n_incidencies_totals
    FROM
        giscedata_qualitat_incidence incidence,
        giscedata_qualitat_type incidence_type,
        giscedata_qualitat_span span,
        giscedata_qualitat_affected_customer customer,
        res_municipi as municipi,
        res_country_state as state,
        giscedata_cts cts,
        giscedata_cts_zona zona
    WHERE
      incidence.affected_means = 2
      AND incidence.type_id = incidence_type.id
      AND incidence_type.code = 1
      AND span.incidence_id = incidence.id 
      AND to_char(span.begin_date, 'MM/YYYY') = '02/2010'
      AND EXTRACT('epoch' FROM span.end_date - span.begin_date) > 180
      AND customer.span_id = span.id
      AND customer.ct_id = cts.id
      AND municipi.id = cts.id_municipi
      AND  municipi.state = state.id
      AND state.id = 82
      AND customer.zone_ct_id = zona.id
    GROUP BY
      state.name,
      state.id,
      zona.name,
      zona.id,
      municipi.name,
      municipi.id
) AS foo
GROUP BY
  provincia,
  provincia_id,
  zona,
  zona_id,
  municipi,
  municipi_id
;
