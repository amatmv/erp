-- CALCULEM EL TIEPI MENSUAL
select 
	data, 
	municipi, 
	codeine, 
	provincia, 
	zona, 
	zona_id, 
	cause, 
	sum(
		power*((temps::float/3600))
	) as pot_affect 
from (
	select 
		s.id as span_id, 
		to_char(s.begin_date, '01-MM-YYYY') as data, 
		to_char(s.end_date - s.begin_date, 'SSSS')::int as temps,
		m.name as municipi, 
		prov.name as provincia,
		inst.codeine,
		z.name as zona, 
		z.id as zona_id,
		c.code as cause,
		sum(
			case 
				when inst.type = 'C' 
					then inst.power/(
						select 
							cosfi 
						from 
							giscedata_qualitat_install_power_data 
						where 
							to_char(name, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')
						) 
				when inst.type = 'T' 
					then inst.power 
			end
		) as power 
	from 
		giscedata_qualitat_affected_installation inst, 
		giscedata_qualitat_span s, 
		giscedata_qualitat_incidence i, 
		giscedata_cts_zona z, 
		giscedata_qualitat_cause c, 
		res_municipi m, 
		res_country_state prov 
	where 
		c.id = i.cause_id 
		and m.id = inst.codeine 
		and m.state = prov.id 
		and z.id = inst.zone_ct_id 
		and s.incidence_id = i.id 
		and inst.span_id = s.id 
		and i.id in (
			select distinct 
				s.incidence_id 
			from 
				giscedata_qualitat_incidence i, 
				giscedata_qualitat_span s 
			where 
				s.incidence_id = i.id 
				and to_char(begin_date, 'MM-YYYY') = %s 
				and i.affected_means = 1 
				and to_char(s.end_date - s.begin_date, 'SSSS')::int > 180
		) 
	group by 
		s.id, 
		s.begin_date, 
		temps, 
		m.name, 
		z.name, 
		z.id, 
		inst.type, 
		c.code, 
		inst.codeine, 
		prov.name 
	order by 
		m.name
	) as foo 
group by 
	data, 
	municipi, 
	codeine, 
	provincia, 
	zona, 
	zona_id, 
	cause 
order by 
	municipi
	
-- BUSQUEM LA POTENCIA INSTAL·LADA

select 
	sum(
		case 
			when pi.type = 'C' 
				then pi.power/d.cosfi 
			when pi.type = 'T' 
				then pi.power 
		end
	) as power 
from 
	giscedata_qualitat_install_power pi, 
	giscedata_qualitat_install_power_data d 
where 
	pi.install_power_data_id = d.id 
	and to_char(d.name, 'MM/YYYY') = %s 
	and  pi.codeine = %i 
	and pi.zone_ct_id = %i

