select 
	data, 
	municipi, 
	codeine, 
	provincia, 
	zona, 
	zona_id, 
	cause, 
	sum(
		power*((temps::float/3600))
	) as pot_affect 
from (
	select 
		s.id as span_id, 
		to_char(s.begin_date, 'YYYY-12-01') as data, 
		to_char(s.end_date - s.begin_date, 'SSSS')::int as temps,
		m.name as municipi, 
		prov.name as provincia,
		inst.codeine,
		z.name as zona, 
		z.id as zona_id,
		c.code as cause,
		sum(
			case 
				when inst.type = 'C' 
					then inst.power/(
						select 
							cosfi 
						from 
							giscedata_qualitat_install_power_data 
						where 
							to_char(name, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')
						) 
				when inst.type = 'T' 
					then inst.power 
			end
		) as power 
	from 
		giscedata_qualitat_affected_installation inst, 
		giscedata_qualitat_span s, 
		giscedata_qualitat_incidence i, 
		giscedata_cts_zona z, 
		giscedata_qualitat_cause c, 
		res_municipi m, 
		res_country_state prov 
	where 
	c.id = i.cause_id 
	and m.id = inst.codeine 
	and m.state = prov.id 
	and prov.id = 82
	and z.id = inst.zone_ct_id 
	and s.incidence_id = i.id 
	and inst.span_id = s.id 
	and i.id in (
		select distinct 
			s.incidence_id 
		from 
			giscedata_qualitat_incidence i, 
			giscedata_qualitat_span s 
		where 
			s.incidence_id = i.id 
			and to_char(begin_date, 'YYYY') = '2007' 
			and i.affected_means = 1 
			and to_char(s.end_date - s.begin_date, 'SSSS')::int > 180) 
		group by 
			s.id, 
			s.begin_date, 
			temps, 
			m.name, 
			z.name, 
			z.id, 
			inst.type, 
			c.code, 
			inst.codeine, 
			prov.name 
		order by 
			m.name
	) as foo 
group by 
	data, 
	municipi, 
	codeine, 
	provincia, 
	zona, 
	zona_id, 
	cause 
order by 
	municipi

-- Totes les provincies on hi ha potencia instal·lada
	
select distinct
	s.id,
	s.name as provincia
from
	giscedata_qualitat_install_power p,
	giscedata_qualitat_install_power_data d,
	res_municipi m,
	res_country_state s
where
	p.install_power_data_id = d.id
	and p.codeine = m.id
	and m.state = s.id
	and to_char(d.name, 'YYYY') = '2007'
	
	
-- Potencies instal·lades per mes i provincia

select 
	sum(
		case 
			when pi.type = 'C' 
				then pi.power/d.cosfi 
			when pi.type = 'T' 
				then pi.power 
		end
	) as power, 
	to_char(d.name, 'MM-YYYY') as data 
from 
	giscedata_qualitat_install_power pi, 
	giscedata_qualitat_install_power_data d,
	res_municipi m
where 
	pi.install_power_data_id = d.id
	and pi.codeine = m.id
	and m.state = %i
	and to_char(d.name, 'YYYY') = %s
group by 
	data 
order by 
	data asc
	
	
	
select 
	cause::varchar, 
	sum(
		power*((temps::float/3600))
	) as pot_affect 
from (
	select 
		s.id as span_id, 
		to_char(s.begin_date, '01-MM-YYYY') as data, 
		to_char(s.end_date - s.begin_date, 'SSSS')::int as temps,
		m.name as municipi, 
		inst.codeine,
		z.name as zona, 
		z.id as zona_id,
		c.code as cause,
		sum(
			case 
				when inst.type = 'C' 
					then inst.power/(
						select 
							cosfi 
						from 
							giscedata_qualitat_install_power_data 
						where 
							to_char(name, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')
						) 
					when inst.type = 'T' 
						then inst.power 
			end
		) as power 
	from 
		giscedata_qualitat_affected_installation inst, 
		giscedata_qualitat_span s, 
		giscedata_qualitat_incidence i, 
		giscedata_cts_zona z, 
		giscedata_qualitat_cause c, 
		res_municipi m
	where 
		c.id = i.cause_id 
		and m.id = inst.codeine 
		and m.state = %i 
		and z.id = inst.zone_ct_id 
		and s.incidence_id = i.id 
		and inst.span_id = s.id 
		and i.id in (
			select distinct 
				s.incidence_id 
			from 
				giscedata_qualitat_incidence i, 
				giscedata_qualitat_span s 
			where 
				s.incidence_id = i.id 
				and to_char(begin_date, 'MM-YYYY') = %s 
				and i.affected_means = 1 
				and to_char(s.end_date - s.begin_date, 'SSSS')::int > 180
			) 
	group by 
		s.id, 
		s.begin_date, 
		temps, 
		m.name, 
		z.name, 
		z.id, 
		inst.type, 
		c.code, 
		inst.codeine
	order by 
		m.name
	) as foo 
group by 
	cause
	
--- TEST 2 ANUAL BEN FET ---

select 
	data, 
	municipi, 
	codeine, 
	zona, 
	zona_id, 
	cause, 
	sum(
		power*((temps::float/3600))
	) as pot_affect 
from (
	select 
		s.id as span_id, 
		to_char(s.begin_date, '01-MM-YYYY') as data, 
		to_char(s.end_date - s.begin_date, 'SSSS')::int as temps,
		m.name as municipi, 
		inst.codeine,
		z.name as zona, 
		z.id as zona_id,
		c.code as cause,
		sum(
			case 
				when inst.type = 'C' 
					then inst.power/(
						select 
							cosfi 
						from 
							giscedata_qualitat_install_power_data 
						where 
							to_char(name, 'MM-YYYY') = to_char(s.begin_date, 'MM-YYYY')
						) 
					when inst.type = 'T' 
						then inst.power 
			end
		) as power 
	from 
		giscedata_qualitat_affected_installation inst, 
		giscedata_qualitat_span s, 
		giscedata_qualitat_incidence i, 
		giscedata_cts_zona z, 
		giscedata_qualitat_cause c, 
		res_municipi m
	where 
		c.id = i.cause_id 
		and m.id = inst.codeine 
		and m.state = %i 
		and z.id = inst.zone_ct_id 
		and s.incidence_id = i.id 
		and inst.span_id = s.id 
		and i.id in (
			select distinct 
				s.incidence_id 
			from 
				giscedata_qualitat_incidence i, 
				giscedata_qualitat_span s 
			where 
				s.incidence_id = i.id 
				and to_char(begin_date, 'MM-YYYY') = %s 
				and i.affected_means = 1 
				and to_char(s.end_date - s.begin_date, 'SSSS')::int > 180
			) 
	group by 
		s.id, 
		s.begin_date, 
		temps, 
		m.name, 
		z.name, 
		z.id, 
		inst.type, 
		c.code, 
		inst.codeine
	order by 
		m.name
	) as foo 
group by
	data, 
	municipi, 
	codeine, 
	zona, 
	zona_id, 
	cause, 

-- COM A SUMA DE TOTS ELS MENSUALS --

select
	tmm.municipi as municpi,
	tmm.zona as zona,
	sum(tmm.pr_transport) as pr_transport,
	sum(tmm.pr_distribucio) as pr_distribucio,
	sum(tmm.im_generacio) as im_generacio,
	sum(tmm.im_transport) as im_transport,
	sum(tmm.im_tercers) as im_tercers,
	sum(tmm.im_major) as im_major,
	sum(tmm.im_propies) as im_propies
from
	giscedata_qualitat_tiepi_mensual_municipi tmm,
	giscedata_qualitat_tiepi_mensual tm
where
	tmm.tiepi_id = tm.id
	and tm.provincia = %i
	and substring(tm.name from 4 for 4) = %s
group by
	tmm.municipi,
	tmm.zona


select
	tmm.id,
	q.tiepi_hores
from
	giscedata_qualitat_tiepi_anual_municipi tam,
	giscedata_qualitat_tiepi_anual ta,
	giscedata_cts_zona_qualitat q
where
	tam.tiepi_id = ta.id
	and tam.zona = q.zona_id
	and q.inici < substring(ta.name, from 4 for 4) || '-12-31'
	and q.final >= substring(ta.name, from 4 for 4) || '-12-31'
	and tmm.id in (15,16)
