# -*- coding: utf-8 -*-
import wizard
import pooler
import calendar

from osv import osv
from tools.translate import _

def _date_selection(self, cr, uid, context=None):
    cr.execute("select distinct to_char(s.begin_date, 'MM/YYYY'), to_char(s.begin_date, 'YYYYMM') as ts from giscedata_qualitat_span s left join giscedata_qualitat_incidence i on (s.incidence_id = i.id and i.affected_means = 2) order by ts desc")
    return [(m[0], m[0]) for m in cr.fetchall()]


_any_form = """<?xml version="1.0"?>
<form string="Avís">
  <label string="Esculli l'any i el mes" colspan="2"/>
  <field name="date" required="1"/>
</form>"""

_any_fields = {
  'date': {'string': 'Mes', 'type': 'selection', 'selection': _date_selection},
}


def _calc(self, cr, uid, data, context=None):
    # Array que utilitzarem per mostrar els models que hem creat a un nou tab.
    print_ids = []

    mun_obj = pooler.get_pool(cr.dbname).get('res.municipi')

    q_prov = pooler.get_pool(cr.dbname).get('giscedata.qualitat.interrupciobt.mensual')

    # Busquem totes les provincies
    sql_provincies = """
        SELECT DISTINCT
          s.id,
          s.name AS provincia
        FROM
            giscedata_qualitat_install_power AS p,
            giscedata_qualitat_install_power_data AS d,
            res_municipi m,
            res_country_state s
        WHERE
            p.install_power_data_id = d.id
            AND p.codeine = m.id
            AND m.state = s.id
            AND to_char(d.name, 'MM/YYYY') = %s
    """
    cr.execute(sql_provincies, (data['form']['date'],))
    provincies = cr.dictfetchall()
    # Deletes all the previous generated menusal interruptoions

    # Ids to delete
    provinces_ids = [provincia['id'] for provincia in provincies]
    search_params = [
        ('name', '=', data['form']['date']),
        ('provincia', 'in', provinces_ids)
    ]
    del_ids = q_prov.search(cr, uid, search_params)
    if del_ids:
        q_prov.unlink(cr, 1, del_ids)

    for provincia in provincies:
        # Creem el nou
        id_q = q_prov.create(cr, uid, {'name': data['form']['date'], 'provincia': provincia['id']})
        print_ids.append(id_q)
        cr.execute("""SELECT
      provincia,
      provincia_id,
      zona_id as zona,
      zona as zona_name,
      municipi as municipi_name,
      municipi_id as municipi,
      SUM(n_incidencies_imprevistes) as n_imprevistes,
      SUM(n_incidencies_programades) as n_programades,
      SUM(n_incidencies_totals) as n_total
    FROM (
        SELECT
            state.name as provincia,
            state.id as provincia_id,
            zona.name as zona,
            zona.id as zona_id,
            municipi.name as municipi,
            municipi.id as municipi_id,
            count(distinct incidence.id) as n_incidencies_imprevistes,
            0 as n_incidencies_programades,
            count(distinct incidence.id) as n_incidencies_totals
        FROM
            giscedata_qualitat_incidence incidence,
            giscedata_qualitat_type incidence_type,
            giscedata_qualitat_span span,
            giscedata_qualitat_affected_customer customer,
            res_municipi as municipi,
            res_country_state as state,
            giscedata_cts cts,
            giscedata_cts_zona zona
        WHERE
          incidence.affected_means = 2
          AND incidence.type_id = incidence_type.id
          AND incidence_type.code = 2
          AND span.incidence_id = incidence.id
          AND to_char(span.begin_date, 'MM/YYYY') = %s
          AND EXTRACT('epoch' FROM span.end_date - span.begin_date) > 180
          AND customer.span_id = span.id
          AND customer.ct_id = cts.id
          AND municipi.id = cts.id_municipi
          AND  municipi.state = state.id
          AND state.id = %s
          AND customer.zone_ct_id = zona.id
        GROUP BY
          state.name,
          state.id,
          zona.name,
          zona.id,
          municipi.name,
          municipi.id

        UNION

        SELECT
            state.name as provincia,
            state.id as provincia_id,
            zona.name as zona,
            zona.id as zona_id,
            municipi.name as municipi,
            municipi.id as municipi_id,
            0 as n_incidencies_imprevistes,
            count(distinct incidence.id) as n_incidencies_programades,
            count(distinct incidence.id) as n_incidencies_totals
        FROM
            giscedata_qualitat_incidence incidence,
            giscedata_qualitat_type incidence_type,
            giscedata_qualitat_span span,
            giscedata_qualitat_affected_customer customer,
            res_municipi as municipi,
            res_country_state as state,
            giscedata_cts cts,
            giscedata_cts_zona zona
        WHERE
          incidence.affected_means = 2
          AND incidence.type_id = incidence_type.id
          AND incidence_type.code = 1
          AND span.incidence_id = incidence.id
          AND to_char(span.begin_date, 'MM/YYYY') = %s
          AND EXTRACT('epoch' FROM span.end_date - span.begin_date) > 180
          AND customer.span_id = span.id
          AND customer.ct_id = cts.id
          AND municipi.id = cts.id_municipi
          AND  municipi.state = state.id
          AND state.id = %s
          AND customer.zone_ct_id = zona.id
        GROUP BY
          state.name,
          state.id,
          zona.name,
          zona.id,
          municipi.name,
          municipi.id
    ) AS foo
    GROUP BY
      provincia,
      provincia_id,
      zona,
      zona_id,
      municipi,
      municipi_id
    """, (data['form']['date'], provincia['id']) * 2)
        poblacions = cr.dictfetchall()
        q_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.interrupciobt.mensual.municipi')
        cache_zones = {}

        # Busca tots els municipis de la provincia i genera una llista de ids
        search_params_pob = [('state', '=', provincia['id'])]
        pob_prov_ids = mun_obj.search(cr, uid, search_params_pob)

        for poble in poblacions:
            # Tenim el mateix noms en les columnes que ens retorna la consulta
            # que els camps de la base de dades, només falta eliminar les
            # columnes provincia i provincia_id ja que en el model de la bbdd
            # es troba a giscedata.qualitat.interrupciobt.mensual i
            # assignar-hi l'id del periode
            vals = poble.copy()
            del vals['provincia']
            del vals['provincia_id']
            del vals['zona_name']
            del vals['municipi_name']
            vals['interrupciobt_id'] = id_q
            q_obj.create(cr, uid, vals)

            # Fem cache per després comprovar si existeixen o no
            if not cache_zones.has_key(vals['municipi']):
                cache_zones[vals['municipi']] = []
            cache_zones[vals['municipi']] += [vals['zona']]

        # Ara actualitzem a 0 tots els municipis que tinguin
        # potencia instal·lada i no estiguin al diccionari de poblacions
        month,year = map(int, data['form']['date'].split('/'))
        last_day = calendar.monthrange(year, month)[1]
        search_params = [
          ('install_power_data_id.name', '>=', '%04i-%02i-01 00:00:00' % (year, month)),
          ('install_power_data_id.name', '<=', '%04i-%02i-%02i' % (year, month, last_day)),
        ]
        potencia_inst_obj = pooler.get_pool(cr.dbname).get('giscedata.qualitat.install_power')
        potencia_inst_ids = potencia_inst_obj.search(cr, uid, search_params)
        if potencia_inst_ids:
            for potencia_inst in potencia_inst_obj.read(cr, uid, potencia_inst_ids,
                                                        ['codeine', 'zone_ct_id', 'code_ct']):
                if potencia_inst['codeine'][0] in pob_prov_ids:
                    try:
                        zone = potencia_inst['zone_ct_id'][0]
                    except:
                        msg = _(u"El CT %s no disposa de zona") % potencia_inst['code_ct']
                        raise osv.except_osv('Error', msg)
                    codeine = potencia_inst['codeine'][0]
                    if zone not in cache_zones.get(codeine, []):
                        if not cache_zones.has_key(codeine):
                            cache_zones[codeine] = []
                        cache_zones[codeine] += [zone]
                        vals = {
                          'zona': zone,
                          'municipi': codeine,
                          'interrupciobt_id': id_q
                        }
                        q_obj.create(cr, uid, vals)

    data['print_ids'] = print_ids[:]
    return {}

def _mostrar_tab(self, cr, uid, data, context=None):
    action = {
      'domain': "[('id','in', ["+','.join(map(str, map(int, data['print_ids'])))+"])]",
      'view_type': 'form',
      'view_mode': 'tree,form',
      'res_model': 'giscedata.qualitat.interrupciobt.mensual',
      'name': 'Interrupcions BT Mensual %s' % (data['form']['date']),
      'view_id': False,
      'limit': len(data['print_ids']),
      'type': 'ir.actions.act_window',
      'res_id': False,
      'auto_refresh': False,
    }
    return action

def _pre_end(self, cr, uid, data, context=None):
    return {}

class wizard_qualitat_interrupciobt_mensual(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'form', 'arch': _any_form,'fields': _any_fields, 'state':[('end', 'Cancelar', 'gtk-cancel'), ('calc', 'Continuar', 'gtk-go-forward')]}
      },
      'calc': {
        'actions': [_calc],
        'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
      },
      'pre_end': {
        'actions': [_pre_end],
        'result': { 'type' : 'state', 'state' : 'end' },
      },
    }

wizard_qualitat_interrupciobt_mensual('giscedata.qualitat.interrupciobt.mensual')
