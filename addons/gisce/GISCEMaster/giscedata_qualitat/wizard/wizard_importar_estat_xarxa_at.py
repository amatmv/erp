# -*- coding: utf-8 -*-

from osv import osv, fields
import base64
import StringIO
import csv
from tools import config

class WizardQualitatEstatAt(osv.osv_memory):

    _name = 'giscedata.qualitat.estat.at'

    def delete_giscegis_config_boto(self, cursor, uid, ids, context=None):
        #Borrar elements taula configdefault
        wizard = self.browse(cursor, uid, ids[0])
        try:
            cursor.execute("TRUNCATE giscegis_configdefault_at")
            wizard.write({'state': 'check',
                          'result_bor': 'Taula esborrada'})
        except:
            wizard.write({'state': 'check',
                          'result_bor': "Error a l'hora d'esborrar"})

    def delete_giscegis_config(self, cursor):
        #Borrar elements taula configdefault
        cursor.execute("TRUNCATE giscegis_configdefault_at")
        return True

    def insert_giscegis_config(self, reader, cursor, nlinies):
        try:
            #Carrego l'arxiu taula temporal
            cursor.execute("""
            CREATE TEMPORARY TABLE IF NOT EXISTS scada (name varchar)
            ON COMMIT DROP
            """)

            #Recorrer linies
            for row in reader:
                # Insert a la taula temporal
                cursor.execute("""
                INSERT INTO scada VALUES (%s)
                """, (row[0],))

                nlinies += 1

            #Borrar les entrades de giscegis_config
            self.delete_giscegis_config(cursor)

            #Insert de l'element
            cursor.execute("""
            INSERT INTO giscegis_configdefault_at (node, blockname_str, name, codi)
            SELECT

             COALESCE(f.node, i.node, sec.node, u.node) as node,
             COALESCE(bf.name, bi.name, bsec.name, bu.name) as blockname_str,
             'DEFAULT' as name,
             s.name as codi
            FROM scada s
            LEFT JOIN giscegis_blocs_fusiblesat f ON (s.name=f.codi)
            LEFT JOIN giscegis_blocs_fusiblesat_blockname bf ON (bf.id=f.blockname)
            LEFT JOIN giscegis_blocs_interruptorat i ON (s.name=i.codi)
            LEFT JOIN giscegis_blocs_interruptorat_blockname bi ON (bi.id=i.blockname)
            LEFT JOIN giscegis_blocs_seccionadorat sec ON (s.name=sec.codi)
            LEFT JOIN giscegis_blocs_seccionadorat_blockname bsec ON (bsec.id=sec.blockname)
            LEFT JOIN giscegis_blocs_seccionadorunifilar u ON (s.name=u.codi)
            LEFT JOIN giscegis_blocs_seccionador_unifilar_blockname bu ON (bu.id=u.blockname)
            """)
            return True, nlinies
        except Exception:
            cursor.rollback()
            return False, nlinies

    def importar_estat_at(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        # Extreure el contingut del fitxer
        file_content = [base64.b64decode(wizard.file)]

        #Carrego l'arxiu
        csv_file = StringIO.StringIO(file_content[0])
        reader = csv.reader(csv_file, delimiter='\t')
        nlinies = 0

        res = self.insert_giscegis_config(reader, cursor, nlinies)
        if res[0]:
            wizard.write({'state': 'end',
                          'n_linies': res[1],
                          'result': 'Importació realitzada amb èxit'})
        else:
            wizard.write({'state': 'end',
                          'result': 'Error a la importació'})

    _columns = {
        'file': fields.binary('Fitxer'),
        'n_linies': fields.integer('# Línies procesades'),
        'result': fields.text('Resultat'),
        'result_bor': fields.text('Resultat'),
        'state': fields.selection([('init', 'Init'),
                                   ('check', 'Check'),
                                   ('end', 'End'), ],
                                  'State'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'n_linies': lambda *a: 0,
    }



WizardQualitatEstatAt()