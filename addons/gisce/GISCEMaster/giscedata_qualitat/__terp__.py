# -*- coding: utf-8 -*-
{
    "name": "GISCE Qualitat",
    "description": """Modulo para el control de la calidad de servicio""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCE",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_cts",
        "base_extended"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_importar_estat_xarxa_at_view.xml",
        "giscedata_qualitat_view.xml",
        "giscedata_polissa_view.xml",
        "giscedata_qualitat_report.xml",
        "giscedata_qualitat_wizard.xml",
        "data.xml",
        "security/giscedata_qualitat_security.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
