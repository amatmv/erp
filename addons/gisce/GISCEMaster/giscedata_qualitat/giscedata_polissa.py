# -*- coding: utf-8 -*-
from osv import fields, osv


class GiscedataPolissa(osv.osv):

    _name = "giscedata.polissa"
    _inherit = "giscedata.polissa"

    def _talls(self, cr, uid, ids, field_name, arg, context):
        # Inicialitzem tots els valors
        res = dict([(x, []) for x in ids])
        # Fem un diccionari {'id': 'name'} per les pòlisses, ja que les tenim
        # relacionades pel número de pòlissa i no per l'id
        polisses = {}
        for p in self.read(cr, uid, ids, ['id', 'name']):
            if p['name']:
                polisses[p['name']] = p['id']
        if not polisses:
            return res
        cr.execute('''
            select policy,span_id from giscedata_qualitat_affected_customer
            where policy in %s''', (tuple(polisses.keys()), )
        )
        for r in cr.fetchall():
            pid = polisses[r[0]]
            res[pid].append(r[1])

        return res

    def _ff_n_talls(self, cursor, uid, ids, field_name, arg, context=None):
        # Inicialitzem
        res = dict([(x, 0) for x in ids])
        polisses = {}
        for p in self.read(cursor, uid, ids, ['id', 'name']):
            if p['name']:
                polisses[p['name']] = p['id']

        cursor.execute('''
            select policy, count(span_id)
            from
                giscedata_qualitat_affected_customer
            where policy in %s group by policy
        ''', (tuple(polisses.keys()), )) 
        res.update(dict([(polisses[r[0]], r[1]) for r in cursor.fetchall()]))
        return res

    def _trg_n_talls(self, cursor, uid, ids, context=None):
        polissa_obj = self.pool.get('giscedata.polissa')
        polisses = [x['policy']
                    for x in self.read(cursor, uid, ids, ['policy'])]
        pol_ids = polissa_obj.search(cursor, uid, [('name', 'in', polisses)],
                                     context={'active_test': False})
        if len(pol_ids) == len(polisses):
            # Si les que busquem les trobem només recalculem aquestes
            return pol_ids
        else:
            # Si no les trobem totes les recalculem totes
            return polissa_obj.search(cursor, uid, [],
                                     context={'active_test': False})

    _columns = {
        'talls': fields.function(
            _talls,
            method=True,
            type='one2many',
            relation='giscedata.qualitat.span',
            string='Incidències'
        ),
        'n_talls': fields.function(
            _ff_n_talls,
            method=True,
            type='integer',
            string='Número d\'incidències',
            store={
                'giscedata.qualitat.affected_customer':
                    (_trg_n_talls, ['policy'], 0)
            },
        )
      }

GiscedataPolissa()
