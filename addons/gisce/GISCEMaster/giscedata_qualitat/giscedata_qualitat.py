# -*- coding: utf-8 -*-
from osv import osv, fields
from datetime import datetime
from time import *
from tools.translate import _
import netsvc


logger = netsvc.Logger()

#
# Mes
#
class giscedata_qualitat_mes(osv.osv):
    _name = 'giscedata.qualitat.mes'
    _description = 'GISCE Qualitat mes'
    _columns = {
      'name': fields.char('Nom', size=20, required=True),
      'codi': fields.integer('Codi'),
    }
giscedata_qualitat_mes()

#
# Origen
#
#
class giscedata_qualitat_origin(osv.osv):
    _name = 'giscedata.qualitat.origin'
    _description = 'GISCE Qualitat origen incidencia'
    _columns = {
      'name': fields.char('Nom', size=50, required=True),
      'code': fields.integer('Codi'),
    }
    _defaults = {}
giscedata_qualitat_origin()


class GiscedataQualitatType(osv.osv):
    """
        Tipo
    """
    _name = 'giscedata.qualitat.type'
    _description = 'GISCE Qualitat tipo incidencia'
    _columns = {
        'name': fields.char('Nom', size=50, required=True),
        'code': fields.integer('Codi'),
        'default': fields.boolean('Default')
    }
    _defaults = {
        'default': lambda *a: False,
    }
GiscedataQualitatType()

#
# Causa
#
#
class giscedata_qualitat_cause(osv.osv):
    _name = 'giscedata.qualitat.cause'
    _description = 'GISCE Qualitat causa incidencia'
    _columns = {
      'name': fields.char('Nom', size=50, required=True),
      'code': fields.integer('Codi'),
      'type': fields.many2one('giscedata.qualitat.type', 'Type'),
    }
    _defaults = {}
giscedata_qualitat_cause()


class GiscedataQualitatIncidence(osv.osv):
    """
    Incidencia
    """

    def _data(self, cr, uid, ids, name, arg, context=None):
        """
        Calculates the data field
        :param cr: Database cursor
        :param uid: User id
        :type uid: int
        :param ids: Incidence ids to calculate
        :type ids: list
        :param name: Unused
        :param arg: Unused
        :param context: OpenERP Context
        :return: dict {incidence id: value}
        :rtype: dict
        """
        if not context:
            context = {}
        span_obj = self.pool.get("giscedata.qualitat.span")
        spans = self.read(cr, uid, ids, ['spans_ids'])
        incidences_ids = [x["id"] for x in spans]
        res = dict.fromkeys(incidences_ids, False)
        for incidence in spans:
            if incidence['spans_ids']:
                spans = span_obj.read(cr, uid, incidence['spans_ids'],
                                      ['begin_date'])
                res[incidence['id']] = spans[0]['begin_date']
        return res

    def create_incidence(self, cursor, uid, id, name, origin, incidence_type,
                         cause, network, context=None):
        vals = {}
        vals['name'] = name
        vals['origin_id'] = origin
        vals['type_id'] = incidence_type
        vals['cause_id'] = cause
        if network.lower() == 'at':
            vals['affected_means'] = 1
        elif network.lower() == 'bt':
            vals['affected_means'] = 2
        else:
            return False
        model = self.pool.get("giscedata.qualitat.incidence")
        return model.create(cursor, uid, vals)

    def get_incidence(self,cursor, uid, id, network, context=None):
        model = self.pool.get("giscedata.qualitat.incidence")
        if isinstance(network, (list, tuple)):
            network = network[0]
        if network.lower() == 'bt':
            ids = model.search(cursor, uid, [('affected_means', '=', 2)])
            return model.read(cursor, uid,ids,['name'])
        elif network.lower() == 'at':
            ids = model.search(cursor, uid, [('affected_means', '=', 1)])
            return model.read(cursor, uid,ids,['name'])
        else:
            return False

    def _change_data_incidencia(self, cursor, uid, ids, context=None):
        """
        Function returns the ids of modified elements of a span to update the
        stored data field

        :param cursor: Database cursor
        :param uid: User id
        :param ids: Changed ids of giscedata.qualitat.span
        :param context:OpenERP context
        :return: Incidence ids list
        :rtype: list of int
        """

        span_obj = self.pool.get("giscedata.qualitat.span")
        return_ids = []
        for span in span_obj.read(cursor, uid, ids, ["incidence_id"]):
            return_ids.append(span["incidence_id"][0])
        return return_ids

    def _data_search(self, cr, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not len(args):
            return []
        else:
            search_str = ''
            for c in args:
                # verifiquem les dates
                if c[1] not in ('=', '<=', '>=', 'ilike'):
                    raise Exception(_(u"Paràmetres d'entrada incorrectes"))
                try:
                    datetime.strptime(c[2], '%Y-%m-%d')
                except ValueError:
                        raise Exception(_(u"Paràmetres d'entrada incorrectes"))

            search_str += "AND s.begin_date %s '%s'" % (
                    str(c[1]),
                    str(c[2])
                    )
            cr.execute("""select distinct i.id
            from giscedata_qualitat_incidence i, giscedata_qualitat_span s
            where s.incidence_id = i.id %s """ % search_str)
            res = cr.fetchall()
            if not len(res):
                return [('id','=','0')]
            return [('id','in',map(lambda x:x[0], res))]


    _name = "giscedata.qualitat.incidence"
    _description = "GISCE Qualitat incidencia"
    _columns = {'code': fields.char('Codi',size=50),
                'name': fields.char('Nom', size=255),
                'origin_id': fields.many2one('giscedata.qualitat.origin',
                                             'Origen'),
                'type_id': fields.many2one('giscedata.qualitat.type', 'Tipus'),
                'cause_id': fields.many2one('giscedata.qualitat.cause',
                                            'Causa'),
                'spans_ids': fields.one2many('giscedata.qualitat.span',
                                             'incidence_id', 'Intervals'),
                'affected_means': fields.selection([(1, 'AT'), (2, 'BT')],
                                                   'Medi afectat'),
                "data": fields.function(
                    _data, type="datetime", method=True,
                    fnct_search=_data_search, string='Data',
                    store={
                        "giscedata.qualitat.span":
                            (_change_data_incidencia, ["begin_date"], 10)
                    }
                ),

                }

    _defaults = {}
    _order = "data desc"


GiscedataQualitatIncidence()


#
# Intervalo
#
class giscedata_qualitat_span_cause(osv.osv):
    _name = "giscedata.qualitat.span.cause"
    _description = "GISCE Qualitat Interval - Causes"
    _columns = {
      'code' : fields.integer('Codi'),
      'name' : fields.char('Nom', size=64),
    }
    _defaults = {}

giscedata_qualitat_span_cause()


class giscedata_qualitat_span_type(osv.osv):
    _name = "giscedata.qualitat.span.type"
    _description = "GISCE Qualitat Interval - Tipus"
    _columns = {
      'code' : fields.integer('Codi'),
      'name' : fields.char('Nom', size=64)
    }

    _defaults = {}

giscedata_qualitat_span_type()


class GiscedataQualitatSpan(osv.osv):

    def _timespan_diff(self, cr, uid, ids, name, arg, context={}):
        res = dict.fromkeys(ids, False)
        cr.execute(
            "select id, extract('epoch' from end_date - begin_date) as time "
            "from giscedata_qualitat_span where id in %s",
            (tuple(ids), )
        )
        for diff in cr.fetchall():
            res[diff[0]] = int(diff[1])
        return res

    def _potencia_total_clients(self, cr, uid, ids, field_name, args, context={}):
        res = dict.fromkeys(ids, False)
        customers_obj = self.pool.get('giscedata.qualitat.affected_customer')
        for span in self.read(cr, uid, ids, ['affected_customer_ids']):
            potencia = 0.0
            customers = customers_obj.read(cr, uid,
                                           span['affected_customer_ids'],
                                           ['contracted_power'])
            for customer in customers:
                potencia += customer['contracted_power']
            res[span['id']] = potencia
        return res

    def _total_clients(self, cr, uid, ids, field_name, args, context={}):
        res = dict.fromkeys(ids, False)
        for span in self.read(cr, uid, ids, ['affected_customer_ids']):
            res[span['id']] = len(span['affected_customer_ids'])
        return res

    def _potencia_total_installacions(self, cr, uid, ids, field_name, args, context={}):
        res = dict.fromkeys(ids, False)
        for span in self.read(cr, uid, ids, ['begin_date', 'affected_installation_ids']):
            potencia = 0.0
            cosfi = self.pool.get('giscedata.qualitat.install_power.data').get_cosfi(cr, span['begin_date'])
            for installacio in self.pool.get('giscedata.qualitat.affected_installation').read(cr, uid, span['affected_installation_ids'], ['type', 'power']):
                if installacio['type'] == 'T':
                    potencia += installacio['power']
                else:
                    potencia += installacio['power'] / cosfi
            res[span['id']] = potencia
        return res

    def _total_installacions(self, cr, uid, ids, field_name, args, context=None):

        if context is None:
            context = {}

        res = dict.fromkeys(ids, False)
        for span in self.read(cr, uid, ids,['id', 'affected_installation_ids']):
            res[span['id']] = len(span['affected_installation_ids'])
        return res

    def _total_switches(self, cr, uid, ids, field_name, args, context=None):

        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        for span in self.read(cr, uid, ids, ['id', 'switches_ids']):
            res[span['id']] = len(span['switches_ids'])
        return res

    def _incidence_type(self, cr, uid, ids, field_name, args, context=None):

        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        for span in self.browse(cr, uid, ids):
            if span.incidence_id and span.incidence_id.type_id:
                res[span.id] = span.incidence_id.type_id.name
            else:
                res[span.id] = ''
        return res

    def _span_incidence(self, cr, uid, ids, field_name, arg, context=None):

        res = dict.fromkeys(ids, False)
        incidencia_obj = self.pool.get('giscedata.qualitat.incidence')
        for span in self.read(cr, uid, ids, ['incidence_id']):
            incidence_id_name = span['incidence_id']
            if incidence_id_name:
                res[span['id']] = incidence_id_name[1]
            else:
                res[span['id']] = ''
        return res

    def _span_incidence_search(self, cr, uid, obj, name, args, context=None):

        if context is None:
            context = {}

        mod_incidence = self.pool.get('giscedata.qualitat.incidence')
        if not len(args):
            return []
        else:
            search_params = [('name', args[0][1], args[0][2])]
            incidence_ids = mod_incidence.search(cr, uid, search_params)
            if not len(incidence_ids):
                return [('id', '=', 0)]
            else:
                search_params_self = [('incidence_id.id', 'in', incidence_ids)]
                ids = self.search(cr, uid, search_params_self)
                return [('id', 'in', ids)]

    def crear(self, cr, uid, simulation_id, incidencia_id, data_inici,
              hora_inici, data_final, hora_final, causa_interval,
              tipus_interval, xarxa="AT", context=None):
        """
        Mètode per guardar un nou interval des del GIS.

        :param cr: Database cursor
        :param uid: User id
        :param simulation_id: Simulation id
        :param incidencia_id: Incidence id
        :param data_inici: Start date
        :param hora_inici: Start hour
        :param data_final:End date
        :param hora_final: End hour
        :param causa_interval:
        :param tipus_interval:
        :param xarxa: un entre ['AT', 'BT']
        :param context: OpenERP Context
        :return:
        """

        if context is None:
            context = {}
        if isinstance(simulation_id, (list, tuple)):
            simulation_id = simulation_id[0]

        if causa_interval == '':
            causa_interval = False
        if tipus_interval == '':
            tipus_interval = False

        # obtenir resultats de simulation_id
        # ATENCIO! aquí s'haurà  de fer alguna cosa per mirar si és
        # AT o BT... (també més avall als punts de tall)
        if xarxa == 'BT':
            mod_escaomeses_bt = 'giscegis.simulacio.bt.resultats.escomeses'
            sim_escomeses_obj = self.pool.get(mod_escaomeses_bt)
        else:
            mod_escaomeses_at = 'giscegis.simulacio.at.resultats.escomeses'
            sim_escomeses_obj = self.pool.get(mod_escaomeses_at)
            sim_trafos_obj = self.pool.get('giscegis.simulacio.at.resultats.transformadors')

        interval_obj = self.pool.get('giscedata.qualitat.span')

        vals = {}

        # crear interval primer
        vals['name'] = 'INT'
        vals['incidence_id'] = incidencia_id
        range_start = "-".join((data_inici, hora_inici))
        range_end = "-".join((data_final, hora_final))
        vals['begin_date'] = asctime(strptime(range_start, "%d/%m/%Y-%H:%M:%S")) # ?
        vals['end_date'] = asctime(strptime(range_end, "%d/%m/%Y-%H:%M:%S")) # ?

        # guardar i obtenir id per poder crear els resultats
        interval_id = interval_obj.create(cr, uid, vals)
        # assignar resultats del interval
        installacions_obj = self.pool.get('giscedata.qualitat.affected_installation')
        clients_obj = self.pool.get('giscedata.qualitat.affected_customer')
        # per fer instal.lacions: agafar trafos i conta-at
        # trafos:
        if xarxa == 'AT':
            trafos_obj = self.pool.get('giscedata.transformador.trafo')
            search_params = [
                ('simulacio', '=', int(simulation_id)),
                ('estat', '=', -1),
                ('name', '!=', False)
            ]
            sim_trafos_ids = sim_trafos_obj.search(cr, uid, search_params)
            for tid in sim_trafos_ids:
                vals_t = {}
                sim_t = sim_trafos_obj.browse(cr, uid, tid)
                t = trafos_obj.browse(cr, uid, sim_t.name.id)
                if t.tiepi and t.id_estat and t.id_estat.codi == 1:
                    vals_t['name'] = t.name
                    vals_t['span_id'] = interval_id
                    vals_t['type'] = 'T'
                    vals_t['code_ct'] = t.ct.name
                    vals_t['zone_ct_id'] = t.ct.zona_id.id
                    vals_t['code_inst'] = t.id
                    vals_t['power'] = t.potencia_nominal
                    vals_t['codeine'] = t.ct.id_municipi.id
                    vals_t['data'] = asctime()
                    installacions_obj.create(cr, uid, vals_t)
        # end if xarxa == 'AT'

        # conta's (at _installation i altres _customer):
        search_params = [
            ('simulacio', '=', int(simulation_id)),
            ('estat', '=', -1),
            ('name', '!=', False)
        ]
        sim_escomeses_ids = sim_escomeses_obj.search(cr, uid, search_params)
        escomeses_obj = self.pool.get('giscedata.cups.escomesa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        trace_obj = self.pool.get('giscegis.escomeses.traceability')
        conta_obj = self.pool.get('giscedata.lectures.comptador')
        for sid in sim_escomeses_ids:
            vals_e = {}
            sim_e = sim_escomeses_obj.browse(cr, uid, sid)
            e = escomeses_obj.browse(cr, uid, sim_e.name.id)
            # agafem la traceability de l'escomesa per treure-hi el ct
            trace_id = trace_obj.search(cr, uid, [('escomesa', '=', e.id)])
            if len(trace_id) > 0:
                trace = trace_obj.browse(cr, uid, trace_id[0])
                # conta-at (si xarxa == 'BT' no hi hauria de sortir cap)
                if e.blockname.code == 1:
                    vals_e['name'] = e.name
                    vals_e['span_id'] = interval_id
                    vals_e['type'] = 'C'
                    vals_e['code_ct'] = trace.ct.name
                    vals_e['zone_ct_id'] = trace.ct.zona_id.id
                    vals_e['code_inst'] = e.id
                    vals_e['power'] = e.potencia_polisses
                    vals_e['codeine'] = trace.ct.id_municipi.id
                    vals_e['data'] = asctime()
                    installacions_obj.create(cr, uid, vals_e)
                else: # conta-*
                    # per cada cups de l'escomesa amb polissa...
                    search_params_cups = [('id_escomesa', '=', e.id)]
                    cups_ids = cups_obj.search(cr, uid, search_params_cups)
                    for cups in cups_obj.browse(cr, uid, cups_ids):
                        polissa = cups._get_polissa()
                        if polissa:
                            if not cups.nv:
                                logger.notifyChannel(
                                    'qualitat:crear_interval',
                                    netsvc.LOG_WARNING,
                                    u'CUPS %s sense carrer' % cups.name
                                )
                            vals_e['span_id'] = interval_id
                            vals_e['name'] = polissa.titular.name
                            vals_e['code_ct'] = trace.ct.name
                            vals_e['ct_id'] = trace.ct.id
                            vals_e['zone_ct_id'] = trace.ct.zona_id.id or 0
                            vals_e['codeine'] = cups.id_municipi.id
                            vals_e['codecustomer'] = polissa.codi or polissa.name
                            vals_e['line'] = cups.linia
                            vals_e['cups'] = cups.name
                            vals_e['policy'] = polissa.name
                            vals_e['contracted_power'] = polissa.potencia
                            vals_e['tension'] = polissa.tensio_normalitzada.name
                            if polissa.tarifa:
                                vals_e['pricelist'] = polissa.tarifa.name
                            else:
                                vals_e['pricelist'] = 'S.tarifa'
                            vals_e['escomesa'] = e.name
                            vals_e['escomesa_type'] = e.blockname.name
                            vals_e['street'] = cups.nv or ''
                            vals_e['number'] = cups.pnp
                            vals_e['level'] = cups.pt
                            vals_e['appartment_number'] = cups.pu
                            comptadors = polissa.comptadors_actius(polissa.data_alta)
                            if comptadors:
                                vals_e['contador'] = conta_obj.read(cr, uid,
                                                            [comptadors[0]],
                                                            ['name'])[0]['name']
                            else:
                                vals_e['contador'] = '0000000'
                            vals_e['data_alta'] = polissa.data_alta
                            clients_obj.create(cr, uid, vals_e)
        # guardar els punts de tall
        # ATENCIÓ !! simulation_id és l'id de la taula
        # giscegis_simulacio_at_resultats !!
        # (NO de giscegis_configsimulacio_at!)
        if xarxa == 'AT':
            r_obj = self.pool.get('giscegis.simulacio.at.resultats')
        elif xarxa == 'BT':
            r_obj = self.pool.get('giscegis.simulacio.bt.resultats')

        r = r_obj.browse(cr, uid, int(simulation_id))
        giscegis_configsimulacio_id = r.simulacio.id

        # ATENCIÓ !! aquí canvia de AT a BT!
        if xarxa == 'AT':
            oberts_obj = self.pool.get('giscegis.configsimulacio.at.oberts')
        elif xarxa == 'BT':
            oberts_obj = self.pool.get('giscegis.configsimulacio.bt.oberts')
        search_params_oberts = [('config', '=', giscegis_configsimulacio_id)]
        oberts_ids = oberts_obj.search(cr, uid, search_params_oberts)
        span_switch_obj = self.pool.get('giscedata.qualitat.span.switch')
        for ob in oberts_obj.browse(cr, uid, oberts_ids):
            vals_p = {}
            # crear punt de tall i guardar
            node = ob.node
            if not node.id:
                continue
            vals_p['node'] = node.id
            vals_p['name'] = node.name
            bloc = node.get_bloc()
            if bloc and bloc.blockname:
                vals_p['blockname'] = bloc.blockname.name
            else:
                vals_p['blockname'] = None
            vals_p['codi'] = node.get_codi()
            vals_p['x'] = node.vertex.x
            vals_p['y'] = node.vertex.y
            vals_p['span_id'] = interval_id
            span_switch_obj.create(cr, uid, vals_p)
        return True

    _name = "giscedata.qualitat.span"
    _description = "GISCE Qualitat intervalo incidencia"
    _columns = {
        'id': fields.integer('Id'),
        'name': fields.char('Nom', size=16, required=True),
        'incidence_id': fields.many2one(
            'giscedata.qualitat.incidence', 'Incidencia', ondelete='cascade'),
        'begin_date': fields.datetime('Inici', required=True),
        'end_date': fields.datetime('Final', required=True),
        'duration': fields.function(
            _timespan_diff, method=True, type='integer',
            string='Periode (Segons)'),
        'affected_installation_ids': fields.one2many(
            'giscedata.qualitat.affected_installation', 'span_id',
            'Instal·lacions Afectades'),
        'affected_customer_ids': fields.one2many(
            'giscedata.qualitat.affected_customer', 'span_id',
            'Instal·lacions Afectades'),
        'switches_ids': fields.one2many('giscedata.qualitat.span.switch',
                                        'span_id', 'Punts de tall'),
        'user_create': fields.char('Usuari', size=50),
        'date_create': fields.datetime('Data'),
        'potencia_total_clients': fields.function(
            _potencia_total_clients, method=True, type='float',
            string='Potència total (kW)'),
        'total_clients': fields.function(_total_clients, method=True,
                                         type='integer',
                                         string='Total clients'),
        'potencia_total_installacions': fields.function(
            _potencia_total_installacions, method=True, type='float',
            string='Potència total (kVA)'),
        'total_installacions': fields.function(
            _total_installacions, method=True, type='integer',
            string='Total instal·lacions'),
        'total_switches': fields.function(
            _total_switches, method=True, type='integer',
            string='Total punts de tall'),
        'incidence_type': fields.function(
            _incidence_type, method=True, type='char', string='Tipus'),
        'cause': fields.many2one('giscedata.qualitat.span.cause', 'Causa'),
        'type': fields.many2one('giscedata.qualitat.span.type', 'Tipus'),
        'span_incidence': fields.function(
            _span_incidence, fnct_search=_span_incidence_search,
            method=True, type='char', string='Incidència', size=50),
    }
    _defaults = {}

GiscedataQualitatSpan()


class GiscedataQualitatSpanSwitch(osv.osv):
    _name = 'giscedata.qualitat.span.switch'
    _description = 'GISCE Qualitat Span Switch'

    _columns = {
      'span_id': fields.many2one('giscedata.qualitat.span', 'Interval', ondelete='cascade'),
      'name': fields.char('Id', size=10),
      'codi': fields.char('Codi', size=50),
      'blockname': fields.char('Blockname', size=50),
      'node': fields.integer('Node'),
      'x': fields.float('X'),
      'y': fields.float('Y'),

    }

GiscedataQualitatSpanSwitch()


class GiscedataQualitatAffectedInstallation(osv.osv):
    """
    Instalacion Afectada
    """

    _name = 'giscedata.qualitat.affected_installation'

    _description = 'GISCE Qualitat Instalacion afectada'

    _columns = {
      'name': fields.char('Nom', size=16),
      'span_id': fields.many2one('giscedata.qualitat.span','Intèrvals', ondelete='cascade'),
      'type': fields.selection([('T', 'Trafo'), ('C', 'Conta-AT')], 'Tipus', required=True),
      'code_ct': fields.char('Codi CT',size=50),
      'zone_ct_id': fields.many2one('giscedata.cts.zona', 'Zona'),
      'code_inst': fields.char('Codi instal·lació',size=16),
      'power': fields.float('Potència'),
      'codeine': fields.many2one('res.municipi', 'Municipi'),
      'data': fields.datetime('Data'),
    }
    _defaults = {}

GiscedataQualitatAffectedInstallation()


class GiscedataQualitatInstallPowerData(osv.osv):

    _name = 'giscedata.qualitat.install_power.data'

    _description = 'GISCE Qualitat Data potència instal·lada'

    def get_cosfi(self, cr, timestamp):
        # Retornara el cosfi de la data que li passem
        cr.execute("select cosfi from giscedata_qualitat_install_power_data where to_char(name, 'MM-YYYY') = to_char(timestamp %s, 'MM-YYYY') limit 1", (timestamp,))
        cosfi = cr.fetchone()
        if cosfi and len(cosfi):
            return cosfi[0]
        else:
            raise osv.except_osv('Error', "Abans s'ha de generar la potencia instal·lada del mes")

    def _potencia_total_installacions(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        for data in self.read(cr, uid, ids, ['name', 'install_power_ids', 'cosfi']):
            potencia = 0.0
            for installacio in self.pool.get('giscedata.qualitat.install_power').read(cr, uid, data['install_power_ids'], ['type', 'power']):
                if installacio['type'] == 'T':
                    potencia += installacio['power']
                else:
                    potencia += installacio['power'] / data['cosfi']
            res[data['id']] = potencia
        return res

    def _total_installacions(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        for data in self.read(cr, uid, ids, ['install_power_ids']):
            res[data['id']] = len(data['install_power_ids'])
        return res

    def _mes(self, cr, uid, ids, field_name, args, context={}):
        res = dict.fromkeys(ids, False)
        cr.execute(
            "SELECT d.id , m.name as month "
            "from giscedata_qualitat_install_power_data d, "
            "giscedata_qualitat_mes m where to_char(d.name, 'MM')::int = m.codi "
            "and d.id in %s",
            (tuple(ids),)
        )
        for data in cr.fetchall():
            res[data[0]] = data[1]
        return res

    _columns = {
        'name': fields.datetime('Data', readonly=True),
        'cosfi': fields.float('Cosfi', readonly=True),
        'mes': fields.function(_mes, type='char', size=50, method=True,
                               string='Mes'),
        'user_create': fields.char('Usuari', size=50),
        'date_create': fields.datetime('Data'),
        'install_power_ids': fields.one2many('giscedata.qualitat.install_power',
                                             'install_power_data_id',
                                             'Instal·lacions'),
        'potencia_total_installacions':
            fields.function(_potencia_total_installacions, method=True,
                            type='float',
                            string='Potència total (kVA)'),
        'total_installacions': fields.function(_total_installacions,
                                               method=True,
                                               type='integer',
                                               string='Total instal·lacions'),
    }

    _order = 'name asc'

GiscedataQualitatInstallPowerData()


class GiscedataQualitatInstallPower(osv.osv):
    """
    Potencia Instalada
    """

    _name = 'giscedata.qualitat.install_power'
    _description = 'GISCE Qualitat Potencia instalada'

    def _description_ct(self, cr, uid, ids, field_name, args, context={}):
        res = {}
        sql = """
            SELECT descripcio
            FROM giscedata_cts
            WHERE name = %s
        """
        for data in self.read(cr, uid, ids,['code_ct']):
            cr.execute(sql, (data['code_ct'],))
            ct = cr.dictfetchone()
            if ct:
                res[data['id']] = ct['descripcio']
            else:
                res[data['id']] = ''
        return res

    _columns = {
        'install_power_data_id': fields.many2one(
            'giscedata.qualitat.install_power.data',
            'ID Potència Instal·lada Data', ondelete='cascade'),
        'type': fields.selection([('T', 'Trafo'), ('C', 'Conta-AT')], 'Tipo',
                                 required=True),
        'name': fields.char('Número', size=50, required=True),
        'code_ct': fields.char('Codi CT', size=50),
        'description_ct': fields.function(_description_ct, type='char',
                                          size=255, method=True,
                                          string='Descripció CT'),
        'zone_ct_id': fields.many2one('giscedata.cts.zona', 'Zona'),
        'codeine': fields.many2one('res.municipi', 'Municipi'),
        'power': fields.float('Potència'),
        'x': fields.float('X'),
        'y': fields.float('Y'),
    }

    _defaults = {}


GiscedataQualitatInstallPower()


class GiscedataQualitatAffectedCustomer(osv.osv):
    """
    Clientes Afectados
    """
    _name = 'giscedata.qualitat.affected_customer'
    _description = 'GISCE Qualitat Cliente Afectado'
    _columns = {
        'span_id': fields.many2one(
            'giscedata.qualitat.span', 'Id Intèrval', ondelete='cascade'),
        'name': fields.char('Nombre', size=255, required=True),
        'code_ct': fields.char('Codi CT', size=50),
        'ct_id': fields.many2one('giscedata.cts', 'CT'),
        'zone_ct_id': fields.many2one('giscedata.cts.zona', 'Zona'),
        'codeine': fields.many2one('res.municipi', 'Municipi'),
        'codecustomer': fields.char('Codi client', size=50),
        'line': fields.char('Línia', size=50),
        'cups': fields.char('CUPS', size=50),
        'policy': fields.char('Polissa', size=50, select=True),
        'contracted_power': fields.float('Potència Contractada (kW)'),
        'tension': fields.char('Tensió', size=15),
        'pricelist': fields.char('Tarifa', size=10),
        'escomesa': fields.char('Escomesa', size=50),
        'escomesa_type': fields.char('Tipo Escomesa', size=50),
        'street': fields.char('Carrer', size=255, required=True),
        'number': fields.char('Número', size=10),
        'level': fields.char('Planta', size=10),
        'appartment_number': fields.char('Pis', size=10),
        'contador': fields.char('Contador', size=50),
        'data_alta': fields.datetime('Data alta'),
        'x': fields.float('X'),
        'y': fields.float('Y'),
    }
    _defaults = {}

GiscedataQualitatAffectedCustomer()


class GiscedataQualitatReportTiepiZonalNes(osv.osv):
    """
    Reports
    """
    _name = 'giscedata.qualitat.report.tiepi.zonal.mes'
    _description = 'Report pel tiepi mensual'

    _columns = {
      'data': fields.datetime('Data'),
      'municipi': fields.char('Municipi', size=255),
      'provincia': fields.char('Provincia', size=255),
      'zona': fields.char('Zona', size=255),
      'potencia_installada': fields.float('Potència instal·lada'),
      'pr_transport': fields.float('Programat transport'),
      'pr_distribucio': fields.float('Programat distribució'),
      'im_generacio': fields.float('Imprevist generació'),
      'im_transport': fields.float('Imprevist transport'),
      'im_tercers': fields.float('Imprevist tercers'),
      'im_major': fields.float('Imprevist força major'),
      'im_propies': fields.float('Imprevist pròpies'),
    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

GiscedataQualitatReportTiepiZonalNes()

class giscedata_qualitat_report_tiepi_zonal_any(osv.osv):
    _name = 'giscedata.qualitat.report.tiepi.zonal.any'
    _description = 'Report pel tiepi anual'

    _columns = {
      'data': fields.datetime('Data'),
      'municipi': fields.char('Municipi', size=255),
      'provincia': fields.char('Provincia', size=255),
      'zona': fields.char('Zona', size=255),
      'potencia_installada': fields.float('Potència instal·lada'),
      'pr_transport': fields.float('Programat transport'),
      'pr_distribucio': fields.float('Programat distribució'),
      'im_generacio': fields.float('Imprevist generació'),
      'im_transport': fields.float('Imprevist transport'),
      'im_tercers': fields.float('Imprevist tercers'),
      'im_major': fields.float('Imprevist força major'),
      'im_propies': fields.float('Imprevist pròpies'),
    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

giscedata_qualitat_report_tiepi_zonal_any()

class giscedata_qualitat_report_niepi_zonal_mes(osv.osv):
    _name = 'giscedata.qualitat.report.niepi.zonal.mes'
    _description = 'Report pel niepi mensual'

    _columns = {
      'data': fields.datetime('Data'),
      'municipi': fields.char('Municipi', size=255),
      'provincia': fields.char('Provincia', size=255),
      'zona': fields.char('Zona', size=255),
      'potencia_installada': fields.float('Potència instal·lada'),
      'pr_transport': fields.float('Programat transport'),
      'pr_distribucio': fields.float('Programat distribució'),
      'im_generacio': fields.float('Imprevist generació'),
      'im_transport': fields.float('Imprevist transport'),
      'im_tercers': fields.float('Imprevist tercers'),
      'im_major': fields.float('Imprevist força major'),
      'im_propies': fields.float('Imprevist pròpies'),
    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

giscedata_qualitat_report_niepi_zonal_mes()

class giscedata_qualitat_report_niepi_zonal_any(osv.osv):
    _name = 'giscedata.qualitat.report.niepi.zonal.any'
    _description = 'Report pel niepi anual'

    _columns = {
      'data': fields.datetime('Data'),
      'municipi': fields.char('Municipi', size=255),
      'provincia': fields.char('Provincia', size=255),
      'zona': fields.char('Zona', size=255),
      'potencia_installada': fields.float('Potència instal·lada'),
      'pr_transport': fields.float('Programat transport'),
      'pr_distribucio': fields.float('Programat distribució'),
      'im_generacio': fields.float('Imprevist generació'),
      'im_transport': fields.float('Imprevist transport'),
      'im_tercers': fields.float('Imprevist tercers'),
      'im_major': fields.float('Imprevist força major'),
      'im_propies': fields.float('Imprevist pròpies'),
    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

giscedata_qualitat_report_niepi_zonal_any()

class giscedata_qualitat_report_tiepi_percentil80(osv.osv):
    _name = 'giscedata.qualitat.report.tiepi.percentil80'
    _description = 'Report pel tiepi percentil 80'

    _columns = {
      'data': fields.datetime('Data'),
      'municipi': fields.char('Municipi', size=255),
      'provincia': fields.char('Provincia', size=255),
      'zona': fields.char('Zona', size=255),
      'tiepi': fields.float('TIEPI'),
      'percentil80': fields.boolean('Percentil 80'),
    }

giscedata_qualitat_report_tiepi_percentil80()

class giscedata_qualitat_tiepi_anual(osv.osv):

    _name = 'giscedata.qualitat.tiepi.anual'
    _description = 'TIEPI Anual'

    _columns = {
      'name': fields.char('Any', size=4, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', ondelete='restrict', required=True),
      'municipis': fields.one2many('giscedata.qualitat.tiepi.anual.municipi', 'tiepi_id', 'Municipis'),
      'pr_tot_transport': fields.float('Total Transport', digits=(16,4)),
      'pr_tot_distribucio': fields.float('Total Districució', digits=(16,4)),
      'im_tot_generacio': fields.float('Total Generació', digits=(16,4)),
      'im_tot_transport': fields.float('Total Transport', digits=(16,4)),
      'im_tot_tercers': fields.float('Total Tercers', digits=(16,4)),
      'im_tot_major': fields.float('Total Major', digits=(16,4)),
      'im_tot_propies': fields.float('Total Pròpies', digits=(16,4)),
    }

    _defaults = {
      'pr_tot_transport': lambda *a: 0.0,
      'pr_tot_distribucio': lambda *a: 0.0,
      'im_tot_generacio': lambda *a: 0.0,
      'im_tot_transport': lambda *a: 0.0,
      'im_tot_tercers': lambda *a: 0.0,
      'im_tot_major': lambda *a: 0.0,
      'im_tot_propies': lambda *a: 0.0,
    }

    _order = "name, provincia desc"


giscedata_qualitat_tiepi_anual()

class giscedata_qualitat_tiepi_anual_municipi(osv.osv):

    _name = 'giscedata.qualitat.tiepi.anual.municipi'
    _description = 'Municipis del TIEPI Anual'

    def _pr_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,pr_transport+pr_distribucio from giscedata_qualitat_tiepi_anual_municipi where id in (%s)" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _im_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,im_generacio+im_transport+im_tercers+im_major+im_propies from giscedata_qualitat_tiepi_anual_municipi where id in (%s)" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _limit_zona(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("""
    select
            tam.id,
            q.tiepi_hores
    from
            giscedata_qualitat_tiepi_anual_municipi tam,
            giscedata_qualitat_tiepi_anual ta,
            giscedata_cts_zona_qualitat q
    where
            tam.tiepi_id = ta.id
            and tam.zona = q.zona_id
            and to_char(q.inici, 'YYYY-MM-DD') < (ta.name || '-12-31')
            and to_char(q.final, 'YYYY-MM-DD') >= (ta.name || '-12-31')
      and tam.id in (%s)""" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    _columns = {
      'tiepi_id': fields.many2one('giscedata.qualitat.tiepi.anual', 'TIEPI', ondelete='cascade', required=True),
      'municipi': fields.many2one('res.municipi', 'Municipi', ondelete='restrict', required=True),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', ondelete='restrict', required=True),
      'potencia_installada': fields.float('Pot. Instal·lada', digits=(16,4), required=True),
      'pr_transport': fields.float('Pr. Transport', digits=(16,4)),
      'pr_distribucio': fields.float('Pr. Distribució', digits=(16,4)),
      'pr_total': fields.function(_pr_total, method=True, string='Pr. Total', type='float', digits=(16,4)),
      'im_generacio': fields.float('Im. Generació', digits=(16,4)),
      'im_transport': fields.float('Im. Transport', digits=(16,4)),
      'im_tercers': fields.float('Im. Tercers', digits=(16,4)),
      'im_major': fields.float('Im. F. Major', digits=(16,4)),
      'im_propies': fields.float('Im. Pròpies', digits=(16,4)),
      'im_total': fields.function(_im_total, method=True, string='Im. Total', type='float', digits=(16,4)),
      'limit_zona': fields.function(_limit_zona, method=True, string="Limit Zona", type='float'),

    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

    _order = "municipi asc"

giscedata_qualitat_tiepi_anual_municipi()


class giscedata_qualitat_tiepi_mensual(osv.osv):

    _name = 'giscedata.qualitat.tiepi.mensual'
    _description = 'TIEPI Mensual'

    _columns = {
      'name': fields.char('Mes', size=7, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', ondelete='restrict', required=True),
      'municipis': fields.one2many('giscedata.qualitat.tiepi.mensual.municipi', 'tiepi_id', 'Municipis'),
    }

    _defaults = {
    }

    _order = "name, provincia desc"


giscedata_qualitat_tiepi_mensual()

class giscedata_qualitat_tiepi_mensual_municipi(osv.osv):

    _name = 'giscedata.qualitat.tiepi.mensual.municipi'
    _description = 'Municipis del TIEPI Mensual'

    def _pr_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,pr_transport+pr_distribucio from giscedata_qualitat_tiepi_mensual_municipi where id in (%s)" % (','.join(map(str,map(int,ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _im_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,im_generacio+im_transport+im_tercers+im_major+im_propies from giscedata_qualitat_tiepi_mensual_municipi where id in (%s)" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _limit_zona(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("""
    select
            tam.id,
            q.tiepi_hores
    from
            giscedata_qualitat_tiepi_mensual_municipi tam,
            giscedata_qualitat_tiepi_mensual ta,
            giscedata_cts_zona_qualitat q
    where
            tam.tiepi_id = ta.id
            and tam.zona = q.zona_id
            and to_char(q.inici, 'YYYY-MM-DD') < (substring(ta.name from 4 for 7) || '-12-31')
            and to_char(q.final, 'YYYY-MM-DD') >= (substring(ta.name from 4 for 7) || '-12-31')
      and tam.id in (%s)""" % (','.join(map(str,map(int,ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    _columns = {
      'tiepi_id': fields.many2one('giscedata.qualitat.tiepi.mensual', 'TIEPI', ondelete='cascade', required=True),
      'municipi': fields.many2one('res.municipi', 'Municipi', ondelete='restrict', required=True),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', ondelete='restrict', required=True),
      'potencia_installada': fields.float('Pot. Instal·lada', digits=(16,4), required=True),
      'pr_transport': fields.float('Pr. Transport', digits=(16,4)),
      'pr_distribucio': fields.float('Pr. Distribució', digits=(16,4)),
      'pr_total': fields.function(_pr_total, method=True, string='Pr. Total', type='float', digits=(16,4)),
      'im_generacio': fields.float('Im. Generació', digits=(16,4)),
      'im_transport': fields.float('Im. Transport', digits=(16,4)),
      'im_tercers': fields.float('Im. Tercers', digits=(16,4)),
      'im_major': fields.float('Im. F. Major', digits=(16,4)),
      'im_propies': fields.float('Im. Pròpies', digits=(16,4)),
      'im_total': fields.function(_im_total, method=True, string='Im. Total', type='float', digits=(16,4)),
      'limit_zona': fields.function(_limit_zona, method=True, string="Limit Zona", type='float'),
    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

    _order = "municipi asc"

giscedata_qualitat_tiepi_mensual_municipi()


class giscedata_qualitat_niepi_anual(osv.osv):

    _name = 'giscedata.qualitat.niepi.anual'
    _description = 'NIEPI Anual'

    _columns = {
      'name': fields.char('Any', size=4, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', ondelete='restrict', required=True),
      'municipis': fields.one2many('giscedata.qualitat.niepi.anual.municipi', 'niepi_id', 'Municipis'),
      'pr_tot_transport': fields.float('Total Transport', digits=(16,4)),
      'pr_tot_distribucio': fields.float('Total Districucio', digits=(16,4)),
      'im_tot_generacio': fields.float('Total Generació', digits=(16,4)),
      'im_tot_transport': fields.float('Total Transport', digits=(16,4)),
      'im_tot_tercers': fields.float('Total Tercers', digits=(16,4)),
      'im_tot_major': fields.float('Total Major', digits=(16,4)),
      'im_tot_propies': fields.float('Total Pròpies', digits=(16,4)),
    }

    _defaults = {
      'pr_tot_transport': lambda *a: 0.0,
      'pr_tot_distribucio': lambda *a: 0.0,
      'im_tot_generacio': lambda *a: 0.0,
      'im_tot_transport': lambda *a: 0.0,
      'im_tot_tercers': lambda *a: 0.0,
      'im_tot_major': lambda *a: 0.0,
      'im_tot_propies': lambda *a: 0.0,
    }

    _order = "name, provincia desc"


giscedata_qualitat_niepi_anual()

class giscedata_qualitat_niepi_anual_municipi(osv.osv):

    _name = 'giscedata.qualitat.niepi.anual.municipi'
    _description = 'Municipis del NIEPI Anual'

    def _pr_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,pr_transport+pr_distribucio from giscedata_qualitat_niepi_anual_municipi where id in (%s)" % (','.join(map(str,map(int,ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _im_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,im_generacio+im_transport+im_tercers+im_major+im_propies from giscedata_qualitat_niepi_anual_municipi where id in (%s)" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _limit_zona(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("""
    select
            tam.id,
            q.niepi
    from
            giscedata_qualitat_niepi_anual_municipi tam,
            giscedata_qualitat_niepi_anual ta,
            giscedata_cts_zona_qualitat q
    where
            tam.niepi_id = ta.id
            and tam.zona = q.zona_id
            and to_char(q.inici, 'YYYY-MM-DD') < (ta.name || '-12-31')
            and to_char(q.final, 'YYYY-MM-DD') >= (ta.name || '-12-31')
      and tam.id in (%s)""" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res


    _columns = {
      'niepi_id': fields.many2one('giscedata.qualitat.niepi.anual', 'TIEPI', ondelete='cascade', required=True),
      'municipi': fields.many2one('res.municipi', 'Municipi', ondelete='restrict', required=True),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', ondelete='restrict', required=True),
      'potencia_installada': fields.float('Pot. Instal·lada', digits=(16,4), required=True),
      'pr_transport': fields.float('Pr. Transport', digits=(16,4)),
      'pr_distribucio': fields.float('Pr. Distribució', digits=(16,4)),
      'pr_total': fields.function(_pr_total, method=True, string='Pr. Total', type='float', digits=(16,4)),
      'im_generacio': fields.float('Im. Generació', digits=(16,4)),
      'im_transport': fields.float('Im. Transport', digits=(16,4)),
      'im_tercers': fields.float('Im. Tercers', digits=(16,4)),
      'im_major': fields.float('Im. F. Major', digits=(16,4)),
      'im_propies': fields.float('Im. Pròpies', digits=(16,4)),
      'im_total': fields.function(_im_total, method=True, string='Im. Total', type='float', digits=(16,4)),
      'limit_zona': fields.function(_limit_zona, method=True, string="Limit Zona", type='float'),

    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

    _order = "municipi asc"

giscedata_qualitat_niepi_anual_municipi()

class giscedata_qualitat_niepi_mensual(osv.osv):

    _name = 'giscedata.qualitat.niepi.mensual'
    _description = 'NIEPI Mensual'

    _columns = {
      'name': fields.char('Mes', size=7, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', ondelete='restrict', required=True),
      'municipis': fields.one2many('giscedata.qualitat.niepi.mensual.municipi', 'niepi_id', 'Municipis'),
    }

    _defaults = {
    }

    _order = "name, provincia desc"


giscedata_qualitat_niepi_mensual()

class giscedata_qualitat_niepi_mensual_municipi(osv.osv):

    _name = 'giscedata.qualitat.niepi.mensual.municipi'
    _description = 'Municipis del NIEPI Mensual'

    def _pr_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,pr_transport+pr_distribucio from giscedata_qualitat_niepi_mensual_municipi where id in (%s)" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _im_total(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("select id,im_generacio+im_transport+im_tercers+im_major+im_propies from giscedata_qualitat_niepi_mensual_municipi where id in (%s)" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res

    def _limit_zona(self, cr, uid, ids, field_name, arg, context):
        res = {}
        cr.execute("""
    select
            tam.id,
            q.niepi
    from
            giscedata_qualitat_niepi_mensual_municipi tam,
            giscedata_qualitat_niepi_mensual ta,
            giscedata_cts_zona_qualitat q
    where
            tam.niepi_id = ta.id
            and tam.zona = q.zona_id
            and to_char(q.inici, 'YYYY-MM-DD') < (substring(ta.name from 4 for 7) || '-12-31')
            and to_char(q.final, 'YYYY-MM-DD') >= (substring(ta.name from 4 for 7) || '-12-31')
      and tam.id in (%s)""" % (','.join(map(str,map(int, ids)))))
        for r in cr.fetchall():
            res[r[0]] = r[1]
        return res


    _columns = {
      'niepi_id': fields.many2one('giscedata.qualitat.niepi.mensual', 'TIEPI', ondelete='cascade', required=True),
      'municipi': fields.many2one('res.municipi', 'Municipi', ondelete='restrict', required=True),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', ondelete='restrict', required=True),
      'potencia_installada': fields.float('Pot. Instal·lada', digits=(16,4), required=True),
      'pr_transport': fields.float('Pr. Transport', digits=(16,4)),
      'pr_distribucio': fields.float('Pr. Distribució', digits=(16,4)),
      'pr_total': fields.function(_pr_total, method=True, string='Pr. Total', type='float', digits=(16,4)),
      'im_generacio': fields.float('Im. Generació', digits=(16,4)),
      'im_transport': fields.float('Im. Transport', digits=(16,4)),
      'im_tercers': fields.float('Im. Tercers', digits=(16,4)),
      'im_major': fields.float('Im. F. Major', digits=(16,4)),
      'im_propies': fields.float('Im. Pròpies', digits=(16,4)),
      'im_total': fields.function(_im_total, method=True, string='Im. Total', type='float', digits=(16,4)),
      'limit_zona': fields.function(_limit_zona, method=True, string="Limit Zona", type='float'),
    }

    _defaults = {
      'pr_transport': lambda *a: 0.0,
      'pr_distribucio':lambda *a: 0.0,
      'im_generacio': lambda *a: 0.0,
      'im_transport': lambda *a: 0.0,
      'im_tercers': lambda *a: 0.0,
      'im_major': lambda *a: 0.0,
      'im_propies': lambda *a: 0.0,
    }

    _order = "municipi asc"

giscedata_qualitat_niepi_mensual_municipi()

class giscedata_qualitat_percentil80(osv.osv):
    _name = 'giscedata.qualitat.percentil80'
    _description = 'Percentil 80'

    _columns = {
      'name': fields.char('Any', size=4, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', required=True, ondelete='restrict'),
      'municipis': fields.one2many('giscedata.qualitat.percentil80.municipi', 'percentil_id', 'Municipis'),
    }

    _order = "name desc, provincia asc"

giscedata_qualitat_percentil80()

class giscedata_qualitat_percentil80_municipi(osv.osv):

    _name = 'giscedata.qualitat.percentil80.municipi'

    _columns = {
      'percentil_id': fields.many2one('giscedata.qualitat.percentil80', 'Any', ondelete='cascade', required=True),
      'municipi': fields.many2one('res.municipi', 'Municipi', required=True, ondelete='restrict'),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', required=True, ondelete='restrict'),
      'tiepi': fields.float('TIEPI', digits=(16,4)),
      'percentil80': fields.boolean('Percentil 80'),
    }

    _defaults = {
      'tiepi': lambda *a: 0.0,
      'percentil80': lambda *a: 0,
    }

    _order = 'zona,tiepi desc'

giscedata_qualitat_percentil80_municipi()

class giscedata_qualitat_interrupciobt_mensual(osv.osv):

    _name = 'giscedata.qualitat.interrupciobt.mensual'
    _description = 'Interrupció BT Mensual'

    _columns = {
      'name': fields.char('Mes', size=7, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', ondelete='restrict', required=True),
      'municipis': fields.one2many('giscedata.qualitat.interrupciobt.mensual.municipi', 'interrupciobt_id', 'Municipis'),
    }

    _defaults = {
    }

    _order = "name, provincia desc"

giscedata_qualitat_interrupciobt_mensual()

class giscedata_qualitat_interrupciobt_menusal_municipi(osv.osv):

    _name = 'giscedata.qualitat.interrupciobt.mensual.municipi'

    _columns = {
      'interrupciobt_id': fields.many2one('giscedata.qualitat.interrupciobt.mensual', 'Interrupció BT', required=True, ondelete='cascade'),
      'municipi': fields.many2one('res.municipi', 'Municipi', ondelete='restrict', required=True),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', ondelete='restrict', required=True),
      'n_imprevistes': fields.integer('Imprevistes', required=True),
      'n_programades': fields.integer('Programades', required=True),
      'n_total': fields.integer('Total', required=True, readonly=True),
    }

    _defaults = {
      'n_imprevistes': lambda *a: 0,
      'n_programades': lambda *a: 0,
      'n_total': lambda *a: 0,
    }

giscedata_qualitat_interrupciobt_menusal_municipi()


class giscedata_qualitat_interrupciobt_anual(osv.osv):

    _name = 'giscedata.qualitat.interrupciobt.anual'
    _description = 'Interrupció BT Anual'

    _columns = {
      'name': fields.char('Mes', size=7, required=True),
      'provincia': fields.many2one('res.country.state', 'Provincia', ondelete='restrict', required=True),
      'municipis': fields.one2many('giscedata.qualitat.interrupciobt.anual.municipi', 'interrupciobt_id', 'Municipis'),
    }

    _defaults = {
    }

    _order = "name, provincia desc"

giscedata_qualitat_interrupciobt_anual()

class giscedata_qualitat_interrupciobt_anual_municipi(osv.osv):

    _name = 'giscedata.qualitat.interrupciobt.anual.municipi'

    _columns = {
      'interrupciobt_id': fields.many2one('giscedata.qualitat.interrupciobt.anual', 'Interrupció BT', required=True, ondelete='cascade'),
      'municipi': fields.many2one('res.municipi', 'Municipi', ondelete='restrict', required=True),
      'zona': fields.many2one('giscedata.cts.zona', 'Zona', ondelete='restrict', required=True),
      'n_imprevistes': fields.integer('Imprevistes', required=True),
      'n_programades': fields.integer('Programades', required=True),
      'n_total': fields.integer('Total', required=True, readonly=True),
    }

    _defaults = {
      'n_imprevistes': lambda *a: 0,
      'n_programades': lambda *a: 0,
      'n_total': lambda *a: 0,
    }

giscedata_qualitat_interrupciobt_anual_municipi()

class GiscedataQualitatIndividualTiepi(osv.osv):

    _name = 'giscedata.qualitat.individual.tiepi'
    _description = 'TIEPI Qualitat individual'

    _columns = {
        'name': fields.char('Any', size=4, required=True),
        'linies': fields.one2many('giscedata.qualitat.individual.tiepi.linia',
                                  'year', 'Linies'),
    }

    _defaults = {
    }

    _order = "name desc"

GiscedataQualitatIndividualTiepi()

class GiscedataQualitatIndividualTiepiLinia(osv.osv):

    _name = 'giscedata.qualitat.individual.tiepi.linia'
    _description = 'TIEPI Qualitat individual'

    def _ff_calc_limit(self, cursor, uid, ids, field, arg, context=None):
        res = {}

        obj_zq = self.pool.get('giscedata.cts.zona.qualitat')

        for linia in self.browse(cursor, uid, ids):
            any_linia = int(linia.year.name)
            id_zq = obj_zq.search(cursor, uid, [('zona_id', '=',
                                                linia.zona.id),
                                                ('final', '>',
                                                 str(any_linia)+'-01-01'),
                                                ('inici', '<=',
                                                 str(any_linia)+'-01-01')])

            hores = obj_zq.read(cursor, uid, id_zq[0], ['at_hores', 'bt_hores'])
            if linia.tarifa.startswith('3.1') or linia.tarifa.startswith('6'):
                res[linia.id] = hores['at_hores'] * 3600
            else:
                res[linia.id] = hores['bt_hores'] * 3600

        return res

    def _ff_dret_descompte(self, cursor, uid, ids, field, arg, context=None):
        res = {}

        for id in ids:
            obj_temps = self.read(cursor, uid, id, ['limit_temps_zona',
                                                          'temps_total_inter',
                                                          'span_ids'])
            if obj_temps['limit_temps_zona'] > obj_temps['temps_total_inter']:
                res[id] = 0
                if obj_temps['span_ids']:
                    span_obj = self.pool.get(
                        'giscedata.qualitat.individual.tiepi.span')
                    span_obj.write(
                        cursor, uid, obj_temps['span_ids'],
                        {'dret_descompte': 0})
            else:
                res[id] = 1
        return res

    _columns = {
        'year': fields.many2one('giscedata.qualitat.individual.tiepi',
                                'Any', required=True, select=1,
                                ondelete='cascade'),
        'cups': fields.char('CUPS', size=50),
        'polissa': fields.char('Polissa', size=50),
        'tarifa': fields.char('Tarifa', size=10),
        'zona': fields.many2one('giscedata.cts.zona', 'Zona',
                                ondelete='restrict', required=True),
        'limit_temps_zona': fields.function(_ff_calc_limit, type='float',
                                            method=True,
                                            store=True,
                                            string='Limit Temps de la Zona'),
        'tipus_tall': fields.char('Tipus de Interrupció', size=50),
        'temps_total_inter': fields.integer('Temps Total Interrupció'),
        'dret_descompte': fields.function(_ff_dret_descompte, type='boolean',
                                          method=True, size=10,
                                          store=True,
                                          string='Dret a descompte', select=1),
        'span_ids': fields.one2many('giscedata.qualitat.individual.tiepi.span',
                                    'linia', 'Intervals')
    }

    _defaults = {
    }

    _order = "temps_total_inter desc"

GiscedataQualitatIndividualTiepiLinia()


class GiscedataQualitatIndividualTiepiSpan(osv.osv):
    _name = 'giscedata.qualitat.individual.tiepi.span'
    _description = 'TIEPI Qualitat individual interval'

    def _ff_dret_descompte(self, cursor, uid, ids, field, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, 0)
        fields = ['tipus_code', 'causa_code']
        for obj_temps in self.read(cursor, uid, ids, fields):
            if obj_temps['tipus_code'] != 1 and obj_temps['causa_code'] == 7:
                res[obj_temps['id']] = 1
        return res

    _columns = {
        'linia': fields.many2one(
            'giscedata.qualitat.individual.tiepi.linia',
            u'Línia', required=True, select=1, ondelete='cascade'),
        'nom_incidencia': fields.char(u'Nom incidència', size=50),
        'tipus_tall': fields.char('Tipus de Interrupció', size=50),
        'tipus_code': fields.integer(u'Codi tipus interrupció'),
        'causa_nom': fields.char(u'Causa interrupció', size=50),
        'causa_code': fields.integer(u'Codi causa interrupció'),
        'data_inici': fields.datetime('Data i hora inici'),
        'data_final': fields.datetime('Data i hora final'),
        'duracio': fields.integer(u'Duració'),
        'dret_descompte': fields.boolean(string='Dret a descompte', select=1),
    }

GiscedataQualitatIndividualTiepiSpan()


class GiscedataQualitatIndividualNiepi(osv.osv):

    _name = 'giscedata.qualitat.individual.niepi'
    _description = 'NIEPI Qualitat individual'

    _columns = {
        'name': fields.char('Any', size=4, required=True),
        'linies': fields.one2many('giscedata.qualitat.individual.niepi.linia',
                                  'year', 'Linies'),
    }

    _defaults = {
    }

    _order = "name desc"

GiscedataQualitatIndividualNiepi()

class GiscedataQualitatIndividualNiepiLinia(osv.osv):

    _name = 'giscedata.qualitat.individual.niepi.linia'
    _description = 'NIEPI Qualitat individual'

    def _ff_calc_limit(self, cursor, uid, ids, field, arg, context=None):
        res = {}

        obj_zq = self.pool.get('giscedata.cts.zona.qualitat')

        for linia in self.browse(cursor, uid, ids):
            any_linia = int(linia.year.name)
            id_zq = obj_zq.search(cursor, uid, [('zona_id', '=',
                                                linia.zona.id),
                                                ('final', '>',
                                                 str(any_linia)+'-01-01'),
                                                ('inici', '<=',
                                                 str(any_linia)+'-01-01')])

            talls = obj_zq.read(cursor, uid, id_zq[0], ['at_interrupcions',
                                                        'bt_interrupcions'])
            if linia.tarifa.startswith('3.1') or linia.tarifa.startswith('6'):
                res[linia.id] = talls['at_interrupcions']
            else:
                res[linia.id] = talls['bt_interrupcions']

        return res

    def _ff_dret_descompte(self, cursor, uid, ids, field, arg, context=None):
        res = {}

        for obj_talls in self.read(cursor, uid, ids, ['limit_talls_zona',
                                                      'numero_total_inter',
                                                      'span_ids']):
            if obj_talls['limit_talls_zona'] > obj_talls['numero_total_inter']:
                dret_descompte = 0
            else:
                dret_descompte = 1
            res[obj_talls['id']] = dret_descompte
            if not dret_descompte and obj_talls['span_ids']:
                    span_obj = self.pool.get(
                        'giscedata.qualitat.individual.niepi.span')
                    span_obj.write(
                        cursor, uid, obj_talls['span_ids'],
                        {'dret_descompte': dret_descompte})
        return res

    _columns = {
        'year': fields.many2one('giscedata.qualitat.individual.niepi',
                                'Any', required=True,
                                ondelete='cascade'),
        'cups': fields.char('CUPS', size=50),
        'polissa': fields.char('Polissa', size=50),
        'tarifa': fields.char('Tarifa', size=10),
        'zona': fields.many2one('giscedata.cts.zona', 'Zona',
                                ondelete='restrict', required=True),
        'limit_talls_zona': fields.function(_ff_calc_limit, type='integer',
                                            method=True,
                                            store=True,
                                            string='Limit Número de Talls de '
                                                   'la Zona'),
        'tipus_tall': fields.char('Tipus de Interrupció', size=50),
        'numero_total_inter': fields.integer('Número Total Interrupcions'),
        'dret_descompte': fields.function(_ff_dret_descompte, type='boolean',
                                          method=True, size=10,
                                          store=True,
                                          string='Dret a descompte', select=1),
        'span_ids': fields.one2many('giscedata.qualitat.individual.niepi.span',
                                    'linia', 'Intervals')
    }

    _defaults = {
    }

    _order = "numero_total_inter desc"

GiscedataQualitatIndividualNiepiLinia()

class GiscedataQualitatIndividualNiepiSpan(osv.osv):
    _name = 'giscedata.qualitat.individual.niepi.span'
    _description = 'TIEPI Qualitat individual interval'

    def _ff_dret_descompte(self, cursor, uid, ids, field, arg, context=None):
        if not context:
            context = {}
        res = dict.fromkeys(ids, 0)
        fields = ['tipus_code', 'causa_code']
        for obj_temps in self.read(cursor, uid, ids, fields):
            if obj_temps['tipus_code'] != 1 and obj_temps['causa_code'] == 7:
                res[obj_temps['id']] = 1
        return res

    _columns = {
        'linia': fields.many2one(
            'giscedata.qualitat.individual.niepi.linia',
            u'Línia', required=True, select=1, ondelete='cascade'),
        'nom_incidencia': fields.char(u'Nom incidència', size=50),
        'tipus_tall': fields.char('Tipus de Interrupció', size=50),
        'tipus_code': fields.integer(u'Codi tipus interrupció'),
        'causa_nom': fields.char(u'Causa interrupció', size=50),
        'causa_code': fields.integer(u'Codi causa interrupció'),
        'data_inici': fields.datetime('Data i hora inici'),
        'data_final': fields.datetime('Data i hora final'),
        'duracio': fields.integer(u'Duració'),
        'dret_descompte': fields.boolean(string='Dret a descompte', select=1),
    }


GiscedataQualitatIndividualNiepiSpan()


class ResMunicipi(osv.osv):
    _name = 'res.municipi'
    _inherit = 'res.municipi'

    _columns = {
         "zona": fields.many2one("giscedata.cts.zona", "Zona"),
    }


ResMunicipi()

