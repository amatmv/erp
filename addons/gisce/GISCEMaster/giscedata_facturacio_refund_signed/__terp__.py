# -*- coding: utf-8 -*-
{
    "name": "Factures Abonadores amb signe",
    "description": """Afegeix el signe negatiu a les factures abonadores""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "account_invoice_refund_signed",
        "giscedata_facturacio"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "giscedata_facturacio_view.xml"
    ],
    "active": False,
    "installable": True
}
