# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataCts(osv.osv):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_cts_rel',
            'ct_id',
            'obra_id',
            'Obres'
        )
    }

GiscedataCts()