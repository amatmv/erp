# -*- coding: utf-8 -*-
{
    "name": "Obres per CT's",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a CT's
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cts",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cts_view.xml"
    ],
    "active": False,
    "installable": True
}
