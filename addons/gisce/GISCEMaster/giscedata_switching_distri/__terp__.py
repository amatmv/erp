# -*- coding: utf-8 -*-
{
    "name": "Gestión ATR (Distribución)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Wizard to create a Work order from a ATR Case
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_polissa_crm",
        "giscedata_switching"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_switching_view.xml",
        "crm_data.xml"
    ],
    "active": False,
    "installable": True
}
