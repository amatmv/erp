# -*- coding: utf-8 -*-
from osv import osv, fields, orm

AUTOMATIZATION_PROCESS = {}


class GiscedataSwitching(osv.osv):
    _name = 'giscedata.switching'
    _inherit = "giscedata.switching"

    def get_defaults_values(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        model = context.get('model', False)
        result = {}
        if model == 'giscedata.switching':
            sw_obj = self.pool.get('giscedata.switching')
            sw_step_obj = self.pool.get('giscedata.switching.step')
            fields_to_read = ['step_id', 'additional_info']
            sw_data = sw_obj.read(cursor, uid, sw_id, fields_to_read)
            step_id = sw_data['step_id'][0]
            step = sw_step_obj.browse(cursor, uid, step_id)
            key = '{process}-{step}'.format(
                process=step.proces_id.name.upper(),
                step=step.name.upper()
            )
            automatic_process = AUTOMATIZATION_PROCESS.get(key, None)
            if automatic_process:
                step_model = step.get_step_model()
                pas_id = self.pool.get(step_model).search(
                    cursor, uid, [('sw_id', '=', sw_id)]
                )[0]
                pas = self.pool.get(step_model).browse(cursor, uid, pas_id)
                conditions = automatic_process.get('conditions')
                values = {}
                if not conditions:
                    values = automatic_process.get('default', {})
                else:
                    for condition in conditions:
                        model_value = getattr(pas, condition['field_name'])
                        if model_value == condition['value']:
                            values = {
                                'ot_section_code': condition['ot_section_code'],
                                'title': condition['title'],
                            }
                if values.get('title'):
                    result.update(
                        {
                            'title': values.get('title').format(
                                sw_data['additional_info'])
                        }
                    )
                    description = result.get("title", "")
                    if pas.comentaris:
                        description = "{0}\n\n{1}".format(
                            description, pas.comentaris
                        )
                    if pas.document_ids:
                        for doc in pas.document_ids:
                            description = "{0}\n\n{1}".format(
                                description, doc.url
                            )
                    result.update({'description': description})
                if values.get('ot_section_code'):
                    crm_sec_obj = self.pool.get('crm.case.section')
                    section_id = crm_sec_obj.search(
                        cursor, uid,
                        [('code', '=', values.get('ot_section_code'))]
                    )
                    if section_id:
                        user_id = crm_sec_obj.read(
                            cursor, uid, section_id[0], ['user_id']
                        )
                        result.update({
                            'section_id': section_id[0],
                        })
                        if user_id['user_id']:
                            result.update({
                                'user_id': user_id['user_id'][0]
                            })
        return result

    def activa_polissa_cas_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitching, self).activa_polissa_cas_atr(
                      cursor, uid, sw_id, context=context)
        ctx = context.copy()
        ctx['model'] = 'giscedata.switching'
        automatic_values = self.get_defaults_values(cursor, uid, sw_id, context=ctx)
        if automatic_values:
            crm_obj = self.pool.get('crm.case')
            automatic_values['name'] = automatic_values['title']
            automatic_values['description'] = automatic_values.get(
                'description', automatic_values['name']
            )
            crm_obj.create_case_generic(
                cursor, uid, [sw_id], context=ctx, extra_vals=automatic_values
            )
        return res

GiscedataSwitching()
