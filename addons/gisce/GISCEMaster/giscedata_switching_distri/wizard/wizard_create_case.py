# -*- encoding: utf-8 -*-
from osv import osv

class WizardCreateCase(osv.osv_memory):

    _name = 'wizard.create.case'
    _inherit = 'wizard.create.case'

    def default_get(self, cursor, uid, fields, context=None):
        result = super(WizardCreateCase, self).default_get(cursor, uid, fields, context)
        sw_id = context.get('active_ids', False)
        if len(sw_id) == 1:
            sw_obj = self.pool.get('giscedata.switching')
            result.update(
                sw_obj.get_defaults_values(cursor, uid, sw_id[0], context=context)
            )
        return result


WizardCreateCase()
