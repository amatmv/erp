from osv import osv
from tools.translate import _

OT_ATR_CONNECTION = {}
"""
Structure of OT_ATR_CONNECTION:
{
    <new_ot_state>:
        [
         // List with conditions to acomplish
         // and ATR steps to create and new atr states to pass
         // if conditions are acomplished
            {
                // Conditions
                'ot_old_state': current state of OT. Empty means any state.
                'ot_sections': list of OT sections. OT must be of one of this sections.
                'related_ot_states': [
                    list of ot states
                    // Related OTs must be in one of these states.
                    // If it's an empty list it's ignored.
                    // A related OT is any OT returned by method 'get_related_ots'.
                    // By defrault it returns an empty list, it must be
                    // implemented in custom modules.
                ]
                'required_steps': {
                    // Used to define for each proces wich steps must exist.
                    // If a reject step is required, then the step must be
                    // written with "_rej" at the end. For example: '02_rej'.
                    // If the proces has no entry it will check the ones defined in 'default'.
                    // 'default' entry must exist even if its for an empty list.
                    'default': [list of steps],
                    <ATR_proces>: [list of steps]
                }
                // Actions
                'atr_state': pass the ATR case to this state
                'atr_steps': {
                    // Used to define for each proces wich steps will be created.
                    // If a reject step must be created, then the step must be
                    // written with "_rej" at the end. For example: '02_rej'.
                    // The specific error code wanted in the reject can be specified with
                    // '-<code>' at the end, for example: '04_rej-42'
                    // If the proces has no entry it will create the ones defined in 'default'.
                    // 'default' entry must exist even if its for an empty list.
                    'default': [list of steps],
                    <ATR_proces>: [list of steps]
                }
            }
        ]
}
"""


class CrmCase(osv.osv):

    _name = 'crm.case'
    _inherit = 'crm.case'

    def create_case_generic(self, cursor, uid, model_ids, context=None,
                                  description='Generic Case',
                                  section='CASE',
                                  category=None,
                                  assign=None,
                                  extra_vals=None):
        sw_obj = self.pool.get('giscedata.switching')

        case_ids = super(CrmCase, self).create_case_generic(
                                  cursor, uid, model_ids, context,
                                  description, section,
                                  category, assign, extra_vals)

        if not context:
            context = {}
        model = context.get('model', False)

        if model == 'giscedata.switching':
            for case in self.browse(cursor, uid, case_ids, context):
                ref_spl = case.ref.split(',')
                if len(ref_spl) == 2:
                    sw_id = int(ref_spl[1])
                    sw = sw_obj.browse(cursor, uid, sw_id)
                    polissa = sw.cups_polissa_id
                    vals = {'polissa_id': polissa.id}
                    if polissa.comptadors:
                        vals['ref2'] = '{0},{1}'.format(
                            'giscedata.lectures.comptador',
                            polissa.comptadors[0].id
                        )
                    case.write(vals)

        return case_ids

    def proces_ot_atr_connection(self, cursor, uid, ids, vals, context=None):
        ot_atr_info = OT_ATR_CONNECTION.get(vals['state'], [])
        if not ot_atr_info:
            return
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        mrej_obj = self.pool.get("giscedata.switching.motiu.rebuig")
        for ot_info in self.read(cursor, uid, ids, ['ref']):
            if ot_info['id'] in context.get("ignore_ot_atr_connection", []):
                continue
            # check if the case has referenced an atr case
            if not ot_info.get('ref') or not ot_info.get('ref').split(",")[0] == "giscedata.switching":
                continue
            sw = sw_obj.browse(cursor, uid, int(ot_info['ref'].split(",")[1]))
            proces = sw.proces_id.name
            ot_id = ot_info['id']
            # check which steps have to be created and the state to pass
            for step_info in ot_atr_info:
                try:
                    # Check conditions
                    if ot_id in context.get('ignore_ot_atr_connection', []):
                        continue
                    if not self.check_ot_old_state(cursor, uid, step_info, ot_id):
                        continue
                    if not self.check_ot_sections(cursor, uid, step_info, ot_id):
                        continue
                    if not self.check_related_ot_states(cursor, uid, step_info, ot_id):
                        continue
                    if not self.check_required_steps(cursor, uid, step_info, ot_id):
                        continue

                except osv.except_osv, e:
                    self.historize_msg(cursor, uid, ot_info['id'], unicode(e.value), context)
                    raise e

                # Create steps
                for step_to_create in step_info['atr_steps'].get(proces, step_info['atr_steps']['default']):
                    reject = False
                    code = []

                    if len(step_to_create.split("-")) > 1:
                        step_to_create, code = step_to_create.split("-")
                        rid = mrej_obj.search(cursor, uid, [('name', '=', code)])
                        if len(rid):
                            rej = mrej_obj.browse(cursor, uid, rid[0])
                            code = [rej]

                    if step_to_create.endswith('_rej'):
                        step_to_create = step_to_create[:-4]
                        reject = True

                    # get step model and create step
                    step_model = "giscedata.switching.{0}.{1}".format(
                        proces.lower(), step_to_create
                    )
                    step_obj = self.pool.get(step_model)
                    if not step_obj:
                        continue

                    if sw_obj.pas_inesperat(cursor, uid, sw.id, step_to_create):
                        msg = _(u"{0}-{1} no creat: el pas {1} no es un pas esperat.\n").format(proces, step_to_create)
                        self.historize_msg(cursor, uid, ot_info['id'], msg, context)
                        continue

                    try:
                        if reject and getattr(step_obj, 'dummy_create_reb', False):
                            pas_id = step_obj.dummy_create_reb(cursor, uid, sw.id, code, context)
                        else:
                            pas_id = step_obj.dummy_create(cursor, uid, sw.id, context)

                        header_id = step_obj.read(cursor, uid, pas_id, ['header_id'])
                        header_obj.write(cursor, uid, header_id['header_id'][0], {'enviament_pendent': True})
                        sw_obj.create_step(cursor, uid, sw.id, step_to_create, pas_id, context=context)
                        msg = _(u"{0}-{1} creat correctament.\n").format(proces, step_to_create)
                    except osv.except_osv, e:
                        msg = _(u"{0}-{1} no creat: {2}.\n").format(proces, step_to_create, unicode(e.value))

                    self.historize_msg(cursor, uid, ot_info['id'], msg, context)

                # write atr new state
                if step_info['atr_state']:
                    sw_obj.write(cursor, uid, sw.id, {'state': step_info['atr_state']})

    def check_ot_old_state(self, cursor, uid, step_info, ot_id, context=None):
        ot_state = self.read(cursor, uid, ot_id, ['state'])['state']
        return not step_info.get('ot_old_state', False) or ot_state == step_info.get('ot_old_state', False)

    def check_ot_sections(self, cursor, uid, step_info, ot_id, context=None):
        section_obj = self.pool.get('crm.case.section')
        section_id = self.read(cursor, uid, ot_id, ['section_id'])['section_id'][0]
        section_code = section_obj.read(cursor, uid, section_id, ['code'])['code']
        return not step_info.get('ot_sections', False) or section_code in step_info.get('ot_sections', [])

    def check_related_ot_states(self, cursor, uid, step_info, ot_id, context=None):
        required_states = step_info.get('related_ot_states', [])
        if not required_states:
            return True
        related_ot_ids = self.get_related_ots(cursor, uid, ot_id)
        for related_ot_inf in self.read(cursor, uid, related_ot_ids, ['state']):
            if related_ot_inf['state'] not in required_states:
                return False
        return True

    def check_required_steps(self, cursor, uid, step_info, ot_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        ot_info = self.read(cursor, uid, ot_id, ['ref'])
        sw = sw_obj.browse(cursor, uid, int(ot_info['ref'].split(",")[1]))
        proces = sw.proces_id.name
        steps = [x.step_id.name for x in sw.step_ids]
        for step_name in step_info['required_steps'].get(proces, step_info['required_steps']['default']):
            if step_name.endswith('_rej') and (step_name[:-4] not in steps or not sw.rebuig):
                raise osv.except_osv(
                    "Error",
                    _(u"No existeix {0}-{1} de rebuig, no es realitzen els canvis al ATR associat.")
                    .format(proces, step_name[:2])
                )
            elif not step_name.endswith('_rej') and (step_name not in steps or sw.rebuig):
                raise osv.except_osv(
                    "Error",
                    _(u"No existeix {0}-{1}, no es realitzen els canvis al ATR associat.")
                    .format(proces, step_name)
                )
        return True

    def historize_msg(self, cursor, uid, ids, msg, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        self.case_log(cursor, uid, ids, context)
        self.write(cursor, uid, ids, {'description': msg})
        self.case_log(cursor, uid, ids, context)

    def write(self, cr, uid, ids, vals, context=None):
        if 'state' in vals.keys():
            self.proces_ot_atr_connection(cr, uid, ids, vals, context)
        return super(CrmCase, self).write(cr, uid, ids, vals, context=context)

    def get_related_ots(selfcursor, uid, ot_id, context=None):
        """ To be implemented in other modules """
        return []

CrmCase()
