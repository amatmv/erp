# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscedata_switching.tests.common_tests import TestSwitchingImport

from destral.transaction import Transaction
from addons import get_module_resource
from destral import testing
from expects import expect, raise_error
from osv.orm import except_orm
from datetime import datetime, timedelta
from workdays import *


class TestC1Distri(TestSwitchingImport):
    def set_final_date_last_modcon(self, cursor, uid, polissa_id, date_str):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        mod_obj = self.openerp.pool.get('giscedata.polissa.modcontractual')
        mod_id = pol_obj.read(
            cursor, uid, polissa_id, ['modcontractual_activa']
        )['modcontractual_activa'][0]

        mod_obj.write(cursor, uid, mod_id, {'data_final': date_str})

    def test_c1_02_change_comer_and_generate_05(self):
        sw_help_dist_obj = self.openerp.pool.get(
            'giscedata.switching.helpers.distri'
        )

        c1_01_obj = self.openerp.pool.get('giscedata.switching.c1.01')
        c1_05_obj = self.openerp.pool.get('giscedata.switching.c1.05')

        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)
            sw_id = res[0]

            # Use wizard to create step 02, this will create step 11 too
            wiz_create_step_obj = self.openerp.pool.get("wizard.create.step")
            wiz_create_step_id = wiz_create_step_obj.create(cursor, uid, {},
                                                            context={
                                                                'active_ids': [
                                                                    sw_id]})
            wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id],
                                      {'step': '02'})
            wiz_create_step_obj.action_create_steps(cursor, uid,
                                                    [wiz_create_step_id],
                                                    context={
                                                        'active_ids': [sw_id]})

            pas_01_id = c1_01_obj.search(cursor, uid, [('sw_id', '=', sw_id)])[0]

            # PAS 01
            pas_01 = c1_01_obj.browse(cursor, uid, pas_01_id)
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertNotEqual(
                pas_01.emisor_id.id, sw.cups_polissa_id.comercialitzadora.id
            )
            pas_01.emisor_id.write({'payment_type_customer': 2})

            imd_obj = self.openerp.pool.get('ir.model.data')
            lect_obj = self.openerp.pool.get('giscedata.lectures.lectura')

            first_lecture_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'lectura_0001'
            )[1]

            data_primera_lectura_str = lect_obj.read(
                cursor, uid, first_lecture_id, ['name']
            )['name']

            data_primera = datetime.strptime(
                data_primera_lectura_str, '%Y-%m-%d'
            )
            today = datetime.today()

            diff_days = (today - data_primera).days

            ctx = {'days_before': diff_days, 'lang': 'es_ES'}

            self.set_final_date_last_modcon(
                cursor, uid, sw.cups_polissa_id.id,
                datetime.strftime(today, '%Y-%m-%d'))

            sw_help_dist_obj.activa_polissa_from_cn(cursor, uid, sw_id, context=ctx)

            sw = sw_obj.browse(cursor, uid, sw_id)
            polissa = sw.cups_polissa_id
            comer = pas_01.emisor_id
            self.assertEqual(comer.id, polissa.comercialitzadora.id)
            self.assertEqual(comer.id, polissa.pagador.id)
            self.assertEqual(polissa.pagador_sel, "comercialitzadora")
            self.assertEqual(comer.payment_type_customer.id, polissa.tipo_pago.id)

            pas_05_id = c1_05_obj.search(
                cursor, uid, [('sw_id', '=', sw_id)]
            )[0]

            self.assertTrue(pas_05_id)
