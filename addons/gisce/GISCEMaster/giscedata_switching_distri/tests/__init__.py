# -*- coding: utf-8 -*-
from destral.transaction import Transaction
from addons import get_module_resource
from giscedata_switching.tests.common_tests import TestSwitchingImport

from test_a3 import *
from test_c1_distri import *

TEST_NULLIFIER_OT_ATR_CONNECTION = {
    'open': [
        {
            'ot_old_state': '',  # empty old state means any old state
            'ot_sections': ['AR', 'OTA', 'OTB', 'OTF', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['02']},
            'atr_state': 'open',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['11']},
            'atr_state': 'open',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF', 'OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': []},
            'atr_state': 'open',
        }
    ],
    'pending': [
        {
            'ot_old_state': '',
            'ot_sections': ['AR', 'OTA', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['03']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['OTF', 'OTB', 'OTC'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['06']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF', 'OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': []},
            'atr_state': 'pending',
        }
    ],
    'done': [
        {
            'ot_old_state': '',
            'ot_sections': ['OTC', 'OTL', 'OTT'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['02']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['AR', 'OTA', 'OTB', 'OTF', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': ['02']},
            'atr_steps': {'default': ['05']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ICML'],
            'required_steps': {'default': ['02']},
            'related_ot_states': [],
            'atr_steps': {'default': ['06']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'C1': ['09', '10'], 'C2': ['09', '10'], 'A3': ['07'], 'M1': ['07'], 'B1': ['04']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANICML'],
            'related_ot_states': ['open', 'pending', 'done', 'cancel'],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'C1': ['09', '10'], 'C2': ['09', '10']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANICML'],
            'related_ot_states': ['draft'],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'C1': ['09'], 'C2': ['09']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [], 'B1': ['04']},
            'atr_state': '',
        },
    ],
    'cancel': [
        {
            'ot_old_state': 'draft',
            'ot_sections': [],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['02_rej']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'pending',
            'ot_sections': ['OTF', 'OTB', 'OTC'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['07']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'pending',
            'ot_sections': ['AR', 'OTA', 'ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['04_rej']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': 'pending',
            'ot_sections': ['ICML'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': ['12']},
            'atr_state': 'pending',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['ANAR', 'ANICML', 'ANRIAM', 'ANOTA', 'ANRCAM', 'ANOTB', 'ANOTT', 'ANOTF'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [''], 'C1': ['09_rej-42'], 'C2': ['09_rej-42'], 'A3': ['07_rej-42'], 'M1': ['07_rej-42'], 'B1': ['04_rej-42']},
            'atr_state': 'draft',
        },
        {
            'ot_old_state': '',
            'ot_sections': ['OTR'],
            'related_ot_states': [],
            'required_steps': {'default': []},
            'atr_steps': {'default': [''], 'C1': ['09_rej-43'], 'C2': ['09_rej-43'], 'A3': ['07_rej-43'], 'M1': ['07_rej-43'], 'B1': ['04_rej-43']},
            'atr_state': '',
        },
    ]
}


class BaseTest(TestSwitchingImport):

    def test_proces_ot_atr_connection(self):
        sw_obj = self.openerp.pool.get('giscedata.switching')
        case_obj = self.openerp.pool.get('crm.case')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            cursor = txn.cursor
            uid = txn.user

            # Define that when a draft crm is open a reject step 02 is generated
            # and the atr is passed to pending
            from giscedata_switching_distri.crm import OT_ATR_CONNECTION
            OT_ATR_CONNECTION.update({
                'open': [
                    {
                        'ot_old_state': '',
                        'ot_sections': [],
                        'related_ot_states': [],
                        'required_steps': {'default': []},
                        'atr_steps': {'default': ['02'], 'R1': ['02_rej-42']},
                        'atr_state': 'pending',
                    }
                ],
                'pending': [
                    {
                        'ot_old_state': 'pending',
                        'ot_sections': [],
                        'related_ot_states': [],
                        'required_steps': {'default': ['10'], 'A3': []},
                        'atr_steps': {'default': [], 'B1': ['02'], 'A3': ['05']},
                        'atr_state': 'pending',
                    },
                    {
                        'ot_old_state': 'open',
                        'ot_sections': [],
                        'related_ot_states': [],
                        'required_steps': {'default': ['10'], 'A3': []},
                        'atr_steps': {'default': [], 'B1': ['02'], 'A3': ['04']},
                        'atr_state': 'done',
                    }
                ],
                'done': [
                    {
                        'ot_old_state': '',
                        'ot_sections': [],
                        'related_ot_states': [],
                        'required_steps': {'default': [], 'A3': ['07_rej']},
                        'atr_steps': {'default': []},
                        'atr_state': 'pending',
                    }
                ],
            })

            # Create an ATR case
            a3_xml_path = get_module_resource('giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
            with open(a3_xml_path, 'r') as f:
                a3_xml = f.read()
            sw_id, _ = sw_obj.importar_xml(cursor, uid, a3_xml, 'a301.xml')

            # Create an OT, the OT must have referenced the atr case
            case_id = case_obj.create(cursor, uid, {
                'name': "Ordre de Treball",
                'ref': 'giscedata.switching, {0}'.format(sw_id),
                'section_id': 1,
            })

            case = case_obj.browse(cursor, uid, case_id)
            sw = sw_obj.browse(cursor, uid, sw_id)

            # Check init states
            self.assertEqual(case.state, 'draft')
            self.assertEqual(case.ref, 'giscedata.switching, {0}'.format(sw.id))
            self.assertEqual(sw.state, 'open')
            self.assertEqual(sw.step_id.name, '01')
            self.assertEqual(sw.proces_id.name, 'A3')

            # Set crm case state to 'open'
            case.write({'state': 'open'})
            # Check if the changes have been made
            case = case_obj.browse(cursor, uid, case_id)
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(case.state, 'open')
            self.assertEqual(sw.state, 'pending')
            self.assertEqual(sw.step_id.name, '02')
            self.assertEqual(sw.proces_id.name, 'A3')
            self.assertFalse(sw.rebuig)

            # Set crm case state to 'pending'
            case.write({'state': 'pending'})
            # Check if correct changes have been made
            case = case_obj.browse(cursor, uid, case_id)
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(case.state, 'pending')
            self.assertEqual(sw.state, 'done')
            self.assertEqual(sw.step_id.name, '04')
            self.assertEqual(sw.proces_id.name, 'A3')
            self.assertTrue(sw.rebuig)

            # Set crm case state to 'done'
            # It will raise an error because a required step is missing
            try:
                case.write({'state': 'done'})
            except Exception, e:
                pass
            # Check changes in ATR haven't been done
            case = case_obj.browse(cursor, uid, case_id)
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(case.state, 'pending')
            self.assertEqual(
                case.email_last,
                "No existeix A3-07 de rebuig, no es realitzen els canvis al ATR associat."
            )
            self.assertEqual(sw.state, 'done')
            self.assertEqual(sw.step_id.name, '04')
            self.assertEqual(sw.proces_id.name, 'A3')

            # Create another ATR case
            r1_xml_path = get_module_resource('giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
            with open(r1_xml_path, 'r') as f:
                r1_xml = f.read()
            sw_id, _ = sw_obj.importar_xml(cursor, uid, r1_xml, 'r101.xml')

            # Create an OT, the OT must have referenced the atr case
            case_id = case_obj.create(cursor, uid, {
                'name': "Ordre de Treball",
                'ref': 'giscedata.switching, {0}'.format(sw_id),
                'section_id': 1,
            })

            case = case_obj.browse(cursor, uid, case_id)
            sw = sw_obj.browse(cursor, uid, sw_id)

            # Check init states
            self.assertEqual(case.state, 'draft')
            self.assertEqual(case.ref, 'giscedata.switching, {0}'.format(sw.id))
            self.assertEqual(sw.state, 'open')
            self.assertEqual(sw.step_id.name, '01')
            self.assertEqual(sw.proces_id.name, 'R1')

            # Set crm case state to 'open'
            case.write({'state': 'open'})
            # Check if the changes have been made
            case = case_obj.browse(cursor, uid, case_id)
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(case.state, 'open')
            self.assertEqual(sw.state, 'pending')
            self.assertEqual(sw.step_id.name, '02')
            self.assertEqual(sw.proces_id.name, 'R1')
            self.assertEqual(sw.get_pas().rebuig_ids[0].motiu_rebuig.name, "42")
            self.assertTrue(sw.rebuig)
