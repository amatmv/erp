# -*- coding: utf-8 -*-
from giscedata_switching.tests.common_tests import TestSwitchingImport
from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
from destral.patch import PatchNewCursors


class TestA3(TestSwitchingImport):

    def test_load_and_activate_a3_01(self):
        self.openerp.install_module('giscedata_polissa_distri')
        self.openerp.install_module('giscedata_facturacio_distri')
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml'
        )
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()
            a3_xml = a3_xml.replace("B82420654", "ES11111111H")

        sw_obj = self.openerp.pool.get('giscedata.switching')
        tarifa_obj = self.openerp.pool.get("giscedata.polissa.tarifa")
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        imd_obj = self.openerp.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            # aquest cups no te una polissa lligada.
            cups_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_cups', 'cups_06'
            )[1]
            cups_name = cups_obj.read(cursor, uid, [cups_id], ['name'])[0]['name']

            act_obj = self.openerp.pool.get("giscedata.switching.activation.config")
            act_obj.write(cursor, uid, act_obj.search(cursor, uid, [], context={"active_test": False}), {'active': True, 'is_automatic': False})

            a3_xml = a3_xml.replace('ES1234000000000001JN0F', str(cups_name))

            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '01')
            self.assertEqual(a3.partner_id.ref, '4321')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.codi_sollicitud, '201412111009')
            self.assertEqual(a3.data_sollicitud, '2014-04-16')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            a301 = sw_obj.get_pas(cursor, uid, a3)
            a301.emisor_id.write({
                'payment_type_customer': 2
            })

            self.assertEqual(a301.activacio_cicle, 'L')
            self.assertEqual(a301.data_accio, '2015-05-18')
            self.assertEqual(a301.cnae.name, '9820')
            self.assertEqual(a301.data_final, '2018-01-01')
            self.assertEqual(a301.tipus_autoconsum, '00')
            self.assertEqual(a301.tipus_contracte, '01')
            self.assertEqual(a301.tarifaATR, '001')
            self.assertEqual(len(a301.pot_ids), 1)
            self.assertEqual(a301.pot_ids[0].potencia, 2300)
            self.assertEqual(a301.control_potencia, '1')
            self.assertEqual(a301.cont_nom, 'Nombre Inventado')
            self.assertEqual(a301.cont_telefons[0].numero, '666777444')
            self.assertEqual(a301.cont_telefons[1].numero, '666777555')
            self.assertEqual(a301.tipus_document, 'NI')
            self.assertEqual(a301.codi_document, 'ES11111111H')
            self.assertEqual(a301.persona, 'J')
            self.assertEqual(a301.nom, 'Camptocamp')
            self.assertEqual(a301.telefons[0].numero, '555123123')
            self.assertEqual(a301.ind_direccio_fiscal, 'F')
            self.assertEqual(a301.fiscal_address_id.country_id.name, u'España')
            self.assertEqual(a301.fiscal_address_id.id_municipi.ine, '17079')
            self.assertEqual(a301.fiscal_address_id.nv[:30], u'MELA MUTERMILCH')
            self.assertEqual(a301.fiscal_address_id.aclarador, u'Bloque de Pisos')
            self.assertEqual(a301.dt_cie_codigo, '1234567')
            self.assertTrue(a301.dt_cie_electronico)
            self.assertEqual(a301.comentaris, 'Comentario')

            self.assertEqual(len(a301.document_ids), 1)
            self.assertEqual(a301.document_ids[0].type, '06')
            self.assertEqual(
                a301.document_ids[0].url,
                'https://www.dropbox.com/s/q40impgt3tn0vtj/Reclama_%20da%C3%B1os_%20Montero%20Simon%2C%20Eduardo%20Tarsicio.pdf?dl=0'
            )

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

            # Activate A3-01. A contract in draft should be created. As the
            # partner and its address don't exist they should b created too.
            with PatchNewCursors():
                sw_obj.activa_polissa_cas_atr(cursor, uid, a3.id)
            tarifa_id = tarifa_obj.search(
                cursor, uid, [('codi_ocsum', '=', a301.tarifaATR)]
            )[0]
            pot_max = (max([p.potencia for p in a301.pot_ids]) / 1000.0)
            vat = a301.codi_document
            if not vat.startswith("ES"):
                vat = "ES" + vat
            sw = sw_obj.browse(cursor, uid, a3.id)
            self.assertTrue(sw.cups_polissa_id)
            polissa = sw.cups_polissa_id
            self.assertEqual(polissa.cups.id, sw.cups_id.id)
            self.assertEqual(polissa.comercialitzadora.id, a301.emisor_id.id)
            self.assertEqual(polissa.titular.vat, vat)
            self.assertEqual(polissa.cnae.id, a301.cnae.id)
            self.assertEqual(polissa.autoconsumo, a301.tipus_autoconsum)
            self.assertEqual(polissa.tarifa.id, tarifa_id)
            self.assertEqual(polissa.potencia, pot_max)
            self.assertEqual(polissa.titular.vat, a301.codi_document)
            self.assertTrue(a301.fiscal_address_id in polissa.titular.address)

    def inner_test_load_and_activate_a3_01_eventual_no_meter(
            self, cursor, uid, a3_xml, xml_file):

        sw_obj = self.openerp.pool.get('giscedata.switching')
        tarifa_obj = self.openerp.pool.get("giscedata.polissa.tarifa")
        imd_obj = self.openerp.pool.get('ir.model.data')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')

        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_06'
        )[1]
        cups_name = cups_obj.read(cursor, uid, [cups_id], ['name'])[0]['name']

        a3_xml = a3_xml.replace('ES1234000000000001JN0F', str(cups_name))
        act_obj = self.openerp.pool.get("giscedata.switching.activation.config")
        act_obj.write(cursor, uid, act_obj.search(cursor, uid, [], context={"active_test": False}), {'active': True, 'is_automatic': False})
        a = sw_obj.importar_xml(cursor, uid, a3_xml, xml_file)

        res = sw_obj.search(cursor, uid, [
            ('proces_id.name', '=', 'A3'),
            ('step_id.name', '=', '01'),
            ('codi_sollicitud', '=', '201412111009')
        ])
        self.assertEqual(len(res), 1)

        a3 = sw_obj.browse(cursor, uid, res[0])

        a301 = sw_obj.get_pas(cursor, uid, a3)
        a301.emisor_id.write({
            'payment_type_customer': 2
        })

        # Check that there is no reject
        res = sw_obj.search(cursor, uid, [
            ('proces_id.name', '=', 'A3'),
            ('step_id.name', '=', '02'),
            ('codi_sollicitud', '=', '201412111009')
        ])
        if len(res) > 0:
            pas_02 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(pas_02.rebuig, False)

        # Activate A3-01. A contract in draft should be created. As the
        # partner and its address don't exist they should b created too.
        with PatchNewCursors():
            sw_obj.activa_polissa_cas_atr(cursor, uid, a3.id, {'no_store_function': True})
        tarifa_id = tarifa_obj.search(
            cursor, uid, [('codi_ocsum', '=', a301.tarifaATR)]
        )[0]

        expected_consumption = a301.expected_consumption  # consumption in kWh
        start_date = a301.data_accio
        end_date = a301.data_final

        sw = sw_obj.browse(cursor, uid, a3.id)
        self.assertTrue(sw.cups_polissa_id)
        polissa = sw.cups_polissa_id

        self.assertEqual(polissa.cups.id, sw.cups_id.id)
        self.assertEqual(polissa.contract_type, a301.tipus_contracte)

        # Eventual without meter specific fields
        self.assertEqual(polissa.data_alta, start_date)
        self.assertEqual(
            polissa.expected_consumption, expected_consumption
        )
        self.assertEqual(polissa.renovacio_auto, False)
        self.assertEqual(polissa.data_baixa, end_date)

        # Polissa must be linked to a meter.
        meters = polissa.comptadors
        self.assertEqual(len(meters), 1)
        meter = meters[0]
        # and it must be a fake meter
        meter_fake_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'meter_fake'
        )[1]
        self.assertEqual(meter.serial.product_id.id, meter_fake_id)

        return polissa

    def test_load_and_activate_a3_01_eventual_no_meter_20A(self):
        self.openerp.install_module('giscedata_polissa_distri')
        self.openerp.install_module('giscedata_facturacio_distri')
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures',
            'a301_new_eventual_no_meters.xml'
        )
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()
            a3_xml = a3_xml.replace("B82420654", "ES11111111H")

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            polissa = self.inner_test_load_and_activate_a3_01_eventual_no_meter(
                cursor, uid, a3_xml, 'a301_new_eventual_no_meters.xml'
            )
            meter = polissa.comptadors[0]
            # the meter should have 2 readings
            readings = meter.lectures
            self.assertEqual(len(readings), 2)
            meter_data_alta = datetime.strptime(meter.data_alta, "%Y-%m-%d")
            meter_data_alta_mud = (meter_data_alta - timedelta(days=1))
            meter_data_alta_mud_str = meter_data_alta_mud.strftime("%Y-%m-%d")
            for reading in readings:
                self.assertIn(
                    reading.name, [meter_data_alta_mud_str, meter.data_baixa]
                )
                # the reading of the older one must be 0
                if reading.name == meter_data_alta_mud_str:
                    self.assertEqual(reading.lectura, 0)
                # and the newest one must be the pacted power
                else:
                    self.assertEqual(reading.lectura, polissa.expected_consumption)

    def test_load_and_activate_a3_01_eventual_no_meter_30A(self):
        self.openerp.install_module('giscedata_polissa_distri')
        self.openerp.install_module('giscedata_facturacio_distri')
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures',
            'a301_new_eventual_no_meters30A.xml'
        )
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()
            a3_xml = a3_xml.replace("B82420654", "ES11111111H")

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            polissa = self.inner_test_load_and_activate_a3_01_eventual_no_meter(
                cursor, uid, a3_xml, 'a301_new_eventual_no_meters30A.xml'
            )
            meter = polissa.comptadors[0]
            # the meter should have 12 readings
            readings = meter.lectures
            self.assertEqual(len(readings), 12)
            meter_data_alta = datetime.strptime(meter.data_alta, "%Y-%m-%d")
            meter_data_alta_mud = (meter_data_alta - timedelta(days=1))
            meter_data_alta_mud_str = meter_data_alta_mud.strftime("%Y-%m-%d")
            for reading in readings:
                self.assertIn(
                    reading.name, [meter_data_alta_mud_str, meter.data_baixa]
                )
                # the reading of the older one must be 0
                if reading.name == meter_data_alta_mud_str:
                    self.assertEqual(reading.lectura, 0)
                # the first period must be the 25% of the pacted power
                elif reading.periode.name == u'P1':
                    self.assertEqual(reading.lectura, polissa.expected_consumption * 0.25)
                # the second one must be the 55% of the pacted power
                elif reading.periode.name == u'P2':
                    self.assertEqual(reading.lectura, polissa.expected_consumption * 0.55)
                # the third one must be the 20% of the pacted power
                elif reading.periode.name == u'P3':
                    self.assertEqual(reading.lectura, polissa.expected_consumption * 0.20)
                # and the other ones 0
                else:
                    self.assertEqual(reading.lectura, 0)
