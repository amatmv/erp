# -*- coding: utf-8 -*-
from osv import osv
from giscedata_comerdist import Sync, OOOPPool

class GiscedataCupsPs(osv.osv):
    """Cups pel mòdul de sincronització.
    """
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for cups in self.browse(cursor, uid, ids, context):
            partner_id = cups.polissa_polissa.comercialitzadora.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            config = config_obj.browse(cursor, uid, config_ids[0])
            return config

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si aquest objecte s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for cups in self.browse(cursor, uid, ids, context):
            if cups.polissa_polissa and \
            cups.polissa_polissa.comercialitzadora and \
            cups.polissa_polissa.comercialitzadora.id in partner_ids and \
            cups.get_config().user_local.id != uid:
                return True
            else:
                return False

    def create(self, cursor, uid, vals, context=None):
        """Mètode create per la sincronització.
        """
        res_id = super(GiscedataCupsPs, self).create(cursor, uid, vals,
                                                     context)
        cups = self.browse(cursor, uid, res_id, context)
        if cups.must_be_synched(context):
            config_id = cups.get_config().id
            ooop = OOOPPool.get_ooop(cursor, uid, config_id)
            sync = Sync(cursor, uid, ooop, self._name, config=config_id)
            sync.sync(cups.id)
        return res_id

    def write(self, cursor, uid, ids, vals, context=None):
        """Mètode write per la sincronització.
        """
        res = super(GiscedataCupsPs, self).write(cursor, uid, ids, vals,
                                                 context)
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for cups in self.browse(cursor, uid, ids, context):
            if cups.must_be_synched(context):
                config_id = cups.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                sync.sync(cups.id, vals, active_test=False)
        return res

    def unlink(self, cursor, uid, ids, context=None):
        """Mètode unlink per la sincronització.
        """
        if not isinstance(ids, list) and not isinstance(ids, tuple):
            ids = [ids]
        for cups in self.browse(cursor, uid, ids, context):
            if cups.must_be_synched(context):
                config_id = cups.get_config().id
                ooop = OOOPPool.get_ooop(cursor, uid, config_id)
                sync = Sync(cursor, uid, ooop, self._name, config=config_id)
                obj = sync.get_object(cups.id, active_test=False)
                if obj:
                    obj.unlink()
        res = super(GiscedataCupsPs, self).unlink(cursor, uid, ids, context)
        return res

    def create_en_comer(self, cursor, uid, ids, config_id, context=None):
        """Funció que la farem servir des de la comercialitzadora per crear el
        CUPS.
        """
        ooop = OOOPPool.get_ooop(cursor, uid, config_id)
        try:
            sync = Sync(cursor, uid, ooop, self._name, config=config_id)
            for cups in self.browse(cursor, uid, ids, context):
                obj = sync.sync(cups.id)
                ooop.commit()
            return obj.id
        except Exception, ecx:
            ooop.rollback()
            raise ecx
        finally:
            ooop.close()


GiscedataCupsPs()
