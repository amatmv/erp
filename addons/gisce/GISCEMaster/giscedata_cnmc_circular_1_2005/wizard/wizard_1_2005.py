#so-8859-1 -*-
from osv import osv, fields
from tools.sql import install_array_agg
from tools import config
from datetime import datetime
import base64
import time
import netsvc
from tools import config
#
# Reports
#


class giscedata_cne_circular(osv.osv_memory):
    """
    Assistent per generar els inventaris de la CNMC: Circular 1/2005
    """
    _name = 'giscedata.cne.circular'

    def _obte_tipus_informe(self, cursor, uid, context=None):
        """
        Function that returns the availabe inventories to generate

        :param      cursor:         Database cursor, unused
        :type       cursor:         cursor
        :param      uid:            User id, unused
        :type       uid:            int
        :param      context:    OpenERP context, unused
        :type       context:    dict
        :return:    List of tuples with the availabe inventories to generate
        :rtype:     list of (str, str)
        """

        opcions = [('1', '1'),
                   ('2NA1', '2NA1'),
                   ('4A', '4A'),
                   ('4B', '4B'),
                   ('5A', '5A'),
                   ('5B', '5B'),
                   ('7NA', '7NA')]
        return opcions

    def _obte_trimestre(self, cursor, uid, context=None):
        """
        Function that returns the availabe trimestres to generate inventories
    
        :param      cursor:         Database cursor, unused
        :type       cursor:         cursor
        :param      uid:            User id, unused
        :type       uid:            int
        :param      context:    OpenERP context, unused
        :type       context:    dict
        :return:    List of tuples with the availabe trimestres to generate
                    inventories
        :rtype:     list of (str, str)
        """

        opcions = [('T-1', 'T-1'),
                   ('T-2', 'T-2'),
                   ('T-3', 'T-3'),
                   ('T-4', 'T-4')]

        return opcions

    def _obte_semestre(self, cursor, uid, context=None):
        """
        Function that returns the availabe semestres to generate inventories

        :param      cursor:         Database cursor, unused
        :type       cursor:         cursor
        :param      uid:            User id, unused
        :type       uid:            int
        :param      context:    OpenERP context, unused
        :type       context:    dict
        :return:    List of tuples with the availabe semestres to generate
                    inventories
        :rtype:     list of (str, str)
        """

        opcions = [('S-1', 'S-1'),
                   ('S-2', 'S-2')]

        return opcions

    _columns = {
        'type': fields.selection(
            _obte_tipus_informe,
            'Tipus',
            required=True
        ),
        'periode_t': fields.selection(
            _obte_trimestre,
            'Trimestre'
        ),
        'periode_s': fields.selection(
            _obte_semestre,
            'Semestre'
        ),
        'any_t': fields.selection(
            [(num, str(num)) for num in
             range((datetime.now().year) - 10, (datetime.now().year) + 1)],
            'Any'
        ),
        'any_s': fields.selection(
            [(num, str(num)) for num in
             range((datetime.now().year) - 10, (datetime.now().year) + 1)],
            'Any'
        ),
        'ini_per_energia': fields.date('Periode d\'energia del'),
        'fin_per_energia': fields.date('al'),
        'dinici_ener': fields.date('Periode del'),
        'dfinal_ener': fields.date('al'),
        'dinici_noener': fields.date('Periode del'),
        'dfinal_noener': fields.date('al'),
        'file_name': fields.char('Nom del fitxer', size=256),
        'file': fields.binary('Fitxer sortida'),
        # State controllers
        'state': fields.selection([('init', 'Init'),
                                   ('fready', 'File ready')]),
        'period_state': fields.selection([('not_set', 'Not set'),
                                          ('trim', 'Trimestral'),
                                          ('sem', 'Semestral')]),
        'filter_criteria_state': fields.selection(
            [('not_set', 'Not set'),
             ('ener', 'Energia'),
             ('noener', 'No energia')]
        ),
        'file_state': fields.selection([('nogen', 'nogen'),
                                        ('ready', 'ready'),
                                        ('gen', 'gen')]),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'file': lambda *a: False,
        'period_state': lambda *a: 'not_set',
        'filter_criteria_state': lambda *a: 'not_set',
        'file_state': lambda *a: 'nogen',
        'periode_t': lambda *a: False,
        'periode_s': lambda *a: False,
        'any_t': lambda *a: False,
        'any_s': lambda *a: False,
    }

    def onchange_update_wizard(
            self, cursor, uid, ids, type, periode_t, any_t, periode_s, any_s,
            context=None):
        """
        Function that updates the wizard view to show only the necesary fields
        in function of wich fields are selected

        :param      cursor:         Database cursor, unused
        :type       cursor:         cursor
        :param      uid:            User id, unused
        :type       uid:            int
        :param      ids:            Affected ids, unused
        :type       uid:            int
        :param      type:           Iventory to generate
        :type       type:           str
        :param      periode_t:      Trimestre to generate the inventory
        :type       periode_t:      str
        :param      any_t:          Year of the trimestre to generate the inven.
        :type       any_t:          str
        :param      periode_s:      Semestre to generate the inventory
        :type       periode_s:      str
        :param      any_s:          Year of the semestre to generate the inven.
        :type       any_s:          str
        :param      context:    OpenERP context, unused
        :type       context:    dict
        :return:    Dictionary with the fields to update of the view.
        :rtype:     dict[str, str or bool]
        """

        # Definim el periode temporal i el periode d'energia
        periode = {
            'T-1': ('{}-01-01'.format(any_t), '{}-03-31'.format(any_t)),
            'T-2': ('{}-04-01'.format(any_t), '{}-06-30'.format(any_t)),
            'T-3': ('{}-07-01'.format(any_t), '{}-09-30'.format(any_t)),
            'T-4': ('{}-10-01'.format(any_t), '{}-12-31'.format(any_t))
        }
        periode_ener = {
            'T-1': ('{}-01-01'.format(any_t - 1), '{}-12-31'.format(any_t - 1)),
            'T-2': ('{}-04-01'.format(any_t - 1), '{}-03-31'.format(any_t)),
            'T-3': ('{}-07-01'.format(any_t - 1), '{}-06-30'.format(any_t)),
            'T-4': ('{}-10-01'.format(any_t - 1), '{}-09-30'.format(any_t))
        }

        if type == '5A' or type == '5B':
            # Redefinim el periode temporal per adaptarlo al inventaris
            # semestrals
            periode = {
                'S-1': ('{}-07-01'.format(any_s - 1), '{}-06-30'.format(any_s)),
                'S-2': ('{}-01-01'.format(any_s), '{}-12-31'.format(any_s))
            }
            # En el cas que l'any o el periode no hagi estat definit deixarem
            # les dates de filtratge en fals, altrament asignarem els valors
            # corresponents
            if any_s is False or periode_s is False:
                return {
                    'value': {
                        'dinici_noener': False,
                        'dfinal_noener': False,
                        'period_state': 'sem',
                        'filter_criteria_state': 'noener',
                        'file_state': 'ready'
                    }
                }
            else:
                return {
                    'value': {
                        'dinici_noener': periode[periode_s][0],
                        'dfinal_noener': periode[periode_s][1],
                        'period_state': 'sem',
                        'filter_criteria_state': 'noener',
                        'file_state': 'ready'
                    }
                }
        elif type == '7NA':
            # En el cas que l'any o el periode no hagi estat definit deixarem
            # les dates de filtratge en fals, altrament asignarem els valors
            # corresponents
            if any_t is False or periode_t is False:
                return {
                    'value': {
                        'dinici_ener': False,
                        'dfinal_ener': False,
                        'ini_per_energia': False,
                        'fin_per_energia': False,
                        'period_state': 'trim',
                        'filter_criteria_state': 'ener',
                        'file_state': 'ready'
                    }
                }
            else:
                return {
                    'value': {
                        'dinici_ener': periode[periode_t][0],
                        'dfinal_ener': periode[periode_t][1],
                        'ini_per_energia': periode_ener[periode_t][0],
                        'fin_per_energia': periode_ener[periode_t][1],
                        'period_state': 'trim',
                        'filter_criteria_state': 'ener',
                        'file_state': 'ready'
                    }
                }
        else:
            # En el cas que l'any o el periode no hagi estat definit deixarem
            # les dates de filtratge en fals, altrament asignarem els valors
            # corresponents
            if any_t is False or periode_t is False:
                return {
                    'value': {
                        'dinici_noener': False,
                        'dfinal_noener': False,
                        'period_state': 'trim',
                        'filter_criteria_state': 'noener',
                        'file_state': 'ready'
                    }
                }
            else:
                return {
                    'value': {
                        'dinici_noener': periode[periode_t][0],
                        'dfinal_noener': periode[periode_t][1],
                        'period_state': 'trim',
                        'filter_criteria_state': 'noener',
                        'file_state': 'ready'
                    }
                }

    def export_file(self, cursor, uid, ids, context=None):
        """
        Function that executes the inventory query and create the file with the
        result of the query. It also updates the view to show the file manager.

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        int
        :param      ids:        Affected ids
        :type       uid:        int
        :param      context:    OpenERP context
        :type       context:    dict
        """

        sql_name = {'1': '1.sql',
                    '2NA1': '2NA1.sql',
                    '5A': '5A.sql',
                    '5B': '5B.sql',
                    '7NA': '7NA.sql',
                    '4A': '4A.sql',
                    '4B': '4B.sql'}

        logger = netsvc.Logger()
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Starting CNE export")
        # Actualitzem refs
        sql = open('%s/%s/sql/refs.sql'
                   % (config['addons_path'],
                      'giscedata_cne')).read()
        cursor.execute(sql)

        # Carreguem parametres de l'informe
        wizard = self.read(cursor,
                           uid,
                           ids[0],
                           ['ini_per_energia',
                            'fin_per_energia',
                            'dinici_ener',
                            'dfinal_ener',
                            'dinici_noener',
                            'dfinal_noener',
                            'type',
                            'any_s',
                            'any_t',
                            'periode_s',
                            'periode_t'])[0]
        sql_file = sql_name.get(wizard['type'], False)
        sql = open('%s/%s/sql/%s' % (config['addons_path'], 'giscedata_cne',
                                     sql_file)).read()

        # Executem informe amb els parametres seleccionats en el wizard
        params = {
            'ini_per_energia': wizard['ini_per_energia'],
            'fin_per_energia': wizard['fin_per_energia'],
            'dinici_ener': wizard['dinici_ener'],
            'dfinal_ener': wizard['dfinal_ener'],
            'dinici_noener': wizard['dinici_noener'],
            'dfinal_noener': wizard['dfinal_noener'],
        }
        cursor.execute(sql, params)

        # Generem l'informe amb les dades retornades per la query
        informe = []
        for line in cursor.fetchall():
            line = list(line)
            informe.append(';'.join([str(a) or '' for a in line]))
        informe = '\n'.join(informe)
        informe += '\n'
        mfile = base64.b64encode(informe.encode('utf-8'))

        # Generem el nom de l'informe amb el que es guardara el fitxer
        r1 = self.pool.get('res.users')
        r1 = str(r1.browse(cursor, uid, 1).company_id.codi_r1)
        if wizard['type'] in ('5A', '5B'):
            year = wizard['any_s']
            period = wizard['periode_s'].split('-')[1]
        else:
            year = wizard['any_t']
            period = wizard['periode_t'].split('-')[1]
        filename = 'CIR1_2005_{}_R1-{}_{}{}.txt'.format(
            wizard['type'], r1, year, period
        )
        self.write(
            cursor,
            uid,
            ids,
            {'state': 'fready',
             'file': mfile,
             'file_name': filename
             },
            context)
        logger.notifyChannel("addons", netsvc.LOG_INFO,
                             "Ending CNE export")

    def reengage(self, cursor, uid, ids, context=None):
        """
        Function that updates the wizard view to generate another inventory
        without close the wizard.

        :param      cursor:     Database cursor
        :type       cursor:     cursor
        :param      uid:        User id
        :type       uid:        int
        :param      ids:        Affected ids
        :type       uid:        int
        :param      context:    OpenERP context
        :type       context:    dict
        """
        self.write(cursor, uid, ids, {'state': 'init'}, context)

giscedata_cne_circular()
