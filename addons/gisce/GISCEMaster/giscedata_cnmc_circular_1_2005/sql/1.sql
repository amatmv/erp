SELECT
to_char(DATE(%(dfinal_noener)s),'YYYY') as AÑO,
(SELECT CASE WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (1,2,3) THEN 'T1'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (4,5,6) THEN 'T2'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (7,8,9) THEN 'T3'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (10,11,12) THEN 'T4'
  END) as TRIMESTRE,
(select ref2 from res_partner where id = 1) as COD_DIS1,
'1' as COD_OC,
p.agree_tipus as COD_TPM,
count(m1.polissa_id)::int as NUM_SUM
FROM giscedata_polissa_modcontractual m1
INNER JOIN giscedata_polissa_modcontractual m2
ON (m1.modcontractual_ant = m2.id
AND m1.comercialitzadora != m2.comercialitzadora)
INNER JOIN giscedata_polissa p ON m1.polissa_id = p.id 
INNER JOIN res_partner r1 ON m1.comercialitzadora = r1.id 
INNER JOIN res_partner r2 ON m2.comercialitzadora = r2.id 
INNER JOIN giscedata_polissa_tarifa t ON m1.tarifa = t.id
INNER JOIN giscedata_cups_ps c ON m1.cups = c.id 
INNER JOIN res_municipi v ON c.id_municipi = v.id
INNER JOIN res_country_state s ON v.state = s.id
WHERE m1.data_inici >= %(dinici_noener)s
AND m1.data_inici <= %(dfinal_noener)s
AND ((r1.ref in ('0636','0638','0642','0644','0688') AND r2.ref not in ('0636','0638','0642','0644','0688'))
OR (r1.ref not in ('0636','0638','0642','0644','0688') AND r2.ref not in ('0636','0638','0642','0644','0688')))
GROUP BY p.agree_tipus
ORDER BY p.agree_tipus;
