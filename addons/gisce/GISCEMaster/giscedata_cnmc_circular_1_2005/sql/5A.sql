SELECT
to_char(DATE(%(dfinal_noener)s),'YYYY') AS AÑO,
(
  SELECT CASE
    WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (1,2,3,4,5,6) THEN 1
    WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (7,8,9,10,11,12) THEN 2
  END
) AS SEMESTRE,
(
  SELECT ref2 FROM res_partner WHERE id = 1
) AS COD_DIS1,
pol.agree_tipus AS COD_TPM,
'1' AS PERIODO,
(
  SELECT CASE
    WHEN count(modact.polissa_id)::int <= 5 THEN count(modact.polissa_id)::int
    WHEN count(modact.polissa_id)::int > 5 AND count(modact.polissa_id)::int <= 10 THEN 10
    WHEN count(modact.polissa_id)::int > 10 THEN 99
  END
) AS NUM_CAMB,
(
  SELECT count(distinct(modactT.polissa_id)) FROM giscedata_polissa_modcontractual modactT
  INNER JOIN giscedata_polissa polT ON modactT.polissa_id = polT.id
  INNER JOIN giscedata_polissa_tarifa tar ON tar.id = polT.tarifa
  WHERE (polT.agree_tipus)::int = (pol.agree_tipus)::int
  AND modactT.data_final >= %(dfinal_noener)s
  AND modactT.data_inici < %(dfinal_noener)s
  AND tar.name NOT LIKE '%%RE%%'
) AS NUM_SUM
FROM giscedata_polissa_modcontractual modact
LEFT JOIN giscedata_polissa_modcontractual modant ON (modact.modcontractual_ant = modant.id
AND modact.comercialitzadora != modant.comercialitzadora)
LEFT JOIN giscedata_polissa pol ON modact.polissa_id = pol.id
LEFT JOIN res_partner partact ON modact.comercialitzadora = partact.id
LEFT JOIN res_partner partant ON modant.comercialitzadora = partant.id
WHERE modact.data_inici >= %(dinici_noener)s
AND modact.data_inici <= %(dfinal_noener)s
AND (partact.ref NOT IN ('0636','0638','0642','0644','0688') AND partant.ref IN ('0636','0638','0642','0644','0688'))
GROUP BY pol.agree_tipus
ORDER BY pol.agree_tipus
