SELECT
to_char(DATE(%(dfinal_noener)s),'YYYY') as AÑO,
(SELECT CASE WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (1,2,3) THEN 'T1'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (4,5,6) THEN 'T2'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (7,8,9) THEN 'T3'
  WHEN to_char(DATE(%(dfinal_noener)s),'MM')::int in (10,11,12) THEN 'T4'
  END) as TRIMESTRE,
(select ref2 from res_partner where id = 1) as COD_DIS1,
r1.ref2 as COD_COM,
r2.ref2 as COD_COM_SAL,
p.agree_tipus::int as COD_TPM, 
'0' as COD_TCE,
count(m1.polissa_id) as NUM_SUM,
(
	SELECT CASE
    WHEN t.name = '2.0A' THEN '01'
    WHEN t.name = '2.0DHA' THEN '02'
    WHEN t.name = '3.0A' THEN '03'
    WHEN t.name = '3.0A LB' THEN '03'
    WHEN t.name = '3.1A' THEN '04'
    WHEN t.name = '3.1A LB' THEN '04'
    WHEN t.name = '6.1' THEN '05'
    WHEN t.name = '6.1A' THEN '05'
    WHEN t.name = '6.1B' THEN '17'
    WHEN t.name = '6.2' THEN '06'
    WHEN t.name = '6.3' THEN '07'
    WHEN t.name = '6.4' THEN '08'
    WHEN t.name = '6.5' THEN '09'
    WHEN t.name = '2.1A' THEN '10'
    WHEN t.name = '2.1DHA' THEN '11'
    WHEN t.name = '2.0DHS' THEN '12'
    WHEN t.name = '2.1DHS' THEN '13'
	END
) AS TIPO_TAR_ACCESO
FROM giscedata_polissa_modcontractual m1
INNER JOIN giscedata_polissa_modcontractual m2
ON (m1.modcontractual_ant = m2.id
and m1.comercialitzadora != m2.comercialitzadora)
INNER JOIN giscedata_polissa p ON m1.polissa_id = p.id 
INNER JOIN res_partner r1 ON m1.comercialitzadora = r1.id 
INNER JOIN res_partner r2 ON m2.comercialitzadora = r2.id 
INNER JOIN giscedata_polissa_tarifa t ON m1.tarifa = t.id
INNER JOIN giscedata_cups_ps c ON m1.cups = c.id 
INNER JOIN res_municipi v ON c.id_municipi = v.id
INNER JOIN res_country_state s ON v.state = s.id
WHERE m1.data_inici >= %(dinici_noener)s and m1.data_inici <= %(dfinal_noener)s
GROUP BY r1.ref2, r2.ref2, p.agree_tipus, t.name
ORDER BY r1.ref2, r2.ref2, p.agree_tipus, t.name;
