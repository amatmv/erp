<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="tensions-per-provincia" match="tensio" use="provincia" />
  

  <xsl:template match="/">
    <xsl:apply-templates select="estadistiques"/>
  </xsl:template>

  <xsl:template match="estadistiques">
    <document compression="1">
      <template pageSize="(297mm,19cm)" topMargin="1cm" bottomMargin="1cm" rightMargin="1cm">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="277mm" height="17cm"/>
          <pageGraphics>
            <setFont name="Helvetica-Bold" size="7"/>
            <drawString x="10mm" y="10mm" t="1"><xsl:value-of select="estadistica/provincia" />: <xsl:value-of select="estadistica/any" /> versi� <xsl:value-of select="estadistica/revisio" /> de <xsl:value-of select="estadistica/create_date" /></drawString>
            <lines>10mm 9mm 287mm 10mm</lines>
          </pageGraphics>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="titol"
      		fontName="Helvetica"
      		fontSize="12"
      		leading="10"
      		alignment="center" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula2">
      		<lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
      		<blockBackground colorName="silver" start="0,0" stop="-1,0" />
      	</blockTableStyle>
      		
      	<blockTableStyle id="taula1">
      		<lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="2,0" />
      		<lineStyle kind="LINEAFTER" colorName="black" start="3,0" stop="5,0" />
      		<lineStyle kind="LINEABOVE" colorName="black" start="1,0" stop="4,0" />
      		<lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
      		<blockBackground colorName="silver" start="0,0" stop="-1,0" />
      	</blockTableStyle>
      		
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="GRID" colorName="black" />
          <blockBackground colorName="silver" start="0,1" stop="0,-1" />
          <blockBackground colorName="silver" start="0,0" stop="-1,0" />
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<xsl:apply-templates select="estadistica" mode="story"/>
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="estadistica" mode="story">
  
      	<para style="titol" t="1">DATOS T�CNICOS DE LA DISTRIBUCI�N (<xsl:value-of select="any" /> - <xsl:value-of select="provincia" />)</para>
      	<spacer length="10" />
      	<para style="titol" t="1" leading="10">INSTALACIONES COSTEADAS POR LA EMPRESA</para>
      	<spacer length="10" />
      	<blockTable style="taula">
      		<tr>
      		<td></td>
      	<xsl:for-each select="lat">
      		<xsl:sort select="nom" data-type="number" />
      		<td><xsl:value-of select="nom" /></td><td></td>
      	</xsl:for-each>
      		</tr>
      		<tr>
      		<td></td>
      	<xsl:for-each select="lat">
      		<td>A�reas</td><td>Subterr</td>
      	</xsl:for-each>
      		</tr>
      		<tr>
      		<td>kms.</td>
      	<xsl:for-each select="lat">
      		<xsl:sort select="nom" data-type="number" />
      		<td><xsl:value-of select="format-number(aer div 1000, '#')" /></td><td><xsl:value-of select="format-number(sub div 1000, '#')" /></td>
      	</xsl:for-each>
      		</tr>
      	</blockTable>
      	
      	<spacer length="0.5cm" />
      	<!-- CTS -->
      	<blockTable style="taula">
      		<tr>
      		<td>Tipo</td>
      		<td>0</td>
      		<td>20</td>
      		<td>25</td>
      		<td>30</td>
      		<td>50</td>
      		<td>75</td>
      		<td>100</td>
      		<td>125</td>
      		<td>160</td>
      		<td>200</td>
      		<td>250</td>
      		<td>315</td>
      		<td>400</td>
      		<td>500</td>
      		<td>630</td>
      		<td>800</td>
      		<td>1000</td>
      		<td>1250</td>
      		</tr>
      		<tr>
      		<td>Intemperie</td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='I' and nom='1250']/num" /></td>
      		</tr>
      		<tr>
   			<td>Local</td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='L' and nom='1250']/num" /></td>
      		</tr>
      		<tr>
      		<td>Caseta</td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='C' and nom='1250']/num" /></td>
      		</tr>
      		<tr>
      		<td>Soterrat</td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts[tipus_codi='S' and nom='1250']/num" /></td>
      		</tr>
      	</blockTable>
      	<para style="titol" t="1" leading="10">INSTALACIONES CEDIDAS</para>
      	<spacer length="10" />
      	<blockTable style="taula">
      		<tr>
      		<td></td>
      	<xsl:for-each select="lat_cedits">
      		<xsl:sort select="nom" data-type="number" />
      		<td><xsl:value-of select="nom" /></td><td></td>
      	</xsl:for-each>
      		</tr>
      		<tr>
      		<td></td>
      	<xsl:for-each select="lat_cedits">
      		<td>A�reas</td><td>Subterr</td>
      	</xsl:for-each>
      		</tr>
      		<tr>
      		<td>kms.</td>
      	<xsl:for-each select="lat_cedits">
      		<xsl:sort select="nom" data-type="number" />
      		<td><xsl:value-of select="format-number(aer div 1000, '#')" /></td><td><xsl:value-of select="format-number(sub div 1000, '#')" /></td>
      	</xsl:for-each>
      		</tr>
      	</blockTable>
      	
      	<spacer length="0.5cm" />
      	<!-- CTS CEDITS-->
      	<blockTable style="taula">
      		<tr>
      		<td>Tipo</td>
      		<td>0</td>
      		<td>20</td>
      		<td>25</td>
      		<td>30</td>
      		<td>50</td>
      		<td>75</td>
      		<td>100</td>
      		<td>125</td>
      		<td>160</td>
      		<td>200</td>
      		<td>250</td>
      		<td>315</td>
      		<td>400</td>
      		<td>500</td>
      		<td>630</td>
      		<td>800</td>
      		<td>1000</td>
      		<td>1250</td>
      		</tr>
      		<tr>
      		<td>Intemperie</td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='I' and nom='1250']/num" /></td>
      		</tr>
      		<tr>
   			<td>Local</td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='L' and nom='1250']/num" /></td>
      		</tr>
      		<tr>
      		<td>Caseta</td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='C' and nom='1250']/num" /></td>
      		</tr>
      		<tr>
      		<td>Soterrat</td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='0']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='20']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='25']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='30']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='50']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='75']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='100']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='125']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='160']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='200']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='250']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='315']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='400']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='500']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='630']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='800']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='1000']/num" /></td>
      		<td><xsl:value-of select="cts_cedits[tipus_codi='S' and nom='1250']/num" /></td>
      		</tr>
      	</blockTable>
      	<nextFrame />
</xsl:template>
  
</xsl:stylesheet>
