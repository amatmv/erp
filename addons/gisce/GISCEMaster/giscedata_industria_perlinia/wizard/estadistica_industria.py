# -*- coding: utf-8 -*-
import wizard
import pooler

_escollir_any_form = """<?xml version="1.0"?>
<form string="Generar Estadística Indústria" col="2">
  <field name="year" />
  <field name="print_pdf" />
</form>"""

_escollir_any_fields = {
  'year': {'string': 'Any', 'type': 'char', 'size': 4, 'required': True},
  'print_pdf': {'string': 'Mostrar PDF al finalitzar', 'type': 'boolean'},
}

def _calc(self, cr, uid, data, context={}):
  data['print_ids'] = []
  perlinia_year = pooler.get_pool(cr.dbname).get('giscedata.industria.perlinia.year')
  perlinia = pooler.get_pool(cr.dbname).get('giscedata.industria.perlinia')
  # Borrem dades antigues
  perlinia_year_ids = perlinia_year.search(cr, uid, [('name', '=', data['form']['year'])])
  perlinia_year.unlink(cr, uid, perlinia_year_ids)
  #Si ens eliminem l'any, per cascada ens eliminara les linies de l'perlinia d'aquell any
  #cr.execute("delete from giscedata_industria_perlinia")
  #cr.commit()

  # Per a cada provincia en (LAT, LBT)
  cr.execute("""
select distinct 
	p.id as id 
from 
	res_municipi m, 
	res_country_state p, 
	giscedata_at_linia lat, 
	giscedata_at_tram tram 
where 
	lat.municipi = m.id 
	and m.state = p.id 
	and tram.linia = lat.id 
	and lat.baixa = False 
	and lat.propietari = True 
	and tram.baixa = False 
	and lat.tensio is not null 
	and tram.tipus in (1,2) 
union select distinct 
	p.id as id 
from 
	giscedata_bt_element e, 
	res_municipi m, 
	res_country_state p 
where 
	e.municipi = m.id 
	and m.state = p.id 
	and (e.baixa = False or e.baixa is null) 
	and e.tipus_linia in (1,2)""")
  for provincia in cr.dictfetchall():
  
    # Creem el nou any
    year_id = perlinia_year.create(cr, uid, {'name': data['form']['year'], 'provincia': provincia['id']})
    
    # Calculem les dades de linies AT
    cr.execute("""
select 
	sum(tram.longitud_cad::float) as long, 
	tram.tipus, 
	tram.circuits,
	lat.name
from 
	res_municipi m, 
	res_country_state p, 
	giscedata_at_linia lat, 
	giscedata_at_tram tram 
where 
	lat.municipi = m.id 
	and m.state = p.id 
	and p.id = %i 
	and tram.linia = lat.id 
	and lat.baixa = False 
	and lat.propietari = True 
	and tram.baixa = False 
	and lat.tensio is not null 
	and tram.tipus in (1,2) 
group by 
	lat.name,
	tram.tipus,
	tram.circuits""", (provincia['id'],))

    for lat in cr.dictfetchall():
      vals = {}
      vals['name'] = lat['name']
      vals['year'] = year_id
      if lat['circuits'] == 1 and lat['tipus'] == 1:
        vals['l_1c'] = lat['long']
      elif lat['circuits'] == 2:
        vals['l_2c'] = lat['long']
      elif lat['circuits'] > 2:
        vals['l_m2c'] = lat['long']
      elif lat['tipus'] == 2:
        vals['l_sub'] = lat['long']
      perlinia.create(cr, uid, vals)

    data['print_ids'].append(year_id)

  return {}

def _check_print_report(self, cr, uid, data, context={}):
  if len(data['print_ids']) and data['form']['print_pdf']:
    return 'print_report'
  elif not len(data['print_ids']):
    return 'end'
  elif len(data['print_ids']) and not data['form']['print_pdf']:
    return 'mostrar_tab'

def _print_report(self, cr, uid, data, context={}):
  return {'ids': data['print_ids']}

def _mostrar_tab(self, cr, uid, data, context={}):
  action = {
    'domain': "[('id','in', ["+','.join(map(str,map(int, data['print_ids'])))+"])]",
		'view_type': 'form',
		'view_mode': 'tree,form',
		'res_model': 'giscedata.industria.perlinia.year',
		'view_id': False,
    'limit': len(data['print_ids']),
		'type': 'ir.actions.act_window'
  }
  return action

def _pre_end(self, cr, uid, data, context={}):
  return {}


class wizard_perlinia_industria(wizard.interface):

  states = {
    'init': {
    	'actions': [],
      'result': {'type': 'state', 'state': 'escollir_any'}
    },
    'escollir_any': {
      'actions': [],
      'result': {'type': 'form', 'arch': _escollir_any_form, 'fields': _escollir_any_fields, 'state': [('calc', 'Generar')]}
    },
    'calc': {
    	'actions': [_calc],
      'result': {'type': 'choice', 'next_state':_check_print_report}
    },
    'print_report': {
      'actions': [_print_report],
      'result': {'type': 'print', 'report': 'giscedata.industria.perlinia', \
        'get_id_from_action':True, 'state':'pre_end'}
    },
    'mostrar_tab': {
      'actions': [],
      'result': {'type': 'action', 'action': _mostrar_tab, 'state': 'pre_end'}
    },
    'pre_end': {
    	'actions': [_pre_end],
    	'result': { 'type' : 'state', 'state' : 'end' },
    },
    'end': {
      'actions': [],
      'result': {'type': 'state', 'state': 'end'}
    }
  }

wizard_perlinia_industria('giscedata.industria.perlinia')


