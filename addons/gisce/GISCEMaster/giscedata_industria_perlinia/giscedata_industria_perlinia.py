# -*- coding: utf-8 -*-

from osv import osv, fields



#
# Reports
#

class giscedata_industria_perlinia_year(osv.osv):

    _name = 'giscedata.industria.perlinia.year'

    _columns = {
      'name': fields.char('Any', size=4),
      'linies': fields.one2many('giscedata.industria.perlinia', 'year', 'Lí­nies'),
      'provincia': fields.many2one('res.country.state', 'Provincia'),
    }

giscedata_industria_perlinia_year()


class giscedata_industria_perlinia(osv.osv):
    _name = 'giscedata.industria.perlinia'

    _columns = {
      'name': fields.char('Línia', size=256),
      'l_1c': fields.float('Lí­nies 1 circuit'),
      'l_2c': fields.float('Línies 2 circuits'),
      'l_m2c': fields.float('Lí­nies 2+ circuits'),
      'l_sub': fields.float('Línies subtarrànies'),
      'year': fields.many2one('giscedata.industria.perlinia.year', 'Any', ondelete='cascade'),
    }

    _defaults = {
      'l_1c': lambda *a: 0.0,
      'l_2c': lambda *a: 0.0,
      'l_m2c': lambda *a: 0.0,
      'l_sub': lambda *a: 0.0,
    }

    _order = 'name asc'

    def create(self, cr, uid, vals, context={}):
        ids = self.search(cr, uid, [('name', '=', vals['name']), ('year', '=', vals['year'])])
        if len(ids):
            self.write(cr, uid, ids[0], vals)
            return ids[0]
        else:
            return super(osv.osv, self).create(cr, uid, vals, context)


giscedata_industria_perlinia()
