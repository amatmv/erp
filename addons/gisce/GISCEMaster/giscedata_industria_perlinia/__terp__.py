# -*- coding: utf-8 -*-
{
    "name": "GISCE Indústria",
    "description": """""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base_extended",
        "giscedata_cts",
        "giscedata_at",
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_industria_perlinia_wizard.xml",
        "giscedata_industria_perlinia_report.xml",
        "giscedata_industria_perlinia_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
