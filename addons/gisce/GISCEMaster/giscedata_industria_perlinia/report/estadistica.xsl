<?xml version="1.0" encoding="iso-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">

  <xsl:key name="tensions-per-provincia" match="tensio" use="provincia" />
  

  <xsl:template match="/">
    <xsl:apply-templates select="estadistiques"/>
  </xsl:template>

  <xsl:template match="estadistiques">
    <document compression="1">
      <template pageSize="(21cm, 29.7cm)">
        <pageTemplate id="main">
          <frame id="first" x1="1cm" y1="1cm" width="19cm" height="247mm"/>
        </pageTemplate>
      </template>
      
      <stylesheet> 
      	<paraStyle name="text"
      		fontName="Helvetica"
      		fontSize="10" />
      		
      	<paraStyle name="titol"
      		fontName="Helvetica"
      		fontSize="14"
      		leading="16"
      		alignment="center" />
      		
      	<paraStyle name="vermell"
      		textColor="darkred"
      		fontName="Helvetica-Bold"
      		fontSize="10" />
      		
      	<paraStyle name="blau"
      		textColor="darkblue"
      		fontName="Helvetica-Bold"
      		fontSize="10"
      		leftIndent="0.5cm" />
      		
      	<blockTableStyle id="taula2">
      		<lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
      		<blockBackground colorName="silver" start="0,0" stop="-1,0" />
      	</blockTableStyle>
      		
      	<blockTableStyle id="taula1">
      		<lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEABOVE" colorName="black" start="0,0" stop="-1,0" />
      		<lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
      		<blockBackground colorName="silver" start="0,0" stop="-1,0" />
      	</blockTableStyle>
      		
      		
      	<blockTableStyle id="taula">
          <blockFont name="Helvetica" size="10" />
          <lineStyle kind="LINEBEFORE" colorName="black" start="0,0" stop="0,0" />
          <lineStyle kind="GRID" colorName="black" start="0,1" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="0,1" stop="-1,-1"/>
          <lineStyle kind="GRID" colorName="black" start="1,0" stop="-1,0" />
          <lineStyle kind="LINEAFTER" colorName="black" start="0,0" stop="1,0" />
          <lineStyle kind="LINEAFTER" colorName="black" start="6,0" stop="6,0" />

          <blockBackground colorName="silver" start="0,0" stop="-1,0" />
        </blockTableStyle>

      		
      </stylesheet>
    
      <story>
      	<xsl:apply-templates select="estadistica" mode="story"/>
      </story>
    </document>
  </xsl:template>
  
  <xsl:template match="estadistica" mode="story">
  
      	<para style="titol" t="1">ESTAD�STICA DE LA IND�STRIA D'ENERGIA EL�CTRICA (<xsl:value-of select="any" />)</para>
      	<spacer length="16" />
      	<para style="titol" t="1" leading="16">INSTAL�LACIONS PER AL TRANSPORT I DISTRIBUCI� D'ENERGIA EL�CTRICA PROPIETAT DE L'EMPRESA</para>
      	<spacer length="16" />
      	<xsl:variable name="year" select="any" />
      	<xsl:variable name="prov" select="provincia" />
      	<para style="text"><xsl:value-of select="provincia" /></para>
      		
      		<blockTable colWidths="5cm,10cm" style="taula2">
      			<tr>
      				<td><para style="text" t="1" alignment="center">L�NIA AT</para></td>
      				<td><para style="text" t="1" alignment="center">Longitud de l�nies el�ctriques (metres)</para></td>
      			</tr>
      		</blockTable>
      		<blockTable colWidths="5cm,7.5cm,2.5cm" style="taula1">
      			<tr>
      				<td></td>
      				<td><para style="text" t="1" alignment="center">A�ries</para></td>
      				<td><para style="text" t="1" alignment="center">Subterr�nies</para></td>
      			</tr>
      		</blockTable>
      		<blockTable style="taula" colWidths="5cm,2.5cm,2.5cm,2.5cm,2.5cm" repeatRows="1">
      			<tr>
      				<td></td>
      				<td><para style="text" t="1" alignment="center">1 cir.</para></td>
      				<td><para style="text" t="1" alignment="center">2 cir.</para></td>
      				<td><para style="text" t="1" alignment="center">M�s de 2 cir.</para></td>
      				<td></td>
      			</tr>
      			
      			<xsl:apply-templates match="linia" mode="story" />
      			
      		</blockTable>
      	<nextFrame />
</xsl:template>

<xsl:template match="linia" mode="story">
	<tr>
		<td><xsl:value-of select="nom" /></td>
		<td><xsl:value-of select="format-number(l_1c, '0')" /></td>
		<td><xsl:value-of select="format-number(l_2c, '0')" /></td>
		<td><xsl:value-of select="format-number(l_m2c, '0')" /></td>
		<td><xsl:value-of select="format-number(l_sub, '0')" /></td>
	</tr>
</xsl:template>
  
</xsl:stylesheet>
