# -*- coding: utf-8 -*-
{
    "name": "Convertir SIPS",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Convertir un registre SIPS a un contracte, CUPS i partner
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_polissa",
        "giscedata_polissa_responsable",
        "giscedata_sips",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_sips_convert_view.xml"
    ],
    "active": False,
    "installable": True
}
