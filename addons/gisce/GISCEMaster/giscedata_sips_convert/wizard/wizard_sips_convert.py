# -*- coding: utf-8 -*-
from osv import osv, fields
from addons.giscedata_sips.giscedata_sips import (
    get_distri, get_poblacio, get_municipi, get_tarifa, get_tensio_norm
)


class WizardSipsConvert(osv.osv_memory):
    _name = 'wizard.sips.convert'

    def _default_num_sips(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context.get('active_ids', []))

    def importar_sips(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0])
        sips_ids = context.get('active_ids', [])
        ps_obj = self.pool.get('giscedata.sips.ps')

        cups_obj = self.pool.get('giscedata.cups.ps')
        partner_obj = self.pool.get('res.partner')
        partner_addr_obj = self.pool.get('res.partner.address')
        polissa_obj = self.pool.get('giscedata.polissa')

        polisses = []

        for sips in ps_obj.read(cursor, uid, sips_ids, context=context):
            # Creem el CUPS
            cups_vals = {
                'name': sips['name'],
                'distribuidora_id': get_distri(self, cursor, uid,
                                               sips['distri'],
                                               sips['cod_distri']),
                'id_municipi': get_municipi(self, cursor, uid, sips['poblacio']),
                'id_poblacio': get_poblacio(self, cursor, uid, sips['poblacio']),
                'nv': sips['direccio'],
                'dp': sips['codi_postal']
            }
            cups_id = cups_obj.create(cursor, uid, cups_vals)

            titular_vals = {
                'name': '{}, {}'.format(sips['cognom'], sips['nom']),
                'customer': 1,
            }

            titular_id = partner_obj.create(cursor, uid, titular_vals)

            titular_addr_vals = {
                'partner_id': titular_id,
                'name': titular_vals['name'],
                'street': sips['direccio_titular'],

            }

            titular_addr_id = partner_addr_obj.create(cursor, uid,
                                                      titular_addr_vals)

            vals_polissa = {
                'name': 'SIPS_%s' % sips['id'],
                'cups': cups_id,
                'titular': titular_id,
                'distribuidora': cups_vals['distribuidora_id'],
                'potencia': max([sips['pot_cont_p%s' % j]
                                 for j in range(1, 7)]),
                'tarifa': get_tarifa(self, cursor, uid, sips['tarifa']),
                'tensio_normalitzada': get_tensio_norm(self, cursor, uid,
                                                       sips['tensio']),
                'user_id': uid,
                'state': 'esborrany'

            }

            polissa_id = polissa_obj.create(cursor, uid, vals_polissa)
            polissa_obj.generar_periodes_potencia(cursor, uid, [polissa_id])
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            for idx, pot in enumerate(polissa.potencies_periode):
                key = 'pot_cont_p%s' % (idx + 1)
                pot.write({'potencia': sips[key]})

            polisses.append(polissa_id)
            # Desactivem el SIPS i el marquem com a exportat
            ps_obj.write(cursor, uid, [sips['id']], {
                'active': 0, 'exportat': 1
            })

        wiz.write({'sips_importats': len(polisses), 'state': 'done'})
        return True

    _columns = {
        'num_sips': fields.integer('Número de sips a importar', readonly=True),
        'sips_importats': fields.integer('Número de sips importats',
                                         readonly=True),
        'state': fields.char('Estat', size=16)
    }

    _defaults = {
        'num_sips': _default_num_sips,
        'state': lambda *a: 'init'
    }

WizardSipsConvert()

