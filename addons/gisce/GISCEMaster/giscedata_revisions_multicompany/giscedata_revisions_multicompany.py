# -*- coding: iso-8859-1 -*-
from osv import osv, fields

class giscedata_revisions_ct_revisio(osv.osv):
	_name = "giscedata.revisions.ct.revisio"
	_inherit = "giscedata.revisions.ct.revisio"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_revisions_ct_revisio()

class giscedata_revisions_at_revisio(osv.osv):
	_name = "giscedata.revisions.at.revisio"
	_inherit = "giscedata.revisions.at.revisio"
	_columns = {
		'company_id': fields.many2one('res.company', 'Company', required=True),
	}
	_defaults = {
		#'company_id': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.id,
	}
giscedata_revisions_at_revisio()

