# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Revisions Multicompany",
    "description": """Revisions 


 $Id: __terp__.py 324 2007-08-17 14:17:57Z eduard $""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Multi companyy",
    "depends":[
        "giscedata_revisions"
    ],
    "init_xml":[
        "record_rules.xml"
    ],
    "demo_xml": [],
    "update_xml":[
        "giscedata_revisions_multicompany_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
