# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from addons import get_module_resource


class TestImportCNMCSIPSData(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.polissa_obj = self.pool.get('giscedata.polissa')
        self.sips_data_obj = self.pool.get('giscedata.cnmc.sips.comer.ps')
        self.sips_cons_obj = self.pool.get('giscedata.cnmc.sips.consums.comer.ps')
        self.txn = Transaction().start(self.database)

        sips_info_path = get_module_resource(
            'giscedata_cnmc_sips_comer', 'tests', 'data', 'SIPS_PS_ELECTRICIDAD.txt'
        )
        self.sips_info = open(sips_info_path, 'r')

        cons_info_path = get_module_resource(
            'giscedata_cnmc_sips_comer', 'tests', 'data', 'SIPS2_CONSUMOS_ELECTRICIDAD_30A.txt'
        )
        self.cons_info = open(cons_info_path, 'r')

    def tearDown(self):
        self.sips_info.close()
        self.txn.stop()

    def test_creation_import_cnmc_sips_data(self):

        uid = self.txn.user
        cursor = self.txn.cursor
        datas = self.sips_info

        for data in datas:
            data = eval(data)
            new_id = self.sips_data_obj.create(cursor, uid, data)
            new_cups = self.sips_data_obj.read(cursor, uid, new_id, ['cups'])['cups']
            expect(data['cups']).to(equal(new_cups))

    def test_update_import_cnmc_sips_data(self):

        uid = self.txn.user
        cursor = self.txn.cursor
        datas = self.sips_info

        for data in datas:
            data = eval(data)
            new_id = self.sips_data_obj.create(cursor, uid, data)
            new_item = self.sips_data_obj.read(cursor, uid, new_id, ['cups', 'CNAE'])
            expect(data['CNAE']).to(equal(new_item['CNAE']))

            data['CNAE'] = '9820'
            self.sips_data_obj.create(cursor, uid, data)
            new_item = self.sips_data_obj.read(cursor, uid, new_id, ['cups', 'CNAE'])
            expect(data['cups']).to(equal(new_item['cups']))
            expect(data['CNAE']).to(equal(new_item['CNAE']))

    def test_creation_import_cnmc_consumptions_data(self):

        uid = self.txn.user
        cursor = self.txn.cursor
        datas = self.cons_info

        for data in datas:
            data = eval(data)
            new_id = self.sips_cons_obj.create(cursor, uid, data)
            new_item = self.sips_cons_obj.read(cursor, uid, new_id, ['cups', 'fechaInicioMesConsumo', 'fechaFinMesConsumo'])
            expect(data['cups']).to(equal(new_item['cups']))
            expect(data['fechaInicioMesConsumo']).to(equal(new_item['fechaInicioMesConsumo']))
            expect(data['fechaFinMesConsumo']).to(equal(new_item['fechaFinMesConsumo']))

    def test_update_import_cnmc_consumptions_data(self):

        uid = self.txn.user
        cursor = self.txn.cursor
        datas = self.cons_info

        for data in datas:
            data = eval(data)
            new_id = self.sips_cons_obj.create(cursor, uid, data)
            new_item = self.sips_cons_obj.read(cursor, uid, new_id, ['consumoEnergiaActivaEnWhP1'])
            expect(data['consumoEnergiaActivaEnWhP1']).to(equal(new_item['consumoEnergiaActivaEnWhP1']))

            data['consumoEnergiaActivaEnWhP1'] = '11111'
            self.sips_cons_obj.create(cursor, uid, data)
            new_item = self.sips_cons_obj.read(cursor, uid, new_id, ['cups', 'fechaInicioMesConsumo', 'fechaFinMesConsumo', 'consumoEnergiaActivaEnWhP1'])
            expect(data['cups']).to(equal(new_item['cups']))
            expect(data['fechaInicioMesConsumo']).to(equal(new_item['fechaInicioMesConsumo']))
            expect(data['fechaFinMesConsumo']).to(equal(new_item['fechaFinMesConsumo']))
            expect(data['consumoEnergiaActivaEnWhP1']).to(equal(new_item['consumoEnergiaActivaEnWhP1']))
