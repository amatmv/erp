# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from expects import *
from addons import get_module_resource
import csv
from giscedata_cnmc_sips_comer import giscedata_cnmc_sips_comer
import mock


class TestImportCNMCSIPSData(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.contract_obj = self.pool.get('giscedata.polissa')
        self.sips_data_obj = self.pool.get('giscedata.cnmc.sips.comer.ps')
        self.sips_cons_obj = self.pool.get('giscedata.cnmc.sips.consums.comer.ps')
        self.wizard_obj = self.pool.get('wizard.update.contract.sips.data')
        self.txn = Transaction().start(self.database)

        self.sips_info_path = get_module_resource(
            'giscedata_cnmc_sips_comer', 'tests', 'data', 'SIPS_PS_ELECTRICIDAD.txt'
        )
        self.sips_info = open(self.sips_info_path, 'r')

    def tearDown(self):
        self.sips_info.close()
        self.txn.stop()

    @mock.patch('giscedata_cnmc_sips_comer.giscedata_cnmc_sips_comer.fetch_SIPS')
    def test_no_update_sips_data_on_already_filled_contract(self, mock_function):
        # Construim una resposta de mentida que fem que retorni giscedata_cnmc_
        # sips_comer.fetch_SIPS.
        # Les dades d'aquesta resposta son diferents a les que tindren al
        # contracte per despres validar que no es modifica el contracte
        response_file = open(self.sips_info_path, 'r')
        csv_data = response_file.read()
        mock_function.return_value = [eval(csv_data)]

        uid = self.txn.user
        cursor = self.txn.cursor
        datas = self.sips_info

        wiz_id = self.wizard_obj.create(cursor, uid, {})
        wiz = self.wizard_obj.browse(cursor, uid, wiz_id, {})
        for data in datas:
            data = eval(data)
            new_id = self.sips_data_obj.create(cursor, uid, data)
            new_cups = self.sips_data_obj.read(cursor, uid, new_id, ['cups'])['cups']
        contract_id = self.contract_obj.search(cursor, uid, [('cups', '=', new_cups)])[0]
        contract = self.contract_obj.browse(cursor, uid, contract_id)
        expect(contract.cnae.name).to(equal('0111'))
        expect(contract.tipus_vivenda).to(equal('habitual'))
        expect(contract.tarifa.codi_ocsum).to(equal('001'))
        expect(contract.distribuidora.id).to(equal(2))
        expect(contract.potencia).to(equal(6.0))
        expect(len(contract.potencies_periode)).to(equal(1))

        wiz.action_update_contract_sips_data(context={'active_ids': [1]})

        contract = self.contract_obj.browse(cursor, uid, contract_id)
        expect(contract.cnae.name).to(equal('0111'))
        expect(contract.tipus_vivenda).to(equal('habitual'))
        expect(contract.tarifa.codi_ocsum).to(equal('001'))
        expect(contract.distribuidora.id).to(equal(2))
        expect(contract.potencia).to(equal(6.0))
        expect(len(contract.potencies_periode)).to(equal(1))

    @mock.patch('giscedata_cnmc_sips_comer.giscedata_cnmc_sips_comer.fetch_SIPS')
    def test_update_sips_data_on_empty_contract(self, mock_function):
        # Construim una resposta de mentida que fem que retorni giscedata_cnmc_
        # sips_comer.fetch_SIPS.
        # Les dades buides s'agafaran d'aquesta resposta. El camp
        # 'tipus_vivenda' mantindra el valor original perque no esta buit.
        response_file = open(self.sips_info_path, 'r')
        csv_data = response_file.read()
        mock_function.return_value = [eval(csv_data)]

        uid = self.txn.user
        cursor = self.txn.cursor
        datas = self.sips_info

        wiz_id = self.wizard_obj.create(cursor, uid, {})
        wiz = self.wizard_obj.browse(cursor, uid, wiz_id, {})
        for data in datas:
            data = eval(data)
            data['cups'] = 'ES8756113412585495XW'
            data['codigoEmpresaDistribuidora'] = '0031'
            new_id = self.sips_data_obj.create(cursor, uid, data)
            new_cups = self.sips_data_obj.read(cursor, uid, new_id, ['cups'])['cups']
        contract_id = self.contract_obj.search(cursor, uid, [('cups', '=', new_cups)])[0]
        contract = self.contract_obj.browse(cursor, uid, contract_id)
        contract.write({
            'cnae': False,
            'tarifa': False,
            'distribuidora': False,
            'potencia': 0,
            'potencies_periode': [(6,0,[])],
            'tipus_vivenda': 'habitual'
        })
        contract = self.contract_obj.browse(cursor, uid, contract_id)
        expect(bool(contract.cnae)).to(be_false)
        expect(contract.tipus_vivenda).to(equal('habitual'))
        expect(bool(contract.tarifa)).to(be_false)
        expect(bool(contract.distribuidor)).to(be_false)
        expect(contract.potencia).to(equal(0))
        expect(len(contract.potencies_periode)).to(equal(0))

        wiz.action_update_contract_sips_data(context={'active_ids': [7]})

        contract = self.contract_obj.browse(cursor, uid, contract_id)
        expect(contract.cnae.name).to(equal('0115'))
        expect(contract.tipus_vivenda).to(equal('habitual'))
        expect(contract.tarifa.codi_ocsum).to(equal('003'))
        expect(contract.distribuidora.name).to(equal('La Tester'))
        expect(contract.potencia).to(equal(43.646))
        expect(len(contract.potencies_periode)).to(equal(3))
