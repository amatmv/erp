Camp SIPS | Camp contracte
--- | --- 
'codigoEmpresaDistribuidora' | ref_dist, distribuidora
'cups' | cups
'nombreEmpresaDistribuidora' | distribuidora
'codigoPostalPS' | cups_cp
'municipioPS' | cups_poblacio
'codigoProvinciaPS' | **cups.state_id?**
'fechaAltaSuministro' | **data_alta?**
'codigoTarifaATREnVigor' | agree_tarifa, tarifa, tarifa_codi
'codigoTensionV' | agree_tensio, tensio
'potenciaMaximaBIEW' | 
'potenciaMaximaAPMW' | 
'codigoClasificacionPS' | agree_tipus
'codigoDisponibilidadICP' | 
'tipoPerfilConsumo' | 
'valorDerechosExtensionW' | 
'valorDerechosAccesoW' | potencia(kw)
'codigoPropiedadEquipoMedida' | **comptador.lloguer?**
'codigoPropiedadICP' | **comptador.lloguer?**
'potenciasContratadasEnWP1' | potencies_periode
'potenciasContratadasEnWP2' | potencies_periode
'potenciasContratadasEnWP3' | potencies_periode
'potenciasContratadasEnWP4' | potencies_periode
'potenciasContratadasEnWP5' | potencies_periode
'potenciasContratadasEnWP6' | potencies_periode
'fechaUltimoMovimientoContrato' | 
'fechaUltimoCambioComercializador' | 
'fechaLimiteDerechosReconocidos' | 
'fechaUltimaLectura' | data_ultima_lectura
'informacionImpagos' | **cups.observacions?**
'importeDepositoGarantiaEuros' | deposit
'tipoIdTitular' | 
'esViviendaHabitual' | tipus_vivenda, **9820 cnae?**
'codigoComercializadora' | comercialitzadora
'codigoTelegestion' | tg
'codigoFasesEquipoMedida' | comptador(trifasic, monofasic)
'codigoAutoconsumo' | autoconsumo
'codigoTipoContrato' | contract_type
'codigoPeriodicidadFacturacion' | facturacio
'codigoBIE' | 
'fechaEmisionBIE' | 
'fechaCaducidadBIE' | 
'codigoAPM' | 
'fechaEmisionAPM' | 
'fechaCaducidadAPM' | 
'relacionTransformacionIntensidad' | 
'CNAE' | cnae
'codigoModoControlPotencia' | comptador(ICP, maximetre, sense control)
'potenciaCGPW' | 
'codigoDHEquipoDeMedida' | agree_dh
'codigoAccesibilidadContador' | 
'codigoPSContratable' | 
'motivoEstadoNoContratable' | 
'codigoTensionMedida' | 
'codigoClaseExpediente' | 
'codigoMotivoExpediente' | 
'codigoTipoSuministro' | 
'aplicacionBonoSocial' | bono_social_disponible
