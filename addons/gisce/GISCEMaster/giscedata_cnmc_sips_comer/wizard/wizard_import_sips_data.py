# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from tools.translate import _
from osv import osv, fields
import base64


class WizardImportSIPSData(osv.osv_memory):

    _name = 'wizard.import.cnmc.sips.data'

    def action_import_cnmc_sips_data(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        sips_obj = False
        info = ''
        if wizard.file_type == 'ps_info':
            sips_obj = self.pool.get('giscedata.cnmc.sips.comer.ps')
        elif wizard.file_type == 'ps_consume':
            sips_obj = self.pool.get('giscedata.cnmc.sips.consums.comer.ps')
        else:
            wizard.write({
                'info': 'This type of file is not supported'
            })
        if sips_obj:
            cups_names = [c.strip() for c in (wizard.cups_names or "").split(";")]
            if wizard.mode == 'import_erp':
                # Get SIPS file as CSV reader
                result = sips_obj.import_sips_data(cursor, uid, cups_names,
                                                   context=context)
                for cups, res in result.items():
                    if res:
                        info += _('Descarga de datos correcta para el CUPS '
                                  '{}\n').format(cups)
                    else:
                        info += _('The data retrieved is not compatible for the '
                                 'requested CUPS ({})\n').format(cups)
            else:
                result = sips_obj.download_sips_data(cursor, uid, cups_names,
                                                     context=context)
                wizard.write({'report': base64.b64encode(result), 'filename_report': wizard.file_type+".csv"})
                info = "Informació descarregada per els CUPS {}\n".format(cups_names)

            wizard.write({
                'info': info
            })

    def _default_cups(self,cursor, uid, context=None):
        if context is None:
            context = {}
        if context.get("from_cups", False):
            cups_obj = self.pool.get('giscedata.cups.ps')
            cups_names = ""
            for inf in cups_obj.read(cursor, uid,  context['active_ids'], ['name']):
                if inf['name']:
                    cups_names += inf['name'].strip() + ";"
            return cups_names
        else:
            return "Escriu aqui els cups a consultar separats per ';'.\n"

    _columns = {
        'file_type': fields.selection([('ps_info', 'CUPS Information (SIPS2_PS_ELECTRICIDAD)'),
                                       ('ps_consume', 'CUPS Consumption (SIPS2_CONSUMOS_ELECTRICIDAD)')],
                                      'File type', required=True),
        'info': fields.text('Info'),
        'cups_names': fields.text('Cups a consultar', help="Codis de CUPS a consultar separats per ';'"),
        'mode': fields.selection([('import_erp', 'Guardar dades al ERP'), ('import_file', 'Descarregar CSV')], "Acció"),
        'report': fields.binary('Resultat'),
        'filename_report': fields.char('Nom fitxer exportat', size=256),
    }

    _defaults = {
        'info': lambda *a: 'Wizard to import SIPS data for the specified CUPS',
        'mode': lambda *a: 'import_erp',
        'file_type': lambda *a: 'ps_consume',
        'cups_names': _default_cups,
    }

WizardImportSIPSData()
