# -*- coding: utf-8 -*-
from __future__ import (absolute_import)
from osv import osv, fields
from tools.translate import _


class WizardUpdateContractSIPSData(osv.osv_memory):

    _name = 'wizard.update.contract.sips.data'

    def action_update_contract_sips_data(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context=context)
        sips_obj = self.pool.get('giscedata.cnmc.sips.comer.ps')
        info = ''
        contracts = context['active_ids']
        result = sips_obj.import_and_update_contract_sips_data(cursor, uid,
                                                               contracts,
                                                               context=context)
        for contract, res in result.items():
            if res:
                info += res
            else:
                info += _('This contract {} could not be updated\n').format(
                    contract)

        wizard.write({'info': info})

    _columns = {
        'info': fields.text('Info')
    }

    _defaults = {
        'info': lambda *a: 'Wizard to import SIPS data for the specified CUPS'
    }

WizardUpdateContractSIPSData()
