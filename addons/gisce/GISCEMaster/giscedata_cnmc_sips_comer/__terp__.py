# -*- coding: utf-8 -*-
{
    "name": "GISCE CNMC SIPS v3",
    "description": """ CNMC SIPS v3 generation

  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_butlletins",
        "giscedata_tensions"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cnmc_sips_comer_view.xml",
        "wizard/wizard_import_sips_data_view.xml",
        "wizard/wizard_update_contract_sips_data_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
