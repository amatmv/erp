# -*- coding: utf-8 -*-

from osv import osv, fields
from mongodb_backend import osv_mongodb
from gestionatr.defs import TABLA_6, TABLA_9, TABLA_17, TABLA_30, TABLA_32, TABLA_35, TABLA_44, TABLA_62, TABLA_64, TABLA_108, TABLA_111
from cnmc_client import Client
from tools import config
from tools.translate import _
import csv
import StringIO

_config = {
    'environment': config.get('cnmc_sips_environ', 'prod'),
    'key': config.get('cnmc_sips_key', False),
    'secret': config.get('cnmc_sips_secret', False),
}


def fetch_SIPS(cups, file_type, as_csv=False):
    client = Client(**_config)
    response = client.fetch(cups=cups, file_type=file_type, as_csv=as_csv)
    assert not response.error
    return response.result


class GiscedataCNMCSipsComerPs(osv_mongodb.osv_mongodb):
    _name = 'giscedata.cnmc.sips.comer.ps'

    _cnmc_file_name = 'SIPS2_PS_ELECTRICIDAD'

    def create(self, cursor, uid, data, context=None):
        reg_id = self.search(cursor, uid, [('cups', '=', data['cups'])])
        if not reg_id:
            reg_id = super(GiscedataCNMCSipsComerPs, self).create(cursor, uid,
                                                          data, context=context)
        else:
            self.write(cursor, uid, reg_id, data, context=context)
            reg_id = reg_id[0]
        return reg_id

    def import_and_update_contract_sips_data(self, cursor, uid, ids,
                                             context=None):
        contract_obj = self.pool.get('giscedata.polissa')
        _cups = contract_obj.read(cursor, uid, ids, ['cups'],
                                  context=context)
        cups_names = [cups['cups'][1] for cups in _cups]
        self.import_sips_data(cursor, uid, cups_names, context=context)
        updated = self.update_contract_sips_data(cursor, uid, ids,
                                                 context=context)
        return updated

    def import_sips_data(self, cursor, uid, cups_names, context=None):
        # Get SIPS file as CSV reader
        file_name = self._cnmc_file_name
        SIPS_csv = fetch_SIPS(cups_names, file_name, as_csv=True)
        result = {}
        for line in SIPS_csv:
            if line['cups'] in cups_names:
                sips_id = self.create(cursor, uid, line, context=context)
                result[line['cups']] = sips_id
            else:
                result[line['cups']] = False
        return result

    def download_sips_data(self, cursor, uid, cups_names, context=None):
        file_name = self._cnmc_file_name
        SIPS_csv = fetch_SIPS(cups_names, file_name, as_csv=True)
        csvfile = StringIO.StringIO()
        writer = csv.DictWriter(csvfile, fieldnames=SIPS_csv.fieldnames)
        writer.writeheader()
        for line in SIPS_csv:
            writer.writerow(line)
        return csvfile.getvalue()

    def update_contract_sips_data(self, cursor, uid, ids, context=None):
        contract_obj = self.pool.get('giscedata.polissa')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        result = {}
        info = ''
        for contract in contract_obj.browse(cursor, uid, ids, context=context):
            if contract.state != 'esborrany':
                result[str(contract.name)] = False
                continue
            cups_name = contract.cups.name
            sips_id = self.search(cursor, uid, [('cups', '=', cups_name)])
            if sips_id:
                info += self.update_data(cursor, uid, contract, sips_id[0],
                                         info, cups_name, context=context)
                result[str(contract.name)] = info
            else:
                result[str(contract.name)] = False
        return result

    def update_data(self, cursor, uid, contract, sips_id, info, cups_name,
                    context=None):
        contract_obj = self.pool.get('giscedata.polissa')
        sips_obj = self.pool.get('giscedata.cnmc.sips.comer.ps')
        sips_data = sips_obj.read(cursor, uid, sips_id, context=context)
        mapped_dict, mod_info = self.get_mapped_sips_data(cursor, uid,
                                                          sips_data,
                                                          contract)
        if not mapped_dict:
            info = info + _(
                'No changes. The contract for CUPS {} data is already up to date\n').format(
                cups_name)
        else:
            contract_obj.write(cursor, uid, contract.id, mapped_dict)
            info = info + _(
                'The contract for CUPS {} data has been updated\n').format(
                cups_name) + mod_info
            contract = contract_obj.browse(cursor, uid, contract.id,
                                           context=context)
        power_info = self.update_contract_power_fields(
            cursor, uid, sips_data, contract, context=context)
        info += power_info
        return info

    def get_mapped_sips_data(self, cursor, uid, sips_data, contract, context=None):
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        cnae_obj = self.pool.get('giscemisc.cnae')
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        vals = {}
        info = ''
        distri_code = sips_data.get('codigoEmpresaDistribuidora', False)
        if sips_data.get('CNAE', False):
            cnae_id = cnae_obj.search(cursor, uid, [('name', '=', sips_data['CNAE'])])
            if cnae_id:
                current_cnae = contract.cnae.id
                if not current_cnae:
                    vals['cnae'] = cnae_id[0]
                    info = info + _('  - CNAE field set to : {}\n').format(sips_data['CNAE'])
        if sips_data.get('esViviendaHabitual', False):
            if not contract.tipus_vivenda:
                vals['tipus_vivenda'] = 'habitual' if sips_data['esViviendaHabitual'] == '1' else 'no_habitual'
                info = info + _('  - Housing type field set to: {}\n').format(vals['tipus_vivenda'])
        if sips_data.get('codigoTarifaATREnVigor', False):
            tariff_id = tariff_obj.search(cursor, uid, [('codi_ocsum', '=', sips_data['codigoTarifaATREnVigor'])])
            if tariff_id:
                current_tariff = contract.tarifa.id
                if not current_tariff:
                    vals['tarifa'] = tariff_id[0]
                    tariff_name = tariff_obj.read(cursor, uid, tariff_id[0], ['name'])['name']
                    info = info + _('  - Tariff field set to: {}\n').format(tariff_name)
        if distri_code:
            current_distri_id = contract.distribuidora.id
            if not current_distri_id:
                sips_distri_id = partner_obj.search(cursor, uid, [('ref', '=', distri_code)])
                if sips_distri_id:
                    vals['distribuidora'] = sips_distri_id[0]
                    distri_name = partner_obj.read(cursor, uid, sips_distri_id[0], ['name'])['name']
                    info = info + _('  - Distributor company set to: {}\n').format(distri_name)
                elif sips_data.get('nombreEmpresaDistribuidora', False):
                    # Create a new distri partner
                    distri_address_vals = {
                        'name': sips_data['nombreEmpresaDistribuidora']
                    }
                    new_distri_address_id = partner_address_obj.create(
                        cursor, uid, distri_address_vals, context=context)
                    distri_vals = {
                        'ref': distri_code,
                        'name': sips_data['nombreEmpresaDistribuidora'],
                        'address': [(6, 0, [new_distri_address_id])]
                    }
                    new_distri_id = partner_obj.create(
                        cursor, uid, distri_vals, context=context)
                    vals['distribuidora'] = new_distri_id
                    current_distri_id = new_distri_id
                    info = info + _(
                        '  - New distributor company created and set: {}\n').format(
                        distri_vals['name'])
            if not contract.cups.distribuidora_id:
                contract.cups.write(
                    {'distribuidora_id': current_distri_id}, context=context)
        return vals, info

    def update_contract_power_fields(self, cursor, uid, sips_data, contract, context=None):
        contract_obj = self.pool.get('giscedata.polissa')
        info = ''
        power_periods = {}
        info_periods = ''
        if not contract.potencia:
            if sips_data.get('valorDerechosAccesoW', False):
                max_power = float(sips_data['valorDerechosAccesoW'])/1000
                contract_obj.write(cursor, uid, contract.id, {'potencia': max_power})
                info = info + _('The power of the contract has been modified to'
                                ': {}\n').format(max_power)
        if not contract.potencies_periode:
            if sips_data.get('potenciasContratadasEnWP1', False):
                power_periods['P1'] = sips_data['potenciasContratadasEnWP1']
            if sips_data.get('potenciasContratadasEnWP2', False):
                power_periods['P2'] = sips_data['potenciasContratadasEnWP2']
            if sips_data.get('potenciasContratadasEnWP3', False):
                power_periods['P3'] = sips_data['potenciasContratadasEnWP3']
            if sips_data.get('potenciasContratadasEnWP4', False):
                power_periods['P4'] = sips_data['potenciasContratadasEnWP4']
            if sips_data.get('potenciasContratadasEnWP5', False):
                power_periods['P5'] = sips_data['potenciasContratadasEnWP5']
            if sips_data.get('potenciasContratadasEnWP6', False):
                power_periods['P6'] = sips_data['potenciasContratadasEnWP6']
            if power_periods:
                info_periods = self.check_and_overwrite_power(
                    cursor, uid, power_periods, contract, context=context)
        if power_periods and info_periods:
            info = info + _('These power periods were created:\n') + info_periods
        if not info:
            info = info + _('No power periods were modified\n')
        return info

    def check_and_overwrite_power(self, cursor, uid, power_periods, contract, context=None):
        period_obj = self.pool.get('giscedata.polissa.potencia.contractada.periode')
        tariff_period_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        info = ''
        for period, power in power_periods.items():
            t_period_id = tariff_period_obj.search(cursor, uid, [
                ('name', '=', period), ('tipus', '=', 'tp'),
                ('tarifa', '=', contract.tarifa.id)])
            if t_period_id:
                kw_power = float(power)/1000
                period_obj.create(cursor, uid, {'active': True,
                                                'periode_id': t_period_id[0],
                                                'polissa_id': contract.id,
                                                'potencia': kw_power})
                info = info + _('  - Power of {} period set to: {}\n').format(period, kw_power)
        return info

    _columns = {
        'codigoEmpresaDistribuidora': fields.char('Código distribuidora', size=4, readonly=True),
        'cups': fields.char('CUPS', size=22, readonly=True, required=True),
        'nombreEmpresaDistribuidora': fields.char('Empresa distribuidora', size=60, readonly=True),
        'codigoPostalPS': fields.char('Código postal', size=5, readonly=True),
        'municipioPS': fields.char('Municipio', size=5, readonly=True),
        'codigoProvinciaPS': fields.char('Provincia', size=2, readonly=True),
        'fechaAltaSuministro': fields.date('Alta suministro', readonly=True),
        'codigoTarifaATREnVigor': fields.selection(TABLA_17, 'Tarifa ATR', readonly=True),
        'codigoTensionV': fields.selection(TABLA_64, 'Tensión (v)', readonly=True),
        'potenciaMaximaBIEW': fields.char('Potencia máxima BIE', readonly=True, size=16),
        'potenciaMaximaAPMW': fields.char('Potencia máxima APM', readonly=True, size=16),
        'codigoClasificacionPS': fields.selection(TABLA_30, 'Clasificación PS', readonly=True),
        'codigoDisponibilidadICP': fields.char('Disponibilidad ICP', size=1, readonly=True),
        'tipoPerfilConsumo': fields.char('Tipo perfil consumo', size=2, readonly=True),
        'valorDerechosExtensionW': fields.char('Valor derechos extensión (w)', readonly=True, size=16),
        'valorDerechosAccesoW': fields.char('Valor derechos acceso (w)', readonly=True, size=16),
        'codigoPropiedadEquipoMedida': fields.selection(TABLA_32, 'Propiedad equipo medida', readonly=True),
        'codigoPropiedadICP': fields.selection(TABLA_32, 'Propiedad ICP', readonly=True, size=16),
        'potenciasContratadasEnWP1': fields.char('Potencia contratada P1', readonly=True, size=16),
        'potenciasContratadasEnWP2': fields.char('Potencia contratada P2', readonly=True, size=16),
        'potenciasContratadasEnWP3': fields.char('Potencia contratada P3', readonly=True, size=16),
        'potenciasContratadasEnWP4': fields.char('Potencia contratada P4', readonly=True, size=16),
        'potenciasContratadasEnWP5': fields.char('Potencia contratada P5', readonly=True, size=16),
        'potenciasContratadasEnWP6': fields.char('Potencia contratada P6', readonly=True, size=16),
        'fechaUltimoMovimientoContrato': fields.date('Último movimiento contrato', readonly=True),
        'fechaUltimoCambioComercializador': fields.date('Último cambio comercializador', readonly=True),
        'fechaLimiteDerechosReconocidos': fields.date('Límite derechos reconocidos', readonly=True),
        'fechaUltimaLectura': fields.date('Última lectura', readonly=True),
        'informacionImpagos': fields.char('Información impagos', size=255, readonly=True),
        'importeDepositoGarantiaEuros': fields.char('Importe depósito garantía', readonly=True, size=16),
        'tipoIdTitular': fields.selection(TABLA_6, 'Tipo ID titular', readonly=True),
        'esViviendaHabitual': fields.char('Vivienda habitual', size=1, readonly=True),
        'codigoComercializadora': fields.char('Código comercializadora', size=4, readonly=True),
        'codigoTelegestion': fields.selection(TABLA_111, 'Telegestión', readonly=True),
        'codigoFasesEquipoMedida': fields.char('Fases equipo medida', size=1, readonly=True),
        'codigoAutoconsumo': fields.char('Autoconsumo', size=2, readonly=True),
        'codigoTipoContrato': fields.selection(TABLA_9, 'Tipo contrato', readonly=True),
        'codigoPeriodicidadFacturacion': fields.selection(TABLA_108, 'Periodicidad facturación', readonly=True),
        'codigoBIE': fields.char('BIE', size=30, readonly=True),
        'fechaEmisionBIE': fields.date('Emisión BIE', readonly=True),
        'fechaCaducidadBIE': fields.date('Caducidad BIE', readonly=True),
        'codigoAPM': fields.char('APM', size=30, readonly=True),
        'fechaEmisionAPM': fields.date('Emisión APM', readonly=True),
        'fechaCaducidadAPM': fields.date('Caducidad APM', readonly=True),
        'relacionTransformacionIntensidad': fields.char('Transformación intensidad', size=15, readonly=True),
        'CNAE': fields.char('CNAE', size=4, readonly=True),
        'codigoModoControlPotencia': fields.char('Modo control potencia', size=1, readonly=True),
        'potenciaCGPW': fields.char('Potencia CGP', readonly=True, size=16),
        'codigoDHEquipoDeMedida': fields.selection(TABLA_35, 'Código DH equipo de medida', readonly=True),
        'codigoAccesibilidadContador': fields.char('Accesibilidad contador', size=1, readonly=True),
        'codigoPSContratable': fields.char('PS contratable', size=1, readonly=True),
        'motivoEstadoNoContratable': fields.char('Estado no contratable', size=255, readonly=True),
        'codigoTensionMedida': fields.selection(TABLA_64, 'Tensión medida', readonly=True),
        'codigoClaseExpediente': fields.char('Clase expediente', size=1, readonly=True),
        'codigoMotivoExpediente': fields.char('Motivo expediente', size=2, readonly=True),
        'codigoTipoSuministro': fields.selection(TABLA_62, 'Tipo suministro', readonly=True),
        'aplicacionBonoSocial': fields.char('Aplicación bono social', size=1, readonly=True),
    }

GiscedataCNMCSipsComerPs()


class GiscedataCNMCSipsConsumsComerPs(osv_mongodb.osv_mongodb):
    _name = 'giscedata.cnmc.sips.consums.comer.ps'

    _cnmc_file_name = 'SIPS2_CONSUMOS_ELECTRICIDAD'

    def create(self, cursor, uid, data, context=None):
        reg_id = self.search(cursor, uid, [('cups', '=', data['cups']),
                                           ('fechaInicioMesConsumo', '=', data['fechaInicioMesConsumo']),
                                           ('fechaFinMesConsumo', '=', data['fechaFinMesConsumo'])])
        if not reg_id:
            reg_id = super(GiscedataCNMCSipsConsumsComerPs, self).create(cursor,
                                                     uid, data, context=context)
        else:
            self.write(cursor, uid, reg_id, data, context=context)
            reg_id = reg_id[0]
        return reg_id

    def import_sips_data(self, cursor, uid, cups_names, context=None):
        # Get SIPS file as CSV reader
        file_name = self._cnmc_file_name
        SIPS_csv = fetch_SIPS(cups_names, file_name, as_csv=True)
        result = {}
        for line in SIPS_csv:
            if line['cups'] in cups_names:
                sips_id = self.create(cursor, uid, line, context=context)
                result[line['cups']] = sips_id
            else:
                result[line['cups']] = False
        return result

    def download_sips_data(self, cursor, uid, cups_names, context=None):
        file_name = self._cnmc_file_name
        SIPS_csv = fetch_SIPS(cups_names, file_name, as_csv=True)
        csvfile = StringIO.StringIO()
        writer = csv.DictWriter(csvfile, fieldnames=SIPS_csv.fieldnames)
        writer.writeheader()
        for line in SIPS_csv:
            writer.writerow(line)
        return csvfile.getvalue()

    _columns = {
        'cups': fields.char('CUPS', size=22, readonly=True, required=True),
        'fechaInicioMesConsumo': fields.date('Fecha inicio ', readonly=True),
        'fechaFinMesConsumo': fields.date('Fin', readonly=True),
        'codigoTarifaATR': fields.selection(TABLA_17, 'Tarifa ATR', readonly=True),
        'consumoEnergiaActivaEnWhP1': fields.char('Energía activa P1 (Wh)', readonly=True, size=16),
        'consumoEnergiaActivaEnWhP2': fields.char('Energía activa P2 (Wh)', readonly=True, size=16),
        'consumoEnergiaActivaEnWhP3': fields.char('Energía activa P3 (Wh)', readonly=True, size=16),
        'consumoEnergiaActivaEnWhP4': fields.char('Energía activa P4 (Wh)', readonly=True, size=16),
        'consumoEnergiaActivaEnWhP5': fields.char('Energía activa P5 (Wh)', readonly=True, size=16),
        'consumoEnergiaActivaEnWhP6': fields.char('Energía activa P6 (Wh)', readonly=True, size=16),
        'consumoEnergiaReactivaEnVArhP1': fields.char('Energía reactiva P1 (Wh)', readonly=True, size=16),
        'consumoEnergiaReactivaEnVArhP2': fields.char('Energía reactiva P2 (Wh)', readonly=True, size=16),
        'consumoEnergiaReactivaEnVArhP3': fields.char('Energía reactiva P3 (Wh)', readonly=True, size=16),
        'consumoEnergiaReactivaEnVArhP4': fields.char('Energía reactiva P4 (Wh)', readonly=True, size=16),
        'consumoEnergiaReactivaEnVArhP5': fields.char('Energía reactiva P5 (Wh)', readonly=True, size=16),
        'consumoEnergiaReactivaEnVArhP6': fields.char('Energía reactiva P6 (Wh)', readonly=True, size=16),
        'potenciaDemandadaEnWP1': fields.char('Potencia demandada P1 (Wh)', readonly=True, size=16),
        'potenciaDemandadaEnWP2': fields.char('Potencia demandada P2 (Wh)', readonly=True, size=16),
        'potenciaDemandadaEnWP3': fields.char('Potencia demandada P3 (Wh)', readonly=True, size=16),
        'potenciaDemandadaEnWP4': fields.char('Potencia demandada P4 (Wh)', readonly=True, size=16),
        'potenciaDemandadaEnWP5': fields.char('Potencia demandada P5 (Wh)', readonly=True, size=16),
        'potenciaDemandadaEnWP6': fields.char('Potencia demandada P6 (Wh)', readonly=True, size=16),
        'codigoDHEquipoDeMedida': fields.selection(TABLA_35, 'Tipus Discriminació Horaria', readonly=True),
        'codigoTipoLectura': fields.selection(TABLA_44, 'Origen Lectura', readonly=True),
    }

GiscedataCNMCSipsConsumsComerPs()
