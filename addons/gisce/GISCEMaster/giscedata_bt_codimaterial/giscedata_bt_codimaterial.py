# -*- coding: utf-8 -*-

from osv import osv, fields

class giscedata_bt_cables(osv.osv):
  _name = 'giscedata.bt.cables'
  _inherit = 'giscedata.bt.cables'

  def name_get(self, cr, uid, ids, context={}):
    if not len(ids):
      return []
    res = []
    for cable in self.browse(cr, uid, ids):
      if cable.name and cable.material:
        name = '%s "%s"' % (cable.name.strip(),  cable.material.name.strip())
      else:
        name = '%s' % (cable.name)
      res.append((cable.id, name))
    return res
giscedata_bt_cables()
