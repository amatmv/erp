# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Baixa Codi+Material",
    "description": """Fa que en el codi ja hi aparegui el material del cable""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Baixa Tensió",
    "depends":[
        "giscedata_bt"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
