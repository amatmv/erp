# -*- coding: utf-8 -*-
from osv import osv


class GiscedataTrafo(osv.osv):

    """
        The only purpose of this module is to remove the constrain of the field
        name of the table giscedata_transformador_trafo and allow to use
        alphabetic characters on it
    """

    _name = 'giscedata.transformador.trafo'
    _inherit = 'giscedata.transformador.trafo'
    _order = "name, id"

    def init(self, cr):
        cr.execute("""
            SELECT conname 
            FROM pg_constraint 
            WHERE conname = 'giscedata_transformador_trafo_name_numeric'
        """)
        constraint = cr.fetchall()
        if constraint and len(constraint):
            cr.execute("""
                ALTER TABLE giscedata_transformador_trafo 
                DROP CONSTRAINT giscedata_transformador_trafo_name_numeric
            """)


GiscedataTrafo()
