# -*- coding: utf-8 -*-
{
    "name": "Transformadors NoNameConstrain",
    "description": """
    Modul que elimina la constrain sobre el camp 'name' del model GiscedataTransformadorTrafo
    permetent a l'usuari definir noms alfanumèrics per als Transformadors.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Centres Transformadors/Transformadors",
    "depends":[
        "giscedata_transformadors"
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
