# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_comerdist_polissa_dist
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-04-25 16:11:02+0000\n"
"PO-Revision-Date: 2012-04-25 16:11:02+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_comerdist_polissa_dist
#: model:ir.module.module,description:giscedata_comerdist_polissa_dist.module_meta_information
msgid "\n"
"    This module provide :\n"
"      * Part específica de sincronització de pòlisses en la distribuidora.\n"
"    "
msgstr ""

#. module: giscedata_comerdist_polissa_dist
#: model:ir.module.module,shortdesc:giscedata_comerdist_polissa_dist.module_meta_information
msgid "Comerdist pòlissa (distribuidora)"
msgstr ""

