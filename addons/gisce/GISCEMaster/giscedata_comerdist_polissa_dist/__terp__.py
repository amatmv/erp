# -*- coding: utf-8 -*-
{
    "name": "Comerdist pòlissa (distribuidora)",
    "description": """
    This module provide :
      * Part específica de sincronització de pòlisses en la distribuidora.
    """,
    "version": "0-dev",
    "author": "GISCe",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_comerdist_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
