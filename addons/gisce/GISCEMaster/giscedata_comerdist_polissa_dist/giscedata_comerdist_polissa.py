# -*- coding: utf-8 -*-
from osv import osv

class GiscedataPolissa(osv.osv):
    """Pòlissa per sincronitzar.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si aquest objecte s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for modcont in self.browse(cursor, uid, ids, context):
            if modcont.comercialitzadora and \
            modcont.comercialitzadora.id in partner_ids and \
            modcont.get_config().user_local.id != uid:
                return True
            else:
                return False

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for polissa in self.browse(cursor, uid, ids, context):
            partner_id = polissa.comercialitzadora.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            config = config_obj.browse(cursor, uid, config_ids[0])
            return config

GiscedataPolissa()

class GiscedataPolissaModcontractual(osv.osv):
    """Modificacions contractuals per sincronitzar.
    """
    _name = 'giscedata.polissa.modcontractual'
    _inherit = 'giscedata.polissa.modcontractual'

    def must_be_synched(self, cursor, uid, ids, context=None):
        """Comprova si aquest objecte s'ha de sincronitzar o no.
        """
        if not context:
            context = {}
        if not context.get('sync', True):
            return False
        config_obj = self.pool.get('giscedata.comerdist.config')
        config_ids = config_obj.search(cursor, uid, [])
        if not config_ids:
            return False
        partner_ids = [config['partner_id'][0]
                       for config in config_obj.read(cursor, uid, config_ids,
                                                     ['partner_id'])]
        for modcont in self.browse(cursor, uid, ids, context):
            if modcont.comercialitzadora and \
            modcont.comercialitzadora.id in partner_ids and \
            modcont.get_config().user_local.id != uid:
                return True
            else:
                return False

    def get_config(self, cursor, uid, ids, context=None):
        """Retorna l'objecte config.
        """
        config_obj = self.pool.get('giscedata.comerdist.config')
        for modcont in self.browse(cursor, uid, ids, context):
            partner_id = modcont.comercialitzadora.id
            search_params = [('partner_id.id', '=', partner_id)]
            config_ids = config_obj.search(cursor, uid, search_params, limit=1)
            config = config_obj.browse(cursor, uid, config_ids[0])
            return config

GiscedataPolissaModcontractual()
