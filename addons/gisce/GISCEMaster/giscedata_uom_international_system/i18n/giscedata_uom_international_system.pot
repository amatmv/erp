# Translation of OpenERP Server.
# This file contains the translation of the following modules:
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: GISCE-ERP\n"
"Report-Msgid-Bugs-To: https://github.com/gisce/erp/issues\n"
"POT-Creation-Date: 2019-07-29 13:35\n"
"PO-Revision-Date: 2019-07-29 11:55+0000\n"
"Last-Translator: Eduard Berloso <eberloso@gisce.net>\n"
"Language-Team: English (United States) (http://trad.gisce.net/projects/p/erp/language/en_US/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_cm
msgid "cm"
msgstr "cm"

#. module: giscedata_uom_international_system
#: model:ir.module.module,description:giscedata_uom_international_system.module_meta_information
msgid ""
"\n"
"        Afegeix noves unitats estàndards definides pel sistema internacional:\n"
"            - segons (s)\n"
"            - minuts (min)\n"
"            - hores (h)\n"
"            - grams (g)\n"
"            - kilograms (kg)\n"
"            - tones (t)\n"
"            - milimetres (mm)\n"
"            - centimetres (cm)\n"
"            - metres (m)\n"
"            - kilometres (km)\n"
"            - centimetres quadrats (cm2)\n"
"            - metres quadrats (m2)\n"
"            - centimetres cúbics (cm3)\n"
"            - metres cúbics (m3)\n"
"            - litres (L)\n"
"        A més d'altres unitats no estandards:\n"
"            - caixa (Box)\n"
"            - cartutx (Cartridge)\n"
"            - bidó (Drum)\n"
"        \n"
"        També defineix noves categories:\n"
"            - distància o mida (Distance/Size)\n"
"            - superficie (Surface)\n"
"            - volum (Volume)\n"
"            - temps (Time)\n"
"    "
msgstr "\n        Afegeix noves unitats estàndards definides pel sistema internacional:\n            - segons (s)\n            - minuts (min)\n            - hores (h)\n            - grams (g)\n            - kilograms (kg)\n            - tones (t)\n            - milimetres (mm)\n            - centimetres (cm)\n            - metres (m)\n            - kilometres (km)\n            - centimetres quadrats (cm2)\n            - metres quadrats (m2)\n            - centimetres cúbics (cm3)\n            - metres cúbics (m3)\n            - litres (L)\n        A més d'altres unitats no estandards:\n            - caixa (Box)\n            - cartutx (Cartridge)\n            - bidó (Drum)\n        \n        També defineix noves categories:\n            - distància o mida (Distance/Size)\n            - superficie (Surface)\n            - volum (Volume)\n            - temps (Time)\n    "

#. module: giscedata_uom_international_system
#: model:ir.module.module,shortdesc:giscedata_uom_international_system.module_meta_information
msgid "Unitats bàsiques UOM del sistema internacional"
msgstr "Unitats bàsiques UOM del sistema internacional"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_ut
msgid "ut"
msgstr "ut"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_km
msgid "km"
msgstr "km"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_cartridge
msgid "Cartridge"
msgstr "Cartridge"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_minute
msgid "min"
msgstr "min"

#. module: giscedata_uom_international_system
#: model:product.uom.categ,name:giscedata_uom_international_system.product_uom_categ_volume
msgid "Volume"
msgstr "Volume"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_m3
msgid "m3"
msgstr "m3"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_m2
msgid "m2"
msgstr "m2"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_cm3
msgid "cm3"
msgstr "cm3"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_cm2
msgid "cm2"
msgstr "cm2"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_box
msgid "Box"
msgstr "Box"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_drum
msgid "Drum"
msgstr "Drum"

#. module: giscedata_uom_international_system
#: model:product.uom.categ,name:giscedata_uom_international_system.product_uom_categ_size
msgid "Distance/Size"
msgstr "Distance/Size"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_l
msgid "L"
msgstr "L"

#. module: giscedata_uom_international_system
#: model:product.uom.categ,name:giscedata_uom_international_system.product_uom_categ_time
msgid "Time"
msgstr "Time"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_kg
msgid "kg"
msgstr "kg"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_g
msgid "g"
msgstr "g"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_mm
msgid "mm"
msgstr "mm"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_hour
msgid "h"
msgstr "h"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_m
msgid "m"
msgstr "m"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_day
msgid "Day"
msgstr "Day"

#. module: giscedata_uom_international_system
#: model:product.uom.categ,name:giscedata_uom_international_system.product_uom_categ_surface
msgid "Surface"
msgstr "Surface"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_month
msgid "Month"
msgstr "Month"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_second
msgid "s"
msgstr "s"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_t
msgid "t"
msgstr "t"

#. module: giscedata_uom_international_system
#: model:product.uom,name:giscedata_uom_international_system.product_uom_year
msgid "Year"
msgstr "Year"
