# -*- coding: utf-8 -*-
{
    "name": "Unitats bàsiques UOM del sistema internacional",
    "description": """
        Afegeix noves unitats estàndards definides pel sistema internacional:
            - segons (s)
            - minuts (min)
            - hores (h)
            - grams (g)
            - kilograms (kg)
            - tones (t)
            - milimetres (mm)
            - centimetres (cm)
            - metres (m)
            - kilometres (km)
            - centimetres quadrats (cm2)
            - metres quadrats (m2)
            - centimetres cúbics (cm3)
            - metres cúbics (m3)
            - litres (L)
        A més d'altres unitats no estandards:
            - caixa (Box)
            - cartutx (Cartridge)
            - bidó (Drum)
        
        També defineix noves categories:
            - distància o mida (Distance/Size)
            - superficie (Surface)
            - volum (Volume)
            - temps (Time)
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "product",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "uom_data.xml"
    ],
    "active": False,
    "installable": True
}
