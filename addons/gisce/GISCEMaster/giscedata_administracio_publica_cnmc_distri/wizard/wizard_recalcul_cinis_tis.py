# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


MODELS_SELECTION = []


class WizardRecalculCinisTis(osv.osv_memory):
    _name = 'wizard.recalcul.cinis.tis'

    def _default_info(self, cursor, uid, context=None):
        informacio = _(
            u"Aquest assistent recalcularà els CINIs i/o TIs del elements del"
            u" model seleccionat.\nNOTA: NO es recalcularàn els CINIs o TIs "
            u"bloquejats."
        )
        return informacio

    def action_update_data(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        module_obj = self.pool.get(wiz.model)
        update_fields = []
        if wiz.mode == 'ti':
            update_fields = ['bloquejar_cnmc_tipus_install']
        elif wiz.mode == 'cini':
            update_fields = ['bloquejar_cini']
        elif wiz.mode == "ciniti":
            update_fields = ['bloquejar_cnmc_tipus_install',
                             'bloquejar_cini']
        counter = []
        for field in update_fields:
            search_params = [(field, '=', 0)]
            ids = module_obj.search(
                cursor, uid, search_params, 0, 0, False,
                {'active_test': False}
            )
            module_obj.write(cursor, uid, ids, {field: 1})
            module_obj.write(cursor, uid, ids, {field: 0})
            counter.append(len(ids))

        model_name = ''
        for m in MODELS_SELECTION:
            if m[0] == wiz.model:
                model_name = m[1]

        if wiz.mode == "ciniti":
            info = (
                "S'han actualitzat un total de {} TI i {} CINI "
                "del model {}.\n\n".format(
                    counter[0], counter[1], model_name
                ) + wiz.info
            )
        elif wiz.mode == "ti":
            info = (
                "S'han actualitzat un total de {} TI del model {}.\n\n".format(
                    counter[0], model_name
                ) + wiz.info
            )
        else:
            info = (
                "S'han actualitzat un total de {} CINI del model "
                "{}.\n\n".format(counter[0], model_name) + wiz.info
            )

        wiz.write({
            'state': 'done',
            'info': info
        })
    _columns = {
        'model': fields.selection(
            MODELS_SELECTION,
            _('Model'),
            required=True
        ),
        'mode': fields.selection([
            ('ti', 'TI'),
            ('cini', 'CINI'),
            ('ciniti', 'CINI i TI')
        ], _(u'Camp a recalcular'), required=True),
        'info': fields.text('info', readonly=True),
        'state': fields.char('Estat', size=16),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'mode': lambda *a: ('cini', 'CINI'),
    }


WizardRecalculCinisTis()
