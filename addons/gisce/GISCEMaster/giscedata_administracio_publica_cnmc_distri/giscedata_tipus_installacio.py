# -*- coding: utf-8 -*-

from osv import osv, fields

TIPUS_REGULATORI = [
    ("criteri", "Segons criteri"),
    ("incloure", "Forçar incloure"),
    ("excloure", "Forçar excloure"),
]


class GiscedataTipusInstallacio(osv.osv):
    '''
        Tipus d'instal·lació
    '''
    _name = 'giscedata.tipus.installacio'

    def name_search(self, cursor, uid, name='', args=[], operator='ilike',
                    context=None, limit=80):
        '''
        Name search implementation

        :param cursor: Database cursor
        :param uid: User id
        :param name: Search text
        :param args: Search arguments
        :param operator: search operator
        :param context: OpenERP context
        :param limit: Search limit

        :return:
        '''
        if not context:
            context = {}
        ids = []
        if not ids:
            ids = self.search(cursor, uid, [('name', operator, name)] + args,
                              limit=limit, context=context)
            ids += self.search(cursor, uid, [('descripcio', operator, name)] + args,
                              limit=limit, context=context)

        return self.name_get(cursor, uid, ids)

    def name_get(self, cursor, uid, ids, context=None):
        '''
        Name get implementation

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of afected ids
        :param context: OpenERP Context
        :return: List of tuples of id,name
        '''
        if not context:
            context = {}
        res = []
        fields = ['name', 'descripcio']
        for reg in self.read(cursor, uid, ids, fields, context=context):
            res.append((reg['id'], '{name} {descripcio}'.format(**reg)))
        return res

    _columns = {
        'name': fields.char('Codi', size=256, required=True, select=True),
        'descripcio': fields.char('Descripció', size=256, required=True, select=True),
        'tensio': fields.char('Tensió', size=256, select=True),
        'section_size': fields.char('Secció en mm', size=256, select=True),
        'investment': fields.float("Inversió", digits=(16,3)),
        'maintainance': fields.float("Operació i Manteniment", digits=(16,3)),
        'active': fields.boolean('Activa'),
        'model_id': fields.many2one('ir.model', 'Model'),
        'search_params': fields.text('Parametres de cerca'),
        'data_inici': fields.date('Data inici'),
        'data_fi': fields.date('Data fi')
    }

    _defaults = {
        'active': lambda *a: 1
    }

GiscedataTipusInstallacio()


class GiscedataCodiInstallacio(osv.osv):

    """Codi d'instal·lació"""

    _name = 'giscedata.codi.installacio'
    _description = __doc__

    _cod_ins_types = [
        ('installacio', 'Codi d\'instal·lació'),
        ('equip_mesura', 'Codi d\'equip de mesura')
    ]

    def name_search(self, cursor, uid, name='', args=None, operator='ilike',
                    context=None, limit=80):
        '''
        Name search implementation.

        Returns a list of tuples of (id, name) of the matching
        <giscedata.codi.installacio> records. The matching will be decided if
        the name or description of the record match the *name* param.

        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param name: Search text
        :type name: str | unicode
        :param args: Search arguments
        :type args: list
        :param operator: search operator
        :type operator: str | unicode
        :param context: OpenERP Context
        :type context: dict
        :param limit: Search limit
        :type limit: int

        :return: name_get return
        '''
        if context is None:
            context = {}

        if args is None:
            args = []

        ids = self.search(
            cursor, uid, [('name', operator, name)] + args, limit=limit,
            context=context
        )
        ids += self.search(
            cursor, uid, [('descripcio', operator, name)] + args, limit=limit,
            context=context
        )

        return self.name_get(cursor, uid, list(set(ids)))

    def name_get(self, cursor, uid, ids, context=None):
        """
        Name get implementation

        :param cursor: Database cursor
        :param uid: User id
        :param ids: List of afected ids
        :param context: OpenERP Context
        :return: List of tuples of id,name
        """
        if context is None:
            context = {}
        res = []
        cod_ins_vals = self.read(
            cursor, uid, ids, ['name', 'descripcio'], context=context
        )
        for reg in cod_ins_vals:
            res.append((reg['id'], '{name} {descripcio}'.format(**reg)))
        return res

    _columns = {
        'name': fields.char('Codi', size=256, required=True, select=True),
        'descripcio': fields.char('Descripció', size=256, required=True,
                                  select=True),
        'tipus': fields.selection(_cod_ins_types, 'Tipus de Codi',
                                  required=True),
        'actiu': fields.boolean('Actiu'),
    }

    _defaults = {
        'actiu': lambda *args: True
    }


GiscedataCodiInstallacio()
