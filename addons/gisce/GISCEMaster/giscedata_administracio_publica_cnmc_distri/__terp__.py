# -*- coding: utf-8 -*-
{
    "name": "GISCE Tipus instal·lació",
    "description": """ Adnministracio publica CNMC per distri

  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_administracio_publica_cne"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tipus_installacio_view.xml",
        "security/ir.model.access.csv",
        "giscedata_tipus_installacio_data.xml",
        "wizard/wizard_recalcul_cinis_tis_view.xml"
    ],
    "active": False,
    "installable": True
}
