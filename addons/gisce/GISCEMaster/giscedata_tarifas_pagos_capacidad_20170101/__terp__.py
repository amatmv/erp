# -*- coding: utf-8 -*-
{
    "name": "Pagos por capacidad Gener 2017",
    "description": """
  Actualització de les tarifes pagos por capacidad segons el BOE nº 314 - 23/12/2016.
  ETU/1976/2016
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_polissa",
        "giscedata_pagos_capacidad"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_tarifas_pagos_capacidad_20170101_data.xml"
    ],
    "active": False,
    "installable": True
}
