# -*- coding: utf-8 -*-
{
    "name": "Facturació a Tarifa",
    "description": """Facturació a Tarifa""",
    "version": "0-dev",
    "author": "GISCEMaster",
    "category": "Master",
    "depends":[
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_tarifa_data.xml"
    ],
    "active": False,
    "installable": True
}
