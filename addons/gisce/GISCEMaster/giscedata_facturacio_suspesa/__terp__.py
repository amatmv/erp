# -*- coding: utf-8 -*-
{
    "name": "Facturació Suspesa",
    "description": """Facturació Suspesa.
        Per aturar la facturació de certes pólisses a través del lot de facturació.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_facturacio",
    ],
    "init_xml": [],
    "demo_xml":[],
    "update_xml":[
        "giscedata_polissa_view.xml",
        "giscedata_facturacio_validation_data.xml",
    ],
    "active": False,
    "installable": True
}
