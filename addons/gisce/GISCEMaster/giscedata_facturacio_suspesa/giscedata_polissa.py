# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'facturacio_suspesa': fields.boolean(string="Facturació Suspesa")
    }

    _defaults = {
        'facturacio_suspesa': lambda *a: False
    }

GiscedataPolissa()
