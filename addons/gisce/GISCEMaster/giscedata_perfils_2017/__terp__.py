# -*- coding: utf-8 -*-
{
    "name": "Perfilació de lectures (valors 2017)",
    "description": """
Actualitza els valors de perfilació per l'any 2017
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_perfils"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_perfils_2017_data.xml"
    ],
    "active": False,
    "installable": True
}
