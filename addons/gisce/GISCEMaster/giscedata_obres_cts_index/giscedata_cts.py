from base_index.base_index import BaseIndex


class GiscedataCts(BaseIndex):
    _name = 'giscedata.cts'
    _inherit = 'giscedata.cts'
    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataCts()
