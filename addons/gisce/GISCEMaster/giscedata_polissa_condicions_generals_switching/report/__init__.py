# -*- coding: utf-8 -*-
import tempfile
import base64

from c2c_webkit_report import webkit_report
from report import report_sxw
from tools import config
import pooler

import pypdftk


class report_webkit_html(report_sxw.rml_parse):
    def __init__(self, cursor, uid, name, context):
        super(report_webkit_html, self).__init__(cursor, uid, name,
                                                 context=context)
        self.localcontext.update({
            'cursor': cursor,
            'uid': uid,
            'addons_path': config['addons_path']
        })

    def set_context(self, objects, data, ids, report_type=None):
        polissa_date = data.get('form', {}).get('polissa_date')
        if polissa_date:
            ctx = {'date': polissa_date}
            polissa_obj = self.pool.get('giscedata.polissa')
            objects = polissa_obj.browse(self.cr, self.uid, ids, context=ctx)
        super(report_webkit_html, self).set_context(
            objects, data, ids, report_type
        )


class ReportContracteCondicionsGeneralsATR(webkit_report.WebKitParser):

    def create(self, cursor, uid, ids, datas, context=None):
        """Create reports and join.

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: records to print into the report
        :param data: Data passed to report
        :param context: Application context
        :returns a tuple with the type and the content of the report
        """
        if context is None:
            context = {}
        to_join_final = []
        pool = pooler.get_pool(cursor.dbname)
        sw_obj = pool.get('giscedata.switching')
        for case in sw_obj.browse(cursor, uid, ids, context=context):
            to_join = []
            res = super(ReportContracteCondicionsGeneralsATR, self).create(
                cursor, uid, [case.id], datas, context
            )
            if res[1] != 'pdf':
                continue
            res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
            with open(res_path, 'w') as f:
                f.write(res[0])
            to_join.append(res_path)
            polissa = case.cups_polissa_id
            if polissa.condicions_generals_id:
                attachment = polissa.condicions_generals_id.attachment_id
                if attachment and attachment.datas:
                    res_path = tempfile.mkstemp('-join.pdf', 'report-')[1]
                    with open(res_path, 'w') as f:
                        f.write(base64.b64decode(attachment.datas))
                    to_join.append(res_path)
            if len(to_join) > 1:
                out = pypdftk.concat(files=to_join)
            else:
                out = res_path
            to_join_final.append(out)
        if len(to_join_final) > 1:
            out = pypdftk.concat(files=to_join_final)
        else:
            out = to_join_final[0]
        with open(out, 'r') as f:
            content = f.read()
        return content, 'pdf'


ReportContracteCondicionsGeneralsATR(
    'report.giscedata.switching.info_cas_ATR',
    'giscedata.switching',
    parser=report_webkit_html
)
