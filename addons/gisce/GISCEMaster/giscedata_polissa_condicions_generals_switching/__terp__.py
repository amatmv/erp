# -*- coding: utf-8 -*-
{
    "name": "Condicions generals (ATR)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
    * Condicions generals pòlisses
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_switching",
        "giscedata_polissa_condicions_generals"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
