# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_polissa_name_auto
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2012-04-25 16:10:36+0000\n"
"PO-Revision-Date: 2012-04-25 16:10:36+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_polissa_name_auto
#: model:ir.module.module,shortdesc:giscedata_polissa_name_auto.module_meta_information
msgid "Numeració automàtica en pòlisses/contractes"
msgstr ""

#. module: giscedata_polissa_name_auto
#: model:ir.module.module,description:giscedata_polissa_name_auto.module_meta_information
msgid "\n"
"    This module provide :\n"
"      * Automatic naming for giscedata.polissa names\n"
"    "
msgstr ""

