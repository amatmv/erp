# -*- coding: utf-8 -*-
from osv import osv

class GiscedataPolissa(osv.osv):
    """Afegim funcionaltiat de nom automàtic.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def default_name(self, cursor, uid, context):
        """Returns next sequence.
        """
        return self.pool.get('ir.sequence').get(cursor, uid,
                                                'giscedata.polissa')

    def create(self, cursor, uid, vals, context=None):
        """Posem el següent número quan es crea la pòlissa.
        """
        if not vals.get('name',False):
            vals['name'] = self.default_name(cursor, uid, context)
        res_id = super(GiscedataPolissa,
                       self).create(cursor, uid, vals, context)
        return res_id

GiscedataPolissa()
