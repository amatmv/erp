# -*- coding: utf-8 -*-
{
    "name": "Numeració automàtica en pòlisses/contractes",
    "description": """
    This module provide :
      * Automatic naming for giscedata.polissa names
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_polissa"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
