# *-*  codig: utf-8 *-*

from osv import osv, fields

class giscedata_expedients_expedient(osv.osv):

    _name = 'giscedata.expedients.expedient'
    _inherit = 'giscedata.expedients.expedient'

    _columns = {
      'dates_omplertes': fields.boolean('Dates omplertes')
    }

    _defaults = {
      'dates_omplertes': lambda *a: False,
    }

giscedata_expedients_expedient()
