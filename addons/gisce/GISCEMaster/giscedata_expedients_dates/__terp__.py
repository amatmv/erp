# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients Dates",
    "description": """Filtre per saber quins expedients tenen totes les dates i quins no. També afegeix un checkbox pels expedients que encara que no tinguin totes les dates ja els donem com per si els tinguessin""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_dates_view.xml"
    ],
    "active": False,
    "installable": True
}
