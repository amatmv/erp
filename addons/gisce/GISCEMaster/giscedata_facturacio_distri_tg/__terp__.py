# -*- coding: utf-8 -*-
{
    "name": "Lectures Telegestio Distribuïdora",
    "description": """
    Create billing reads from lot
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "mongodb_backend",
        "giscedata_lectures_telegestio",
        "giscedata_facturacio_distri",
        "giscedata_administracio_publica_cne",
        "giscedata_administracio_publica_ree"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/wizard_create_tg_reads_lot_view.xml",
        "giscedata_facturacio_view.xml",
        "wizard/export_cch_lot_wizard.xml",
        "wizard/wizard_cnmc_type5_meter_report_view.xml",
        "wizard/wizard_ree_type5_tg_deployment_view.xml",
        "wizard/wizard_change_to_remote_managed_with_cch.xml",
        "giscedata_ftp_provider_view.xml",
        "giscedata_facturacio_data.xml",
        "security/giscedata_facturacio_distri_tg_security.xml",
        "security/ir.model.access.csv",
        "giscedata_facturacio_distri_tg_scheduler.xml"
    ],
    "active": False,
    "installable": True
}
