# -*- coding: utf-8 -*-
from osv import osv, fields


class GiscedataFTPProvider(osv.osv):

    _inherit = 'giscedata.ftp.provider'

    _columns = {
        'f5d_enabled': fields.boolean('Enabled for F5D'),
        'f5d_upload': fields.char(
            "F5D Upload folder",
            size=256,
        )
    }

    _defaults = {
        'f5d_enabled': lambda *a: False,
        'f5d_upload': lambda *a: '/upload/{comer}/Salida',
    }


GiscedataFTPProvider()
