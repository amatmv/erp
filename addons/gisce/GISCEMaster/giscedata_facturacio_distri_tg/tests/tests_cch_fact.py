# coding=utf-8
from destral import testing
from destral.transaction import Transaction

from giscedata_facturacio_distri_tg.giscedata_facturacio import fix_dates
from giscedata_polissa.tests.utils import activar_polissa, crear_modcon


class TestFacturaFixCCH(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_comptador(self):

        imd_obj = self.openerp.pool.get('ir.model.data')
        comptador_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        comptador_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures',
            'comptador_0001'
        )[1]
        comptador = comptador_obj.browse(self.cursor, self.uid, comptador_id)
        return comptador

    def activar_polissa(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa',
            'polissa_0001'
        )[1]
        polissa_obj.write(self.cursor, self.uid, [polissa_id], {
            'data_alta': '2017-01-01',
            'data_baixa': False,
            'lot_facturacio': False
        })
        activar_polissa(self.openerp.pool, self.cursor, self.uid, polissa_id)
        return polissa_id

    def test_fix_dates_no_tg_must_return_start_end(self):
        start = '2017-01-01'
        end = '2017-01-31'

        comptador = self.get_comptador()
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, start)
        self.assertEqual(res_end, end)

    def test_fix_dates_tg_new_meter(self):
        start = '2017-01-01'
        end = '2017-01-31'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-01-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-01-01 01:00:00')
        self.assertEqual(res_end, '2017-02-01 00:00:00')

    def test_fix_dates_tg_new_meter_cnmc_criteria(self):
        start = '2016-12-31'
        end = '2017-01-31'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-01-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-01-01 01:00:00')
        self.assertEqual(res_end, '2017-02-01 00:00:00')

    def test_fix_dates_tg_no_new_meter(self):
        start = '2017-01-31'
        end = '2017-02-28'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-01-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-02-01 01:00:00')
        self.assertEqual(res_end, '2017-03-01 00:00:00')

    def test_fix_dates_tg_new_tariff(self):
        polissa_id = self.activar_polissa()

        imd_obj = self.openerp.pool.get('ir.model.data')
        tarifa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'tarifa_20DHA_new'
        )[1]

        crear_modcon(self.openerp.pool, self.cursor, self.uid, polissa_id, {
            'tarifa': tarifa_id
        }, '2017-11-01', '2018-10-31')

        start = '2017-11-01'
        end = '2017-11-30'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-01-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-11-01 01:00:00')
        self.assertEqual(res_end, '2017-12-01 00:00:00')

    def test_fix_dates_tg_new_tariff_cnmc_criteria(self):
        polissa_id = self.activar_polissa()

        imd_obj = self.openerp.pool.get('ir.model.data')
        tarifa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'tarifa_20DHA_new'
        )[1]

        crear_modcon(self.openerp.pool, self.cursor, self.uid, polissa_id, {
            'tarifa': tarifa_id
        }, '2017-11-01', '2018-10-31')

        start = '2017-10-31'
        end = '2017-11-30'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-01-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-11-01 01:00:00')
        self.assertEqual(res_end, '2017-12-01 00:00:00')

    def test_fix_dates_tg_new_meter_and_new_tariff(self):
        polissa_id = self.activar_polissa()

        imd_obj = self.openerp.pool.get('ir.model.data')
        tarifa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'tarifa_20DHA_new'
        )[1]

        crear_modcon(self.openerp.pool, self.cursor, self.uid, polissa_id, {
            'tarifa': tarifa_id
        }, '2017-11-01', '2018-10-31')

        start = '2017-11-01'
        end = '2017-11-30'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-11-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-11-01 01:00:00')
        self.assertEqual(res_end, '2017-12-01 00:00:00')

    def test_fix_dates_tg_new_meter_and_new_tariff_cnmc_criteria(self):
        polissa_id = self.activar_polissa()

        imd_obj = self.openerp.pool.get('ir.model.data')
        tarifa_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_polissa', 'tarifa_20DHA_new'
        )[1]

        crear_modcon(self.openerp.pool, self.cursor, self.uid, polissa_id, {
            'tarifa': tarifa_id
        }, '2017-11-01', '2018-10-31')

        start = '2017-10-31'
        end = '2017-11-30'

        comptador = self.get_comptador()
        comptador.write({
            'tg': 1,
            'data_alta': '2017-11-01'
        })
        res_start, res_end = fix_dates(comptador, start, end)

        self.assertEqual(res_start, '2017-11-01 01:00:00')
        self.assertEqual(res_end, '2017-12-01 00:00:00')
