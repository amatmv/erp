# coding=utf-8
from destral import testing
from destral.transaction import Transaction


class TestForceFixCCH(testing.OOTestCase):

    def setUp(self):
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user

    def tearDown(self):
        self.txn.stop()

    def get_invoice(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        comptador_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        # with lectures_energia_ids
        invoice_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_facturacio',
            'factura_0008'
        )[1]
        return comptador_obj.browse(self.cursor, self.uid, invoice_id)

    def set_metter_tg(self):
        imd_obj = self.openerp.pool.get('ir.model.data')
        metter_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        metter_id = imd_obj.get_object_reference(
            self.cursor, self.uid, 'giscedata_lectures',
            'comptador_0001'
        )[1]
        metter_obj.browse(self.cursor, self.uid, metter_id).write({'tg': True})

    def get_wizard_fix_cch_fact_again(self):
        wiz_obj = self.openerp.pool.get('wizard.fix.cch.again')
        wiz_id = wiz_obj.create(self.cursor, self.uid, {}, {})
        return wiz_obj.browse(self.cursor, self.uid, wiz_id, {})

    def test_invoice_with_cch_without_force(self):
        invoice = self.get_invoice()
        invoice.write({'cch_fact_available': True, 'polissa_tg': '1'})

        wizard_fix_cch = self.get_wizard_fix_cch_fact_again()
        wizard_fix_cch.write({
            'force': False,
        })
        self.set_metter_tg()

        context = {'active_ids': [invoice.id]}
        wizard_fix_cch.fix_cch_again(context=context)

        self.assertEqual(invoice.cch_fact_available, True)

    def test_invoice_with_cch_with_force(self):
        invoice = self.get_invoice()
        invoice.write({'cch_fact_available': True, 'polissa_tg': '1'})

        wizard_fix_cch = self.get_wizard_fix_cch_fact_again()
        wizard_fix_cch.write({
            'force': True,
        })
        self.set_metter_tg()

        context = {'active_ids': [invoice.id]}
        wizard_fix_cch.fix_cch_again(context=context)

        self.assertEqual(invoice.cch_fact_available, True)

    def test_invoice_without_cch_withouth_force(self):
        invoice = self.get_invoice()
        invoice.write({'cch_fact_available': False, 'polissa_tg': '1'})

        wizard_fix_cch = self.get_wizard_fix_cch_fact_again()
        wizard_fix_cch.write({
            'force': False,
        })
        self.set_metter_tg()

        context = {'active_ids': [invoice.id]}
        wizard_fix_cch.fix_cch_again(context=context)

        self.assertEqual(
            'No es pot fer el FIX CCH FACT' in wizard_fix_cch.info, False
        )

    def test_invoice_without_cch_with_force(self):
        invoice = self.get_invoice()
        invoice.write({'cch_fact_available': False, 'polissa_tg': '1'})

        wizard_fix_cch = self.get_wizard_fix_cch_fact_again()
        wizard_fix_cch.write({
            'force': True,
        })
        self.set_metter_tg()

        context = {'active_ids': [invoice.id]}
        wizard_fix_cch.fix_cch_again(context=context)

        self.assertEqual(
            'No es pot fer el FIX CCH FACT' in wizard_fix_cch.info, False
        )

    def test_invoice_without_policy_tg(self):
        invoice = self.get_invoice()
        invoice.write({'cch_fact_available': False, 'polissa_tg': '2'})

        wizard_fix_cch = self.get_wizard_fix_cch_fact_again()
        wizard_fix_cch.write({
            'force': True,
        })
        self.set_metter_tg()

        context = {'active_ids': [invoice.id]}
        wizard_fix_cch.fix_cch_again(context=context)

        self.assertEqual(invoice.cch_fact_available, False)