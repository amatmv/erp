# -*- coding: utf-8 -*-

from osv import osv
from tools.translate import _
from datetime import date


class GiscedataPolissa(osv.osv):
    """
    Add remote managed meter features to policies.
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'
    _description = __doc__

    def create_case_no_D1_created(
            self,
            cursor,
            uid,
            policy_ids,
            context=None
    ):
        """
        Create case to alert about D1 wasn't created.

        :param cursor: database cursor
        :param uid: user identifier
        :param policy_ids: list of policy identifiers
        :param context: dict with the context
        :return: None
        """

        crm_case = self.pool.get('crm.case')

        if context is None:
            context = {}

        for policy in self.browse(cursor, uid, policy_ids, context=context):
            case_fail_d1_description = _(
                "No s'ha pogut crear el D1 per la polissa {}"
            ).format(policy.name)

            ref = '{},{}'.format('giscedata.polissa', policy.id)
            case_id = crm_case.search(
                cursor,
                uid,
                [
                    ('ref', '=', ref),
                    ('description', '=', case_fail_d1_description)
                ],
                context=context
            )
            if not case_id:
                context_case = context.copy()
                context_case.update({'model': self._name})

                case_id = crm_case.create_case_generic(
                    cursor,
                    uid,
                    [policy.id],
                    context=context_case,
                    description=case_fail_d1_description,
                    section='TG',
                )
            crm_case.case_open(cursor, uid, case_id)

    def change_to_remote_managed_with_cch(
            self,
            cursor,
            uid,
            policy_ids,
            activation_date=None,
            context=None
    ):
        """
        Change policy to monthly and mark it as remote managed with CCH.

        :param cursor: database cursor
        :param uid: user idenitifier
        :param policy_ids: list of policy identifiers
        :param activation_date: Date of change. By default the first day of \
            the current month.
        :param context: dict with the context
        :return: list of the identifier of D1 cases created
        """

        giscedata_facturacio_lot = self.pool.get('giscedata.facturacio.lot')
        giscedata_polissa_crear_contracte = \
            self.pool.get('giscedata.polissa.crear.contracte')

        if activation_date is None:
            activation_date = date.today().replace(day=1).strftime('%Y-%m-%d')

        if context is None:
            context = {}

        batch_id = giscedata_facturacio_lot.get_id_from_date(
            cursor,
            uid,
            date=activation_date
        )

        new_values = {
            'facturacio': 1,
            'tg': '1'
        }

        atr_case_ids = []

        for policy in self.browse(cursor, uid, policy_ids, context=context):
            if policy.tg == '1':
                continue
            if policy.state == 'tall':
                continue
            policy.send_signal('modcontractual')
            policy.write(new_values)
            if policy.lot_facturacio:
                if not context.get('keep_lot', False):
                    policy.write({'lot_facturacio': batch_id})

            wizard_context = context.copy()
            wizard_context.update({'active_id': policy.id})
            parameters = {'duracio': 'actual', 'accio': 'nou'}

            wizard_id = giscedata_polissa_crear_contracte.create(
                cursor,
                uid,
                parameters,
                context=wizard_context
            )
            wizard = giscedata_polissa_crear_contracte.browse(
                cursor,
                uid,
                wizard_id,
                context=wizard_context
            )
            wizard_result = wizard.onchange_duracio(
                activation_date,
                wizard.duracio,
                context=wizard_context
            )

            if wizard_result.get('warning', False):
                parameters = {'accio': 'modificar'}
                wizard_id = giscedata_polissa_crear_contracte.create(
                    cursor,
                    uid,
                    parameters,
                    context=wizard_context
                )
                wizard = giscedata_polissa_crear_contracte.browse(
                    cursor,
                    uid,
                    wizard_id,
                    context=wizard_context
                )
            else:
                end_date = wizard_result['value']['data_final'] or ''
                wizard.write(
                    {
                        'data_final': end_date,
                        'data_inici': activation_date
                    }
                )

            wizard.action_crear_contracte()
            if self.pool.get('giscedata.switching'):
                cvals = {"data_activacio": activation_date}
                atr_case_result = policy.crear_cas_atr('D1', config_vals=cvals)
                if 'ERROR' in atr_case_result[1].upper():
                    policy.create_case_no_D1_created()
                else:
                    atr_case_ids.append(atr_case_result[2])
        return atr_case_ids

GiscedataPolissa()
