from datetime import datetime, timedelta
from calendar import monthrange
import calendar
from mongodb_backend.mongodb2 import mdbpool
from enerdata.datetime.timezone import TIMEZONE as TZ
import logging
from osv import osv
from tqdm import tqdm

logger = logging.getLogger('openerp.' + __name__)
logger.info = tqdm.write

class TgLecturesComptador(osv.osv):

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    def get_situacion(self, cursor, uid, year, context=None):
        """Returns the first page (situacion) of ree report as a list per month
        list. If year is current year, it only contains until the last complete
        year on current date
        :param year: Year to generate
        :return: A list per month list. Every month as
        [TOTAL, TG WITH CCH, TG NOT INTEGRATED, TG NO CCH, %TG]
        """

        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        report_year = year

        today = datetime.today()
        if today.year == report_year:
            last_month = today.month - 1
        else:
            last_month = 12

        res = []

        for month in tqdm(range(1, last_month + 1), desc="get_situacion: "):

            ts_start = '{}-{:02}-01 00:00:00'.format(report_year, month)
            ts_end = '{}-{:02}-{} 23:59:59'.format(
                report_year, month, monthrange(report_year, month)[1]
            )

            total_metters_type5 = len(comptador_obj.get_by_type(
                cursor, uid, ts_start.split()[0], ts_end.split()[0], '05'
            ))

            total_metters_type5_cch_1 = len(comptador_obj.get_cch_enabled(
                cursor, uid, ts_start.split()[0], ts_end.split()[0], '1'
            ))

            total_metters_type5_cch_2 = len(comptador_obj.get_cch_enabled(
                cursor, uid, ts_start.split()[0], ts_end.split()[0], '2'
            ))

            total_metters_type5_cch_3 = len(comptador_obj.get_cch_enabled(
                cursor, uid, ts_start.split()[0], ts_end.split()[0], '3'
            ))

            try:
                perc_deploy = float("{0:.5f}".format((100 * float(
                   total_metters_type5_cch_1 + total_metters_type5_cch_2 +
                   total_metters_type5_cch_3
                ) / float(total_metters_type5))))

                perc_cch_deploy = float("{0:.5f}".format((
                    100 * float(total_metters_type5_cch_1) / float(total_metters_type5)
                )))
            except ZeroDivisionError:
                perc_deploy = 0
                perc_cch_deploy = 0
                logger.error('No metters type5 found')

            res.append([
                total_metters_type5,
                total_metters_type5_cch_1,
                total_metters_type5_cch_2,
                total_metters_type5_cch_3,
                perc_deploy,
                perc_cch_deploy
            ])

        return res

    def get_porcentaje(self, cursor, uid, year, context=None):
        """
        Count the values of section 3. Porcentaje de Curva de Carga Horaria

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: wizard identifier
        :param context: dictionary with context
        :return: a list of lists, one list for each month with a list of values
        """
        lectures_comptador = self.pool.get('giscedata.lectures.comptador')
        tg_profile_mdb = mdbpool.get_collection('tg_profile')

        if context is None:
            context = {}
        context.update({'active_test': False})

        today = datetime.today()
        if today.year == year:
            last_month = today.month - 1
        else:
            last_month = 12

        res = []
        for month in tqdm(range(1, last_month + 1), desc="get_porcentaje: "):
            first_month_day = datetime(year,month,1)
            days_of_month = calendar.monthrange(year,month)[1]
            first_next_month_day = first_month_day + timedelta(
                days=days_of_month
            )
            last_month_day = first_month_day + timedelta(
                days=days_of_month - 1
            )

            meter_ids = lectures_comptador.get_cch_enabled(
                cursor, uid, first_month_day.strftime('%Y-%m-%d'),
                last_month_day.strftime('%Y-%m-%d'), '1'
            )
            serial_by_id = lectures_comptador.build_names_tg(cursor, uid,
                                                             meter_ids,
                                                             context=context)
            serials = [serial_by_id[meter_id] for meter_id in serial_by_id]

            meters_mongo = tg_profile_mdb.aggregate(
                [
                    {
                        '$match': {
                            'timestamp': {
                                    '$gte': first_month_day,
                                    '$lt': first_next_month_day
                            },
                            'cch_bruta': True,
                            'name': {'$in': serials}
                        }
                    },
                    {
                        '$group': {'_id': "$name", 'count': {'$sum': 1}}
                    }
                ]
            )['result']

            meters_profiles = {}
            for meter in meters_mongo:
                meters_profiles[meter['_id']] = meter['count']

            logger.info('Found {} meters in CCH'.format(len(meter_ids)))
            meters = lectures_comptador.read(cursor, uid, meter_ids,
                                             ['data_alta','data_baixa'],
                                             context=context)

            count = {'all': 0, '100': 0, '95': 0, '90': 0, '80': 0}
            for meter in meters:
                serial = serial_by_id[str(meter['id'])]
                if serial not in meters_profiles:
                    logger.info('No CCH found for meter: {}'.format(serial))
                    continue
                count['all'] += 1
                create_date = datetime.strptime(meter['data_alta'], "%Y-%m-%d")
                first_date = max(first_month_day, create_date)
                if meter['data_baixa']:
                    remove_date = datetime.strptime(meter['data_baixa'],
                                                    "%Y-%m-%d")
                    last_date = min(last_month_day, remove_date)
                else:
                    last_date = last_month_day

                days = (last_date - first_date).days + 1
                expected_profiles = days * 24

                if month in [3,10]:
                    last_sunday = max(week[-1] for week
                                      in calendar.monthcalendar(year,month))
                    last_sunday = datetime(year,month,last_sunday)
                if month == 3 and first_date <= last_sunday <= last_date:
                    expected_profiles -= 1
                if month == 10 and first_date <= last_sunday <= last_date:
                    expected_profiles += 1
                if expected_profiles == 0:
                    continue
                percent = float(meters_profiles[serial]) / float(expected_profiles)

                if percent >= 1:
                    count['100'] += 1
                if percent > .95:
                    count['95'] += 1
                if percent > .9:
                    count['90'] += 1
                if percent > .8:
                    count['80'] += 1

            try:
                perc_100 = float("{0:.5f}".format((float(count['100']) / float(len(meter_ids))) * 100))
                perc_95 = float("{0:.5f}".format((float(count['95']) / float(len(meter_ids))) * 100))
                perc_90 = float("{0:.5f}".format((float(count['90']) / float(len(meter_ids))) * 100))
                perc_80 = float("{0:.5f}".format((float(count['80']) / float(len(meter_ids))) * 100))
            except ZeroDivisionError:
                logger.error('No metters type5 enabled CCH found')
                perc_100 = 0
                perc_95 = 0
                perc_90 = 0
                perc_80 = 0

            res.append([
                count['all'],
                count['100'],
                perc_100,
                count['95'],
                perc_95,
                count['90'],
                perc_90,
                count['80'],
                perc_80
            ])
        return res

    def get_incidencias(self, cursor, uid, year, context=None):
        if context is None:
            context = {}
        profile_obj = self.pool.get('tg.profile')
        comptador_obj = self.pool.get('giscedata.lectures.comptador')

        today = datetime.today()
        if today.year == year:
            last_month = today.month - 1
        else:
            last_month = 12

        data = []
        for m in tqdm(range(1, last_month + 1), desc="get_incidencias: "):

            ts_start = '{}-{:02}-01 00:00:00'.format(year, m)
            ts_end = '{}-{:02}-{} 23:59:59'.format(
                year, m, monthrange(year, m)[1]
            )

            metters_type5_ids = comptador_obj.get_cch_enabled(
                cursor, uid, ts_start.split()[0], ts_end.split()[0]
            )
            logger.info('Found {} metters type5 from {} to {}'.format(
                len(metters_type5_ids), ts_start, ts_end
            ))
            serial_by_id = comptador_obj.build_names_tg(
                cursor, uid, metters_type5_ids
            )
            meters_type5 = serial_by_id.values()

            brutas = profile_obj.search(cursor, uid, [
                ('name', 'in', meters_type5),
                ('cch_bruta', '=', True),
                ('timestamp', '>=', ts_start),
                ('timestamp', '<=', ts_end)
            ], count=True)
            logger.info('Found {} brutas for period {} - {}'.format(
                brutas, ts_start, ts_end
            ))
            validas = profile_obj.search(cursor, uid, [
                ('name', 'in', meters_type5),
                ('cch_bruta', '=', True),
                ('valid', '=', True),
                ('timestamp', '>=', ts_start),
                ('timestamp', '<=', ts_end)
            ], count=True)
            logger.info('Found {} validas for period {} - {}'.format(
                validas, ts_start, ts_end
            ))
            # TODO: How to differenciate and invalid measure from once which is
            #       not already validated
            invalidas = profile_obj.search(cursor, uid, [
                ('name', 'in', meters_type5),
                ('cch_bruta', '=', True),
                ('valid', '=', False),
                ('timestamp', '>=', ts_start),
                ('timestamp', '<=', ts_end)
            ], count=True)
            logger.info('Found {} invalidas for period {} - {}'.format(
                invalidas, ts_start, ts_end
            ))
            estimadas = profile_obj.search(cursor, uid, [
                ('name', 'in', meters_type5),
                ('cch_bruta', '=', False),
                ('timestamp', '>=', ts_start),
                ('timestamp', '<=', ts_end)
            ], count=True)
            logger.info('Found {} estimadas for period {} - {}'.format(
                estimadas, ts_start, ts_end
            ))
            # TODO: More accurated way is do the operation meter by meter but
            # for now this is the best aprox
            teoric_n_hours = (int(((
                TZ.localize(datetime.strptime(ts_end, '%Y-%m-%d %H:%M:%S')) -
                TZ.localize(datetime.strptime(ts_start, '%Y-%m-%d %H:%M:%S'))
            ).total_seconds() / 3600)) * len(meters_type5))
            logger.info('Teoric hours: {}'.format(teoric_n_hours))
            try:
                perc_estimadas = float("{0:.5f}".format((100 * float(estimadas) / float(teoric_n_hours))))
            except ZeroDivisionError:
                perc_estimadas = 0
            try:
                perc_validas_brutas = float("{0:.5f}".format((100 * float(validas) / float(brutas))))
            except ZeroDivisionError:
                perc_validas_brutas = 0
            data.append([
                brutas,
                validas,
                invalidas,
                perc_validas_brutas,
                perc_estimadas
            ])
        return data

TgLecturesComptador()
