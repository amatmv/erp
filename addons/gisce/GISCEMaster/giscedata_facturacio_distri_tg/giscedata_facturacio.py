# -*- coding: utf-8 -*-
try:
    from collections import Counter
except ImportError:
    from backport_collections import Counter
from datetime import datetime, timedelta, date
import re

from osv import osv, fields
import netsvc
from tools.translate import _
from base_extended.base_extended import MultiprocessBackground, NoDependency
from oorq.decorators import job, create_jobs_group

import os
import glob
import bz2
from base64 import b64encode
import tempfile
from shutil import copyfileobj

from calendar import monthrange
import logging
from autoworker import AutoWorker
from osv.expression import OOQuery
from giscedata_telemesures_base.giscedata_facturacio import CCH_FIX_TECHNOLOGY


CNMC_TYPE5_METER_REPORT_FILENAME = 'CONTADORESINTEGRADOS_{0}_{1}T{2}.csv'
CMNC_TYPE5_METER_REPORT_TARIFF_CODE = {
    '2.0A': '01',
    '2.0DHA': '02',
    '2.0DHS': '03',
    '2.1A': '04',
    '2.1DHA': '05',
    '2.1DHS': '06',
    '3.0A': '07',
    '3.1A': '11',
    '3.1A LB': '11',
    '6.1A': '12',
    '6.1B': '13',
    '6.2': '14',
    '6.3': '15',
    '6.4': '16',
    '6.5': '17',
}
CNMC_TYPE5_METER_REPORT_HEADER = 'ANO;' \
                                 'MES;' \
                                 'DIS;' \
                                 'COM;' \
                                 'PEAJE;' \
                                 'NUM_CONT_TOTAL;' \
                                 'NUM_CONT_INT;' \
                                 'NUM_CCH_FACT;' \
                                 'REG_HOR_TOTAL;' \
                                 'REG_HOR_EST1;' \
                                 'REG_HOR_EST2;' \
                                 'REG_HOR_EST3;' \
                                 'REG_HOR_EST4;' \
                                 'REG_HOR_EST5;' \
                                 'REG_HOR_EST6'

if 'prime' not in dict(CCH_FIX_TECHNOLOGY).keys():
    CCH_FIX_TECHNOLOGY += [('prime', 'PRIME')]


def fix_dates(comptador, start, end):
    """Try to fix common dates problems with measures and metters.
    """

    if comptador.tg:
        end = datetime.strptime(end[0:10], '%Y-%m-%d')
        start = datetime.strptime(start[0:10], '%Y-%m-%d')

        modcons = comptador.polissa.get_modcontractual_intervals(
            (start - timedelta(days=1)).strftime('%Y-%m-%d'),
            end.strftime('%Y-%m-%d'), {'ffields': ['tarifa']}
        )
        changes = modcons.get(
            start.strftime('%Y-%m-%d'), {}
        ).get('changes', [])

        data_alta = datetime.strptime(comptador.data_alta[0:10], '%Y-%m-%d')
        # End is the end date + 1 day at our 00:00
        end += timedelta(days=1)
        end = end.strftime('%Y-%m-%d 00:00:00')

        if start != data_alta and 'tarifa' not in changes:
            # Start is and old measure date, we have to sum one day
            # and start at hour 01:00
            start += timedelta(days=1)

        start = start.strftime('%Y-%m-%d 01:00:00')

    return start, end


class GiscedataFacturacioContracteLotDistriTG(osv.osv):

    _name = 'giscedata.facturacio.contracte_lot'
    _inherit = 'giscedata.facturacio.contracte_lot'

    @job(queue='create_tg_read')
    def create_tg_read_async(self, cursor, uid, ids, context=None):
        return self.create_tg_read(cursor, uid, ids, context=context)

    def create_tg_read(self, cursor, uid, ids, context=None):

        if context is None:
            context = {}

        comptador_obj = self.pool.get('giscedata.lectures.comptador')
        lectures_obj = self.pool.get('giscedata.lectures.lectura')
        polissa_obj = self.pool.get('giscedata.polissa')
        origin_obj = self.pool.get('giscedata.lectures.origen')
        config_obj = self.pool.get('res.config')

        for clot_id in ids:
            clot = self.browse(cursor, uid, clot_id)
            date_limit = clot.lot_id.data_final
            ctx = {'date_limit': date_limit, 'lot_id': clot.lot_id.id}
            #Pick last meter from polissa
            comptador = clot.polissa_id.comptadors[0]
            polissa = polissa_obj.browse(
                cursor, uid, clot.polissa_id.id, {'date': date_limit}
            )
            if comptador.tg_cnc_conn:
                cch_enabled = (polissa.tg == '1')
                estimate = context.get('estimate', True)
                res = comptador.create_read_from_tg(context=ctx)
                date_read = res[comptador.id]['date_read']
                data_ult_lec = polissa.data_ultima_lectura
                # if cch_enabled and (not date_read or date_read <= data_ult_lec):
                if cch_enabled and estimate and not date_read:
                    # Get invoicing period
                    (factura_start,
                     factura_end) = polissa_obj.get_inici_final_a_facturar(
                                                    cursor,
                                                    uid, clot.polissa_id.id,
                                                    use_lot=clot.lot_id.id,
                                                    context=context)
                    data_inici = max(factura_start, comptador.data_alta)
                    data_final = factura_end
                    if comptador.data_baixa:
                        data_final = min(factura_end, comptador.data_baixa)
                    # We consider factura_start included so we have add one day
                    days = (datetime.strptime(data_final, '%Y-%m-%d') -
                            datetime.strptime(data_inici, '%Y-%m-%d')).days + 1

                    consumption, method = polissa_obj.estimate_consumption(
                        cursor,
                        uid,
                        clot.polissa_id.id,
                        clot.lot_id.id,
                        days
                    )
                    if method == 'historic':
                        subcodi = '01'
                    else:
                        subcodi = '02'
                    origin_search = [
                        ('codi', '=', '40'),
                        ('subcodi', '=', subcodi)
                    ]
                    origen_id = origin_obj.search(cursor, uid, origin_search)[0]
                    polissa = polissa_obj.browse(
                        cursor, uid, clot.polissa_id.id,
                        context={'date': date_limit}
                    )
                    # Agafar les últimes lectures facturades per poder fer
                    # la lectura actual, ja que l'estimació retorna el consum
                    # i nosaltres necessitem entrar una lectura
                    last_measure = max(polissa.data_ultima_lectura,
                                       comptador.data_alta)
                    last_measures = comptador.get_lectures(
                        polissa.tarifa.id, last_measure, tipus='A'
                    )
                    for period, values in last_measures.items():
                        vals = {
                            'name': date_limit,
                            'origen_id': origen_id,
                            'lectura': (
                                values['actual']['lectura'] +
                                consumption[period]
                            ),
                            'periode': values['actual']['periode'][0],
                            'tipus': 'A',
                            'comptador': comptador.id
                        }
                        lectures_obj.create(cursor, uid, vals, context=context)
                    # Check if we have to insert a reactiva measure
                    tg_insert_reactiva = config_obj.get(cursor, uid,
                                                        'tg_reactive_insert',
                                                        'never')
                    if tg_insert_reactiva != 'zero':
                        continue
                    last_measures = comptador.get_lectures(
                        polissa.tarifa.id, last_measure, tipus='R'
                    )
                    for period, values in last_measures.items():
                        vals = {
                            'name': date_limit,
                            'origen_id': origen_id,
                            'lectura': 0,
                            'periode': values['actual']['periode'][0],
                            'tipus': 'R',
                            'comptador': comptador.id
                        }
                        lectures_obj.create(cursor, uid, vals, context=context)

        return True

GiscedataFacturacioContracteLotDistriTG()


class GiscedataFacturacioLotDistriTG(osv.osv):

    _name = 'giscedata.facturacio.lot'
    _inherit = 'giscedata.facturacio.lot'

    def create_tg_reads(self, cursor, uid, lot_id, context=None):

        clot_obj = self.pool.get('giscedata.facturacio.contracte_lot')

        search_params = [('lot_id', '=', lot_id)]
        clot_ids = clot_obj.search(cursor, uid, search_params)
        jobs_ids = []
        with NoDependency() as nd:
            for clot_id in clot_ids:
                j = clot_obj.create_tg_read_async(
                    cursor, uid, [clot_id], context=context)
                jobs_ids.append(j.id)

        create_jobs_group(
            cursor.dbname, uid, _('Carregar tancaments TG'),
            'invoicing.tg.create_reads', jobs_ids
        )
        aw = AutoWorker(queue='create_tg_read', default_result_ttl=24 * 3600)
        aw.work()
        return True

    @MultiprocessBackground.background(skip_check=True)
    def create_tg_reads_button(self, cursor, uid, ids, context=None):
        '''Acció per crear lectures de TG en background amb multiprocés'''

        _(u"Acció per crear lectures de TG en background amb multiprocés")

        for lot_id in ids:
            self.create_tg_reads(cursor, uid, lot_id, context=context)
        return True

    def get_last_month_batch_id(self, cursor, uid, ids, context=None):
        """
        Return the identifier of the batch of the last month.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param context: dictionary with context
        :return: identifier of the batch of the last month
        """

        one_day = timedelta(days=1)
        first_day_of_the_month = date.today().replace(day=1)
        last_day_of_last_month = first_day_of_the_month - one_day
        last_batch_name = (last_day_of_last_month).strftime('%m/%Y')

        batch_ids = self.search(
            cursor,
            uid,
            [
                ('name', '=' , last_batch_name)
            ],
            context=context
        )

        return batch_ids[0]

    def create_case_no_f5d_uploaded(
            self,
            cursor,
            uid,
            batch_ids,
            context=None
    ):
        """
        Create case if F5D are not uploaded

        :param cursor: database cursor
        :param uid: user identifier
        :param batch_ids: batch ids
        :param context: dictionari with context
        :return: None
        """

        if context is None:
            context = {}
        context.update({'model': self._name})

        crm_case = self.pool.get('crm.case')

        for batch in self.browse(cursor, uid, batch_ids, context=context):
            description = _(
                "Falten CCH_FACT per tal de generar i pujar els F5D del lot {}"
            ).format(batch.name)
            case_id = crm_case.search(
                cursor,
                uid,
                [
                    ('ref', '=', '{},{}'.format(self._name,batch.id)),
                    ('description', '=', description),
                    ('section_id.code', '=', 'TG')
                ],
                context=context
            )
            if not case_id:
                case_id = crm_case.create_case_generic(
                    cursor,
                    uid,
                    [batch.id],
                    context=context,
                    description=description,
                    section='TG'
                )
            crm_case.case_open(cursor, uid, case_id)

    def upload_F5D(self, cursor, uid, batch_ids, force=False, context=None):
        """
        Generate and upload the F5D to the SFTP CCH server.

        :param cursor: database cursor
        :param uid: user identifier
        :param batch_ids: identifier of the batch
        :param context: dictionary with the context
        :return: None
        """

        if context is None:
            context = {}

        giscedata_facturacio_factura = \
            self.pool.get('giscedata.facturacio.factura')
        fact_cch_wizard = \
            self.pool.get('giscedata.export.fact.cch.curve.file.wizard')
        tg_cch_upload_status = self.pool.get('tg.cch.upload.status')
        res_config = self.pool.get('res.config')
        tg_f5d_last_day = int(
            res_config.get(cursor, uid, 'tg_f5d_last_day', 7)
        )

        wiz_data = {
            'overwrite': False,
            'upload': True,
            'set_last_cch': True,
            'curve_type': 'cch_fact'
        }

        for batch in self.browse(cursor, uid, batch_ids, context=context):
            tg_invoices_amount = len(
                giscedata_facturacio_factura.search(
                    cursor,
                    uid,
                    [
                        ('polissa_tg', '=', '1'),
                        ('potencia', '<=', 15),
                        ('lectures_energia_ids', '!=', []),
                        ('lot_facturacio','=', batch.id)
                    ],
                    context=context
                )
            )
            if tg_invoices_amount:
                invoice_with_cch_amount = len(
                    giscedata_facturacio_factura.search(
                        cursor,
                        uid,
                        [
                            ('cch_fact_available', '=', True),
                            ('lot_facturacio','=', batch.id)
                        ],
                        context=context
                    )
                )
                tg_cch_upload_status_ids = tg_cch_upload_status.search(
                    cursor,
                    uid,
                    [
                        ('attachment.res_id', '=' , batch.id),
                         ('attachment.res_model', '=', self._name)
                    ],
                    context=context
                )
                if not tg_cch_upload_status_ids:
                    if invoice_with_cch_amount == tg_invoices_amount or force:
                        context.update({'active_id': batch.id})

                        try:
                            fact_cch_wizard.action_lot_curve_files(
                                cursor,
                                uid,
                                None,
                                wiz_data,
                                context
                            )
                        except osv.except_osv:
                            pass
                    elif date.today().day >= tg_f5d_last_day:
                        self.create_case_no_f5d_uploaded(
                            cursor,
                            uid,
                            [batch.id],
                            context=context
                        )

    def _cronjob_upload_F5D(self, cursor, uid, ids, context=None):
        """
        Upload F5D of last month.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param context: dictionary with context
        :return: None
        """

        last_month_batch_id = self.get_last_month_batch_id(
            cursor,
            uid,
            ids,
            context=context
        )
        self.upload_F5D(
            cursor,
            uid,
            [last_month_batch_id],
            context=context
        )


GiscedataFacturacioLotDistriTG()


class GiscedataFacturacioFacturador(osv.osv):
    _name = 'giscedata.facturacio.facturador'
    _inherit = 'giscedata.facturacio.facturador'

    def fact_via_lectures(self, cursor, uid, polissa_id, lot_id, context=None):
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        polissa_obj = self.pool.get('giscedata.polissa')
        if context is None:
            context = {}
        res = super(GiscedataFacturacioFacturador, self).fact_via_lectures(
            cursor, uid, polissa_id, lot_id, context
        )
        invoices_to_fix = []
        for invoice in factura_obj.read(cursor, uid, res, ['polissa_tg',
                                                           'polissa_id']):
            q = OOQuery(polissa_obj, cursor, uid)
            sql = q.select(
                ['tarifa.name'], only_active=False
            ).where([('id', '=', invoice['polissa_id'][0])])
            cursor.execute(*sql)
            tariff_name = cursor.fetchone()[0]
            if invoice['polissa_tg'] == '1' or (invoice['polissa_tg'] == '2' and
                                                tariff_name == '3.0A'):
                invoices_to_fix.append(invoice['id'])
        if invoices_to_fix:
            factura_obj.fix_cch_fact(
                cursor, uid, invoices_to_fix, context=context
            )
        return res

GiscedataFacturacioFacturador()


class GiscedataFacturacioFactura(osv.osv):
    """Extends Factura to fix the CCH from the invoice values.
    """
    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def get_balance_per_meter(self, cursor, uid, ids, context=None):
        """Obtain the balance of an invoice.

        The goal is obtain a dict with the following structure:
        {invoice_id: {
            'dates': {
                'meter_serial_number': (min_date, max_date)
            },
            'balance': {
                'meter_serial_number': {
                    'Period_Number': Consum
                }
            },
            'origin': {
                'meter_serial_number': 'codi-subcodi'
            },
            'tariff': 'Tariff name'
        }
        """
        res = {}
        for invoice in self.browse(cursor, uid, ids, context=context):
            metters = {
                'balance': {},
                'dates': {},
                'tariff': None,
                'origin': {}
            }
            balances = metters['balance']
            dates = metters['dates']
            metters['tariff'] = invoice.tarifa_acces_id.name
            for lectura in invoice.lectures_energia_ids:
                if lectura.tipus == 'reactiva':
                    continue
                comptador = lectura.comptador_id.build_name_tg()
                if comptador not in balances:
                    balances[comptador] = Counter()
                period = re.findall('.*(P[1-6]).*', lectura.name)[0]
                balances[comptador][period] += lectura.consum
                metters['origin'][comptador] = '{0}-{1}'.format(
                    lectura.origen_id.codi, lectura.origen_id.subcodi
                )
                lect_dates = fix_dates(lectura.comptador_id,
                                       lectura.data_anterior,
                                       lectura.data_actual)
                if comptador not in dates:
                    dates[comptador] = lect_dates
                else:
                    start_meter, end_meter = dates[comptador]
                    start_lect, end_lect = lect_dates
                    start = min(start_meter, start_lect)
                    end = max(end_meter, end_lect)
                    dates[comptador] = start, end
            res[invoice.id] = metters.copy()
        return res

    @job(queue='fix_cch_fact')
    def fix_cch_fact(self, cursor, uid, ids, context=None):
        return self.fix_cch_fact_sync(cursor, uid, ids, context=context)

    def fix_cch_fact_sync(self, cursor, uid, ids, context=None):
        tg_profile_obj = self.pool.get('tg.profile')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        logger = logging.getLogger('openerp.{}'.format(__name__))

        # Filter... maybe some ids are removed when job is executed
        # Criteria: type 5, CCH or all type 3/4
        ids = self.search(cursor, uid, [
            ('id', 'in', ids),
            ('type', '=', 'out_invoice'),
            '|',
            ('polissa_tg', '=', '1'),
            ('tarifa_acces_id.name', '=', '3.0A')
        ], context={
            'active_test': False
        })
        for invoice in self.browse(cursor, uid, ids, context=context):
            meters = invoice.get_balance_per_meter()[invoice.id]
            if meters['balance']:
                for meter, balance in meters['balance'].items():
                    start, end = meters['dates'][meter]
                    tariff = meters['tariff']
                    origin = meters['origin'][meter]

                    meter_id = meter_obj.search(cursor, uid, [
                        ('meter_tg_name', '=', meter),
                        ('tg', '=', True),
                        ('polissa', '=', invoice.polissa_id.id)],
                         context={'active_test': False})
                    if meter_id:
                        tg_profile_obj.fix(
                            cursor,
                            uid,
                            meter,
                            start,
                            end,
                            tariff,
                            balance,
                            origin
                        )
                        invoice.write({'cch_fact_available': 1})
            else:
                logger.warning(
                    'No balance for invoice {}, not fixing CCH_FACT'.format(
                        invoice.number))
                invoice.write({'cch_fact_available': 0})

        return True

    def get_cch_curve(self, cursor, uid, ids, cch_type='cch_fact',
                      fformat='csv', output='memory', context=None):

        res_config = self.pool.get('res.config')

        tg_cch_fact_invoice_length = int(
            res_config.get(
                cursor,
                uid,
                'tg_cch_fact_invoice_length',
                '0'
            )
        )

        if not context:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        if cch_type == 'cch_cons' and fformat == 'xls':
            ids = ids[:1]

        cups_obj = self.pool.get('giscedata.cups.ps')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        lect_obj = self.pool.get('giscedata.facturacio.lectures.energia')

        filetext = ''
        if output == 'disk':
            filetext = tempfile.NamedTemporaryFile(delete=False)

        inv_fields = ['data_inici', 'data_final', 'cups_id', 'number',
                      'lectures_energia_ids']

        inv_vals = self.read(cursor, uid, ids, inv_fields)

        has_data = False
        for inv in inv_vals:
            if not inv['lectures_energia_ids']:
                # It has not energy lines
                continue

            if tg_cch_fact_invoice_length and cch_type == 'cch_fact':
                start_date = inv['data_inici']
                end_date = inv['data_final']
            else:
                # dates init
                start_date = inv['data_final']
                end_date = inv['data_inici']

                # Search for energy dates interval
                lect_fields = ['data_anterior', 'data_actual', 'comptador_id']
                for lect_id in inv['lectures_energia_ids']:
                    lect_vals = lect_obj.read(
                        cursor, uid, lect_id, lect_fields
                    )
                    meter = meter_obj.browse(
                        cursor, uid, lect_vals['comptador_id'][0]
                    )
                    start_date_str, end_date_str = fix_dates(
                        meter, lect_vals['data_anterior'], lect_vals['data_actual']
                    )
                    # End date as in invoice
                    end_date_tmp = lect_vals['data_actual']
                    start_date = min(start_date_str, start_date)
                    end_date = max(end_date_tmp, end_date)

            cups_id = inv['cups_id'][0]
            invoice_number = inv['number']
            ctx = context.copy()
            ctx.update({'fact_number': invoice_number})
            txt = cups_obj.get_CCH(
                cursor, uid, cups_id, cch_type, start_date, end_date, fformat,
                context=ctx
            )
            if txt and not has_data:
                has_data = True
            if output == 'disk':
                filetext.write(txt)
            else:
                filetext += txt

        if output == 'disk':
            filename = filetext.name
            filetext.close()
            return has_data and filename or False
        else:
            return filetext

    def get_last_cch_file_version(self, filedir, filename):
        """Get file version in filedir """
        # current prefix
        filename_split = filename.rsplit('.', 1)
        prefix = filename_split[0]

        lastfile_name = sorted(
            glob.glob('{0}/{1}.*'.format(filedir, prefix)),
            key=lambda x: int(x.split('.')[1])  # to order values with different number of digits
        )

        if not lastfile_name:
            return None

        version = lastfile_name[-1].split('.')[1]

        if not version.isdigit():
            return None
        else:
            return version

    def get_comer_cch_fact(
            self,
            cursor,
            uid,
            fact_ids,
            comer_id,
            cch_type='cch_fact',
            filedir=None,
            overwrite=False,
            context=None,
            upload=False
    ):
        """
        Create a CCH_FACT curve from invoices in lot for a comer selected
            by comer_id only

        :param cursor: database cursor
        :param uid: user identifier
        :param fact_ids: invoice identifiers
        :param comer_id: marketer identifier
        :param cch_type: type of CCH
        :param filedir: directory path to save the file
        :param overwrite: True to overwrite the last file version. False to \
            create new version.
        :param context: dictionary with the context
        :param upload: True to upload CCH file to SFTP or FTP servers. False do not \
            upload it.
        :return: two string one with the filename and another with the file \
            content
        """
        logger = netsvc.Logger()
        if not context:
            context = {}

        if isinstance(comer_id, (list, tuple)):
            comer_id = comer_id[0]

        if not filedir:
            res_obj = self.pool.get('res.config')
            filedir = res_obj.get(
                cursor, uid, 'tg_profile_temp_filedir', '/tmp/curves'
            )

        fact_obj = self.pool.get('giscedata.facturacio.factura')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        partner_obj = self.pool.get('res.partner')
        ir_attachment = self.pool.get('ir.attachment')

        comer_vals = partner_obj.read(cursor, uid, comer_id, ['name', 'ref'])
        comer_name = comer_vals['name']

        fact_vals = fact_obj.read(
            cursor,
            uid,
            [fact_ids[0]],
            [
                'cups_id',
                'comptadors',
                'partner_id',
                'polissa_id',
                'lot_facturacio'
            ]
        )
        if not fact_vals:
            logger.notifyChannel(
                'exporta_curves_tg', netsvc.LOG_INFO,
                _(u'WARNING: Exporting {0} for comer "{1}". '
                  u'No invoices found').format(cch_type.upper(), comer_name))
        if fact_vals[0]['lot_facturacio']:
            invoicing_batch_id = fact_vals[0]['lot_facturacio'][0]
        else:
            invoicing_batch_id = False

        ref = comer_vals['ref']

        filename = meter_obj.get_cch_filename(
            cursor,
            uid,
            None,
            cch_type,
            0,
            context=context,
            default_marketer=ref
        )

        # one dir per comer
        dest_dir = '{0}/{1}'.format(filedir, ref)
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)

        last_version = self.get_last_cch_file_version(dest_dir, filename)
        if last_version:
            if overwrite:
                new_version = last_version
            else:
                new_version = int(last_version) + 1

            filename = filename.replace('.0', '.{0}'.format(new_version))

        filename = '{}.bz2'.format(filename)
        curve_filepath = '{0}/{1}'.format(dest_dir, filename)

        tmp_filename = fact_obj.get_cch_curve(
            cursor,
            uid,
            fact_ids,
            cch_type=cch_type,
            output='disk',
            context=context
        )

        if tmp_filename:
            with open(tmp_filename, 'rb') as input:
                with bz2.BZ2File(curve_filepath, 'wb') as output:
                    logger.notifyChannel(
                        'exporta_curves_tg', netsvc.LOG_INFO,
                        _(u'Exporting {0}  for comer '
                          u'"{1}" on "{2}" file').format(
                            cch_type,
                            comer_name,
                            curve_filepath
                        )
                    )
                    copyfileobj(input, output)

            output = open(curve_filepath, 'r')

            attachment_values = {
                'name': filename,
                'res_id': invoicing_batch_id,
                'res_model': 'giscedata.facturacio.lot',
                'datas': b64encode(output.read()),
                'marketer': comer_id,
                'type': 'f5d'
            }
            ir_attachment_id = ir_attachment.create(
                cursor,
                uid,
                attachment_values,
                context=context
            )

            if upload:
                sftp_obj = self.pool.get('tg.cch.server')
                ftp_obj = self.pool.get('giscedata.ftp.connections')
                sftp_server = sftp_obj.search(cursor, uid, [])
                ftp_server = ftp_obj.search(cursor, uid, [('category_type.code', '=', 'TGM')])
                if sftp_server:
                    # SFTP Mode
                    ir_attachment.upload(
                        cursor,
                        uid,
                        ir_attachment_id,
                        context=context
                    )
                elif ftp_server:
                    # FTP Mode
                    server = self.pool.get('tg.cch.server')
                    server.ftp_upload(
                        cursor, uid, 'f5d', ref, filename, curve_filepath, ir_attachment_id, context=context
                    )

        return filename, tmp_filename

    def distributor_r1(self, cursor, uid, context=None):
        """
        Returns the R1 code for the distributor.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dictionary with the context
        :return: string with the R1 code of the distributor
        """
        res_users = self.pool.get('res.users')
        distributor = res_users.browse(cursor, uid, uid, context=context)
        return distributor.company_id.partner_id.ref2

    def cnmc_type5_meter_report_filename(
            self,
            cursor,
            uid,
            year,
            quarter,
            context=None
    ):
        """
        Build the filename of CNMC type5 meter report.

        :param cursor: database cursor
        :param uid: user identifier
        :param year: the year of the report
        :param quarter: the quarter of the report
        :param context: dictionary with the context
        :return: string with the filename
        """
        r1 = self.distributor_r1(cursor, uid, context=context)

        return CNMC_TYPE5_METER_REPORT_FILENAME.format(
            r1,
            year,
            quarter
        )

    def create_cnmc_type5_meter_report(
            self,
            cursor,
            uid,
            year,
            quarter,
            context=None
    ):
        """
        Build the content of the CNMC type5 meter report. \
            http://sede.cnmc.es/procedimiento.aspx?codigo=019

        :param cursor: database cursor
        :param uid: user identifier
        :param year: the year of the report
        :param quarter: the quarter of the report
        :param context: dictionary with the context
        :return: string CVS formated with the content of the CNMC type5 meter \
            report
        """
        giscedata_polissa_modcontractual = \
            self.pool.get('giscedata.polissa.modcontractual')
        giscedata_facturacio_factura = \
            self.pool.get('giscedata.facturacio.factura')
        giscedata_lectures_comptador = \
            self.pool.get('giscedata.lectures.comptador')
        tg_billing = self.pool.get('tg.billing')
        tg_profile = self.pool.get('tg.profile')
        r1 = self.distributor_r1(cursor, uid)
        logger = logging.getLogger(
            'openerp.{0}.cnmc_type5_meter_report'.format(__name__)
        )

        if context is None:
            context_no_active_test = {}
        else:
            context_no_active_test = context.copy()
        context_no_active_test['active_test'] = False

        data = [CNMC_TYPE5_METER_REPORT_HEADER]
        quarter_months = range((quarter-1) * 3 + 1, quarter * 3 + 1)
        for quarter_month in quarter_months:
            month = str(quarter_month).zfill(2)
            last_day = monthrange(year, quarter_month)[1]
            first_date = '{year}-{month}-01'.format(**locals())
            last_date = '{year}-{month}-{last_day}'.format(**locals())
            last_date_datetime = datetime.strptime(last_date,'%Y-%m-%d')
            last_date_next_day = last_date_datetime + timedelta(days=1)
            last_date_next_day_timestamp = last_date_next_day.strftime(
                '%Y-%m-%d %H:%M:%S'
            )

            excluded_contracts = ['09', '10']
            policy_amendment_ids = giscedata_polissa_modcontractual.search(
                cursor,
                uid,
                [
                    ('data_inici', '<=', last_date),
                    ('data_final', '>=', last_date),
                    ('potencia', '<=', 15),
                    ('tarifa', 'not in', ['RE', 'RE12']),
                    ('contract_type', 'not in', excluded_contracts)
                ],
                context=context_no_active_test
            )

            summary = {}
            for n_modcon, policy_amendment_id in enumerate(
                    policy_amendment_ids, start=1):
                logger.info('[{0}] Modcon #{1} of {2}'.format(
                    quarter_month, n_modcon, len(policy_amendment_ids))
                )
                policy_amendment = giscedata_polissa_modcontractual.browse(
                    cursor,
                    uid,
                    policy_amendment_id,
                    context=context_no_active_test
                )

                marketer_code = policy_amendment.comercialitzadora.ref2

                if not marketer_code:
                    raise osv.except_osv(
                        _('Error!'),
                        _("Marketer {} without R2 code").format(
                            policy_amendment.comercialitzadora.name
                        )
                    )

                if marketer_code not in summary:
                    summary[marketer_code] = {}

                tariff_name = policy_amendment.tarifa.name
                tariff_code = CMNC_TYPE5_METER_REPORT_TARIFF_CODE[tariff_name]

                if tariff_code in summary[marketer_code]:
                    summary[marketer_code][tariff_code]['count'] += 1
                else:
                    summary[marketer_code][tariff_code] = {
                        'count': 1,
                        'integrated': 0,
                        'cch_fact': 0,
                        'cch_fact_reg': {
                            'count': 0,
                            '1': 0,
                            '2': 0,
                            '3': 0,
                            '4': 0,
                            '5': 0,
                            '6': 0
                        }
                    }

                tariff = summary[marketer_code][tariff_code]

                integrated = False
                # Get active meters on last_date
                meter_ids = giscedata_lectures_comptador.search(
                    cursor,
                    uid,
                    [
                        ('polissa', '=', policy_amendment.polissa_id.id),
                        ('tg', '=', True),
                        ('data_alta', '<=', last_date),
                        '|',
                        ('data_baixa', '>=', last_date),
                        ('data_baixa', '=', False)
                    ],
                    context=context_no_active_test
                )
                for meter_id in meter_ids:
                    # Integrated criteria is tg 1
                    if policy_amendment.tg == '1':
                        integrated = True

                if integrated:
                    tariff['integrated'] += 1
                else:
                    continue

                invoice_ids = giscedata_facturacio_factura.search(
                    cursor,
                    uid,
                    [
                        ('polissa_id', '=', policy_amendment.polissa_id.id),
                        ('date_invoice', '>=', first_date),
                        ('date_invoice', '<=', last_date),
                        ('state', 'in', ('open', 'paid')),
                        ('tipo_rectificadora', '=', 'N'),
                        ('cch_fact_available', '=', True),
                        ('partner_id', '=',
                         policy_amendment.comercialitzadora.id),
                    ]
                )
                invoices = giscedata_facturacio_factura.browse(
                    cursor,
                    uid,
                    invoice_ids
                )
                for invoice in invoices:
                    if not invoice.journal_id.code.startswith('ENERGIA'):
                        continue 
                    tariff['cch_fact'] += 1

                    # end curve must be day after invoice end at 01:00:00
                    # get_curve_tg function do not includes last hour '[ )'

                    start_curve = datetime.strptime(
                        invoice.data_inici, '%Y-%m-%d'
                    ) + timedelta(hours=1)
                    end_curve = datetime.strptime(
                        invoice.data_final, '%Y-%m-%d'
                    ) + timedelta(days=1, hours=1)
                    for meter in invoice.comptadors:
                        profile_ids = meter.get_curve_tg(
                            '{0}'.format(start_curve),
                            '{0}'.format(end_curve),
                            cch_type='fact',
                            context=context
                        )
                        profiles = tg_profile.read(
                            cursor,
                            uid,
                            profile_ids,
                            ['cch_fact', 'kind_fact'],
                            context=context
                        )
                        for profile in profiles:
                            if profile['cch_fact']:
                                tariff['cch_fact_reg']['count'] += 1
                                kind_fact = profile['kind_fact']
                                tariff['cch_fact_reg'][kind_fact] += 1

            for marketer_code in summary:
                for tariff_code in summary[marketer_code]:
                    tariff = summary[marketer_code][tariff_code]
                    item = [
                        str(year),
                        str(quarter_month),
                        r1,
                        marketer_code,
                        tariff_code,
                        str(tariff['count']),
                        str(tariff['integrated']),
                        str(tariff['cch_fact']),
                        str(tariff['cch_fact_reg']['count']),
                        str(tariff['cch_fact_reg']['1']),
                        str(tariff['cch_fact_reg']['2']),
                        str(tariff['cch_fact_reg']['3']),
                        str(tariff['cch_fact_reg']['4']),
                        str(tariff['cch_fact_reg']['5']),
                        str(tariff['cch_fact_reg']['6'])
                    ]
                    data.append(';'.join(item))
        csv = '\n'.join(data)
        return csv

GiscedataFacturacioFactura()
