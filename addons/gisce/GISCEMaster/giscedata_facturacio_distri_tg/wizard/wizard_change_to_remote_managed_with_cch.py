# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _

from datetime import date
from dateutil.relativedelta import relativedelta
from calendar import monthrange
from workalendar.europe import Spain


class GiscedataChangeToRemoteManagedWithCCH(osv.osv_memory):
    """
    Wizard to change policies to remote managed with CCH.
    """

    _name = 'giscedata.change.to.remote.managed.with.cch.wizard'

    def _get_meter_ids(self, cursor, uid, ids, context=None):
        """
        List of meter identifiers from active_ids of context. The active_ids \
            can be meters or policies depending on the from_model value of \
            context dictionary.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param context: dictionary with the context
        :return: list of meter identifiers
        """

        giscedata_lectures_comptador = \
            self.pool.get('giscedata.lectures.comptador')

        if context['from_model'] == 'giscedata.lectures.comptador':
            meter_ids = context['active_ids']
        elif context['from_model'] == 'giscedata.polissa':
            meter_ids = giscedata_lectures_comptador.search(
                cursor,
                uid,
                [('polissa', 'in', context['active_ids'])],
                context=context
            )

        return meter_ids

    def _classify_meters(
            self,
            cursor,
            uid,
            ids,
            activation_date,
            context=None
    ):
        """
        Classify meters to change to remote managed with CCH. It returns a \
            dictionary with the following keys:
        'inactive': list with the name of meters that are not active
        'not_remote_managed': list with the name of meters that aren't remote \
            managed
        'not_connected': list with the name of meters that aren't connected \
            to any concentrator.
        'policy_remote_managed': list with the name of meters that its policy \
            is already remote managed with CCH
        'to_remote_managed_meters_id: list with the meter identifications of \
            meters that can be changed to remote managed with CCH

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param activation_date: the meter should be active on this date
        :param context: dictionary with the context, the 'active_ids' key \
            must be a list of meter identifiers
        :return: dictionary with the classification
        """

        giscedata_lectures_comptador = \
            self.pool.get('giscedata.lectures.comptador')

        meter_ids = self._get_meter_ids(cursor, uid, ids, context=context)

        classification = {
            'inactive': [],
            'not_remote_managed': [],
            'not_connected': [],
            'policy_remote_managed': [],
            'policy_not_type_5': [],
            'policy_cut_off': [],
            'to_remote_managed_meter_ids': []
        }
        for meter in giscedata_lectures_comptador.browse(
            cursor,
            uid,
            meter_ids,
            context=context
        ):
            if not meter.active or meter.data_alta > activation_date:
                classification['inactive'].append(meter.name)
            elif not meter.tg:
                classification['not_remote_managed'].append(meter.name)
            elif not meter.tg_cnc_conn:
                classification['not_connected'].append(meter.name)
            elif meter.polissa.tg == '1':
                classification['policy_remote_managed'].append(meter.name)
            elif meter.polissa.potencia > 15:
                classification['policy_not_type_5'].append(meter.name)
            elif meter.polissa.state == 'tall':
                classification['policy_cut_off'].append(meter.name)
            else:
                classification['to_remote_managed_meter_ids'].append(meter.id)

        return classification

    def _skipped_report(self, classification, past=True):
        """
        Build a report based on the classification. See also \
            _classify_meters().

        :param classification: dictionary with the classification
        :param past: False if the notice is previous to the action. True, the \
            default, if the notice is posterior to the action.
        :return: string with the report
        """

        skipped = len(classification['inactive']) \
                  + len(classification['not_remote_managed']) \
                  + len(classification['not_connected']) \
                  + len(classification['policy_remote_managed']) \
                  + len(classification['policy_not_type_5']) \
                  + len(classification['policy_cut_off'])

        skipped_report = ''
        if skipped > 0:
            if past:
                skipped_report = _(
                    u"No s'ha canviat la pòlissa de {0} comptadors:\n"
                ).format(skipped)
            else:
                skipped_report = _(
                    u"No es canviarà la pòlissa de {0} comptadors:\n"
                ).format(skipped)
            if classification['inactive']:
                skipped_report += _(
                    u"Comptadors inactius:\n{0}\n"
                ).format(
                    '\n'.join(
                        map(str, classification['inactive'])
                    )
                )
            if classification['not_remote_managed']:
                skipped_report += _(
                    u"Comptadors no telegestionats:\n{0}\n"
                ).format(
                    '\n'.join(
                        map(str, classification['not_remote_managed'])
                    )
                )
            if classification['not_connected']:
                skipped_report += _(
                    u"Comptadors no connectats:\n{0}\n"
                ).format(
                    '\n'.join(
                        map(str,classification['not_connected'])
                    )
                )
            if classification['policy_remote_managed']:
                skipped_report += _(
                    u"Comptadors amb pòlissa ja Operativa amb CCH:\n{0}\n"
                ).format(
                    '\n'.join(
                        map(str, classification['policy_remote_managed'])
                    )
                )
            if classification['policy_not_type_5']:
                skipped_report += _(
                    u"Comptadors amb pòlissa diferent de tipus 5:\n{0}\n"
                ).format(
                    '\n'.join(
                        map(str, classification['policy_not_type_5'])
                    )
                )
            if classification['policy_cut_off']:
                skipped_report += _(
                    u"Comptadors amb pòlissa en estat de tall:\n{0}\n"
                ).format(
                    '\n'.join(
                        map(str, classification['policy_cut_off'])
                    )
                )

        return skipped_report

    def select_date_to_change_to_remote_managed_with_cch(
            self,
            cursor,
            uid,
            ids,
            context=None
    ):
        """
        Select date to change to Operative with CCH.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: list with the wizard identifier
        :param context: dict with the context
        :return: None
        """

        wizard = self.browse(cursor, uid, ids[0], context=context)

        meter_ids = self._get_meter_ids(cursor, uid, ids, context=context)

        self.classification = self._classify_meters(
            cursor,
            uid,
            meter_ids,
            wizard.month,
            context=context
        )

        info = _(u"Es canviarà {0} pòlisses a Operativa amb CCH.\n").format(
            len(self.classification['to_remote_managed_meter_ids'])
        )
        info += self._skipped_report(self.classification, past=False)

        wizard.write({'state': 'info', 'info': info})

    def action_change_to_remote_managed_with_cch(
            self,
            cursor,
            uid,
            ids,
            context=None
    ):
        """
        Change policies of the provided meters to remote managed with CCH.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: list with the wizard identifier
        :param context: dict with the context
        :return: None
        """

        giscedata_lectures_comptador = \
            self.pool.get('giscedata.lectures.comptador')

        wizard = self.browse(cursor, uid, ids[0], context=context)

        skipped = self._skipped_report(self.classification)

        atr_case_ids = []
        atr_cups_failures = []
        for meter in giscedata_lectures_comptador.browse(
            cursor,
            uid,
            self.classification['to_remote_managed_meter_ids'],
            context=context
        ):
            context.update({'keep_lot': wizard.keep_lot})
            case_id = meter.polissa.change_to_remote_managed_with_cch(
                    wizard.month,
                    context=context
                )

            if case_id:
                atr_case_ids.extend(case_id)
            else:
                atr_cups_failures.append(meter.polissa.cups.name)

        atr_failures = len(atr_cups_failures)

        failures = ''
        if atr_failures > 0:
            failures = _(
                u"ALERTA: S'ha canviat les pòlises però no s'ha pogut generar {0} casos D1 pels següents CUPS:\n{1}\n"
            ).format(
                atr_failures,
                '\n'.join(atr_cups_failures),
            )

        atr_cases = len(atr_case_ids)

        successful = ''
        if atr_cases > 0:
            successful = _(
                u"S'han creat {0} casos D1 amb identificació:\n{1}\n"
            ).format(
                atr_cases,
                '\n'.join(map(str,atr_case_ids)),
            )

        info = failures + successful + skipped
        wizard.write({'state': 'end', 'info': info})

    def _deadline_for_last_month(
            self,
            cursor,
            uid,
            considered_date=None,
            context=None
    ):
        """
        It's past the deadline for last month?

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the contextual parameters
        :param considered_date: date to be considered. Today is used if it is \
            None.
        :return: True if deadline has past and False if deadline hasn't past.
        """

        if considered_date is None:
            considered_date = date.today()

        res_config = self.pool.get('res.config')

        deadline_working_days = int(
            res_config.get(
                cursor,
                uid,
                'change_to_remote_managed_with_cch_deadline_working_days',
                '2'
            )
        )

        last_month_date = considered_date - relativedelta(months=1)
        last_day = monthrange(last_month_date.year, last_month_date.month)[1]
        last_day_last_month_date = last_month_date.replace(day=last_day)
        calendar = Spain()
        deadline_date = calendar.add_working_days(
            last_day_last_month_date,
            deadline_working_days
        )

        return considered_date > deadline_date

    def _months(self, cursor, uid, context=None):
        """
        List of valid months. Current month and last month until deadline. \
            The deadline is configured with \
            change_to_remote_managed_with_cch_deadline_day.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the contextual parameters
        :return: list with the valid months
        """

        today = date.today()
        months = []
        if not self._deadline_for_last_month(cursor, uid, context=context):
            last_month = today - relativedelta(months=1)
            months.append(
                (
                    str(last_month.replace(day=1)),
                    last_month.strftime('%m/%Y')
                )
            )
        months.append(
            (
                str(today.replace(day=1)),
                today.strftime('%m/%Y')
            )
        )

        return months

    def _default_info(self, cursor, uid, context=None):
        """
        The notice about the action with skipped meters.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dict with the context
        :return: string with the information
        """

        info = _(
            u"S'han seleccionat {} pòlisses per canviar a Operativa amb CCH.\n"
            u"\nATENCIÓ: Es crearà una nova modificació contractual amb "
            u"periodicitat de facturació mensual. Si heu seleccionat pòlisses "
            u"amb facturació d'energia bimestral cal haver facturat tot "
            u"l'anterior període o bé caldrà generar una factura manual del "
            u"període no facturat.\n"
        ).format(
            len(context['active_ids'])
        )

        return info

    _columns = {
        'state': fields.selection(
            [
                ('init', 'Inicial'),
                ('info', 'Informació'),
                ('fail', 'Fallit'),
                ('end', 'Final')
            ],
            'State'
        ),
        'info': fields.text('Info'),
        'month': fields.selection(_months, 'Mes'),
        'keep_lot': fields.boolean(
            'Mantenir el lot de facturació',
            help='Si es deixa aquest camp actiu el lot de facturació '
                 'no canviarà'
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'month': str(date.today().replace(day=1)),
        'keep_lot': lambda *a: False,
    }

GiscedataChangeToRemoteManagedWithCCH()