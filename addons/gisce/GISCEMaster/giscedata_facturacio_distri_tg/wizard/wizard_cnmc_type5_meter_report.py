# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime
from base64 import b64encode

from base_extended.base_extended import MultiprocessBackground


class WizardCnmcType5MeterReport(osv.osv_memory):
    """
    Wizard to create CNMC Type5 Meter Report.
    """

    _name = 'wizard.cnmc.type5.meter.report'

    def _default_info(self, cursor, uid, context=None):
        """
        Returns the wizard information.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dictionary with the context
        :return: the translated wizard information
        """
        return _("Crear informe de comptadors de telegestió de tipus 5 de la "
                 "CNMC.")

    def _default_year(self, cursor, uid, context=None):
        """
        Returns the year of last quarter.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dictionary with the context
        :return: the year
        """
        year = datetime.now().year
        if datetime.now().month <= 3:
            return year - 1
        else:
            return year

    def _default_quarter(self, cursor, uid, context=None):
        """
        Returns last quarter.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dictionary with the context
        :return: the quarter
        """
        month = datetime.now().month
        if month <= 3:
            return 4
        else:
            return (month - 1) / 3

    def _check_quarter(self, cursor, uid, ids, context=None):
        """
        Check quarter value is valid.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: record identifiers
        :param context: dictionary with the context
        :return: True if valid value or False if invalid value
        """
        record = self.browse(cursor, uid, ids, context=context)
        return 1 <= record[0].quarter <= 4

    def action_create_cnmc_type5_meter_report(
            self,
            cursor,
            uid,
            ids,
            context=None
    ):
        """
        Create CNMC Type5 Meter Report.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: dict with the wizard identifier
        :param context: context
        """
        wizard = self.browse(cursor, uid, ids[0], context=context)

        try:
            self.create_cnmc_type5_meter_report(
                cursor,
                uid,
                wizard.year,
                wizard.quarter,
                context=context
            )
        except osv.except_osv:
            pass

        wizard.write(
            {
                'state': 'end',
                'info': _("S'està creant l'informe en segon pla."),
            },
            context=context
        )

    @MultiprocessBackground.background(queue='low')
    def create_cnmc_type5_meter_report(
            self,
            cursor,
            uid,
            year,
            quarter,
            context=None
    ):
        """
        Create CNMC Type5 Meter Report.

        :param cursor: database cursor
        :param uid: user identifier
        :param year: the year of the report
        :param quarter: the quarter of the report
        :param context: dictionary with the context
        :return: None
        """

        giscedata_facturacio_factura = \
            self.pool.get('giscedata.facturacio.factura')
        ir_attachment = self.pool.get('ir.attachment')

        filename = \
            giscedata_facturacio_factura.cnmc_type5_meter_report_filename(
                cursor,
                uid,
                year,
                quarter,
                context=context
            )
        report = giscedata_facturacio_factura.create_cnmc_type5_meter_report(
            cursor,
            uid,
            year,
            quarter,
            context=context
        )
        file_content_b64 = b64encode(report)
        attachment_values = {
            'name': filename,
            'res_model': self._name,
            'datas': file_content_b64
        }
        ir_attachment.create(
            cursor,
            uid,
            attachment_values,
            context=context
        )

    _columns = {
        'state': fields.selection(
            [
                ('init', 'Inicial'),
                ('fail', 'Fallit'),
                ('end', 'Final')
            ],
            'State'
        ),
        'info': fields.text('Informació', readonly=True),
        'year': fields.integer('Any', required=True),
        'quarter': fields.integer('Trimestre', required=True),
    }

    _constraints = [
        (_check_quarter, 'Trimestre invàlid', ['quarter'])
    ]

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        # set last quarter
        'quarter': _default_quarter,
        'year': _default_year,
    }

WizardCnmcType5MeterReport()
