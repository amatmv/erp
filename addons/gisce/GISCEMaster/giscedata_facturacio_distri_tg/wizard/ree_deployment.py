# coding=utf-8
from __future__ import unicode_literals

from xlwt.Workbook import Workbook


class REEDeloymentTG(object):
    def __init__(self):
        self.xls = Workbook()
        self.xls.add_sheet("SITUACION DESPLIEGUE")
        self.xls.add_sheet("%CCH")
        self.xls.add_sheet("INCIDENCIAS EN CCH")

        sheet = self.xls.get_sheet(0)
        header = ("FECHA DE DATOS	TOTAL SUMINISTROS	"
                  "TELEGESTIÓN OPERATIVA CON CCH	"
                  "TELEGESTIÓN NO OPERATIVA	TELEGESTIÓN OPERATIVA SIN CCH	"
                  "% DESPLEGADO	% TG OPERATIVA CON CCH vs TOTAL")
        for idx, h in enumerate(header.split('\t')):
            sheet.write(0, idx, h)

        for idx in range(1, 13):
            sheet.write(idx, 0, 'M-{0}'.format(idx))

        header = "MES	ALGÚN VALOR CCH	CONTADORES CON 100% CCH	% DE CONTADORES CON 100% CCH	CONTADORES CON AL MENOS 95% CCH	% DE CONTADORES CON 95% CCH	CONTADORES CON AL MENOS 90% CCH	% DE CONTADORES CON 90% CCH	CONTADORES CON AL MENOS 80% CCH	% DE CONTADORES CON 80% CCH"
        sheet = self.xls.get_sheet(1)
        for idx, h in enumerate(header.split('\t')):
            sheet.write(0, idx, h)
        for idx in range(1, 13):
            sheet.write(idx, 0, 'M-{0}'.format(idx))

        header = "MES 	BRUTAS	VÁLIDAS	INVÁLIDAS	% VÁLIDAS/BRUTAS	% ESTIMADAS"
        sheet = self.xls.get_sheet(2)
        for idx, h in enumerate(header.split('\t')):
            sheet.write(0, idx, h)
        for idx in range(1, 13):
            sheet.write(idx, 0, 'M-{0}'.format(idx))

    def __set_data(self, sheetnum, data):
        sheet = self.xls.get_sheet(sheetnum)
        for row, month in enumerate(data):
            for col, value in enumerate(month):
                sheet.write(row + 1, col + 1, value)

    def set_situacion(self, data):
        self.__set_data(0, data)

    def set_porcentaje(self, data):
        self.__set_data(1, data)

    def set_incidencias(self, data):
        self.__set_data(2, data)
