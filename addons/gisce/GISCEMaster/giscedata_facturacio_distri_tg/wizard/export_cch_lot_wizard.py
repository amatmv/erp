# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _

from base_extended.base_extended import MultiprocessBackground
from StringIO import StringIO
from zipfile import ZipFile
from base64 import b64encode
from datetime import datetime


class GiscedataExportInvoicesCCHCurveFileWizard(osv.osv_memory):
    """Wizard to export curves from invoices"""
    _name = 'giscedata.export.fact.cch.curve.file.wizard'

    def _default_islot(self, cursor, uid, context=None):
        if not context:
            context = None

        return not context.get('factures', '1') == '1'

    def _default_info(self, cursor, uid, context=None):

        if not context:
            context = None

        if self._default_islot(cursor, uid, context):
            # lot de facturació
            lot_obj = self.pool.get('giscedata.facturacio.lot')
            lot_id = context.get('active_id')

            lot_vals = lot_obj.read(cursor, uid, lot_id, ['name'])

            txt = _(u"Exportarem un fitxer de curves de les factures obertes "
                    u"del lot de Facturació {0}").format(lot_vals['name'])
        else:
            # lot de facturació
            fact_ids = context.get('active_ids')

            txt = _(u"Exportarem un fitxer de curves de les {0} "
                    u"factures seleccionades").format(len(fact_ids))

        return txt

    def _default_choose_invoices(self, cursor, uid, context=None):
        if not context:
            context = None
        choose_invoices = context.get('choose_invoices', False)
        return choose_invoices

    def _get_curve_formats(self, cursor, uid, context=None):
        ''' Returns Available Curve types'''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        curve_types = meter_obj.get_curve_type_dict(cursor, uid, context)
        return [(k, v[2]) for k, v in curve_types.items() if v[0] == 'fact']

    def onchange_curve_type(self, cursor, uid, ids, curve_type):

        if curve_type == 'cch_fact':
            return {'value': {'curve_extension': 'csv'}}
        elif curve_type == 'cch_cons':
            return {
                'value': {
                    'upload': False,
                    'set_last_cch': False
                }
            }

    def action_curve_files(self, cursor, uid, ids, context=None):

        wiz_fields = self.fields_get(cursor, uid).keys()
        wiz_data = self.read(cursor, uid, ids[0], wiz_fields, context=context)

        ctx = context.copy()
        ctx.update({'set_last_cch': wiz_data[0]['set_last_cch']})

        try:
            if self._default_islot(cursor, uid, ctx):
                self.action_lot_curve_files(
                    cursor,
                    uid,
                    ids,
                    wiz_data[0],
                    ctx
                )
            else:
                if wiz_data[0]['choose_invoices']:
                    fact_ids = wiz_data[0]['invoices']
                    ctx.update({'active_ids': fact_ids})
                self.action_invoices_curve_files(
                    cursor,
                    uid,
                    ids,
                    wiz_data[0],
                    ctx
                )
        except osv.except_osv:
            pass


        res = {
            'state': 'done',
            'info': _(u"Els fitxers s'estan generant en 2on pla"),
            'choose_invoices': 0
        }

        self.write(cursor, uid, [ids[0]], res, context=ctx)

    @MultiprocessBackground.background(queue='low')
    def action_lot_curve_files(
            self,
            cursor,
            uid,
            ids,
            wiz_data=None,
            context=None
    ):
        """
        Create the profile file of the batch.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param wiz_data: dict with the wizard values
        :param context: dict with the context
        :return: True
        """
        # Invoice Lot
        if not context:
            context = {}

        if not wiz_data:
            return False

        cch_type = wiz_data['curve_type']
        overwrite = wiz_data['overwrite']
        upload = wiz_data['upload']

        res_config = self.pool.get('res.config')
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        modcon_obj = self.pool.get('giscedata.polissa.modcontractual')
        giscedata_facturacio_lot = self.pool.get('giscedata.facturacio.lot')
        ir_attachment = self.pool.get('ir.attachment')

        lot_id = context.get('active_id')
        today = datetime.today().strftime("%Y%m%d")
        if res_config.get(cursor, uid, 'tg_f5d_create_zip_file', '0') == '1':
            create_f5d_zip = True
        else:
            create_f5d_zip = False

        if create_f5d_zip:
            filedir = res_config.get(
                cursor, uid, 'tg_profile_temp_filedir', '/tmp/curves'
            )
            f5ds_file = '{}/F5D_{}.zip'.format(filedir, today)
            zip_f5ds_file = ZipFile(f5ds_file, 'w')

        # get comers
        comer_ids = modcon_obj.get_tg_comers(cursor, uid)

        for comer_id in comer_ids:
            # Search lot invoices
            search_vals = [
                ('lot_facturacio', '=', lot_id),
                ('state', 'in', ('open','paid')),
                ('partner_id', '=', comer_id),
                ('cch_fact_available', '=', True),
            ]
            fact_ids = fact_obj.search(
                cursor, uid, search_vals, context=context
            )

            if fact_ids:
                filename, file_content = fact_obj.get_comer_cch_fact(
                    cursor,
                    uid,
                    fact_ids,
                    comer_id,
                    cch_type=cch_type,
                    overwrite=overwrite,
                    filedir=None,
                    context=context,
                    upload=upload
                )
                if create_f5d_zip:
                    arc_filename = filename.replace('.bz2', '')
                    zip_f5ds_file.write(file_content, arcname=arc_filename)

        if create_f5d_zip:
            zip_f5ds_file.close()
            with open(f5ds_file, 'rb') as myzip:
                content = myzip.read()
            attachment_values = {
                'name': 'F5D_{0}.zip'.format(today),
                'res_id': lot_id,
                'res_model': 'giscedata.facturacio.lot',
                'datas': b64encode(content)
            }
            ir_attachment.create(
                cursor,
                uid,
                attachment_values,
                context=context
            )

        return True

    @MultiprocessBackground.background(queue='low')
    def action_invoices_curve_files(
            self,
            cursor,
            uid,
            ids,
            wiz_data=None,
            context=None
    ):
        """
        Create the profile file of the invoice.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: not used
        :param wiz_data: dict with the wizard values
        :param context: dict with the context
        :return: True
        """
        # Invoices
        if not context:
            context = {}

        if not wiz_data:
            return False

        overwrite = wiz_data['overwrite']
        cch_type = wiz_data['curve_type']
        upload = wiz_data['upload']

        fact_obj = self.pool.get('giscedata.facturacio.factura')

        fact_ids = context.get('active_ids')

        # Get selected invoices comers
        fact_vals = fact_obj.read(cursor, uid, fact_ids, ['partner_id'])
        comers_dict = {}
        for fact in fact_vals:
            comer_id = fact['partner_id'][0]
            comers_dict.setdefault(comer_id, [])
            comers_dict[comer_id].append(fact['id'])

        for comer_id in comers_dict.keys():
            fact_obj.get_comer_cch_fact(
                cursor,
                uid,
                comers_dict[comer_id],
                comer_id,
                cch_type=cch_type,
                overwrite=overwrite,
                context=context,
                upload=upload
            )

        return True

    def onchange_upload(self, cursor, uid, ids, upload):

        if upload:
            set_last_cch = True
        else:
            set_last_cch = False

        return {'value': {'set_last_cch': set_last_cch}}

    _columns = {
        'state': fields.char('Estat', size=16),
        'info': fields.text('Info'),
        'curve_type': fields.selection(_get_curve_formats, 'Format Fitxer',),
        'curve_extension': fields.selection([('csv', 'CSV'),
                                             ('xls', 'Excel (xls)')],
                                            'Extensió'),
        'file': fields.binary('Nom'),
        'name': fields.char('Nom', size=256),
        'overwrite': fields.boolean('Sobreescriu el fitxer existent',
                                    help=u"Si ja exitesix un fitxer de la "
                                         u"mateixa comercialitzadora y dia, "
                                         u"el sobreescriu, si no, crea "
                                         u"una nova versió"),
        'filepaths': fields.text('Generated Filepaths json'),
        'islot': fields.boolean('Es Lot'),
        'upload': fields.boolean(
            'Upload files to SFTP CCH server',
            help='Upload the generated files to SFTP CCH server'
        ),
        'set_last_cch': fields.boolean('Marca Última curva',
                                       help=u"Marca al CUPS la última "
                                            u"curva generada per començar "
                                            u"a partir d'aquesta en la "
                                            u"següent generació del P5D, un "
                                            u"cop generat el F5D"),
        'choose_invoices': fields.boolean('Escollir factures',
                                         help=u"Activant aquesta opció és "
                                              u"possible escollir les factures "
                                              u"d'una en una per exportar "
                                              u"només les desitjades."),
        'invoices': fields.many2many(
            'giscedata.facturacio.factura', 'wizard_export_multiple_f5d_p5d_invoices',
            'wiz_invoice_id', 'factura_id', 'FACTURES'
        ),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _default_info,
        'overwrite': lambda *a: False,
        'filepaths': lambda *a: '',
        'curve_type': lambda *a: 'cch_fact',
        'curve_extension': lambda *a: 'csv',
        'islot': _default_islot,
        'upload': lambda *a: False,
        'set_last_cch': lambda *a: False,
        'choose_invoices': _default_choose_invoices,
    }

GiscedataExportInvoicesCCHCurveFileWizard()
