# coding=utf-8
from osv import osv, fields


class WizardCreateTgReadsLot(osv.osv_memory):
    _name = 'wizard.create.tg.reads.lot'

    def _get_default_lot_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('active_id', False)

    def create_tg_reads_button(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context=context)
        ctx = context.copy()
        ctx['estimate'] = wiz.estimate
        wiz.lot_id.create_tg_reads_button(context=ctx)
        return {}

    _columns = {
        'estimate': fields.boolean('Crear estimades'),
        'lot_id': fields.many2one('giscedata.facturacio.lot', 'Lot')
    }

    _defaults = {
        'estimate': lambda *a: 1,
        'lot_id': _get_default_lot_id
    }


WizardCreateTgReadsLot()
