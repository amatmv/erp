# coding=utf-8
from __future__ import absolute_import, division
from datetime import datetime
from StringIO import StringIO
import base64
import logging
from tools.translate import _

from osv import osv, fields

from .ree_deployment import REEDeloymentTG
from base_extended.base_extended import MultiprocessBackground


logger = logging.getLogger('openerp.' + __name__)


class WizardReeType5TGDeployment(osv.osv_memory):
    _name = 'wizard.ree.type5.tg.deployment'

    _states_selection = [
        ('init', 'Init'),
        ('end', 'End'),
    ]

    _columns = {
        'year': fields.integer(_('Any'), required=True),
        'state': fields.selection(_states_selection, string='State',
                                  readonly=True),
        'info': fields.text('', readonly=True),
    }

    def _default_year(self, cursor, uid, context=None):
        """
        Returns the year of last quarter.

        :param cursor: database cursor
        :param uid: user identifier
        :param context: dictionary with the context
        :return: the year
        """
        year = datetime.now().year
        return year - 1

    def action_create_ree_type5_tg_deployment_report(
            self,
            cursor,
            uid,
            ids,
            context=None
    ):
        """
        Create REE Type5 Meter Report.

        :param cursor: database cursor
        :param uid: user identifier
        :param ids: dict with the wizard identifier
        :param context: context
        """
        wizard = self.browse(cursor, uid, ids[0], context=context)

        try:
            self.create_report(
                cursor,
                uid,
                ids,
                wizard.year,
                context=context
            )
        except osv.except_osv:
            pass

    @MultiprocessBackground.background(queue='low', skip_check=True)
    def create_report(self, cursor, uid, ids, year, context=None):
        if context is None:
            context = {}
        ir_attachment = self.pool.get('ir.attachment')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        report = REEDeloymentTG()

        report.set_situacion(meter_obj.get_situacion(
            cursor, uid, year, context=context))
        report.set_porcentaje(meter_obj.get_porcentaje(
            cursor, uid, year, context=context))
        report.set_incidencias(meter_obj.get_incidencias(
            cursor, uid, year, context=context))

        stream = StringIO()
        report.xls.save(stream)

        file_content_b64 = base64.b64encode(stream.getvalue())
        attachment_values = {
            'name': 'report_ree_tg_{}.xls'.format(
                datetime.now().strftime('%Y%m%d')),
            'res_model': self._name,
            'datas': file_content_b64
        }
        ir_attachment.create(
            cursor,
            uid,
            attachment_values,
            context=context
        )

        wizard = self.browse(cursor, uid, ids[0], context=context)
        wizard.write({'state': 'end', 'info': _(u"El procés de generació s'ha iniciat en segon pla.")})

        stream.close()

    _defaults = {
        'year': _default_year,
        'state': lambda *a: 'init',
        'info': '',
    }

WizardReeType5TGDeployment()
