# -*- coding: utf-8 -*-
from __future__ import absolute_import

import logging
import base64
import tools
import time
import calendar
import traceback
from datetime import datetime, timedelta
import sys

from addons import logger
from osv.osv import TransactionExecute
from giscedata_switching.utils import get_address_dicct
from .rebuig import Rebuig

try:
    from collections import OrderedDict, namedtuple
except:
    from ordereddict import OrderedDict

import pooler
from osv import osv, fields, orm
from tools.translate import _
from tools.translate import _ as transomatic
from tools.misc import cache
from tools import config

from lxml import etree

from gestionatr.defs import *
from gestionatr.input.messages import message, A1, C1, C2, M1, A3, B1, D1, W1, R1
from gestionatr.output.messages import sw_a1 as a1
from gestionatr.output.messages import sw_c1 as c1
from gestionatr.output.messages import sw_c2 as c2
from gestionatr.output.messages import sw_m1 as m1
from gestionatr.output.messages import sw_a3 as a3
from gestionatr.output.messages import sw_b1 as b1
from gestionatr.output.messages import sw_d1 as d1
from gestionatr.output.messages import sw_w1 as w1
from gestionatr.output.messages import sw_r1 as r1
from gestionatr.input.messages.R1 import MinimumFieldsChecker
from gestionatr.utils import validate_xml, ValidationResult
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from osv.expression import OOQuery
from ast import literal_eval
from mongodb_backend import osv_mongodb

# Versió de Switching
VERSIO = '02'
TIPUS_PERIODES = {'AE': 'te', 'R1': 'te', 'PM': 'tp', 'EP': 'tp'}

PERIODES_SEL = [('P0', 'P0'), ('P1', 'P1'), ('P2', 'P2'), ('P3', 'P3'),
                ('P4', 'P4'), ('P5', 'P5'), ('P6', 'P6')]


class SwitchingException(Exception):
    def __init__(self, value, error_fields=[]):
        self.error_fields = error_fields
        super(Exception, SwitchingException).__init__(self, value)


class GiscedataSwitchingProces(osv.osv):

    _name = 'giscedata.switching.proces'
    _desc = 'Procés de switching'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        """Get initial steps for proces"""
        return []

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        """get steps where we are emisor"""
        return []

    def get_reject_steps(self, cursor, uid, proces, context=None):
        """get steps that can be in a rejected state"""
        return []

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        """get steps that should be sent to the old company"""
        return []

    def get_proces(self, cursor, uid, name):
        search_params = [('name', '=', name)]
        return self.search(cursor, uid, search_params)[0]

    def get_steps(self, cursor, uid, proces_id, proces='', context=None):
        if context is None:
            context = {}
        if not proces_id and proces:
            search_params = [('name', '=', proces)]
            proces_id = self.search(cursor, uid, search_params)[0]
        proces = self.browse(cursor, uid, proces_id)
        res = []
        if context.get("add_description"):
            return [(step.name, step.get_step_model(), step.description)
                    for step in proces.step_ids]
        else:
            return [(step.name, step.get_step_model())
                    for step in proces.step_ids]

    _columns = {
        'name': fields.char('Proces', size=2, required=True),
    }

GiscedataSwitchingProces()


class GiscedataSwitchingStep(osv.osv):

    _name = 'giscedata.switching.step'
    _desc = 'Passos dels processos de switching'

    def get_step(self, cursor, uid, name, proces, context=None):
        search_params = [('name', '=', name),
                         ('proces_id.name', '=', proces)]
        return self.search(cursor, uid, search_params)[0]

    def get_next_steps(self, cursor, uid, step, proces, where, context=None):
        '''returns next valid step depending on process, step
        and where we are (comer or distri)'''
        step_obj = self.pool.get('giscedata.switching.step')
        model = self.get_step_model(cursor, uid, [], step, proces,
                                    context=context)
        model_obj = self.pool.get(model)
        return model_obj.get_next_steps(cursor, uid, where,
                                       context=context)

    def get_step_model(self, cursor, uid, ids,
                       name='', proces='', context=None):

        #If a step name is passed and no ids
        #search for id with that name first
        if not ids and name and proces:
            search_params = [('name', '=', name),
                             ('proces_id.name', '=', proces)]
            ids = self.search(cursor, uid, search_params)[0]
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        step = self.browse(cursor, uid, ids, context=context)
        return 'giscedata.switching.%s.%s' % (step.proces_id.name.lower(),
                                               step.name)

    def name_get_step(self, cursor, uid, pas_ids, proces, step, context=None):
        res = []
        model_obj = self.pool.get(self.get_step_model(cursor, uid, [],
                                                      name=step,
                                                      proces=proces,
                                                      context=context))
        for pas in model_obj.browse(cursor, uid, pas_ids, context=context):
            solicitud = pas.sw_id and pas.sw_id.codi_sollicitud
            res.append((pas.id,
                        '%s (%s) %s (%s -> %s)' % (proces,
                                                   step,
                                                   solicitud,
                                                   pas.emisor_id.ref,
                                                   pas.receptor_id.ref)))
        return res

    _columns = {
        'name': fields.char('Pas', size=2, required=True),
        'proces_id': fields.many2one('giscedata.switching.proces', 'Proces',
                                     required=True),
        'accio_pendent': fields.selection(
            string="Agent amb accio pendent",
            selection=[('comer', "Comercialitzadora"), ("distri", "Distribuidora")]
        ),
        'description': fields.char('Descripcio', size=64, required=False, translate=True),
        'is_rejectable': fields.boolean('És rebutjable')
    }

    _defaults = {
        'is_rejectable': lambda *a: False,
    }


GiscedataSwitchingStep()


class GiscedataSwitchingProces2(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    _columns = {
        'step_ids': fields.one2many('giscedata.switching.step', 'proces_id',
                                    'Passos'),
    }
GiscedataSwitchingProces2()


class GiscedataSwitching(osv.osv):
    """Classe per gestionar el canvi de comercialitzador
    """

    _name = "giscedata.switching"
    # s'usa OrderectDict per tal que el 'name' correspongui al de crm.case
    _inherits = OrderedDict([('poweremail.conversation', 'conversation_id'),
                             ('crm.case', 'case_id')])
    # _estats_valids = {estat futur: [possibles estats actuals]}
    _order = 'id desc'

    def create(self, cursor, uid, values, context=None):
        '''Assign a name to the case if not done yet'''
        proces_obj = self.pool.get('giscedata.switching.proces')
        cups_obj = self.pool.get('giscedata.cups.ps')

        if 'cups_id' in values and values['cups_id']:
            cups_name = cups_obj.read(
                cursor, uid, values['cups_id'], ['name']
            )['name']
            values['cups_input'] = cups_name
        else:
            cups_name = values.get('cups_input', False)

        if 'name' not in values or not values['name']:
            proces_name = proces_obj.read(cursor, uid, values['proces_id'],
                                          ['name'], context=context)['name']
            if proces_name == 'A1' and values.get('cau'):
                case_name = _(u"Procés %s pel CAU: %s") % (proces_name, values['cau'])
            else:
                case_name = _(u"Procés %s pel CUPS: %s") % (proces_name, cups_name)
            values['name'] = case_name

        return super(GiscedataSwitching, self).create(cursor, uid, values,
                                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        """S'elimina el cas i tots els passos rel·lacionats
        """
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        case_obj = self.pool.get('crm.case')
        conv_obj = self.pool.get('poweremail.conversation')
        step_o = self.pool.get("giscedata.switching.step.info")

        for sw in self.read(cursor, uid, ids, ['case_id', 'conversation_id', 'step_ids', 'state'], context=context):
            try:
                for step in sw['step_ids']:
                    step_o.unlink(cursor, uid, step, context=context)
                self.write(cursor, uid, sw['id'], {'case_id': False, 'conversation_id': False})
                case_obj.unlink(cursor, uid, [sw['case_id'][0]], context=context)
                conv_obj.unlink(cursor, uid, [sw['conversation_id'][0]], context=context)
            except KeyError, e:
                ids.remove(sw['id'])
        super(GiscedataSwitching, self).unlink(cursor, uid,
                                               ids, context=context)

    def copy(self, cursor, uid, sw_id, default=None, context=None):
        """Quan es copia un cas, se li ha d'assignar una seqüència nova"""
        # actualitzem el codi i data de sol·licitud
        data_sol = time.strftime('%Y-%m-%d'),
        codi_sol = self._get_default_codi_sollicitud(cursor, uid, context)
        vals = {'codi_sollicitud': codi_sol, 'data_sollicitud': data_sol,
                'step_ids': None}

        res_id = super(GiscedataSwitching, self).copy(cursor, uid, sw_id, vals,
                                                      context)
        # hem de duplicar els pasos, perquè sinó fa una referència als del cas
        # original i no els duplica
        sw = self.browse(cursor, uid, sw_id, context)
        swi_obj = self.pool.get('giscedata.switching.step.info')
        vals = {'sw_id': res_id}
        for info in sw.step_ids:
            pas_model = self.pool.get(info.pas_id.split(',')[0])
            pas_id = info.pas_id.split(',')[1]
            nou_pas_id = pas_model.copy(cursor, uid, pas_id, vals, context)
            pas_val = '%s,%s' % (info.pas_id.split(',')[0], nou_pas_id)
            vals = {'sw_id': res_id, 'pas_id': pas_val}
            info_id = swi_obj.copy(cursor, uid, info.id, vals, context)
        return res_id

    def get_pas_model_name(
            self, cursor, uid, sw_id, step_id=False, context=None):
        """
        Return PAS Model Name of a SW Case on `step_id`
        :param cursor:  OpenERP Cursor
        :param uid:     Current User ID
        :type uid:      int
        :param sw_id:   SW Case ID
        :type sw_id:    long
        :param step_id:   SW Step ID
        :type step_id:    long
        :param context: OpenERP context
        :type context:  dict
        :return:
        """
        sw_obj = self.pool.get('giscedata.switching')
        step_obj = self.pool.get('giscedata.switching.step')
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        if not step_id:
            step_id = sw_obj.read(
                cursor, uid, sw_id, ['step_id'], context=context)['step_id']
            if not step_id:
                return False
            step_id = step_id[0]
        return step_obj.get_step_model(cursor, uid, step_id, context=context)

    def get_pas_id(self, cursor, uid, sw_id, step_id=False, context=None):
        """
        Return current PAS ID of a SW Case
        :param cursor:  OpenERP Cursor
        :param uid:     Current User ID
        :type uid:      int
        :param sw_id:   SW Case ID
        :type sw_id:    long
        :param step_id: SW Step ID
        :type step_id:  long
        :param context: OpenERP context
        :type context:  dict
        :return:
        """
        if context is None:
            context = {}
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        step_model = self.get_pas_model_name(
            cursor, uid, sw_id, step_id=step_id, context=context)
        pas_obj = self.pool.get(step_model)
        return pas_obj.search(cursor, uid, [('sw_id', '=', sw_id)], order='id desc')[0]

    def get_pas(self, cursor, uid, sw, step_id=False, context=None):
        """
        Return a Browse Record of the current PAS of a SW Case
        :param cursor:  OpenERP Cursor
        :param uid:     Current User ID
        :param sw:      Browse record of a SW Case (or ID list/tuple)
        :type sw:       browse_record, [int]
        :param step_id: ID of current SW Step
        :type step_id:  long
        :param context: OpenERP Context
        :type context:  dict
        :return:
        """
        if context is None:
            context = {}
        if isinstance(sw, (list, tuple)):
            sw = self.browse(cursor, uid, sw[0], context=context)
        pas_obj = self.pool.get(
            sw.get_pas_model_name(step_id, context=context))
        if not pas_obj:
            return False
        return pas_obj.browse(
            cursor, uid,
            sw.get_pas_id(step_id, context=context), context=context
        )

    def get_destinatari_actual(self, cursor, uid, sw_id, step_id=False,
                               context=None):
        """Retorna el id del receptor del pas actual o el pas escollit
        (step_id)"""
        if not context:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')
        step_obj = self.pool.get('giscedata.switching.step')
        sw = sw_obj.browse(cursor, uid, sw_id)
        if not step_id:
            if not sw.step_id:
                raise osv.except_osv('Error', _(u"El cas no te cap pas"))
            step_id = sw.step_id.id

        step_model = step_obj.get_step_model(cursor, uid, step_id)
        pas_obj = self.pool.get(step_model)
        pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
        pas = pas_obj.browse(cursor, uid, pas_id[0])

        return pas.receptor_id.id

    def get_passos_pendents_enviament(self, cursor, uid, sw_id, context=None):
        step_ids = []
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        proces_obj = self.pool.get('giscedata.switching.proces')
        step_info_obj = self.pool.get('giscedata.switching.step.info')

        hids = header_obj.search(
            cursor, uid, [('sw_id', '=', sw_id), ('enviament_pendent', '=', True)]
        )

        sw_br = sw_obj.read(cursor, uid, sw_id, ['whereiam', 'proces_id'])
        proces_name = sw_br['proces_id'][1]
        whereiam = sw_br['whereiam']
        emisorsteps = proces_obj.get_emisor_steps(
            cursor, uid, proces_name, whereiam
        )
        if not len(emisorsteps):
            return step_ids
        q = OOQuery(step_info_obj, cursor, uid)
        sql = q.select(['pas_id', 'step_id', 'step_id.name']).where([
            ('step_id.name', 'in', emisorsteps),
            ('sw_id', '=', sw_id)
        ])
        cursor.execute(*sql)
        # mirar per cada header amb quins passos del procés encaixa
        # per obtenir el step_id
        for info in cursor.dictfetchall():
            step_model = info.get('pas_id', ",").split(',')[0]
            if step_model in ["", "giscedata.switching.c1.07", "giscedata.switching.c2.07"]:
                continue
            mdl = self.pool.get(step_model)
            for hid in hids:
                encaixa = mdl.search(cursor, uid, [('header_id', '=', hid)])
                if encaixa and info['step_id.name'] in emisorsteps:
                    step_ids.append(info['step_id'])
        return step_ids

    def get_passos_enviables(self, cursor, uid, sw_id, context=None):
        step_ids = []
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        proces_obj = self.pool.get('giscedata.switching.proces')
        step_info_obj = self.pool.get('giscedata.switching.step.info')

        hids = header_obj.search(
            cursor, uid, [('sw_id', '=', sw_id)]
        )

        sw_br = sw_obj.read(cursor, uid, sw_id, ['whereiam', 'proces_id'])
        proces_name = sw_br['proces_id'][1]
        whereiam = sw_br['whereiam']
        emisorsteps = proces_obj.get_emisor_steps(
            cursor, uid, proces_name, whereiam
        )
        if not len(emisorsteps):
            return step_ids
        q = OOQuery(step_info_obj, cursor, uid)
        sql = q.select(['pas_id', 'step_id', 'step_id.name']).where([
            ('step_id.name', 'in', emisorsteps),
            ('sw_id', '=', sw_id)
        ])
        cursor.execute(*sql)
        # mirar per cada header amb quins passos del procés encaixa
        # per obtenir el step_id
        for info in cursor.dictfetchall():
            step_model = info.get('pas_id', ",").split(',')[0]
            if step_model in ["", "giscedata.switching.c1.07", "giscedata.switching.c2.07"]:
                continue
            mdl = self.pool.get(step_model)
            for hid in hids:
                encaixa = mdl.search(cursor, uid, [('header_id', '=', hid)])
                if encaixa and info['step_id.name'] in emisorsteps:
                    step_ids.append(info['step_id'])
        return step_ids

    def _get_crm_base_ids(self, cursor, uid, ids, context=None):
        """Definim una funció on li passarem els ids del model i ens
           retornarà els ids dels crm rel·lacionats
        """
        return [a['case_id'][0] for a in self.read(cursor, uid, ids,
                                                      ['case_id'], context)]

    def _get_conversation_base_ids(self, cursor, uid, ids, context=None):
        """Definim una funció on li passarem els ids del model i ens
           retornarà els ids dels conversations rel·lacionats
        """
        return [a['conversation_id'][0]
                    for a in self.read(cursor, uid, ids,
                                       ['conversation_id'], context)]

    def _get_crm_to_sw(self, cursor, uid, ids, context=None):
        """Retorna un diccionari en que les claus són els ids de crm i els
        valors una tupla d'ids de switching, conversation_id i pas."""

        res = {}
        for a in self.read(cursor, uid, ids, ['case_id', 'conversation_id',
                                              'pas_comm'], context):
            res.update({a['case_id'][0]: (a['id'], a['conversation_id'][0],
                                          a['pas_comm'])})
        return res

    def _get_proces_model(self, cr, uid, ids, context=None):
        '''retorna el model de l'objecte corresponent al tipus de proces'''
        if not context:
            context = {}
        #If create proces in model, return value for this key in context
        #The value of the key will be the code of the proces
        if context.get('proces', False):
            proces = context.get('proces', False)
            model_name = 'giscedata.switching.%s' % proces.lower()
            model_obj = self.pool.get(model_name)
            return model_obj
        res = {}
        for sw in self.browse(cr, uid, ids, context=context):
            model_name = 'giscedata.switching.%s' % sw.proces.lower()
            model_obj = self.pool.get(model_name)
            res[sw.id] = model_obj
        return res

    def historize_msg(self, cursor, uid, ids, msg, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cursor, uid, ids)
        return crm_obj.historize_msg(cursor, uid, crm_ids, msg, context=context)

    def case_close(self, cr, uid, ids, *args):
        '''close the case'''
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        crm_obj = self.pool.get('crm.case')

        context = args and args[0] or {}
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_close(cr, uid, crm_ids, *args)

    def case_pending(self, cr, uid, ids, *args):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_pending(cr, uid, crm_ids, *args)

    def case_log(self, cr, uid, ids, context=None, email=False, *args):
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_log(cr, uid, crm_ids, context=context,
                                email=email, *args)

    def case_reset(self, cr, uid, ids, *args):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cr, uid, ids)
        return crm_obj.case_reset(cr, uid, crm_ids, *args)

    def case_open(self, cr, uid, ids, *args):
        crm_obj = self.pool.get('crm.case')

        context = args and args[0] or {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for sw in self.browse(cr, uid, ids, context=context):
            #If force_open in context, only open the case
            if context.get('force_open', False):
                sw.case_id.case_open()
                continue
            if sw.state in ['draft', 'pending']:
                sw.case_id.case_open()
        return True

    def localitzar_adjunt(self, cr, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, list):
            ids = ids[0]
        sw = self.browse(cr, uid, ids, context=context)
        attach_obj = self.pool.get('ir.attachment')
        search_params = [('res_id', '=', ids),
                         ('res_model', '=', 'giscedata.switching')]
        att_ids = attach_obj.search(cr, uid, search_params)
        #Referenciem la classe que correspon al proces
        #per fer el parse de l'XML
        xml_class = globals()[sw.proces_id.name]
        val = None
        for att_id in att_ids:
            att_data = attach_obj.read(cr, uid, att_id, ['datas'])
            xml = xml_class(base64.decodestring(att_data['datas']), sw.proces)
            xml.parse_xml()
            if xml.get_pas_xml() == sw.pas and \
                        xml.codi_sollicitud == sw.codi_sollicitud:
                val = i
                break
        return val

    def poweremail_write_callback(self, cursor, uid, ids, vals, context=None):
        logger = logging.getLogger(
            'openerp.{0}.poweremail_write_callback'.format(__name__)
        )
        logger.info(
            'PE callback for mark as send ATR cases {0} and from user {1}'.format(
                ids, uid)
        )
        if context is None:
            context = {}
        meta = context.get('meta')
        attach_obj = self.pool.get('ir.attachment')
        mail_obj = self.pool.get('poweremail.mailbox')
        for sw_id in ids:
            if not meta:
                meta = {}
            model = meta[sw_id].get('model', False)
            pas_id = meta[sw_id].get('pas_id', False)
            store_attachment = meta[sw_id].get('store_attachment', True)
            mark_step_send = meta[sw_id].get('mark_step_send', True)
            if 'folder' in vals and 'date_mail' in vals:
                if vals.get('folder', False) == 'sent':
                    # marquem com a enviat el pas
                    if mark_step_send:
                        pas_obj = self.pool.get(model)
                        pas = pas_obj.browse(cursor, uid, pas_id)
                        pas.header_id.set_enviament_pendent(False)
                    if not store_attachment:
                        mail_id = context.get('pe_callback_origin_ids')
                        if mail_id:
                            mail_id = mail_id[sw_id]
                            attch_ids = mail_obj.read(
                                cursor, uid, mail_id, ['pem_attachments_ids']
                            )
                            attach_obj.unlink(
                                cursor, uid, attch_ids['pem_attachments_ids'])
                            vals.update({'pem_attachments_ids': []})
                else:
                    Exception("Error", _(u"El missatge s'ha creat correctament "
                                       u"però no s'ha pogut enviar. Revisi els"
                                       u"errors a la pestanya de "
                                       u"Comunicacions"))

        return True

    def case_log_reply(self, cursor, uid, ids, context=None,
                       email=False, *args, **kwargs):
        if not context:
            context = {}

        acc_obj = self.pool.get('poweremail.core_accounts')
        attach_obj = self.pool.get('ir.attachment')
        mail_obj = self.pool.get('poweremail.mailbox')
        crm_obj = self.pool.get('crm.case')
        crm_ids = self._get_crm_base_ids(cursor, uid, ids)
        conv_ids = self._get_conversation_base_ids(cursor, uid, ids)
        cases = crm_obj.browse(cursor, uid, crm_ids)
        email_cc = kwargs.get('email_cc', False)
        email_cco = kwargs.get('email_cco', False)
        #Check for some values
        for sw in self.browse(cursor, uid, ids, context=context):
            if not sw.email_from:
                raise osv.except_osv(_('Error!'),
                        _(u"You must put a Partner eMail to use this action!"))
            if not sw.user_id:
                raise osv.except_osv(_('Error!'),
                        _(u"You must define a responsible user "
                          u"for this case in order to use this action!"))
            if not sw.description:
                raise osv.except_osv(_('Error!'),
                        _(u"Can not send mail with empty body,you "
                          u"should have description in the body"))
            #Historize send action
            crm_obj._history(cursor, uid, [sw.case_id], _('Send'),
                             history=True, email=False)
            #Send using poweremail
            crm_vals = {'description': False,
                        'som': False,
                        'canal_id': False,
                       }
            sw.write(crm_vals)
            if email:
                emails = email.split(',')
            else:
                emails = [sw.email_from] + (sw.email_cc or '').split(',')
            emails = filter(None, emails)
            body = sw.description or ''
            if sw.user_id.signature:
                body += '\n\n%s' % (sw.user_id.signature)
            xml = self.exportar_xml(cursor, uid, sw.id,
                                    step_id=kwargs.get('step_id', False),
                                    context=context)
            vals = {
                'name': xml[0],
                'datas': base64.b64encode(xml[1]),
                'res_model': 'giscedata.switching',
                'res_id': sw.id
            }
            att = attach_obj.create(cursor, uid, vals, context)
            if context.get('from_account', False):
                acc_id = context.get('from_account', False)
            else:
                acc_id = acc_obj.search(cursor, uid, [('user', '=', uid),
                            ('state', '=', 'approved')], context=context)
                if not acc_id:
                    err = _(u"ERROR: No existeix compte de correu d'enviament")
                    raise orm.except_orm('Error', err)
                if isinstance(acc_id, (list, tuple)):
                    acc_id = acc_id[0]
            emailfrom = acc_obj.read(cursor, uid,
                                     acc_id, ['email_id'])['email_id']
            vals = {
                'conversation_id': sw.conversation_id.id,
                'pem_from': emailfrom,
                'pem_to': ','.join(emails),
                'pem_subject': '[' + str(sw.id) + '] ' + sw.name,
                'pem_body_text': crm_obj.format_body(body),
                'pem_attachments_ids': [(4, att)],
                'pem_account_id': acc_id,
                'pem_cc': email_cc,
                'pem_bcc': email_cco,

            }
            # get step info and save to
            pas_obj = self.pool.get(sw.step_id.get_step_model(cursor, uid))
            pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
            meta_ctx = context.get('meta', {})
            meta_ctx.update({
                'model': sw.step_id.get_step_model(cursor, uid),
                'pas_id': pas_id[0]
            })
            context.update({
                'meta': meta_ctx,
                'src_rec_id': sw.id,
                'src_model': 'giscedata.switching'
            })
            mail_id = mail_obj.create(cursor, uid, vals, context)
            mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

    def send_email(
            self, cursor, uid, email_values, email_from=False, context=None):
        if not context:
            context = {}
        acc_obj = self.pool.get('poweremail.core_accounts')
        mail_obj = self.pool.get('poweremail.mailbox')
        if not email_values.get('pem_account_id', False):
            if context.get('from_account', False):
                acc_id = context.get('from_account', False)
            else:
                search_values = [('user', '=', uid), ('state', '=', 'approved')]
                if email_from:
                    search_values.append(('email_id', '=', email_from))
                acc_id = acc_obj.search(
                    cursor, uid, search_values,context=context)
                if not acc_id:
                    err = _(u"ERROR: No existeix compte de correu d'enviament")
                    raise orm.except_orm('Error', err)
                if isinstance(acc_id, (list, tuple)):
                    acc_id = acc_id[0]
            email_values.update({'pem_account_id': acc_id})
        else:
            acc_id = email_values.get('pem_account_id', False)

        emailfrom = acc_obj.read(
            cursor, uid, acc_id, ['email_id'])['email_id']
        email_values.update({'pem_from': emailfrom})
        # Using new cursor to persist mail before use
        db = pooler.get_db_only(cursor.dbname)
        pbcr = db.cursor()
        mail_id = mail_obj.create(pbcr, uid, email_values, context)
        pbcr.commit()
        pbcr.close()
        mail_obj.send_this_mail(cursor, uid, [mail_id], context)

    def check_case_email(self, cursor, uid, switching_case, context=None):
        if not context:
            context = {}
        if isinstance(switching_case, int):
            sw = self.pool.get('giscedata.switching').browse(
                cursor, uid, switching_case)
        else:
            sw = switching_case
        if not sw.email_from:
            raise osv.except_osv(_('Error!'),
                    _(u"You must put a Partner eMail to use this action!"))
        if not sw.user_id:
            raise osv.except_osv(_('Error!'),
                    _(u"You must define a responsible user "
                      u"for this case in order to use this action!"))
        if not sw.description:
            raise osv.except_osv(_('Error!'),
                    _(u"Can not send mail with empty body,you "
                      u"should have description in the body"))

    def send_responsible_and_historize(self, cursor, uid, ids, context=None):
        logger = logging.getLogger(
            'openerp.{0}.send_responsible_and_historize'.format(__name__)
        )
        logger.info(
            'Send responsible message for cases {0} and from user {1}'.format(
                ids, uid)
        )
        conf_obj = self.pool.get('res.config')
        crm_obj = self.pool.get('crm.case')
        emails_cc = conf_obj.get(
            cursor, uid, 'sw_responsible_email_cc', None)
        email_from_user = conf_obj.get(
            cursor, uid, 'sw_responsible_email_from_user', None)
        if not email_from_user:
            user_obj = self.pool.get('res.users')
            user = user_obj.browse(cursor, uid, uid)
            email_from_user = user.address_id.email

        for sw in self.browse(cursor, uid, ids, context=context):
            self.check_case_email(cursor, uid, sw)
            if not sw.user_id:
                raise osv.except_osv(_('Error!'),
                        _(u"Cal definir un responsable "
                          u"per aquest cas per oder usar aquesta acció"))
            email = sw.user_id.address_id.email
            if not email:
                raise osv.except_osv(
                    _('Error!'),
                    _(u"Cal que el responsable tingui "
                      u"un mail per usar aquesta acció")
                )
            if email:
                context['meta'] = {'mark_step_send': False,
                                   'store_attachment': False}
                # get step info and save to
                pas_obj = self.pool.get(sw.step_id.get_step_model(cursor, uid))
                pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
                meta_ctx = {
                    'model': sw.step_id.get_step_model(cursor, uid),
                    'pas_id': pas_id[0]
                }

                context.update({
                    'meta': meta_ctx,
                    'src_rec_id': sw.id,
                    'src_model': 'giscedata.switching'
                })
                body = sw.description or ''
                if sw.user_id.signature:
                    body += '\n\n{0}'.format(sw.user_id.signature)
                subject = _(u'[{0}] {1} - Contracte: {2}').format(
                    sw.id, sw.name, sw.cups_polissa_id.name)
                vals = {
                    'conversation_id': sw.conversation_id.id,
                    'pem_to': email,
                    'pem_subject': subject,
                    'pem_body_text': crm_obj.format_body(body)
                }
                if emails_cc:
                    vals.update({'pem_cc': emails_cc})
                self.send_email(
                    cursor, uid, vals, email_from_user, context=context)
                #Historize send action
                crm_obj._history(cursor, uid, [sw.case_id], _('Send'),
                             history=True, email=False)
                sw.write({'description': False})

    def get_idioma(self, cursor, uid, ids, context=None):
        """
        Obtains the switching language. Used to determine the language of the
        report <info_cas_ATR>.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <giscedata.switching> ids
        :type ids: list
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the model id and the value
        is the language (xx_XX format).
        :rtype: dict[long, str]
        """
        res = {}
        language = tools.config.get('default_language', 'es_ES')

        for sw_id in ids:
            res[sw_id] = language

        return res

    def partner_map_id(self, cr, uid, cups_id, partner_id, context=None):
        """ Per cridar-lo des de OOOP"""
        if not cups_id:
            return False
        cups = self.pool.get('giscedata.cups.ps').browse(cr, uid, cups_id,
                                                         context=context)
        return self.partner_map(cr, uid, cups, partner_id, context=context)

    def partner_map(self, cr, uid, cups, partner_id, context=None):
        """ Hi ha empreses que han canviat la codificació dels CUPS de forma
            que el codi REE que s'ha d'enviar a l'XML no coincideix amb el que
            surt al CUPS. Aquesta funció s'encarrega de la traducció"""
        if not context:
            context = {}
        if not partner_id:
            return False
        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cr, uid, partner_id)
        # Només ENDESA i FENOSA
        if partner.ref not in ['0031', '0390']:
            return partner_id
        cups_ref = cups.name[2:8]
        partner_ref = False
        if cups_ref[:5] == '00314':
            provincia = cups.id_provincia
            if (provincia.code in provincies_arago):
                partner_ref = '0029'
            else:
                if cups_ref[:6] == '003144':
                    #hidroelèctrica de l'empordà
                    partner_ref = '0396'
                else:
                    partner_ref = '0024'

        if cups_ref[:5] in conv_dict_5:
            partner_ref = conv_dict_5[cups_ref[:5]]

        if cups_ref[:6] in conv_dict_6:
            partner_ref = conv_dict_6[cups_ref[:6]]

        if partner_ref:
            search_params = [('ref', '=', partner_ref)]
            partner_ref_id = partner_obj.search(cr, uid, search_params)
            if len(partner_ref_id) == 1:
                # The most probable case first
                return partner_ref_id[0]
            elif len(partner_ref_id) > 1:
                raise osv.except_osv(
                    'Error',
                    _(u"S'han trobat {0} distribuidores pel codi {1}").format(
                        len(partner_ref_id), partner_ref
                    )
                )
            else:
                raise osv.except_osv(
                    'Error',
                    _(u"No s'ha trobat cap distribuidora pel codi {0}").format(
                        partner_ref
                    )
                )

        return partner_id

    def partner_parent(self, cr, uid, partner_id, context=None):
        '''En el cas d'empreses amb diferents codis, pero amb
        una mateixa matriu (Endesa), afegim el parent per comparar'''

        if not partner_id:
            return []
        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cr, uid, partner_id)
        if partner.parent_id:
            return [partner.id, partner.parent_id.id]
        else:
            return [partner_id]

    def onchange_partner_id(self, cr, uid, ids, partner_id, email=False):
        address_obj = self.pool.get('res.partner.address')
        partner_obj = self.pool.get('res.partner')
        if not partner_id:
            data = {}
            data_dom = {}
            return {'value': data, 'domain': data_dom}
        addr = self.pool.get('res.partner').get_partner_address(cr, uid,
                                                                partner_id)
        data_val = {'partner_address_id': addr}
        if addr and not email:
            data_val['email_from'] = address_obj.browse(cr, uid, addr).email
        val = partner_obj.read(cr, uid, partner_id, ['ref'])
        if not val['ref']:
            raise osv.except_osv(_(u"Error"),
                                 _(u"Cal configurar el codi REE"
                                   u" de l'empresa destí"))
        data_dom = {'cups_polissa_id': "[('distribuidora', '=', partner_id)]",
                    'cups_id': "[('distribuidora_id', '=', partner_id)]"}
        return {'value': data_val, 'domain': data_dom}

    def onchange_categ_id(self, cr, uid, ids, categ, context=None):

        categ_obj = self.pool.get('crm.case.categ')
        if not context:
            context = {}
        if not categ:
            return {'value': {}}
        cat = categ_obj.browse(cr, uid, categ, context).probability
        return {'value': {'probability': cat}}

    def onchange_partner_address_id(self, cr, uid, ids,
                                    address_id, email=False):

        address_obj = self.pool.get('res.partner.address')

        if not address_id:
            return {'value': {}}
        data = {}
        if not email:
            data['email_from'] = address_obj.browse(cr, uid, address_id).email

        return {'value': data}

    def onchange_cups_id(self, cursor, uid, ids, cups_id,
                         partner_id, proces_id=None, context=None):

        cups_obj = self.pool.get('giscedata.cups.ps')
        polissa_obj = self.pool.get('giscedata.polissa')
        proces_obj = self.pool.get('giscedata.switching.proces')

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not context:
            context = {}
        if not cups_id:
            res['value'] = {'cups_id': False,
                            'cups_input': '',
                            'cups_polissa_id': False,
                            'ref_contracte': False}
            return res
        if proces_id:
            proces_data = proces_obj.read(cursor, uid, proces_id, ['name'])
            if proces_data:
                context.update({'proces_name': proces_data['name']})
        try:
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
            self.check_si_processant_cups(cursor, uid, cups_id, cups_name,
                                          ids=ids, context=context)
        except osv.except_osv, e:
            res['warning'] = {'title': e.name, 'message': e.value}
            res['value'] = {'cups_id': False,
                            'cups_input': '',
                            'cups_polissa_id': False,
                            'ref_contracte': False}
            return res

        cups = cups_obj.browse(cursor, uid, cups_id)
        polissa_id = self.trobar_polissa_w_cups(cursor, uid, cups_id,
                                                context=context)
        data = {'cups_input': cups.name}
        if polissa_id:
            data.update({'cups_polissa_id': polissa_id})
            ret_values = self.onchange_polissa_id(cursor, uid, ids,
                                                 polissa_id,
                                                 partner_id)
            if ret_values.get('warning', {}):
                res['warning'] = ret_values['warning']
                return res
            if ret_values.get('value', {}):
                data.update(ret_values['value'])
            #If cups_id is returned, remove from data
            if 'cups_id' in data:
                del data['cups_id']
            if not partner_id:
                partner_id = 'partner_id' in data and data['partner_id']
        where = self.whereiam(cursor, uid, cups_id, context=context)
        if not partner_id:
            if where == 'comer':
                partner_id = self.partner_map(cursor, uid, cups,
                                              cups.distribuidora_id.id)
            elif where == 'distri' and polissa_id:
                polissa = polissa_obj.browse(cursor, uid, polissa_id)
                partner_id = polissa.comercialitzadora.id
            if partner_id:
                data.update({'partner_id': partner_id})
                data.update(self.onchange_partner_id(cursor, uid, ids,
                                                     partner_id)['value'])
        partner_ids = self.partner_parent(cursor, uid, partner_id)
        #If in comer, check that cups belongs to partner_id in case
        if where == 'comer' and cups.distribuidora_id.id not in partner_ids:
            _msg = _(u"El CUPS no pertany a l'empresa "
                     u"distribuidora escollida")
            res['warning'] = {'title': _(u"Avís"), 'message': _msg}
            res['value'] = {'cups_id': False, 'cups_input': ''}
            return res

        res.update({'value': data})
        return res

    def onchange_polissa_id(self, cursor, uid, ids, polissa_id, partner_id,
                            proces_id=None, context=None):
        if context is None:
            context = {}
        polissa_obj = self.pool.get('giscedata.polissa')
        proces_obj = self.pool.get('giscedata.switching.proces')

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not polissa_id:
            return res
        if proces_id:
            proces_data = proces_obj.read(cursor, uid, proces_id, ['name'])
            if proces_data:
                context.update({'proces_name': proces_data['name']})

        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        where = self.whereiam(cursor, uid, polissa.cups.id, context=context)
        try:
            if where == 'comer':
                self.check_partner_nif(cursor, uid, polissa.titular.id)
            self.check_si_processant_cups(cursor, uid, polissa.cups.id,
                                          polissa.cups.name, ids=ids,
                                          context=context)
        except osv.except_osv, e:
            res['warning'] = {'title': e.name, 'message': e.value}
            res['value'] = {'cups_polissa_id': False}
            return res
        data = {'cups_id': polissa.cups.id}
        if where == 'comer':
            data.update({'ref_contracte': polissa.ref_dist or ''})
        elif where == 'distri':
            data.update({'ref_contracte': polissa.name})

        if not partner_id:
            if where == 'comer':
                partner_id = self.partner_map(cursor, uid, polissa.cups,
                                              polissa.distribuidora.id)
            elif where == 'distri':
                partner_id = polissa.comercialitzadora.id
            data.update({'partner_id': partner_id})
            data.update(self.onchange_partner_id(cursor, uid, ids,
                                                 partner_id)['value'])
        partner_ids = self.partner_parent(cursor, uid, partner_id)
        if where == 'comer' and polissa.distribuidora.id not in partner_ids:
            _msg = _(u"El CUPS de la pòlissa no pertany a l'empresa "\
                     u"distribuidora escollida")
            res['warning'] = {'title': _(u"Avís"), 'message': _msg}
            res['value'] = {'cups_polissa_id': False,
                            'cups_id': False,
                            'ref_contracte': False}
            return res
        if where == 'distri' and polissa.comercialitzadora.id != partner_id:
            _msg = _(u"La pòlissa no pertany a l'empresa "\
                     u"comercialitzadora escollida")
            res['warning'] = {'title': _(u"Avís"), 'message': _msg}
            res['value'] = {'cups_polissa_id': False,
                            'cups_id': False,
                            'ref_contracte': False}
            return res

        res.update({'value': data})
        return res

    def generar_nom_xml(self, cr, uid, ids, emisor, receptor,
                        pas=False, context=None):
        """Retorna el nom per l'xml"""
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        sw = self.browse(cr, uid, ids)
        if not pas:
            pas = sw.step_id.name
        date_sw = datetime.strptime(sw.data_sollicitud,
                                    '%Y-%m-%d').strftime('%Y%m')
        emi_obj = self.pool.get('res.partner')
        ctx = context.copy()
        ctx.update({'active_test': False})
        id_partner = emi_obj.search(cr, uid, [('ref', '=', receptor)],
                                    context=ctx)[0]
        if id_partner:
            emi = emi_obj.browse(cr, uid, id_partner)
            receptor = emi.comercial or emi.ref

        return u'%s_%s_%s_%s_%s_%s_%s.xml' % (sw.proces_id.name,
                                           pas,
                                           emisor,
                                           receptor,
                                           date_sw,
                                           sw.codi_sollicitud,
                                           sw.cups_id.name)

    def trobar_polissa_w_cups(self, cr, uid, cups_id, context=None):
        """Retorna la pòlissa activa associada al cups"""

        if not context:
            context = {}
        if not cups_id:
            return None
        polissa_obj = self.pool.get('giscedata.polissa')
        search_params = [
            ('cups', '=', cups_id),
            ('state', 'not in', CONTRACT_IGNORED_STATES + ['baixa'])
        ]
        polissa_id = polissa_obj.search(cr, uid, search_params,
                                        context=context)
        #If not found, search for polissa in esborrany or validar state
        if not polissa_id:
            search_params = [('cups', '=', cups_id),
                             ('state', 'in', CONTRACT_IGNORED_STATES)]
            polissa_id = polissa_obj.search(cr, uid, search_params,
                                            context=context)
            if not polissa_id and context.get('search_non_active', False):
                search_params = [('cups', '=', cups_id),
                                 ('state', 'in', ['baixa'])]
                context['active_test'] = False
                polissa_id = polissa_obj.search(cr, uid, search_params,
                                                context=context)
        if polissa_id:
            return polissa_id[0]
        else:
            return None

    def trobar_comer_sortint_w_polissa(self, cursor, uid, contract_id,
                                       context=None):
        """Returns current Retailer partner from contract"""
        if not context:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')

        pol_fields = ['comercialitzadora', 'cups']
        pol_vals = polissa_obj.read(cursor, uid, contract_id, pol_fields,
                                    context=context)
        if not pol_vals:
            return False
        cups_id = pol_vals.get('cups', (False, ))[0]
        if not cups_id:
            return False
        where = self.whereiam(cursor, uid, cups_id, context)
        if where != 'distri':
            return False

        comercialitzadora = pol_vals.get('comercialitzadora', (False, ))
        if isinstance(comercialitzadora, (list, tuple)):
            return comercialitzadora[0]
        else:
            return comercialitzadora

    def vat_enterprise(self, cursor, uid, sw_id, context=None):
        '''checks if we have an enterprise or
        a person depending on vat value'''
        vat = self.browse(cursor, uid, sw_id[0]).cups_polissa_id.titular.vat
        return self.pool.get('res.partner').vat_es_empresa(cursor, uid, vat)

    def sw_vat_type(self, cursor, uid, vat, distri_ref, context=None):
        """Some DSO's needs diferent vat type (TipoDocumento) depending on
           DSO's according to CNMC TABLA_6 codification.
           STATIC FUNCTION
           Not for multiple id's"""

        partner_obj = self.pool.get('res.partner')
        if vat[:2] == 'PS':
            return 'PS'

        is_enterprise = partner_obj.vat_es_empresa(cursor, uid, vat)

        if not is_enterprise:
            is_nie = partner_obj.vat_es_nie(cursor, uid, vat)
            if is_nie:
                res = 'NE'
            else:
                res = 'NI'
        else:
            res = 'NI'
        return res

    def get_vat_info(self, cursor, uid, vat, distri_ref):
        """
        :param vat:
        :param distri_ref:
        :return: dict {
            'vat': Vat without prefix,
            'document_type': As defined in CNMC doc
            'is_enterprise': Wether is a bussines (True)
                             or a personal VAT (False)
        """

        document_type = self.sw_vat_type(
            cursor, uid, vat, distri_ref
        )
        partner_obj = self.pool.get('res.partner')
        is_enterprise = partner_obj.vat_es_empresa(cursor, uid, vat)
        if vat[:2] in ['ES', 'PS']:
            vat = vat[2:]

        return {
            'vat': vat,
            'document_type': document_type,
            'is_enterprise': is_enterprise,
        }

    def check_partner_nif(self, cr, uid, ids, context=None):
        '''checks if partner in ids has a vat number'''
        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        partner_obj = self.pool.get('res.partner')
        partner = partner_obj.browse(cr, uid, ids)
        if not partner.vat:
            err = _(u'El partner %s especificat no té nif') % partner.name
            raise osv.except_osv('Error', err)

        return True

    def check_si_processant_cups(self, cursor, uid, cups_id, cups_name,
                                 ids=None, context=None):
        """Comprova si ja hi ha un cas en estat obert amb el mateix cups,
           excepte dels processos que poden ser simultanis (D1, W1 i R1)"

        """
        if not context:
            context = {}
        if not ids:
            ids = []
        process_exceptions = ['D1', 'W1', 'R1', 'B1']
        proces_name = context.get('proces_name')
        if proces_name in process_exceptions:
            return True

        if cups_id:
            cups_obj = self.pool.get('giscedata.cups.ps')
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']

        search_params = [('cups_input', '=', cups_name),
                         ('state', 'not in', ('done', 'cancel')),
                         ('proces_id.name', 'not in', process_exceptions),
                         ('id', 'not in', ids)]
        sw_ids = self.search(cursor, uid, search_params)
        if sw_ids:
            err = _(u"Ja existeix un cas obert rel·lacionat amb el CUPS "
                    u"'%s'") % cups_name
            raise osv.except_osv('Error', err)
        return True

    def check_destinatari_is_cur(self, cursor, uid, sw_id, pas=False, context=None):
        if not context:
            context = {}
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        if not pas:
            sw_obj = self.pool.get('giscedata.switching')
            sw = sw_obj.browse(cursor, uid, sw_id)
            if not sw.step_id:
                raise osv.except_osv('Error', _(u"El cas no te cap pas"))
            pas = sw.get_pas()

        return pas.receptor_id.ref in ['0636', '0638', '0642', '0644', '0688']

    def get_casos_actiu_cups_process(self, cursor, uid, cups, processes, context=None):
        """Retorna si el cups esta dins d'un dels processos de la llista de
        processos 'processes'"""
        if context is None:
            context = {}
        pro_obj = self.pool.get('giscedata.switching.proces')
        cups_obj = self.pool.get('giscedata.cups.ps')
        cups_id = cups_obj.search(cursor, uid, [('name', '=', cups)])
        if not cups_id:
            return []
        cups_id = cups_id[0]
        search_params = [
            ('name', 'not in', processes)
        ]
        processes_id = pro_obj.search(cursor, uid, search_params)
        names_list = pro_obj.read(cursor, uid, processes_id, ['name'])
        context['process_exceptions'] = [name['name'] for name in names_list]
        return self.get_casos_actiu_cups(cursor, uid, cups_id, context)

    def get_casos_actiu_cups(self, cursor, uid, cups_id, context=None):
        """Retorna els ids dels casos amb la cups_id,
           excepte dels processos que poden ser simultanis (D1, W1 i R1)"""
        if context is None:
            context = {}

        # Per no trencar el comportament anterior.
        process_exceptions = context.get(
            'process_exceptions', ['D1', 'W1', 'R1']
        )
        if cups_id:
            cups_obj = self.pool.get('giscedata.cups.ps')
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']

        search_params = [('cups_input', '=', cups_name),
                         ('state', 'not in', ('done', 'cancel')),
                         ('proces_id.name', 'not in', process_exceptions)]
        return self.search(cursor, uid, search_params)

    def check_camps_pendents(self, cr, uid, ids, context=None):
        """Comprova si hi ha camps pendents pel cas especificat
        """
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        sw = self.read(cr, uid, ids)
        pas_obj = self.pool.get(sw.step_id.get_step_model())
        view = pas_obj.fields_view_get(cr, uid, context=context)
        xml = etree.fromstring(view['arch'])
        grouptag = []
        inv = False
        for elem in xml.iter():
            keys = [x.lower() for x in elem.keys()]
            vals = elem.values()
            if elem.tag.lower() == 'group':
                while grouptag and elem.getparent() != grouptag[-1:][0][0]:
                    grouptag.pop()
                if not 'attrs' in keys:
                    if not grouptag:
                        inv = False
                    continue
                try:
                    attrs = eval(vals[keys.index('attrs')])
                except:
                    traceback.print_stack()
                    continue
                if grouptag and grouptag[-1:][0][1]:
                    inv = True
                elif 'invisible' in attrs:
                    for cond in attrs['invisible']:
                        inv = eval("'%s' %s '%s'" % (sw[cond[0]],
                                                        cond[1],
                                                        cond[2]))
                        if not inv:
                            break
                grouptag.append((elem, inv))
            elif elem.tag.lower() == 'field' and not inv:
                req = False
                if 'required' in keys and vals[keys.index('required')] == "1":
                    req = True
                elif not 'attrs' in keys:
                    continue
                if not req:
                    try:
                        attrs = eval(vals[keys.index('attrs')])
                    except:
                        traceback.print_stack()
                        continue
                    if 'required' in attrs:
                        for cond in attrs['required']:
                            req = eval("'%s' %s '%s'" % (sw[cond[0]],
                                                            cond[1],
                                                            cond[2]))
                            if not req:
                                break
                if req:
                    if not sw[vals[keys.index('name')]]:
                        msg = _(u"Si us plau, ompliu els camps pendents")
                        raise osv.except_osv(_(u"Error"), msg)
        return True

    def generar_descripcio(self, cursor, uid, filename, context=None):
        if not context:
            context = {}
        desc = _(u"Fitxer importat manualment.\n"
                 u"Nom del fitxer: %s")
        return (desc % filename)

    def get_partner_address(self, cursor, uid, partner_id):
        partner_obj = self.pool.get('res.partner')
        address_obj = self.pool.get('res.partner.address')
        address = partner_obj.address_get(cursor, uid,
                                          [partner_id], ['contact'])
        if address['contact']:
            email = address_obj.read(cursor, uid,
                                     address['contact'], ['email'])
            return (address['contact'], email['email'])
        else:
            return (address['contact'], False)

    def get_estats_anteriors(self, cursor, uid, proces, pas):
        """Retorna una llista de passos anteriors vàlids tals
           que el pas següent sigui el pas passat per paràmetre
        """
        context = {'proces': proces}
        model_obj = self._get_proces_model(cursor, uid, [],
                                           context=context)
        if pas in model_obj._estats_valids:
            return model_obj._estats_valids[pas]
        else:
            return []

    def anullar_sollicitud(self, cursor, uid, ids, context=None):
        """Anul·la la sol·licitud actual i accedeix al pas corresponent
           per notificar-ho a la distribuidora.
        """
        if not context:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for sw in self.browse(cursor, uid, ids, context=context):
            if sw.state == 'draft':
                return False
            model_obj = self._get_proces_model(cursor, uid, [sw.id],
                                               context=context)[sw.id]
            model_obj.anullar_sollicitud(self, cursor, uid,
                                         sw, context=None)
        return True

    def find_sollicitud(self, cursor, uid, xml, context=None):
        """Retorna el cas vinculat amb el codi de sol·licitud en cas
           d'existir. El codi ha de ser unic.
        """
        if not context:
            context = {}

        context.update({'proces': xml.tipus})
        codi_emisor = xml.get_codi_emisor
        codi_destinatari = xml.get_codi_destinatari

        search_params = [('codi_sollicitud', '=', xml.codi_sollicitud),
                         ('proces_id.name', '=', xml.tipus),
                         ('cups_id.name', 'like', xml.cups[:-2]),
                         ('case_id.partner_id.ref', 'in',
                          (codi_emisor, codi_destinatari))]

        sw_ids = self.search(cursor, uid, search_params, context=context)
        return sw_ids and sw_ids[0] or False

    def find_case_per_polissa(self, cursor, uid, polissa_id, context=None):
        """Localitzar cas associat a la pòlissa amb id polissa_id"""
        if not context:
            context = {}

        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        search_params = [('cups_id', '=', polissa.cups.id)]
        return self.search(cursor, uid, search_params, context=context)

    def check_cas_coincident(self, cursor, uid, sw_id, xml, context=None):
        """Evalua si l'estat del cas correspon a l'xml
        """
        if not context:
            context = {}
        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        sw = self.browse(cursor, uid, sw_id, context)
        for sinfo in sw.step_ids:
            if (sw.codi_sollicitud == xml.codi_sollicitud
                and sw.proces_id.name.upper() == xml.get_tipus_xml()
                and sw.cups_id.name == xml.cups
                and sinfo.step_id.name == xml.get_pas_xml()
                and sinfo.get_pas().seq_sollicitud == xml.seq_sollicitud):
                raise osv.except_osv("Error",
                                     _(u"Fitxer processat anteriorment."))

    def get_header_vals(self, cursor, uid, sw_id, context=None):

        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        sw = self.browse(cursor, uid, sw_id)
        vals = {
            'codigo_del_proceso': sw.proces_id.name.upper(),
            'codigo_de_solicitud': sw.codi_sollicitud,
            'secuencial_de_solicitud': sw.get_pas().seq_sollicitud,
        }
        if sw.cups_id:
            cups = sw.cups_id.name
        else:
            cups = sw.cups_input
        vals.update({'cups': cups})
        return vals

    def create_step(self, cursor, uid, sw_id, pas, pas_id, context=None):

        step_obj = self.pool.get('giscedata.switching.step')
        info_obj = self.pool.get('giscedata.switching.step.info')

        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]
        sw = self.browse(cursor, uid, sw_id)
        pas_model = step_obj.get_step_model(cursor, uid, [],
                                            name=pas,
                                            proces=sw.proces_id.name,
                                            context=context)

        vals = {'sw_id': sw_id,
                'step_id': step_obj.get_step(cursor, uid, pas,
                                             sw.proces_id.name),
                'proces_id': sw.proces_id.id,
                'pas_id': '%s,%s' % (pas_model,
                                     pas_id),
                }

        return info_obj.create(cursor, uid, vals)

    def check_cups(self, cursor, uid, cups_name, process, context=None):
        """Retorna cups_id del CUPS amb cups_name
        """

        cups_obj = self.pool.get('giscedata.cups.ps')
        conf_obj = self.pool.get('res.config')

        if not context:
            context = {}
        # Check len of cups to work with
        used_len = int(conf_obj.get(cursor, uid, 'check_cups_length', '20'))
        if len(cups_name) > used_len:
            cups_name = cups_name[:used_len]
        search_params = [('name', 'like', cups_name)]
        cups_ids = cups_obj.search(cursor, uid, search_params, context=context)
        return cups_ids and cups_ids[0] or None

    def check_partner(self, cursor, uid, partner_ref,
                      type='emisor', context=None):
        '''Check partner ref'''
        partner_obj = self.pool.get('res.partner')
        user_obj = self.pool.get('res.users')
        err = ''

        if type == 'emisor':
            #Check that partner_ref exists and it is not duplicated
            search_params = [('ref', '=', partner_ref)]
            partner_ids = partner_obj.search(cursor, uid, search_params,
                                             context=context)
            if not partner_ids:
                err = _("Empresa amb referència '%s' inexistent") % partner_ref
            if len(partner_ids) > 1:
                err = _(u"Existeix més d'una empresa "
                        u"amb la referència '%s'") % partner_ref
            if not err:
                partner_id = partner_ids[0]
        elif type == 'receptor':
            #Check that partner_ref it is the same than our company ref
            user = user_obj.browse(cursor, uid, uid)
            if not user.company_id.partner_id.ref:
                err = _(u"Cal configurar el codi REE de %s.") % \
                                            user.company_id.partner_id.name
            if partner_ref != user.company_id.partner_id.ref:
                err = _(u"El destinatari de l'xml, amb referència '%s' no "
                        u"es correspon a %s (REF: %s)") % (partner_ref,
                                            user.company_id.partner_id.name,
                                            user.company_id.partner_id.ref)
            if not err:
                partner_id = user.company_id.partner_id.id
        #If any error found, notify
        if err:
            raise osv.except_osv('Error', err)

        return partner_id

    def check_ccaa(self, cursor, uid, ccaa_ine_code, context=None):
        """Check CCAA INE code"""
        ccaa_obj = self.pool.get('res.comunitat_autonoma')
        user_obj = self.pool.get('res.users')
        err = ''

        #Check that CCAA INE code exists and it is not duplicated
        search_params = [('codi', '=', ccaa_ine_code)]
        ccaa_ids = ccaa_obj.search(cursor, uid, search_params,
                                         context=context)
        if not ccaa_ids:
            err = _(u"Comunitat autònoma amb codi INE '%s' inexistent") % ccaa_ine_code
        if len(ccaa_ids) > 1:
            err = _(u"Existeix més d'una comunitat autònoma "
                    u"amb la referència '%s'") % ccaa_ine_code
        if err:
            raise osv.except_osv('Error', err)

        return ccaa_ids[0]

    def check_motius_rebuig(self, cursor, uid, sw_id, cas, context=None):
        rebuig_obj = self.pool.get('giscedata.switching.motiu.rebuig')
        rebuig_checker = self.get_rebuig_checker(cursor, uid, context)

        sw = self.browse(cursor, uid, sw_id, context=context)

        res = []

        logger = logging.getLogger('openerp')

        for rebuig in rebuig_obj.get_all_motius(cursor, uid, cas):
            check = getattr(rebuig_checker, 'check_{0}'.format(rebuig[1]), None)
            if check is not None:
                if not check(self.pool, cursor, uid, sw):
                    res.append(rebuig)
                    logger.info(
                        'case {0}: check_{1} not passed'.format(cas, rebuig[1]))
                    # R1 only allows up to 5 "motius rebuig"
                    if cas == 'R1' and len(res) >= 5:
                        break
                else:
                    logger.info(
                        'case {0}: check_{1} succsesfully passed!'
                        .format(cas, rebuig[1])
                    )
            else:
                logger.info(
                    'case {0}: check_{1} is not implemented'
                    .format(cas, rebuig[1])
                )

        return res

    def get_rebuig_checker(self, cursor, uid, context):
        return Rebuig()

    def notificar_pas_inesperat(self, cursor, uid, sw_id, xml, context=None):
        """Genera una excepció avisant que el pas del document no es
           l'esperat pel cas"""

        step_obj = self.pool.get('giscedata.switching.step')
        conf_obj = self.pool.get('res.config')
        if not context:
            context = {}

        sw = self.browse(cursor, uid, sw_id)
        ctx = context.copy()
        ctx['rebuig'] = sw.rebuig
        pas_xml = xml.get_pas_xml()
        seq_xml = xml.seq_sollicitud

        validacio = conf_obj.get(cursor, uid, 'switching_step_validation', 'total')

        # Primera validació: comprova si s'està carregant el pas actual
        pas_actual = sw.step_id.name
        seq_actual = sw.get_pas().seq_sollicitud
        if pas_xml == pas_actual and seq_xml == seq_actual:
            raise osv.except_osv(
                'Error',
                _(u"Aquest pas és igual a l'actual {0} "
                  u"(seq: {1})").format(pas_actual, seq_actual)
            )
        if validacio == 'total':
            # Segona validació: comprovem si es un pas esperat
            next_steps = step_obj.get_next_steps(cursor, uid, sw.step_id.name,
                                                 sw.proces_id.name,
                                                 sw.whereiam, context=ctx)
            if pas_xml not in next_steps:
                err = _(u"L'xml amb codi de pas '%s' no pertany al llistat "
                        u"de passos esperats per la sollicitud %s.") % \
                      (xml.get_pas_xml(), sw.codi_sollicitud)
                raise osv.except_osv(_(u"Atenció"), err)

            # Tercera validació: comprovem que no s'hagi carregat abans
            for step in sw.step_ids:
                seq_pas = step.get_pas().seq_sollicitud
                if pas_xml == step.step_id.name and seq_pas == seq_xml:
                    raise osv.except_osv(
                        'Error',
                        _(u"Aquest pas {0} (seq: {2}) ja ha estat carregat a "
                          u"la sol·licitud {1}"
                          ).format(pas_xml, sw.codi_sollicitud, seq_pas)
                    )
        return True

    def pas_inesperat(self, cursor, uid, sw_id, pas, context=None):
        """Comprova si el pas és l'esperat en el cas"""

        step_obj = self.pool.get('giscedata.switching.step')
        if not context:
            context = {}

        sw = self.browse(cursor, uid, sw_id)
        ctx = context.copy()
        if sw.rebuig:
            ctx.update({"rebuig": True})
        next_steps = step_obj.get_next_steps(cursor, uid, sw.step_id.name, sw.proces_id.name, sw.whereiam, context=ctx)
        return pas not in next_steps or pas == sw.step_id.name or pas in [s.step_id.name for s in sw.step_ids]

    def update_from_xml(self, cursor, uid, sw_id, filename, partner_id,
                        cups_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        step_obj = self.pool.get('giscedata.switching.step')
        proces_obj = self.pool.get('giscedata.switching.proces')
        pol_obj = self.pool.get('giscedata.polissa')
        if not context:
            context = {}

        addresses = self.get_partner_address(cursor, uid, partner_id)
        desc = self.generar_descripcio(cursor, uid, filename, context)
        vals = {}
        if not sw_id:
            ctx2 = context.copy()
            if xml.get_tipus_xml() == 'R1':
                ctx2['search_non_active'] = True
            contract_id = self.trobar_polissa_w_cups(
                cursor, uid, cups_id, context=ctx2
            )
            cont_ref = '0000'
            if contract_id:
                out_retailer = self.trobar_comer_sortint_w_polissa(
                    cursor, uid, contract_id, context=context
                )
                contract_name = pol_obj.read(cursor, uid, contract_id, ['name'])
                if contract_name['name']:
                    cont_ref = contract_name['name']
            else:
                out_retailer = False
            vals.update({
                    'partner_id': partner_id,
                    'partner_address_id': addresses[0],
                    'email_from': addresses[1],
                    'description': desc,
                    'codi_sollicitud': xml.codi_sollicitud,
                    'seq_sollicitud': xml.seq_sollicitud,
                    'data_sollicitud': xml.data_sollicitud,
                    'cups_id': cups_id,
                    'cups_input': xml.cups,
                    'cups_polissa_id': contract_id,
                    'ref_contracte': (getattr(xml, 'contrato', False) and
                                      xml.contrato.cod_contrato or cont_ref),
                    'proces_id': proces_obj.get_proces(cursor, uid,
                                                    xml.get_tipus_xml()),
                    'step_id': step_obj.get_step(cursor, uid,
                                             xml.get_pas_xml(),
                                             xml.get_tipus_xml()),
            })

            if xml.get_tipus_xml() == 'A1':
                cups_a1 = ''
                for datos in xml.datos_suministro:
                    cups_a1 = '{}{},'.format(cups_a1, datos.cups)
                vals['cups_input'] = cups_a1
                vals.update({'cau': xml.autoconsumo.cau})

            if out_retailer:
                vals.update({'comer_sortint_id': out_retailer})

            sw_id = self.create(cursor, uid, vals, context=context)

        pas_model = step_obj.get_step_model(cursor, uid, [],
                                            xml.get_pas_xml(),
                                            xml.get_tipus_xml())
        pas_obj = self.pool.get(pas_model)
        pas_obj.create_from_xml(cursor, uid, sw_id, xml, context=context)

        # update deadline
        proces = pas_model.split(".")[-2].upper()
        step = pas_model.split(".")[-1]
        self.update_deadline(cursor, uid, sw_id, proces=proces, step=step)
        return sw_id

    def adjuntar_xml(self, cursor, uid, sw_id, xml, fname, context=None):
        """Desa l'xml en l'erp"""
        attach_obj = self.pool.get('ir.attachment')
        if not context:
            context = {}

        desc = _(u"Codi de sol·licitud: %s, Proces: %s, Pas: %s") % \
                                 (xml.codi_sollicitud,
                                  xml.tipus,
                                  xml.get_pas_xml())
        vals = {
            'name': fname,
            'datas': base64.b64encode(xml.str_xml),
            'datas_fname': fname,
            'res_model': 'giscedata.switching',
            'res_id': sw_id,
            'description': desc,
        }
        attach_obj.create(cursor, uid, vals, context=context)
        return True

    def generar_rebuig_auto(self, cursor, uid, sw_id, checks, cas, context=None):
        pas_obj = self.pool.get(
            'giscedata.switching.{0}.02'.format(cas.lower())
        )

        pas_id = pas_obj.dummy_create_reb(cursor, uid, sw_id, checks, context)
        header_id = pas_obj.read(cursor, uid, pas_id, ['header_id'])
        header_obj = self.pool.get('giscedata.switching.step.header')
        vals = {'enviament_pendent': True}
        header_obj.write(cursor, uid, header_id['header_id'][0], vals)
        self.create_step(cursor, uid, sw_id, '02', pas_id, context=context)
        self.update_deadline(cursor, uid, sw_id, context=context)

    def importar_xml(self, cursor, uid, data, fname, context=None):
        """Importar un nou xml i retorna l'id del cas del crm"""

        if not context:
            context = {}
        fname = unicode(fname)
        cups_obj = self.pool.get('giscedata.cups.ps')
        proces_obj = self.pool.get('giscedata.switching.proces')
        conf_obj = self.pool.get('res.config')
        motiu_obj = self.pool.get('giscedata.switching.motiu.rebuig')
        swlog_obj = self.pool.get('giscedata.switching.log')
        cups_id = False
        partner_id = False
        sw_id = False
        emisor = ''
        cups_xml = ''
        xml = False
        fname = unicode(fname)
        # Get the file type
        try:
            cr = cursor
            db = pooler.get_db(cursor.dbname)
            cursor = db.cursor()
            zip_name = context.get('zip_name', '')

            general_xml = message.Message(data)
            general_xml.parse_xml(validate=False)
            xml_class = globals()[general_xml.tipus]
            # Use the correct class for parsing xml
            xml = xml_class(data)
            xml.parse_xml(validate=False)
            emisor = xml.get_codi_ccaa_emissora if xml.tipus == 'A1' and xml.pas == '01' else xml.get_codi_emisor
            desti = xml.get_codi_ccaa_desti if xml.tipus == 'A1' and xml.pas == '02' else xml.get_codi_destinatari
            cups_xml = xml.cups
            # We have set validate to False so by default it will try to
            # validate but if it can't it will work anyway, just setting it's
            # value of valid to false. Therefore, we can always assume that
            # the parsing should work and if it doesn't we need to raise the
            # exception

            user = self.pool.get('res.users').browse(cursor, uid, uid,
                                                     context=context)
            validacio = conf_obj.get(
                cursor, uid, 'switching_step_validation', 'total'
            )
            if user.company_id.partner_id.ref == emisor and validacio != 'total':
                emisor, desti = desti, emisor

            if xml.tipus == 'A1' and xml.pas == '02':
                self.check_ccaa(cursor, uid, desti, context=context)
            self.check_partner(cursor, uid, desti, type='receptor', context=context)
            if xml.tipus == 'A1' and xml.pas == '01':
                self.check_ccaa(cursor, uid, emisor, context=context)
            partner_id = self.check_partner(cursor, uid, emisor, context=context)

            if cups_xml:
                cups_id = self.check_cups(cursor, uid, cups_xml, xml.tipus,
                                          context=context)

            sw_id = self.find_sollicitud(cursor, uid, xml, context=context)
            where = self.whereiam(cursor, uid, cups_id, context)
            init_steps = proces_obj.get_init_steps(cursor, uid,
                                                   xml.get_tipus_xml(),
                                                   where, context=context)
            msg = []

            if sw_id:
                self.check_cas_coincident(
                    cursor, uid, sw_id, xml, context=context
                )
                self.notificar_pas_inesperat(cursor, uid, sw_id, xml,
                                             context=context)
                self.update_from_xml(cursor, uid, sw_id, fname,
                                     partner_id, cups_id,
                                     xml, context=context)
                self.case_open(cursor, uid, sw_id, {'force_open': True,
                                                    'context': context})
                msg.append(_(u"S'ha actualitzat el cas amb id %d") % sw_id)
            else:
                if xml.get_pas_xml() not in init_steps:
                    err = _(u"No existeix cap cas rel·lacionat amb l'xml.\n"
                            u"No és possible iniciar una nova comunicació "
                            u"del tipus %s amb codi de pas '%s'.\n"
                            u"Codi de sol·licitud '%s'") % (xml.get_tipus_xml(),
                                                             xml.get_pas_xml(),
                                                             xml.codi_sollicitud
                                                            )
                    raise orm.except_orm(_(u"Atenció"), err)
                sw_id = self.update_from_xml(cursor, uid, None, fname,
                                             partner_id, cups_id,
                                             xml, context=context)
                msg.append(_(u"S'ha creat el cas amb id %d") % sw_id)

            self.adjuntar_xml(cursor, uid, sw_id, xml, fname, context=context)
            self.case_log(cursor, uid, sw_id, context=context)
            msg.insert(0, _(u"Fitxer processat correctament."))

            cas = xml.get_tipus_xml()
            checks = None
            if xml.get_pas_xml() == '01':
                checks = self.check_motius_rebuig(
                    cursor, uid, sw_id, cas, context=None
                )
                if checks:
                    self.generar_rebuig_auto(
                        cursor, uid, sw_id, checks, cas, context=None)

                    msg.append(_(u"\nS'ha generat el rebuig automaticament "
                                 u"pels següents motius: "))

                    checks_ids = [check[0] for check in checks]
                    mot_desc = motiu_obj.read(
                        cursor, uid, checks_ids, ['name', 'text']
                    )
                    for motiu in mot_desc:
                        msg.append(
                            u"    {0} - {1}".format(motiu['name'], motiu['text'])
                        )
                        # Si motiu rebuig 85 (falten camps al R1-01),
                        # busquem quins camps falten i esl posem al missatge.
                        if motiu['name'] == '85':
                            min_camps_no_trobats = MinimumFieldsChecker(xml).check()
                            min_no_trobats_str = ""
                            for c in min_camps_no_trobats:
                                min_no_trobats_str += "           * {}\n".format(c)
                            msg.append(min_no_trobats_str)

            if xml.get_pas_xml() in init_steps:
                # Obrir o deixar en Borrador el cas segons variable de
                # configuració
                case_state_open = conf_obj.get(
                    cursor, uid, 'sw_importation_case_state_open', '1'
                )
                try:
                    case_state_open = int(case_state_open)
                except:
                    case_state_open = False
                if case_state_open or checks: # Si es genera rebuig tambe s'obre
                    self.case_open(cursor, uid, [sw_id], {'context': context})

            info = '\n'.join(msg)

            cursor.commit()
            cursor.close()
            cursor = cr

            add_message = self.importar_xml_post_hook(cursor, uid, sw_id, context=context)
            info = "{0} {1}".format(info, add_message)

            params = {'origin': zip_name if zip_name else fname or '',
                      'file': fname,
                      'status': 'correcte',
                      'request_code': xml.codi_sollicitud,
                      'pas': xml.get_pas_xml(),
                      'proces': xml.get_tipus_xml(),
                      'info': info,
                      'emisor_id': partner_id,
                      'cups_text': cups_xml,
                      'tipus': 'import',
                      'codi_emisor': emisor
                      }

            swlog_id = swlog_obj.create(cursor, uid, params, context=context)
            self.create_attachment(cursor, uid, swlog_id, fname, data, context=context)

            return sw_id, info
        except Exception, e:
            cursor.rollback()
            cursor.close()
            cursor = cr
            zip_name = context.get('zip_name', '')
            e_string = str(e)
            if not e_string:
                e_string = e.value
                e.message = e.value
            params = {'origin': zip_name if zip_name else fname or '',
                      'file': fname,
                      'status': 'error',
                      'info': e_string,
                      'request_code': xml.codi_sollicitud if xml else False,
                      'pas': xml.get_pas_xml() if xml else False,
                      'proces': xml.get_tipus_xml() if xml else False,
                      'emisor_id': partner_id,
                      'codi_emisor': emisor,
                      'cups_text': cups_xml,
                      'tipus': 'import',
                      }
            # With this param will force crate of switching logs
            ctx = context.copy()
            ctx['force_log'] = True
            swlog_id = swlog_obj.create(cursor, uid, params, context=ctx)
            self.create_attachment(cursor, uid, swlog_id, fname, data, context=context)
            raise e

    def create_attachment(self, cursor, uid, swlog_id, fname, data, context=None):
        ir_attachment_obj = TransactionExecute(cursor.dbname, uid, 'ir.attachment')

        att_search_values = [
            ('name', '=', fname),
            ('res_id', '=', swlog_id),
            ('res_model', '=', 'giscedata.switching.log'),
            ('datas', '=', base64.encodestring(data)),
        ]
        attach_id = ir_attachment_obj.search(att_search_values, context=context)

        if not attach_id:
            attachment_values = {
                'name': fname,
                'res_id': swlog_id,
                'res_model': 'giscedata.switching.log',
                'datas': base64.encodestring(data),
            }
            ir_attachment_obj.create(attachment_values, context=context)

    def importar_xml_post_hook(self, cursor, uid, sw_id, context=None):
        """ To be overwritten in custom modules """
        ctx = context.copy()
        ctx['activate_only_automatic'] = True
        try:
            res = self.activa_polissa_cas_atr(cursor, uid, sw_id, context=ctx)
            if res and len(res) > 1 and "no implementada/activada" in res[1]:
                res = ""
            elif res and len(res) > 1:
                res = res[1]
            else:
                res = ""
        except Exception, e:
            res = e.message
        return res


    def exportar_xml(self, cursor, uid, sw_id, step_id=False, pas_id=False, context=None):
        """
        Llença l'script per generar l'xml.
        Retorna tupla amb:
            (nom_fitxer, xml_generat, resultat_validació)

        'resultat_validació' es una namedtuple ValidationResult amb:
            (valid, missatge)

        on 'valid': boolea indicant si el XML es valid o no
        i 'error':
            - missatge d'error produït al generar el xml
            - missatge d'error al validar contra XSD.
            - missatge buit quan el XML es valid

        Segons la variable de configuracio 'sw_xsd_validation' es tracten els
        errors de validacio com errors o no.
        """
        if not context:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')
        step_obj = self.pool.get('giscedata.switching.step')
        res_config = self.pool.get('res.config')
        swlog_obj = self.pool.get('giscedata.switching.log')

        sw = sw_obj.browse(cursor, uid, sw_id)
        # If no step id, use the last one of the case
        if not step_id:
            if not sw.step_id.id:
                raise osv.except_osv('Error',
                                     _(u"El cas no te cap pas"))
            step_id = sw.step_id.id

        step_fields = ['name', 'proces_id.name']
        domain = [('id', '=', step_id)]
        query = OOQuery(step_obj, cursor, uid).select(step_fields).where(domain)
        cursor.execute(*query)
        step_vals = cursor.dictfetchall()[0]

        # when marking a draft as sent, open it
        if sw.state == 'cancel':
            raise osv.except_osv(
                _(u"Error Usuari"),
                _(u"El cas {0} està en estat cancelat. No es poden exportar "
                  u"casos cancelats. Si el vols exportar canvia l'estat a esborrany "
                  u"i després a obert").format(sw.codi_sollicitud)
            )
        elif sw.state == 'draft' and context.get('mark_as_sended', False):
            sw.case_open()

        step_model = step_obj.get_step_model(cursor, uid, step_id)
        pas_obj = self.pool.get(step_model)
        filters = [('sw_id', '=', sw_id)]
        if pas_id:
            filters.append(('id', '=', pas_id))
        pas_id = pas_obj.search(cursor, uid, filters)
        pas = pas_obj.browse(cursor, uid, pas_id[0])

        log_params = {
            'origin': _('Exportació ATR'),
            'pas': step_vals['name'],
            'proces': step_vals['proces_id.name'],
            'file': 'Cas: {}'.format(sw.id),
            'cups_text': sw.cups_id.name,
            'tipus': 'export',
            'codi_emisor': pas.emisor_id.ref,
            'emisor_id': pas.emisor_id.id,
            'request_code': sw.codi_sollicitud or '',
        }

        meta_ctx = context.get('meta', {})
        if meta_ctx.get('store_attachment', True):
            if pas.emisor_id.id != sw.company_id.id:
                raise osv.except_osv('Error',
                                     _(u"No és l'emisor del pas actual"))
        try:
            xml = pas_obj.generar_xml(cursor, uid, pas_id, context)

            validation = xml and validate_xml(xml[1])
            if not validation.valid:
                log_params['info'] = validation.error
                # For validation errors we check config to manage
                # them as errors or not
                val = int(res_config.get(cursor, uid, 'sw_xsd_validation', 1))
                if not val:
                    validation = ValidationResult(True, validation.error)
            else:
                log_params['info'] = 'Cas {} exportat correctament'.format(sw.id)
                if xml and context.get('mark_as_sended', False):
                    pas.header_id.set_enviament_pendent(False, context)
                self.check_auto_close(cursor, uid, sw_id, context=context)

            res = (xml[0], xml[1], validation)
            log_params['status'] = 'correcte' if validation.valid else 'error'

        except osv.except_osv as e:
            res = ("", "", ValidationResult(False, "Error: " + str(e.value)))
            log_params.update({'status': 'error', 'info': res[2].error})

        swlog_obj.create(cursor, uid, log_params, context=context)
        return res

    def check_auto_close(self, cursor, uid, sw_id, sw=False, context=None):
        if context is None:
            context = {}
        conf_obj = self.pool.get('res.config')
        autoclose = int(conf_obj.get(cursor, uid, 'sw_autoclose_on_last_send', "0"))
        if not autoclose:
            return False
        if not sw:
            sw = self.browse(cursor, uid, sw_id)
        passos_pendents = self.get_passos_pendents_enviament(cursor, uid, sw_id, context=context)
        if not len(passos_pendents) and sw._ff_get_final(None, None, context=context).get(sw.id):
            sw.case_close()
            return True
        return False

    def whereiam(self, cursor, uid, cups_id, context=None):
        '''retorna si estem a distri o a comer depenent de si el
        cups pertany a la nostra companyia o no'''
        return self.pool.get('giscedata.cups.ps').whereiam(cursor, uid, context)

    def search_address(self, cursor, uid, sw_id, direccio, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')
        addr_obj = self.pool.get('res.partner.address')
        tv_obj = self.pool.get('res.tipovia')
        mun_obj = self.pool.get('res.municipi')
        country_obj = self.pool.get('res.country')
        state_obj = self.pool.get('res.country.state')

        sw = sw_obj.browse(cursor, uid, sw_id)

        search_params = [('zip', '=', direccio.cod_postal),
                         ('id_municipi.ine', '=', direccio.municipio),
                         ('country_id.name', 'ilike', direccio.pais),
                         ('state_id.code', '=', direccio.provincia)]
        if direccio.aclarador_finca:
            param = ('aclarador', '=', direccio.aclarador_finca)
            search_params.append(param)
        if direccio.apartado_de_correos:
            param = ('apartat_correus', '=', direccio.apartado_de_correos)
            search_params.append(param)
        if direccio.numero_finca:
            param = ('pnp', '=', direccio.numero_finca)
            search_params.append(param)
        if direccio.calle:
            param = ('nv', '=', direccio.calle)
            search_params.append(param)
        if direccio.tipo_via:
            tv_ids = tv_obj.name_search(cursor, uid, direccio.tipo_via)
            if tv_ids:
                param = ('tv', '=', tv_ids[0][0])
                search_params.append(param)
        if direccio.escalera:
            param = ('es', '=', direccio.escalera)
            search_params.append(param)
        if direccio.piso:
            param = ('pt', '=', direccio.piso)
            search_params.append(param)
        if direccio.puerta:
            param = ('pu', '=', direccio.puerta)
            search_params.append(param)
        addr_ids = addr_obj.search(cursor, uid, search_params)
        if addr_ids:
            ares = None
            if len(addr_ids) == 1:
                ares = addr_ids[0]
            else:
                for addr in addr_obj.browse(cursor, uid, addr_ids):
                    if addr.partner_id.id == sw.cups_polissa_id.titular.id:
                        ares = addr.id
                        break
                if not ares:
                    ares = addr_ids[0]
            if context.get("new_phones", []) and ares:
                addr = addr_obj.browse(cursor, uid, ares)
                if not addr.phone and len(context.get("new_phones", [])) > 0:
                    addr.write({'phone': context.get("new_phones")[0]})
                if not addr.mobile:
                    if len(context.get("new_phones", [])) > 1:
                        addr.write({'mobile': context.get("new_phones")[1]})
                    elif len(context.get("new_phones", [])) > 0:
                        addr.write({'mobile': context.get("new_phones")[0]})
            return ares

        mun_ids = mun_obj.search(cursor, uid,
                                [('ine', '=', direccio.municipio)])
        country_ids = country_obj.search(cursor, uid,
                                [('name', '=', direccio.pais)])
        if country_ids:
            country_id = country_ids[0]
            search_params = [('country_id', '=', country_id),
                             ('code', '=', direccio.provincia)]
            state_ids = state_obj.search(cursor, uid, search_params)
            state_id = state_ids and state_ids[0] or False
        else:
            country_id = False
            state_id = False

        vals = {'nv': direccio.calle,
                'pnp': direccio.numero_finca,
                'aclarador': direccio.aclarador_finca,
                'zip': direccio.cod_postal,
                'id_municipi': mun_ids and mun_ids[0] or False,
                'country_id': country_id,
                'state_id': state_id,
                'apartat_correus': direccio.apartado_de_correos
                }
        if len(context.get("new_phones", [])) > 0:
            vals.update({'phone': context.get("new_phones")[0]})
        if len(context.get("new_phones", [])) > 1:
            vals.update({'mobile': context.get("new_phones")[1]})
        if direccio.tipo_via:
            tv_ids = tv_obj.name_search(cursor, uid, direccio.tipo_via)
            if tv_ids:
                vals.update({
                    'tv': tv_ids[0][0]
                })
        if direccio.escalera:
            vals.update({
                'es': direccio.escalera
            })
        if direccio.piso:
            vals.update({
                'pt': direccio.piso
            })
        if direccio.puerta:
            vals.update({
                'pu': direccio.puerta
            })
        if context.get("cliente"):
            cliente = context.get("cliente")
            if cliente.tipo_persona == 'F':
                name_as_dict = {
                    "N": cliente.nombre_de_pila,
                    "C1": cliente.primer_apellido,
                    "C2": cliente.segundo_apellido
                }
                partner_name = self.pool.get("res.partner").ajunta_cognoms(cursor, uid, name_as_dict, context=context)
            else:
                partner_name = cliente.razon_social
            vals.update({'name': partner_name})

        return addr_obj.create(cursor, uid, vals)

    def create_partner_addres_from_cups(self, cursor, uid, cups_id, context=None):
        if not context:
            context = {}

        addr_obj = self.pool.get('res.partner.address')
        cups = self.pool.get("giscedata.cups.ps").browse(cursor, uid, cups_id)
        municipi_id, country_id, state_id = False, False, False
        if cups.id_municipi:
            municipi_id = cups.id_municipi.id
            if cups.id_municipi.state:
                state_id = cups.id_municipi.state.id
                if cups.id_municipi.state.country_id:
                    country_id = cups.id_municipi.state.country_id.id

        addr_vals = {
            'nv': cups.nv,
            'pnp': cups.pnp,
            'aclarador': cups.aclarador,
            'zip': cups.dp,
            'id_municipi': municipi_id,
            'country_id': country_id,
            'state_id': state_id,
            'tv': cups.tv and cups.tv.id or False,
            'es': cups.es,
            'pt': cups.pt,
            'pu': cups.pu,
            'name': context.get('new_name')
        }
        if len(context.get("new_phones", [])) > 0:
            addr_vals.update({'phone': context.get("new_phones")[0]})
        if len(context.get("new_phones", [])) > 1:
            addr_vals.update({'mobile': context.get("new_phones")[1]})
        return addr_obj.create(cursor, uid, addr_vals, context=context)

    def search_cnae(self, cursor, uid, cnae, context=None):

        cnae_obj = self.pool.get('giscemisc.cnae')
        search_params = [('name', '=', cnae)]
        cnae_ids = cnae_obj.search(cursor, uid, search_params)
        return cnae_ids and cnae_ids[0] or False

    def notify_sw(self, cursor, uid, email_from, email_to, context=None):
        '''Send notifications by email
        * past deadline'''

        mail_obj = self.pool.get('poweremail.mailbox')
        acc_obj = self.pool.get('poweremail.core_accounts')
        user_obj = self.pool.get('res.users')

        if not context:
            context = {}
        if 'lang' not in context:
            user = user_obj.browse(cursor, uid, uid)
            lang = user.context_lang
            company = user.company_id.name
            context.update({'lang': lang})

        # Search for the account = email_from
        search_params = [('email_id', '=', email_from)]
        acc_id = acc_obj.search(cursor, uid, search_params)[0]

        subject = _(u"[Gestió ATR] Notificacions") + " " + company
        now = datetime.strftime(datetime.now(), '%Y-%m-%d 23:59:59')
        search_params = [('date_deadline', '<=', now),
                         ('state', 'not in', ('draft', 'cancel', 'done'))]
        sw_ids = self.search(cursor, uid, search_params, context=context)
        mes = ''
        for sw_id in sw_ids:
            sw = self.browse(cursor, uid, sw_id)
            deadline = datetime.strftime(
                            datetime.strptime(sw.date_deadline[:10],
                                              '%Y-%m-%d'),
                            '%d/%m/%Y')
            mes += (_(u"El cas %s (S:%s PR:%s P:%s) "
                      u"ha superat la data límit (%s)\n")
                        % (sw_id, sw.codi_sollicitud, sw.proces_id.name,
                           sw.step_id.name, deadline))
        if mes:
            vals = {
                'pem_from': email_from,
                'pem_to': email_to,
                'pem_subject': subject,
                'pem_body_text': mes,
                'pem_account_id': acc_id,
            }
            mail_id = mail_obj.create(cursor, uid, vals, context)
            mail_obj.send_this_mail(cursor, uid, [mail_id], context)

        return True

    def get_final(self, cursor, uid, sw, context=None):

        return False

    def activa_cas_atr(self, cursor, uid, sw, context=None):
        if context is None:
            context = {}
        act_obj = self.pool.get("giscedata.switching.activation.config")
        helperdist_obj = self.pool.get('giscedata.switching.helpers.distri')
        helpercomer_obj = self.pool.get('giscedata.switching.helpers')
        if sw.whereiam == "distri":
            helper_obj = helperdist_obj
            whereiam = _(u'Distribuidora')
        else:
            helper_obj = helpercomer_obj
            whereiam = _(u'Comercializadora')

        method_names = act_obj.get_activation_method(cursor, uid, sw.get_pas(), context=context)
        if not method_names:
            return [_(u'Atenció'),
                    _(u'Activació cas %s-%s no implementada/activada a %s.\n\n'
                      u'Pots consultar les activacions disponibles a:\n'
                      u'* Gestió ATR -> Configuració -> Activacions de Casos ATR'
                    ) % (sw.proces_id.name, sw.step_id.name, whereiam)]
        else:
            all_res = []
            all_res_description = []
            for method_name in method_names:
                method_caller = getattr(helper_obj, method_name, False)
                if not method_caller and helper_obj != helpercomer_obj:
                    # Els metodes comuns estan al de comer, busquem allà
                    method_caller = getattr(helpercomer_obj, method_name, False)
                if not method_caller:
                    raise osv.except_osv(
                        _('Error'),
                        _("No s'ha pogut activar el cas {0} perqué no es troba el "
                          "métode d'activació {1}.").format(sw.name, method_name)
                    )
                init_str = _(u"Resultat Activacio:\n")
                db = pooler.get_db(cursor.dbname)
                tmp_cursor = db.cursor()
                msg_h = ""
                try:
                    res = method_caller(tmp_cursor, uid, sw.id, context=context)
                    if len(res) == 2:
                        msg = res[0] + ": " + res[1]
                        all_res.append(res[0].upper())
                        all_res_description.append(res[1])
                    elif len(res) == 1:
                        msg = res[0]
                        all_res.append("INFO.")
                        all_res_description.append(res[0])
                    elif not res:
                        continue
                    else:
                        raise Exception("ERROR", _(u"La activacio no ha retornat la informacio esperada"))
                    msg_h = init_str + msg
                    tmp_cursor.commit()
                except Exception, e:
                    msg_h = init_str + str(e)
                    tmp_cursor.rollback()
                    return ("ERROR", str(e))
                finally:
                    tmp_cursor.close()
                    db = pooler.get_db(cursor.dbname)
                    tmp_cursor = db.cursor()
                    self.update_deadline(tmp_cursor, uid, sw.id)
                    self.historize_msg(tmp_cursor, uid, sw.id, msg_h, context=context)
                    tmp_cursor.commit()
                    tmp_cursor.close()
            if "ERROR" in all_res:
                final_res = ["ERROR"]
            elif "OK" in all_res:
                final_res = ["OK"]
            else:
                final_res = [all_res[0]]

            n_msg = ""
            try:
                init_str = _(u"Resultat Notificacio:\n")
                notificate_on_activate = int(
                    self.pool.get("res.config").get(cursor, 1, "sw_notificate_on_activate", "0")
                )
                if notificate_on_activate and final_res[0] != "ERROR":
                    pas = sw.get_pas()
                    if pas.notificacio_pendent:
                        ctx = context.copy()
                        ctx['check_cas_tancat_on_activate'] = True
                        ares = sw.notifica_a_client(context=ctx)
                        n_msg = init_str + ares[1]
                        self.historize_msg(cursor, uid, sw.id, n_msg, context=context)
            except Exception, e:
                n_msg = init_str + str(e)
                self.historize_msg(cursor, uid, sw.id, n_msg, context=context)

            final_res.append("\n\n".join(all_res_description+[n_msg]))
            return final_res

    def activa_polissa_cas_atr(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')

        #Cridem la funció al model que correspongui
        sw = sw_obj.browse(cursor, uid, sw_id)
        ctx = context.copy()
        ctx.update({'sync': False})
        res = self.activa_cas_atr(cursor, uid, sw, context=ctx)

        return res

    def notifica_pas_a_client(self, cursor, uid, sw_id, pas_id, step_model_name,
                              template_id=False, context=None):
        """
        Envia mail a client per a un pas d'un cas atr segons plantilla
        :param cursor:          OpenERP Cursor
        :param uid:             OpenERP User ID
        :type uid:              int
        :param sw_id:           Switching Object ID
        :type sw_id:            long
        :param template_id:     Model data ID (Poweremail Template)
        :type template_id:      int
        :param pas_id:          Switching Step Object ID
        :type pas_id:           int
        :param step_model_name: Step Model Name
        :type step_model_name:  str
        :param context:         OpenERP Cursor
        :type context:          dict
        :return:
        """
        pw_account_obj = self.pool.get('poweremail.core_accounts')
        pwetmpl_obj = self.pool.get('poweremail.templates')
        pwswz_obj = self.pool.get('poweremail.send.wizard')
        partner_obj = self.pool.get('res.partner')
        user_obj = self.pool.get('res.users')
        pol_obj = self.pool.get('giscedata.polissa')
        sw_obj = self.pool.get('giscedata.switching')
        notify_obj = self.pool.get('giscedata.switching.notify')
        step_obj = self.pool.get('giscedata.switching.step')
        pas_obj = self.pool.get(step_model_name)
        header_obj = self.pool.get('giscedata.switching.step.header')
        conf_obj = self.pool.get('res.config')

        if not context:
            context = {}
        if isinstance(pas_id, list):
            pas_id = pas_id[0]

        sw_data = sw_obj.read(
            cursor, uid, sw_id, ['cups_polissa_id', 'name', 'proces_id']
        )
        polissa_name = sw_data['cups_polissa_id'][1]
        sw_name = sw_data['name']

        # Check PAS pendent de notificar
        pas_data = pas_obj.read(cursor, uid, pas_id, ['notificacio_pendent',
                                                      'header_id'])
        if not pas_data.get('notificacio_pendent', False):
            return False, _(u'Aquest pas ja està notificat al client')

        # Si no es facilita la plantilla, la obtenim del SW.Step.Header
        if not template_id:
            try:
                header_id = pas_data['header_id'][0]
                template_id = header_obj.get_notification_mail(
                    cursor, uid, [header_id]
                )[0]
            except Exception as e:
                return (
                    False, _(
                        u"No s'ha pogut notificar al usuari del cas {}_\n{}"
                    ).format(sw_name, e)
                )
        template = pwetmpl_obj.browse(cursor, uid, template_id)

        mail_from = False

        # Obtenir compte de correu a utilitzar
        if template.enforce_from_account:
            mail_from = template.enforce_from_account.id
        if not mail_from or not isinstance(mail_from, (int, long)):
            mail_from = context.get('use_mail_account', False)
        if not mail_from or not isinstance(mail_from, (int, long)):
            address_mail_noti = conf_obj.get(
                cursor, uid, 'sw_email_address_user_notification'
            ) or False
            if address_mail_noti:
                mail_from = pw_account_obj.search(cursor, uid, [
                    ('email_id', 'ilike', address_mail_noti)
                ])
                if mail_from:
                    mail_from = mail_from[0]
        if not mail_from or not isinstance(mail_from, (int, long)):
            return (False, _(
                u"No s'ha enviat el mail de notificació a "
                u"l'usuari de la pólissa '%s' perquè la plantilla "
                u"de correu '%s' no té cap compte assignat"
            ) % (polissa_name, template.name))

        ctx = {'active_ids': [sw_id], 'active_id': sw_id,
               'template_id': template_id, 'src_rec_ids': [sw_id],
               'src_model': 'giscedata.switching', 'from': mail_from,
               'state': 'single', 'priority': '0', 'folder': 'outbox',
               'save_async': True}
        params = {'state': 'single', 'priority': '0', 'from': ctx['from']}

        # Obtenir el llenguatge del titular
        titular_id = pol_obj.read(
            cursor, uid, sw_data['cups_polissa_id'][0], ['titular']
        )['titular'][0]
        templ_lang = partner_obj.read(cursor, uid, titular_id, ['lang'])['lang']
        # Si no en té, l'obtenim de la configuració del ERP
        if not templ_lang:
            templ_lang = config.get('language', False)
        # Si no en té, l'obtenim del context
        if not templ_lang:
            templ_lang = context.get('lang', False)
        # Si no en té, l'obtenim del usuari que realitza l'acció
        if not templ_lang:
            templ_lang = user_obj.read(
                cursor, uid, uid, ['context_lang'])['context_lang']
        # Si hem obtingut algun llenguatge, el posem al context
        if templ_lang:
            ctx.update({'lang': templ_lang})

        pas = pas_obj.browse(cursor, uid, pas_id)
        if not pas.notificacio_pendent:
            return False, _(
                u"El Procés ATR: {} en el pas {} no està pendent de notificar"
            ).format(sw_name, pas._nom_pas)
        proces_id = sw_data['proces_id'][0]
        # Cerca notificacio
        step_id = step_obj.search(cursor, uid, [
            ('name', '=', pas._nom_pas),
            ('proces_id', '=', proces_id)
        ])[0]

        notify_id = notify_obj.search(cursor, uid, [
            ('step_id', '=', step_id),
            ('proces_id', '=', proces_id)
        ])
        rebuig = False
        if pas.rebuig:
            if not notify_id:
                return False, _(
                    u"No hi ha notificacions habilitades per rebuigs d'aquest"
                    u"Proces/Pas ATR: {}"
                ).format(sw_name)
            notify_dades = notify_obj.read(
                cursor, uid, notify_id, ['rebuig_ids'])
            for notify in notify_dades:
                # Check temporal fins que sapiguem tractar correctament
                #   la notificació per més d'un rebuig en el mateix pas
                if len(pas.rebuig_ids) > 1:
                    return False, _(
                        u"Hi ha més d'un rebuig en el Procés ATR: {}"
                    ).format(sw_name)
                for tipus_rebuig in pas.rebuig_ids:
                    if tipus_rebuig.motiu_rebuig.id in notify['rebuig_ids']:
                        if rebuig:
                            return False, _(
                                u"Hi ha més d'una notificació habilitada per el"
                                u" rebuig \"{}\" en el Procés ATR: {}"
                            ).format(tipus_rebuig.motiu_rebuig.text, sw_name)
                        rebuig = notify_obj.get_notification_text(
                            cursor, uid, notify['id'], sw_id, ctx
                        )
            if not rebuig:
                return False, _(
                    u"No hi ha notificacions habilitades per rebuigs d'aquest"
                    u"Proces/Pas ATR: {}"
                ).format(sw_name)
        try:
            if rebuig:
                ctx['extra_render_values'] = {'notificacio_text': rebuig}
            pwswz_id = pwswz_obj.create(cursor, uid, params, ctx)
            pwemb_id = pwswz_obj.save_to_mailbox(cursor, uid, [pwswz_id], ctx)
            # If OORQ_ASYNC, pwemb_id -> Job_id
            # Else, pwemb_id -> PoweremailMailbox.id
            pas_obj.write(cursor, uid, [pas_id], {
                'notificacio_pendent': False
            })
            # Quan notifiquem 05 marcar 02 (acceptacio) notificat
            if pas._nom_pas in ('04', '05'):
                step02_modelname = step_model_name.replace(pas._nom_pas, '02')
                step02_obj = self.pool.get(step02_modelname)
                step02_id = step02_obj.search(cursor, uid, [
                    ('sw_id', '=', sw_id)
                ])
                step02_obj.write(cursor, uid, step02_id,
                                 {'notificacio_pendent': False})
            elif pas._nom_pas in ('06', ):
                step11_modelname = step_model_name.replace(pas._nom_pas, '11')
                step11_obj = self.pool.get(step11_modelname)
                step11_id = step11_obj.search(cursor, uid, [
                    ('sw_id', '=', sw_id)
                ])
                step11_obj.write(cursor, uid, step11_id,
                                 {'notificacio_pendent': False})

            return (
                True, _(
                    u"S'ha notificat correctament per mail a l'usuari "
                    u"del cas {}").format(sw_name)
            )
        except Exception as e:
            return (
                False, _(
                    u"No s'ha pogut notificar al usuari del cas {}_\n{}"
                ).format(sw_name, e)
            )

    def notifica_a_client(self, cursor, uid, sw_id, template=None,
                          context=None):
        """
        Envia mail a client segons plantilla
        :param cursor:      OpenERP cursor
        :param uid:         OpenERP User ID
        :type uid:          int
        :param sw_id:       Switching Object ID
        :type sw_id:        long
        :param template:    Poweremail Template Name
        :type template:     str
        :param context:     OpenERP Context
        :type context:      dict
        :return:
        """
        if not context:
            context = {}

        if isinstance(sw_id, (list, tuple)):
            sw_id = sw_id[0]

        mdata_obj = self.pool.get('ir.model.data')
        conf_obj = self.pool.get('res.config')
        pw_account_obj = self.pool.get('poweremail.core_accounts')

        # enable/disable user mail notifications
        mail_noti = conf_obj.get(
            cursor, uid, 'sw_mail_user_notification_on_activation'
        ) or False
        if not mail_noti:
            return ('OK', _(u'Enviaments de correu de notificació per switching'
                            u' deshabilitats'))
        try:
            sw = self.browse(cursor, uid, sw_id)
            pas = sw.get_pas()
            if not pas.notificacio_pendent:
                return 'OK', _(u'Aquest pas ja està notificat al client')

            es_notificable = self.pool.get(
                "giscedata.switching.notificacio.config"
            ).check_is_notificable(cursor, uid, pas, context=context)
            sw_name = '{}-{}'.format(sw.proces_id.name, sw.step_id.name)
            if mail_noti != 'all' and sw_name not in mail_noti and not es_notificable:
                return ('OK', _(u'Enviaments de correu de notificació'
                                u' per switching {} deshabilitats').format(
                    sw_name
                ))

            if template:
                # Busquem la plantilla de mail d'activació si s'ha facilitat
                search_vals = ([
                    ('model', '=', 'poweremail.templates'),
                    ('name', '=', template)
                ])

                model_data_id = mdata_obj.search(cursor, uid, search_vals)[0]
                template_id = mdata_obj.read(
                    cursor, uid, model_data_id, ['res_id'])['res_id']
            else:
                template_id = False

            state, msg = self.notifica_pas_a_client(
                cursor, uid, sw_id,  sw.get_pas_id(), sw.get_pas_model_name(),
                template_id, context
            )
            return (_('OK'), msg) if state else (_('ERROR'), msg)

        except osv.except_osv as e:
                info = u'%s' % unicode(e.value)
                return _(u'ERROR'), info

        except Exception as e:
                raise Exception(e)

    def escull_tarifa_comer(self, cursor, uid, ids, tarifa_atr,
                            context=None):
        return None

    def _get_sw_from_step_info(self, cursor, uid, ids, context=None):

        info_obj = self.pool.get('giscedata.switching.step.info')
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        sw_ids = info_obj.read(cursor, uid, ids, ['sw_id'], context=context)
        return [x['sw_id'][0] for x in sw_ids]

    def _ff_get_step(self, cursor, uid, ids, field_name, arg, context=None):

        res = {}
        for sw in self.browse(cursor, uid, ids):
            actual_step = False
            actual_step_date = False
            for step in sw.step_ids:
                if step.pas_id:
                    model, pas_id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(pas_id))
                    new_date = pas.header_id.perm_read()[0]['create_date']
                    if new_date >= actual_step_date:
                        actual_step_date = new_date
                        actual_step = step.step_id.id
                else:  # we are creating a new step, so it will be the newest
                    res[sw.id] = step.step_id.id
            if sw.id not in res:
                res[sw.id] = actual_step
        return res

    def _ff_whereiam(self, cursor, uid, ids, field_name, arg, context=None):
        '''retorna si estem a distri o a comer depenent de si el
        cups pertany a la nostra companyia o no'''

        cups_obj = self.pool.get('giscedata.cups.ps')
        where = cups_obj.whereiam(cursor, uid)
        res = dict.fromkeys(ids, where)
        return res

    def _ff_get_stages(self, cursor, uid, ids, field_name, arg, context=None):
        '''returns all stages associated with switching case'''
        stage_obj = self.pool.get('giscedata.switching.stage')
        res = {}
        for sw in self.read(cursor, uid, ids, ['case_id', 'proces_id', 'step_id', 'whereiam'], context=context):
            if not sw['case_id']:
                continue
            if not sw['proces_id'] or not sw['step_id'] or not sw['whereiam']:
                continue
            res[sw['id']] = stage_obj.get_stages(
                cursor, uid, sw['proces_id'][1], sw['step_id'][1], sw['whereiam'], context=context
            )
        return res

    def _ff_get_final(self, cursor, uid, ids, field_name, arg, context=None):
        '''returns True if the case has arrived to a final step'''

        res = {}
        for sw in self.browse(cursor, uid, ids, context=context):
            res[sw.id] = self.get_final(cursor, uid, sw, context=context)

        return res

    def _ff_enviament_pendent(self, cursor, uid, ids, field_name, arg,
                              context=None):
        """retorna False si no hi ha cap dels passos amb enviament pendent"""
        if context is None:
            context = {}

        res = dict.fromkeys(ids, False)

        for sw_id in ids:
            res[sw_id] = len(self.get_passos_pendents_enviament(cursor, uid, sw_id, context=context))

        return res

    def _ff_send_pendent_search(self, cursor, uid, obj, name, args, context):
        swh_obj = self.pool.get('giscedata.switching.step.header')
        search_params = [
            ('enviament_pendent', args[0][2] and '=' or '!=', True)
        ]
        if args[0][2] == 1: # We want to search for enviament_pendent
            # So we want to add emisor_id to the search params
            user_obj = self.pool.get('res.users')
            user_vals = user_obj.read(cursor, uid, uid, ['company_id'])
            search_params.append(('emisor_id', '=', user_vals['company_id'][0]))

        sql_q = swh_obj.q(cursor, uid).select(['id']).where(search_params)
        cursor.execute(*sql_q)
        h_ids = [h_id[0] for h_id in cursor.fetchall()]
        sw_ids = swh_obj.read(cursor, uid, h_ids, ['sw_id'])
        return [('id', 'in', [s['sw_id'][0] for s in sw_ids if s['sw_id']])]

    def _ff_document_adjunt_solicitud(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)

        swh_obj = self.pool.get('giscedata.switching.step.header')
        swh_ids = swh_obj.search(cursor, uid, [
            ('sw_id', 'in', ids),
            ('document_ids', '!=', False),
        ])
        switching_headers = swh_obj.read(cursor, uid, swh_ids, ['sw_id'])
        sw_ids = list(set([
            sw_header['sw_id'][0] for sw_header in switching_headers
        ]))
        for sw_id in sw_ids:
            res[sw_id] = True

        return res

    def update_ff_accio_pendent_comerdist(self, cursor, uid, ids, context=None):
        res = self._ff_accio_pendent_comerdist(cursor, uid, ids, False, False, context=context)
        for sw_id, val in res.items():
            self.write(cursor, uid, sw_id, {'accio_pendent_comerdist': val}, context=context)
        return True

    def _ff_accio_pendent_comerdist(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, 'comer')
        step_obj = self.pool.get('giscedata.switching.step')
        for sw_info in self.read(cursor, uid, ids, ['step_id', 'rebuig', 'state']):
            if sw_info['state'] in ['done']:
                continue
            if sw_info['step_id']:
                step_info = step_obj.read(cursor, uid, sw_info['step_id'][0], ['accio_pendent', 'description'])
                if sw_info['rebuig'] and "Acceptacio / Rebuig" in step_info['description']:
                    res[sw_info['id']] = 'comer' if step_info['accio_pendent'] == 'distri' else 'distri'
                else:
                    res[sw_info['id']] = step_info['accio_pendent']
        return res

    def _ff_notificacio_pendent(self, cursor, uid, ids, field_name, arg,
                                context=None):
        if context is None:
            context = {}

        res = dict.fromkeys(ids, False)

        swh_obj = self.pool.get('giscedata.switching.step.header')
        swh_ids = swh_obj.search(cursor, uid, [
            ('sw_id', 'in', ids), ('notificacio_pendent', '=', True)
        ])

        switching_headers = swh_obj.read(cursor, uid, swh_ids, ['sw_id'])
        sw_ids = list(set([
            sw_header['sw_id'][0] for sw_header in switching_headers
        ]))
        for sw_id in sw_ids:
            res[sw_id] = True

        return res

    def _ff_notificacio_pendent_search(
            self, cursor, uid, obj, name, args, context):
        swh_obj = self.pool.get('giscedata.switching.step.header')
        h_ids = swh_obj.search(cursor, uid, args)
        sw_ids = [
            s['sw_id'][0]
            for s in swh_obj.read(cursor, uid, h_ids, ['sw_id'])
            if s['sw_id']
        ]
        return [('id', 'in', sw_ids)]

    def _ff_get_contacte_polissa(self, cursor, uid, ids, field_name, arg,
                                 context=None):
        res = {}
        swh = self.pool.get('giscedata.switching.step.header')
        pol_obj = self.pool.get('giscedata.polissa')

        for sw in self.read(cursor, uid, ids, ['cups_polissa_id', 'proces_id'], context=context):
            if not sw['cups_polissa_id']:
                res[sw['id']] = False
                continue
            if sw['proces_id'][1] != 'A1':
                polissa = pol_obj.browse(cursor, uid, sw['cups_polissa_id'][0], context=context)
                camps = {
                    'titular_polissa': False,
                    'nif_titular_polissa': False,
                    'dir_pagador_polissa': False,
                    'tel_pagador_polissa': False,
                    'mail_pagador_polissa': False
                }
                if polissa:
                    camps['titular_polissa'] = polissa.titular.id
                    camps['nif_titular_polissa'] = polissa.titular.vat
                    camps['dir_pagador_polissa'] = polissa.direccio_pagament.id
                    phone = (polissa.direccio_pagament.phone or
                             polissa.direccio_pagament.mobile)
                    tel_str = swh.clean_tel_number(phone)
                    camps['tel_pagador_polissa'] = tel_str['tel']
                    camps['mail_pagador_polissa'] = polissa.direccio_pagament.email

                res[sw['id']] = camps[field_name]

        return res

    def _ff_titular_search(self, cursor, uid, obj, name, args,
                                         context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            ids = self.search(cursor, uid, [
                ('cups_polissa_id.titular.name', 'ilike', args[0][2])
            ])
            ids.append(-1)
            return [('id', 'in', ids)]

    def _ff_nif_titular_polissa_search(self, cursor, uid, obj, name, args, context=None):
        if not context:
            context = {}
        if not args:
            return [('id', '=', 0)]
        else:
            ids = self.search(cursor, uid, [
                ('cups_polissa_id.titular.vat', 'ilike', args[0][2])
            ])
            ids.append(-1)
            return [('id', 'in', ids)]

    def _ff_get_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna True si és un cas en estat rebuig. Cal que estigui en un pas
        que pugui ser de rebuig"""
        if not context:
            context = {}
        res = dict([(i, False) for i in ids])

        steps = self.read(cursor, uid, ids, ['proces_id', 'step_id',
                                             'step_ids'])
        for step in steps:
            proces = step['proces_id'][1]
            pas = step['step_id'] and step['step_id'][1] or False
            proces_obj = self.pool.get('giscedata.switching.proces')
            if pas not in proces_obj.get_reject_steps(cursor, uid, proces,
                                                      context):
                res[step['id']] = False
            else:
                info_obj = self.pool.get('giscedata.switching.step.info')
                passos = info_obj.read(cursor, uid, step['step_ids'],
                                       ['pas_id'])
                model = 'giscedata.switching.%s.%s' % (proces.lower(), pas)
                pas_id = False
                for p in passos:
                    if p['pas_id'] and p['pas_id'].split(',')[0] == model:
                        pas_id = p['pas_id'].split(',')[1]

                if pas_id:
                    rebuig = self.pool.get(model).read(cursor, uid,
                                                       int(pas_id),
                                                       ['rebuig'])['rebuig']
                else:
                    rebuig = False

                res[step['id']] = rebuig

        return res

    def _ff_rebuig_search(self, cursor, uid, obj, name, args, context=None):
        """Per poder cercar els casos en estat rebuig"""
        if not context:
            context = None

        proces_obj = self.pool.get('giscedata.switching.proces')

        processos = ['A3', 'B1', 'C1', 'C2', 'M1', 'W1']
        plantilla = 'giscedata.switching.%s.%s'
        ids_rebuig = []

        # busquem x models
        for p in processos:
            steps = proces_obj.get_reject_steps(cursor, uid, p)

            model_names = [plantilla % (p.lower(), s) for s in steps]
            for m in model_names:
                model_obj = self.pool.get(m)
                ids_r = model_obj.search(cursor, uid, [('rebuig', '=', True)])
                ids_sw_vals = model_obj.read(cursor, uid, ids_r, ['sw_id'])
                ids_sw = [i['sw_id'][0] for i in ids_sw_vals]
                ids_rebuig += ids_sw

        # rebutjats: els de la llista (in)
        # no rebutjats: els que no estan a la llista (not in)
        operacio = args[0][2] and 'in' or 'not in'
        return [('id', operacio, ids_rebuig)]

    def update_additional_info(self, cursor, uid, step_id, model, context=None):
        if context is None:
            context = {}
        pas_obj = self.pool.get(model)
        sw_id = pas_obj.read(cursor, uid, step_id, ['sw_id'])['sw_id'][0]

        if not getattr(pas_obj, 'get_additional_info', False):
            # If the step doesn't have the method 'get_additional_info' we
            # try to use step 01 info.
            # Step 01 always should have 'get_additional_info'
            pas01_mod = "giscedata.switching.{0}.01".format(model.split(".")[2])
            pas_obj = self.pool.get(pas01_mod)
            step_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
            if not step_id:
                return
            step_id = step_id[0]

        add_info = pas_obj.get_additional_info(cursor, uid, step_id, context=context)
        self.write(cursor, uid, [sw_id], {'additional_info': add_info})

    def update_deadline(self, cursor, uid, sw_id, proces=None,  step=None,
                        init_date=None, context=None):
        if context is None:
            context = {}

        # Check if it's reject step, then we've ended and there is no deadline
        reject = self.read(cursor, uid, sw_id, ['rebuig'])['rebuig']
        if reject:
            self.write(cursor, uid, [sw_id], {'date_deadline': False})
            return

        deadline = self.get_deadline_for_step(
            cursor, uid, sw_id, proces=proces, step=step, init_date=init_date,
            context=context
        )

        if deadline:
            self.write(cursor, uid, [sw_id], {'date_deadline': deadline})

    def get_deadline_for_step(self, cursor, uid, sw_id, proces=None,  step=None,
                              init_date=None, context=None):
        if context is None:
            context = {}

        if not proces or not step:
            # Get current proces and step
            sw = self.browse(cursor, uid, sw_id)
            proces = sw.step_id.proces_id.name
            step = sw.step_id.name

        if not init_date:
            # Get init deadline date from step
            step_model = "giscedata.switching.{0}.{1}".format(proces.lower(), step)
            step_obj = self.pool.get(step_model)
            init_date = False
            if getattr(step_obj, 'get_init_deadline_date', False):
                init_date = step_obj.get_init_deadline_date(cursor, uid, sw_id, context)

            if not init_date:
                init_date = datetime.today().strftime("%Y-%m-%d %H:%M:%S")
                init_date = datetime.strptime(init_date, "%Y-%m-%d %H:%M:%S")

        import gestionatr.input.messages as sm
        pas_message = getattr(sm, proces)

        modifier = None
        if proces == 'B1':  # In B1 we need the 'motiu_baixa'
            step_01 = self.pool.get('giscedata.switching.b1.01')
            pas_id = step_01.search(cursor, uid, [('sw_id', '=', sw_id)])
            if len(pas_id) > 0:
                info_01 = step_01.read(cursor, uid, pas_id[0],
                                       ['motiu', 'enviament_pendent'])
                modifier = info_01['motiu']
                # In B1 we have deadlines before sending step 01
                if info_01['enviament_pendent'] and step == '01':
                    step = '00'
        elif proces == 'M1':  # In M1 we need the 'canvi_titular'
            step_01 = self.pool.get('giscedata.switching.m1.01')
            pas_id = step_01.search(cursor, uid, [('sw_id', '=', sw_id)])
            if len(pas_id) > 0:
                info_01 = step_01.read(cursor, uid, pas_id[0],
                                       ['sollicitudadm', 'canvi_titular'])
                if info_01['sollicitudadm'] == 'S' \
                        and info_01['canvi_titular'] in ['S', 'A', 'C']:
                    modifier = info_01['canvi_titular']
                else:
                    modifier = 'T'

        deadline_obj = pas_message.get_deadline(step, modifier=modifier)

        if deadline_obj:
            return deadline_obj.limit(init_date).strftime("%Y-%m-%d")
        return False

    def historize_additional_info(self, cursor, uid, sw, context=None):
        """
        Historize (write in observations) the current additional info of sw to
        its contract.

        :param sw: browse of giscedata_switching
        """
        if sw.cups_polissa_id and sw.additional_info:
            pol = sw.cups_polissa_id
            info = u"\n{0} ({1}):\n{2}\n".format(
                sw.proces_id.name, sw.codi_sollicitud, sw.additional_info
            )
            msg = u"{0}\n{1}".format(pol.observacions, info)
            pol.write({'observacions': msg}, context=context)

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitching, self).write(cursor, uid, ids, vals, context=context)
        if 'step_ids' in vals:
            self.update_ff_accio_pendent_comerdist(cursor, uid, ids, context=context)
            for sw in self.browse(cursor, uid, ids):
                pas = self.get_pas(cursor, uid, sw, context=context)
                if not pas:
                    continue
                model = "giscedata.switching.{0}.{1}".format(
                    sw.proces_id.name.lower(), sw.step_id.name)
                self.update_additional_info(cursor, uid, pas.id, model, context)
        return res

    def _ff_get_polissa_ref_id(self, cursor, uid, ids, field_name, arg, context=None):

        if context is None:
            context = {}

        res = dict.fromkeys(ids, None)
        for case in self.read(cursor, uid, ids, ['ref', 'cups_polissa_id']):
            if case.get('ref', False):
                model, ref_id = case['ref'].split(',')
                if model == 'giscedata.polissa':
                    res[case['id']] = int(ref_id)

            if not res[case['id']] and case.get('cups_polissa_id',False):
                res[case['id']] = case['cups_polissa_id'][0]

        return res

    _STORE_GET_STEP = {
        'giscedata.switching': (lambda self, cr, uid, ids, c=None: ids,
                                ['step_ids'], 10),
        'giscedata.switching.step.info': (_get_sw_from_step_info, [], 10),
        }

    _STORE_WHEREIAM = {
        'giscedata.switching': (lambda self, cr, uid, ids, c=None: ids,
                                ['cups_id'], 10)
        }

    _STORE_REF = {
        'giscedata.switching': (lambda self, cr, uid, ids, c=None: ids,
                                ['ref'], 10)
        }

    _WHEREIAM = [('comer', 'Comercialitzadora'),
                 ('distri', 'Distribuïdora'),
                 ('ND', 'No disponible')]

    _columns = {
        'case_id': fields.many2one('crm.case', 'Cas'),
        'conversation_id': fields.many2one('poweremail.conversation',
                                           'Conversa'),
        'accio_pendent': fields.boolean("Pendent de l'acció de l'usuari",
                                        readonly=True),
        'accio_pendent_comerdist': fields.selection(
            string="Agente con accion pendiente",
            selection=[('comer', "Comercialitzadora"), ("distri", "Distribuidora")], select=2
        ),
        'validacio_pendent': fields.boolean('Pendent de validar',
                                            readonly=True),
        'enviament_pendent': fields.function(_ff_enviament_pendent,
                                             method=True,
                                             string="Pendent d'enviar",
                                             type="boolean", readonly=True,
                                             fnct_search=_ff_send_pendent_search),
        'obrir_cas_pendent': fields.boolean("Pendent d'obrir", readonly=True),
        'notificacio_pendent': fields.function(
            _ff_notificacio_pendent, string="Pendent de notificar",
            method=True, type="boolean", readonly=True,
            fnct_search=_ff_notificacio_pendent_search
        ),
        'documents_solicitud_adjunt': fields.function(
            _ff_document_adjunt_solicitud, method=True, string="Document adjunt", type="boolean", readonly=True,
            store={'giscedata.switching': (lambda self, cr, uid, ids, c=None: ids, ['step_ids'], 10)}
        ),

        # capçalera
        'company_id': fields.many2one('res.partner', u'Companyia',
                                      readonly=True),
        'codi_sollicitud': fields.char(u'Codi sol·licitud', size=12,
                                       readonly=True),
        'data_sollicitud': fields.date(u'Data de sol·licitud', required=True,
                                       select=1),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS', size=22),
        'cups_input': fields.char('CUPS', size=22),
        # Cups_input es un camp per permetre guardar el codi de CUPS que no
        # existeixen a la database (utilitzat en importar CUPS incorrectes)
        'proces_id': fields.many2one('giscedata.switching.proces', u'Procés',
                                     required=True, readonly=True,
                                     states={'draft': [('readonly', False)]}),
        'step_id': fields.function(_ff_get_step, method=True,
                                   string='Pas', type='many2one',
                                   obj='giscedata.switching.step',
                                   readonly=True,
                                   store=_STORE_GET_STEP),

        'step_description': fields.related(
            'step_id', 'description', type='char', relation='giscedata.switching.step',
            string='Descripicio Pas', size=64
        ),
        'cups_polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa'),
        'ref_contracte': fields.char('Ref. Contracte', size=12, required=True),
        'comer_sortint_id': fields.many2one('res.partner',
                                            'Comercialitzadora sortint'),
        'whereiam': fields.function(_ff_whereiam, method=True,
                                 string="On som", type='selection',
                                 selection=_WHEREIAM,
                                 size=6, readonly=True,
                                 store=_STORE_WHEREIAM),
        'stage_ids': fields.function(_ff_get_stages, method=True,
                                     string='Etapes', type='many2many',
                                     relation='giscedata.switching.stage'),
        'finalitzat': fields.function(_ff_get_final, method=True,
                                      string="Finalitzat", type='boolean',
                                      store=_STORE_GET_STEP),
        #Contacte client
        'titular_polissa': fields.function(_ff_get_contacte_polissa,
                                           method=True, string='Client',
                                           type='many2one', obj='res.partner',
                                           readonly=True,
                                           fnct_search=_ff_titular_search),
        'nif_titular_polissa': fields.function(_ff_get_contacte_polissa,
                                               method=True, string='NIF',
                                               type='char', size='11',
                                               readonly=True,
                                               fnct_search=_ff_nif_titular_polissa_search),
        'dir_pagador_polissa': fields.function(_ff_get_contacte_polissa,
                                               method=True, type='many2one',
                                               string='Adreça Pagador',
                                               obj='res.partner.address',
                                               readonly=True),
        'tel_pagador_polissa': fields.function(_ff_get_contacte_polissa,
                                               method=True, string='Telèfon',
                                               type='char', size='20',
                                               readonly=True),
        'mail_pagador_polissa': fields.function(_ff_get_contacte_polissa,
                                                method=True, string='E-mail',
                                                type='char', size='50',
                                                readonly=True),
        # Per poder buscar casos de rebuig
        'rebuig': fields.function(_ff_get_rebuig, method=True, type='boolean',
                                  string=u'Sol·licitud rebutjada',
                                  fnct_search=_ff_rebuig_search,
                                  readonly=True),
        # Informació adicional sobre el pas
        'additional_info': fields.char(u'Informació Adicional', size=400),

        'polissa_ref_id': fields.function(_ff_get_polissa_ref_id, method=True, string='Id de pòlissa referenciada', type='many2one', relation="giscedata.polissa", store=_STORE_REF),
    }

    def _get_id_company(self, cr, uid, context=None):
        if not context:
            context = {}
        user = self.pool.get('res.users').browse(cr, uid, uid)
        if not user.company_id.partner_id.ref:
            raise osv.except_osv(_(u"Error"),
                        _(u"Cal configurar el codi REE de l'empresa %s") %
                         user.company_id.partner_id.name)
        return user.company_id.partner_id.id

    def _get_default_section(self, cursor, uid, context=None):
        '''returns SW section id'''

        section_obj = self.pool.get('crm.case.section')
        search_params = [('code', '=', 'SW')]
        section_ids = section_obj.search(cursor, uid, search_params)
        if not section_ids:
            err = _(u"No s'ha trobat la secció del CRM amb codi 'SW'")
            raise osv.except_osv(u'Atenció', err)

        return section_ids[0]

    def _get_default_codi_sollicitud(self, cr, uid, context=None):
        '''Genera el codi de sol·licitud'''
        if not context:
            context = {}
        data = time.strftime('%Y%m%d')
        seq_obj = self.pool.get('ir.sequence')
        seq_code = 'giscedata.switching.sollicitud'
        codi = int(seq_obj.get_next(cr, uid, seq_code))
        return "%s%04d" % (data, codi % 10000)

    _defaults = {
        'data_sollicitud': lambda *a: time.strftime('%Y-%m-%d'),
        'codi_sollicitud': _get_default_codi_sollicitud,
        'company_id': _get_id_company,
        'accio_pendent': lambda *a: False,
        'validacio_pendent': lambda *a: False,
        'enviament_pendent': lambda *a: False,
        'state': lambda *a: 'draft',
        'notificacio_pendent': lambda *a: False,
        'section_id': _get_default_section,

    }

    def _check_comer_sortint(self, cursor, uid, ids, context=None):

        for sw in self.read(cursor, uid, ids, ['cups_id', 'cups_polissa_id', 'whereiam', 'proces_id', 'comer_sortint_id'], context=context):
            if sw['cups_id'] and sw['cups_polissa_id']:
                if (sw['whereiam'] == 'distri' and
                        sw['proces_id'] and sw['proces_id'][1] in ('C1', 'C2') and
                        not sw['comer_sortint_id']):
                    return False
        return True

    def _cnt_steps_proces(self, cursor, uid, ids):

        for sw in self.browse(cursor, uid, ids):
            step_ids = [x.id for x in sw.step_ids
                        if x.proces_id.id != sw.proces_id.id]
            if step_ids:
                return False
        return True

    _constraints = [(_check_comer_sortint,
                     _(u"No s'ha trobat la comercialitzadora sortint"),
                     ['comer_sortint_id']),
                    (_cnt_steps_proces,
                     u"No es pot assignar un codi de procés "
                     u"diferent a l\'assignat als passos",
                     ['steps_ids', 'proces_id'])]

GiscedataSwitching()


class GiscedataSwitchingStepInfo(osv.osv):

    _name = 'giscedata.switching.step.info'
    _desc = 'Passos dels fluxes de switching'

    def create(self, cursor, uid, vals, context=None):

        if context is None:
            context = {}

        proces_obj = self.pool.get('giscedata.switching.proces')
        res = super(GiscedataSwitchingStepInfo,
                    self).create(cursor, uid, vals, context=context)

        step = self.browse(cursor, uid, res, context=context)
        if step.pas_id:
            pas_model = self.pool.get(step.pas_id.split(',')[0])
            pas_id = int(step.pas_id.split(',')[1])
            pas_vals = {'sw_id': step.sw_id.id}
            pas_model.write(cursor, uid, [pas_id], pas_vals)
            pas_model_name = step.pas_id.split(',')[0]
            self.update_notify_info(
                cursor, uid, step.sw_id.id, pas_model_name, pas_id, context)
        else:
            pas_model_name = ('giscedata.switching.%s.%s' %
                               (step.proces_id.name.lower(),
                                step.step_id.name))
            pas_model = self.pool.get(pas_model_name)
            if hasattr(pas_model, 'dummy_create'):
                #Check if we can create this step
                emisor_steps = proces_obj.get_emisor_steps(cursor, uid,
                                                   step.proces_id.name,
                                                   step.sw_id.whereiam,
                                                   context=context)
                if step.step_id.name not in emisor_steps:
                    raise osv.except_osv('Error',
                                 _(u"No es pot crear el pas %s "
                                   u"com a emisor") % (step.step_id.name))

                if context.get('step_is_rejectable', False) and context.get('option', False) == 'R':
                    motiu_obj = self.pool.get('giscedata.switching.motiu.rebuig')
                    motiu = motiu_obj.browse(cursor, uid, context['motiu_rebuig'])
                    pas_id = pas_model.dummy_create_reb(cursor, uid,
                                                        step.sw_id.id, [motiu],
                                                        context=context)
                else:
                    pas_id = pas_model.dummy_create(cursor, uid,
                                                    step.sw_id.id,
                                                    context=context)
                step.write({'pas_id': '%s,%s' % (pas_model_name, pas_id)})
                self.update_notify_info(
                    cursor, uid, step.sw_id.id, pas_model_name, pas_id, context)
        return res

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitchingStepInfo,
                    self).write(cursor, uid, ids, vals, context=context)

        for step in self.browse(cursor, uid, ids, context=context):
            if step.pas_id:
                pas_model = self.pool.get(step.pas_id.split(',')[0])
                pas_id = int(step.pas_id.split(',')[1])
                pas_vals = {'sw_id': step.sw_id.id}
                pas_model.write(cursor, uid, [pas_id], pas_vals)

        return res

    def unlink(self, cursor, uid, ids, context=None):
        if not isinstance(ids, (list, tuple)):
            ids = [ids]
        for step in self.read(cursor, uid, ids, ['pas_id'], context=context):
            if step.get('pas_id'):
                pas_model = self.pool.get(step.get('pas_id').split(',')[0])
                pas_id = int(step.get('pas_id').split(',')[1])
                pas_model.unlink(cursor, uid, [pas_id])
        return super(GiscedataSwitchingStepInfo,
                     self).unlink(cursor, uid, ids, context=context)

    def update_notify_info(
            self, cursor, uid, sw_id, model_name, pas_id, context=None):
        """
        Set default value to:
        - `enviament_pendent` of a GiscedataSwitchingStepHeader
            if PAS in EmisorSteps
        - `notificacio_pendent` of a GiscedataSwitchingStepHeader
            if PAS in config_var["sw_mail_user_notification_on_activation"]
        :param cursor:      OpenERP Cursor
        :param uid:         OpenERP User ID
        :type uid:          int
        :param sw_id:       GiscedataSwitching ID
        :type sw_id:        int
        :param model_name:  GiscedataSwitching Model Object Name
        :type model_name:   str
        :param pas_id:      GiscedataSwitching Model ID
        :type pas_id:       int
        :param context:     OpenERP Context
        :type context:      dict
        :return:            -
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        proces_obj = self.pool.get('giscedata.switching.proces')
        conf_obj = self.pool.get('res.config')
        pas_obj = self.pool.get(model_name)

        if context is None:
            context = {}

        pas = model_name.split('.')[-1]
        sw = sw_obj.browse(cursor, uid, sw_id)

        # Set enviament on sw.step.header after pas is created
        step_emissors = proces_obj.get_emisor_steps(cursor, uid,
                                                    sw.proces_id.name,
                                                    sw.whereiam,
                                                    context)
        enviament = (pas in step_emissors)

        # Set notificacio_pendent on sw.step.header after pas is created
        pas_br = self.pool.get(model_name).browse(cursor, uid, pas_id)
        notificacio = self.pool.get(
            "giscedata.switching.notificacio.config"
        ).check_is_notificable(cursor, uid, pas_br, context=context)

        if notificacio and sw.proces_id.name == 'B1' and sw.step_id.name == '02':
            sw01_obj = self.pool.get('giscedata.switching.b1.01')
            motiu = sw01_obj.get_motiu_baixa(
                cursor, uid, sw.id, context=context
            )
            # motiu '01', '02'...
            conf_obj = self.pool.get('res.config')

            motius_list = conf_obj.get(
                cursor, uid, 'sw_notify_motivos_baja', "all"
            )

            if motius_list != "all":
                try:
                    motius_list = literal_eval(motius_list)
                except ValueError:
                    logger.error('Error evaluating motius_list config value')
                    motius_list = []
                notificacio = motiu in motius_list

        header_id = pas_obj.read(
            cursor, uid, pas_id, ['header_id'])['header_id'][0]
        header_vals = {
            'enviament_pendent': enviament,
            'notificacio_pendent': notificacio,
        }
        header_obj.write(cursor, uid, header_id, header_vals)

    def _get_passos(self, cursor, uid, context=None):

        step_obj = self.pool.get('giscedata.switching.step')

        if not context:
            context = {}
        res = []
        step_ids = step_obj.search(cursor, uid, [], context=context)
        for step in step_obj.browse(cursor, uid, step_ids, context=context):
            step_model = step.get_step_model()
            step_name = '%s (%s)' % (step.proces_id.name, step.name)
            res.append((step_model, step_name))

        return res

    def _get_default_sw(self, cursor, uid, context=None):
        if not context:
            context = {}
        return context.get('sw_id', False)

    def _get_default_proces(self, cursor, uid, context=None):
        if not context:
            context = {}
        sw_id = context.get('sw_id', False)
        if sw_id:
            sw_obj = self.pool.get('giscedata.switching')
            return sw_obj.read(cursor, uid,
                               sw_id, ['proces_id'])['proces_id'][0]

        return False

    def _ff_get_info(self, cursor, uid, ids, field_name, arg, context=None):
        '''returns string with step info'''

        step_obj = self.pool.get('giscedata.switching.step')

        res = {}
        for step_info in self.browse(cursor, uid, ids, context=context):
            info = '%s (%s)' % (step_info.proces_id.name,
                                step_info.step_id.name)
            if step_info.pas_id:
                step_model = self.pool.get(step_info.pas_id.split(',')[0])
                step_id = int(step_info.pas_id.split(',')[1])
                step = step_model.browse(cursor, uid, step_id)
                info += ': %s (%s) -> %s (%s) ' % (step.emisor_id.name,
                                                 step.emisor_id.ref,
                                                 step.receptor_id.name,
                                                 step.receptor_id.ref)
                if 'rebuig' in step_model.fields_get_keys(cursor, uid):
                    if step.rebuig:
                        info += _(u"--Rebuig--")
                    else:
                        info += _(u"--Acceptació--")

            res[step_info.id] = info

        return res

    def get_pas(self, cursor, uid, sinfo_id, context=None):
        if context is None:
            context = {}
        if isinstance(sinfo_id, (list, tuple)):
            sinfo_id = sinfo_id[0]

        res = False
        sinfo = self.read(cursor, uid, sinfo_id, ['pas_id'], context=context)
        if sinfo.get('pas_id'):
            pas_model = self.pool.get(sinfo.get('pas_id').split(',')[0])
            pas_id = int(sinfo.get('pas_id').split(',')[1])
            res = pas_model.browse(cursor, uid, pas_id)
        return res

    _columns = {
        'step_id': fields.many2one('giscedata.switching.step',
                                   'Pas', required=True),
        'proces_id': fields.many2one('giscedata.switching.proces', 'Procés',
                                     required=True),
        'pas_id': fields.reference('Info del pas', selection=_get_passos,
                                   size=128),
        'sw_id': fields.many2one('giscedata.switching', 'Cas',
                                 ondelete="cascade",
                                 required=True, readonly=True),
        'name': fields.function(_ff_get_info, method=True,
                                type='char', size=200,
                                string='Info',
                                store=False)

    }

    _defaults = {
        'sw_id': _get_default_sw,
        'proces_id': _get_default_proces,
    }
GiscedataSwitchingStepInfo()


class GiscedataSwitching2(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    _columns = {
        'step_ids': fields.one2many('giscedata.switching.step.info', 'sw_id',
                                    'Passos generats',)
                                    #states={'draft': [('readonly', True)]})
    }

GiscedataSwitching2()


class GiscedataSwitchingPuntMesura(osv.osv):

    _name = 'giscedata.switching.pm'
    _desc = "(SW) Punt de mesura"

    def name_get(self, cursor, uid, ids, context=None):

        res = []
        for pmesura in self.browse(cursor, uid, ids, context=context):
            res.append((pmesura.id,
                        _(u'Punt de mesura %s (%s)') % (pmesura.tipus,
                                                        pmesura.id)))
        return res

    def create_from_xml(self, cursor, uid, xml, context=None):
        '''creates pms based on info in xml'''
        ap_obj = self.pool.get('giscedata.switching.aparell')
        mesura_obj = self.pool.get('giscedata.switching.mesura')

        pm_ids = []
        for xml_pm in xml.puntos_medida:
            pm_vals = {
                'tipus_moviment': xml_pm.tipo_movimiento,
                'tipus': xml_pm.tipo_pm,
                'mode_lectura': xml_pm.modo_lectura,
                'funcio_pm': xml_pm.funcion,
                'tensio_pm': xml_pm.tension_pm,
                'data': xml_pm.fecha_vigor,
                'data_alta': xml_pm.fecha_alta,
                'data_baixa': xml_pm.fecha_baja or False,
                'comentaris': xml_pm.comentarios,
            }
            pm_id = self.create(cursor, uid, pm_vals, context=context)
            pm_ids.append(pm_id)
            # Per cada aparell del punt de mesura
            pa_list = getattr(xml_pm, 'aparatos', [])
            for xml_ap in pa_list:
                ap_vals = {
                    'pm_id': pm_id,
                    'tipus': xml_ap.tipo_aparato,
                    'marca': xml_ap.marca_aparato,
                    'model': xml_ap.modelo_marca,
                    'tipus_moviment': xml_ap.tipo_movimiento,
                    'tipus_em': xml_ap.tipo_equipo_medida,
                    'propietat': xml_ap.tipo_propiedad_aparato,
                    'propietari': xml_ap.propietario,
                    'precinte': xml_ap.cod_precinto,
                    'periode_fabricacio': xml_ap.periodo_fabricacion,
                    'num_serie': xml_ap.numero_serie,
                    'funcio': xml_ap.funcion_aparato,
                    'num_integradors': xml_ap.num_integradores,
                    'constant_energia': xml_ap.constante_energia,
                    'constant_max': xml_ap.constante_maximetro,
                    'enters': xml_ap.ruedas_enteras,
                    'decimals': xml_ap.ruedas_decimales,
                    }
                ap_id = ap_obj.create(cursor, uid, ap_vals, context=context)
                #Per cada mesura
                med_list = getattr(xml_ap, 'medidas', [])
                for xml_mesura in med_list:
                    mesura_vals = {
                        'aparell_id': ap_id,
                        'tipus_dh': xml_mesura.tipo_dhedm,
                        'periode': xml_mesura.periodo,
                        'magnitud': xml_mesura.magnitud_medida,
                        'origen': xml_mesura.procedencia,
                        'lectura': xml_mesura.ultima_lectura_firme,
                        'data_lectura': xml_mesura.fecha_lectura_firme,
                        'anomalia': xml_mesura.anomalia,
                        'text_anomalia': xml_mesura.comentarios
                        }
                    mesura_obj.create(cursor, uid, mesura_vals, context=context)
        return pm_ids

    def generar_xml(self, cursor, uid, pas, context=None):

        mesura_obj = self.pool.get('giscedata.switching.mesura')
        pm_list = []
        sw = pas.sw_id
        for pm in pas.pm_ids:
            #Per cada aparell del punt de mesura
            aparatos = c1.Aparatos()
            aparato_list = []
            for aparell in pm.aparell_ids:
                modelo = c1.ModeloAparato()
                modelo.feed({
                    'tipo_aparato': aparell.tipus,
                    'marca_aparato': aparell.marca,
                    'modelo_marca': aparell.model
                    })
                datos = c1.DatosAparato()
                datos.feed({
                    'periodo_fabricacion': aparell.periode_fabricacio,
                    'numero_serie': aparell.num_serie,
                    'funcion_aparato': aparell.funcio,
                    'num_integradores': aparell.num_integradors,
                    'constante_energia': aparell.constant_energia,
                    'constante_maximetro': aparell.constant_max,
                    'ruedas_enteras': aparell.enters,
                    'ruedas_decimales': aparell.decimals,
                })

                # Per cada mesura de l'aparell
                mesures_xml = []
                mesures_ids = [x.id for x in aparell.mesura_ids]
                mesures_data = mesura_obj.read(cursor, uid, mesures_ids,
                                               ['tipus_dh', 'periode',
                                                'magnitud', 'origen',
                                                'lectura', 'data_lectura',
                                                'anomalia', 'text_anomalia'])
                for mesura_data in mesures_data:
                    mesura_vals = {
                        'tipo_dhedm': mesura_data['tipus_dh'],
                        'periodo': mesura_data['periode'],
                        'magnitud_medida': mesura_data['magnitud'],
                        'procedencia': mesura_data['origen'],
                        'ultima_lectura_firme': mesura_data['lectura'],
                        'fecha_lectura_firme': mesura_data['data_lectura'],
                        'anomalia': mesura_data['anomalia'],
                        'comentarios': mesura_data['text_anomalia'],
                    }
                    medida = c1.Medida()
                    medida.feed(mesura_vals)
                    mesures_xml.append(medida)
                medidas = c1.Medidas()
                medidas.feed({'medida_list': mesures_xml})

                aparato = c1.Aparato()
                aparato_feed = {
                    'modelo_aparato': modelo,
                    'tipo_movimiento': aparell.tipus_moviment,
                    'tipo_equipo_medida': aparell.tipus_em,
                    'tipo_propiedad_aparato': aparell.propietat,
                    'cod_precinto': aparell.precinte,
                    'datos_aparato': datos,
                    'medidas': medidas
                }
                aparato.feed(aparato_feed)
                aparato_list.append(aparato)
            aparatos.feed({'aparato_list': aparato_list})

            punt = c1.PuntoDeMedida()
            cod_pm = sw.cups_id.name
            if len(cod_pm) == 20:
                cod_pm = cod_pm + "0F"
            punt.feed({
                'cod_pm': cod_pm,
                'tipo_movimiento': pm.tipus_moviment,
                'cod_pm_principal': cod_pm,
                'tipo_pm': pm.tipus,
                'modo_lectura': pm.mode_lectura,
                'funcion': pm.funcio_pm,
                'tensio_pm': pm.tensio_pm,
                'fecha_alta': pm.data_alta,
                'fecha_vigor': (hasattr(pas, 'data_activacio')
                               and pas.data_activacio or pm.data),
                'aparatos': aparatos,
                'fecha_baja': pm.data_baixa,
                'comentarios': pm.comentaris,
            })
            pm_list.append(punt)
        punts_mesura = c1.PuntosDeMedida()
        punts_mesura.feed({
            'punto_de_medida_list': pm_list
        })
        return punts_mesura

    def dummy_create(self, cursor, uid, sw, context=None):

        ap_obj = self.pool.get('giscedata.switching.aparell')
        mes_obj = self.pool.get('giscedata.switching.mesura')

        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        #Create PM's
        comptador = (polissa.comptadors and
                     polissa.comptadors[0] or
                     False)

        mode_lectura = '1'
        if comptador:
            if comptador.tg_cnc_conn:
                mode_lectura = '4'
            elif comptador.polissa.potencia > 50:
                mode_lectura = '4'

        proces = sw.proces_id.name
        step = sw.step_id.name
        if proces == 'A3' or (proces in ['C1', 'C2'] and step == '05'):
            tipo_mov = 'A'
        elif proces == 'B1' or (proces in ['C1', 'C2'] and step == '06'):
            tipo_mov = 'B'
        else:
            tipo_mov = 'M'

        pm_vals = {
            'tipus_moviment': tipo_mov,
            'tipus': polissa.agree_tipus,
            'mode_lectura': mode_lectura,
            'funcio_pm': 'P',
            'tensio_pm': polissa.tensio,
            'data': today,
            'data_alta': polissa.data_alta,
            'data_baixa': (comptador and comptador.data_baixa)
                          or polissa.data_baixa,
            }
        pm_id = self.create(cursor, uid, pm_vals)
        #Create aparells
        if comptador:
            periode_fabricacio = (comptador.serial and comptador.serial.date[:4] or
                                  comptador.data_alta and comptador.data_alta[:4] or
                                  "0000")
            ctx = context.copy()
            ctx.update({'aparell_model': "giscedata.lectures.comptador"})
            marca_model = ap_obj.get_marca_model(cursor, uid, [comptador.id],
                                                 context=ctx)
            propietat = (comptador.propietat == 'empresa' and '1' or '2')
            if comptador.active:
                moviment = 'CX'
            else:
                moviment = 'DX'
            ap_vals = {
                'pm_id': pm_id,
                'tipus': 'CA',
                'marca': marca_model[comptador.id]['marca'],
                'model': marca_model[comptador.id]['model'],
                'tipus_moviment': moviment,
                'tipus_em': 'L00',
                'propietat': propietat,
                'precinte': '0000',
                'periode_fabricacio': periode_fabricacio,
                'num_serie': comptador.name,
                'funcio': 'M',
                'constant_energia': 1,
                'constant_max': 1,
                'enters': str(comptador.giro).count('0'),
                'decimals': 0,
                }
            mesures = ap_obj.get_mesures(cursor, uid, [comptador.id],
                                         polissa.tarifa.id, context=context)
            #El valor de numero de integradores es la quantitat de mesures
            ap_vals.update({'num_integradors': len(mesures)})
            ap_id = ap_obj.create(cursor, uid, ap_vals)
            for mesura in mesures:
                mesura.update({'aparell_id': ap_id})
                mes_obj.create(cursor, uid, mesura)

        icps = polissa.icps or []
        for icp in icps:
            #Si no te serial o intensitat no informem
            if not icp.name or not icp.intensitat:
                continue
            periode_fabricacio = (icp.serial and icp.serial.date[:4] or
                                  icp.data_alta and icp.data_alta[:4] or
                                  "0000")
            ctx = context.copy()
            ctx.update({'aparell_model': 'giscedata.polissa.icp'})
            marca_model = ap_obj.get_marca_model(cursor, uid, [icp.id],
                                                 context=ctx)
            propietat = (comptador and comptador.propietat == 'empresa' and '1'
                         or '2')
            ap_vals = {
                'pm_id': pm_id,
                'tipus': 'IP',
                'marca': marca_model[icp.id]['marca'],
                'model': marca_model[icp.id]['model'],
                'tipus_moviment': 'CX',
                'tipus_em': 'L00',
                'propietat': propietat,
                'precinte': '0000',
                'periode_fabricacio': periode_fabricacio,
                'num_serie': icp.name or comptador and comptador.name,
                'funcio': 'C'
                }
            ap_obj.create(cursor, uid, ap_vals)

        return pm_id

    _columns = {
        # punt de mesura
        'tipus_moviment': fields.selection(TIPUS_MOVIMENT, u"Tipus Mov."),
        'tipus': fields.selection(TIPUS_PM, u"Tipus Punt"),
        'mode_lectura': fields.selection(MODE_PM, u"Mode lectura"),
        'funcio_pm': fields.selection(FUNCIO_PM, u"Funció"),
        'tensio_pm': fields.integer(u'Tensió'),
        'data': fields.date(u"Data vigor"),
        'data_alta': fields.date(u"Data d'alta"),
        'data_baixa': fields.date(u"Data de baixa"),
        'comentaris': fields.char('Comentaris', size=4000),
    }

    _defaults = {
        'funcio_pm': lambda *a: 'P',
    }

GiscedataSwitchingPuntMesura()


class GiscedataSwitchingAparell(osv.osv):

    _name = 'giscedata.switching.aparell'
    _desc = '(SW) Aparell punt de mesura'

    def name_get(self, cursor, uid, ids, context=None):

        res = []
        for aparell in self.browse(cursor, uid, ids, context=context):
            res.append((aparell.id,
                        'Aparato %s' % (aparell.marca)))
        return res

    def get_marca_model(self, cursor, uid, ids, context=None):
        res = {}
        obj = context.get('aparell_model', 'giscedata.lectures.comptador')
        meter_obj = self.pool.get(obj)
        for aparell in meter_obj.browse(cursor, uid, ids):
            marca = (aparell.product_id and
                     aparell.product_id.name.split(' ')[0]
                     or '')
            model = (aparell.product_id and
                     aparell.product_id.name[len(marca):].strip()
                     or 'ND')
            res[aparell.id] = {'marca': marca,
                               'model': model}
            marcas = dict([(x[1], x[0]) for x in MARCA_APARATO])
            if marca in marcas:
                res[aparell.id]['marca'] = marcas[marca]
            else:
                res[aparell.id]['marca'] = '199'
        return res

    def create_mesures_vals(self, cursor, uid, lectures, magnitud, dh,
                       origen, num_periodes, context=None):
        res = []
        total = 0

        for periode in lectures:
            #Maximetres i excesos no mes retornen un valor
            if magnitud in ('EP', 'PM'):
                lectura = lectures[periode]
            else:
                lectura = lectures[periode]['actual']

            total += lectura['lectura']
            data_lect = lectura['name']
            if num_periodes != 1:
                vals = {
                    'tipus_dh': dh,
                    'periode': '%s%s' % (dh, periode[-1]),
                    'magnitud': magnitud,
                    'origen': origen,
                    'lectura': lectura['lectura'],
                    'data_lectura': data_lect,
                }
                res.append(vals)
        #Create total period
        if magnitud not in ('EP', 'PM'):
            total_vals = {
                    'tipus_dh': dh,
                    'periode': '%s0' % dh,
                    'magnitud': magnitud,
                    'origen': origen,
                    'lectura': total,
                    'data_lectura': data_lect,
                }
            res.append(total_vals)
        return res

    def get_mesures(self, cursor, uid, ids, tarifa_id, context=None):
        '''obtindre les mesures associades a l'aparell'''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        origen_obj = self.pool.get('giscedata.lectures.origen')
        tarifa = tarifa_obj.browse(cursor, uid, tarifa_id)
        res = []
        for meter in meter_obj.browse(cursor, uid, ids, context=context):
            num_periodes = tarifa.get_num_periodes()
            #No mes retorna el numero agrupat
            #Per tant s'ha de multiplicar
            #per 2 per obtindre el numero real en les 3.X
            if tarifa.name.startswith('3.'):
                num_periodes *= 2
            if tarifa.name in ('2.0DHS', '2.1DHS'):
                dh = '8'
            else:
                dh = str(num_periodes)
            ult_lect = meter_obj.data_ultima_lectura(cursor, uid,
                                                     [meter.id],
                                                     tarifa_id,
                                                     context=context)
            #Get lectures de reactiva
            mesures = []
            if ult_lect:
                #Lectures d'activa
                lectures_act = meter.get_lectures(tarifa_id, ult_lect)
                origen = origen_obj.read(cursor, uid,
                            lectures_act['P1']['actual']['origen_id'][0],
                            ['codi'])['codi']
                mesures.extend(self.create_mesures_vals(cursor, uid,
                                                   lectures_act,
                                                   'AE', dh, origen,
                                                   num_periodes,
                                                   context=context))
                #Lectures de reactiva
                lectures_react = meter.get_lectures(tarifa_id, ult_lect,
                                                    tipus='R')
                # Tests if it has got reactive measures
                if 'name' in lectures_react['P1'].get('actual', {}).keys():
                    mesures.extend(
                        self.create_mesures_vals(
                            cursor, uid, lectures_react, 'R1', dh, origen,
                            num_periodes, context=context
                        )
                    )
                #Si facturem per maximetro hem de passar les lectures també
                if meter.polissa.facturacio_potencia == 'max':
                    #La funcio de obtindre els maximetres els cerca dins un
                    #periode. Com que nosaltres volem els de un dia concret
                    #el que fem es restar un dia a ult_lect
                    data_desde = datetime.strftime(datetime.strptime(ult_lect,
                                                                '%Y-%m-%d') -
                                               timedelta(days=1), '%Y-%m-%d')
                    lectures_max = meter.get_maximetres_per_facturar(tarifa_id,
                                                             data_desde,
                                                             ult_lect,
                                                             context=context)
                    new_lect = {}
                    for periode in lectures_max:
                        new_lect.update({
                            periode: {
                                'lectura': lectures_max[periode]['maximetre'],
                                'name': lectures_act['P1']['actual']['name']
                            }
                        })

                    mesures.extend(self.create_mesures_vals(cursor, uid,
                                                       new_lect,
                                                       'PM', dh, origen,
                                                       num_periodes,
                                                       context=context))
                #Si es una 6.X hem d'afegir els excesos tambe
                if tarifa.name.startswith('6.'):
                    lectures_exc = meter.get_excesos_per_facturar(tarifa_id,
                                                              ult_lect,
                                                              ult_lect,
                                                              context=context)
                    new_lect = {}
                    for periode in lectures_max:
                        new_lect.update({
                            periode: {
                                'lectura': lectures_max[periode]['maximetre'],
                                'name': lectures_act['P1']['actual']['name']
                            }
                        })
                    mesures.extend(self.create_mesures_vals(cursor, uid,
                                                       new_lect,
                                                       'EP', dh, origen,
                                                       num_periodes,
                                                       context=context))
        return mesures

    def get_lectures_vals(self, cursor, uid, ids, comptador_id=None,
                          tarifa_atr=None, context=None):
        """Genera val lectures apte per carregar a ERP"""
        if context is None:
            context = {}
        if not isinstance(ids, (list, tuple)):
            ids = [ids]

        res = {'lectures': [], 'maximetres': []}

        mesura_obj = self.pool.get('giscedata.switching.mesura')
        origen_obj = self.pool.get('giscedata.lectures.origen')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')

        if not comptador_id or not tarifa_atr:
            return res

        # La tarifa
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid, tarifa_atr)

        if not tarifa_id:
            return res

        aparell_vals = self.read(cursor, uid, ids, ['mesura_ids', 'num_serie'])
        for apa in aparell_vals:
            # busquem els "periodes" que ens arriben
            fields = ['lectura', 'magnitud', 'origen', 'periode',
                          'text_anomalia', 'anomalia', 'tipus_dh',
                          'data_lectura']
            mesures_vals = mesura_obj.read(
                cursor, uid, apa['mesura_ids'], fields, context=context
            )
            # Tots els periodes que ens arriben
            periodes = {}
            periodes.update(
                {'AE': [m['periode'] for m in mesures_vals
                        if m['magnitud'] == 'AE']}
            )
            periodes.update(
                {'R1': [m['periode'] for m in mesures_vals
                        if m['magnitud'] == 'R1']}
            )
            for mesura_vals in mesures_vals:
                vals = {}
                data_lectura = mesura_vals['data_lectura']
                # Observacions
                observacions = ''
                if mesura_vals['anomalia']:
                    avals = (mesura_vals['anomalia'],
                             mesura_vals['text_anomalia'])
                    anom_txt = _(u"Anomalia %s: %s\n") % avals
                    observacions += anom_txt
                vals.update({'observacions': observacions})

                #TIPUS
                # Període
                # No creeem lectures pels totalitzadors
                periode = mesura_vals['periode'][-1]
                magnitud = mesura_vals['magnitud']
                if periode == '0':
                    if magnitud == 'PM':
                        periode = '1'
                    elif mesura_vals['periode'] == '10':
                        pers = periodes[magnitud]
                        if not [p for p in pers if p in ['21', '81']]:
                            # P1 de 2.0A
                            periode = '1'
                    else:
                        continue

                per_name = "P%s" % periode
                tipus_per = TIPUS_PERIODES.get(magnitud)
                if not tipus_per:
                    continue
                per_search = [('tarifa', '=', tarifa_id),
                              ('tipus', '=', tipus_per),
                              ('name', '=', per_name)]
                per_id = periode_obj.search(cursor, uid, per_search)
                if not per_id:
                    continue
                # LA RESTA
                vals.update({'comptador': (comptador_id,),
                             'lectura': mesura_vals['lectura'],
                             'name': data_lectura,
                             'periode': (per_id[0],),
                             'codi_periode': mesura_vals['periode'],
                             'incidencia_id': False,
                             })
                if magnitud in ['PM', 'EP']:
                    #LECTURA DE POTÈNCIA
                    lectura_pot = mesura_vals['lectura'] / 1000
                    # Protecció maxímetre en watts
                    if lectura_pot < 1 and lectura_pot > 0:
                        lectura_pot = mesura_vals['lectura']
                    vals.update({'exces': 0,
                                 'lectura': lectura_pot,
                                 }),
                    res['maximetres'].append(vals)
                else:
                    #LECTURA D'ENERGIA
                    # Origen
                    origen_search = [('codi', '=', mesura_vals['origen'])]
                    origen = origen_obj.search(cursor, uid, origen_search)
                    if not origen:
                        continue
                    vals.update({'origen_id': (origen[0],)})

                    #Activa/Reactiva
                    tipus = magnitud[0]
                    vals.update({'tipus': (tipus,)})
                    res['lectures'].append(vals)

        return res

    _columns = {
        'pm_id': fields.many2one('giscedata.switching.pm',
                                 u"Punt de mesura", required=True,
                                 ondelete='cascade'),
        'tipus': fields.selection(TIPO_APARATO, u"Tipus"),
        'marca': fields.selection(MARCA_APARATO, u"Marca"),
        'model': fields.char("Model", size=30),
        'tipus_moviment': fields.selection(TIPO_MOVIMIENTO_APARATO,
                                           u"Tipus de moviment"),
        'tipus_em': fields.selection(TIPO_EM_APARATO,
                                     u"Tipus d'equip"),
        'propietat': fields.selection(TIPO_PROPIEDAD_APARATO,
                                      u"Propietat"),
        'propietari': fields.char(u"Propietari", size=20),
        'dh_activa': fields.selection(TIPO_DH_APARATO, u"DH Activa"),
        'dh_max': fields.selection(TIPO_DH_MAX, u"DH Max."),
        'precinte': fields.char(u"Precinte", size=8),
        'periode_fabricacio': fields.char(u"Període de fabricació", size=4),
        'num_serie': fields.char('Num. Serie', size=18),
        'funcio': fields.selection(FUNCION_APARATO, u"Funció"),
        'num_integradors': fields.integer(u"Num. Integradors"),
        'constant_energia': fields.float(u"Constant energia", digits=(16, 3)),
        'constant_max': fields.float(u"Constant max.", digits=(16, 3)),
        'enters': fields.integer(u"Rodes enteres"),
        'decimals': fields.integer(u"Rodes decimals"),
    }

    _defaults = {
        'tipus': lambda *a: 'CA',
        'funcio': lambda *a: 'M',
        'num_integradors': lambda *a: 1,
        'constant_energia': lambda *a: 1,
        'constant_max': lambda *a: 1,
    }
GiscedataSwitchingAparell()


class GiscedataSwitchingMesura(osv.osv):

    _name = 'giscedata.switching.mesura'
    _desc = '(SW) Mesura aparell punt de mesura'
    _order = 'magnitud, periode'

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        for mesura in self.browse(cursor, uid, ids, context=context):
            res.append((mesura.id,
                        '%s %s' % (mesura.periode,
                                   mesura.magnitud)))
        return res

    _columns = {
        'aparell_id': fields.many2one('giscedata.switching.aparell',
                                 u"Aparell", required=True,
                                 ondelete='cascade'),
        'tipus_dh': fields.selection(TIPO_DH_APARATO, u"Tipus DH"),
        'periode': fields.selection(PERIODO, u"Període"),
        'magnitud': fields.selection(MAGNITUD, u"Magnitud"),
        'origen': fields.selection(PROCEDENCIA, u"Origen"),
        'lectura': fields.float(u"Última Lectura", digits=(16, 2)),
        'data_lectura': fields.date(u"Data Última Lectura"),
        'anomalia': fields.selection(ANOMALIA_MESURA, u"Anomalia"),
        'text_anomalia': fields.char(u"Text anomalia", size=250)
    }

GiscedataSwitchingMesura()


class GiscedataSwitchingAparell2(osv.osv):

    _name = 'giscedata.switching.aparell'
    _inherit = 'giscedata.switching.aparell'

    _columns = {
        'mesura_ids': fields.one2many('giscedata.switching.mesura',
                                      'aparell_id', u"Mesures"),
    }

GiscedataSwitchingAparell2()


class GiscedataSwitchingPuntMesura2(osv.osv):

    _name = 'giscedata.switching.pm'
    _inherit = 'giscedata.switching.pm'

    _columns = {
        'aparell_ids': fields.one2many('giscedata.switching.aparell',
                                       'pm_id', required=True),
    }

GiscedataSwitchingPuntMesura2()


class GiscedataSwitchingPotencia(osv.osv):

    _name = 'giscedata.switching.potencia'
    _desc = '(SW) Potència'
    _order = 'name'

    def generar_xml(self, cursor, uid, pas, context=None):

        potencies = c1.PotenciasContratadas()
        pots = dict([('p%s' % x.name[-1], x.potencia) for x in pas.pot_ids])
        potencies.feed(pots)
        return potencies

    def create_pots(self, cursor, uid, pots, context=None):
        '''Create pots
        @pots: ordered list of tuples containing (periode, potencia)
        return list of ids created'''
        if not pots:
            return []
        res = []
        for i in range(len(pots)):
            pot_vals = {
                    'name': 'P%s' % (i + 1),
                    'potencia': int(pots[i][1]),
                }
            created_pot_id = self.create(cursor, uid, pot_vals,
                                         context=context)
            res.append(created_pot_id)

        return res

    _columns = {
        'name': fields.char(u"Període", size=3),
        'potencia': fields.integer(u"Potència (W)"),
    }

GiscedataSwitchingPotencia()


class GiscedataSwitchingMotiuRebuig(osv.osv):

    _name = 'giscedata.switching.motiu.rebuig'

    def get_motiu(self, cursor, uid, code, context=None):
        '''returns motiu id with name = code'''
        code = code.lstrip("0")
        search_params = [('name', '=', code)]
        motiu_ids = self.search(cursor, uid, search_params)
        if motiu_ids:
            return motiu_ids[0]
        return False

    def get_all_motius(self, cursor, uid, process_name):
        ids = self.search(cursor, uid, [('proces_ids.name', '=', process_name)])
        Motiu = namedtuple('Motiu', ['id', 'name', 'text'])

        return [Motiu(x['id'], x['name'], x['text'])
                for x in self.read(cursor, uid, ids, ['name', 'text'])]

    _columns = {
        'proces_ids': fields.many2many('giscedata.switching.proces',
                                       'sw_motiu_rebuig_proces',
                                       'motiu_id', 'proces_id',
                                       'Procesos'),
        'name': fields.char('Codi', size=2),
        'text': fields.char('Descripció', size=200),
    }

GiscedataSwitchingMotiuRebuig()


class GiscedataSwitchingStage(osv.osv):
    '''Defines staging messages'''

    _name = 'giscedata.switching.stage'
    _desc = '(SW) Etapes'
    _rec_name = 'text'

    def get_stages(self, cursor, uid, proces, step, where, context=None):
        '''Returns all stages associated'''

        search_params = [('proces_id.name', '=', proces),
                         ('step_ids.name', '=', step),
                         ('where', '=', where)]

        return self.search(cursor, uid, search_params, context=context)

    _columns = {
        'proces_id': fields.many2one('giscedata.switching.proces', 'Procés'),
        'step_ids': fields.many2many('giscedata.switching.step',
                                     'sw_stage_step',
                                     'stage_id', 'step_id',
                                     'Passos'),
        'where': fields.selection([('distri', 'Distribució'),
                                   ('comer', 'Comercialització')], 'On som'),
        'text': fields.char('Descripció', translate=True, size=250)
    }

GiscedataSwitchingStage()


class GiscedataSwitchingRebuig(osv.osv):
    '''Classes general per definir els models:
    * RechazoATRDistribuidoras
    * RechazoAnulacion'''

    _name = 'giscedata.switching.rebuig'

    def onchange_motiu(self, cursor, uid, ids, motiu_id, context=None):

        motiu_obj = self.pool.get('giscedata.switching.motiu.rebuig')
        res = {}
        if motiu_id:
            motiu = motiu_obj.browse(cursor, uid, motiu_id)
            res['value'] = {'desc_rebuig': motiu.text}

        return res

    def generar_xml(self, cursor, uid, pas, context=None):

        if not context:
            context = {}

        sw = pas.sw_id
        pas_name = pas.sw_id.proces_id.name
        if pas_name == 'R1':
            msg = r1.MensajeRechazoReclamacion()
        elif pas_name == 'D1':
            msg = d1.MensajeRechazo()
        elif pas_name == 'A1':
            msg = a1.MensajeActualizacionRegistroAutoconsumo()
        else:
            msg = c1.MensajeRechazo()

        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # rebuig
        list_rebuig = []

        header = c1.Rechazos() if pas_name != 'A1' else a1.Rechazos()

        for pas_rebuig in pas.rebuig_ids:
            if pas_name == 'A1':
                rebuig = a1.Rechazo()
            else:
                rebuig = c1.Rechazo()

            rebuig_vals = {
                'secuencial': pas_rebuig.seq_rebuig,
                'codigo_motivo': pas_rebuig.motiu_rebuig.name.zfill(2),
                'comentarios': (pas_rebuig.desc_rebuig or
                                pas_rebuig.motiu_rebuig.text)
            }

            if pas_name == 'A1':
                rebuig_vals.update({'fecha_rechazo': pas.data_rebuig})

            rebuig.feed(rebuig_vals)
            list_rebuig.append(rebuig)

        header_vals = {
            'rechazo_list': list_rebuig,
        }
        if pas_name != 'A1':
            header_vals.update({
                'fecha_rechazo': pas.data_rebuig,
            })

        # Documents de registre
        doc_xml = pas.header_id.generar_xml_document(pas, context=context)
        if pas.document_ids:
            header_vals.update({'registros_documento': doc_xml})

        header.feed(header_vals)

        # missatge
        if pas_name in ('R1'):
            msg.feed({
                'cabecera_reclamacion': capcalera,
                'rechazos': header
            })

        elif pas_name == 'A1':
            registro = a1.ActualizacionRegistroAutoconsumo()
            registro.feed({
                'cau': pas.cau,
                'rechazos': header,
                'comentarios': pas.comentaris
            })

            msg.feed({
                'cabecera': capcalera,
                'actualizacion_registro_autoconsumo': registro
            })
        else:
            msg.feed({
                'cabecera': capcalera,
                'rechazos': header
            })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        motiu_obj = self.pool.get('giscedata.switching.motiu.rebuig')

        if not context:
            context = {}
        model = context.get('model', False)
        nom_pas = context.get('nom_pas', False)
        model_obj = self.pool.get(model)

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        model_fields = model_obj.fields_get_keys(cursor, uid)

        if 'rebuig' in model_fields:
            vals.update({'rebuig': True})
        if 'data_rebuig' in model_fields and getattr(xml, 'fecha_rechazo', None):
            vals.update({'data_rebuig': xml.fecha_rechazo})

        rebuig_xml_list = xml.rechazos
        rebuig_list = []
        for rebuig in rebuig_xml_list:
            motiu_id = motiu_obj.get_motiu(cursor, uid, rebuig.codigo_motivo)
            rebuig_vals = {
                'seq_rebuig': rebuig.secuencial,
                'motiu_rebuig': motiu_id,
                'desc_rebuig': rebuig.comentarios,
            }
            rebuig_list.append(self.create(cursor, uid, rebuig_vals))

        vals.update({'rebuig_ids': [(6, 0, rebuig_list)]})

        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = model_obj.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw, motius, context=None):
        rebuigs = []
        seq = 0
        for mot in motius:
            seq += 1
            reb_vals = {
                'motiu_rebuig': mot.id,
                'desc_rebuig': mot.text,
                'seq_rebuig': seq,
            }
            rebuigs.append(self.create(cursor, uid, reb_vals))
        return rebuigs

    _columns = {
        'seq_rebuig': fields.integer('Sequencial', required=True),
        'motiu_rebuig': fields.many2one('giscedata.switching.motiu.rebuig',
                                "Motiu de rebuig", required=True),
        'desc_rebuig': fields.char(u"Descripció", size=4000, required=True),
    }

    _defaults = {
        'seq_rebuig': lambda *a: 1,
    }

GiscedataSwitchingRebuig()


class GiscedataSwitchingIncidencia(osv.osv):
    '''Classe per definir incidencies'''

    _name = 'giscedata.switching.incidencia'

    def onchange_motiu(self, cursor, uid, ids, motiu, context=None):

        res = {}
        DICT_28 = dict(TABLA_28)
        desc = DICT_28.get(motiu, '')
        res['value'] = {'desc_incidencia': desc}
        return res

    def generar_xml(self, cursor, uid, pas, context=None):
        if not context:
            context = {}
        if not pas.incidencia_ids:
            raise osv.except_osv('Error', _(u"No hi ha cap incidència"))
        incidencies = []
        DICT_28 = dict(TABLA_28)
        for inc in pas.incidencia_ids:
            incidencia = c2.Incidencia()
            desc = DICT_28.get(inc.motiu_incidencia, '')
            inc_vals = {
                'secuencial': inc.seq_incidencia,
                'codigo_motivo': inc.motiu_incidencia,
                'comentarios': desc,
            }
            incidencia.feed(inc_vals)
            incidencies.append(incidencia)
        return incidencies

    def create_from_xml(self, cursor, uid, xml, context=None):
        if not context:
            context = {}
        inc_list = []
        for incidencia in xml.incidencias:
            inc_vals = {
                'seq_incidencia': incidencia.secuencial,
                'motiu_incidencia': incidencia.codigo_motivo.rjust(2, '0'),
                'desc_incidencia': incidencia.comentarios,
                }
            inc_list.append(self.create(cursor, uid, inc_vals))
        return inc_list

    _columns = {
        'seq_incidencia': fields.integer('Sequencial', required=True),
        'motiu_incidencia': fields.selection(TABLA_28, "Incidència",
                                             required=True),
        'desc_incidencia': fields.text(u"Comentaris", size=400,
                                       required=True)
    }

    _defaults = {
        'seq_incidencia': lambda *a: 1,
    }

GiscedataSwitchingIncidencia()


class GiscedataSwitchingAnulacio(osv.osv):
    """Anulacio de proces
    """
    _name = "giscedata.switching.anulacio"
    _auto = False

    def generar_xml(self, cursor, uid, pas, context=None):

        if not context:
            context = {}

        sw = pas.sw_id
        msg = c1.MensajeAnulacionSolicitud()
        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        msg.feed({
            'cabecera': capcalera,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref, pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        model = context.get('model', False)
        model_obj = self.pool.get(model)

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        pas_id = model_obj.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, model_obj._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        model = context.get('model', False)
        model_obj = self.pool.get(model)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        return model_obj.create(cursor, uid, vals, context=context)

GiscedataSwitchingAnulacio()


class GiscedataSwitchingAccAnulacio(osv.osv):

    _name = 'giscedata.switching.acc.anulacio'
    _auto = False

    def generar_xml(self, cursor, uid, pas, context=None):

        if not context:
            context = {}

        sw = pas.sw_id
        if context.get('hora_ac', False):
            msg = b1.MensajeAceptacionAnulacionBaja()
        else:
            msg = b1.MensajeAceptacionAnulacion()
        #capçalera
        capcalera = pas.header_id.generar_xml(pas, context=context)
        # acceptació
        dades = b1.AceptacionAnulacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
        })
        if context.get('hora_ac', False):
            dades.feed({
                'hora_aceptacion': pas.hora_acceptacio,
            })
        # missatge
        msg.feed({
            'cabecera': capcalera,
            'aceptacion_anulacion': dades,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        if not context:
            context = {}
        model = context.get('model', False)
        model_obj = self.pool.get(model)

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        if 'rebuig' in model_obj.fields_get_keys(cursor, uid):
            vals.update({'rebuig': False})

        vals.update({
            'data_acceptacio': xml.fecha_aceptacion,
        })
        if context.get('hora_ac', False):
            vals.update({
                'hora_acceptacio': xml.hora_aceptacion,
            })

        pas_id = model_obj.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, model_obj._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id

        model = context.get('model', False)
        model_obj = self.pool.get(model)

        #By default we will dummy create without rejection
        #If rejected, do it manually, no dummy here :)
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        if 'rebuig' in model_obj.fields_get_keys(cursor, uid):
            vals.update({'rebuig': False})
        vals.update({
            'data_acceptacio': today,
            })
        if context.get('hora_ac', False):
            now = datetime.strftime(datetime.now(), '%H:%M:%S')
            vals.update({
                'hora_acceptacio': now,
            })

        return model_obj.create(cursor, uid, vals)

GiscedataSwitchingAccAnulacio()


class GiscedataSwitchingNotificacioComerSortint(osv.osv):

    _name = 'giscedata.switching.not.comer'
    _auto = False

    def generar_xml(self, cursor, uid, pas, context=None):

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})
        sw = pas.sw_id

        msg = c1.MensajeActivacionComercializadorSaliente()

        #capçalera
        capcalera = pas.header_id.generar_xml(pas, context=ctx)
        # Dades activació
        dades_notificacio = c1.DatosNotificacion()
        dades_notificacio.feed({
            'fecha_activacion': pas.data_activacio,
            'ind_bono_social': pas.ind_bono_social
        })
        # Contracte
        idcontracte = c1.IdContrato()
        nom_ctr = filter(lambda x: x.isdigit(), sw.ref_contracte)
        idcontracte.feed({
            'cod_contrato': nom_ctr
        })

        contracte = c1.Contrato()
        contracte.feed({
            'id_contrato': idcontracte,
        })

        punts_mesura = pas.header_id.generar_xml_pm(pas)

        # Notificacio
        notificacio = c1.NotificacionComercializadorSaliente()
        notificacio.feed({
            'datos_notificacion': dades_notificacio,
            'contrato': contracte,
            'puntos_de_medida': punts_mesura,
        })
        msg.feed({
            'cabecera': capcalera,
            'notificacion_comercializador_saliente': notificacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        if not context:
            context = {}
        model = context.get('model', False)
        model_obj = self.pool.get(model)

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        pm_ids = header_obj.create_from_xml_pm(cursor, uid, xml,
                                               context=context)

        vals.update({
            'data_activacio': xml.datos_notificacion.fecha_activacion,
            'ind_bono_social': xml.datos_notificacion.ind_bono_social,
            'pm_ids': [(6, 0, pm_ids)],
            })

        pas_id = model_obj.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, model_obj._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})
        model = context.get('model', False)
        model_obj = self.pool.get(model)

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        if polissa.modcontractual_activa and polissa.modcontractual_activa.data_inici:
            activation_date = polissa.modcontractual_activa.data_inici
            date_act = datetime.strptime(activation_date, "%Y-%m-%d") - timedelta(days=1)
            date_act = date_act.strftime("%Y-%m-%d")
        else:
            date_act = datetime.strftime(datetime.now(), '%Y-%m-%d')

        pm_ids = header_obj.dummy_create_pm(cursor, uid, sw, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=ctx)
        bono_social = 'N'
        if sw.cups_polissa_id and sw.cups_polissa_id.bono_social == '1':
            bono_social = 'S'
        vals.update({
            'data_activacio': date_act,
            'ind_bono_social': bono_social,
            'pm_ids': [(6, 0, [pm_ids])],
            })

        return model_obj.create(cursor, uid, vals)

GiscedataSwitchingNotificacioComerSortint()


class GiscedataSwitchingLectura(osv.osv):

    _name = 'giscedata.switching.lectura'
    _desc = '(SW) Lectures Aportades'
    _order = 'name'

    def get_measures_list(self, cursor, uid, energy_ids, maximeters_ids,
                          pool=False, context=None):
        """ Generates a SW friendly list of tuples for
        giscedata.switching.lectura model from meter measures (
        giscedata.lectures.lectura and giscedata.lectures.potencia family models
        )
        :param energy_ids: ids of giscedata.lectures.lectura/pool measures
        :param maximeters_ids: ids of giscedata.lectures.potencia/pool measures
        :param pool: either measures are from pool models (True) or not (False)
        :return: measure_date,
                 dh_code,
                 list of tuples (period, magnitud, measure) to inject in
                    create_lect function
        """
        obj_sufix = pool and '.pool' or ''

        fare_obj = self.pool.get('giscedata.polissa.tarifa')
        period_obj = self.pool.get('giscedata.polissa.tarifa.periodes')

        measure_obj_name = 'giscedata.lectures.lectura' + obj_sufix
        maximeter_obj_name = 'giscedata.lectures.potencia' + obj_sufix

        measure_obj = self.pool.get(measure_obj_name)
        maximeter_obj = self.pool.get(maximeter_obj_name)

        sw_measures = []
        dh_code = None
        measure_date = None

        for model, ids in [(measure_obj, energy_ids),
                           (maximeter_obj, maximeters_ids)]:
            measure_fields = ['periode', 'lectura', 'tipus', 'name', 'exces']
            if ids:
                measures = model.read(
                    cursor, uid, ids, measure_fields)
                for measure in measures:
                    per_id = measure['periode'][0]
                    per_vals = period_obj.read(
                        cursor, uid, per_id, ['name', 'tarifa']
                    )
                    per = per_vals['name']
                    tipus = measure.get('tipus', False)
                    lectura = float(measure['lectura'])
                    tarifa_name = per_vals['tarifa'][1]
                    if not dh_code:
                        dh_code = fare_obj.get_codi_dh(
                            cursor, uid, per_vals['tarifa']
                        )
                    if not tipus:
                        # maximeter
                        if '6.' in tarifa_name:
                            sw_type = 'EP'
                            lectura = float(measure['exces'])
                        else:
                            sw_type = 'PM'
                    elif tipus == 'A':
                        sw_type = 'AE'
                    elif tipus == 'R':
                        sw_type = 'R1'
                    if not measure_date:
                        measure_date = datetime.strptime(
                            measure['name'], '%Y-%m-%d'
                        )
                    sw_measures.append((per, sw_type, lectura))

        return measure_date, dh_code, sw_measures

    def create_lects(self, cursor, uid, lects, context=None):
        '''Create lects
        @lects: ordered list of tuples containing (periode, magnitud, lectura)
        return list of ids created'''
        res = []

        lects.sort()
        for i in range(len(lects)):
            lect_vals = {
                'name': 'P%s' % lects[i][0][-1],
                'magnitud': lects[i][1],
                'lectura': int(lects[i][2]),
            }
            created_lect_id = self.create(cursor, uid, lect_vals,
                                          context=context)
            res.append(created_lect_id)

        return res

    def generar_xml(self, cursor, uid, pas, var_r1=None, context=None):
        """ var: is useful for R1 Variable Detalle Reclamacion model"""
        # lecturas
        lecturas = []
        num_periodes = pas.sw_id.cups_polissa_id.tarifa.get_num_periodes()
        if var_r1 is None:
            dh = pas.codi_dh
            lect_ids = pas.header_id.lect_ids
        else:
            dh = var_r1.codi_dh
            lect_ids = var_r1.lect_ids
            # Si venim de una VariableReclamacio i aquesta te un numero de
            # factura que es reclama, utilitzarem la tarifa d'aquesta factura
            # per determinar el numero de periodes
            if var_r1.num_factura:
                fact_obj = self.pool.get("giscedata.facturacio.factura")
                fact_ids = fact_obj.search(cursor, uid, [('origin', '=', var_r1.num_factura)])
                if len(fact_ids):
                    tarifa_id = fact_obj.read(cursor, uid, fact_ids[0], ['tarifa_acces_id'])
                    if tarifa_id['tarifa_acces_id']:
                        tid = tarifa_id['tarifa_acces_id'][0]
                        tarif_obj = self.pool.get('giscedata.polissa.tarifa')
                        num_periodes = tarif_obj.get_num_periodes(cursor, uid, [tid])
        if not dh:
            dh = '0'
        for lect in lect_ids:
            if num_periodes > 1:
                periode_dh = '%s%s' % (dh, lect.name[-1])
            else:
                periode_dh = '%s0' % dh
            if var_r1 is None:
                lectura = w1.LecturaAportada()
                lectura.feed(dict(
                    integrador=lect.magnitud,
                    tipo_codigo_periodo_dh=periode_dh,
                    lectura_propuesta=lect.lectura,
                ))
            else:
                lectura = r1.LecturaAportada()
                lectura.feed(dict(
                    integrador=lect.magnitud,
                    codigo_periodo_dh=periode_dh,
                    lectura_propuesta=lect.lectura,
                ))
            lecturas.append(lectura)
        return lecturas

    _columns = {
        'name': fields.selection(PERIODES_SEL, u"Període"),
        'magnitud': fields.selection(MAGNITUD, 'Magnitud', required=True),
        'lectura': fields.float(u"Lectura (kWh)", digits=(12, 2),
                                required=True),
    }

GiscedataSwitchingLectura()


class GiscedataSwitchingDocument(osv.osv):

    _name = 'giscedata.switching.document'
    _desc = '(SW) Documents aportats'

    def create_docs(self, cr, user, docs, context=None):
        '''Create docs
        @docs: unordered list of tuples containing (type, url)
        return list of ids created'''
        res = []
        if not docs:
            return res
        for index, doc in enumerate(docs):
            docs_vals = {
                'type': doc.tipo_doc_aportado,
                'url': doc.direccion_url,
            }
            created_doc_id = self.create(cr, user, docs_vals, context=context)

            res.append(created_doc_id)

        return res

    def generar_xml(self, cursor, uid, pas, context=None):

        docs_xml = c1.RegistrosDocumento()

        doc_list = []

        for doc in pas.document_ids:
            doc_xml = c1.RegistroDoc()
            doc_xml.feed({
                'tipo_doc_aportado': doc.type,
                'direccion_url': doc.url,
            })

            doc_list.append(doc_xml)
        docs_xml.feed({'registro_doc_list': doc_list})
        return docs_xml

    def name_get(self, cursor, uid, ids, context=None):
        res = []
        for item in self.read(cursor, uid, ids, ['url']):
            res += [(item['id'], item['url'])]
        return res

    _columns = {
        'type': fields.selection(TABLA_61, u'Tipus de Document', required=True),
        'url': fields.char(u'URL del document', size=1024, required=True),
    }

GiscedataSwitchingDocument()


class GiscedataSwitchingTelefon(osv.osv):

    _name = 'giscedata.switching.telefon'
    _desc = 'Telefons ATR'

    def create_from_xml(self, cursor, uid, telefons_xml, context=None):
        tel_ids = []
        for (prefix, numero) in telefons_xml:
            tel_ids.append(
                self.create(
                    cursor, uid, {'numero': numero, 'prefix': prefix}
                )
            )
        return tel_ids

    def generar_xml(self, cursor, uid, teledons_atr, context=None):
        telefons = []
        for tel in teledons_atr:
            tel_xml = c1.Telefono()
            tel_xml.feed({
                'numero': tel.numero,
                'prefijo_pais': tel.prefix
            })
            telefons.append(tel_xml)
        return telefons

    def dummy_create(self, cursor, uid, partner_addr_id, context=None):
        partner_addr_obj = self.pool.get("res.partner.address")
        hsw_obj = self.pool.get("giscedata.switching.step.header")
        telefons_ids = []

        partner_addr_info = partner_addr_obj.read(
            cursor, uid, partner_addr_id, ['phone', 'mobile']
        )

        if partner_addr_info['phone']:
            tel_info = hsw_obj.clean_tel_number(partner_addr_info['phone'])
            tel_id = self.create(
                cursor, uid, {'numero': tel_info['tel'], 'prefix': tel_info['pre']}
            )
            telefons_ids.append(tel_id)

        if partner_addr_info['mobile']:
            tel_info = hsw_obj.clean_tel_number(partner_addr_info['mobile'])
            tel_id = self.create(
                cursor, uid,
                {'numero': tel_info['tel'], 'prefix': tel_info['pre']}
            )
            telefons_ids.append(tel_id)

        return telefons_ids

    _columns = {
        'numero': fields.char(u'Numero', size=12, required=True),
        'prefix': fields.char(u'Prefix', size=4, required=True),
    }
    _defaults = {
        'numero': '',
        'prefix': lambda *a: '34',
    }

GiscedataSwitchingTelefon()


class GiscedataSwitchingGenerador(osv.osv):

    _name = 'giscedata.switching.generador'
    _desc = 'Generadors ATR'
    _rec_name = 'cil'

    def create_from_xml(self, cursor, uid, generadors_xml, sw_id, context=None):
        generador_ids = []

        sw_obj = self.pool.get('giscedata.switching')
        tel_obj = self.pool.get('giscedata.switching.telefon')

        for datos_inst_gen in generadors_xml:
            vals = {}

            # DatosInstGen
            vals.update({
                'cil': datos_inst_gen.cil,
                'tec_generador': datos_inst_gen.tec_generador,
                'combustible': datos_inst_gen.combustible,
                'pot_installada_gen': datos_inst_gen.pot_instalada_gen,
                'tipus_installacio': datos_inst_gen.tipo_instalacion,
                'esquema_mesura': datos_inst_gen.esquema_medida,
                'ssaa': True if datos_inst_gen.ssaa == 'S' else False,
                'ref_cadastre_inst_gen': datos_inst_gen.ref_catastro,
            })
            # UTM
            utm = datos_inst_gen.utm
            if utm:
                vals.update({
                    'utm_x': utm.x,
                    'utm_y': utm.y,
                    'utm_fus': utm.huso,
                    'utm_banda': utm.banda,
                })

            # Checking if its an enterprise to select name
            sw = sw_obj.browse(cursor, uid, sw_id, context=context)
            titular = datos_inst_gen.titular_representante_gen
            nombre = datos_inst_gen.titular_representante_gen.nombre

            if titular:
                # IdTitular
                id_titular = datos_inst_gen.titular_representante_gen.id_titular
                vat_titular = id_titular.identificador
                vat_titular = vat_titular.strip()
                if 'ES' not in vat_titular:
                    vat_titular = 'ES' + vat_titular

                vals.update({
                    'tipus_identificador': id_titular.tipo_identificador,
                    'identificador': vat_titular,
                })
                if not nombre.razon_social:
                    name_as_dict = {
                        "N": nombre.nombre_de_pila,
                        "C1": nombre.primer_apellido,
                        "C2": nombre.segundo_apellido
                    }
                    vals.update({
                        'persona': 'F',
                        'nom': nombre.nombre_de_pila,
                        'cognom_1': nombre.primer_apellido,
                        'cognom_2': nombre.segundo_apellido,
                    })
                    new_name = self.pool.get("res.partner").ajunta_cognoms(cursor, uid, name_as_dict, context=context)
                else:
                    vals.update({
                        'persona': 'J',
                        'nom': nombre.razon_social,
                    })
                    new_name = nombre.razon_social

                # email
                vals.update({
                    'email': datos_inst_gen.titular_representante_gen.correo_electronico,
                })
                # Search for address in Titular
                cups_id = sw.cups_id.id if sw.cups_id else False
                addr_id = False
                new_phones = []
                for tel in datos_inst_gen.titular_representante_gen.telefono:
                    new_phones.append(tel[1])

                context.update({
                    'new_phones': new_phones,
                    'new_name': new_name
                })
                if datos_inst_gen.titular_representante_gen.direccion:
                    direccio = datos_inst_gen.titular_representante_gen.direccion
                    # TODO Add cliente data to use in search
                    ctx = context.copy()
                    addr_id = sw_obj.search_address(cursor, uid, sw_id, direccio, context=ctx)
                    vals.update({'fiscal_address_id': addr_id})
                elif cups_id:
                    # Use cups info
                    addr_id = sw_obj.create_partner_addres_from_cups(
                        cursor, uid, cups_id, context=context
                    )

                tel_xml_obj = datos_inst_gen.titular_representante_gen.telefono
                tels = tel_obj.create_from_xml(cursor, uid, tel_xml_obj)

                vals.update({
                    'fiscal_address_id': addr_id,
                    'telefons': [(6, 0, tels)],
                })

            generador_ids.append(self.create(cursor, uid, vals))

        return generador_ids

    def generar_xml(self, cursor, uid, generadors_atr, context=None):
        tel_obj = self.pool.get("giscedata.switching.telefon")
        generadors = []

        for gen in generadors_atr:

            # UTM
            utm = d1.UTM()
            utm_fields = {
                'x': gen.utm_x,
                'y': gen.utm_y,
                'huso': gen.utm_fus,
                'banda': gen.utm_banda,
            }
            utm.feed(utm_fields)

            id_titular = d1.IdTitular()
            id_titular_fields = {
                'tipo_identificador': gen.tipus_identificador,
                'identificador': gen.identificador
            }
            id_titular.feed(id_titular_fields)

            # Nombre
            nombre = d1.Nombre()
            if gen.persona == 'J':
                nombre_fields = {'razon_social': gen.nom}
            else:
                nombre_fields = {
                    'nombre_de_pila': gen.nom,
                    'primer_apellido': gen.cognom_1,
                    'segundo_apellido': gen.cognom_2
                }
            nombre.feed(nombre_fields)

            # TitularRepresentanteGen
            titular = d1.TitularRepresentanteGen()
            titular_representante_gen_fields = {
                'id_titular': id_titular,
                'nombre': nombre,
            }

            if gen.telefons:
                titular_representante_gen_fields.update(
                    {'telefono': tel_obj.generar_xml(cursor, uid, gen.telefons)}
                )
            titular_representante_gen_fields.update({
                'correo_electronico': gen.email,
            })

            dir_fiscal = gen.fiscal_address_id
            direccion = d1.Direccion()
            info_direccion = get_address_dicct(dir_fiscal)
            via, apartado = False, False
            if info_direccion['apartado_de_correos']:
                apartado = info_direccion['apartado_de_correos']
            else:
                via = d1.Via()
                via_fields = {
                    'tipo_via': 'CL',  # TODO manage road type
                    'calle': info_direccion['calle'],
                    'numero_finca': info_direccion['numfinca'],
                }
                if dir_fiscal.aclarador:
                    via_fields.update({'tipo_aclarador_finca': 'NO',
                                       'aclarador_finca': dir_fiscal.aclarador[:40]})
                via.feed(via_fields)
            direccion_fields = {
                'pais': info_direccion['pais'],
                'provincia': info_direccion['provincia'],
                'municipio': info_direccion['municipio'],
                'cod_postal': info_direccion['codpostal'],
                'via': via,
                'apartado_de_correos': apartado
            }
            direccion.feed(direccion_fields)
            titular_representante_gen_fields.update({'direccion': direccion})
            titular.feed(titular_representante_gen_fields)

            # DatosInstGen
            datos = d1.DatosInstGen()
            datos_inst_gen_fields = {
                'cil': gen.cil,
                'tec_generador': gen.tec_generador,
                'combustible': gen.combustible,
                'pot_instalada_gen': gen.pot_installada_gen,
                'tipo_instalacion': gen.tipus_installacio,
                'esquema_medida': gen.esquema_mesura,
                'ssaa': 'N',
                'ref_catastro': gen.ref_cadastre_inst_gen,
                'utm': utm,
                'titular_representante_gen': titular,
            }

            if gen.ssaa:
                datos_inst_gen_fields['ssaa'] = 'S'

            datos.feed(datos_inst_gen_fields)
            generadors.append(datos)
        return generadors

    def _get_is_con_excedentes(self, cursor, uid, context=None):
        if not context:
            context = {}

        seccioregistre = context.get('seccioregistre', False)
        return seccioregistre and seccioregistre == '2'

    _columns = {
        'cil': fields.char(u'CIL', size=25),
        'tec_generador': fields.selection(
            TABLA_126, u"Tec. Generador"
        ),
        'tipus_installacio': fields.selection(
            TABLA_129, u"Tipus d'instal·lació"
        ),
        'esquema_mesura': fields.selection(
            TABLA_130, u"Esquema mesura"
        ),
        'combustible': fields.char(u'Combustible', size=60),
        'pot_installada_gen': fields.float(u'Potència Instal·lada (KW)'),
        'ssaa': fields.boolean(string=u'SSAA'),
        'ref_cadastre_inst_gen': fields.char(u'Ref. Cadastre Inst.', size=20),
        'utm_x': fields.char(u'Coord. X (m)', size=60),
        'utm_y': fields.char(u'Coord. Y (m)', size=60),
        'utm_fus': fields.char(u'Fus', size=2, help=u"Entre 1 i 60"),
        'utm_banda': fields.char(u'Banda', help=u"Des de la C a la X (excloent I i O)", size=60),

        'persona': fields.selection(PERSONA, u'Persona'),
        'tipus_identificador': fields.selection(TABLA_6, u"Tipus document"),
        'identificador': fields.char(u'Document', size=45),
        'nom': fields.char(u'Nom de la persona o societat', size=45),
        'cognom_1': fields.char(u'Primer cognom', size=45, help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45, help=u"Opcional per a persones físiques"),
        'email': fields.char(u"Email", size=45),
        'fiscal_address_id': fields.many2one('res.partner.address', 'Adreça fiscal'),
        'telefons': fields.many2many(
            'giscedata.switching.telefon', "sw_step_generador_telefons_ref",
            'generador_id', 'telefon_id', string=u"Telèfons"),
        'is_con_excedentes': fields.boolean(u"És amb excedents"),
    }

    _defaults = {
        'is_con_excedentes': _get_is_con_excedentes,
    }


GiscedataSwitchingGenerador()


class GiscedataSwitchingSuministro(osv.osv):

    _name = 'giscedata.switching.suministro'
    _desc = 'Subministrament ATR'
    _rec_name = 'cups'

    def create_from_xml(self, cursor, uid, subministrament_xml, sw_id, context=None):
        subministrament_ids = []

        for datos_suministro in subministrament_xml:
            vals = {}

            # DatosSuministro
            vals.update({
                'cups': datos_suministro.cups,
                'tipus_cups': datos_suministro.tipo_cups,
                'ref_cadastre_cups': datos_suministro.ref_catastro,
            })
            subministrament_ids.append(self.create(cursor, uid, vals))

        return subministrament_ids

    def generar_xml(self, cursor, uid, suministros_atr, context=None):
        subministraments = []

        for sumin in suministros_atr:
            datos_suministro = d1.DatosSuministro()
            datos_suministro_fields = {
                'cups': sumin.cups,
                'tipus_cups': sumin.tipo_cups,
                'ref_cadastre_cups': sumin.ref_catastro,
            }
            datos_suministro.fredd(datos_suministro_fields)

            subministraments.append(datos_suministro)
        return subministraments

    _columns = {
        'cups': fields.char(u'CUPS', size=22, required=True),
        'tipus_cups': fields.selection(
            TABLA_131, u"Tipus CUPS"
        ),
        'ref_cadastre_cups': fields.char(u'Ref. Cadastre CUPS', size=20),
    }


GiscedataSwitchingSuministro()


class GiscedataSwitchingReclamacio(osv.osv):
    """"Detall Reclamació per R1"""
    _name = 'giscedata.switching.reclamacio'

    def create_from_xml(self, cursor, uid, xml, context=None):
        mun_obj = self.pool.get('res.municipi')
        tel_obj = self.pool.get("giscedata.switching.telefon")
        rec_ids = []
        if context.get('var_retip', False):
            var_list = xml.variables_aportacion_informacion_para_retipificacion
        else:
            var_list = xml.variables_detalle_reclamacion
        for xml_rec in var_list:
            lec_ids = self.get_lectures_ids(cursor, uid, xml_rec, context)
            rec_vals = {
                'num_expedient_escomesa': xml_rec.num_expediente_acometida,
                'num_expedient_frau': xml_rec.num_expediente_fraude,
                'data_incident': xml_rec.fecha_incidente,
                'num_factura': xml_rec.num_factura_atr,
                'tipus_concepte_facturat': xml_rec.tipo_concepto_facturado,
                'codi_incidencia': xml_rec.codigo_incidencia,
                'codi_sollicitud': xml_rec.codigo_solicitud,
                'parametre_contractacio': xml_rec.parametro_contratacion,
                'concepte_disconformitat': xml_rec.concepto_disconformidad,
                'tipus_atencio_incorrecte': xml_rec.tipo_de_atencion_incorrecta,
                'iban': xml_rec.iban,
                'codi_sollicitud_reclamacio':
                    xml_rec.codigo_solicitud_reclamacion,
                'data_inici': xml_rec.fecha_desde,
                'data_fins': xml_rec.fecha_hasta,
                'import_reclamat': xml_rec.importe_reclamado,
                # Lectures
                'codi_dh': xml_rec.tipo_dhedm,
                'data_lectura': xml_rec.fecha_lectura,
                'lect_ids': [(6, 0, lec_ids)]
            }
            # Contacte
            if xml_rec.contacto:
                con_tels = tel_obj.create_from_xml(
                    cursor, uid, xml_rec.contacto.telefonos
                )
                rec_vals.update({
                    'cont_nom': xml_rec.contacto.persona_de_contacto,
                    'cont_telefons': [(6, 0, con_tels)],
                    'cont_email': xml_rec.contacto.correo_electronico,
                })
            # Ubicacion
            if xml_rec.ubicacion_incidencia:
                direccio = xml_rec.ubicacion_incidencia
                rec_vals.update({
                    'desc_ubicacio': direccio.des_ubicacion_incidencia,
                    'poblacio': direccio.poblacion,
                    'codi_postal': direccio.cod_postal,
                })
                # Search for municipi
                cmun = direccio.municipio and direccio.municipio[:5] or False
                municipi_id = mun_obj.search(cursor, uid, [('ine', '=', cmun)])
                if not municipi_id:
                    cmun = str(int(cmun))
                    municipi_id = mun_obj.search(cursor, uid, [('ine', '=ilike', '%'+cmun)])
                if municipi_id:
                    municipi = mun_obj.browse(cursor, uid, municipi_id[0])
                    rec_vals.update({
                        'provincia': municipi.state.id,
                        'municipi': municipi.id,
                    })
            rec_ids.append(self.create(cursor, uid, rec_vals))
        return rec_ids

    def get_lectures_ids(self, cursor, uid, xml, context=None):
        lect_obj = self.pool.get('giscedata.switching.lectura')
        lecturas_vals = []
        codi_dh = xml.tipo_dhedm
        for lect in xml.lecturas_aportadas:
            xml_period = lect.codigo_periodo_dh
            period = xml_period[-1]
            if period == '0':
                if xml_period == '10':
                    # P1 de 2.0A
                    period = '1'
                elif xml_period == '00' and codi_dh == '1':
                    # FENOSA 2.0A amb periode 00
                    period = '1'
            # P2 de 2.0 DHA (Iberdrola)
            if xml_period == '03':
                period = '2'
            lecturas_vals.append(
                (
                    'P%s' % period,
                    lect.integrador,
                    float(lect.lectura_propuesta)
                )
            )

        return lect_obj.create_lects(cursor, uid, lecturas_vals)

    def generar_xml(self, cursor, uid, pas, context=None):
        """Returns VariableDetalle in XML format XML R1-01
        """
        if not context:
            context = {}

        lect_obj = self.pool.get('giscedata.switching.lectura')
        tel_obj = self.pool.get("giscedata.switching.telefon")

        detalls = []
        for var in pas.header_id.reclamacio_ids:
            if context.get('var_retip', False):
                variable = r1.VariableAportacionInformacionParaRetipificacion()
            else:
                variable = r1.VariableDetalleReclamacion()
            if var.import_reclamat > 0:
                import_reclam = var.import_reclamat
            else:
                import_reclam = None
            var_vals = {
                'num_expediente_acometida': var.num_expedient_escomesa,
                'num_expediente_fraude': var.num_expedient_frau,
                'fecha_incidente': var.data_incident,
                'num_factura_atr': var.num_factura,
                'tipo_concepto_facturado': var.tipus_concepte_facturat,
                'fecha_lectura': var.data_lectura,
                'tipo_dhedm': var.codi_dh,
                'codigo_incidencia': var.codi_incidencia,
                'codigo_solicitud': var.codi_sollicitud,
                'parametro_contratacion': var.parametre_contractacio,
                'concepto_disconformidad': var.concepte_disconformitat,
                'tipo_de_atencion_incorrecta': var.tipus_atencio_incorrecte,
                'iban': var.iban,
                'codigo_solicitud_reclamacion': var.codi_sollicitud_reclamacio,
                'fecha_desde': var.data_inici,
                'fecha_hasta': var.data_fins,
                'importe_reclamado': import_reclam,
            }
            # lectures
            if var.lect_ids:
                measures = lect_obj.generar_xml(
                    cursor, uid, pas, var_r1=var, context=context
                )
                aported_measures = r1.LecturasAportadas()
                aported_measures.feed({
                    'lectura_aportada_list': measures
                })
                var_vals.update({'lecturas_aportadas': aported_measures})
            # ubicacio
            if var.desc_ubicacio:
                ubicacion_incidencia = r1.UbicacionIncidencia()
                ubicacion_incidencia_fields = {
                    'des_ubicacion_incidencia': var.desc_ubicacio,
                    'provincia': var.provincia.code,
                    'municipio': var.municipi.ine,
                    'poblacion': var.poblacio,
                    'cod_postal': var.codi_postal,
                }
                ubicacion_incidencia.feed(ubicacion_incidencia_fields)
                var_vals.update({'ubicacion_incidencia': ubicacion_incidencia})
            # contact person
            if var.cont_nom:
                contacte = r1.Contacto()
                con_fields = {'persona_de_contacto': var.cont_nom}
                if var.cont_telefons:
                    con_fields.update({
                        'telefonos': tel_obj.generar_xml(cursor, uid, var.cont_telefons)
                    })
                if var.cont_email:
                    con_fields.update({'correo_electronico': var.cont_email})
                contacte.feed(con_fields)
                var_vals.update({'contacto': contacte})
            variable.feed(var_vals)
            detalls.append(variable)
        if context.get('var_retip', False):
            variables = r1.VariablesAportacionInformacionParaRetipificacion()
            variables.feed({'variable_aportacion_informacion_para_retipificacion_list': detalls})
        else:
            variables = r1.VariablesDetalleReclamacion()
            variables.feed({'variable_detalle_reclamacion_list': detalls})
        return variables

    def unlink(self, cursor, uid, ids, context=None):
        '''unlink many2many associated models'''
        tel_obj = self.pool.get('giscedata.switching.telefon')
        for header in self.browse(cursor, uid, ids, context=context):
            if header.cont_telefons:
                tel_ids = [tel.id for tel in header.cont_telefons]
                tel_obj.unlink(cursor, uid, tel_ids)
        return super(GiscedataSwitchingReclamacio,
                     self).unlink(cursor, uid, ids, context=context)

    _columns = {
        'num_expedient_escomesa': fields.char(
            u'Núm. Expedient Escomesa', size=20
        ),
        'num_expedient_frau': fields.char(
            u'Núm. Expedient Frau', size=16
        ),
        'data_incident': fields.date(u"Data incident"),
        'num_factura': fields.char(u"Num. Factura", size=26),
        'tipus_concepte_facturat': fields.selection(
            TABLA_77, u'Tipus concepte facturat'
        ),
        'codi_incidencia': fields.selection(
            [t[:2] for t in TABLA_86], u'Codi incidència'
        ),
        'codi_sollicitud': fields.char(
            u"Codi Sol·licitud", size=12,
            help=u"Codi de de Sol·licitud ATR objecte d'aquesta reclamació"
        ),
        'parametre_contractacio': fields.selection(
            TABLA_79, u"Paràmetre de contractació"
        ),
        'concepte_disconformitat': fields.text(
            u"Concepte de disconformitat", size=120
        ),
        'tipus_atencio_incorrecte': fields.selection(
            [t[:2] for t in TABLA_87], u'Tipus atenció incorrecte',
        ),
        'iban': fields.char(u'IBAN', size=34),
        'codi_sollicitud_reclamacio': fields.char(
            u"Codi Sol·licitud Reclamació Anterior", size=12,
            help=u"Codi de de Sol·licitud de la reclamació anterior "
                 u"relacionada"
        ),
        'data_inici': fields.date(u"Data Des de"),
        'data_fins': fields.date(u"Data Fins"),
        'import_reclamat': fields.float(u"Import Reclamat", digits=(12, 2)),

        # Lectures
        'data_lectura': fields.date(u"Data lectura"),
        'codi_dh': fields.selection(TIPO_DH_APARATO, 'Codi DH'),
        'lect_ids': fields.many2many('giscedata.switching.lectura',
                                     'sw_reclamacio_lectura_ref',
                                     'reclamacio_detall_id', 'lectura_id',
                                     'Lectures'),
        # Dades de contacte addicionals (opcionals)
        'cont_nom': fields.char(u'Nom de la persona o societat', size=150),
        'cont_prefix': fields.char(u"Prefix telefònic", size=2),
        'cont_telefon': fields.char(u"Telèfon", size=9),
        'cont_telefons': fields.many2many(
            'giscedata.switching.telefon', "sw_reclamacio_cont_telefons_ref",
            'header_id', 'telefon_id', string=u"Telèfons", required=False
        ),
        'cont_email': fields.char(u"Email", size=60),
        # Ubicacio
        'desc_ubicacio': fields.char(u"Descripció Ubicació Incidència", size=45),
        'provincia': fields.many2one('res.country.state', u'Província'),
        'municipi': fields.many2one('res.municipi', u'Municipi'),
        'poblacio': fields.char(u'Codi Població', size=9),
        'codi_postal': fields.char(u'Codi Postal', size=9),
    }

    _defaults = {
    }

GiscedataSwitchingReclamacio()


class GiscedataSwitchingIntervencio(osv.osv):
    """"Intervencio per R1"""
    _name = 'giscedata.switching.intervencio'

    def create_from_xml(self, cursor, uid, xml, context=None):
        int_ids = []
        for xml_int in xml.informacion_intermedia.intervenciones:
            rec_vals = {
                'tipus_intervencio': xml_int.tipo_intervencion,
                'data': xml_int.fecha,
                'hora_desde': xml_int.hora_desde,
                'hora_fins': xml_int.hora_hasta,
                'numero_visita': xml_int.numero_visita,
                'resultat': xml_int.resultado,
                'detall_resultat': xml_int.detalle_resultado,
            }
            int_ids.append(self.create(cursor, uid, rec_vals))
        return int_ids

    def generar_xml(self, cursor, uid, pas, context=None):
        if not context:
            context = {}

        detalls = []
        for var in pas.header_id.intervencio_ids:
            variable = r1.Intervencion()
            var_vals = {
                'tipo_intervencion': var.tipus_intervencio,
                'fecha': var.data,
                'hora_desde': var.hora_desde,
                'hora_hasta': var.hora_fins,
                'numero_visita': var.numero_visita,
                'resultado': var.resultat,
                'detalle_resultado': var.detall_resultat,
            }
            variable.feed(var_vals)
            detalls.append(variable)
        variables = r1.Intervenciones()
        variables.feed({'intervencion_list': detalls})
        return variables

    _columns = {
        'tipus_intervencio': fields.selection(
            TABLA_74, u"Tipus de Intervenció", required=True
        ),
        'data': fields.date(u"Data Intervencio", required=True),
        'hora_desde': fields.time("Hora Desde", required=True),
        'hora_fins': fields.time("Hora Fins", required=True),
        'numero_visita': fields.char(
            u"Numero de Visita", size=2
        ),
        'resultat': fields.selection(
            TABLA_75, u"Resultat de la Intervenció", required=True
        ),
        'detall_resultat': fields.text(
            u"Detalls del resultat", size=255
        )
    }

GiscedataSwitchingIntervencio()


class GiscedataSwitchingSollicitudInfoAdicional(osv.osv):
    """"Intervencio per R1"""
    _name = 'giscedata.switching.sollicitud_informacio_adicional'

    def create_from_xml(self, cursor, uid, xml, context=None):
        sol_ids = []
        for xml_sol in xml.solicitudes_informacion_adicional:
            rec_vals = {
                'tipus_info_adicional': xml_sol.tipo_informacion_adicional,
                'descripcio_peticio_informacio': xml_sol.desc_peticion_informacion,
                'data_limit': xml_sol.fecha_limite_envio,
            }
            sol_ids.append(self.create(cursor, uid, rec_vals))
        return sol_ids

    def generar_xml(self, cursor, uid, pas, context=None):
        if not context:
            context = {}

        detalls = []
        for var in pas.header_id.sollicitud_ids:
            variable = r1.SolicitudInformacionAdicional()
            var_vals = {
                'tipo_informacion_adicional': var.tipus_info_adicional,
                'desc_peticion_informacion': var.descripcio_peticio_informacio,
                'fecha_limite_envio': var.data_limit,
            }
            variable.feed(var_vals)
            detalls.append(variable)
        siar = None
        if pas.hi_ha_sol_info_retip:
            siar = r1.SolicitudInformacionAdicionalParaRetipificacion()
            sol_fields = {
                'tipo': pas.sol_retip_tipus,
                'subtipo': pas.sol_retip_subtipus_id.name,
                'fecha_limite_envio': pas.sol_retip_data_limit,
            }
            siar.feed(sol_fields)
        variables = r1.SolicitudesInformacionAdicional()
        variables.feed({
            'solicitud_informacion_adicional_list': detalls,
            'solicitud_informacion_adicional_para_retipificacion': siar
        })
        return variables

    _columns = {
        'tipus_info_adicional': fields.selection(
            TABLA_85, u"Tipus de Informació Addicional", required=True
        ),
        'data_limit': fields.date(u"Data Limit d'Enviament", required=True),
        'descripcio_peticio_informacio': fields.text(
            u"Detalls de la Informació Sol.licitada", size=255
        )
    }

GiscedataSwitchingSollicitudInfoAdicional()


class GiscedataSwitchingVariableAportacioInformacio(osv.osv):
    _name = 'giscedata.switching.var_aportacio_info'

    def create_from_xml(self, cursor, uid, xml, context=None):
        var_ids = []
        l_var = xml.variables_aportacion_informacion
        for xml_var in l_var:
            var_vals = {
                'tipus_info': xml_var.tipo_informacion,
                'desc_peticio_info': xml_var.desc_peticion_informacion,
                'variable': xml_var.variable,
                'valor': xml_var.valor
            }
            var_ids.append(self.create(cursor, uid, var_vals))
        return var_ids

    def generar_xml(self, cursor, uid, pas, context=None):
        if not context:
            context = {}

        detalls = []
        for var in pas.header_id.vars_aportacio_info_ids:
            variable = r1.VariableAportacionInformacion()
            var_vals = {
                'tipo_informacion': var.tipus_info,
                'desc_peticion_informacion': var.desc_peticio_info,
                'variable': var.variable,
                'valor': var.valor,
            }
            variable.feed(var_vals)
            detalls.append(variable)
        variables = r1.VariablesAportacionInformacion()
        variables.feed({'variable_aportacion_informacion_list': detalls})
        return variables

    _columns = {
        'tipus_info': fields.selection(
            TABLA_85, u"Tipus de Informació Addicional", required=True
        ),
        'desc_peticio_info': fields.text(
            u"Detalls de la Informació Sol.licitada", size=255
        ),
        'variable': fields.selection(TABLA_76, u"Variable", required=False),
        'valor': fields.char(u"Valor", size=45),

    }

GiscedataSwitchingVariableAportacioInformacio()


class GiscedataSwitchingStepHeader(osv.osv):

    _name = 'giscedata.switching.step.header'
    _rec_name = 'sw_id'

    def unlink(self, cursor, uid, ids, context=None):
        '''unlink many2many associated models'''
        pot_obj = self.pool.get('giscedata.switching.potencia')
        pm_obj = self.pool.get('giscedata.switching.pm')
        inc_obj = self.pool.get('giscedata.switching.incidencia')
        reb_obj = self.pool.get('giscedata.switching.rebuig')
        rec_obj = self.pool.get('giscedata.switching.reclamacio')
        doc_obj = self.pool.get('giscedata.switching.document')
        var_obj = self.pool.get('giscedata.switching.var_aportacio_info')
        tel_obj = self.pool.get('giscedata.switching.telefon')
        stepinfo_obj = self.pool.get("giscedata.switching.step.info")
        for header in self.browse(cursor, uid, ids, context=context):
            if header.pot_ids:
                pot_ids = [pot.id for pot in header.pot_ids]
                pot_obj.unlink(cursor, uid, pot_ids)
            if header.pm_ids:
                pm_ids = [pm.id for pm in header.pm_ids]
                pm_obj.unlink(cursor, uid, pm_ids)
            if header.incidencia_ids:
                inc_ids = [inc.id for inc in header.incidencia_ids]
                inc_obj.unlink(cursor, uid, inc_ids)
            if header.rebuig_ids:
                reb_ids = [reb.id for reb in header.rebuig_ids]
                reb_obj.unlink(cursor, uid, reb_ids)
            if header.reclamacio_ids:
                rec_ids = [reb.id for reb in header.reclamacio_ids]
                rec_obj.unlink(cursor, uid, rec_ids)
            if header.document_ids:
                document_ids = [doc.id for doc in header.document_ids]
                doc_obj.unlink(cursor, uid, document_ids)
            if header.document_ids:
                var_ids = [doc.id for doc in header.vars_aportacio_info_ids]
                var_obj.unlink(cursor, uid, var_ids)
            if header.telefons:
                tel_ids = [tel.id for tel in header.telefons]
                tel_obj.unlink(cursor, uid, tel_ids)
            if header.cont_telefons:
                tel_ids = [tel.id for tel in header.cont_telefons]
                tel_obj.unlink(cursor, uid, tel_ids)
            if header.rec_telefons:
                tel_ids = [tel.id for tel in header.rec_telefons]
                tel_obj.unlink(cursor, uid, tel_ids)

            # Obtenim eliminem els step_info que no tinguin pas_id
            # (en aquest punt ja s'ha eliminat el pas)
            for step in header.sw_id.step_ids:
                if not step.read(['pas_id'])[0].get('pas_id'):
                    stepinfo_obj.unlink(cursor, uid, step.id, context=context)
        return super(GiscedataSwitchingStepHeader, self).unlink(cursor, uid, ids, context=context)

    def generar_xml(self, cursor, uid, ids, pas, context=None):
        """Retorna la sol·licitud en format XML C2, pas 07
        """
        if not context:
            context = {}
        meta_ctx = context.get('meta', {})
        if meta_ctx.get('store_attachment', True):
            if context.get('sortint', False):
                #Check for comer sortint
                if pas.sw_id.comer_sortint_id.ref != pas.receptor_id.ref:
                    raise osv.except_osv('Error',
                                _(u"La comercialitzadora sortint no es correspon "
                                  u"al receptor del pas"))
        capcalera_vals = pas.sw_id.get_header_vals()

        if pas.sw_id.proces_id.name in ('R1'):
            capcalera = r1.CabeceraReclamacion()
        elif pas.sw_id.proces_id.name in ('A1'):
            capcalera = a1.CabeceraAutoconsumoRechazo()
        else:
            capcalera = c1.Cabecera()

        if pas.sw_id.proces_id.name == 'A1':
            capcalera_vals.update({'codigo_empresa_destino': pas.receptor_id.ref})
        else:
            capcalera_vals.update({'codigo_ree_empresa_destino': pas.receptor_id.ref})

        capcalera_vals.update({'codigo_ree_empresa_emisora': pas.emisor_id.ref,
                               'codigo_del_paso': pas._nom_pas,
                               'fecha': pas.date_created})
        capcalera.feed(capcalera_vals)
        return capcalera

    def generar_xml_pot(self, cursor, uid, ids, pas, context=None):

        pot_obj = self.pool.get('giscedata.switching.potencia')
        return pot_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_pm(self, cursor, uid, ids, pas, context=None):

        pm_obj = self.pool.get('giscedata.switching.pm')
        return pm_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_lectures(self, cursor, uid, ids, pas, context=None):

        lect_obj = self.pool.get('giscedata.switching.lectura')
        return lect_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_reclamacio(self, cursor, uid, ids, pas, context=None):

        detall_obj = self.pool.get(
            'giscedata.switching.reclamacio'
        )
        return detall_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_document(self, cursor, uid, ids, pas, context=None):

        document_obj = self.pool.get('giscedata.switching.document')
        return document_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_intervencio(self, cursor, uid, ids, pas, context=None):

        detall_obj = self.pool.get(
            'giscedata.switching.intervencio'
        )
        return detall_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_sollicitud(self, cursor, uid, ids, pas, context=None):

        detall_obj = self.pool.get(
            'giscedata.switching.sollicitud_informacio_adicional'
        )
        return detall_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_var_aport_info(self, cursor, uid, ids, pas, context=None):

        detall_obj = self.pool.get(
            'giscedata.switching.var_aportacio_info'
        )
        return detall_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')

        res = {
            'sw_id': sw_id,
            'emisor_id': sw_obj.check_partner(cursor, uid, xml.get_codi_emisor),
            'receptor_id': sw_obj.check_partner(cursor, uid, xml.get_codi_destinatari),
            'date_created': xml.data_sollicitud,
            'seq_sollicitud': xml.seq_sollicitud
        }

        return res

    def create_pots(self, cursor, uid, pots, polissa=False, pas01=False, context=None):

        pot_obj = self.pool.get('giscedata.switching.potencia')

        if polissa:
            pots = sorted([(pot.periode_id.product_id.name,
                        int(pot.potencia * 1000))
                        for pot in polissa.potencies_periode],
                        key=lambda a: a[0])
        elif pas01 and pas01.pot_ids:
            pots = sorted(
                [(p.name, p.potencia) for p in pas01.pot_ids],
                key=lambda a: a[0]
            )
        created_pots = pot_obj.create_pots(cursor, uid, pots,
                                           context=context)
        return created_pots

    def create_from_xml_doc(self, cursor, uid, xml, context=None):

        doc_obj = self.pool.get('giscedata.switching.document')
        return doc_obj.create_docs(cursor, uid, xml, context=context)

    def create_from_xml_pm(self, cursor, uid, xml, context=None):

        pm_obj = self.pool.get('giscedata.switching.pm')
        return pm_obj.create_from_xml(cursor, uid, xml, context=context)

    def create_from_xml_reclamacio(self, cursor, uid, xml, context=None):
        rec_obj = self.pool.get('giscedata.switching.reclamacio')
        return rec_obj.create_from_xml(cursor, uid, xml, context=context)

    def create_from_xml_intervencio(self, cursor, uid, xml, context=None):
        int_obj = self.pool.get('giscedata.switching.intervencio')
        return int_obj.create_from_xml(cursor, uid, xml, context=context)

    def create_from_xml_sollicitud(self, cursor, uid, xml, context=None):
        int_obj = self.pool.get('giscedata.switching.sollicitud_informacio_adicional')
        return int_obj.create_from_xml(cursor, uid, xml, context=context)

    def create_from_xml_var_aport_info(self, cursor, uid, xml, context=None):
        int_obj = self.pool.get(
            'giscedata.switching.var_aportacio_info')
        return int_obj.create_from_xml(cursor, uid, xml, context=context)

    def get_telephone_number(self, cursor, uid, sw, context=None):
        ''' Gets telephone number '''
        res = {}
        if context is None:
            context = {}

        pol_obj = self.pool.get("giscedata.polissa")
        direccio = pol_obj.get_address_with_phone(
            cursor, uid, sw.cups_polissa_id.id, context=context
        )

        if direccio:
            tel_obj = self.pool.get("giscedata.switching.telefon")
            res.update({
                'telefons': tel_obj.dummy_create(cursor, uid, direccio.id)
            })
        return res

    def get_contact_info(self, cursor, uid, sw, context=None):
        res = {}
        if context is None:
            context = {}

        tel_obj = self.pool.get("giscedata.switching.telefon")
        model_name = context.get('model', False)
        if not model_name:
            model_name = sw.step_id.get_step_model()
        model_obj = self.pool.get(model_name)
        if 'cont_nom' in model_obj.fields_get(cursor, uid).keys():
            direccio = sw.cups_polissa_id.direccio_notificacio
            if not direccio or (not direccio.phone and not direccio.mobile):
                direccio = sw.cups_polissa_id.direccio_pagament
            if direccio:
                cont_name = (direccio.partner_id and direccio.partner_id.name or
                             sw.cups_polissa_id.titular.name)
                res.update({
                    'cont_nom': cont_name,
                    'cont_telefons': [
                        (6, 0, tel_obj.dummy_create(cursor, uid, direccio.id))
                    ],
                })
        return res

    def clean_phone(self, cursor, uid, tel_number):
        '''Per poder cridar-la des de ooop'''
        return self.clean_tel_number(tel_number)

    def clean_tel_number(self, tel_number):
        ''' Strips non-number chars from telephone number except "+"'''
        res = {'tel': tel_number, 'pre': '' , 'fuzzy': False}
        if not tel_number:
            return res
        tel_net = ''.join([c for c in tel_number if c.isdigit()])
        tel = tel_net
        pre = '34'
        fuzzy = False
        if len(tel_net) > 9:
            pre = tel_net[:-9][-2:]
            tel = tel_net[-9:]
            fuzzy = len(tel) + len(pre) < len(tel_net)

        res = {'tel': tel, 'pre': pre, 'fuzzy': fuzzy}
        return res

    def get_cognoms(self, cursor, uid, sw, context=None):
        ''' splits surnames an name'''
        if context is None:
            context = {}
        res = pas_vals = {}
        model_name = context.get('model', False)
        if not model_name:
            model_name = sw.step_id.get_step_model()
        model_obj = self.pool.get(model_name)
        if 'cognom_1' in model_obj.fields_get(cursor, uid).keys():
            #Arreglem nom i cognoms
            partner_obj = self.pool.get('res.partner')

            nom_titular = sw.cups_polissa_id.titular.name
            nom = partner_obj.separa_cognoms(cursor, uid, nom_titular)

            vat_enterprise = sw.vat_enterprise()
            warn_nom = True
            if not vat_enterprise:
                if not nom['fuzzy']:
                    warn_nom = False
                pas_vals = {'nom': nom['nom'],
                            'cognom_1': nom['cognoms'][0],
                            'cognom_2': nom['cognoms'][1]}
            else:
                """ Empresa només té nom"""
                if len(nom_titular) <= 50 and not nom['fuzzy']:
                    warn_nom = False
                pas_vals = {'nom': nom_titular,
                            'cognom_1': '',
                            'cognom_2': ''}
            if warn_nom:
                pas_vals.update({'validacio_pendent': True})

            res.update(pas_vals)

        return res

    def dummy_create(self, cursor, uid, sw, context=None):
        '''create with dummy values'''
        sw_obj = self.pool.get('giscedata.switching')
        if not context:
            context = {}
        receptor_id = sw.partner_id.id
        if context.get('sortint', False):
            if not sw.comer_sortint_id:
                raise osv.except_osv('Error',
                                 _(u"Es necessari omplir la comercialitzadora "
                                   u"sortint per poder generar el pas"))
            receptor_id = sw.comer_sortint_id.id

        res = {'sw_id': sw.id,
               'emisor_id': sw.company_id.id,
               'receptor_id': receptor_id,
               }

        # Calc the seq_sollicitud
        step_to_create = sw.step_id.name
        nseq = len([sid for sid in sw.step_ids if sid.step_id.name == step_to_create])
        res.update({
            'seq_sollicitud': str(nseq).zfill(2)
        })
        # Refresh sw object for pending triggers
        sw.write({})
        sw = sw_obj.browse(cursor, uid, sw.id)
        # split name an surname's
        if sw.cups_id and sw.cups_polissa_id and sw.cups_polissa_id.titular:
            pas_vals = self.get_cognoms(cursor, uid, sw, context=context)
            # import telephone number
            tels = self.get_telephone_number(
                cursor, uid, sw, context=context
            ).get('telefons', [])
            pas_vals.update({'telefons': [(6, 0, tels)]})

            pas_vals.update(self.get_contact_info(cursor, uid, sw, context=context))
            res.update(pas_vals)

        return res

    def dummy_create_pm(self, cursor, uid, sw, context=None):

        pm_obj = self.pool.get('giscedata.switching.pm')
        return pm_obj.dummy_create(cursor, uid, sw, context=context)

    def dummy_create_reb(self, cursor, uid, sw, motius, context=None):
        reb_obj = self.pool.get('giscedata.switching.rebuig')
        return reb_obj.dummy_create(cursor, uid, sw, motius, context=context)

    def get_xml_encoding(self, cursor, uid, ids, context=None):
        """Retorna la codificació XML configurada al partner receptor"""
        p_obj = self.pool.get('res.partner')
        xmlenc_obj = self.pool.get('giscedata.switching.xml.encoding')
        header = self.browse(cursor, uid, ids, context=context)[0]
        xmlenc = p_obj.read(cursor, uid, header.receptor_id.id,
                            ['property_switching_xml_encoding'])
        xmlenc_id = xmlenc['property_switching_xml_encoding'][0]
        xml_enc = xmlenc_obj.read(cursor, uid, xmlenc_id, ['acronim'])

        return xml_enc['acronim']

    def set_enviament_pendent(self, cursor, uid, ids, pendent, context=None):
        """Marca el pas com a XML pendent d'enviar (o no)"""
        if not context:
            context = {}
        header = self.browse(cursor, uid, ids, context=context)[0]
        header.write({'enviament_pendent': pendent})

        return True

    def write(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (list,tuple)):
            ids = [ids]
        res = super(GiscedataSwitchingStepHeader, self).write(
            cursor, uid, ids, vals, context=context)
        self.update_additional_info(cursor, uid, ids, context)
        if 'enviament_pendent' in vals:
            self.update_deadline(cursor, uid, ids, context=context)
        # Force update _ff_accio_pendent_comerdist
        sw_ids = []
        for inf in self.read(cursor, uid, ids, ['sw_id'], context=context):
            if inf['sw_id']:
                sw_ids.append(inf['sw_id'][0])
        self.pool.get("giscedata.switching").update_ff_accio_pendent_comerdist(cursor, uid, sw_ids, context=context)
        return res

    def update_additional_info(self, cursor, uid, ids, context=None):
        """
        Actualitza el camp additional_info del cas de switching associat al
        header quan el pas associat al header és el pas actual del cas de
        switching
        """
        sw_obj = self.pool.get('giscedata.switching')
        if not isinstance(ids, list):
            ids = [ids]
        info = self.get_case_of_header(cursor, uid, ids)
        for hid in info.keys():
            pas_id, model = info.get(hid)
            sw_obj.update_additional_info(cursor, uid, pas_id, model,
                                          context=context)

    def update_deadline(self, cursor, uid, ids, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        if not isinstance(ids, list):
            ids = [ids]
        info = self.get_case_of_header(cursor, uid, ids)
        for hid in info.keys():
            pas_id, model = info.get(hid)
            proces = model.split(".")[-2].upper()
            step = model.split(".")[-1]
            sw_id = self.read(cursor, uid, hid, ['sw_id'])['sw_id'][0]
            sw_obj.update_deadline(cursor, uid, sw_id, proces, step,
                                   context=context)

    def get_case_of_header(self, cursor, uid, header_ids, context=None):
        """ Returns a dict of tuples:
                {header_id: ( case_id, case_model )}
            with the case info. of each header_id.
        """
        if not isinstance(header_ids, list):
            header_ids = [header_ids]
        sw_obj = self.pool.get('giscedata.switching')
        step_info_obj = self.pool.get('giscedata.switching.step.info')
        res = {}
        # Get the switching cases of the headers
        sw_ids = self.read(cursor, uid, header_ids, ['sw_id'])
        for i in range(len(header_ids)):
            header_id = header_ids[i]
            sw_id = sw_ids[i]['sw_id'][0]
            # Get all steps info of switching case
            step_ids = sw_obj.read(cursor, uid, sw_id, ['step_ids'])['step_ids']
            # Search for each step if its header_id is the current one
            for step_inf_id in step_ids:
                pinfo = step_info_obj.read(cursor, uid, step_inf_id,
                                           ['step_id', 'proces_id'])
                model = "giscedata.switching.{0}.{1}".format(
                    pinfo['proces_id'][1].lower(), pinfo['step_id'][1]
                )
                pas_obj = self.pool.get(model)
                # Search if there is an step with the current header_id
                pas_ids = pas_obj.search(cursor, uid,
                                         [('header_id', '=', header_id)])
                if len(pas_ids) > 0:
                    res.update({header_id: (pas_ids[0], model)})
                    break
        return res

    def get_notification_mail(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        template = None
        templates = []
        data_model = self.pool.get('ir.model.data')
        for header_id in ids:
            step_related_info_dict = self.get_case_of_header(
                cursor, uid, header_id, context=context
            )

            step_related_info_dict = step_related_info_dict[header_id]
            # ex. {sw_id: (step_id, 'giscedata.switching.a3.05')} ->
            # (step_id, 'giscedata.switching.a3.05')

            real_step_obj = self.pool.get(step_related_info_dict[1])
            real_step_id = step_related_info_dict[0]

            template_name = real_step_obj.get_notification_mail_name(
                cursor, uid, real_step_id, context=context
            )
            template = data_model.get_object_reference(
                cursor, uid, 'giscedata_switching', template_name
            )[1]

            templates.append(template)
        return templates

    _columns = {
        'sw_id': fields.many2one('giscedata.switching', 'Switching'),
        'emisor_id': fields.many2one('res.partner', 'Emisor',
                                     required=True),
        'receptor_id': fields.many2one('res.partner', 'Receptor',
                                       required=True),
        'date_created': fields.datetime('Data creació', required=True),
        'seq_sollicitud': fields.char(u'Seqüencial de sol·licitud', size=2),
        'pot_ids': fields.many2many('giscedata.switching.potencia',
                                    'sw_step_header_potencia_ref',
                                    'header_id', 'potencia_id',
                                    'Potències'),
        'rebuig_ids': fields.many2many('giscedata.switching.rebuig',
                                       'sw_step_header_rebuig_ref',
                                       'header_id', 'rebuig_id',
                                       'Rebuig'),
        'incidencia_ids': fields.many2many('giscedata.switching.incidencia',
                                           'sw_step_header_incidencia_ref',
                                           'header_id', 'incidencia_id',
                                           'Incidencies'),
        'pm_ids': fields.many2many('giscedata.switching.pm',
                                   'sw_step_header_pm_ref',
                                   'header_id', 'pm_id',
                                   'Punts de mesura'),
        # validacions
        'validacio_pendent': fields.boolean(u'Per validar',
                                            help=u"Durant la generació "
                                                 u"automàtica del cas s'ha "
                                                 u"detectat algun problema amb"
                                                 u" algun camp, p.e. nom"),
        'enviament_pendent': fields.boolean(u'Per enviar',
                                            help=u"Encara no s'ha enviat el "
                                                 u"fitxer XML a l'empresa "
                                                 u"corresponent per aquest pas"
                                            ),
        'notificacio_pendent': fields.boolean(u'Per notificar'),
        'lect_ids': fields.many2many('giscedata.switching.lectura',
                                     'sw_step_header_lectura_ref',
                                     'header_id', 'lectura_id',
                                     'Lectures'),
        'reclamacio_ids': fields.many2many(
            'giscedata.switching.reclamacio',
            'sw_step_header_reclamacio_ref', 'header_id', 'reclamacio_id',
            'Variables Detall Reclamacio'
        ),

        'document_ids': fields.many2many(
            'giscedata.switching.document', 'sw_step_header_doc_ref',
            'header_id', 'document_id', 'Documents'
        ),

        'intervencio_ids': fields.many2many(
            'giscedata.switching.intervencio',
            'sw_step_header_intervencio_ref', 'header_id', 'intervencio_id',
            'Intervencions'
        ),

        'sollicitud_ids': fields.many2many(
            'giscedata.switching.sollicitud_informacio_adicional',
            'sw_step_header_sollicitud_ref', 'header_id', 'sollicitud_id',
            'Sol.licituds Informació Adicional'
        ),

        'vars_aportacio_info_ids': fields.many2many(
            'giscedata.switching.var_aportacio_info',
            'sw_step_header_var_aportacio_info_ref', 'header_id',
            'var_aportacio_info', 'Variable Aportacio Informació'
        ),
        'telefons': fields.many2many(
            'giscedata.switching.telefon', "sw_step_header_telefons_ref",
            'header_id', 'telefon_id', string=u"Telèfons", required=False
        ),
        'cont_telefons': fields.many2many(
            'giscedata.switching.telefon', "sw_step_header_cont_telefons_ref",
            'header_id', 'telefon_id', string=u"Telèfons", required=False
        ),
        'rec_telefons': fields.many2many(
            'giscedata.switching.telefon', "sw_step_header_rec_telefons_ref",
            'header_id', 'telefon_id', string=u"Telèfons", required=False
        ),
    }

    _defaults = {
        'date_created': lambda *a: datetime.strftime(datetime.now(),
                                                     '%Y-%m-%d %H:%M:%S'),
        'validacio_pendent': lambda *a: False,
        'enviament_pendent': lambda *a: True,
        'seq_sollicitud': lambda *a: "01",
    }


GiscedataSwitchingStepHeader()


class GiscedataSwitchingPuntMesura2(osv.osv):
    _name = 'giscedata.switching.pm'
    _inherit = 'giscedata.switching.pm'

    _columns = {
        'header_ids': fields.many2many('giscedata.switching.step.header',
                                       'sw_step_header_pm_ref',
                                       'pm_id', 'header_id',
                                        u'Capçalera Cas')
    }

GiscedataSwitchingPuntMesura2()


class GiscedataSwitchingDetalleResultado(osv.osv):
    ''' Tabla 73 From CNMC Gestión ATR documentation'''

    _name = 'giscedata.switching.detalle.resultado'

    def get_detalle(self, cursor, uid, code, context=None):
        '''returns detalle id with name = code'''

        search_params = [('name', '=', code)]
        detalle_ids = self.search(cursor, uid, search_params)
        if detalle_ids:
            return detalle_ids[0]
        return False

    _columns = {
        'name': fields.char('Codi', size=7),
        'text': fields.char('Descripció', size=120),
    }

GiscedataSwitchingDetalleResultado()


class GiscedataSwitchingXmlEncoding(osv.osv):
    """ Possibles encodings dels XML's generats
    """
    _name = 'giscedata.switching.xml.encoding'

    _columns = {
        'name': fields.char(u'Nom', size=32, required=True),
        'acronim': fields.char(u'Acrònim', size=32, required=True)
    }

GiscedataSwitchingXmlEncoding()


class GiscedataSwitchingNotificacioPasRebuig(osv.osv):
    """
    Model per les notificacions de passos de casos ATR de rebuig
    """

    _name = 'giscedata.switching.notify'

    def get_notification_text(
            self, cursor, uid, notify_id, sw_id, context=False):
        from mako.template import Template
        if not context:
            context = {}
        if isinstance(notify_id, list):
            notify_id = notify_id[0]
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id) if sw_id else {}
        notify = self.read(
            cursor, uid, notify_id, ['notify_text', 'rebuig_text', 'step_id'],
            context=context)
        pas = sw.get_pas(step_id=notify['step_id']) if sw else {}
        t = Template(text=notify['notify_text'])
        return t.render_unicode(cas=sw, pas=pas)

    def _ff_notifica_rebuig(
            self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        proc_obj = self.pool.get('giscedata.switching.proces')
        step_obj = self.pool.get('giscedata.switching.step')
        for notify in self.read(cursor, uid, ids, [
            'proces_id', 'step_id'
        ], context=context):
            if notify['proces_id'] and notify['step_id']:
                step_name = step_obj.read(
                    cursor, uid, notify['step_id'][0], ['name'])['name']
                proc_name = proc_obj.read(
                    cursor, uid, notify['proces_id'][0], ['name'])['name']
                res[notify['id']] = (
                    step_name in proc_obj.get_reject_steps(
                        cursor, uid, proc_name
                    )
                )
            else:
                res[notify.id] = False
        return res

    def _ff_notifica_rebuig_search(
            self, cursor, uid, obj, field_name, args, context):
        ids = []
        notify_ids = self.search(
            cursor, uid, [], context={'active_test': False})
        rebuig = self._ff_notifica_rebuig(
            cursor, uid, notify_ids, field_name, args, context)
        for notify_id in notify_ids:
            if rebuig[notify_id]:
                ids.append(notify_id)
        return [('id', 'in', ids)]

    def _ff_info_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        for notify in self.browse(cursor, uid, ids, context=context):
            res[notify.id] = ''
            if notify.rebuig_ids:
                for rebuig in notify.rebuig_ids:
                    if res[notify.id]:
                        res[notify.id] += '\n'
                    res[notify.id] += '{} - {}'.format(
                        rebuig.name, rebuig.text
                    )
        return res

    _columns = {
        'step_id': fields.many2one(
            'giscedata.switching.step', 'Pas del procés'),
        'proces_id': fields.many2one(
            'giscedata.switching.proces', 'Procés ATR'),
        'notify_text': fields.text('Text per la notificació'),
        'rebuig_ids': fields.many2many(
            'giscedata.switching.motiu.rebuig',
            'giscedata_switching_notify_rebuigs_rel',
            id1='notify_id', id2='rebuig_id', string='Rebuigs acceptats'),
        'active': fields.boolean('Notificació activa'),
        'notifica_rebuig': fields.function(
            _ff_notifica_rebuig,
            method=True,
            string="Notificació amb Rebuig",
            type="boolean",
            fnct_search=_ff_notifica_rebuig_search),
        'info_rebuig': fields.function(
            _ff_info_rebuig,
            method=True,
            string='Informació dels rebuigs',
            type="text",
        )
    }

    _defaults = {
        'active': lambda *a: False
    }

GiscedataSwitchingNotificacioPasRebuig()


class GiscedataSwitchingLog(osv_mongodb.osv_mongodb):
    """Class to store atr cases log process"""

    _name = 'giscedata.switching.log'
    _description = 'Classe log per els casos atr'
    _rec_name = 'file'

    def create(self, cursor, uid, values, context=None):
        '''Create log line or update one'''
        if context is None:
            context = {}
        if not context.get("update_logs"):
            return super(GiscedataSwitchingLog, self).create(
                cursor, uid, values, context=context
            )

        data_actual = datetime.now() - timedelta(hours=1)
        data_actual = data_actual.strftime("%Y-%m-%d %H:%M:%S")
        search_params = [
            ('origin', '=', values.get('origin')),
            ('file', '=', values.get('file')),
            ('request_code', '=', values.get('request_code')),
            ('pas', '=', values.get('pas')),
            ('proces', '=', values.get('proces')),
            ('codi_emissor', '=', values.get('codi_emissor')),
            ('tipus', '=', values.get('tipus')),
            ('cups_text', '=', values.get('cups_text')),
            ('status', '=', 'error'),
            ('case_date', '>=', data_actual)
        ]
        log_exists = self.search(cursor, uid, search_params)
        if log_exists:
            update_data = {
                'case_date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'status': values['status'],
                'info': values['info']
            }
            self.write(cursor, uid, log_exists, update_data)
            return log_exists[0]
        else:
            return super(GiscedataSwitchingLog, self).create(
                cursor, uid, values, context=context
            )

    def name_get(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        res = []

        for record in self.read(cursor, uid, ids, ['file', 'origin'], context):
            name = record['file'] or record['origin']
            res.append((record['id'], name))
        return res

    def unlink(self, cursor, uid, ids, context=None):
        ir_attachment_obj = self.pool.get('ir.attachment')
        attachment_values = [
            ('res_id', 'in', ids),
            ('res_model', '=', 'giscedata.switching.log'),
        ]
        attach_ids = ir_attachment_obj.search(cursor, uid, attachment_values)
        res = super(GiscedataSwitchingLog, self).unlink(cursor, uid, ids, context=context)
        ir_attachment_obj.unlink(cursor, uid, attach_ids)
        return res

    _columns = {
        'case_date': fields.datetime('Data de creació', select=True,
                                 required=True
                                 ),
        'origin': fields.char('Origen', size=128, required=True, select=1),
        'file': fields.char('Fitxer', size=128, required=True, select=1),
        'request_code': fields.char('Codi sol·licitud', size=12, select=1),
        'status': fields.selection([('error', 'Error'), ('correcte', 'Correcte')],
                                   'Estat', required=True, select=1
                                   ),
        'info': fields.text('Informació', readonly=True),
        'pas': fields.char('Pas', size=2, select=1),
        'proces': fields.char('Procés', size=2, select=1),
        'emisor_id': fields.many2one('res.partner', 'Emissor', readonly=True, select=1),
        'codi_emisor': fields.char('Codi emissor', size=4),
        'tipus': fields.selection(
            [('import', 'Importació'), ('export', 'Exportació'),
            ('generate', 'Generació')], 'Tipus',
            readonly=True
        ),
        'cups_text': fields.char('CUPS', size=128)
    }

    _defaults = {
        'case_date': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
    }

    _order = "case_date desc, id desc"


GiscedataSwitchingLog()


class GiscedataSwitchingActivacionsConfig(osv.osv):
    """Class to store atr cases log process"""

    _name = 'giscedata.switching.activation.config'
    _description = "Configuracio d'activacions ATR"

    def get_activation_method(self, cursor, uid, pas, context=None):
        """
        :param pas: browse object of step (giscedata.switching.{proces}.{step}) to activate.
        :return: string with method name or False
        """
        if context is None:
            context = {}
        logger = logging.getLogger('openerp')
        sw = pas.sw_id
        proces_id = sw.proces_id.id
        step_id = sw.step_id.id
        whereiam = sw.whereiam

        search_params = [
            ('proces_id', '=', proces_id),
            ('step_id', '=', step_id),
            ('comerdist', 'in', ["all", whereiam]),
            ('active', '=', True)
        ]
        if context.get("activate_only_automatic"):
            search_params.append(('is_automatic', '=', True))

        sql = self.q(cursor, uid).select(['id', 'conditions', 'method']).where(search_params)
        cursor.execute(*sql)
        res = []
        for info in cursor.dictfetchall():
            try:
                if info.get("conditions", "[]") != "Aplica sempre":
                    conditions = eval(info.get("conditions", "[]"))
                else:
                    conditions = []
            except Exception, e:
                logger.error("ATR: condiccions de l'activacio {0} mal definides: {1}".format(info.get('id'), e))
                continue
            # Si no te condicions vol dir que s'utilitza sempre
            if not len(conditions):
                res.append(info.get('method'))
                continue
            # Altrament hem de comprovar si passa les condicions
            ok = True
            for pas_name, camp, operant, valor in conditions:
                if isinstance(valor, str):
                    valor = "'{0}'".format(valor)
                pas_to_check = pas
                if pas_name != pas._nom_pas:
                    pas_to_check_obj = self.pool.get("giscedata.switching.{0}.{1}".format(sw.proces_id.name.lower(), pas_name))
                    pas_to_check_id = pas_to_check_obj.search(cursor, uid, [("sw_id", "=", sw.id)])
                    if not len(pas_to_check_id):
                        continue
                    pas_to_check = pas_to_check_obj.browse(cursor, uid, pas_to_check_id[0])
                if not eval("pas_to_check.{0} {1} {2}".format(camp, operant, valor)):
                    ok = False
                    break
            # Hem passat totes les condicions
            if ok:
                res.append(info.get('method'))
        return res

    _columns = {
        'proces_id': fields.many2one(
            'giscedata.switching.proces', 'Proces', required=True, select=1),
        'step_id': fields.many2one(
            'giscedata.switching.step', 'Pas', required=True, select=1),
        'description': fields.text(
            'Descripcio', required=True, select=1, translate=True),
        'method': fields.char('Metode activacio', size=64),
        'conditions': fields.char(
            "Condicions Extres", size=256, help=(
                u"Condicions a aplicar per poder fer activacio."
                u" Llistat de tuples del tipus"
                u" (pas_name, camp, operant, valor)."
            )
        ),
        'comerdist': fields.selection(string='Tipus', selection=[
            ('all', 'Comer. i Distri.'),
            ('comer', "Comercialitzadora"),
            ('distri', "Distribuidora")
        ]),
        'active': fields.boolean("Actiu"),
        'is_automatic': fields.boolean(
            "Activacio automàtica",
            help="Si es marca aquesta opció, l'activació s'executarà "
                 "automàticament al importar el XML del pas corresponent."
        ),
    }

    _defaults = {
        'comerdist': lambda *a: 'all',
        'conditions': lambda *a: '[]',
        'active': lambda *a: True,
        'is_automatic': lambda *a: False,
    }

    _order = "proces_id asc, step_id asc, method"

GiscedataSwitchingActivacionsConfig()


class GiscedataSwitchingNotificacionsConfig(osv.osv):

    _name = 'giscedata.switching.notificacio.config'
    _description = "Configuracio de notificacions ATR"

    def check_is_notificable(self, cursor, uid, pas, context=None):
        """
        :param pas: browse object of step (giscedata.switching.{proces}.{step}) to notify.
        :return: string with method name or False
        """
        if context is None:
            context = {}
        if self.is_in_sw_mail_user_notification_on_activation(cursor, uid, pas.sw_id.proces_id.name, pas._nom_pas):
            return True
        logger = logging.getLogger('openerp')
        sw = pas.sw_id
        proces_id = sw.proces_id.id
        step_id = sw.step_id.id
        whereiam = sw.whereiam

        search_params = [
            ('proces_id', '=', proces_id),
            ('step_id', '=', step_id),
            ('active', '=', True)
        ]
        sql = self.q(cursor, uid).select(['id', 'conditions', 'check_cas_tancat']).where(search_params)
        cursor.execute(*sql)
        for info in cursor.dictfetchall():
            try:
                if context.get('check_cas_tancat_on_activate') and info.get("check_cas_tancat") and sw.state != "done":
                    continue
                if info.get("conditions", "[]") != "Aplica sempre":
                    conditions = eval(info.get("conditions", "[]"))
                else:
                    conditions = []
            except Exception, e:
                logger.error("ATR: condiccions de la notificació {0} mal definides: {1}".format(info.get('id'), e))
                continue
            # Si no te condicions vol dir que s'utilitza sempre
            if not len(conditions):
                return True
            # Altrament hem de comprovar si passa les condicions
            ok = True
            for pas_name, camp, operant, valor in conditions:
                if isinstance(valor, str):
                    valor = "'{0}'".format(valor)
                pas_to_check = pas
                if pas_name != pas._nom_pas:
                    pas_to_check_obj = self.pool.get("giscedata.switching.{0}.{1}".format(sw.proces_id.name.lower(), pas_name))
                    pas_to_check_id = pas_to_check_obj.search(cursor, uid, [("sw_id", "=", sw.id)])
                    if not len(pas_to_check_id):
                        continue
                    pas_to_check = pas_to_check_obj.browse(cursor, uid, pas_to_check_id[0])
                if not eval("pas_to_check.{0} {1} {2}".format(camp, operant, valor)):
                    ok = False
                    break
            # Hem passat totes les condicions
            if ok:
                return True
        return False

    def is_in_sw_mail_user_notification_on_activation(self, cursor, uid, proces_name, step_name, context=None):
        conf_obj = self.pool.get("res.config")
        notificable_cases = conf_obj.get(
            cursor, uid, 'sw_mail_user_notification_on_activation', False
        )
        if notificable_cases == 'all':
            notificacio = True
        elif notificable_cases:
            pas_name = '{}-{}'.format(proces_name.upper(), step_name)
            notificable_cases = eval(notificable_cases)
            notificacio = True if pas_name in notificable_cases else False
        else:
            notificacio = False
        return notificacio

    _columns = {
        'proces_id': fields.many2one(
            'giscedata.switching.proces', 'Proces', required=True, select=1),
        'step_id': fields.many2one(
            'giscedata.switching.step', 'Pas', required=True, select=1),
        'description': fields.text(
            'Descripcio', required=True, select=1, translate=True
        ),
        'conditions': fields.char(
            "Condicions Extres", size=256, help=(
                u"Condicions a aplicar per poder fer la notificacio."
                u" Llistat de tuples del tipus"
                u" (pas_name, camp, operant, valor)."
            )
        ),
        'active': fields.boolean("Actiu"),
        'check_cas_tancat': fields.boolean("Notificar nomes quan el cas ATR esta tancat i finalitzat")
    }

    _defaults = {
        'conditions': lambda *a: '[]',
        'active': lambda *a: False,
        'check_cas_tancat': lambda *a: False,
    }

    _order = "proces_id asc, step_id asc, description"

GiscedataSwitchingNotificacionsConfig()
