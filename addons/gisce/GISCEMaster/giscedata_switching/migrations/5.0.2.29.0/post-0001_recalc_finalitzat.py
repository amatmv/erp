# -*- coding: utf-8 -*-
"""Recalcula el camp funció una vegada finalitzada la càrrega del mòdul.
La primera vegada no ho calcula bé, per que el mòdul de switching es
carrega abans que aquells que ho sobreescriuen
"""

import netsvc
import pooler

def migrate(cursor, installed_version):
    uid = 1
    logger = netsvc.Logger()
    
    pool = pooler.get_pool(cursor.dbname)
    sw_obj = pool.get('giscedata.switching')
    search_params = []
    sw_ids = sw_obj.search(cursor, uid, search_params)
    for sw_id in sw_ids:
        sw = sw_obj.browse(cursor, uid, sw_id)
        if sw.step_ids:
            sw.step_ids[0].write({})

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         "Camp finalitzat recalculat correctament")
