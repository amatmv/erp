# -*- coding: utf-8 -*-
"""Passa tipus activacio de character varying(1) a character varying(2)
"""
import pooler
import logging
from tqdm import tqdm


def up(cursor, installed_version):
    logger = logging.getLogger('migration')

    if not installed_version:
        return

    logger.info('Posar els casos tancats amb agent_accio_pendent \'comer\'')
    query = """UPDATE giscedata_switching SET accio_pendent_comerdist = 'comer' WHERE id in (
        select atr.id from giscedata_switching atr JOIN crm_case cas on cas.id=atr.case_id where cas.state in ('done', 'cancel')
    )"""
    cursor.execute(query)
    logger.info('Fet')
    logger.info('Calcular el camp agent_accio_pendent per els casos no tancats')

    logger.info('Actualitzant casos no rebutjats amb accio pendent comer')
    query = """UPDATE giscedata_switching SET accio_pendent_comerdist = 'comer' WHERE id in 
(
SELECT atr.id FROM giscedata_switching atr JOIN crm_case cas ON cas.id=atr.case_id JOIN giscedata_switching_step sw_step ON atr.step_id=sw_step.id
WHERE cas.state not in ('done', 'cancel') 
AND not (atr.additional_info like '%Rebuig%' or atr.additional_info like '%Rechazo%') 
AND sw_step.accio_pendent = 'comer'
)"""
    cursor.execute(query)

    logger.info('Actualitzant casos no rebutjats amb accio pendent distri')
    query = """UPDATE giscedata_switching SET accio_pendent_comerdist = 'distri' WHERE id in 
(
SELECT atr.id FROM giscedata_switching atr JOIN crm_case cas ON cas.id=atr.case_id JOIN giscedata_switching_step sw_step ON atr.step_id=sw_step.id
WHERE cas.state not in ('done', 'cancel') 
AND not (atr.additional_info like '%Rebuig%' or atr.additional_info like '%Rechazo%') 
AND sw_step.accio_pendent = 'distri'
)"""
    cursor.execute(query)

    logger.info('Actualitzant casos rebutjats amb accio pendent distri')
    query = """UPDATE giscedata_switching SET accio_pendent_comerdist = 'distri' WHERE id in 
(
SELECT atr.id FROM giscedata_switching atr JOIN crm_case cas ON cas.id=atr.case_id JOIN giscedata_switching_step sw_step ON atr.step_id=sw_step.id
WHERE cas.state not in ('done', 'cancel') 
AND (atr.additional_info like '%Rebuig%' or atr.additional_info like '%Rechazo%')
AND sw_step.accio_pendent = 'comer'
)"""
    cursor.execute(query)

    logger.info('Actualitzant casos rebutjats amb accio pendent comer')
    query = """UPDATE giscedata_switching SET accio_pendent_comerdist = 'comer' WHERE id in 
(
SELECT atr.id FROM giscedata_switching atr JOIN crm_case cas ON cas.id=atr.case_id JOIN giscedata_switching_step sw_step ON atr.step_id=sw_step.id
WHERE cas.state not in ('done', 'cancel') 
AND (atr.additional_info like '%Rebuig%' or atr.additional_info like '%Rechazo%')
AND sw_step.accio_pendent = 'distri'
)"""
    cursor.execute(query)

    logger.info('Fet')

migrate = up
