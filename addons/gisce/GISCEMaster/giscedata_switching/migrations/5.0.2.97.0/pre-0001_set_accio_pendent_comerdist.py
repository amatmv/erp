# -*- coding: utf-8 -*-
"""Passa tipus activacio de character varying(1) a character varying(2)
"""
import pooler
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('migration')

    if not installed_version:
        return

    logger.info('Crear columna agent_accio_pendent a giscedata_switching')
    query = """ALTER TABLE giscedata_switching ADD accio_pendent_comerdist text"""
    cursor.execute(query)
    logger.info('Fet')

migrate = up
