# -*- coding: utf-8 -*-
"""Passa tipus activacio de character varying(1) a character varying(2)
"""

import logging


def up(cursor, installed_version):
    logger = logging.getLogger('migration')

    if not installed_version:
        return

    logger.info('Eliminant les plantilles de rebuig del ir_model_data')
    query = """DELETE FROM ir_model_data
WHERE model ilike 'poweremail.templates'
AND res_id IN (
  SELECT id FROM poweremail_templates
  WHERE NAME ilike 'ATR%%rechazo%%'
)
    """
    cursor.execute(query)
    logger.info('Eliminant les plantilles de rebuig de poweremail_templates')
    query = """DELETE FROM poweremail_templates
  WHERE NAME ilike 'ATR%%rechazo%%'"""
    cursor.execute(query)
    logger.info('Plantilles de rebuig eliminades!')

migrate = up
