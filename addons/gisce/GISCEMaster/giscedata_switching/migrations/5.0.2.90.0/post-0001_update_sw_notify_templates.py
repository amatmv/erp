# -*- coding: utf-8 -*-
"""
Copiar el cos dels missatges de les plantilles "antigues" a les noves
"""
import logging
import pooler


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    template_obj = pool.get('poweremail.templates')
    model_data_obj = pool.get('ir.model.data')

    new_a305 = 'notification_atr_A3_05'
    old_a305 = 'switching_notificacio_activacio_a3_email'
    old_m105 = 'switching_notificacio_activacio_m1_email'
    new_m105 = 'notification_atr_M1_05'
    old_m102 = 'switching_notificacio_accceptacio_m1_email'
    new_m102 = 'notification_atr_M1_02_acceptacio'
    old_change = 'switching_notificacio_activacio_cn_email'
    new_change1 = 'notification_atr_C1_05'
    new_change2 = 'notification_atr_C2_05'

    update_query = """
    UPDATE ir_model_data
    SET res_id = %s
    WHERE name = %s
    """

    for new_templ_name, old_templ_name in [
        (new_a305, old_a305),
        (new_m102, old_m102),
        (new_m105, old_m105),
        (new_change1, old_change),
        (new_change2, old_change)
    ]:
        logger.info('Updating template "{}" from "{}"'.format(
            new_templ_name, old_templ_name))
        new_template_id = model_data_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', new_templ_name
        )[1]
        old_template_id = model_data_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', old_templ_name
        )[1]
        new_name = template_obj.read(
            cursor, uid, [new_template_id], ['name'])[0]['name']
        template_obj.unlink(cursor, uid, [new_template_id])
        tid = template_obj.copy(cursor, uid, old_template_id)
        template_obj.write(cursor, uid, [tid], {
            'name': new_name
        })
        cursor.execute(update_query, (tid, new_templ_name))


migrate = up
