# -*- coding: utf-8 -*-
"""
Actualitzar les plantilles de power email perquè el correu sigui el de la
direcció de notificació en comptes de la primera adreça del titular
"""
import logging
import pooler


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1

    template_obj = pool.get('poweremail.templates')
    imd_obj = pool.get('ir.model.data')

    xml_data_ids = [
        # A3
        'notification_atr_A3_01',
        'notification_atr_A3_02_acceptacio',
        'notification_atr_A3_02_rebuig',
        'notification_atr_A3_03',
        'notification_atr_A3_04',
        'notification_atr_A3_05',
        'notification_atr_A3_06',
        'notification_atr_A3_07_acceptacio',
        'notification_atr_A3_07_rebuig',

        # B1
        'notification_atr_B1_01',
        'notification_atr_B1_02_acceptacio',
        'notification_atr_B1_02_rebuig',
        'notification_atr_B1_03',
        'notification_atr_B1_04_rebuig',
        'notification_atr_B1_04_acceptacio',
        'notification_atr_B1_05',
        'notification_atr_B1_06',
        'notification_atr_B1_07',

        # C1
        'notification_atr_C1_01',
        'notification_atr_C1_02_acceptacio',
        'notification_atr_C1_02_rebuig',
        'notification_atr_C1_04',
        'notification_atr_C1_05',
        'notification_atr_C1_06',
        'notification_atr_C1_08',
        'notification_atr_C1_09_acceptacio',
        'notification_atr_C1_09_rebuig',
        'notification_atr_C1_10',
        'notification_atr_C1_11',
        'notification_atr_C1_12',

        # C2
        'notification_atr_C2_01',
        'notification_atr_C2_02_acceptacio',
        'notification_atr_C2_02_rebuig',
        'notification_atr_C2_03',
        'notification_atr_C2_04',
        'notification_atr_C2_05',
        'notification_atr_C2_06',
        'notification_atr_C2_08',
        'notification_atr_C2_09_acceptacio',
        'notification_atr_C2_09_rebuig',
        'notification_atr_C2_10',
        'notification_atr_C2_11',
        'notification_atr_C2_12',

        # M1
        'notification_atr_M1_01',
        'notification_atr_M1_02_acceptacio',
        'notification_atr_M1_02_rebuig',
        'notification_atr_M1_03',
        'notification_atr_M1_04',
        'notification_atr_M1_05',
        'notification_atr_M1_06',
        'notification_atr_M1_07_acceptacio',
        'notification_atr_M1_07_rebuig',

        # R1
        'notification_atr_R1_01',
        'notification_atr_R1_02_acceptacio',
        'notification_atr_R1_02_rebuig',
        'notification_atr_R1_03',
        'notification_atr_R1_04',
        'notification_atr_R1_05',

        # W1
        'notification_atr_W1_01',
        'notification_atr_W1_02_acceptacio',
        'notification_atr_W1_02_rebuig',
    ]

    template_ids = [imd_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', xml_data_id
        )[1] for xml_data_id in xml_data_ids]

    old_def_to = '${object.cups_polissa_id.titular.address[0].email}'
    new_def_to = '${object.cups_polissa_id.direccio_notificacio.email}'
    template_ids_to_update = template_obj.search(
        cursor, uid, [('id', 'in', template_ids), ('def_to', '=', old_def_to)]
    )

    logger.info('Updating "{}" poweremail templates'.format(
        len(template_ids_to_update))
    )
    for template_id in template_ids_to_update:
        logger.info('Updating template "{}"'.format(template_id))
        template_obj.write(cursor, uid, [template_id], {'def_to': new_def_to})

    logger.info('Updated poweremail templates successfully!')


def down(cursor, installed_version):
    pass


migrate = up
