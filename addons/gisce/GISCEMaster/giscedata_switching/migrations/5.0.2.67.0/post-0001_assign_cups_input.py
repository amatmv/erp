# -*- coding: utf-8 -*-
"""Assigna el valor cups_id.name a tots els  cups_input de la base de dades.
"""

from oopgrade import oopgrade
import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()

    if not installed_version:
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Start of the script to assign cups_input')
    cursor.execute("UPDATE giscedata_switching AS s "
                   "SET cups_input=c.name "
                   "FROM giscedata_cups_ps AS c "
                   "WHERE s.cups_id = c.id")
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Successfully changed!')


migrate = up
