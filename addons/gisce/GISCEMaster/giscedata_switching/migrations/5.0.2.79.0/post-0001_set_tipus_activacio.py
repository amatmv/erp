# -*- coding: utf-8 -*-
"""Passa tipus activacio de character varying(1) a character varying(2)
"""

import netsvc


def up(cursor, installed_version):
    logger = netsvc.Logger()

    if not installed_version:
        return

    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Start of the script to alter tipus_activacio size')
    cursor.execute("ALTER TABLE giscedata_switching_a3_02 ALTER COLUMN tipus_activacio TYPE character varying(2)")
    cursor.execute("ALTER TABLE giscedata_switching_b1_02 ALTER COLUMN tipus_activacio TYPE character varying(2)")
    cursor.execute("ALTER TABLE giscedata_switching_c1_02 ALTER COLUMN tipus_activacio TYPE character varying(2)")
    cursor.execute("ALTER TABLE giscedata_switching_c2_02 ALTER COLUMN tipus_activacio TYPE character varying(2)")
    cursor.execute("ALTER TABLE giscedata_switching_m1_02 ALTER COLUMN tipus_activacio TYPE character varying(2)")
    logger.notifyChannel('migration', netsvc.LOG_INFO,
                         'Successfully changed!')


migrate = up
