# -*- coding: utf-8 -*-
import dateutil.parser
from dateutil.relativedelta import relativedelta
from datetime import datetime


def add_months(sourcedate, months):
    """Adds n months to a iso format sourcedate"""
    parsed_date = dateutil.parser.parse(sourcedate)
    new_date = parsed_date + relativedelta(months=+months)
    return new_date.strftime("%Y-%m-%d")


def calc_months(data_inici, data_final):
    """Calculate the months from data_inici to data_fi"""
    format_date = "%Y-%m-%d"
    data_inici_f = datetime.strptime(data_inici, format_date)
    data_final_f = datetime.strptime(data_final, format_date)
    anys = (data_final_f.year - data_inici_f.year) * 12
    mesos = data_final_f.month - data_inici_f.month
    return anys + mesos


def get_address_dicct(address):
    if address.nv:
        via = address.nv[:30]
    else:
        via = " "
    addres_dict = {
        'pais': address.country_id.name,
        'provincia': address.state_id.code,
        'municipio': address.id_municipi.ine,
        'poblacion': address.id_municipi.ine,
        'codpostal': address.zip,
        'calle': via,
        'numfinca': address.pnp or '0',
        'apartado_de_correos': address.apartat_correus
    }
    return addres_dict


def check_contracte_pas_is_eventual(step):
    """
    :param step: browse of giscedata.switching.a3.01 or giscedata.switching.m1.01 or giscedata.switching.c2.01
    :return:
    """
    return step.tipus_contracte in ['02', '03', '07', '09']


def check_contracte_is_eventual(polissa):
    """
    :param polissa: browse of giscedata.polissa
    :return:
    """
    return polissa.contract_type in ['02', '03', '09']
