<%
    from gestionatr import defs
    from giscedata_facturacio.giscedata_polissa import FACTURACIO_POTENCIA_SELECTION

    pool = objects[0].pool
    cursor = objects[0]._cr
    uid = user.id
    if data and len(data.get('casos', [])):
        casos_ids = data.get('casos')
        sw_obj = pool.get("giscedata.switching")
        casos = sw_obj.browse(cursor, uid, casos_ids)
    else:
        casos = objects

    def get_pas01(cas):
        pas01 = cas.step_ids[0].pas_id.split(',')
        pas_obj = objects[0].pool.get(pas01[0])
        return pas_obj.browse(cursor, uid, long(pas01[1]))

    def get_desc_taula(atribut, taula):
        field = [x[1] for x in taula if x[0] == atribut]
        if len(field) > 0:
          return field[0]
        return None
    setLang(user.context_lang)
%>
<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
  <style type="text/css">
    ${css}
  </style>
  <link rel="stylesheet" href="${addons_path}/giscedata_switching/report/info_cas_ATR.css"/>
</head>
<body>
%for cas in casos:
<%
report_language = cas.get_idioma()[cas.id]
setLang(report_language)
pas = get_pas01(cas)
activacio = pas.activacio_cicle

if not activacio:
  activacio = pas.activacio

comptador_id = cas.cups_polissa_id.comptadors_actius(cas.data_sollicitud.val, cas.data_sollicitud.val)

comptador = ""
if comptador_id:
    if len(comptador_id) == 1:
        meter_o = pool.get('giscedata.lectures.comptador')
        comptador = meter_o.read(cursor, uid, comptador_id, ['name'])
        comptador = comptador[0]['name']
        if not comptador:
            comptador = ""
%>
<div id="header">
    <div style="float:left; width: 25%;">
        <div id="logo">
            <img id="logo" src="data:image/jpeg;base64,${company.logo}" style='height: 50px'/>
        </div>
    </div>
    <div style="float:left; width: 55%;">
        <div class="company">
            ${company.name}
        </div>
        <div class="text_capcalera">
            <%block name="text_capcalera">
            </%block>
        </div>
    </div>
    <div style="float:right; width: 20%;">
        <div class="text_capcalera">
            ${_(u"Data sol.licitud:")}
            ${formatLang(pas.date_created, date=True)}
            <br>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="titol" style="font-size: 14px;">
    % if cas.proces_id.name == 'A3':
    ${_(u"SOL·LICITUD D'ALTA DE SUBMINISTRAMENT")}
    % elif cas.proces_id.name == 'C1':
    ${_(u"SOL·LICITUD DE CANVI DE COMERCIALITZADORA")}
    % elif cas.proces_id.name == 'C2':
    ${_(u"SOL·LICITUD DE CANVI DE COMERCIALITZADORA AMB CANVIS")}
    % elif cas.proces_id.name == 'B1':
    ${_(u"SOL·LICITUD DE BAIXA DE SUBMINISTRAMENT")}
    % elif cas.proces_id.name == 'R1':
    ${_(u"RECLAMACIO")}
    % elif cas.proces_id.name == 'M1':
    ${_(u"SOL·LICITUD DE MODIFICACIONS CONTRACTUALS")}
    % endif
    </div>
</div>

<div class="seccio">
<!-- DADES SOL·LICITUD -->
${_(u"DADES DE LA SOL·LICITUD")}
<br>
    % if cas.proces_id.name in ['M1', 'C2']:
      <table style="margin-top: 5px;">
        <tr>
          <td><div class="field" style="width: 100%;">${get_desc_taula(pas.sollicitudadm, defs.TABLA_7)}</div></td>
          <td></td>
        </tr>
        % if pas.sollicitudadm != 'N':
          <tr>
            <td><div class="field" style="width: 100%;">${get_desc_taula(pas.canvi_titular, defs.TABLA_53)}</div></td>
            <td></td>
          </tr>
        % endif
      </table>
    % elif cas.proces_id.name == 'B1':
       <table style="margin-top: 5px;">
        <tr>
          <td><div class="label">${_(u"Tipus Baixa:")} </div> <div class="field">${get_desc_taula(pas.motiu, defs.TABLA_10)}</div></td>
          <td></td>
        </tr>
      </table>
    % endif
     <table style="margin-top: 5px;">
      <tr>
        % if activacio == 'F':
          <td><div class="label">${_(u"Activació Sol.licitada:")} </div> <div class="field">${_(u"A data fixa.")}</div></td>
          <td><div class="label">${_(u"Data:")} </div> <div class="field">${pas.data_accio}</div></td>
        % else:
          <td><div class="label">${_(u"Activació Sol.licitada:")} </div> <div class="field">${get_desc_taula(activacio, defs.TABLA_8)}</div></td>
          <td></td>
        % endif
      </tr>
    </table>
     <table style="margin-top: 5px;">
      <tr>
        <td><div class="label">${_(u"Codi Sol.licitud:")} </div> <div class="field">${cas.codi_sollicitud}</div></td>
        <td><div class="label">${_(u"CUPS:")} </div> <div class="field">${cas.cups_input}</div></td>
      </tr>
    </table>
     <table style="margin-top: 5px;">
      <tr>
        <td><div class="label">${_(u"Sol·licitant:")} </div> <div class="field">${pas.emisor_id.name}</div></td>
        <td><div class="label">${_(u"Receptor:")} </div> <div class="field">${pas.receptor_id.name}</div></td>
      </tr>
    </table>

    % if cas.proces_id.name in ['R1']:
    <div class="seccio">
        <br>
        ${_(u"TIPUS RECLAMACIÓ")}
        <br>
        <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">(${pas.tipus})  ${get_desc_taula(pas.tipus, defs.TABLA_81)}</div></td>
              <td></td>
            </tr>
            <tr>
              <td><div class="label">(${pas.subtipus_id.name}) ${pas.subtipus_id.desc}</div></td>
              <td></td>
            </tr>
        </table>
    </div>
    % endif
<!-- FI DADES SOL·LICITUD -->
</div>

<!-- DADES ADMINISTRATIVES -->
%if cas.proces_id.name in ['A3', 'M1', 'C2', 'B1', 'R1', 'C1']:
    <%
        direccio = pas.fiscal_address_id
        if not direccio:
          direccio = cas.cups_polissa_id.direccio_pagament

        cnae = pas.cnae
        if not cnae:
          cnae = cas.cups_polissa_id.cnae

        telefon_str = ""
        for tel in pas.telefons:
          telefon_str += tel.numero + ". "
        cont_telefon_str = ""
        for tel in pas.cont_telefons:
          cont_telefon_str += tel.numero + ". "
        rec_telefon_str = ""
        for tel in pas.rec_telefons:
          rec_telefon_str += tel.numero + ". "
    %>
<div class="titol">${_(u"DADES ADMINISTRATIVES")}</div>
      <div class="seccio">
          ${_(u"DADES DEL CONTRACTE")}
          <br>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Comercialitzadora:")} </div> <div class="field">${pas.emisor_id.name}</div></td>
              %if cas.proces_id.name in ('M1', 'B1', 'C1', 'C2'):
                  <td><div class="label">${_(u"Contracte:")} </div> <div class="field">${cas.cups_polissa_id.name}</div></td>
              %else:
                  <td></td>
              %endif
            </tr>
          </table>
          %if cas.proces_id.name in ['A3', 'M1', 'C2']:
             <table style="margin-top: 5px;">
              <tr>
                <td><div class="label">${_(u"Tipus de Contracte:")} </div> <div class="field">${get_desc_taula(pas.tipus_contracte, defs.TABLA_9)}</div></td>
                <td><div class="label">${_(u"Tipus Autoconsum:")} </div> <div class="field">${get_desc_taula(pas.tipus_autoconsum, defs.TABLA_113)}</div></td>
              </tr>
              % if pas.tipus_contracte == '09':
                <tr>
                  <td><div class="label">${_(u"Consum Pactat:")} </div> <div class="field">${pas.expected_consumption} kW</td>
                  <td></td>
                </tr>
              % endif
            </table>
          % endif
      </div>
      <div class="seccio">
          ${_(u"DADES DEL CLIENT")}
          <br>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Nom/Motiu Social:")} </div> <div class="field">${pas.nom} ${pas.cognom_1} ${pas.cognom_2}</div></td>
              <td><div class="label">${_(u"NIF/CIF:")} </div> <div class="field">${pas.codi_document}</div></td>
            </tr>
          </table>
           <table style="margin-top: 5px;">
            <tr>
              <td style="width:100%;"><div class="label">${_(u"CNAE:")} </div> <div class="field">${cnae.descripcio} (${cnae.name})</td>
              <td></td>
            </tr>
          </table>
           <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Direccio:")} </div> <div class="field">${direccio.name_get()[0][1]}</div></td>
              <td></td>
            </tr>
            <tr>
              <td><div class="label">${_(u"Codi Postal/Població:")} </div> <div class="field">${direccio.zip} ${direccio.city}</div></td>
              <td><div class="label">${_(u"País:")} </div> <div class="field">${direccio.country_id.name}</div></td>
            </tr>
          </table>
           <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Telèfon:")} </div> <div class="field">${telefon_str}</div></td>
              <td></td>
            </tr>
          </table>
      </div>
      %if cas.proces_id.name in ['A3', 'M1', 'C2', 'B1']:
      <div class="seccio">
          ${_(u"DADES DE CONTACTE")}
          <br>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Nom/Motiu Social:")} </div> <div class="field">${pas.cont_nom}</div></td>
              <td><div class="label">${_(u"Teléfon:")} </div> <div class="field">${cont_telefon_str}</div></td>
            </tr>
          </table>
      </div>
      % endif
      % if cas.proces_id.name in ['M1'] and pas.sollicitudadm != 'N':
      <div class="seccio">
          ${_(u"DADES DEL ANTIC TITULAR")}
          <br>
          <%
             ant_titular = cas.cups_polissa_id.titular
          %>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Nom/Motiu Social:")} </div> <div class="field">${ant_titular.name}</div></td>
              <td><div class="label">${_(u"NIF/CIF:")} </div> <div class="field">${ant_titular.vat}</div></td>
            </tr>
          </table>
      </div>
      % endif
      % if cas.proces_id.name == 'R1' and pas.tipus_reclamant != '06':
      <div class="seccio">
          ${_(u"DADES RECLAMANT")}
          <br>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Nom/Motiu Social:")} </div> <div class="field">${pas.rec_nom} ${pas.rec_cognom_1} ${pas.rec_cognom_2}</div></td>
              <td><div class="label">${_(u"NIF/CIF:")} </div> <div class="field">${pas.rec_codi_document}</div></td>
            </tr>
          </table>
           <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Telèfon:")} </div> <div class="field">${rec_telefon_str}</div></td>
              <td><div class="label">${_(u"Email:")} </div> <div class="field">${pas.rec_email}</div></td>
            </tr>
          </table>
      </div>
      % endif
%endif
<!-- FI DADES ADMINISTRATIVES -->


<!-- DADES TÈCNO-ECONÒMIQUES -->
%if cas.proces_id.name in ['A3', 'M1', 'C2', 'B1']:

<div class="titol">${_(u"DADES TÈCNO-ECONÒMIQUES")}</div>
      <div class="seccio">
          ${_(u"DADES DEL PUNT DE SUBMINISTRAMENT")}
          <br>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"CUPS:")} </div> <div class="field">${cas.cups_input}</div></td>
              %if comptador:
                <td><div class="label">${_(u"Comptador:")} </div> <div class="field">${comptador}</div></td>
              %endif
            </tr>
          </table>
          % if cas.cups_id:
             <table style="margin-top: 5px;">
              <tr>
                <td><div class="label">${_(u"Direcció:")} </div> <div class="field">${cas.cups_id.direccio}</div></td>
                <td></td>
              </tr>
              <tr>
                <td><div class="label">${_(u"Codi Postal/Població:")} </div> <div class="field">${cas.cups_id.dp} ${cas.cups_id.id_poblacio.name}</div></td>
                <td><div class="label">${_(u"Província/País:")} </div> <div class="field">${cas.cups_id.id_provincia.name} ${cas.cups_id.id_provincia.country_id.name}</div></td>
              </tr>
            </table>
             <table style="margin-top: 5px;">
              <tr>
                <td><div class="label">${_(u"Distribuïdora:")} </div> <div class="field">${cas.cups_id.distribuidora_id.name}</div></td>
                <td></td>
              </tr>
            </table>
          % endif
      </div>
      %if cas.proces_id.name in ['A3', 'M1', 'C2']:
      <div class="seccio">
          ${_(u"DADES TARIFA D'ACCÉS")}
          <br>
          <%
          pots = {0: " ", 1: " ", 2: " ", 3: " ", 4: " ", 5: " "}
          npots = 0
          for p in pas.header_id.pot_ids:
              pots[npots] = p.potencia/1000.0
              npots += 1
          %>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Tarifa:")} </div> <div class="field">${get_desc_taula(pas.tarifaATR, defs.TABLA_17)}</div></td>
              % if cas.tarifa_comer_id:
                <td><div class="label">${_(u"Tarifa de Comercialitzadora:")} </div> <div class="field">${cas.tarifa_comer_id.name}</div></td>
              % else:
                <td></td>
              % endif
            </tr>
          </table>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Potència Contractada:")} </div> <div class="field">${pots[0]} kW</div></td>
              <td><div class="label">${_(u"Control Potència:")} </div> <div class="field">${get_desc_taula(pas.control_potencia, defs.TABLA_51)}</div></td>
            </tr>
          </table>
          <table style="margin-top: 5px;">
            <tr>
              <td style="width: 16%"><div class="label">P1: </div> <div class="field">${pots[0]}</div></td>
              <td style="width: 16%"><div class="label">P2: </div> <div class="field">${pots[1]}</div></td>
              <td style="width: 16%"><div class="label">P3: </div> <div class="field">${pots[2]}</div></td>
              <td style="width: 16%"><div class="label">P4: </div> <div class="field">${pots[3]}</div></td>
              <td style="width: 16%"><div class="label">P5: </div> <div class="field">${pots[4]}</div></td>
              <td style="width: 16%"><div class="label">P6: </div> <div class="field">${pots[5]}</div></td>
            </tr>
          </table>
      </div>
      % endif
      % if cas.proces_id.name in ['M1'] and pas.sollicitudadm != 'S':
      <div class="seccio">
          ${_(u"DADES TARIFA D'ACCÉS ANTERIOR")}
          <br>
          <%
          polissa = cas.cups_polissa_id
          pots = {0: " ", 1: " ", 2: " ", 3: " ", 4: " ", 5: " "}
          npots = 0
          for p in polissa.potencies_periode:
              pots[npots] = float(p.potencia)
              npots += 1
          %>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Tarifa:")} </div> <div class="field">${polissa.tarifa.name}</div></td>
              % if polissa.llista_preu:
                <td><div class="label">${_(u"Tarifa de Comercialitzadora:")} </div> <div class="field">${polissa.llista_preu.name}</div></td>
              % else:
                <td></td>
              % endif
            </tr>
          </table>
          <table style="margin-top: 5px;">
            <tr>
              <td><div class="label">${_(u"Potència Contractada:")} </div> <div class="field">${pots[0]} kW</div></td>
              <td><div class="label">${_(u"Control Potència:")} </div> <div class="field">${get_desc_taula(polissa.facturacio_potencia, FACTURACIO_POTENCIA_SELECTION)}</div></td>
            </tr>
          </table>
          <table style="margin-top: 5px;">
            <tr>
              <td style="width: 16%"><div class="label">P1: </div> <div class="field">${pots[0]}</div></td>
              <td style="width: 16%"><div class="label">P2: </div> <div class="field">${pots[1]}</div></td>
              <td style="width: 16%"><div class="label">P3: </div> <div class="field">${pots[2]}</div></td>
              <td style="width: 16%"><div class="label">P4: </div> <div class="field">${pots[3]}</div></td>
              <td style="width: 16%"><div class="label">P5: </div> <div class="field">${pots[4]}</div></td>
              <td style="width: 16%"><div class="label">P6: </div> <div class="field">${pots[5]}</div></td>
            </tr>
          </table>
      </div>
      % endif
      <%
          conf_obj = pool.get('res.config')
          sw_report_customer_signature = conf_obj.get(
                cursor, uid, 'sw_report_customer_signature', '1'
          )
      %>
      %if sw_report_customer_signature:
          <div style="float:left; width: 25%;">
              &nbsp;
          </div>
          <div style="float:left; width: 45%;">
              &nbsp;
          </div>
          <div style="float:right; width: 30%;">
              ${_(u"Signatura Client:")}
          </div>
      %endif
% endif
<!-- FI DADES TÈCNO-ECONÒMIQUES -->

%if cas != casos[-1]:
    <p style="page-break-after:always;"></p>
%endif
%endfor
</body>
</html>
