# -*- coding: utf-8 -*-

# Funcions compartides per diferents processos de switching

import pooler
import logging
from tools.translate import _
from osv import osv, fields, orm

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from gestionatr.defs import *
from gestionatr.utils import get_description
from workdays import workday
from ast import literal_eval as eval


logger = logging.getLogger('openerp.switching')


def get_facturacio_potencia(ocsum_tarifa, control_potencia):
    ''' Retorna el mètode de facturació de potència segons tarifa i pas'''
    if ocsum_tarifa in TARIFES_SEMPRE_MAX:
        return 'max'

    return control_potencia == '2' and 'max' or 'icp'


class GiscedataSwitchingException(Exception):
    def __init__(self, message, type_error):
        super(GiscedataSwitchingException, self).__init__(message)
        self.type_error = type_error

CONV_T111_T109 = dict(
        (v, str(eval(k))) for (k, v) in CONV_T109_T111.items()
    )


class GiscedataSwitchingHelpers(osv.osv):

    _name = 'giscedata.switching.helpers'
    _auto = False

    def get_tensio_normalitzada(self, cursor, uid, pm_ids, context=None):
        """Gets normalized tension from 'metering points'"""
        tensio_obj = self.pool.get('giscedata.tensions.tensio')
        conf_obj = self.pool.get('res.config')
        tensio_ids = []

        # when pm_ids is a list of ids
        if isinstance(pm_ids[0], int):
            pm_obj = self.pool.get('giscedata.switching.pm')
            pm_vals = pm_obj.read(
                cursor, uid, pm_ids, ['tensio_pm'], context=context
            )
            tensio_pm = max([p['tensio_pm'] for p in pm_vals])
        else:
            # when pm_ids is a list oh pm objects
            tensio_pm = max([p.tensio_pm for p in pm_ids])
        if tensio_pm:
            tensio_ids = tensio_obj.search(
                cursor, uid, [('tensio', '=', tensio_pm)], context=context
            )

        if not tensio_ids or (tensio_ids and len(tensio_ids) > 1):
            def_t = conf_obj.get(
                cursor, uid, 'sw_default_normalized_tension_name', '230'
            )
            tensio_ids = tensio_obj.search(
                cursor, uid, [('name', '=', def_t)], context=context
            )

        return tensio_ids[0]

    def get_tensio_normalitzada_from_code(self, cursor, uid, tensio_code, context=None):
        tensio_obj = self.pool.get('giscedata.tensions.tensio')

        tensio_ids = tensio_obj.search(
            cursor, uid, [('cnmc_code', '=', tensio_code)], limit=1, context=context
        )

        return len(tensio_ids) and tensio_ids[0] or False

    def processa_punt_de_mesura(self, cursor, uid, pm_ids, context=None):
        # Carreguem els Punts de mesura des del pas Cn-06
        # Els punts de mesura inclouen el comptador i la lectura d'activació
        pool = self.pool
        pm_obj = pool.get('giscedata.switching.pm')

        info = ''
        punts_mesura = pm_obj.activa_pm_a_polissa(cursor, uid,
                                                  [pm.id for pm in pm_ids])
        pm_afegits = []
        for v in punts_mesura.values():
            if v.get('comptadors'):
                pm_afegits += v['comptadors']

        pm_baixa = []
        for v in punts_mesura.values():
            if v.get('baixes'):
                pm_baixa += v['baixes']

        lectures_afegides = sum([v.get('lectures', 0) for v in punts_mesura.values()])

        errors_pm = []
        for v in punts_mesura.values():
            if v.get('errors'):
                errors_pm += v['errors']

        if pm_afegits:
            pm_txt = ','.join(pm_afegits)
            info_tmpl = _(u" INFO: S'han creat comptadors: %s.")
            info += info_tmpl % pm_txt
        if lectures_afegides:
            info += _(u"S'han creat %s lectures.") % lectures_afegides
        if errors_pm:
            info_tmpl = _(u'Hi ha hagut errors processant punts de mesura: %s')
            info += info_tmpl % ','.join(errors_pm)
        if pm_baixa:
            baixa_txt = ','.join(pm_baixa)
            info_tmpl = _(u"S'han donat de baixa comptadors: %s.")
            info += info_tmpl % baixa_txt
        return info

    def get_lot_facturacio(self, cursor, uid, data_lot, data_activacio):

        lot_obj = self.pool.get('giscedata.facturacio.lot')
        conf_obj = self.pool.get('res.config')
        conf_var = 'sw_activacio_en_lot_actual'
        act_en_lot_actual= bool(int(conf_obj.get(cursor, uid, conf_var, '0')))

        if not act_en_lot_actual:
            search_param = [
                ('data_final', '>=', data_lot),
                ('state', '!=', 'tancat')
            ]
        else:
            search_param = [('data_final', '>=', data_activacio),
                            ('state', '!=', 'tancat')]

        lot = lot_obj.search(cursor, uid, search_param, order='data_inici')
        return lot

    # Funció per activar una pólissa a partir d'un cas de switching
    # A3-05, C1-05, C2-05 i C2-07
    def activa_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        """Activa les modificacions d'un cas Cn (05/07)"""
        try:
            pool = self.pool

            sw_obj = pool.get('giscedata.switching')
            polissa_obj = pool.get('giscedata.polissa')
            tarifa_obj = pool.get('giscedata.polissa.tarifa')
            tarVer_obj = pool.get('product.pricelist.version')
            mandate_obj = pool.get('payment.mandate')
            conf_obj = pool.get('res.config')
            info = ''

            sw = sw_obj.browse(cursor, uid, sw_id)

            #Validem que el pas és final
            if not sw.finalitzat:
                #ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) no està finalitzat '
                         u'(%s-%s)') % (sw.name, sw.proces_id.name,
                                        sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))
            #Validem que el pas no és un rebuig (02)
            # La funció de rebuig ho hauria de fer bé
            # (mirar get_reject_step)
            # De moment ho fem per codi de pas
            if sw.rebuig:
                #ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) està finalitzat però és un '
                         u'rebuig (%s-%s)') % (sw.name, sw.proces_id.name,
                                               sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            pas_obj = pool.get(sw.step_id.get_step_model())
            pas = pas_obj.browse(cursor, uid,
                                 pas_obj.search(cursor, uid,
                                                [('sw_id', '=',
                                                  sw.id)])[0])

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            if not polissa.state == 'esborrany':
                #ERROR, la Pólissa ha d'estar activa
                info = _(u"Aquesta pólissa (%s) del cas '%s' ha "
                         u"d'estar en esborrany") % (sw.cups_polissa_id.name,
                                                     sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            #data activació
            data_activacio = pas.data_activacio

            # Configuració per saber si la periodicitat de facturació
            # ha de ser la mateixa que la de distri
            conf_id = 'sw_activacio_facturacio_segons_pas'
            per_fact_de_pas = bool(int(conf_obj.get(cursor, uid, conf_id, '0')))

            # El lot de facturació dependrà de la data d'activació i
            # de la periodicitat de facturació
            # Agafarem el primer lot NO tancat a partir de la data
            # calculada
            per_fact = int(pas.periodicitat_facturacio) or 2
            # El lot de facturació el calculem segons el període de facturació
            # que té (o tindrà) la pólissa a comercialitzadora.
            # Si aquest període és el mateix que el del pas, agafem el del pas
            per_fact_lot = (per_fact_de_pas and per_fact
                            or polissa.facturacio
                            or per_fact)
            data_lot = (datetime.strptime(data_activacio, '%Y-%m-%d') +
                        relativedelta(months=+per_fact_lot)
                        ).strftime('%Y-%m-%d')

            lot = self.get_lot_facturacio(cursor, uid, data_lot, data_activacio)

            if not lot:
                #ERROR, No hem trobat el lot amb la data corresponent
                info = _(u"No s'ha trobat cap LOT de facturació obert "
                         u"per la pólissa (%s) del cas '%s' ha a partir "
                         u"de la data '%s'") % (sw.cups_polissa_id.name,
                                                sw.name, data_lot)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            lot_id = lot[0]

            vals = {}
            canvi_tarifa = canvi_potencia = False
            # actualitzem tarifa (només si canvia)
            # check if tarif is LB
            tarif_lb = False
            if pas.marca_medida_bt == 'S':
                tarif_lb = True
            tarifa_pas_id = tarifa_obj.get_tarifa_from_ocsum(cursor, uid,
                                                             pas.tarifaATR,
                                                             tarifa_lb=tarif_lb)

            if tarifa_pas_id != polissa.tarifa.id:
                tarifa_pas_nom = tarifa_obj.read(cursor, uid, tarifa_pas_id,
                                                 ['name'])['name']
                canvi_tarifa_txt = "(%s -> %s)" % (polissa.tarifa.name,
                                                   tarifa_pas_nom)
                vals.update({'tarifa': tarifa_pas_id})
                canvi_tarifa = True

            pot_max = (max([p.potencia for p in pas.pot_ids]) / 1000.0)
            if pot_max != polissa.potencia:
                canvi_pot_txt = "(%s -> %s)" % (polissa.potencia, pot_max)
                vals.update({'potencia': pot_max})
                canvi_potencia = True

            if canvi_potencia or canvi_tarifa:
                polissa.write(vals)
                polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
                polissa.generar_periodes_potencia(
                    context={'force_genpot': True})

            # Calculem els períodes
            if len(polissa.potencies_periode) > 1:
                # omplim amb les potències del pas [kW]
                for periode in polissa.potencies_periode:
                    potencia = (max([p.potencia for p in pas.pot_ids
                                    if p.name == periode.periode_id.name])
                                / 1000.0)
                    periode.write({'potencia': potencia})

            tarifa_id = tarifa_pas_id

            # Si la pólissa no té tarifa, l'escollim
            # Calculem la tarifa. S'ha de fer particular de cada empresa
            # De moment, com si només n'hi hagués una per tarifa ATR
            if polissa.llista_preu and not canvi_tarifa:
                tarifa_comer = polissa.llista_preu
            else:
                tarifa = tarifa_obj.browse(cursor, uid, tarifa_id)
                llistes_preus = [l for l in tarifa.llistes_preus_comptatibles
                                 if l.type == 'sale']
                if not llistes_preus:
                    #ERROR, No hem trobat llista de preus
                    info = _(u"No hem trobat llista de preus per la tarifa ATR"
                             u" '%s' del cas '%s'") % (tarifa.name, sw.name)
                    raise GiscedataSwitchingException(info, _(u'ERROR'))
                elif polissa.llista_preu in llistes_preus:
                    # Si la tarifa_comer que tenim al contracte es compatible,
                    # la utilitzem
                    llista_preus = polissa.llista_preu
                else:
                    # funció per escollir la tarifa única (si es pot)
                    llista_preus = polissa.escull_llista_preus(llistes_preus)

                if not llista_preus:
                    #ERROR, No hem trobat una tarifa única
                    tarifes_comer = [t.name for t in llistes_preus]
                    txt = _(u"No hem pogut escollir una tarifa automàticament"
                            u" per la tarifa ATR '%s' del cas '%s' perquè hem"
                            u" trobat %d tarifes: %s")
                    info = txt % (tarifa.name, sw.name, len(llistes_preus),
                                  tarifes_comer)
                    raise GiscedataSwitchingException(info, _(u'ERROR'))

                tarifa_comer = llista_preus

            # versió primera facturació segons data activació
            search_vals = [
                ('pricelist_id', '=', tarifa_comer.id),
                ('date_start', '<=', pas.data_activacio),
                '|',
                ('date_end', '>=', pas.data_activacio),
                ('date_end', '=', False)
            ]

            ver_prim_fact = tarVer_obj.search(cursor, uid,
                                              search_vals)
            if not ver_prim_fact:
                info = _(u"per la pólissa '%s' no hem"
                         u"trobat cap versió de preus per la tarifa "
                         u"'%s' per la data '%s' "
                         u"del cas '%s'") % (polissa.name,
                                             tarifa_comer.name,
                                             data_activacio,
                                             sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            if polissa.bank and polissa.bank.partner_id.id != polissa.pagador.id:
                info = _(u"La pólissa '{0}' te un compte bancari asignat({1}, {2} amb ID {3}) que no pertany al "
                         u"pagador ({4} amb ID {5})").format(
                    polissa.name,
                    polissa.bank.iban,
                    polissa.bank.partner_id.name,
                    polissa.bank.partner_id.id,
                    polissa.pagador.name,
                    polissa.pagador.id,
                )
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            facturacio_potencia = get_facturacio_potencia(pas.tarifaATR,
                                                          pas.control_potencia)

            vals = {
                'data_alta': data_activacio,
                'lot_facturacio': lot_id,
                'llista_preu': tarifa_comer.id,
                'versio_primera_factura': ver_prim_fact[0],
                'ref_dist': pas.contracte_atr or '',
                'facturacio_potencia': facturacio_potencia,
                'facturacio_distri': per_fact,
                'autoconsumo': pas.tipus_autoconsum
            }

            #if contract type is informed we update it
            if pas.tipus_contracte:
                vals.update({'contract_type': pas.tipus_contracte})

            # if tarif is LB set trafo
            if pas.marca_medida_bt == 'S':
                vals.update({'trafo': pas.kvas_trafo})

            if per_fact_de_pas:
                vals.update({'facturacio': per_fact})

            tensio_norm = self.get_tensio_normalitzada_from_code(
                    cursor, uid, pas.tensio_suministre, context=context
            )
            if not tensio_norm:
                # Una tensió per defecte
                tensio_norm = self.get_tensio_normalitzada(
                    cursor, uid, pas.pm_ids, context=context
                )
            vals.update({'tensio_normalitzada': tensio_norm})

            data_firma = polissa.data_firma_contracte
            # data firma la d'activació si no n'hi ha
            if not data_firma:
                data_firma = data_activacio
                vals.update({'data_firma_contracte': data_firma})

            if sw.step_id.name == '05':
                tg_type = CONV_T111_T109.get(pas.tipus_telegestio, False)

                if tg_type:
                    vals.update({'tg': tg_type})
            if sw.actualitzar_mode_facturacio and sw.mode_facturacio:
                vals.update({
                    'mode_facturacio': sw.mode_facturacio
                })

            try:
                polissa.write(vals)
                #tornem a llegir la pólissa
                polissa.send_signal(['validar', 'contracte'])
            except (osv.except_osv, orm.except_orm), e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"activar: %s'") % (polissa.name, sw.name, e.value)
                raise GiscedataSwitchingException(info, _(u'ERROR'))
            except Exception as e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"activar: %s'") % (polissa.name, sw.name, e.message)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            #Aprofitem per crear el "mandato"
            if polissa.payment_mode_id and polissa.payment_mode_id.require_bank_account:
                reference = 'giscedata.polissa,%s' % polissa.id
                search_mandates = [('reference', '=', reference), ('date_end', '=', False)]
                mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
                if not mandates_ids:
                    mandate_obj.create(cursor, uid, {
                        'reference': reference,
                        'date': polissa.data_firma_contracte or data_firma})

            # Carreguem els Punts de mesura des del pas Cn-05/07
            # Els punts de mesura inclouen el comptador i la lectura d'activació
            pm_txt = self.processa_punt_de_mesura(cursor, uid, pas.pm_ids,
                                                  context=None)

            # plantilla notificació a client
            # if sw.proces_id.name == 'A3':
            #     pwe_template = 'switching_notificacio_activacio_a3_email'
            # else:
            #     pwe_template = 'switching_notificacio_activacio_cn_email'
            # res = sw.notifica_a_client(context=context)
            #
            # if len(res) != 2 and res[0] != 'OK':
            #     return res

            #tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (polissa.name,
                                          sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            info = _(u"Les modificacions del cas  %s "
                     u"s'han realitzat correctament.") % (sw.name)
            if canvi_tarifa:
                info += _(u" INFO: La tarifa ha canviat %s.") % canvi_tarifa_txt

            if canvi_potencia:
                info += _(u" INFO: La potència ha canviat %s.") % canvi_pot_txt

            if pm_txt:
                info += pm_txt

            return ('OK', info)

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception as e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    # Funció per donar de baixa una pólissa a partir d'un cas de switching
    # C1-06, C2-06, B1-05
    def baixa_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        """Dóna de baixa una pólissa a partir d'un Cn-06"""
        try:
            pool = self.pool

            sw_obj = pool.get('giscedata.switching')
            polissa_obj = pool.get('giscedata.polissa')
            factura_obj = pool.get('giscedata.facturacio.factura')
            conf_obj = pool.get('res.config')

            sw = sw_obj.browse(cursor, uid, sw_id)
            pas = sw.get_pas()
            
            info = u''
            #Validem que el pas és final
            if not sw.finalitzat:
                #ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) no està finalitzat '
                         u'(%s-%s)') % (sw.name, sw.proces_id.name,
                                        sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            notificable_cases = conf_obj.get(
                cursor, uid,
                'sw_mail_user_notification_on_activation', False
            )
            if notificable_cases == 'all':
                notificacio = True
            elif notificable_cases:
                pas_name = '{}-{}'.format(
                    sw.proces_id.name, sw.step_id.name
                )
                notificable_cases = eval(notificable_cases)
                notificacio = True if pas_name in notificable_cases else False
            else:
                notificacio = False

            info = _(u'El cas %s:\n') % (sw.name)
            # Notificar el pas
            # if pas.notificacio_pendent and notificacio:
            #     res = sw.notifica_a_client(context=context)
            #     pas = pas.browse()[0]
            #     if pas.notificacio_pendent and res[0] == "OK":
            #         info = _(u"Hi ha hagut un problema notificant el cas "
            #                  u"%s: %s: %s però es marcara com a notificat.") % (sw.name, res[0], res[1])
            #     elif pas.notificacio_pendent and res[0] != "OK":
            #         info = _(u"Hi ha hagut un problema notificant el cas "
            #                  u"%s: %s: %s.") % (
            #                sw.name, res[0], res[1])
            #     else:
            #         info += _(u"  * S'ha notificat al client.")

            # Validem que el pas no és un rebuig (02)
            # La funció de rebuig ho hauria de fer bé
            # (mirar get_reject_step)
            # De moment ho fem per codi de pas
            if sw.rebuig:
                #ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) està finalitzat però és un '
                         u'rebuig (%s-%s)') % (sw.name, sw.proces_id.name,
                                               sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            pas_obj = pool.get(sw.step_id.get_step_model())
            pas = pas_obj.browse(cursor, uid,
                                 pas_obj.search(cursor, uid,
                                                [('sw_id', '=',
                                                  sw.id)])[0])

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            b1_impago = sw.proces_id.name == 'B1' and polissa.state == 'tall'
            if polissa.state == 'baixa':
                for comptador in polissa.comptadors:
                    if comptador.active:
                        vals = {'active': 0}
                        if not comptador.data_baixa:
                            vals['data_baixa'] = polissa.data_baixa
                        comptador.write(vals)
                #tanquem el cas
                if not sw.case_close():
                    info += _(u"Per la pólissa '%s' no hem pogut tancar "
                              u"el cas %s") % (polissa.name, sw.name)
                    return (_(u'ERROR'), info)
                info += _(u"La baixa de polissa del cas  %s "
                          u"s'ha realitzat correctament.") % (sw.name)

                sw_obj.historize_additional_info(cursor, uid, sw)
                return ('OK', info)
            elif not polissa.state == 'activa' and not b1_impago:
                #ERROR, la Pólissa ha d'estar activa
                info = _(u"Aquesta pólissa (%s) del cas '%s' ha "
                         u"d'estar activa") % (sw.cups_polissa_id.name,
                                               sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            #data baixa
            data_baixa = pas.data_activacio

            # Carreguem els Punts de mesura des del pas Cn-06
            # Els punts de mesura inclouen el comptador i la lectura d'activació
            info += self.processa_punt_de_mesura(cursor, uid, pas.pm_ids,
                                                 context=None)

            polissa.write({'renovacio_auto': False, 'data_baixa': data_baixa})
            for comptador in polissa.comptadors:
                if comptador.active:
                    vals = {'active': 0}
                    if not comptador.data_baixa:
                        vals['data_baixa'] = data_baixa
                    comptador.write(vals)

            conf_var = 'sw_allow_baixa_polissa_from_cn_without_invoice'
            allow_without_invoice = bool(int(conf_obj.get(cursor, uid, conf_var, '0')))

            if not allow_without_invoice:
                # Busquem si ha arribat la última factura/lectura i la hem enviat.
                # Si no ens haurem d'esperar
                #1) Totes les factures amb data final la data d'activació
                search_vals = [('polissa_id', '=', polissa.id),
                               ('invoice_id.journal_id.code', 'ilike', 'ENERGIA')]

                factura_ids = factura_obj.search(cursor, uid, search_vals,
                                                 context={'active_test': False})
                factura_vals = factura_obj.read(cursor, uid, factura_ids,
                                                ['data_final', 'number', 'type'])

                tenim_fact_proveidor = tenim_fact_client = False
                for factura in factura_vals:
                    if factura['data_final'] == data_baixa:
                        if factura['type'] == 'in_invoice':
                            tenim_fact_proveidor = True
                        else:
                            tenim_fact_client = True

                if not tenim_fact_client:
                    #ERROR, no estem en un pas final
                    txt_tenim = _(u"Proveïdor: %s. Client: %s") % (
                        tenim_fact_proveidor and 'Si' or 'No',
                        tenim_fact_client and 'Si' or 'No',
                    )
                    info += _(u'Aquesta polissa (%s) encara no té les factures '
                              u"finals amb data '%s' pel cas %s."
                              u"%s") % (polissa.name, data_baixa, sw.name,
                                        txt_tenim)
                    return ('INFO', info)

            # I donem de baixa la polissa
            vals = {'data_baixa': data_baixa, 'renovacio_auto': 0}

            if not allow_without_invoice:
                if polissa.lot_facturacio:
                    vals.update({'lot_facturacio': False})
            polissa.write(vals, context={'from_baixa': True})
            polissa.send_signal(['baixa'])

             #tanquem el cas
            if not sw.case_close():
                info += _(u"Per la pólissa '%s' no hem pogut tancar "
                          u"el cas %s") % (polissa.name,
                                           sw.name)
                return (_(u'ERROR'), info)

            info += _(u"La baixa de polissa del cas  %s "
                      u"s'ha realitzat correctament.") % (sw.name)

            sw_obj.historize_additional_info(cursor, uid, sw)
            return ('OK', info)

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception, e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    def get_ultima_lectura(self, cursor, uid, polissa_id):
        pol_obj = self.pool.get("giscedata.polissa")

        # Comprovar última lectura facturada
        polissa = pol_obj.browse(cursor, uid, polissa_id)
        if polissa.data_ultima_lectura:
            return polissa.data_ultima_lectura

        # Comprovar última lectura comptador
        if polissa.comptadors and polissa.comptadors[0].active:
            comptador = polissa.comptadors[0]
            tarifa_id = polissa.tarifa.id
            data = comptador.data_ultima_lectura(tarifa_id)
            if data:
                return data

        return None

    def tancar_reclamacio(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        pas_obj = self.pool.get('giscedata.switching.r1.05')
        pas_id = pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
        pas_info = pas_obj.browse(cursor, uid, pas_id)

        polisses_obj = self.pool.get('giscedata.polissa')
        observacions = '\n{}'.format(polissa.observacions) \
            if polissa.observacions else ''

        user_obj = self.pool.get('res.users')
        resultat = get_description(pas_info.resultat, 'TABLA_80')
        desc_tipus = get_description(pas_info.tipus, 'TABLA_81')
        desc_subtipus = pas_info.subtipus_id.desc
        observ_values = {
            'data': str(datetime.today()),
            'usuari': user_obj.read(cursor, uid, uid, ['name'])['name'],
            'tipus': pas_info.tipus,
            'subtipus': pas_info.subtipus_id.name,
            'desc_tipus': desc_tipus,
            'desc_subtipus': desc_subtipus,
            'resultat': resultat
        }
        is_closed = ''
        if pas_info.resultat == '01':
            is_closed = 'Cas Tancat'

        observacions = _(
            '{data} {usuari} Reclamació ({tipus}-{subtipus}) '
            '{desc_tipus}-{desc_subtipus}. '
            'Generat el pas R1-05. {resultat}. {closed_case}{obs}').format(
            closed_case=is_closed, obs=observacions, **observ_values)
        polisses_obj.write(
            cursor, uid, polissa.id, {'observacions': observacions})
        return ('OK', u"Les modificacions del cas  %s "
                      u"s'han realitzat correctament" % sw.name)

    def get_data_prevista_activacio(self, cursor, uid, sw, context=None):
        """
        Per processos A3, C2, C2, M1 i B1 retorna el tipus de activació i una
        estimació de la data_activacio segons la activacio_cicle
        ('activacio' en B1) del pas01
        """
        if sw.proces_id.name not in ['A3', 'C2', 'C1', 'M1', 'B1']:
            return False

        model = "giscedata.switching.{0}.01".format(sw.proces_id.name.lower())
        pas01_obj = self.pool.get(model)
        pas01id = pas01_obj.search(cursor, uid, [('header_id.sw_id', '=', sw.id)])

        today_plus_15 = datetime.today() + timedelta(days=15)
        today_plus_15 = today_plus_15.strftime("%Y-%m-%d")

        if not len(pas01id):  # it should be a step 01...
            # Default dummy values
            return 'L0', today_plus_15

        pas01_info = pas01_obj.read(
            cursor, uid, pas01id,
            ['activacio_cicle', 'data_accio', 'activacio', 'header_id',
             'motiu', 'sollicitudadm', 'canvi_titular']
        )[0]

        if pas01_info.get('sollicitudadm', False) == "S" and pas01_info.get('canvi_titular', False) == "S" and sw.cups_polissa_id and sw.proces_id.name == 'M1':
            # For M1 type 'S' we inform the last reading
            polissa = sw.cups_polissa_id
            readings = [
                meter.data_ultima_lectura(polissa.tarifa.id)
                for meter in polissa.comptadors
            ]
            if len(readings):
                return 'L0', max(readings)

        activacio = (pas01_info.get('activacio_cicle', False) or
                     pas01_info.get('activacio', False))
        if activacio == 'F' and pas01_info['data_accio']:  # FechaFija
            # Minim 5 dies hàbils des de la sol.licitud
            mdays = 5
            # Si es B1 motiu 02 son minim 15 dies
            if pas01_info.get('motiu', False) == '02':
                mdays = 15
            header_obj = self.pool.get("giscedata.switching.step.header")
            data_sol = header_obj.read(cursor, uid, pas01_info['header_id'][0],
                                       ['date_created'])['date_created']
            data_sol = datetime.strptime(data_sol, '%Y-%m-%d %H:%M:%S')
            data_fixa = datetime.strptime(pas01_info['data_accio'], '%Y-%m-%d')
            if workday(data_sol, mdays) > data_fixa:
                data_fixa = workday(data_sol, mdays)
            elif data_fixa < datetime.today():
                data_fixa = datetime.today()
            return 'F0', data_fixa.strftime("%Y-%m-%d")

        data_activacio = today_plus_15
        data_activacio2 = data_activacio

        polissa = sw.cups_polissa_id
        fi_lot = (polissa.lot_facturacio and
                  polissa.lot_facturacio.data_final or
                  False)

        today = datetime.today().strftime("%Y-%m-%d")
        if fi_lot and fi_lot >= today:
            data_activacio2 = datetime.strptime(fi_lot, '%Y-%m-%d') + \
                              timedelta(days=1)
            data_activacio2 = data_activacio2.strftime("%Y-%m-%d")

        return 'L0', min([data_activacio, data_activacio2])

    def get_init_deadline_cn_m1(self, cursor, uid, sw_id, proces, context=None):
        model01 = "giscedata.switching.{0}.01".format(proces.lower())
        pas01_obj = self.pool.get(model01)
        model02 = "giscedata.switching.{0}.02".format(proces.lower())
        pas02_obj = self.pool.get(model02)

        pas_01_id = pas01_obj.search(cursor, uid,
                                     [("header_id.sw_id", "=", sw_id)])
        pas_02_id = pas02_obj.search(cursor, uid,
                                     [("header_id.sw_id", "=", sw_id)])

        if not len(pas_02_id) or not len(pas_01_id):
            return datetime.today()

        info_accep = pas02_obj.read(
            cursor, uid, pas_02_id[0],
            ['tipus_activacio', 'data_activacio']
        )
        # En data fixa el limit es la data que es va marcar
        if info_accep.get('tipus_activacio') == 'F0':
            return datetime.strptime(info_accep['data_activacio'], '%Y-%m-%d')

        sw_obj = self.pool.get("giscedata.switching")
        pol_obj = self.pool.get("giscedata.polissa")

        # Altrament depén del punt de mesura i la data sollicitud
        info_sol = pas01_obj.read(cursor, uid, pas_01_id[0], ['date_created'])
        data_sol = datetime.strptime(info_sol['date_created'],
                                     '%Y-%m-%d %H:%M:%S')

        sw_info = sw_obj.read(cursor, uid, sw_id, ['cups_polissa_id'])
        if not sw_info['cups_polissa_id']:
            return False

        pol_info = pol_obj.read(
            cursor, uid, sw_info['cups_polissa_id'][0], ['agree_tipus', 'tg'])

        # L0: Activacio cicle lectura
        if info_accep.get('tipus_activacio') == 'L0':

            # Punts Mesura tipus 5
            if pol_info['agree_tipus'] == '05':
                if pol_info['tg'] in ['1', '3']:  # Telegestionats
                    return data_sol + timedelta(days=36)
                else:  # No Telegestionats
                    return data_sol + timedelta(days=66)

            # Altres Punts de Mesura
            else:
                return data_sol + timedelta(days=36)

        # Cuanto antes
        else:
            # Punts Mesura tipus 5
            if pol_info['agree_tipus'] == '05':
                return data_sol + timedelta(days=15)
            # Altres Punts de Mesura
            else:
                return data_sol + timedelta(days=21)

    def desfer_baixa_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        try:
            pool = self.pool

            sw_obj = pool.get('giscedata.switching')
            polissa_obj = pool.get('giscedata.polissa')

            sw = sw_obj.browse(cursor, uid, sw_id)
            if sw.step_id.name != '10' or sw.proces_id.name not in ['C1', 'C2']:
                info = _(u"En el cas '%s' no estem en un pas d'anul.lació "
                         u"(Cn-10), no es pot desfer la baixa.") % (sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            info = ''
            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            if polissa.state != 'baixa':
                info = _(u"La pólissa (%s) del cas '%s' no està de baixa, no "
                         u"és necessari anul.lar la baixa.") % (
                    sw.cups_polissa_id.name, sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            for comptador in polissa.comptadors:
                if not comptador.active and comptador.data_baixa == polissa.data_baixa:
                    vals = {'active': True, 'data_baixa': False}
                    comptador.write(vals)

            polissa.write({'renovacio_auto': True, 'data_baixa': False})
            polissa.send_signal(['reactivar'])

            # tanquem el cas
            if not sw.case_close():
                info += _(u"Per la pólissa '%s' no hem pogut tancar "
                          u"el cas %s") % (polissa.name, sw.name)
                return _(u'ERROR'), info

            info += _(u"S'ha desfet la baixa de la pólissa del cas '%s'") % sw.name
            return 'OK', info

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception, e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    def is_cancelable_from_cn_a3(self, cursor, uid, polissa, context=None):
        '''This functions check if a contract is cancelable for se
         Cn-09 and A3-07'''
        if context is None:
            context = {}

        return polissa.state == 'esborrany' or \
               (polissa.state == 'activa' and not polissa.data_ultima_lectura)

    def cancelar_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        try:
            pool = self.pool

            sw_obj = pool.get('giscedata.switching')
            polissa_obj = pool.get('giscedata.polissa')
            info = ''

            sw = sw_obj.browse(cursor, uid, sw_id)
            if not (sw.step_id.name == '07' and sw.proces_id.name == 'A3'
                    or sw.step_id.name == '09' and sw.proces_id.name in ['C1', 'C2']):
                info = _(u"Aquest cas (%s) no es troba en el pas final "
                         u"d'anul.lació (A3-07 o Cn-09") % (sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            pas = sw.get_pas()
            # Validem que l'anul.lació s'ha acceptat
            if pas.rebuig:
                info = _(u"Aquest cas (%s) és una sol.licitud d'anulació però "
                         u"està rebutjada") % sw.name
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

            if not self.is_cancelable_from_cn_a3(cursor, uid, polissa, context=context):
                motiu_error = u'la pólissa no està en esborrany, hauria ' \
                              u'd\'estar en esborrany o activa ' \
                              u'sense data d\'última lectura'
                if polissa.state == 'activa':
                    motiu_error = u'està activa i amb data d\'última lectura'

                info = _(u"No es pot cancel.lar la pólissa (%s) del cas '%s', "
                         u"per que %s.") % (
                    sw.cups_polissa_id.name, sw.name, motiu_error
                )
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            polissa_obj.pre_canceling_behaviour(cursor, uid, polissa.id)

            try:
                polissa.send_signal(['cancelar'])
            except (osv.except_osv, orm.except_orm), e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"cancel.lar: %s'") % (polissa.name, sw.name, e.value)
                raise GiscedataSwitchingException(info, _(u'ERROR'))
            except Exception as e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"cancel.lar: %s'") % (polissa.name, sw.name, e.message)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            # tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (polissa.name,
                                          sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            info = _(u"La pólissa de cas  %s "
                     u"s'ha cancel.lat correctament.") % (sw.name)
            return ('OK', info)

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception as e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    def activar_polissa_from_m1(self, cursor, uid, sw_id, context=None):
        pool = self.pool
        sw_obj = pool.get('giscedata.switching')
        polissa_obj = pool.get('giscedata.polissa')
        swhelp_obj = pool.get('giscedata.switching.helpers')
        m101_obj = pool.get('giscedata.switching.m1.01')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)

        try:
            # Validem que el pas és final
            if not sw.finalitzat:
                # ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) no està finalitzat '
                         u'(M1-%s)') % (sw.name, sw.step_id.name)
                return (_(u'ERROR'),
                        info)
            # Validem que el pas no és un rebuig (04 o 02)
            # La funció de rebuig ho hauria de fer bé
            # (mirar get_reject_step)
            # De moment ho fem per codi de pas
            if sw.rebuig or sw.step_id.name == '04':
                # ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) està finalitzat però és un '
                         u'rebuig (M1-%s)') % (sw.name, sw.step_id.name)
                return (_(u'ERROR'),
                        info)
            pas_obj = pool.get(sw.step_id.get_step_model())
            pas = pas_obj.browse(
                cursor, uid,
                pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )

            # validem que no és un canvi de titular a partir del m101
            pas01 = m101_obj.browse(
                cursor, uid, m101_obj.search(
                    cursor, uid, [('sw_id', '=', sw.id)])[0]
            )
            if pas01.sollicitudadm in ('S', 'A'):
                polissa = None
                return self.activar_polissa_from_m1_canvi_titular(
                    cursor, uid, sw_id, context=context
                )
            if not polissa.state == 'activa':
                # ERROR, la Pólissa ha d'estar activa
                info = _(u"Aquesta pólissa (%s) del cas '%s' ha "
                         u"d'estar activa") % (sw.cups_polissa_id.name,
                                               sw.name)
                return (_(u'ERROR'),
                        info)

            vals = self.get_vals_for_m1_tarpot(cursor, uid, pas, polissa, context=context)

            data_activacio = pas.data_activacio

            polissa.send_signal('modcontractual')
            polissa.write(vals)
            polissa = self.actualitzar_periodes_from_m1(cursor, uid, pas, polissa, context=context)

            # activem la modificacio contractual
            wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa.id}
            ctx.update(
                {'activacio_from_atr': context.get('activacio_from_atr', False)}
            )
            params = {'duracio': context.get('periode_modcon', 'actual'), 'accio': 'nou'}

            wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id], data_activacio, wiz.duracio, ctx
            )
            if res.get('warning', False):
                info = _(
                    u"Hem trobat un error amb les dates al crear la "
                    u"modificació contractual de la pòlissa '{polissa_name}' "
                    u"pel cas '{cas_sw}'. Data activació '{data_activacio}'. "
                    u"{warning_title}: {warning_message}. "
                    u"AQUEST CANVI JA HA ESTAT ACTIVAT?"
                ).format(**{
                    'polissa_name': polissa.name,
                    'cas_sw': sw.name,
                    'data_activacio': pas.data_activacio,
                    'warning_title': res['warning']['title'],
                    'warning_message': res['warning']['message']
                })
                polissa.send_signal('undo_modcontractual')

                return (_(u'ERROR'), info)

            wiz.write({'data_final': res['value']['data_final'] or '',
                       'data_inici': data_activacio})
            wiz.action_crear_contracte(ctx)

            if sw.step_id.name == '05' and polissa.autoconsumo != '00' and pas.tipus_autoconsum != '00':
                autoconsum_obj = self.pool.get('giscedata.autoconsum')
                act_mod_res = autoconsum_obj.activar_modificar_autoconsum(cursor, uid, polissa.id, polissa.autoconsumo)

            # Carreguem els Punts de mesura des del pas M1-05
            # Els punts de mesura inclouen el comptador i la lectura
            # d'activació
            pm_txt = swhelp_obj.processa_punt_de_mesura(cursor, uid,
                                                        pas.pm_ids,
                                                        context=None)

            # pwe_template = 'switching_notificacio_activacio_m1_email'
            # res = sw.notifica_a_client(context=context)
            #
            # if len(res) != 2 and res[0] != 'OK':
            #     return res

            # tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (polissa.name, sw.name)
                return (_(u'ERROR'), info)

            info = _(u"Les modificacions del cas  %s "
                     u"s'han realitzat correctament.") % (sw.name)

            if act_mod_res.get('status', False) == 'WARNING':
                info += '\n{}'.format(act_mod_res.get('message', ''))

            if pm_txt:
                info += pm_txt

            return 'OK', info

        except osv.except_osv, e:
            info = u'%s (M1-%s) %s' % (sw.name, sw.step_id.name,
                                       unicode(e.value))
            if polissa:
                polissa.send_signal('undo_modcontractual')
            return (_(u'ERROR'), info)
        except orm.except_orm, e:
            info = u'%s (M1-%s) %s' % (sw.name, sw.step_id.name,
                                       unicode(e.value))
            if polissa:
                polissa.send_signal('undo_modcontractual')
            return (_(u'ERROR'), info)
        except Exception, e:
            if polissa:
                polissa.send_signal('undo_modcontractual')
            raise Exception(e)

    def get_vals_for_m1_tarpot(self, cursor, uid, pas, polissa, context=None):
        """
        :param cursor:
        :param uid:
        :param pas: browse del pas 05 del m1
        :param context:
        :return: retorna un diccionari  amb valors per fer el write al contracte
        """
        pool = self.pool
        tarifa_obj = pool.get('giscedata.polissa.tarifa')
        fact_obj = pool.get('giscedata.facturacio.factura')
        conf_obj = pool.get('res.config')
        sw = pas.sw_id
        # Calculem la tarifa. S'ha de fer particular de cada empresa
        # De moment, com si només n'hi hagués una per tarfa ATR
        tarif_lb = pas.marca_medida_bt == 'S'
        tarifa_id = tarifa_obj.get_tarifa_from_ocsum(
            cursor, uid, pas.tarifaATR, tarifa_lb=tarif_lb
        )
        tarifa_comer_id = sw.escull_tarifa_comer(
            tarifa_id, context=context
        )

        pot_max = (max([p.potencia for p in pas.pot_ids]) / 1000.0)

        # Configuració per saber si la periodicitat de facturació
        # ha de ser la mateixa que la de distri
        conf_id = 'sw_activacio_facturacio_segons_pas'
        per_fact_de_pas = bool(int(conf_obj.get(cursor, uid, conf_id, '0')))

        per_fact = int(pas.periodicitat_facturacio)
        fact_pot = get_facturacio_potencia(pas.tarifaATR, pas.control_potencia)
        vals = {
            'tarifa': tarifa_id,
            'facturacio_distri': per_fact,
            'potencia': pot_max,
            'llista_preu': tarifa_comer_id,
            'facturacio_potencia': fact_pot,
            'autoconsumo': pas.tipus_autoconsum,
        }

        if per_fact_de_pas:
            vals.update({'facturacio': per_fact})

        # Mirem si hi ha factures per saber si hem de posar data
        # primera factura
        n_fact = fact_obj.search_count(
            cursor, uid, [('polissa_id.id', '=', polissa.id)],
            context={'active_test': False}
        )
        if not n_fact:
            # Si no hi ha cap factura emesa (n_fact = 0) busquem
            # la versio de primera factura
            tarVer_obj = pool.get('product.pricelist.version')
            search_vals = [
                ('pricelist_id', '=', tarifa_comer_id),
                ('date_start', '<=', pas.data_activacio),
                '|',
                ('date_end', '>=', pas.data_activacio),
                ('date_end', '=', False)
            ]
            ver_prim_fact = tarVer_obj.search(cursor, uid, search_vals)
            if not ver_prim_fact:
                tarcomer_obj = self.pool.get('product.pricelist')
                tarifa_comer_name = tarcomer_obj.read(
                    cursor, uid, tarifa_comer_id, ['name']
                )['name']
                info = _(u"La pólissa '%s' no té cap factura i no hem "
                         u"trobat cap versió de preus per la tarifa "
                         u"'%s' per la data '%s' "
                         u"del cas '%s'") % (polissa.name,
                                             tarifa_comer_name,
                                             pas.data_activacio,
                                             sw.name)
                return (_(u'ERROR'), info)

            vals.update({'versio_primera_factura': ver_prim_fact[0]})

        tensio_norm = self.get_tensio_normalitzada_from_code(
            cursor, uid, pas.tensio_suministre, context=context
        )
        if not tensio_norm:
            # Una tensió per defecte
            tensio_norm = self.get_tensio_normalitzada(
                cursor, uid, pas.pm_ids, context=context
            )
        vals.update({'tensio_normalitzada': tensio_norm})

        if sw.step_id.name == '05':
            tg_type = CONV_T111_T109.get(pas.tipus_telegestio, False)
            if tg_type:
                vals.update({'tg': tg_type})

        if sw.actualitzar_mode_facturacio and sw.mode_facturacio:
            vals.update({
                'mode_facturacio': sw.mode_facturacio
            })
        return vals

    def actualitzar_periodes_from_m1(self, cursor, uid, pas, polissa, context=None):
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa.generar_periodes_potencia(context={'force_genpot': True})
        # tornem a llegir la pólissa
        polissa = polissa_obj.browse(cursor, uid, polissa.id)
        # omplim amb les potències del pas [kW]
        for periode in polissa.potencies_periode:
            potencia = max([p.potencia for p in pas.pot_ids if
                            p.name == periode.periode_id.name]) / 1000.0
            periode.write({'potencia': potencia})
        polissa = polissa_obj.browse(cursor, uid, polissa.id)
        return polissa

    def activar_polissa_from_m1_canvi_titular_subrogacio(self, cursor, uid, sw_id, context=None):
        """
         Activa un M1 de canvi de titular fent una modificació contractual al
         contracte actual.
         En aquest punt ja s'ha comprovat que el M1 es un canvi de titular
         amb subrogacio en pas 02 (o 05 si hi havia treballs en camp).
        """
        if context is None:
            context = {}
        pool = self.pool
        sw_obj = pool.get('giscedata.switching')
        partner_obj = self.pool.get('res.partner')
        m101_obj = pool.get('giscedata.switching.m1.01')
        polissa_obj = self.pool.get("giscedata.polissa")
        conf_obj = self.pool.get('res.config')
        try:
            sw = sw_obj.browse(cursor, uid, sw_id, context=context)
            polissa = sw.cups_polissa_id
            pas_activacio = sw.get_pas()
            pas01 = m101_obj.browse(
                cursor, uid,
                m101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )

            # Obtenim i escrivim els nous valors
            # Titular
            titular_id = partner_obj.search(cursor, uid, [("vat", "=", "ES"+pas01.codi_document)])
            if len(titular_id) > 1:
                info = _(u"S'ha trobat més de un client amb el NIF {0}: "
                         u"{1}.").format("ES"+pas01.codi_document, titular_id)
                return _(u'ERROR'), info
            elif not len(titular_id):
                info = _(u"No s'ha trobat cap client amb el NIF {0}.").format("ES"+pas01.codi_document)
                return _(u'ERROR'), info
            titular_id = titular_id[0]
            # Pagador
            pagador_id = pas01.direccio_pagament.partner_id.id
            dir_pagament = pas01.direccio_pagament.id
            pagador_sel = "titular" if titular_id == pagador_id else "altre_p"
            # Notificacio
            not_partner_id = pas01.direccio_notificacio.partner_id.id
            direccio_notificacio = pas01.direccio_notificacio.id
            if not_partner_id == titular_id:
                notificacio = "titular"
                altre_p = False
            elif not_partner_id == pagador_id:
                notificacio = "pagador"
                altre_p = False
            else:
                notificacio = "altre_p"
                altre_p = not_partner_id
            vals = {
                'titular': titular_id,
                'pagador': pagador_id,
                'direccio_pagament': dir_pagament,
                'pagador_sel': pagador_sel,
                'altre_p': altre_p,
                'notificacio': notificacio,
                'direccio_notificacio': direccio_notificacio,
                'bank': pas01.bank.id,
                'tipo_pago': pas01.tipo_pago.id,
                'payment_mode_id': pas01.payment_mode_id.id,
            }
            data_activacio = pas_activacio.data_activacio
            if pas_activacio._nom_pas == "02":
                # En els 02, la data que donen es la última lectura firme,
                # per tant la modcon ha de començar el dia després
                data_activacio = datetime.strptime(data_activacio, "%Y-%m-%d")
                data_activacio = data_activacio + timedelta(days=1)
                data_activacio = data_activacio.strftime("%Y-%m-%d")
            elif pas_activacio._nom_pas == "05" and pas01.sollicitudadm == "A":
                add_vals = self.get_vals_for_m1_tarpot(cursor, uid, pas_activacio, polissa, context=context)
                vals.update(add_vals)

            polissa.send_signal('modcontractual')
            polissa.write(vals)
            if pas01.sollicitudadm == 'A':
                polissa = self.actualitzar_periodes_from_m1(
                    cursor, uid, pas_activacio, polissa, context=context
                )

            # Activem la modificacio contractual
            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa.id}
            ctx.update(
                {'activacio_from_atr': context.get('activacio_from_atr', False)}
            )
            params = {'duracio': context.get('periode_modcon', 'actual'), 'accio': 'nou'}
            wz_id = wz_crear_mc_obj.create(cursor, uid, params, ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, ctx)
            res = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id], data_activacio, wiz.duracio, ctx
            )
            if res.get('warning', False):
                info = _(
                    u"Hem trobat un error amb les dates al crear la "
                    u"modificació contractual de la pòlissa '{polissa_name}' "
                    u"pel cas '{cas_sw}'. Data activació '{data_activacio}'. "
                    u"{warning_title}: {warning_message}. "
                    u"AQUEST CANVI JA HA ESTAT ACTIVAT?"
                ).format(**{
                    'polissa_name': polissa.name,
                    'cas_sw': sw.name,
                    'data_activacio': pas_activacio.data_activacio,
                    'warning_title': res['warning']['title'],
                    'warning_message': res['warning']['message']
                })
                polissa.send_signal('undo_modcontractual')
                return _(u'ERROR'), info

            wiz.write({'data_final': res['value']['data_final'] or '', 'data_inici': data_activacio})
            wiz.action_crear_contracte(ctx)

            # Aprofitem per crear el "mandato"
            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            if polissa.payment_mode_id and polissa.payment_mode_id.require_bank_account:
                reference = 'giscedata.polissa,%s' % polissa.id
                search_mandates = [('reference', '=', reference), ('date_end', '=', False)]
                mandate_obj = pool.get('payment.mandate')
                mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
                if not mandates_ids:
                    mandate_obj.create(cursor, uid, {
                        'reference': reference,
                        'date': data_activacio})

            # Carreguem els Punts de mesura des del pas M1-05
            # Els punts de mesura inclouen el comptador i la lectura
            # d'activació
            pm_txt = False
            if pas_activacio._nom_pas == "05":
                pm_txt = self.processa_punt_de_mesura(
                    cursor, uid, pas_activacio.pm_ids, context=None
                )

            # Notifiquem a client si fa falta
            # res = sw.notifica_a_client(context=context)
            #
            # Comprovem si tot ha anat be
            # if len(res) != 2 and res[0] != 'OK':
            #     return res

            # Tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (polissa.name, sw.name)
                return _(u'ERROR'), info

            info = _(u"Les modificacions del cas  %s "
                     u"s'han realitzat correctament.") % (sw.name)

            if pm_txt:
                info += pm_txt

            return 'OK', info
        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception as e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    def activar_polissa_from_m1_canvi_titular(self, cursor, uid, sw_id, context=None):
        pool = self.pool
        sw_obj = pool.get('giscedata.switching')
        partner_obj = self.pool.get('res.partner')
        m101_obj = pool.get('giscedata.switching.m1.01')
        pol_obj = self.pool.get("giscedata.polissa")
        conf_obj = self.pool.get('res.config')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        pas = sw.get_pas()
        old_contract = sw.cups_polissa_id
        pas01 = m101_obj.browse(
            cursor, uid, m101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
        )

        # Les subrogacions es tracten de dos maneres diferents segons una
        # variable de configuracio:
        #     - Creant contracte nou (similar al traspas)
        #     - Fent modcon al contracte actual
        use_new_contract = bool(int(conf_obj.get(
            cursor, uid, "sw_m1_owner_change_subrogacio_new_contract", '1'
        )))
        if pas01.canvi_titular == 'S' and not use_new_contract:
            return self.activar_polissa_from_m1_canvi_titular_subrogacio(
                cursor, uid, sw_id, context=context
            )

        m1_owner_auto = bool(int(conf_obj.get(cursor, uid, "sw_m1_owner_change_auto", '0')))
        if not m1_owner_auto:
            info = _(
                u'La activació automàtica dels canvis de titular no está '
                u'activada, cas %s.'
            ) % sw.name
            return _(u'ERROR'), info

        try:
            if pas._nom_pas != '05' and pas01.sollicitudadm == 'A':
                info = _(
                    u"Els canvis de titular amb modificacions técniques s'activen al pas 05."
                    u"No estem al pas 05 per el cas %s.") % sw.name
                return _(u'ERROR'), info

            if pas._nom_pas != '05' and pas01.canvi_titular == 'T':
                info = _(
                    u"Els canvis de titular amb traspas (sense subrogació) s'activen al pas 05."
                    u"No estem al pas 05 per el cas %s.") % sw.name
                return _(u'ERROR'), info

            if pas01.sollicitudadm not in ('S', 'A') or pas01.canvi_titular not in ['T', 'S']:
                info = _(u'No es tracte de un canvi de titular (S / A) amb tipus de '
                         u'canvi "sin subrogación" (T) o "con subrogación" (S),'
                         u' cas %s.') % sw.name
                return _(u'ERROR'), info

            if pas._nom_pas not in ['02', '05'] or (pas._nom_pas == '02' and pas.actuacio_camp == 'S'):
                info = _(u"No estem en un pas d'activació per els canvis de titular"
                         u" 'A' (02 sense actuacions en camp o 05), cas %s.") % sw.name
                return _(u'ERROR'), info

            if not sw.ref:
                info = _(u"No hi ha cap pólissa lligada al cas ATR (camp 'ref') "
                         u"per poder activar, cas %s.") % sw.name
                return _(u'ERROR'), info

            m1_traspas_data_distri = bool(int(conf_obj.get(cursor, uid, "sw_m1_traspas_data_distri", '1')))
            use_data_distri = (m1_traspas_data_distri and pas01.canvi_titular == 'T') or pas01.sollicitudadm == 'A'
            if use_data_distri:
                data_baixa = datetime.strptime(pas.data_activacio, "%Y-%m-%d")
                data_baixa = data_baixa - timedelta(days=1)
                data_baixa = data_baixa.strftime("%Y-%m-%d")
            else:
                data_baixa = max(old_contract.data_ultima_lectura, pas01.data_accio)
            polissa = pol_obj.browse(cursor, uid, int(sw.ref.split(",")[1]))
            info = ""
            if pas01.activacio_cicle == 'L' and not use_data_distri:
                if (not old_contract.data_ultima_lectura or
                        pas01.data_accio > old_contract.data_ultima_lectura):
                    info = _(u"No s'ha realitzat l'activació: la pólissa %s "
                             u"encara no s'ha facturat (data acció pas 01 > data"
                             u" última lectura facturada), cas %s.") \
                           % (old_contract.name, sw.name)
                    return _(u'ERROR'), info
                # Ja no s'ha de facturar res mes, el treiem del lot
                old_contract.write({'lot_facturacio': None})
            else:
                lect_helper = self.pool.get("giscedata.lectures.switching.helper")
                if lect_helper:
                    aux = lect_helper.move_meters_of_contract(
                        cursor, uid, old_contract, polissa, data_baixa
                    )
                    info = _(u"S'han creat {0} comptadors (ids {1}) a la "
                             u"pólissa {2}").format(len(aux), aux, polissa.id)
                fact_helper = self.pool.get("giscedata.facturacio.switching.helper")
                if fact_helper:
                    aux = fact_helper.move_invoices_of_contract(
                        cursor, uid, old_contract, polissa, data_baixa
                    )
                    info = _(u"{0}. S'han mogut {1} factures(ids {2}) de la pólissa"
                             u" {3} a la {4}").format(
                        info, len(aux), aux, old_contract.id, polissa.id
                    )
            old_contract = old_contract.browse()[0]
            polissa = polissa.browse()[0]
            # Donar de baixa contadors i polissa antiga
            for comptador in old_contract.comptadors:
                if comptador.active:
                    vals = {'active': 0}
                    if not comptador.data_baixa:
                        vals['data_baixa'] = data_baixa
                    comptador.write(vals)

            vals = {
                'data_baixa': data_baixa,
                'renovacio_auto': 0,
                # El treiem de facturacio suspesa per si queda algun periode
                # sense facturar
                'facturacio_suspesa': False
            }
            old_contract.write(vals, context={'from_baixa': True})
            old_contract.send_signal(['baixa'])

            # Donar d'alta nova pólissa
            if not polissa.state == 'esborrany':
                # ERROR, la Pólissa ha d'estar activa
                info = _(u"Aquesta pólissa (%s) del cas '%s' ha "
                         u"d'estar en esborrany.") % (polissa.name, sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            if use_data_distri:
                data_activacio = pas.data_activacio
            else:
                data_activacio = datetime.strptime(data_baixa, "%Y-%m-%d")
                if old_contract.data_ultima_lectura:
                    data_activacio += timedelta(days=1)
                data_activacio = data_activacio.strftime("%Y-%m-%d")

            titular = polissa.titular.id
            titular_pas_ids = partner_obj.search(cursor, uid, [('vat', 'ilike', pas01.codi_document)])
            if len(titular_pas_ids):
                titular = titular_pas_ids[0]

            data_firma = polissa.data_firma_contracte
            # data firma la d'activació si no n'hi ha
            if not data_firma:
                data_firma = data_activacio

            # We want the current opened lot, so we pass data_activacio twice
            lot = self.get_lot_facturacio(cursor, uid, data_activacio, data_activacio)
            if len(lot):
                lot = lot[0]
            vals = {
                'data_alta': data_activacio,
                'titular': titular,
                'lot_facturacio': lot,
                'data_firma_contracte': data_firma
            }
            if pas01.sollicitudadm == 'A':
                add_vals = self.get_vals_for_m1_tarpot(cursor, uid, pas, polissa, context=context)
                vals.update(add_vals)
            try:
                polissa.write(vals)
                if pas01.sollicitudadm == 'A':
                    polissa = self.actualitzar_periodes_from_m1(
                        cursor, uid, pas, polissa, context=context
                    )
                polissa.send_signal(['validar', 'contracte'])
            except (osv.except_osv, orm.except_orm), e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"activar: %s'") % (polissa.name, sw.name, e.value)
                raise GiscedataSwitchingException(info, _(u'ERROR'))
            except Exception as e:
                info = _(u"La pólissa '%s' del cas '%s' no s'ha pogut "
                         u"activar: %s'") % (polissa.name, sw.name, e.message)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            #Aprofitem per crear el "mandato"
            if polissa.payment_mode_id and polissa.payment_mode_id.require_bank_account:
                reference = 'giscedata.polissa,%s' % polissa.id
                search_mandates = [('reference', '=', reference), ('date_end', '=', False)]
                mandate_obj = pool.get('payment.mandate')
                mandates_ids = mandate_obj.search(cursor, uid, search_mandates)
                if not mandates_ids:
                    mandate_obj.create(cursor, uid, {
                        'reference': reference,
                        'date': polissa.data_firma_contracte or polissa.data_alta})


            # Notifiquem a client si fa falta
            # res = sw.notifica_a_client(context=context)
            # Comprovem si tot ha anat be
            # if len(res) != 2 and res[0] != 'OK':
            #     return res

            # tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (old_contract.name, sw.name)
                return _(u'ERROR'), info

            info = _(u"Les modificacions del cas  %s "
                     u"s'han realitzat correctament. %s") % (sw.name, info)

            sw_obj.historize_additional_info(cursor, uid, sw)
            return 'OK', info

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception as e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    def activar_pas_acceptacio_rebuig(self, cursor, uid, sw_id, context=None):
        """
        Activacio pasos 02 i 04:
            * Pas 02 acceptació: Es notifica a client.
            * Pas 02/04/07 rebuig: Si está marcat per notificar, es notifica i tanca el cas.
                                   Si no es pot notificar (no estava marcat o hi ha errors) no es tanca.
        """
        pool = self.pool
        sw_obj = pool.get('giscedata.switching')
        polissa_obj = pool.get('giscedata.polissa')
        sw = sw_obj.browse(cursor, uid, sw_id)
        try:
            pas = sw.get_pas()
            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            if not (sw.step_id.name in ['02'] or pas.rebuig):
                info = _(u"Aquest cas (%s) no es troba en el pas 02 "
                         u"d'acceptació o en un pas de rebuig.") % (sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            info = _(u'El cas %s:\n') % (sw.name)
            # Notificar el pas
            if not pas.notificacio_pendent:
                info = _(u"El cas %s no s'ha notificat perqué no estava "
                         u"marcat com a pendent de notificar. ") % (sw.name)
                if sw.proces_id.name != '02':
                    info += _(u"No es tancará el cas.")
                return _(u'ERROR'), info
            else:
                res = sw.notifica_a_client(context=context)
                pas = pas.browse()[0]
                if pas.notificacio_pendent:
                    info = _(u"Hi ha hagut un problema notificant el cas "
                             u"%s: %s: %s.") % (sw.name, res[0], res[1])
                    if sw.step_id.name != '02' or pas.rebuig:
                        info += _(u"No es tancará el cas.")
                    return _(u'ERROR'), info
                else:
                    info += _(u"  * S'ha notificat al client.")

            if pas.rebuig:
                # tanquem el cas
                if not sw.case_close():
                    info = _(u"Per la pólissa '%s' no hem pogut tancar "
                             u"el cas %s") % (polissa.name, sw.name)
                    return _(u'ERROR'), info
                else:
                    info += _(u"  * S'ha tancat correctament.")

            return 'OK', info

        except osv.except_osv, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        except orm.except_orm, e:
            info = u'%s (%s-%s) %s' % (sw.name, sw.proces_id.name,
                                       sw.step_id.name, unicode(e.value))
            raise GiscedataSwitchingException(info, _(u'ERROR'))

        except Exception as e:
            raise GiscedataSwitchingException(e.message, _(u'ERROR'))

    def reactivar_contracte(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        sw.cups_polissa_id.send_signal(['activar'])
        polissa = sw.cups_polissa_id.browse()[0]
        if polissa.state == 'activa':
            res = (
                'OK',
                _(u'Pòlissa passada a estat activa correctament')
            )
            sw.case_close()
        else:
            res = (
                'ERROR',
                _(u"La pòlissa no s'ha pogut passar a activa")
            )
        return res

    def tall_polissa_b1(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        return sw_obj.tall_polissa_b1(cursor, uid, sw)

    def activar_d1(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        pool = self.pool
        polissa = None
        polissa_obj = pool.get('giscedata.polissa')
        conf_obj = pool.get('res.config')
        try:
            # Validem que el pas és final
            if not sw.finalitzat:
                # ERROR, no estem en un pas final
                info = _(u'Aquest cas (%s) no està finalitzat '
                         u'(D1-%s)') % (sw.name, sw.step_id.name)
                return (_(u'ERROR'), info)
            pas_obj = self.pool.get(sw.step_id.get_step_model())
            pas = pas_obj.browse(cursor, uid, pas_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0])
            if not sw.cups_polissa_id:
                # ERROR, la CUPS ha de tenir una Pólissa assignada
                info = _(
                    u"El CUPS {0} del cas {1} no tenia cap contracte actiu "
                    u"quan s'ha carregat el D1. No es poden aplicar els "
                    u"canvis".format(sw.cups_id.name, sw.name)
                ).format(sw.cups_polissa_id.name, sw.name)
                return (_(u'ERROR'), info)

            polissa = polissa_obj.browse(cursor, uid, sw.cups_polissa_id.id)
            if not polissa.state in ('activa', 'tall'):
                # ERROR, la Pólissa ha d'estar activa o en tall
                info = _(
                    u"Aquesta pólissa ({0}) del cas '{1}' ha d'estar activa "
                    u"o en tall"
                ).format(sw.cups_polissa_id.name, sw.name)
                return (_(u'ERROR'), info)

            # Configuració per saber si la periodicitat de facturació
            # ha de ser la mateixa que la de distri
            conf_id = 'sw_activacio_facturacio_segons_pas'
            per_fact_de_pas = bool(int(conf_obj.get(cursor, uid, conf_id, '0')))
            per_fact = int(pas.periodicitat_facturacio)
            tg = int(pas.motiu_canvi)
            vals = {'facturacio_distri': per_fact, 'tg': '%s' % tg}
            if per_fact_de_pas:
                vals.update({'facturacio': per_fact})

            hora_activacio = datetime.strptime(pas.date_created, '%Y-%m-%d %H:%M:%S')
            data_activacio = pas.data_activacio or hora_activacio.strftime('%Y-%m-%d')

            # Fem la modificació contractual
            polissa.send_signal('modcontractual')
            polissa.write(vals, context=context)
            wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')
            ctx = {'active_id': polissa.id, 'sync': False}
            params = {'duracio': 'actual', 'accio': 'modificar'}
            wz_id = wz_crear_mc_obj.create(cursor, uid, params, context=ctx)
            wiz = wz_crear_mc_obj.browse(cursor, uid, wz_id, context=ctx)

            cfg_obj = self.pool.get('res.config')
            d1act_new_modcon = int(cfg_obj.get(cursor, uid, 'sw_d1_activation_creates_modcon', '0'))
            if d1act_new_modcon:
                res = wz_crear_mc_obj.onchange_duracio(
                    cursor, uid, [wz_id], data_activacio, wiz.duracio, ctx
                )
                if res.get('warning', False):
                    info = _(
                        u"Hem trobat un error amb les dates al crear la "
                        u"modificació contractual de la pòlissa '{polissa_name}' "
                        u"pel cas '{cas_sw}'. Data activació '{data_activacio}'. "
                        u"{warning_title}: {warning_message}. "
                        u"AQUEST CANVI JA HA ESTAT ACTIVAT?"
                    ).format(**{
                        'polissa_name': polissa.name,
                        'cas_sw': sw.name,
                        'data_activacio': pas.data_activacio,
                        'warning_title': res['warning']['title'],
                        'warning_message': res['warning']['message']
                    })
                    polissa.send_signal('undo_modcontractual')
                    return (_(u'ERROR'), info)
                wiz.write({'data_final': res['value']['data_final'] or '', 'data_inici': data_activacio, 'accio': 'nou'})

            wiz.action_crear_contracte(context=ctx)

            # tanquem el cas
            if not sw.case_close():
                info = _(u"Per la pólissa '%s' no hem pogut tancar "
                         u"el cas %s") % (polissa.name, sw.name)
                return (_(u'ERROR'), info)
            return ('OK', u"Les modificacions del cas  %s "
                          u"s'han realitzat correctament" % (sw.name))
        except osv.except_osv, e:
            info = u'%s (D1-%s) %s' % (sw.name, sw.step_id.name,
                                       unicode(e.value))
            if polissa:
                polissa.send_signal('undo_modcontractual')
            return (_(u'ERROR'), info)
        except orm.except_orm, e:
            info = u'%s (D1-%s) %s' % (sw.name, sw.step_id.name,
                                       unicode(e.value))
            return (_(u'ERROR'), info)
        except Exception, e:
            if polissa:
                polissa.send_signal('undo_modcontractual')
            raise Exception(e)

    def activar_d1_autoconsum(self, cursor, uid, sw_id, context=None):
        logger.info('Activating D1-Autoconsum for SW (id: {})'.format(sw_id))
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        step = sw.get_pas()

        if step.motiu_canvi in ['01', '02', '03']:
            info = _(u"S'ha produït un error desconegut i no es pot continuar amb el procés.")
            return (_(u'ERROR'), info)

        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        ac_generador_obj = self.pool.get('giscedata.autoconsum.generador')
        d1_generador_obj = self.pool.get('giscedata.switching.generador')
        partner_obj = self.pool.get('res.partner')
        cups_obj = self.pool.get('giscedata.cups.ps')

        autoconsum_id = autoconsum_obj.search(cursor, uid, [('cau', '=', step.cau)])
        autoconsum_data = {
            'cau': step.cau,
            'data_alta': step.data_activacio,
            'seccio_registre': step.seccio_registre,
            'subseccio': step.subseccio,
            'comentaris': step.comentaris
        }

        if not autoconsum_id:
            autoconsum_id = [autoconsum_obj.create(cursor, uid, autoconsum_data, context=context)]
        else:
            autoconsum_obj.write(cursor, uid, autoconsum_id, autoconsum_data)

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        for generador in step.generadors:
            d1_generador = d1_generador_obj.browse(cursor, uid, generador.id, context=context)

            partner_vat = d1_generador.identificador
            if not 'ES' in partner_vat:
                partner_vat = 'ES{}'.format(partner_vat)
            partner_id = partner_obj.search(cursor, uid, [('vat', '=', partner_vat)])
            if not partner_id:
                info = _(u"S'ha produït un error en recuperar el titular de l'autoconsum. "
                         u"Revisi les dades d'identificació del titular dels generadors d'autoconsum")
                return (_(u'ERROR'), info)

            ac_generador_id = ac_generador_obj.search(cursor, uid, [('cil', '=', d1_generador.cil)])
            ac_generador_data = {
                'cil': d1_generador.cil,
                'tec_generador': d1_generador.tec_generador,
                'combustible': d1_generador.combustible,
                'pot_instalada_gen': d1_generador.pot_installada_gen,
                'tipus_installacio': d1_generador.tipus_installacio,
                'esquema_mesura': d1_generador.esquema_mesura,
                'ssaa': d1_generador.ssaa,
                'ref_cadastre': d1_generador.ref_cadastre_inst_gen,
                'autoconsum_id': autoconsum_id[0],
                'utm_x': d1_generador.utm_x,
                'utm_y': d1_generador.utm_y,
                'utm_fus': d1_generador.utm_fus,
                'utm_banda': d1_generador.utm_banda,
                'partner_id': partner_id[0],
                'data_alta': step.data_activacio or today,
            }

            if not ac_generador_id:
                ac_generador_obj.create(cursor, uid, ac_generador_data, context=context)
            else:
                ac_generador_obj.write(cursor, uid, ac_generador_id, ac_generador_data, context=context)

        cups_id = cups_obj.search(cursor, uid, [('name', 'ilike', '{}%'.format(step.cups))])
        if not cups_id:
            info = _(u"S'ha produït un error en recuperar el CUPS del generador. "
                     u"El CUPS indicat no existeix a la base de dades")
            return (_(u'ERROR'), info)

        cups_autoconsum_id = cups_obj.read(cursor, uid, cups_id[0], ['autoconsum_id'])['autoconsum_id']
        if not cups_autoconsum_id:
            wiz_alta_autoconsum_obj = self.pool.get('wizard.alta.baixa.autoconsum')
            params = {
                'autoconsum_id': autoconsum_id[0],
                'data': step.data_activacio or today,
            }
            wiz_id = wiz_alta_autoconsum_obj.create(cursor, uid, params, context=context)
            wiz = wiz_alta_autoconsum_obj.browse(cursor, uid, wiz_id)

            ctx = context.copy()
            ctx.update({'active_id': cups_id[0]})
            wiz.action_donar_alta_autoconsum(context=ctx)

        elif cups_autoconsum_id and cups_autoconsum_id[0] != autoconsum_id[0]:
            info = _(u"El CUPS ja té associat un autoconsum diferent de l'informat al cas D1. "
                     u"Revisi el CUPS %s afectat" % step.cups)
            return (_(u'ERROR'), info)

        # Notifiquem a client si fa falta
        res = sw.notifica_a_client(context=context)

        # Comprovem si tot ha anat be
        if len(res) != 2 and res[0] != 'OK':
            return res

        return ('OK', u"L'autoconsum amb CAU %s  ha sigut creat correctament" % (step.cau))

    def activar_r1_05_038(self, cursor, uid, sw_id, context=None):
        logger.info('Activating R1-038 for SW (id: {})'.format(sw_id))
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        sw.cups_polissa_id.write({'no_cessio_sips': 'active'})
        return (
            'OK',
            _(u'Contracte {} activat amb no cesió de SIPS').format(
                sw.cups_polissa_id.name
            )
        )


GiscedataSwitchingHelpers()
