# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class ResPartner(osv.osv):
    """Funcions utilitzades pel switching
    """
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'property_switching_xml_encoding': fields.property(
            'giscedata.switching.xml.encoding',
            type='many2one', relation='giscedata.switching.xml.encoding',
            string=u'Encoding XML switching', method=True, view_load=True,
            domain=[('name', '!=', False)],
            help=u"Encoding XML amb el qual es generaran els XML's "
                 u"de switching per aquesta empresa",
            required=True, readonly=False),
        'events': fields.one2many('res.partner.event', 'partner_id', 'Events',
                                  limit=10),
    }

ResPartner()
