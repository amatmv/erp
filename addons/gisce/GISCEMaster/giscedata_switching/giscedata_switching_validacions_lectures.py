# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from collections import namedtuple
from datetime import datetime, timedelta

ValidationResult = namedtuple('ValidationResult', ['valid', 'error_msg'])


class GiscedataSwitchingValidacioLectures(osv.osv):

    _name = 'giscedata.switching.validacio.lectures'

    def get_active_validations(self, cursor, uid, check_names="all", context=None):
        if check_names == "all":
            val_ids = self.search(cursor, uid, [], context={'active_test': True})
        else:
            val_ids = self.search(
                cursor, uid, [('method', 'in', check_names)],
                context={'active_test': False}
            )
        return self.read(cursor, uid, val_ids, [])

    _columns = {
        'method': fields.char(
            u'Mètode a executar', size=100, required=True, readonly=True
        ),
        'active': fields.boolean(u'Actiu', readonly=False),
        'description': fields.text(u'Descripció'),
        'show_message': fields.boolean(u"Mostrar missatge d'error")
    }

    _defaults = {
        'active': lambda *a: False,
        'show_message': lambda *a: True
    }

GiscedataSwitchingValidacioLectures()


class ValidadorLecturesATR(object):

    @staticmethod
    def agrupar_lectures_per_data(lectures):
        """
        Given a dictionari of lectures generated with the method
        get_lectures_vals() from GiscedataSwitchingAparell, returns a
        dictionari with the lectures grouped by date.

        The elements of the returned dictionari are used in method validate().

        Given:
        [{lecture_info_1}, {lecture_info_2}, {lecture_info_3}, ...]

        Returns:
        {
            'date1': [{lecture_info_1}, {lecture_info_2}, ....],
            'date2': [{lecture_info_3}, ...]
        }
        """
        lectures_agrupades = {}
        for lect in lectures:
            data_lectura = lect['name']
            if data_lectura not in lectures_agrupades:
                lectures_agrupades[data_lectura] = []
            lectures_agrupades[data_lectura].append(lect)
        return lectures_agrupades

    def validate(self, pool, cursor, uid, measures_list, sw_id=False, checks_list='all', context=None):
        """
        :param measures_list: list with dictionaries with lectures info.
        :param sw_id: switching case from where lectures where generated.
                      Some checks don't require sw_id, so it can be False
                      depending on the checks that are executed.
        :param checks_list: list with the names of the checks to execute.
                            If the string "all" is passed all active checks will
                            be executed.

        Execute methods of giscedata.switching.validacio.lectures
        and returns the result in a named tuple with:
        (
            valid: True if measures passed all check. Otherwise False.
            error_msg: information about not passed checks
        )

        """
        validations_obj = pool.get('giscedata.switching.validacio.lectures')
        correct = True
        res_msg = ""
        for check_info in validations_obj.get_active_validations(cursor, uid, checks_list):
            check_method = getattr(self, check_info['method'])
            res = check_method(pool, cursor, uid, measures_list, sw_id, context)
            if not res.valid:
                correct = False
            if res.error_msg and check_info['show_message']:
                res_msg = u"{0}{1}\n".format(res_msg, res.error_msg)

        return ValidationResult(correct, res_msg)

    def check_dates(self, pool, cursor, uid, measures_list, sw_id, context=None):
        sw_obj = pool.get("giscedata.switching")
        pas = sw_obj.get_pas(cursor, uid, [sw_id], context=context)

        activation_date = pas.data_activacio
        ant_date = datetime.strptime(activation_date, "%Y-%m-%d") - timedelta(days=1)
        ant_date = ant_date.strftime("%Y-%m-%d")
        # By now, "correct" lects. are only the ones odf the day of activation
        # or the day before
        correct_dates = [activation_date, ant_date]
        if isinstance(sw_id, (int, long)):
            sw_id = sw_obj.browse(cursor, uid, sw_id, context=context)
        proces = sw_id.proces_id.name
        step = sw_id.step_id.name
        correct = True
        msg = ""
        for lect in measures_list:
            if (proces in ('C1', 'C2') and step == '06') or (proces == 'B1' and step == '05'):
                if lect['name'] != activation_date:
                    del correct_dates[1]
                    correct = False
            elif proces in ('A3', 'M1', 'C1', 'C2') and step == '05':
                if lect['name'] != ant_date:
                    del correct_dates[0]
                    correct = False
            else:
                if lect['name'] not in correct_dates:
                    correct = False

            if not correct:
                msg = _(u"Les lectures del dia {0} no s'han carregat, no es "
                        u"troben en una de les dates a carregar ({1})").format(
                    lect['name'], str(correct_dates)[1:-1]
                )
                break
        return ValidationResult(correct, msg)

    def check_periods(self, pool, cursor, uid, measures_list, sw_id=False, context=None):
        tarifa_obj = pool.get('giscedata.polissa.tarifa')
        sw_obj = pool.get('giscedata.switching')
        period_obj = pool.get("giscedata.polissa.tarifa.periodes")

        if not len(measures_list):
            return ValidationResult(True, "")

        if context is None:
            context = {}

        mdate = measures_list[0]['name']
        polissa = False
        if not context.get("polissa_vals") and sw_id:
            polissa = sw_obj.browse(cursor, uid, sw_id, context=context).cups_polissa_id
            try:
                ctx = {'date': mdate}
                polissa_vals = polissa.read(['tarifa', 'facturacio_potencia'], context=ctx)[0]
            except Exception, e:
               # If we can't read on date we try to read in the current state
               polissa_vals = polissa.read(['tarifa', 'facturacio_potencia'])[0]
        else:
            polissa_vals = context.get("polissa_vals")

        tariff_id = polissa_vals['tarifa'][0]
        tariff = tarifa_obj.browse(cursor, uid, tariff_id)

        # We might be in initial lects for new modcon. Lects are from D-1
        # and modcon starts at D. If all lects. have a difenet tariff than
        # contract in day D-1 we will work in day D
        lect_tarifs = []
        for lect in measures_list:
            pinfo = period_obj.read(cursor, uid, lect['periode'][0], ['tarifa'])
            if pinfo['tarifa'] not in lect_tarifs:
                lect_tarifs.append(pinfo['tarifa'])

        if len(lect_tarifs) == 1 and lect_tarifs[0][0] != tariff.id and polissa:
            try:
                datePlus1 = datetime.strptime(mdate, "%Y-%m-%d")
                datePlus1 = datePlus1 + timedelta(days=1)
                datePlus1 = datePlus1.strftime("%Y-%m-%d")
                ctx = {'date': datePlus1}
                polissa_vals = polissa.read(['tarifa', 'facturacio_potencia'],
                                            context=ctx)[0]
            except Exception, e:
                # If we can't read on date we try to read in the current state
                polissa_vals = polissa.read(['tarifa', 'facturacio_potencia'])[0]

            tariff_id = polissa_vals['tarifa'][0]
        return self._check_periods_for_tariff(pool, cursor, uid, measures_list, tariff_id, fact_potencia=polissa_vals['facturacio_potencia'], context=context)

    def _check_periods_for_tariff(self, pool, cursor, uid, measures_list, tariff_id, fact_potencia="icp", context=None):
        if context is None:
            context = {}

        tarifa_obj = pool.get('giscedata.polissa.tarifa')
        period_obj = pool.get("giscedata.polissa.tarifa.periodes")
        tariff = tarifa_obj.browse(cursor, uid, tariff_id)

        expected_periods = {}
        active_periods = set(tariff.get_periodes(tipus='te'))
        reactive_periods = set(tariff.get_periodes(tipus='tr'))
        maximeter_periods = set(tariff.get_periodes(tipus='tp'))
        excess_periods = set(tariff.get_periodes(tipus='ep'))
        mdate = measures_list[0]['name']

        if active_periods:
            expected_periods['A'] = active_periods

        reactive_periods_required = not tariff.name.startswith('2')
        if reactive_periods and reactive_periods_required:
            expected_periods['R'] = reactive_periods

        maximeter_contract = fact_potencia == 'max'
        is_6_tariff = tariff.name.startswith('6')
        max_periods_required = maximeter_contract and not is_6_tariff
        expected_periods['M'] = maximeter_periods

        excess_periods_required = tariff.name.startswith('6')
        if excess_periods and excess_periods_required:
            expected_periods['EP'] = maximeter_periods

        total_extras = []
        total_missing = []
        incorrect_periods = []
        TYPES_TO_CHECK = ['A', 'R', 'M', 'EP']
        read = {}
        for lect in measures_list:
            pinfo = period_obj.read(cursor, uid, lect['periode'][0], ['name', 'tarifa'])
            pname = pinfo['name']
            ptarifa = pinfo['tarifa']
            tipus = lect.get('tipus') and lect['tipus'][0] or 'M'
            read.setdefault(tipus, set()).add(pname)
            if ptarifa[0] != tariff.id:
                incorrect_periods.append((pname, ptarifa))

        if len(incorrect_periods):
            msg = _(u"Les lectures del dia {0} no s'han carregat perqué tenim "
                    u"periodes que no pertanyen a la tarifa {1}: {2}").format(
                mdate, tariff.name, incorrect_periods
            )
            return ValidationResult(False, msg)

        for tipus in TYPES_TO_CHECK:
            if tipus in expected_periods and tipus in read:
                periods_read = read[tipus]

                extras = sorted(periods_read - expected_periods[tipus])
                missing = sorted(expected_periods[tipus] - periods_read)

                # If we don't need 'M' periods, we only check that
                # there are no extra periods to avoid future errors
                # when trying to proces the lectures
                if tipus == 'M' and not max_periods_required:
                    missing = []

                if tariff.name in ['003', '011']:
                    # If the tariff is a 3.0A or a 3.1A
                    if missing == ['P4', 'P5', 'P6']:
                        missing = []

                total_extras.extend(extras)
                total_missing.extend(missing)

            elif tipus in expected_periods:
                if tipus == 'M' and not max_periods_required:
                    continue
                total_missing.extend(expected_periods[tipus])

        correct = not len(total_extras) and not len(total_missing)
        if not correct:
            msg = _(u"Les lectures del dia {0} no s'han carregat perqué ens "
                    u"falten els periodes {1} i sobren els periodes {2}").format(
                mdate, total_missing, total_extras
            )
        else:
            msg = ""
        return ValidationResult(correct, msg)

    def check_origin(self, pool, cursor, uid, measures_list, sw_id, context=None):
        # We don't want lectures with origin "Sin Lectura" or "Estimada"
        origen_obj = pool.get("giscedata.lectures.origen")
        ignore_origins = origen_obj.search(cursor, uid, [("codi", 'in', ['40', '99'])])
        correct = True
        msg = ""
        for lect in measures_list:
            if lect.get('origen_id') and lect['origen_id'][0] in ignore_origins:
                msg = _(u'Les lectures del dia {0} no s\'han carregat perqué '
                        u'tenen com a origen "Estimada" (40) o "Sin Lectura" '
                        u'(99)').format(lect['name'])
                correct = False
                break

        return ValidationResult(correct, msg)

    def check_volta_contador(self, pool, cursor, uid, measures_list, sw_id=False, context=None):
        # We don't want lectures with lower value than previous ones
        lect_pol_obj = pool.get("giscedata.lectures.lectura.pool")
        pot_pol_obj = pool.get("giscedata.lectures.potencia.pool")
        correct = True
        msg = ""
        for lect in measures_list:
            compt_id = lect['comptador'][0]
            period_id = lect['periode'][0]
            name = lect['name']
            lids = lect_pol_obj.search(cursor, uid, [
                ('name', '<=', name), ('periode', '=', period_id),
                ('comptador', '=', compt_id), ('lectura', '>', lect['lectura'])
            ], context=context)
            pids = pot_pol_obj.search(cursor, uid, [
                ('name', '<=', name), ('periode', '=', period_id),
                ('comptador', '=', compt_id), ('lectura', '>', lect['lectura'])
            ], context=context)
            if len(lids) or len(pids):
                msg = _(u"Les lectures del dia {0} no s'han carregat perqué "
                        u"produirien una volta de comptador.").format(lect['name'])
                correct = False
                break

        return ValidationResult(correct, msg)
