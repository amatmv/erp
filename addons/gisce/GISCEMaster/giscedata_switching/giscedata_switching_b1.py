# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_b1 as b1

from tools.translate import _
from gestionatr.defs import *
from gestionatr.utils import get_description
from datetime import datetime, timedelta
import calendar
from .utils import get_address_dicct
from workdays import workday


class GiscedataSwitchingProcesB1(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        '''returns B1 inital steps depending on where we are'''
        if proces == 'B1':
            if where == 'distri':
                return ['01', '03']
            elif where == 'comer':
                return ['01', '03']

        return super(GiscedataSwitchingProcesB1,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        '''returns B1 emisor steps depending on where we are'''
        if proces == 'B1':
            if where == 'distri':
                return ['02', '04', '05', '06', '07']
            elif where == 'comer':
                return ['01', '03']

        return super(GiscedataSwitchingProcesB1,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == 'B1':
            return ['02', '04', '07']

        return super(GiscedataSwitchingProcesB1,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

GiscedataSwitchingProcesB1()


class GiscedataSwitchingB1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == 'B1':
            #Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('04', '05'):
                    return True

        return super(GiscedataSwitchingB1,
                     self).get_final(cursor, uid, sw, context=context)

    def tall_polissa_b1(self, cursor, uid, sw, context=None):
        """ Posa en estat de tall la pólissa d'un cas de switching i
        retorna un misastge amb el resultat"""
        sw.cups_polissa_id.send_signal(['impagament', 'tallar'])
        polissa = sw.cups_polissa_id.browse()[0]
        if polissa.state == 'tall':
            self.historize_additional_info(cursor, uid, sw)
            res = (
                'INFO',
                _(u'Pòlissa passada a estat tall correctament')
            )
        else:
            res = (
                'ERROR',
                _(u"La pòlissa no s'ha pogut passar a tall")
            )
        return res


GiscedataSwitchingB1()


class GiscedataSwitchingB1_01(osv.osv):
    """ Classe pel pas 01
    """

    _name = "giscedata.switching.b1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                      context=context)

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        Changes step vals accordingly with vals dict:
            'motiu': Reason
            'comments': Comment field
            'activacio': activation type
            'data_accio': activation date for 'F' activations

        If no 'activacio' and 'data_accio' passed, updates them depending on
        the 'motiu'.
        """
        if context is None:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        self.config_step_validation(cursor, uid, [], vals, context=context)

        pas = self.browse(cursor, uid, ids, context=context)

        new_vals = vals.copy()

        aux_vals = self.onchange_motiu(
            cursor, uid, [pas.id], vals.get('motiu'), pas.activacio, context
        )['value']

        if not new_vals.get('activacio'):
            new_vals.update({'activacio': aux_vals['activacio']})

        if not new_vals.get('data_accio'):
            new_vals.update({'data_accio': aux_vals['data_accio']})

        if new_vals.get('comments', False):
            del new_vals['comments']

        pas.write(new_vals)

        return True

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_01,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['02', '03']
    
    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML B1, pas 01
        """

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        tel_obj = self.pool.get("giscedata.switching.telefon")
        self.check_cognom(cursor, uid, pas_id, context=context)
        pas = self.browse(cursor, uid, pas_id, context)

        sw = pas.sw_id
        msg = b1.MensajeBajaSuspension()
        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        #sol·licitud
        sollicitud = b1.DatosSolicitud()
        sol_fields = {
            'ind_activacion': pas.activacio,
            'motivo': pas.motiu,
        }
        if pas.activacio == 'F':
            sol_fields.update({'fecha_prevista_accion': pas.data_accio})
        sollicitud.feed(sol_fields)
        #client
        idclient = b1.IdCliente()
        idclient.feed({
            'tipo_identificador': pas.tipus_document,
            'identificador': pas.codi_document,
            'tipo_persona': pas.persona,
        })
        nomclient = b1.Nombre()
        if pas.persona == 'J':
            nom = {'razon_social': pas.nom}
        else:
            nom = {'nombre_de_pila': pas.nom,
                   'primer_apellido': pas.cognom_1,
                   'segundo_apellido': pas.cognom_2}
        nomclient.feed(nom)
        client = b1.Cliente()
        cli_fields = {
            'id_cliente': idclient,
            'nombre': nomclient,
        }
        if pas.telefons:
            cli_fields.update({
                'telefonos': tel_obj.generar_xml(cursor, uid, pas.telefons)
            })
        client.feed(cli_fields)
        # contacte
        contacte = b1.Contacto()
        nom = pas.cont_nom
        cont_telef = []
        if pas.cont_telefons:
            cont_telef = tel_obj.generar_xml(cursor, uid, pas.cont_telefons)
        contacte.feed({
            'persona_de_contacto': nom,
            'telefonos': cont_telef,
        })
        #sol·licitud
        baja = b1.BajaSuspension()
        baja_vals = {
            'datos_solicitud': sollicitud,
            'cliente': client,
            'contacto': contacte,
            'comentarios': pas.comentaris or False,
            'iban': pas.iban
        }

        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            baja_vals.update({'registros_documento': doc_xml})

        baja.feed(baja_vals)
        msg.feed({
            'cabecera': capcalera,
            'baja_suspension': baja,
        })

        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def check_cognom(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context=context)
        if pas.persona == 'F' and not pas.cognom_1:
            raise osv.except_osv('Error',
                                 _(u"El camp cognom és obligatori "
                                   u"per persones físiques"))
        return True
    
    def onchange_persona(self, cr, uid, ids, pers, cog1, cog2, context=None):
        if not context:
            context = {}
        if pers == 'J':
            data = {'tipus_document': 'NI', 'cognom_1': '', 'cognom_2': ''}
        else:
            data = {'tipus_document': 'NI'}
        return {'value': data}

    def onchange_motiu(self, cr, uid, ids, motiu, activacio, context=None):
        if not context:
            context = {}
        if motiu == '03':
            data = {'activacio': 'A'}
        else:
            data = {'activacio': activacio}
            create_date = self.read(cr, uid, ids[0], ['date_created'])['date_created']
            create_date = datetime.strptime(create_date, "%Y-%m-%d %H:%M:%S")
            if motiu in ['01', '04']:
                data_accio = workday(create_date, 5)
            else:  # motiu == '02'
                data_accio = workday(create_date, 15)
            data.update({'data_accio': data_accio.strftime("%Y-%m-%d")})
        return {'value': data}

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        tel_obj = self.pool.get("giscedata.switching.telefon")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        tels = tel_obj.create_from_xml(cursor, uid, xml.cliente.telefonos)
        vals.update({
            # sol·licitud
            'activacio': xml.datos_solicitud.ind_activacion,
            'motiu': xml.datos_solicitud.motivo,
            'data_accio': xml.datos_solicitud.fecha_prevista_accion,
            # client
            'tipus_document': xml.cliente.tipo_identificador,
            'codi_document': xml.cliente.identificador,
            'persona': xml.cliente.tipo_persona,
            'telefons': [(6, 0, tels)],
            # IBAN
            'iban': xml.iban,
            # Comentaris
            'comentaris': xml.comentarios,
        })
        if xml.contacto:
            con_tels = tel_obj.create_from_xml(cursor, uid,
                                               xml.contacto.telefonos)
            vals.update({
                # Contacte
                'cont_nom': xml.contacto.persona_de_contacto,
                'cont_telefons': [(6, 0, con_tels)]
            })
        if xml.cliente.tipo_persona == 'F':
            vals.update({
                'nom': xml.cliente.nombre_de_pila,
                'cognom_1': xml.cliente.primer_apellido,
                'cognom_2': xml.cliente.segundo_apellido,
            })
        else:
            vals.update({
                'nom': xml.cliente.razon_social
            })

        # Registro Documents
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id
        
    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''
        
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.today()

        titular_vat = sw.cups_polissa_id.titular.vat
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, polissa.distribuidora.ref
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'motiu': '02',
            'activacio': 'F',
            'data_accio': datetime.strftime(workday(today, 15), '%Y-%m-%d'),
            'tipus_document': vat_info['document_type'],
            'codi_document': vat_info['vat'],
            'persona': vat_info['is_enterprise'] and 'J' or 'F',
        })

        if not vals.get('cont_nom'):
            # dades de contacte
            if vals['persona'] == 'J':
                nom = vals['nom']
            else:
                nom = "{0} {1} {2}".format(
                    vals['nom'], vals['cognom_1'], vals['cognom_2']
                )
            vals.update({
                'cont_nom': nom,
                'cont_telefons': vals['telefons'],
            })

        res = self.create(cursor, uid, vals, context=context)
        sw_obj.update_deadline(cursor, uid, sw_id, proces='B1', step='01')
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}

        info = self.read(cursor, uid, step_id, ['motiu', 'activacio', 'data_accio'])
        motiu = info['motiu']
        activacio = info['activacio']
        msg = _(u"({0}) {1}. Activació: {2}")
        if activacio == 'F':
            info_act = info['data_accio']
        elif activacio:
            info_act = get_description(activacio, "TABLA_8")
        else:
            info_act = ''

        return msg.format(motiu, get_description(motiu, "TABLA_10"), info_act)

    def get_motiu_baixa(self, cursor, uid, sw_id, context=None):
        '''
        Gets "motivo baja" from proces step 01
        :param ids:
        :param context:
        :return: dict {sw_id: motive, sw_id: motive}' from TABLA_10
        '''
        sw01_ids = self.search(
            cursor, uid, [('sw_id', '=', sw_id)], context=context
        )
        if not sw01_ids:
            return False

        sw01_vals = self.read(cursor, uid, sw01_ids[0], ['motiu'])
        return sw01_vals['motiu']

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        pas01_obj = self.pool.get("giscedata.switching.b1.01")
        pas_01_id = pas01_obj.search(cursor, uid,
                                     [("header_id.sw_id", "=", sw_id)])
        if not len(pas_01_id):
            return datetime.today()

        info = pas01_obj.read(cursor, uid, pas_01_id[0],
                              ['data_accio', 'enviament_pendent'])
        if info['enviament_pendent']:
            return datetime.strptime(info['data_accio'], '%Y-%m-%d')
        else:
            return datetime.today()

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        # Cabecera
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # DatosSolicitud
        'activacio': fields.selection(TABLA_8, u"Activació", required=True),
        'data_accio': fields.date(u"Data prevista del canvi o alta"),
        'motiu': fields.selection(TABLA_10, u"Motiu de baixa", size=2,
                                  required=True),
        # Cliente
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2,
                                           required=True),
        'codi_document': fields.char(u'Codi document', size=11,
                                     required=True),
        'persona': fields.selection(PERSONA, u'Persona', required=True),
        'nom': fields.char(u'Nom de la persona o societat', size=45,
                           required=True),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9),
        # Comentaris
        'comentaris': fields.text(u"Comentaris", size=4000),
        # Dades de contacte
        'cont_nom': fields.char(u'Nom de la persona o societat', size=45),
        'cont_prefix': fields.char(u"Prefix telefònic", size=2),
        'cont_telefon': fields.char(u"Telèfon", size=9),
        'iban': fields.char(u'IBAN', size=34),
    }

    _defaults = {
        'motiu': lambda *a: '01',
        'prefix': lambda *a: '34',
    }

GiscedataSwitchingB1_01()


class GiscedataSwitchingB1_02(osv.osv):
    """Classe pel pas 02
    """
    _name = "giscedata.switching.b1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                    context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if context and context.get("rebuig", False):
            return []
        return ['03', '04', '05', '06', '07']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_02,
                     self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML B1, pas 02
           d'acceptació
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id

        msg = b1.MensajeAceptacionBajaSuspension()
        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades acceptació
        dades = b1.DatosAceptacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
            'actuacion_campo': pas.actuacio_camp,
            'fecha_ultima_lectura_firme': pas.data_ult_lect,
            'tipo_activacion_prevista': pas.tipus_activacio,
            'fecha_activacion_prevista': pas.data_activacio
        })

        # Acceptacio
        acceptacio = b1.AceptacionBajaSuspension()
        acceptacio.feed({
            'datos_aceptacion': dades,
        })

        msg.feed({
            'cabecera': capcalera,
            'aceptacion_baja_suspension': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return (fname, str(msg))

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML B1, pas 02
           de rebuig
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML B1, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
            'rebuig': False,
            'data_acceptacio': xml.datos_aceptacion.fecha_aceptacion,
            'actuacio_camp': xml.datos_aceptacion.actuacion_campo,
            'data_ult_lect': xml.datos_aceptacion.fecha_ultima_lectura_firme,
            'tipus_activacio': xml.datos_aceptacion.tipo_activacion_prevista,
            'data_activacio': xml.datos_aceptacion.fecha_activacion_prevista,
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id
    
    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)
        
    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML B1, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)
           
    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        sw_helper = self.pool.get("giscedata.switching.helpers")

        activacio, data_activacio = sw_helper.get_data_prevista_activacio(
            cursor, uid, sw, context
        )

        data_lect = sw_helper.get_ultima_lectura(cursor, uid, polissa.id)
        vals.update({
            'rebuig': False,
            'data_acceptacio': today,
            'data_activacio': data_activacio,
            'tipus_activacio': activacio,
            'actuacio_camp': 'N',
            'data_ult_lect': data_lect or data_activacio,
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.b1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def onchange_tipus_activacio(self, cursor, uid, ids, tipus, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids)

        data = {}

        if tipus == 'L0':
            try:
                fi_lot = pas.sw_id.cups_polissa_id.lot_facturacio.data_final
                data_act = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                            timedelta(days=1)).strftime('%Y-%m-%d')

                data = {'data_activacio': data_act}
            except:
                pass
        return {'value': data}

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        pas02_id = self.search(cursor, uid,
                               [("header_id.sw_id", "=", sw_id)])
        if not len(pas02_id):
            return datetime.today()

        info_accep = self.read(
            cursor, uid, pas02_id[0], ['data_activacio']
        )
        return datetime.strptime(info_accep['data_activacio'], '%Y-%m-%d')

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template
    
    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        'actuacio_camp': fields.selection(SINO, u"Treball de camp"),
        'data_ult_lect': fields.date(u"Data última lectura", readonly=True),
        'tipus_activacio': fields.selection(TIPUS_ACTIVACIO,
                                            u"Activació", size=2),
        'data_activacio': fields.date(u"Data de baixa"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }
    
GiscedataSwitchingB1_02()


class GiscedataSwitchingB1_03(osv.osv):
    """Classe pel pas 03
    """
    _name = "giscedata.switching.b1.03"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '03'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                    context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['04', '02']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_03,
                     self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        anulacio_obj = self.pool.get('giscedata.switching.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return anulacio_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return anulacio_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return anulacio_obj.dummy_create(cursor, uid, sw_id, context=ctx)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
    }

GiscedataSwitchingB1_03()


class GiscedataSwitchingB1_04(osv.osv):
    """Classe pel pas 04
    """
    _name = "giscedata.switching.b1.04"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '04'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                    context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if context and context.get("rebuig", False):
            return ['05', '06', '07', '02']
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_04,
                     self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return acc_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        context.update({'hora_ac': True})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return acc_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name,
                    'nom_pas': self._nom_pas})

        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        if not context:
            context = {}
        context.update({'hora_ac': True})
        if xml.fecha_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'hora_ac': True})

        return acc_obj.dummy_create(cursor, uid, sw_id, context=ctx)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.b1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació",
                                       help=u"Data de execució de la reconnexió"
                                            u" (incluida) per les baixes"
                                            u" motiu 03 amb tall executat"),
        'hora_acceptacio': fields.time(u"Hora d'acceptació"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }

GiscedataSwitchingB1_04()


class GiscedataSwitchingB1_05(osv.osv):
    """Classe pel pas 05
    """
    _name = "giscedata.switching.b1.05"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '05'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                    context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['03']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_05,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context)
        sw = pas.sw_id
        msg = b1.MensajeActivacionBajaSuspension()

        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades activació
        dades_activacio = b1.DatosActivacionBaja()
        dades_activacio.feed({
            'fecha_activacion': pas.data_activacio,
        })
        # Contracte
        idcontracte = b1.IdContrato()
        nom_ctr = filter(lambda x: x.isdigit(), sw.cups_polissa_id.name)
        idcontracte.feed({
            'cod_contrato': nom_ctr
        })

        contracte = b1.Contrato()
        contracte.feed({
            'id_contrato': idcontracte,
        })

        punts_mesura = pas.header_id.generar_xml_pm(pas)

        # Notificacio
        activacio = b1.ActivacionBaja()
        activacio.feed({
            'datos_activacion_baja': dades_activacio,
            'contrato': contracte,
            'puntos_de_medida': punts_mesura
        })
        msg.feed({
            'cabecera': capcalera,
            'activacion_baja': activacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        pm_ids = header_obj.create_from_xml_pm(cursor, uid, xml,
                                               context=context)
        vals.update({
            # dades activació
            'data_activacio': xml.datos_activacion_baja.fecha_activacion,
            'contracte_atr': xml.contrato.cod_contrato or '',
            'pm_ids': [(6, 0, pm_ids)]
            })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        pm_ids = header_obj.dummy_create_pm(cursor, uid, sw, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'data_activacio': polissa.data_baixa or today,
            'contracte_atr': polissa.name or '',
            'pm_ids': [(6, 0, [pm_ids])],
            })

        return self.create(cursor, uid, vals)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades activació
        'data_activacio': fields.date(u"Data d'activació"),
        'contracte_atr': fields.char(u'Codi contracte ATR', size=12,
                                     readonly=True,
                                     help=u'Codi contracte a la distribuidora'),
    }

GiscedataSwitchingB1_05()


class GiscedataSwitchingB1_06(osv.osv):
    """Classe pel pas 06
    """
    _name = "giscedata.switching.b1.06"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '06'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                    context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['03', '04', '05', '07']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_06,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = b1.MensajeIncidenciasATRDistribuidor()
        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Incidencies
        inc_obj = self.pool.get("giscedata.switching.incidencia")
        incidencies = inc_obj.generar_xml(cursor, uid, pas)
        dades = b1.IncidenciasATRDistribuidor()
        dades.feed({
            'fecha_incidencia': pas.data_incidencia,
            'fecha_prevista_accion': pas.data_prevista_accio,
            'incidencia_list': incidencies,
        })

        msg.feed({
            'cabecera': capcalera,
            'incidencias_atr_distribuidor': dades,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref, pas.receptor_id.ref,
                                   pas='06')
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        inc_obj = self.pool.get('giscedata.switching.incidencia')

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml)
        incidencies = inc_obj.create_from_xml(cursor, uid, xml)

        vals.update({
            'data_incidencia': xml.fecha_incidencia,
            'data_prevista_accio': xml.fecha_prevista_accion,
            'incidencia_ids': [(6, 0, incidencies)],
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        'data_incidencia': fields.date(u"Data Incidència", required=True),
        'data_prevista_accio': fields.date(u"Data Prevista Acció"),
    }

    _defaults = {
        'data_incidencia': lambda *a: datetime.today(),
    }

GiscedataSwitchingB1_06()


class GiscedataSwitchingB1_07(osv.osv):

    _name = 'giscedata.switching.b1.07'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '07'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'B1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingB1_07,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '07'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
            """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'data_rebuig': datetime.today(),
        })
        return self.create(cursor, uid, vals)

    def _get_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Sempre és rebuig"""
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        return {}.fromkeys(ids, True)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_B1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        # Rebuig
        'rebuig': fields.function(_get_rebuig, 'Sol·licitud rebutjada',
                                  type='boolean', method=True),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

GiscedataSwitchingB1_07()
