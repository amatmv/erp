# -*- coding: utf-8 -*-

from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_r1 as r1
from gestionatr.input.messages import R1
from gestionatr.input.messages.R1 import get_type_from_subtype, get_subtypes
from .giscedata_switching import SwitchingException

from tools.translate import _
from gestionatr.defs import *
from gestionatr.utils import get_description
from datetime import datetime, timedelta, date
import calendar
from .utils import get_address_dicct


def get_type_subtype(proces):
    """
    :param proces: String like 'R1-TTSS' where TT is type and SS is subtype
    :return: tuple (type, subtype)
    """
    return proces[-5:-3], proces[-3:]


class GiscedataSwitchingProcesR1(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        """returns R1 inital steps depending on where we are"""
        if proces == 'R1':
            if where == 'distri':
                return ['01']
            elif where == 'comer':
                return ['01']

        return super(GiscedataSwitchingProcesR1,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        """returns R1 emisor steps depending on where we are"""
        if proces == 'R1':
            if where == 'distri':
                return ['02', '03', '05']
            elif where == 'comer':
                return ['01', '04']

        return super(GiscedataSwitchingProcesR1,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == 'R1':
            return ['02']

        return super(GiscedataSwitchingProcesR1,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

GiscedataSwitchingProcesR1()


class GiscedataSwitchingR1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        """Check if the case has arrived to the end or not"""

        if sw.proces_id.name == 'R1':
            # Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('05'):
                    return True

        return super(GiscedataSwitchingR1,
                     self).get_final(cursor, uid, sw, context=context)


GiscedataSwitchingR1()


class GiscedataSwitchingR1_01(osv.osv):
    """Classe pel pas 01
    """
    _name = "giscedata.switching.r1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        if not context:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        tel_obj = self.pool.get("giscedata.switching.telefon")
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        # Camps obligatoris
        vals.update({
            # Datos Solicitud
            'tipus': xml.datos_solicitud.tipo,
            'subtipus_id': subtipus_obj.get_subtipus(
                cursor, uid, xml.datos_solicitud.subtipo),
            'reforigen': xml.datos_solicitud.referencia_origen,
            'data_limit': xml.datos_solicitud.fecha_limite,
            'prioritari': xml.datos_solicitud.prioritario,
            # Tipo Reclamante
            'tipus_reclamant': xml.tipo_reclamante,
            # Comentaris
            'comentaris': xml.comentarios,
        })

        # Camps opcionals
        # VariablesDetalleReclamacion
        if xml.variables_detalle_reclamacion:
            recs = header_obj.create_from_xml_reclamacio(cursor, uid, xml,
                                                         context=context)
            vals.update({
                'reclamacio_ids': [(6, 0, recs)]
            })

        # Reg.Doc.
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        # Cliente
        if xml.cliente:
            tels = tel_obj.create_from_xml(cursor, uid, xml.cliente.telefonos)
            vals.update({
                'tipus_document': xml.cliente.tipo_identificador,
                'codi_document': xml.cliente.identificador,
                'persona': xml.cliente.tipo_persona,
                'telefons': [(6, 0, tels)],
                'email': xml.cliente.correo_electronico,
                'ind_direccio_fiscal': xml.cliente.indicador_tipo_direccion,
            })
            if xml.cliente.tipo_persona == 'F':
                vals.update({
                    'nom': xml.cliente.nombre_de_pila,
                    'cognom_1': xml.cliente.primer_apellido,
                    'cognom_2': xml.cliente.segundo_apellido,
                })
            else:
                vals.update({
                    'nom': xml.cliente.razon_social
                })

            # Search for address in cliente
            if xml.cliente.direccion:
                direccio = xml.cliente.direccion
                ctx = context.copy()
                ctx['cliente'] = xml.cliente
                addr_id = sw_obj.search_address(cursor, uid, sw_id, direccio, context=ctx)
                vals.update({'fiscal_address_id': addr_id})

        # Reclamante
        if xml.reclamante:
            tels = tel_obj.create_from_xml(cursor, uid, xml.reclamante.telefonos)
            vals.update({
                'rec_tipus_document': xml.reclamante.tipo_identificador,
                'rec_codi_document': xml.reclamante.identificador,
                'rec_persona': xml.reclamante.tipo_persona,
                'rec_telefons': [(6, 0, tels)],
                'rec_email': xml.reclamante.correo_electronico,
            })
            if xml.reclamante.tipo_persona == 'F':
                vals.update({
                    'rec_nom': xml.reclamante.nombre_de_pila,
                    'rec_cognom_1': xml.reclamante.primer_apellido,
                    'rec_cognom_2': xml.reclamante.segundo_apellido,
                })
            else:
                vals.update({
                    'rec_nom': xml.reclamante.razon_social
                })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(
            cursor, uid, sw_id, self._nom_pas, pas_id, context=context
        )

        return pas_id

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'R1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return ['02']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingR1_01, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML R1, pas 01
        """

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        tel_obj = self.pool.get("giscedata.switching.telefon")

        sw = pas.sw_id
        msg = r1.MensajeReclamacionPeticion()
        # capçalera
        capcalera = pas.header_id.generar_xml(pas, context=context)
        # detalls
        variables = pas.header_id.generar_xml_reclamacio(
            pas, context=context
        )

        dades = r1.DatosSolicitud()
        dades_vals = {
            'tipo': pas.tipus,
            'subtipo': pas.subtipus_id.name,
            'referencia_origen': pas.reforigen,
            'fecha_limite': pas.data_limit,
            'prioritario': pas.prioritari,
        }
        dades.feed(dades_vals)

        # client
        client = None
        if pas.tipus_document:
            idclient = r1.IdCliente()
            idclient.feed({
                'tipo_identificador': pas.tipus_document,
                'identificador': pas.codi_document,
                'tipo_persona': pas.persona,
            })
            nomclient = r1.Nombre()
            if pas.persona == 'J':
                nom = {'razon_social': pas.nom}
            else:
                nom = {'nombre_de_pila': pas.nom,
                       'primer_apellido': pas.cognom_1,
                       'segundo_apellido': pas.cognom_2}
            nomclient.feed(nom)
            client = r1.Cliente()
            cli_fields = {
                'id_cliente': idclient,
                'nombre': nomclient,
                'indicador_tipo_direccion': pas.ind_direccio_fiscal,
                'correo_electronico': pas.email,
            }
            if pas.ind_direccio_fiscal == 'F':
                address = pas.fiscal_address_id
                dir_fiscal = r1.Direccion()
                addres_info = get_address_dicct(address)
                via, apartado = False, False
                if addres_info['apartado_de_correos']:
                    apartado = addres_info['apartado_de_correos']
                else:
                    via = r1.Via()
                    via_vals = {
                        'calle': addres_info['calle'],
                        'numero_finca': addres_info['numfinca'],
                    }
                    if address.aclarador:
                        via_vals.update({'tipo_aclarador_finca': 'NO',
                                         'aclarador_finca': address.aclarador[:40]})
                    via.feed(via_vals)
                dir_vals = {'pais': addres_info['pais'],
                            'provincia': addres_info['provincia'],
                            'municipio': addres_info['municipio'],
                            'poblacion': addres_info['poblacion'],
                            'cod_postal': addres_info['codpostal'],
                            'via': via,
                            'apartado_de_correos': apartado
                            }
                dir_fiscal.feed(dir_vals)
                cli_fields.update({'direccion': dir_fiscal})
            if pas.telefons:
                cli_fields.update(
                    {'telefonos': tel_obj.generar_xml(cursor, uid, pas.telefons)}
                )
            client.feed(cli_fields)

        # reclamante
        reclamant = None
        if pas.tipus_reclamant != '06':
            idrec = r1.IdReclamante()
            idrec.feed({
                'tipo_identificador': pas.rec_tipus_document,
                'identificador': pas.rec_codi_document,
            })

            nomrec = r1.Nombre()
            if pas.rec_persona == 'J':
                nom = {'razon_social': pas.rec_nom}
            else:
                nom = {'nombre_de_pila': pas.rec_nom,
                       'primer_apellido': pas.rec_cognom_1,
                       'segundo_apellido': pas.rec_cognom_2}
            nomrec.feed(nom)
            rec_vals = {
                'id_reclamante': idrec,
                'nombre': nomrec,
                'correo_electronico': pas.rec_email,
            }

            if pas.rec_telefons:
                rec_vals.update({
                    'telefonos': tel_obj.generar_xml(cursor, uid, pas.rec_telefons)
                })
            reclamant = r1.Reclamante()
            reclamant.feed(rec_vals)

        solicitud = r1.SolicitudReclamacion()
        solicitud_vals ={
            'datos_solicitud': dades,
            'variables_detalle_reclamacion': variables,
            'tipo_reclamante': pas.tipus_reclamant,
            'reclamante': reclamant,
            'cliente': client,
            'comentarios': pas.comentaris,
        }
        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            solicitud_vals.update({'registros_documento': doc_xml})

        solicitud.feed(solicitud_vals)
        msg.feed({
            'cabecera_reclamacion': capcalera,
            'solicitud_reclamacion': solicitud
        })

        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        # Check Minimum Fields
        # Build R1 object with generated XML
        r1_obj = R1(str(msg))
        r1_obj.set_xsd()
        r1_obj.parse_xml(validate=False)
        min_camps_no_trobats = r1_obj.check_minimum_fields()
        if min_camps_no_trobats:
            msg = ""
            for c in min_camps_no_trobats:
                msg += "           * {}\n".format(c)
            raise osv.except_osv('Error',
                                 _(u"Falten camps mínims per aquest "
                                   u"subtipus de reclamació ({1}-{2}):\n{0}")
                                 .format(msg, pas.tipus, pas.subtipus_id.name))

        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def update_contract(self, cursor, uid, pas_id, context=None):
        pas_obj = self.pool.get('giscedata.switching.r1.01')
        pas = pas_obj.browse(cursor, uid, pas_id, context=context)
        polissa = pas.header_id.sw_id.cups_polissa_id

        polisses_obj = self.pool.get('giscedata.polissa')
        observacions = '\n{}'.format(polissa.observacions) \
            if polissa.observacions else ''

        user_obj = self.pool.get('res.users')
        desc_tipus = get_description(pas.tipus, 'TABLA_81')
        desc_subtipus = pas.subtipus_id.desc
        observ_values = {
            'data': date.today().strftime('%Y-%m-%d'),
            'usuari': user_obj.read(cursor, uid, uid, ['name'])['name'],
            'tipus': pas.tipus,
            'subtipus': pas.subtipus_id.name,
            'desc_tipus': desc_tipus,
            'desc_subtipus': desc_subtipus
        }
        observacions = _(
            '{data} {usuari} Reclamació ({tipus}-{subtipus}) '
            '{desc_tipus}-{desc_subtipus}. '
            'Generat el pas R1-01{obs}').format(
            obs=observacions, **observ_values)
        polissa.write({'observacions': observacions})

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        polissa = sw.cups_polissa_id
        if polissa:
            titular_vat = sw.cups_polissa_id.titular.vat
            vat_info = sw_obj.get_vat_info(
                cursor, uid, titular_vat, polissa.distribuidora.ref
            )
            vals.update(
                {'data_lectura': polissa.data_ultima_lectura,
                 'tipus_document': vat_info['document_type'],
                 'codi_document': vat_info['vat'],
                 'persona': vat_info['is_enterprise'] and 'J' or 'F',
                 'fiscal_address_id': polissa.direccio_pagament.id,
                 }
            )

            conf_obj = self.pool.get('res.config')
            conf_var = 'sw_r1_from_invoice_mail_add'
            invoice_mail_add = bool(int(conf_obj.get(cursor, uid, conf_var, '0')))
            if invoice_mail_add:
                vals.update({'email': polissa.direccio_pagament.email})

        return self.create(cursor, uid, vals, context=context)

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        """ Validate's data
        :param vals: dict with values to modify (refer to config_step
            funtion
        :return: True or raises a SwitchingException
        """
        vals_to_check = vals
        if "r1_values" in vals:
            vals_to_check = vals.get("r1_values")

        mandatory_fields = []
        if vals_to_check.get("type"):
            mandatory_fields.append("type")
        else:
            mandatory_fields.append("tipus")

        if vals_to_check.get("subtype"):
            mandatory_fields.append("subtype")
        else:
            mandatory_fields.append("subtipus_id")

        if vals_to_check.get("comments"):
            mandatory_fields.append("comments")
        elif vals_to_check.get("comentaris"):
            mandatory_fields.append("comentaris")

        if vals_to_check.get('type', 0) == '02' and vals_to_check.get('subtype', 0) == '036':
            mandatory_fields.append('invoice_number')

        for field in mandatory_fields:
            if not vals_to_check.get(field, False):
                raise SwitchingException(
                    _("Falten dades. Camp no omplert."),
                    [field]
                )

        invoice_obj = self.pool.get("giscedata.facturacio.factura")

        # checks if invoice exists
        if vals_to_check.get('invoice_number', False):
            reference = vals_to_check['invoice_number']
            inv_ids = invoice_obj.search(
                cursor, uid, [('reference', '=', reference)]
            )
            if not inv_ids:
                raise SwitchingException(
                    _("No s'ha trobat cap factura de proveïdor amb referència "
                      "'{0}'").format(reference),
                    ['invoice_number']
                )
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """ Changes step vals accordingly with vals dict:
        :param vals: dict with values to modify
            'type': R1-type i.e. 02,
            'subtype': R1-subtype i.e 036,
            'objector': Who objects i.e 06 (comer),
            'invoice_number': invoice reference,
            'comments': comments on R1-01 step,
            'measure_date': Date of measures
            'codi_dh': CNMC CodigoDh code of measures
            'measures': List of tuples in the form
                (periode(Pn),
                {'AE','Rn','PM','EP '},
                measure)
            'r1_values': values to make a write directly to the pas
            to be created with create_lects function
        :return: True or False
        """
        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        reclama_obj = self.pool.get("giscedata.switching.reclamacio")
        measures_obj = self.pool.get("giscedata.switching.lectura")

        self.config_step_validation(cursor, uid, [], vals, context=context)

        pas = self.browse(cursor, uid, ids, context=context)

        new_vals = vals.get("r1_values", {})
        reclama_vals = vals.get('reclama_vals', {})
        reclama_ids = vals.get("reclamacio_ids", [])

        # complaint generation
        if vals.get('invoice_number', False):
            reclama_vals.update({
                'num_factura': vals['invoice_number']
            })

        if vals.get('measures', False):
            measures_ids = measures_obj.create_lects(
                cursor, uid, vals['measures'], context=context
            )
            #complaint
            reclama_vals.update({
                'data_lectura': vals['measure_date'],
                'codi_dh': vals['dh_code'],
                'lect_ids': [(6, 0, measures_ids)]
            })

        if reclama_vals:
            reclama_id = reclama_obj.create(
                cursor, uid, reclama_vals, context=context
            )
            reclama_ids.append(reclama_id)

        if reclama_ids and not (len(reclama_ids) == 1 and isinstance(reclama_ids[0], tuple)):
            new_vals.update(
                {'reclamacio_ids': [(6, 0, reclama_ids)]}
            )

        # step generation
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        # step
        if vals.get('type', False) and vals.get('subtype', False):
            new_vals.update({
                'tipus': vals['type'],
                'subtipus_id': subtipus_obj.get_subtipus(cursor, uid,
                                                         vals['subtype']),
            })

        if vals.get('objector', False):
            new_vals.update({'tipus_reclamant': vals['objector']})

        if vals.get('comments', False):
            new_vals.update({'comentaris': vals['comments']})

        if vals.get("ref_model") and vals.get("ref_id"):
            ref_model = self.pool.get(vals["ref_model"])
            ref_model.write(
                cursor, uid, vals["ref_id"],
                {"ref": "giscedata.switching, {}".format(pas.sw_id.id)}
            )
            pas.sw_id.write(
                {"ref": "{0}, {1}".format(vals["ref_model"], vals["ref_id"])}
            )
            pas = pas.browse()[0]

        if vals.get("auto_r1_atc") and pas.sw_id.ref and "giscedata.atc" in pas.sw_id.ref:
            tracking_obj = self.pool.get("crm.time.tracking")
            tracking = tracking_obj.search(cursor, uid, [('code', '=', '1')])[0]
            atc_o = self.pool.get("giscedata.atc")
            atc_id = int(pas.sw_id.ref.split(",")[-1])
            atc_o.case_pending(cursor, uid, [atc_id])
            atc_o.write(cursor, uid, atc_id, {
                'agent_actual': '10',
                'time_tracking_id': tracking
            }, context=context)

        pas.write(new_vals)
        return True

    def onchange_subtipus(self, cr, uid, ids, subtipus, context=None):
        if not context:
            context = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, subtipus, ['type'])
        data = {
            'tipus': subinfo['type']
        }
        return {'value': data}

    def get_desc_taula(self, atribut, taula):
        field = [x[1] for x in taula if x[0] == atribut]
        if len(field) > 0:
            return field[0]
        return None

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        if isinstance(step_id, (list, tuple)):
            step_id = step_id[0]
        step = self.read(cursor, uid, step_id, ['tipus', 'subtipus_id'])
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cursor, uid, step['subtipus_id'][0], ['name', 'desc'])
        return u'{0}-{1}: {2}'.format(step['tipus'], subinfo['name'],
                                      subinfo['desc'])

    def _get_default_subtipus(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        return imd_obj.get_object_reference(
            cursor, uid, 'giscedata_subtipus_reclamacio', 'subtipus_reclamacio_009'
        )[1]

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_R1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'tipus': fields.selection(TABLA_81, u"Tipus", required=True),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipus",
                                       domain="[('sector','in', ('t', 'e'))]",
                                       required=True),
        'reforigen': fields.char(u"Referència interna", size=35,
                                 help=u'Referència propia per seguiment '
                                      u'intern'),
        'data_limit': fields.date(u"Data Limit"),
        'prioritari': fields.selection(TABLA_26, u"Prioritari"),
        # client
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2),
        'codi_document': fields.char(u'Codi document', size=11),
        'persona': fields.selection(PERSONA, u'Persona'),
        'nom': fields.char(u'Nom de la persona o societat', size=45),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9),
        'email': fields.char(u"Correu Electrònic", size=60),
        'ind_direccio_fiscal': fields.selection(TABLA_11,
                                                u"Indicador direcció"),
        'fiscal_address_id': fields.many2one('res.partner.address',
                                             u"Adreça fiscal"),

        'tipus_reclamant': fields.selection(TABLA_83, u"Tipus Reclamant"),

        # reclamante
        'rec_tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2),
        'rec_codi_document': fields.char(u'Codi document', size=11),
        'rec_persona': fields.selection(PERSONA, u'Persona'),
        'rec_nom': fields.char(u'Nom de la persona o societat', size=45),
        'rec_cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'rec_cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'rec_prefix': fields.char(u"Prefix telefònic", size=2),
        'rec_telefon': fields.char(u"Telèfon", size=9),
        'rec_email': fields.char(u"Correu Electrònic", size=60),

        # comentaris
        'comentaris': fields.text(u"Comentaris", size=4000)
    }

    _defaults = {
        'tipus': lambda *a: '02',
        'subtipus_id': _get_default_subtipus,
        'tipus_reclamant': lambda *a: '06',
        'ind_direccio_fiscal': lambda *a: 'S',
        'comentaris': lambda *a: '',
    }

GiscedataSwitchingR1_01()


class GiscedataSwitchingR1_02(osv.osv):
    """Classe pel pas 02
    """
    _name = "giscedata.switching.r1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'R1', self._nom_pas,context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        if context and context.get("rebuig", False):
            return []
        return ['03', '04', '05']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingR1_02,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
            'rebuig': False,
            'data_acceptacio': xml.datos_aceptacion.fecha_aceptacion,
            'codi_reclamacio_distri':
                xml.datos_aceptacion.codigo_reclamacion_distribuidora
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML C1, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna un text amb els motius del rebuig"""
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML R1, pas 02
        """

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = r1.MensajeAceptacionReclamacion()
        # capçalera
        capcalera = pas.header_id.generar_xml(pas)

        # Dades acceptació
        acceptacio = r1.AceptacionReclamacion()
        dades = r1.DatosAceptacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
            'codigo_reclamacion_distribuidora': pas.codi_reclamacio_distri,
        })
        acceptacio.feed({'datos_aceptacion': dades})
        msg.feed({
            'cabecera_reclamacion': capcalera,
            'aceptacion_reclamacion': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return (fname, str(msg))

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(
            cursor, uid, step_id,
            ['rebuig', 'motiu_rebuig', 'sw_id']
        )
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.r1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        vals.update({
            # Dades acceptació
            'data_acceptacio': date.today(),
            'codi_reclamacio_distri': sw_id,

            # Rebuig
            'rebuig': False,
        })
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_R1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació", required=True),
        'codi_reclamacio_distri': fields.char(
            u"Codi Reclamació Distribuïdora", size=26),

        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today(),
        'data_acceptacio': lambda *a: datetime.today(),
    }

GiscedataSwitchingR1_02()


class GiscedataSwitchingR1_03(osv.osv):
    """Classe pel pas 03
    """
    _name = "giscedata.switching.r1.03"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '03'

    TAULA_INFO_INTERMITJA = [
        ('01', u'Descripció Informacio Intermitja'),
        ('02', u'Intervencions')
    ]

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'R1', self._nom_pas, context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return ['03', '04', '05']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingR1_03,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        if not context:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
            # Datos Informacion
            'tipus_comunicacio': xml.datos_informacion.tipo_comunicacion,
            'codi_reclamacio_distri': xml.datos_informacion.codigo_reclamacion_distribuidora,
            'num_expedient_escomesa': xml.datos_informacion.num_expediente_acometida,
            # Comentaris
            'comentaris': xml.comentarios,
        })

        # Informacio Intermitja
        if xml.informacion_intermedia:
            if xml.informacion_intermedia.desc_informacion_intermedia:
                vals.update({
                    'desc_info_intermitja': xml.informacion_intermedia.desc_informacion_intermedia,
                    'escollir': '01',
                    'hi_ha_info_intermitja': True
                })
            else:
                intervencions = header_obj.create_from_xml_intervencio(
                    cursor, uid, xml, context=context
                )
                vals.update({
                    'intervencio_ids': [(6, 0, intervencions)],
                    'escollir': '02',
                    'hi_ha_info_intermitja': True
                })

        # Retipificacio
        if xml.retipificacion:
            vals.update({
                'retip_tipus': xml.retipificacion.tipo,
                'retip_subtipus_id': subtipus_obj.get_subtipus(cursor, uid, xml.retipificacion.subtipo),
                'retip_desc': xml.retipificacion.desc_retipificacion,
                'hi_ha_retipificacio': True
            })

        # Solicituds Informacio Adicional
        if xml.solicitudes_informacion_adicional:
            sollicituds = header_obj.create_from_xml_sollicitud(
                    cursor, uid, xml, context=context
            )
            vals.update({
                'sollicitud_ids': [(6, 0, sollicituds)],
                'hi_ha_sollicitud': True
            })
            if xml.solicitud_informacion_adicional_para_retipificacion:
                sol = xml.solicitud_informacion_adicional_para_retipificacion
                vals.update({
                    'sol_retip_tipus': sol.tipo,
                    'sol_retip_subtipus_id': subtipus_obj.get_subtipus(cursor, uid, sol.subtipo),
                    'sol_retip_data_limit': sol.fecha_limite_envio,
                    'hi_ha_sol_info_retip': True
                })

            # Registro Documents
            if xml.registros_documento:
                doc_ids = header_obj.create_from_xml_doc(
                    cursor, uid, xml.registros_documento, context=context
                )
                vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(
            cursor, uid, sw_id, self._nom_pas, pas_id, context=context
        )

        return pas_id

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id, context)
        msg = r1.MensajePeticionInformacionAdicionalReclamacion()

        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)

        # Informacion Adicional
        info_adicional = r1.InformacionAdicional()

        # Datos Informacion
        dades = r1.DatosInformacion()
        dades.feed({
            'tipo_comunicacion':  pas.tipus_comunicacio,
            'codigo_reclamacion_distribuidora':  pas.codi_reclamacio_distri,
            'num_expediente_acometida': pas.num_expedient_escomesa
        })

        info_adicional.feed({'datos_informacion': dades})

        # Informacion Intermedia
        if pas.hi_ha_info_intermitja:
            info_intermitja = r1.InformacionIntermedia()
            if pas.escollir == '01':
                info_intermitja.feed({
                    'desc_informacion_intermedia': pas.desc_info_intermitja
                })
            else:
                intervencions = pas.header_id.generar_xml_intervencio(
                    pas, context=context
                )
                info_intermitja.feed({
                    'intervenciones': intervencions
                })

            info_adicional.feed({'informacion_intermedia': info_intermitja})


        # Retipificación
        if pas.hi_ha_retipificacio:
            retificacio = r1.Retipificacion()
            retificacio.feed({
                'tipo': pas.retip_tipus,
                'subtipo': pas.retip_subtipus_id.name,
                'desc_retipificacion': pas.retip_desc,
            })

            info_adicional.feed({'retipificacion': retificacio})

        # Solicitudes Informacion Adicional
        if pas.hi_ha_sollicitud:
            sollicituds = pas.header_id.generar_xml_sollicitud(
                pas, context=context
            )

            info_adicional.feed({'solicitudes_informacion_adicional': sollicituds})

        # Comentarios
        info_adicional.feed({'comentarios': pas.comentaris})

        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            info_adicional.feed({'registros_documento': doc_xml})


        msg.feed({
            'cabecera_reclamacion': capcalera,
            'informacion_adicional': info_adicional,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = pas.sw_id.generar_nom_xml(pas.emisor_id.ref,
                                          pas.receptor_id.ref,
                                          pas=pas._nom_pas)
        return fname, str(msg)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        # Get info from steps 01 and 02
        pas_01 = self.pool.get("giscedata.switching.r1.01")
        pas_02 = self.pool.get("giscedata.switching.r1.02")
        pas_01_info = None
        pas_02_info = None
        pas_01_id = pas_01.search(cursor, uid, [('header_id.sw_id', '=', sw.id)])
        if len(pas_01_id) > 0:
            pas_01_info = pas_01.read(
                cursor, uid, pas_01_id[0], ['tipus', 'subtipus_id'])
        pas_02_id = pas_02.search(cursor, uid, [('header_id.sw_id', '=', sw.id)])
        if len(pas_02_id) > 0:
            pas_02_info = pas_02.read(
                cursor, uid, pas_02_id[0], ['codi_reclamacio_distri'])

        comentari = "Reclamacio per al CUPS {0}".format(sw.cups_id.name)
        vals.update({
            'codi_reclamacio_distri':
                pas_02_info and pas_02_info['codi_reclamacio_distri'] or '',
            'comentaris': comentari,
            'tipus_comunicacio': '01',
            'hi_ha_sollicitud': True,
            'retip_tipus': pas_01_info and pas_01_info['tipus'] or '',
            'retip_subtipus_id': pas_01_info and pas_01_info['subtipus_id'][0] or '',
            'sol_retip_tipus': pas_01_info and pas_01_info['tipus'] or '',
            'sol_retip_subtipus_id': pas_01_info and pas_01_info['subtipus_id'][0] or '',
        })
        return self.create(cursor, uid, vals, context=context)

    def onchange_tipus_comunicacio(self, cr, uid, ids, tipus_comunicacio, context=None):
        if not context:
            context = {}
        data = {
            'hi_ha_info_intermitja': False,
            'hi_ha_retipificacio': False,
            'hi_ha_sollicitud': False,
            'hi_ha_sol_info_retip': False,
        }
        if tipus_comunicacio == '01':
            data .update({'hi_ha_sollicitud': True})
        elif tipus_comunicacio == '02':
            data.update({'hi_ha_info_intermitja': True})
        elif tipus_comunicacio == '03':
            data.update({'hi_ha_retipificacio': True})
        else:  # tipus_comunicacio == '04'
            data.update({
                'hi_ha_sollicitud': True,
                'hi_ha_sol_info_retip': True,
            })
        return {'value': data}

    def onchange_subtipus(self, cr, uid, ids, subtipus, mod='retip', context=None):
        if not context:
            context = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, subtipus, ['type'])
        data = {
            '{0}_tipus'.format(mod): subinfo['type']
        }
        return {'value': data}

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_R1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(
            cursor, uid, step_id, ['sw_id', 'tipus_comunicacio']
        )
        sw_id = step['sw_id'][0]
        step02 = self.pool.get('giscedata.switching.r1.02')
        step02_id = step02.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step02_id) > 0:
            antinf = step02.get_additional_info(cursor, uid, step02_id[0])

        res_info = ""
        if step['tipus_comunicacio']:
            tipus_comunicacio_text = get_description(step['tipus_comunicacio'], 'TABLA_84')
            res_info = u" Comunicación R1-03: {0}".format(tipus_comunicacio_text)
        return u"{0}{1}".format(antinf, res_info)

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # DatosInformacion
        'num_expedient_escomesa': fields.char(
             u'Núm. Expedient Escomesa', size=20
        ),
        'tipus_comunicacio': fields.selection(
            TABLA_84, u"Tipus de la Comunicacio", required=True
        ),
        'codi_reclamacio_distri': fields.char(
            u"Codi reclamació distribuidora", size=26, required=True
        ),

        # InformacionIntermedia
        'hi_ha_info_intermitja': fields.boolean(u'Informació Intermitja'),
        'escollir': fields.selection(TAULA_INFO_INTERMITJA, u"Tipus Informació Intermitja"),
        'desc_info_intermitja': fields.text(
            u"Descripció de la Informació Intermitja", size=255),

        # Sol.licituds Infromacio Adicional
        'hi_ha_sollicitud': fields.boolean(u'Sol.licituds Informació Adicional'),
        # Soll.licitud Informacio Retipificacio
        'sol_retip_tipus': fields.selection(TABLA_81, u"Tipus"),
        'sol_retip_subtipus_id': fields.many2one("giscedata.subtipus.reclamacio", u"SubTipus",
                                                 domain="[('sector','in', ('t', 'e'))]"),
        'sol_retip_data_limit': fields.date(u"Data Limit d'Enviament"),
        'hi_ha_sol_info_retip': fields.boolean(u'Sol.licitud Informació per Retipificació'),
        # Retificació
        'hi_ha_retipificacio': fields.boolean(u'Retipificació'),
        'retip_tipus': fields.selection(TABLA_81, u"Tipus Retipificació"),
        'retip_subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                    u"SubTipus Retipificació", domain="[('sector','in', ('t', 'e'))]"),
        'retip_desc': fields.text(u"Descripció Retipificació", size=255),

        # Comentaris
        'comentaris': fields.text(u"Comentaris", size=4000, required=True)
    }

    _defaults = {
        'escollir': lambda *a: '01',
        'hi_ha_info_intermitja': lambda *a: False,
        'hi_ha_sollicitud': lambda *a: False,
        'hi_ha_retipificacio': lambda *a: False,
        'hi_ha_sol_info_retip': lambda *a: False,
    }


GiscedataSwitchingR1_03()


class GiscedataSwitchingR1_04(osv.osv):
    """Classe pel pas 03
    """
    _name = "giscedata.switching.r1.04"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '04'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'R1', self._nom_pas, context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return ['03', '04', '05']

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingR1_04,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        if not context:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        tel_obj = self.pool.get("giscedata.switching.telefon")

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        dades_enviament = xml.datos_envio_informacion
        vals.update({
            # Datos Envio Informacion
            'data_informacio': dades_enviament.fecha_informacion,
            'num_expedient_escomesa': dades_enviament.num_expediente_acometida,
            # Comentaris
            'comentaris': xml.comentarios,
        })

        # Variables Aportacio informacio
        if xml.variables_aportacion_informacion:
            variables = header_obj.create_from_xml_var_aport_info(
                cursor, uid, xml, context=context
            )
            vals.update({
                'vars_aportacio_info_ids': [(6, 0, variables)],
                'hi_ha_var_info': True
            })

        # Variables Aportacio Informacio Retipificacio
        if xml.variables_aportacion_informacion_para_retipificacion:
            context.update({'var_retip': True})
            variables = header_obj.create_from_xml_reclamacio(
                cursor, uid, xml, context=context
            )
            vals.update({
                'reclamacio_ids': [(6, 0, variables)],
                'hi_ha_var_info_retip': True
            })

        # Client
        if xml.cliente:
            tels = tel_obj.create_from_xml(cursor, uid, xml.cliente.telefonos)
            vals.update({
                'hi_ha_client': True,
                'tipus_document': xml.cliente.tipo_identificador,
                'codi_document': xml.cliente.identificador,
                'telefons': [(6, 0, tels)],
                'ind_direccio_fiscal': xml.cliente.indicador_tipo_direccion,
            })
            if xml.cliente.tipo_persona == 'J':
                vals.update({
                    'persona': 'J',
                    'nom': xml.cliente.razon_social,
                })
            else:
                vals.update({
                    'persona': 'F',
                    'nom': xml.cliente.nombre_de_pila,
                    'cognom_1': xml.cliente.primer_apellido,
                    'cognom_2': xml.cliente.segundo_apellido,
                })

            # Search for address in cliente
            if xml.cliente.direccion:
                direccio = xml.cliente.direccion
                ctx = context.copy()
                ctx['cliente'] = xml.cliente
                addr_id = sw_obj.search_address(cursor, uid, sw_id, direccio, context=ctx)
                vals.update({'fiscal_address_id': addr_id})

        # Registro Documents
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(
            cursor, uid, sw_id, self._nom_pas, pas_id, context=context
        )
        return pas_id

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id, context)
        tel_obj = self.pool.get("giscedata.switching.telefon")
        msg = r1.MensajeEnvioInformacionReclamacion()

        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)

        # Enviament Informacio Reclamacio
        info_reclamacio = r1.EnvioInformacionReclamacion()

        # Dades Enviament Informaci
        dades = r1.DatosEnvioInformacion()
        dades.feed({
            'num_expediente_acometida': pas.num_expedient_escomesa,
            'fecha_informacion': pas.data_informacio,
        })
        info_reclamacio.feed({'datos_envio_informacion': dades})

        # Variables Aportacio Informacio
        if pas.hi_ha_var_info:
            var_info = pas.header_id.generar_xml_var_aport_info(
                pas, context=context
            )
            info_reclamacio.feed({'variables_aportacion_informacion': var_info})

        # VariablesAportacionInformacionParaRetipificacion
        if pas.hi_ha_var_info_retip:
            context.update({'var_retip': True})
            var_info = pas.header_id.generar_xml_reclamacio(
                pas, context=context
            )
            info_reclamacio.feed({'variables_aportacion_informacion_para_retipificacion': var_info})

        if pas.hi_ha_client:
            idclient = r1.IdCliente()
            idclient.feed({
                'tipo_identificador': pas.tipus_document,
                'identificador': pas.codi_document,
                'tipo_persona': pas.persona,
            })
            nomclient = r1.Nombre()
            if pas.persona == 'J':
                nom = {'razon_social': pas.nom}
            else:
                nom = {'nombre_de_pila': pas.nom,
                       'primer_apellido': pas.cognom_1,
                       'segundo_apellido': pas.cognom_2}
            nomclient.feed(nom)
            client = r1.Cliente()
            cli_fields = {
                'id_cliente': idclient,
                'nombre': nomclient,
                'indicador_tipo_direccion': pas.ind_direccio_fiscal,
            }
            if pas.ind_direccio_fiscal == 'F':
                address = pas.fiscal_address_id
                dir_fiscal = r1.Direccion()
                addres_info = get_address_dicct(address)
                via, apartado = False, False
                if addres_info['apartado_de_correos']:
                    apartado = addres_info['apartado_de_correos']
                else:
                    via = r1.Via()
                    via_vals = {
                        'calle': addres_info['calle'],
                        'numero_finca': addres_info['numfinca'],
                    }
                    if address.aclarador:
                        via_vals.update({'tipo_aclarador_finca': 'NO',
                                         'aclarador_finca': address.aclarador[:40]})
                    via.feed(via_vals)
                dir_vals = {'pais': addres_info['pais'],
                            'provincia': addres_info['provincia'],
                            'municipio': addres_info['municipio'],
                            'poblacion': addres_info['poblacion'],
                            'cod_postal': addres_info['codpostal'],
                            'via': via,
                            'apartado_de_correos': apartado
                            }
                dir_fiscal.feed(dir_vals)
                cli_fields.update({'direccion': dir_fiscal})
            if pas.telefons:
                cli_fields.update(
                    {'telefonos': tel_obj.generar_xml(cursor, uid, pas.telefons)}
                )
            client.feed(cli_fields)
            info_reclamacio.feed({'cliente': client})

        # Reg. Doc.
        docs_info = pas.header_id.generar_xml_document(pas, context=context)
        info_reclamacio.feed({'registros_documento': docs_info})

        # Comentarios
        info_reclamacio.feed({'comentarios': pas.comentaris})

        msg.feed({
            'cabecera_reclamacion': capcalera,
            'envio_informacion_reclamacion': info_reclamacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = pas.sw_id.generar_nom_xml(pas.emisor_id.ref,
                                          pas.receptor_id.ref,
                                          pas=pas._nom_pas)
        return fname, str(msg)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        return self.create(cursor, uid, vals, context=context)

    def onchange_persona(self, cr, uid, ids, pers, cog1, cog2, context=None):
        if not context:
            context = {}

        if pers == 'J':
            data = {
                'tipus_document': 'NI',
                'cognom_1': '',
                'cognom_2': '',
            }
        else:
            data = {
                'tipus_document': 'NI',
            }

        return {'value': data}

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_R1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Datos Envio Informacion
        'num_expedient_escomesa': fields.char(
            u'Núm. Expedient Escomesa', size=20
        ),
        'data_informacio': fields.date("Data", required=True),

        # Client
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2),
        'codi_document': fields.char(u'Codi document', size=11),
        'persona': fields.selection(PERSONA, u'Persona'),
        'nom': fields.char(u'Nom de la persona o societat', size=45),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9),
        'ind_direccio_fiscal': fields.selection(TABLA_11,
                                                'Indicador direcció'),
        'fiscal_address_id': fields.many2one('res.partner.address',
                                             'Adreça fiscal'),

        # Comentaris
        'comentaris': fields.text(u"Comentaris", size=4000, required=True),

        'hi_ha_var_info': fields.boolean(u'Variables Aportacio informacio'),
        'hi_ha_var_info_retip': fields.boolean(u'Variables Aportacio informacio per Retipificació'),
        'hi_ha_client': fields.boolean(u'Client'),

    }

    _defaults = {
        'comentaris': lambda *a: '',
        'hi_ha_var_info': lambda *a: False,
        'hi_ha_var_info_retip': lambda *a: False,
        'hi_ha_client': lambda *a: False,
        'data_informacio': lambda *a: date.today(),
        'prefix': lambda *a: '34',
    }

GiscedataSwitchingR1_04()


class GiscedataSwitchingR1_05(osv.osv):
    """Classe pel pas 05
    """
    _name = "giscedata.switching.r1.05"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '05'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'R1', self._nom_pas, context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingR1_05,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id, context)
        msg = r1.MensajeCierreReclamacion()

        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)

        # Tancament
        tancament = r1.CierreReclamacion()

        # Datos Cierre
        dades = r1.DatosCierre()
        dades.feed({
            'num_expediente_acometida': pas.num_expedient_escomesa,
            'fecha': pas.data,
            'hora': pas.hora,
            'tipo': pas.tipus,
            'subtipo': pas.subtipus_id.name,
            'codigo_reclamacion_distribuidora': pas.codi_reclamacio_distri,
            'resultado_reclamacion': pas.resultat,
            'detalle_resultado': pas.detall_resultat,
            'observaciones': pas.observacions,
            'indemnizacion_abonada': pas.indemnitzacio_abonada,
            'num_expediente_anomalia_fraude': pas.num_expedient_anomalia_frau,
            'fecha_movimiento': pas.data_moviment,
        })
        tancament.feed({
            'datos_cierre': dades,
            'cod_contrato': pas.cod_contracte and filter(lambda x: x.isdigit(), pas.cod_contracte),
            'comentarios': pas.comentaris,
        })

        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            tancament.feed({'registros_documento': doc_xml})

        msg.feed({
            'cabecera_reclamacion': capcalera,
            'cierre_reclamacion': tancament,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = pas.sw_id.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=pas._nom_pas)
        return fname, str(msg)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML R1, pas 05
        """
        if not context:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        dades_tancament = xml.datos_cierre
        vals.update({
            'num_expedient_escomesa': dades_tancament.num_expediente_acometida,
            'data': dades_tancament.fecha,
            'hora': dades_tancament.hora,
            'tipus': dades_tancament.tipo,
            'subtipus_id': subtipus_obj.get_subtipus(cursor, uid, dades_tancament.subtipo),
            'codi_reclamacio_distri': dades_tancament.codigo_reclamacion_distribuidora,
            'resultat': dades_tancament.resultado_reclamacion,
            'detall_resultat': dades_tancament.detalle_resultado,
            'observacions': dades_tancament.observaciones,
            'indemnitzacio_abonada': dades_tancament.indemnizacion_abonada,
            'num_expedient_anomalia_frau':
                dades_tancament.num_expediente_anomalia_fraude,
            'data_moviment': dades_tancament.fecha_movimiento,
            # Codi contracte
            'cod_contracte': xml.cod_contrato,
            # comentaris
            'comentaris': xml.comentarios,
        })

        # Registro Documents
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        polissa = sw.cups_polissa_id

        header_id = header_obj.search(cursor, uid, [('sw_id', '=', sw_id)])[0]
        r101_obj = self.pool.get('giscedata.switching.r1.01')
        r101_id = r101_obj.search(cursor, uid, [('header_id', '=', header_id)])
        r101 = r101_obj.read(cursor, uid, r101_id[0], ['tipus', 'subtipus_id'])
        vals.update({
            'data': date.today(),
            'hora': datetime.today().strftime('%H:%M:%S'),
            'codi_reclamacio_distri': sw_id,
            'tipus': r101['tipus'],
            'subtipus_id': r101['subtipus_id'][0],
            'resultat': '',
            'data_moviment': datetime.today(),
            # Codi contracte
            'cod_contracte': polissa.name,
        })
        return self.create(cursor, uid, vals, context=context)

    def onchange_subtipus(self, cr, uid, ids, subtipus, context=None):
        if not context:
            context = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, subtipus, ['type'])
        data = {
            'tipus': subinfo['type']
        }
        return {'value': data}

    def _detalls_resultat(self, cursor, uid, context=None):
        """ Result details list"""
        detail_obj = self.pool.get('giscedata.switching.detalle.resultado')
        ids = detail_obj.search(cursor, uid, [])
        vals = detail_obj.read(cursor, uid, ids, ['name', 'text'])
        return [(v['name'], v['text']) for v in vals]

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_R1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(
            cursor, uid, step_id, ['sw_id', 'resultat', 'detall_resultat']
        )
        sw_id = step['sw_id'][0]
        step03 = self.pool.get('giscedata.switching.r1.03')
        step03_id = step03.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step03_id) > 0:
            antinf = step03.get_additional_info(cursor, uid, step03_id[0])
        else:
            step02 = self.pool.get('giscedata.switching.r1.02')
            step02_id = step02.search(cursor, uid, [('sw_id', '=', sw_id)])
            if len(step02_id) > 0:
                antinf = step02.get_additional_info(cursor, uid, step02_id[0])

        res_info = ""
        if step['resultat']:
            resultat_text = get_description(step['resultat'], 'TABLA_80')
            detall_text = step['detall_resultat']
            for line in self._detalls_resultat(cursor, uid, context=context):
                if line[0] == step['detall_resultat']:
                    detall_text = line[1]
            res_info = u" {0}: {1}".format(resultat_text, detall_text)
        return u"{0}{1}".format(antinf, res_info)

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Closing data
        'num_expedient_escomesa': fields.char(
            u'Número Expedient Escomesa', size=20
        ),
        'data': fields.date("Data", required=True),
        'hora': fields.time(u"Hora d'activació", required=True),
        'tipus': fields.selection(TABLA_81, u"Tipus", required=True),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipus",  domain="[('sector','in', ('t', 'e'))]",
                                       required=True),
        'codi_reclamacio_distri': fields.char(
            u"Codi reclamació distribuidora", size=26, required=True
        ),
        'resultat': fields.selection(
            TABLA_80, u"Resultat reclamació", required=True
        ),
        'detall_resultat': fields.selection(
            _detalls_resultat, u"Detall Resultat reclamació"
        ),
        'observacions': fields.text(u"Observacions", size=400),
        'indemnitzacio_abonada': fields.float(
            u"Indemnització Abonada", digits=(12, 2)
        ),
        'num_expedient_anomalia_frau': fields.char(
            u"Num Expedient Anomalia/Frau", size=20
        ),
        'data_moviment': fields.date(u"Data moviment"),

        # Codi contracte
        'cod_contracte': fields.char(u"Codi Contracte", size=12),

        # comentaris
        'comentaris': fields.text(u"Comentaris", size=4000)
    }

    _defaults = {
        'comentaris': lambda *a: '',
    }

GiscedataSwitchingR1_05()
