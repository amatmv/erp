# -*- coding: utf-8 -*-
from __future__ import absolute_import

import traceback

from osv import osv, fields, orm
from tools.translate import _
import netsvc

from giscedata_switching.giscedata_switching import SwitchingException


class GiscedataFacturacioFactura(osv.osv):
    """ Extend invoice as suport to switching cases"""

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def get_last_self_measure(self, cursor, uid, invoice_id, context=None):
        """ Returns self measures to object (R1) from invoice start date. We
        select only pool measures with retail origin:
            Manual (MA),
            Self-Measure (AL),
            Virtual Office (OV)
        :param invoice_id: invoice id
        :param from_date: Initial search date not include (until today)
        :param origins: origins dict by id
        :return: measures tuple of id lists ([energy], [maximeters]) with all
        available periods found for more recent measure group
        """
        if isinstance(invoice_id, (tuple, list)):
            invoice_id = invoice_id[0]

        energy_measures = []
        maximeter_measures = []

        orig_obj = self.pool.get('giscedata.lectures.origen_comer')
        measure_obj = self.pool.get('giscedata.lectures.lectura.pool')
        maximeter_obj = self.pool.get('giscedata.lectures.potencia.pool')
        if not orig_obj:
            return energy_measures, maximeter_measures

        orig_codes = ['AL', 'MA', 'OV']
        orig_ids = orig_obj.search(
            cursor, uid, [('codi', 'in', orig_codes)]
        )

        invoice_vals = self.read(
            cursor, uid, invoice_id, ['data_inici', 'comptadors']
        )

        # search origins
        comptadors = invoice_vals['comptadors']
        start_date = invoice_vals['data_inici']
        search_vals = [('comptador', 'in', comptadors),
                       ('origen_comer_id', 'in', orig_ids),
                       ('name', '>', start_date)]

        measures_ids = measure_obj.search(
            cursor, uid, search_vals, order='name DESC'
        )
        if measures_ids:
            # TODO get_last_origin_measure(
            #     invoice_id, from_date, retail_origins=None, from_pool=True
            # )
            measure_vals = measure_obj.read(
                cursor, uid, measures_ids[0], ['name'], context=context
            )
            date = measure_vals['name']
            search_vals.append(('name', '=', date))
            energy_measures = measure_obj.search(
                cursor, uid, search_vals, context=context
            )
            maximeter_measures = maximeter_obj.search(
                cursor, uid, search_vals, context=context
            )

        return energy_measures, maximeter_measures

    def get_r101_invoicing_info(self, cursor, uid, invoice_id, context=None):
        """
        Returns a dict to create R1-01 02 type steps from id. Measures are the
        selfmeasures returned by get_last_self_measure
        Useful in R1-01 creation and R1-01 from invoice Wizard
        :param invoice_id: objected invoice id
        :return: info dict:
            'objector': Who objects. Retailer (06) by default
            'invoice_number': Original invoice number
            'comments': comment 'Aportamos lectura de titular para '
                                'refacturación',
            'measure_date': date of measure,
            'dh_code': dh code of measure depending on fare,
            'measures': measures in format returned by get_measures_list
                        function
        }
        """
        swmeasures_obj = self.pool.get('giscedata.switching.lectura')
        inv_obj = self.pool.get('giscedata.facturacio.factura')

        invoice = inv_obj.browse(cursor, uid, invoice_id, context=context)

        # measures
        energy_ids, maximeters_ids = self.get_last_self_measure(
            cursor, uid, invoice_id, context=context
        )
        # creates a switching-friendly measures list
        # from measures ids
        sw_measures_tuple = swmeasures_obj.get_measures_list(
            cursor, uid, energy_ids, maximeters_ids, pool=True,
            context=context
        )

        dh_code = sw_measures_tuple[1]
        if not dh_code:
            dh_code = invoice.tarifa_acces_id.get_codi_dh()

        if context.get('proces', False) == 'R1-02009':
            # R1-02009
            comments = (
                u"Solicitamos revisión de la lectura facturada a partir "
                u"de histórico de lecturas y consiguiente refacturación."
            )
        else:
            # R1-02036
            comments = u"Aportamos lectura de titular para refacturación"

        # Specífic R1-01 02-36/09 process
        config_vals = {
            'objector': '06',  # retailer
            'invoice_number': invoice.reference,
            'invoice': invoice.id,
            'comments': comments,
            'measure_date': (
                sw_measures_tuple[0] and sw_measures_tuple[0].date()
            ),
            'dh_code': dh_code,
            'measures': sw_measures_tuple[2]
        }

        return config_vals

    def create_atr_case(self, cursor, uid, invoice_id, proces=None,
                        config_vals=None, context=None):
        """ creates ATR cases from invoices (R1-01 [02-009] and [02-036])
        :param invoice_id:
        :param extra_data:
        :return: tuple (action, message, case_id) where:
            action: '(proces_name) contract_name.invoice_number'
            message: Textual result of case creation
            case_id: Created case or None otherwise
        """
        if context is None:
            context = {}

        # Do this due the browse record object always pass the id as a list
        # of one element.
        if isinstance(invoice_id, (list, tuple)):
            invoice_id = invoice_id[0]

        inv_obj = self.pool.get('giscedata.facturacio.factura')
        contract_obj = self.pool.get('giscedata.polissa')
        sw_obj = self.pool.get('giscedata.switching')
        swproc_obj = self.pool.get('giscedata.switching.proces')
        swpas_obj = self.pool.get('giscedata.switching.step')
        swinfo_obj = self.pool.get('giscedata.switching.step.info')
        swmeasures_obj = self.pool.get('giscedata.switching.lectura')

        supported_cases = ['R1-02009', 'R1-02036']

        proces = context.get('proces', proces or 'R1-02036')
        proces_name = proces

        invoice = inv_obj.browse(cursor, uid, invoice_id, context=context)

        def formatResult(message, case_id=None):
            logger = netsvc.Logger()
            action = '({0}) {1}.{2}'.format(
                proces_name,
                invoice.polissa_id.name,
                invoice.reference or invoice.number
            )
            logger.notifyChannel('switching', netsvc.LOG_DEBUG,
                                 '{0}: {1}'.format(action, message))
            return [action, message, case_id]

        if proces_name not in supported_cases:
            return formatResult(_(u'Cas {0} no suportat').format(proces_name))

        proces_basic_name = proces_name.split('-')[0]

        proces_id = swproc_obj.search(cursor, uid,
                                      [('name', '=', proces_basic_name)])[0]

        # test for ONLY DSO invoice
        if invoice.type not in ["in_invoice", "in_refund"]:
            return formatResult(
                _(u'Factura ha de ser de distribuidor ({0}), '
                  u'la saltem').format(invoice.type)
            )

        contract = invoice.polissa_id

        # Partner address and partner
        sw_params = {
            'proces_id': proces_id,
            'cups_polissa_id': contract.id
        }
        vals = sw_obj.onchange_polissa_id(
            cursor, uid, [], contract.id, None, proces_id=proces_id,
            context=context
        )

        if vals['warning']:
            # Problem searching contract/CUPS
            warn_txt = '{0}: {1}'.format(
                vals['warning']['title'],
                vals['warning']['message'],
            )
            return formatResult(warn_txt)

        sw_params.update(vals['value'])
        # Fake contract when not found
        if not sw_params['ref_contracte']:
            sw_params['ref_contracte'] = '111111111'

        # comprovem que hi hagi CUPS
        if not contract.cups:
            return formatResult(
                _(u"ERROR: Pólissa no te CUPS, no es pot fer cas ATR"))

        # CUPS standarization
        result = contract_obj.arregla_cups(
            cursor, uid, contract.id, context=context
        )
        if result:
            # warning: The CUPS has been modified
            formatResult(result)

        sw_id = None
        pas_id = None
        info_id = None
        try:
            # Case Creation
            sw_id = sw_obj.create(cursor, uid, sw_params)

            # Step Creation
            pas_id = swpas_obj.get_step(cursor, uid, '01', proces_name)
            # Info creates step info automatically
            info_vals = {
                'sw_id': sw_id,
                'proces_id': proces_id,
                'step_id': pas_id,
            }
            info_id = swinfo_obj.create(cursor, uid, info_vals)
            info = swinfo_obj.browse(cursor, uid, info_id)
            model_obj, model_id = info.pas_id.split(',')
            pas_obj = self.pool.get(model_obj)
            pas = pas_obj.browse(cursor, uid, int(model_id))

            warn_txt = ''

            if proces in ['R1-02009', 'R1-02036']:
                if not config_vals:
                    # Default vals when no form
                    config_vals = self.get_r101_invoicing_info(
                        cursor, uid, invoice_id, context=context
                    )
                config_vals.update({
                    'type': proces[3:5],
                    'subtype': proces[5:8],
                })

                pas_obj.config_step(
                    cursor, uid, pas.id, config_vals, context=context
                )
            if not config_vals['measures'] and proces == 'R1-02036':
                warn_txt += _("WARN! No s'han trobat lectures")

            message = _(u'{0}. Cas {1} creat correctament. ').format(
                warn_txt, sw_id
            )
            if sw_id and context.get("ref_id") and context.get("ref_model"):
                ref_model = self.pool.get(context["ref_model"])
                ref_model.write(
                    cursor, uid, context["ref_id"],
                    {"ref": "giscedata.switching, {}".format(sw_id)}
                )
                sw_obj.write(
                    cursor, uid, sw_id,
                    {"ref": "{0}, {1}".format(context["ref_model"], context["ref_id"])}
                )
            result = formatResult(message, sw_id)
            sw_obj.case_open(cursor, uid, sw_id)
            sw_r1_01_obj = self.pool.get('giscedata.switching.r1.01')
            sw_r1_01_obj.update_contract(cursor, uid, pas.id, context=context)
            return result

        except SwitchingException, e:
                return formatResult(
                    _(u"ERROR: No s'ha creat el Cas per problemes amb la "
                      u"configuració del Pas 01: "
                      u"{0} -> {1}. ").format(e, e.error_fields),
                    sw_id)
        except Exception, e:
            traceback.print_stack()
            if not sw_id or not pas_id or not info_id:
                return formatResult(_(u"ERROR: No s'ha creat Cas: "
                                      u"{0}").format(e,), sw_id)
            return formatResult(_(u'ERROR Creant el Cas: '
                                  u'({0}). ').format(e,), sw_id)

GiscedataFacturacioFactura()
