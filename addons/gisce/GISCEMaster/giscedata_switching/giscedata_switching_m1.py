# -*- coding: utf-8 -*-
from __future__ import absolute_import

from enerdata.contracts.tariff import (
    get_tariff_by_code, NotPositivePower, IncorrectPowerNumber,
    IncorrectMaxPower, NotAscendingPowers, NotNormalizedPower, IncorrectMinPower
)
from osv import osv, fields, orm
from gestionatr.output.messages import sw_m1 as m1

from tools.translate import _
from gestionatr.defs import *
from .utils import add_months, get_address_dicct, check_contracte_pas_is_eventual, check_contracte_is_eventual
from datetime import datetime, timedelta
import calendar
import pooler
from .giscedata_switching import SwitchingException

from .giscedata_switching_helpers import get_facturacio_potencia
import vatnumber
from giscedata_switching.giscedata_switching_validacions_lectures import ValidadorLecturesATR
from gestionatr.utils import get_description


class GiscedataSwitchingProcesM1(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        '''returns M1 inital steps depending on where we are'''
        if proces == 'M1':
            if where == 'distri':
                return ['01']
            elif where == 'comer':
                return ['01']

        return super(GiscedataSwitchingProcesM1,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        '''returns M1 emisor steps depending on where we are'''
        if proces == 'M1':
            if where == 'distri':
                return ['02', '03', '04', '05', '07']
            elif where == 'comer':
                return ['01', '06']

        return super(GiscedataSwitchingProcesM1,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == 'M1':
            return ['02', '04', '07']

        return super(GiscedataSwitchingProcesM1,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

GiscedataSwitchingProcesM1()


class GiscedataSwitchingM1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == 'M1':
            # Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                    else:
                        m101_obj = self.pool.get("giscedata.switching.m1.01")
                        m101_id = m101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])
                        if not len(m101_id):
                            continue
                        pas01 = m101_obj.browse(cursor, uid, m101_id[0])
                        if pas.actuacio_camp == 'N' and pas01.sollicitudadm == 'S' and pas01.canvi_titular != 'T':
                            return True
                elif step_name == '07':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if not pas.rebuig:
                        return True
                elif step_name in ('04', '05'):
                    return True

        return super(GiscedataSwitchingM1,
                     self).get_final(cursor, uid, sw, context=context)


GiscedataSwitchingM1()


class GiscedataSwitchingM1_01(osv.osv):
    """Classe pel pas 01
    """
    _name = "giscedata.switching.m1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'M1', self._nom_pas, context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['02', '06']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_01, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def onchange_tarifa(self, cr, uid, ids, tarifa, pot_ids, context=None):
        """Valida les potències segons tarifa escollida"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if not pot_ids or not tarifa:
            return res
        tar_obj = self.pool.get('giscedata.polissa.tarifa')
        tarifa_name = dict(TABLA_17)[tarifa]
        tarifa_ids = tar_obj.search(cr, uid, [('name', '=', tarifa_name)])
        if not tarifa_ids:
            return res
        tarifa = tar_obj.browse(cr, uid, tarifa_ids[0])

        potencies = [p[2]['potencia'] for p in pot_ids]

        for p in potencies:
            if p > tarifa.pot_max * 1000 or p <= tarifa.pot_min * 1000:
                res['warning'] = {'title': _(u'Potències fora de límits'),
                                  'message': _(u'Repassa que la potència de '
                                               u'cada període està dins els '
                                               u'límits de la Tarifa. El botó '
                                               u'"Omple Períodes" assignarà '
                                               u'potències dins els marges.')}
                break
        return res

    def onchange_persona(self, cr, uid, ids, pers, cog1,
                         cog2, client, context=None):
        if not context:
            context = {}
        data = {}
        if pers == 'J':
            data = {'cognom_1': '', 'cognom_2': ''}

        return {'value': data}

    def check_cognom(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context=context)
        if pas.persona == 'F' and not pas.cognom_1:
            raise osv.except_osv('Error',
                                 _(u"El camp cognom és obligatori "
                                   u"per persones físiques"))
        return True

    def recalcula_periodes(self, cursor, uid, ids, context=None):
        """ Recalcula els períodes si canviem de tarifa """
        tar_obj = self.pool.get('giscedata.polissa.tarifa')
        swhead_obj = self.pool.get('giscedata.switching.step.header')

        sw_step = self.browse(cursor, uid, ids[0])
        tarifa_name = dict(TABLA_17)[sw_step.tarifaATR]
        tarifa_ids = tar_obj.search(cursor, uid, [('name', '=', tarifa_name)])
        if not tarifa_ids or len(tarifa_ids) > 1:
            raise osv.except_osv('Error',
                                 _(u"No s'ha trobat la tarifa"))
        tarifa = tar_obj.browse(cursor, uid, tarifa_ids[0])

        per_prod = [p.name for p in tarifa.periodes
                    if not p.agrupat_amb and p.tipus == 'tp']
        periodes = sorted(per_prod)
        # Agafem la potència del periode o si no el de la pólissa
        # Es és més gran o més petita que la permesa, posa la del límit que
        # toqui
        per_pot = []
        pots = sw_step.pot_ids
        for i in xrange(0, len(periodes)):
            pot = (pots and len(pots) >= i+1 and pots[i].potencia
                   or ((sw_step.sw_id.cups_polissa_id.potencia * 1000) + 1))
            # validem els límits
            pot = (pot > tarifa.pot_max * 1000 and tarifa.pot_max * 1000
                   or pot <= tarifa.pot_min * 1000 and tarifa.pot_min * 1000
                   or pot)
            per_pot.append((periodes[i], pot))

        created_pots = swhead_obj.create_pots(cursor, uid, per_pot,
                                              context=context)
        return sw_step.write({'pot_ids': [(6, 0, created_pots)]})

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML M1, pas 01
        """

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        tel_obj = self.pool.get("giscedata.switching.telefon")
        self.check_cognom(cursor, uid, pas_id, context=context)
        pas = self.browse(cursor, uid, pas_id, context)

        sw = pas.sw_id
        msg = m1.MensajeModificacionDeATR()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # sol·licitud
        sollicitud = m1.DatosSolicitud()
        sol_fields = {
            'tipo_modificacion': pas.sollicitudadm,
            'ind_activacion': pas.activacio_cicle,
            'cnae': pas.cnae and pas.cnae.name or False,
            'bono_social': pas.bono_social,
            'solicitud_tension': pas.solicitud_tensio
        }
        # Només s'envía tipo_cambio quan solicitudadm és Subrogació (S) o
        # Canvi de dades Administratives (A)
        if pas.sollicitudadm in ['S', 'A']:
            sol_fields.update(
                {'tipo_solicitud_administrativa': pas.canvi_titular}
            )
            # Only sends periodicidad_facturacion when canvi_titular is (P)
            if pas.canvi_titular in ['P']:
                sol_fields.update(
                    {'periodicidad_facturacion': pas.periodicitat_facturacio}
                )
        if pas.activacio_cicle == 'F':
            sol_fields.update({'fecha_prevista_accion': pas.data_accio})
        sollicitud.feed(sol_fields)

        # contracte
        # Condicions contractuals
        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = m1.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'modo_control_potencia': pas.control_potencia,
            'potencias_contratadas': potencies,
        })

        # Contacte
        contacte = m1.Contacto()
        contacte.feed({
            'persona_de_contacto': pas.cont_nom,
            'telefonos': tel_obj.generar_xml(cursor, uid, pas.cont_telefons),
        })

        contracte = m1.Contrato()
        ctr_fields = {
            'condiciones_contractuales': condicions,
            'contacto': contacte,
            'tipo_autoconsumo': pas.tipus_autoconsum,
            'tipo_contrato_atr': pas.tipus_contracte,
        }
        if pas.tipus_contracte == '09':
            ctr_fields['consumo_anual_estimado'] = pas.expected_consumption

        if pas.data_final and check_contracte_pas_is_eventual(pas):
            ctr_fields.update({'fecha_finalizacion': pas.data_final})
        contracte.feed(ctr_fields)

        # client
        idclient = m1.IdCliente()
        idclient.feed({
            'tipo_identificador': pas.tipus_document,
            'identificador': pas.codi_document,
            'tipo_persona': pas.persona,
        })
        nomclient = m1.Nombre()
        if pas.persona == 'J':
            nom = {'razon_social': pas.nom}
        else:
            nom = {'nombre_de_pila': pas.nom,
                   'primer_apellido': pas.cognom_1,
                   'segundo_apellido': pas.cognom_2}
        nomclient.feed(nom)
        client = m1.Cliente()
        cli_fields = {
            'id_cliente': idclient,
            'nombre': nomclient,
            'indicador_tipo_direccion': pas.ind_direccio_fiscal,
        }
        if pas.ind_direccio_fiscal == 'F':
            address = pas.fiscal_address_id
            dir_fiscal = m1.Direccion()
            addres_info = get_address_dicct(address)
            via, apartado = False, False
            if addres_info['apartado_de_correos']:
                apartado = addres_info['apartado_de_correos']
            else:
                via = m1.Via()
                via_vals = {
                    'calle': addres_info['calle'],
                    'numero_finca': addres_info['numfinca'],
                }
                if address.aclarador:
                    via_vals.update({'tipo_aclarador_finca': 'NO',
                                     'aclarador_finca': address.aclarador[:40]})
                via.feed(via_vals)
            dir_vals = {'pais': addres_info['pais'],
                        'provincia': addres_info['provincia'],
                        'municipio': addres_info['municipio'],
                        'poblacion': addres_info['poblacion'],
                        'cod_postal': addres_info['codpostal'],
                        'via': via,
                        'apartado_de_correos': apartado
                        }
            dir_fiscal.feed(dir_vals)
            cli_fields.update({'direccion': dir_fiscal})
        if pas.telefons:
            cli_fields.update(
                {'telefonos': tel_obj.generar_xml(cursor, uid, pas.telefons)}
            )
        client.feed(cli_fields)

        # mesura
        mesura = m1.Medida()
        mesura.feed({
            'propiedad_equipo': pas.equip_aportat_client,
            'tipo_equipo_medida': pas.tipus_equip_mesura,
        })

        # sol·licitud de modificacions
        mod = m1.ModificacionDeATR()
        mod_vals = {
            'comentarios': pas.comentaris or False,
            'datos_solicitud': sollicitud,
            'contrato': contracte,
            'cliente': client,
            'medida': mesura,
        }

        # Doc Tecnica
        doctecnica = m1.DocTecnica()
        # Dades CIE si procedeix
        if pas.dt_add:
            dades_cie = m1.DatosCie()
            dades_cie.feed({'cie_electronico': pas.dt_cie_electronico})

            if pas.dt_cie_electronico == 'S':
                cie_elec = m1.CIEElectronico()
                cie_elec.feed({
                    'codigo_instalador': pas.dt_codi_instalador,
                    'sello_electronico': pas.dt_cie_sello_elec
                })
                dades_cie.feed({'cie_electronico': cie_elec})
            else:
                cie_paper = m1.CIEPapel()
                cie_paper.feed({
                    'codigo_cie': pas.dt_cie_codigo,
                    'potencia_inst_bt': pas.dt_cie_papel_potenciainstbt,
                    'fecha_emision_cie': pas.dt_cie_papel_data_emissio,
                    'fecha_caducidad_cie': pas.dt_cie_papel_data_caducitat,
                    'tension_suministro_cie': pas.dt_cie_papel_tensio_suministre,
                    'tipo_suministro': pas.dt_cie_papel_tipus_suministre,
                })

                if pas.dt_tipus_codi_instalador == 'nif':
                    vals = {'nif_instalador': pas.dt_codi_instalador}
                else:
                    vals = {'codigo_instalador': pas.dt_codi_instalador}
                cie_paper.feed(vals)
                dades_cie.feed({'cie_papel': cie_paper})
            doctecnica.feed({'datos_cie': dades_cie})

        # Dades APM si procedeix
        if pas.dt_add_apm:
            dades_apm = m1.DatosAPM()
            dades_apm.feed({
                'codigo_apm': pas.dt_apm_codigo,
                'potencia_inst_at': pas.dt_apm_potenciainstat,
                'fecha_emision_apm': pas.dt_apm_data_emissio,
                'fecha_caducidad_apm': pas.dt_apm_data_caducitat,
                'tension_suministro_apm': pas.dt_apm_tensio_suministre,
            })
            if pas.dt_apm_tipus_codi_instalador == 'nif':
                vals = {'nif_instalador': pas.dt_apm_codi_instalador}
            else:
                vals = {'codigo_instalador': pas.dt_apm_codi_instalador}
            dades_apm.feed(vals)
            doctecnica.feed({'datos_apm': dades_apm})

        if pas.dt_add_apm or pas.dt_add:
            mod_vals.update({'doc_tecnica': doctecnica})

        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            mod_vals.update({'registros_documento': doc_xml})

        mod.feed(mod_vals)
        msg.feed({
            'cabecera': capcalera,
            'modificacion_de_atr': mod,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        tel_obj = self.pool.get("giscedata.switching.telefon")

        # Per cada potencia hem de crear el registre
        xml_pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, xml_pots,
                                              context=context)
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        tels = tel_obj.create_from_xml(cursor, uid, xml.cliente.telefonos)
        vals.update({
            # sol·licitud
            'activacio_cicle': xml.datos_solicitud.ind_activacion,
            'sollicitudadm': xml.datos_solicitud.tipo_modificacion,
            'periodicitat_facturacio': xml.datos_solicitud.periodicidad_facturacion,
            'canvi_titular': xml.datos_solicitud.tipo_solicitud_administrativa,
            'data_accio': xml.datos_solicitud.fecha_prevista_accion,
            'bono_social': xml.datos_solicitud.bono_social,
            'solicitud_tensio': xml.datos_solicitud.solicitud_tension,
            # contracte
            'tipus_autoconsum': xml.contrato.tipo_autoconsumo,
            'tipus_contracte': xml.contrato.tipo_contrato_atr,
            'data_final': xml.contrato.fecha_finalizacion,
            'control_potencia': xml.contrato.modo_control_potencia,
            'pot_ids': [(6, 0, created_pots)],
            # client
            'tipus_document': xml.cliente.tipo_identificador,
            'codi_document': xml.cliente.identificador,
            'persona': xml.cliente.tipo_persona,
            'telefons': [(6, 0, tels)],
            'ind_direccio_fiscal': xml.cliente.indicador_tipo_direccion,
            # Comentaris
            'comentaris': xml.comentarios,
        })

        if xml.contrato.tipo_contrato_atr == '09':
            vals['expected_consumption'] = xml.contrato.consumo_anual_estimado

        if xml.medida:
            vals.update({
                'equip_aportat_client': xml.medida.propiedad_equipo,
                'tipus_equip_mesura': xml.medida.tipo_equipo_medida,
            })

        # We can only store the "tarifa" if it's a correct one
        if xml.contrato.tarifa_atr in dict(TABLA_17):
            vals.update({'tarifaATR': xml.contrato.tarifa_atr})

        if xml.cliente.tipo_persona == 'F':
            name_as_dict = {
                "N": xml.cliente.nombre_de_pila,
                "C1": xml.cliente.primer_apellido,
                "C2": xml.cliente.segundo_apellido
            }
            vals.update({
                'nom': xml.cliente.nombre_de_pila,
                'cognom_1': xml.cliente.primer_apellido,
                'cognom_2': xml.cliente.segundo_apellido,
            })
            new_name = self.pool.get("res.partner").ajunta_cognoms(cursor, uid, name_as_dict, context=context)
        else:
            vals.update({
                'nom': xml.cliente.razon_social
            })
            new_name = xml.cliente.razon_social

        # Search for address in cliente
        sw_info = sw_obj.read(cursor, uid, sw_id, ['cups_id'])
        addr_id = False
        new_phones = []
        if xml.contrato.contacto:
            for tel in xml.contrato.contacto.telefonos:
                new_phones.append(tel[1])
        if xml.cliente:
            for tel in xml.cliente.telefonos:
                new_phones.append(tel[1])

        context.update({
            'new_phones': new_phones,
            'new_name': new_name
        })
        if xml.cliente.direccion:
            direccio = xml.cliente.direccion
            ctx = context.copy()
            ctx['cliente'] = xml.cliente
            addr_id = sw_obj.search_address(cursor, uid, sw_id, direccio, context=ctx)
            vals.update({'fiscal_address_id': addr_id})
        elif sw_info['cups_id']:
            # Use cups info
            addr_id = sw_obj.create_partner_addres_from_cups(
                cursor, uid, sw_info['cups_id'][0], context=context
            )
        vals.update({'fiscal_address_id': addr_id})

        # Contact
        if xml.contrato.contacto:
            con_tels = tel_obj.create_from_xml(cursor, uid,
                                               xml.contrato.contacto.telefonos)
            vals.update({
                'cont_nom': xml.contrato.contacto.persona_de_contacto,
                'cont_telefons': [(6, 0, con_tels)]
            })

        # Documentacio Tecnica
        if xml.doc_tecnica and xml.doc_tecnica.datos_cie:
            if xml.doc_tecnica.datos_cie.cie_papel:
                is_elec = 'N'
                datos_cie = xml.doc_tecnica.datos_cie.cie_papel
                if datos_cie.codigo_instalador:
                    codi_tipusInstalador = datos_cie.codigo_instalador
                    tipusInstalador = 'codigo'
                else:
                    codi_tipusInstalador = datos_cie.nif_instalador
                    tipusInstalador = 'nif'

                vals.update({
                    'dt_add': True,
                    'dt_cie_electronico': is_elec,
                    'dt_cie_codigo': datos_cie.codigo_cie,
                    'dt_cie_papel_potenciainstbt': datos_cie.potencia_inst_bt,
                    'dt_cie_papel_data_emissio': datos_cie.fecha_emision_cie,
                    'dt_cie_papel_data_caducitat':
                        datos_cie.fecha_caducidad_cie,
                    'dt_tipus_codi_instalador': tipusInstalador,
                    'dt_codi_instalador': codi_tipusInstalador,
                    'dt_cie_papel_tensio_suministre':
                        datos_cie.tension_suministro_cie,
                    'dt_cie_papel_tipus_suministre': datos_cie.tipo_suministro,
                })
            else:
                is_elec = 'S'
                datos_cie = xml.doc_tecnica.datos_cie.cie_electronico

                vals.update({
                    'dt_add': True,
                    'dt_cie_electronico': is_elec,
                    'dt_cie_codigo': datos_cie.codigo_cie,
                    'dt_cie_sello_elec': datos_cie.sello_electronico,
                })
        if xml.doc_tecnica and xml.doc_tecnica.datos_apm:
            datos_apm = xml.doc_tecnica.datos_apm
            if datos_apm.codigo_instalador:
                codi_tipusInstalador = datos_apm.codigo_instalador
                tipusInstalador = 'codigo'
            else:
                codi_tipusInstalador = datos_apm.nif_instalador
                tipusInstalador = 'nif'

            vals.update({
                'dt_add_apm': True,
                'dt_apm_codigo': datos_apm.codigo_apm,
                'dt_apm_potenciainstat': datos_apm.potencia_inst_at,
                'dt_apm_data_emissio': datos_apm.fecha_emision_apm,
                'dt_apm_data_caducitat': datos_apm.fecha_caducidad_apm,
                'dt_apm_tipus_codi_instalador': tipusInstalador,
                'dt_apm_codi_instalador': codi_tipusInstalador,
                'dt_apm_tensio_suministre': datos_apm.tension_suministro_apm,
            })

        # Registro Documents
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        # Search for cnae
        if xml.datos_solicitud.cnae:
            cnae_id = sw_obj.search_cnae(cursor, uid,
                                         xml.datos_solicitud.cnae,
                                         context=context)
            vals.update({'cnae': cnae_id})

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = ['change_type', 'phone_num', 'con_name', 'tariff',
                            'power_p1', 'power_invoicing']

        power_check_fields = ['power_p1']

        tariff = vals.get('tariff', False)

        if tariff in TARIFES_SEMPRE_MAX:
            mandatory_fields.extend(['power_p2', 'power_p3'])
            power_check_fields.extend(['power_p2', 'power_p3'])
            if vals.get('power_invoicing', '1') != '2':
                raise SwitchingException(
                    _(u"Les tarifes de més de 15kW SEMPRE es facturen amb "
                      u"maxímetre")
                )

        if tariff in TARIFES_6_PERIODES:
            mandatory_fields.extend(['power_p4', 'power_p5', 'power_p6'])
            power_check_fields.extend(['power_p4', 'power_p5', 'power_p6'])

        for field in mandatory_fields:
            if not vals.get(field, False):
                raise SwitchingException(_("Falten dades. Camp no omplert."),
                                         [field])

        if vals['change_type'] not in dict(SEL_CONFIG_MODCON_WIZ_TYPE).keys():
                raise SwitchingException(_("Valor incorrecte"),
                                         ['change_type'])

        # The tariff and power
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')

        if vals.get("new_contract_to_link"):
            pol_id = vals.get("new_contract_to_link")
            pol_info = self.pool.get("giscedata.polissa").read(cursor, uid, pol_id, ['lectura_en_baja', 'tarifa'])
            if pol_info['tarifa']:
                tarifa_info = self.pool.get("giscedata.polissa.tarifa").read(cursor, uid, pol_info['tarifa'][0], ['mesura_ab'])
                tarifa_lb = tarifa_info['mesura_ab'] == 'B'
            else:
                tarifa_lb = False
            lb = pol_info['lectura_en_baja'] or tarifa_lb
        else:
            lb = False

        tariff_id = tariff_obj.get_tarifa_from_ocsum(cursor, uid, tariff, tarifa_lb=lb)
        retail_tariff = vals.get('retail_tariff', False)
        if tariff_id:
            tariff_values = tariff_obj.read(
                cursor, uid, tariff_id, ['name', 'llistes_preus_comptatibles'],
                context=context
            )
            tariff_name = tariff_values['name']
            compatible_fares = tariff_values.get(
                'llistes_preus_comptatibles', [])

            try:
                powers = [vals[power] / 1000.0 for power in
                          sorted(power_check_fields)]
                get_tariff_by_code(tariff_name)().evaluate_powers(powers)
            except NotPositivePower as e:
                raise SwitchingException(
                    _(u'Una de les potències és igual o inferior a 0.'),
                    power_check_fields
                )
            except IncorrectPowerNumber:
                raise SwitchingException(
                    _(u'El nombre de potències donades és incorrecte.'),
                    power_check_fields
                )
            except IncorrectMaxPower:
                raise SwitchingException(
                    _(u'El valor de potència màxim no està entre '
                      u'els límits esperats.'),
                    power_check_fields
                )
            except NotAscendingPowers:
                raise SwitchingException(
                    _(u'Els valors de potència no estan en ordre ascendent.'),
                    power_check_fields
                )
            except NotNormalizedPower:
                # We allow the creation of switching with not normalized powers
                pass
            except IncorrectMinPower:
                pass

            if retail_tariff:
                # Checks is retail_tariff is compatible with atr fare
                if retail_tariff not in compatible_fares:
                    raise SwitchingException(
                        _(u"Tarifa de comercialitzadora incompatible amb la "
                          u"tarifa ATR escollida"), ['retail_tariff']
                    )

        # Comment length 4000 chars maximum
        comments = vals.get('comments', '')
        if comments and len(comments) > 4000:
            raise SwitchingException(
                _("Comentaris de 4000 caràcters com a màxim"), ['comments']
            )

        if vals.get("lectures_vals"):
            self.validar_lectures_for_m1(cursor, uid, vals.get("lectures_vals"), context=context)

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        Changes step vals accordingly with vals dict:
            'change_type':
                'tarpot': 'Tarifa/potència'
                'owner': 'Titular'
                'both': 'Ambdós'
            'con_name': Contact name
            'con_sur1': Contact first surname
            'con_sur2': Contact second (antisemitic) surname,
            'phone_pre' & 'phone_num': Contact phone info
            'tariff': New tariff
            'retail_tariff': Retail Tariff of new mod con
            'power_p1' to 'power_p3': New contract Power demanded
            'comments': Comment field
            'change_type': owner change type
            'new_contract_values':  valors del nou contracte a crear. Si son
                                    informats es duplicarà el contracte actual i
                                    es farà el write al nou contracte amb el
                                    diccionari new_contract_values
            'lectures_vals':    valors per crear i introduir noves lectures al
                                pool del contracte actual. Es un diccionari en
                                que ha d'haver-hi el següent:
                                    - measure_date: data de les lectures
                                    - num_periods: numero de periodes
                                    - contract: id del contracte on
                                                s'introdueixen les lectures.
                                    - send_T_measures:  on T pot ser ['ae', 'r1', 'pm', 'ep'].
                                                        Indica si s'han de entrar lectures del tipus T.
                                    - measure_T_pN: on T pot ser ['ae', 'r1', 'pm', 'ep'] i N ['1', '2', '3', '4', '5', '6'].
                                                    Valor de la lectura de tipus T per el periode N en data measure_date.
                                Aquests valors de lectures passaran per una
                                serie de valodacions que comproven que tinguin
                                tots els periodes necessaris i que no produeixin
                                voltes de contadors. Si no es passen,
                                no es crea el cas ATR.
        """
        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        self.config_step_validation(cursor, uid, [], vals, context=context)

        pas = self.browse(cursor, uid, ids, context=context)

        change_type = vals['change_type']

        new_vals = {}
        if vals.get('activacio_cicle'):
            new_vals['activacio_cicle'] = vals.get('activacio_cicle')

        if vals.get('data_accio'):
            new_vals['data_accio'] = vals.get('data_accio')

        # Comment
        new_vals.update({'comentaris': vals.get("comments", '')})

        if vals.get('docs_ids', False):
            docs_to_set = vals.get('docs_ids', False)
            if docs_to_set:
                header_id = pas.header_id.id
                for doc_id in docs_to_set:
                    cursor.execute(
                        "INSERT INTO sw_step_header_doc_ref "
                        "(header_id, document_id) "
                        "VALUES (%s, %s)",
                        (header_id, doc_id)
                    )

        if vals.get('dt_add', False):
            doc_tecnica_dict = {
                'dt_add': vals.get('dt_add'),
                'dt_cie_electronico': vals.get('dt_cie_electronico'),
                'dt_cie_codigo': vals.get('dt_cie_codigo'),
                'dt_cie_papel_potenciainstbt': vals.get('dt_cie_papel_potenciainstbt'),
                'dt_cie_papel_data_emissio': vals.get('dt_cie_papel_data_emissio'),
                'dt_cie_papel_data_caducitat': vals.get('dt_cie_papel_data_caducitat'),
                'dt_tipus_codi_instalador': vals.get('dt_tipus_codi_instalador'),
                'dt_codi_instalador': vals.get('dt_codi_instalador'),
                'dt_cie_papel_tensio_suministre': vals.get('dt_cie_papel_tensio_suministre'),
                'dt_cie_papel_tipus_suministre': vals.get('dt_cie_papel_tipus_suministre'),
                'dt_cie_sello_elec': vals.get('dt_cie_sello_elec')
            }
            new_vals.update(doc_tecnica_dict)
        if vals.get('solicitud_tensio', False):
            new_vals.update({
                'solicitud_tensio': vals['solicitud_tensio']
            })
        if vals.get('mod_autoconsum', False):
            new_vals.update({
                'tipus_autoconsum': vals['autoconsum']
            })

        if change_type == 'owner' or change_type == 'both':
            # OWNER CHANGE
            new_vals.update({
                'sollicitudadm': 'S',
                'canvi_titular': vals['owner_change_type'],
            })
            owner_id = vals['owner']

            # Fills new owner fields
            ochcs_res = self.onchange_client_seleccionat(
                cursor, uid, [], owner_id, context=context
            )
            if ochcs_res.get('value', False):
                new_vals.update(ochcs_res['value'])
                new_vals.update({'dades_client': owner_id})
                owner_address = self.pool.get('res.partner').read(cursor, uid, owner_id, ['address'])['address']
                if len(owner_address):
                    new_vals.update({
                        'fiscal_address_id': owner_address[0]
                    })
            if ochcs_res.get('warning'):
                new_vals.update({'validacio_pendent': True})

            # Overwrite vat_kind with user defined one
            new_vals.update({'tipus_document': vals['vat_kind']})

        if change_type == 'tarpot' or change_type == 'both':
            # TARIFF / POWER CHANGE
            new_vals.update({
                'tarifaATR': vals['tariff'],
                'sollicitudadm': 'N',
                'control_potencia': vals['power_invoicing'],
            })

        if change_type == 'both':
            # We overwrite the change type to 'both'
            new_vals.update({'sollicitudadm': 'A'})

        nom = "{0} {1} {2}".format(
            vals['con_name'], vals['con_sur1'], vals['con_sur2']
        )
        tel_obj = self.pool.get("giscedata.switching.telefon")
        tel_id = tel_obj.create(
            cursor, uid,
            {'numero': vals['phone_num'], 'prefix': vals['phone_pre']}
        )
        new_vals.update({
            'cont_nom': nom,
            'cont_telefons': [(6, 0, [tel_id])],
        })
        if not pas.telefons or (pas.telefons and not pas.telefons[0].numero):
            new_vals.update({
                'telefons': [(6, 0, [tel_id])],
            })

        new_vals.update({
            'cnae': vals.get('cnae', False)
        })

        # Guardem les dades que s'utilitzen per l'activacio de subrogacio
        # sense nou contracte
        dp = vals.get('direccio_pagament', False)
        dp = dp[0] if isinstance(dp, (list, tuple)) else dp
        dn = vals.get('direccio_notificacio', False)
        dn = dp[0] if isinstance(dn, (list, tuple)) else dn
        bank = vals.get('bank', False)
        bank = bank[0] if isinstance(bank, (list, tuple)) else bank
        tp = vals.get('tipo_pago', False)
        tp = tp[0] if isinstance(tp, (list, tuple)) else tp
        pid = vals.get('payment_mode_id', False)
        pid = pid[0] if isinstance(pid, (list, tuple)) else pid
        new_vals.update({
            'direccio_pagament': dp, 'direccio_notificacio': dn,
            'bank': bank, 'tipo_pago': tp, 'payment_mode_id': pid,
        })

        pas.write(new_vals)
        # Power Update
        self.recalcula_periodes(cursor, uid, [pas.id], context=context)
        pas = self.browse(cursor, uid, pas.id, context=context)
        for pot in pas.pot_ids:
            potencia_periode = vals.get('power_{0}'.format(pot.name.lower()), 0)
            pot_vals = {'potencia': potencia_periode}
            pot.write(pot_vals)

        if vals.get("new_contract_values"):
            ctx = context.copy()
            ctx['from_atr'] = True
            pol_obj = self.pool.get('giscedata.polissa')
            pol_id = pol_obj.copy(
                cursor, uid, pas.sw_id.cups_polissa_id.id,
                default={
                    'observacions': _(u"Duplicat procedent de canvi de titular ({0})").format(datetime.today().strftime("%d-%m-%Y"))
                }, context=ctx
            )
            pol_obj.write(cursor, uid, pol_id, vals.get("new_contract_values"))
            pas.sw_id.write({
                'ref': "giscedata.polissa,{0}".format(pol_id)
            })
        elif vals.get("new_contract_to_link"):
            new_con_id = vals.get("new_contract_to_link")
            pas.sw_id.write({'ref': "giscedata.polissa,{0}".format(new_con_id)})

        if vals.get("lectures_vals"):
            self.introduir_lectures_for_m1(cursor, uid, vals.get("lectures_vals"), context=context)

        return True

    def validar_lectures_for_m1(self, cursor, uid, lect_vals, context=None):
        if context is None:
            context = {}

        if not lect_vals:
            raise SwitchingException(_(u"No hi ha lectures entrades"))

        pol_obj = self.pool.get("giscedata.polissa")
        validator = ValidadorLecturesATR()
        checks_m1 = ["check_periods", "check_volta_contador"]

        lect, maxs = self.prepare_for_inserta_lectures(cursor, uid, lect_vals, context)
        measures_list = lect + maxs

        ctx = context.copy()
        ctx.update({
            "polissa_vals": pol_obj.read(cursor, uid, lect_vals['contract'], ['tarifa', 'facturacio_potencia'])
        })
        res = validator.validate(self.pool, cursor, uid, measures_list, checks_list=checks_m1, context=ctx)
        if not res.valid:
            raise SwitchingException(
                _(u"Les lectures introduïdes no son vàlides:\n{0}\n").format(res.error_msg)
            )
        return True

    def prepare_for_inserta_lectures(self, cursor, uid, lect_vals, context=None):
        pol_obj = self.pool.get("giscedata.polissa")
        periode_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        imd_obj = self.pool.get('ir.model.data')

        if not lect_vals['measure_date']:
            return

        comptadors = pol_obj.comptadors_actius(
            cursor, uid, lect_vals['contract'], lect_vals['measure_date']
        )
        tarifa_id = pol_obj.read(cursor, uid, lect_vals['contract'], ['tarifa'])['tarifa'][0]
        if not len(comptadors):
            return

        comptador_id = comptadors[0]
        comptador_id = (comptador_id,)
        lectures = []
        maximetres = []
        origen_comer = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures_pool', 'origen_comer_autolectura'
        )[1]
        origen_comer = (origen_comer,)
        origen = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_lectures', 'origen50'
        )[1]
        origen = (origen,)

        for i in range(1, lect_vals['num_periods'] + 1):
            per_search = [
                ('tarifa', '=', tarifa_id),
                ('tipus', '=', "te"),
                ('name', '=', "P%s" % i)
            ]
            per_id = periode_obj.search(cursor, uid, per_search)
            if per_id:
                newl = {
                    'comptador': comptador_id,
                    'name': lect_vals['measure_date'],
                    'periode': per_id,
                    'tipus': ('A',),
                    'lectura': lect_vals['measure_ae_p{0}'.format(i)],
                    'observacions': _(u"Lectura introduida al generar M1 de canvi de titular"),
                    'origen_comer_id': origen_comer,
                    'origen_id': origen,
                    'incidencia_id': False
                }
                lectures.append(newl)

            if lect_vals['send_r1_measures']:
                per_search = [
                    ('tarifa', '=', tarifa_id),
                    ('tipus', '=', "te"),
                    ('name', '=', "P%s" % i)
                ]
                per_id = periode_obj.search(cursor, uid, per_search)
                if per_id:
                    newl = {
                        'comptador': comptador_id,
                        'name': lect_vals['measure_date'],
                        'periode': per_id,
                        'tipus': ('R',),
                        'lectura': lect_vals['measure_r1_p{0}'.format(i)],
                        'observacions': _(u"Lectura introduida al generar M1 de canvi de titular"),
                        'origen_comer_id': origen_comer,
                        'origen_id': origen,
                        'incidencia_id': False
                    }
                    lectures.append(newl)

            if lect_vals['send_pm_measures']:
                per_search = [
                    ('tarifa', '=', tarifa_id),
                    ('tipus', '=', "tp"),
                    ('name', '=', "P%s" % i)
                ]
                per_id = periode_obj.search(cursor, uid, per_search)
                if per_id:
                    newl = {
                        'comptador': comptador_id,
                        'name': lect_vals['measure_date'],
                        'periode': per_id,
                        'exces': 0,
                        'lectura': lect_vals['measure_pm_p{0}'.format(i)],
                        'observacions': _(u"Lectura introduida al generar M1 de canvi de titular"),
                        'origen_comer_id': origen_comer,
                        'incidencia_id': False
                    }
                    maximetres.append(newl)
        return lectures, maximetres

    def introduir_lectures_for_m1(self, cursor, uid, lect_vals, context=None):
        if context is None:
            context = {}

        compt_obj = self.pool.get("giscedata.lectures.comptador")
        lect, maxs = self.prepare_for_inserta_lectures(cursor, uid, lect_vals, context)
        res = compt_obj.inserta_lectures(
            cursor, uid, lect, maxs, pool=True, context=context
        )
        return res

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        titular_vat = sw.cups_polissa_id.titular.vat
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, polissa.distribuidora.ref
        )

        created_pots = header_obj.create_pots(cursor, uid, False,
                                              polissa=polissa,
                                              context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        # Agafem la periodicitat de la distribuidora (si n'hi ha)
        periodicitat_facturacio = (polissa.facturacio_distri
                                   or polissa.facturacio)
        vals.update(
            {'data_accio': today,
             # required with contracts <= 1 year
             # (eventuales, tempoerada, obreas...)
             # 'data_final': today,
             'tipus_document': vat_info['document_type'],
             'codi_document': vat_info['vat'],
             'persona': vat_info['is_enterprise'] and 'J' or 'F',
             'fiscal_address_id': polissa.direccio_pagament.id,
             'pot_ids': [(6, 0, created_pots)],
             'tarifaATR': polissa.tarifa.codi_ocsum,
             'control_potencia': (polissa.facturacio_potencia == 'max' and '2'
                                  or '1'),
             'periodicitat_facturacio': '0%d' % periodicitat_facturacio,
             'tipus_autoconsum': polissa.autoconsumo,
             'tipus_contracte': polissa.contract_type or '01'
             })

        if vals['tipus_contracte'] == '09':
            vals.update(
                {'expected_consumption': round(polissa.expected_consumption, 2)}
            )

        if check_contracte_is_eventual(polissa):
            vals.update({
                'data_final':
                    polissa.data_baixa or datetime.now().strftime('%Y-%m-%d'),
            })

        if not vals.get('cont_nom'):
            # dades de contacte
            if vals['persona'] == 'J':
                nom = vals['nom']
            else:
                nom = "{0} {1} {2}".format(
                    vals['nom'], vals['cognom_1'], vals['cognom_2']
                )
            vals.update({
                'cont_nom': nom,
                'cont_telefons': vals['telefons']
            })

        return self.create(cursor, uid, vals, context=context)

    def onchange_client_seleccionat(self, cursor, uid, ids, client_id,
                                    context=None):
        if not context:
            context = None

        res = {}
        vals = {}
        warning = {}

        partner_obj = self.pool.get('res.partner')
        swh_obj = self.pool.get('giscedata.switching.step.header')
        client = partner_obj.browse(cursor, uid, client_id)

        if client:
            vat = client.vat or ''
            es_empresa = partner_obj.vat_es_empresa(cursor, uid, client.vat)

            if not es_empresa:
                nom = partner_obj.separa_cognoms(cursor, uid, client.name)
                vals.update(
                    {'nom': nom['nom'],
                     'cognom_1': nom['cognoms'][0],
                     'cognom_2': nom['cognoms'][1]}
                )
                if nom['fuzzy']:
                    warning = {'title': _(u'Nom/Cognoms'),
                               'message': _(u'Els cognoms es poden haver '
                                            u'tallat malament, repassa-ho')}
            else:
                vals.update({
                    'nom': client.name,
                    'cognom_1': '',
                    'cognom_2': ''}
                )

            vals.update({
                'persona': es_empresa and 'J' or 'F',
                'tipus_document': 'NI',
                'codi_document': len(vat) == 9 and vat or vat[2:],
            })
            # dades de contacte
            if es_empresa:
                nom = client.name
            else:
                nom = "{0} {1} {2}".format(
                    vals['nom'], vals['cognom_1'], vals['cognom_2']
                )
            vals.update({'cont_nom': nom})

        # agafa el telèfon de la primera adreça que troba
        for adr in client.address:
            phone = adr.phone or adr.mobile
            if phone:
                tel_obj = self.pool.get("giscedata.switching.telefon")
                tel_ids = tel_obj.dummy_create(cursor, uid, adr.id)
                vals.update({'telefons': [(6, 0, tel_ids)]})
                # dades de contacte
                vals.update({'cont_telefons': vals['telefons']})
                break

        res['value'] = vals
        if warning:
            res.update({'warning': warning})

        return res

    def onchange_sol_adm(self, cursor, uid, ids, sollicitudadm, persona,
                         tipus_document, codi_document, nom, cognom_1, cognom_2,
                         telefon, prefix, context=None):
        if not context:
            context = None

        res = {}
        vals = {}

        camps = ['persona', 'tipus_document', 'codi_document', 'nom',
                 'cognom_1', 'cognom_2', 'telefons']

        if sollicitudadm not in ['A', 'S']:
            return res

        for camp in camps:
            vals[camp] = ''

        res['value'] = vals

        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        polissa = step.sw_id.cups_polissa_id
        if not polissa:
            return " "
        max_data_fi_modcons = None
        if polissa.modcontractual_activa:
            max_data_fi_modcons = polissa.modcontractual_activa.data_final
        if (max_data_fi_modcons and
                polissa.data_alta <= step.date_created <= max_data_fi_modcons):
            polissa = polissa.browse(context={'date': step.date_created})[0]
        info_cli = "({})".format(step.sollicitudadm)
        if step.sollicitudadm in ['S', 'A'] and step.canvi_titular:
            tipus_canvi = "[{}]".format(step.canvi_titular)
            cli_act = u'{}'.format(polissa.titular_nif)
            nou_cli = u'{}'.format(step.codi_document)
            info_cli += u'{2} {0} -> {1}'.format(cli_act, nou_cli, tipus_canvi)

        if step.sollicitudadm in ['S'] and step.canvi_titular:
            extra_info = _(u". Tarifa:{0}, Pot.: {1}").format(polissa.tarifa.name, polissa.potencia)
            info_cli += extra_info

        inf_a = ""
        if step.sollicitudadm in ['N', 'A']:
            antigues_p = ""
            for p in polissa.potencies_periode:
                antigues_p = ''.join([
                    antigues_p,
                    "{0}: {1}, ".format(p.periode_id.name, p.potencia)
                ])
                antigues_p = antigues_p[0:-2]

            actual_tarif = polissa.tarifa.descripcio.split()[1]
            tarif_obj = self.pool.get("giscedata.polissa.tarifa")
            tarif_id = tarif_obj.get_tarifa_from_ocsum(
                cursor, uid, step.tarifaATR, context
            )
            if not tarif_id:
                return " "
            nova_tarif = tarif_obj.read(cursor, uid, tarif_id, ['name'])['name']
            inf_a = u' {0} -> {1}'.format(actual_tarif, nova_tarif)

            fact_pot = ""
            if step.control_potencia:
                act_fp = polissa.facturacio_potencia
                new_fp = get_description(step.control_potencia, 'TABLA_51')
                fact_pot = _(u". {0} -> {1}").format(act_fp, new_fp)
            inf_a += fact_pot

            noves_p = ""
            for p in step.pot_ids:
                noves_p = ''.join([
                    noves_p,
                    "{0}: {1}, ".format(p.name, p.potencia / 1000.0)
                ])
            inf_a = _(u'{0}; Potencies: {1} -> {2}. ').format(
                inf_a, antigues_p, noves_p[0:-2]
            )

        bono_social = ""
        if step.bono_social == '1':
            bono_social = _(u"Bo Social: Si. ")
        elif step.bono_social == '0':
            bono_social = _(u"Bo Social: No. ")
        return u'{2}{0}{1}'.format(info_cli, inf_a, bono_social)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_M1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'sollicitudadm': fields.selection(TABLA_7, 'Sol·licitud', required=True,
                                          help=u"* S quan només es fa un canvi "
                                               u"de titular\n"
                                               u"* N quan només es fan canvis "
                                               u"d'ATR\n"
                                               u"* A quan es fa canvi de "
                                               u"titular i canvi d'ATR"),
        'canvi_titular': fields.selection(TABLA_53, 'Canvi de titular',
                                          help=u"Obligatori quan es fa un "
                                               u"canvi de titular"),
        'activacio_cicle': fields.selection(TABLA_8, u"Activació",
                                            required=True),
        'data_accio': fields.date(u"Data prevista del canvi o alta"),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE'),
        'solicitud_tensio': fields.selection(TABLA_117, u"Tensió Sol.licitada"),
        # contracte
        'tipus_autoconsum': fields.selection(TABLA_113, u"Tipus Autoconsum"),
        'tipus_contracte': fields.selection(TABLA_9,
                                            u"Tipus de contracte",
                                            required=True),
        'data_final': fields.date(u"Data final contracte"),
        # client
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2,
                                           required=True),
        'codi_document': fields.char(u'Codi document', size=11,
                                     required=True),
        'persona': fields.selection(PERSONA, u'Persona', required=True),
        'nom': fields.char(u'Nom de la persona o societat', size=45,
                           required=True),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9),
        'ind_direccio_fiscal': fields.selection(TABLA_11,
                                                'Indicador direcció'),
        'fiscal_address_id': fields.many2one('res.partner.address',
                                             'Adreça fiscal'),
        'bono_social': fields.selection(TABLA_116, u"Bo Social", size=1),
        # mesura
        'equip_aportat_client': fields.selection(TABLA_20,
                                                 'Equip Aportat client'),
        'tipus_equip_mesura': fields.selection(TABLA_22,
                                               'Tipus equip de mesura'),
        # altres
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa'),
        'control_potencia': fields.selection(CONTROL_POTENCIA,
                                             u"Control de potència"),
        'periodicitat_facturacio': fields.selection(TABLA_108,
                                                    u"Periodicitat de "
                                                    u"facturació", size=2),
        'dades_client': fields.many2one('res.partner', method="true",
                                        string="Client Seleccionat",
                                        help=u'Selecciona un partner per '
                                             u'omplir les dades del client '
                                             u'automàticament'),
        # Dades de contacte addicionals (opcionals)
        'cont_nom': fields.char(u'Nom de la persona o societat', size=45),
        'cont_prefix': fields.char(u"Prefix telefònic", size=2),
        'cont_telefon': fields.char(u"Telèfon", size=9),
        # Comentaris
        'comentaris': fields.text(u"Comentaris", size=4000),
        # Tecnical Documentacion (Doc Tecnica)
        'dt_add': fields.boolean(u"Afegir Dades CIE"),
        'dt_cie_electronico': fields.selection(SINO, u"Cie Electronico"),
        'dt_cie_codigo': fields.char(u"Código CIE", size=35),
        'dt_cie_papel_potenciainstbt': fields.integer(u"Potència Instalada"),
        'dt_cie_papel_data_emissio': fields.date(u"Data Emissió"),
        'dt_cie_papel_data_caducitat': fields.date(u"Data Caducitat"),
        'dt_tipus_codi_instalador': fields.selection(
            TIPUS_DOCUMENT_INST_CIE, u"Tipus codi Instal·lador"
        ),
        'dt_codi_instalador': fields.char(u"Codi instal·lador", size=9),
        'dt_cie_papel_tensio_suministre': fields.selection(
            TABLA_64, u"Tensió suministre"
        ),
        'dt_cie_papel_tipus_suministre': fields.selection(
            TABLA_62, u"Tipus Suministre"
        ),
        'dt_cie_sello_elec': fields.char(u"Segell Electrónic", size=35),
        'dt_add_apm': fields.boolean(u"Afegir Dades APM"),
        'dt_apm_codigo': fields.char(u"Código APM", size=35),
        'dt_apm_potenciainstat': fields.integer(u"Potència Instalada"),
        'dt_apm_data_emissio': fields.date(u"Data Emissió APM"),
        'dt_apm_data_caducitat': fields.date(u"Data Caducitat APM"),
        'dt_apm_tensio_suministre': fields.selection(
            TABLA_64, u"Tensió suministre"
        ),
        'dt_apm_tipus_codi_instalador': fields.selection(
            TIPUS_DOCUMENT_INST_CIE, u"Tipus codi Instal·lador APM"
        ),
        'dt_apm_codi_instalador': fields.char(u"Codi instal·lador APM", size=9),

        'expected_consumption': fields.float(u"Consum pactat", readonly=True,
                                             help="Aquest camp calcula el "
                                                  "consum total pactat per "
                                                  "contractes eventuals sense "
                                                  "comptador"),
        # Dades extra per a l'activacio amb subrogacio sense nou contracte
        'direccio_pagament': fields.many2one('res.partner.address', u'Adreça fiscal'),
        'direccio_notificacio': fields.many2one('res.partner.address', 'Adreça contacte'),
        'bank': fields.many2one('res.partner.bank', 'Compte bancari'),
        'tipo_pago': fields.many2one('payment.type', 'Tipo de pago'),
        'payment_mode_id': fields.many2one('payment.mode', 'Grup de pagament'),
    }

    _defaults = {
        'ind_direccio_fiscal': lambda *a: 'S',
        'activacio_cicle': lambda *a: 'L',
        'sollicitudadm': lambda *a: 'N',
        'tipus_autoconsum': lambda *a: '00',
        'tipus_contracte': lambda *a: '01',
        'canvi_titular': lambda *a: 'A',
        'dades_client': lambda *a: False,
        'control_potencia': lambda *a: 1,
        'prefix': lambda *a: '34',
        'cont_prefix': lambda *a: '34',
        # Tecnical Documentation
        'dt_add': lambda *a: False,
        'dt_add_apm': lambda *a: False,
        'dt_cie_electronico': lambda *a: 'N',
        'dt_cie_papel_tensio_suministre': lambda *a: '02',
        'dt_apm_tensio_suministre': lambda *a: '02',
        'dt_cie_papel_tipus_suministre': lambda *a: 'UV',
        'dt_tipus_codi_instalador': lambda *a: 'nif',
        'dt_apm_tipus_codi_instalador': lambda *a: 'nif',
    }

GiscedataSwitchingM1_01()


class GiscedataSwitchingM1_02(osv.osv):
    """Classe pel pas 02
    """
    _name = "giscedata.switching.m1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'M1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if context and context.get("rebuig", False):
            return []
        return ['03', '04', '05', '06']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_02, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def onchange_tipus_activacio(self, cursor, uid, ids, tipus, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids)

        data = {}

        if tipus == 'L0':
            try:
                fi_lot = pas.sw_id.cups_polissa_id.lot_facturacio.data_final
                data_act = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                            timedelta(days=1)).strftime('%Y-%m-%d')

                data = {'data_activacio': data_act}
            except:
                pass
        return {'value': data}

    def onchange_rebuig(self, cursor, uid, ids, rebuig, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        data = {}
        if not rebuig:
            pas = self.browse(cursor, uid, ids)
            if pas.sw_id.cups_polissa_id and pas.sw_id.cups_polissa_id.tarifa:
                polissa = pas.sw_id.cups_polissa_id
                data = {'tarifaATR': polissa.tarifa.codi_ocsum}
        return {'value': data}

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML M1, pas 02
           d'acceptació
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = m1.MensajeAceptacionModificacionDeATR()
        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades acceptació
        dades = m1.DatosAceptacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
            'actuacion_campo': pas.actuacio_camp,
        })
        if sw.check_destinatari_is_cur(pas):
            dades.feed({'bono_social': pas.bono_social})
        # Contracte
        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = m1.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'potencias_contratadas': potencies,
        })
        contracte = m1.Contrato()
        contracte_vals = {
            'condiciones_contractuales': condicions,
            'tipo_contrato_atr': pas.tipus_contracteATR
        }
        if pas.tipus_activacio:
            contracte_vals.update(
                {'tipo_activacion_prevista': pas.tipus_activacio})
        if pas.data_activacio:
            contracte_vals.update(
                {'fecha_activacion_prevista': pas.data_activacio})
        contracte.feed(contracte_vals)
        # Acceptacio
        acceptacio = m1.AceptacionModificacionDeATR()
        acceptacio.feed({
            'datos_aceptacion': dades,
            'contrato': contracte,
        })

        msg.feed({
            'cabecera': capcalera,
            'aceptacion_modificacion_de_atr': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref, pas.receptor_id.ref,
                                   pas='02')
        return (fname, str(msg))

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML M1, pas 02
           de rebuig
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML M1, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, pots,
                                              context=context)

        vals.update({
            'rebuig': False,
            'data_acceptacio': xml.datos_aceptacion.fecha_aceptacion,
            'actuacio_camp': xml.datos_aceptacion.actuacion_campo,
            'bono_social': xml.datos_aceptacion.bono_social,
            # Contracte - pas 02
            'tarifaATR': xml.contrato.tarifa_atr,
            'pot_ids': [(6, 0, created_pots)],
            'tipus_activacio': xml.contrato.tipo_activacion_prevista,
            'data_activacio': xml.contrato.fecha_activacion_prevista,
            'tipus_contracteATR': xml.contrato.tipo_contrato_atr,
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML M1, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id
        # Com que al fer el 02 encara no s'han fet els canvis al contracte,
        # s'ha de agafar  la informació del pas 01 i no del contracte ja que
        # en el 02 s'ha de confirmar la informació solicitada
        pas01_obj = self.pool.get("giscedata.switching.m1.01")
        pas01 = pas01_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
        if pas01:
            pas01 = pas01_obj.browse(cursor, uid, pas01[0], context=context)
            created_pots = header_obj.create_pots(cursor, uid, False,
                                                  pas01=pas01,
                                                  context=context)
        else:
            created_pots = header_obj.create_pots(cursor, uid, False,
                                                  polissa=polissa,
                                                  context=context)

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        helper_obj = self.pool.get("giscedata.switching.helpers")
        activacio, data_activacio = helper_obj.get_data_prevista_activacio(
            cursor, uid, sw, context
        )

        if pas01:
            vals.update({
                'tarifaATR': pas01.tarifaATR,
            })
        else:
            vals.update({
                'tarifaATR': polissa.tarifa.codi_ocsum,
            })

        conf_obj = self.pool.get('res.config')
        treball_camp = eval(conf_obj.get(cursor, uid, 'sw_m1_S_with_service_order', "0"))
        treball_camp = 'S' if treball_camp else "N"

        if treball_camp != 'S' and pas01.sollicitudadm == "S" and pas01.canvi_titular == 'S':
            active_modcon = polissa.modcontractual_activa
            if active_modcon and active_modcon.data_inici and active_modcon.data_inici < data_activacio:
                # S'ha de informar de la ultima lectura firme, per tant se
                # suposa que sera el dia abnas que l'inici de la modcon
                data_activacio = datetime.strptime(active_modcon.data_inici, "%Y-%m-%d") - timedelta(days=1)
                data_activacio = data_activacio.strftime("%Y-%m-%d")

        bs = False
        if sw.cups_polissa_id:
            bs = sw.cups_polissa_id.bono_social
            pas01_obj = self.pool.get("giscedata.switching.m1.01")
            pas01 = pas01_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
            if pas01:
                pas01 = pas01_obj.read(cursor, uid, pas01[0], {'bono_social'}, context=context)
                bs = pas01.get('bono_social') or bs

        vals.update({
            'rebuig': False,
            'data_acceptacio': today,
            'data_rebuig': today,
            'actuacio_camp': treball_camp,
            'bono_social': bs,
            # Contracte - pas 02
            'tipus_activacio': activacio,
            'data_activacio': data_activacio,
            'pot_ids': [(6, 0, created_pots)],
        })
        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.m1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        m101_obj = self.pool.get("giscedata.switching.m1.01")
        m101_id = m101_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
        if not len(m101_id):
            return datetime.today()
        tipus = m101_obj.read(cursor, uid, m101_id[0],
                              ['sollicitudadm', 'canvi_titular'])

        if tipus['sollicitudadm'] == 'S' \
                and tipus['canvi_titular'] in ['S', 'A', 'C']:
            return datetime.today()
        hobj = self.pool.get("giscedata.switching.helpers")
        return hobj.get_init_deadline_cn_m1(cursor, uid, sw_id, "M1", context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_M1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        'actuacio_camp': fields.selection(SINO, u"Treball de camp"),
        # Contracte - pas 02
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa'),
        'tipus_contracteATR': fields.selection(TABLA_9, u'Tipus Contracte'),
        'tipus_activacio': fields.selection(TIPUS_ACTIVACIO,
                                            u"Activació", size=2),
        'data_activacio': fields.date(u"Data de canvi o alta"),
        'bono_social': fields.selection(TABLA_116, u"Bo Social", size=1),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True,),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today(),
        'tipus_contracteATR': lambda *a: '01',
    }

GiscedataSwitchingM1_02()


class GiscedataSwitchingM1_03(osv.osv):

    _name = 'giscedata.switching.m1.03'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '03'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'M1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['04', '05', '06']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_03, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = m1.MensajeIncidenciasATRDistribuidor()
        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Incidencies
        inc_obj = self.pool.get("giscedata.switching.incidencia")
        incidencies = inc_obj.generar_xml(cursor, uid, pas)
        dades = m1.IncidenciasATRDistribuidor()
        dades.feed({
            'fecha_incidencia': pas.data_incidencia,
            'fecha_prevista_accion': pas.data_prevista_accio,
            'incidencia_list': incidencies,
        })

        msg.feed({
            'cabecera': capcalera,
            'incidencias_atr_distribuidor': dades,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref, pas.receptor_id.ref,
                                   pas='03')
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        inc_obj = self.pool.get('giscedata.switching.incidencia')

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml)
        incidencies = inc_obj.create_from_xml(cursor, uid, xml)

        vals.update({
            'data_incidencia': xml.fecha_incidencia,
            'data_prevista_accio': xml.fecha_prevista_accion,
            'incidencia_ids': [(6, 0, incidencies)],
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_M1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        'data_incidencia': fields.date(u"Data Incidència", required=True),
        'data_prevista_accio': fields.date(u"Data Prevista Acció"),
    }

    _defaults = {
        'data_incidencia': lambda *a: datetime.today(),
    }

GiscedataSwitchingM1_03()


class GiscedataSwitchingM1_04(osv.osv):

    _name = 'giscedata.switching.m1.04'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '04'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'M1', self._nom_pas, context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_04,
                     self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML M1, pas 04
            """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '04'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
            """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'data_rebuig': datetime.today(),
        })
        return self.create(cursor, uid, vals)

    def _get_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Sempre és rebuig"""
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        return {}.fromkeys(ids, True)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.m1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_M1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        # Rebuig
        'rebuig': fields.function(_get_rebuig, 'Sol·licitud rebutjada',
                                  type='boolean', method=True),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True,),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

GiscedataSwitchingM1_04()


class GiscedataSwitchingM1_05(osv.osv):
    """Classe pel pas 05
    """
    _name = "giscedata.switching.m1.05"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '05'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'M1', self._nom_pas, context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == 'distri':
            return ['06']
        elif where == 'comer':
            return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_05, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context)
        sw = pas.sw_id
        msg = m1.MensajeActivacionModificacionDeATR()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades activació
        dades_activacio = m1.DatosActivacion()
        dades_activacio.feed({
            'fecha': pas.data_activacio,
        })
        if sw.check_destinatari_is_cur(pas):
            dades_activacio.feed({'bono_social': pas.bono_social})
        # Contracte
        idcontracte = m1.IdContrato()
        nom_ctr = filter(lambda x: x.isdigit(), sw.cups_polissa_id.name)
        idcontracte.feed({
            'cod_contrato': nom_ctr
        })

        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = m1.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'periodicidad_facturacion': pas.periodicitat_facturacio,
            'tipode_telegestion': pas.tipus_telegestio,
            'potencias_contratadas': potencies,
            'modo_control_potencia': pas.control_potencia,
            'tension_del_suministro': pas.tensio_suministre,
            'marca_medida_con_perdidas': pas.marca_medida_bt,
            'porcentaje_perdidas': pas.perdues,
        })
        if pas.marca_medida_bt == 'S':
            condicions.feed({'vas_trafo': int(pas.kvas_trafo * 1000)})
        contracte = m1.Contrato()
        contracte.feed({
            'id_contrato': idcontracte,
            'tipo_autoconsumo': pas.tipus_autoconsum,
            'tipo_contrato_atr': pas.tipus_contracte,
            'condiciones_contractuales': condicions,
        })

        punts_mesura = pas.header_id.generar_xml_pm(pas)

        # Activacio
        activacio = m1.ActivacionModificaciones()
        activacio.feed({
            'datos_activacion': dades_activacio,
            'contrato': contracte,
            'puntos_de_medida': punts_mesura
        })
        msg.feed({
            'cabecera': capcalera,
            'activacion_modificaciones': activacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, pots,
                                              context=context)
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        pm_ids = header_obj.create_from_xml_pm(cursor, uid, xml,
                                               context=context)

        tipo_telegestion_xml = xml.contrato.tipo_de_telegestion

        vals.update({
            # dades activació
            'data_activacio': xml.datos_activacion.fecha,
            'bono_social': xml.datos_activacion.bono_social,
            'tipus_autoconsum': xml.contrato.tipo_autoconsumo,
            'tipus_contracte': xml.contrato.tipo_contrato_atr,
            'tarifaATR': xml.contrato.tarifa_atr,
            'periodicitat_facturacio': xml.contrato.periodicidad_facturacion,
            'tipus_telegestio': tipo_telegestion_xml,
            'pot_ids': [(6, 0, created_pots)],
            'pm_ids': [(6, 0, pm_ids)],
            'contracte_atr': xml.contrato.cod_contrato or '',
            'control_potencia': xml.contrato.modo_control_potencia,
            'tensio_suministre': xml.contrato.tension_del_suministro,
        })
        if xml.contrato.marca_medida_con_perdidas == 'S':
            vals.update({
                'marca_medida_bt': 'S',
                'kvas_trafo': float(xml.contrato.vas_trafo) / 1000,
                'perdues': xml.contrato.porcentaje_perdidas,
            })
        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)

        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        active_modcon = polissa.modcontractual_activa
        modcon_start_date = polissa.modcontractual_activa.data_inici

        if active_modcon and modcon_start_date:
            activation_date = modcon_start_date
        else:
            activation_date = datetime.strftime(datetime.now(), '%Y-%m-%d')

        control_potencia = (polissa.facturacio_potencia == 'max' and '2' or '1')
        created_pots = header_obj.create_pots(cursor, uid, False,
                                              polissa=polissa,
                                              context=context)
        pm_ids = header_obj.dummy_create_pm(cursor, uid, sw,
                                            context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        tipus_tg = CONV_T109_T111.get('{0:02d}'.format(int(polissa.tg)), '03')

        vals.update({
            'data_activacio': activation_date,
            'bono_social': polissa.bono_social,
            'tipus_autoconsum': polissa.autoconsumo,
            'tipus_contracte': '01',
            'tarifaATR': polissa.tarifa.codi_ocsum,
            'periodicitat_facturacio': '0%d' % polissa.facturacio,
            'tipus_telegestio': '{0}'.format(tipus_tg),
            'control_potencia': control_potencia,
            'contracte_atr': polissa.name or '',
            'pot_ids': [(6, 0, created_pots)],
            'pm_ids': [(6, 0, [pm_ids])],
            'tensio_suministre': (
                (polissa.tensio_normalitzada and
                 polissa.tensio_normalitzada.cnmc_code) or '01'
            ),
        })
        # If codi_ocsum is 011 (3.1A) and there is 'trafo' in polissa then
        # the tarif is 3.1A LB and we add 'marca_medida_bt' and 'kvas_trafo'
        mesura = polissa.tarifa.mesura_ab
        if polissa.tarifa.codi_ocsum == '011' and mesura == 'B':
            vals.update({
                'marca_medida_bt': 'S',
                'kvas_trafo': polissa.trafo,
                'perdues': 4.0,
            })
        else:
            vals.update({
                'marca_medida_bt': 'N',
            })
        return self.create(cursor, uid, vals)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_M1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades activació
        'data_activacio': fields.date(u"Data d'activació"),
        'tipus_autoconsum': fields.selection(TABLA_113, u"Tipus Autoconsum"),
        'tipus_contracte': fields.selection(TABLA_9, u"Tipus de contracte"),
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa', readonly=True),
        'periodicitat_facturacio': fields.selection(TABLA_108,
                                                    u"Periodicitat facturació",
                                                    size=2, required=True),
        'control_potencia': fields.selection(CONTROL_POTENCIA,
                                             u"Control de potència"),
        'contracte_atr': fields.char(u'Codi contracte ATR', size=12,
                                     readonly=True,
                                     help=u'Codi contracte a la distribuidora'),
        'tipus_telegestio': fields.selection(TABLA_111,
                                             u'Tipus de Telegestió'),
        'marca_medida_bt': fields.selection([('S', 'Si'), ('N', 'No')],
                                            u'Marca Mesura BT amb Perdues',
                                            readonly=True),
        'kvas_trafo': fields.float(u"KVAs Trafo"),
        'tensio_suministre': fields.selection(TABLA_64, u'Tensió Suministre',
                                              required=True),
        'perdues': fields.float(u"Percentatge Pèrdues"),
        'bono_social': fields.selection(TABLA_116, u"Bo Social", size=1),
    }

GiscedataSwitchingM1_05()


class GiscedataSwitchingM1_06(osv.osv):
    """Classe pel pas 06
    """
    _name = "giscedata.switching.m1.06"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '06'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'M1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['07']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_06,
                     self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML M2, pas 06
        """
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return anulacio_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return anulacio_obj.create_from_xml(cursor, uid, sw_id, xml,
                                            context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return anulacio_obj.dummy_create(cursor, uid, sw_id,
                                         context=ctx)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_M1_{}'.format(self._nom_pas)

        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
    }

GiscedataSwitchingM1_06()


class GiscedataSwitchingM1_07(osv.osv):
    """Classe pel pas 07
    """
    _name = "giscedata.switching.m1.07"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '07'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(
            cursor, uid, ids, 'M1', self._nom_pas, context=context
        )

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['03', '04', '05']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingM1_07, self).unlink(
            cursor, uid, ids, context=context
        )
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return acc_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name,
                    'nom_pas': self._nom_pas})

        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        if not context:
            context = {}
        if xml.fecha_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.dummy_create(cursor, uid, sw_id, context=ctx)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.m1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_M1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }

GiscedataSwitchingM1_07()
