# -*- coding: utf-8 -*-
""" Assigna el valor get_addtional_info() a tots els giscedata_switching.additional_info
    de la base de dades que no tinguessin valor previ.
"""
import click
import logging
from erppeek import Client
from datetime import datetime
from progressbar import ProgressBar, ETA, Percentage, Bar


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def set_additional_info(c, language):
    logger = logging.getLogger('set_additional_info')
    sw_obj = c.model('giscedata.switching')
    step_obj = c.model('giscedata.switching.step')
    sw_ids = sw_obj.search([('additional_info', '=', False)])
    widgets = [Percentage(), ' ', Bar(), ' ', ETA()]
    pbar = ProgressBar(widgets=widgets, maxval=len(sw_ids)).start()
    done = 0
    for sw_id in sw_ids:
        try:
            sw = sw_obj.read(sw_id, ['additional_info', 'proces_id'])
            step_id = step_obj.search([
                ('name', '=', '01'),
                ('proces_id', '=', sw['proces_id'][0])])[0]
            if step_id:
                step_model = step_obj.get_step_model(step_id)
                pas_obj = c.model(step_model)
                pas_id = pas_obj.search([('sw_id', '=', sw['id'])])
                if pas_id:
                    info = pas_obj.get_additional_info(
                        pas_id[0], {'lang': language}
                    )
                    sw_obj.write([sw_id], {'additional_info': info})
                    logger.info("sw_id {0}: OK with: {1}".format(sw_id, info))
        except Exception as e:
            logger.error("Fail in sw_id {0}: {1}".format(sw_id, e))
        done += 1
        pbar.update(done)
    pbar.finish()


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=18069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('--log-path', default='/tmp/set_additional_info.log',
                help='Path del fitxer de log. Per defecte /tmp/set_additional_info.log')
@click.option('-l', '--language', default='es_ES', help='Idioma')
def main(**kwargs):
    log_file = kwargs['log_path']
    setup_log('set_additional_info', log_file)
    logger = logging.getLogger('set_additional_info')
    logger.info('#######Start now {0}'.format(datetime.today()))
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])

    set_additional_info(c, language=kwargs['language'])

if __name__ == '__main__':
    main()
