# -*- coding: utf-8 -*-
"""
Utilitzar la informació dels camps "telefon" i "cont_telefon" (antic format 1.0)
per crear els objectes giscedata.switching.telefon (nou format 1.1) i
assignarlos als passos corresponents.
"""
import click
import logging
from erppeek import Client
from datetime import datetime
from tqdm import tqdm


def setup_log(instance, filename):
    log_file = filename
    logger = logging.getLogger(instance)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr = logging.FileHandler(log_file)
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)


def set_telefons_switching(c):
    logger = logging.getLogger('set_telefons_switching')
    tel_obj = c.model("giscedata.switching.telefon")
    sw_obj = c.model('giscedata.switching')
    sw_ids = sw_obj.search([('state', '!=', 'done')])
    a3_obj = c.model('giscedata.switching.a3.01')
    b1_obj = c.model('giscedata.switching.b1.01')
    c1_obj = c.model('giscedata.switching.c1.01')
    c2_obj = c.model('giscedata.switching.c2.01')
    m1_obj = c.model('giscedata.switching.m1.01')
    r1_obj = c.model('giscedata.switching.r1.01')
    a3_ids = a3_obj.search([('sw_id', 'in', sw_ids), '|', ('telefons', '=', False), ('cont_telefons', '=', False)])
    b1_ids = b1_obj.search([('sw_id', 'in', sw_ids), '|', ('telefons', '=', False), ('cont_telefons', '=', False)])
    c1_ids = c1_obj.search([('sw_id', 'in', sw_ids), ('telefons', '=', False)])
    c2_ids = c2_obj.search([('sw_id', 'in', sw_ids), '|', ('telefons', '=', False), ('cont_telefons', '=', False)])
    m1_ids = m1_obj.search([('sw_id', 'in', sw_ids), '|', ('telefons', '=', False), ('cont_telefons', '=', False)])
    r1_ids = r1_obj.search([('sw_id', 'in', sw_ids), ('telefons', '=', False)])

    logger.info("Updating steps A3-01")
    print "Updating steps A3-01"
    ok = []
    fail = []
    if len(a3_ids):
        object = a3_obj
        pas_infos = object.read(a3_ids, ['telefon', 'cont_telefon', 'telefons', 'cont_telefons'])
        for pas_info in tqdm(pas_infos):
            try:
                if not len(pas_info.get('telefons', [])) and pas_info.get("telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("telefon")})
                    object.write(pas_info['id'], {"telefons": [(6, 0, [tel.id])]})
                if not len(pas_info.get('cont_telefons', [])) and pas_info.get("cont_telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("cont_telefon")})
                    object.write(pas_info['id'], {"cont_telefons": [(6, 0, [tel.id])]})
                ok.append(pas_info['id'])
                logger.info("Step 01 id {0}: OK".format(pas_info['id']))
            except Exception as e:
                fail.append(pas_info['id'])
                logger.error("Fail in Step 01 id {0}: {1}".format(pas_info['id'], e))
    print "  * OK: {0}  Failed: {1}".format(len(ok), len(fail))

    logger.info("Updating steps B1-01")
    print "Updating steps B1-01"
    ok = []
    fail = []
    if len(b1_ids):
        object = b1_obj
        pas_infos = object.read(b1_ids, ['telefon', 'cont_telefon', 'telefons', 'cont_telefons'])
        for pas_info in tqdm(pas_infos):
            try:
                if not len(pas_info.get('telefons', [])) and pas_info.get("telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("telefon")})
                    object.write(pas_info['id'], {"telefons": [(6, 0, [tel.id])]})
                if not len(pas_info.get('cont_telefons', [])) and pas_info.get("cont_telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("cont_telefon")})
                    object.write(pas_info['id'], {"cont_telefons": [(6, 0, [tel.id])]})
                ok.append(pas_info['id'])
                logger.info("Step 01 id {0}: OK".format(pas_info['id']))
            except Exception as e:
                fail.append(pas_info['id'])
                logger.error("Fail in Step 01 id {0}: {1}".format(pas_info['id'], e))
    print "  * OK: {0}  Failed: {1}".format(len(ok), len(fail))

    logger.info("Updating steps C1-01")
    print "Updating steps C1-01"
    ok = []
    fail = []
    if len(c1_ids):
        object = c1_obj
        pas_infos = object.read(c1_ids, ['telefon', 'telefons'])
        for pas_info in tqdm(pas_infos):
            try:
                if not len(pas_info.get('telefons', [])) and pas_info.get("telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("telefon")})
                    object.write(pas_info['id'], {"telefons": [(6, 0, [tel.id])]})
                ok.append(pas_info['id'])
                logger.info("Step 01 id {0}: OK".format(pas_info['id']))
            except Exception as e:
                fail.append(pas_info['id'])
                logger.error("Fail in Step 01 id {0}: {1}".format(pas_info['id'], e))
    print "  * OK: {0}  Failed: {1}".format(len(ok), len(fail))

    logger.info("Updating steps C2-01")
    print "Updating steps C2-01"
    ok = []
    fail = []
    if len(c2_ids):
        object = c2_obj
        pas_infos = object.read(c2_ids, ['telefon', 'cont_telefon', 'telefons', 'cont_telefons'])
        for pas_info in tqdm(pas_infos):
            try:
                if not len(pas_info.get('telefons', [])) and pas_info.get("telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("telefon")})
                    object.write(pas_info['id'], {"telefons": [(6, 0, [tel.id])]})
                if not len(pas_info.get('cont_telefons', [])) and pas_info.get("cont_telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("cont_telefon")})
                    object.write(pas_info['id'], {"cont_telefons": [(6, 0, [tel.id])]})
                ok.append(pas_info['id'])
                logger.info("Step 01 id {0}: OK".format(pas_info['id']))
            except Exception as e:
                fail.append(pas_info['id'])
                logger.error("Fail in Step 01 id {0}: {1}".format(pas_info['id'], e))
    print "  * OK: {0}  Failed: {1}".format(len(ok), len(fail))

    logger.info("Updating steps M1-01")
    print "Updating steps M1-01"
    ok = []
    fail = []
    if len(m1_ids):
        object = m1_obj
        pas_infos = object.read(m1_ids, ['telefon', 'cont_telefon', 'telefons', 'cont_telefons'])
        for pas_info in tqdm(pas_infos):
            try:
                if not len(pas_info.get('telefons', [])) and pas_info.get("telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("telefon")})
                    object.write(pas_info['id'], {"telefons": [(6, 0, [tel.id])]})
                if not len(pas_info.get('cont_telefons', [])) and pas_info.get("cont_telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("cont_telefon")})
                    object.write(pas_info['id'], {"cont_telefons": [(6, 0, [tel.id])]})
                ok.append(pas_info['id'])
                logger.info("Step 01 id {0}: OK".format(pas_info['id']))
            except Exception as e:
                fail.append(pas_info['id'])
                logger.error("Fail in Step 01 id {0}: {1}".format(pas_info['id'], e))
    print "  * OK: {0}  Failed: {1}".format(len(ok), len(fail))

    logger.info("Updating steps R1-01")
    print "Updating steps R1-01"
    ok = []
    fail = []
    if len(r1_ids):
        object = r1_obj
        pas_infos = object.read(r1_ids, ['telefon', 'telefons'])
        for pas_info in tqdm(pas_infos):
            try:
                if not len(pas_info.get('telefons', [])) and pas_info.get("telefon"):
                    tel = tel_obj.create({"numero": pas_info.get("telefon")})
                    object.write(pas_info['id'], {"telefons": [(6, 0, [tel.id])]})
                ok.append(pas_info['id'])
                logger.info("Step 01 id {0}: OK".format(pas_info['id']))
            except Exception as e:
                fail.append(pas_info['id'])
                logger.error("Fail in Step 01 id {0}: {1}".format(pas_info['id'], e))
    print "  * OK: {0}  Failed: {1}".format(len(ok), len(fail))


@click.command()
@click.option('-s', '--server', default='http://localhost',
              help=u'Adreça servidor ERP')
@click.option('-p', '--port', default=8069, help='Port servidor ERP',
              type=click.INT)
@click.option('-u', '--user', default='admin', help='Usuari servidor ERP')
@click.option('-w', '--password', default='admin',
              help='Contrasenya usuari ERP')
@click.option('-d', '--database', help='Nom de la base de dades')
@click.option('--log-path', default='/tmp/set_additional_info.log',
                help='Path del fitxer de log. Per defecte /tmp/set_telefons_switching.log')
def main(**kwargs):
    log_file = kwargs['log_path']
    setup_log('set_telefons_switching', log_file)
    logger = logging.getLogger('set_telefons_switching')
    logger.info('#######Start now {0}'.format(datetime.today()))
    server = '{}:{}'.format(kwargs['server'], kwargs['port'])
    c = Client(server=server, db=kwargs['database'], user=kwargs['user'],
               password=kwargs['password'])
    set_telefons_switching(c)

if __name__ == '__main__':
    main()
