# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_a3 as a3

from tools.translate import _
from gestionatr.defs import *
from .utils import add_months, calc_months, get_address_dicct, check_contracte_pas_is_eventual, check_contracte_is_eventual
from datetime import datetime, timedelta
import calendar
from .giscedata_switching import SwitchingException
from workdays import workday
from giscedata_switching.giscedata_switching_helpers import  GiscedataSwitchingException
from gestionatr.utils import get_description


class GiscedataSwitchingProcesA3(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 initial steps depending on where we are '''

        if proces == 'A3':
            return ['01']  # In both comer and distri

        return super(GiscedataSwitchingProcesA3,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        ''' returns A3 emisor steps depending on where we are '''
        if proces == 'A3':
            if where == 'distri':
                return ['02', '03', '04', '05', '07']
            elif where == 'comer':
                return ['01', '06']

        return super(GiscedataSwitchingProcesA3,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == 'A3':
            return ['02', '04', '07']

        return super(GiscedataSwitchingProcesA3,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

GiscedataSwitchingProcesA3()


class GiscedataSwitchingA3(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == 'A3':
            #Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name in ('04', '05'):
                    return True
                elif step_name == '07':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if not pas.rebuig:
                        return True

        return super(GiscedataSwitchingA3,
                     self).get_final(cursor, uid, sw, context=context)


GiscedataSwitchingA3()


class GiscedataSwitchingA3_01(osv.osv):
    """ Classe pel pas 01
    """

    _name = "giscedata.switching.a3.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_01,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['02', '06']

    def onchange_persona(self, cr, uid, ids, pers, cog1, cog2, context=None):
        if not context:
            context = {}

        if pers == 'J':
            data = {
                'tipus_document': 'NI',
                'cognom_1': '',
                'cognom_2': '',
            }
        else:
            data = {
                'tipus_document': 'NI',
            }

        return {'value': data}

    def check_cognom(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context=context)
        if pas.persona == 'F' and not pas.cognom_1:
            raise osv.except_osv('Error',
                                 _(u"El camp cognom és obligatori "
                                   u"per persones físiques"))
        return True

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 01
        """
        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        tel_obj = self.pool.get("giscedata.switching.telefon")
        self.check_cnae(cr, uid, pas_id, context=context)
        self.check_cognom(cr, uid, pas_id, context=context)
        pas = self.browse(cr, uid, pas_id, context)

        sw = pas.sw_id
        msg = a3.MensajeAlta()
        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        #sol·licitud
        sollicitud = a3.DatosSolicitud()
        sol_fields = {
            'ind_activacion': pas.activacio_cicle,
            'cnae': pas.cnae.name,
            'solicitud_tension': pas.solicitud_tensio
        }
        if pas.activacio_cicle == 'F':
            sol_fields.update({'fecha_prevista_accion': pas.data_accio})

        sollicitud.feed(sol_fields)

        # contracte
        # Condicions contractuals
        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = a3.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'modo_control_potencia': pas.control_potencia,
            'potencias_contratadas': potencies,
        })

        # Contacte
        contacte = a3.Contacto()
        contacte.feed({
            'persona_de_contacto': pas.cont_nom,
            'telefonos': tel_obj.generar_xml(cr, uid, pas.cont_telefons),
        })

        contracte = a3.Contrato()
        ctr_fields = {
            'condiciones_contractuales': condicions,
            'contacto': contacte,
            'tipo_autoconsumo': pas.tipus_autoconsum,
            'tipo_contrato_atr': pas.tipus_contracte
        }
        if pas.tipus_contracte == '09':
            ctr_fields['consumo_anual_estimado'] = pas.expected_consumption

        if pas.data_final and check_contracte_pas_is_eventual(pas):
            ctr_fields.update({'fecha_finalizacion': pas.data_final})
        contracte.feed(ctr_fields)

        #client
        idclient = a3.IdCliente()
        idclient.feed({
            'tipo_identificador': pas.tipus_document,
            'identificador': pas.codi_document,
            'tipo_persona': pas.persona,
        })
        nomclient = a3.Nombre()
        if pas.persona == 'J':
            nom = {'razon_social': pas.nom}
        else:
            nom = {'nombre_de_pila': pas.nom,
                   'primer_apellido': pas.cognom_1,
                   'segundo_apellido': pas.cognom_2}
        nomclient.feed(nom)
        client = a3.Cliente()
        cli_fields = {
            'id_cliente': idclient,
            'nombre': nomclient,
            'indicador_tipo_direccion': pas.ind_direccio_fiscal,
        }
        if pas.ind_direccio_fiscal == 'F':
            address = pas.fiscal_address_id
            dir_fiscal = a3.Direccion()
            addres_info = get_address_dicct(address)
            via, apartado = False, False
            if addres_info['apartado_de_correos']:
                apartado = addres_info['apartado_de_correos']
            else:
                via = a3.Via()
                via_vals = {
                    'calle': addres_info['calle'],
                    'numero_finca': addres_info['numfinca'],
                }
                if address.aclarador:
                    via_vals.update({'tipo_aclarador_finca': 'NO',
                                     'aclarador_finca': address.aclarador[:40]})
                via.feed(via_vals)
            dir_vals = {'pais': addres_info['pais'],
                        'provincia': addres_info['provincia'],
                        'municipio': addres_info['municipio'],
                        'poblacion': addres_info['poblacion'],
                        'cod_postal': addres_info['codpostal'],
                        'via': via,
                        'apartado_de_correos': apartado
                        }
            dir_fiscal.feed(dir_vals)
            cli_fields.update({'direccion': dir_fiscal})
        if pas.telefons:
            cli_fields.update(
                {'telefonos': tel_obj.generar_xml(cr, uid, pas.telefons)}
            )
        client.feed(cli_fields)

        # mesura
        mesura = a3.Medida()
        mesura.feed({
            'propiedad_equipo': pas.equip_aportat_client,
            'tipo_equipo_medida': pas.tipus_equip_mesura,
        })

        # sol·licitud de canvi
        alta = a3.Alta()
        alta_vals = {
            'comentarios': pas.comentaris or False,
            'datos_solicitud': sollicitud,
            'contrato': contracte,
            'cliente': client,
            'medida': mesura,
        }

        # Doc Tecnica
        doctecnica = a3.DocTecnica()
        # Dades CIE si prodeceix
        if pas.dt_add:
            dades_cie = a3.DatosCie()
            dades_cie.feed({'cie_electronico': pas.dt_cie_electronico})

            if pas.dt_cie_electronico == 'S':
                cie_elec = a3.CIEElectronico()
                cie_elec.feed({
                    'codigo_instalador': pas.dt_codi_instalador,
                    'sello_electronico': pas.dt_cie_sello_elec
                })
                dades_cie.feed({'cie_electronico': cie_elec})
            else:
                cie_paper = a3.CIEPapel()
                cie_paper.feed({
                    'codigo_cie': pas.dt_cie_codigo,
                    'potencia_inst_bt': pas.dt_cie_papel_potenciainstbt,
                    'fecha_emision_cie': pas.dt_cie_papel_data_emissio,
                    'fecha_caducidad_cie': pas.dt_cie_papel_data_caducitat,
                    'tension_suministro_cie': pas.dt_cie_papel_tensio_suministre,
                    'tipo_suministro': pas.dt_cie_papel_tipus_suministre,
                })

                if pas.dt_tipus_codi_instalador == 'nif':
                    vals = {'nif_instalador': pas.dt_codi_instalador}
                else:
                    vals = {'codigo_instalador': pas.dt_codi_instalador}
                cie_paper.feed(vals)
                dades_cie.feed({'cie_papel': cie_paper})

            doctecnica.feed({'datos_cie': dades_cie})

        # Dades APM si procedeix
        if pas.dt_add_apm:
            dades_apm = a3.DatosAPM()
            dades_apm.feed({
                'codigo_apm': pas.dt_apm_codigo,
                'potencia_inst_at': pas.dt_apm_potenciainstat,
                'fecha_emision_apm': pas.dt_apm_data_emissio,
                'fecha_caducidad_apm': pas.dt_apm_data_caducitat,
                'tension_suministro_apm': pas.dt_apm_tensio_suministre,
            })
            if pas.dt_apm_tipus_codi_instalador == 'nif':
                vals = {'nif_instalador': pas.dt_apm_codi_instalador}
            else:
                vals = {'codigo_instalador': pas.dt_apm_codi_instalador}
            dades_apm.feed(vals)
            doctecnica.feed({'datos_apm': dades_apm})

        if pas.dt_add_apm or pas.dt_add:
            alta_vals.update({'doc_tecnica': doctecnica})

        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            alta_vals.update({'registros_documento': doc_xml})

        alta.feed(alta_vals)
        msg.feed({
            'cabecera': capcalera,
            'alta': alta,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        tel_obj = self.pool.get("giscedata.switching.telefon")

        #Per cada potencia hem de crear el registre
        xml_pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, xml_pots,
                                              context=context)
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        tels = tel_obj.create_from_xml(cursor, uid, xml.cliente.telefonos)
        con_tels = tel_obj.create_from_xml(cursor, uid,
                                           xml.contrato.contacto.telefonos)
        vals.update({
            # sol·licitud
            'activacio_cicle': xml.datos_solicitud.ind_activacion,
            'data_accio': xml.datos_solicitud.fecha_prevista_accion,
            'solicitud_tensio': xml.datos_solicitud.solicitud_tension,
            # contracte
            'data_final': xml.contrato.fecha_finalizacion,
            'tipus_autoconsum': xml.contrato.tipo_autoconsumo,
            'tipus_contracte': xml.contrato.tipo_contrato_atr,
            'control_potencia': xml.contrato.modo_control_potencia,
            'pot_ids': [(6, 0, created_pots)],
            # Contacte
            'cont_nom': xml.contrato.contacto.persona_de_contacto,
            'cont_telefons': [(6, 0, con_tels)],
            # client
            'tipus_document': xml.cliente.tipo_identificador,
            'codi_document': xml.cliente.identificador,
            'persona': xml.cliente.tipo_persona,
            'telefons': [(6, 0, tels)],
            'ind_direccio_fiscal': xml.cliente.indicador_tipo_direccion,
            # Comentaris
            'comentaris': xml.comentarios,
        })
        if xml.contrato.tipo_contrato_atr == '09':
            vals['expected_consumption'] = xml.contrato.consumo_anual_estimado

        if xml.medida:
            vals.update({
                # mesura
                'equip_aportat_client': xml.medida.propiedad_equipo,
                'tipus_equip_mesura': xml.medida.tipo_equipo_medida,
            })

        # CNAE
        if xml.datos_solicitud.cnae:
            cnae_id = sw_obj.search_cnae(cursor, uid,
                                         xml.datos_solicitud.cnae,
                                         context=context)
            vals.update({'cnae': cnae_id})

        # We can only store the "tarifa" if it's a correct one
        if xml.contrato.tarifa_atr in dict(TABLA_17):
            vals.update({'tarifaATR': xml.contrato.tarifa_atr})

        if xml.cliente.tipo_persona == 'F':
            vals.update({
                'nom': xml.cliente.nombre_de_pila,
                'cognom_1': xml.cliente.primer_apellido,
                'cognom_2': xml.cliente.segundo_apellido,
            })
        else:
            vals.update({
                'nom': xml.cliente.razon_social
            })

        # Search for address in cliente
        if xml.cliente.direccion:
            direccio = xml.cliente.direccion
            ctx = context.copy()
            ctx['cliente'] = xml.cliente
            addr_id = sw_obj.search_address(cursor, uid, sw_id, direccio, context=ctx)
            vals.update({'fiscal_address_id': addr_id})

        # Documentacio Tecnica
        if xml.doc_tecnica and xml.doc_tecnica.datos_cie:
            if xml.doc_tecnica.datos_cie.cie_papel:
                is_elec = 'N'
                datos_cie = xml.doc_tecnica.datos_cie.cie_papel
                if datos_cie.codigo_instalador:
                    codi_tipusInstalador = datos_cie.codigo_instalador
                    tipusInstalador = 'codigo'
                else:
                    codi_tipusInstalador = datos_cie.nif_instalador
                    tipusInstalador = 'nif'

                vals.update({
                    'dt_add': True,
                    'dt_cie_electronico': is_elec,
                    'dt_cie_codigo': datos_cie.codigo_cie,
                    'dt_cie_papel_potenciainstbt': datos_cie.potencia_inst_bt,
                    'dt_cie_papel_data_emissio': datos_cie.fecha_emision_cie,
                    'dt_cie_papel_data_caducitat': datos_cie.fecha_caducidad_cie,
                    'dt_tipus_codi_instalador': tipusInstalador,
                    'dt_codi_instalador': codi_tipusInstalador,
                    'dt_cie_papel_tensio_suministre': datos_cie.tension_suministro_cie,
                    'dt_cie_papel_tipus_suministre': datos_cie.tipo_suministro,
                })
            else:
                is_elec = 'S'
                datos_cie = xml.doc_tecnica.datos_cie.cie_electronico

                vals.update({
                    'dt_add': True,
                    'dt_cie_electronico': is_elec,
                    'dt_cie_codigo': datos_cie.codigo_cie,
                    'dt_cie_sello_elec': datos_cie.sello_electronico,
                })
        if xml.doc_tecnica and xml.doc_tecnica.datos_apm:
            datos_apm = xml.doc_tecnica.datos_apm
            if datos_apm.codigo_instalador:
                codi_tipusInstalador = datos_apm.codigo_instalador
                tipusInstalador = 'codigo'
            else:
                codi_tipusInstalador = datos_apm.nif_instalador
                tipusInstalador = 'nif'

            vals.update({
                'dt_add_apm': True,
                'dt_apm_codigo': datos_apm.codigo_apm,
                'dt_apm_potenciainstat': datos_apm.potencia_inst_at,
                'dt_apm_data_emissio': datos_apm.fecha_emision_apm,
                'dt_apm_data_caducitat': datos_apm.fecha_caducidad_apm,
                'dt_apm_tipus_codi_instalador': tipusInstalador,
                'dt_apm_codi_instalador': codi_tipusInstalador,
                'dt_apm_tensio_suministre': datos_apm.tension_suministro_apm,
            })

        # Registro Documents
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id,
                           context=context)
        return pas_id

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        mandatory_fields = ['cnae']
        for field in mandatory_fields:
            if not vals.get(field, False):
                raise SwitchingException(_("Falten dades. Camp no omplert."),
                                         [field])
        return True

    def check_cnae(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context=context)
        if not pas.cnae or len(pas.cnae.name) != 4:
            raise osv.except_osv('Error',
                                 _(u'El CNAE escollit no es valid. '
                                   u'Només es permeten CNAE de 4 digits'))
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        Changes step vals accordingly with vals dict:
            'activacio_cicle': A|L|F
        If given S|N then:
            S -> L
            N -> A
        """
        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        self.config_step_validation(cursor, uid, [], vals, context=context)

        pas = self.browse(cursor, uid, ids, context=context)

        new_vals = vals.copy()

        if 'activacio_cicle' in vals:
            if vals['activacio_cicle'] == 'S':
                val = 'L'
            elif vals['activacio_cicle'] == 'N':
                val = 'A'
            else:
                val = vals['activacio_cicle']
            new_vals.update({'activacio_cicle': val})

        if vals.get('docs_ids', False):
            docs_to_set = vals.get('docs_ids', False)
            if 'docs_ids' in new_vals:
                new_vals.pop('docs_ids')
            if 'doc_add' in new_vals:
                new_vals.pop('doc_add')
            if docs_to_set:
                header_id = pas.header_id.id
                for doc_id in docs_to_set:
                    cursor.execute(
                        "INSERT INTO sw_step_header_doc_ref "
                        "(header_id, document_id) "
                        "VALUES (%s, %s)",
                        (header_id, doc_id)
                    )

        if vals.get('dt_add', False):
            doc_tecnica_dict = {
                'dt_add': vals.get('dt_add'),
                'dt_cie_electronico': vals.get('dt_cie_electronico'),
                'dt_cie_codigo': vals.get('dt_cie_codigo'),
                'dt_cie_papel_potenciainstbt': vals.get('dt_cie_papel_potenciainstbt'),
                'dt_cie_papel_data_emissio': vals.get('dt_cie_papel_data_emissio'),
                'dt_cie_papel_data_caducitat': vals.get('dt_cie_papel_data_caducitat'),
                'dt_tipus_codi_instalador': vals.get('dt_tipus_codi_instalador'),
                'dt_codi_instalador': vals.get('dt_codi_instalador'),
                'dt_cie_papel_tensio_suministre': vals.get('dt_cie_papel_tensio_suministre'),
                'dt_cie_papel_tipus_suministre': vals.get('dt_cie_papel_tipus_suministre'),
                'dt_cie_sello_elec': vals.get('dt_cie_sello_elec')
            }
            new_vals.update(doc_tecnica_dict)

        if vals.get('solicitud_tensio', False):
            new_vals.update({
                'solicitud_tensio': vals['solicitud_tensio']
            })

        pas.write(new_vals)

        return True

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        titular_vat = sw.cups_polissa_id.titular.vat
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, polissa.distribuidora.ref
        )

        created_pots = header_obj.create_pots(cursor, uid, False,
                                              polissa=polissa,
                                              context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        # check telefon
        if not len(vals.get('telefons', [])):
            info_pagador = ""
            if polissa.pagador:
                nom = polissa.pagador.name
                vat = polissa.pagador.vat
                info_pagador = u"{0} ({1}), ".format(nom, vat)
            raise osv.except_osv(
                _(U"Error"),
                _(u"El contacte del pagador ({0}contracte {1}) no te cap "
                  u"Telefon assignat"
                  ).format(info_pagador, polissa.name)
            )
        vals.update({
            'data_accio': today,
            # required with contracts <= 1 year
            # (eventuales, tempoerada, obreas...)
            # 'data_final': today,
            'tipus_document': vat_info['document_type'],
            'codi_document': vat_info['vat'],
            'persona': vat_info['is_enterprise'] and 'J' or 'F',
            'fiscal_address_id': polissa.direccio_pagament.id,
            'pot_ids': [(6, 0, created_pots)],
            'tarifaATR': polissa.tarifa.codi_ocsum,
            'control_potencia': (polissa.facturacio_potencia == 'max' and '2'
                                 or '1'),
            'tipus_autoconsum': "00",
            'cnae': polissa.cnae and polissa.cnae.id or False,
            'tipus_contracte': polissa.contract_type or '01',
        })
        if vals['tipus_contracte'] == '09':
            vals.update(
                {'expected_consumption': round(polissa.expected_consumption, 2)}
            )

        if check_contracte_is_eventual(polissa):
            vals.update({
                'data_final': polissa.data_baixa or datetime.now().strftime('%Y-%m-%d'),
            })

        # dades de contacte
        if not vals.get('cont_nom'):
            if vals['persona'] == 'J':
                nom = vals['nom']
            else:
                nom = "{0} {1} {2}".format(
                    vals['nom'], vals['cognom_1'], vals['cognom_2']
                )
            vals.update({
                'cont_nom': nom,
                'cont_telefons': vals['telefons'],
            })

        return self.create(cursor, uid, vals, context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        codi_t = step.tarifaATR
        tarif_obj = self.pool.get("giscedata.polissa.tarifa")
        tarif_id = tarif_obj.get_tarifa_from_ocsum(cursor, uid, codi_t, context)
        tarifa_name = tarif_obj.read(cursor, uid, tarif_id, ['name'])['name']
        pots = ""
        for p in step.pot_ids:
            pots += "{0}: {1}, ".format(p.name, p.potencia)
        res = _(u'Tarifa {0}: {1}.').format(tarifa_name, pots[0:-2])
        if step.control_potencia:
            new_fp = get_description(step.control_potencia, 'TABLA_51')
            res += _(u" Facturacio {0}").format(new_fp)
        return res

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'activacio_cicle': fields.selection(TABLA_8, u"Activació",
                                            required=True),
        'data_accio': fields.date(u"Data prevista del canvi o alta"),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE'),
        # contracte
        'tipus_autoconsum': fields.selection(TABLA_113, u"Tipus Autoconsum",
                                             required=True),
        'tipus_contracte': fields.selection(TABLA_9,
                                            u"Tipus de contracte",
                                            required=True),
        'data_final': fields.date(u"Data final contracte"),
        'solicitud_tensio': fields.selection(TABLA_117, u"Tensió Sol.licitada"),
        # client
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2,
                                           required=True),
        'codi_document': fields.char(u'Codi document', size=11,
                                     required=True),
        'persona': fields.selection(PERSONA, u'Persona', required=True),
        'nom': fields.char(u'Nom de la persona o societat', size=45,
                           required=True),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9, required=False),
        'ind_direccio_fiscal': fields.selection(TABLA_11,
                                                'Indicador direcció',
                                                required=True),
        'fiscal_address_id': fields.many2one('res.partner.address',
                                             'Adreça fiscal'),
        # mesura
        'equip_aportat_client': fields.selection(TABLA_20,
                                                 'Equip Aportat client'),
        'tipus_equip_mesura': fields.selection(TABLA_22,
                                               'Tipus equip de mesura'),
        # altres
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa', readonly=True,
                                      required=True),
        'control_potencia': fields.selection(CONTROL_POTENCIA,
                                             u"Control de potència"),
        # Comentaris
        'comentaris': fields.text(u"Comentaris", size=4000),
        # Dades de contacte addicionals
        'cont_nom': fields.char(u'Nom de la persona o societat', size=45,
                                required=True),
        'cont_prefix': fields.char(u"Prefix telefònic", size=2),
        'cont_telefon': fields.char(u"Telèfon", size=9, required=False),
        # Tecnical Documentacion (Doc Tecnica)
        'dt_add': fields.boolean(u"Afegir Dades CIE"),
        'dt_cie_electronico': fields.selection(SINO, u"Cie Electronico"),
        'dt_cie_codigo': fields.char(u"Código CIE", size=35),
        'dt_cie_papel_potenciainstbt': fields.integer(u"Potència Instalada"),
        'dt_cie_papel_data_emissio': fields.date(u"Data Emissió"),
        'dt_cie_papel_data_caducitat': fields.date(u"Data Caducitat"),
        'dt_tipus_codi_instalador': fields.selection(
            TIPUS_DOCUMENT_INST_CIE, u"Tipus codi Instal·lador"
        ),
        'dt_codi_instalador': fields.char(u"Codi instal·lador", size=9),
        'dt_cie_papel_tensio_suministre': fields.selection(
            TABLA_64, u"Tensió suministre"
        ),
        'dt_cie_papel_tipus_suministre': fields.selection(
            TABLA_62, u"Tipus Suministre"
        ),
        'dt_cie_sello_elec': fields.char(u"Segell Electrónic", size=35),
        'dt_add_apm': fields.boolean(u"Afegir Dades APM"),
        'dt_apm_codigo': fields.char(u"Código APM", size=35),
        'dt_apm_potenciainstat': fields.integer(u"Potència Instalada"),
        'dt_apm_data_emissio': fields.date(u"Data Emissió APM"),
        'dt_apm_data_caducitat': fields.date(u"Data Caducitat APM"),
        'dt_apm_tensio_suministre': fields.selection(
            TABLA_64, u"Tensió suministre"
        ),
        'dt_apm_tipus_codi_instalador': fields.selection(
            TIPUS_DOCUMENT_INST_CIE, u"Tipus codi Instal·lador APM"
        ),
        'dt_apm_codi_instalador': fields.char(u"Codi instal·lador APM", size=9),

        'expected_consumption': fields.float(u"Consum pactat", readonly=True,
                                             help="Aquest camp calcula el "
                                                  "consum total pactat per "
                                                  "contractes eventuals sense "
                                                  "comptador"),
    }

    _defaults = {
        'ind_direccio_fiscal': lambda *a: 'S',
        'activacio_cicle': lambda *a: 'A',
        'tipus_autoconsum': lambda *a: '00',
        'tipus_contracte': lambda *a: '01',
        'control_potencia': lambda *a: 1,
        # Tecnical Documentation
        'dt_add': lambda *a: False,
        'dt_add_apm': lambda *a: False,
        'dt_cie_electronico': lambda *a: 'N',
        'dt_cie_papel_tensio_suministre': lambda *a: '02',
        'dt_apm_tensio_suministre': lambda *a: '02',
        'dt_cie_papel_tipus_suministre': lambda *a: 'UV',
        'dt_tipus_codi_instalador': lambda *a: 'nif',
        'dt_apm_tipus_codi_instalador': lambda *a: 'nif',
    }

GiscedataSwitchingA3_01()


class GiscedataSwitchingA3_02(osv.osv):
    """Classe pel pas 02
    """
    _name = "giscedata.switching.a3.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if context and context.get("rebuig", False):
            return []
        return ['03', '04', '05', '06']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_02,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def onchange_tipus_activacio(self, cursor, uid, ids, tipus, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids)

        data = {}

        if tipus == 'L0':
            try:
                fi_lot = pas.sw_id.cups_polissa_id.lot_facturacio.data_final
                data_act = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                            timedelta(days=1)).strftime('%Y-%m-%d')

                data = {'data_activacio': data_act}
            except:
                pass
        return {'value': data}

    def onchange_rebuig(self, cursor, uid, ids, rebuig, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        data = {}
        if not rebuig:
            pas = self.browse(cursor, uid, ids)
            if pas.sw_id.cups_polissa_id and pas.sw_id.cups_polissa_id.tarifa:
                polissa = pas.sw_id.cups_polissa_id
                data = {'tarifaATR': polissa.tarifa.codi_ocsum}
        return {'value': data}

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 02
           de rebuig
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 02
           d'acceptació
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = a3.MensajeAceptacionAlta()
        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades acceptació
        dades = a3.DatosAceptacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
            'actuacion_campo': pas.actuacio_camp,
        })
        # Contracte
        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = a3.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'potencias_contratadas': potencies,
        })
        contracte = a3.Contrato()
        contracte_vals = {
            'condiciones_contractuales': condicions,
        }
        if pas.tipus_activacio:
            contracte_vals.update(
                {'tipo_activacion_prevista': pas.tipus_activacio})
        if pas.data_activacio:
            contracte_vals.update(
                {'fecha_activacion_prevista': pas.data_activacio})
        contracte.feed(contracte_vals)
        # Acceptacio
        acceptacio = a3.AceptacionAlta()
        acceptacio.feed({
            'datos_aceptacion': dades,
            'contrato': contracte,
        })

        msg.feed({
            'cabecera': capcalera,
            'aceptacion_alta': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref, pas.receptor_id.ref,
                                   pas='02')
        return (fname, str(msg))

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A3, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, pots,
                                              context=context)

        vals.update({
            'rebuig': False,
            'data_acceptacio': xml.datos_aceptacion.fecha_aceptacion,
            'actuacio_camp': xml.datos_aceptacion.actuacion_campo,
            # Contracte - pas 02
            'tarifaATR': xml.contrato.tarifa_atr,
            'pot_ids': [(6, 0, created_pots)],
            'tipus_activacio': xml.contrato.tipo_activacion_prevista,
            'data_activacio': xml.contrato.fecha_activacion_prevista,
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML A3, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'rebuig': True,
            'data_acceptacio': today,
            'data_rebuig': today,
            'actuacio_camp': 'N'
        })
        if polissa:
            # Podem agafar la informació del contracte perqué al ser una alta
            # la informació del contracte i la solicitada en el pas 01 es la
            # mateixa (o hauria de ser-ho, en tot cas es la que la
            # distribuidora activarà)
            created_pots = header_obj.create_pots(cursor, uid, False,
                                                  polissa=polissa,
                                                  context=context)
            helper_obj = self.pool.get("giscedata.switching.helpers")
            activacio, data_activacio = helper_obj.get_data_prevista_activacio(
                cursor, uid, sw, context
            )
            vals.update({
                'rebuig': False,
                # Contracte - pas 02
                'tarifaATR': polissa.tarifa.codi_ocsum,
                'tipus_activacio': activacio,
                'data_activacio': data_activacio,
                'pot_ids': [(6, 0, created_pots)],
            })
        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res


    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template


    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.a3.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        pas02_obj = self.pool.get("giscedata.switching.a3.02")
        pas_02_id = pas02_obj.search(cursor, uid,
                                     [("header_id.sw_id", "=", sw_id)])
        if not len(pas_02_id):
            return datetime.today()

        sw_obj = self.pool.get("giscedata.switching")
        pol_obj = self.pool.get("giscedata.polissa")
        tari_obj = self.pool.get("giscedata.polissa.tarifa")

        info_accep = pas02_obj.read(
            cursor, uid, pas_02_id[0],
            ['data_acceptacio', 'tipus_activacio', 'data_activacio']
        )
        # En data fixa el limit es la data que es va marcar
        if info_accep['tipus_activacio'] == 'F0':
            return datetime.strptime(info_accep['data_activacio'], '%Y-%m-%d')

        # Altrament depén de alta/baixa tensió
        data_acceptacio = info_accep['data_acceptacio']
        data_acceptacio = datetime.strptime(data_acceptacio, '%Y-%m-%d')
        sw_info = sw_obj.read(cursor, uid, sw_id, ['cups_polissa_id'])
        pol_info = pol_obj.read(cursor, uid,
                                sw_info['cups_polissa_id'][0], ['tarifa'])
        tari_info = tari_obj.read(cursor, uid, pol_info['tarifa'][0], ['tipus'])
        tipus = tari_info['tipus']
        if tipus == 'BT':
            return workday(data_acceptacio, 5)
        else:
            return data_acceptacio + timedelta(days=15)

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        'actuacio_camp': fields.selection(SINO, u"Treball de camp"),
        # Contracte - pas 02
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa'),
        'tipus_activacio': fields.selection(TIPUS_ACTIVACIO,
                                            u"Activació", size=2),
        'data_activacio': fields.date(u"Data de canvi o alta"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True,),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }

GiscedataSwitchingA3_02()


class GiscedataSwitchingA3_03(osv.osv):

    _name = 'giscedata.switching.a3.03'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '03'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['03', '04', '05', '06']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_03,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = a3.MensajeIncidenciasATRDistribuidor()
        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Incidencies
        inc_obj = self.pool.get("giscedata.switching.incidencia")
        incidencies = inc_obj.generar_xml(cursor, uid, pas)
        dades = a3.IncidenciasATRDistribuidor()
        dades.feed({
            'fecha_incidencia': pas.data_incidencia,
            'fecha_prevista_accion': pas.data_prevista_accio,
            'incidencia_list': incidencies,
        })

        msg.feed({
            'cabecera': capcalera,
            'incidencias_atr_distribuidor': dades,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref, pas.receptor_id.ref,
                                   pas='03')
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        inc_obj = self.pool.get('giscedata.switching.incidencia')

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml)
        incidencies = inc_obj.create_from_xml(cursor, uid, xml)

        vals.update({
            'data_incidencia': xml.fecha_incidencia,
            'data_prevista_accio': xml.fecha_prevista_accion,
            'incidencia_ids': [(6, 0, incidencies)],
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        return self.create(cursor, uid, vals, context=context)

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        'data_incidencia': fields.date(u"Data Incidència", required=True),
        'data_prevista_accio': fields.date(u"Data Prevista Acció"),
    }

    _defaults = {
        'data_incidencia': lambda *a: datetime.today(),
    }

GiscedataSwitchingA3_03()


class GiscedataSwitchingA3_04(osv.osv):

    _name = 'giscedata.switching.a3.04'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '04'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_04,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '04'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
            """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'data_rebuig': datetime.today(),
        })
        return self.create(cursor, uid, vals)

    def _get_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Sempre és rebuig"""
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        return {}.fromkeys(ids, True)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.a3.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        # Rebuig
        'rebuig': fields.function(_get_rebuig, 'Sol·licitud rebutjada',
                                  type='boolean', method=True),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

GiscedataSwitchingA3_04()


class GiscedataSwitchingA3_05(osv.osv):
    """Classe pel pas 05
    """
    _name = "giscedata.switching.a3.05"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '05'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == 'distri':
            return ['06']
        elif where == 'comer':
            return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_05,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, ids, context=None):

        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context)
        sw = pas.sw_id
        msg = a3.MensajeActivacionAlta()

        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades activació
        dades_activacio = a3.DatosActivacion()
        dades_activacio.feed({
            'fecha': pas.data_activacio,
        })
        # Contracte
        idcontracte = a3.IdContrato()
        nom_ctr = filter(lambda x: x.isdigit(), sw.cups_polissa_id.name)
        idcontracte.feed({
            'cod_contrato': nom_ctr
        })

        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = a3.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'periodicidad_facturacion': pas.periodicitat_facturacio,
            'tipode_telegestion': pas.tipus_telegestio,
            'potencias_contratadas': potencies,
            'modo_control_potencia': pas.control_potencia,
            'tension_del_suministro': pas.tensio_suministre,
            'marca_medida_con_perdidas': pas.marca_medida_bt,
            'porcentaje_perdidas': pas.perdues,
        })
        if pas.marca_medida_bt == 'S':
            condicions.feed({'vas_trafo': int(pas.kvas_trafo * 1000)})
        contracte = a3.Contrato()
        contracte.feed({
            'id_contrato': idcontracte,
            'tipo_autoconsumo': pas.tipus_autoconsum,
            'tipo_contrato_atr': pas.tipus_contracte,
            'condiciones_contractuales': condicions,
        })

        punts_mesura = pas.header_id.generar_xml_pm(pas)

        # Activacio
        activacio = a3.ActivacionAlta()
        activacio.feed({
            'datos_activacion': dades_activacio,
            'contrato': contracte,
            'puntos_de_medida': punts_mesura
        })
        msg.feed({
            'cabecera': capcalera,
            'activacion_alta': activacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        xml_pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, xml_pots,
                                              context=context)
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        pm_ids = header_obj.create_from_xml_pm(cursor, uid, xml,
                                               context=context)
        tipo_telegestion_xml = xml.contrato.tipo_de_telegestion
        vals.update({
            # dades activació
            'data_activacio': xml.datos_activacion.fecha,
            'tipus_autoconsum': xml.contrato.tipo_autoconsumo,
            'tipus_contracte': xml.contrato.tipo_contrato_atr,
            'tarifaATR': xml.contrato.tarifa_atr,
            'periodicitat_facturacio': xml.contrato.periodicidad_facturacion,
            'tipus_telegestio': tipo_telegestion_xml,
            'pot_ids': [(6, 0, created_pots)],
            'pm_ids': [(6, 0, pm_ids)],
            'contracte_atr': xml.contrato.cod_contrato or '',
            'control_potencia': xml.contrato.modo_control_potencia,
            'tensio_suministre': xml.contrato.tension_del_suministro,
        })
        if xml.contrato.marca_medida_con_perdidas == 'S':
            vals.update({
                'marca_medida_bt': 'S',
                'kvas_trafo': float(xml.contrato.vas_trafo) / 1000,
                'perdues': xml.contrato.porcentaje_perdidas,
            })
        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)

        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')

        control_potencia = (polissa.facturacio_potencia == 'max' and '2' or '1')
        created_pots = header_obj.create_pots(cursor, uid, False,
                                              polissa=polissa,
                                              context=context)
        pm_ids = header_obj.dummy_create_pm(cursor, uid, sw, context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        tipus_tg = CONV_T109_T111.get('{0:02d}'.format(int(polissa.tg)), '03')

        vals.update({
            'data_activacio': polissa.data_alta or today,
            'tipus_autoconsum': polissa.autoconsumo,
            'tipus_contracte': polissa.contract_type or '01',
            'tarifaATR': polissa.tarifa.codi_ocsum,
            'periodicitat_facturacio': '0%d' % polissa.facturacio,
            'tipus_telegestio': '{0}'.format(tipus_tg),
            'control_potencia': control_potencia,
            'contracte_atr': polissa.name or '',
            'pot_ids': [(6, 0, created_pots)],
            'pm_ids': [(6, 0, [pm_ids])],
            'tensio_suministre': (
                (polissa.tensio_normalitzada and
                 polissa.tensio_normalitzada.cnmc_code) or '01'
            ),
        })
        # If codi_ocsum is 011 (3.1A) and there is 'trafo' in polissa then
        # the tarif is 3.1A LB and we add 'marca_medida_bt' and 'kvas_trafo'
        mesura = polissa.tarifa.mesura_ab
        if polissa.tarifa.codi_ocsum == '011' and mesura == 'B':
            vals.update({
                'marca_medida_bt': 'S',
                'kvas_trafo': polissa.trafo,
                'perdues': 4.0,
            })
        else:
            vals.update({
                'marca_medida_bt': 'N',
            })
        return self.create(cursor, uid, vals)

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades activació
        'data_activacio': fields.date(u"Data d'activació"),
        'tipus_autoconsum': fields.selection(TABLA_113, u"Tipus Autoconsum"),
        'tipus_contracte': fields.selection(TABLA_9, u"Tipus de contracte"),
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa', readonly=True),
        'periodicitat_facturacio': fields.selection(TABLA_108,
                                                    u"Periodicitat facturació",
                                                    size=2, required=True),
        'contracte_atr': fields.char(u'Codi contracte ATR', size=12,
                                     readonly=True,
                                     help=u'Codi contracte a la distribuidora'),
        'control_potencia': fields.selection(CONTROL_POTENCIA,
                                             u"Control de potència"),
        'tipus_telegestio': fields.selection(TABLA_111,
                                             u'Tipus de Telegestió'),
        'marca_medida_bt': fields.selection([('S', 'Si'), ('N', 'No')],
                                            u'Marca Mesura BT amb Perdues',
                                            readonly=True),
        'kvas_trafo': fields.float(u"KVAs Trafo"),
        'tensio_suministre': fields.selection(TABLA_64, u'Tensió Suministre',
                                              required=True),
        'perdues': fields.float(u"Percentatge Pèrdues")
    }

GiscedataSwitchingA3_05()


class GiscedataSwitchingA3_06(osv.osv):
    """Classe pel pas 06
    """
    _name = "giscedata.switching.a3.06"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '06'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['07']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_06,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return anulacio_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return anulacio_obj.create_from_xml(cursor, uid, sw_id, xml,
                                            context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        return anulacio_obj.dummy_create(cursor, uid, sw_id,
                                         context=ctx)

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
    }

GiscedataSwitchingA3_06()


class GiscedataSwitchingA3_07(osv.osv):
    """Classe pel pas 07
    """
    _name = "giscedata.switching.a3.07"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '07'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A3', self._nom_pas,
                                      context=context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_A3_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['03', '04', '05']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA3_07,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return acc_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml(self, cursor, uid, pas_id, context=None):

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name,
                    'nom_pas': self._nom_pas})

        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        if not context:
            context = {}
        if xml.fecha_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.dummy_create(cursor, uid, sw_id, context=ctx)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.a3.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }

GiscedataSwitchingA3_07()
