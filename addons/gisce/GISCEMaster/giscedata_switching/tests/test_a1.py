# -*- coding: utf-8 -*-
from __future__ import absolute_import

from datetime import datetime

from addons import get_module_resource
from destral.transaction import Transaction
from .common_tests import TestSwitchingImport
from destral.patch import PatchNewCursors


class TestA1(TestSwitchingImport):

    def generate_step_01(self, txn):
        a1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a101_new.xml')
        with open(a1_xml_path, 'r') as f:
            a1_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        self.switch(txn, 'distri')
        self.activar_polissa_CUPS(txn)
        uid = txn.user
        cursor = txn.cursor
        with PatchNewCursors():
            res = sw_obj.importar_xml(
                cursor, uid, a1_xml, 'a101.xml'
            )
        return res

    def test_load_a1_01(self):

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.generate_step_01(txn)

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            self.assertEqual(len(res), 1)

            a1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a1.proces_id.name, 'A1')
            self.assertEqual(a1.step_id.name, '01')
            self.assertEqual(a1.partner_id.ref, '17')
            self.assertEqual(a1.company_id.ref, '1234')
            self.assertEqual(a1.codi_sollicitud, '201608120830')
            self.assertEqual(a1.data_sollicitud, '2016-08-12')

            a101 = sw_obj.get_pas(cursor, uid, a1)
            self.assertEqual(a101.cau, 'ES1234000000000001JN0FA001')
            self.assertEqual(a101.seccio_registre, '2')
            self.assertEqual(a101.subseccio, 'a0')
            self.assertEqual(a101.collectiu, True)
            # Subministraments
            self.assertEqual(a101.subministraments[0].cups, 'ES1234000000000001JN0F')
            self.assertEqual(a101.subministraments[0].tipus_cups, '01')
            self.assertEqual(a101.subministraments[0].ref_cadastre_cups, '1234567890qwertyuiop')
            self.assertEqual(a101.subministraments[1].cups, 'ES1234000000000002JN0F')
            self.assertEqual(a101.subministraments[1].tipus_cups, '01')
            self.assertEqual(a101.subministraments[1].ref_cadastre_cups, '1234567890qwertyuiop')
            self.assertEqual(a101.subministraments[2].cups, 'ES1234000000000003JN0F')
            self.assertEqual(a101.subministraments[2].tipus_cups, '01')
            self.assertEqual(a101.subministraments[2].ref_cadastre_cups, '1234567890qwertyuiop')
            self.assertEqual(a101.subministraments[3].cups, 'ES1234000000000004JN0F')
            self.assertEqual(a101.subministraments[3].tipus_cups, '01')
            self.assertEqual(a101.subministraments[3].ref_cadastre_cups, '1234567890qwertyuiop')
            # Generador
            self.assertEqual(a101.generadors[0].cil, 'ES1234000000000001JN0F001')
            self.assertEqual(a101.generadors[0].tec_generador, 'b12')
            self.assertEqual(a101.generadors[0].combustible, 'Diesel')
            self.assertEqual(a101.generadors[0].pot_installada_gen, 100.0)
            self.assertEqual(a101.generadors[0].tipus_installacio, '01')
            self.assertEqual(a101.generadors[0].esquema_mesura, 'A')
            self.assertEqual(a101.generadors[0].ssaa, True)
            self.assertEqual(a101.generadors[0].ref_cadastre_inst_gen, '1234567890qwertyuidf')
            self.assertEqual(a101.generadors[0].utm_x, '100')
            self.assertEqual(a101.generadors[0].utm_y, '200')
            self.assertEqual(a101.generadors[0].utm_fus, '40')
            self.assertEqual(a101.generadors[0].utm_banda, 'E')
            self.assertEqual(a101.generadors[0].tipus_identificador, 'NI')
            self.assertEqual(a101.generadors[0].identificador, 'ES111111111H')
            self.assertEqual(a101.generadors[0].nom, 'Juan')
            self.assertEqual(a101.generadors[0].cognom_1, 'López')
            self.assertEqual(a101.generadors[0].email, 'mail_falso@dominio.com')

            self.assertEqual(len(a101.generadors[0].telefons), 3)
            self.assertEqual(a101.generadors[0].telefons[0].numero, '911111111')
            self.assertEqual(a101.generadors[0].telefons[1].numero, '611111111')
            self.assertEqual(a101.generadors[0].telefons[2].numero, '622222222')

            self.assertEqual(a101.generadors[0].fiscal_address_id.nv, 'Pau Casals')
            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_creation_a1_02(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_id, _ = self.generate_step_01(txn)

            swinfo_obj = self.openerp.pool.get('giscedata.switching.step.info')
            swinfo_id = swinfo_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
            pas_info = swinfo_obj.read(cursor, uid, swinfo_id[0], ['pas_id'])['pas_id']
            model_obj, model_id = pas_info.split(',')
            step_id = int(model_id)

            step_obj = self.openerp.pool.get('giscedata.switching.a1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a101 = step_obj.browse(cursor, uid, step_id)
            a1 = sw_obj.browse(cursor, uid, a101.sw_id.id)

            self.switch(txn, 'distri')
            self.create_step(
                cursor, uid, a1, 'A1', '02', context=None
            )

            a1 = sw_obj.browse(cursor, uid, a101.sw_id.id)
            a102 = a1.get_pas()

            self.assertEqual(a1.proces_id.name, 'A1')
            self.assertEqual(a1.step_id.name, '02')
            self.assertEqual(a1.partner_id.ref, '17')
            self.assertEqual(a1.company_id.ref, '1234')

            self.assertEqual(a102.cau, 'ES1234000000000001JN0FA001')
            self.assertEqual(a102.sub_seccio, 'a0')

            today = datetime.strftime(datetime.now(), '%Y-%m-%d')
            self.assertEqual(a102.data_acceptacio, today)

    def test_additional_info_a1(self):

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.generate_step_01(txn)

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201608120830')
            ])

            a1 = sw_obj.browse(cursor, uid, res[0])
            inf = u'(A) Alta,  - CAU: ES1234000000000001JN0FA001'
            self.assertEqual(inf, a1.additional_info)
