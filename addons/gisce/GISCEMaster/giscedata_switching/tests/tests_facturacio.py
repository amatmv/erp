# -*- coding: utf-8 -*-
from expects import expect
from expects import contain

from destral import testing
from destral.transaction import Transaction

from .common_tests import TestSwitchingImport

class TestsInvoiceValidation(TestSwitchingImport):
    def setUp(self):
        fact_obj = self.model('giscedata.facturacio.factura')
        line_obj = self.model('giscedata.facturacio.factura.linia')
        warn_obj = self.model(
            'giscedata.facturacio.validation.warning.template'
        )
        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        for fact_id in fact_obj.search(cursor, uid, []):
            fact_obj.write(cursor, uid, fact_id, {'state': 'open'})

        # We make sure that all warnings are active
        warn_ids = warn_obj.search(
            cursor, uid, [], context={'active_test': False}
        )
        warn_obj.write(cursor, uid, warn_ids, {'active': True})

    def tearDown(self):
        self.txn.stop()

    def model(self, model_name):
        return self.openerp.pool.get(model_name)

    def get_fixture(self, model, reference):
        imd_obj = self.model('ir.model.data')
        return imd_obj.get_object_reference(
            self.txn.cursor, self.txn.user,
            model,
            reference
        )[1]

    def validation_warnings_message(self, factura_id):
        vali_obj = self.model('giscedata.facturacio.validation.validator')
        warn_obj = self.model('giscedata.facturacio.validation.warning')
        warning_ids = vali_obj.validate_invoice(
            self.txn.cursor, self.txn.user,
            factura_id
        )
        warning_vals = warn_obj.read(
            self.txn.cursor, self.txn.user,
            warning_ids,
            ['name','message']
        )
        return dict([(warn['name'], warn['message']) for warn in warning_vals])

    def validation_warnings(self, factura_id):
        return self.validation_warnings_message(factura_id).keys()

    def create_complete_case(self, polissa_id,
            case, step, status, substep = None
        ):
        sw_obj = self.model('giscedata.switching')
        step_obj = self.model(
            'giscedata.switching.{}.{}'.format(case.lower(), step))

        cursor = self.txn.cursor
        uid = self.txn.user

        step_id = self.create_case_and_step(
            cursor, uid, polissa_id, case, step)
        step_d = step_obj.browse(cursor, uid, step_id)

        case_id = step_d.sw_id.id

        sw_obj.write(cursor, uid, step_d.sw_id.id, {'state': status})

        if substep:
            substep_obj = self.model('giscedata.subtipus.reclamacio')
            substep_ids = substep_obj.search(
                cursor, uid, [('name','=',substep)])
            if substep_ids:
                step_obj.write(cursor, uid, step_id, {
                    'subtipus_id': substep_ids[0],
                    })

        # refresh after modifications
        step_d = step_obj.browse(cursor, uid, step_id)
        case_d = sw_obj.browse(cursor, uid, case_id)
        return (case_d,step_d)

    def test_create_complete_case__C101open(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C1', '01', 'open')

        self.assertEqual(r1.proces_id.name, 'C1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__C101closed(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C1', '01', 'done')

        self.assertEqual(r1.proces_id.name, 'C1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'done')

    def test_create_complete_case__C201open(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C2', '01', 'open')

        self.assertEqual(r1.proces_id.name, 'C2')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__C201closed(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'C2', '01', 'done')

        self.assertEqual(r1.proces_id.name, 'C2')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'done')

    def test_create_complete_case__M101open(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'M1', '01', 'open')

        self.assertEqual(r1.proces_id.name, 'M1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__M101closed(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'M1', '01', 'done')

        self.assertEqual(r1.proces_id.name, 'M1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r1.state, 'done')

    def test_create_complete_case__R101open009(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '009')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '009')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101open036(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '036')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '036')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101open002(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '002')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '002')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101open036(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'open', '002')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '002')
        self.assertEqual(r1.state, 'open')

    def test_create_complete_case__R101done009(self):
        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)
        contract_id = self.get_contract_id(self.txn)
        r1,r101 = self.create_complete_case(
            contract_id, 'R1', '01', 'done', '009')

        self.assertEqual(r1.proces_id.name, 'R1')
        self.assertEqual(r1.step_id.name, '01')
        self.assertEqual(r101.subtipus_id.name, '009')
        self.assertEqual(r1.state, 'done')

    def test_open_r1_subtypes__withoutR1(self):
        factura_id = self.get_fixture('giscedata_facturacio', 'factura_0006')

        warnings = self.validation_warnings(factura_id)

        expect(warnings).to_not(contain('F012'))

    def test_open_r1_subtypes__withOpenR1Subtype009(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'open', '009')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F012'))

    def test_open_r1_subtypes__withPendingR1Subtype009(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'pending', '009')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F012'))

    def test_open_r1_subtypes__withOpenR1Subtype036(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]

        self.create_complete_case(polissa_id, 'R1', '01', 'open', '036')
        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F012'))

    def test_open_r1_subtypes__withPendingR1Subtype036(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]

        self.create_complete_case(polissa_id, 'R1', '01', 'pending', '036')
        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F012'))

    def test_open_r1_subtypes__withOpenR1SubtypeNon009or036(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'open', '002')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).not_to(contain('F012'))

    def test_open_r1_subtypes__withPendingR1SubtypeNon009or036(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'pending', '013')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).not_to(contain('F012'))

    def test_open_r1_subtypes__withClosedR1(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'done', '009')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).not_to(contain('F012'))

    def test_open_r1_subtypes__with2R1OneOkOneNotOk(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'done', '009')
        self.create_complete_case(polissa_id, 'R1', '01', 'open', '009')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).to(contain('F012'))

    def test_open_r1_subtypes__withManyR1AllNotOk(self):
        fact_obj = self.model('giscedata.facturacio.factura')

        self.switch(self.txn, 'comer')
        self.change_polissa_comer(self.txn)
        self.update_polissa_distri(self.txn)

        fact_id = self.get_fixture('giscedata_facturacio', 'factura_0006')
        fact_vals = fact_obj.read(
            self.txn.cursor, self.txn.user, fact_id, ['polissa_id']
        )

        polissa_id = fact_vals['polissa_id'][0]
        self.create_complete_case(polissa_id, 'R1', '01', 'done', '009')
        self.create_complete_case(polissa_id, 'R1', '01', 'done', '009')
        self.create_complete_case(polissa_id, 'R1', '01', 'done', '036')
        self.create_complete_case(polissa_id, 'R1', '01', 'done', '034')
        self.create_complete_case(polissa_id, 'R1', '01', 'pending', '022')
        self.create_complete_case(polissa_id, 'R1', '01', 'cancel', '015')
        self.create_complete_case(polissa_id, 'R1', '01', 'draft', '012')
        self.create_complete_case(polissa_id, 'R1', '01', 'open', '002') #only one open alowed
        self.create_complete_case(polissa_id, 'R1', '01', 'pending', '037')
        self.create_complete_case(polissa_id, 'R1', '01', 'draft', '040')
        self.create_complete_case(polissa_id, 'R1', '01', 'cancel', '001')

        warnings = self.validation_warnings(fact_id)

        expect(warnings).not_to(contain('F012'))
