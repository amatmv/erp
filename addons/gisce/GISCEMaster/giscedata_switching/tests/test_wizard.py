# -*- coding: utf-8 -*-
from __future__ import absolute_import

from datetime import datetime
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from .common_tests import TestSwitchingImport
from osv.osv import except_osv

import json

power_invoice_modes = {'1': 'icp', '2': 'max'}


class TestSwitchingWizardNotification(TestSwitchingImport):

    def consultar_mails(self, cursor, uid):
        mail_obj = self.openerp.pool.get('poweremail.mailbox')
        mail_ids = mail_obj.search(cursor, uid, [])
        mails = mail_obj.read(cursor, uid, mail_ids, [])
        return mails

    def test_02_and_04_or_05_only_notify_last(self):
        conf_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')
        sw_ayudame_obj = self.openerp.pool.get('giscedata.switching.helpers')
        wiz_notificacion_obj = self.openerp.pool.get(
            'giscedata.atr.notification.mail.wizard'
        )
        head_obj = self.openerp.pool.get('giscedata.switching.step.header')
        power_template_obj = self.openerp.pool.get('poweremail.templates')
        pw_account_obj = self.openerp.pool.get('poweremail.core_accounts')

        with Transaction().start(self.database) as txn:

            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            conf_obj.set(
                cursor, uid,
                'sw_mail_user_notification_on_activation', 'all'
            )

            b1_02_template_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching', 'notification_atr_B1_02_acceptacio'
            )[1]

            b1_04_template_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching', 'notification_atr_B1_04_acceptacio'
            )[1]

            pw_id = pw_account_obj.create(cursor, uid, {
                'name': 'test',
                'user': 1,
                'email_id': 'test@email',
                'smtpserver': 'smtp.gmail.com',
                'smtpport': '587',
                'company': 'no',
                'state': 'approved',
            })

            power_template_obj.write(cursor, uid, [b1_02_template_id, b1_04_template_id],
                                     {'enforce_from_account': pw_id})

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)

            step_b1_02_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '02'
            )
            step_02_obj = self.openerp.pool.get('giscedata.switching.b1.02')
            b102 = step_02_obj.browse(cursor, uid, step_b1_02_id)

            step_b1_04_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '04', b102.header_id.sw_id.id
            )

            step_04_obj = self.openerp.pool.get('giscedata.switching.b1.04')

            b104 = step_04_obj.browse(cursor, uid, step_b1_04_id)

            sw_ids = [b102.header_id.sw_id.id]

            ctx = {
                'active_ids': sw_ids
            }

            header_ids = [b102.header_id.id, b104.header_id.id]

            head_reads = head_obj.read(
                cursor, uid, header_ids, ['notificacio_pendent']
            )

            self.assertTrue(all(h['notificacio_pendent'] for h in head_reads))

            wz_notify_id = wiz_notificacion_obj.create(cursor, uid, {}, context=ctx)

            wz_notify = wiz_notificacion_obj.browse(cursor, uid, wz_notify_id)

            wz_notify.send_notification_mail(context=ctx)

            wz_notify = wiz_notificacion_obj.browse(cursor, uid, wz_notify_id)

            head_reads = head_obj.read(
                cursor, uid, header_ids, ['notificacio_pendent']
            )

            self.assertTrue(all(not h['notificacio_pendent'] for h in head_reads))

            self.assertIn(
                u"Successfully updated 2 steps",
                wz_notify.output
            )

            send_mail = self.consultar_mails(cursor, uid)

            template_sub = send_mail[0]['pem_subject']
            tmplate_obj = self.openerp.pool.get('poweremail.templates')

            object = step_04_obj.browse(cursor, uid, step_b1_04_id
                                        ).header_id.sw_id

            expected_template_sub = tmplate_obj.read(
                cursor, uid, b1_04_template_id, ['def_subject']
            )['def_subject'].format(object=object).replace('$', '')

            self.assertEqual(template_sub, expected_template_sub)

    def test_02_and_04_or_05_only_notify_last_A3_04_rebuig_enabled(self):
        conf_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')
        sw_ayudame_obj = self.openerp.pool.get('giscedata.switching.helpers')
        wiz_notificacion_obj = self.openerp.pool.get(
            'giscedata.atr.notification.mail.wizard'
        )
        head_obj = self.openerp.pool.get('giscedata.switching.step.header')
        power_template_obj = self.openerp.pool.get('poweremail.templates')
        pw_account_obj = self.openerp.pool.get('poweremail.core_accounts')

        with Transaction().start(self.database) as txn:

            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            conf_obj.set(
                cursor, uid,
                'sw_mail_user_notification_on_activation', 'all'
            )

            a3_02_template_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching', 'notification_atr_A3_02_acceptacio'
            )[1]

            a3_04_template_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching', 'notification_atr_A3_04'
            )[1]

            pw_id = pw_account_obj.create(cursor, uid, {
                'name': 'test',
                'user': 1,
                'email_id': 'test@email',
                'smtpserver': 'smtp.gmail.com',
                'smtpport': '587',
                'company': 'no',
                'state': 'approved',
            })

            power_template_obj.write(
                cursor, uid,
                [a3_02_template_id, a3_04_template_id],
                {'enforce_from_account': pw_id}
            )

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)

            step_02_obj = self.openerp.pool.get('giscedata.switching.a3.02')
            step_a3_02_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '02'
            )
            a302 = step_02_obj.browse(cursor, uid, step_a3_02_id)

            step_a3_04_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '04', a302.header_id.sw_id.id
            )

            step_04_obj = self.openerp.pool.get('giscedata.switching.a3.04')

            a304 = step_04_obj.browse(cursor, uid, step_a3_04_id)

            sw_rebuig_obj = self.openerp.pool.get('giscedata.switching.rebuig')
            swr_id = sw_rebuig_obj.create(cursor, uid, {
                'motiu_rebuig': 1,
                'desc_rebuig': 'rej'
            })

            notify_obj = self.openerp.pool.get('giscedata.switching.notify')
            nid = notify_obj.create(cursor, uid, {
                'step_id': a304.header_id.sw_id.step_id.id,
                'proces_id': a304.header_id.sw_id.proces_id.id,
                'notify_text': 'Has sido rechazado',
                'rebuig_ids': [(6, 0, [1])],
                'active': True
            })

            a304.header_id.write({'rebuig_ids': [(6, 0, [swr_id])]})

            sw_ids = [a302.header_id.sw_id.id]

            ctx = {
                'active_ids': sw_ids
            }

            header_ids = [a302.header_id.id, a304.header_id.id]

            head_reads = head_obj.read(
                cursor, uid, header_ids, ['notificacio_pendent']
            )

            self.assertTrue(all(h['notificacio_pendent'] for h in head_reads))

            wz_notify_id = wiz_notificacion_obj.create(cursor, uid, {}, context=ctx)

            wz_notify = wiz_notificacion_obj.browse(cursor, uid, wz_notify_id)

            wz_notify.send_notification_mail(context=ctx)

            wz_notify = wiz_notificacion_obj.browse(cursor, uid, wz_notify_id)

            head_reads = head_obj.read(
                cursor, uid, header_ids, ['notificacio_pendent']
            )

            self.assertTrue(all(not h['notificacio_pendent'] for h in head_reads))

            self.assertIn(
                u"Successfully updated 2 steps",
                wz_notify.output
            )

            send_mail = self.consultar_mails(cursor, uid)
            template_sub = send_mail[0]['pem_subject']
            tmplate_obj = self.openerp.pool.get('poweremail.templates')

            object = step_04_obj.browse(cursor, uid, step_a3_04_id
                                        ).header_id.sw_id

            expected_template_sub = tmplate_obj.read(
                cursor, uid, a3_04_template_id, ['def_subject']
            )['def_subject'].format(object=object).replace('$', '')

            self.assertEqual(template_sub, expected_template_sub)

    def test_02_and_04_or_05_only_notify_last_A3_05(self):
        conf_obj = self.openerp.pool.get('res.config')
        imd_obj = self.openerp.pool.get('ir.model.data')
        sw_ayudame_obj = self.openerp.pool.get('giscedata.switching.helpers')
        wiz_notificacion_obj = self.openerp.pool.get(
            'giscedata.atr.notification.mail.wizard'
        )
        head_obj = self.openerp.pool.get('giscedata.switching.step.header')
        power_template_obj = self.openerp.pool.get('poweremail.templates')
        pw_account_obj = self.openerp.pool.get('poweremail.core_accounts')

        with Transaction().start(self.database) as txn:

            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            conf_obj.set(
                cursor, uid,
                'sw_mail_user_notification_on_activation', 'all'
            )

            a3_02_template_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching', 'notification_atr_A3_02_acceptacio'
            )[1]

            a3_05_template_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_switching', 'notification_atr_A3_05'
            )[1]

            pw_id = pw_account_obj.create(cursor, uid, {
                'name': 'test',
                'user': 1,
                'email_id': 'test@email',
                'smtpserver': 'smtp.gmail.com',
                'smtpport': '587',
                'company': 'no',
                'state': 'approved',
            })

            power_template_obj.write(
                cursor, uid,
                [a3_02_template_id, a3_05_template_id],
                {'enforce_from_account': pw_id}
            )

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)

            step_02_obj = self.openerp.pool.get('giscedata.switching.a3.02')
            step_a3_02_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '02'
            )
            a302 = step_02_obj.browse(cursor, uid, step_a3_02_id)

            step_a3_05_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '05', a302.header_id.sw_id.id
            )

            step_05_obj = self.openerp.pool.get('giscedata.switching.a3.05')

            a305 = step_05_obj.browse(cursor, uid, step_a3_05_id)

            sw_ids = [a302.header_id.sw_id.id]

            ctx = {
                'active_ids': sw_ids
            }

            header_ids = [a302.header_id.id, a305.header_id.id]

            head_reads = head_obj.read(
                cursor, uid, header_ids, ['notificacio_pendent']
            )

            self.assertTrue(all(h['notificacio_pendent'] for h in head_reads))

            wz_notify_id = wiz_notificacion_obj.create(cursor, uid, {}, context=ctx)

            wz_notify = wiz_notificacion_obj.browse(cursor, uid, wz_notify_id)

            wz_notify.send_notification_mail(context=ctx)

            wz_notify = wiz_notificacion_obj.browse(cursor, uid, wz_notify_id)

            head_reads = head_obj.read(
                cursor, uid, header_ids, ['notificacio_pendent']
            )

            self.assertTrue(all(not h['notificacio_pendent'] for h in head_reads))

            self.assertIn(
                u"Successfully updated 2 steps",
                wz_notify.output
            )

            send_mail = self.consultar_mails(cursor, uid)

            template_sub = send_mail[0]['pem_subject']
            tmplate_obj = self.openerp.pool.get('poweremail.templates')

            object = step_05_obj.browse(cursor, uid, step_a3_05_id
                                        ).header_id.sw_id

            expected_template_sub = tmplate_obj.read(
                cursor, uid, a3_05_template_id, ['def_subject']
            )['def_subject'].format(object=object).replace('$', '')

            self.assertEqual(template_sub, expected_template_sub)


class TestSwitchingWizards(TestSwitchingImport):

    def test_create_wizard_B1_not_allowed_due_to_nocutoff(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')

            uid = txn.user
            cursor = txn.cursor

            wiz_obj = self.openerp.pool.get('giscedata.switching.b101.wizard')
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = contract_obj.search(cursor, uid, [('state', '=', 'esborrany')])[0]
            contract = contract_obj.browse(cursor, uid, contract_id)

            if not contract.nocutoff:
                contract_obj.write(cursor, uid, contract_id, {'nocutoff': 1})

            contract.send_signal(['validar', 'contracte'])

            context = {'cas': 'B1', 'contract_id': contract_id}

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            wiz_obj.write(cursor, uid, [wiz_id], {'motive': '01'})

            def generate_atr_cases():
                wiz.genera_casos_atr(context=context)
            self.assertRaises(except_osv, generate_atr_cases)

            wiz = wiz.browse()[0]

            self.assertEqual(wiz.state, 'init')
            self.assertEqual(wiz.casos_generats, '[]')

    def test_create_wizard_mod_con_C2_instance_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'C2', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.proces, 'C2')
            self.assertEqual(wiz.tariff, contract.tarifa.codi_ocsum)
            self.assertEqual(
                power_invoice_modes[wiz.power_invoicing],
                contract.facturacio_potencia
            )
            self.assertFalse(wiz.new_contract)
            # wizard default owner is the contract "owner"
            self.assertEqual(wiz.owner, contract.titular)
            self.assertEqual(wiz.owner_pre, contract.titular)
            # wizard default vat is the contract "owner" vat
            self.assertEqual(wiz.vat, contract.titular.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, contract.direccio_pagament.id)

    def test_create_wizard_mod_con_M1_instance_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            new_contract_id = self.get_contract_id(txn, xml_id="polissa_0005")
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            imd_obj = self.openerp.pool.get('ir.model.data')
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]
            new_contract.write({
                'cups': contract.cups.id,
                'tarifa': 2,
                'facturacio_potencia': 'icp',
                'titular': other_id,
                'pagador': another_id
            })

            # We have a new contract with the same cups but we are NOT in an owner
            # change so it should use info of old contrtact
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.proces, 'M1')
            self.assertEqual(wiz.tariff, contract.tarifa.codi_ocsum)
            self.assertEqual(
                power_invoice_modes[wiz.power_invoicing],
                contract.facturacio_potencia
            )
            self.assertEqual(wiz.owner, contract.pagador)
            self.assertEqual(wiz.owner_pre, contract.pagador)
            self.assertEqual(wiz.vat, contract.pagador.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, contract.direccio_pagament.id)

    def test_create_wizard_mod_con_M1_use_new_contract_info(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard'
            )
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            new_contract_id = self.get_contract_id(txn, xml_id="polissa_0005")
            new_contract = contract_obj.browse(cursor, uid, new_contract_id)
            imd_obj = self.openerp.pool.get('ir.model.data')
            other_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_agrolait'
            )[1]
            another_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_c2c'
            )[1]
            new_contract.write({
                'cups': contract.cups.id,
                'tarifa': 2,
                'facturacio_potencia': 'icp',
                'titular': other_id,
                'pagador': another_id
            })

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            # We have a new contract with the same cups and we are in an owner
            # change so it should use info of new contrtact
            wiz.write({'change_adm': True, 'change_atr': False})
            # We simulate the onchange_type
            vals = wiz.onchange_type(
                False, True, wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False, "M1"
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.proces, 'M1')
            self.assertEqual(wiz.tariff, new_contract.tarifa.codi_ocsum)
            self.assertEqual(
                power_invoice_modes[wiz.power_invoicing],
                new_contract.facturacio_potencia
            )
            self.assertEqual(wiz.owner, new_contract.pagador)
            self.assertEqual(wiz.owner_pre, new_contract.pagador)

            self.assertEqual(wiz.vat, new_contract.pagador.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, new_contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, new_contract.direccio_pagament.id)
            self.assertEqual(wiz.generate_new_contract, "exists")

            # We change "generate_new_contract" to create, the values should
            # change to the old contract ones
            wiz.write({"generate_new_contract": "create"})
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            vals = wiz.onchange_new_contact(
                wiz.contract.id, wiz.generate_new_contract,
                wiz.new_contract and wiz.new_contract.id or False, "M1"
            )
            wiz.write(vals['value'])
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.tariff, contract.tarifa.codi_ocsum)
            self.assertEqual(
                power_invoice_modes[wiz.power_invoicing],
                contract.facturacio_potencia
            )
            self.assertEqual(wiz.owner, contract.pagador)
            self.assertEqual(wiz.owner_pre, contract.pagador)
            self.assertEqual(wiz.vat, contract.pagador.vat[2:])
            self.assertEqual(wiz.direccio_notificacio.id, contract.direccio_pagament.id)
            self.assertEqual(wiz.direccio_pagament.id, contract.direccio_pagament.id)

    def test_create_wizard_mod_con_M1_subrogacy_NO_new_contract(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            uid = txn.user
            cursor = txn.cursor
            conf_obj = self.openerp.pool.get("res.config")
            config_id = conf_obj.search(cursor, uid, [('name', '=', 'sw_m1_owner_change_auto')])
            conf_obj.write(cursor, uid, config_id, {'value': '1'})
            wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            partner_obj = self.openerp.pool.get("res.partner")
            imd_obj = self.openerp.pool.get('ir.model.data')
            bank_obj = self.openerp.pool.get("res.partner.bank")
            payment_type_obj = self.openerp.pool.get("payment.type")
            group_payment_obj = self.openerp.pool.get("payment.mode")

            contract_id = self.get_contract_id(txn)
            contract = contract_obj.browse(cursor, uid, contract_id)
            contract.send_signal(['validar', 'contracte'])

            # Busquem un partner que no sigui l'actual, sera el nou titular
            new_titular_id = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_11'
            )[1]
            new_titular = partner_obj.browse(cursor, uid, new_titular_id)
            new_titular.write({"vat": "ES00000000T"})
            new_titular = partner_obj.browse(cursor, uid, new_titular_id)
            # Una direccio de un partner diferent al nou titular
            new_pagador = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_15'
            )[1]
            new_direccio_pagament = imd_obj.get_object_reference(
                cursor, uid, 'base', 'res_partner_address_14'
            )[1]
            new_direccio_notificacio = new_direccio_pagament
            # Obtenir un nou compte bancari i tipus de pagament
            journal_id = imd_obj.get_object_reference(
                cursor, uid, 'account', 'sales_journal'
            )[1]
            new_bank_id = imd_obj.get_object_reference(
                cursor, uid, 'base_iban', 'res_partner_bank_iban_0001'
            )[1]
            bank_obj.write(cursor, uid, new_bank_id, {'partner_id': new_pagador})
            new_type = payment_type_obj.create(cursor, uid, {"code": "SWDEMO", "name": "SW DEMO"})
            new_group = group_payment_obj.create(cursor, uid, {
                'name': 'Mode of Payment',
                'bank_id': new_bank_id,
                'journal': journal_id,
                'type': new_type,
                "code": "AA"
            })

            # Ara que ja tenim totes les dades del nou titular, podem crear el cas
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id}
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({
                'change_atr': False, 'change_adm': True, 'activacio_cicle': 'L',
                'change_retail_tariff': False, 'retail_tariff': False,
                'owner_change_type': 'S',
                'direccio_pagament': new_direccio_pagament,
                'direccio_notificacio': new_direccio_notificacio,
                'bank': new_bank_id, 'payment_mode_id': new_group,
                'tipo_pago': new_type, 'owner': new_titular_id
            })
            wiz.genera_casos_atr(context={'pol_id': contract_id})

            created_cases = sw_obj.search(
                cursor, uid, [('cups_polissa_id', '=', contract_id),
                              ('proces_id.name', '=', wiz.proces)]
            )

            self.assertEqual(len(created_cases), 1)
            sw = sw_obj.browse(cursor, uid, created_cases[0])
            pas01 = sw.get_pas()
            # El pas 01 hauria de tenir guardada la informacio del nou titular
            # en els nous camps auxiliars
            self.assertEqual(pas01.activacio_cicle, 'L')
            self.assertEqual(pas01.canvi_titular, "S")
            self.assertEqual(pas01.direccio_pagament.id, new_direccio_pagament)
            self.assertEqual(pas01.direccio_notificacio.id, new_direccio_notificacio)
            self.assertEqual(pas01.bank.id, new_bank_id)
            self.assertEqual(pas01.tipo_pago.id, new_type)
            self.assertEqual(pas01.payment_mode_id.id, new_group)
            self.assertEqual("ES"+pas01.codi_document, new_titular.vat)
            old_contract = contract_obj.browse(cursor, uid, contract_id)
            self.assertFalse(old_contract.facturacio_suspesa)

    def test_create_switching_wizard_comer_instance_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.wizard'
            )

            contract_id = self.get_contract_id(txn)

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={
                    'active_id': contract_id,
                    'active_ids': [contract_id]
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertIsNotNone(wiz)

    def test_create_switching_wizard_distri_instance_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.wizard'
            )

            contract_id = self.get_contract_id(txn)

            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={
                    'active_id': contract_id,
                    'active_ids': [contract_id]
                }
            )
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertIsNotNone(wiz)

    def test_get_email_to_returns_correct_email(self):
        wiz_obj = self.openerp.pool.get('giscedata.switching.wizard')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        case_obj = self.openerp.pool.get('crm.case')

        xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(xml_path, 'r') as f:
            xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            # We import an invalid xml so that it automatically creates step 02
            sw_id, info = sw_obj.importar_xml(
                cursor, uid, xml, 'xml.xml'
            )

            sw = sw_obj.browse(cursor, uid, sw_id)
            case_obj.write(
                cursor, uid, sw.case_id.id, {'email_from': 'test@test.com'}
            )

            # For a normal step
            res = wiz_obj._get_email_to(cursor, uid, sw_id, '02')
            self.assertEqual(res, 'test@test.com')

            # For a old company step
            res = wiz_obj._get_email_to(cursor, uid, sw_id, '06')
            self.assertEqual(res, 'info@camptocamp.com')

    # ATR Case from contract
    def test_launches_r1_wizard_from_contract(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.polissa.c101.wizard'
            )

            contract_id = self.get_contract_id(txn)

            # With 1 contract it's used wizard.create.r1
            context = {
                'active_id': contract_id,
                'active_ids': [contract_id]
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'init')

            context.update({'proces': 'R1'})
            res = wiz_obj.genera_casos_atr(
                cursor, uid, [wiz_id], context=context
            )

            self.assertIsInstance(res, dict)
            self.assertEqual(
                res.get('res_model', False), 'wizard.create.r1'
            )
            self.assertEqual(
                res.get('context', False),
                "{'polissa_id': %s}" % (
                    contract_id
                )
            )

            # With more than 1 contract it's used wizard.r1.from.contract
            context = {
                'active_id': contract_id,
                'active_ids': [contract_id, contract_id]
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'init')

            context.update({'proces': 'R1'})
            res = wiz_obj.genera_casos_atr(
                cursor, uid, [wiz_id], context=context
            )

            self.assertIsInstance(res, dict)
            self.assertEqual(
                res.get('res_model', False), 'wizard.r101.from.multiple.contracts'
            )
            self.assertEqual(
                res.get('context', False),
                "{'contract_id': %s, 'contract_ids': %s}" % (
                    contract_id, [contract_id, contract_id]
                )
            )

    def test_support_functions_r1_wizard_from_contract(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('wizard.r101.from.contract')
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            contract_vals = contract_obj.read(
                cursor, uid, contract_id, ['data_alta', 'data_ultima_lectura'])

            # get_contract_ids
            context = {
                'active_id': 1,
                'active_ids': [1],
            }
            self.assertTupleEqual(wiz_obj.get_contract_ids(context), (1, [1]))
            context.update({
                'contract_id': 2,
                'contract_ids': [2],
            })
            self.assertTupleEqual(wiz_obj.get_contract_ids(context), (2, [2]))

            #  get_default_comment
            msg = 'Procés R1-9999 no suportat'
            with self.assertRaisesRegexp(ValueError, msg):
                wiz_obj.get_default_comment(
                    cursor, uid, 'R1-9999', contract_id, context=context
                )
            msg_date = (
                contract_vals['data_ultima_lectura']
                or contract_vals['data_alta']
            )
            expected_comment = (
                u"De este suministro no tenemos lecturas desde 2016-01-01.\n"
                u"El Real Decreto 1718/2012 indica claramente la obligación "
                u"de tomar una lectura real cada dos meses. Solicitamos "
                u"lecturas reales o, en caso contrario, una copia del aviso "
                u"de imposible lectura  conforme no ha sido posible tomarla. "
                u"\nGracias.".format(msg_date))
            self.assertEqual(
                wiz_obj.get_default_comment(
                    cursor, uid, 'R1-02006', contract_id, context=context
                ),
                expected_comment
            )

            # get_config_vals_per_contract
            context.update({'proces': 'R1-02006'})
            self.assertDictEqual(
                wiz_obj.get_config_vals_per_contract(
                    cursor, uid, contract_id, context=context
                ),
                {
                    'type': '02',
                    'subtype': '006',
                    'objector': '06',
                    'comments': expected_comment
                }
            )

    def test_create_r1_wizard_from_contract(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('wizard.r101.from.contract')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            context = {
                'contract_id': contract_id,
                'contract_ids': [contract_id],
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.whereiam, 'comer')
            self.assertEqual(wiz.state, 'init')

            # Button create case
            context.update({'proces': 'R1-02006'})
            wiz_obj.action_create_atr_case(
                cursor, uid, [wiz_id], context=context
            )

            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            self.assertEqual(wiz.state, 'done')
            cases_ids = json.loads(wiz.generated_cases)
            self.assertEqual(len(cases_ids), 1)

            # Button open cases
            res = wiz_obj.action_cases(
                cursor, uid, [wiz_id], context=context
            )
            self.assertIsInstance(res, dict)
            self.assertEqual(
                res.get('res_model', False), 'giscedata.switching'
            )
            self.assertEqual(
                res.get('domain', False),
                "[('id','in', {0})]".format(cases_ids)
            )

    # ATR Case from invoice
    def test_create_wizard_atr_not_suported_case(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'wizard.atr.from.invoice'
            )

            invoice_id = self.get_invoice_id(txn)
            context = {
                'active_id': invoice_id,
                'active_ids': [invoice_id]
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.has_dso_invoices, False)
            self.assertEqual(wiz.state, 'init')

            context.update({'proces': 'R1-9999'})
            wiz.generate_atr_cases(context=context)

            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertIn(
                "0001.0001/F: Cas R1-9999 no suportat",
                wiz.info
            )

    # for R1-01 (0209 and 0236) cases
    def test_create_wizard_atr_from_invoice_1_out_invoice_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'wizard.atr.from.invoice'
            )

            invoice_id = self.get_invoice_id(txn)
            context = {
                'active_id': invoice_id,
                'active_ids': [invoice_id]
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.has_dso_invoices, False)
            self.assertEqual(wiz.state, 'init')

            context.update({'proces': 'R1-02009'})
            res = wiz.generate_atr_cases(context=context)

            self.assertIsInstance(res, dict)
            self.assertEqual(
                res.get('res_model', False), 'giscedata.switching.r101.wizard'
            )
            self.assertEqual(
                res.get('context', False),
                "{'invoice_id': %s, 'proces': '%s'}" % (invoice_id, 'R1-02009')
            )

    def test_create_wizard_atr_from_invoice_1_in_invoice_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'wizard.atr.from.invoice'
            )

            invoice_id = self.get_invoice_id(txn, 'factura_0003')
            context = {
                'active_id': invoice_id,
                'active_ids': [invoice_id]
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)

            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.has_dso_invoices, True)
            self.assertEqual(wiz.state, 'init')

            context.update({'proces': 'R1-02009'})

            res = wiz.generate_atr_cases(context=context)
            self.assertIsInstance(res, dict)
            self.assertEqual(
                res.get('res_model', False), 'giscedata.switching.r101.wizard'
            )
            self.assertEqual(
                res.get('context', False),
                "{'invoice_id': %s, 'proces': '%s'}" % (invoice_id, 'R1-02009')
            )

    def test_create_wizard_atr_from_autoconsum(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('giscedata.switching.d101.wizard')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            context = {
                'contract_id': contract_id,
                'contract_ids': [contract_id],
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.whereiam, 'distri')
            self.assertEqual(wiz.state, 'init')

            autoconsum_id = self.crear_autoconsum(txn)

            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')

            cups_id = polissa_obj.read(cursor, uid, contract_id, ['cups'])['cups']
            cups_obj.write(cursor, uid, cups_id[0], {'autoconsum_id': autoconsum_id})

            # Button create case
            context.update({'autoconsum_id': autoconsum_id})
            wiz.genera_casos_atr(context=context)
            wiz = wiz.browse()[0]

            sw_obj = self.openerp.pool.get('giscedata.switching')

            d1_ids = eval(wiz.casos_generats)
            d1 = sw_obj.browse(cursor, uid, d1_ids[0])

            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '01')

            d101 = sw_obj.get_pas(cursor, uid, d1)
            self.assertEqual(d101.motiu_canvi, '04')
            self.assertEqual(d101.periodicitat_facturacio, '01')
            today = datetime.strftime(datetime.now(), '%Y-%m-%d')
            self.assertEqual(d101.data_activacio, today)

    def test_create_wizard_step_2_accept(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('giscedata.switching.d101.wizard')
            wiz_step_obj = self.openerp.pool.get('wizard.create.step')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            context = {
                'contract_id': contract_id,
                'contract_ids': [contract_id],
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.whereiam, 'distri')
            self.assertEqual(wiz.state, 'init')

            autoconsum_id = self.crear_autoconsum(txn)

            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')

            cups_id = polissa_obj.read(cursor, uid, contract_id, ['cups'])['cups']
            cups_obj.write(cursor, uid, cups_id[0], {'autoconsum_id': autoconsum_id})

            # Button create case
            context.update({'autoconsum_id': autoconsum_id})
            wiz.genera_casos_atr(context=context)
            wiz = wiz.browse()[0]

            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'comer')

            d1_ids = eval(wiz.casos_generats)
            cups_id = sw_obj.read(cursor, uid, d1_ids[0], ['cups_id'])['cups_id']
            sw_obj.write(cursor, uid, d1_ids[0], {'cups_id': cups_id[0]})
            d1 = sw_obj.browse(cursor, uid, d1_ids[0])

            context.update({'active_ids': d1_ids})
            params = {
                'step': '02',
                'option': 'A',
            }
            wiz_step_id = wiz_step_obj.create(cursor, uid, params, context=context)
            wiz_step = wiz_step_obj.browse(cursor, uid, wiz_step_id)
            wiz_step.action_create_steps(context=context)
            wiz_step = wiz_step.browse()[0]

            d1_02_ids = eval(wiz_step.sw_ids)
            d1 = sw_obj.browse(cursor, uid, d1_02_ids[0])
            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '02')

            d102 = sw_obj.get_pas(cursor, uid, d1)
            self.assertEqual(d102.motiu_rebuig, '')
            self.assertEqual(d102.rebuig, False)

    def test_create_wizard_step_2_reject(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('giscedata.switching.d101.wizard')
            wiz_step_obj = self.openerp.pool.get('wizard.create.step')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            context = {
                'contract_id': contract_id,
                'contract_ids': [contract_id],
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.whereiam, 'distri')
            self.assertEqual(wiz.state, 'init')

            autoconsum_id = self.crear_autoconsum(txn)

            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            cups_obj = self.openerp.pool.get('giscedata.cups.ps')

            cups_id = polissa_obj.read(cursor, uid, contract_id, ['cups'])['cups']
            cups_obj.write(cursor, uid, cups_id[0], {'autoconsum_id': autoconsum_id})

            # Button create case
            context.update({'autoconsum_id': autoconsum_id})
            wiz.genera_casos_atr(context=context)
            wiz = wiz.browse()[0]

            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'comer')

            d1_ids = eval(wiz.casos_generats)
            cups_id = sw_obj.read(cursor, uid, d1_ids[0], ['cups_id'])['cups_id']
            sw_obj.write(cursor, uid, d1_ids[0], {'cups_id': cups_id[0]})

            motiu_obj = self.openerp.pool.get('giscedata.switching.motiu.rebuig')
            motiu_rebuig_id = motiu_obj.search(cursor, uid, [('name', '=', 'F1')])
            motiu_rebuig_text = motiu_obj.read(cursor, uid, motiu_rebuig_id[0], ['text'])['text']

            context.update({'active_ids': d1_ids})
            params = {
                'step': '02',
                'option': 'R',
                'motiu_rebuig': motiu_rebuig_id[0],
                'step_is_rejectable': True
            }
            wiz_step_id = wiz_step_obj.create(cursor, uid, params, context=context)
            wiz_step = wiz_step_obj.browse(cursor, uid, wiz_step_id)
            wiz_step.action_create_steps(context=context)
            wiz_step = wiz_step.browse()[0]

            d1_02_ids = eval(wiz_step.sw_ids)
            d1 = sw_obj.browse(cursor, uid, d1_02_ids[0])
            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '02')

            d102 = sw_obj.get_pas(cursor, uid, d1)
            self.assertEqual(d102.motiu_rebuig, motiu_rebuig_text)
            self.assertEqual(d102.rebuig, True)


    def crear_autoconsum(self, txn):
        uid = txn.user
        cursor = txn.cursor

        autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum')
        vals = {
            'cau': 'ES1234000000000001JN0FA001',
            'seccio_registre': '1',
            'data_alta': '2019-10-10'
        }
        autoconsum_id = autoconsum_obj.create(cursor, uid, vals)

        generador_obj = self.openerp.pool.get('giscedata.autoconsum.generador')
        vals = {
            'autoconsum_id': autoconsum_id,
            'cil': 'ES1234000000000001JN0F001',
            'data_alta': '2019-01-01',
            'tec_generador': 'b11',
            'pot_instalada_gen': 100,
            'tipus_installacio': '01',
            'utm_x': 100,
            'utm_y': 100,
            'utm_fus': 30,
            'partner_id': 2,
        }
        generador_obj.create(cursor, uid, vals)

        partner_obj = self.openerp.pool.get('res.partner')
        partner_obj.write(cursor, uid, 2, {
            'vat': 'ESE56152556',

        })

        part_address_obj = self.openerp.pool.get('res.partner.address')
        part_address_id = part_address_obj.search(cursor, uid, [('partner_id', '=', 2)])
        part_address_obj.write(cursor, uid, part_address_id, {'phone': '+34 123 123 123'})
        return autoconsum_id

    def test_create_wizard_atr_from_invoice_many_invoices(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get(
                'wizard.atr.from.invoice'
            )

            invoice_xml_ids = [
                'factura_0001', 'factura_0002', 'factura_0003'
            ]

            self.update_polissa_distri(txn)

            invoice_ids = []
            for xml_id in invoice_xml_ids:
                invoice_ids.append(self.get_invoice_id(txn, xml_id))

            context = {
                'active_id': invoice_ids[0],
                'active_ids': invoice_ids
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.has_dso_invoices, True)
            self.assertEqual(wiz.state, 'init')

            context.update({'proces': 'R1-02009'})
            wiz.generate_atr_cases(context=context)

            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'done')
            self.assertEqual(len(json.loads(wiz.generated_cases)), 1)

    # R1_01 wizard
    def test_create_wizard_r1_invoice_0209_load_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('giscedata.switching.r101.wizard')
            invoice_obj = self.openerp.pool.get('giscedata.facturacio.factura')

            invoice_id = self.get_invoice_id(txn, 'factura_0003')
            invoice_obj.write(cursor, uid, invoice_id, {'reference': '0030003'})
            context = {
                'invoice_id': invoice_id,
                'proces': 'R1-02009',
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'init')
            self.assertEqual(wiz.whereiam, 'comer')
            self.assertEqual(wiz.proces, 'R1-02009')

            vals = wiz_obj.get_config_vals(
                cursor, uid, [wiz_id], context=context
            )
            default_comment = (
                u"Solicitamos revisión de la lectura facturada a partir de "
                u"histórico de lecturas y consiguiente refacturación."
            )
            self.assertEqual(vals.get('type', False), '02')
            self.assertEqual(vals.get('subtype', False), '009')
            self.assertEqual(vals.get('objector', False), '06')
            self.assertEqual(vals.get('invoice_number', False), '0030003')
            self.assertEqual(vals.get('comments', False), default_comment)
            # no measures
            self.assertNotIn('codi_dh', vals)
            self.assertEqual(vals['measure_date'], False)
            self.assertEqual(len(vals.get('measures', False)), 0)

    def test_create_wizard_r1_invoice_0236_load_ok(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            wiz_obj = self.openerp.pool.get('giscedata.switching.r101.wizard')
            invoice_obj = self.openerp.pool.get('giscedata.facturacio.factura')

            invoice_id = self.get_invoice_id(txn, 'factura_0003')
            invoice_obj.write(cursor, uid, invoice_id, {'reference': '0030003'})
            context = {
                'invoice_id': invoice_id,
                'proces': 'R1-02036',
            }

            wiz_id = wiz_obj.create(cursor, uid, {}, context=context)
            wiz = wiz_obj.browse(cursor, uid, wiz_id)

            self.assertEqual(wiz.state, 'init')
            self.assertEqual(wiz.whereiam, 'comer')
            self.assertEqual(wiz.proces, 'R1-02036')

            vals = wiz_obj.get_config_vals(
                cursor, uid, [wiz_id], context=context
            )
            default_comment = (
                u"Aportamos lectura de titular para refacturación"
            )
            self.assertEqual(vals.get('type', False), '02')
            self.assertEqual(vals.get('subtype', False), '036')
            self.assertEqual(vals.get('objector', False), '06')
            self.assertEqual(vals.get('invoice_number', False), '0030003')
            self.assertEqual(vals.get('comments', False), default_comment)
            # no measures
            self.assertNotIn('codi_dh', vals)
            self.assertEqual(vals['measure_date'], False)
            self.assertEqual(len(vals.get('measures', False)), 0)

    def test_warning_creating_already_used_cups_A3(self):
        with Transaction().start(self.database) as txn:

            # Configurem un contracte i el validem
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            pol_obj = self.openerp.pool.get('giscedata.polissa')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)

            # Busquem una factura amb el mateix cups i intentem activar el pas
            # A3 amb el contracte d'aquesta factura.
            wiz_obj = self.openerp.pool.get('giscedata.switching.a301.wizard')
            invoice_obj = self.openerp.pool.get('giscedata.facturacio.factura')

            invoice_id = self.get_invoice_id(txn, 'factura_0003')
            contract_id = invoice_obj.read(
                cursor, uid, invoice_id, ['polissa_id']
            )['polissa_id'][0]
            context = {
                'contract_id': contract_id,
                'proces': 'A3',
            }
            onchange = wiz_obj.onchange_activacio(cursor, uid,
                                                  invoice_id, context)

            # Com que hi ha una factura amb el mateix CUPS activa, ens salta a
            # l'onchange el Warning que valida aquest test
            self.assertIsNotNone(onchange['warning'])

    def test_if_owner_changes_on_m1_wizard(self):
        with Transaction().start(self.database) as txn:

            # Configurem l'entorn del test (som una comer)
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            # Creem un contracte actiu per probar la validació
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)

            # Obrim el Wizard i l'executem, si fa raise és que funciona bé ja
            # que no s'ha canviat l'owner
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard')
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id})

            context = {'pol_id': contract_id,
                       'cas': 'M1',
                       'active_ids': [1],
                       'active_id': False}
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({'owner': 4, 'change_atr': False, 'change_adm': True})

            self.assertRaises(except_osv, wiz.genera_casos_atr, context=context)

    def test_if_power_or_tariff_changes_on_m1_wizard(self):
        with Transaction().start(self.database) as txn:

            # Configurem l'entorn del test (som una comer)
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            # Creem un contracte actiu per probar la validació
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)

            # Obrim el Wizard i l'executem, si fa raise és que funciona bé ja
            # que no s'ha canviat ni la potencia ni la tarifa
            wiz_obj = self.openerp.pool.get(
                'giscedata.switching.mod.con.wizard')
            wiz_id = wiz_obj.create(
                cursor, uid, {}, context={'cas': 'M1', 'pol_id': contract_id})

            context = {'pol_id': contract_id,
                       'cas': 'M1',
                       'active_ids': [1],
                       'active_id': False}
            wiz = wiz_obj.browse(cursor, uid, wiz_id)
            wiz.write({'owner': 4, 'change_atr': True, 'change_adm': False})

            self.assertRaises(except_osv, wiz.genera_casos_atr, context=context)
