# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from datetime import date, datetime
from destral import testing
from expects import expect, raise_error
from osv.orm import except_orm
from gestionatr.input.messages import R1, Message
from gestionatr.input.messages.C2 import Direccion
from workdays import *
from destral.patch import PatchNewCursors


class TestR1(TestSwitchingImport):

    def test_load_r1_01(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '01')
            self.assertEqual(r1.partner_id.ref, '4321')
            self.assertEqual(r1.company_id.ref, '1234')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')
            self.assertEqual(r1.codi_sollicitud, '201602231255')
            self.assertEqual(r1.data_sollicitud, '2016-02-23')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(r1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            r101 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r101.tipus, '05')
            self.assertEqual(r101.subtipus_id.name, '039')
            self.assertEqual(r101.reforigen, '30968')
            self.assertEqual(r101.data_limit, '2016-02-22')
            self.assertEqual(r101.prioritari, 'S')
            self.assertEqual(r101.tipus_document, 'NI')
            self.assertEqual(r101.codi_document, 'B82420654')
            self.assertEqual(r101.persona, 'J')
            self.assertEqual(r101.nom, 'Camptocamp')
            self.assertEqual(len(r101.telefons), 3)
            self.assertEqual(r101.telefons[2].numero, '333444555')
            self.assertEqual(r101.email, 'rrunner@acme.com')
            self.assertEqual(r101.ind_direccio_fiscal, 'S')
            self.assertEqual(r101.fiscal_address_id.id_municipi.ine, '17079')
            self.assertEqual(r101.tipus_reclamant, '06')
            self.assertEqual(r101.rec_tipus_document, 'NI')
            self.assertEqual(r101.rec_codi_document, 'A86774353')
            self.assertEqual(r101.rec_persona, 'J')
            self.assertEqual(len(r101.rec_telefons), 2)
            self.assertEqual(r101.rec_telefons[1].numero, '666777888')
            self.assertEqual(r101.rec_nom, 'ACME Corporation')
            self.assertEqual(r101.rec_email, 'reclamaatr@acme.es')
            self.assertEqual(r101.comentaris, 'no calcula sus consumos desea revisio y facturas')
            self.assertEqual(len(r101.reclamacio_ids), 1)
            vr1 = r101.reclamacio_ids[0]
            self.assertEqual(vr1.num_expedient_escomesa, '11111')
            self.assertEqual(vr1.num_expedient_frau, '22222')
            self.assertEqual(vr1.data_incident, '2016-02-08')
            self.assertEqual(vr1.num_factura, '243615')
            self.assertEqual(vr1.tipus_concepte_facturat, '01')
            self.assertEqual(vr1.codi_incidencia, '02')
            self.assertEqual(vr1.codi_sollicitud, '201602231236')
            self.assertEqual(vr1.parametre_contractacio, '01')
            self.assertEqual(vr1.concepte_disconformitat, '100')
            self.assertEqual(vr1.tipus_atencio_incorrecte, '05')
            self.assertEqual(vr1.iban, '4444222211113333')
            self.assertEqual(vr1.codi_sollicitud_reclamacio, '201602231236')
            self.assertEqual(vr1.data_inici, '2016-01-01')
            self.assertEqual(vr1.data_fins, '2016-02-09')
            self.assertEqual(vr1.import_reclamat, 204.49)
            self.assertEqual(vr1.data_lectura, '2016-08-31')
            self.assertEqual(vr1.codi_dh, '1')
            self.assertEqual(vr1.cont_nom, 'Camptocamp')
            self.assertEqual(len(vr1.cont_telefons), 2)
            self.assertEqual(vr1.cont_telefons[1].numero, '55512345')
            self.assertEqual(vr1.cont_email, 'perico@acme.com')
            self.assertEqual(vr1.desc_ubicacio, 'Destino')
            self.assertEqual(vr1.municipi.ine, '17079')
            self.assertEqual(vr1.provincia.code, '17')
            self.assertEqual(vr1.poblacio, '17079')
            self.assertEqual(vr1.codi_postal, '17001')
            self.assertEqual(len(vr1.lect_ids), 1)
            lect1 = vr1.lect_ids[0]
            self.assertEqual(lect1.name, 'P1')
            self.assertEqual(lect1.magnitud, 'AE')
            self.assertEqual(lect1.lectura, 0.00)

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_load_r1_01_generate_xml_without_client_mail(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            # Change sw_r1_from_invoice_mail_add value to 0
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            res_part_addr_obj = self.openerp.pool.get('res.partner.address')
            direccio_pagament_id = contract_obj.read(
                cursor, uid, contract_id, ['direccio_pagament']
            )['direccio_pagament'][0]
            res_part_addr_obj.write(
                cursor, uid, direccio_pagament_id,
                {'email': 'prova_email@fakemail.com'})
            polissa = contract_obj.browse(cursor, uid, contract_id)
            self.assertEqual(
                polissa.direccio_pagament.email, 'prova_email@fakemail.com')

            conf_obj = self.openerp.pool.get('res.config')
            conf_var = 'sw_r1_from_invoice_mail_add'
            conf_var_id = conf_obj.search(
                cursor, uid, [('name', '=', conf_var)])[0]
            conf_obj.write(cursor, uid, conf_var_id, {'value': 0})
            invoice_mail_add = bool(
                int(conf_obj.get(cursor, uid, conf_var, None)))
            self.assertEqual(invoice_mail_add, False)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.r1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            r101 = step_obj.browse(cursor, uid, step_id)
            r1 = sw_obj.browse(cursor, uid, r101.sw_id.id)

            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '01')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(r101.tipus, '02')
            self.assertEqual(r101.subtipus_id.name, '009')
            self.assertEqual(r101.tipus_reclamant, '06')
            self.assertEqual(r101.email, False)

    def test_load_r1_01_generate_xml_with_client_mail(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            # Change sw_r1_from_invoice_mail_add value to 0
            contract_obj = self.openerp.pool.get('giscedata.polissa')
            res_part_addr_obj = self.openerp.pool.get('res.partner.address')
            direccio_pagament_id = contract_obj.read(
                cursor, uid, contract_id, ['direccio_pagament']
            )['direccio_pagament'][0]
            res_part_addr_obj.write(
                cursor, uid, direccio_pagament_id,
                {'email': 'prova_email@fakemail.com'})
            polissa = contract_obj.browse(cursor, uid, contract_id)
            self.assertEqual(
                polissa.direccio_pagament.email, 'prova_email@fakemail.com')

            conf_obj = self.openerp.pool.get('res.config')
            conf_var = 'sw_r1_from_invoice_mail_add'
            conf_var_id = conf_obj.search(
                cursor, uid, [('name', '=', conf_var)])[0]
            conf_obj.write(cursor, uid, conf_var_id, {'value': 1})
            invoice_mail_add = bool(
                int(conf_obj.get(cursor, uid, conf_var, None)))
            self.assertEqual(invoice_mail_add, True)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.r1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            r101 = step_obj.browse(cursor, uid, step_id)
            r1 = sw_obj.browse(cursor, uid, r101.sw_id.id)

            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '01')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(r101.tipus, '02')
            self.assertEqual(r101.subtipus_id.name, '009')
            self.assertEqual(r101.tipus_reclamant, '06')
            self.assertIsNotNone(r101.email)
            self.assertNotEqual(r101.email, False)

    def test_creation_r1_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.r1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            r101 = step_obj.browse(cursor, uid, step_id)
            r1 = sw_obj.browse(cursor, uid, r101.sw_id.id)

            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '01')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(r101.tipus, '02')
            self.assertEqual(r101.subtipus_id.name, '009')
            self.assertEqual(r101.tipus_reclamant, '06')

    def test_create_r1_02(self):
        """Test dummy create for r1-02"""
        # First we need an R1-01 to create a R1-02
        #   So we load the r101 demo file
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()
        with Transaction().start(self.database) as txn:

            step_obj = self.openerp.pool.get('giscedata.switching.r1.02')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            contract_id = self.get_contract_id(txn)

            # Import R1 01

            sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r101.xml'
            )

            # Create R1 02 from the R1 01 codi_sollicitud

            ctx = {'codi_sollicitud': '201602231255'}

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '02', context=ctx
            )

            # Get the new R1 02

            r102 = step_obj.browse(cursor, uid, step_id)
            r1 = r102.sw_id

            # Check R1 values

            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '02')
            self.assertEqual(r1.partner_id.ref, '4321')
            self.assertEqual(r1.company_id.ref, '1234')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')

            # Check R1 02 acceptacio values

            self.assertEqual(r102.rebuig, False)
            self.assertEqual(
                r102.data_acceptacio, str(date.today())
            )
            self.assertEqual(r102.codi_reclamacio_distri, str(r1.id))

    def test_activate_r1_05_038(self):
        """Test dummy create for r1-02"""
        # First we need an R1-01 to create a R1-02
        #   So we load the r101 demo file
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_038.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()
        with Transaction().start(self.database) as txn:

            sw_obj = self.openerp.pool.get('giscedata.switching')
            step_obj = self.openerp.pool.get('giscedata.switching.r1.05')
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            contract_id = self.get_contract_id(txn, 'polissa_0003')

            # Import R1 01
            sw_id, info = sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r101.xml'
            )

            # Create R1 02 from the R1 01 codi_sollicitud
            ctx = {'codi_sollicitud': '201906070005'}

            self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '02', sw_id, context=ctx
            )

            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.no_cessio_sips, 'unactive')

            self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '05', sw_id, context=ctx
            )

            sw = sw_obj.browse(cursor, uid, sw_id)
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.no_cessio_sips, 'active')

    def test_activate_r1_01_038(self):
        """Test dummy create for r1-02"""
        # First we need an R1-01 to create a R1-02
        #   So we load the r101 demo file
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_038.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()
        with Transaction().start(self.database) as txn:

            sw_obj = self.openerp.pool.get('giscedata.switching')
            step_obj = self.openerp.pool.get('giscedata.switching.r1.05')
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            contract_id = self.get_contract_id(txn, 'polissa_0003')

            # Import R1 01
            sw_id, info = sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r101.xml'
            )

            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.no_cessio_sips, 'unactive')

            # Create R1 02 from the R1 01 codi_sollicitud
            ctx = {'codi_sollicitud': '201906070005'}

            sw = sw_obj.browse(cursor, uid, sw_id)
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, sw)
            pol = pol_obj.browse(cursor, uid, contract_id)
            self.assertEqual(pol.no_cessio_sips, 'requested')

    def test_load_r1_02_accepta_and_activate(self):
        data_old = '<FechaSolicitud>2016-09-29T09:39:08'
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            r101_obj = self.openerp.pool.get('giscedata.switching.r1.01')

            act_obj = self.openerp.pool.get("giscedata.switching.activation.config")
            act_obj.write(cursor, uid, act_obj.search(cursor, uid, [], context={"active_test": False}), {'active': True, 'is_automatic': True})

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            # Creates step R1-01
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            r101 = r101_obj.browse(cursor, uid, step_id)
            sw_obj.write(
                cursor, uid, r101.sw_id.id, {'codi_sollicitud': '201602231255'}
            )
            # El creem ara perque la data sigui posterior a la posada al r101
            with open(r1_xml_path, 'r') as f:
                data_new = datetime.strftime(datetime.now(),
                                             '%Y-%m-%dT%H:%M:%S')
                r1_xml = f.read()
                r1_xml = r1_xml.replace(
                    data_old, "<FechaSolicitud>{}".format(data_new)
                )

            sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r102.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '02')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')
            self.assertEqual(r1.codi_sollicitud, '201602231255')

            deadline = workday(datetime.today(), 15)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(r1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            r102 = sw_obj.get_pas(cursor, uid, r1)

            self.assertEqual(r102.data_acceptacio, '2016-09-29')
            self.assertEqual(r102.codi_reclamacio_distri, 'CAS-131596')
            self.assertEqual(r102.rebuig, False)
            # Check activation is called
            self.assertIn("ERROR: No s'ha enviat el mail", r1.history_line[0].description)

    def test_load_r1_03(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r103_new.xml')
        with open(r1_xml_path, 'r') as f:
            r103_xml = f.read()

        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')
        with open(r1_xml_path, 'r') as f:
            r102_xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.r1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            r101 = step_obj.browse(cursor, uid, step_id)
            r1 = sw_obj.browse(cursor, uid, r101.sw_id.id)
            codi = r1.codi_sollicitud

            # change code of r102.xml
            r102_xml = r102_xml.replace(
                "<CodigoDeSolicitud>201602231255",
                "<CodigoDeSolicitud>{0}".format(codi)
            )

            # import r102
            sw_obj.importar_xml(
                cursor, uid, r102_xml, 'r102.xml'
            )

            # change code of r103.xml
            r103_xml = r103_xml.replace(
                "<CodigoDeSolicitud>201602231255",
                "<CodigoDeSolicitud>{0}".format(codi)
            )

            # import r103
            sw_obj.importar_xml(
                cursor, uid, r103_xml, 'r103.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', codi)
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '03')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')

            deadline = datetime.today() + timedelta(days=20)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(r1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            r103 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r103.num_expedient_escomesa, '1111122222')
            self.assertEqual(r103.tipus_comunicacio, '01')
            self.assertEqual(r103.codi_reclamacio_distri, '12345678')
            self.assertEqual(r103.desc_info_intermitja, 'Descripcion de la informacion intermedia aportada.')
            self.assertEqual(r103.escollir, '01')
            self.assertTrue(r103.hi_ha_info_intermitja)
            self.assertEqual(r103.retip_tipus, '02')
            self.assertEqual(r103.retip_subtipus_id.name, '003')
            self.assertEqual(r103.retip_desc, 'descripcio de la retipificacio.')
            self.assertTrue(r103.hi_ha_retipificacio)
            self.assertTrue(r103.hi_ha_sollicitud)
            self.assertEqual(len(r103.sollicitud_ids), 2)
            sol = r103.sollicitud_ids[1]
            self.assertEqual(sol.tipus_info_adicional, '02')
            self.assertEqual(sol.descripcio_peticio_informacio, 'Descripcion de la peticion.')
            self.assertEqual(sol.data_limit, '2016-07-10')
            self.assertTrue(r103.hi_ha_sol_info_retip)
            self.assertEqual(r103.sol_retip_tipus, '03')
            self.assertEqual(r103.sol_retip_subtipus_id.name, '003')
            self.assertEqual(r103.sol_retip_data_limit, '2016-08-10')
            self.assertEqual(r103.comentaris, 'R1 03.')

    def test_load_r1_03_intervencions(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r103_new_intervenciones.xml')
        with open(r1_xml_path, 'r') as f:
            r103_xml = f.read()

        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')
        with open(r1_xml_path, 'r') as f:
            r102_xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.r1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            r101 = step_obj.browse(cursor, uid, step_id)
            r1 = sw_obj.browse(cursor, uid, r101.sw_id.id)
            codi = r1.codi_sollicitud

            # change code of r102.xml
            r102_xml = r102_xml.replace(
                "<CodigoDeSolicitud>201602231255",
                "<CodigoDeSolicitud>{0}".format(codi)
            )

            # import r102
            sw_obj.importar_xml(
                cursor, uid, r102_xml, 'r102.xml'
            )

            # change code of r103.xml
            r103_xml = r103_xml.replace(
                "<CodigoDeSolicitud>201602231255",
                "<CodigoDeSolicitud>{0}".format(codi)
            )

            # import r103
            sw_obj.importar_xml(
                cursor, uid, r103_xml, 'r103.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', codi)
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '03')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')

            deadline = datetime.today() + timedelta(days=20)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(r1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            r103 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r103.num_expedient_escomesa, '1111122222')
            self.assertEqual(r103.tipus_comunicacio, '01')
            self.assertEqual(r103.codi_reclamacio_distri, '12345678')
            self.assertFalse(r103.hi_ha_sollicitud)
            self.assertFalse(r103.hi_ha_sol_info_retip)
            self.assertFalse(r103.hi_ha_retipificacio)
            self.assertEqual(r103.escollir, '02')
            self.assertTrue(r103.hi_ha_info_intermitja)
            self.assertEqual(len(r103.intervencio_ids), 2)
            int = r103.intervencio_ids[1]
            self.assertEqual(int.tipus_intervencio, '02')
            self.assertEqual(int.data, '2016-06-10')
            self.assertEqual(int.hora_desde, '08:00:00')
            self.assertEqual(int.hora_fins, '09:00:00')
            self.assertEqual(int.numero_visita, '10')
            self.assertEqual(int.resultat, '001')
            self.assertEqual(int.detall_resultat, 'Descripcion de los resultados obtenidos.')
            self.assertEqual(r103.comentaris, "R1 03 with 'Intervenciones'.")

    def test_create_r1_03(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()

        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')
        with open(r1_xml_path, 'r') as f:
            r102_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)

            # import r101
            sw_obj.importar_xml(cursor, uid, r1_xml, 'r101.xml')

            # import r102
            self.switch(txn, 'comer')
            sw_id = sw_obj.importar_xml(
                cursor, uid, r102_xml, 'r102.xml'
            )[0]
            # create r103
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.switch(txn, 'distri')
            step_id = self.create_step(cursor, uid, sw, 'R1', '03')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', sw.codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '03')
            self.assertEqual(r1.partner_id.ref, '4321')
            self.assertEqual(r1.company_id.ref, '1234')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')

            # Get info from steps 01 and 02
            pas_01 = self.openerp.pool.get("giscedata.switching.r1.01")
            pas_02 = self.openerp.pool.get("giscedata.switching.r1.02")
            pas_01_id = pas_01.search(cursor, uid, [('sw_id', '=', r1.id)])[0]
            pas_02_id = pas_02.search(cursor, uid, [('sw_id', '=', r1.id)])[0]
            pas_01_info = pas_01.read(
                cursor, uid, pas_01_id, ['tipus', 'subtipus_id'])
            pas_02_info = pas_02.read(
                cursor, uid, pas_02_id, ['codi_reclamacio_distri'])

            r103 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r103.codi_reclamacio_distri,
                             pas_02_info['codi_reclamacio_distri'])
            comentari = "Reclamacio per al CUPS {0}".format(r1.cups_id.name)
            self.assertEqual(r103.comentaris,comentari)
            self.assertEqual(r103.retip_tipus, pas_01_info['tipus'])
            self.assertEqual(r103.retip_subtipus_id.id, pas_01_info['subtipus_id'][0])
            self.assertEqual(r103.sol_retip_tipus, pas_01_info['tipus'])
            self.assertEqual(r103.sol_retip_subtipus_id.id, pas_01_info['subtipus_id'][0])
            self.assertEqual(
                r1.additional_info,
                '05-039: SOLICITUD DE CERTIFICADO / INFORME DE CALIDAD. '
                'Comunicación R1-03: Solicitud de Información adicional')

    def test_load_r1_04(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r101_xml = f.read()

        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r104_new.xml')
        with open(r1_xml_path, 'r') as f:
            r104_xml = f.read()

        with Transaction().start(self.database) as txn:
            sw_obj = self.openerp.pool.get('giscedata.switching')
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)

            # Import R1 01
            sw_id = sw_obj.importar_xml(
                cursor, uid, r101_xml, 'r101.xml'
            )

            # Create R1 02
            sw = sw_obj.browse(cursor, uid, sw_id[0])
            self.create_step(cursor, uid, sw, 'R1', '02')

            # Import R1 04
            sw_obj.importar_xml(
                cursor, uid, r104_xml, 'r104.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '04'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '04')
            self.assertEqual(r1.partner_id.ref, '4321')
            self.assertEqual(r1.company_id.ref, '1234')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')
            self.assertEqual(r1.codi_sollicitud, '201602231255')

            r104 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r104.num_expedient_escomesa, '0123456789ABCD')
            self.assertEqual(r104.data_informacio, '2016-01-20')
            self.assertEqual(len(r104.vars_aportacio_info_ids), 2)
            var1 = r104.vars_aportacio_info_ids[0]
            self.assertEqual(var1.tipus_info, '01')
            self.assertEqual(var1.desc_peticio_info, 'Informacio per fer testos.')
            self.assertEqual(var1.variable, '01')
            self.assertEqual(var1.valor, '125')
            self.assertEqual(len(r104.reclamacio_ids), 1)
            vr1 = r104.reclamacio_ids[0]
            self.assertEqual(vr1.num_expedient_escomesa, '11111')
            self.assertEqual(vr1.num_expedient_frau, '22222')
            self.assertEqual(vr1.data_incident, '2016-02-10')
            self.assertEqual(vr1.num_factura, '243615')
            self.assertEqual(vr1.tipus_concepte_facturat, '01')
            self.assertEqual(vr1.codi_incidencia, '01')
            self.assertEqual(vr1.codi_sollicitud, '201602231236')
            self.assertEqual(vr1.parametre_contractacio, '01')
            self.assertEqual(vr1.concepte_disconformitat, '100')
            self.assertEqual(vr1.tipus_atencio_incorrecte, '05')
            self.assertEqual(vr1.iban, '4444222211113333')
            self.assertEqual(vr1.codi_sollicitud_reclamacio, '201602231236')
            self.assertEqual(vr1.data_inici, '2016-01-01')
            self.assertEqual(vr1.data_fins, '2016-02-09')
            self.assertEqual(vr1.import_reclamat, 204.49)
            self.assertEqual(vr1.data_lectura, '2016-08-31')
            self.assertEqual(vr1.codi_dh, '1')
            self.assertEqual(vr1.cont_nom, 'Camptocamp')
            self.assertEqual(vr1.cont_telefons[1].numero, '55512345')
            self.assertEqual(vr1.cont_email, 'perico@acme.com')
            self.assertEqual(vr1.desc_ubicacio, 'Destino')
            self.assertEqual(vr1.municipi.ine, '17079')
            self.assertEqual(vr1.provincia.code, '17')
            self.assertEqual(vr1.poblacio, '17079')
            self.assertEqual(vr1.codi_postal, '17001')
            self.assertEqual(len(vr1.lect_ids), 1)
            lect1 = vr1.lect_ids[0]
            self.assertEqual(lect1.name, 'P1')
            self.assertEqual(lect1.magnitud, 'AE')
            self.assertEqual(lect1.lectura, 0000001162.00)
            self.assertEqual(r104.tipus_document, 'NI')
            self.assertEqual(r104.codi_document, 'B36385870')
            self.assertEqual(r104.telefons[0].numero, '888555222')
            self.assertEqual(r104.ind_direccio_fiscal, 'F')
            self.assertEqual(r104.nom, 'Camptocamp')
            self.assertEqual(r104.fiscal_address_id.nv, u'MELA MUTERMILCH')
            self.assertEqual(r104.comentaris, 'R104 test with VariablesAportacionInformacion.')
            self.assertEqual(len(r104.document_ids), 3)

    def test_load_r1_05(self):
        data_old_02 = '<FechaSolicitud>2016-09-29T09:39:08'
        data_old_05 = '<FechaSolicitud>2016-09-30T12:42:16'
        r1_02_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')

        r1_05_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r105_new.xml')

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            r101_obj = self.openerp.pool.get('giscedata.switching.r1.01')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            # Creates step R1-01
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            r101 = r101_obj.browse(cursor, uid, step_id)
            sw_obj.write(
                cursor, uid, r101.sw_id.id, {'codi_sollicitud': '201602231255'}
            )
            # El creem ara perque la data sigui posterior a la posada al r101
            with open(r1_02_xml_path, 'r') as f:
                r1_02_xml = f.read()
                data_new = datetime.strftime(datetime.now(),
                                             '%Y-%m-%dT%H:%M:%S')
                r1_02_xml = r1_02_xml.replace(
                    data_old_02, "<FechaSolicitud>{}".format(data_new)
                )
            # R1-02 step loading
            sw_obj.importar_xml(
                cursor, uid, r1_02_xml, 'r102.xml'
            )

            # El creem ara perque la data sigui posterior a la posada al r102
            with open(r1_05_xml_path, 'r') as f:
                r1_05_xml = f.read()
                data_nw = datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%S')
                r1_05_xml = r1_05_xml.replace(
                    data_old_05, "<FechaSolicitud>{}".format(data_nw)
                )
            sw_obj.importar_xml(
                cursor, uid, r1_05_xml, 'r105.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.proces_id.name, 'R1')
            self.assertEqual(r1.step_id.name, '05')
            self.assertEqual(r1.partner_id.ref, '1234')
            self.assertEqual(r1.company_id.ref, '4321')
            self.assertEqual(r1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(r1.cups_polissa_id.name, '0001')
            self.assertEqual(r1.codi_sollicitud, '201602231255')

            r105 = sw_obj.get_pas(cursor, uid, r1)

            comentaris = (u'Comentarios generales')

            self.assertEqual(r105.comentaris, comentaris)
            self.assertEqual(r105.num_expedient_escomesa, '11111')
            self.assertEqual(r105.data, '2016-04-12')
            self.assertEqual(r105.hora, '16:02:25')
            self.assertEqual(r105.tipus, '03')
            self.assertEqual(r105.subtipus_id.name, '013')
            self.assertEqual(r105.codi_reclamacio_distri, '3291970')
            self.assertEqual(r105.resultat, '02')
            self.assertEqual(r105.detall_resultat, '0010101')
            self.assertEqual(r105.observacions, 'Observaciones generales')
            self.assertEqual(r105.indemnitzacio_abonada, 0.0)
            self.assertEqual(r105.num_expedient_anomalia_frau, '22222')
            self.assertEqual(r105.data_moviment, '2016-04-12')
            self.assertEqual(r105.cod_contracte, '383922379')
            self.assertEqual(
                r1.additional_info,
                u"02-009: DISCONFORMIDAD CON LECTURA FACTURADA. "
                u"Improcedente / Desfavorable: Se piden disculpas"
            )

    def test_additional_info_r1_01(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)
            r1 = sw_obj.browse(cursor, uid, res[0])
            inf = u'05-039: SOLICITUD DE CERTIFICADO / INFORME DE CALIDAD'
            self.assertEqual(inf, r1.additional_info)

    def test_additional_info_r1_02_accept(self):
        data_old = '<FechaSolicitud>2016-09-29T09:39:08'
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            r101_obj = self.openerp.pool.get('giscedata.switching.r1.01')

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            # Creates step R1-01
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            r101 = r101_obj.browse(cursor, uid, step_id)
            sw_obj.write(
                cursor, uid, r101.sw_id.id, {'codi_sollicitud': '201602231255'}
            )
            # El creem ara perque la data sigui posterior a la posada al r101
            with open(r1_xml_path, 'r') as f:
                data_new = datetime.strftime(datetime.now(),
                                             '%Y-%m-%dT%H:%M:%S')
                r1_xml = f.read()
                r1_xml = r1_xml.replace(
                    data_old, "<FechaSolicitud>{}".format(data_new)
                )

            sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r102.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            inf = u'02-009: DISCONFORMIDAD CON LECTURA FACTURADA.'
            self.assertEqual(inf, r1.additional_info)

    def test_additional_info_r1_02_rej(self):
        inv_fact_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(inv_fact_xml_path, 'r') as f:
            rec_xml = f.read()
            rec_xml = rec_xml.replace(
                '<Subtipo>039</Subtipo>',
                '<Subtipo>047</Subtipo>'
            )
            rec_xml = rec_xml.replace(
                '<NumFacturaATR>243615</NumFacturaATR>',
                '<NumFacturaATR>0001/F</NumFacturaATR>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, rec_xml, 'rec_rep.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            r1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(r1.rebuig, True)
            inf = u'05-047: SOLICITUD RECALCULO CCH SIN MODIFICACION CIERRE ATR. Rebuig: Comercializadora incorrecta,Factura no telegestionada'
            self.assertEqual(inf, r1.additional_info)


class TestNotificationMailsR1(TestSwitchingNotificationMail):
    _proces = 'R1'

    def test_get_notification_mail_templates_r1(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        b1_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = b1_obj.get_steps(cursor, uid, False, self._proces)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)

                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)

