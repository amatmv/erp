# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction

from giscedata_switching.giscedata_switching_validacions_lectures \
    import ValidadorLecturesATR
from addons import get_module_resource


class TestValidationDates(TestSwitchingImport):
    '''
    Test to check check_dates validation on step change
        # Validation for Cn-06 and B1-05:
            - La fecha de activacion tiene que ser la misma que la lectura
        # Validation for A3-05, M1-05, Cn-05
            - La fecha de la lectura tiene que ser 1 dia antes que la
            fecha de activacion
        # Other cases
            - La lectura tiene que ser o de la fecha de activacion o
            1 dia anterior a la activacion
    '''

    def generate_c1_05(self, cursor, uid, txn):
        c102_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        c105_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c105_new.xml')
        with open(c102_xml_path, 'r') as f:
            c102_xml = f.read()
        with open(c105_xml_path, 'r') as f:
            c105_xml = f.read()

        self.switch(txn, 'comer', other_id_name='res_partner_asus')

        # create step 01
        contract_id = self.get_contract_id(txn)
        step_id = self.create_case_and_step(
            cursor, uid, contract_id, 'C1', '01'
        )
        step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        c101 = step_obj.browse(cursor, uid, step_id)
        c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
        codi_sollicitud = c1.codi_sollicitud

        # change the codi sol.licitud of c102.xml
        c102_xml = c102_xml.replace(
            "<CodigoDeSolicitud>201607211259",
            "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
        )
        # import step 02
        sw_obj.importar_xml(cursor, uid, c102_xml, 'c102.xml')

        # change the codi sol.licitud of c105.xml
        c105_xml = c105_xml.replace(
            "<CodigoDeSolicitud>201607211259",
            "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
        )
        # import step 05
        sw_obj.importar_xml(cursor, uid, c105_xml, 'c105.xml')

        res = sw_obj.search(cursor, uid, [
            ('proces_id.name', '=', 'C1'),
            ('step_id.name', '=', '05'),
            ('codi_sollicitud', '=', codi_sollicitud)
        ])
        self.assertEqual(len(res), 1)

        sw_c1_id = sw_obj.browse(cursor, uid, res[0])#sw_obj.browse(cursor, uid, res[0])

        return sw_c1_id

    def generate_b1_02(self, cursor, uid, txn):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b102_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        uid = txn.user
        cursor = txn.cursor
        self.switch(txn, 'comer')

        # create step 01
        contract_id = self.get_contract_id(txn)
        self.change_polissa_comer(txn)
        self.update_polissa_distri(txn)
        step_id = self.create_case_and_step(
            cursor, uid, contract_id, 'B1', '01'
        )
        step_obj = self.openerp.pool.get(
            'giscedata.switching.b1.01')
        sw_obj = self.openerp.pool.get('giscedata.switching')
        b101 = step_obj.browse(cursor, uid, step_id)
        b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
        codi_sollicitud = b1.codi_sollicitud

        # change the codi sol.licitud of b102.xml
        b1_xml = b1_xml.replace(
            "<CodigoDeSolicitud>201412111009",
            "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
        )

        # import b102.xml
        sw_obj.importar_xml(cursor, uid, b1_xml, 'b102.xml')

        res = sw_obj.search(cursor, uid, [
            ('proces_id.name', '=', 'B1'),
            ('step_id.name', '=', '02'),
            ('codi_sollicitud', '=', codi_sollicitud)
        ])

        b1 = sw_obj.browse(cursor, uid, res[0])

        b102 = sw_obj.get_pas(cursor, uid, b1)

        return b1, b102

    def test_validate_dates_cn_06_b1_05(self):
        # Validation 1 ACTIVATION DATE == READ DATE OK
        # Validation 1 ACTIVATION DATE != READ DATE ERROR
        # (WE TRAY DAY BEFORE)
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool

            sw_obj = pool.get("giscedata.switching")

            faka_lecturas = [{'name': '1917-10-25'}]

            sw, pas_02 = self.generate_b1_02(cursor, uid, txn)

            self.create_step(cursor, uid, sw, 'b1', '05')

            sw = sw_obj.browse(cursor, uid, sw.id)

            pas_05 = sw_obj.get_pas(cursor, uid, sw)

            # Validation correct
            pas_05.write({'data_activacio': '1917-10-25'})

            self.assertEqual(sw.step_id.name, '05')

            pas_05 = sw_obj.get_pas(cursor, uid, sw)
            self.assertEqual(pas_05.data_activacio, '1917-10-25')

            res_validation_1_correct = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw.id
            )

            # Validation error
            self.assertTrue(res_validation_1_correct.valid)
            self.assertFalse(res_validation_1_correct.error_msg)

            pas_05.write({'data_activacio': '1917-10-26'})
            pas_05 = sw_obj.get_pas(cursor, uid, sw)
            self.assertEqual(pas_05.data_activacio, '1917-10-26')

            res_validation_1_error = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw.id
            )

            self.assertFalse(res_validation_1_error.valid)
            self.assertTrue(res_validation_1_error.error_msg)

    def test_validate_dates_cn_05_a3_05_m1_05(self):

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool

            sw_obj = pool.get("giscedata.switching")

            faka_lecturas = [{'name': '1917-10-25'}]

            # Validation 2 - Read one day before activation date OK

            sw_id = self.generate_c1_05(cursor, uid, txn)
            pas = sw_obj.get_pas(cursor, uid, sw_id)

            pas.write({'data_activacio': '1917-10-26'})

            pas = sw_obj.get_pas(cursor, uid, sw_id)

            self.assertEqual(pas.data_activacio, '1917-10-26')

            res_validation_2_correct = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw_id.id)

            self.assertTrue(res_validation_2_correct.valid)
            self.assertFalse(res_validation_2_correct.error_msg)

            # Validation 2 error if same day
            pas.write({'data_activacio': '1917-10-25'})

            pas = sw_obj.get_pas(cursor, uid, sw_id)
            self.assertEqual(pas.data_activacio, '1917-10-25')

            res_validation_error = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw_id.id)

            self.assertFalse(res_validation_error.valid)
            self.assertTrue(res_validation_error.error_msg)

            # Validation 2 END

    def test_validate_dates_others(self):
        # Validation 3 ACTIVATION DATE == READ DATE OK
        # Validation 3 READ DATE ERROR == ACTIVATION DATE OK
        # Else ERROR

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            pool = self.openerp.pool

            faka_lecturas = [{'name': '1917-10-25'}]

            sw, pas_02 = self.generate_b1_02(cursor, uid, txn)

            pas_02.write({'data_activacio': '1917-10-25'})

            res_validation_3_correct = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw.id)

            self.assertTrue(res_validation_3_correct.valid)
            self.assertFalse(res_validation_3_correct.error_msg)

            pas_02.write({'data_activacio': '1917-10-26'})

            res_validation_3_correct2 = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw.id)

            self.assertTrue(res_validation_3_correct2.valid)
            self.assertFalse(res_validation_3_correct2.error_msg)

            pas_02.write({'data_activacio': '1917-10-29'})

            res_validation_3_error = ValidadorLecturesATR().check_dates(
                pool, cursor, uid, faka_lecturas, sw.id)

            self.assertFalse(res_validation_3_error.valid)
            self.assertTrue(res_validation_3_error.error_msg)