# -*- coding: utf-8 -*-
from __future__ import absolute_import

from datetime import datetime

from destral.patch import PatchNewCursors

from .common_tests import TestSwitchingImport

from destral.transaction import Transaction
from addons import get_module_resource


class TestD1(TestSwitchingImport):

    def generate_step_01(self, txn):
        d1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'd101_new.xml')
        with open(d1_xml_path, 'r') as f:
            d1_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        self.switch(txn, 'comer')
        self.activar_polissa_CUPS(txn)
        uid = txn.user
        cursor = txn.cursor
        sw_obj.importar_xml(
            cursor, uid, d1_xml, 'd101.xml'
        )

    def test_load_d1_01(self):

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.generate_step_01(txn)

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'D1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            self.assertEqual(len(res), 1)

            d1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '01')
            self.assertEqual(d1.partner_id.ref, '1234')
            self.assertEqual(d1.company_id.ref, '4321')
            self.assertEqual(d1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(d1.cups_polissa_id.name, '0001')
            self.assertEqual(d1.codi_sollicitud, '201608120830')
            self.assertEqual(d1.data_sollicitud, '2016-08-12')

            d101 = sw_obj.get_pas(cursor, uid, d1)
            self.assertEqual(d101.motiu_canvi, '04')
            self.assertEqual(d101.periodicitat_facturacio, '01')
            self.assertEqual(d101.data_activacio, '2016-06-09')

            self.assertEqual(d101.cau, 'ES1234000000000001JN0FA001')
            self.assertEqual(d101.seccio_registre, '2')
            self.assertEqual(d101.subseccio, 'a0')
            self.assertEqual(d101.collectiu, True)
            self.assertEqual(d101.cups, 'ES1234000000000001JN0F')
            self.assertEqual(d101.tipus_cups, '01')
            self.assertEqual(d101.ref_cadastre_cups, '1234567890qwertyuiop')
            self.assertEqual(d101.generadors[0].cil, 'ES1234000000000001JN0F001')
            self.assertEqual(d101.generadors[0].tec_generador, 'b12')
            self.assertEqual(d101.generadors[0].combustible, 'Diesel')
            self.assertEqual(d101.generadors[0].pot_installada_gen, 100.0)
            self.assertEqual(d101.generadors[0].tipus_installacio, '01')
            self.assertEqual(d101.generadors[0].esquema_mesura, 'A')
            self.assertEqual(d101.generadors[0].ssaa, True)
            self.assertEqual(d101.generadors[0].ref_cadastre_inst_gen, '1234567890qwertyuidf')
            self.assertEqual(d101.generadors[0].utm_x, '100')
            self.assertEqual(d101.generadors[0].utm_y, '200')
            self.assertEqual(d101.generadors[0].utm_fus, '40')
            self.assertEqual(d101.generadors[0].utm_banda, 'E')
            self.assertEqual(d101.generadors[0].tipus_identificador, 'NI')
            self.assertEqual(d101.generadors[0].identificador, 'ES11111111H')
            self.assertEqual(d101.generadors[0].nom, 'LOPEZ SA.')
            self.assertEqual(d101.generadors[0].email, 'mail_falso@dominio.com')
            self.assertEqual(d101.generadors[0].fiscal_address_id.nv, 'Pau Casals')

            self.assertEqual(len(d101.generadors[0].telefons), 3)
            self.assertEqual(d101.generadors[0].telefons[0].numero, '911111111')
            self.assertEqual(d101.generadors[0].telefons[1].numero, '611111111')
            self.assertEqual(d101.generadors[0].telefons[2].numero, '622222222')

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'D1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_load_d1_02_accept(self):
        d1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'd102_accept_new.xml')
        with open(d1_xml_path, 'r') as f:
            d1_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.generate_step_01(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, d1_xml, 'd102_accept.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'D1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            self.assertEqual(len(res), 1)

            d1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '02')
            self.assertEqual(d1.partner_id.ref, '1234')
            self.assertEqual(d1.company_id.ref, '4321')
            self.assertEqual(d1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(d1.cups_polissa_id.name, '0001')
            self.assertEqual(d1.codi_sollicitud, '201608120830')
            self.assertEqual(d1.data_sollicitud, '2016-08-12')

            d102 = sw_obj.get_pas(cursor, uid, d1)
            self.assertEqual(d102.data_acceptacio, '2016-06-06')

    def test_load_d1_02_reject(self):
        d1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'd102_reject_new.xml')
        with open(d1_xml_path, 'r') as f:
            d1_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.generate_step_01(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, d1_xml, 'd102_reject.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'D1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            self.assertEqual(len(res), 1)

            d1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '02')
            self.assertEqual(d1.partner_id.ref, '1234')
            self.assertEqual(d1.company_id.ref, '4321')
            self.assertEqual(d1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(d1.cups_polissa_id.name, '0001')
            self.assertEqual(d1.codi_sollicitud, '201608120830')
            self.assertEqual(d1.data_sollicitud, '2016-08-12')

            d102 = sw_obj.get_pas(cursor, uid, d1)
            self.assertEqual(d102.data_rebuig, '2016-07-20')
            self.assertEqual(d102.motiu_rebuig, 'Motiu de rebuig F1')

    def test_creation_d1_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri', is_autocons=True)
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'D1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.d1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            d101 = step_obj.browse(cursor, uid, step_id)
            d1 = sw_obj.browse(cursor, uid, d101.sw_id.id)

            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '01')
            self.assertEqual(d1.partner_id.ref, '5555')
            self.assertEqual(d1.company_id.ref, '4321')
            self.assertEqual(d1.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(d101.motiu_canvi, '01')
            self.assertEqual(d101.periodicitat_facturacio, '01')

    def test_creation_d1_02(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'distri', is_autocons=True)

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'D1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.d1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            d101 = step_obj.browse(cursor, uid, step_id)
            d1 = sw_obj.browse(cursor, uid, d101.sw_id.id)

            self.switch(txn, 'comer')
            self.create_step(
                cursor, uid, d1, 'D1', '02', context=None
            )

            d1 = sw_obj.browse(cursor, uid, d101.sw_id.id)
            d102 = d1.get_pas()

            self.assertEqual(d1.proces_id.name, 'D1')
            self.assertEqual(d1.step_id.name, '02')
            self.assertEqual(d1.partner_id.ref, '5555')
            self.assertEqual(d1.company_id.ref, '4321')
            self.assertEqual(d1.cups_id.name, 'ES1234000000000001JN0F')

            today = datetime.strftime(datetime.now(), '%Y-%m-%d')
            self.assertEqual(d102.data_acceptacio, today)

    def test_additional_info_d1(self):

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.generate_step_01(txn)

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'D1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201608120830')
            ])

            d1 = sw_obj.browse(cursor, uid, res[0])
            inf = u'(04) Alta en autoconsumo, Mensual'
            self.assertEqual(inf, d1.additional_info)

    def test_activate_d1(self):
        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.generate_step_01(txn)

            sw_id = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'D1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201608120830')
            ])
            self.assertEqual(len(sw_id), 1)

            sw = sw_obj.browse(cursor, uid, sw_id[0])
            d101 = sw_obj.get_pas(cursor, uid, sw)

            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj.write(cursor, uid, 5, {'vat': d101.generadors[0].identificador})

            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, sw, context={})
                self.assertIn('ES1234000000000001JN0FA001', res[1])

                cups_obj = self.openerp.pool.get('giscedata.cups.ps')
                autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum')
                autoconsum_generador_obj = self.openerp.pool.get('giscedata.autoconsum.generador')

                ac_id = autoconsum_obj.search(cursor, uid, [('cau', '=', 'ES1234000000000001JN0FA001')])
                self.assertEqual(len(ac_id), 1)

                ac_gen_id = autoconsum_generador_obj.search(cursor, uid, [('autoconsum_id', '=', ac_id[0])])
                self.assertGreater(len(ac_gen_id), 0)

                cups_id = cups_obj.search(cursor, uid, [('autoconsum_id', '=', ac_id[0])])
                self.assertGreater(len(cups_id), 0)
