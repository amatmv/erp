# -*- coding: utf-8 -*-
from __future__ import absolute_import

from addons import get_module_resource
from destral.transaction import Transaction
from .common_tests import TestSwitchingImport
import base64
from destral.patch import PatchNewCursors


class TestSwitchingImportXml(TestSwitchingImport):

    def test_import_zip_shows_correct_log(self):
        wiz_obj = self.openerp.pool.get('giscedata.switching.wizard')

        zip_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'test_cases.zip')

        with open(zip_xml_path, "r") as f:
            bytes_f = f.read()
            binary_zip = base64.b64encode(bytes_f)

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            with PatchNewCursors():
                contract_id = self.get_contract_id(txn)
                auto_obj = self.openerp.pool.get('giscedata.switching.activation.config')
                auto_ids = auto_obj.search(cursor, uid, [])
                auto_obj.write(cursor, uid, auto_ids, {'is_automatic': False})

                wiz_values = {
                    'file': binary_zip
                }
                wiz_id = wiz_obj.create(
                    cursor, uid, wiz_values, context={
                        'active_id': contract_id,
                        'active_ids': [contract_id],
                    }
                )
                wiz = wiz_obj.browse(cursor, uid, wiz_id)
                wiz.action_importar_xml()

                cas_ids = eval(wiz.cas)

                expected_output = u"S'han carregat un total de 4 casos\nS'han generat 1 casos erronis\nS'han generat 3 casos correctes\n\n\n------Errors------:\n+ Fitxer: m102_new.xml\nEl destinatari de l'xml, amb refer\xe8ncia '4321' no es correspon a Tiny sprl (REF: 1234)\n\n\n------Correctes------:\n+ Fitxer: a301_new.xml\nFitxer processat correctament.\nS'ha creat el cas amb id {0} \n\n+ Fitxer: b101_new.xml\nFitxer processat correctament.\nS'ha creat el cas amb id {1}\n\nS'ha generat el rebuig automaticament pels seg\xfcents motius: \n    2 - Inexistencia de Contrato de ATR previo en vigor\n    37 - Existencia de Solicitud previa en curso A3 \n\n+ Fitxer: c101_new.xml\nFitxer processat correctament.\nS'ha creat el cas amb id {2}\n\nS'ha generat el rebuig automaticament pels seg\xfcents motius: \n    2 - Inexistencia de Contrato de ATR previo en vigor\n    37 - Existencia de Solicitud previa en curso A3 \n\n\n"
                self.assertEqual(
                    expected_output.format(
                        cas_ids[0], cas_ids[1], cas_ids[2]), wiz.info
                )
