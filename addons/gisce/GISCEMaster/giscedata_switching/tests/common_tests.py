# -*- coding: utf-8 -*-
import unittest

from destral import testing
from destral.transaction import Transaction
from expects import expect, raise_error
from osv.orm import except_orm
from osv import osv
from addons import get_module_resource
import time
from tools.misc import cache
from ..utils import get_address_dicct
from workdays import workday
from datetime import datetime
import mock
from destral.patch import PatchNewCursors


class GiscedataSwitching(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def importar_xml(self, cursor, uid, data, fname, context=None):
        if not context:
            context = {}
        with PatchNewCursors():
            res = super(GiscedataSwitching, self).importar_xml(cursor, uid, data, fname, context)
        return res

GiscedataSwitching()


class BaseTest(testing.OOTestCase):
    ''' Test Polissa Tarifa '''

    require_demo_data = True

    tarifes = {'2.0A': '1',
               '2.1A': '1',
               '2.1DHA': '2',
               '2.1DHS': '8',
               '3.0A': '6',
               }

    invoices = {
        '0001/F': ([1], []),
        '0003/F': ([], []),
    }

    def test_get_old_company_steps_returns_correct_steps(self):
        sw_proc_obj = self.openerp.pool.get('giscedata.switching.proces')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            assert sw_proc_obj.get_old_company_steps(cursor, uid, 'A3') == []
            assert sw_proc_obj.get_old_company_steps(cursor, uid, 'B1') == []
            old_com_steps_C1 = sw_proc_obj.get_old_company_steps(
                cursor, uid, 'C1'
            )
            assert old_com_steps_C1 == ['06', '10', '11', '12']
            old_com_steps_C2 = sw_proc_obj.get_old_company_steps(
                cursor, uid, 'C2'
            )
            assert old_com_steps_C2 == ['06', '10', '11', '12']
            assert sw_proc_obj.get_old_company_steps(cursor, uid, 'D1') == []
            assert sw_proc_obj.get_old_company_steps(cursor, uid, 'M1') == []
            assert sw_proc_obj.get_old_company_steps(cursor, uid, 'W1') == []
            assert sw_proc_obj.get_old_company_steps(cursor, uid, 'R1') == []

    def test_codi_dh_all(self):
        tarifa_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        with Transaction().start(self.database) as txn:
            tarifa_ids = tarifa_obj.search(
                txn.cursor, txn.user, [('name', 'in', self.tarifes.keys())]
            )
            tarifes_list = tarifa_obj.read(
                txn.cursor, txn.user, tarifa_ids, ['name'])
            for tarifa_vals in tarifes_list:
                tarifa_id = tarifa_vals['id']
                codi_dh = tarifa_obj.get_codi_dh(txn.cursor, txn.user,
                                                 tarifa_id)
                assert self.tarifes[tarifa_vals['name']] == codi_dh

    def test_all_rebuig_codes(self):
        rebuig_obj = self.openerp.pool.get(
            'giscedata.switching.motiu.rebuig')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            user = txn.user
            llista_motius = rebuig_obj.get_all_motius(cursor, user, 'C2')

            llista_no_hi_son = [
                '21', '23', '32', '34', '35', '44', '54', '64', '66', '67',
                '68', '69', '71', '72', '73', '74', '75', '76', '77', '78',
                '79', '80', '83', '84', '85', '86', '87', '88', '89', '91',
                '92', '93', 'D1', 'D2']
            self.assertEqual(len(llista_motius), 74)
            for i in llista_no_hi_son:
                self.assertNotIn(i, llista_motius)

    #giscedata_facturacio
    def test_factura_get_last_self_measure(self):
        self.openerp.install_module('giscedata_lectures_pool')
        invoice_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            invoice_ids = invoice_obj.search(
                cursor, uid, [('number', 'in', self.invoices.keys())]
            )

            for invoice_id in invoice_ids:
                measures = invoice_obj.get_last_self_measure(
                    cursor, uid, invoice_id
                )
                inv_vals = invoice_obj.read(
                    cursor, uid, invoice_id, ['number']
                )
                number = inv_vals['number']
                self.assertEqual(measures, self.invoices[number])


class TestSwitchingImport(testing.OOTestCase):

    def get_contract_id(self, txn, xml_id='polissa_0001'):
        uid = txn.user
        cursor = txn.cursor
        imd_obj = self.openerp.pool.get('ir.model.data')

        return imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', xml_id
        )[1]

    def get_pricelist_id(self, txn, xml_id='pricelist_tarifas_electricidad'):
        uid = txn.user
        cursor = txn.cursor
        imd_obj = self.openerp.pool.get('ir.model.data')

        return imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', xml_id
        )[1]

    def get_invoice_id(self, txn, xml_id='factura_0001'):
        uid = txn.user
        cursor = txn.cursor
        imd_obj = self.openerp.pool.get('ir.model.data')

        return imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', xml_id
        )[1]

    @mock.patch('giscedata_switching.giscedata_switching.GiscedataSwitching.whereiam')
    def switch(self, txn, where, mock_function, is_autocons=False, other_id_name='res_partner_agrolait'):
        cursor = txn.cursor
        uid = txn.user
        mock_function.return_value = where
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        other_id = imd_obj.get_object_reference(
            cursor, uid, 'base',  other_id_name
        )[1]
        another_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]

        if is_autocons:
            codes = {'distri': '4321', 'comer': '1234'}
        else:
            codes = {'distri': '1234', 'comer': '4321'}

        partner_obj.write(cursor, uid, [partner_id], {
            'ref': codes.pop(where)
        })
        partner_obj.write(cursor, uid, [other_id], {
            'ref': codes.values()[0]
        })
        partner_obj.write(cursor, uid, [another_id], {
            'ref': '5555'
        })
        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_01'
        )[1]
        distri_ids = {'distri': partner_id, 'comer': other_id}
        cups_obj.write(cursor, uid, [cups_id], {
            'distribuidora_id': distri_ids[where]
        })
        cache.clean_caches_for_db(cursor.dbname)

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    def baixa_tall_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.write(cursor, uid, [polissa_id], {
            'renovacio_auto': True,
            'data_baixa': time.strftime('%Y-%m-%d')
        })

        compt_id = polissa_obj.read(
            cursor, uid, polissa_id, ['comptadors']
        )['comptadors'][0]

        compt_obj.write(cursor, uid, [compt_id], {
            'data_baixa': time.strftime('%Y-%m-%d'),
            'active': False
        })

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'impagament', 'tallar', 'baixa'
        ])

    def update_polissa_distri(self, txn):
        """
        Sets the distribuidora_id field in contract as the same of related cups
        """
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')

        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        cups_id = pol_obj.read(cursor, uid, contract_id, ['cups'])['cups'][0]
        distri_id = cups_obj.read(
            cursor, uid, cups_id, ['distribuidora_id']
        )['distribuidora_id'][0]
        pol_obj.write(cursor, uid, contract_id, {'distribuidora': distri_id})

    def change_distri_code(self, new_ref, txn, polissa_name='polissa_0001'):
        """
            Sets the ref of the distri related with the CUPS of
            polissa_name to 'new_ref'
        """
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_obj = self.openerp.pool.get('res.partner')

        contract_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', polissa_name
        )[1]
        cups_id = pol_obj.read(cursor, uid, contract_id, ['cups'])['cups'][0]
        distri_id = cups_obj.read(
            cursor, uid, cups_id, ['distribuidora_id']
        )['distribuidora_id'][0]

        partner_obj.write(cursor, uid, distri_id, {'ref': new_ref})

    def change_polissa_comer(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        new_comer_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol_obj.write(cursor, uid, [pol_id], {
            'comercialitzadora': new_comer_id
        })

    def crear_modcon(self, txn, potencia, ini, fi):
        cursor = txn.cursor
        uid = txn.user
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')
        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        pol = polissa_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])
        polissa_obj.write(cursor, uid, polissa_id, {'potencia': potencia})
        wz_crear_mc_obj = pool.get('giscedata.polissa.crear.contracte')

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )
        wiz_mod.write({
            'data_inici': ini,
            'data_final': fi
        })
        wiz_mod.action_crear_contracte(ctx)

    def set_autoconsumo(self, txn, autoconsumo_mode='00'):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        polissa_obj.write(cursor, uid, [polissa_id], {
            'autoconsumo': autoconsumo_mode,
        })

    def set_nova_tarifa(self, txn, tarif_name):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        tarif_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        tarif_id = tarif_obj.search(cursor, uid, [('name', '=', tarif_name)])
        polissa_obj.write(cursor, uid, [polissa_id], {
            'tarifa': tarif_id[0],
        })

    def set_trafo(self, txn, trafo):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.write(cursor, uid, [polissa_id], {
            'trafo': trafo,
        })

    def set_mesura_ab(self, txn, mesura):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        tarifa_id = polissa_obj.read(cursor, uid, polissa_id, ['tarifa'])
        tarif_obj = self.openerp.pool.get('giscedata.polissa.tarifa')
        tarif_obj.write(cursor, uid, tarifa_id['tarifa'][0], {
            'mesura_ab': mesura,
        })

    def assignar_factura_potencia(self, txn, tipus_facturacio='icp'):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]
        polissa_obj.write(cursor, uid, [polissa_id], {
            'facturacio_potencia': tipus_facturacio,
        })

    def create_case_and_step(
            self, cursor, uid, contract_id, proces_name, step_name, sw_id_origin=None, context=None
    ):
        """
        Creates case and step
        :param proces_name:
        :param step_name:
        :param context:
        :return:
        """
        if context is None:
            context = {}

        if isinstance(sw_id_origin, dict):
            context = sw_id_origin
            sw_id_origin = None

        sw_obj = self.openerp.pool.get('giscedata.switching')

        swproc_obj = self.openerp.pool.get('giscedata.switching.proces')
        swpas_obj = self.openerp.pool.get('giscedata.switching.step')
        swinfo_obj = self.openerp.pool.get(
            'giscedata.switching.step.info'
        )
        contract_obj = self.openerp.pool.get('giscedata.polissa')

        proces_id = swproc_obj.search(
            cursor, uid, [('name', '=', proces_name)]
        )[0]

        sw_params = {
            'proces_id': proces_id,
            'cups_polissa_id': contract_id,
        }

        vals = sw_obj.onchange_polissa_id(
            cursor, uid, [], contract_id, None, context=context
        )

        sw_params.update(vals['value'])
        # si no tenim ref_contracte, ens l'inventem (de moment)
        if not sw_params.get('ref_contracte', False):
            sw_params['ref_contracte'] = '111111111'

        if sw_id_origin:
            sw_id = sw_id_origin
        else:
            sw_id = sw_obj.create(cursor, uid, sw_params)

        if proces_name in ['C1', 'C2']:
            out_retail = contract_obj.read(
                cursor, uid, contract_id, ['comercialitzadora']
            )['comercialitzadora'][0]
            sw_obj.write(cursor, uid, sw_id, {'comer_sortint_id': out_retail})

        # creeem el pas
        pas_id = swpas_obj.get_step(cursor, uid, step_name, proces_name)
        #Creant info ja crea automaticament tota la info del pas
        info_vals = {
            'sw_id': sw_id,
            'proces_id': proces_id,
            'step_id': pas_id,
        }
        info_id = swinfo_obj.create(cursor, uid, info_vals, context=context)
        info = swinfo_obj.browse(cursor, uid, info_id)
        model_obj, model_id = info.pas_id.split(',')

        return int(model_id)

    def no_telegestio_factura(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        fac_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        fac_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0001'
        )[1]
        fac_obj.write(cursor, uid, [fac_id], {
            'polissa_tg': '2'
        })

    def create_step(self, cursor, uid, sw, proces_name, step_name, context=None):
        if not context:
            context = {}
        sw_obj = self.openerp.pool.get('giscedata.switching')
        pas_obj = self.openerp.pool.get(
            'giscedata.switching.{0}.{1}'.format(proces_name.lower(), step_name)
        )

        pas_id = pas_obj.dummy_create(cursor, uid, sw.id, context)
        return sw_obj.create_step(cursor, uid, sw.id, step_name, pas_id, context=context)

    def create_step_wiz(self, cursor, uid, sw, proces_name, step_name, context=None):
        if not context:
            context = {}
        wiz_create_step_obj = self.openerp.pool.get("wizard.create.step")
        wiz_create_step_id = wiz_create_step_obj.create(cursor, uid, {}, context={'active_ids': [sw.id]})
        wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id], {'step': step_name, 'check_repeated': False})
        wiz_create_step_obj.action_create_steps(cursor, uid, [wiz_create_step_id], context={'active_ids': [sw.id]})

    def set_nocutoff(self, txn, essencial=True):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        nocutoff_obj = self.openerp.pool.get('giscedata.polissa.nocutoff')
        nocutoff_id = nocutoff_obj.search(
            cursor, uid, [('boe_check', '=', essencial)]
        )[0]

        polissa_obj.write(cursor, uid, [polissa_id], {
            'nocutoff': nocutoff_id,
        })


class TestSwitchingFeatures(TestSwitchingImport):

    def test_get_step_id(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)

            # Importem xml B1-01 i es crearà objecte switching i pas 01
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b101_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])

            # Comprovem que sw.step_id retorna el pas 01
            sw = sw_obj.browse(cursor, uid, sw_ids[0])
            self.assertEqual(sw.step_id.name, '01')

            # Creem el pas 03 amb una data posterior a pas 01
            self.create_step(cursor, uid, sw, 'B1', '03')
            # Comprovem que sw.step_id retorna el pas 03
            sw = sw_obj.browse(cursor, uid, sw_ids[0])
            self.assertEqual(sw.step_id.name, '03')

            # Creem el pas 02 amb una data posterior a pas 03
            self.create_step(cursor, uid, sw, 'B1', '02')
            # Comprovem que sw.step_id retorna el pas 02
            sw = sw_obj.browse(cursor, uid, sw_ids[0])
            self.assertEqual(sw.step_id.name, '02')

    def test_data_limit_onchange_enviat(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '02'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.02')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            # Posar que s'ha enviat per actualitzar data limit
            step_obj.write(cursor, uid, step_id, {'enviament_pendent': False})

            a302 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a302.sw_id.id)

            deadline = workday(datetime.today(), 6)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

    def test_export_incorrect_address(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            # import a R1 with address
            sw_obj.importar_xml(
                cursor, uid, r1_xml, 'r101_new.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)
            r1 = sw_obj.browse(cursor, uid, res[0])
            r101 = sw_obj.get_pas(cursor, uid, r1)

            # Delete the field 'calle' from address
            r101.fiscal_address_id.nv = None

            # Use the new method 'get_address_dicct'
            # instead of getting directly de data from r101.fiscal_address_id
            addres_info = get_address_dicct(r101.fiscal_address_id)
            self.assertEqual(" ", addres_info['calle'])

    def test_c101_from_active_contract_fails(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            contract_obj = self.openerp.pool.get('giscedata.polissa')

            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            contract_id = self.get_contract_id(txn)

            contract_data = contract_obj.read(
                cursor, uid, contract_id, ['name', 'state']
            )
            res = contract_obj.crear_cas_atr(
                cursor, uid, contract_id, proces='C1'
            )
            self.assertEqual(res[0], '(C1) {0}'.format(contract_data['name']))
            self.assertEqual(
                res[1],
                u'Pólissa en estat {0}, la saltem'.format(contract_data['state'])
            )
            self.assertEqual(res[2], False)
    
    def test_importation_conf_var(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            # Change config var for case state in importation to set new cases
            # to 'draft'
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'sw_importation_case_state_open')]
            )[0]
            conf_obj.write(cursor, uid, var_id, {'value': '0'})

            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.state, "draft")

    def test_creation_from_polissa_conf_var(self):
        with Transaction().start(self.database) as txn:
            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            cursor = txn.cursor
            uid = txn.user

            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            # Change config var for case state in creation from polissa to
            # set new cases to 'draft'
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'sw_from_polissa_case_state_open')]
            )[0]
            conf_obj.write(cursor, uid, var_id, {'value': '0'})
            context = {'proces': 'A3'}
            res = polissa_obj.crear_cas_atr(cursor, uid, polissa_id, context=context)
            sw_id = res[2]
            sw_info = sw_obj.read(cursor, uid, sw_id, ['state'])
            self.assertEqual(sw_info['state'], 'draft')

    def test_baixa_polissa_close_cases(self):
        with Transaction().start(self.database) as txn:
            polissa_obj = self.openerp.pool.get('giscedata.polissa')
            imd_obj = self.openerp.pool.get('ir.model.data')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            cursor = txn.cursor
            uid = txn.user

            self.switch(txn, 'comer')
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            # Change config var
            conf_obj = self.openerp.pool.get('res.config')
            var_id = conf_obj.search(
                cursor, uid, [('name', '=', 'sw_baixa_contract_close_cases')]
            )[0]
            conf_obj.write(cursor, uid, var_id, {'value': "['A3']"})
            context = {'proces': 'A3'}
            res = polissa_obj.crear_cas_atr(cursor, uid, polissa_id,
                                            context=context)
            sw_id = res[2]

            # Baixa polissa
            polissa_obj.wkf_baixa(cursor, uid, [polissa_id])
            # Check state
            sw_info = sw_obj.read(cursor, uid, sw_id, ['state'])
            self.assertEqual(sw_info['state'], 'done')

    def test_get_case_of_header(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            header_obj = self.openerp.pool.get('giscedata.switching.step.header')
            self.switch(txn, 'distri')

            # Importem xml A3-01 i es crearà objecte switching i pas 01
            sw_obj.importar_xml(cursor, uid, a3_xml, 'a301_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            sw = sw_obj.browse(cursor, uid, sw_ids[0])

            headers = []
            # Obtenim el header del step 01
            step = sw.get_pas()
            s1 = step.id
            hid1 = step.header_id.id
            headers.append(hid1)

            # Creem el pas 02
            self.create_step(cursor, uid, sw, 'A3', '02')
            # Obtenim el header del step 02
            step = sw.get_pas()
            s2 = step.id
            hid2 = step.header_id.id
            headers.append(hid2)

            # Creem el pas 03
            self.create_step(cursor, uid, sw, 'A3', '03')
            # Obtenim el header del step 03
            step = sw.get_pas()
            s3 = step.id
            hid3 = step.header_id.id
            headers.append(hid3)

            # Comprovem que s'ens retornin el step_id i models adequats i que el
            # header inventat no esta al diccionari
            res = header_obj.get_case_of_header(cursor, uid, headers)
            info = res.get(hid1)
            self.assertEqual(s1, info[0])
            self.assertEqual("giscedata.switching.a3.01", info[1])
            info = res.get(hid2)
            self.assertEqual(s2, info[0])
            self.assertEqual("giscedata.switching.a3.02", info[1])
            info = res.get(hid3)
            self.assertEqual(s3, info[0])
            self.assertEqual("giscedata.switching.a3.03", info[1])

    def test_import_seq_sollicitud(self):
        r1_xml_path = get_module_resource('giscedata_switching', 'tests', 'fixtures', 'r103_new.xml')
        with open(r1_xml_path, 'r') as f:
            r103_xml = f.read()

        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')
        with open(r1_xml_path, 'r') as f:
            r102_xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'R1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.r1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            r101 = step_obj.browse(cursor, uid, step_id)
            r1 = sw_obj.browse(cursor, uid, r101.sw_id.id)
            codi = r1.codi_sollicitud

            # change code of r102.xml
            r102_xml = r102_xml.replace(
                "<CodigoDeSolicitud>201602231255",
                "<CodigoDeSolicitud>{0}".format(codi)
            )
            # import r102
            sw_obj.importar_xml(
                cursor, uid, r102_xml, 'r102.xml'
            )

            # change code of r103.xml
            r103_xml = r103_xml.replace(
                "<CodigoDeSolicitud>201602231255",
                "<CodigoDeSolicitud>{0}".format(codi)
            )
            # import r103
            sw_obj.importar_xml(
                cursor, uid, r103_xml, 'r103.xml'
            )
            r1 = sw_obj.browse(cursor, uid, r1.id)
            r103 = r1.get_pas()
            self.assertEqual(r103.seq_sollicitud, '01')

            # Import another step 03 with diferent seq_sollicitud
            r103_xml = r103_xml.replace(
                "<SecuencialDeSolicitud>01",
                "<SecuencialDeSolicitud>03"
            )
            # import r103
            sw_obj.importar_xml(
                cursor, uid, r103_xml, 'r103.xml'
            )
            r1 = sw_obj.browse(cursor, uid, r1.id)
            r103 = r1.get_pas()
            self.assertEqual(r103.seq_sollicitud, '03')

            # Import another r103 but now with a repeated seq_sollicitud
            r103_xml = r103_xml.replace(
                "<SecuencialDeSolicitud>03",
                "<SecuencialDeSolicitud>01"
            )
            # import r103. It will raise en exception
            def import_repeated():
                sw_obj.importar_xml(cursor, uid, r103_xml, 'r103.xml')
            self.assertRaises(osv.except_osv, import_repeated)
            self.assertEqual(len(r1.step_ids), 4)

    def test_create_seq_sollicitud(self):
        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(r1_xml_path, 'r') as f:
            r1_xml = f.read()

        r1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r102_new.xml')
        with open(r1_xml_path, 'r') as f:
            r102_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)

            # import r101
            sw_obj.importar_xml(cursor, uid, r1_xml, 'r101.xml')

            # import r102
            self.switch(txn, 'comer')
            sw_id = sw_obj.importar_xml(
                cursor, uid, r102_xml, 'r102.xml'
            )[0]

            # create r103. It will have seq_sollicitud 01
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.switch(txn, 'distri')
            step_id = self.create_step_wiz(cursor, uid, sw, 'R1', '03')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', sw.codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            r1 = sw_obj.browse(cursor, uid, res[0])
            r103 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r103.seq_sollicitud, "01")

            # create r103. It will have seq_sollicitud 02
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.switch(txn, 'distri')
            step_id = self.create_step_wiz(cursor, uid, sw, 'R1', '03')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', sw.codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            r1 = sw_obj.browse(cursor, uid, res[0])
            r103 = sw_obj.get_pas(cursor, uid, r1)
            self.assertEqual(r103.seq_sollicitud, "02")

    def test_unlink_step_info(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')

            # Importem xml i crearà objecte switching, pas 01, header i stepinfo
            sw_obj.importar_xml(cursor, uid, a3_xml, 'a301_new.xml')
            sw_ids = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            sw = sw_obj.browse(cursor, uid, sw_ids[0])

            # Si eliminem el pas directament tambe s'hauria d'eliminar el
            # objecte step_info que el te com a pas_id
            self.assertEqual(len(sw.step_ids), 1)
            self.assertTrue(sw.step_id)
            sw.step_ids[0].unlink()
            sw = sw.browse()[0]
            self.assertEqual(len(sw.step_ids), 0)
            self.assertFalse(sw.step_id)

    def test_activar_pas_acceptacio_rebuig_02_acc(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            act_obj = self.openerp.pool.get("giscedata.switching.activation.config")
            act_obj.write(cursor, uid, act_obj.search(cursor, uid, [], context={"active_test": False}), {'active': True})
            pw_account_obj = self.openerp.pool.get('poweremail.core_accounts')
            pw_id = pw_account_obj.create(cursor, uid, {
                'name': 'test',
                'user': 1,
                'email_id': 'test@email',
                'smtpserver': 'smtp.gmail.com',
                'smtpport': '587',
                'company': 'no',
                'state': 'approved',
            })
            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)

            # change the codi sol.licitud of c102.xml
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud
            c1_xml = c1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c102.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])
            c102 = sw_obj.get_pas(cursor, uid, c1)
            # Marquem pendent de notificar
            c102.write({'notificacio_pendent': True})
            c1 = c1.browse()[0]

            # Activem 02 d'acceptacio. Es desmarcara el pendent de notificar.
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, c1, context={'use_mail_account': pw_id})
            c1 = c1.browse()[0]
            c101 = c1.get_pas()
            self.assertEqual(res[0], 'OK')
            self.assertEqual(c1.state, 'open')
            self.assertFalse(c101.notificacio_pendent)
            # Si el tornem a activar, al estar ja notificat donarà error
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, c1, context={'use_mail_account': pw_id})
            self.assertEqual(res[0], 'ERROR')

    def test_activar_pas_acceptacio_rebuig_02_rej(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            act_obj = self.openerp.pool.get("giscedata.switching.activation.config")
            act_obj.write(cursor, uid, act_obj.search(cursor, uid, [], context={"active_test": False}), {'active': True})
            pw_account_obj = self.openerp.pool.get('poweremail.core_accounts')
            pw_id = pw_account_obj.create(cursor, uid, {
                'name': 'test',
                'user': 1,
                'email_id': 'test@email',
                'smtpserver': 'smtp.gmail.com',
                'smtpport': '587',
                'company': 'no',
                'state': 'approved',
            })
            sw_rebuig_obj = self.openerp.pool.get('giscedata.switching.rebuig')
            swr_id = sw_rebuig_obj.create(cursor, uid, {
                'motiu_rebuig': 1,
                'desc_rebuig': 'rej'
            })

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)

            # change the codi sol.licitud of c102.xml
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud
            c1_xml = c1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c102.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])
            c102 = sw_obj.get_pas(cursor, uid, c1)
            # Marquem pendent de notificar i creem rebutjos
            c102.write({'notificacio_pendent': True, 'rebuig': True, 'rebuig_ids': [(6, 0, [swr_id])]})
            notify_obj = self.openerp.pool.get('giscedata.switching.notify')
            nid = notify_obj.create(cursor, uid, {
                'step_id': c1.step_id.id,
                'proces_id': c1.proces_id.id,
                'notify_text': 'not reb test',
                'rebuig_ids': [(6, 0, [1])],
                'active': True
            })
            c1 = c1.browse()[0]

            # Activem 02 de rebuig. Es desmarcara el pendent de notificar i tancara.
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, c1, context={'use_mail_account': pw_id})
            c1 = c1.browse()[0]
            c101 = c1.get_pas()
            self.assertEqual(res[0], 'OK')
            self.assertEqual(c1.state, 'done')
            self.assertFalse(c101.notificacio_pendent)
            # Si activem un rebuig no pendent de notificar, no tanca el cas.
            c1.write({'state': 'open'})
            c1 = c1.browse()[0]
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, c1, context={'use_mail_account': pw_id})
            self.assertEqual(res[0], 'ERROR')
            self.assertEqual(c1.state, 'open')



class TestSwitchingAutoReject(TestSwitchingImport):

    def test_reject_invalid_cups_1(self):
        cups_ori = 'ES1234000000000001JN0F'
        cups_inv = 'ES1234567890123456TP0F'

        invalid_cups_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_cups_xml_path, 'r') as f:
            invalid_cups_xml = f.read()
            invalid_cups_xml = invalid_cups_xml.replace(cups_ori, cups_inv)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_cups_xml, 'invalid_cups.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_cups = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_cups.rebuig, True)

            inv_cups02 = sw_obj.get_pas(cursor, uid, invalid_cups)
            self.assertTrue(inv_cups02.enviament_pendent)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_cups02.rebuig_ids]
            self.assertEqual(codis_reb, ['1'])

    def test_reject_invalid_contract_ATR_2(self):
        invalid_contract_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_contract_xml_path, 'r') as f:
            invalid_contract_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_contract_xml, 'invalid_contract.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_contract = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_contract.rebuig, True)

            inv_ATR = sw_obj.get_pas(cursor, uid, invalid_contract)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_ATR.rebuig_ids]
            self.assertEqual(codis_reb, ['2'])

    def test_reject_invalid_NIF_3(self):
        invalid_nif_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_nif_xml_path, 'r') as f:
            invalid_nif_xml = f.read()
            invalid_nif_xml = invalid_nif_xml.replace('B36385870', 'J5731503H')
            invalid_nif_xml = invalid_nif_xml.replace(
                '<TipoModificacion>S',
                '<TipoModificacion>N')

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_nif_xml, 'invalid_nif.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_nif = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_nif.rebuig, True)

            inv_nif02 = sw_obj.get_pas(cursor, uid, invalid_nif)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_nif02.rebuig_ids]
            self.assertEqual(codis_reb, ['3'])

    def test_reject_solicitud_previa_6(self):
        codi_sol_old = '201412111009'
        codi_sol_new = '201412111010'

        solicitud_previa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(solicitud_previa_xml_path, 'r') as f:
            sol_prev_xml = f.read()
            sol_prev_mod_xml = sol_prev_xml.replace(codi_sol_old, codi_sol_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            # We first import a correct XML
            sw_obj.importar_xml(
                cursor, uid, sol_prev_xml, 'solicitud_previa.xml'
            )

            # And then add a new XML with the same CUPS
            sw_obj.importar_xml(
                cursor, uid, sol_prev_mod_xml, 'solicitud_previa_mod.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sol_new)
            ])
            self.assertEqual(len(res), 1)

            sol_prev = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(sol_prev.rebuig, True)

            sol_prev02 = sw_obj.get_pas(cursor, uid, sol_prev)
            codis_reb = [reb.motiu_rebuig.name for reb in sol_prev02.rebuig_ids]
            self.assertEqual(codis_reb, ['39'])

    def test_reject_unspecified_contract_type_7(self):
        contract_type = '<TipoContratoATR>02</TipoContratoATR>'

        unspec_con_type_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(unspec_con_type_xml_path, 'r') as f:
            unspec_con_type_xml = f.read()
            unspec_con_type_xml = unspec_con_type_xml.replace(contract_type, '')

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, unspec_con_type_xml, 'unspec_con_type.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            unspec_con_type = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(unspec_con_type.rebuig, True)

            uns_c_ty02 = sw_obj.get_pas(cursor, uid, unspec_con_type)
            codis_reb = [reb.motiu_rebuig.name for reb in uns_c_ty02.rebuig_ids]
            self.assertEqual(codis_reb, ['7'])

    def test_reject_invalid_duracio_8(self):
        invalid_duracio_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_duracio_xml_path, 'r') as f:
            invalid_duracio_xml = f.read()
            invalid_duracio_xml = invalid_duracio_xml.replace(
                '<FechaFinalizacion>2018-01-01', '<FechaFinalizacion>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_duracio_xml, 'invalid_duracio.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_duracio = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_duracio.rebuig, True)

            inv_duracio02 = sw_obj.get_pas(cursor, uid, invalid_duracio)
            cod_re = [reb.motiu_rebuig.name for reb in inv_duracio02.rebuig_ids]
            self.assertEqual(cod_re, ['8'])

    def test_reject_invalid_comer_11(self):
        invalid_comer_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_comer_xml_path, 'r') as f:
            invalid_comer_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_comer_xml, 'invalid_comer.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_comer = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_comer.rebuig, True)

            inv_comer02 = sw_obj.get_pas(cursor, uid, invalid_comer)
            cod_re = [reb.motiu_rebuig.name for reb in inv_comer02.rebuig_ids]
            self.assertEqual(cod_re, ['11'])

    def test_reject_cutoff_contract_12(self):
        no_cutoff_contract_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(no_cutoff_contract_xml_path, 'r') as f:
            no_cutoff_contract_xml = f.read()
        cutoff_contract_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new_tarpot.xml')
        with open(cutoff_contract_xml_path, 'r') as f:
            cutoff_contract_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)

            cursor = txn.cursor
            uid = txn.user

            # Tallar la polissa
            imd_obj = self.openerp.pool.get('ir.model.data')

            polissa_obj = self.openerp.pool.get('giscedata.polissa')

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            polissa_obj.send_signal(cursor, uid, [polissa_id], [
                'impagament', 'tallar'
            ])

            sw_obj.importar_xml(
                cursor, uid, no_cutoff_contract_xml, 'no_cutoff_contract.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            sw_obj.write(cursor, uid, res, {'state': 'done'})

            sw_obj.importar_xml(
                cursor, uid, cutoff_contract_xml, 'cutoff_contract.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111909')
            ])
            self.assertEqual(len(res), 1)

            cutoff_contract = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(cutoff_contract.rebuig, True)

            cut_cont02 = sw_obj.get_pas(cursor, uid, cutoff_contract)
            codis_reb = [reb.motiu_rebuig.name for reb in cut_cont02.rebuig_ids]
            self.assertEqual(codis_reb, ['12'])

    def test_reject_invalid_potencies_sollicitades_14(self):
        invalid_pot_sol_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_pot_sol_xml_path, 'r') as f:
            invalid_pot_sol_xml = f.read()
            invalid_pot_sol_xml = invalid_pot_sol_xml.replace(
                '<Potencia Periodo="1">2300', '<Potencia Periodo="1">14000'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_pot_sol_xml, 'invalid_pot_sol.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_pot_sol = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_pot_sol.rebuig, True)

            inv_pol_sol02 = sw_obj.get_pas(cursor, uid, invalid_pot_sol)
            cod_re = [reb.motiu_rebuig.name for reb in inv_pol_sol02.rebuig_ids]
            self.assertEqual(cod_re, ['14', '52'])
            # Will always return a 52 because if the power is too high for the
            # tariff the tariff won't ever be in the range of powers selected

    def test_reject_invalid_potencies_periode_15(self):
        invalid_pot_sol_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_pot_sol_xml_path, 'r') as f:
            invalid_pot_sol_xml = f.read()
            invalid_pot_sol_xml = invalid_pot_sol_xml.replace(
                '<Potencia Periodo="1">2300</Potencia>',
                '<Potencia Periodo="1">2300</Potencia>'
                '<Potencia Periodo="2">3450</Potencia>'
                '<Potencia Periodo="3">2300</Potencia>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_pot_sol_xml, 'invalid_pot_sol.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_pot_sol = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_pot_sol.rebuig, True)

            inv_pol_sol02 = sw_obj.get_pas(cursor, uid, invalid_pot_sol)
            cod_re = [reb.motiu_rebuig.name for reb in inv_pol_sol02.rebuig_ids]
            self.assertEqual(cod_re, ['15', '52'])

    def test_reject_invalid_tarifa_17(self):
        invalid_tarifa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_tarifa_xml_path, 'r') as f:
            invalid_tarifa_xml = f.read()
            invalid_tarifa_xml = invalid_tarifa_xml.replace(
                '<TarifaATR>001</TarifaATR>', '<TarifaATR>011</TarifaATR>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_tarifa_xml, 'invalid_tarifa.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])

            self.assertEqual(len(res), 1)

            invalid_tarifa = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_tarifa.rebuig, True)

            inv_tarifa02 = sw_obj.get_pas(cursor, uid, invalid_tarifa)
            cod_re = [reb.motiu_rebuig.name for reb in inv_tarifa02.rebuig_ids]
            self.assertEqual(cod_re, ['17', '52'])

    def test_reject_active_cups_21(self):
        active_cups_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(active_cups_xml_path, 'r') as f:
            active_cups_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, active_cups_xml, 'active_cups.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            active_cups = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(active_cups.rebuig, True)

            act_cups02 = sw_obj.get_pas(cursor, uid, active_cups)
            cod_re = [reb.motiu_rebuig.name for reb in act_cups02.rebuig_ids]
            self.assertEqual(cod_re, ['21'])

    def test_reject_invalid_CNAE_23(self):
        invalid_CNAE_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_CNAE_xml_path, 'r') as f:
            invalid_CNAE_xml = f.read()
            invalid_CNAE_xml = invalid_CNAE_xml.replace(
                '<CNAE>9820</CNAE>', '<CNAE></CNAE>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_CNAE_xml, 'invalid_CNAE.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_CNAE = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_CNAE.rebuig, True)

            inv_cnae02 = sw_obj.get_pas(cursor, uid, invalid_CNAE)
            cod_re = [reb.motiu_rebuig.name for reb in inv_cnae02.rebuig_ids]
            self.assertEqual(cod_re, ['23'])

    def test_no_reject_C2_without_cnae_23(self):

        solicitud_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')

        with open(solicitud_xml_path, 'r') as f:
            sol_xml = f.read()
            sol_xml = sol_xml.replace(
                '<CNAE>2222</CNAE>', ''
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, sol_xml, 'solicitud.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

    def test_reject_no_tallable_suministre_essencial_32(self):
        invalid_tall_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(invalid_tall_xml_path, 'r') as f:
            invalid_tall_xml = f.read()
            invalid_tall_xml = invalid_tall_xml.replace(
                '<Motivo>01</Motivo>',
                '<Motivo>03</Motivo>'
            )
        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.set_nocutoff(txn, essencial=True)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_tall_xml, 'invalid_tall.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_tall = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_tall.rebuig, True)

            inv_tall02 = sw_obj.get_pas(cursor, uid, invalid_tall)
            cod_re = [reb.motiu_rebuig.name for reb in inv_tall02.rebuig_ids]
            self.assertEqual(cod_re, ['32'])

    def test_reject_no_tallable_altres_circumstancies_34(self):
        invalid_tall_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(invalid_tall_xml_path, 'r') as f:
            invalid_tall_xml = f.read()
            invalid_tall_xml = invalid_tall_xml.replace(
                '<Motivo>01</Motivo>',
                '<Motivo>03</Motivo>'
            )
        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.set_nocutoff(txn, essencial=False)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_tall_xml, 'invalid_tall.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_tall = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_tall.rebuig, True)

            inv_tall02 = sw_obj.get_pas(cursor, uid, invalid_tall)
            cod_re = [reb.motiu_rebuig.name for reb in inv_tall02.rebuig_ids]
            self.assertEqual(cod_re, ['34'])

    def test_reject_invalid_tall_client_35(self):
        invalid_tall_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(invalid_tall_xml_path, 'r') as f:
            invalid_tall_xml = f.read()
            invalid_tall_xml = invalid_tall_xml.replace(
                '<Motivo>01</Motivo>',
                '<Motivo>03</Motivo>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.baixa_tall_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_tall_xml, 'invalid_tall.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_tall = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_tall.rebuig, True)

            inv_tall02 = sw_obj.get_pas(cursor, uid, invalid_tall)
            cod_re = [reb.motiu_rebuig.name for reb in inv_tall02.rebuig_ids]
            # Hi ha codi 2 i 35 perquè sempre que falli el 35 el 2 també ho farà.
            # El 35 falla quan el client al que se l'hi vol tallar el suministre ja se l'hi ha tallat,
            # per tan la seva polissa ja no estarà 'activa'.
            # Si ja no està 'activa', no hi haurà contracteATR en vigor i el 2 també fallarà
            self.assertEqual(cod_re, ['2', '35'])

    def test_reject_invalid_NIF_format_36(self):
        active_cups_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(active_cups_xml_path, 'r') as f:
            active_cups_xml = f.read()
            active_cups_xml = active_cups_xml.replace(
                '<Identificador>B82420654', '<Identificador>11111'
            )
        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, active_cups_xml, 'active_cups.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            active_cups = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(active_cups.rebuig, True)

            act_cups02 = sw_obj.get_pas(cursor, uid, active_cups)
            cod_re = [reb.motiu_rebuig.name for reb in act_cups02.rebuig_ids]
            self.assertEqual(cod_re, ['36'])

    def test_reject_solicitud_previa_A3_37(self):
        codi_sol_old = '201412111009'
        codi_sol_new = '201412111010'

        solicitud_previa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        solicitud_nova_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(solicitud_previa_xml_path, 'r') as f:
            sol_prev_xml = f.read()
        with open(solicitud_nova_xml_path, 'r') as f:
            sol_nova_xml = f.read()
            sol_nova_xml = sol_nova_xml.replace(codi_sol_old, codi_sol_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            # We first import a correct A3 XML
            sw_obj.importar_xml(
                cursor, uid, sol_prev_xml, 'solicitud_previa.xml'
            )
            # Without any reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

            # And then add a new XML with the same CUPS
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, sol_nova_xml, 'solicitud_nova.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sol_new)
            ])
            self.assertEqual(len(res), 1)

            sol_prev = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(sol_prev.rebuig, True)

            sol_prev02 = sw_obj.get_pas(cursor, uid, sol_prev)
            codis_reb = [reb.motiu_rebuig.name for reb in sol_prev02.rebuig_ids]
            self.assertEqual(codis_reb, ['37'])

    def test_reject_solicitud_previa_C1_38(self):
        solicitud_previa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        solicitud_nova_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(solicitud_previa_xml_path, 'r') as f:
            sol_prev_xml = f.read()
        with open(solicitud_nova_xml_path, 'r') as f:
            sol_nova_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            # We first import a correct C1 XML
            sw_obj.importar_xml(
                cursor, uid, sol_prev_xml, 'solicitud_previa.xml'
            )
            # Without any reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 0)

            # And then add a new XML with the same CUPS
            sw_obj.importar_xml(
                cursor, uid, sol_nova_xml, 'solicitud_nova.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            sol_prev = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(sol_prev.rebuig, True)

            sol_prev02 = sw_obj.get_pas(cursor, uid, sol_prev)
            codis_reb = [reb.motiu_rebuig.name for reb in sol_prev02.rebuig_ids]
            self.assertEqual(codis_reb, ['38'])

    def test_reject_solicitud_previa_C2_39(self):
        codi_sol_old = '201412111009'
        codi_sol_new = '201412111010'

        solicitud_previa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')

        with open(solicitud_previa_xml_path, 'r') as f:
            sol_prev_xml = f.read()
            sol_nova_xml = sol_prev_xml.replace(codi_sol_old, codi_sol_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            # We first import a correct C2 XML
            sw_obj.importar_xml(
                cursor, uid, sol_prev_xml, 'solicitud_previa.xml'
            )
            # Without any reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)
            # And then add a new XML with the same CUPS
            sw_obj.importar_xml(
                cursor, uid, sol_nova_xml, 'solicitud_nova.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sol_new)
            ])
            self.assertEqual(len(res), 1)

            sol_prev = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(sol_prev.rebuig, True)

            sol_prev02 = sw_obj.get_pas(cursor, uid, sol_prev)
            codis_reb = [reb.motiu_rebuig.name for reb in sol_prev02.rebuig_ids]
            self.assertEqual(codis_reb, ['39'])

    def test_reject_solicitud_previa_M1_40(self):
        codi_sol_old = '201412111009'
        codi_sol_new = '201412111010'

        solicitud_previa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        solicitud_nova_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(solicitud_previa_xml_path, 'r') as f:
            sol_prev_xml = f.read()
        with open(solicitud_nova_xml_path, 'r') as f:
            sol_nova_xml = f.read()
            sol_nova_xml = sol_nova_xml.replace(codi_sol_old, codi_sol_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor

            # We first import a correct C1 XML
            sw_obj.importar_xml(
                cursor, uid, sol_prev_xml, 'solicitud_previa.xml'
            )
            # Without any reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

            # And then add a new XML with the same CUPS
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, sol_nova_xml, 'solicitud_nova.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sol_new)
            ])
            self.assertEqual(len(res), 1)

            sol_prev = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(sol_prev.rebuig, True)

            sol_prev02 = sw_obj.get_pas(cursor, uid, sol_prev)
            codis_reb = [reb.motiu_rebuig.name for reb in sol_prev02.rebuig_ids]
            self.assertEqual(codis_reb, ['40'])

    def test_reject_retroactive_solicitation_date_46(self):
        sol_date_old = '<FechaPrevistaAccion>2016-06-06</FechaPrevistaAccion>'
        sol_date_new = '<FechaPrevistaAccion>2014-04-01</FechaPrevistaAccion>'

        reto_soli_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(reto_soli_xml_path, 'r') as f:
            ret_soli_xml = f.read()
            ret_soli_xml = ret_soli_xml.replace(sol_date_old, sol_date_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, ret_soli_xml, 'retroactive_solicitation.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            sol_prev = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(sol_prev.rebuig, True)

            sol_prev02 = sw_obj.get_pas(cursor, uid, sol_prev)
            codis_reb = [reb.motiu_rebuig.name for reb in sol_prev02.rebuig_ids]
            self.assertEqual(codis_reb, ['46'])

    def test_accept_equal_dates_check_46(self):
        sol_date_old = '<FechaPrevistaAccion>2015-05-27</FechaPrevistaAccion>'
        sol_date_new = '<FechaPrevistaAccion>2014-04-16</FechaPrevistaAccion>'

        reto_soli_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(reto_soli_xml_path, 'r') as f:
            ret_soli_xml = f.read()
            ret_soli_xml = ret_soli_xml.replace(sol_date_old, sol_date_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, ret_soli_xml, 'retroactive_solicitation.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

    def test_reject_tariff_not_valid_with_contracted_power_52(self):
        tariff_old = '<TarifaATR>001</TarifaATR>'
        tariff_new = '<TarifaATR>005</TarifaATR>'

        tariff_power_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(tariff_power_xml_path, 'r') as f:
            tariff_power_xml = f.read()
            tariff_power_xml = tariff_power_xml.replace(tariff_old, tariff_new)

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)

            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, tariff_power_xml, 'tariff_power.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            tariff_power = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(tariff_power.rebuig, True)

            tarif_power02 = sw_obj.get_pas(cursor, uid, tariff_power)
            cod_re = [reb.motiu_rebuig.name for reb in tarif_power02.rebuig_ids]
            self.assertEqual(cod_re, ['14', '52'])

    def test_reject_invalid_canvi_client_55(self):
        invalid_canvi_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_canvi_xml_path, 'r') as f:
            invalid_canvi_xml = f.read()
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<Identificador>B36385870', '<Identificador>B82420654'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_canvi_xml, 'invalid_canvi.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_canvi = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_canvi.rebuig, True)

            inv_cnv02 = sw_obj.get_pas(cursor, uid, invalid_canvi)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_cnv02.rebuig_ids]
            self.assertEqual(codis_reb, ['55'])

    def test_reject_invalid_tipus_canvi_client_56(self):
        invalid_canvi_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(invalid_canvi_xml_path, 'r') as f:
            invalid_canvi_xml = f.read()
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<Identificador>B36385870', '<Identificador>J5731503H')
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<TipoSolicitudAdministrativa>S</TipoSolicitudAdministrativa>',
                ''
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_canvi_xml, 'invalid_canvi.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_canvi = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_canvi.rebuig, True)

            inv_cnv02 = sw_obj.get_pas(cursor, uid, invalid_canvi)
            self.assertTrue(inv_cnv02.enviament_pendent)
            codis_reb = [reb.motiu_rebuig.name for reb in
                         inv_cnv02.rebuig_ids]
            self.assertEqual(codis_reb, ['56'])

    def test_reject_invalid_mod_potencia_65(self):
        invalid_canvi_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(invalid_canvi_xml_path, 'r') as f:
            invalid_canvi_xml = f.read()
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<FechaPrevistaAccion>2015-05-18',
                '<FechaPrevistaAccion>2016-08-18'
            )
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<TipoModificacion>S</TipoModificacion>',
                '<TipoModificacion>N</TipoModificacion>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            # Creem la segona modificació contractual fent un canvi de potencia
            self.crear_modcon(txn, 8.5, '2016-03-02', '2016-05-02')

            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_canvi_xml, 'invalid_canvi.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_canvi = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_canvi.rebuig, True)

            inv_cnv02 = sw_obj.get_pas(cursor, uid, invalid_canvi)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_cnv02.rebuig_ids]
            self.assertEqual(codis_reb, ['3', '65'])

    def test_reject_invalid_mod_potencia_65_no_fecha(self):
        """
        When no FechaPrevistaAccion, test is always ok
        """
        invalid_canvi_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(invalid_canvi_xml_path, 'r') as f:
            invalid_canvi_xml = f.read()
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<FechaPrevistaAccion>2016-06-06</FechaPrevistaAccion>',
                ''
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            # Creem la segona modificació contractual fent un canvi de potencia
            self.crear_modcon(txn, 8.5, '2016-03-02', '2016-05-02')

            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_canvi_xml, 'invalid_canvi.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

    def test_reject_ended_contract_72(self):
        # Ended means that the contract is "baixa"
        ended_contract_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(ended_contract_xml_path, 'r') as f:
            ended_contract_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.baixa_tall_polissa_CUPS(txn)

            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, ended_contract_xml, 'ended_contract.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            ended_contract = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(ended_contract.rebuig, True)

            end_contr02 = sw_obj.get_pas(cursor, uid, ended_contract)
            cod_re = [reb.motiu_rebuig.name for reb in end_contr02.rebuig_ids]
            self.assertTrue('72' in cod_re)

    def test_reject_excessive_power_73(self):
        excessive_power_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(excessive_power_xml_path, 'r') as f:
            excessive_power_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'icp')
            uid = txn.user
            cursor = txn.cursor

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]
            polissa_obj.write(cursor, uid, [polissa_id], {
                'potencia': 20,
            })

            sw_obj.importar_xml(
                cursor, uid, excessive_power_xml, 'excessive_power.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            excessive_power = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(excessive_power.rebuig, True)

            exc_power02 = sw_obj.get_pas(cursor, uid, excessive_power)
            cod_re = [reb.motiu_rebuig.name for reb in exc_power02.rebuig_ids]
            self.assertEqual(cod_re, ['73'])

    def test_reject_two_or_more_meters_74(self):
        excessive_meters_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(excessive_meters_xml_path, 'r') as f:
            excessive_meters_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'icp')
            uid = txn.user
            cursor = txn.cursor

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            compt_obj.create(
                cursor, uid, {'name': '063011077', 'polissa': polissa_id}
            )

            sw_obj.importar_xml(
                cursor, uid, excessive_meters_xml, 'excessive_meters.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            excessive_meters = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(excessive_meters.rebuig, True)

            exc_meters02 = sw_obj.get_pas(cursor, uid, excessive_meters)
            cod_re = [reb.motiu_rebuig.name for reb in exc_meters02.rebuig_ids]
            self.assertEqual(cod_re, ['74'])

    def test_reject_wrong_number_readings_75(self):
        lectura_old = '	</LecturaAportada>'
        lectura_new = '	</LecturaAportada>\n' \
                      '	<LecturaAportada>\n' \
                      '		<Integrador>AE</Integrador>\n' \
                      '		<TipoCodigoPeriodoDH>22</TipoCodigoPeriodoDH>\n' \
                      '		<LecturaPropuesta>0000003106.00</LecturaPropuesta>\n' \
                      '	</LecturaAportada>'

        extra_readings_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(extra_readings_xml_path, 'r') as f:
            extra_readings_xml = f.read()
            extra_readings_xml = extra_readings_xml.replace(
                lectura_old, lectura_new, 1
            )
            extra_readings_xml = extra_readings_xml.replace(
                '<CodigoPeriodoDH>10</CodigoPeriodoDH>',
                '<CodigoPeriodoDH>21</CodigoPeriodoDH>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'icp')
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, extra_readings_xml, 'extra_readings.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            extra_readings = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(extra_readings.rebuig, True)

            extra_read02 = sw_obj.get_pas(cursor, uid, extra_readings)
            cod_re = [reb.motiu_rebuig.name for reb in extra_read02.rebuig_ids]
            self.assertEqual(cod_re, ['75'])

    def test_correct_check_75(self):
        extra_readings_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(extra_readings_xml_path, 'r') as f:
            extra_readings_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'icp')
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, extra_readings_xml, 'extra_readings.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

    def test_reject_too_many_digits_76(self):
        lectura_old = '<LecturaPropuesta>0000001162.00</LecturaPropuesta>'
        lectura_new = '<LecturaPropuesta>1000001162.00</LecturaPropuesta>'

        extra_digits_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(extra_digits_xml_path, 'r') as f:
            extra_digits_xml = f.read()
            extra_digits_xml = extra_digits_xml.replace(
                lectura_old, lectura_new
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'icp')
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, extra_digits_xml, 'extra_digits.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            extra_digits = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(extra_digits.rebuig, True)

            extra_dig02 = sw_obj.get_pas(cursor, uid, extra_digits)
            cod_re = [reb.motiu_rebuig.name for reb in extra_dig02.rebuig_ids]
            self.assertEqual(cod_re, ['76'])

    def test_reject_periodo_lectura_inferior_79(self):
        periodo_inf_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(periodo_inf_xml_path, 'r') as f:
            periodo_inf_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        imd_obj = self.openerp.pool.get('ir.model.data')
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'icp')
            uid = txn.user
            cursor = txn.cursor

            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

            compt_id = polissa_obj.read(
                cursor, uid, polissa_id, ['comptadors']
            )['comptadors'][0]

            periode_id = imd_obj.get_object_reference(
                cursor, uid,
                'giscedata_polissa', 'p1_e_tarifa_20A_new'
            )[1]

            origen_id = imd_obj.get_object_reference(
                cursor, uid,
                'giscedata_lectures', 'origen10'
            )[1]

            # We create a 'lectura' with a date after the one we are importing
            lect_obj.create(
                cursor, uid, {
                    'name': '2017-03-18',
                    'lectura': 1000,
                    'comptador': compt_id,
                    'tipus': 'A',
                    'periode': periode_id,
                    'origen_id': origen_id,
                }
            )

            # And then try to import the xml
            sw_obj.importar_xml(
                cursor, uid, periodo_inf_xml, 'periodo_inf.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            periodo_inf = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(periodo_inf.rebuig, True)

            periodo_inf02 = sw_obj.get_pas(cursor, uid, periodo_inf)
            cod_re = [reb.motiu_rebuig.name for reb in periodo_inf02.rebuig_ids]
            self.assertEqual(cod_re, ['79'])

    def test_reject_subministration_with_maximeter_80(self):
        subm_maximeter_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(subm_maximeter_xml_path, 'r') as f:
            subm_max_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, 'max')
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, subm_max_xml, 'subm_max.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            subm_max = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(subm_max.rebuig, True)

            subm_max02 = sw_obj.get_pas(cursor, uid, subm_max)
            self.assertTrue(subm_max02.enviament_pendent)
            cod_re = [reb.motiu_rebuig.name for reb in subm_max02.rebuig_ids]
            self.assertEqual(cod_re, ['80'])

    def test_reject_reclamacio_rep_84(self):
        rec_rep_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(rec_rep_xml_path, 'r') as f:
            rec_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor

            # Import the firts R101
            sw_obj.importar_xml(
                cursor, uid, rec_xml, 'rec_.xml'
            )
            # Import the same R101 but with different 'CodigoDeSolicitud'
            rec_rep_xml = rec_xml.replace(
                '<CodigoDeSolicitud>201602231255',
                '<CodigoDeSolicitud>201412111009'
            )
            sw_obj.importar_xml(
                cursor, uid, rec_rep_xml, 'rec_rep.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            rec_rep = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(rec_rep.rebuig, True)

            rec_rep02 = sw_obj.get_pas(cursor, uid, rec_rep)
            codis_reb = [reb.motiu_rebuig.name for reb in rec_rep02.rebuig_ids]
            self.assertEqual(codis_reb, ['84'])

    def test_reject_reclamacio_camps_minims_85(self):
        rec_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(rec_xml_path, 'r') as f:
            rec_xml = f.read()
            rec_xml = rec_xml.replace(
                '<Subtipo>039</Subtipo>',
                '<Subtipo>003</Subtipo>'
            )
            rec_xml = rec_xml.replace(
                '<CodigoIncidencia>02</CodigoIncidencia>',
                ''
            )
        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, rec_xml, 'rec_.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            rec = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(rec.rebuig, True)

            rec02 = rec.get_pas()
            self.assertTrue(rec02.enviament_pendent)
            codis_reb = [reb.motiu_rebuig.name for reb in rec02.rebuig_ids]
            self.assertEqual(codis_reb, ['11', '85'])

    def test_reject_invalid_factura_87(self):
        inv_fact_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(inv_fact_xml_path, 'r') as f:
            rec_xml = f.read()
            rec_xml = rec_xml.replace(
                '<Tipo>05</Tipo>',
                '<Tipo>02</Tipo>'
            )
            rec_xml = rec_xml.replace(
                '<Subtipo>039</Subtipo>',
                '<Subtipo>010</Subtipo>'
            )
            rec_xml = rec_xml.replace(
                '<NumFacturaATR>243615</NumFacturaATR>',
                '<NumFacturaATR>9991/F</NumFacturaATR>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, rec_xml, 'rec_rep.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            inv_fac = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(inv_fac.rebuig, True)

            inv_fac02 = sw_obj.get_pas(cursor, uid, inv_fac)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_fac02.rebuig_ids]
            self.assertEqual(codis_reb, ['11', '87'])

    def test_reject_factura_no_telegestionada_88(self):
        inv_fact_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'r101_new.xml')
        with open(inv_fact_xml_path, 'r') as f:
            rec_xml = f.read()
            rec_xml = rec_xml.replace(
                '<Subtipo>039</Subtipo>',
                '<Subtipo>047</Subtipo>'
            )
            rec_xml = rec_xml.replace(
                '<NumFacturaATR>243615</NumFacturaATR>',
                '<NumFacturaATR>0001/F</NumFacturaATR>'
            )
            rec_xml = rec_xml.replace(
                """<VariableDetalleReclamacion>
                <NumExpedienteAcometida>22222</NumExpedienteAcometida>
            </VariableDetalleReclamacion>""",
                ''
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.no_telegestio_factura(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, rec_xml, 'rec_rep.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'R1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201602231255')
            ])
            self.assertEqual(len(res), 1)

            inv_fac = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(inv_fac.rebuig, True)

            inv_fac02 = sw_obj.get_pas(cursor, uid, inv_fac)
            codis_reb = [reb.motiu_rebuig.name for reb in inv_fac02.rebuig_ids]
            self.assertEqual(codis_reb, ['11', '88'])

    def test_reject_solicitud_previa_B1m01_B5(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            sw = sw_obj.browse(cursor, uid, res[0])
            self.assertFalse(sw.rebuig)

            # Import an M1 that will be rejected for B1m01 en curs
            sw_obj.importar_xml(cursor, uid, m1_xml, ' m101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertTrue(m1.rebuig)
            m102 = sw_obj.get_pas(cursor, uid, m1)
            codis_reb = [reb.motiu_rebuig.name for reb in m102.rebuig_ids]
            self.assertEqual(codis_reb, ['B5'])

    def test_reject_solicitud_previa_B1m02_B6(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()
            b1_xml = b1_xml.replace("<Motivo>01", "<Motivo>02")

        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            sw = sw_obj.browse(cursor, uid, res[0])
            self.assertFalse(sw.rebuig)

            # Import an M1 that will be rejected for B1m02 en curs
            sw_obj.importar_xml(cursor, uid, m1_xml, ' m101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertTrue(m1.rebuig)
            m102 = sw_obj.get_pas(cursor, uid, m1)
            codis_reb = [reb.motiu_rebuig.name for reb in m102.rebuig_ids]
            self.assertEqual(codis_reb, ['B6'])

    def test_reject_solicitud_previa_B1m03_B7(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()
            b1_xml = b1_xml.replace("<Motivo>01", "<Motivo>03")

        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()
            c1_xml = c1_xml.replace(
                "<ContratacionIncondicionalPS>S",
                "<ContratacionIncondicionalPS>N"
            )

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            sw = sw_obj.browse(cursor, uid, res[0])
            self.assertFalse(sw.rebuig)

            # Import an C1 without ContractacionIncondicionalPS
            # that will be rejected for B1m03 en curs
            self.switch(txn, 'distri')
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(cursor, uid, c1_xml, ' c101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertTrue(c1.rebuig)
            c102 = sw_obj.get_pas(cursor, uid, c1)
            codis_reb = [reb.motiu_rebuig.name for reb in c102.rebuig_ids]
            self.assertEqual(codis_reb, ['B7'])

    def test_no_reject_solicitud_previa_B1m03_B7(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()
            b1_xml = b1_xml.replace("<Motivo>01", "<Motivo>03")

        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            sw = sw_obj.browse(cursor, uid, res[0])
            self.assertFalse(sw.rebuig)

            # Import an C1 with ContractacionIncondicionalPS
            # that won't be rejected for B1m03 en curs
            self.switch(txn, 'distri')
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(cursor, uid, c1_xml, ' c101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertFalse(c1.rebuig)

    def test_reject_solicitud_previa_B1m04_B8(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()
            b1_xml = b1_xml.replace("<Motivo>01", "<Motivo>04")

        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()
            c1_xml = c1_xml.replace(
                "<ContratacionIncondicionalPS>S",
                "<ContratacionIncondicionalPS>N"
            )

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            sw = sw_obj.browse(cursor, uid, res[0])
            self.assertFalse(sw.rebuig)

            # Import an C1 without ContractacionIncondicionalPS
            # that will be rejected for B1m03 en curs
            self.switch(txn, 'distri')
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(cursor, uid, c1_xml, ' c101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)
            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertTrue(c1.rebuig)
            c102 = sw_obj.get_pas(cursor, uid, c1)
            codis_reb = [reb.motiu_rebuig.name for reb in c102.rebuig_ids]
            self.assertEqual(codis_reb, ['B8'])

    def test_reject_invalid_potencies_multiples_D3(self):
        invalid_pot_sol_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_pot_sol_xml_path, 'r') as f:
            invalid_pot_sol_xml = f.read()
            invalid_pot_sol_xml = invalid_pot_sol_xml.replace(
                '<Potencia Periodo="1">2300', '<Potencia Periodo="1">2111'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, invalid_pot_sol_xml, 'invalid_pot_sol.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            invalid_pot_sol = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(invalid_pot_sol.rebuig, True)

            inv_pol_sol02 = sw_obj.get_pas(cursor, uid, invalid_pot_sol)
            cod_re = [reb.motiu_rebuig.name for reb in inv_pol_sol02.rebuig_ids]
            self.assertEqual(cod_re, ['D3'])


    def test_reject_valid_potencies_all_0_D3(self):
        # Some partners send all powers filled with 0
        invalid_pot_sol_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_pot_sol_xml_path, 'r') as f:
            invalid_pot_sol_xml = f.read()
            invalid_pot_sol_xml = invalid_pot_sol_xml.replace(
                '<Potencia Periodo="1">2300',
                '''<Potencia Periodo="1">2300</Potencia>
                   <Potencia Periodo="2">0</Potencia>
                   <Potencia Periodo="3">0</Potencia>
                   <Potencia Periodo="4">0</Potencia>
                   <Potencia Periodo="5">0</Potencia>
                   <Potencia Periodo="6">0
                   '''
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            sw_obj.importar_xml(
                cursor, uid, invalid_pot_sol_xml, 'invalid_pot_sol.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

class TestSwitchingNotificationMail(TestSwitchingImport):
    _template_base_name = 'notification_atr_{}_{}'
    _model_base_name = 'giscedata.switching.{}.{}'

    def check_rebuig_step(self, process, step):
        if step == '02':
            return True
        if process in ['A3', 'M1']:
            if step == '07':
                return True
        if process in ['B1']:
            if step == '04':
                return True
        if process in ['C1', 'C2']:
            if step == '09':
                return True
        return False

    def create_init_step(self, process, step):
        pre_step = '01'
        pool = self.openerp.pool
        if self.txn is None:
            raise ValueError("Can't create previous step without Transaction")
        if pre_step == '01':
            self.switch(self.txn, 'comer', other_id_name='res_partner_asus')
        uid = self.txn.user
        cursor = self.txn.cursor
        contract_id = self.get_contract_id(self.txn)
        # Check for previous step emisor

        step_id = self.create_case_and_step(
            cursor, uid, contract_id, str.upper(process), pre_step
        )

        model = self._model_base_name.format(str.lower(process), pre_step)
        model_obj = pool.get(model)
        sw = model_obj.browse(cursor, uid, step_id).sw_id
        if pre_step == '01':
            self.switch(self.txn, 'distri')
        return sw

    def check_template_for_process_and_step(
            self, cursor, uid, contract_id, process, step_name, rebuig=False):
        pool = self.openerp.pool
        data_obj = pool.get('ir.model.data')

        rebuig_step = self.check_rebuig_step(process, step_name)
        template_name = self._template_base_name.format(
            str.upper(process), step_name
        )
        model = self._model_base_name.format(str.lower(process), step_name)
        try:
            # Try to create step without previous step
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, str.upper(process), step_name
            )
        except:
            # If create case and step fails, first create the case with a 01
            sw_case = self.create_init_step(process, step_name)
            self.create_step(cursor, uid, sw_case, process, step_name)
            step_id = sw_case.get_pas().id

        model_obj = pool.get(model)

        if rebuig_step:
            if rebuig:
                template_name += '_rebuig'
                model_obj.write(cursor, uid, [step_id], {
                    'rebuig': rebuig
                })
            else:
                template_name += '_acceptacio'

        step = model_obj.browse(cursor, uid, step_id)

        # Test template name getter
        sw_template_name = step.get_notification_mail_name()
        self.assertTrue(sw_template_name,
                        msg='Step {}-{} does not return template_name'.format(
                            process, step_name))
        self.assertEqual(
            sw_template_name[0],
            template_name,
            msg=(
                'Get Notification Mail from SW Step {}-{}'
                ' does not return a correct template name'.format(
                    process, step_name
                )
            )
        )

        # Get template resource directly
        # Must exist in DB a template in "giscedata_switching" module
        # With XML id name as "notification_atr_{ProcesName}_{StepName}
        # If rebuig step, append "_acceptacio" or "_rebuig"
        template = data_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', template_name
        )[1]

        # Compare template got directly and the one got by step method
        assert step.header_id.get_notification_mail()[0] == template

        return step_id

    def test_default_enviament_pendent(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        a301_obj = pool.get('giscedata.switching.a3.01')

        # Load A3-01 (distri) - EnviamentPendent : False

        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            sw_a3 = sw_obj.browse(cursor, uid, res[0])
            res = a301_obj.search(cursor, uid, [
                ('sw_id', '=', sw_a3.id),
            ])
            a301 = a301_obj.browse(cursor, uid, res[0])
            self.assertFalse(
                a301.enviament_pendent,
                'EnviamentPendent should be FALSE. '
                'A3-01 on DISTRI is not an Emisor Step.'
            )

        # Load A3-01 (comer) - EnviamentPendent : True

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )
            a301 = a301_obj.browse(cursor, uid, step_id)
            self.assertTrue(
                a301.enviament_pendent,
                'EnviamentPendent should be TRUE. '
                'A3-01 on COMER is an Emisor Step.'
            )

    def check_default_notificacio_pendent(self, conf_var='all',
                                          case_name='A3', step_name='01',
                                          expected_notify=True):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            cursor = txn.cursor
            uid = txn.user
            pool = self.openerp.pool
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            contract_id = self.get_contract_id(txn)
            sw_obj = pool.get('giscedata.switching')
            pas_model_name = 'giscedata.switching.{}.{}'.format(
                case_name, step_name).lower()
            pas_obj = pool.get(pas_model_name)
            config_obj = pool.get('res.config')
            sw_notify_steps_id = config_obj.search(
                cursor, uid,
                [('name', '=', 'sw_mail_user_notification_on_activation')]
            )
            self.assertTrue(
                sw_notify_steps_id,
                'Config Var `sw_mail_user_notification_on_activation`'
                ' not initialized!'
            )
            config_obj.write(
                cursor, uid, sw_notify_steps_id, {'value': conf_var}
            )

            # Create the sw step
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, case_name, step_name
            )
            pas = pas_obj.browse(cursor, uid, step_id)
            self.assertEqual(
                pas.notificacio_pendent,
                expected_notify,
                'NotificacioPendent on {}-{} should be {}'
                ' when conf_var is `{}`'.format(
                    case_name, step_name, expected_notify, conf_var
                )
            )

    def test_default_notificacio_pendent(self):

        # When conf_var = 'all', new steps should be 'notificacio_pendent'=True
        self.check_default_notificacio_pendent()

        # When conf_var = ['A3-01'],
        # - new A301 should have 'notificacio_pendent'=True
        # - new steps (not A301) should have 'notificacio_pendent'=False
        conf_var_value = "['A3-01']"
        self.check_default_notificacio_pendent(conf_var=conf_var_value)
        self.check_default_notificacio_pendent(conf_var=conf_var_value,
                                               case_name='C1',
                                               expected_notify=False)

        # When conf_var = ['A3-01', 'C1-01'],
        # - new A301 should have 'notificacio_pendent'=True
        # - new C101 should have 'notificacio_pendent'=True
        # - new steps (not A301 or C101) should have 'notificacio_pendent'=False
        conf_var_value = "['A3-01', 'C1-01']"
        self.check_default_notificacio_pendent(conf_var=conf_var_value)
        self.check_default_notificacio_pendent(conf_var=conf_var_value,
                                               case_name='C1')
        self.check_default_notificacio_pendent(conf_var=conf_var_value,
                                               case_name='B1',
                                               expected_notify=False)
