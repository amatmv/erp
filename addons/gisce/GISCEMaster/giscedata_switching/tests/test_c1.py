# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport
from .common_tests import TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from destral import testing
from expects import expect, raise_error
from osv.orm import except_orm
from datetime import datetime, timedelta
from workdays import *


class TestC1(TestSwitchingImport):

    def test_load_c1_01(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '01')
            self.assertEqual(c1.partner_id.ref, '4321')
            self.assertEqual(c1.company_id.ref, '1234')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')
            self.assertEqual(c1.codi_sollicitud, '201607211259')
            self.assertEqual(c1.data_sollicitud, '2016-07-21')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            c101 = sw_obj.get_pas(cursor, uid, c1)
            self.assertEqual(c101.data_accio, '2016-08-06')
            self.assertEqual(c101.activacio_cicle, 'L')
            self.assertEqual(c101.contratacion_incondicional_ps, 'S')
            self.assertEqual(c101.contratacion_incondicional_bs, 'S')
            self.assertEqual(c101.bono_social, '1')
            self.assertEqual(c101.tipus_document, 'NI')
            self.assertEqual(c101.codi_document, 'B82420654')
            self.assertEqual(c101.persona, 'J')
            self.assertEqual(c101.nom, u'ACC Y COMP DE COCINA MILLAN Y MUÑOZ')
            self.assertEqual(len(c101.telefons), 2)
            self.assertEqual(c101.telefons[1].numero, '666777999')

            self.assertEqual(c101.comentaris, 'Comentario sobre C1')
            self.assertEqual(len(c101.document_ids), 2)
            reg2 = c101.document_ids[1]
            self.assertEqual(reg2.type, '07')
            self.assertEqual(reg2.url, 'http://eneracme.com/docs/NIF11111111H.pdf')

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_create_c1_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c101 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)

            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '01')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(len(c101.telefons), 1)
            self.assertEqual(c101.telefons[0].numero, '935555555')
            self.assertEqual(c101.contratacion_incondicional_bs, 'N')
            self.assertFalse(c101.bono_social)

    def test_create_c1_02(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            self.change_polissa_comer(txn)
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '02'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.02')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c102 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c102.sw_id.id)

            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '02')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')

            today = datetime.now().strftime('%Y-%m-%d')
            self.assertEqual(c102.tarifaATR, '001')
            self.assertEqual(c102.tipus_activacio, 'L0')
            self.assertEqual(c102.actuacio_camp, 'N')
            self.assertFalse(c102.rebuig)
            self.assertFalse(c102.bono_social)
            self.assertGreaterEqual(c102.data_activacio, today)

    def test_load_c1_02(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)

            # Get data sol.licitud
            data_sol = c101.date_created

            # change the codi sol.licitud of c102.xml
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud
            c1_xml = c1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c102.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '02')
            self.assertEqual(c1.partner_id.ref, '1234')
            self.assertEqual(c1.company_id.ref, '4321')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')

            deadline = datetime.strptime(data_sol, "%Y-%m-%d %H:%M:%S") \
                       + timedelta(days=15)
            deadline = workday(deadline, 1)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            inf = u"Activació: La activación se debe producir cuanto antes. Data prevista: {0}. Tarifa:2.0A, Pot.: 6.0.".format(
                datetime.today().strftime("%Y-%m-%d")
            )
            self.assertEqual(inf, c1.additional_info)

            c102 = sw_obj.get_pas(cursor, uid, c1)
            self.assertEqual(c102.tarifaATR, '003')
            self.assertEqual(len(c102.pot_ids), 2)
            self.assertEqual(c102.tipus_contracteATR, '02')
            self.assertEqual(c102.tipus_activacio, 'C0')
            self.assertFalse(c102.rebuig)
            self.assertEqual(c102.bono_social, '1')

    def test_deadline_fix_activation_c1_02(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)

            # change the codi sol.licitud of c102.xml
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud
            c1_xml = c1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            c1_xml = c1_xml.replace(
                "<TipoActivacionPrevista>C0</TipoActivacionPrevista>",
                "<TipoActivacionPrevista>F0</TipoActivacionPrevista>"
            )
            # import step 02
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c102.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            deadline = workday(datetime.strptime("2016-07-06", "%Y-%m-%d"), 1)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

    def test_deadline_ciclo_lectura_c1_02(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)

            # Get data sol.licitud
            data_sol = c101.date_created

            # change the codi sol.licitud of c102.xml
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud
            c1_xml = c1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            c1_xml = c1_xml.replace(
                "<TipoActivacionPrevista>C0</TipoActivacionPrevista>",
                "<TipoActivacionPrevista>L0</TipoActivacionPrevista>"
            )
            # import step 02
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c102.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            deadline = datetime.strptime(data_sol, "%Y-%m-%d %H:%M:%S") \
                       + timedelta(days=36)
            deadline = workday(deadline, 1)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

    def test_load_c1_05(self):
        c102_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c102_new.xml')
        c105_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c105_new.xml')
        with open(c102_xml_path, 'r') as f:
            c102_xml = f.read()
        with open(c105_xml_path, 'r') as f:
            c105_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud

            # change the codi sol.licitud of c102.xml
            c102_xml = c102_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(cursor, uid, c102_xml, 'c102.xml')

            # change the codi sol.licitud of c105.xml
            c105_xml = c105_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, c105_xml, 'c105.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '05')
            self.assertEqual(c1.partner_id.ref, '1234')
            self.assertEqual(c1.company_id.ref, '4321')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')
            c105 = sw_obj.get_pas(cursor, uid, c1)
            self.assertEqual(c105.data_activacio, '2016-08-21')
            self.assertEqual(c105.bono_social, '1')
            self.assertEqual(c105.tipus_autoconsum, '00')
            self.assertEqual(c105.tipus_contracte, '02')
            self.assertEqual(c105.tarifaATR, '003')
            self.assertEqual(c105.periodicitat_facturacio, '01')
            self.assertEqual(c105.control_potencia, '1')
            self.assertEqual(c105.contracte_atr, '00001')
            self.assertEqual(c105.tipus_telegestio, '01')
            self.assertEqual(c105.marca_medida_bt, 'S')
            self.assertEqual(c105.kvas_trafo, 0.05)
            self.assertEqual(c105.perdues, 5.0)
            self.assertEqual(c105.tensio_suministre, '10')
            self.assertEqual(len(c105.pot_ids), 2)
            self.assertEqual(c105.pot_ids[1].potencia, 2000)
            self.assertEqual(len(c105.pm_ids), 1)
            pm = c105.pm_ids[0]
            self.assertEqual(pm.tipus_moviment, 'A')
            self.assertEqual(pm.tipus, '03')
            self.assertEqual(pm.mode_lectura, '1')
            self.assertEqual(pm.funcio_pm, 'P')
            self.assertEqual(pm.tensio_pm, 0)
            self.assertEqual(pm.data, '2003-01-01')
            self.assertEqual(pm.data_alta, '2003-01-01')
            self.assertEqual(pm.data_baixa, '2003-02-01')
            self.assertEqual(len(pm.aparell_ids), 1)
            ap = pm.aparell_ids[0]
            self.assertEqual(ap.tipus, 'CG')
            self.assertEqual(ap.marca, '132')
            self.assertEqual(ap.tipus_em, 'L03')
            self.assertEqual(ap.propietari, 'Desc. Propietario')
            self.assertEqual(ap.periode_fabricacio, '2005')
            # self.assertEqual(ap.lectura_directa, 'N')
            self.assertEqual(ap.num_serie, '0000539522')
            self.assertEqual(ap.funcio, 'M')
            self.assertEqual(ap.constant_energia, 1)
            self.assertEqual(len(ap.mesura_ids), 2)
            mes = ap.mesura_ids[0]
            self.assertEqual(mes.tipus_dh, '6')
            self.assertEqual(mes.periode, '65')
            self.assertEqual(mes.magnitud, 'PM')
            self.assertEqual(mes.origen, '30')
            self.assertEqual(mes.lectura, 6.00)
            self.assertEqual(mes.data_lectura, '2003-01-02')
            self.assertEqual(mes.anomalia, '01')
            self.assertEqual(mes.text_anomalia, 'Comentario sobre anomalia')

    def test_creation_c1_05(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c105 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c105.sw_id.id)

            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '05')
            self.assertEqual(c1.partner_id.ref, '5555')
            self.assertEqual(c1.company_id.ref, '1234')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')

            self.assertEqual(c105.contracte_atr, '0001')
            self.assertEqual(c105.control_potencia, '1')
            self.assertEqual(c105.tipus_contracte, '01')
            self.assertEqual(c105.tarifaATR, '001')
            self.assertEqual(c105.periodicitat_facturacio, '01')
            self.assertEqual(c105.tipus_telegestio, '01')
            self.assertEqual(c105.tipus_autoconsum, '00')
            self.assertFalse(c105.bono_social)

    def test_creation_c1_05_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c105 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c105.sw_id.id)

            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '05')
            self.assertEqual(c1.partner_id.ref, '5555')
            self.assertEqual(c1.company_id.ref, '1234')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')

            self.assertEqual(c105.contracte_atr, '0001')
            self.assertEqual(c105.control_potencia, '1')
            self.assertEqual(c105.tipus_contracte, '01')
            self.assertEqual(c105.tarifaATR, '001')
            self.assertEqual(c105.periodicitat_facturacio, '01')
            self.assertEqual(c105.tipus_telegestio, '01')
            self.assertEqual(c105.tipus_autoconsum, '01')
            self.assertEqual(c105.marca_medida_bt, 'N')

    def test_additional_info_c1(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            c1 = sw_obj.browse(cursor, uid, res[0])
            inf = u"Bo Social: Si. Activació: La activación se debe producir con próxima lectura del ciclo. Data prevista: 2016-08-06. Tarifa:2.0A, Pot.: 6.0"
            self.assertEqual(inf, c1.additional_info)

    def test_additional_info_c1_no_bono(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()
            c1_xml = c1_xml.replace("<BonoSocial>1</BonoSocial>", "")

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            c1 = sw_obj.browse(cursor, uid, res[0])
            inf = u"Activació: La activación se debe producir con próxima lectura del ciclo. Data prevista: 2016-08-06. Tarifa:2.0A, Pot.: 6.0"
            self.assertEqual(inf, c1.additional_info)

    def test_additional_info_c1_02_rej(self):

        solicitud_previa_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        solicitud_nova_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(solicitud_previa_xml_path, 'r') as f:
            sol_prev_xml = f.read()
        with open(solicitud_nova_xml_path, 'r') as f:
            sol_nova_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor

            # We first import a correct C1 XML
            sw_obj.importar_xml(
                cursor, uid, sol_prev_xml, 'solicitud_previa.xml'
            )
            # Without any reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 0)

            # And then add a new XML with the same CUPS
            sw_obj.importar_xml(
                cursor, uid, sol_nova_xml, 'solicitud_nova.xml'
            )

            # Which should generate a reject for the second XML
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c1.rebuig, True)
            inf = u'Bo Social: No. (S) ESB82420654 -> B36385870. Tarifa:2.0A, Pot.: 6.0. Rebuig: Existencia de Solicitud previa en curso C1'
            self.assertEqual(inf, c1.additional_info)

    def test_creation_c1_05_marca_medida_bt(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            self.set_nova_tarifa(txn, '3.1A LB')
            self.set_trafo(txn, 160)
            self.set_mesura_ab(txn, 'B')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c105 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c105.sw_id.id)

            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '05')
            self.assertEqual(c1.partner_id.ref, '5555')
            self.assertEqual(c1.company_id.ref, '1234')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')

            self.assertEqual(c105.control_potencia, '1')
            self.assertEqual(c105.tipus_contracte, '01')
            self.assertEqual(c105.tarifaATR, '011')
            self.assertEqual(c105.periodicitat_facturacio, '01')
            self.assertEqual(c105.tipus_telegestio, '01')
            self.assertEqual(c105.tipus_autoconsum, '00')
            self.assertEqual(c105.marca_medida_bt, 'S')
            self.assertEqual(c105.perdues, 4.0)
            self.assertEqual(c1.cups_polissa_id.trafo, 160.00)

    def test_create_c1_08(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '08'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c1.08')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c108 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c108.sw_id.id)

            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '08')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c108.header_id.sw_id.id, c1.id)

    def test_load_c1_08(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c108_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c101 = step_obj.browse(cursor, uid, step_id)
            c1 = sw_obj.browse(cursor, uid, c101.sw_id.id)
            codi_sollicitud = c1.codi_sollicitud
            # change the codi sol.licitud of c108.xml
            c1_xml = c1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import step 08
            self.switch(txn, 'distri', other_id_name='res_partner_asus')
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c108.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '08'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c1.proces_id.name, 'C1')
            self.assertEqual(c1.step_id.name, '08')
            self.assertEqual(c1.partner_id.ref, '4321')
            self.assertEqual(c1.company_id.ref, '1234')
            self.assertEqual(c1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c1.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)
            c108 = sw_obj.get_pas(cursor, uid, c1)
            self.assertEqual(c108.header_id.sw_id.id, c1.id)

    def test_create_c1_02_with_11_and_05_with_06(self):
        c1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c101_new.xml')
        with open(c1_xml_path, 'r') as f:
            c1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            sw_obj.importar_xml(
                cursor, uid, c1_xml, 'c101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201607211259')
            ])
            self.assertEqual(len(res), 1)
            sw_id = res[0]

            # Use wizard to create step 02, this will create step 11 too
            wiz_create_step_obj = self.openerp.pool.get("wizard.create.step")
            wiz_create_step_id = wiz_create_step_obj.create(cursor, uid, {}, context={'active_ids': [sw_id]})
            wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id], {'step': '02'})
            wiz_create_step_obj.action_create_steps(cursor, uid, [wiz_create_step_id], context={'active_ids': [sw_id]})
            # Check created steps
            sw = sw_obj.browse(cursor, uid, sw_id)
            self.assertEqual(sw.get_pas()._nom_pas, "02",
                             msg="El pas actual hauria de ser 02")
            step_11_created, step_02_created = False, False
            for step in sw.step_ids:
                if step.step_id.name == "02":
                    step_02_created = True
                    self.assertEqual(
                        sw.get_pas(step_id=step.step_id.id)._nom_pas, "02")
                elif step.step_id.name == "11":
                    step_11_created = True
                    self.assertEqual(
                        sw.get_pas(step_id=step.step_id.id)._nom_pas, "11")
            self.assertTrue(step_02_created)
            self.assertTrue(step_11_created)

            # Use wizard to create step 05, this will create step 06 too
            wiz_create_step_id = wiz_create_step_obj.create(cursor, uid, {}, context={'active_ids': [sw_id]})
            wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id], {'step': '05'})
            wiz_create_step_obj.action_create_steps(cursor, uid, [wiz_create_step_id], context={'active_ids': [sw_id]})
            # Check created steps
            sw = sw_obj.browse(cursor, uid, sw_id)
            step_06_created, step_05_created = False, False
            for step in sw.step_ids:
                if step.step_id.name == "05":
                    step_05_created = True
                    self.assertEqual(
                        sw.get_pas(step_id=step.step_id.id)._nom_pas, "05")
                elif step.step_id.name == "06":
                    step_06_created = True
                    self.assertEqual(
                        sw.get_pas(step_id=step.step_id.id)._nom_pas, "06")
            self.assertTrue(step_05_created)
            self.assertTrue(step_06_created)


class TestNotificationMailsC1(TestSwitchingNotificationMail):
    _proces = 'C1'

    def test_get_notification_mail_templates_c1(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        b1_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = b1_obj.get_steps(cursor, uid, False, self._proces)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)

                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
