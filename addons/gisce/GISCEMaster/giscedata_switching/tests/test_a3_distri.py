# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport
from .common_tests import TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime


class TestA3Distri(TestSwitchingImport):

    def test_a3_02_activate_contract_and_generate_step_05(self):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura')
        a3_05_obj = self.openerp.pool.get('giscedata.switching.a3.05')
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        sw_help_dist_obj = self.openerp.pool.get(
            'giscedata.switching.helpers.distri'
        )
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a302.xml
            a3_xml = a3_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import a302.xml
            sw_obj.importar_xml(cursor, uid, a3_xml, 'a302.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '02')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a301.seq_sollicitud, '01')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            deadline = workday(datetime.strptime("2016-12-07", "%Y-%m-%d"), 6)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            a302 = sw_obj.get_pas(cursor, uid, a3)
            self.assertEqual(a302.data_acceptacio, '2016-12-07')
            self.assertEqual(a302.actuacio_camp, 'N')
            self.assertEqual(a302.tarifaATR, '001')
            self.assertEqual(a302.tipus_activacio, 'C0')

            self.switch(txn, 'distri')
            # Change to distri
            sw_obj.write(cursor, uid, a3.id, {'whereiam': 'distri'})
            ssql = '''
            update giscedata_switching set whereiam = 'distri'
            where id = %s
            '''
            cursor.execute(ssql, (a3.id,))

            a3 = sw_obj.browse(cursor, uid, res[0])

            sql = '''
            SELECT MAX(l.name) FROM giscedata_lectures_lectura AS l
            RIGHT JOIN giscedata_lectures_comptador AS c ON (l.comptador = c.id)
            RIGHT JOIN giscedata_polissa AS p ON (c.polissa = p.id)
            WHERE p.id = %s
            '''

            cursor.execute(sql, (a3.cups_polissa_id.id,))

            ultima_lectura = cursor.fetchall()[0][0]

            expected_date_activacio = datetime.strftime(
                datetime.strptime(ultima_lectura, '%Y-%m-%d') + timedelta(
                    days=1),
                '%Y-%m-%d'
            )

            data_lectura = datetime.strptime(
                ultima_lectura, '%Y-%m-%d'
            )
            today = datetime.today()

            diff_days = (today - data_lectura).days

            ctx = {'days_before': diff_days}

            pol_state = pol_obj.read(
                cursor, uid, a3.cups_polissa_id.id, ['state']
            )['state']
            self.assertEqual(pol_state, 'esborrany')

            pas_05_id = a3_05_obj.search(
                cursor, uid, [('sw_id', '=', a3.id)]
            )

            self.assertFalse(pas_05_id)

            pol_obj.write(
                cursor, uid, a3.cups_polissa_id.id, {
                    'lot_facturacio': False, 'data_baixa': False}
            )
            # self.change_polissa_comer(txn)
            res = sw_help_dist_obj.activa_polissa_from_a3(
                cursor, uid, a3.id, context=ctx
            )

            self.assertEqual(res[0], 'OK')

            pol_reads = pol_obj.read(
                cursor, uid, a3.cups_polissa_id.id, ['state', 'data_alta', 'data_firma_contracte']
            )
            self.assertEqual(pol_reads['state'], 'activa')
            self.assertEqual(pol_reads['data_alta'], expected_date_activacio)
            self.assertTrue(pol_reads['data_firma_contracte'])

            pas_05_id = a3_05_obj.search(
                cursor, uid, [('sw_id', '=', a3.id)]
            )[0]

            self.assertTrue(pas_05_id)
