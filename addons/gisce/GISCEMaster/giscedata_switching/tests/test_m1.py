# -*- coding: utf-8 -*-
from __future__ import absolute_import

import mock

from destral.patch import PatchNewCursors
from osv import osv, fields

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from destral import testing
from expects import expect, raise_error
from osv.orm import except_orm
from workdays import *
from datetime import date, datetime


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'facturacio_distri': fields.selection([(1, 'Mensual'), (2, 'Bimestral')],
                                              u'Facturació de distribuïdora',
                                              ),
    }

    _defaults = {
        'facturacio_distri': lambda *a: 1,
    }


GiscedataPolissa()


class TestM1(TestSwitchingImport):

    def test_load_m1_01(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, m1_xml, 'm101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '01')
            self.assertEqual(m1.partner_id.ref, '4321')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.codi_sollicitud, '201412111009')
            self.assertEqual(m1.data_sollicitud, '2014-04-16')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(m1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            m101 = sw_obj.get_pas(cursor, uid, m1)
            self.assertEqual(m101.sollicitudadm, 'S')
            self.assertEqual(m101.canvi_titular, 'S')
            self.assertEqual(m101.periodicitat_facturacio, '01')
            self.assertEqual(m101.activacio_cicle, 'L')
            self.assertEqual(m101.data_accio, '2016-06-06')
            self.assertEqual(m101.cnae.name, '2222')
            self.assertEqual(m101.data_final, '2018-01-01')
            self.assertEqual(m101.tipus_autoconsum, '00')
            self.assertEqual(m101.solicitud_tensio, 'T')
            self.assertEqual(m101.tipus_contracte, '02')
            self.assertEqual(m101.tarifaATR, '003')
            self.assertEqual(len(m101.pot_ids), 3)
            self.assertEqual(m101.pot_ids[1].potencia, 20000)
            self.assertEqual(m101.control_potencia, '1')
            self.assertEqual(m101.cont_nom, 'Nombre Inventado')
            self.assertEqual(len(m101.cont_telefons), 2)
            self.assertEqual(m101.cont_telefons[1].numero, '666777999')
            self.assertEqual(m101.tipus_document, 'NI')
            self.assertEqual(m101.codi_document, 'B36385870')
            self.assertEqual(m101.persona, 'J')
            self.assertEqual(m101.nom, u'ACC Y COMP DE COCINA MILLAN Y MUÑOZ')
            self.assertEqual(len(m101.telefons), 3)
            self.assertEqual(m101.telefons[2].numero, '666777666')
            self.assertEqual(m101.ind_direccio_fiscal, 'S')
            self.assertEqual(m101.fiscal_address_id.country_id.name, u'España')
            self.assertEqual(m101.fiscal_address_id.id_municipi.ine, '17079')
            self.assertEqual(m101.fiscal_address_id.nv[:30], u'MELA MUTERMILCH')
            self.assertEqual(m101.fiscal_address_id.aclarador, u'Bloque de Pisos')
            self.assertEqual(m101.equip_aportat_client, 'C')
            self.assertEqual(m101.tipus_equip_mesura, 'L00')
            self.assertEqual(m101.dt_cie_codigo, '1234567')
            self.assertEqual(m101.dt_codi_instalador, '12345678Z')
            self.assertEqual(m101.dt_apm_codigo, '1111111111')
            self.assertEqual(m101.dt_apm_potenciainstat, 5000)
            self.assertEqual(m101.dt_apm_data_emissio, '2015-06-04')
            self.assertEqual(m101.dt_apm_data_caducitat, '2016-06-04')
            self.assertEqual(m101.dt_apm_tipus_codi_instalador, 'codigo')
            self.assertEqual(m101.dt_apm_codi_instalador, '0550')
            self.assertEqual(m101.dt_apm_tensio_suministre, '20')
            self.assertEqual(m101.bono_social, '1')

            self.assertEqual(m101.comentaris, 'Comentario')

            self.assertEqual(len(m101.document_ids), 1)
            self.assertEqual(m101.document_ids[0].type, '06')
            self.assertEqual(
                m101.document_ids[0].url,
                'https://www.dropbox.com/s/q40impgt3tn0vtj/Reclama_%20da%C3%B1os_%20Montero%20Simon%2C%20Eduardo%20Tarsicio.pdf?dl=0'
            )

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def load_m1_01_automated_core(self, partner_param):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new_persona.xml'
        )
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            partner_obj = self.openerp.pool.get('res.partner')
            partner_ids = partner_obj.search(
                cursor, uid, [('vat', '=', "ES11111111H")]
            )
            partner_obj.write(cursor, uid, partner_ids, {'vat': "ES46557483D"})  # NIF aleatori per evitar treballar amb el ES1111111H

            imd_obj = self.openerp.pool.get('ir.model.data')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            conf_obj = self.openerp.pool.get('res.config')
            swhelp_distri_obj = self.openerp.pool.get(
                'giscedata.switching.helpers.distri'
            )
            auto_obj = self.openerp.pool.get('giscedata.switching.activation.config')
            auto_ids = auto_obj.search(cursor, uid, [])
            auto_obj.write(cursor, uid, auto_ids, {'is_automatic': False})

            # per aquest test, el titular ha de ser una persona i no una empresa
            # ja que més endavant ens fixarem si ha creat el nom correctament.
            m1_xml = m1_xml.replace(
                "<Identificador>11111111H",
                "<Identificador>ES11111111H"
            )

            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(cursor, uid, m1_xml, 'm101.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            sw_id = res[0]
            m1 = sw_obj.browse(cursor, uid, sw_id)
            m101 = sw_obj.get_pas(cursor, uid, m1)

            # Ens assegurem de que el partner no existeix
            partner_ids = partner_obj.search(
                cursor, uid, [('vat', '=', m101.codi_document)]
            )
            self.assertEqual(len(partner_ids), 0)

            name_struct = conf_obj.get(
                cursor, uid, 'partner_name_format', 'C1 C2, N'
            )
            if name_struct == 'C1 C2, N':
                name_formatter = "{C1} {C2}, {N}"
            else:
                name_formatter = "{N} {C1} {C2}"

            name_as_dict = {
                "N": m101.nom, "C1": m101.cognom_1, "C2": m101.cognom_2
            }

            if partner_param['create']:
                addr_id = imd_obj.get_object_reference(
                   cursor, uid, 'base', 'main_address'
                )[1]
                params = {
                    'vat': m101.codi_document,
                    'name': name_formatter.format(**name_as_dict),
                    'address': [(6, 0, [addr_id])]
                }
                partner_obj.create(cursor, uid, params)

            if partner_param['address']:
                m101.write({'fiscal_address_id': partner_param['address']})

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

            # Invoquem el mètode que automatitza algunes tasques quan s'importa
            # l'm1_01
            swhelp_distri_obj.activar_polissa_from_m1(cursor, uid, sw_id)

            partner_id = partner_obj.search(
                cursor, uid, [('vat', '=', m101.codi_document)]
            )
            self.assertEqual(len(partner_id), 1)
            partner_vals = partner_obj.read(
                cursor, uid, partner_id[0], ['address', 'name', 'vat']
            )

            # només es comprova quan és una persona jurídica o física.
            if not partner_obj.vat_es_empresa(cursor, uid, partner_vals['vat']):
                if name_struct == 'C1 C2, N':
                    assertName = self.assertIn
                else:
                    assertName = self.assertNotIn

                assertName(',', partner_vals['name'])

            partner_adrresses = partner_vals['address']
            if partner_param['address']:
                self.assertEqual(len(partner_adrresses), 1)
            else:
                self.assertIn(m101.fiscal_address_id.id, partner_adrresses)

    def test_load_m1_01_automated(self):
        partner = {
            'create': False,
            'address': None
        }
        # case 1 - create partner.
        self.load_m1_01_automated_core(partner)
        partner['create'] = True
        # case 2 - update partner.
        self.load_m1_01_automated_core(partner)
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            partner['address'] = imd_obj.get_object_reference(
                cursor, uid, 'base', 'main_address'
            )[1]
        # case 3 - no update partner.
        self.load_m1_01_automated_core(partner)

    def test_load_m1_01_eventual_no_meters(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures',
            'm101_new_eventual_no_meters.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, m1_xml, 'm101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '01')
            self.assertEqual(m1.partner_id.ref, '4321')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.codi_sollicitud, '201412111009')
            self.assertEqual(m1.data_sollicitud, '2014-04-16')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(m1.date_deadline,
                                       "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            m101 = sw_obj.get_pas(cursor, uid, m1)
            self.assertEqual(m101.sollicitudadm, 'S')
            self.assertEqual(m101.canvi_titular, 'S')
            self.assertEqual(m101.periodicitat_facturacio, '01')
            self.assertEqual(m101.activacio_cicle, 'L')
            self.assertEqual(m101.data_accio, '2016-06-06')
            self.assertEqual(m101.cnae.name, '2222')
            self.assertEqual(m101.data_final, '2018-01-01')
            self.assertEqual(m101.tipus_autoconsum, '00')
            self.assertEqual(m101.tipus_contracte, '09')
            self.assertEqual(m101.tarifaATR, '003')
            self.assertEqual(len(m101.pot_ids), 3)
            self.assertEqual(m101.pot_ids[1].potencia, 20000)
            self.assertEqual(m101.control_potencia, '1')
            self.assertEqual(m101.cont_nom, 'Nombre Inventado')
            self.assertEqual(m101.tipus_document, 'NI')
            self.assertEqual(m101.codi_document, 'B36385870')
            self.assertEqual(m101.persona, 'J')
            self.assertEqual(m101.nom,
                             u'ACC Y COMP DE COCINA MILLAN Y MUÑOZ')
            self.assertEqual(m101.ind_direccio_fiscal, 'S')
            self.assertEqual(m101.fiscal_address_id.country_id.name,
                             u'España')
            self.assertEqual(m101.fiscal_address_id.id_municipi.ine,
                             '17079')
            self.assertEqual(m101.fiscal_address_id.nv[:30],
                             u'MELA MUTERMILCH')
            self.assertEqual(m101.fiscal_address_id.aclarador,
                             u'Bloque de Pisos')
            self.assertEqual(m101.equip_aportat_client, 'C')
            self.assertEqual(m101.tipus_equip_mesura, 'L00')
            self.assertEqual(m101.dt_cie_codigo, '1234567')
            self.assertEqual(m101.dt_codi_instalador, '12345678Z')

            self.assertEqual(m101.comentaris, 'Comentario')

            self.assertEqual(len(m101.document_ids), 1)
            self.assertFalse(m101.bono_social)
            self.assertEqual(m101.document_ids[0].type, '06')
            self.assertEqual(
                m101.document_ids[0].url,
                'https://www.dropbox.com/s/q40impgt3tn0vtj/Reclama_%20da%C3%B1os_%20Montero%20Simon%2C%20Eduardo%20Tarsicio.pdf?dl=0'
            )
            self.assertEqual(m101.expected_consumption, 600)
            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_load_m1_01_eventual_no_meters_fails_if_no_consumption(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures',
            'm101_new_eventual_no_meters_no_consume.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, m1_xml, 'm101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            #if len(res) > 0:
            pas_02 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(pas_02.rebuig, True)

    def test_creation_m1_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '01')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            self.assertEqual(m101.control_potencia, '2')
            self.assertEqual(m101.tarifaATR, '001')
            self.assertEqual(m101.codi_document, 'B82420654')
            self.assertEqual(m101.tipus_autoconsum, '00')
            self.assertEqual(len(m101.telefons), 1)
            self.assertEqual(m101.cont_telefons[0].numero, '935555555')
            self.assertFalse(m101.bono_social)

    def test_creation_m1_01_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '01')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')
            self.assertEqual(m101.control_potencia, '1')
            self.assertEqual(m101.tarifaATR, '001')
            self.assertEqual(m101.codi_document, 'B82420654')
            self.assertEqual(m101.tipus_autoconsum, '01')

    def test_creation_m1_01_eventual_contract_no_meters(self):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            pol_obj.write(cursor, uid, contract_id, {'contract_type': '09',
                                                     'expected_consumption': 600
                                                     })
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '01')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            self.assertEqual(m101.control_potencia, '2')
            self.assertEqual(m101.tarifaATR, '001')
            self.assertEqual(m101.codi_document, 'B82420654')
            self.assertEqual(m101.tipus_autoconsum, '00')
            self.assertEqual(m101.expected_consumption, 600)

    def test_load_m1_02(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm102_new.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            m101 = step_obj.browse(cursor, uid, step_id)

            # Get data sol.licitud
            data_sol = m101.date_created

            # change the codi sol.licitud of m102.xml
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)
            codi_sollicitud = m1.codi_sollicitud
            m1_xml = m1_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import m102.xml
            sw_obj.importar_xml(cursor, uid, m1_xml, 'm102.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '02')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            deadline = datetime.strptime(data_sol, "%Y-%m-%d %H:%M:%S") \
                       + timedelta(days=15)
            deadline = workday(deadline, 1)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(m1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            inf = u'(N) 2.0A -> 2.0A. max -> Maxímetro; Potencies: P1: 6.0 -> P1: 6.0. .'
            self.assertEqual(inf, m1.additional_info)

            m102 = sw_obj.get_pas(cursor, uid, m1)
            self.assertEqual(m102.data_acceptacio, '2016-12-07')
            self.assertEqual(m102.actuacio_camp, 'N')
            self.assertEqual(m102.tipus_contracteATR, '02')
            self.assertEqual(m102.tarifaATR, '001')
            self.assertEqual(len(m102.pot_ids), 1)
            self.assertEqual(m102.data_activacio, '2016-07-06')
            self.assertEqual(m102.tipus_activacio, 'C0')
            self.assertEqual(m102.bono_social, '1')

    def test_creation_m1_02(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = step_obj.browse(cursor, uid, step_id)
            m101.write({
                'sollicitudadm': "S",
                'canvi_titular': "S",
                'tarifaATR': '003'
            })
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)

            polissa = m1.cups_polissa_id
            readings = [
                meter.data_ultima_lectura(polissa.tarifa.id)
                for meter in polissa.comptadors
            ]
            conf_obj = self.openerp.pool.get('res.config')
            conf_obj.set(cursor, uid, 'sw_m1_S_with_service_order', '1')
            self.switch(txn, 'distri')
            self.create_step(
                cursor, uid, m1, 'M1', '02', context=None
            )

            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)
            m102 = m1.get_pas()

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '02')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            today = datetime.today().strftime("%Y-%m-%d")
            self.assertEqual(m102.data_acceptacio, today)
            self.assertEqual(m102.actuacio_camp, 'S')
            self.assertEqual(m102.tarifaATR, '003')
            pots_m101 = sorted(
                [(p.name, p.potencia) for p in m101.pot_ids],
                key=lambda a: a[0]
            )
            pots_m102 = sorted(
                [(p.name, p.potencia) for p in m102.pot_ids],
                key=lambda a: a[0]
            )
            self.assertEqual(pots_m101, pots_m102)
            self.assertTrue(len(readings))
            data_prevista = max(readings)
            self.assertEqual(m102.data_activacio, data_prevista)
            self.assertFalse(m102.bono_social)

    def test_load_m1_03(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm102_new.xml')
        with open(m1_xml_path, 'r') as f:
            m102_xml = f.read()
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm103_new.xml')
        with open(m1_xml_path, 'r') as f:
            m103_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)
            codi_sollicitud = m1.codi_sollicitud

            # change the codi sol.licitud of m102.xml
            m102_xml = m102_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change the codi sol.licitud of m103.xml
            m103_xml = m103_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import m102.xml
            sw_obj.importar_xml(cursor, uid, m102_xml, 'm102_new.xml')

            # import m103.xml
            sw_obj.importar_xml(cursor, uid, m103_xml, 'm103_new.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '03')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            deadline = datetime.today() + timedelta(days=30)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(m1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            m103 = sw_obj.get_pas(cursor, uid, m1)
            self.assertEqual(m103.data_incidencia, '2016-07-21')
            self.assertEqual(m103.data_prevista_accio, '2016-07-22')
            self.assertEqual(len(m103.incidencia_ids), 3)
            incidencia = m103.incidencia_ids[1]
            self.assertEqual(incidencia.seq_incidencia, 2)
            self.assertEqual(incidencia.motiu_incidencia, '08')
            self.assertEqual(incidencia.desc_incidencia, 'Com 2')

    def test_creation_m1_03(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '03'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.03')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m103 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m103.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '03')
            self.assertEqual(m1.partner_id.ref, '5555')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')
            today = datetime.today().strftime("%Y-%m-%d")
            self.assertEqual(m103.data_incidencia, today)

    def test_creation_m1_05(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m105 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m105.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '05')
            self.assertEqual(m1.partner_id.ref, '5555')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            self.assertEqual(m105.control_potencia, '1')
            self.assertEqual(m105.tipus_contracte, '01')
            self.assertEqual(m105.tarifaATR, '001')
            self.assertEqual(m105.periodicitat_facturacio, '01')
            self.assertEqual(m105.tipus_telegestio, '01')
            self.assertEqual(m105.tipus_autoconsum, '00')
            self.assertEqual(m105.marca_medida_bt, 'N')
            self.assertFalse(m105.bono_social)

    def test_creation_m1_05_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m105 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m105.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '05')
            self.assertEqual(m1.partner_id.ref, '5555')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            self.assertEqual(m105.control_potencia, '1')
            self.assertEqual(m105.tipus_contracte, '01')
            self.assertEqual(m105.tarifaATR, '001')
            self.assertEqual(m105.periodicitat_facturacio, '01')
            self.assertEqual(m105.tipus_telegestio, '01')
            self.assertEqual(m105.tipus_autoconsum, '01')

    @mock.patch('giscedata_switching.giscedata_switching.GiscedataSwitching.escull_tarifa_comer')
    def test_creation_m1_05_activate_autoconsumo(self, mock_function):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            mock_function.return_value = self.get_pricelist_id(txn)
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '51')
            self.switch(txn, 'comer')

            partner_obj = self.openerp.pool.get('res.partner')
            partner_obj.write(cursor, uid, 2, {'ref': '4321'})
            m101_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            m101_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            m105_obj = self.openerp.pool.get('giscedata.switching.m1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = m101_obj.browse(cursor, uid, m101_id)
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)

            self.switch(txn, 'distri')
            info_id = self.create_step(cursor, uid, m1, 'M1', '05')
            info_obj = self.openerp.pool.get('giscedata.switching.step.info')
            m105_id = int(info_obj.read(cursor, uid, info_id, ['pas_id'])['pas_id'].split(',')[1])
            m105_obj.write(cursor, uid, m105_id, {'data_activacio': '2016/12/01'})

            m105 = m105_obj.browse(cursor, uid, m105_id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '05')
            self.assertEqual(m1.partner_id.ref, '4321')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            self.assertEqual(m105.control_potencia, '1')
            self.assertEqual(m105.tipus_contracte, '01')
            self.assertEqual(m105.tarifaATR, '001')
            self.assertEqual(m105.periodicitat_facturacio, '01')
            self.assertEqual(m105.tipus_telegestio, '01')
            self.assertEqual(m105.tipus_autoconsum, '51')

            ssql = '''
            update giscedata_switching set whereiam = 'comer'
            where id = %s
            '''
            cursor.execute(ssql, (m1.id,))
            m1 = m1.browse()[0]
            context = {'active_id': m1.id, 'active_ids': [m1.id], 'sync': False, 'activacio_from_atr': False}

            autoconsum_obj = self.openerp.pool.get('giscedata.autoconsum')
            ac_generador_obj = self.openerp.pool.get('giscedata.autoconsum.generador')
            wiz_obj = self.openerp.pool.get('wizard.alta.baixa.autoconsum')

            autoconsum_vals = {
                'cau': m1.cups_id.name + 'A001',
                'data_alta': '2019-10-10',
                'seccio_registre': '1',
            }
            autoconsum_id = autoconsum_obj.create(cursor, uid, autoconsum_vals)
            context = {'active_id': m1.cups_id.id}

            generador_vals = {
                'cil': m1.cups_id.name + '001',
                'tec_generador': 'b11',
                'pot_instalada_gen': 6.0,
                'tipus_installacio': '01',
                'ssaa': False,
                'autoconsum_id': autoconsum_id,
                'utm_x': '100',
                'utm_y': '100',
                'utm_fus': '31',
                'partner_id': 4,
                'data_alta': '2019-10-10',
            }
            ac_generador_obj.create(cursor, uid, generador_vals)

            wiz_id = wiz_obj.create(cursor, uid, {
                'autoconsum_id': autoconsum_id,
                'data': '2019-10-10'}, context=context)

            wiz_obj.action_donar_alta_autoconsum(cursor, uid, [wiz_id], context=context)

            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, m1, context=context)

            ac_vals = autoconsum_obj.read(cursor, uid, autoconsum_id, [])
            self.assertEqual(ac_vals['state'], 'actiu')
            self.assertEqual(ac_vals['seccio_registre'], '2')
            self.assertEqual(ac_vals['subseccio'], 'b1')
            self.assertFalse(ac_vals['collectiu'])
            self.assertEqual(len(ac_vals['modcontractuals_ids']), 2)

    def test_additional_info_m1(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(m1_xml_path, 'r') as f:
            m1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, m1_xml, 'm101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            m1 = sw_obj.browse(cursor, uid, res[0])
            inf = u'Bo Social: Si. (S)[S] ESB82420654 -> B36385870. Tarifa:2.0A, Pot.: 6.0'
            self.assertEqual(inf, m1.additional_info)

    def test_additional_info_m1_02_rej(self):
        invalid_canvi_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm101_new.xml')
        with open(invalid_canvi_xml_path, 'r') as f:
            invalid_canvi_xml = f.read()
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<FechaPrevistaAccion>2016-06-06',
                '<FechaPrevistaAccion>2016-08-18'
            )
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<TipoModificacion>S</TipoModificacion>',
                '<TipoModificacion>N</TipoModificacion>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            # Creem la segona modificació contractual fent un canvi de potencia
            self.crear_modcon(txn, 8.5, '2016-03-02', '2016-05-02')

            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_canvi_xml, 'invalid_canvi.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.rebuig, True)
            inf = u"Bo Social: Si. (N) 2.0A -> 3.0A. max -> ICP; Potencies: " \
                  u"P1: 8.5 -> P1: 15.1, P2: 20.0, P3: 30.0. . Rebuig: " \
                  u"NIF-CIF No coincide con el del Contrato en vigor,Más de " \
                  u"una modificación de potencia en un punto de suministro en" \
                  u" menos de 12 meses para el mismo titular del punto de " \
                  u"suministro"
            self.assertEqual(inf, m1.additional_info)

    def test_creation_m1_05_marca_medida_bt(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            self.set_nova_tarifa(txn, '3.1A LB')
            self.set_trafo(txn, 160)
            self.set_mesura_ab(txn, 'B')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m105 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m105.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '05')
            self.assertEqual(m1.partner_id.ref, '5555')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            self.assertEqual(m105.control_potencia, '1')
            self.assertEqual(m105.tipus_contracte, '01')
            self.assertEqual(m105.tarifaATR, '011')
            self.assertEqual(m105.periodicitat_facturacio, '01')
            self.assertEqual(m105.tipus_telegestio, '01')
            self.assertEqual(m105.tipus_autoconsum, '00')
            self.assertEqual(m105.marca_medida_bt, 'S')
            self.assertEqual(m105.perdues, 4.0)
            self.assertEqual(m1.cups_polissa_id.trafo, 160.00)

    def test_load_m1_05(self):
        m102_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm102_new.xml')
        m105_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm105_new.xml')
        with open(m102_xml_path, 'r') as f:
            m102_xml = f.read()
        with open(m105_xml_path, 'r') as f:
            m105_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            self.activar_polissa_CUPS(txn)
            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            m1 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m1.sw_id.id)
            codi_sollicitud = m1.codi_sollicitud

            # change the codi sol.licitud of m102.xml
            m102_xml = m102_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(cursor, uid, m102_xml, 'm102_new.xml')

            # change the codi sol.licitud of m105.xml
            m105_xml = m105_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, m105_xml, 'm105_new.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '05')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')
            m105 = sw_obj.get_pas(cursor, uid, m1)
            self.assertEqual(m105.data_activacio, '2016-08-21')
            self.assertEqual(m105.bono_social, '1')
            self.assertEqual(m105.tipus_autoconsum, '00')
            self.assertEqual(m105.tipus_contracte, '02')
            self.assertEqual(m105.tarifaATR, '003')
            self.assertEqual(m105.periodicitat_facturacio, '01')
            self.assertEqual(m105.control_potencia, '1')
            self.assertEqual(m105.contracte_atr, '00001')
            self.assertEqual(m105.tipus_telegestio, '01')
            self.assertEqual(m105.marca_medida_bt, 'S')
            self.assertEqual(m105.kvas_trafo, 0.05)
            self.assertEqual(m105.perdues, 5.0)
            self.assertEqual(m105.tensio_suministre, '10')
            self.assertEqual(len(m105.pot_ids), 2)
            self.assertEqual(m105.pot_ids[1].potencia, 2000)
            self.assertEqual(len(m105.pm_ids), 1)
            pm = m105.pm_ids[0]
            self.assertEqual(pm.tipus_moviment, 'M')
            self.assertEqual(pm.tipus, '03')
            self.assertEqual(pm.mode_lectura, '1')
            self.assertEqual(pm.funcio_pm, 'P')
            self.assertEqual(pm.tensio_pm, 0)
            self.assertEqual(pm.data, '2003-01-01')
            self.assertEqual(pm.data_alta, '2003-01-01')
            self.assertEqual(pm.data_baixa, '2003-02-01')
            self.assertEqual(len(pm.aparell_ids), 1)
            ap = pm.aparell_ids[0]
            self.assertEqual(ap.tipus, 'CG')
            self.assertEqual(ap.marca, '132')
            self.assertEqual(ap.tipus_em, 'L03')
            self.assertEqual(ap.propietari, 'Desc. Propietario')
            self.assertEqual(ap.periode_fabricacio, '2005')
            self.assertEqual(ap.num_serie, '0000539522')
            self.assertEqual(ap.funcio, 'M')
            self.assertEqual(ap.constant_energia, 1)
            self.assertEqual(len(ap.mesura_ids), 2)
            mes = ap.mesura_ids[0]
            self.assertEqual(mes.tipus_dh, '6')
            self.assertEqual(mes.periode, '65')
            self.assertEqual(mes.magnitud, 'PM')
            self.assertEqual(mes.origen, '30')
            self.assertEqual(mes.lectura, 6.00)
            self.assertEqual(mes.data_lectura, '2003-01-02')
            self.assertEqual(mes.anomalia, '01')
            self.assertEqual(mes.text_anomalia, 'Comentario sobre anomalia')

    def test_load_m1_06(self):
        m1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'm106_new.xml')
        with open(m1_xml_path, 'r') as f:
            m106_xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor

            # create 01
            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m101 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m101.sw_id.id)
            codi_sollicitud = m1.codi_sollicitud

            # change the codi sol.licitud of m106.xml
            m106_xml = m106_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import m106.xml
            self.switch(txn, 'distri', other_id_name='res_partner_asus')
            sw_obj.importar_xml(cursor, uid, m106_xml, 'm106.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'M1'),
                ('step_id.name', '=', '06'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            m1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '06')
            self.assertEqual(m1.partner_id.ref, '4321')
            self.assertEqual(m1.company_id.ref, '1234')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(m1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

    def test_creation_m1_06(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'M1', '06'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.m1.06')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            m106 = step_obj.browse(cursor, uid, step_id)
            m1 = sw_obj.browse(cursor, uid, m106.sw_id.id)

            self.assertEqual(m1.proces_id.name, 'M1')
            self.assertEqual(m1.step_id.name, '06')
            self.assertEqual(m1.partner_id.ref, '1234')
            self.assertEqual(m1.company_id.ref, '4321')
            self.assertEqual(m1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(m1.cups_polissa_id.name, '0001')


class TestNotificationMailsC2(TestSwitchingNotificationMail):
    _proces = 'C2'

    def test_get_notification_mail_templates_c2(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        b1_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = b1_obj.get_steps(cursor, uid, False, self._proces)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)

                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
