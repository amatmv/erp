# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
from destral.patch import PatchNewCursors


class TestB1(TestSwitchingImport):

    def test_load_b1_01(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            step_obj = self.openerp.pool.get('giscedata.switching.b1.01')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, b1_xml, 'b101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            b1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(b1.proces_id.name, 'B1')
            self.assertEqual(b1.step_id.name, '01')
            self.assertEqual(b1.partner_id.ref, '4321')
            self.assertEqual(b1.company_id.ref, '1234')
            self.assertEqual(b1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(b1.codi_sollicitud, '201412111009')
            self.assertEqual(b1.data_sollicitud, '2014-04-16')
            self.assertEqual(b1.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(b1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            b101 = sw_obj.get_pas(cursor, uid, b1)
            self.assertEqual(b101.activacio, 'L')
            self.assertEqual(b101.data_accio, '2015-05-18')
            self.assertEqual(b101.motiu, '01')
            self.assertEqual(b101.tipus_document, 'NI')
            self.assertEqual(b101.codi_document, 'B82420654')
            self.assertEqual(b101.persona, 'J')
            self.assertEqual(b101.nom, 'Camptocamp')
            self.assertFalse(b101.cognom_1)
            self.assertEqual(len(b101.telefons), 3)
            self.assertEqual(b101.telefons[1].numero, '555123124')
            self.assertEqual(b101.comentaris, 'Comentario.')
            self.assertEqual(b101.cont_nom, 'Nombre Inventado')
            self.assertEqual(len(b101.cont_telefons), 2)
            self.assertEqual(b101.cont_telefons[1].numero, '666777555')
            self.assertEqual(b101.iban, '444555666')
            self.assertEqual(b101.document_ids, [])

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

            self.assertEqual(
                step_obj.get_motiu_baixa(cursor, uid, b1.id), '01'
            )

    def test_additional_info_b1(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            sw_obj = self.openerp.pool.get('giscedata.switching')
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            sw_obj.importar_xml(
                cursor, uid, b1_xml, 'b101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            b1 = sw_obj.browse(cursor, uid, res[0])
            inf = u'(01) Cese Actividad. Activació: La activación se debe producir con próxima lectura del ciclo'
            self.assertEqual(inf, b1.additional_info)

    def test_additional_info_b1_02_reb(self):
        invalid_tall_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b101_new.xml')
        with open(invalid_tall_xml_path, 'r') as f:
            invalid_tall_xml = f.read()
            invalid_tall_xml = invalid_tall_xml.replace(
                '<Motivo>01</Motivo>',
                '<Motivo>03</Motivo>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.baixa_tall_polissa_CUPS(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_tall_xml, 'invalid_tall.xml'
            )
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            b1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(b1.rebuig, True)
            inf = u'(03) Corte de suministro. Activació: La activación se debe producir con próxima lectura del ciclo. Rebuig: Inexistencia de Contrato de ATR previo en vigor,Cliente ya cortado'
            self.assertEqual(inf, b1.additional_info)

    def test_creation_b1_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.b1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)

            self.assertEqual(b1.proces_id.name, 'B1')
            self.assertEqual(b1.step_id.name, '01')
            self.assertEqual(b1.partner_id.ref, '1234')
            self.assertEqual(b1.company_id.ref, '4321')
            self.assertEqual(b1.cups_id.name, 'ES1234000000000001JN0F')

            deadline = workday(
                datetime.strptime(b101.data_accio, "%Y-%m-%d"), -15)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(b1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            self.assertEqual(b101.codi_document, 'B82420654')
            self.assertEqual(b101.nom, 'Camptocamp')
            self.assertEqual(b101.cont_nom, 'Agrolait')
            self.assertEqual(b101.persona, 'J')

            self.assertEqual(
                step_obj.get_motiu_baixa(cursor, uid, b1.id), '02'
            )
            self.assertEqual(len(b101.telefons), 1)
            self.assertEqual(b101.cont_telefons[0].numero, '935555555')

    def test_load_b1_02(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b102_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )
            step_obj = self.openerp.pool.get(
                'giscedata.switching.b1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            codi_sollicitud = b1.codi_sollicitud

            # change the codi sol.licitud of b102.xml
            b1_xml = b1_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import b102.xml
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b102.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            b1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(b1.proces_id.name, 'B1')
            self.assertEqual(b1.step_id.name, '02')
            self.assertEqual(b1.partner_id.ref, '1234')
            self.assertEqual(b1.company_id.ref, '4321')
            self.assertEqual(b1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(b1.cups_polissa_id.name, '0001')

            str_deadline = "2016-06-09"
            s_date = datetime.strptime(b1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            adate = datetime.strftime(workday(datetime.today(), 15), '%Y-%m-%d')
            inf = u'(02) Fin de contrato de energía. Activació: {0}.'.format(adate)
            self.assertEqual(inf, b1.additional_info)

            b102 = sw_obj.get_pas(cursor, uid, b1)
            self.assertEqual(b102.data_acceptacio, '2016-12-07')
            self.assertEqual(b102.actuacio_camp, 'S')
            self.assertEqual(b102.data_ult_lect, '2016-05-31')
            self.assertEqual(b102.tipus_activacio, 'B1')
            self.assertEqual(b102.data_activacio, '2016-06-08')
            self.assertFalse(b102.rebuig)

            self.assertEqual(
                step_obj.get_motiu_baixa(cursor, uid, b1.id), '02'
            )

    def test_create_b1_02(self):
        with Transaction().start(self.database) as txn:
            sw_helper = self.openerp.pool.get("giscedata.switching.helpers")
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            self.change_polissa_comer(txn)
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '02'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.b1.02')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            b102 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b102.sw_id.id)

            self.assertEqual(b1.proces_id.name, 'B1')
            self.assertEqual(b1.step_id.name, '02')
            self.assertEqual(b1.cups_id.name, 'ES1234000000000001JN0F')

            pol_obj = self.openerp.pool.get('giscedata.polissa')
            polissa = pol_obj.browse(cursor, uid, contract_id)
            data_ultime_lect = sw_helper.get_ultima_lectura(
                cursor, uid, polissa.id
            )

            today = datetime.now().strftime('%Y-%m-%d')
            self.assertFalse(b102.rebuig)
            self.assertEqual(b102.data_acceptacio, today)
            self.assertEqual(b102.actuacio_camp, 'N')
            self.assertEqual(b102.data_ult_lect, data_ultime_lect)
            self.assertEqual(b102.tipus_activacio, 'L0')
            self.assertGreaterEqual(b102.data_activacio, today)

    def test_load_b1_03(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b103_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.b1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            codi_sollicitud = b1.codi_sollicitud
            # change the codi sol.licitud of b103.xml
            b1_xml = b1_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import step 03
            self.switch(txn, 'distri', other_id_name='res_partner_asus')
            sw_obj.importar_xml(
                cursor, uid, b1_xml, 'b103.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'B1'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            b1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(b1.proces_id.name, 'B1')
            self.assertEqual(b1.step_id.name, '03')
            self.assertEqual(b1.partner_id.ref, '4321')
            self.assertEqual(b1.company_id.ref, '1234')
            self.assertEqual(b1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(b1.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(b1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)
            self.assertEqual(
                step_obj.get_motiu_baixa(cursor, uid, b1.id), '02'
            )

    def test_b1_reject_activation(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b102_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.b1.01')
            b102_obj = self.openerp.pool.get('giscedata.switching.b1.02')

            # Chanage motiu baixa to 03
            step_obj.write(cursor, uid, [step_id], {'motiu': '03'})

            sw_obj = self.openerp.pool.get('giscedata.switching')
            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            codi_sollicitud = b1.codi_sollicitud

            # change the codi sol.licitud of b102.xml
            b1_xml = b1_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import b102.xml
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b102.xml')
            # set step 02 to reject
            step_02 = b1.get_pas()
            b102_obj.write(cursor, uid, [step_02.id], {'rebuig': True})

            # browse to update polissa info
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            self.assertTrue(b1.cups_polissa_id.active)
            # activate changes
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, b1)
            # browse to update polissa info
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            # check it is still active
            self.assertTrue(b1.cups_polissa_id.active)
            self.assertEqual(b1.cups_polissa_id.state, 'activa')

    def test_b1_corte_activation(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b102_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.b1.01')

            # Chanage motiu baixa to 03
            step_obj.write(cursor, uid, [step_id], {'motiu': '03'})

            sw_obj = self.openerp.pool.get('giscedata.switching')
            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            codi_sollicitud = b1.codi_sollicitud

            # change the codi sol.licitud of b102.xml
            b1_xml = b1_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import b102.xml
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b102.xml')

            # browse to update polissa info
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            self.assertTrue(b1.cups_polissa_id.active)
            # get current observacions and aditional info
            obs = b1.cups_polissa_id.observacions
            info = u"\nBaixa {0} ({1}):\n{2}\n".format(
                b1.proces_id.name, b1.codi_sollicitud, b1.additional_info
            )
            new_obs = u"{0}\n{1}".format(obs, info)
            # activate changes
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, b1)
            # browse to update polissa info
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            # check it is in tall
            self.assertEqual(b1.cups_polissa_id.state, 'tall')
            # check observacions
            self.assertEqual(new_obs, b1.cups_polissa_id.observacions)

    def test_b1_corte_activation(self):
        b1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'b102_new.xml')
        with open(b1_xml_path, 'r') as f:
            b1_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.activar_polissa_CUPS(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.b1.01')

            # Chanage motiu baixa to 03
            step_obj.write(cursor, uid, [step_id], {'motiu': '03'})

            sw_obj = self.openerp.pool.get('giscedata.switching')
            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            codi_sollicitud = b1.codi_sollicitud

            # change the codi sol.licitud of b102.xml
            b1_xml = b1_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import b102.xml
            sw_obj.importar_xml(cursor, uid, b1_xml, 'b102.xml')

            # browse to update polissa info
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            self.assertTrue(b1.cups_polissa_id.active)
            # get current observacions and aditional info
            obs = b1.cups_polissa_id.observacions
            info = u"\n{0} ({1}):\n{2}\n".format(
                b1.proces_id.name, b1.codi_sollicitud, b1.additional_info
            )
            new_obs = u"{0}\n{1}".format(obs, info)
            # activate changes
            with PatchNewCursors():
                sw_obj.activa_cas_atr(cursor, uid, b1)
            # browse to update polissa info
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            # check it is in tall
            self.assertEqual(b1.cups_polissa_id.state, 'tall')
            # check observacions
            self.assertEqual(new_obs, b1.cups_polissa_id.observacions)


class TestNotificationMailsB1(TestSwitchingNotificationMail):
    _proces = 'B1'

    def test_get_notification_mail_templates_b1(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        b1_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = b1_obj.get_steps(cursor, uid, False, self._proces)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)

                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
