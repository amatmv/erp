# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime


class TestC2(TestSwitchingImport):

    def test_load_c2_01(self):
        c2_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(c2_xml_path, 'r') as f:
            c2_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, c2_xml, 'c201.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '01')
            self.assertEqual(c2.partner_id.ref, '4321')
            self.assertEqual(c2.company_id.ref, '1234')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')
            self.assertEqual(c2.codi_sollicitud, '201412111009')
            self.assertEqual(c2.data_sollicitud, '2014-04-16')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c2.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            c201 = sw_obj.get_pas(cursor, uid, c2)
            self.assertEqual(c201.tarifaATR, '001')
            self.assertEqual(c201.codi_document, 'B36385870')
            self.assertEqual(c201.tipus_equip_mesura, 'L00')
            self.assertEqual(c201.canvi_titular, 'S')
            self.assertEqual(c201.comentaris, 'Comentario')
            self.assertEqual(len(c201.cont_telefons), 1)
            self.assertEqual(c201.cont_telefons[0].numero, '666777888')
            self.assertEqual(len(c201.telefons), 2)
            self.assertEqual(c201.telefons[1].numero, '666777999')
            self.assertEqual(c201.contratacion_incondicional_bs, 'N')
            self.assertEqual(c201.bono_social, '0')
            self.assertEqual(c201.solicitud_tensio, 'T')
            self.assertEqual(c201.dt_apm_codigo, '1111111111')
            self.assertEqual(c201.dt_apm_potenciainstat, 5000)
            self.assertEqual(c201.dt_apm_data_emissio, '2015-06-04')
            self.assertEqual(c201.dt_apm_data_caducitat, '2016-06-04')
            self.assertEqual(c201.dt_apm_tipus_codi_instalador, 'codigo')
            self.assertEqual(c201.dt_apm_codi_instalador, '0550')
            self.assertEqual(c201.dt_apm_tensio_suministre, '20')

            self.assertEqual(len(c201.document_ids), 1)
            self.assertEqual(c201.document_ids[0].type, '06')
            self.assertEqual(
                c201.document_ids[0].url,
                'https://www.dropbox.com/s/q40impgt3tn0vtj/Reclama_%20da%C3%B1os_%20Montero%20Simon%2C%20Eduardo%20Tarsicio.pdf?dl=0'
            )

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_creation_c2_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c201 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '01')
            self.assertEqual(c2.partner_id.ref, '1234')
            self.assertEqual(c2.company_id.ref, '4321')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            self.assertEqual(c201.control_potencia, '2')
            self.assertEqual(c201.tarifaATR, '001')
            self.assertEqual(c201.codi_document, 'B82420654')
            self.assertEqual(c201.tipus_autoconsum, '00')
            self.assertEqual(len(c201.telefons), 1)
            self.assertEqual(c201.cont_telefons[0].numero, '935555555')
            self.assertEqual(c201.contratacion_incondicional_bs, 'N')
            self.assertFalse(c201.bono_social)

    def test_creation_c2_01_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c201 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '01')
            self.assertEqual(c2.partner_id.ref, '1234')
            self.assertEqual(c2.company_id.ref, '4321')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            self.assertEqual(c201.control_potencia, '1')
            self.assertEqual(c201.tarifaATR, '001')
            self.assertEqual(c201.codi_document, 'B82420654')
            self.assertEqual(c201.tipus_autoconsum, '01')

    def test_load_c2_02(self):
        c2_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c202_new.xml')
        with open(c2_xml_path, 'r') as f:
            c2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c201 = step_obj.browse(cursor, uid, step_id)

            # Get data sol.licitud
            data_sol = c201.date_created

            # change the codi sol.licitud of c202.xml
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)
            codi_sollicitud = c2.codi_sollicitud
            c2_xml = c2_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import c202.xml
            sw_obj.importar_xml(cursor, uid, c2_xml, 'c202.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '02')
            self.assertEqual(c2.partner_id.ref, '1234')
            self.assertEqual(c2.company_id.ref, '4321')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            inf = u'(S) ESB82420654 -> B82420654. Tarifa:2.0A, Pot.: 6.0.'
            self.assertEqual(inf, c2.additional_info)

            deadline = datetime.strptime(data_sol, "%Y-%m-%d %H:%M:%S") \
                       + timedelta(days=15)
            deadline = workday(deadline, 1)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c2.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            c202 = sw_obj.get_pas(cursor, uid, c2)
            self.assertEqual(c202.data_acceptacio, '2016-06-06')
            self.assertEqual(c202.potencia_actual, 5000)
            self.assertEqual(len(c202.pot_ids), 2)
            self.assertEqual(c202.actuacio_camp, 'S')
            self.assertEqual(c202.data_ult_lect, '2016-06-01')
            self.assertEqual(c202.tarifaATR, '003')
            self.assertEqual(c202.tipus_contracteATR, '02')
            self.assertFalse(c202.rebuig)
            self.assertEqual(c202.bono_social, '0')

    def test_create_c2_02(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c201 = step_obj.browse(cursor, uid, step_id)
            c201.write({
                'tarifaATR': '003'
            })
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)

            self.switch(txn, 'distri')
            self.create_step(
                cursor, uid, c2, 'C2', '02', context=None
            )

            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)
            c202 = c2.get_pas()

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '02')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(c202.tarifaATR, '003')
            pots_c201 = sorted(
                [(p.name, p.potencia) for p in c201.pot_ids],
                key=lambda a: a[0]
            )
            pots_c202 = sorted(
                [(p.name, p.potencia) for p in c202.pot_ids],
                key=lambda a: a[0]
            )
            self.assertEqual(pots_c201, pots_c202)
            self.assertFalse(c202.rebuig)
            self.assertEqual(c202.potencia_actual, 6000)
            self.assertFalse(c202.bono_social)

    def test_load_c2_03(self):
        c2_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c202_new.xml')
        with open(c2_xml_path, 'r') as f:
            c202_xml = f.read()
        c2_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c203_new.xml')
        with open(c2_xml_path, 'r') as f:
            c203_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c201 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)
            codi_sollicitud = c2.codi_sollicitud

            # change the codi sol.licitud of c202.xml
            c202_xml = c202_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change the codi sol.licitud of c203.xml
            c203_xml = c203_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import c202.xml
            sw_obj.importar_xml(cursor, uid, c202_xml, 'c202_new.xml')

            # import c203.xml
            sw_obj.importar_xml(cursor, uid, c203_xml, 'c203_new.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '03')
            self.assertEqual(c2.partner_id.ref, '1234')
            self.assertEqual(c2.company_id.ref, '4321')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            deadline = datetime.today() + timedelta(days=30)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c2.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            c203 = sw_obj.get_pas(cursor, uid, c2)
            self.assertEqual(c203.data_incidencia, '2016-07-21')
            self.assertEqual(c203.data_prevista_accio, '2016-07-22')
            self.assertEqual(len(c203.incidencia_ids), 2)
            incidencia = c203.incidencia_ids[1]
            self.assertEqual(incidencia.seq_incidencia, 2)
            self.assertEqual(incidencia.motiu_incidencia, '08')
            self.assertEqual(incidencia.desc_incidencia, 'Com 2')

    def test_creation_c2_03(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '03'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.03')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c203 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c203.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '03')
            self.assertEqual(c2.partner_id.ref, '5555')
            self.assertEqual(c2.company_id.ref, '1234')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')
            today = datetime.today().strftime("%Y-%m-%d")
            self.assertEqual(c203.data_incidencia, today)

    def test_load_c2_05(self):
        c202_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c202_new.xml')
        c205_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c205_new.xml')
        with open(c202_xml_path, 'r') as f:
            c202_xml = f.read()
        with open(c205_xml_path, 'r') as f:
            c205_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c201 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)
            codi_sollicitud = c2.codi_sollicitud

            # change the codi sol.licitud of c202.xml
            c202_xml = c202_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(cursor, uid, c202_xml, 'c202.xml')

            # change the codi sol.licitud of c205.xml
            c205_xml = c205_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, c205_xml, 'c205.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '05')
            self.assertEqual(c2.partner_id.ref, '1234')
            self.assertEqual(c2.company_id.ref, '4321')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')
            c205 = sw_obj.get_pas(cursor, uid, c2)
            self.assertEqual(c205.data_activacio, '2016-08-21')
            self.assertEqual(c205.bono_social, '0')
            self.assertEqual(c205.tipus_autoconsum, '00')
            self.assertEqual(c205.tipus_contracte, '02')
            self.assertEqual(c205.tarifaATR, '003')
            self.assertEqual(c205.periodicitat_facturacio, '01')
            self.assertEqual(c205.control_potencia, '1')
            self.assertEqual(c205.contracte_atr, '00001')
            self.assertEqual(c205.tipus_telegestio, '01')
            self.assertEqual(c205.marca_medida_bt, 'S')
            self.assertEqual(c205.kvas_trafo, 0.05)
            self.assertEqual(c205.perdues, 5.0)
            self.assertEqual(c205.tensio_suministre, '10')
            self.assertEqual(len(c205.pot_ids), 2)
            self.assertEqual(c205.pot_ids[1].potencia, 2000)
            self.assertEqual(len(c205.pm_ids), 1)
            pm = c205.pm_ids[0]
            self.assertEqual(pm.tipus_moviment, 'A')
            self.assertEqual(pm.tipus, '03')
            self.assertEqual(pm.mode_lectura, '1')
            self.assertEqual(pm.funcio_pm, 'P')
            self.assertEqual(pm.tensio_pm, 0)
            self.assertEqual(pm.data, '2003-01-01')
            self.assertEqual(pm.data_alta, '2003-01-01')
            self.assertEqual(pm.data_baixa, '2003-02-01')
            self.assertEqual(len(pm.aparell_ids), 1)
            ap = pm.aparell_ids[0]
            self.assertEqual(ap.tipus, 'CG')
            self.assertEqual(ap.marca, '132')
            self.assertEqual(ap.tipus_em, 'L03')
            self.assertEqual(ap.propietari, 'Desc. Propietario')
            self.assertEqual(ap.periode_fabricacio, '2005')
            # self.assertEqual(ap.lectura_directa, 'N')
            self.assertEqual(ap.num_serie, '0000539522')
            self.assertEqual(ap.funcio, 'M')
            self.assertEqual(ap.constant_energia, 1)
            self.assertEqual(len(ap.mesura_ids), 2)
            mes = ap.mesura_ids[0]
            self.assertEqual(mes.tipus_dh, '6')
            self.assertEqual(mes.periode, '65')
            self.assertEqual(mes.magnitud, 'PM')
            self.assertEqual(mes.origen, '30')
            self.assertEqual(mes.lectura, 6.00)
            self.assertEqual(mes.data_lectura, '2003-01-02')
            self.assertEqual(mes.anomalia, '01')
            self.assertEqual(mes.text_anomalia, 'Comentario sobre anomalia')

    def test_creation_c2_05(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c205 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c205.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '05')
            self.assertEqual(c2.partner_id.ref, '5555')
            self.assertEqual(c2.company_id.ref, '1234')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            self.assertEqual(c205.contracte_atr, '0001')
            self.assertEqual(c205.control_potencia, '1')
            self.assertEqual(c205.tipus_contracte, '01')
            self.assertEqual(c205.tarifaATR, '001')
            self.assertEqual(c205.periodicitat_facturacio, '01')
            self.assertEqual(c205.tipus_telegestio, '01')
            self.assertEqual(c205.tipus_autoconsum, '00')
            self.assertFalse(c205.bono_social)

    def test_creation_c2_05_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c205 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c205.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '05')
            self.assertEqual(c2.partner_id.ref, '5555')
            self.assertEqual(c2.company_id.ref, '1234')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            self.assertEqual(c205.contracte_atr, '0001')
            self.assertEqual(c205.control_potencia, '1')
            self.assertEqual(c205.tipus_contracte, '01')
            self.assertEqual(c205.tarifaATR, '001')
            self.assertEqual(c205.periodicitat_facturacio, '01')
            self.assertEqual(c205.tipus_telegestio, '01')
            self.assertEqual(c205.tipus_autoconsum, '01')
            self.assertEqual(c205.marca_medida_bt, 'N')

    def test_additional_info_c2(self):
        c2_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(c2_xml_path, 'r') as f:
            c2_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, c2_xml, 'c201.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            inf = u'Bo Social: No. (S) ESB82420654 -> B36385870. Tarifa:2.0A, Pot.: 6.0'
            self.assertEqual(inf, c2.additional_info)

    def test_additional_info_c2_02_rej(self):
        invalid_canvi_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c201_new.xml')
        with open(invalid_canvi_xml_path, 'r') as f:
            invalid_canvi_xml = f.read()
            invalid_canvi_xml = invalid_canvi_xml.replace(
                '<Identificador>B36385870', '<Identificador>B82420654')

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_canvi_xml, 'invalid_canvi.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c2.rebuig, True)
            inf = u'Bo Social: No. (S) ESB82420654 -> B82420654. Tarifa:2.0A, Pot.: 6.0. Rebuig: Nuevo Cliente coincide con el Actual'
            self.assertEqual(inf, c2.additional_info)

    def test_creation_c2_05_marca_medida_bt(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            self.set_nova_tarifa(txn, '3.1A LB')
            self.set_trafo(txn, 160)
            self.set_mesura_ab(txn, 'B')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c205 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c205.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '05')
            self.assertEqual(c2.partner_id.ref, '5555')
            self.assertEqual(c2.company_id.ref, '1234')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            self.assertEqual(c205.control_potencia, '1')
            self.assertEqual(c205.tipus_contracte, '01')
            self.assertEqual(c205.tarifaATR, '011')
            self.assertEqual(c205.periodicitat_facturacio, '01')
            self.assertEqual(c205.tipus_telegestio, '01')
            self.assertEqual(c205.tipus_autoconsum, '00')
            self.assertEqual(c205.marca_medida_bt, 'S')
            self.assertEqual(c205.perdues, 4.0)
            self.assertEqual(c2.cups_polissa_id.trafo, 160.00)

    def test_create_c2_08(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '08'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.c2.08')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            c208 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c208.sw_id.id)

            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '08')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')

    def test_load_c2_08(self):
        c2_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'c208_new.xml')
        with open(c2_xml_path, 'r') as f:
            c2_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'C2', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.c2.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            c201 = step_obj.browse(cursor, uid, step_id)
            c2 = sw_obj.browse(cursor, uid, c201.sw_id.id)
            codi_sollicitud = c2.codi_sollicitud
            # change the codi sol.licitud of c208.xml
            c2_xml = c2_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import step 08
            self.switch(txn, 'distri', other_id_name='res_partner_asus')
            sw_obj.importar_xml(
                cursor, uid, c2_xml, 'c208.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'C2'),
                ('step_id.name', '=', '08'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            c2 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(c2.proces_id.name, 'C2')
            self.assertEqual(c2.step_id.name, '08')
            self.assertEqual(c2.partner_id.ref, '4321')
            self.assertEqual(c2.company_id.ref, '1234')
            self.assertEqual(c2.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(c2.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(c2.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)


class TestNotificationMailsC2(TestSwitchingNotificationMail):
    _proces = 'C2'

    def test_get_notification_mail_templates_c2(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        b1_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = b1_obj.get_steps(cursor, uid, False, self._proces)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)

                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)

