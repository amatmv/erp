# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport

from destral.transaction import Transaction
from addons import get_module_resource
from datetime import datetime
from destral.patch import PatchNewCursors


class TestB1Distri(TestSwitchingImport):

    def set_final_date_last_modcon(self, cursor, uid, polissa_id, date_str):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        mod_obj = self.openerp.pool.get('giscedata.polissa.modcontractual')
        mod_id = pol_obj.read(
            cursor, uid, polissa_id, ['modcontractual_activa']
        )['modcontractual_activa'][0]

        mod_obj.write(cursor, uid, mod_id, {'data_final': date_str})

    def test_b1_02_motius_generate_05_change_state_and_baixa_contract(self):
            b1_xml_path = get_module_resource(
                'giscedata_switching', 'tests', 'fixtures', 'b102_new.xml')
            with open(b1_xml_path, 'r') as f:
                b1_xml = f.read()

            with Transaction().start(self.database) as txn:
                uid = txn.user
                cursor = txn.cursor
                self.switch(txn, 'comer')
                self.activar_polissa_CUPS(txn)
                # create step 01
                contract_id = self.get_contract_id(txn)
                self.change_polissa_comer(txn)
                self.update_polissa_distri(txn)
                step_id = self.create_case_and_step(
                    cursor, uid, contract_id, 'B1', '01'
                )
                step_obj = self.openerp.pool.get(
                    'giscedata.switching.b1.01')
                sw_obj = self.openerp.pool.get('giscedata.switching')
                b101 = step_obj.browse(cursor, uid, step_id)
                b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
                codi_sollicitud = b1.codi_sollicitud

                # change the codi sol.licitud of b102.xml
                b1_xml = b1_xml.replace(
                    "<CodigoDeSolicitud>201412111009",
                    "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
                )

                # import b102.xml
                sw_obj.importar_xml(cursor, uid, b1_xml, 'b102.xml')

                res = sw_obj.search(cursor, uid, [
                    ('proces_id.name', '=', 'B1'),
                    ('step_id.name', '=', '02'),
                    ('codi_sollicitud', '=', codi_sollicitud)
                ])
                self.assertEqual(len(res), 1)

                b1 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(b1.proces_id.name, 'B1')
                self.assertEqual(b1.step_id.name, '02')
                self.assertEqual(b101.seq_sollicitud, '01')
                self.assertEqual(b1.cups_polissa_id.name, '0001')

                b102 = sw_obj.get_pas(cursor, uid, b1)
                self.assertEqual(b102.actuacio_camp, 'S')
                self.assertEqual(b102.tipus_activacio, 'B1')
                self.assertFalse(b102.rebuig)

                self.assertEqual(
                    step_obj.get_motiu_baixa(cursor, uid, b1.id), '02'
                )

                self.assertEqual(b101.motiu, '02')

                ssql = '''
                        update giscedata_switching set whereiam = 'distri'
                        where id = %s
                '''
                cursor.execute(ssql, (b1.id,))

                sw_obj.write(cursor, uid, b1.id, {'whereiam': 'distri'})
                b1 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(b1.whereiam, 'distri')
                pol_obj = self.openerp.pool.get('giscedata.polissa')

                pol_state = pol_obj.read(
                    cursor, uid, contract_id, ['state']
                )['state']

                self.assertEqual(pol_state, 'activa')

                imd_obj = self.openerp.pool.get('ir.model.data')
                lect_obj = self.openerp.pool.get('giscedata.lectures.lectura')

                first_lecture_id = imd_obj.get_object_reference(
                    cursor, uid, 'giscedata_lectures', 'lectura_0001'
                )[1]

                data_primera_lectura_str = lect_obj.read(
                    cursor, uid, first_lecture_id, ['name']
                )['name']

                data_primera = datetime.strptime(
                    data_primera_lectura_str, '%Y-%m-%d'
                )

                today = datetime.today()

                diff_days = (today - data_primera).days

                ctx = {'days_before': diff_days, 'lang': 'es_ES'}

                self.set_final_date_last_modcon(
                    cursor, uid, b1.cups_polissa_id.id,
                    datetime.strftime(today, '%Y-%m-%d'))

                comp_obj = self.openerp.pool.get('giscedata.lectures.comptador')

                comptadors_ids = comp_obj.search(
                    cursor, uid, [('polissa', '=', contract_id)]
                )

                self.assertTrue(comptadors_ids)
                com_states = comp_obj.read(
                    cursor, uid, comptadors_ids, ['active']
                )

                self.assertTrue(all([c['active'] for c in com_states]))
                with PatchNewCursors():
                    res = sw_obj.activa_cas_atr(cursor, uid, b1, ctx)

                self.assertEqual(res[0], 'OK')

                com_states = comp_obj.read(
                    cursor, uid, comptadors_ids, ['active']
                )

                self.assertFalse(all([c['active'] for c in com_states]))

                # contracte a baixa + comptadors baixa

                pol_reads = pol_obj.read(
                    cursor, uid, contract_id, ['state', 'data_baixa']
                )

                self.assertEqual(pol_reads['state'], 'baixa')

                b1_05_obj = self.openerp.pool.get('giscedata.switching.b1.05')

                pas_05_id = b1_05_obj.search(
                    cursor, uid, [('sw_id', '=', b1.id)]
                )[0]

                self.assertTrue(pas_05_id)

    def test_b1_01_motiu_3_tall_polissa_generate_pas_02(self):
        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')
            self.activar_polissa_CUPS(txn)
            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'B1', '01'
            )
            step_obj = self.openerp.pool.get(
                'giscedata.switching.b1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            b101 = step_obj.browse(cursor, uid, step_id)
            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)

            self.assertEqual(b1.proces_id.name, 'B1')
            self.assertEqual(b1.step_id.name, '01')
            self.assertEqual(b101.seq_sollicitud, '01')
            self.assertEqual(b1.cups_polissa_id.name, '0001')

            b101.write({'motiu': '03'})

            self.assertEqual(
                step_obj.get_motiu_baixa(cursor, uid, b1.id), '03'
            )

            ssql = '''
                    update giscedata_switching set whereiam = 'distri'
                    where id = %s
                   '''
            cursor.execute(ssql, (b1.id,))

            b1 = sw_obj.browse(cursor, uid, b101.sw_id.id)
            self.assertEqual(b1.whereiam, 'distri')
            pol_obj = self.openerp.pool.get('giscedata.polissa')

            pol_state = pol_obj.read(
                cursor, uid, contract_id, ['state']
            )['state']

            self.assertEqual(pol_state, 'activa')

            imd_obj = self.openerp.pool.get('ir.model.data')
            lect_obj = self.openerp.pool.get(
                'giscedata.lectures.lectura')

            first_lecture_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'lectura_0001'
            )[1]

            data_primera_lectura_str = lect_obj.read(
                cursor, uid, first_lecture_id, ['name']
            )['name']

            data_primera = datetime.strptime(
                data_primera_lectura_str, '%Y-%m-%d'
            )

            today = datetime.today()

            diff_days = (today - data_primera).days

            ctx = {'days_before': diff_days, 'lang': 'es_ES'}

            self.set_final_date_last_modcon(
                cursor, uid, b1.cups_polissa_id.id,
                datetime.strftime(today, '%Y-%m-%d'))
            with PatchNewCursors():
                res = sw_obj.activa_cas_atr(cursor, uid, b1, ctx)

            self.assertEqual(res[0], 'OK')

            # contracte a baixa + comptadors baixa

            pol_state = pol_obj.read(
                cursor, uid, contract_id, ['state']
            )['state']

            self.assertEqual(pol_state, 'tall')

            b1_02_obj = self.openerp.pool.get(
                'giscedata.switching.b1.02')

            pas_02_id = b1_02_obj.search(
                cursor, uid, [('sw_id', '=', b1.id)]
            )[0]

            self.assertTrue(pas_02_id)
