# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport
from .common_tests import TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from workdays import *
from datetime import datetime
from destral.patch import PatchNewCursors


class TestA3(TestSwitchingImport):

    def test_load_a3_01(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '01')
            self.assertEqual(a3.partner_id.ref, '4321')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.codi_sollicitud, '201412111009')
            self.assertEqual(a3.data_sollicitud, '2014-04-16')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            a301 = sw_obj.get_pas(cursor, uid, a3)
            self.assertEqual(a301.activacio_cicle, 'L')
            self.assertEqual(a301.data_accio, '2015-05-18')
            self.assertEqual(a301.cnae.name, '9820')
            self.assertEqual(a301.data_final, '2018-01-01')
            self.assertEqual(a301.tipus_autoconsum, '00')
            self.assertEqual(a301.tipus_contracte, '01')
            self.assertEqual(a301.solicitud_tensio, 'M')
            self.assertEqual(a301.tarifaATR, '001')
            self.assertEqual(len(a301.pot_ids), 1)
            self.assertEqual(a301.pot_ids[0].potencia, 2300)
            self.assertEqual(a301.control_potencia, '1')
            self.assertEqual(a301.cont_nom, 'Nombre Inventado')
            self.assertEqual(len(a301.cont_telefons), 2)
            self.assertEqual(a301.cont_telefons[1].numero, '666777555')
            self.assertEqual(a301.tipus_document, 'NI')
            self.assertEqual(a301.codi_document, 'B82420654')
            self.assertEqual(a301.persona, 'J')
            self.assertEqual(a301.nom, 'Camptocamp')
            self.assertEqual(len(a301.telefons), 3)
            self.assertEqual(a301.telefons[1].numero, '555123124')
            self.assertEqual(a301.ind_direccio_fiscal, 'F')
            self.assertEqual(a301.fiscal_address_id.country_id.name, u'España')
            self.assertEqual(a301.fiscal_address_id.id_municipi.ine, '17079')
            self.assertEqual(a301.fiscal_address_id.nv[:30], u'MELA MUTERMILCH')
            self.assertEqual(a301.fiscal_address_id.aclarador, u'Bloque de Pisos')
            self.assertEqual(a301.dt_cie_codigo, '1234567')
            self.assertTrue(a301.dt_cie_electronico)
            self.assertEqual(a301.dt_apm_codigo, '1111111111')
            self.assertEqual(a301.dt_apm_potenciainstat, 5000)
            self.assertEqual(a301.dt_apm_data_emissio, '2015-06-04')
            self.assertEqual(a301.dt_apm_data_caducitat, '2016-06-04')
            self.assertEqual(a301.dt_apm_tipus_codi_instalador, 'codigo')
            self.assertEqual(a301.dt_apm_codi_instalador, '0550')
            self.assertEqual(a301.dt_apm_tensio_suministre, '20')
            self.assertEqual(a301.comentaris, 'Comentario')

            self.assertEqual(len(a301.document_ids), 1)
            self.assertEqual(a301.document_ids[0].type, '06')
            self.assertEqual(
                a301.document_ids[0].url,
                'https://www.dropbox.com/s/q40impgt3tn0vtj/Reclama_%20da%C3%B1os_%20Montero%20Simon%2C%20Eduardo%20Tarsicio.pdf?dl=0'
            )

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_load_a3_01_eventual_no_meters(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures',
            'a301_new_eventual_no_meters.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '01')
            self.assertEqual(a3.partner_id.ref, '4321')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.codi_sollicitud, '201412111009')
            self.assertEqual(a3.data_sollicitud, '2014-04-16')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            a301 = sw_obj.get_pas(cursor, uid, a3)
            self.assertEqual(a301.activacio_cicle, 'L')
            self.assertEqual(a301.data_accio, '2015-05-18')
            self.assertEqual(a301.cnae.name, '9820')
            self.assertEqual(a301.data_final, '2018-01-01')
            self.assertEqual(a301.tipus_autoconsum, '00')
            self.assertEqual(a301.tipus_contracte, '09')
            self.assertEqual(a301.tarifaATR, '001')
            self.assertEqual(len(a301.pot_ids), 1)
            self.assertEqual(a301.pot_ids[0].potencia, 2300)
            self.assertEqual(a301.control_potencia, '1')
            self.assertEqual(a301.cont_nom, 'Nombre Inventado')
            self.assertEqual(a301.cont_telefons[0].numero, '666777888')
            self.assertEqual(a301.tipus_document, 'NI')
            self.assertEqual(a301.codi_document, 'B82420654')
            self.assertEqual(a301.persona, 'J')
            self.assertEqual(a301.nom, 'Camptocamp')
            self.assertEqual(a301.telefons[0].prefix, '34')
            self.assertEqual(a301.telefons[0].numero, '555123123')
            self.assertEqual(a301.ind_direccio_fiscal, 'F')
            self.assertEqual(a301.fiscal_address_id.country_id.name, u'España')
            self.assertEqual(a301.fiscal_address_id.id_municipi.ine, '17079')
            self.assertEqual(a301.fiscal_address_id.nv[:30], u'MELA MUTERMILCH')
            self.assertEqual(a301.fiscal_address_id.aclarador,
                             u'Bloque de Pisos')
            self.assertEqual(a301.dt_cie_codigo, '1234567')
            self.assertTrue(a301.dt_cie_electronico)
            self.assertEqual(a301.comentaris, 'Comentario')

            self.assertEqual(len(a301.document_ids), 1)
            self.assertEqual(a301.document_ids[0].type, '06')
            self.assertEqual(
                a301.document_ids[0].url,
                'https://www.dropbox.com/s/q40impgt3tn0vtj/Reclama_%20da%C3%B1os_%20Montero%20Simon%2C%20Eduardo%20Tarsicio.pdf?dl=0'
            )
            self.assertEqual(a301.expected_consumption, 600)

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            if len(res) > 0:
                pas_02 = sw_obj.browse(cursor, uid, res[0])
                self.assertEqual(pas_02.rebuig, False)

    def test_load_a3_01_eventual_no_meters_fails_if_no_consumption(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures',
            'a301_new_eventual_no_meters_no_consume.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 0)

            # Check that there is no reject
            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            # if len(res) > 0:
            pas_02 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(pas_02.rebuig, True)

    def test_creation_a3_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '01')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a301.control_potencia, '2')
            self.assertEqual(a301.tarifaATR, '001')
            self.assertEqual(a301.codi_document, 'B82420654')
            self.assertEqual(a301.tipus_autoconsum, '00')
            self.assertEqual(len(a301.telefons), 1)
            self.assertEqual(a301.cont_telefons[0].numero, '935555555')

    def test_creation_a3_01_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '01')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a301.control_potencia, '1')
            self.assertEqual(a301.tarifaATR, '001')
            self.assertEqual(a301.codi_document, 'B82420654')
            self.assertEqual(a301.tipus_autoconsum, '00')

    def test_creation_a3_01_eventual_contract_no_meters(self):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            pol_obj.write(cursor, uid, contract_id, {'contract_type': '09',
                                                     'expected_consumption': 600
                                                     })
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '01')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(a301.control_potencia, '2')
            self.assertEqual(a301.tarifaATR, '001')
            self.assertEqual(a301.codi_document, 'B82420654')
            self.assertEqual(a301.tipus_autoconsum, '00')
            self.assertEqual(a301.expected_consumption, 600)

    def test_load_a3_02(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a302.xml
            a3_xml = a3_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import a302.xml
            sw_obj.importar_xml(cursor, uid, a3_xml, 'a302.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '02')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            deadline = workday(datetime.strptime("2016-12-07", "%Y-%m-%d"), 6)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            a302 = sw_obj.get_pas(cursor, uid, a3)
            self.assertEqual(a302.data_acceptacio, '2016-12-07')
            self.assertEqual(a302.actuacio_camp, 'N')
            self.assertEqual(a302.tarifaATR, '001')
            self.assertEqual(a302.tipus_activacio, 'C0')

    def test_deadline_fix_activation_a3_02(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a302.xml
            a3_xml = a3_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change activation
            a3_xml = a3_xml.replace(
                """<TipoActivacionPrevista>C0</TipoActivacionPrevista>
            <FechaActivacionPrevista>2016-07-06</FechaActivacionPrevista>""",
                """<TipoActivacionPrevista>F0</TipoActivacionPrevista>
            <FechaActivacionPrevista>2014-04-22</FechaActivacionPrevista>"""
            )

            # import a302.xml
            sw_obj.importar_xml(cursor, uid, a3_xml, 'a302.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            a3 = sw_obj.browse(cursor, uid, res[0])
            str_deadline = "2014-04-23"
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

    def test_creation_a3_02(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '02'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.02')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            # Posar que s'ha enviat pert actualitzar data limit
            step_obj.write(cursor, uid, step_id, {'enviament_pendent': False})

            a302 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a302.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '02')
            self.assertEqual(a3.partner_id.ref, '5555')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 6)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            today = datetime.today().strftime("%Y-%m-%d")
            self.assertEqual(a302.data_acceptacio, today)
            self.assertEqual(a302.actuacio_camp, 'N')
            self.assertEqual(a302.tarifaATR, '001')

    def test_load_a3_03(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        with open(a3_xml_path, 'r') as f:
            a302_xml = f.read()
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a303_new.xml')
        with open(a3_xml_path, 'r') as f:
            a303_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            self.assignar_factura_potencia(txn, 'max')
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a302.xml
            a302_xml = a302_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # change the codi sol.licitud of a303.xml
            a303_xml = a303_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import a302.xml
            sw_obj.importar_xml(cursor, uid, a302_xml, 'a302.xml')

            # import a303.xml
            sw_obj.importar_xml(cursor, uid, a303_xml, 'a303.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '03'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '03')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            deadline = datetime.today() + timedelta(days=30)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            a303 = sw_obj.get_pas(cursor, uid, a3)
            self.assertEqual(a303.data_incidencia, '2016-07-21')
            self.assertEqual(a303.data_prevista_accio, '2016-07-22')
            self.assertEqual(len(a303.incidencia_ids), 3)
            incidencia = a303.incidencia_ids[1]
            self.assertEqual(incidencia.seq_incidencia, 2)
            self.assertEqual(incidencia.motiu_incidencia, '08')
            self.assertEqual(incidencia.desc_incidencia, 'Com 2')

    def test_creation_a3_03(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '03'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.03')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a303 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a303.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '03')
            self.assertEqual(a3.partner_id.ref, '5555')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            today = datetime.today().strftime("%Y-%m-%d")
            self.assertEqual(a303.data_incidencia, today)

    def test_load_a3_05(self):
        a302_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        a305_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a305_new.xml')
        with open(a302_xml_path, 'r') as f:
            a302_xml = f.read()
        with open(a305_xml_path, 'r') as f:
            a305_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer', other_id_name='res_partner_asus')

            # create step 01
            contract_id = self.get_contract_id(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            a3 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a3.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a302.xml
            a302_xml = a302_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 02
            sw_obj.importar_xml(cursor, uid, a302_xml, 'a302_new.xml')

            # change the codi sol.licitud of a305.xml
            a305_xml = a305_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )
            # import step 05
            sw_obj.importar_xml(cursor, uid, a305_xml, 'a305_new.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '05'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '05')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')
            a305 = sw_obj.get_pas(cursor, uid, a3)
            self.assertEqual(a305.data_activacio, '2016-08-21')
            self.assertEqual(a305.tipus_autoconsum, '00')
            self.assertEqual(a305.tipus_contracte, '02')
            self.assertEqual(a305.tarifaATR, '003')
            self.assertEqual(a305.periodicitat_facturacio, '01')
            self.assertEqual(a305.control_potencia, '1')
            self.assertEqual(a305.contracte_atr, '00001')
            self.assertEqual(a305.tipus_telegestio, '01')
            self.assertEqual(a305.marca_medida_bt, 'S')
            self.assertEqual(a305.kvas_trafo, 0.05)
            self.assertEqual(a305.perdues, 5.0)
            self.assertEqual(a305.tensio_suministre, '10')
            self.assertEqual(len(a305.pot_ids), 2)
            self.assertEqual(a305.pot_ids[1].potencia, 2000)
            self.assertEqual(len(a305.pm_ids), 1)
            pm = a305.pm_ids[0]
            self.assertEqual(pm.tipus_moviment, 'A')
            self.assertEqual(pm.tipus, '03')
            self.assertEqual(pm.mode_lectura, '1')
            self.assertEqual(pm.funcio_pm, 'P')
            self.assertEqual(pm.tensio_pm, 0)
            self.assertEqual(pm.data, '2003-01-01')
            self.assertEqual(pm.data_alta, '2003-01-01')
            self.assertEqual(pm.data_baixa, '2003-02-01')
            self.assertEqual(len(pm.aparell_ids), 1)
            ap = pm.aparell_ids[0]
            self.assertEqual(ap.tipus, 'CG')
            self.assertEqual(ap.marca, '132')
            self.assertEqual(ap.tipus_em, 'L03')
            self.assertEqual(ap.propietari, 'Desc. Propietario')
            self.assertEqual(ap.periode_fabricacio, '2005')
            self.assertEqual(ap.num_serie, '0000539522')
            self.assertEqual(ap.funcio, 'M')
            self.assertEqual(ap.constant_energia, 1)
            self.assertEqual(len(ap.mesura_ids), 2)
            mes = ap.mesura_ids[0]
            self.assertEqual(mes.tipus_dh, '6')
            self.assertEqual(mes.periode, '65')
            self.assertEqual(mes.magnitud, 'PM')
            self.assertEqual(mes.origen, '30')
            self.assertEqual(mes.lectura, 6.00)
            self.assertEqual(mes.data_lectura, '2003-01-02')
            self.assertEqual(mes.anomalia, '01')
            self.assertEqual(mes.text_anomalia, 'Comentario sobre anomalia')

    def test_creation_a3_05_autoconsumo(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.assignar_factura_potencia(txn, 'icp')
            # Autoconsumo tipo 1
            self.set_autoconsumo(txn, '01')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a305 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a305.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '05')
            self.assertEqual(a3.partner_id.ref, '4321')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            self.assertEqual(a305.contracte_atr, '0001')
            self.assertEqual(a305.control_potencia, '1')
            self.assertEqual(a305.tipus_contracte, '01')
            self.assertEqual(a305.tarifaATR, '001')
            self.assertEqual(a305.periodicitat_facturacio, '01')
            self.assertEqual(a305.tipus_autoconsum, '01')
            self.assertEqual(a305.marca_medida_bt, 'N')

    def test_additional_info_a3(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, a3_xml, 'a301.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)
            a3 = sw_obj.browse(cursor, uid, res[0])
            inf = u'Tarifa 2.0A: P1: 2300. Facturacio ICP'
            self.assertEqual(inf, a3.additional_info)

    def test_additional_info_a3_02_reb(self):
        invalid_pot_sol_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a301_new.xml')
        with open(invalid_pot_sol_xml_path, 'r') as f:
            invalid_pot_sol_xml = f.read()
            invalid_pot_sol_xml = invalid_pot_sol_xml.replace(
                '<Potencia Periodo="1">2300</Potencia>',
                '<Potencia Periodo="1">2300</Potencia>'
                '<Potencia Periodo="2">3450</Potencia>'
                '<Potencia Periodo="3">2300</Potencia>'
            )

        sw_obj = self.openerp.pool.get('giscedata.switching')
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, invalid_pot_sol_xml, 'invalid_pot_sol.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.rebuig, True)
            inf = u'Tarifa 2.0A: P1: 2300, P2: 3450, P3: 2300. Facturacio ICP. Rebuig: Secuencia de Potencias incorrectas, Pi>Pi+1,Tarifa no válida para Potencias Solicitadas'
            self.assertEqual(inf, a3.additional_info)

    def test_additional_info_a3_02_accept(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a302_new.xml')
        with open(a3_xml_path, 'r') as f:
            a3_xml = f.read()

        with Transaction().start(self.database) as txn:
            uid = txn.user
            cursor = txn.cursor
            self.switch(txn, 'comer')

            # create step 01
            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )
            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')
            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a302.xml
            a3_xml = a3_xml.replace(
                "<CodigoDeSolicitud>201412111009",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import a302.xml
            sw_obj.importar_xml(cursor, uid, a3_xml, 'a302.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '02'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)
            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.rebuig, False)
            inf = u'Tarifa 2.0A: P1: 6000. Facturacio Maxímetro.'
            self.assertEqual(inf, a3.additional_info)

    def test_creation_a3_05_marca_medida_bt(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            pol_obj = self.openerp.pool.get('giscedata.polissa')
            pol_obj.write(cursor, uid, [contract_id], {'contract_type': '02'})
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            self.set_nova_tarifa(txn, '3.1A LB')
            self.set_trafo(txn, 160)
            self.set_mesura_ab(txn, 'B')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '05'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.05')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a305 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a305.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '05')
            self.assertEqual(a3.partner_id.ref, '5555')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            self.assertEqual(a305.control_potencia, '1')
            self.assertEqual(a305.tipus_contracte, '02')
            self.assertEqual(a305.tarifaATR, '011')
            self.assertEqual(a305.periodicitat_facturacio, '01')
            self.assertEqual(a305.tipus_telegestio, '01')
            self.assertEqual(a305.tipus_autoconsum, '00')
            self.assertEqual(a305.marca_medida_bt, 'S')
            self.assertEqual(a305.perdues, 4.0)
            self.assertEqual(a3.cups_polissa_id.trafo, 160.00)

    def test_load_a3_06(self):
        a3_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'a306_new.xml')
        with open(a3_xml_path, 'r') as f:
            a306_xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor

            # create 01
            contract_id = self.get_contract_id(txn)
            self.activar_polissa_CUPS(txn)
            self.change_polissa_comer(txn)
            self.assignar_factura_potencia(txn, 'icp')

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a301 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a301.sw_id.id)
            codi_sollicitud = a3.codi_sollicitud

            # change the codi sol.licitud of a306.xml
            a306_xml = a306_xml.replace(
                "<CodigoDeSolicitud>201607211259",
                "<CodigoDeSolicitud>{0}".format(codi_sollicitud)
            )

            # import a306.xml
            self.switch(txn, 'distri', other_id_name='res_partner_asus')
            sw_obj.importar_xml(cursor, uid, a306_xml, 'a306.xml')

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'A3'),
                ('step_id.name', '=', '06'),
                ('codi_sollicitud', '=', codi_sollicitud)
            ])
            self.assertEqual(len(res), 1)

            a3 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '06')
            self.assertEqual(a3.partner_id.ref, '4321')
            self.assertEqual(a3.company_id.ref, '1234')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(a3.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

    def test_creation_a3_06(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)

            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'A3', '06'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.a3.06')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            a306 = step_obj.browse(cursor, uid, step_id)
            a3 = sw_obj.browse(cursor, uid, a306.sw_id.id)

            self.assertEqual(a3.proces_id.name, 'A3')
            self.assertEqual(a3.step_id.name, '06')
            self.assertEqual(a3.partner_id.ref, '1234')
            self.assertEqual(a3.company_id.ref, '4321')
            self.assertEqual(a3.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(a3.cups_polissa_id.name, '0001')


class TestNotificationMailsA3(TestSwitchingNotificationMail):
    _proces = 'A3'

    def test_get_notification_mail_templates_a3(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        a3_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = a3_obj.get_steps(cursor, uid, False, self._proces)
            step_names = a3_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = a3_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)

