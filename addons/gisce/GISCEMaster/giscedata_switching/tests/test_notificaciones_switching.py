# -*- coding: utf-8 -*-
from __future__ import absolute_import
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from giscedata_switching.tests.common_tests import TestSwitchingImport

from osv import osv
from giscedata_cups.giscedata_cups import GiscedataCupsPs as base_cups
from destral.patch import PatchNewCursors


power_invoice_modes = {'1': 'icp', '2': 'max'}


class FakeCups(osv.osv):
    '''
    Creamos esta clase ya que alguna de las dpendencias del modulo instala
    giscedata_cups_comer lo qual implica que el methodo whereiam esta reescrito
    para que siempre devuelva comer, pero nos interesa que use el metodo base y
    no este para poder generar el paso como distribuidora.
    '''
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    def whereiam(self, cursor, uid, context=None):
        return base_cups.whereiam(self, cursor, uid, context=context)

FakeCups()


class TestsNotifySwitching(TestSwitchingImport):

    def setUp(self):
        """
        Definición de variables de entorno.
        """
        with Transaction().start(self.database) as txn:
            # Arrange
            cursor = txn.cursor
            uid = txn.user
            ir_view_obj = self.openerp.pool.get('ir.ui.view')
            views_before_install_comer = ir_view_obj.search(cursor, uid, [])
            imd_obj = self.openerp.pool.get('ir.model.data')
            imd_before_install_comer = imd_obj.search(cursor, uid, [('model', '=', 'ir.ui.view')])

        self.openerp.install_module("giscedata_switching_comer")
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.views_before_install_comer = views_before_install_comer
        self.imd_before_install_comer = imd_before_install_comer

    def tearDown(self):
        cursor = self.txn.cursor
        uid = self.txn.user

        ir_view_obj = self.openerp.pool.get('ir.ui.view')
        new_view_ids = ir_view_obj.search(cursor, uid, [])

        created_views_ids = list(set(new_view_ids) - set(self.views_before_install_comer))

        imd_obj = self.openerp.pool.get('ir.model.data')
        new_imd_ids = imd_obj.search(cursor, uid, [('model', '=', 'ir.ui.view')])
        created_imds = list(set(new_imd_ids) - set(self.imd_before_install_comer))
        self.txn.stop()
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            if new_view_ids:
                ir_view_obj.unlink(cursor, uid, created_views_ids)

            if created_imds:
                imd_obj.unlink(cursor, uid, created_imds)
            cursor.commit()

    def test_cn_06_notify_and_deactivate_notify_pending(self):
        '''
        Case: Cn 06 activation (baja contrato)
        Expected:
            Que siempre notifique quando entre en esta activacion,
            desactive el check de notificacion pendiente
            si no se ha notificado.

            Si la baja no se puede activar, que el caso quede abierto i la
            siguiente vez ya no se notificara


        '''
        sw_ayudame_obj = self.openerp.pool.get('giscedata.switching.helpers')
        lect_pol_obj = self.openerp.pool.get("giscedata.lectures.lectura.pool")

        fact_obj = self.openerp.pool.get('giscedata.facturacio.factura')
        imd_obj = self.openerp.pool.get('ir.model.data')

        pol_obj = self.openerp.pool.get('giscedata.polissa')
        power_template_obj = self.openerp.pool.get('poweremail.templates')
        conf_obj = self.openerp.pool.get('res.config')

        txn = self.txn
        uid = self.txn.user
        cursor = self.txn.cursor

        conf_obj.set(
            cursor, uid,
            'sw_mail_user_notification_on_activation', 'all'
        )

        # Configurem la template poweremail ################################
        c1_06_template_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', 'notification_atr_C1_06'
        )[1]

        pw_account_obj = self.openerp.pool.get('poweremail.core_accounts')
        pw_id = pw_account_obj.create(cursor, uid, {
            'name': 'test',
            'user': 1,
            'email_id': 'test@email',
            'smtpserver': 'smtp.gmail.com',
            'smtpport': '587',
            'company': 'no',
            'state': 'approved',
        })

        power_template_obj.write(cursor, uid, c1_06_template_id, {'enforce_from_account': pw_id})
        # END Conf la template poweremail ##################################

        self.switch(txn, 'distri')

        contract_id = self.get_contract_id(txn)

        self.activar_polissa_CUPS(txn)

        fact_0001 = imd_obj.get_object_reference(cursor, uid, 'giscedata_facturacio', 'factura_0001')[1]

        pol = pol_obj.browse(cursor, uid, contract_id)

        self.assertEqual(pol.state, 'activa')

        step_id = self.create_case_and_step(
            cursor, uid, contract_id, 'C1', '06'
        )
        step_obj = self.openerp.pool.get('giscedata.switching.c1.06')
        c106 = step_obj.browse(cursor, uid, step_id)

        self.assertTrue(c106.notificacio_pendent)
        self.assertTrue(c106.header_id.sw_id.finalitzat)
        c106.sw_id.case_id.write({'state': 'open'})
        self.assertNotEqual(c106.sw_id.case_id.state, 'done')
        self.assertEqual(c106.sw_id.case_id.state, 'open')

        pol.modcontractual_activa.write({'data_inici': c106.data_activacio})

        # Preparem la factura amb data final ifual a la d'activacio del pas
        fact_obj.write(cursor, uid, fact_0001, {'data_final': c106.data_activacio})

        fact_obj.invoice_open(cursor, uid, [fact_0001])

        lect_pool_ids = lect_pol_obj.search(cursor, uid, [])

        lect_pol_obj.write(cursor, uid, lect_pool_ids, {'lectura': 0})

        with PatchNewCursors():
            ssql = '''
                update giscedata_switching set whereiam = 'comer'
                where id = %s
            '''
            cursor.execute(ssql, (c106.sw_id.id,))
            pp = self.openerp.pool.get("giscedata.switching").activa_cas_atr(cursor, uid, c106.sw_id)

        pol = pol_obj.browse(cursor, uid, contract_id)
        c106 = step_obj.browse(cursor, uid, step_id)

        self.assertEqual(pol.state, 'baixa')

        self.assertFalse(c106.notificacio_pendent)

        self.assertEqual(c106.sw_id.case_id.state, 'done')
