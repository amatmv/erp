# -*- coding: utf-8 -*-
from __future__ import absolute_import

from .common_tests import TestSwitchingImport, TestSwitchingNotificationMail

from destral.transaction import Transaction
from addons import get_module_resource
from datetime import date, datetime
from destral import testing
from expects import expect, raise_error
from osv.orm import except_orm
from gestionatr.input.messages import W1
from workdays import *


class TestW1(TestSwitchingImport):

    def test_load_w1_01(self):
        w1_xml_path = get_module_resource(
            'giscedata_switching', 'tests', 'fixtures', 'w101_new.xml')
        with open(w1_xml_path, 'r') as f:
            w101_xml = f.read()

        with Transaction().start(self.database) as txn:
            self.switch(txn, 'distri')
            self.activar_polissa_CUPS(txn)
            self.assignar_factura_potencia(txn, "icp")
            sw_obj = self.openerp.pool.get('giscedata.switching')

            uid = txn.user
            cursor = txn.cursor
            sw_obj.importar_xml(
                cursor, uid, w101_xml, 'w101.xml'
            )

            res = sw_obj.search(cursor, uid, [
                ('proces_id.name', '=', 'W1'),
                ('step_id.name', '=', '01'),
                ('codi_sollicitud', '=', '201412111009')
            ])
            w1 = sw_obj.browse(cursor, uid, res[0])
            self.assertEqual(w1.proces_id.name, 'W1')
            self.assertEqual(w1.step_id.name, '01')
            self.assertEqual(w1.partner_id.ref, '4321')
            self.assertEqual(w1.company_id.ref, '1234')
            self.assertEqual(w1.cups_id.name, 'ES1234000000000001JN0F')
            self.assertEqual(w1.cups_polissa_id.name, '0001')
            self.assertEqual(w1.data_sollicitud, '2014-04-16')

            deadline = workday(datetime.today(), 5)
            str_deadline = deadline.strftime("%Y-%m-%d")
            s_date = datetime.strptime(w1.date_deadline, "%Y-%m-%d %H:%M:%S")
            sw_deadline = s_date.strftime("%Y-%m-%d")
            self.assertEqual(sw_deadline, str_deadline)

            w101 = sw_obj.get_pas(cursor, uid, w1)
            self.assertEqual(w101.codi_dh, '2')
            self.assertEqual(w101.data_lectura, "2017-02-18")
            self.assertEqual(len(w101.lect_ids), 1)
            lect1 = w101.lect_ids[0]
            self.assertEqual(lect1.magnitud, 'AE')
            self.assertEqual(lect1.name, 'P1')
            self.assertEqual(lect1.lectura, 0000001162.00)

    def test_creation_w1_01(self):
        with Transaction().start(self.database) as txn:
            self.switch(txn, 'comer')
            uid = txn.user
            cursor = txn.cursor

            contract_id = self.get_contract_id(txn)
            self.change_polissa_comer(txn)
            self.update_polissa_distri(txn)
            step_id = self.create_case_and_step(
                cursor, uid, contract_id, 'W1', '01'
            )

            step_obj = self.openerp.pool.get('giscedata.switching.w1.01')
            sw_obj = self.openerp.pool.get('giscedata.switching')

            w101 = step_obj.browse(cursor, uid, step_id)
            w1 = sw_obj.browse(cursor, uid, w101.sw_id.id)

            self.assertEqual(w1.proces_id.name, 'W1')
            self.assertEqual(w1.step_id.name, '01')
            self.assertEqual(w1.partner_id.ref, '1234')
            self.assertEqual(w1.company_id.ref, '4321')
            self.assertEqual(w1.cups_id.name, 'ES1234000000000001JN0F')

            self.assertEqual(w101.codi_dh, '1')
            today = datetime.today().strftime("%Y-%m-%d")
            self.assertEqual(w101.data_lectura, today)


class TestNotificationMailsW1(TestSwitchingNotificationMail):
    _proces = 'W1'

    def test_get_notification_mail_templates_w1(self):
        pool = self.openerp.pool
        sw_obj = pool.get('giscedata.switching')
        b1_obj = pool.get('giscedata.switching.proces')
        # Check comer templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'comer', other_id_name='res_partner_asus')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            all_steps = b1_obj.get_steps(cursor, uid, False, self._proces)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'comer'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)
                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)
        # Check distri templates
        with Transaction().start(self.database) as txn:
            self.txn = txn
            self.switch(txn, 'distri')
            uid = txn.user
            cursor = txn.cursor
            contract_id = self.get_contract_id(txn)
            step_names = b1_obj.get_emisor_steps(
                cursor, uid, self._proces, 'distri'
            )
            steps = [
                (name, mod) for name, mod in all_steps if name in step_names
            ]
            for step_name, step_module in steps:
                model_obj = pool.get(step_module)

                step_id = self.check_template_for_process_and_step(
                    cursor, uid, contract_id, self._proces, step_name
                )
                if self.check_rebuig_step(self._proces, step_name):
                    sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                    sw_id = sw_id['sw_id'][0]
                    sw_obj.unlink(cursor, uid, sw_id)
                    step_id = self.check_template_for_process_and_step(
                        cursor, uid, contract_id, self._proces, step_name,
                        rebuig=True
                    )
                sw_id = model_obj.read(cursor, uid, step_id, ['sw_id'])
                sw_id = sw_id['sw_id'][0]
                sw_obj.unlink(cursor, uid, sw_id)