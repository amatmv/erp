
# -*- coding: utf-8 -*-
from tools.translate import _
from osv import osv, fields
from ast import literal_eval as eval
from osv.expression import OOQuery
from json import dumps, loads


class GiscedataSwitchingNotifyWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.atr.notification.mail.wizard'

    def _get_default_headers(self, cursor, uid, context=None):
        if not context:
            context = {}
        case_ids = context.get('active_ids', [])
        if not case_ids:
            return []

        filtred_steps_dict = self.filter_steps_to_notify(
            cursor, uid, case_ids, context=context
        )

        return dumps(filtred_steps_dict)

    def filter_steps_to_notify(self, cursor, uid, case_ids, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        step_info_obj = self.pool.get('giscedata.switching.step.info')

        filtred_steps_dict = {}
        casos_desactivats = 0

        sw_cases_steps = sw_obj.read(
            cursor, uid, case_ids, ['step_ids'], context=context
        )

        for sw_steps in sw_cases_steps:
            sw_id = sw_steps['id']
            filtred_steps_dict[sw_id] = []
            step_ids = sw_steps['step_ids']

            q = OOQuery(step_info_obj, cursor, uid)
            sql = q.select([
                'pas_id', 'proces_id.name', 'step_id.name',
            ]).where([
                ('id', 'in', step_ids)
            ])
            cursor.execute(*sql)
            steps_in_sw_not_filtred = sorted(cursor.dictfetchall(), key=lambda k: k['step_id.name'])

            has_step_02_trigger = False  # Check if step 2 is added to avoid redundant searches
            for indx, header in enumerate(steps_in_sw_not_filtred):
                # {'proces_id.name': u'A3', 'pas_id': u'giscedata.switching.a3.02,49', 'step_id.name': u'02'}
                model, pas_id = header['pas_id'].split(',')
                pas_id = eval(pas_id)

                step_number = header['step_id.name']  # 02....

                pas_obj = self.pool.get(model)

                q = OOQuery(pas_obj, cursor, uid)
                sql = q.select([
                    'header_id.notificacio_pendent',
                ]).where([
                    ('id', '=', pas_id)
                ])
                cursor.execute(*sql)

                step_per_notificar = cursor.fetchone()[0]

                if step_per_notificar:
                    if step_number in ('04', '05') and has_step_02_trigger:
                        for sindx, s in enumerate(filtred_steps_dict[sw_id]):
                            if s['step_id.name'] == '02':
                                filtred_steps_dict[sw_id].pop(sindx)
                                # todo comprobar si es desaciva sol sino fer-ho aqui
                                casos_desactivats += 1
                        has_step_02_trigger = False
                    else:
                        if step_number == '02':
                            has_step_02_trigger = True
                    header['pas_id.id'] = pas_id
                    header['pas_id.model'] = model
                    filtred_steps_dict[sw_id].append(header)
        filtred_steps_dict['casos_desactivats'] = casos_desactivats
        return filtred_steps_dict

    def _default_initial_message(self, cursor, uid, context=None):

        if not context:
            context = {}

        case_ids = context.get('active_ids', [])

        if case_ids:
            head_obj = self.pool.get('giscedata.switching.step.header')
            order = "create_date asc"

            header_ids = head_obj.search(
                cursor, uid, [
                    ('sw_id', 'in', case_ids),
                    ('notificacio_pendent', '=', True)
                ], order=order
            )

            output_msg = _("En {} casos:\n "
                           "\nHi ha {} passos pendents de notificar\n").format(
                            len(case_ids),
                            len(header_ids)
            )
        else:
            output_msg = _("No s'han sel·leccionat casos")

        return output_msg

    def send_notification_mail(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        cases = context.get('active_ids', [])
        steps_json_dic = self.read(
            cursor, uid, ids, ['header_ids'], context=context
        )[0]['header_ids']
        steps_json_dic = loads(steps_json_dic)

        if not cases or not steps_json_dic:
            return {}

        sw_obj = self.pool.get('giscedata.switching')
        conf_obj = self.pool.get('res.config')
        pw_account_obj = self.pool.get('poweremail.core_accounts')
        req_obj = self.pool.get('res.request')
        address_mail_noti_id = False
        address_mail_noti = conf_obj.get(
            cursor, uid, 'sw_email_address_user_notification'
        ) or False
        if address_mail_noti:
            address_mail_noti_id = pw_account_obj.search(cursor, uid, [
                ('email_id', 'ilike', address_mail_noti)
            ])
            if address_mail_noti_id:
                context.update({'use_mail_account': address_mail_noti_id[0]})

        pasos_desactivats = steps_json_dic.pop('casos_desactivats')
        errors = 0
        msg_error = ''
        casos_totals = pasos_desactivats
        for str_sw_id in steps_json_dic.keys():
            sw_id = int(str_sw_id)
            for pas in steps_json_dic[str_sw_id]:
                casos_totals += 1
                model_name = pas['pas_id.model']
                pas_id = pas['pas_id.id']

                sent, msg = sw_obj.notifica_pas_a_client(
                    cursor, uid, sw_id, pas_id, model_name,
                    context=context.copy()
                )

                if not sent:
                    msg_error = '{}{}\n'.format(msg_error, msg)
                    errors += 1

        msg = '' if address_mail_noti_id else _(
            'WARNING: "sw_email_address_user_notification" has not been set.\n')
        msg += _("Successfully updated {} steps".format(
            (casos_totals - errors)
        ))
        if errors:
            msg += "\n\n{}".format(
                _("Found the following errors:\n\n{}").format(
                    msg_error
                )
            )

        req_obj.create(
            cursor, uid, {
                'name': _('Assistent de Notificacions ATR'),
                'body': msg,
                'act_from': uid,
                'act_to': uid
            }
        )
        self.write(cursor, uid, ids, {
            'output': msg,
            'state': 'end'
        })

    _columns = {
        'state': fields.selection(
            [('init', 'Init'), ('end', 'Done')],
            string='Progress State', translate=False
        ),
        'output': fields.text(string='Output Text', readonly=True),
        'header_ids': fields.json('Header ids'),
    }

    _defaults = {
        'state': 'init',
        'header_ids': _get_default_headers,
        'output': _default_initial_message
    }

GiscedataSwitchingNotifyWizard()
