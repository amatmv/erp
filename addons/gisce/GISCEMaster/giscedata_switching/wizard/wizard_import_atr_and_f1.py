# -*- coding: utf-8 -*-

try:
    import cStringIO as StringIO
except:
    import StringIO
import zipfile
import base64
from re import search
from os.path import basename
import os
import pooler
import traceback
from datetime import datetime
import shutil
import glob

from osv import osv, fields, orm
from tools.translate import _


class WizardImportAtrAndF1(osv.osv_memory):

    _name = 'wizard.import.atr.and.f1'

    def _get_default_info(self, cursor, uid, context=None):
        _info = _(u"""
                Assistent per carregar fitxers XML F1 i casos ATR.
        """)
        return _info

    def _get_working_directory_name(self, *args):
        """
        :return: Returns a random name as the working directory of the wizard
        """
        return '/tmp/wizard_import_f1_and_atr_{}'.format(
            datetime.strftime(datetime.today(), '%Y%m%d%H%M%S'),
        )

    def _create_working_directory(self, cursor, uid, ids, context=None):
        """
        Creates the working directory and returns it's name
        """
        temporal_folder_name = self.read(
            cursor, uid, ids, ['working_directory']
        )[0]['working_directory']
        os.makedirs(temporal_folder_name)
        return temporal_folder_name

    def _unlink_working_directory(self, cursor, uid, ids, context=None):
        working_directory = self.read(
            cursor, uid, ids, ['working_directory']
        )[0]['working_directory']
        shutil.rmtree(working_directory, ignore_errors=True)

    @staticmethod
    def _get_b64_zip_files(zip_path, context=None):
        zip_filename = glob.glob(
            '{path}/{type}_*.zip.b64'.format(path=zip_path, type=context['type'])
        )[0]
        with open(zip_filename, 'r') as zip_handler:
            return zip_filename, zip_handler.read()

    def _ensure_xml_file(self, file_handler, filename):
        """
        Ensure the file_handler contains a valid XML file.
        If it isn't, will raise a except_osv exception.
        """
        assert file_handler.getvalue()[:5] == '<?xml', osv.except_osv(
            _('Error'),
            _('El fitxer {} no té format XML ni ZIP').format(filename)
        )

    def _move_xmls_to_tmp(self, cursor, uid, ids, input_file, input_filename):
        """
        We create a temporal directory to /tmp to move all input XMLs to it.
        Whether the input file is a ZIP or an XML, we want to move it to the directory.

        :param input_file: ZIP containing XMLs or an XML itself
        :param input_filename: name of the file
        :return:
        """

        working_path = self._create_working_directory(cursor, uid, ids)

        data = base64.b64decode(input_file)
        file_handler = StringIO.StringIO(data)
        try:
            input_file = zipfile.ZipFile(file_handler)
            for xml_filename in input_file.namelist():
                with open('{}/{}'.format(working_path, xml_filename), "w+") as outfile:
                    outfile.write(input_file.read(xml_filename))
        except zipfile.BadZipfile:
            self._ensure_xml_file(file_handler, input_filename)
            with open('{}/{}'.format(working_path, input_filename), 'w+') as xml_file:
                xml_file.write(file_handler.getvalue())

        return working_path

    def action_import_xmls(self, cursor, uid, ids, context=None):
        """
        This method takes the input file and creates two zips in the /tmp folder.

        The first contains F1 XML files and the other ATR cases.

        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param context: OpenERP context

        """

        def _b64_encode_zipfile(_filename):
            """
            Encodes a file, given it's absolute path, to b64 and removes the
            original one
            """
            b64file = '{}.b64'.format(_filename)
            with open(_filename, 'rb') as zip_file, open(b64file, 'w') as b64:
                base64.encode(zip_file, b64)
            os.remove(_filename)
            return b64file

        def _create_tmp_zip(directory, prefix):
            """
            Creates a temporal zip file and returns it's **opened** handler.
            :param prefix: prefix of the temporal zip name
            """
            tmp_zip = open('{temporal_folder}/{pref}{date}.zip'.format(
                temporal_folder=directory, pref=prefix,
                date=datetime.strftime(datetime.today(), '%Y%m%d_%H%M%S')),
                'w+'
            )
            zip_handler = zipfile.ZipFile(tmp_zip, 'w')
            return zip_handler

        if context is None:
            context = {}
            
        wizard = self.read(
            cursor, uid, ids[0], ['filename', 'file'], context=context
        )[0]
        if not wizard['file']:
            err_msg = _(u'Sel·leccioneu un fitxer')
            raise orm.except_orm(u'Atenció', err_msg)

        # noinspection PyBroadException
        try:
            # Opening a temporal cursor in case something goes wrong
            db = pooler.get_db(cursor.dbname)
            tmp_cursor = db.cursor()

            # Moving XMLs to temporal folder
            working_dir_path = self._move_xmls_to_tmp(
                cursor, uid, ids, wizard['file'], wizard['filename']
            )

            # We will loop inside the temporal directory to send each XML to
            # their corresponding zip
            exist_f1_xml = exist_atr_xml = False
            f1_zip = atr_zip = None
            for xml_file in os.listdir(working_dir_path):
                filename = '{}/{}'.format(working_dir_path, xml_file)
                with open(filename, 'r') as xml_handler:
                    xml_data = xml_handler.read()
                    if self.is_f1(xml_data):
                        if f1_zip is None:
                            # If it doesn't exist, we create a zip containing
                            # the F1s. This will be sent to the F1s importing
                            # wizard
                            f1_zip = _create_tmp_zip(
                                directory=working_dir_path, prefix='F1_'
                            )
                        f1_zip.write(filename, basename(filename))
                        exist_f1_xml = True
                    else:
                        if atr_zip is None:
                            atr_zip = _create_tmp_zip(
                                directory=working_dir_path, prefix='ATR_'
                            )
                        atr_zip.write(filename, basename(filename))
                        exist_atr_xml = True
            if atr_zip is not None:
                atr_zip.close()
            if f1_zip is not None:
                f1_zip.close()

            if exist_f1_xml and exist_atr_xml:
                msg = _(u"""
                    S'han trobat fitxers F1.
                    Vols importar-los juntament amb els casos ATR?
                """)
                # Next, let's convert the generated zips to base64 so
                # that the wizards can read it normally.
                _b64_encode_zipfile(f1_zip.filename)

                _b64_encode_zipfile(atr_zip.filename)

                self.write(cursor, uid, ids, {
                    'state': 'load',
                    'info': msg
                })

            if not exist_f1_xml and exist_atr_xml:
                _b64_encode_zipfile(atr_zip.filename)
                msg = self.send_atr_xmls(tmp_cursor, uid, ids, context=context)
                # Finally the working directory can be removed
                self._unlink_working_directory(cursor, uid, ids, context)
                info = _(u"""
                    S'han importat correctament els fitxers ATR i s'han encuat els F1.\n
                    Resultat XML ATR:\n\n{}
                """).format(msg['info'])
                self.write(cursor, uid, ids, {
                    'state': 'done',
                    'info': info,
                    'casos': msg['cas']
                })
            elif exist_f1_xml and not exist_atr_xml:
                _b64_encode_zipfile(f1_zip.filename)
                ok = self.send_f1_xmls(tmp_cursor, uid, ids, context=context)
                # Finally the working directory can be removed
                self._unlink_working_directory(cursor, uid, ids, context)
                if ok:
                    info = _(u"""
                        S'ha encuat l'importació dels fitxers F1.\n
                    """)
                    self.write(cursor, uid, ids, {
                        'state': 'done',
                        'info': info,
                    })
                else:
                    self.write(cursor, uid, ids, {
                        'state': 'done',
                        'info': _(u"""
                            No s'ha importat cap fitxer.
                        """)
                    })
            elif not exist_f1_xml and not exist_atr_xml:
                self.write(cursor, uid, ids, {
                    'state': 'done',
                    'info': _(u"""
                        No s'ha importat cap fitxer.
                    """)
                })

            tmp_cursor.commit()
        except Exception:
            traceback.print_exc()
            tmp_cursor.rollback()
        finally:
            tmp_cursor.close()

        return True

    def action_send_xmls(self, cursor, uid, ids, context=None):
        """
        The zips created will be sent to the F1 and ATR respectively to their
        wizards to process it.
        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param context: OpenERP context
        """
        if context is None:
            context = {}
        conf_obj = self.pool.get('res.config')
        first_f1 = bool(conf_obj.get(
            cursor, uid, 'sw_wizard_import_f1_and_atr_sends_f1_first', "1"
        ))

        if first_f1:
            self.send_f1_xmls(cursor, uid, ids, context=context)
            res = self.send_atr_xmls(cursor, uid, ids, context=context)
        else:
            res = self.send_atr_xmls(cursor, uid, ids, context=context)
            self.send_f1_xmls(cursor, uid, ids, context=context)

        # Finally the working directory can be removed
        self._unlink_working_directory(cursor, uid, ids, context)

        msg = _(u"""
            S'han importat correctament els fitxers ATR i s'han encuat els F1.\n
            Resultat XML ATR:\n\n{}
        """).format(res['info'])
        self.write(cursor, uid, ids, {
            'state': 'done',
            'info': msg,
            'casos': res['cas']
        })
        return True

    def send_atr_xmls(self, cursor, uid, ids, context=None):
        """
        Creates a ATR Wizard to which will be sent the previously generated
        zip with the XMLs.
        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param context: OpenERP context

        :return:    Dictionary with the info of the ATR importation.
                    Fields: [info, casos]
        """
        if context is None:
            context = {}

        # We get the zip files stored to the working directory
        working_directory = self.read(
            cursor, uid, ids, ['working_directory']
        )[0]['working_directory']
        filename, data = self._get_b64_zip_files(
            working_directory, context={'type': 'ATR'}
        )

        # We create the wizard to sent the zip's
        atr_wiz_obj = self.pool.get('giscedata.switching.wizard')
        context = {
            'active_id': context['active_id'],
            'active_ids': context['active_ids'],
        }
        wiz_id = atr_wiz_obj.create(
            cursor, uid, {
                'file': data,
                'name': basename(filename),
            }, context=context
        )
        atr_wiz_obj.action_importar_xml(cursor, uid, [wiz_id])
        return atr_wiz_obj.read(cursor, uid, wiz_id, ['info', 'cas'])[0]

    def action_mostrar_casos_importats(self, cursor, uid, ids, context=None):
        casos = self.read(cursor, uid, ids, ['casos'])[0]['casos']
        return {
            'name': _(u'Casos ATR importats correctament'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', {})]".format(casos),
        }

    def send_f1_xmls(self, cursor, uid, ids, context=None):
        """
        Creates a F1 Wizard to which will be sent the previously generated
        zip with the XMLs.
        :param cursor: Database cursor
        :param uid: User identifier
        :param ids: Elements ids
        :param context: OpenERP context
        """
        if context is None:
            context = {}

        # We get the zip files stored to the working directory
        working_directory = self.read(
            cursor, uid, ids, ['working_directory']
        )[0]['working_directory']
        filename, data = self._get_b64_zip_files(
            working_directory, context={'type': 'F1'}
        )

        f1_wiz_obj = self.pool.get('wizard.importacio.f1')
        context = {
            'active_id': context['active_id'],
            'active_ids': context['active_ids'],
        }
        wiz_id = f1_wiz_obj.create(
            cursor, uid, {
                'file': data,
                'filename': basename(filename)
            }, context=context
        )
        f1_wiz_obj.action_importar_f1(cursor, uid, [wiz_id])
        return True

    def is_f1(self, xml_data):
        return search(
            '<CodigoDelProceso>F1</CodigoDelProceso>',
            xml_data
        ) is not None

    _columns = {
        'file': fields.binary('Fitxer', required=True),
        'filename': fields.char('Nom', size=1024),
        'info': fields.text('Informació'),
        'state': fields.selection([
            ('init', 'Init'),
            ('load', 'Fitxers carregats'),
            ('done', 'Final')
        ], 'Estat'),
        'casos': fields.text('Casos importats'),
        'working_directory': fields.char('Directory temporal', size=128),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_default_info,
        'filename': lambda *a: False,
        'file': lambda *a: False,
        'working_directory': _get_working_directory_name
    }


WizardImportAtrAndF1()
