# -*- coding: utf-8 -*-
import json

from osv import osv, fields, orm
from tools.translate import _
from gestionatr.defs import (
    TABLA_8, TIPUS_DOCUMENT_INST_CIE, TABLA_64, TABLA_62, SINO, TABLA_117
)


class GiscedataSwitchingWizardA301(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.switching.a301.wizard'

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Generació de A3-01\n")

        return txt

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_obj = self.pool.get('giscedata.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_id', False)
        cnae = pol_obj.read(cursor, uid, pol_id, ['cnae'])
        config_vals = {
            'cnae': cnae['cnae'] and cnae['cnae'][0],
            'activacio_cicle': wizard.activacio_cicle,
            'data_accio': wizard.data_accio,
            'comentaris': wizard.comentaris
        }

        if wizard.dt_add:
            config_vals.update(
                {
                    'dt_add': wizard.dt_add,
                    'dt_cie_electronico': wizard.dt_cie_electronico,
                    'dt_cie_codigo': wizard.dt_cie_codigo,
                    'dt_cie_papel_potenciainstbt': wizard.dt_cie_papel_potenciainstbt,
                    'dt_cie_papel_data_emissio': wizard.dt_cie_papel_data_emissio,
                    'dt_cie_papel_data_caducitat': wizard.dt_cie_papel_data_caducitat,
                    'dt_tipus_codi_instalador': wizard.dt_tipus_codi_instalador,
                    'dt_codi_instalador': wizard.dt_codi_instalador,
                    'dt_cie_papel_tensio_suministre': wizard.dt_cie_papel_tensio_suministre,
                    'dt_cie_papel_tipus_suministre': wizard.dt_cie_papel_tipus_suministre,
                    'dt_cie_sello_elec': wizard.dt_cie_sello_elec,
                }
            )

        if wizard.doc_add and wizard.docs_ids:
            config_vals.update(
                {
                    'doc_add': wizard.doc_add,
                    'docs_ids': [d.id for d in wizard.docs_ids]
                }
            )

        if wizard.solicitud_tensio:
            config_vals.update(
                {'solicitud_tensio': wizard.solicitud_tensio}
            )


        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, 'A3', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.switching',
            'report_name': 'giscedata.switching.info_cas_ATR',
            'report_webkit': "'giscedata_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'casos': casos_ids
            },
        }

    def onchange_activacio(self, cursor, uid, ids, context=None):

        res = {}
        if context['contract_id']:
            contracte_id = context['contract_id']

            pol_obj = self.pool.get('giscedata.polissa')
            cups_obj = self.pool.get('giscedata.cups.ps')
            cups = pol_obj.read(cursor, uid, contracte_id, ['cups'])['cups'][0]

            contractes = pol_obj.search(cursor, uid, [('cups', '=', cups),
                                                      ('state', '=', 'activa')])

            if contractes:
                warning = {'title': _(u'Contracte actiu'),
                           'message': _(u'Ja existeix un contracte actiu pel '
                                        u'CUPS seleccionat.')}
                res.update({'warning': warning})

        return res

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generats': fields.text('Casos generats'),
        'contract': fields.many2one('giscedata.polissa', 'Contracte'),
        # A3-01 specific data
        'activacio_cicle': fields.selection(TABLA_8, u"Activació", required=True),
        'data_accio': fields.date(u"Data prevista d'activació"),
        'comentaris': fields.text('Comentari', size=4000),
        'solicitud_tensio': fields.selection(TABLA_117, u"Tensió Sol.licitada"),

        # Documentació Tècnica
        'dt_add': fields.boolean(u"Documentacio Tècnica"),
        'dt_cie_electronico': fields.selection(SINO, u"Cie Electronico"),
        'dt_cie_codigo': fields.char(u"Código CIE", size=11),
        'dt_cie_papel_potenciainstbt': fields.integer(u"Potència Instalada"),
        'dt_cie_papel_data_emissio': fields.date(u"Data Emissió"),
        'dt_cie_papel_data_caducitat': fields.date(u"Data Caducitat"),
        'dt_tipus_codi_instalador': fields.selection(
            TIPUS_DOCUMENT_INST_CIE, u"Tipus codi Instal·lador"
        ),
        'dt_codi_instalador': fields.char(u"Codi instal·lador", size=9),
        'dt_cie_papel_tensio_suministre': fields.selection(
            TABLA_64, u"Tensió suministre"
        ),
        'dt_cie_papel_tipus_suministre': fields.selection(
            TABLA_62, u"Tipus Suministre"
        ),
        'dt_cie_sello_elec': fields.char(u"Segell Electrónic", size=35),

        'doc_add': fields.boolean(u"Documents adjunts"),

        # En realidad la tabla relacional no se crea, al ser un objeto en
        # memoria actua como un campo one2many pero en memoria sin relacion
        # con el objeto destino
        # Mediante un read del campo veremos los que seleccionamos en el
        # asistente
        'docs_ids': fields.many2many(
            'giscedata.switching.document',
            'wizard_sw_a3_from_contract_docs_rel',
            'wiz_mod_id', 'doc_id',
            'Documents adjunts',
            domain=[('type', '=', 'commodore64')]
        ),

    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_info_default,
        'whereiam': _whereiam,
        'casos_generats': lambda *a: json.dumps([]),
        'activacio_cicle': lambda *a: 'A',
        'solicitud_tensio': lambda *a: False,
        'dt_cie_electronico': lambda *a: 'S',
        'doc_add': lambda *a: False,
        'dt_add': lambda *a: False,
    }


GiscedataSwitchingWizardA301()
