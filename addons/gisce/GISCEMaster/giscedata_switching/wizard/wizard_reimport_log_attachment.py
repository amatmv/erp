# -*- encoding: utf-8 -*-
import base64

from osv import osv, fields
from tools.translate import _


class WizardReimportLogAttachment(osv.osv_memory):
    _name = 'wizard.giscedata.switching.log.reimport'

    def _get_default_msg(self, cursor, uid, context=None):
        if context is None:
            context = {}
        log_ids = context.get('active_ids', [])
        return _(u"Es reimportaràn {} arxius".format(len(log_ids)))

    def reimport_files(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        log_ids = context.get('active_ids', False)

        if not log_ids:
            raise osv.except_osv('Error', _('No s\'han seleccionat ids'))
        log_obj = self.pool.get('giscedata.switching.log')
        attach_obj = self.pool.get('ir.attachment')
        sw_obj = self.pool.get('giscedata.switching')

        context.update({
            'update_logs': True
        })
        failed_files = []
        log_vals = log_obj.read(cursor, uid, log_ids, ['status'])
        for log in log_vals:
            if log['status'] == 'correcte':
                continue
            attach_ids = attach_obj.search(cursor, uid, [
                ('res_model', '=', 'giscedata.switching.log'),
                ('res_id', '=', log['id'])])
            if not attach_ids:
                continue
            data_vals = attach_obj.read(cursor, uid, attach_ids[0], ['datas', 'datas_fname'])
            data = base64.decodestring(data_vals['datas'])
            fname = data_vals['datas_fname']
            try:
                sw_obj.importar_xml(cursor, uid, data, fname, context=context)
            except Exception, e:
                e_string = str(e)
                if not e_string:
                    e_string = e.value
                failed_files.append((fname, e_string))
        info = _(u"S'han processat {} fitxers.\n".format(len(log_vals)))
        if failed_files:
            info += _(u"S'han produït els següents errors en els arxius importats:\n")
            for failed_f in failed_files:
                info += _(u"\t- Fitxer {} -> Error: {}\n".format(failed_f[0], failed_f[1]))

        self.write(cursor, uid, ids, {'state': 'end', 'msg': info}, context=context)

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'msg': fields.text('Missatge')
    }

    _defaults = {
        'state': lambda *a: 'init',
        'msg': _get_default_msg,
    }


WizardReimportLogAttachment()
