from osv import osv, fields
from tools.translate import _


PROCESSES = [
    'A3',
    'C1',
    'C2',
    'M1',
    'B1',
    'D1'
]


class WizardGiscedataSwitchingListImported(osv.osv):
    _name = 'wizard.giscedata.switching.list.imported'

    _columns = {
        'processes': fields.selection(
            [('all', 'All'), ('select', 'Select')],
            'Process to list'
        ),
        'a3': fields.boolean('A3'),
        'c1': fields.boolean('C1'),
        'c2': fields.boolean('C2'),
        'm1': fields.boolean('M1'),
        'b1': fields.boolean('B1'),
        'd1': fields.boolean('D1')
    }

    def get_domain(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')
        proc_obj = self.pool.get('giscedata.switching.proces')
        cups_obj = self.pool.get('giscedata.cups.ps')
        wiz = self.browse(cursor, uid, ids[0], context)
        cups_id = cups_obj.search(cursor, uid, [], limit=1)[0]
        where = sw_obj.whereiam(cursor, uid, cups_id, context)
        if where == 'distri':
            where = 'comer'
        else:
            where = 'distri'
        domain = []
        if wiz.processes == 'all':
            processes = PROCESSES
        else:
            processes = [
                proc for proc in PROCESSES if getattr(wiz, proc.lower())
            ]
        for idx, proc in enumerate(processes):
            steps = proc_obj.get_emisor_steps(cursor, uid, proc, where, context)
            item = [
                '&',
                ('proces_id.name', '=', proc),
                ('step_id.name', 'in', steps)
            ]
            if processes > 1 and idx >= 1:
                item.insert(0, '|')
            for x in reversed(item):
                domain.insert(0, x)
        if not domain:
            domain = [('id', '=', -1)]
        return domain

    def list(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        wiz = self.browse(cursor, uid, ids[0], context)
        domain = wiz.get_domain()
        return {
            'domain': domain,
            'name': _('ATR Imported'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    _defaults = {
        'processes': lambda *a: 'all'
    }

WizardGiscedataSwitchingListImported()