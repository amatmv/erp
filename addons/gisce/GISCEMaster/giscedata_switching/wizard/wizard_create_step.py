# -*- coding: utf-8 -*-
from osv import osv, fields, orm
from tools.translate import _
from collections import namedtuple
import json

CreationResult = namedtuple('CreationResult', ['code', 'case', 'result'])

TAULA_OPCIONS = [
    ('A', u'Acceptació'),
    ('R', u'Rebuig'),
]

class WizardCreateStep(osv.osv_memory):
    """Wizard to generate steps of switching cases."""
    _name = 'wizard.create.step'

    def action_create_steps(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        sw_ids = context.get('active_ids', [])
        winfo = self.read(cursor, uid, ids[0], ['step', 'check_repeated'])[0]
        pas = winfo['step']
        check_rep = winfo['check_repeated']
        created = []
        already_created = []
        invalid_step = []
        wiz_values = self.read(cursor, uid, ids[0], ['step_is_rejectable', 'option', 'motiu_rebuig'])[0]
        if wiz_values and wiz_values['step_is_rejectable']:
            del wiz_values['id']
            context.update(wiz_values)
        for sw_id in sw_ids:
            res = self.proces_step_creation(cursor, uid, sw_id, pas, check_repeated=check_rep, context=context)
            if res.code == 1:
                created.append(res)
            elif res.code == 2:
                already_created.append(res)
            else:
                invalid_step.append(res)
        msg = _(u"""
Casos amb passos creats: {0}
Casos que ja tenien el pas creat: {1}
Casos en que no s'ha creat el pas: {2}

        """).format(len(created), len(already_created), len(invalid_step))

        if len(already_created) or len(invalid_step):
            msg = _(u"""{0}
========================================
Casos Erronis
========================================
        """).format(msg)

        for case_info in already_created+invalid_step:
            msg = _(
                u"{0}\n * Error en el cas amb id {1}: {2}.\n").format(
                msg, case_info.case, case_info.result
            )
        correct_ids = json.dumps([x.case for x in created])
        self.write(cursor, uid, ids, {'state': 'end', 'info': msg, 'sw_ids': correct_ids})

    def proces_step_creation(self, cursor, uid, sw_id, pas, check_repeated=True, context=None):
        sw_obj = self.pool.get("giscedata.switching")
        step_obj = self.pool.get('giscedata.switching.step')
        info_obj = self.pool.get('giscedata.switching.step.info')

        proces_id, proces_name = sw_obj.read(cursor, uid, sw_id, ['proces_id'])['proces_id']

        if check_repeated and self.sw_has_step(cursor, uid, sw_id, proces_name, pas, context=context):
            res = _(u"Ja existia el pas {0}").format(pas)
            return CreationResult(2, sw_id, res)

        additional_step = None
        if proces_name in ["C1", "C2"] and pas == "02":
            # Create step 11
            additional_step = "11"
        elif proces_name in ["C1", "C2"] and pas == "05":
            # Create step 06
            additional_step = "06"
        if additional_step:
            self.proces_step_creation(cursor, uid, sw_id, additional_step, check_repeated, context)

        res_code = 1
        res = _(u"Pas {0} creat correctament").format(pas)

        try:
            step = step_obj.get_step(cursor, uid, pas, proces_name)
        except Exception as e:
            res_code = 3
            res = _(u"No s'ha trobat el pas {0} per el procés {1}").format(
                pas, proces_name
            )
            return CreationResult(res_code, sw_id, res)

        vals = {'sw_id': sw_id,
                'step_id': step,
                'proces_id': proces_id,
                'pas_id': False,
                }
        try:
            info_obj.create(cursor, uid, vals, context=context)
        except Exception as e:
            res_code = 3
            res = e.message
            # If we have already created the step.info object,
            # we have to delete it because it doesn't have pas_id
            info_id = info_obj.search(cursor, uid, [
                ('sw_id', '=', sw_id),
                ('step_id', '=', step),
                ('pas_id', '=', None)
            ])
            if info_id:
                info_obj.unlink(cursor, uid, info_id)

        return CreationResult(res_code, sw_id, res)

    def sw_has_step(self, cursor, uid, sw_id, proces_name, pas, context=None):
        model = "giscedata.switching.{0}.{1}".format(proces_name.lower(), pas)
        obj = self.pool.get(model)
        if not obj:
            return False
        return len(obj.search(cursor, uid, [('sw_id', '=', sw_id)]))

    def action_open_cases(self, cursor, uid, ids, context=None):
        sw_ids = self.read(cursor, uid, ids[0], ['sw_ids'])[0]['sw_ids']
        sw_ids = json.loads(sw_ids)
        return {
            'domain': "[('id','in', %s)]" % str(sw_ids),
            'name': 'Casos ATR',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def _get_aviable_steps(self, cursor, uid, context=None):
        if context is None:
            context = {}
        sw_ids = context.get('active_ids', [])
        aviable_steps = []
        for sw_id in sw_ids:
            new_steps = self.get_sw_steps(cursor, uid, sw_id, context=context)
            aviable_steps = aviable_steps + new_steps
        return sorted(list(set(aviable_steps)))

    def get_sw_steps(self, cursor, uid, sw_id, context=None):
        if context is None:
            context = {}
        proces_obj = self.pool.get("giscedata.switching.proces")
        sw_obj = self.pool.get("giscedata.switching")
        proces = sw_obj.read(cursor, uid, sw_id, ['proces_id'])['proces_id']
        ctx = context.copy()
        ctx['add_description'] = True
        res = proces_obj.get_steps(cursor, uid, proces[0], context=ctx)
        return [(x[0], "{0} - {1}".format(x[0], x[2])) for x in res]

    def _get_default_info(self, cursor, uid, context=None):
        if context is None:
            context = {}
        sw_ids = context.get('active_ids', [])
        return _(u"S'han seleccionat {0} casos.").format(len(sw_ids))

    def _get_sw_ids(self, cursor, uid, context=None):
        if context is None:
            return {}
        return str(context.get('active_ids', []))

    def onchange_step(self, cursor, uid, ids, sw_ids, step, context=None):
        res = {'value': {}, 'domain': {}, 'warning': {}}
        sw_ids = eval(sw_ids)
        if sw_ids:
            sw_obj = self.pool.get("giscedata.switching")
            step_obj = self.pool.get("giscedata.switching.step")

            proces_id = sw_obj.read(cursor, uid, sw_ids[0], ['proces_id'])['proces_id']
            step_id = step_obj.search(cursor, uid, [('proces_id', '=', proces_id[0]), ('name', '=', step)])
            is_rejectable = step_obj.read(cursor, uid, step_id[0], ['is_rejectable'])['is_rejectable']
            if is_rejectable:
                res['value'].update({'step_is_rejectable': is_rejectable})
            else:
                res['value'].update({'step_is_rejectable': is_rejectable,
                                     'option': 'A'})
        return res

    def _ff_motiu_rebuig(self, cursor, uid, ids, context=None):
        res = []

        sw_obj = self.pool.get('giscedata.switching')
        motiu_obj = self.pool.get('giscedata.switching.motiu.rebuig')

        sw_ids = eval(self._get_sw_ids(cursor, uid, ids))
        if sw_ids:
            proces_info = sw_obj.read(cursor, uid, sw_ids[0], ['proces_id'])['proces_id']

            if proces_info:
                proces_ids = proces_info[0]
                motiu_ids = motiu_obj.search(cursor, uid, [('proces_ids', 'in', proces_ids)])
                motiu_info = motiu_obj.read(cursor, uid, motiu_ids, ['text', 'name'])

                for motiu in motiu_info:
                    motiu_text = "{} - {}".format(motiu['name'], motiu['text'])
                    res.append((motiu['id'], motiu_text))
        return res

    _columns = {
        'state': fields.char("State", size=16),
        'step': fields.selection(_get_aviable_steps, "Pas", required=True),
        'step_is_rejectable': fields.boolean("És rebutjable"),
        'option': fields.selection(TAULA_OPCIONS, "Opció"),
        'motiu_rebuig': fields.selection(_ff_motiu_rebuig, "Motiu rebuig"),
        'info': fields.text(),
        'check_repeated': fields.boolean("Ignorar Repetits", help=_(u"Marcar per no crear passos repetits en els casos")),
        'sw_ids': fields.text(),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'step_is_rejectable': lambda *a: False,
        'option': lambda *a: 'A',
        'info': _get_default_info,
        'sw_ids': _get_sw_ids,
        'check_repeated': lambda *a: True
    }


WizardCreateStep()
