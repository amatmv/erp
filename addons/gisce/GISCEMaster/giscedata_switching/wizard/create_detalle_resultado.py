from gestionatr.defs import TABLA_73

XML_DATA = """        <record model="giscedata.switching.detalle.resultado" id="sw_detalle_resultado_{0}">
            <field name="name">{0}</field>
            <field name="text">{1}</field>
        </record>\n"""

new = open("detalls_data.xml", 'w')
for code, text in TABLA_73:
    new.write(XML_DATA.format(code, text))
new.close()