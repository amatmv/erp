# -*- coding: utf-8 -*-
import base64
import time
from datetime import datetime
import StringIO
import zipfile
import pooler
import json

from osv import osv, fields, orm
from tools.translate import _
from tools import ustr, cache
from slugify import slugify


def fix_vat(vat):
    vat = vat.upper()
    if vat and vat[0].isdigit():
        vat = vat.zfill(9)
    return vat


class GiscedataSwitchingWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.switching.wizard'

    def action_anullar(self, cursor, uid, ids, context=None):
        """Anul·lar la sol·licitud
        """
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        # prevenim l'acció des de la vista tree per a múltiples casos
        if wizard.multicas:
            info = _(u'No és possible anul·lar múltiples casos.')
            wizard.write({'state': 'done', 'info': info})
            return False
        sw_obj = self.pool.get('giscedata.switching')
        sw_id = context['active_id']
        sw = sw_obj.browse(cursor, uid, sw_id)
        if (not sw.check_anullar()
            and wizard.state != 'confirm'
            and not sw.state in ['draft']):
            info = _(u'La sol·licitud no ha estat enviada.\n'
                     u'Desitja continuar anul·lant la communicació?')
            wizard.write({'state': 'confirm', 'cas': sw_id, 'info': info})
            return False
        val = sw.anullar_sollicitud()
        if not val:
            info = _(u'No és possible anul·lar la sol·licitud.')
            if sw_obj.state in ['draft']:
                info += '\n' + _(u'El cas es troba en estat esborrany.')
        else:
            info = _(u"Anul·lada la sol·licitud %s.\n"
                     u"S'accedeix al pas %s.") % \
                    (sw_obj.codi_sollicitud, sw_obj.pas)
        wizard.write({'state': 'done', 'cas': sw_id, 'info': info})

    def action_exportar_xml(self, cursor, uid, ids, context=None):
        """Llença l'script per generar l'xml.
        """
        if not context:
            context = {}
        import netsvc
        logger = netsvc.Logger()
        data = False
        enviables = False
        sw_obj = self.pool.get('giscedata.switching')
        sw_proc_obj = self.pool.get('giscedata.switching.proces')
        partner_obj = self.pool.get('res.partner')
        res_config = self.pool.get('res.config')
        wizard = self.browse(cursor, uid, ids[0], context)
        if wizard.mark_as_processed:
            context.update({'mark_as_sended': True})
        if wizard.send_always:
            enviables = True
        if not wizard.multicas:
            if not wizard.step_id:
                raise osv.except_osv(u'Atenció',
                                     u"No hi ha un codi de pas "
                                     u"seleccionat per exportar")
            sw_id = context['active_id']
            step_info = eval(wizard.step_id)
            try:
                res = sw_obj.exportar_xml(cursor, uid, sw_id,
                                          step_id=step_info[0],
                                          pas_id=step_info[1],
                                          context=context)
                data = base64.encodestring(res[1])
                wizard.write({'name': res[0], 'file': data, 'state': 'done',
                              'info': res[2].error or _(u"Correct File")})
                return True
            except Exception as e:
                raise
        else:
            err = []
            sw_err = []
            sw_valid = []

            zipfiles = {}
            sios = {}
            zipnames = {}
            err_msg = '+ Cas id: %s\n%s\n'
            exptime = time.strftime('%Y%m%d%H%M%S')
            nom = '%s_%s.zip' % (cursor.dbname, exptime)
            fileHandle = StringIO.StringIO()
            comp = zipfile.ZIP_DEFLATED
            zf = zipfile.ZipFile(fileHandle, mode='w', compression=comp)
            for sw_id in context['active_ids']:
                try:
                    passos = []
                    if enviables:
                        passos = sw_obj.get_passos_enviables(
                            cursor, uid, sw_id, context=context)
                    else:
                        passos = sw_obj.get_passos_pendents_enviament(
                            cursor, uid, sw_id, context=context)

                    for step_id in passos:
                        # busquem el destinatari
                        dest_id = sw_obj.get_destinatari_actual(cursor, uid, sw_id, context=context)
                        dest_info = partner_obj.read(cursor, uid, dest_id, ['ref', 'comercial', 'category_id'])
                        proces_id = sw_obj.read(
                            cursor, uid, sw_id, ['proces_id']
                        )['proces_id'][0]
                        r1_proces_id = sw_proc_obj.search(
                            cursor, uid, [('name', '=', 'R1')]
                        )[0]
                        is_type_R = proces_id == r1_proces_id
                        ref = dest_info['ref']
                        for categ_id in dest_info['category_id']:
                            if self.check_categ_is_atr_group(cursor, uid, categ_id, context=context):
                                ref = self.get_categ_name_for_zip(cursor, uid, categ_id, context=context)
                                break
                        for categ_id in dest_info['category_id']:
                            if is_type_R and self.check_categ_is_r1_group(cursor, uid, categ_id, context=context):
                                ref = ref + '_R1'
                                break
                        pname = dest_info['comercial']
                        if pname and ref.endswith('_R1'):
                            pname += '_R1'

                        if not zipfiles.get(ref, False):
                            sios[ref] = StringIO.StringIO()
                            zipfiles[ref] = zipfile.ZipFile(sios[ref], mode='w', compression=comp)
                            zipnames[ref] = pname or ref
                            logstr = 'New destination in zip: %s (%s)'
                            logger.notifyChannel('switching', netsvc.LOG_DEBUG, logstr % (ref, zipnames[ref]))

                        res = sw_obj.exportar_xml(cursor, uid, sw_id, step_id=step_id, context=context)
                        if not res[2].valid:
                            # exportar_xml now manage osv exeptions, so we have to
                            # check if exportation was correct and raise them.
                            raise osv.except_osv('Error', res[2].error)

                        zipfiles[ref].writestr(res[0], res[1])
                        sw_valid.append(sw_id)
                except osv.except_osv as e:
                    sw_err.append(sw_id)
                    err.append(err_msg % (sw_id, ustr(e.value)))
                except Exception as e:
                    raise e
            lenzip = len(zipfiles)
            for ref, zipf in zipfiles.items():
                zipf.close()
                fname = '%s_%s_%s.zip' % (cursor.dbname, zipnames[ref],
                                          exptime)
                zf.writestr(fname, sios[ref].getvalue())
                # Si només tenim una empresa, aquest serà el zip final
                if lenzip == 1:
                    data = base64.encodestring(sios[ref].getvalue())
                    nom = fname
                sios[ref].close()
            state = 'done'
            zf.close()
            if lenzip > 1:
                data = base64.encodestring(fileHandle.getvalue())

            fileHandle.close()
            info = '\n'.join(err)
            wizard.write({'name': nom,
                          'file': data,
                          'state': state,
                          'info': info,
                          'cas_err': sw_err,
                          'cas': sw_valid})
            logger.notifyChannel('switching', netsvc.LOG_DEBUG,
                                 'ZIP file generated: %s' % nom)

        return True

    def get_categ_name_for_zip(self, cursor, uid, categ_id, context=None):
        categ_obj = self.pool.get('res.partner.category')
        cname = categ_obj.read(cursor, uid, categ_id, ['name'])['name']
        return slugify(cname)

    @cache(timeout=180)
    def check_categ_is_atr_group(self, cursor, uid, categ_id, context=None):
        categ_obj = self.pool.get('res.partner.category')
        imd_obj = self.pool.get('ir.model.data')

        base_atr_categ = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_switching', 'res_partner_category_atr_groups'
        )[1]
        return categ_id in categ_obj.search(cursor, uid, [('parent_id', 'child_of', [base_atr_categ])])

    @cache(timeout=180)
    def check_categ_is_r1_group(self, cursor, uid, categ_id, context=None):
        categ_obj = self.pool.get('res.partner.category')
        imd_obj = self.pool.get('ir.model.data')

        atr_r1_categ = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_switching',
            'res_partner_category_atr_r1_groups'
        )[1]
        return categ_id in categ_obj.search(cursor, uid, [
            ('parent_id', 'child_of', [atr_r1_categ])])

    def _import_zip_infolist(self, uid, sw_obj, db, zfile, zipinfolist, context=None):
        _str = _(u'+ Fitxer: %s\n%s\n')
        sw_ids = []
        _msg = []
        _error_msg = []
        _failed_zip_info = []
        for info in zipinfolist:
            data = zfile.read(info.filename)
            try:
                fname = unicode(info.filename, errors='replace')
            except TypeError:
                fname = info.filename
            try:
                tmp_cr = db.cursor()
                sw_id, msg = sw_obj.importar_xml(tmp_cr, uid, data,
                                                 fname, context)
                sw_ids.append(sw_id)
                tmp_cr.commit()
                _msg.append(_str % (ustr(fname), msg))
            except (osv.except_osv, orm.except_orm) as e:
                msg = ustr(e.value)
                _error_msg.append(_str % (ustr(fname), msg))
                _failed_zip_info.append(info)
                tmp_cr.rollback()
            except Exception as e:
                msg = 'Error: %s' % e.message
                _error_msg.append(_str % (ustr(fname), msg))
                _failed_zip_info.append(info)
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()
                tmp_cr.rollback()
            finally:
                tmp_cr.close()
        return {
            'sw_ids': sw_ids,
            'msg': _msg,
            'error_msg': _error_msg,
            'failed_zip_info': _failed_zip_info
        }

    def action_importar_xml(self, cursor, uid, ids, context=None):
        """Importar un XML
        """
        sw_obj = self.pool.get('giscedata.switching')
        mod_obj = self.pool.get('giscedata.polissa.modcontractual')
        if not context:
            context = {}
        context['update_logs'] = True
        wizard = self.browse(cursor, uid, ids[0], context)
        db = pooler.get_db_only(cursor.dbname)
        if not wizard.file:
            err = _(u'Sel·leccioneu un fitxer')
            raise orm.except_orm(u'Atenció', err)
        data = base64.decodestring(wizard.file)
        fileHandle = StringIO.StringIO(data)
        is_xml = False
        try:
            zfile = zipfile.ZipFile(fileHandle, "r")
            if wizard.name:
                context['zip_name'] = wizard.name.replace('_ATR', '.zip')
        except zipfile.BadZipfile as e:
            is_xml = True
        sw_ids = []
        _msg = []
        _err_msg = []
        _str = _(u'+ Fitxer: %s\n%s\n')
        if is_xml:
            try:
                wizard.name = unicode(wizard.name, errors='replace')
            except TypeError:
                    wizard.name = wizard.name
            try:
                tmp_cr = db.cursor() 
                (sw_id, msg) = sw_obj.importar_xml(tmp_cr, uid, data,
                                                   wizard.name, context)
                sw_ids.append(sw_id)
                tmp_cr.commit()
            except (osv.except_osv, orm.except_orm) as e:
                msg = ustr(e.value)
                tmp_cr.rollback()
            except Exception as e:
                msg = 'Error: %s' % e.message
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()
                tmp_cr.rollback()
            finally:
                tmp_cr.close()
        else:
            pending_zip_info_list = zfile.infolist()
            imported_failed = []
            while True:
                import_result = self._import_zip_infolist(
                    uid, sw_obj, db, zfile, pending_zip_info_list, context)
                sw_ids.extend(import_result['sw_ids'])
                _msg.extend(import_result['msg'])
                imported_failed = import_result['failed_zip_info']
                if len(imported_failed) == 0:
                    # No errors, so we are done
                    break;
                elif len(imported_failed) == len(pending_zip_info_list):
                    # The number or errors has not decreased, so we are done
                    _err_msg.extend(import_result['error_msg'])
                    break
                else:
                    # Let's retry failed. Some of them could be succesful due
                    # to ordering issues
                    pending_zip_info_list = imported_failed

            n_errors = len(imported_failed)
            n_total = len(sw_ids) + n_errors
            total_cases_msg = (
                _('S\'han carregat un total de {0} casos').format(n_total)
            )
            total_error_msg = (
                _('S\'han generat {0} casos erronis').format(str(n_errors))
            )
            total_correct_msg = (_('S\'han generat {0} casos correctes').format(
                str(n_total - n_errors))
            )

            header_msg = (
                '{0}\n{1}\n{2}\n\n'.format(
                    total_cases_msg, total_error_msg, total_correct_msg
                )
            )

            err_msg = ''
            crr_msg = ''
            if _err_msg:
                err_msg = '------Errors------:\n' + '\n'.join(_err_msg) + '\n\n'
            if _msg:
                crr_msg = '------Correctes------:\n' + '\n'.join(_msg) + '\n\n'
            msg = header_msg + '\n' + err_msg + crr_msg

            usr_obj = self.pool.get('res.users')
            requests_to = [usr['login'] for usr in usr_obj.read(
                cursor, uid, [1, uid], ['login'])]

            file_name = ''
            subject = 'Carrega zip' + file_name
            mod_obj.send_action_message(
                cursor, uid, emails_to=None, requests_to=requests_to,
                subject=subject, message=header_msg + '\n' + err_msg + crr_msg
            )

        if sw_ids:
            sw_obj.write(cursor, uid, sw_ids, {
                'date': wizard.data_importacio
            })
        wizard.write({'state': 'done',
                      'cas': str(tuple(sw_ids)),
                      'info': msg})

    def action_mostrar_cas(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        if 'result' in context and context['result'] == 'no_export':
            txt = _('no exportats')
            casos = wizard.cas_err
        else:
            txt = _('importats correctament')
            casos = wizard.cas
        return {
            'name': _(u'Casos %s') % txt,
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % casos,
        }

    def action_enviar_xml(self, cursor, uid, ids, context=None):
        """Envia per correu els XML dels passos"""

        sw_obj = self.pool.get('giscedata.switching')
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        if not wizard.body:
            raise osv.except_osv(_(u'Avís'),
                  _(u"Cal omplir el cos de missatge."))

        sw_ids = context['active_ids']
        resum = ''
        email_cc = wizard.email_cc
        email_cco = wizard.email_cco
        for sw_id in sw_ids:
            try:
                sw = sw_obj.browse(cursor, uid, sw_id, context)
                ctx = context.copy()
                ctx.update({'from_account': wizard.from_account})
                sw.write({'description': wizard.body})
                step_id = wizard.step_id and eval(wizard.step_id)[0] or False
                sw.case_log_reply(context=ctx, email=wizard.email_to,
                                  step_id=step_id or False,
                                  email_cc=email_cc, email_cco=email_cco
                                  )
                info = _(u"El correu s'ha enviat correctament.\n\n")
            except osv.except_osv as e:
                info = u'ERROR: %s\n\n' % ustr(e.value)
            except Exception as e:
                raise Exception(e)
            resum += _(u'Id {0}: {1}\n\n'.format(sw_id, info))
        wizard.write({'name': '', 'file': '', 'state': 'done',
                      'info': resum})
        return True

    def set_state(self, cursor, uid, ids, context=None):
        """Canvia el valor de la variable state del wizard segons
           l'especificat en el context"""
        if not context:
            return False
        wizard = self.browse(cursor, uid, ids[0], context)
        wizard.write({'state': context['state']})
        return True

    def onchange_pas(self, cursor, uid, ids, step_id, sw_id):
        sw_proc_obj = self.pool.get('giscedata.switching.proces')

        res = {'value': {}, 'warning': {}, 'domain': {}}

        step_names = dict(
            self._get_gen_steps(cursor, uid, context={'active_id': sw_id})
        )

        if sw_id:
            res['value'].update(
                {'email_to': self._get_email_to(
                    cursor, uid, sw_id, step_names[step_id]
                )}
            )

        return res

    def _get_accounts(self, cursor, uid, context=None):
        if context is None:
            context = {}
        account_obj = self.pool.get('poweremail.core_accounts')
        user_obj = self.pool.get('res.users')
        # Search for personal accounts
        search_params = [('company', '=', 'no'),
                         ('user', '=', uid),
                         ('state', '=', 'approved')]
        account_ids = account_obj.search(cursor, uid, search_params,
                                         context=context)
        res = []
        if account_ids:
            accounts = account_obj.browse(cursor, uid, account_ids, context)
            res.extend([(r.id, r.name + " (" + r.email_id + ")")
                        for r in accounts])
        # Search for permitted company accounts
        search_params = [('company', '=', 'yes'),
                         ('state', '=', 'approved')]
        account_ids = account_obj.search(cursor, uid, search_params,
                                         context=context)
        user = user_obj.browse(cursor, uid, uid)
        for account in account_obj.browse(cursor, uid, account_ids, context):
            user_groups = [x.id for x in user.groups_id]
            allowed = [x.id for x in account.allowed_groups
                       if x.id in user_groups]
            if allowed:
                res.extend([(account.id,
                             account.name + " (" + account.email_id + ")")])
        return res

    def _get_gen_steps(self, cursor, uid, context=None):

        if context is None:
            context = {}

        res = []
        sw_obj = self.pool.get('giscedata.switching')
        sw_id = context.get('active_id', 0)
        if sw_id:
            sw = sw_obj.browse(cursor, uid, sw_id)
            for step in sw.step_ids:
                if step.pas_id:
                    pas_model = self.pool.get(step.pas_id.split(',')[0])
                    pas_id = int(step.pas_id.split(',')[1])
                    pas = pas_model.browse(cursor, uid, pas_id)
                    if pas.emisor_id.ref == sw.company_id.ref:
                        res.append(
                            (str((step.step_id.id, pas_id)),
                             "{0} ({1}) ({2})".format(step.step_id.name, pas.date_created, pas_id)))
            if res:
                res = sorted(res, key=lambda x: x[1])
        return res

    _columns = {
        # 'cas': fields.many2one('giscedata.switching.c1','Cas'),
        'cas': fields.text('Casos importats'),
        'cas_err': fields.text('Casos erronis'),
        'file': fields.binary('Fitxer'),
        'state': fields.char('State', size=16),
        'export': fields.boolean('XML exportable'),
        'name': fields.char('File name', size=256),
        'info': fields.text('Info'),
        'info_export': fields.text('Info'),
        'help': fields.text('Help'),
        'body': fields.text('Body'),
        'from_account': fields.selection(_get_accounts, 'Des de',
                                         required=True),
        'email_to': fields.char('Receptor (Per a)', size=250),
        'multicas': fields.boolean('Múltiples casos'),
        'step_id': fields.selection(_get_gen_steps, 'Passos'),
        'mark_as_processed': fields.boolean('Marca com a processat'),
        'send_always': fields.boolean('Exportar passos ja enviats'),
        'data_importacio': fields.datetime('Data importació'),
        'sw_id': fields.many2one('giscedata.switching'),
        'email_cc': fields.char('CC', size=250),
        'email_cco': fields.char('CCO', size=250),
    }

    def _get_old_company_email(self, cr, uid, sw):
        res_addr_obj = self.pool.get('res.partner.address')

        old_company = None
        if sw.comer_sortint_id:
            old_company = sw.comer_sortint_id.id
        elif sw.cups_id:
            cr.execute(
                "SELECT comercialitzadora "
                "FROM giscedata_polissa_modcontractual "
                "WHERE cups={0} AND data_final <= '{1}' "
                "ORDER BY data_final DESC".format(
                    sw.cups_id.id, sw.get_pas().data_activacio
                )
            )
            result = cr.fetchall()
            if result:
                old_company = result[0][0]

        if old_company:
            addr_id = res_addr_obj.search(cr, uid, [(
                'partner_id', '=', old_company
            )])[0]

            email_addr = res_addr_obj.read(cr, uid, addr_id, ['email'])

            if email_addr:
                return email_addr.get('email', False)

        return False

    def _get_email_to(self, cr, uid, sw_id, step_name=False):
        sw_obj = self.pool.get('giscedata.switching')
        sw_proc_obj = self.pool.get('giscedata.switching.proces')
        sw = sw_obj.browse(cr, uid, sw_id)
        try:
            if step_name:
                old_company_steps = sw_proc_obj.get_old_company_steps(
                    cr, uid, sw.proces_id.name
                )
                step_name = (len(step_name) >= 2 and step_name[:2]) or step_name
                if step_name in old_company_steps:
                    return self._get_old_company_email(cr, uid, sw)

            return sw.case_id.email_from
        except Exception:
            return False

    def _get_default_email_to(self, cr, uid, context=None):
        if context is None:
            return False

        return self._get_email_to(cr, uid, context.get('active_id', False))

    def _get_multicas(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context['active_ids']) > 1

    # TODO review this function
    def _get_info_export(self, cr, uid, context=None):
        if not context:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cr, uid, context['active_id'])
        # val = sw.cas_exportable(context=context)
        val = True
        info = []
        if not val:
            msg = _(u"Si us plau, descarregui el document del llistat "
                    u"d'adjunts associats al cas.")
            info.append(msg)
        value = sw.localitzar_adjunt(context=context)
        if value:
            att = self.pool.get('ir.attachment')
            val = att.read(cr, uid, value, ['name'], context=context)
            msg = _(u"El document es correspon a l'adjunt %s (id %d).") % \
                                                           (val['name'], value)
        else:
            msg = _(u"No s'ha trobat el document adjunt al cas.")
        info.append(msg)
        return '\n'.join(info)

    def _get_default_sw_id(self, cursor, uid, context=None):
        if context is None:
            context = {}

        return context.get('active_id', False)

    _defaults = {
        'state': lambda *a: 'init',
        'email_to': _get_default_email_to,
        'multicas': _get_multicas,
        'mark_as_processed': lambda *a: False,
        'send_always': lambda *a: False,
        'data_importacio': lambda *a: datetime.now().strftime(
            '%Y-%m-%d %H:%M:00'
        ),
        'sw_id': _get_default_sw_id,
        'email_cc': lambda *a: False,
        'email_cco': lambda *a: False,
    }

GiscedataSwitchingWizard()


class GiscedataSwitchingPolissaC101Wizard(osv.osv_memory):
    """Wizard per gestionar casos des de la pólissa"""
    _name = "giscedata.switching.polissa.c101.wizard"

    def _get_multipol(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context['active_ids']) > 1

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Només de les pólisses que:\n")

        if self._whereiam(cursor, uid, context=context) == 'comer':
            extra_txt = _(u" * Estan en esborrany per C1/C2/A3\n"
                          u" * Estan actives per M1/B1/R1\n"
                          u" * Esta actives i amb autolectures per W1 "
                          u"encara que tingui casos ATR creats\n"
                          u" * No tenen cap altre cas creat de gestió ATR\n")
        else:
            extra_txt = _(u" * D1 encara que tingui casos ATR creats\n")
        return txt + extra_txt

    def _te_cap_en_estat(self, cursor, uid, estat, context):
        pol_ids = context.get('active_ids', [])
        pol_obj = self.pool.get('giscedata.polissa')
        return any((
            pol['state'] == estat
            for pol in pol_obj.read(cursor, uid, pol_ids, ['state'])
        ))

    def _te_esborranys(self, cursor, uid, context=None):
        return self._te_cap_en_estat(cursor, uid, 'esborrany', context)

    def _te_actives(self, cursor, uid, context=None):
        return self._te_cap_en_estat(cursor, uid, 'activa', context)

    def _te_autolectures(self, cursor, uid, context=None):
        pol_ids = context.get('active_ids', [])
        pol_obj = self.pool.get('giscedata.polissa')

        return any((
            pol_obj.get_last_selfmetering(cursor, uid, pol_id)
            for pol_id in pol_ids
        ))

    def _whereiam(self, cursor, uid, context=None):
        ''' returns 'distri' or 'comer' '''
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cr, uid, ids, context=None):
        """ Genera el cas C1/C2/D1/M1/A3/B1/W1/R1 pas 01 de les polisses """
        if not context:
            context = {}
        wizard = self.browse(cr, uid, ids[0], context)
        proces = context.get('proces', 'Cn')
        state = wizard.state
        extra_vals = None
        # Created cases
        casos_ids = []

        pol_obj = self.pool.get('giscedata.polissa')
        pol_ids = context.get('active_ids', [])
        if not wizard.multipol:
            pol_ids = [context.get('active_id')]

        if not context.get("allow_multipol", True) and len(pol_ids) > 1:
            raise osv.except_osv(
                _('Atenció!'),
                _('La generació massiva per aquest tipus de cas ({}) no està permesa.').format(proces)
            )

        # Jump to the specific R1-01 form always
        if proces == 'R1':
            return self.generar_r101(cr, uid, ids, pol_ids, context=context)

        # Jump to the specific C2-01 form
        if len(pol_ids) == 1 and proces == 'C2':
            return self.generar_c201(cr, uid, ids, pol_ids[0], context=context)

        # Jump to the specific M1-01 form
        config_vals = None
        if len(pol_ids) == 1 and proces == 'M1':
            return self.generar_m101(cr, uid, ids, pol_ids[0], context=context)

        # Jump to the specific B1-01 form
        if len(pol_ids) == 1 and proces == 'B1':
            return self.generar_b101(cr, uid, ids, context=context)

        # Jump to the specific A3-01 form
        if len(pol_ids) == 1 and proces == 'A3':
            return self.generar_a301(cr, uid, ids, context=context)

        # Jump to the specific C1-01 form
        if len(pol_ids) == 1 and proces == 'C1':
            return self.generar_c101(cr, uid, ids, context=context)

        info = ''
        for pol_id in pol_ids:
            res = pol_obj.crear_cas_atr(
                cr, uid, pol_id, proces, config_vals=config_vals,
                context=context
            )

            info += "%s: %s\n" % (res[0], res[1])
            if res[2]:
                casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def generar_b101(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        contract_id = context.get('active_id', False)
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscedata.switching.b101.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': "{'contract_id': %s}" % contract_id
        }

    def generar_a301(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        contract_id = context.get('active_id', False)
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscedata.switching.a301.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': "{'contract_id': %s}" % contract_id
        }

    def generar_c101(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        contract_id = context.get('active_id', False)
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscedata.switching.c101.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': "{'contract_id': %s}" % contract_id
        }

    def generar_m101(self, cursor, uid, ids, pol_id, context=None):
        return self.generar_mod_con(cursor, uid, ids, pol_id, 'M1', context)

    def generar_c201(self, cursor, uid, ids, pol_id, context=None):
        return self.generar_mod_con(cursor, uid, ids, pol_id, 'C2', context)

    def generar_mod_con(self, cursor, uid, ids, pol_id, cas, context=None):
        if context is None:
            context = {}

        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscedata.switching.mod.con.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context':
                    """{{
                        'cas': '{0}',
                        'pol_id': {1}
                    }}""".format(cas, pol_id)
            # Double {} so that .format ignores them
        }

    def generar_r101(self, cursor, uid, ids, pol_ids, context=None):
        if context is None:
            context = {}
        if len(context.get('active_ids', [])) > 1:  # Old wizard for R1006
            return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'wizard.r101.from.multiple.contracts',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': "{{'contract_id': {0}, 'contract_ids': {1}}}".format(
                    context.get('active_id', 0), context.get('active_ids', [])
                )
            }
        else:  # New wizard, only works with 1 contract
            return {
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'wizard.create.r1',
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': "{{'polissa_id': {0}}}".format(
                    context.get('active_id', 0)
                )
            }

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'multipol': fields.boolean('Multipol'),
        'info': fields.text('Info'),
        'te_esborranys': fields.boolean('Te Esborranys'),
        'te_actius': fields.boolean('Te Actius'),
        'te_autolectures': fields.boolean('Te Autolectures'),
        'casos_generats': fields.text('Casos generats'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multipol': _get_multipol,
        'info': _get_info_default,
        'te_esborranys': _te_esborranys,
        'te_actius': _te_actives,
        'whereiam': _whereiam,
        'te_autolectures': _te_autolectures,
        'casos_generats': lambda *a: json.dumps([]),
    }

GiscedataSwitchingPolissaC101Wizard()


class GiscedataSwitchingPasWizard(osv.osv_memory):
    """Wizard per gestionar els passos des del seu llistat"""
    _name = "giscedata.switching.pas.wizard"

    def _get_multicas(self, cr, uid, context=None):
        if not context:
            context = {}
        return len(context['active_ids']) > 1

    def _get_model(self, cr, uid, context=None):
        """ Agafa el model del context """
        if not context:
            context = {}
        return context.get('step_model', False)

    def fes_accio(self, cr, uid, ids, context=None):
        """ Fa l'acció seleccionada"""
        if not context:
            context = {}
        wizard = self.browse(cr, uid, ids[0], context)

        passos_ids = context.get('active_ids', [])
        if not wizard.multicas:
            passos_ids = [context.get('active_id')]
        if not wizard.accio or wizard.accio == 'casos':
            # obre llistat dels casos
            res = self.accio_mostrar_cas(cr, uid, passos_ids, wizard.model,
                                         context=context)
            return res

    def get_casos(self, cr, uid, passos_ids, model, context=None):
        """Retorna llista amb el ids dels casos a partir del pas i el model"""
        if not model:
            raise osv.except_osv(_(u'Avís'),
                                 _(u"No s'ha detectat el model correctament."))
            return False
        passos = self.pool.get(model).browse(cr, uid, passos_ids)
        return [p.sw_id.id for p in passos]

    def accio_mostrar_cas(self, cr, uid, passos_ids, model, context=None):
        """Llista els casos relacionats amb cada pas en una finestra"""
        casos_ids = self.get_casos(cr, uid, passos_ids, model, context=context)
        return {
            'name': _(u'Casos dels Passos %s') % context.get('descripcio', ''),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'domain': "[('id', 'in', %s)]" % casos_ids or False,
        }

    _columns = {
        'state': fields.char('State', size=16),
        'multicas': fields.boolean('Multicas'),
        'accio': fields.selection([('casos', 'Llistat casos')], 'Acció'),
        'model': fields.char('Model', size=32),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multicas': _get_multicas,
        'model': _get_model,
        'accio': lambda *a: 'casos',
    }

GiscedataSwitchingPasWizard()
