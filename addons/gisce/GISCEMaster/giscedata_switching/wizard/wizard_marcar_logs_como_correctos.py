# -*- encoding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class WizardMarcarComoCorrectoLogs(osv.osv_memory):
    _name = 'wizard.giscedata.switching.set.logs.correcte'

    def set_logs_to_correct(self, cursor, uid, ids, context=None):
        if context is None:
            raise osv.except_osv('Error', _('Context és None'))
        log_ids = context.get('active_ids', False)

        if not log_ids:
            raise osv.except_osv('Error', _('No s\'han seleccionat ids'))
        log_obj = self.pool.get('giscedata.switching.log')

        update_log_dict = {
            'status': 'correcte'
        }

        log_obj.write(cursor, uid, log_ids, update_log_dict, context=context)
        self.write(cursor, uid, ids, {'state': 'end'}, context=context)

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'msg': fields.char('Missatge', size=25)
    }

    _defaults = {
        'state': lambda *a: 'init',
        'msg': 'Logs canviats a correctes'
    }

WizardMarcarComoCorrectoLogs()