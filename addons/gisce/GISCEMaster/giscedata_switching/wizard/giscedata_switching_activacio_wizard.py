# -*- coding: utf-8 -*-
from osv import osv, fields, orm
from osv.osv import TransactionExecute
from tools.translate import _
from giscedata_switching.giscedata_switching_helpers import GiscedataSwitchingException


class GiscedataSwitchingPolissaActivacioWizard(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.switching.polissa.activacio.wizard'

    def _get_default_origen(self, cursor, uid, context=None):
        if not context:
            context = {}
        sw_obj = self.pool.get('giscedata.switching')

        sw_id = context.get('active_id', False)

        where = sw_obj.read(
            cursor, uid, sw_id, ['whereiam'], context=context
        )['whereiam']

        return where

    def _get_multipol(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context['active_ids']) > 1

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        return _(u"Només de les pólisses que:\n+"
                 u" * No existeixen per casos A3-01 (distribuïdora)"
                 u" * Estan en esborrany per A3/C1/C2 (pas 05/07)\n"
                 u" * Estan actives per D1-01, B1-05, C1-06, C2-06 i M1-05\n"
                 u" * No són M1 de canvi de titular\n"
                 u" * No tenen cap altre cas creat de gestió ATR\n"
                 u" * Estan a punt per ser activades (C1/C2 05)\n"
                 u" * Tenen les últimes factures generades (C1-06 i B1-05)\n"
                )

    def action_activar_a_polissa(self, cursor, uid, ids, context=None):
        """Activa les modificacions a la pólissa i/o activa pólissa
        """
        sw_obj = self.pool.get('giscedata.switching')

        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        if wizard.whereiam == 'distri':
            context.update({'days_before': wizard.days_before})

        sw_ids = context.get('active_ids', [])
        if not wizard.multipol:
            sw_ids = [context.get('active_id')]

        info = ''
        for sw_id in sw_ids:
            try:
                res = sw_obj.activa_polissa_cas_atr(cursor, uid, sw_id, context=context)
            except GiscedataSwitchingException as e:
                res = e.type_error, e.message

            info += "%s: %s\n\n" % (res[0], res[1])

        wizard.write({'info': info, 'state': 'done'})

        return

    _columns = {
        'state': fields.char('State', size=16),
        'multipol': fields.boolean('Multipol'),
        'info': fields.text('Info'),
        'days_before': fields.integer(
            'Lectures desde (Dies)', help=u'Es buscara la última data de lectures '
                                   u'entre la data actual i X dies abans'
        ),
        'whereiam': fields.selection(
            [('comer', 'Comercializadora'),
             ('distri', 'Distribuidora')],
            'Origen'
        )
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multipol': _get_multipol,
        'info': _get_info_default,
        'whereiam': _get_default_origen,
        'days_before': lambda *a: 1,
    }

GiscedataSwitchingPolissaActivacioWizard()