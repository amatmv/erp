# -*- coding: utf-8 -*-
from datetime import datetime
from gestionatr.defs import *
from gestionatr.input.messages.R1 import get_minimum_fields
from osv import osv, fields, orm
from tools.translate import _
import xml.etree.ElementTree as ET

# This dictionari translate the field names of the R1 from gestionatr to the
# names used in the R1 from ERP
# The names used in gestionatr are the names used by the CNMC in the file about
# "Campos Minimos" with underscores format
R1_TO_ERP = {
    'nif_cliente': ['tipus_document', 'codi_document'],
    'nombre_cliente': ['nom', 'cognom_1', 'cognom_2', 'ind_direccio_fiscal', 'fiscal_address_id'],
    'telefono_contacto': ['telefon', 'prefix', 'reclamacio_cont_nom', 'reclamacio_cont_prefix', 'reclamacio_cont_telefon'],
    'cups': [],  # The sw case will generate it
    'fecha_incidente': ['reclamacio_data_incident'],
    'comentarios': ['comentaris'],
    'persona_de_contacto': ['reclamacio_cont_nom',
                            'reclamacio_cont_prefix',
                            'reclamacio_cont_telefon',
                            'reclamacio_cont_email'],
    'tipo_atencion_incorrecta': ['reclamacio_tipus_atencio_incorrecte'],
    'codigo_incidencia': ['reclamacio_codi_incidencia'],
    'importe_reclamado': ['reclamacio_import_reclamat'],
    'num_fact': ['reclamacio_num_factura'],
    'tipo_concepto_facturado': ['reclamacio_tipus_concepte_facturat'],
    'codigo_de_solicitud': ['reclamacio_codi_sollicitud'],
    'cta_banco': ['reclamacio_iban'],
    'sol_nuevos_suministro': ['reclamacio_num_expedient_escomesa'],
    'fecha_desde': ['reclamacio_data_inici'],
    'fecha_hasta': ['reclamacio_data_fins'],
    'cod_reclam_anterior': ['reclamacio_codi_sollicitud_reclamacio'],
    'ubicacion_incidencia': ['reclamacio_desc_ubicacio', 'reclamacio_provincia',
                             'reclamacio_municipi'],
    'concepto_contratacion': ['reclamacio_parametre_contractacio'],
    'lectura': ['MEASURE_GROUP', 'measure_codi_dh'],  # Use a predefined view group
    'fecha_de_lectura': ['measure_data_lectura'],
    'numero_expediente_fraude': [],
    'num_factura': ['reclamacio_num_factura']
}


class WizardCreateR1(osv.osv_memory):
    """Wizard to generate R1."""
    _name = 'wizard.create.r1'

    def _get_default_contract(self, cursor, uid, context=None):
        if context is None:
            context = {}
        if context.get("from_model", False) == "giscedata.switching":
            sw_obj = self.pool.get("giscedata.switching")
            sw_id = context.get('active_id', False)
            pol_id = sw_obj.read(
                cursor, uid, sw_id, ['cups_polissa_id'], context=context
            )['cups_polissa_id']
            return pol_id and pol_id[0] or False
        else:
            return context.get('polissa_id')

    def _default_subtipus(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('subtipus_id', False)

    def onchange_subtipus(self, cr, uid, ids, subtipus, context=None):
        if not context:
            context = {}
        if not subtipus:
            return {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, subtipus, ['type'])
        data = {
            'tipus': subinfo['type']
        }
        return {'value': data}

    def onchange_polissa(self, cursor, uid, ids, polissa_id, context=None):
        res = {'warning': {}}
        sw_obj = self.pool.get("giscedata.switching")
        proces_obj = self.pool.get("giscedata.switching.proces")
        proces_r1_id = proces_obj.search(cursor, uid, [('name', '=', 'R1')])[0]
        r1_oberts = sw_obj.search(
            cursor, uid, [('proces_id', '=', proces_r1_id),
                          ('cups_polissa_id', '=', polissa_id),
                          ('state', '!=', 'done')]
        )
        if r1_oberts:
            pol_obj = self.pool.get("giscedata.polissa")
            info = pol_obj.read(cursor, uid, polissa_id, ['name'], context=context)
            res['warning'] = {
                'title': _(u"Avís"),
                'message': _(u"Aquest contracte ({0}) ja te reclamacions "
                             u"obertes amb ids {1}.").format(info['name'], r1_oberts)
            }
        return res

    def action_subtype_fields_view(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0])
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.subtype.r1',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': "{{ "
                       "'subtipus_id':{0}, "
                       "'polissa_id': {1} "
                       "}}".format(wizard.subtipus_id.id, wizard.polissa_id.id)
        }

    _columns = {
        'tipus': fields.selection(TABLA_81, u"Tipus", required=True,
                                  help=u"El tipus s'emplena automàticament "
                                       u"al seleccionar un subtipus"),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipus", required=True),
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True),
    }

    _defaults = {
        'polissa_id': _get_default_contract,
        'subtipus_id': _default_subtipus,
    }


WizardCreateR1()


class WizardSubtypeR1(osv.osv_memory):
    """Wizard to generate R1."""
    _name = 'wizard.subtype.r1'

    BASE_VIEW = """<form string="Crear R1">
                    <notebook>
                        <page string="Dades Sol.licitud">
                            <separator string="Tipus Sol.licitud" colspan="4"/>
                            <field name="tipus" readonly="1"/>
                            <field name="subtipus_id" readonly="1"/>
                            <separator string="Dades Administratives" colspan="4"/>
                            <field name="reforigen"/>
                            <newline/>
                            <field name="prioritari"/>
                            <field name="data_limit"/>
                            <newline/>
                            <separator string="Reclamant" colspan="4"/>
                            <field name="tipus_reclamant" colspan="4"/>
                        </page>
                        <page string="Reclamant" attrs="{'invisible':[('tipus_reclamant','=','06')]}">
                            <field name="rec_persona" colspan="1"/>
                            <separator colspan="4"/>
                            <group string="Dades Reclamant" colspan="4">
                                <field name="rec_codi_document"/>
                                <field name="rec_tipus_document"/>
                                <field name="rec_nom" colspan="4"/>
                                <group attrs="{'invisible':[('rec_persona','!=','F')]}" colspan="4" col="4">
                                    <field name="rec_cognom_1"/>
                                    <field name="rec_cognom_2"/>
                                </group>
                            </group>
                            <separator colspan="4"/>
                            <group string="Contacte Reclamant" colspan="4">
                                <field name="rec_telefon"/>
                                <field name="rec_prefix"/>
                                <field name="rec_email" colspan="4"/>
                            </group>
                        </page>
                    </notebook>
                    <group colspan="4" col="4">
                        <button icon="gtk-ok" name="action_create_r1_case" string="Crear Cas" type="object"/>
                        <button special="cancel" string="Tancar" icon="gtk-no"/>
                    </group>
                </form>"""

    MEASURE_GROUP = """<group colspan="4">
                            <group col="5" colspan="4">
                                <field name="measure_tariff_name" readonly="True" nolabel="1"/>
                                <field name="measure_num_periods" invisible="1"/>
                                <group colspan="1">
                                    <field string="Activa" name="measure_ae_send"/>
                                </group>
                                <group colspan="1">
                                    <field string="Reactiva" name="measure_r1_send"/>
                                </group>
                                <group colspan="1">
                                    <field string="Maxímetre" name="measure_pm_send"/>
                                </group>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field string="Excés" name="measure_ep_send"/>
                                </group>
                            </group>
                            <group col="5" colspan="4">
                                <field name="measure_ae_p1" string="Lectura P1"/>
                                <field name="measure_r1_p1" nolabel="1"/>
                                <field name="measure_pm_p1" nolabel="1"/>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field name="measure_ep_p1" nolabel="1"/>
                                </group>
                            </group>
                            <group col="5" colspan="4" attrs="{'invisible': [('measure_num_periods', '&lt;', 2)]}">
                                <field name="measure_ae_p2" string="Lectura P2"/>
                                <field name="measure_r1_p2" nolabel="1"/>
                                <field name="measure_pm_p2" nolabel="1"/>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field name="measure_ep_p2" nolabel="1"/>
                                </group>
                            </group>
                            <group col="5" colspan="4" attrs="{'invisible': [('measure_num_periods', '&lt;', 3)]}">
                                <field name="measure_ae_p3" string="Lectura P3"/>
                                <field name="measure_r1_p3" nolabel="1"/>
                                <field name="measure_pm_p3" nolabel="1"/>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field name="measure_ep_p3" nolabel="1"/>
                                </group>
                            </group>
                            <group col="5" colspan="4" attrs="{'invisible': [('measure_num_periods', '&lt;', 4)]}">
                                <field name="measure_ae_p4" string="Lectura P4"/>
                                <field name="measure_r1_p4" nolabel="1"/>
                                <field name="measure_pm_p4" nolabel="1"/>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field name="measure_ep_p4" nolabel="1"/>
                                </group>
                            </group>
                            <group col="5" colspan="4" attrs="{'invisible': [('measure_num_periods', '&lt;', 5)]}">
                                <field name="measure_ae_p5" string="Lectura P5"/>
                                <field name="measure_r1_p5" nolabel="1"/>
                                <field name="measure_pm_p5" nolabel="1"/>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field name="measure_ep_p5" nolabel="1"/>
                                </group>
                            </group>
                            <group col="5" colspan="4" attrs="{'invisible': [('measure_num_periods', '&lt;', 6)]}">
                                <field name="measure_ae_p6" string="Lectura P6"/>
                                <field name="measure_r1_p6" nolabel="1"/>
                                <field name="measure_pm_p6" nolabel="1"/>
                                <group attrs="{'invisible': [('measure_num_periods', '&lt;', '6')]}">
                                    <field name="measure_ep_p6" nolabel="1"/>
                                </group>
                            </group>
                        </group>"""

    def fields_view_get(self, cursor, uid, view_id=None, view_type='form',
                        context=None, toolbar=False):
        res = super(WizardSubtypeR1, self).fields_view_get(
            cursor, uid, view_id, view_type, context=context, toolbar=toolbar
        )
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cursor, uid, context.get('subtipus_id'))

        root = ET.fromstring(self.BASE_VIEW)
        notebook = root.find("notebook")

        for min_field in get_minimum_fields(subtipus.name):
            current_page = None
            needed_fields = []
            for field in R1_TO_ERP[min_field]:
                # If ends with '*', add all fields which starts with field
                if field.endswith("*"):
                    needed_fields.extend([x for x in self._columns.keys() if x.startswith(field[:-1])])
                else:
                    needed_fields.append(field)
            for needed_field in needed_fields:
                # Search for the page where fiels will be added
                # If the page doesn't exist we add it
                current_page = None
                field_page = self.get_page_of_field(needed_field.lower())
                for page in notebook.findall("page"):
                    if page.get("string") == field_page:
                        current_page = page
                if not current_page:
                    ET.SubElement(notebook, "page", {'string': field_page})
                    current_page = notebook.findall("page")[-1]

                if needed_field.endswith("_GROUP"):
                    # Parse and add group
                    group = ET.fromstring(getattr(self, needed_field))
                    current_page.append(group)
                else:
                    # Add the field to the page
                    attrs = {'name': needed_field}
                    if needed_field.endswith("_ids"):
                        attrs.update({'widget': "one2many_list"})
                    if needed_field == 'reclamacio_num_factura':
                        attrs.update({'nolabel': "1", 'colspan': "4"})
                    if needed_field not in ['cognom_1', 'cognom_2',
                                            'reclamacio_cont_email']:
                        attrs.update({'required': "1"})
                    ET.SubElement(current_page, "field", attrs)
        # For some reason, the many2many fields are not added to 'fields'
        # as the other fields, so we add it
        domain = [('type', 'in', ['in_invoice', 'in_refund'])]
        if context:
            pol_id = context.get('polissa_id')
            if pol_id:
                domain.append(('polissa_id', '=', pol_id))

        res['fields'].update({
            'reclamacio_num_factura': {'domain': domain, 'relation': 'giscedata.facturacio.factura', 'string': u'Factures per reclamar', 'context': '', 'views': {}, 'type': 'many2many'}
        })
        res['arch'] = ET.tostring(root)
        return res

    def default_values_from_contract(self, cursor, uid, polissa_id, context=None):
        pol_obj = self.pool.get("giscedata.polissa")
        polissa = pol_obj.browse(cursor, uid, polissa_id)

        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cursor, uid, context.get('subtipus_id'))
        res = {}
        default_values = {
            'measure_tariff_name': polissa.tarifa and polissa.tarifa.name,
            'measure_num_periods': polissa.tarifa and
                                   polissa.tarifa.get_num_periodes(
                                       context={'agrupar_periodes': False}
                                   ),
        }
        default_values.update(
            self.default_client_values(cursor, uid, polissa, context))
        default_values.update(
            self.default_reclamacio_values(cursor, uid, polissa, context))
        default_values.update(
            self.default_comment_values(cursor, uid, polissa, context))
        default_values.update(
            self.default_measures_values(cursor, uid, polissa, context))

        needed_fields = []
        for min_field in get_minimum_fields(subtipus.name):
            for field in R1_TO_ERP[min_field]:
                if field == "MEASURE_GROUP":
                    # We want all measure files
                    field = "measure*"
                if field.endswith("*"):
                    needed_fields.extend([x for x in self._columns.keys()
                                          if x.startswith(field[:-1])])
                else:
                    needed_fields.append(field)

        # Get default value for each field
        for field_name in needed_fields:
            if default_values.get(field_name, False):
                res.update({field_name: default_values.get(field_name)})
        return res

    def default_get(self, cursor, uid, fields, context=None):
        if context is None:
            context = {}

        res = super(WizardSubtypeR1, self).default_get(
            cursor, uid, fields, context
        )

        # For some reason 'default_get' it's called two times: one when wizard
        # is created (correct) and another when action_create_r1_case is called
        # (why?). This second call makes that all data introduced by user is
        # overwrited by default values, so we have to check if this is the
        # second time that default_get is called and avoid overwriting these
        # values. In the first call we get all fields in "fields" and in the
        # second we only recive "subtipus_id" and "tipus_id". This is why we
        # check if there is "polissa_id" in fields.
        polissa_id = context.get("polissa_id", False)
        if not polissa_id:
            polissa_id = res.get("polissa_id", False)
        if not polissa_id:
            return res
        dvals = self.default_values_from_contract(
            cursor, uid, polissa_id, context=context
        )
        for f in fields:
            if not f in res and f in dvals:
                res[f] = dvals.get(f)
        return res

    def default_client_values(self, cr, uid, polissa, context=None):
        if not polissa.titular:
            return {}
        part_obj = self.pool.get("res.partner")
        res = {
            'tipus_document': polissa.titular.get_vat_type(),
            'codi_document': polissa.titular.vat[2:],
            'persona': 'J',
            'nom': polissa.titular.name,
            'prefix': '34',
            'ind_direccio_fiscal': 'S',
        }
        titular_addres = polissa.get_address_with_phone()
        if titular_addres:
            res.update({
                'telefon': titular_addres.phone or titular_addres.mobile,
                'fiscal_address_id': titular_addres.id,
            })
            conf_obj = self.pool.get('res.config')
            conf_var = 'sw_r1_from_invoice_mail_add'
            invoice_mail_add = bool(int(conf_obj.get(cr, uid, conf_var, '0')))
            if invoice_mail_add:
                res.update({'email': titular_addres.email or None})
        if not polissa.titular.has_enterprise_vat():
            names_dict = part_obj.separa_cognoms(cr, uid, polissa.titular.name)
            res.update({
                'persona': 'F',
                'nom': names_dict['nom'],
                'cognom_1': names_dict['cognoms'][0],
                'cognom_2': names_dict['cognoms'][1],
            })
        return res

    def default_reclamacio_values(self, cr, uid, polissa, context=None):
        res = {
            'reclamacio_data_incident': datetime.today().strftime("%Y-%m-%d"),
            'reclamacio_iban': polissa.bank and polissa.bank.iban,
            'reclamacio_provincia': polissa.cups.id_provincia.id,
            'reclamacio_municipi': polissa.cups.id_municipi.id,
        }
        if polissa.direccio_notificacio:
            addres = polissa.direccio_notificacio
            phone = addres.phone or addres.mobile
            if not phone:
                addres = polissa.get_address_with_phone()
                phone = addres.phone or addres.mobile
            res.update({
                'reclamacio_cont_nom': addres.partner_id.name,
                'reclamacio_cont_prefix': '34',
                'reclamacio_cont_telefon': phone,
            })

        # Search if there is any non R1 opened case with same contract
        sw_obj = self.pool.get("giscedata.switching")
        sw_id = sw_obj.search(cr, uid,
                              [('cups_polissa_id', '=', polissa.id),
                               ('proces_id.name', '!=', 'R1'),
                               ('state', 'in', ['open', 'pending'])], limit=1, order="date")
        if not sw_id:
            sw_id = sw_obj.search(cr, uid,
                                  [('cups_polissa_id', '=', polissa.id),
                                   ('proces_id.name', '!=', 'R1')], limit=1, order="date")
        if sw_id:
            sw = sw_obj.browse(cr, uid, sw_id[0])
            res.update({
                'reclamacio_codi_sollicitud': sw.codi_sollicitud
            })

        # Search the last R1 with same contract
        sw_id = sw_obj.search(cr, uid,
                              [('cups_polissa_id', '=', polissa.id),
                               ('proces_id.name', '=', 'R1'),
                               ('state', 'in', ['open', 'pending'])], limit=1, order="date")
        if not sw_id:
            sw_id = sw_obj.search(cr, uid,
                                  [('cups_polissa_id', '=', polissa.id),
                                   ('proces_id.name', '=', 'R1')], limit=1, order="date")
        if sw_id:
            sw = sw_obj.browse(cr, uid, sw_id[0])
            res.update({
                'reclamacio_codi_sollicitud_reclamacio': sw.codi_sollicitud
            })

        facts = context.get('reclamacio_num_factura', False)
        if facts:
            res['reclamacio_num_factura'] = facts[str(polissa.id)]

        return res

    def default_comment_values(self, cr, uid, polissa, context=None):
        if not context or not context.get("subtipus_id"):
            return {}

        res = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cr, uid, context.get('subtipus_id'))
        if subtipus.name == "006":
            ultima_lect = None
            if polissa.comptadors:
                comptador_id = polissa.comptadors[0].id
                lect_pool_obj = self.pool.get('giscedata.lectures.lectura.pool')

                orig_obj = self.pool.get('giscedata.lectures.origen_comer')
                ov_orig_ids = orig_obj.search(cr, uid, [('codi', '=', 'F1')])

                search_params = [
                    ('comptador', '=', comptador_id),
                    ('origen_comer_id', 'in', ov_orig_ids)
                ]
                lectura_ids = lect_pool_obj.search(
                    cr, uid, search_params, limit=1, order='name DESC'
                )
                ultima_lectura_id = lectura_ids[0] if lectura_ids else []
                if ultima_lectura_id:
                    ultima_lect = lect_pool_obj.read(
                        cr, uid, ultima_lectura_id, ['name']
                    )['name']
                else:
                    ultima_lect = polissa.data_alta

            if ultima_lect:
                comment_tmpl = 'De este suministro no tenemos lecturas ' \
                               'desde {0}.\nEl Real Decreto 1718/2012 indica ' \
                               'claramente la obligación de tomar una ' \
                               'lectura real cada dos meses. Solicitamos ' \
                               'lecturas reales o, en caso contrario, una ' \
                               'copia del aviso de imposible lectura  ' \
                               'conforme no ha sido posible tomarla. \nGracias.'
                comment = comment_tmpl.format(ultima_lect)
                res.update({'comentaris': comment})
        elif subtipus.name == "029":
            res.update({'comentaris': self.generar_comentari_r029(cr, uid, polissa, context=context)})
        return res

    def generar_comentari_r029(self, cr, uid, polissa_br, context=None):
        if context is None:
            context = {}

        res = ""
        # Search the last R1 with same contract
        sw_obj = self.pool.get("giscedata.switching")
        sw_id = sw_obj.search(
            cr, uid, [
                ('cups_polissa_id', '=', polissa_br.id),
                ('proces_id.name', '=', 'R1'),
                ('state', 'in', ['open', 'pending'])
            ], limit=1, order="date"
        )
        if not sw_id:
            sw_id = sw_obj.search(
                cr, uid, [
                    ('cups_polissa_id', '=', polissa_br.id),
                    ('proces_id.name', '=', 'R1'),
                ], limit=1, order="date"
            )
        if sw_id:
            pas = None
            sw = sw_obj.browse(cr, uid, sw_id[0])
            for step_rel_id in sw.step_ids:
                if step_rel_id.step_id.name == '02':
                    pas = sw.get_pas(step_id=step_rel_id.step_id.id)
                    break
            if pas:
                res = u"En fecha {0} recibimos aceptación de la R con código de solicitud {1}. " \
                      u"Habiendo transcurrido ya los plazos previstos por la " \
                      u"normativa aplicable, solicitamos resuelvan la " \
                      u"reclamación a la mayor brevedad. Nos reservamos las " \
                      u"acciones legales que nos pudieran corresponden a fin " \
                      u"de salvaguardar nuestros derechos y el del cliente " \
                      u"afectado.".format(pas.data_acceptacio, sw.codi_sollicitud)
        return res

    def default_measures_values(self, cr, uid, polissa, context=None):
        if not context or not context.get("subtipus_id"):
            return {}

        res = {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subtipus = subtipus_obj.browse(cr, uid, context.get('subtipus_id'))
        if subtipus.name == "036":
            info = self.get_self_measures_info(cr, uid, polissa, context=context)
            if not info['measure_date']:
                return res

            res['measure_data_lectura'] = str(info['measure_date'])

            for period, magnitud, value in info['measures']:
                field_name = 'measure_{}_{}'.format(
                    magnitud.lower(),
                    period.lower()
                )
                res[field_name] = value
                field_name = 'measure_{}_send'.format(magnitud.lower())
                res[field_name] = True

            # num periods
            res['measure_num_periods'] = polissa.tarifa.get_num_periodes(
                context={'agrupar_periodes': False}
            )
            res['measure_codi_dh'] = polissa.tarifa.get_codi_dh()
            res['measure_tariff_name'] = polissa.tarifa.name

        return res

    def get_self_measures_info(self, cursor, uid, polissa, context=None):
        swmeasures_obj = self.pool.get('giscedata.switching.lectura')
        energy_ids, maximeters_ids = polissa.get_last_self_measure_vals()
        sw_measures_tuple = swmeasures_obj.get_measures_list(
            cursor, uid, energy_ids, maximeters_ids, pool=True,
            context=context
        )
        return {'measures': sw_measures_tuple[2], 'measure_date': sw_measures_tuple[0]}

    @staticmethod
    def get_page_of_field(field_name):
        if field_name.startswith("reclamacio_"):
            return _(u"Detalls Reclamacio")
        elif field_name.startswith("rec_"):
            return _(u"Reclamant")
        elif field_name == 'comentaris':
            return _(u"Comentaris")
        elif field_name.startswith("measure_"):
            return _(u"Lectures")
        else:
            return _(u"Client")

    def _get_default_tipus(self, cr, uid, context=None):
        if not context or not context.get("subtipus_id"):
            return 0
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(cr, uid, context.get('subtipus_id'), ['type'])
        return subinfo['type']

    def action_create_r1_case_from_dict(self, cursor, uid, polissa_id, dict_fields, context=None):
        if context is None:
            context = {}
        contract_obj = self.pool.get('giscedata.polissa')
        tel_obj = self.pool.get('giscedata.switching.telefon')
        res = contract_obj.crear_cas_atr(cursor, uid, polissa_id, proces='R1')
        if not isinstance(res[-1], long):
            # We don't have the sw_id, something went wrong
            raise osv.except_osv(u"ERROR", res[:-1])

        sw_obj = self.pool.get("giscedata.switching")
        r101 = sw_obj.get_pas(cursor, uid, [res[-1]])

        main_values = {}
        llista_reclamacions = []
        reclamacio_values = {}
        lectures_values = {}
        dicts_factures = []
        for field, value in dict_fields.items():
            if not value or field in ['id', 'polissa_id']:
                continue
            # If field is reclamacio_num_factura we will check all invoices
            if field == "reclamacio_num_factura":
                fact_obj = self.pool.get('giscedata.facturacio.factura')
                for factura in fact_obj.read(cursor, uid, value, ['origin'],
                                             context=context):
                    dicts_factures.append({field[11:]: factura['origin']})

            elif field.startswith("reclamacio_"):
                reclamacio_values.update({field[11:]: value})
            elif field.startswith("measure_"):
                lectures_values.update({field: value})
            else:
                main_values.update({field: value})

        # Create measures
        if lectures_values.items():
            measures_obj = self.pool.get("giscedata.switching.lectura")
            lect_ids = measures_obj.create_lects(
                cursor, uid, self.get_measures_vals(lectures_values), context
            )
            if lectures_values.get('measure_data_lectura', False):
                reclamacio_values.update({
                    'data_lectura': lectures_values.get('measure_data_lectura')
                })
            if lectures_values.get('measure_codi_dh', False):
                reclamacio_values.update({
                    'codi_dh': lectures_values.get('measure_codi_dh', False),
                })
            if lect_ids:
                reclamacio_values.update({
                    'lect_ids': [(6, 0, lect_ids)]
                })

        # Create reclamacio
        reclama_obj = self.pool.get("giscedata.switching.reclamacio")
        if reclamacio_values.items():
            if 'cont_telefon' in reclamacio_values:
                tel_id = tel_obj.create(cursor, uid, {
                    'numero': reclamacio_values.get('cont_telefon'),
                    'prefix': reclamacio_values.get('cont_prefix', '34')
                })
                reclamacio_values.update({
                    'cont_telefons': [(6, 0, [tel_id])]
                })
        if not len(dicts_factures):
            rec_id = reclama_obj.create(cursor, uid, reclamacio_values,
                                        context=context)
            llista_reclamacions.append(rec_id)
        else:
            for dict_f in dicts_factures:
                dict_f.update(reclamacio_values)
                rec_id = reclama_obj.create(cursor, uid, dict_f, context=context)
                llista_reclamacions.append(rec_id)
        main_values.update({'reclamacio_ids': [(6, 0, llista_reclamacions)]})

        if 'telefon' in main_values:
            tel_id = tel_obj.create(cursor, uid, {
                'numero': main_values.get('telefon'),
                'prefix': main_values.get('prefix', '34')
            })
            main_values.update({
                'telefons': [(6, 0, [tel_id])]
            })
        if 'rec_telefon' in main_values:
            tel_id = tel_obj.create(cursor, uid, {
                'numero': main_values.get('rec_telefon'),
                'prefix': main_values.get('rec_prefix', '34')
            })
            main_values.update({
                'rec_telefons': [(6, 0, [tel_id])]
            })

        if context.get("extra_r1_values"):
            main_values.update(context.get("extra_r1_values", {}))

        dict_to_config = {'r1_values': main_values}
        if context.get("extra_values"):
            dict_to_config.update(context.get("extra_values", {}))

        r101.config_step(dict_to_config, context=context)
        # Update additional info
        sw_obj.write(cursor, uid, res[-1],
                     {'additional_info': r101.get_additional_info()})
        return res

    def action_create_r1_case(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}

        # {'pol_id': fact_id}
        reclamacions_fact = context.get('reclamacio_num_factura', {})

        wiz_values = self.read(cursor, uid, ids[0], [])[0]

        polissa_id = wiz_values['polissa_id']

        if reclamacions_fact:
            wiz_values.update(
                {"reclamacio_num_factura": reclamacions_fact[str(polissa_id)]}
            )

        res = self.action_create_r1_case_from_dict(
            cursor, uid, wiz_values['polissa_id'], wiz_values, context=context
        )

        return {
            'domain': [('id', '=', res[-1])],
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    @staticmethod
    def get_measures_vals(measures_dict):
        """
        measures_dict: dictionari with fields about measures from wizard.

        :return: List of tuples in the form
                (periode(Pn),
                {'AE','Rn','PM','EP '},
                measure)
        """

        measures = []
        num_periods = measures_dict['measure_num_periods']
        magnitude_list = ['AE', 'R1', 'PM', 'EP']
        for magnitude in magnitude_list:
            # test if the magnitude measures must be added
            field_name = 'measure_{}_send'.format(magnitude.lower())
            if measures_dict.get(field_name, False):
                # Add the magnitude measures
                for period in range(1, num_periods + 1):
                    field_name = 'measure_{}_p{}'.format(
                        magnitude.lower(),
                        period
                    )
                    measures.append(
                        (
                            'P{}'.format(period),
                            magnitude,
                            measures_dict.get(field_name)
                        )
                    )
        return measures

    _columns = {
        'polissa_id': fields.many2one('giscedata.polissa', 'Pòlissa',
                                      required=True),
        # DatosSolicitud
        'tipus': fields.selection(TABLA_81, u"Tipus", required=True),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipus", required=True),
        'reforigen': fields.char(u"Referència interna", size=35,
                                 help=u'Referència propia per seguiment '
                                      u'intern'),
        'data_limit': fields.date(u"Data Limit"),
        'prioritari': fields.selection(TABLA_26, u"Prioritari"),

        # Cliente
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2),
        'codi_document': fields.char(u'Codi document', size=11),
        'persona': fields.selection(PERSONA, u'Persona'),
        'nom': fields.char(u'Nom de la persona o societat', size=45),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9),
        'email': fields.char(u"Correu Electrònic", size=60),
        'ind_direccio_fiscal': fields.selection(TABLA_11,
                                                u"Indicador direcció"),
        'fiscal_address_id': fields.many2one('res.partner.address',
                                             u"Adreça fiscal"),

        'tipus_reclamant': fields.selection(TABLA_83, u"Tipus Reclamant",
                                            required=True),

        # Reclamante
        'rec_tipus_document': fields.selection(TIPUS_DOCUMENT,
                                               u'Document identificatiu',
                                               size=2),
        'rec_codi_document': fields.char(u'Codi document', size=11),
        'rec_persona': fields.selection(PERSONA, u'Persona'),
        'rec_nom': fields.char(u'Nom de la persona o societat', size=45),
        'rec_cognom_1': fields.char(u'Primer cognom', size=45,
                                    help=u"Únicament per a persones físiques"),
        'rec_cognom_2': fields.char(u'Segon cognom', size=45,
                                    help=u"Opcional per a persones físiques"),
        'rec_prefix': fields.char(u"Prefix telefònic", size=2),
        'rec_telefon': fields.char(u"Telèfon", size=9),
        'rec_email': fields.char(u"Correu Electrònic", size=60),

        # comentaris
        'comentaris': fields.text(u"Comentaris", size=4000, nolabel=True),

        # Camps de una VariableDetalleReclamacion
        'reclamacio_num_expedient_escomesa': fields.char(
            u'Núm. Expedient Escomesa', size=20
        ),
        'reclamacio_num_expedient_frau': fields.char(
            u'Núm. Expedient Frau', size=16
        ),
        'reclamacio_data_incident': fields.date(u"Data incident"),
        'reclamacio_num_factura': fields.many2many('giscedata.facturacio.factura',
                                    'factures_reclamacions_rel', 'wiz_id',
                                    'factura_id', 'Factures per reclamar'),
        'reclamacio_tipus_concepte_facturat': fields.selection(
            TABLA_77, u'Tipus concepte facturat'
        ),
        'reclamacio_codi_incidencia': fields.selection(
            [t[:2] for t in TABLA_86], u'Codi incidència'
        ),
        'reclamacio_codi_sollicitud': fields.char(
            u"Codi Sol·licitud", size=12,
            help=u"Codi de de Sol·licitud ATR objecte d'aquesta reclamació"
        ),
        'reclamacio_parametre_contractacio': fields.selection(
            TABLA_79, u"Paràmetre de contractació"
        ),
        'reclamacio_concepte_disconformitat': fields.text(
            u"Concepte de disconformitat", size=120
        ),
        'reclamacio_tipus_atencio_incorrecte': fields.selection(
            [t[:2] for t in TABLA_87], u'Tipus atenció incorrecte',
        ),
        'reclamacio_iban': fields.char(u'IBAN', size=34),
        'reclamacio_codi_sollicitud_reclamacio': fields.char(
            u"Codi Sol·licitud Reclamació Anterior", size=12,
            help=u"Codi de de Sol·licitud de la reclamació anterior "
                 u"relacionada"
        ),
        'reclamacio_data_inici': fields.date(u"Data Des de"),
        'reclamacio_data_fins': fields.date(u"Data Fins"),
        'reclamacio_import_reclamat': fields.float(u"Import Reclamat", digits=(12, 2)),

        # Contacte de la VariableDetalleReclamacion
        'reclamacio_cont_nom': fields.char(u'Nom de la persona o societat', size=150),
        'reclamacio_cont_prefix': fields.char(u"Prefix telefònic", size=2),
        'reclamacio_cont_telefon': fields.char(u"Telèfon", size=9),
        'reclamacio_cont_email': fields.char(u"Email", size=60),
        # Ubicacio de la VariableDetalleReclaamcio
        'reclamacio_desc_ubicacio': fields.char(u"Descripció Ubicació Incidència",
                                     size=45),
        'reclamacio_provincia': fields.many2one('res.country.state', u'Província'),
        'reclamacio_municipi': fields.many2one('res.municipi', u'Municipi'),
        # Lectures de la VariableDetalleReclamacion
        'measure_data_lectura': fields.date(u"Data lectura"),
        'measure_codi_dh': fields.selection(TIPO_DH_APARATO, 'Codi DH'),
        'measure_ae_send': fields.boolean(
            'Amb activa',
            help=u"Marcar per afegir les lectures d'Activa al cas R1"
        ),
        'measure_ae_p1': fields.integer('Lectura P1'),
        'measure_ae_p2': fields.integer('Lectura P2'),
        'measure_ae_p3': fields.integer('Lectura P3'),
        'measure_ae_p4': fields.integer('Lectura P4'),
        'measure_ae_p5': fields.integer('Lectura P5'),
        'measure_ae_p6': fields.integer('Lectura P6'),

        'measure_r1_send': fields.boolean(
            'Amb reactiva',
            help=u"Marcar per afegir les lectures de Reactiva al cas R1"
        ),
        'measure_r1_p1': fields.integer('Reactiva P1'),
        'measure_r1_p2': fields.integer('Reactiva P2'),
        'measure_r1_p3': fields.integer('Reactiva P3'),
        'measure_r1_p4': fields.integer('Reactiva P4'),
        'measure_r1_p5': fields.integer('Reactiva P5'),
        'measure_r1_p6': fields.integer('Reactiva P6'),

        'measure_pm_send': fields.boolean(
            'Amb maxímetre',
            help=u"Marcar per afegir les lectures de Maxímetre al cas R1"
        ),
        'measure_pm_p1': fields.integer('Maxímetre P1'),
        'measure_pm_p2': fields.integer('Maxímetre P2'),
        'measure_pm_p3': fields.integer('Maxímetre P3'),
        'measure_pm_p4': fields.integer('Maxímetre P4'),
        'measure_pm_p5': fields.integer('Maxímetre P5'),
        'measure_pm_p6': fields.integer('Maxímetre P6'),

        'measure_ep_send': fields.boolean(
            'Amb excés',
            help=u"Marcar per afegir les lectures d'Exés de potència al cas R1"
        ),
        'measure_ep_p1': fields.integer('Excés P1'),
        'measure_ep_p2': fields.integer('Excés P2'),
        'measure_ep_p3': fields.integer('Excés P3'),
        'measure_ep_p4': fields.integer('Excés P4'),
        'measure_ep_p5': fields.integer('Excés P5'),
        'measure_ep_p6': fields.integer('Excés P6'),
        # Support fields
        'measure_num_periods': fields.integer("Num. Períodes"),
        'measure_tariff_name': fields.char("Tarifa", size="16"),
    }

    _defaults = {
        'tipus': _get_default_tipus,
        'subtipus_id': lambda obj, cr, uid, ctx: ctx.get('subtipus_id'),
        'polissa_id': lambda obj, cr, uid, ctx: ctx.get('polissa_id'),
        'tipus_reclamant': lambda *a: '06',
        'rec_persona': 'F',
    }

WizardSubtypeR1()
