# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields, orm
from tools.translate import _

import json

from gestionatr.defs import TABLA_83, TIPO_DH_APARATO
from ..giscedata_switching_r1 import get_type_subtype


class GiscedataSwitchingWizardR101(osv.osv_memory):

    _name = "giscedata.switching.r101.wizard"

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Generació de R1-01 tipus 02-036 "
                u"(Reclamació Factura amb lectures) "
                u"o tipus 02-09 (Refacturació)")

        return txt

    def _get_proces_default(self, cursor, uid, context=None):
        if context is None:
            context = {}

        proces = context.get('proces', 'R1-02036')

        return proces

    def _get_invoice_id(self, cursor, uid, context=None):
        """
        Gets invoice_id from context
        :param context:
        :return:
            invoice_id
        """
        if context is None:
            context = {}

        invoice_id = context.get('invoice_id', None)

        return invoice_id

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def get_config_measures_vals(self, cursor, uid, ids, context=None):
        """
        Returns measures from wizard

        :return: List of tuples in the form
                (periode(Pn),
                {'AE','Rn','PM','EP '},
                measure)
        """
        wiz = self.browse(cursor, uid, ids[0], context=context)

        measures = []
        num_periods = wiz.num_periods
        magnitude_list = ['AE', 'R1', 'PM', 'EP']
        for magnitude in magnitude_list:
            # test if the magnitude measures must be added
            field_name = 'send_{}_measures'.format(magnitude.lower())
            if getattr(wiz, field_name):
                # Add the magnitude measures
                for period in range(1, num_periods + 1):
                    field_name = 'measure_{}_p{}'.format(
                        magnitude.lower(),
                        period
                    )
                    measures.append(
                        (
                            'P{}'.format(period),
                            magnitude,
                            getattr(wiz, field_name)
                        )
                    )
        return measures

    def get_config_vals(self, cursor, uid, ids, context=None):
        """
        Returns a dict from wizard data compatible with
        giscedata_switching_r1_01.config_step function
        'type': R1-type i.e. 02,
            'subtype': R1-subtype i.e 36,
            'objector': Who objects i.e 06 (comer),
            'invoice_number': invoice reference,
            'comments': comments on R1-01 step,
            'measure_date': Date of measures
            'codi_dh': CNMC CodigoDh code of measures
            'measures': List of tuples in the form
                (periode(Pn),
                {'AE','Rn','PM','EP '},
                measure)
        """
        if context is None:
            context = {}

        wiz = self.browse(cursor, uid, ids[0], context=context)

        proces = context.get('proces', 'R1-02036')

        r1_type, r1_subtype = get_type_subtype(proces)

        config_vals = {
            'type': r1_type,
            'subtype': r1_subtype,
        }
        # same name in wiz and dict
        wiz_fields = [
            'objector', 'invoice_number', 'comments', 'measure_date', 'dh_code'
        ]
        config_vals.update(wiz.read(wiz_fields, context=True)[0])
        config_vals.update(context.get("extra_values", {}))
        config_vals.pop('id')

        config_vals['measures'] = self.get_config_measures_vals(
            cursor, uid, ids, context=context
        )

        return config_vals

    def action_create_atr_case(self, cursor, uid, ids, context=None):
        """
        Creates the R1 case
        """
        inv_obj = self.pool.get('giscedata.facturacio.factura')

        wiz = self.browse(cursor, uid, ids[0], context=context)

        casos_ids = []
        info = _('Processada factura {}\n\n').format(wiz.invoice_number)

        config_vals = wiz.get_config_vals(context=context)

        res = inv_obj.create_atr_case(
            cursor, uid,
            wiz.invoice.id,
            proces='R1-02036',
            config_vals=config_vals,
            context=context
        )

        info += "{0}: {1}\n".format(res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        cases_json = json.dumps(casos_ids)
        wiz.write(
            {'info': info, 'state': 'done', 'generated_cases': cases_json}
        )

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.generated_cases
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', {0})]".format(str(casos_ids)),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def default_get(self, cursor, uid, fields, context=None):
        if context is None:
            context = {}

        res = super(GiscedataSwitchingWizardR101, self).default_get(
            cursor, uid, fields, context
        )

        invoice_id = self._get_invoice_id(cursor, uid, context=context)

        inv_obj = self.pool.get('giscedata.facturacio.factura')

        info = inv_obj.get_r101_invoicing_info(
            cursor, uid, invoice_id, context=context
        )

        new_res = {k: v for k, v in info.items()
                   if k in fields and v is not None
                   }

        if new_res.get('measure_date', False):
            new_res['measure_date'] = str(new_res['measure_date'])

        for period, magnitud, value in info['measures']:
            field_name = 'measure_{}_{}'.format(
                magnitud.lower(),
                period.lower()
            )
            if field_name in fields:
                new_res[field_name] = value

            field_name = 'send_{}_measures'.format(magnitud.lower())
            if field_name in fields:
                new_res[field_name] = True

        invoice = inv_obj.browse(cursor, uid, invoice_id, context=context)

        if 'num_periods' in fields:

            # num periods
            new_res['num_periods'] = invoice.tarifa_acces_id.get_num_periodes(
                context={'agrupar_periodes': False}
            )
        if 'tariff_name' in fields:
            new_res['tariff_name'] = invoice.tarifa_acces_id.name

        res.update(new_res)

        return res

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'proces': fields.char('Proces', size=9),
        'generated_cases': fields.text('Casos generats'),
        'invoice': fields.many2one('giscedata.facturacio.factura', 'Factura'),
        # R1-01 02-36 specific data
        'invoice_number': fields.char('Num. Factura', size=64, required=True),
        'objector': fields.selection(
            TABLA_83, u"Tipus Reclamant", required=True
        ),
        'measure_date': fields.date('Data lectures'),
        'dh_code': fields.selection(
            TIPO_DH_APARATO, 'Codi DH', size=64
        ),

        'send_ae_measures': fields.boolean(
            'Amb activa',
            help=u"Marcar per afegir les lectures d'Activa al cas R1"
        ),
        'measure_ae_p1': fields.integer('Lectura P1'),
        'measure_ae_p2': fields.integer('Lectura P2'),
        'measure_ae_p3': fields.integer('Lectura P3'),
        'measure_ae_p4': fields.integer('Lectura P4'),
        'measure_ae_p5': fields.integer('Lectura P5'),
        'measure_ae_p6': fields.integer('Lectura P6'),

        'send_r1_measures': fields.boolean(
            'Amb reactiva',
            help=u"Marcar per afegir les lectures de Reactiva al cas R1"
        ),
        'measure_r1_p1': fields.integer('Reactiva P1'),
        'measure_r1_p2': fields.integer('Reactiva P2'),
        'measure_r1_p3': fields.integer('Reactiva P3'),
        'measure_r1_p4': fields.integer('Reactiva P4'),
        'measure_r1_p5': fields.integer('Reactiva P5'),
        'measure_r1_p6': fields.integer('Reactiva P6'),

        'send_pm_measures': fields.boolean(
            'Amb maxímetre',
            help=u"Marcar per afegir les lectures de Maxímetre al cas R1"
        ),
        'measure_pm_p1': fields.integer('Maxímetre P1'),
        'measure_pm_p2': fields.integer('Maxímetre P2'),
        'measure_pm_p3': fields.integer('Maxímetre P3'),
        'measure_pm_p4': fields.integer('Maxímetre P4'),
        'measure_pm_p5': fields.integer('Maxímetre P5'),
        'measure_pm_p6': fields.integer('Maxímetre P6'),

        'send_ep_measures': fields.boolean(
            'Amb excés',
            help=u"Marcar per afegir les lectures d'Exés de potència al cas R1"
        ),
        'measure_ep_p1': fields.integer('Excés P1'),
        'measure_ep_p2': fields.integer('Excés P2'),
        'measure_ep_p3': fields.integer('Excés P3'),
        'measure_ep_p4': fields.integer('Excés P4'),
        'measure_ep_p5': fields.integer('Excés P5'),
        'measure_ep_p6': fields.integer('Excés P6'),

        'comments': fields.text('Comentaris', required=True),
        # support fields
        'num_periods': fields.integer("Num. Períodes"),
        'tariff_name': fields.char("Tarifa", size="16"),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_info_default,
        'proces': _get_proces_default,
        'whereiam': _whereiam,
        'casos_generats': lambda *a: json.dumps([]),
        'send_ae_measures': lambda *a: False,
        'send_r1_measures': lambda *a: False,
        'send_pm_measures': lambda *a: False,
        'send_ep_measures': lambda *a: False,
    }

GiscedataSwitchingWizardR101()
