# -*- coding: utf-8 -*-
from datetime import datetime
import json

from osv import osv, fields, orm
from tools.translate import _
from tools import ustr


class WizardAtrFromInvoice(osv.osv_memory):
    """Wizard to generate ATR Cases from invoices. R1-01
    """
    _name = 'wizard.atr.from.invoice'

    def _get_multipol(self, cursor, uid, context=None):
        if not context:
            context = {}
        return len(context['active_ids']) > 1

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Només de les factures que:\n")

        if self._whereiam(cursor, uid, context=context) == 'comer':
            extra_txt = _(u" * Són de distribuïdor (R1-01)\n")
        else:
            extra_txt = _(u" No hi ha cap acció implementada\n")
        return txt + extra_txt

    def _has_dso_invoices(self, cursor, uid, context=None):
        """
        :return: True if there's at lest one DSO invoice
        """
        if context is None:
            context = {}
        inv_ids = context.get('active_ids', [])
        inv_obj = self.pool.get('giscedata.facturacio.factura')
        return any((
            inv['type'] in ['in_invoice', 'in_refund']
            for inv in inv_obj.read(cursor, uid, inv_ids, ['type'])
        ))
    def _whereiam(self, cursor, uid, context=None):
        ''' returns 'distri' or 'comer' '''
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def generar_r101(self, cursor, uid, ids, context=None):
        if context is None:
            context = {}
        invoice_id = context.get('active_id', False)
        proces = context.get('proces', 'R1-02036')
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'giscedata.switching.r101.wizard',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': "{{'invoice_id': {0}, 'proces': '{1}'}}".format(
                invoice_id, proces
            )
        }

    def generate_atr_cases(self, cursor, uid, ids, context=None):
        ''' Generates the cases from invoices'''
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        proces = context.get('proces', 'R1-02036')
        state = wizard.state
        extra_vals = None
        # Created cases
        casos_ids = []

        inv_ids = context.get('active_ids', [])
        info = _('Processades {0} factures\n\n').format(len(inv_ids))

        inv_obj = self.pool.get("giscedata.facturacio.factura")

        if len(inv_ids) == 1 and proces in ['R1-02009', 'R1-02036']:
            return self.generar_r101(cursor, uid, ids, context=context)
        context['suspesa'] = proces in ['R1-02009', 'R1-02036']
        for inv_id in inv_ids:
            res = inv_obj.create_atr_case(
                cursor, uid, inv_id, proces, context=context)
            info += "{0}: {1}\n".format(res[0], res[1])
            if res[2]:
                casos_ids.append(res[2])

        cases_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'generated_cases': cases_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.generated_cases
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', {0})]".format(str(casos_ids)),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def create_cases_from_invoice_contracts(self, cursor, uid, ids, context=None):
        fact_obj = self.pool.get('giscedata.facturacio.factura')

        if context is None:
            context = {}

        fact_ids = context.get('active_ids', False)

        if not fact_ids:
            raise osv.except_osv('Error',
                                 _("No s'han seleccionat factures"))
        else:
            fact_reads = fact_obj.read(
                cursor, uid, fact_ids, ['polissa_id'], context=context
            )
            polissa_ids = []
            facts_dict = {}
            for fact in fact_reads:
                polissa_ids.append(fact['polissa_id'][0])
                if facts_dict.get(str(fact['polissa_id'][0]), False):
                    facts_dict[str(fact['polissa_id'][0])].append(fact['id'])
                else:
                    facts_dict[str(fact['polissa_id'][0])] = [fact['id']]

            polissa_ids = list(set(polissa_ids))

            if len(polissa_ids) > 1:
                return {
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'wizard.r101.from.multiple.contracts',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': "{{'contract_id': {0}, 'contract_ids': {1}, 'reclamacio_num_factura': {2}}}".format(
                        polissa_ids[0], polissa_ids, facts_dict
                    )
                }
            else:
                return {
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'wizard.create.r1',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': "{{'polissa_id': {0}, 'reclamacio_num_factura': {1}}}".format(
                        polissa_ids[0], facts_dict
                    )
                }

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'multipol': fields.boolean('Multipol'),
        'info': fields.text('Info'),
        'has_dso_invoices': fields.boolean('Factures proveidor'),
        'generated_cases': fields.text('Casos generats'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multipol': _get_multipol,
        'info': _get_info_default,
        'whereiam': _whereiam,
        'has_dso_invoices': _has_dso_invoices,
        'generated_cases': lambda *a: json.dumps([]),
    }

WizardAtrFromInvoice()
