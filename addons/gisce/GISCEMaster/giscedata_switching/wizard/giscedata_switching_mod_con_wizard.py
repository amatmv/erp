# -*- coding: utf-8 -*-
from __future__ import absolute_import
from osv import osv, fields
from tools.translate import _
from gestionatr.defs import (
    TABLA_17, TABLA_53, CONTROL_POTENCIA, PERSONA, TIPUS_DOCUMENT,
    TARIFES_SEMPRE_MAX, TARIFES_6_PERIODES, SEL_CONFIG_MODCON_WIZ_TYPE, TABLA_8,
    TIPO_DH_APARATO, TABLA_117, TABLA_113)
from enerdata.contracts.tariff import (
    get_tariff_by_code, NotPositivePower, IncorrectPowerNumber,
    IncorrectMaxPower, IncorrectMinPower, NotAscendingPowers, NotNormalizedPower
)
import vatnumber
import json
from datetime import datetime, timedelta
from gestionatr.defs import TIPUS_DOCUMENT_INST_CIE, TABLA_64, TABLA_62, SINO

TABLA_AUTOCONSUM = TABLA_113[5:]
TABLA_AUTOCONSUM.append((False, ''))

class GiscedataSwitchingModConWizard(osv.osv_memory):
    _name = 'giscedata.switching.mod.con.wizard'

    def get_modes_facturacio(self, cursor, uid, context=None):
        return self.wrapper_get_modes_facturacio(cursor, uid, context=context)

    def wrapper_get_modes_facturacio(self, cursor, uid, context=None):
        return []

    def onchange_type(self, cursor, uid, ids, change_atr, change_adm, contract, generate_new_contract, new_contract, proces, context=None):
        if not context:
            context = {}

        if not context.get("cas"):
            context['cas'] = proces
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if change_atr or not change_adm:
            res['value']['check_introduir_lectures'] = False

        pol_obj = self.pool.get('giscedata.polissa')
        if change_atr and not change_adm and contract:
            if not contract:
                return res
            pol = pol_obj.browse(cursor, uid, contract, context=context)
            aux = self.update_vals_from_contract(
                cursor, uid, pol, context=context
            )
            new_vals = aux['value']
            res['value'].update(new_vals)
            res['warning'] = aux.get("warning", {})

        elif change_adm and contract:
            if not new_contract and generate_new_contract == u"exists":
                pol = pol_obj.browse(cursor, uid, contract, context=context)
                search_params = [
                    ('cups', '=', pol.cups.id),
                    ('state', '=', 'esborrany'),
                    ('id', '!=', pol.id)
                ]
                new_pol_ids = pol_obj.search(cursor, uid, search_params)
                if len(new_pol_ids):
                    new_contract = new_pol_ids[0]
            aux = self.onchange_new_contact(
                cursor, uid, [], contract, generate_new_contract,
                new_contract, proces, context=context
            )
            new_vals = aux['value']
            res['warning'] = aux.get("warning", {})
            res['value'].update(new_vals)
            res['value']['generate_new_contract'] = generate_new_contract
            res['value']['new_contract'] = new_contract

        if not change_atr:
            res['value']['dt_add'] = False
            res['value']['solicitud_tensio'] = False

        return res

    def onchange_mod_autoconsum(self, cursor, uid, ids, mod_autoconsum, cups, context=None):
        if context is None:
            context = {}
        autoconsum_type = False
        if not cups:
            raise osv.except_osv(_("No s'ha trobat CUPS"),
                                 _(u"S'ha produït un error en recuperar el cups del contracte i "
                                   u"per tant no es pot associar un autoconsum a la pòlissa"))
        if mod_autoconsum:
            autoconsum_obj = self.pool.get('giscedata.autoconsum')
            autoconsum_id = autoconsum_obj.search(cursor, uid, [('cups_id.name', '=', cups)])
            if not autoconsum_id:
                autoconsum_type = '31'
            else:
                autoconsum_type = autoconsum_obj.get_autoconsum_type_from_autoconsum(cursor, uid, autoconsum_id[0],
                                                                                     context=context)
        return {'value': {'autoconsum': autoconsum_type}}

    def onchange_new_contact(self, cursor, uid, ids, old_contract, contract_type, contact_id, proces, context=None):
        if context is None:
            context = {}

        if not context.get("cas"):
            context['cas'] = proces

        if contract_type == "exists" and contact_id:
            pol_id = contact_id
        else:
            pol_id = old_contract

        if not pol_id:
            return

        pol_obj = self.pool.get('giscedata.polissa')
        pol = pol_obj.browse(cursor, uid, pol_id, context=context)
        aux = self.update_vals_from_contract(cursor, uid, pol, context=context)
        return {'value': aux['value'], 'domain': {}, 'warning': aux.get("warning", {})}

    @staticmethod
    def get_change_type(change_atr, change_adm):
        if change_atr and change_adm:
            return 'both'
        elif change_atr:
            return 'tarpot'
        elif change_adm:
            return 'owner'
        else:
            raise ValueError

    def get_phone(self, cursor, uid, address_id):

        res = {'phone_num': ''}

        partner_obj = self.pool.get('res.partner.address')
        swh_obj = self.pool.get('giscedata.switching.step.header')

        address = partner_obj.read(cursor, uid, address_id, ['phone', 'mobile'])

        phone = address['phone'] or address['mobile']
        if phone:
            tel_str = swh_obj.clean_tel_number(phone)
            res.update({'phone_num': tel_str['tel']})
            if tel_str['pre']:
                res.update({'phone_pre': tel_str['pre']})

        return res

    def get_partner_fields(self, cursor, uid, partner_id, prefix='con'):
        """returns [prefix]_name/sur1/sur2 dict parsed from name"""

        partner_obj = self.pool.get('res.partner')
        vals = {}

        partner_vals = partner_obj.read(
            cursor, uid, partner_id, ['name', 'vat']
        )

        vals['{0}_name'.format(prefix)] = partner_vals['name']

        vat_enterprise = partner_obj.vat_es_empresa(
            cursor, uid, partner_vals['vat']
        )

        vals.update({'{0}_type'.format(prefix): vat_enterprise and 'J' or 'F'})

        if not vat_enterprise:
            nom = partner_obj.separa_cognoms(cursor, uid, partner_vals['name'])
            vals.update({
                '{0}_name'.format(prefix): nom['nom'],
                '{0}_sur1'.format(prefix): nom['cognoms'][0],
                '{0}_sur2'.format(prefix): nom['cognoms'][1],
            })
        else:
            # empty surnames
            vals.update({
                '{0}_sur1'.format(prefix): '',
                '{0}_sur2'.format(prefix): '',
            })

        return vals

    def get_tariff_domain(self, cursor, uid, tariff_id, context=None):
        """return the sale pricelists domain compatible with a tariff"""
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        mdata_obj = self.pool.get('ir.model.data')

        tariff_fields = ['llistes_preus_comptatibles']
        tariff_vals = tariff_obj.read(cursor, uid, tariff_id, tariff_fields)

        price_lists = tariff_vals['llistes_preus_comptatibles']

        tariff_domain = [('type', '=', 'sale')]
        if price_lists:
            # We don't need atr tariff pricelist
            atr_price_list_id = mdata_obj.get_object_reference(
                cursor, uid,
                'giscedata_facturacio',
                'pricelist_tarifas_electricidad'
            )[1]
            price_lists.remove(atr_price_list_id)
            tariff_domain.append(('id', 'in', price_lists))

        return tariff_domain, price_lists

    def onchange_contact(self, cursor, uid, ids, contact_id, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}

        contact_obj = self.pool.get('res.partner')

        contact = contact_obj.browse(cursor, uid, contact_id)

        if contact.address:
            res['value'].update(
                self.get_phone(cursor, uid, contact.address[0].id)
            )
            res['value']['direccio_notificacio'] = contact.address[0].id

        res['value'].update(
            self.get_partner_fields(
                cursor, uid, contact_id, 'con'
            )
        )

        return res

    def onchange_contact_dir(self, cursor, uid, ids, contact_dir_id, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}
        if contact_dir_id:
            res['value'].update(
                self.get_phone(cursor, uid, contact_dir_id)
            )
        return res

    def onchange_pagador(self, cursor, uid, ids, pagador_id, context=None):
        if not context:
            context = None

        res = {'value': {}, 'domain': {}, 'warning': {}}

        part_obj = self.pool.get('res.partner')
        pagador = part_obj.browse(cursor, uid, pagador_id)

        if pagador.address:
            res['value']['direccio_pagament'] = pagador.address[0].id

        res['value']['bank'] = False
        if pagador.bank_ids:
            if len(pagador.bank_ids) == 1:
                res['value']['bank'] = pagador.bank_ids[0].id
            else:
                selected_banks = []
                for bank in pagador.bank_ids:
                    if bank.default_bank:
                        selected_banks.append(bank.id)
                if len(selected_banks) == 1:
                    res['value']['bank'] = selected_banks[0]
                elif len(selected_banks) > 1:
                    res['warning'] = {
                        'title': _(u"No s'ha pogut asignar compte bancari"),
                        'message': _(u"L'empresa {0} té més d'1 compte bancari marcat com 'per defecte', no s'ha pogut escollir compte bancari de forma automàtica.").format(pagador.name),
                    }
                elif len(selected_banks) == 0:
                    res['warning'] = {
                        'title': _(u"No s'ha pogut asignar compte bancari"),
                        'message': _(u"L'empresa {0} no té cap compte bancari marcat com 'per defecte', no s'ha pogut escollir compte bancari de forma automàtica.").format(pagador.name),
                    }

        return res

    def onchange_atr(self, cursor, uid, ids, tariff, power_p1, power_p2,
                     power_p3, power_p4, power_p5, power_p6, power_invoicing,
                     contract_id, change_retail_tariff=None, retail_tariff=None,
                     context=None):

        res = {'value': {}, 'domain': {}, 'warning': {}}

        warning_titles = set()
        warning_messages = set()

        if not tariff:
            return res

        power_check_fields = ['power_p1']

        if tariff in TARIFES_SEMPRE_MAX:
            power_check_fields.extend(['power_p2', 'power_p3'])
            res['value']['power_invoicing'] = '2'
            if power_invoicing and power_invoicing != '2':
                warning_titles.add(_(u"Facturació de potència incorrecte"))
                warning_messages.add(_(u"En tarifes de més de 15kW la "
                                          u"facturació de potència SEMPRE ha "
                                          u"de ser amb maxímetre"))

        if tariff in TARIFES_6_PERIODES:
            power_check_fields.extend(['power_p4', 'power_p5', 'power_p6'])

        # The tariff and power
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')
        swh_obj = self.pool.get('giscedata.switching.step.header')
        contract_obj = self.pool.get('giscedata.polissa')

        pol_id = contract_id or context.get('pol_id')
        tariflb = False
        if pol_id:
            pol_inf = contract_obj.read(cursor, uid, pol_id, ['tarifa'])
            if pol_inf['tarifa']:
                tarif_inf = tariff_obj.read(cursor, uid, pol_inf['tarifa'][0],
                                            ['tipus', 'mesura_ab'])
                tariflb = tarif_inf['tipus'] == 'AT' and \
                          tarif_inf['mesura_ab'] == 'B'

        tariff_id = tariff_obj.get_tarifa_from_ocsum(cursor, uid, tariff, tariflb)
        tariff_fields = ['name', 'llistes_preus_comptatibles']
        tariff_vals = tariff_obj.read(cursor, uid, tariff_id, tariff_fields)

        tariff_domain, price_lists = self.get_tariff_domain(
            cursor, uid, tariff_id, context=context
        )

        if price_lists:
            default_retail_tariff = self._get_default_retail_tariff(
                cursor, uid, context=context)
            correct_default = default_retail_tariff in price_lists
            if not change_retail_tariff and correct_default:
                if default_retail_tariff in price_lists:
                    res['value'].update(
                        {'retail_tariff': default_retail_tariff}
                    )
            else:
                res['domain'].update({'retail_tariff': tariff_domain})
                if contract_id and (retail_tariff not in price_lists or not retail_tariff):
                    new_retail_tariff = contract_obj.escull_llista_preus(
                        cursor, uid, contract_id, price_lists, context=context
                    )
                    if new_retail_tariff and new_retail_tariff.id in price_lists:
                        res['value'].update(
                            {'retail_tariff': new_retail_tariff.id,
                             'change_retail_tariff': True
                             }
                        )
                    else:
                        new_retails = {'retail_tariff': False,
                                       'change_retail_tariff': True,
                                       }
                        res['value'].update(new_retails)
        else:
            res['value'].update({
                'retail_tariff': False,
                'change_retail_tariff': True,
            })
            warning_titles.add(_(u"Tarifa ATR incorrecte"))
            warning_messages.add(_(u"No hi ha cap Tarifa Comercialitzadora "
                                      u"per a aquesta tarifa ATR"))

        powers = [locals()[power] / 1000.0 for power in power_check_fields]
        tariff_enerdata = get_tariff_by_code(tariff_vals['name'])()

        if min(powers) != 0:
            # We don't want to do this if some of the needed powers han't been
            # set yet because it would create warnings that it shouldn't
            try:
                tariff_enerdata.evaluate_powers(powers)
            except NotPositivePower as e:
                warning_titles.add(_(u"Potència incorrecte"))
                warning_messages.add(
                    _(u'Una de les potencies és igual o inferior a 0')
                )
                try:
                    new_powers = tariff_enerdata.correct_powers(powers)
                    for i in range(0, len(power_check_fields)):
                        res['value'].update({
                            power_check_fields[i]: new_powers[i] * 1000
                        })
                except NotImplementedError:
                    pass
            except IncorrectPowerNumber:
                warning_titles.add(_(u"Potència incorrecte"))
                warning_messages.add(
                    _(u'El nombre de potencies donades és incorrecte')
                )
            except IncorrectMinPower as e:
                warning_titles.add(_(u"Potència incorrecte"))
                warning_messages.add(
                    _(u'El valor de potència mínim donat ({0}kW) no està entre '
                      u'els limits esperats ({1}-{2}kW)').format(
                        e.power, e.min_power, e.max_power
                    )
                )
                try:
                    new_powers = tariff_enerdata.correct_powers(powers)
                    for i in range(0, len(power_check_fields)):
                        res['value'].update({
                            power_check_fields[i]: new_powers[i] * 1000
                        })
                except NotImplementedError:
                    pass
            except IncorrectMaxPower as e:
                warning_titles.add(_(u"Potència incorrecte"))
                warning_messages.add(
                    _(u'El valor de potència màxim donat ({0}kW) no està entre '
                      u'els limits esperats ({1}-{2}kW)').format(
                        e.power, e.min_power, e.max_power
                    )
                )
                try:
                    new_powers = tariff_enerdata.correct_powers(powers)
                    for i in range(0, len(power_check_fields)):
                        res['value'].update({
                            power_check_fields[i]: new_powers[i] * 1000
                        })
                except NotImplementedError:
                    pass
            except NotAscendingPowers:
                warning_titles.add(_(u"Potència incorrecte"))
                warning_messages.add(
                    _(u'Els valors de potencia no estan en ordre ascendent')
                )
            except NotNormalizedPower:
                # If we are using maximeter and the tariff is >15kw we don't
                # need normalized powers
                # Some distris (like Iberdrola) don't require this but as there
                # are not many tariffs of <15kW (2.X) with maximeter we prefer
                # to keep it like this
                tariff_name = tariff_vals['name']
                always_required = [
                    '2.0A', '2.0DHA', '2.1A', '2.1DHA', '2.0DHS', '2.1DHS'
                ]
                if power_invoicing != '2' or tariff_name in always_required:
                    warning_titles.add(_(u"Potència incorrecte"))
                    warning_messages.add(
                        _(u'Algun(s) valor(s) de potencia no estan normalitzats')
                    )

        for pow in powers:
            if 0 < pow < 0.1:  # If some power is less than 100 W
                warning_titles.add(_(u"Potència molt baixa"))
                warning_messages.add(
                    _(u'Algun(s) valor(s) de potencia són inferiors a 100W. '
                      u'Comproveu que són correctes i recordeu que han d\'anar '
                      u'en watts.')
                )

        if warning_titles:
            res['warning'] = {
                'title': ', '.join(warning_titles),
                'message': '\n'.join(warning_messages)
            }

        res['value']['num_periods'] = tariff_obj.get_num_periodes(cursor, uid,
                                                                  [tariff_id])
        return res

    def get_default_mode_facturacio(self, cursor, uid, llista_preus_id, context=None):
        modef_o = self.pool.get('giscedata.polissa.mode.facturacio')
        mfids = modef_o.search(cursor, uid, [('compatible_price_lists', 'in', llista_preus_id)])
        if len(mfids) == 1:
            return modef_o.read(cursor, uid, mfids[0], ['code'])['code']
        else:
            return False

    def onchange_change_retail_tariff(self, cursor, uid, ids,
                                      change_retail_tariff, tariff,
                                      context=None):
        """ Search the partner with the new VAT, if exists, and fills
            owner field and vat_kind"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        tariff_obj = self.pool.get('giscedata.polissa.tarifa')

        if not tariff:
            return res

        tariff_id = tariff_obj.get_tarifa_from_ocsum(cursor, uid, tariff)
        tariff_domain, llistes_preus = self.get_tariff_domain(
            cursor, uid, tariff_id, context
        )

        if not change_retail_tariff:
            default_retail_tariff = self._get_default_retail_tariff(
                cursor, uid, context=context)
            if default_retail_tariff in llistes_preus:
                res['value'].update({'retail_tariff': default_retail_tariff,
                                     'change_retail_tariff': False,
                                     'mode_facturacio': self.get_default_mode_facturacio(cursor, uid, default_retail_tariff, context=context)
                                     })
            else:
                res['value'].update({'change_retail_tariff': True})
                res['warning'] = {
                    'title': _(u'Tarifa Comercialitzadora Incorrecte'),
                    'message': _(u'La tarifa de comercialitzadora original '
                                 u'és incompatible amb la nova tarifa ATR'),
                }
        else:
            res['value'].update({'retail_tariff': False,
                                 'change_retail_tariff': True}
                                )
        res['domain'] = {'retail_tariff': tariff_domain}

        return res

    def onchange_check_retail_tariff(self, cursor, uid, ids,
                                      change_retail_tariff,
                                      retail_tariff,
                                      context=None):
        """ If selected retail tariff is the actual one, unckeck
        change_retail_tariff field"""
        res = {'value': {}, 'domain': {}, 'warning': {}}
        if context is None:
            context = {}
        tarifa_actual = self._get_default_retail_tariff(cursor, uid,
                                                        context=context)
        same_tariff = tarifa_actual == retail_tariff
        if change_retail_tariff and same_tariff:
            res['value'].update({'change_retail_tariff': False})
        if retail_tariff:
            res['value'].update({
                'mode_facturacio': self.get_default_mode_facturacio(cursor, uid, retail_tariff, context=context)
            })
        else:
            res['value'].update({'mode_facturacio': False})

        return res

    def onchange_vat(self, cursor, uid, ids, vat, contract_id, owner, contact,
                     context=None):
        """ Search the partner with the new VAT, if exists, and fills
            owner field and vat_kind"""
        res = {'value': {}, 'domain': {}, 'warning': {}}

        if not vat or not contract_id:
            return res

        partner_obj = self.pool.get('res.partner')
        contract_obj = self.pool.get('giscedata.polissa')
        sw_obj = self.pool.get('giscedata.switching')
        contract = contract_obj.browse(cursor, uid, contract_id,
                                       context=context)

        vat_info = sw_obj.get_vat_info(
            cursor, uid, vat, contract.distribuidora.ref
        )

        vat = vat_info['vat']
        vat_kind = vat_info['document_type']

        if vat_kind != 'PS':
            if vat and not vat.startswith('ES'):
                vat = 'ES{0}'.format(vat.upper().zfill(9))
                res['value'].update({'vat': vat})
            if not vatnumber.check_vat(vat):
                res.update({
                    'warning': {
                        'title': _(u'NIF/CIF incorrecte'),
                        'message': _(u'El document introduït no és vàlid'),
                    }
                })
                return res

        partner_ids = partner_obj.search(cursor, uid, [('vat', '=', vat)])

        if partner_ids:
            if len(partner_ids) > 1:
                partner_id = ''
                res['warning'].update({
                    'title': _(u'Warning'),
                    'message': _(u"Més de un partner amb el mateix NIF: {0}").format(vat)
                })
            else:
                partner_id = partner_ids[0]
        else:
            partner_id = ''

        res['value'].update({
            'owner': partner_id,
            'vat_kind': vat_kind,
        })

        # If we are changing the owner
        if partner_id != owner:
            # We check what parameters of res (value, domain or warnings)
            # whould change if we changed the owner and we add them to their
            # corresponding place
            canvis_owner = self.onchange_owner(
                cursor, uid, ids, partner_id, owner, contact, vat, contract_id
            )
            for camp in canvis_owner:
                res[camp].update(canvis_owner[camp])

        return res

    def onchange_owner(self, cursor, uid, ids, owner, owner_pre, contact, vat,
                       contract_id):
        """ If the previous owner and the original contract where the same or
        we didn't have a contact, we change the contact to the new owner"""
        res = {'value': {'owner_pre': owner}, 'domain': {}, 'warning': {}}

        if not owner:
            return res

        partner_obj = self.pool.get('res.partner')

        if not contact or owner_pre == contact:
            res['value'].update({'contact': owner})

            # We check what parameters of res (value, domain or warnings)
            # whould change if we changed the contact and we add them to their
            # corresponding place
            canvis_contacte = self.onchange_contact(cursor, uid, ids, owner)
            for camp in canvis_contacte:
                res[camp].update(canvis_contacte[camp])

            new_vat = partner_obj.read(cursor, uid, owner, ['vat'])['vat']
            res['value'].update({'vat': new_vat})
            if new_vat != vat:
                # We do the same as above, now for vat changes
                canvis_vat = self.onchange_vat(
                    cursor, uid, ids, new_vat, contract_id, owner, owner
                )
                for camp in canvis_vat:
                    res[camp].update(canvis_vat[camp])

        res['value']['pagador'] = owner
        aux = self.onchange_pagador(cursor, uid, ids, owner)
        res['value'].update(aux['value'])
        if aux.get("warning"):
            aux_title = ". ".join([aux.get("warning", {}).get("title", ""), res.get("warning", {}).get("title", "")])
            aux_message = ". ".join([aux.get("warning", {}).get("message", ""), res.get("warning", {}).get("message", "")])
            res["warning"] = {
                'title': aux_title,
                'message': aux_message
            }

        return res

    def is_owner_changed(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        pol_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)
        pol_id = context['pol_id']

        config_vals = wizard.read([])[0]
        titular_nou = config_vals['owner']
        titular_orig = pol_obj.read(
            cursor, uid, pol_id, ['titular'])['titular'][0]

        return titular_nou != titular_orig

    def is_power_or_plist_changed(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        pol_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)
        pol_id = context['pol_id']

        config_vals = wizard.read([])[0]
        potencia_nova = {}

        # Es guarden les noves potències registrades al wizard
        for p in range(1, 7):
            potencia_nova['pot' + str(p)] = config_vals['power_p' + str(p)]

        potencia_orig = {}
        periodes_orig = pol_obj.browse(
            cursor, uid, pol_id)['potencies_periode']

        # Es guarden en un diccionari equivalent les potències velles
        for pot in range(0, 6):
            if len(periodes_orig) > pot:
                potencia_orig['pot' + str(pot + 1)] = \
                    int(periodes_orig[pot]['potencia'] * 1000)
            else:
                potencia_orig['pot' + str(pot + 1)] = 0

        tarifa_nova_codi = config_vals['tariff']

        for codis in TABLA_17:
            if codis[0] == tarifa_nova_codi:
                tarifa_nova = codis[1]
                break

        tarifa_orig = pol_obj.read(
            cursor, uid, pol_id, ['tarifa_codi'])['tarifa_codi']

        return potencia_nova != potencia_orig or tarifa_nova != tarifa_orig

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}

        pol_obj = self.pool.get('giscedata.polissa')
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_id = wizard.contract.id
        context['pol_id'] = pol_id
        proces = wizard.proces
        config_vals = wizard.read([])[0]

        change_type = self.get_change_type(wizard.change_atr,
                                           wizard.change_adm)

        if not wizard.skip_validations:
            if change_type in ['both', 'owner'] \
                    and not self.is_owner_changed(cursor, uid, ids, context):
                raise osv.except_osv(
                    _('Canvi de titular no satisfet'),
                    _('Malgrat que el procés és per canviar el titular, '
                      'aquest continua essent el mateix')
                )

            if change_type in ['both', 'tarpot'] \
                    and not self.is_power_or_plist_changed(cursor, uid, ids, context):
                raise osv.except_osv(
                    _('Canvi de potència o tarifa'),
                    _('Malgrat que el procés és per canviar la potència o'
                      ' la tarifa cap de les dues s\'han modificat')
                )

        config_vals['change_type'] = change_type

        config_vals['data_accio'] = self.calc_data_accio(
            cursor, uid, wizard.id, wizard.contract, wizard.activacio_cicle,
            wizard.data_accio, wizard.check_introduir_lectures,
            wizard.measure_date, context=context
        )

        if wizard.proces == 'M1' and wizard.generate_new_contract == "create" and change_type in ['both', 'owner']:
            config_vals['new_contract_values'] = {
                'titular': config_vals['owner'],
                'cnae': config_vals['cnae'],
                'facturacio_potencia': 'icp' if config_vals['power_invoicing'] == '1' else 'max',
                'pagador': config_vals['pagador'],
                'pagador_sel': 'titular',
                'direccio_pagament': config_vals['direccio_pagament'],
                'notificacio': 'titular',
                'altre_p': False,
                'direccio_notificacio': config_vals['direccio_notificacio'],
                'bank': config_vals['bank'],
                'tipo_pago': config_vals['tipo_pago'],
                'payment_mode_id': config_vals['payment_mode_id'],
                'data_firma_contracte': datetime.today().strftime("%Y-%m-%d %H:%M:%S"),
                'mode_facturacio': config_vals['mode_facturacio'],
            }

            if config_vals['owner'] != config_vals['pagador']:
                config_vals['new_contract_values']['pagador_sel'] = 'altre_p'

            if config_vals['contact'] == config_vals['pagador']:
                config_vals['new_contract_values']['notificacio'] = 'pagador'
            elif config_vals['contact'] != config_vals['owner']:
                config_vals['new_contract_values']['notificacio'] = 'altre_p'
                config_vals['new_contract_values']['altre_p'] = config_vals['contact']

            if "new_contract_extra_vals" in context:
                context["new_contract_extra_vals"].setdefault(
                    'ref_dist', wizard.contract.ref_dist
                )
            else:
                context["new_contract_extra_vals"] = {
                    'ref_dist': wizard.contract.ref_dist
                }

            config_vals['new_contract_values'].update(context["new_contract_extra_vals"])
        elif wizard.generate_new_contract == "exists" and wizard.new_contract and wizard.proces == 'M1':
            config_vals['new_contract_to_link'] = wizard.new_contract.id
        else:
            config_vals['new_contract_to_link'] = wizard.contract.id

        if wizard.check_introduir_lectures:
            config_vals['lectures_vals'] = wizard.get_dict_introduir_lectures()

        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, proces, config_vals=config_vals,
            context=context
        )

        if not res[2]:
            info = "{0}: {1}\n".format(res[0], res[1])
            wizard.write({'state': 'end', 'info': info, 'casos_generats': json.dumps([])})
            return

        casos_json = json.dumps([res[2]])

        msg_extr = ""
        if self.check_es_m1_automatitzacio(cursor, uid, ids, context=context):
            if wizard.activacio_cicle == 'A':
                pol_obj.write(cursor, uid, pol_id, {'facturacio_suspesa': True})
                msg_extr += _(u"\nS'ha posat el contracte en facturació suspesa.")

        info = "{0}: {1}\n{2}".format(res[0], res[1], msg_extr)

        wizard.write({'state': 'end', 'info': info, 'casos_generats': casos_json})

    def check_es_m1_automatitzacio(self, cursor, uid, ids, context=None):
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        wizard = self.browse(cursor, uid, ids, context)
        change_type = self.get_change_type(wizard.change_atr, wizard.change_adm)
        conf_obj = self.pool.get('res.config')
        m1_owner_auto = bool(int(conf_obj.get(cursor, uid, "sw_m1_owner_change_auto", '0')))
        tipus_canvi = wizard.owner_change_type
        subrogacio_nou_contracte = bool(int(conf_obj.get(cursor, uid, "sw_m1_owner_change_subrogacio_new_contract", '1')))
        return (
            m1_owner_auto
            and wizard.proces == 'M1'
            and change_type in ('owner', 'both')
            and (
                tipus_canvi == 'T'
                or (tipus_canvi == 'S' and subrogacio_nou_contracte)
            )
            and (
                (wizard.generate_new_contract == "exists" and wizard.new_contract)
                or wizard.generate_new_contract == "create"
            )
        )

    def calc_data_accio(self, cursor, uid, wiz_id, polissa, activacio, data_accio=None,
                        introdui_lectures=False, data_lectures=False, context=None):
        if context is None:
            context = {}

        if activacio == 'F':
            return data_accio

        if not self.check_es_m1_automatitzacio(cursor, uid, wiz_id):
            return data_accio

        data_ultima_lect = polissa.data_ultima_lectura
        if not data_ultima_lect:
            if activacio == 'A':
                fact_obj = self.pool.get("giscedata.facturacio.factura")
                fids = fact_obj.search(cursor, uid, [
                        ('polissa_id', '=', polissa.id),
                        ('type', 'in', ('out_refund', 'out_invoice'))
                ])
                if len(fids):
                    raise osv.except_osv(
                        "Error",
                        _(u"No tenim data de ultima lectura facturada peró "
                          u"existeixen factures de client.\nNo es crerà el M1.")
                    )
            data_ultima_lect = polissa.data_alta or datetime.today().strftime("%Y-%m-%d")

        if activacio == 'A':
            if introdui_lectures and data_lectures:
                return data_lectures
            return data_ultima_lect
        else:  # activacio == 'L'
            dateplus1 = datetime.strptime(data_ultima_lect, "%Y-%m-%d") + timedelta(days=1)
            return dateplus1.strftime("%Y-%m-%d")

    def get_dict_introduir_lectures(self, cursor, uid, wiz_id, context=None):
        if context is None:
            context = {}

        if isinstance(wiz_id, (list, tuple)):
            wiz_id = wiz_id[0]

        vals_to_read = ['measure_date', 'num_periods', 'contract']
        str_tmplate = "measure_{0}_p{1}"
        for tipus in ['ae', 'r1', 'pm', 'ep']:
            vals_to_read.append("send_{}_measures".format(tipus))
            for index in range(1, 7):
                vals_to_read.append(str_tmplate.format(tipus, index))

        res = self.read(cursor, uid, wiz_id, vals_to_read)[0]
        return res

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])

        domain_cups = "('cups_polissa_id','=', {0})".format(wiz.contract.id)
        domain_proces = "('proces_id.name','=','{0}')".format(wiz.proces)
        domain = "[{0},{1}]".format(domain_cups, domain_proces)
        return {
            'domain': domain,
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def default_get(self, cursor, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(GiscedataSwitchingModConWizard, self).default_get(
            cursor, uid, fields, context=context
        )

        pol_obj = self.pool.get('giscedata.polissa')

        pol_id = context['pol_id']

        pol = pol_obj.browse(cursor, uid, pol_id, context=context)

        res.update({
            'proces': context['cas'],
            'phone_pre': '34',
            'cups': pol.cups.name,
            'contract': pol.id,
            'cnae': pol.cnae.id,
            'check_introduir_lectures': False,
        })

        proces_name = context.get('cas', 'M1')
        if proces_name == "C2":
            res.update({'skip_validations': True})

        # In M1 if we have a new contract we'll fill the data with this contract
        if proces_name == 'M1' and res.get('change_adm'):
            res['generate_new_contract'] = u"exists"
            search_params = [
                ('cups', '=', pol.cups.id),
                ('state', '=', 'esborrany')
            ]
            new_pol_ids = pol_obj.search(cursor, uid, search_params)
            if len(new_pol_ids):
                res['new_contract'] = new_pol_ids[0]
                pol2 = pol_obj.browse(
                    cursor, uid, res['new_contract'], context=context
                )
                if pol.llista_preu != pol2.llista_preu:
                    res['change_retail_tariff'] = True
                    res['retail_tariff'] = pol2.llista_preu.id
                pol = pol2
            else:
                res['generate_new_contract'] = u"create"

        res.update(self.update_vals_from_contract(cursor, uid, pol, context=context)['value'])

        retorn = dict([(field, res[field]) for field in fields if res.get(field, False)])
        ir_obj = self.pool.get('ir.values')
        for field, value in retorn.items():
            change_default = getattr(self._columns[field], 'change_default')
            if change_default:
                event = '{}={}'.format(field, value)
                for default_value in ir_obj.get(
                        cursor, uid, 'default', event, [[self._name, False]]
                ):
                    retorn[default_value[1]] = default_value[2]

        return retorn

    def update_vals_from_contract(self, cursor, uid, pol, context=None):
        if context is None:
            context = {}

        sw_obj = self.pool.get('giscedata.switching')
        res = {}
        warnings = []
        # Tarif and pots.
        res['tariff'] = pol.tarifa.codi_ocsum
        res['num_periods'] = pol.tarifa.get_num_periodes()
        power_invoicing = pol.facturacio_potencia == 'icp' and '1' or '2'
        res['power_invoicing'] = power_invoicing
        for p in pol.potencies_periode:
            key = 'power_{}'.format(p.periode_id.name).lower()
            res[key] = p.potencia * 1000

        # Contact
        res['contact'] = pol.direccio_notificacio.partner_id.id
        res['direccio_notificacio'] = pol.direccio_notificacio.id
        res.update(
            self.get_partner_fields(
                cursor, uid, pol.direccio_notificacio.partner_id.id, 'con'
            )
        )
        res.update(self.get_phone(cursor, uid, pol.direccio_pagament.id))

        # Owner (titular)
        owner = pol.titular
        if context.get('cas', 'M1') == 'M1':
            conf_obj = self.pool.get("res.config")
            if int(conf_obj.get(cursor, uid, "sw_m1_use_pagador", "1")):
                owner = pol.pagador

        res['owner'] = owner.id
        res['owner_pre'] = owner.id
        if owner.vat:
            vat_info = sw_obj.get_vat_info(
                cursor, uid, owner.vat, pol.distribuidora.ref
            )
            res['vat'] = vat_info['vat']
            res['vat_kind'] = vat_info['document_type']

        # Rao fiscal
        res['pagador'] = pol.pagador.id
        res['direccio_pagament'] = pol.direccio_pagament.id

        # Pagaments info
        res['bank'] = pol.bank.id if (pol.bank and pol.bank.partner_id.id == res['pagador']) else False
        if not res['bank']:
            aux = self.onchange_pagador(cursor, uid, [], pol.pagador.id, context=context)
            if aux['value'].get('bank'):
                res['bank'] = aux['value'].get('bank')
            if aux.get("warning"):
                warnings.append(aux.get("warning"))
        res['tipo_pago'] = pol.tipo_pago and pol.tipo_pago.id or False
        res['payment_mode_id'] = pol.payment_mode_id and pol.payment_mode_id.id or False

        # Mode facturacio
        res['mode_facturacio'] = pol.mode_facturacio or False

        if len(warnings):
            warnings = {
                'title': ". ".join([x.get("title") for x in warnings]),
                'message': ". ".join([x.get("message") for x in warnings])
            }
        else:
            warnings = {}
        final_res = {'value': res, 'domain': {}, 'warning': warnings}
        return final_res

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        casos = self.pool.get("giscedata.switching").browse(cursor, uid, casos_ids)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.switching',
            'report_name': 'giscedata.switching.info_cas_ATR',
            'report_webkit': "'giscedata_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
               'casos': casos_ids
            },
        }

    def _get_default_retail_tariff(self, cursor, uid, context=None):
        if context is None:
            context = {}

        pol_id = context.get('pol_id', False)
        pol_obj = self.pool.get('giscedata.polissa')

        contract = pol_obj.browse(cursor, uid, pol_id, context=context)
        return contract.llista_preu.id

    _columns = {
        'state': fields.selection([('init', 'Init'), ('end', 'End')], 'State'),
        'proces': fields.selection([
            ('M1', 'M1 - Modificació contractual'),
            ('C2', 'C2 - Canvi de comercialitzadora amb canvis')
        ], 'Cas', change_default=True),
        'change_atr': fields.boolean(string="Canvi Tarifa/Potencia"),
        'change_adm': fields.boolean(string="Canvi Titular"),
        'skip_validations': fields.boolean(string="Saltar validacions"),
        # lazy way to assign power
        'cups': fields.char('CUPS', size=22),
        'cnae': fields.many2one('giscemisc.cnae', 'CNAE'),
        'power_p1': fields.integer('P1'),
        'power_p2': fields.integer('P2'),
        'power_p3': fields.integer('P3'),
        'power_p4': fields.integer('P4'),
        'power_p5': fields.integer('P5'),
        'power_p6': fields.integer('P6'),
        'tariff': fields.selection(sorted(TABLA_17, key=lambda t: t[1]),
                                   string="Tarifa ATR"),
        'retail_tariff': fields.many2one('product.pricelist',
                                         domain=[('type', '=', 'sale')],
                                         string=u"Tarifa Comercialización"),
        'mode_facturacio': fields.selection(
            get_modes_facturacio, u'Mode facturació',),
        'change_mode_facturacio': fields.boolean(
            u'Canvia Mode Facturacio'
        ),
        'change_retail_tariff': fields.boolean(
            u'Canvia Tarifa Comercialitzadora'
        ),
        'power_invoicing': fields.selection(CONTROL_POTENCIA,
                                            string="MCP"),
        'contact': fields.many2one('res.partner', 'Contacte'),
        'con_name': fields.char('Nom', size=256),
        'con_type': fields.selection(PERSONA, 'Tipus'),
        'con_sur1': fields.char('Cognom 1', size=256),
        'con_sur2': fields.char('Cognom 2', size=256),
        'phone_num': fields.char('Telèfon', size=9),
        'phone_pre': fields.char('Prefix', size=2),
        'comments': fields.text('Comentari', size=120),
        # owner change
        'vat': fields.char('Número de Document', size=11),
        'vat_kind': fields.selection(TIPUS_DOCUMENT,
                                     string='Tipus de document'),
        'owner': fields.many2one('res.partner', 'Nou Titular'),
        'owner_pre': fields.many2one('res.partner', 'Ultim titular triat'),
        'owner_change_type': fields.selection(TABLA_53, 'Tipus de canvi'),
        'contract': fields.many2one('giscedata.polissa', 'Contracte'),
        'info': fields.text('Info'),
        'activacio_cicle': fields.selection(TABLA_8, u"Activació", required=True),
        'data_accio': fields.date(u"Data prevista del canvi o alta"),
        # New contract fields
        'new_contract': fields.many2one('giscedata.polissa', 'Contracte Nou'),
        'generate_new_contract': fields.selection(
            [(u"create", u"Crear nou contracte en borrador"),
             (u"exists", u"Utilitzar contracte existent")],
            "Contracte nou"
        ),
        'pagador': fields.many2one('res.partner', u'Raó fiscal'),
        'direccio_pagament': fields.many2one('res.partner.address', u'Adreça fiscal'),
        'direccio_notificacio': fields.many2one('res.partner.address', u'Adreça notificació'),
        'bank': fields.many2one('res.partner.bank', 'Compte bancari'),
        'tipo_pago': fields.many2one('payment.type', 'Tipo de pago'),
        'payment_mode_id': fields.many2one('payment.mode', 'Grup de pagament'),
        # Casos Generats
        'casos_generats': fields.text('Casos generats'),
        # Lectures
        'check_introduir_lectures': fields.boolean(
            "Introduïr Lectures",
            help=_(u"Introduïr lectures en data determinada.\n"
                   u"Aquestes lectures s'utilitzaran com a lectures finals del "
                   u"contracte antic al activar el M1 de canvi de titular")
        ),
        'measure_date': fields.date('Data lectures'),

        'send_ae_measures': fields.boolean(
            'Amb activa', readonly=True,
            help=u"Marcar per afegir les lectures d'Activa"
        ),
        'measure_ae_p1': fields.integer('Lectura P1'),
        'measure_ae_p2': fields.integer('Lectura P2'),
        'measure_ae_p3': fields.integer('Lectura P3'),
        'measure_ae_p4': fields.integer('Lectura P4'),
        'measure_ae_p5': fields.integer('Lectura P5'),
        'measure_ae_p6': fields.integer('Lectura P6'),

        'send_r1_measures': fields.boolean(
            'Amb reactiva',
            help=u"Marcar per afegir les lectures de Reactiva"
        ),
        'measure_r1_p1': fields.integer('Reactiva P1'),
        'measure_r1_p2': fields.integer('Reactiva P2'),
        'measure_r1_p3': fields.integer('Reactiva P3'),
        'measure_r1_p4': fields.integer('Reactiva P4'),
        'measure_r1_p5': fields.integer('Reactiva P5'),
        'measure_r1_p6': fields.integer('Reactiva P6'),

        'send_pm_measures': fields.boolean(
            'Amb maxímetre',
            help=u"Marcar per afegir les lectures de Maxímetre"
        ),
        'measure_pm_p1': fields.integer('Maxímetre P1'),
        'measure_pm_p2': fields.integer('Maxímetre P2'),
        'measure_pm_p3': fields.integer('Maxímetre P3'),
        'measure_pm_p4': fields.integer('Maxímetre P4'),
        'measure_pm_p5': fields.integer('Maxímetre P5'),
        'measure_pm_p6': fields.integer('Maxímetre P6'),

        'send_ep_measures': fields.boolean(
            'Amb excés',
            help=u"Marcar per afegir les lectures d'Exés"
        ),
        'measure_ep_p1': fields.integer('Excés P1'),
        'measure_ep_p2': fields.integer('Excés P2'),
        'measure_ep_p3': fields.integer('Excés P3'),
        'measure_ep_p4': fields.integer('Excés P4'),
        'measure_ep_p5': fields.integer('Excés P5'),
        'measure_ep_p6': fields.integer('Excés P6'),
        # support fields
        'num_periods': fields.integer("Num. Períodes"),
        'solicitud_tensio': fields.selection(TABLA_117, u"Tensió Sol.licitada"),
        'mod_autoconsum': fields.boolean(u"Modificar"),
        'autoconsum': fields.selection(TABLA_AUTOCONSUM, u"Tipus autoconsum"),

        # Documentació Tècnica
        'dt_add': fields.boolean(u"Documentacio Tècnica"),
        'dt_cie_electronico': fields.selection(SINO, u"Cie Electronico"),
        'dt_cie_codigo': fields.char(u"Código CIE", size=11),
        'dt_cie_papel_potenciainstbt': fields.integer(u"Potència Instalada"),
        'dt_cie_papel_data_emissio': fields.date(u"Data Emissió"),
        'dt_cie_papel_data_caducitat': fields.date(u"Data Caducitat"),
        'dt_tipus_codi_instalador': fields.selection(
            TIPUS_DOCUMENT_INST_CIE, u"Tipus codi Instal·lador"
        ),
        'dt_codi_instalador': fields.char(u"Codi instal·lador", size=9),
        'dt_cie_papel_tensio_suministre': fields.selection(
            TABLA_64, u"Tensió suministre"
        ),
        'dt_cie_papel_tipus_suministre': fields.selection(
            TABLA_62, u"Tipus Suministre"
        ),
        'dt_cie_sello_elec': fields.char(u"Segell Electrónic", size=35),

        'doc_add': fields.boolean(u"Documents adjunts"),

        # En realidad la tabla relacional no se crea, al ser un objeto en
        # memoria actua como un campo one2many pero en memoria sin relacion
        # con el objeto destino
        # Mediante un read del campo veremos los que seleccionamos en el
        # asistente
        'docs_ids': fields.many2many(
            'giscedata.switching.document',
            'wizard_sw_mod_docs_rel',
            'wiz_mod_id', 'doc_id',
            'Documents adjunts',
            domain=[('type', '=', 'neogeo')]
        ),

    }

    _defaults = {
        'state': lambda *a: 'init',
        'retail_tariff': _get_default_retail_tariff,
        'change_retail_tariff': lambda *a: False,
        'change_mode_facturacio': lambda *a: True,
        'owner_change_type': lambda *a: 'S',
        'activacio_cicle': lambda *a: 'L',
        'casos_generats': lambda *a: json.dumps([]),
        'generate_new_contract': lambda *a: u"exists",
        'new_contract': lambda *a: False,
        'change_atr': lambda *a: True,
        'change_adm': lambda *a: False,
        'check_introduir_lectures': lambda *a: False,
        'send_ae_measures': lambda *a: True,
        'skip_validations': lambda *a: False,
        'solicitud_tensio': lambda *a: False,
        'mod_autoconsum': lambda *a: False,
        'autoconsum': lambda *a: False,
        'dt_cie_electronico': lambda *a: 'S',
        'doc_add': lambda *a: False,
        'dt_add': lambda *a: False,
    }

GiscedataSwitchingModConWizard()
