# -*- coding: utf-8 -*-
import json

from osv import osv, fields, orm
from tools.translate import _
from datetime import datetime
from workdays import workday
from gestionatr.defs import TABLA_10, TABLA_8
from giscedata_switching.giscedata_switching import SwitchingException


class GiscedataSwitchingWizardB101(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.switching.b101.wizard'

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Generació de B1-01\n")

        return txt

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_obj = self.pool.get('giscedata.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_id', False)

        config_vals = {
            'motiu': wizard.motive,
            'activacio': wizard.activacio,
            'data_accio': wizard.data_accio,
            'comentaris': wizard.comentaris
        }

        polissa = pol_obj.browse(cursor, uid, pol_id, context=context)
        nocutoff = polissa.nocutoff

        if wizard.motive == '01' and nocutoff:
            raise osv.except_osv(
                'Error usuari',
                _(
                    u"No es pot tramitar la baixa d'un contracte amb motiu de subministrament no tallable informat"
                )
            )

        direccio = polissa.get_address_with_phone(context=context)

        if not direccio:
            raise osv.except_osv(
                'Error usuari',
                _(
                    u"Falten dades. Camp no omplert: Telefon. S'ha de emplenar "
                    u"el telefon de la direccio de les dades fiscals"
                )
            )

        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, 'B1', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def onchange_motiu(self, cr, uid, ids, motiu, activacio, context=None):
        if not context:
            context = {}
        if motiu == '03':
            data = {'activacio': 'A'}
        else:
            data = {'activacio': activacio}
            create_date = datetime.today()
            if motiu in ['01', '04']:
                data_accio = workday(create_date, 5)
            else:  # motiu == '02'
                data_accio = workday(create_date, 15)
            data.update({'data_accio': data_accio.strftime("%Y-%m-%d")})
        return {'value': data}

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.switching',
            'report_name': 'giscedata.switching.info_cas_ATR',
            'report_webkit': "'giscedata_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'casos': casos_ids
            },
        }

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generats': fields.text('Casos generats'),
        'contract': fields.many2one('giscedata.polissa', 'Contracte'),
        # B1-01 specific data
        'motive': fields.selection(TABLA_10, 'Motiu de Baixa', requierd=True),
        'activacio': fields.selection(TABLA_8, u"Activació", required=True),
        'data_accio': fields.date(u"Data prevista de baixa"),
        'comentaris': fields.text('Comentari', size=4000),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_info_default,
        'whereiam': _whereiam,
        'casos_generats': lambda *a: json.dumps([]),
        'activacio': lambda *a: 'F',
        'data_accio': lambda *a: datetime.strftime(workday(datetime.today(), 15), '%Y-%m-%d'),
    }

GiscedataSwitchingWizardB101()
