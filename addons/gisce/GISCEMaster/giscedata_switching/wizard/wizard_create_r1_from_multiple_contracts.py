# -*- coding: utf-8 -*-
from datetime import datetime
import json

from osv import osv, fields, orm
from tools.translate import _
from ..giscedata_switching_r1 import get_type_subtype
from gestionatr.defs import TABLA_81
from gestionatr.input.messages.R1 import get_minimum_fields


class WizardR101FromMultipleContracts(osv.osv_memory):
    _name = 'wizard.r101.from.multiple.contracts'
    _description = 'Create r1 from multiple contracts'

    def _default_info(self, cursor, uid, context=None):
        pol_ids = context.get('contract_ids')

        if pol_ids:
            return _(u'Es genereran casos R1 per {0} pòlisses\n').format(
                len(pol_ids)
            )
        else:
            return _(u'No s\'han seleccionat pólisses')

    def _default_subtipus(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('subtipus_id', False)

    def onchange_subtipus(self, cursor, uid, ids, subtipus, context=None):
        if context is None:
            context = {}
        if not subtipus:
            return {}
        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        subinfo = subtipus_obj.read(
            cursor, uid, subtipus, ['type'], context=context
        )
        data = {
            'tipus': subinfo['type']
        }
        return {'value': data}

    def create_r1_from_contracts(self, cursor, uid, ids, context=None):
        polissa_ids = context.get('contract_ids', False)

        if context is None:
            context = {}

        if not polissa_ids:
            raise osv.except_osv('Error',
                                 _("No s'han seleccionat pólisses"))

        # {pol_id: fact_id}
        reclamacions_fact = context.get('reclamacio_num_factura', {})

        subtipus_obj = self.pool.get('giscedata.subtipus.reclamacio')
        imd_obj = self.pool.get('ir.model.data')
        wiz_obj = self.pool.get('wizard.subtype.r1')
        wiz_reads = self.read(
            cursor, uid, ids, ['subtipus_id', 'comentarios'], context=context
        )[0]
        subtipus_id = wiz_reads['subtipus_id']
        tipus = subtipus_obj.read(
            cursor, uid, subtipus_id, ['type'], context=context
        )['type']
        comments = wiz_reads['comentarios']
        subtipus_r6 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_subtipus_reclamacio', 'subtipus_reclamacio_006'
        )[1]
        subtipus_r36 = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_subtipus_reclamacio', 'subtipus_reclamacio_036'
        )[1]

        res = []
        results_info = []
        if polissa_ids:
            context.update({'subtipus_id': subtipus_id})
            if subtipus_id in [subtipus_r6, subtipus_r36]:
                return {
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'wizard.r101.from.contract',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': "{{'contract_id': {0}, 'contract_ids': {1}}}".format(
                        context.get('contract_id', 0),
                        context.get('contract_ids', [])
                    )
                }

            for pol_id in polissa_ids:
                wiz_values = wiz_obj.default_values_from_contract(
                    cursor, uid, pol_id, context=context
                )
                wiz_values.update({
                    'subtipus_id': subtipus_id,
                    'tipus': tipus,
                    'comentaris': comments,
                })

                if reclamacions_fact:
                    wiz_values.update(
                        {"reclamacio_num_factura": reclamacions_fact[
                            str(pol_id)]}
                    )

                wiz_values.update(context.get("extra_r1_vals", {}))
                cas_id = wiz_obj.action_create_r1_case_from_dict(
                    cursor, uid, pol_id, wiz_values, context=context
                )
                if cas_id:
                    results_info.append(cas_id)
                    res.append(cas_id[-1])

            return {
                'domain': "[('id','in', {0})]".format(str(res)),
                'name': _('Casos Creats'),
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'giscedata.switching',
                'type': 'ir.actions.act_window'
            }

    _columns = {
        'tipus': fields.selection(TABLA_81, u"Tipus", required=True),
        'subtipus_id': fields.many2one("giscedata.subtipus.reclamacio",
                                       u"Subtipus", required=True),
        'info': fields.text('Informació'),
        'comentarios': fields.text('Comentaris'),
    }

    _defaults = {
        'info': _default_info,
        'subtipus_id': _default_subtipus,
    }

WizardR101FromMultipleContracts()
