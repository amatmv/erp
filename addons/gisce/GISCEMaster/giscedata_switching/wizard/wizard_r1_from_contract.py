# -*- coding: utf-8 -*-
from datetime import datetime
import json

from osv import osv, fields, orm
from tools.translate import _
from ..giscedata_switching_r1 import get_type_subtype


class WizardR101FromContract(osv.osv_memory):
    """Wizard to generate R1 Cases from contracts.
    """
    _name = 'wizard.r101.from.contract'

    def get_contract_ids(self, context=None):
        """
        contract_ids may be passed as contract_id/contract_ids or
        active_id/active_ids context keys
        :param context: context with contract_id/contract_ids or
        active_id/active_ids keys
        :return: either (contract_id, contract_ids) or (active_id, active_ids)
        tuple
        """
        if not context:
            context = {}
        c_id = context.get('contract_id', context.get('active_id', 0))
        c_ids = context.get('contract_ids', context.get('active_ids', []))

        return c_id, c_ids

    def _get_multipol(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return len(self.get_contract_ids(context)[1]) > 1

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def _get_info_default(self, cursor, uid, context=None):
        if context is None:
            context = {}
        txt = _(u"Creació de casos R1 amb pas 01 de '{0}' contractes").format(
            len(self.get_contract_ids(context)[1])
        )

        return txt

    def get_default_comment(self, cursor, uid, proces, contract_id,
                            context=None):
        """
        Returns default comment depending on typs/subtype
        :param proces: R1-TTSSS where TT is type and SSS is subtype
        :param contract_id: contract id
        :return: The type/subtype comment with contract data
        """
        if context is None:
            context = {}

        contract_obj = self.pool.get('giscedata.polissa')

        r1_type, r1_subtype = get_type_subtype(proces)

        if r1_type == '02' and r1_subtype == '006':
            polissa = contract_obj.browse(cursor, uid, contract_id)
            ultima_lect = None
            if polissa.comptadors:
                comptador_id = polissa.comptadors[0].id
                lect_pool_obj = self.pool.get('giscedata.lectures.lectura.pool')

                orig_obj = self.pool.get('giscedata.lectures.origen_comer')
                ov_orig_ids = orig_obj.search(
                    cursor, uid, [('codi', '=', 'F1')]
                )

                search_params = [('comptador', '=', comptador_id),
                                 ('origen_comer_id', 'in', ov_orig_ids)]
                lectura_ids = lect_pool_obj.search(
                    cursor, uid, search_params, limit=1, order='name DESC')
                ultima_lectura_id = lectura_ids[0] if lectura_ids else []

                if ultima_lectura_id:
                    ultima_lect = lect_pool_obj.read(
                        cursor, uid, ultima_lectura_id, ['name'])['name']
                else:
                    ultima_lect = polissa.data_alta

            comment_tmpl = 'De este suministro no tenemos lecturas ' \
                           'desde {0}.\nEl Real Decreto 1718/2012 indica ' \
                           'claramente la obligación de tomar una ' \
                           'lectura real cada dos meses. Solicitamos ' \
                           'lecturas reales o, en caso contrario, una ' \
                           'copia del aviso de imposible lectura  ' \
                           'conforme no ha sido posible tomarla. \nGracias.'
            comment = comment_tmpl.format(ultima_lect)
        else:
            raise ValueError(_('Procés {0} no suportat').format(proces))
        return comment

    def get_config_vals_per_contract(self, cursor, uid, contract_id,
                                     context=None):
        """Returns a dict from wizard data compatible with
        giscedata_switching_r1_01.config_step function
            'type': R1-type i.e. 02,
            'subtype': R1-subtype i.e 36,
            'objector': Who objects i.e 06 (comer),
            'comments': comments on R1-01 step,
        :param contract_id: Selected contract
        :param context: context with proces name as R1-TTSS key
        """
        if context is None:
            context = {}

        proces = context.get('proces', 'R1-02006')

        r1_type, r1_subtype = get_type_subtype(proces)

        comments = self.get_default_comment(
            cursor, uid, proces, contract_id, context
        )

        # Always retailer
        objector = '06'

        config_vals = {
            'type': r1_type,
            'subtype': r1_subtype,
            'comments': comments,
            'objector': objector
        }
        config_vals.update(context.get("extra_values", {}))

        return config_vals

    def action_create_atr_case(self, cursor, uid, ids, context=None):
        """
        Creates a R1-01 step for every selected contract of selected
        type/subtype
        :param ids: wizard id
        :param context: 'proces' key with desired type/subtype in 'R1-ttss'
        format
        """
        if context is None:
            context = {}
        contract_ids = self.get_contract_ids(context)[1]
        wizard = self.browse(cursor, uid, ids[0], context)
        proces = context.get('proces', 'R1-02006')
        contract_obj = self.pool.get('giscedata.polissa')
        config_vals = {}

        info = _('Processats {0} contractes\n\n').format(len(contract_ids))
        cases_ids = []

        for contract_id in contract_ids:
            config_vals = self.get_config_vals_per_contract(
                cursor, uid, contract_id, context=context
            )
            res = contract_obj.crear_cas_atr(
                cursor, uid, contract_id, proces[2:], config_vals=config_vals,
                context=context
            )

            info += "%s: %s\n" % (res[0], res[1])
            if res[2]:
                cases_ids.append(res[2])

        json_cases = json.dumps(cases_ids)
        wizard.write({'info': info, 'state': 'done', 'proces': proces,
                      'generated_cases': json_cases})

    def action_cases(self, cursor, uid, ids, context=None):
        """Final wizard action.

        Returns generated cases list
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.generated_cases
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos {0} Creats').format(
                context.get('proces', 'R-0206')
            ),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'multipol': fields.boolean('Multipol'),
        'info': fields.text('Info'),
        'proces': fields.char('Procés', size=7),
        'generated_cases': fields.text('Casos generats'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'multipol': _get_multipol,
        'info': _get_info_default,
        'whereiam': _whereiam,
        'proces': lambda *a: 'R1-02006',
        'generated_cases': lambda *a: json.dumps([]),
    }

WizardR101FromContract()
