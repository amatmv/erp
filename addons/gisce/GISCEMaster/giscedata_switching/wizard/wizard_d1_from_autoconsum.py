# -*- coding: utf-8 -*-
import json

from osv import osv, fields
from tools.translate import _
from gestionatr.defs import TABLA_109, TABLA_108, TABLA_113

MOTIU_CANVI_AUTOCONSUM = TABLA_109[3:]
TIPUS_AUTOCONSUM = TABLA_113[5:]

class GiscedataSwitchingWizardD101(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.switching.d101.wizard'

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Generació de D1-01\n")

        return txt

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')

        # Created cases
        casos_ids = []

        autoconsum_id = context.get('autoconsum_id', False)
        cups_ids = autoconsum_obj.read(cursor, uid, autoconsum_id, ['cups_id'])['cups_id']

        if wizard.motiu_canvi == '06':
            is_collectiu = autoconsum_obj.read(cursor, uid, autoconsum_id, ['collectiu'])['collectiu']
            if not is_collectiu:
                raise osv.except_osv(_('No és un autoconsum col·lectiu'),
                                     _(u"No es pot modificar el coeficient de repartiment de "
                                       u"l'autoconsum ja que aquest no és col·lectiu i per tant "
                                       u"sempre serà un coeficient de 1.0"))
        if not cups_ids:
            raise osv.except_osv(_("No s'ha trobat CUPS"),
                                 _(u"No s'ha trobat cap CUPS amb aquest autoconsum associat. Abans de crear "
                                   u"el cas D1 hauria d'associar el mateix al CUPS pertinent des del "
                                   u"formulari del CUPS."))

        info = ''
        for cups_id in cups_ids:
            polissa_id = cups_obj.read(cursor, uid, cups_id, ['polissa_polissa'])['polissa_polissa']

            config_vals = {
                'autoconsum_id': autoconsum_id,
                'motiu_canvi': wizard.motiu_canvi,
                'cups_id': cups_id,
            }

            if wizard.data_activacio:
                config_vals.update({'data_activacio': wizard.data_activacio})

            if wizard.periodicitat_facturacio:
                config_vals.update({'periodicitat_facturacio': wizard.periodicitat_facturacio})

            res = polissa_obj.crear_cas_atr(
                cursor, uid, polissa_id, 'D1', config_vals=config_vals, context=context
            )

            info += "%s: %s\n" % (res[0], res[1])
            if res[2]:
                casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.switching',
            'report_name': 'giscedata.switching.info_cas_ATR',
            'report_webkit': "'giscedata_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'casos': casos_ids
            },
        }

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'motiu_canvi': fields.selection(
            MOTIU_CANVI_AUTOCONSUM, u'Motiu Canvi', required=True
        ),
        'periodicitat_facturacio': fields.selection(
            TABLA_108, u"Periodicitat de facturació", size=2
        ),
        'data_activacio': fields.date(u"Data prevista d'activació"),
        'info': fields.text('Info'),
        'casos_generats': fields.text('Casos generats'),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_info_default,
        'whereiam': _whereiam,
        'casos_generats': lambda *a: json.dumps([]),
        'motiu_canvi': lambda *a: '04',
    }


GiscedataSwitchingWizardD101()
