# -*- coding: utf-8 -*-
import json

from osv import osv, fields, orm
from tools.translate import _
from gestionatr.defs import TABLA_8


class GiscedataSwitchingWizardC101(osv.osv_memory):
    """Wizard pel switching
    """
    _name = 'giscedata.switching.c101.wizard'

    def _get_info_default(self, cursor, uid, context=None):
        if not context:
            context = {}
        txt = _(u"Generació de C1-01\n")

        return txt

    def _whereiam(self, cursor, uid, context=None):
        """ returns 'distri' or 'comer' """
        cups_obj = self.pool.get('giscedata.cups.ps')
        sw_obj = self.pool.get('giscedata.switching')

        cups_id = cups_obj.search(cursor, uid, [], 0, 1)[0]
        return sw_obj.whereiam(cursor, uid, cups_id)

    def genera_casos_atr(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)

        pol_obj = self.pool.get('giscedata.polissa')
        # Created cases
        casos_ids = []
        pol_id = context.get('contract_id', False)
        config_vals = {
            'activacio_cicle': wizard.activacio_cicle,
            'data_accio': wizard.data_accio,
            'comentaris': wizard.comentaris
        }

        info = ''
        res = pol_obj.crear_cas_atr(
            cursor, uid, pol_id, 'C1', config_vals=config_vals, context=context
        )

        info += "%s: %s\n" % (res[0], res[1])
        if res[2]:
            casos_ids.append(res[2])

        casos_json = json.dumps(casos_ids)
        wizard.write({'info': info, 'state': 'done',
                      'casos_generats': casos_json})

    def action_casos(self, cursor, uid, ids, context=None):
        """Acció final del wizard.

        Retornem els casos creats per repassar-los
        """
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        return {
            'domain': "[('id','in', %s)]" % str(casos_ids),
            'name': _('Casos Creats'),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'giscedata.switching',
            'type': 'ir.actions.act_window'
        }

    def print_report(self, cursor, uid, ids, context=None):
        wiz = self.browse(cursor, uid, ids[0])
        casos_str = wiz.casos_generats
        casos_ids = json.loads(casos_str)
        if not context:
            context = {}
        return {
            'type': 'ir.actions.report.xml',
            'model': 'giscedata.switching',
            'report_name': 'giscedata.switching.info_cas_ATR',
            'report_webkit': "'giscedata_switching/report/info_cas_ATR.mako'",
            'webkit_header': 'report_sense_fons',
            'groups_id': casos_ids,
            'multi': '0',
            'auto': '0',
            'header': '0',
            'report_rml': 'False',
            'datas': {
                'casos': casos_ids
            },
        }

    _columns = {
        'state': fields.char('State', size=16),
        'whereiam': fields.char('Whereiam', size=6),
        'info': fields.text('Info'),
        'casos_generats': fields.text('Casos generats'),
        'contract': fields.many2one('giscedata.polissa', 'Contracte'),
        # C1-01 specific data
        'activacio_cicle': fields.selection(TABLA_8, u"Activació", required=True),
        'data_accio': fields.date(u"Data prevista d'activació"),
        'comentaris': fields.text('Comentari', size=4000),
    }

    _defaults = {
        'state': lambda *a: 'init',
        'info': _get_info_default,
        'whereiam': _whereiam,
        'casos_generats': lambda *a: json.dumps([]),
        'activacio_cicle': lambda *a: 'A',
    }

GiscedataSwitchingWizardC101()
