# -*- coding: utf-8 -*-
from enerdata.contracts.normalized_power import NormalizedPower
from enerdata.contracts.tariff import get_tariff_by_code, are_powers_ascending
from enerdata.contracts.tariff import (
    NotNormalizedPower, IncorrectMaxPower, IncorrectPowerNumber
)

from osv import osv
from .utils import *
from gestionatr.defs import *
from gestionatr.input.messages.R1 import *
from gestionatr.input.messages.A1 import *
import vatnumber
import base64
from datetime import date


class Rebuig(object):

    def check_1(self, pool, cursor, uid, sw):
        cups_obj = pool.get('giscedata.cups.ps')
        if sw.proces_id.name == 'A1':
            cups_list = sw.cups_input.split(',')
            cups_id = cups_obj.search(cursor, uid, [
                ('name', 'in', cups_list)
            ])
        else:
            cups_id = cups_obj.search(cursor, uid, [
                ('name', '=', sw.cups_input)
            ])
        return len(cups_id) > 0

    def check_2(self, pool, cursor, uid, sw):
        if sw.cups_id:
            if sw.cups_id.polissa_polissa:
                return True
            else:
                return False
        else:
            return True

    def check_3(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw.cups_polissa_id:
            if sw.proces_id.name in ['M1', 'C2'] \
                    and sw2.sollicitudadm in ['S', 'A']:
                return True
            else:
                if sw2.codi_document:
                    prefix = ''
                    if sw2.tipus_document in ['NI', 'NE']:
                        prefix = 'ES'
                    elif sw2.tipus_document == 'PS':
                        prefix = 'PS'
                    codi = '{0}{1}'.format(prefix, sw2.codi_document)
                    return sw.cups_polissa_id.titular.vat == codi
        return True

    def check_7(self, pool, cursor, uid, sw):
        return bool(sw.get_pas().tipus_contracte)

    def check_8(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw2.tipus_contracte in ['02', '03', '09']:
            return bool(sw2.data_final)
        return True

    def check_11(self, pool, cursor, uid, sw):
        if sw.cups_id and sw.cups_id.polissa_polissa:
            if sw.proces_id.name in ['C1', 'C2']:
                return sw.comer_sortint_id != sw.partner_id
            elif sw.proces_id.name == 'R1':
                if sw.get_pas().subtipus_id.type in ['02']:
                    comer_ids = []
                    for modcon in sw.cups_polissa_id.modcontractuals_ids:
                        cid = modcon.comercialitzadora and modcon.comercialitzadora.id
                        comer_ids.append(cid)
                    return sw.partner_id.id in comer_ids
                else:
                    return True
            else:  # sw.proces_id.name in ['M1', 'B1']
                return sw.comer_sortint_id == sw.partner_id
        else:
            return True

    def check_12(self, pool, cursor, uid, sw):
        if sw.proces_id.name == 'M1'and sw.get_pas().sollicitudadm == 'S' and sw.get_pas().canvi_titular in ['T', 'S']:
            return True
        return sw.cups_polissa_id.state != 'tall'

    def check_14(self, pool, cursor, uid, sw):
        tariff_obj = pool.get('giscedata.polissa.tarifa')

        sw02 = sw.get_pas()

        if sw02.tarifaATR:
            tar_id = tariff_obj.get_tarifa_from_ocsum(
                cursor, uid, sw02.tarifaATR
            )
            tariff = tariff_obj.browse(cursor, uid, tar_id)
            try:
                powers = [pot.potencia / 1000.0 for pot in sw02.pot_ids]
                tarif_eval = get_tariff_by_code(tariff.name)()
                tarif_eval.evaluate_powers(powers)
            except IncorrectMaxPower:
                return False
            except:
                return True
        return True

    def check_15(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            sw2 = sw.get_pas()
            if (sw2.tarifaATR == "003" or
               (not sw2.tarifaATR and sw.cups_polissa_id.tarifa.codi_ocsum == "003")):
                return True
            powers = [pot.potencia / 1000.0 for pot in sw2.pot_ids]
            if sw2.tarifaATR:
                tariff_obj = pool.get('giscedata.polissa.tarifa')
                tar_id = tariff_obj.get_tarifa_from_ocsum(
                    cursor, uid, sw2.tarifaATR
                )
                tariff = tariff_obj.browse(cursor, uid, tar_id)
                num_periods = tariff.get_num_periodes(tipus='tp')
            else:
                num_periods = sw.cups_polissa_id.tarifa.get_num_periodes(
                    tipus='tp'
                )
            if sum(powers[num_periods:]) != 0:
                num_periods = len(powers)
            return are_powers_ascending(powers[:num_periods])
        return True

    def check_17(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            sw2 = pool.get('giscedata.switching').get_pas(cursor, uid, sw)
            tipus_tensio = sw.cups_polissa_id.tensio_normalitzada.tipus
            tarifa_ids = pool.get('giscedata.polissa.tarifa').search(
                cursor, uid, [('codi_ocsum', '=', sw2.tarifaATR)]
            )
            if len(tarifa_ids) > 0:
                for tarifa_id in tarifa_ids:
                    tipus_tarifa = pool.get('giscedata.polissa.tarifa').read(
                        cursor, uid, tarifa_id, ['tipus', 'codi_ocsum']
                    )
                    if tipus_tensio == tipus_tarifa['tipus']:
                        return True
                return False
        return True

    def check_21(self, pool, cursor, uid, sw):
        if sw.cups_id and sw.cups_id.polissa_polissa:
            return False
        return True

    def check_23(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw.proces_id.name == 'A3':
            return sw2.cnae
        return True

    def check_32(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw.cups_polissa_id.nocutoff and sw2.motiu in ['03', '04']:
            if sw.cups_polissa_id.nocutoff.boe_check:
                return False
        return True

    def check_34(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw.cups_polissa_id.nocutoff and sw2.motiu in ['03', '04']:
            if not sw.cups_polissa_id.nocutoff.boe_check:
                return False
        return True

    def check_35(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        codi = 'ES{0}'.format(sw2.codi_document)
        if not sw.cups_id:
            return True
        elif sw2.motiu != '03':
            return True
        else:
            no_tallat = True
            for p in sw.cups_id.polisses:
                if p.titular.vat == codi and p.state == 'baixa':
                    no_tallat = False
                elif p.titular.vat == codi and p.state == 'actiu':
                    return True
            return no_tallat

    def check_36(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw.proces_id.name == 'R1' and not sw2.codi_document:
            return True
        codi = 'ES{0}'.format(sw2.codi_document)
        return vatnumber.check_vat(codi)

    def check_37(self, pool, cursor, uid, sw):
        if sw.cups_id:
            sw_obj = pool.get('giscedata.switching')
            ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            for cas_id in ids:
                sw2 = sw_obj.browse(cursor, uid, cas_id)
                if sw2.proces_id.name == 'A3' and sw2.id != sw.id:
                    return False
        return True

    def check_38(self, pool, cursor, uid, sw):
        if sw.cups_id:
            sw_obj = pool.get('giscedata.switching')
            ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            for cas_id in ids:
                sw2 = sw_obj.browse(cursor, uid, cas_id)
                if sw2.proces_id.name == 'C1' and sw2.id != sw.id:
                    return False
        return True

    def check_39(self, pool, cursor, uid, sw):
        if sw.cups_id:
            sw_obj = pool.get('giscedata.switching')
            ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            for cas_id in ids:
                sw2 = sw_obj.browse(cursor, uid, cas_id)
                if sw2.proces_id.name == 'C2' and sw2.id != sw.id:
                    return False
        return True

    def check_40(self, pool, cursor, uid, sw):
        if sw.cups_id:
            sw_obj = pool.get('giscedata.switching')
            ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            for cas_id in ids:
                sw2 = sw_obj.browse(cursor, uid, cas_id)
                if sw2.proces_id.name == 'M1' and sw2.id != sw.id:
                    return False
        return True

    def check_44(self, pool, cursor, uid, sw):
        step01 = sw.get_pas()
        if step01.tipus_contracte == '09':
            if not step01.expected_consumption:
                return False

        return True

    def check_46(self, pool, cursor, uid, sw):
        sw02 = sw.get_pas()
        if sw.data_sollicitud and sw02.data_accio:
            return sw.data_sollicitud <= sw02.data_accio
        return True

    def check_52(self, pool, cursor, uid, sw):
        tariff_obj = pool.get('giscedata.polissa.tarifa')
        sw02 = sw.get_pas()
        if sw02.tarifaATR:
            tar_id = tariff_obj.get_tarifa_from_ocsum(
                cursor, uid, sw02.tarifaATR)
            tariff = tariff_obj.browse(cursor, uid, tar_id)
            num_periods = tariff.get_num_periodes(tipus='tp')
            powers = []
            try:
                powers = [pot.potencia/1000.0 for pot in sw02.pot_ids]
                tarif_eval = get_tariff_by_code(tariff.name)()
                if sum(powers[num_periods:]) != 0:
                    num_periods = len(powers)
                tarif_eval.evaluate_powers(powers[:num_periods])
            except NotNormalizedPower:
                return True
            except IncorrectPowerNumber:
                # If perque la CNMC resulta que diu que en una tarifa 2.0 DHA que te 2 periodes de energia i
                # 1 de potencia es pot enviar dos periodes de potencia i que ens ho hem de menjar peruqe sino
                # iberdrola es queixa
                if sw02.tarifaATR == "004" and len(powers) == 2:
                    return True
                else:
                    return False
            except:
                return False
        return True

    def check_55(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            sw1 = pool.get('giscedata.switching')
            sw2 = sw1.get_pas(cursor, uid, sw)
            if sw2.sollicitudadm in ['S', 'A'] and sw2.canvi_titular in ['T', 'S']:
                codi = 'ES{0}'.format(sw2.codi_document)
                return codi != sw.cups_polissa_id.titular.vat
        return True

    def check_56(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            sw1 = pool.get('giscedata.switching')
            sw2 = sw1.get_pas(cursor, uid, sw)
            if sw2.sollicitudadm in ['S', 'A']:
                return sw2.canvi_titular in dict(TABLA_53)
        return True

    def check_65(self, pool, cursor, uid, sw):
        if sw.cups_id and sw.cups_id.polissa_polissa:
            sw1 = pool.get('giscedata.switching')
            sw2 = sw1.get_pas(cursor, uid, sw)
            data_accio = date.today().strftime('%Y-%m-%d')
            if sw2.data_accio:
                data_accio = sw2.data_accio
            if sw2.sollicitudadm != 'S':
                end_date = datetime.strptime(data_accio, '%Y-%m-%d')
                start_date = end_date - relativedelta(years=1)
                context = {'ffields': ['potencies_periode']}
                s_date_str = start_date.strftime("%Y-%m-%d")
                e_date_str = end_date.strftime("%Y-%m-%d")
                modif = sw.cups_id.polissa_polissa.get_modcontractual_intervals(
                    s_date_str, e_date_str, context=context
                )
                for mod in modif:
                    if modif[mod]['changes']:
                        return False
        return True

    def check_72(self, pool, cursor, uid, sw):
        if sw.cups_id:
            return bool(sw.cups_id.polissa_polissa)

        return True

    def check_73(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            return sw.cups_polissa_id.potencia <= 15

        return True

    def check_74(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            return len(sw.cups_polissa_id.comptadors) == 1

        return True

    def check_75(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            lec_r = 0
            lec_a = 0
            lec_p = 0
            n_peri_a = sw.cups_polissa_id.tarifa.get_num_periodes(tipus='te')
            n_peri_r = 0
            n_peri_p = 0
            for lectura in sw.get_pas().lect_ids:
                if lectura.magnitud == 'AE':
                    lec_a += 1
                elif lectura.magnitud == 'R1':
                    lec_r += 1
                    n_peri_r = sw.cups_polissa_id.tarifa.get_num_periodes(
                        tipus='te')
                else:
                    lec_p += 1
                    n_peri_p = sw.cups_polissa_id.tarifa.get_num_periodes(
                        tipus='tp')

            # Tariff 3.1A can be correct with both 3 and 6 lectures
            if sw.cups_polissa_id.tarifa.name == '3.1A':
                return lec_a == 3 or lec_a == 6 and n_peri_r == lec_r \
                                     and n_peri_p == lec_p

            return n_peri_a == lec_a and n_peri_r == lec_r and n_peri_p == lec_p

        return True

    def check_76(self, pool, cursor, uid, sw):
        comptadors = sw.cups_polissa_id.comptadors

        if sw.cups_polissa_id and len(comptadors) == 1:
            giro = comptadors[0].giro

            for lect in sw.get_pas().lect_ids:
                if lect.lectura >= giro:
                    return False

        return True

    def check_79(self, pool, cursor, uid, sw):
        nova_lectura = sw.get_pas().data_lectura

        if sw.cups_polissa_id and len(sw.cups_polissa_id.comptadors) > 0:
            comptador = sw.cups_polissa_id.comptadors[0]
            tarifa_id = sw.cups_polissa_id.tarifa.id

            # We get all the 'lectures' with a date after the new one
            lectures = comptador.get_lectures(
                tarifa_id, from_date=nova_lectura, per_facturar=True
            )

            # And we check that we don't have 'lectures' for any periode
            for lect in lectures:
                if 'name' in lectures[lect]['actual']:
                    return False

        return True

    def check_80(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            return sw.cups_polissa_id.facturacio_potencia != 'max'

        return True

    def check_84(self, pool, cursor, uid, sw):
        if not sw.cups_polissa_id:
            return True
        sw_obj = pool.get('giscedata.switching')
        params = [
            ('company_id', '=', sw.company_id.id),
            ('codi_sollicitud', '!=', sw.codi_sollicitud),
            ('cups_id', '=', sw.cups_id.id),
            ('cups_input', '=', sw.cups_input),
            ('proces_id', '=', sw.proces_id.id),
            ('step_id', '=', sw.step_id.id),
            ('cups_polissa_id', '=', sw.cups_polissa_id.id),
            ('ref_contracte', '=', sw.ref_contracte),
            ('comer_sortint_id', '=', sw.comer_sortint_id.id),
            ('nif_titular_polissa', '=', sw.cups_polissa_id.titular.vat)
        ]
        sw_id = sw_obj.search(cursor, uid, params)
        if not sw_id:
            return True
        sw2 = sw.get_pas()
        r_obj = pool.get('giscedata.switching.r1.01')
        params = [
            ('tipus', '=', sw2.tipus),
            ('subtipus_id', '=', sw2.subtipus_id.id),
            ('reforigen', '=', sw2.reforigen),
            ('tipus_document', '=', sw2.tipus_document),
            ('codi_document', '=', sw2.codi_document),
            ('fiscal_address_id', '=', sw2.fiscal_address_id.id),
            ('tipus_reclamant', '=', sw2.tipus_reclamant),
            ('rec_tipus_document', '=', sw2.rec_tipus_document),
            ('rec_codi_document', '=', sw2.rec_codi_document),
        ]
        r_id = r_obj.search(cursor, uid, params)
        if not r_id:
            return True
        for r in sw2.reclamacio_ids:
            reclam_obj = pool.get('giscedata.switching.reclamacio')
            params = [
                ('num_expedient_escomesa', '=', r.num_expedient_escomesa),
                ('data_incident', '=', r.data_incident),
                ('num_factura', '=', r.num_factura),
                ('tipus_concepte_facturat', '=', r.tipus_concepte_facturat),
                ('codi_incidencia', '=', r.codi_incidencia),
                ('codi_sollicitud', '=', r.codi_sollicitud),
                ('parametre_contractacio', '=', r.parametre_contractacio),
                ('concepte_disconformitat', '=', r.concepte_disconformitat),
                ('iban', '=', r.iban),
                ('codi_sollicitud_reclamacio', '=', r.codi_sollicitud_reclamacio
                 ),
                ('data_inici', '=', r.data_inici),
                ('data_fins', '=', r.data_fins),
                ('import_reclamat', '=', r.import_reclamat),
                ('codi_dh', '=', r.codi_dh),
                ('cont_nom', '=', r.cont_nom),
            ]
            reclam_id = reclam_obj.search(cursor, uid, params)
            if not reclam_id:
                return True
            for l in r.lect_ids:
                lect_obj = pool.get('giscedata.switching.lectura')
                params = [
                    ('name', '=', l.name),
                    ('magnitud', '=', l.magnitud),
                    ('lectura', '=', l.lectura),
                ]
                lect_id = lect_obj.search(cursor, uid, params)
                if not lect_id:
                    return True
        return False

    def check_85(self, pool, cursor, uid, sw):
        # Obtenim el xml adjuntat al importar-se r101
        attach_obj = pool.get('ir.attachment')
        attach_ids = attach_obj.search(
            cursor, uid, [
                ('res_id', '=', sw.id),
                ('res_model', '=', 'giscedata.switching')
            ]
        )
        xml = attach_obj.browse(cursor, uid, attach_ids[0])
        xml = base64.b64decode(xml.datas)
        # Creem l'objecte R1() amb el xml obtingut
        if sw.proces_id.name == 'A1':
            sw101 = A1(xml)
        else:
            sw101 = R1(xml)

        sw101.set_xsd()
        sw101.parse_xml()
        # Amb aquest objecte podem construir un "checker" i
        # comprovar els camps minims de un R1-01
        min_camps_no_trobats = sw101.check_minimum_fields()
        if not min_camps_no_trobats:
            return True
        else:
            return False

    def check_87(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw2.subtipus_id.name in ['007', '008', '009', '010', '011', '012',
                                    '036', '037', '040', '047', '055', '056']:
            for reclamacio in sw2.reclamacio_ids:
                if not reclamacio.num_factura:
                    return False
                factura_obj = pool.get('account.invoice')
                params = [('number', '=', reclamacio.num_factura)]
                fact_ids = factura_obj.search(cursor, uid, params)
                if len(fact_ids) == 0:
                    return False
        return True

    def check_88(self, pool, cursor, uid, sw):
        sw2 = sw.get_pas()
        if sw2.subtipus_id.name == '047':
            for reclamacio in sw2.reclamacio_ids:
                if not reclamacio.num_factura:
                    return True
                account_obj = pool.get('account.invoice')
                params = [('number', '=', reclamacio.num_factura)]
                account_ids = account_obj.search(cursor, uid, params)
                account = account_obj.browse(cursor, uid, account_ids[0])

                factura_obj = pool.get('giscedata.facturacio.factura')
                params = [('invoice_id', '=', account.id)]
                factura_id = factura_obj.search(cursor, uid, params)
                factura = factura_obj.browse(cursor, uid, factura_id[0])
                if factura.polissa_tg != '1':
                    return False
        return True

    def check_B5(self, pool, cursor, uid, sw):
        if sw.cups_id:
            b101_obj = pool.get("giscedata.switching.b1.01")
            sw_obj = pool.get('giscedata.switching')
            other_sw_ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            pas_b101_ids = b101_obj.search(cursor, uid, [
                ('header_id.sw_id', 'in', other_sw_ids),
                ('header_id.sw_id', '!=', sw.id),
                ('motiu', '=', '01')
            ])
            return not len(pas_b101_ids)
        return True

    def check_B6(self, pool, cursor, uid, sw):
        if sw.cups_id:
            b101_obj = pool.get("giscedata.switching.b1.01")
            sw_obj = pool.get('giscedata.switching')
            other_sw_ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            pas_b101_ids = b101_obj.search(cursor, uid, [
                ('header_id.sw_id', 'in', other_sw_ids),
                ('header_id.sw_id', '!=', sw.id),
                ('motiu', '=', '02')
            ])
            return not len(pas_b101_ids)
        return True

    def check_B7(self, pool, cursor, uid, sw):
        if sw.cups_id:
            if sw.proces_id.name in ['C1', 'C2']:
                cn01 = sw.get_pas()
                if cn01.contratacion_incondicional_ps == 'S':
                    return True
            elif sw.proces_id.name == 'B1':
                b101 = sw.get_pas()
                if b101.motiu == '04':
                    return True
            elif sw.proces_id.name == 'M1':
                m101 = sw.get_pas()
                if m101.sollicitudadm in ['S'] and sw.cups_polissa_id and sw.cups_polissa_id.state == 'tall':
                    return True
            sw_obj = pool.get('giscedata.switching')
            other_sw_ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            b101_obj = pool.get("giscedata.switching.b1.01")
            pas_b101_ids = b101_obj.search(cursor, uid, [
                ('header_id.sw_id', 'in', other_sw_ids),
                ('header_id.sw_id', '!=', sw.id),
                ('motiu', '=', '03')
            ])
            return not len(pas_b101_ids)
        return True

    def check_B8(self, pool, cursor, uid, sw):
        if sw.cups_id:
            if sw.proces_id.name in ['C1', 'C2']:
                cn01 = sw.get_pas()
                if cn01.contratacion_incondicional_ps == 'S':
                    return True

            sw_obj = pool.get('giscedata.switching')
            other_sw_ids = sw_obj.get_casos_actiu_cups(cursor, uid, sw.cups_id.id)
            b101_obj = pool.get("giscedata.switching.b1.01")
            pas_b101_ids = b101_obj.search(cursor, uid, [
                ('header_id.sw_id', 'in', other_sw_ids),
                ('header_id.sw_id', '!=', sw.id),
                ('motiu', '=', '04')
            ])
            return not len(pas_b101_ids)
        return True

    def check_D1(self, pool, cursor, uid, sw):
        if sw.cups_polissa_id:
            pas = sw.get_pas()
            if pas.contratacion_incondicional_bs == 'N' and sw.cups_polissa_id.bono_social == '1':
                return False
        return True

    def check_D3(self, pool, cursor, uid, sw):
        contract = sw.cups_polissa_id
        meter_obj = pool.get('giscedata.lectures.comptador')
        has_tg = meter_obj.fields_get(cursor, uid).get('tg', False)

        if contract and contract.comptadors and has_tg:
            if getattr(contract.comptadors[0], 'tg'):
                tg_meters = [m for m in contract.comptadors if m.tg]
                if not tg_meters:
                    return True

        pas01 = sw.get_pas()
        if pas01._nom_pas != '01':
            return True

        np = NormalizedPower()
        for power in pas01.pot_ids:
            if power.potencia > 15000 or power.potencia == 0:
                return True
            if not np.is_normalized(power.potencia):
                return False

        return True
