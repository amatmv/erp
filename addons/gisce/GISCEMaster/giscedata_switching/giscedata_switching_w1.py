# -*- coding: utf-8 -*-

from osv import osv, fields, orm
from gestionatr.output.messages import sw_w1 as w1
from tools.translate import _
from gestionatr.defs import *
import pooler
from datetime import datetime


class GiscedataSwitchingProcesW1(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        '''returns W1 inital steps depending on where we are'''
        if proces == 'W1':
            return ['01']

        return super(GiscedataSwitchingProcesW1,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        '''returns W1 emisor steps depending on where we are'''
        if proces == 'W1':
            if where == 'comer':
                return ['01']
            elif where == 'distri':
                return ['02']

        return super(GiscedataSwitchingProcesW1,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == 'W1':
            return ['02']

        return super(GiscedataSwitchingProcesW1,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

GiscedataSwitchingProcesW1()


class GiscedataSwitchingW1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        """ Check if the case has arrived to the end or not """

        if sw.proces_id.name == 'W1':
            #Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02':
                    return True

        return super(GiscedataSwitchingW1,
                     self).get_final(cursor, uid, sw, context=context)

GiscedataSwitchingW1()


class GiscedataSwitchingW1_01(osv.osv):
    """ Classe pel pas 01 """
    _name = "giscedata.switching.w1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'W1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """ returns next valid steps depending where we are """
        return ['02']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingW1_01,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML W1, pas 01
        """

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        lect_obj = self.pool.get('giscedata.switching.lectura')

        pas = self.browse(cursor, uid, pas_id, context)

        sw = pas.sw_id
        msg = w1.MensajeSolicitudAportacionLectura()
        #capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # DatosSolicitudAportacionLectura
        datos_solicitud = w1.DatosSolicitudAportacionLectura()
        datos_solicitud_aportacion_lectura_fields = {
            'fecha_lectura': pas.data_lectura,
            'tipo_dhedm': pas.codi_dh,
        }
        datos_solicitud.feed(datos_solicitud_aportacion_lectura_fields)
        #lecturas
        lecturas = lect_obj.generar_xml(cursor, uid, pas, context=context)

        msg.feed({
            'cabecera': capcalera,
            'datos_solicitud_aportacion_lectura': datos_solicitud,
            'lectura_aportada_list': lecturas,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return fname, str(msg)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        lect_obj = self.pool.get('giscedata.switching.lectura')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        lecturas_vals = []
        codi_dh = xml.datos_solicitud_aportacion_lectura.tipo_dhedm
        data_lect = xml.datos_solicitud_aportacion_lectura.fecha_lectura
        for lect in xml.lecturas_aportadas:
            xml_period = lect.tipo_codigo_periodo_dh
            period = xml_period[-1]
            if period == '0':
                if xml_period == '10':
                    # P1 de 2.0A
                    period = '1'
                elif xml_period == '00' and codi_dh == '1':
                    # FENOSA 2.0A amb periode 00
                    period = '1'
            # P2 de 2.0 DHA (Iberdrola)
            if xml_period == '03':
                period = '2'
            lecturas_vals.append(
                (
                    'P%s' % period,
                    lect.integrador,
                    float(lect.lectura_propuesta)
                )
            )

        lect_ids = lect_obj.create_lects(cursor, uid, lecturas_vals)

        vals.update({
            # sol·licitud
            'data_lectura': data_lect,
            'codi_dh': codi_dh,
            'lect_ids': [(6, 0, lect_ids)],
        })

        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        lect_obj = self.pool.get('giscedata.switching.lectura')
        measure_obj = self.pool.get('giscedata.lectures.lectura.pool')
        per_obj = self.pool.get('giscedata.polissa.tarifa.periodes')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        self_meterings = polissa.get_last_selfmetering()
        lectures = []
        if self_meterings:
            measures = measure_obj.read(cursor, uid, self_meterings,
                                        ['periode', 'lectura', 'tipus', 'name'])
            for measure in measures:
                per_id = measure['periode'][0]
                per_vals = per_obj.read(cursor, uid, per_id, ['name'])
                per = per_vals['name']
                tipus = measure['tipus']
                lectura = measure['lectura']
                measure_date = measure['name']
                if tipus == 'A':
                    lectures.append((per, 'AE', lectura))
            data_lectura = datetime.strptime(measure_date, '%Y-%m-%d')
        else:
            periodes = polissa.tarifa.get_num_periodes()
            for per in range(0, periodes):
                lectures.append(('P%s' % (per + 1), 'AE', 0.0))
            data_lectura = datetime.now().date()

        lect_ids = lect_obj.create_lects(cursor, uid, lectures)

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({'data_lectura': data_lectura,
                     'codi_dh': polissa.tarifa.get_codi_dh(),
                     'lect_ids': [(6, 0, lect_ids)],
                     })
        return self.create(cursor, uid, vals, context=context)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        step = self.read(cursor, uid, step_id, ['data_lectura'])['data_lectura']
        return _(u"Data lectura: {0}").format(step)
 
    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_W1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'data_lectura': fields.date(u"Data lectura"),
        'codi_dh': fields.selection(TIPO_DH_APARATO, u"Codi DH"),
    }

GiscedataSwitchingW1_01()


class GiscedataSwitchingW1_02(osv.osv):
    """Classe pel pas 02 """

    _name = "giscedata.switching.w1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'W1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """ returns next valid steps depending where we are """
        # final step
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingW1_02,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        # Accepted
        vals.update({
            'rebuig': False,
            'data_resposta': xml.datos_aceptacion_lectura.fecha_aceptacion,
        })
        pas_id = self.create(cursor, uid, vals, context=context)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        """ Retorna la sol·licitud en format XML W1, pas 02 """

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id
        msg = w1.AceptacionAportacionLectura()
        #capcalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades acceptació
        dades_accept = w1.DatosAceptacionLectura()
        dades_accept.feed({'fecha_aceptacion': pas.data_resposta})

        msg.feed({
            'cabecera': capcalera,
            'datos_aceptacion_lectura': dades_accept,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return fname, str(msg)

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML W1, pas 02
           de rebuig
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML W1, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML W1, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion_lectura:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """ create with dummy default values for fields in process step """

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        vals.update({
            'data_resposta': today,
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """ Retorna un text amb els motius del rebuig """
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        if step['rebuig']:
            return _(u"Rebuig: {0}").format(step['motiu_rebuig'])
        else:
            sw_id = step['sw_id'][0]
            step01 = self.pool.get('giscedata.switching.w1.01')
            step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
            if step01_id:
                return step01.get_additional_info(cursor, uid, step01_id[0])
            else:
                sw = self.pool.get('giscedata.switching')
                return sw.read(cursor, uid, sw_id, ['additional_info'])
 
    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_W1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades comunes
        'data_resposta': fields.date(u"Data resposta"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
    }

GiscedataSwitchingW1_02()
