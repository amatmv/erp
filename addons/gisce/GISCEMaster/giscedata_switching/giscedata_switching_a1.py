# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields
from gestionatr.output.messages import sw_a1 as a1
from tools.translate import _
from gestionatr.defs import *
from datetime import datetime, timedelta


class GiscedataSwitchingProcesA1(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        """returns A1 inital steps depending on where we are"""
        if proces == 'A1':
            return ['01']

        return super(GiscedataSwitchingProcesA1, self).get_init_steps(cursor, uid, proces, where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        """returns A1 emisor steps depending on where we are"""
        if proces == 'A1':
            if where == 'distri':
                return ['02']
            else:
                return []

        return super(GiscedataSwitchingProcesA1, self).get_emisor_steps(cursor, uid, proces, where, context=context)


GiscedataSwitchingProcesA1()


class GiscedataSwitchingA1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        """Check if the case has arrived to the end or not"""

        if sw.proces_id.name == 'A1':
            # Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02' and sw.whereiam == 'distri':
                    if not step.pas_id:
                        continue
                    model, pas_id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(pas_id))
                    if pas.rebuig:
                        return True

        return super(GiscedataSwitchingA1, self).get_final(cursor, uid, sw, context=context)


GiscedataSwitchingA1()


class GiscedataSwitchingA1_01(osv.osv):
    """Classe pel pas 01"""

    _name = "giscedata.switching.a1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A1', self._nom_pas, context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return ['02']

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        self.config_step_validation(cursor, uid, ids, vals, context=context)
        self.write(cursor, uid, ids, vals, context=context)
        return True

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA1_01,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sum_obj = self.pool.get("giscedata.switching.suministro")
        gen_obj = self.pool.get("giscedata.switching.generador")
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        # InfoRegistroAutocons
        vals.update({
            'moviment': xml.movimiento,
            'comentaris': xml.comentarios,
        })
        # Autoconsumo
        autoconsumo = xml.autoconsumo
        vals.update({
            'cau': autoconsumo.cau,
            'seccio_registre': autoconsumo.seccion_registro,
            'subseccio': autoconsumo.sub_seccion,
            'collectiu': True if autoconsumo.colectivo == 'S' else False,
        })
        # DatosSuministro
        sum_xml_obj = xml.datos_suministro
        sums = sum_obj.create_from_xml(cursor, uid, sum_xml_obj, sw_id, context=context)

        vals.update({
            'subministraments': [(6, 0, sums)],
        })
        # DatosInstGen
        gen_xml_obj = xml.datos_inst_gen
        gens = gen_obj.create_from_xml(cursor, uid, gen_xml_obj, sw_id, context=context)

        vals.update({
            'generadors': [(6, 0, gens)],
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        titular_vat = sw.cups_polissa_id.titular.vat
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, polissa.distribuidora.ref
        )

        data_activacio = datetime.today().strftime("%Y-%m-%d")
        fi_lot = (polissa.lot_facturacio and
                  polissa.lot_facturacio.data_final or
                  False)
        if fi_lot:
            data_activacio = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                              timedelta(days=1)).strftime('%Y-%m-%d')

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'persona': vat_info['is_enterprise'] and 'J' or 'F',
            'fiscal_address_id': polissa.direccio_pagament.id,
        })

        return self.create(cursor, uid, vals, context=context)

    def get_desc_taula(self, atribut, taula):
        field = [x[1] for x in taula if x[0] == atribut]
        if len(field) > 0:
            return field[0]
        return None

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        moviment = self.get_desc_taula(step.moviment, TABLA_37)
        cau = step.cau
        moviment_desc = u"{0}, ".format(moviment)
        return u'({0}) {1} - CAU: {2}'.format(step.moviment, moviment_desc, cau)

    _columns = {
        'header_id': fields.many2one(
            'giscedata.switching.step.header', 'Header', required=True
        ),
        'moviment': fields.selection(
            TABLA_37, u'Moviment', required=True
        ),
        'cau': fields.char(u'CAU', size=26, required=True),
        'seccio_registre': fields.selection(
            TABLA_127, u"Secció registre", required=True
        ),
        'subseccio': fields.selection(
            TABLA_128, u"Subsecció"
        ),
        'collectiu': fields.boolean(string=u'Col·lectiu'),
        'generadors': fields.many2many(
            'giscedata.switching.generador', "sw_step_a1_header_generador_ref",
            'header_id', 'generador_id', string=u"Generadors", required=False
        ),
        'subministraments': fields.many2many(
            'giscedata.switching.suministro', "sw_step_a1_header_suminsitro_ref",
            'header_id', 'subministrament_id', string=u"Subministraments", required=False
        ),
        'comentaris': fields.text(u"Comentaris", size=4000),
    }

    _defaults = {
        'collectiu': lambda *a: False,
    }

GiscedataSwitchingA1_01()


class GiscedataSwitchingA1_02(osv.osv):
    """Classe pel pas 02"""

    _name = "giscedata.switching.a1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'A1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingA1_02,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def onchange_tipus_activacio(self, cursor, uid, ids, tipus, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids)

        data = {}

        if tipus == 'L':
            try:
                fi_lot = pas.sw_id.cups_polissa_id.lot_facturacio.data_final
                data_act = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                            timedelta(days=1)).strftime('%Y-%m-%d')

                data = {'data_activacio': data_act}
            except:
                pass
        return {'value': data}

    def onchange_rebuig(self, cursor, uid, ids, rebuig, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        data = {}
        if not rebuig:
            pas = self.browse(cursor, uid, ids)
            if pas.sw_id.cups_polissa_id and pas.sw_id.cups_polissa_id.tarifa:
                polissa = pas.sw_id.cups_polissa_id
                data = {'tarifaATR': polissa.tarifa.codi_ocsum}
        return {'value': data}

    def generar_xml_reject(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A1, pas 02 de rebuig"""

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml_accept(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A1, pas 02 d'acceptació"""

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        sw_obj = self.pool.get('giscedata.switching')
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id

        msg = a1.MensajeActualizacionRegistroAutoconsumo()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # ActualizacionRegistroAutoconsumo
        dades_registre = a1.ActualizacionDatosRegistro()
        actualizacion_datos_registro_fields = {
            'sub_seccion': pas.sub_seccio,
        }
        dades_registre.feed(actualizacion_datos_registro_fields)

        # ActualizacionRegistroAutoconsumo
        registre = a1.ActualizacionRegistroAutoconsumo()
        actualizacion_registro_autoconsumo_fields = {
            'cau': pas.cau,
            'actualizacion_datos_registro': dades_registre,
            'comentarios': pas.comentaris,

        }
        registre.feed(actualizacion_registro_autoconsumo_fields)

        # MensajeActualizacionRegistroAutoconsumo
        mensaje_actualizacion_registro_autoconsumo_fields = {
            'cabecera': capcalera,
            'actualizacion_registro_autoconsumo': registre,
        }
        msg.feed(mensaje_actualizacion_registro_autoconsumo_fields)
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML A1, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reject(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_accept(cursor, uid, pas_id, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        a101_obj = self.pool.get('giscedata.switching.a1.01')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)

        # By default we will dummy create without rejection
        # If rejected, do it manually, no dummy here :)
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        header_id = header_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
        a101_id = a101_obj.search(cursor, uid, [('header_id', '=', header_id)])
        a101_vals = a101_obj.read(cursor, uid, a101_id[0], ['cau', 'subseccio'])

        vals.update({
            'cau': a101_vals['cau'],
            'data_acceptacio': today,
            'sub_seccio': a101_vals['subseccio']
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna un text amb els motius del rebuig"""

        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.a1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        hobj = self.pool.get("giscedata.switching.helpers")
        return hobj.get_init_deadline_cn_m1(cursor, uid, sw_id, "A1", context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_A1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'cau': fields.char(string=u'CAU', size=26),
        # Dades acceptació
        'data_acceptacio': fields.date(string=u"Data d'acceptació"),
        'sub_seccio': fields.selection(TABLA_128, string=u"SubSecció"),

        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),

        'data_rebuig': fields.date(string=u"Data Rebuig"),
        'comentaris': fields.text(string=u'Comentaris', size=4000),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }


GiscedataSwitchingA1_02()
