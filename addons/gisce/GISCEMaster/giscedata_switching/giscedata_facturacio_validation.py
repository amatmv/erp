# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _

class GiscedataFacturacioValidationValidator(osv.osv):
    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'
    _desc = 'Validador de factures amb casos ATR'

    def check_open_r1_subtypes(self, cursor, uid, fact, parameters):
        sw_obj = self.pool.get('giscedata.switching')
        sw_ids = sw_obj.search(
            cursor, uid,[
            ('cups_polissa_id','=',fact.polissa_id.id),
            ('state','in',('open','pending')),
        ])
        if not sw_ids:
            return None

        subtypes = eval(parameters['subtypes'])
        search_param = [
            ('sw_id','in',sw_ids),
            ('subtipus_id.name','in',subtypes),
            ]
        step_obj = self.pool.get('giscedata.switching.r1.01')
        step_ids = step_obj.search(cursor, uid, search_param)
        if not step_ids:
            return None

        return {'subtypes':', '.join(subtypes)}

GiscedataFacturacioValidationValidator()
