# -*- coding: utf-8 -*-
from __future__ import absolute_import

from osv import osv, fields, orm
from gestionatr.output.messages import sw_c1 as c1

from tools.translate import _
from gestionatr.defs import *
from .utils import add_months, calc_months
from datetime import datetime, timedelta
import calendar


class GiscedataSwitchingProcesC1(osv.osv):
    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        '''returns C1 inital steps depending on where we are'''
        if proces == 'C1':
            if where == 'distri':
                return ['01']
            elif where == 'comer':
                return ['01', '11', '12', '06', '10']

        return super(GiscedataSwitchingProcesC1,
                     self).get_init_steps(cursor, uid, proces,
                                          where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        '''returns C1 emisor steps depending on where we are'''
        if proces == 'C1':
            if where == 'distri':
                return ['02', '04', '05', '06', '09', '10', '11', '12']
            elif where == 'comer':
                return ['01', '08']

        return super(GiscedataSwitchingProcesC1,
                     self).get_emisor_steps(cursor, uid, proces,
                                            where, context=context)

    def get_reject_steps(self, cursor, uid, proces, context=None):
        if proces == 'C1':
            return ['02', '04', '09']

        return super(GiscedataSwitchingProcesC1,
                     self).get_reject_steps(cursor, uid, proces,
                                            context=context)

    def get_old_company_steps(self, cursor, uid, proces, context=None):
        if proces == 'C1':
            return ['06', '10', '11', '12']

        return super(GiscedataSwitchingProcesC1, self).get_old_company_steps(
            cursor, uid, proces, context
        )

GiscedataSwitchingProcesC1()


class GiscedataSwitchingC1(osv.osv):
    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        '''Check if the case has arrived to the end or not'''

        if sw.proces_id.name == 'C1':
            # Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '02':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if pas.rebuig:
                        return True
                elif step_name == '05' and sw.whereiam == 'comer':
                    return True
                elif step_name == '09':
                    if not step.pas_id:
                        continue
                    model, id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(id))
                    if not pas.rebuig:
                        return True
                elif step_name in ('06', '10'):
                    return True

        return super(GiscedataSwitchingC1,
                     self).get_final(cursor, uid, sw, context=context)


GiscedataSwitchingC1()


class GiscedataSwitchingC1_01(osv.osv):
    """Classe pel pas 01
    """
    _name = "giscedata.switching.c1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_01,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['11', '02', '08']

    def onchange_persona(self, cr, uid, ids, pers, cog1, cog2, context=None):
        if not context:
            context = {}
        if pers == 'J':
            data = {'tipus_document': 'NI', 'cognom_1': '', 'cognom_2': ''}
        else:  # if pers == 'F':
            data = {'tipus_document': 'NI'}
        return {'value': data}

    def check_cognom(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context=context)
        if pas.persona == 'F' and not pas.cognom_1:
            raise osv.except_osv('Error',
                                 _(u"El camp cognom és obligatori "
                                   u"per persones físiques"))
        return True

    def generar_xml(self, cr, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 01
        """
        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        tel_obj = self.pool.get("giscedata.switching.telefon")
        self.check_cognom(cr, uid, pas_id, context=context)
        pas = self.browse(cr, uid, pas_id, context)

        sw = pas.sw_id
        msg = c1.MensajeCambiodeComercializadorSinCambios()
        # capçalera
        capcalera = pas.header_id.generar_xml(pas)

        # sol·licitud
        sollicitud = c1.DatosSolicitud()
        sol_fields = {
            'ind_activacion': pas.activacio_cicle,
            'contratacion_incondicional_ps': pas.contratacion_incondicional_ps,
            'contratacion_incondicional_bs': pas.contratacion_incondicional_bs,
            'bono_social': pas.bono_social,
        }
        if pas.activacio_cicle == 'F':
            sol_fields.update({'fecha_prevista_accion': pas.data_accio})
        sollicitud.feed(sol_fields)

        # client
        idclient = c1.IdCliente()
        idclient.feed({
            'tipo_identificador': pas.tipus_document,
            'identificador': pas.codi_document,
            'tipo_persona': pas.persona,
        })
        nomclient = c1.Nombre()
        if pas.persona == 'J':
            nom = {'razon_social': pas.nom}
        else:
            nom = {
                'nombre_de_pila': pas.nom,
                'primer_apellido': pas.cognom_1,
                'segundo_apellido': pas.cognom_2
            }
        nomclient.feed(nom)
        client = c1.Cliente()
        cli_fields = {
            'id_cliente': idclient,
            'nombre': nomclient,
        }
        if pas.email:
            cli_fields.update({'correo_electronico': pas.email})
        if pas.telefons:
            cli_fields.update(
                {'telefonos': tel_obj.generar_xml(cr, uid, pas.telefons)}
            )
        client.feed(cli_fields)

        # sol·licitud de canvi
        canvi = c1.CambiodeComercializadorSinCambios()
        canvi_vals = {
            'datos_solicitud': sollicitud,
            'cliente': client,
            'comentarios': pas.comentaris,
        }

        # Documents de registre
        if pas.document_ids:
            doc_xml = pas.header_id.generar_xml_document(pas, context=context)
            canvi_vals.update({'registros_documento': doc_xml})

        canvi.feed(canvi_vals)

        msg.feed({
            'cabecera': capcalera,
            'cambiode_comercializador_sin_cambios': canvi,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        tel_obj = self.pool.get("giscedata.switching.telefon")
        tels = tel_obj.create_from_xml(cursor, uid, xml.cliente.telefonos)

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        vals.update({
            # sol·licitud
            'activacio_cicle': xml.datos_solicitud.ind_activacion,
            'contratacion_incondicional_ps': xml.datos_solicitud.contratacion_incondicional_ps,
            'contratacion_incondicional_bs': xml.datos_solicitud.contratacion_incondicional_bs,
            'bono_social': xml.datos_solicitud.bono_social,
            'data_accio': xml.datos_solicitud.fecha_prevista_accion or False,
            # client
            'tipus_document': xml.cliente.tipo_identificador,
            'codi_document': xml.cliente.identificador,
            'persona': xml.cliente.tipo_persona,
            'telefons': [(6, 0, tels)],
            'email': xml.cliente.correo_electronico,
            'comentaris': xml.comentarios
        })
        if xml.cliente.tipo_persona == 'J':
            vals.update({
                'nom': xml.cliente.razon_social,
            })
        else:
            vals.update({
                'nom': xml.cliente.nombre_de_pila,
                'cognom_1': xml.cliente.primer_apellido,
                'cognom_2': xml.cliente.segundo_apellido,
            })

        # Registro Documents
        if xml.registros_documento:
            doc_ids = header_obj.create_from_xml_doc(
                cursor, uid, xml.registros_documento, context=context
            )
            vals.update({'document_ids': [(6, 0, doc_ids)]})

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        """
        Changes step vals accordingly with vals dict:
            'activacio_cicle': A|L|F
        If given S|N then:
            S -> L
            N -> A
        """
        if not context:
            context = {}

        if isinstance(ids, (list, tuple)):
            ids = ids[0]

        self.config_step_validation(cursor, uid, [], vals, context=context)

        pas = self.browse(cursor, uid, ids, context=context)

        new_vals = vals.copy()

        if 'activacio_cicle' in vals:
            if vals['activacio_cicle'] == 'S':
                val = 'L'
            elif vals['activacio_cicle'] == 'N':
                val = 'A'
            else:
                val = vals['activacio_cicle']
            new_vals.update({'activacio_cicle': val})

        pas.write(new_vals)
        return True

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        titular_vat = sw.cups_polissa_id.titular.vat
        vat_info = sw_obj.get_vat_info(
            cursor, uid, titular_vat, polissa.distribuidora.ref
        )

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update(
            {'data_accio': today,
             'tipus_document': vat_info['document_type'],
             'codi_document': vat_info['vat'],
             'persona': vat_info['is_enterprise'] and 'J' or 'F',
             }
        )

        return self.create(cursor, uid, vals, context=context)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        data_c1 = self.read(cursor, uid, step_id,
                            ['data_accio', 'activacio_cicle', 'sw_id', 'bono_social'])
        accio = data_c1['activacio_cicle']
        for elem in TABLA_8:
            if elem[0] == data_c1['activacio_cicle']:
                accio = elem[1]
                break
        data_acc = ''
        if data_c1['data_accio']:
            data_acc = _(u". Data prevista: {0}").format(data_c1['data_accio'])

        sw = self.pool.get("giscedata.switching").browse(cursor, uid, data_c1['sw_id'][0])
        extra_info = ""
        if sw.cups_polissa_id:
            pol = sw.cups_polissa_id
            extra_info = _(u"Tarifa:{0}, Pot.: {1}").format(pol.tarifa.name, pol.potencia)
        
        bono_social = ""
        if data_c1.get("bono_social") == '1':
            bono_social = _(u"Bo Social: Si. ")
        elif data_c1.get("bono_social") == '0':
            bono_social = _(u"Bo Social: No. ")
        
        return _(u"{3}Activació: {0}{1}. {2}").format(accio, data_acc, extra_info, bono_social)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Solicitud
        'activacio_cicle': fields.selection(TABLA_8, u"Activació",
                                            required=True),
        'data_accio': fields.date(u"Data prevista del canvi o alta"),
        'contratacion_incondicional_ps': fields.selection(
            TABLA_26, u"Contratacion Incondicional PS", required=True),

        # Client
        'tipus_document': fields.selection(TIPUS_DOCUMENT,
                                           u'Document identificatiu', size=2,
                                           required=True),
        'codi_document': fields.char(u'Codi document', size=11,
                                     required=True),
        'persona': fields.selection(PERSONA, u'Persona', required=True),
        'nom': fields.char(u'Nom de la persona o societat', size=45,
                           required=True),
        'cognom_1': fields.char(u'Primer cognom', size=45,
                                help=u"Únicament per a persones físiques"),
        'cognom_2': fields.char(u'Segon cognom', size=45,
                                help=u"Opcional per a persones físiques"),
        'prefix': fields.char(u"Prefix telefònic", size=2),
        'telefon': fields.char(u"Telèfon", size=9),
        'email': fields.char(u"Email", size=45),
        'comentaris': fields.text("Comentaris", size=4000),
        'bono_social': fields.selection(TABLA_116, u"Bo Social", size=1),
        'contratacion_incondicional_bs': fields.selection(
            TABLA_26, u"Contratació Incondicional BS", size=1),
    }

    _defaults = {
        'activacio_cicle': lambda *a: 'A',
        'enviament_pendent': lambda *a: True,
        'contratacion_incondicional_ps': lambda *a: 'N',
        'contratacion_incondicional_bs': lambda *a: 'N',
    }


GiscedataSwitchingC1_01()


class GiscedataSwitchingC1_02(osv.osv):
    """Classe pel pas 02
    """
    _name = "giscedata.switching.c1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if context and context.get("rebuig", False):
            return []
        if where == 'distri':
            return ['03', '04', '05', '08', '11', '12', '06']
        elif where == 'comer':
            return ['03', '04', '05', '08']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_02,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def onchange_tipus_activacio(self, cursor, uid, ids, tipus, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids)

        data = {}

        if tipus == 'L':
            try:
                fi_lot = pas.sw_id.cups_polissa_id.lot_facturacio.data_final
                data_act = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                            timedelta(days=1)).strftime('%Y-%m-%d')

                data = {'data_activacio': data_act}
            except:
                pass
        return {'value': data}

    def onchange_rebuig(self, cursor, uid, ids, rebuig, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        data = {}
        if not rebuig:
            pas = self.browse(cursor, uid, ids)
            if pas.sw_id.cups_polissa_id and pas.sw_id.cups_polissa_id.tarifa:
                polissa = pas.sw_id.cups_polissa_id
                data = {'tarifaATR': polissa.tarifa.codi_ocsum}
        return {'value': data}

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 02
           de rebuig
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 02
           d'acceptació
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        sw_obj = self.pool.get('giscedata.switching')
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id

        msg = c1.MensajeAceptacionCambiodeComercializadorSinCambios()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades acceptació
        dades = c1.DatosAceptacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
            'actuacion_campo': pas.actuacio_camp,
            'fecha_ultima_lectura_firme': pas.data_ult_lect,
        })
        if sw.check_destinatari_is_cur(pas):
            dades.feed({'bono_social': pas.bono_social})

        # Contracte
        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = c1.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'potencias_contratadas': potencies,
        })
        contracte = c1.Contrato()
        contracte_vals = {
            'tipo_contrato_atr': pas.tipus_contracteATR,
            'condiciones_contractuales': condicions,
            'tipo_activacion_prevista': pas.tipus_activacio,
            'fecha_activacion_prevista': pas.data_activacio
        }
        contracte.feed(contracte_vals)
        # Acceptacio
        acceptacio = c1.AceptacionCambiodeComercializadorSinCambios()
        acceptacio.feed({
            'datos_aceptacion': dades,
            'contrato': contracte,
        })

        msg.feed({
            'cabecera': capcalera,
            'aceptacion_cambiode_comercializador_sin_cambios': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, pots,
                                              context=context)

        vals.update({
            'rebuig': False,
            'data_acceptacio': xml.datos_aceptacion.fecha_aceptacion,
            'actuacio_camp': xml.datos_aceptacion.actuacion_campo,
            'data_ult_lect': xml.datos_aceptacion.fecha_ultima_lectura_firme,
            'bono_social': xml.datos_aceptacion.bono_social,
            # Contracte - pas 02
            'tarifaATR': xml.contrato.tarifa_atr,
            'pot_ids': [(6, 0, created_pots)],
            'tipus_activacio': xml.contrato.tipo_activacion_prevista,
            'data_activacio': xml.contrato.fecha_activacion_prevista,
            'tipus_contracteATR': xml.contrato.tipo_contrato_atr,
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML C1, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        polissa = sw.cups_polissa_id
        # By default we will dummy create without rejection
        # If rejected, do it manually, no dummy here :)
        created_pots = header_obj.create_pots(cursor, uid, False,
                                              polissa=polissa,
                                              context=context)

        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        sw_helper = self.pool.get("giscedata.switching.helpers")

        activacio, data_activacio = sw_helper.get_data_prevista_activacio(
            cursor, uid, sw, context
        )

        bs = polissa.bono_social
        pas01_obj = self.pool.get("giscedata.switching.c1.01")
        pas01 = pas01_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
        if pas01:
            pas01 = pas01_obj.read(cursor, uid, pas01[0], {'bono_social'}, context=context)
            bs = pas01.get('bono_social') and pas01.get('bono_social') or bs

        data_lect = sw_helper.get_ultima_lectura(cursor, uid, polissa.id)
        vals.update({
            'data_acceptacio': today,
            'data_rebuig': today,
            'actuacio_camp': 'N',
            'data_ult_lect': data_lect or data_activacio,
            # Contracte - pas 02
            'tarifaATR': polissa.tarifa.codi_ocsum,
            'tipus_activacio': activacio,
            'data_activacio': data_activacio,
            'pot_ids': [(6, 0, created_pots)],
            'tipus_contracteATR': '01',
            'bono_social': bs
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.c1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        hobj = self.pool.get("giscedata.switching.helpers")
        return hobj.get_init_deadline_cn_m1(cursor, uid, sw_id, "C1", context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        'actuacio_camp': fields.selection(SINO, u"Treball de camp"),
        'data_ult_lect': fields.date(u"Data última lectura", readonly=True),
        # Contracte - pas 02
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa',
                                      readonly=True),
        'tipus_activacio': fields.selection(TIPUS_ACTIVACIO,
                                            u"Activació", size=2),
        'data_activacio': fields.date(u"Data de canvi o alta"),
        'tipus_contracteATR': fields.selection(TABLA_9, u'Tipus Contracte'),
        'bono_social': fields.selection(TABLA_116, u"Bo Social", size=1),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }

GiscedataSwitchingC1_02()


class GiscedataSwitchingC1_04(osv.osv):

    _name = 'giscedata.switching.c1.04'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '04'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == 'distri':
            return ['11', '12']
        else:
            return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_04,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 04
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '04'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'data_rebuig': datetime.today(),
        })
        return self.create(cursor, uid, vals)

    def _get_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Sempre és rebuig"""
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        return {}.fromkeys(ids, True)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id, ['motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.c1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     required=True),
        # Rebuig
        'rebuig': fields.function(_get_rebuig, 'Sol·licitud rebutjada',
                                  type='boolean', method=True),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True,),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }


GiscedataSwitchingC1_04()


class GiscedataSwitchingC1_05(osv.osv):
    """Classe pel pas 05
    """
    _name = "giscedata.switching.c1.05"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '05'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == 'distri':
            return ['08', '11', '12', '06']
        elif where == 'comer':
            return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_05,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, ids, context=None):
        """Retorna la sol·licitud en format XML C1, pas 05
        """

        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context)
        sw = pas.sw_id
        msg = c1.MensajeActivacionCambiodeComercializadorSinCambios()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades activació
        dades_activacio = c1.DatosActivacion()
        dades_activacio.feed({
            'fecha': pas.data_activacio,
        })
        if sw.check_destinatari_is_cur(pas):
            dades_activacio.feed({'bono_social': pas.bono_social})
        # Contracte
        idcontracte = c1.IdContrato()
        nom_ctr = filter(lambda x: x.isdigit(), sw.cups_polissa_id.name)
        idcontracte.feed({
            'cod_contrato': nom_ctr
        })

        potencies = pas.header_id.generar_xml_pot(pas)

        condicions = c1.CondicionesContractuales()
        condicions.feed({
            'tarifa_atr': pas.tarifaATR,
            'periodicidad_facturacion': pas.periodicitat_facturacio,
            'tipode_telegestion': pas.tipus_telegestio,
            'potencias_contratadas': potencies,
            'modo_control_potencia': pas.control_potencia,
            'tension_del_suministro': pas.tensio_suministre,
            'marca_medida_con_perdidas': pas.marca_medida_bt,
            'vas_trafo': int(pas.kvas_trafo * 1000),
            'porcentaje_perdidas': pas.perdues,
        })
        contracte = c1.Contrato()
        contracte.feed({
            'id_contrato': idcontracte,
            'tipo_autoconsumo': pas.tipus_autoconsum,
            'tipo_contrato_atr': pas.tipus_contracte,
            'condiciones_contractuales': condicions,
        })

        punts_mesura = pas.header_id.generar_xml_pm(pas)

        # Activacio
        activacio = c1.ActivacionCambiodeComercializadorSinCambios()
        activacio.feed({
            'datos_activacion': dades_activacio,
            'contrato': contracte,
            'puntos_de_medida': punts_mesura
        })
        msg.feed({
            'cabecera': capcalera,
            'activacion_cambiode_comercializador_sin_cambios': activacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        pots = xml.contrato.potencias_contratadas
        created_pots = header_obj.create_pots(cursor, uid, pots,
                                              context=context)
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)
        pm_ids = header_obj.create_from_xml_pm(cursor, uid, xml,
                                               context=context)

        tipo_telegestion_xml = xml.contrato.tipo_de_telegestion

        vals.update({
            # dades activació
            'data_activacio': xml.datos_activacion.fecha,
            'bono_social': xml.datos_activacion.bono_social,
            'tipus_autoconsum': xml.contrato.tipo_autoconsumo,
            'tipus_contracte': xml.contrato.tipo_contrato_atr,
            'tarifaATR': xml.contrato.tarifa_atr,
            'periodicitat_facturacio':
                xml.contrato.periodicidad_facturacion,
            'tipus_telegestio': tipo_telegestion_xml,
            'pot_ids': [(6, 0, created_pots)],
            'pm_ids': [(6, 0, pm_ids)],
            'contracte_atr': xml.contrato.cod_contrato or '',
            'control_potencia': xml.contrato.modo_control_potencia,
            'tensio_suministre': xml.contrato.tension_del_suministro,
        })
        if xml.contrato.marca_medida_con_perdidas == 'S':
            vals.update({
                'marca_medida_bt': 'S',
                'kvas_trafo': float(xml.contrato.vas_trafo) / 1000,
                'perdues': xml.contrato.porcentaje_perdidas,
            })
        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)


        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        if polissa.modcontractual_activa and polissa.modcontractual_activa.data_inici:
            activation_date = polissa.modcontractual_activa.data_inici
        else:
            activation_date = datetime.strftime(datetime.now(), '%Y-%m-%d')

        control_potencia = (polissa.facturacio_potencia == 'max'
                            and '2' or '1')
        created_pots = header_obj.create_pots(cursor, uid, False,
                                              polissa=polissa,
                                              context=context)
        pm_ids = header_obj.dummy_create_pm(cursor, uid, sw,
                                            context=context)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        tipus_tg = CONV_T109_T111.get('{0:02d}'.format(int(polissa.tg)), '03')

        vals.update({
            'data_activacio': activation_date,
            'bono_social': polissa.bono_social,
            'tipus_autoconsum': polissa.autoconsumo,
            'tipus_contracte': '01',
            'tarifaATR': polissa.tarifa.codi_ocsum,
            'periodicitat_facturacio': '0%d' % polissa.facturacio,
            'tipus_telegestio': '{0}'.format(tipus_tg),
            'control_potencia': control_potencia,
            'contracte_atr': polissa.name or '',
            'pot_ids': [(6, 0, created_pots)],
            'pm_ids': [(6, 0, [pm_ids])],
            'tensio_suministre': (
                (polissa.tensio_normalitzada and
                 polissa.tensio_normalitzada.cnmc_code) or '01'
            ),
        })
        #  If codi_ocsum is 011 (3.1A) and there is 'trafo' in polissa then
        # the tarif is 3.1A LB and we add 'marca_medida_bt' and 'kvas_trafo'
        mesura = polissa.tarifa.mesura_ab
        if polissa.tarifa.codi_ocsum == '011' and mesura == 'B':
            vals.update({
                'marca_medida_bt': 'S',
                'kvas_trafo': polissa.trafo,
                'perdues': 4.0,
            })
        else:
            vals.update({
                'marca_medida_bt': 'N',
            })
        return self.create(cursor, uid, vals)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades activació
        'data_activacio': fields.date(u"Data d'activació"),
        'tipus_autoconsum': fields.selection(TABLA_113, u"Tipus Autoconsum"),
        'tipus_contracte': fields.selection(TABLA_9, u"Tipus de contracte"),
        'tarifaATR': fields.selection(TABLA_17, u'Tarifa', readonly=True),
        'periodicitat_facturacio': fields.selection(TABLA_108,
                                                    u"Periodicitat de facturació",
                                                    size=2, required=True),
        'control_potencia': fields.selection(CONTROL_POTENCIA,
                                             u"Control de potència"),
        'contracte_atr': fields.char(u'Codi contracte ATR', size=12,
                                     readonly=True,
                                     help=u'Codi contracte a la distribuidora'),
        'tipus_telegestio': fields.selection(TABLA_111,
                                             u'Tipus de Telegestió'),
        'marca_medida_bt': fields.selection([('S', 'Si'), ('N', 'No')],
                                            u'Marca Mesura BT amb Perdues',
                                            readonly=True),
        'kvas_trafo': fields.float(u"KVAs Trafo"),
        'tensio_suministre': fields.selection(TABLA_64, u'Tensió Suministre',
                                              required=True),
        'perdues': fields.float(u"Percentatge Pèrdues"),
        'bono_social': fields.selection(TABLA_116, u"Bo Social", size=1),
    }


GiscedataSwitchingC1_05()


class GiscedataSwitchingC1_06(osv.osv):
    """Classe pel pas 06
    """
    _name = "giscedata.switching.c1.06"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '06'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == 'distri':
            return ['05', '08']
        elif where == 'comer':
            return ['10']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_06,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 06
        """
        not_obj = self.pool.get('giscedata.switching.not.comer')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return not_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        not_obj = self.pool.get('giscedata.switching.not.comer')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return not_obj.create_from_xml(cursor, uid, sw_id, xml,
                                       context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        not_obj = self.pool.get('giscedata.switching.not.comer')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return not_obj.dummy_create(cursor, uid, sw_id,
                                    context=ctx)

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        sw_obj = self.pool.get("giscedata.switching")
        step = self.read(cursor, uid, step_id, ['sw_id', 'data_activacio'])
        if sw_obj.whereiam(cursor, uid, False, context=context) == 'comer':
            return _(u"Data Activació: {0}").format(step['data_activacio'])
        else:
            sw_id = step['sw_id'][0]
            step01 = self.pool.get('giscedata.switching.c1.01')
            step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
            antinf = ''
            if len(step01_id) > 0:
                antinf = step01.get_additional_info(cursor, uid,  step01_id[0])
                antinf += "."
            return antinf

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades activació
        'data_activacio': fields.date(u"Data d'activació"),
        'ind_bono_social': fields.selection(TABLA_26, u"Indicatiu Bo Social", size=1, required=1)
    }


GiscedataSwitchingC1_06()


class GiscedataSwitchingC1_08(osv.osv):
    """Classe pel pas 08
    """
    _name = "giscedata.switching.c1.08"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '08'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return ['09']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_08,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 08
        """
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return anulacio_obj.generar_xml(cursor, uid, pas, context=context)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return anulacio_obj.create_from_xml(cursor, uid, sw_id, xml,
                                            context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        anulacio_obj = self.pool.get('giscedata.switching.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return anulacio_obj.dummy_create(cursor, uid, sw_id,
                                         context=ctx)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
    }

GiscedataSwitchingC1_08()


class GiscedataSwitchingC1_09(osv.osv):
    """Classe pel pas 09
    """
    _name = "giscedata.switching.c1.09"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '09'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', '09',
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == "distri":
            return ['10', '11', '12', '06', '05']
        else:
            return ['10', '11']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_09,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml_acc(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 09
           acceptació
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')
        if not context:
            context = {}

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return acc_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml_reb(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 09
           de rebuig
        """

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=context)

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 09
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reb(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_acc(cursor, uid, pas_id, context)

    def create_from_xml_acc(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml_reb(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name,
                    'nom_pas': self._nom_pas})

        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML C1, pas 02
        """
        if not context:
            context = {}

        if xml.fecha_aceptacion:
            return self.create_from_xml_acc(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reb(cursor, uid, sw_id, xml, context)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.dummy_create(cursor, uid, sw_id, context=ctx)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        "Retorna un text amb els motius del rebuig"
        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.c1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),
        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),
        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }

GiscedataSwitchingC1_09()


class GiscedataSwitchingC1_10(osv.osv):
    """Classe pel pas 10
    """
    _name = "giscedata.switching.c1.10"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '10'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_10,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 10
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        pas = self.browse(cursor, uid, pas_id)
        return acc_obj.generar_xml(cursor, uid, pas, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})

        return acc_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy values'''

        acc_obj = self.pool.get('giscedata.switching.acc.anulacio')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name})
        ctx.update({'sortint': True})

        return acc_obj.dummy_create(cursor, uid, sw_id, context=ctx)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'data_acceptacio': fields.date(u"Data d'acceptació", required=True),
    }


GiscedataSwitchingC1_10()


class GiscedataSwitchingC1_11(osv.osv):
    """Classe pel pas 11
    """
    _name = "giscedata.switching.c1.11"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '11'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        if where == 'distri':
            return ['03', '04', '05', '06', '08', '12']
        elif where == 'comer':
            return ['12', '06', '10']

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_11,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML C1, pas 11
           d'acceptació
        """
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        sw_obj = self.pool.get('giscedata.switching')
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id

        msg = c1.MensajeAceptacionCambiodeComercializadorSaliente()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas, context=ctx)

        # Acceptacio
        acceptacio = c1.AceptacionCambioComercializadorSaliente()
        acceptacio.feed({
            'fecha_activacion_prevista': pas.data_activacio,
            'ind_bono_social': pas.ind_bono_social
        })

        msg.feed({
            'cabecera': capcalera,
            'aceptacion_cambio_comercializador_saliente': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
            'data_activacio': xml.fecha_activacion_prevista,
            'ind_bono_social': xml.ind_bono_social
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)

        vals = header_obj.dummy_create(cursor, uid, sw, context=ctx)
        sw_helper = self.pool.get("giscedata.switching.helpers")
        _, data_activacio = sw_helper.get_data_prevista_activacio(
            cursor, uid, sw, context
        )

        bs = False
        if sw.cups_polissa_id:
            bs = sw.cups_polissa_id.bono_social
            pas01_obj = self.pool.get("giscedata.switching.c1.01")
            pas01 = pas01_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
            if pas01:
                pas01 = pas01_obj.read(cursor, uid, pas01[0], {'bono_social'}, context=context)
                bs = pas01.get('bono_social') or bs

        bono_social = 'N'
        if bs == '1':
            bono_social = 'S'
        vals.update({
            'data_activacio': data_activacio,
            'ind_bono_social': bono_social
        })

        return self.create(cursor, uid, vals)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'data_activacio': fields.date(u"Data de canvi o alta", required=True),
        'ind_bono_social': fields.selection(TABLA_26, u"Indicatiu Bo Social", size=1, required=1)
    }


GiscedataSwitchingC1_11()


class GiscedataSwitchingC1_12(osv.osv):

    _name = 'giscedata.switching.c1.12'
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '12'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'C1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        '''returns next valid steps depending where we are'''
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingC1_12,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def generar_xml(self, cursor, uid, pas_id, context=None):
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id

        msg = c1.MensajeRechazoCambiodeComercializadorSaliente()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas, context=ctx)

        # Acceptacio
        rebuig = c1.RechazoCambioComercializadorSaliente()
        rebuig.feed({
            'fecha_rechazo': pas.data_rebuig,
        })

        msg.feed({
            'cabecera': capcalera,
            'rechazo_cambio_comercializador_saliente': rebuig,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
            'data_rebuig': xml.fecha_rechazo,
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        '''create with dummy default values for fields in process step'''
        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'sortint': True})
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=ctx)
        vals.update({
            'data_rebuig': datetime.today().strftime("%Y-%m-%d"),
        })
        return self.create(cursor, uid, vals)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_C1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        'data_rebuig': fields.date(u"Data de rebuig", required=True),
    }

GiscedataSwitchingC1_12()
