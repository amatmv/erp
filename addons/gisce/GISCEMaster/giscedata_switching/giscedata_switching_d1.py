# -*- coding: utf-8 -*-
from __future__ import absolute_import

from giscedata_switching.giscedata_switching import SwitchingException
from osv import osv, fields
from gestionatr.output.messages import sw_d1 as d1
from tools.translate import _
from gestionatr.defs import *
from datetime import datetime, timedelta


class GiscedataSwitchingProcesD1(osv.osv):

    _name = 'giscedata.switching.proces'
    _inherit = 'giscedata.switching.proces'

    def get_init_steps(self, cursor, uid, proces, where, context=None):
        """returns D1 inital steps depending on where we are"""
        if proces == 'D1':
            return ['01']

        return super(GiscedataSwitchingProcesD1, self).get_init_steps(cursor, uid, proces, where, context=context)

    def get_emisor_steps(self, cursor, uid, proces, where, context=None):
        """returns D1 emisor steps depending on where we are"""
        if proces == 'D1':
            if where == 'distri':
                return ['01']
            elif where == 'comer':
                return ['02']

        return super(GiscedataSwitchingProcesD1, self).get_emisor_steps(cursor, uid, proces, where, context=context)


GiscedataSwitchingProcesD1()


class GiscedataSwitchingD1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def get_final(self, cursor, uid, sw, context=None):
        """Check if the case has arrived to the end or not"""

        if sw.proces_id.name == 'D1':
            # Check steps because they can be unordered
            for step in sw.step_ids:
                step_name = step.step_id.name
                if step_name == '01' and sw.whereiam == 'comer':
                    return True
                elif step_name == '02' and sw.whereiam == 'distri':
                    if not step.pas_id:
                        continue
                    model, pas_id = step.pas_id.split(',')
                    pas = self.pool.get(model).browse(cursor, uid, int(pas_id))
                    if pas.rebuig:
                        return True

        return super(GiscedataSwitchingD1, self).get_final(cursor, uid, sw, context=context)


GiscedataSwitchingD1()


class GiscedataSwitchingD1_01(osv.osv):
    """Classe pel pas 01"""

    _name = "giscedata.switching.d1.01"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '01'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'D1', self._nom_pas, context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return ['02']

    def config_step_validation(self, cursor, uid, ids, vals, context=None):

        autoconsum_obj = self.pool.get('giscedata.autoconsum')
        autoconsum_id = vals['autoconsum_id']
        autoconsum = autoconsum_obj.browse(cursor, uid, autoconsum_id)

        if vals['motiu_canvi'] not in ['01', '02', '03']:
            mandatory_fields = ['cau', 'seccio_registre', 'cups_id',
                                'generador_id', 'data_alta']

            for field in mandatory_fields:
                if not getattr(autoconsum, field):
                    raise SwitchingException(_("Falten dades. Camp no omplert."), [field])

        return True

    def config_step(self, cursor, uid, ids, vals, context=None):
        if not context:
            context = {}
        self.config_step_validation(cursor, uid, ids, vals, context=context)

        if vals['motiu_canvi'] not in ['01', '02', '03']:
            cups_obj = self.pool.get('giscedata.cups.ps')
            autoconsum_obj = self.pool.get('giscedata.autoconsum')
            generador_obj = self.pool.get('giscedata.autoconsum.generador')
            partner_obj = self.pool.get('res.partner')
            partner_address_obj = self.pool.get('res.partner.address')
            sw_gen_obj = self.pool.get('giscedata.switching.generador')
            telefon_obj = self.pool.get('giscedata.switching.telefon')

            autoconsum_id = vals['autoconsum_id']
            autoconsum_vals = autoconsum_obj.read(cursor, uid, autoconsum_id, [
                'cau', 'generador_id', 'seccio_registre', 'subseccio'
            ])

            # Guardem el cups_id per utilitzar-lo i l'esborrem de la llista de valors passada per paràmetre.
            cups_id = vals['cups_id']
            cups_name = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
            del vals['cups_id']

            generadors_vals = generador_obj.read(cursor, uid, autoconsum_vals['generador_id'], [])

            if not generadors_vals:
                raise osv.except_osv(_("No s'han trobat Generadors"),
                                     _(u"No s'ha trobat cap Generador associat a l'autoconsum. Abans de crear "
                                       u"el cas D1 hauria d'associar els generadors pertinents al mateix"))

            generador_ids = []
            for generador_vals in generadors_vals:
                if not generador_vals['partner_id']:
                    raise osv.except_osv(_("No s'ha trobat Titular"),
                                         _(u"No s'ha trobat cap Titular associat a l'autoconsum. Abans de crear "
                                           u"el cas D1 hauria d'associar el un titular al generador pertinent des del "
                                           u"formulari. Generador afectat: {}".format(generador_vals['cil'])))

                partner_vals = partner_obj.read(cursor, uid, generador_vals['partner_id'][0], [])
                if not partner_vals['vat']:
                    raise osv.except_osv(_("No s'ha trobat identificador pel titular"),
                                         _(u"El Titular associat a l'autoconsum no diposa de CIF/NIF informat. "
                                           u"Introdueixi un CIF/NIF pel titular abans de continuar."))

                if partner_obj.vat_es_empresa(cursor, uid, partner_vals['vat']):
                    persona = 'J'
                    partner_name = partner_vals['name']
                    if partner_vals.get('cifnif', False) == 'CI':
                        partner_vals['cifnif'] = 'NI'
                else:
                    persona = 'F'
                    partner_dict = partner_obj.separa_cognoms(cursor, uid, partner_vals['name'])
                    partner_name = partner_vals['name']
                    generador_vals.update({
                        'cognom_1': partner_dict['cognoms'][0],
                        'cognom_2': partner_dict['cognoms'][1],
                    })

                partner_address = partner_address_obj.search(cursor, uid, [
                    ('partner_id', '=', generador_vals['partner_id'][0])
                ])

                telefons = []
                partner_phones = partner_address_obj.read(cursor, uid, partner_address[0], ['phone', 'mobile'])
                if not partner_phones['phone'] and not partner_phones['mobile']:
                    raise osv.except_osv(_("No s'ha trobat cap telèfon pel titular"),
                                         _(u"El Titular associat al generador de l'autoconsum ha de tenir "
                                           u"informat com a mínim un telèfon. Si us plau introdueixi un "
                                           u"abans de continuar"))

                for tel in (tels for keys, tels in partner_phones.items() if tels and keys != 'id'):
                    telefon = tel.replace(" ", "").replace("+", "")
                    if len(telefon) == 9:
                        prefix = '34'
                        numero = telefon
                    elif len(telefon) == 11:
                        prefix = telefon[:2]
                        numero = telefon[2:]
                    else:
                        raise osv.except_osv(_("Format de telèfon erroni"),
                                             _(u"El Titular associat al generador de l'autoconsum te informat "
                                               u"un telèfon amb format incorrecte. Si us plau informi "
                                               u"correctament el telèfon."))
                    tel_id = telefon_obj.create(cursor, uid, {
                        'numero': numero,
                        'prefix': prefix,
                    })
                    telefons.append(tel_id)

                generador_vals.update({
                    'persona': persona,
                    'tipus_identificador': partner_vals['cifnif'],
                    'identificador': partner_vals['vat'],
                    'nom': partner_name,
                    'email': partner_vals['emails'],
                    'fiscal_address_id': partner_address[0],
                    'telefons': [(6, 0, telefons)]
                })

                generador_vals.update({
                    'ref_cadastre_inst_gen': generador_vals['ref_cadastre'],
                    'pot_installada_gen': generador_vals['pot_instalada_gen'],
                })

                keys_to_delete = [
                    'ref_cadastre', 'pot_instalada_gen', 'autoconsum_id', 'id',
                    'partner_id', 'active', 'data_alta', 'data_baixa'
                ]

                generador_vals = {key: generador_vals[key] for key in generador_vals if key not in keys_to_delete}
                sw_gen_id = sw_gen_obj.create(cursor, uid, generador_vals)
                generador_ids.append(sw_gen_id)

            vals.update({'generadors': [(6, 0, generador_ids)]})

            subseccio = False
            if autoconsum_vals['seccio_registre'] != '1':
                subseccio = autoconsum_vals['subseccio']

            vals.update({
                'cau': autoconsum_vals['cau'],
                'seccio_registre': autoconsum_vals['seccio_registre'],
                'subseccio': subseccio,
                'cups': cups_name,
            })

            # Finalment eliminem autoconsum_id dels valors que escriurem al registre
            del vals['autoconsum_id']

        self.write(cursor, uid, ids, vals, context=context)

        return True

    def unlink(self, cursor, uid, ids, context=None):
        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingD1_01,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def check_periodicitat(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        if isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids, context=context)
        if pas.motiu_canvi in ('01', '02') and not pas.periodicitat_facturacio:
            raise osv.except_osv('Error',
                                 _(u"El camp Periodicitat Facturació és "
                                   u"obligatori per aquest motiu de canvi"))

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML D1, pas 01"""

        if not context:
            context = {}

        if not pas_id:
            return None

        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]

        gen_obj = self.pool.get("giscedata.switching.generador")
        self.check_periodicitat(cursor, uid, pas_id, context=context)
        pas = self.browse(cursor, uid, pas_id, context)

        sw = pas.sw_id
        msg = d1.MensajeNotificacionCambiosATRDesdeDistribuidor()
        # Capçalera
        capcalera = pas.header_id.generar_xml(pas)

        # NotificacionCambiosATRDesdeDistribuidor·licitud
        notificacion = d1.NotificacionCambiosATRDesdeDistribuidor()
        notificacion_fields = {
            'motivo_cambio_atr_desde_distribuidora': pas.motiu_canvi,
            'fecha_prevista_aplicacion_cambio_atr': pas.data_activacio,
            'periodicidad_facturacion': pas.periodicitat_facturacio
        }

        if pas.motiu_canvi in ['04', '05', '07']:
            # Autoconsumo
            autoconsumo = d1.Autoconsumo()
            autoconsumo_fields = {
                'cau': pas.cau,
                'seccion_registro': pas.seccio_registre,
                'colectivo': 'N',
            }
            if pas.collectiu:
                autoconsumo_fields['colectivo'] = 'S'

            if pas.seccio_registre == '2':
                autoconsumo_fields.update({
                    'sub_seccion': pas.subseccio
                })
            autoconsumo.feed(autoconsumo_fields)

            # DatosSuministro
            suministro = d1.DatosSuministro()
            suministro_fields = {
                'cups': pas.cups,
                'tipo_cups': pas.tipus_cups,
                'ref_catastro': pas.ref_cadastre_cups,
            }
            suministro.feed(suministro_fields)

            generadors = gen_obj.generar_xml(cursor, uid, pas.generadors)

            # InfoRegistoAutocons
            info = d1.InfoRegistroAutocons()
            info_registro_autocons_fields = {
                'autoconsumo': autoconsumo,
                'datos_suministro': suministro,
                'datos_inst_gen': generadors,
                'comentarios': pas.comentaris
            }
            info.feed(info_registro_autocons_fields)

            notificacion_fields.update({
                'info_registro_autocons': info,
            })

        notificacion.feed(notificacion_fields)
        msg.feed({
            'cabecera': capcalera,
            'notificacion_cambios_atr_desde_distribuidor': notificacion,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        # NotificacionCambiosATRDesdeDistribuidor
        vals.update({
            'motiu_canvi': xml.motivo_cambio_atr_desde_distribuidora,
            'periodicitat_facturacio': xml.periodicidad_facturacion,
            'data_activacio': xml.fecha_prevista_aplicacion_cambio_atr,

        })
        motiu_canvi = xml.motivo_cambio_atr_desde_distribuidora
        if motiu_canvi in ['04', '05', '07']:
            gen_obj = self.pool.get('giscedata.switching.generador')
            gen_xml_obj = xml.info_registro_autocons.datos_inst_gen
            gens = gen_obj.create_from_xml(cursor, uid, gen_xml_obj, sw_id, context=context)

            # InfoRegistroAutocons
            info_registro_autocons = xml.info_registro_autocons
            vals.update({
                'comentaris': info_registro_autocons.comentarios,
            })
            # Autoconsumo
            autoconsumo = info_registro_autocons.autoconsumo
            vals.update({
                'cau': autoconsumo.cau,
                'seccio_registre': autoconsumo.seccion_registro,
                'subseccio': autoconsumo.sub_seccion,
                'collectiu': True if autoconsumo.colectivo == 'S' else False,
            })
            # DatosSuministro
            datos_suministro = info_registro_autocons.datos_suministro
            vals.update({
                'cups': datos_suministro.cups,
                'tipus_cups': datos_suministro.tipo_cups,
                'ref_cadastre_cups': datos_suministro.ref_catastro,
            })

            vals.update({
                'generadors': [(6, 0, gens)],
            })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        polissa = sw.cups_polissa_id

        vals = header_obj.dummy_create(cursor, uid, sw, context=context)
        vals.update({
            'motiu_canvi': '{0:02d}'.format(int(polissa.tg)),
            'periodicitat_facturacio': '{0:02d}'.format(polissa.facturacio),
        })

        return self.create(cursor, uid, vals, context=context)

    def get_desc_taula(self, atribut, taula):
        field = [x[1] for x in taula if x[0] == atribut]
        if len(field) > 0:
            return field[0]
        return None

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.browse(cursor, uid, step_id)
        motiu_desc = self.get_desc_taula(step.motiu_canvi, TABLA_109)
        perid = ""
        if step.periodicitat_facturacio:
            perid = self.get_desc_taula(step.periodicitat_facturacio, TABLA_108)
            motiu_desc = u"{0}, ".format(motiu_desc)
        return u'({0}) {1}{2}'.format(step.motiu_canvi, motiu_desc, perid)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template = 'notification_atr_D1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = [template for st_id in step_id]
            return templates
        return template

    _columns = {
        'header_id': fields.many2one(
            'giscedata.switching.step.header', 'Header', required=True
        ),
        'motiu_canvi': fields.selection(
            TABLA_109, u'Motiu Canvi', required=True
        ),
        'periodicitat_facturacio': fields.selection(
            TABLA_108, u"Periodicitat de facturació", size=2
        ),
        'data_activacio': fields.date(
            u"Data prevista d'activació"
        ),
        'cau': fields.char(u'CAU', size=26),
        'seccio_registre': fields.selection(
            TABLA_127, u"Secció registre"
        ),
        'subseccio': fields.selection(
            TABLA_128, u"Subsecció"
        ),
        'collectiu': fields.boolean(string=u'Col·lectiu'),
        'cups': fields.char(u'CUPS', size=22),
        'tipus_cups': fields.selection(
            TABLA_131, u"Tipus CUPS"
        ),
        'ref_cadastre_cups': fields.char(u'Ref. Cadastre CUPS', size=20),
        'generadors': fields.many2many(
            'giscedata.switching.generador', "sw_step_header_generador_ref",
            'header_id', 'generador_id', string=u"Generadors", required=False
        ),
        'comentaris': fields.text(u"Comentaris", size=4000),
    }

    _defaults = {
        'motiu_canvi': lambda *a: '01',
        'data_activacio': lambda *a: datetime.today(),
        'collectiu': lambda *a: False,
    }

GiscedataSwitchingD1_01()


class GiscedataSwitchingD1_02(osv.osv):
    """Classe pel pas 02"""

    _name = "giscedata.switching.d1.02"
    _inherits = {'giscedata.switching.step.header': 'header_id'}
    _nom_pas = '02'

    def name_get(self, cursor, uid, ids, context=None):
        step_obj = self.pool.get('giscedata.switching.step')
        return step_obj.name_get_step(cursor, uid, ids, 'D1', self._nom_pas,
                                      context=context)

    def get_next_steps(self, cursor, uid, where, context=None):
        """returns next valid steps depending where we are"""
        return []

    def unlink(self, cursor, uid, ids, context=None):

        header_obj = self.pool.get('giscedata.switching.step.header')
        header_read_ids = self.read(cursor, uid, ids, ['header_id'])
        header_ids = [x['header_id'][0] for x in header_read_ids]
        res = super(GiscedataSwitchingD1_02,
                    self).unlink(cursor, uid, ids, context=context)
        header_obj.unlink(cursor, uid, header_ids)
        return res

    def onchange_tipus_activacio(self, cursor, uid, ids, tipus, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        pas = self.browse(cursor, uid, ids)

        data = {}

        if tipus == 'L':
            try:
                fi_lot = pas.sw_id.cups_polissa_id.lot_facturacio.data_final
                data_act = (datetime.strptime(fi_lot, '%Y-%m-%d') +
                            timedelta(days=1)).strftime('%Y-%m-%d')

                data = {'data_activacio': data_act}
            except:
                pass
        return {'value': data}

    def onchange_rebuig(self, cursor, uid, ids, rebuig, context=None):
        if not context:
            context = {}
        if ids and isinstance(ids, (list, tuple)):
            ids = ids[0]
        data = {}
        if not rebuig:
            pas = self.browse(cursor, uid, ids)
            if pas.sw_id.cups_polissa_id and pas.sw_id.cups_polissa_id.tarifa:
                polissa = pas.sw_id.cups_polissa_id
                data = {'tarifaATR': polissa.tarifa.codi_ocsum}
        return {'value': data}

    def generar_xml_reject(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML D1, pas 02 de rebuig"""

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'nom_pas': '02'})
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')
        pas = self.browse(cursor, uid, pas_id)
        return rebuig_obj.generar_xml(cursor, uid, pas, context=ctx)

    def generar_xml_accept(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML D1, pas 02 d'acceptació"""

        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        sw_obj = self.pool.get('giscedata.switching')
        pas = self.browse(cursor, uid, pas_id, context)
        sw = pas.sw_id

        msg = d1.MensajeAceptacionNotificacionCambiosATRDesdeDistribuidor()

        # capçalera
        capcalera = pas.header_id.generar_xml(pas)
        # Dades acceptació
        dades = d1.DatosAceptacion()
        dades.feed({
            'fecha_aceptacion': pas.data_acceptacio,
        })

        acceptacio = d1.AceptacionNotificacionCambiosATRDesdeDistribuidor()
        acceptacio.feed({
            'datos_aceptacion': dades,
        })

        msg.feed({
            'cabecera': capcalera,
            'aceptacion_notificacion_cambios_atr_desde_distribuidor': acceptacio,
        })
        msg.set_xml_encoding(pas.header_id.get_xml_encoding())
        msg.build_tree()
        fname = sw.generar_nom_xml(pas.emisor_id.ref,
                                   pas.receptor_id.ref,
                                   pas=self._nom_pas)
        return (fname, str(msg))

    def generar_xml(self, cursor, uid, pas_id, context=None):
        """Retorna la sol·licitud en format XML D1, pas 02
        """
        if not context:
            context = {}
        if isinstance(pas_id, (list, tuple)):
            pas_id = pas_id[0]
        pas = self.browse(cursor, uid, pas_id, context)

        if pas.rebuig:
            return self.generar_xml_reject(cursor, uid, pas_id, context)
        else:
            return self.generar_xml_accept(cursor, uid, pas_id, context)

    def create_from_xml_accept(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        vals = header_obj.create_from_xml(cursor, uid, sw_id, xml,
                                          context=context)

        vals.update({
            'rebuig': False,
            'data_acceptacio': xml.datos_aceptacion.fecha_aceptacion,
        })

        pas_id = self.create(cursor, uid, vals)
        sw_obj.create_step(cursor, uid, sw_id, self._nom_pas, pas_id)
        return pas_id

    def create_from_xml_reject(self, cursor, uid, sw_id, xml, context=None):
        """Omple el vals amb els valors especificats
        """
        rebuig_obj = self.pool.get('giscedata.switching.rebuig')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': self._name, 'nom_pas': self._nom_pas})
        return rebuig_obj.create_from_xml(cursor, uid, sw_id, xml, context=ctx)

    def create_from_xml(self, cursor, uid, sw_id, xml, context=None):
        """Retorna la sol·licitud en format XML D1, pas 02
        """
        if not context:
            context = {}

        if xml.datos_aceptacion:
            return self.create_from_xml_accept(cursor, uid, sw_id, xml, context)
        else:
            return self.create_from_xml_reject(cursor, uid, sw_id, xml, context)

    def dummy_create_reb(self, cursor, uid, sw_id, motius, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')

        sw = sw_obj.browse(cursor, uid, sw_id)
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        reb_list = header_obj.dummy_create_reb(cursor, uid, sw, motius, context)

        vals.update({
            'rebuig': True,
            'rebuig_ids': [(6, 0, reb_list)],
            'data_rebuig': datetime.today(),
        })

        return self.create(cursor, uid, vals)

    def dummy_create(self, cursor, uid, sw_id, context=None):
        """create with dummy default values for fields in process step"""

        sw_obj = self.pool.get('giscedata.switching')
        header_obj = self.pool.get('giscedata.switching.step.header')
        sw = sw_obj.browse(cursor, uid, sw_id)

        # By default we will dummy create without rejection
        # If rejected, do it manually, no dummy here :)
        today = datetime.strftime(datetime.now(), '%Y-%m-%d')
        vals = header_obj.dummy_create(cursor, uid, sw, context=context)

        vals.update({
            'data_acceptacio': today,
        })

        return self.create(cursor, uid, vals)

    def _ff_motiu_rebuig(self, cursor, uid, ids, field_name, arg, context=None):
        """Retorna un text amb els motius del rebuig"""

        res = dict.fromkeys(ids, '')
        for pas in self.browse(cursor, uid, ids):
            if pas.rebuig:
                txt = ','.join([r.desc_rebuig for r in pas.rebuig_ids])
                res[pas.id] = txt
        return res

    def get_additional_info(self, cursor, uid, step_id, context=None):
        if context is None:
            context = {}
        step = self.read(cursor, uid, step_id,
                         ['rebuig', 'motiu_rebuig', 'sw_id'])
        sw_id = step['sw_id'][0]
        step01 = self.pool.get('giscedata.switching.d1.01')
        step01_id = step01.search(cursor, uid, [('sw_id', '=', sw_id)])
        antinf = ''
        if len(step01_id) > 0:
            antinf = step01.get_additional_info(cursor, uid, step01_id[0]) + "."
        if step['rebuig']:
            return _(u"{0} Rebuig: {1}").format(antinf, step['motiu_rebuig'])
        else:
            return antinf

    def get_init_deadline_date(self, cursor, uid, sw_id, context=None):
        hobj = self.pool.get("giscedata.switching.helpers")
        return hobj.get_init_deadline_cn_m1(cursor, uid, sw_id, "D1", context)

    def get_notification_mail_name(self, cursor, uid, step_id, context=None):
        template_base = 'notification_atr_D1_{}'.format(self._nom_pas)
        if isinstance(step_id, list):
            templates = []
            for st_id in step_id:
                step = self.browse(cursor, uid, st_id)
                template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
                template = template.format(template_base)
                templates.append(template)
            return templates

        step = self.browse(cursor, uid, step_id)
        template = '{}_rebuig' if step.rebuig else '{}_acceptacio'
        template = template.format(template_base)
        return template

    _columns = {
        'header_id': fields.many2one('giscedata.switching.step.header',
                                     'Header', required=True),
        # Dades acceptació
        'data_acceptacio': fields.date(u"Data d'acceptació"),

        # Rebuig
        'rebuig': fields.boolean('Sol·licitud rebutjada'),
        'motiu_rebuig': fields.function(_ff_motiu_rebuig, method=True,
                                        string=u"Motiu rebuig", type='char',
                                        size=128, readonly=True, ),

        'data_rebuig': fields.date(u"Data Rebuig"),
    }

    _defaults = {
        'rebuig': lambda *a: False,
        'data_rebuig': lambda *a: datetime.today()
    }


GiscedataSwitchingD1_02()
