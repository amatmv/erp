# -*- coding: utf-8 -*-

from __future__ import absolute_import

import time
from datetime import datetime, timedelta
import calendar
import netsvc
import traceback
from ast import literal_eval as eval

from osv import osv, fields, orm
from tools.translate import _

from .giscedata_switching import SwitchingException

class GiscedataPolissa(osv.osv):
    """Funcions utilitzades pel switching
    """
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def estimar_consum_anual(self, cursor, uid, ids, context=None):
        """Retorna el consum anual (kWh) estimat per la polissa ids i l'any de
           la data especificada
        """
        if isinstance(ids, list):
            ids = ids[0]
        query = """
           select round(coalesce(mes.consumo,0)) as facturado,
           coalesce(mes.total, 0) as total
           from giscedata_polissa polissa
           LEFT JOIN (SELECT polissa_id,sum(il.quantity) as consumo,
           i.amount_total as total from
           giscedata_facturacio_factura f
           inner join account_invoice i on i.id = f.invoice_id
           inner join account_journal journal on journal.id = i.journal_id
           inner join giscedata_facturacio_factura_linia fl
           on fl.factura_id = f.id
           inner join account_invoice_line il
           ON fl.invoice_line_id = il.id
           where to_char(i.date_invoice, 'MM/YYYY') = '%02d/%s'
           and i.type = 'out_invoice' and journal.code like 'ENERGIA%%'
           and fl.tipus = 'energia'
           and f.id not in (select ref from giscedata_facturacio_factura f
           inner join account_invoice i on i.id = f.invoice_id
           inner join account_journal journal on journal.id = i.journal_id
           where i.date_invoice between '%s' and '%s'
           and i.type = 'out_refund' and journal.code like 'ENERGIA%%'
           and coalesce(ref,0)<>0)
           group by polissa_id, i.amount_total)mes
           ON mes.polissa_id = polissa.id where polissa.id = %d
           """
        p_obj = self.pool.get('giscedata.polissa')
        poli = p_obj.browse(cursor, uid, ids, context=context)
        data_actual_str = time.strftime('%Y-%m-%d')
        if poli.data_alta:
            data_alta = datetime.strptime(poli.data_alta, '%Y-%m-%d')
        else:
            data_alta = None
        data_final = datetime.strptime(data_actual_str, '%Y-%m-%d')
        data_inici = data_final - timedelta(days=365)
        if data_alta and data_alta > data_inici:
            data_inici = data_alta
        mes_inici = data_inici.month
        data_inici_str = data_inici.strftime('%Y-%m-%d')
        data_final_str = data_final.strftime('%Y-%m-%d')
        consum_anual = consum = 0
        n_reb = 0
        for i in range(12):
            _q = query % (data_inici.month, data_inici.year,
                          data_inici_str, data_final_str, ids)
            cursor.execute(_q)
            res = cursor.fetchall()[0]
            if res[0]:
                consum += res[0]
                n_reb += 1
            days = calendar.monthrange(data_inici.year, data_inici.month)[1]
            data_inici = data_inici + timedelta(days=days)
        if n_reb:
            if poli.facturacio == 1:
                consum_anual = consum * 12 / n_reb
            else:
                consum_anual = consum * 12 / (n_reb * 2)
        return consum_anual

    def arregla_cups(self, cursor, uid, polissa_id, context=None):
        ''' Arreglem CUPS
         això ens ho podem estalviar si ja ho arreglem al crear-lo, ja
         sigui des de web o automàticament
         FENOSA -> 1P
         ALTRES -> 0F'''
        if not polissa_id:
            return
        p_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')

        pol = p_obj.browse(cursor, uid, polissa_id, context)

        # Si no hi ha CUPS no podem fer res
        if not pol.cups:
            return ''

        res = ''

        # Si es de la nostra distribuidora no el podem canviar
        # ja que trencaria la sincronitzacio
        cups_fields = cups_obj.fields_get_keys(cursor, uid)
        if 'propi' in cups_fields and pol.cups.propi:
            return res

        if len(pol.cups.name) == 20:
            # FENOSA
            cups_name = pol.cups.name
            if pol.cups.distribuidora_id.ref == '0022':
                sufix = '1P'
            else:
                sufix = '0F'
            nou_cups = cups_name + sufix
            pol.cups.write({'name': nou_cups})
            pol = p_obj.browse(cursor, uid, polissa_id, context)
            res = (_(u'CUPS modificat (%s)  %s -> %s ') %
                   (sufix, cups_name, pol.cups.name))
        return res

    def crear_cas_atr(self, cursor, uid, polissa_id, proces=None,
                      config_vals=None, context=None):
        """ A partir d'una pólissa crea un cas C1/C2/M1/A3/B1/R1 pas 01.
            Per defecte crea C1/C2, però per context es pot seleccionar un M1
            amb el paràmetre 'proces'
        """
        if not context:
            context = {}
        log_params = {
            'origin': '',
            'pas': '01',
            'proces': '',
            'status': 'error',
            'info': '',
            'file': '',
            'emisor_id': False,
            'cups_text': '',
            'tipus': 'generate',
            'codi_emisor': ''
        }
        msg_err = False

        swlog_obj = self.pool.get('giscedata.switching.log')
        try:
            # Do this due the browse record object always pass the id as a list
            # of one element.
            if isinstance(polissa_id, (list, tuple)):
                polissa_id = polissa_id[0]

            supported_cases = 'C1', 'C2', 'M1', 'A3', 'B1', 'W1', 'D1', 'R1'

            proces = context.get('proces', proces or 'Cn')

            pol_obj = self.pool.get('giscedata.polissa')
            sw_obj = self.pool.get('giscedata.switching')
            swproc_obj = self.pool.get('giscedata.switching.proces')
            swpas_obj = self.pool.get('giscedata.switching.step')
            swinfo_obj = self.pool.get('giscedata.switching.step.info')

            pol = pol_obj.browse(cursor, uid, polissa_id, context)

            if proces == 'Cn':
                if u'proces: C1' in (pol.observacions or ''):
                    proces_name = 'C1'
                elif u'proces: C2' in (pol.observacions or ''):
                    proces_name = 'C2'
                elif u'proces: A3' in (pol.observacions or ''):
                    proces_name = 'A3'
                elif u'canvi_titular: 1' in (pol.observacions or ''):
                    proces_name = 'C2'
                else:
                    proces_name = 'C1'
            elif proces.startswith('R1'):
                proces_name = 'R1'
            else:
                proces_name = proces

            context['proces_name'] = proces_name
            # Fill origin and pas of sw log
            log_params['origin'] = _('Generació -> Polissa')
            log_params['file'] = _('Contracte: {0}').format(pol.name)
            log_params['cups_text'] = pol.cups and pol.cups.name
            log_params['proces'] = proces_name
            where = sw_obj.whereiam(cursor, uid, pol.cups.id, context=context)
            if where == 'comer':
                partner_id = sw_obj.partner_map(cursor, uid, pol.cups,
                                              pol.distribuidora.id)
            elif where == 'distri':
                partner_id = pol.comercialitzadora.id
            log_params['emisor_id'] = partner_id
            partner_obj = self.pool.get('res.partner')
            ref_emi = partner_obj.read(cursor, uid, partner_id, ['ref'], context=context)
            ref_emi = ref_emi.get('ref', '')
            log_params['codi_emisor'] = ref_emi

            def formatResult(message, case_id=False):
                logger = netsvc.Logger()
                action = '(%s) %s' % (proces_name, pol.name)
                logger.notifyChannel('switching', netsvc.LOG_DEBUG,
                                     '%s: %s' % (action, message))
                return [action, message, case_id or False]

            if proces_name not in supported_cases:
                return formatResult(_(u'Cas %s no suportat') % (proces_name))

            proces_id = swproc_obj.search(cursor, uid,
                                          [('name', '=', proces_name)])[0]

            # comprovem que hi hagi CUPS
            if not pol.cups:
                msg_err = _(u'Pólissa no te CUPS, no es pot fer cas ATR')
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err)

            #comprovem que no està en esborrany per M1, B1 i R1
            if pol.state == 'esborrany' and proces_name in ('M1', 'B1', 'W1', 'D1'):
                msg_err = _(u'Pólissa en estat %s, la saltem') % _(pol.state)
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err)

            #comprovem que està en esborrany per Cn i A3
            if pol.state != 'esborrany' and proces_name not in ('M1', 'B1', 'W1', 'D1', 'R1'):
                msg_err = _(u'Pólissa en estat %s, la saltem') % _(pol.state)
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err)

            if proces_name == 'W1' and not pol.get_last_selfmetering():
                msg_err = _(u'Pólissa sense autolectures, la saltem')
                log_params['info'] = msg_err
                swlog_obj.create(cursor, uid, log_params, context=context)
                return formatResult(msg_err)

            #calculem l'adressa i el mail a partir del partner
            sw_params = {
                'proces_id': proces_id,
                'cups_polissa_id': polissa_id,
            }
            vals = sw_obj.onchange_polissa_id(cursor, uid, [], polissa_id, None,
                                              context=context)

            if vals['warning']:
                #Hi ha hagut algun problema buscant les dades
                warn_txt = '%s: %s' % (
                    vals['warning']['title'],
                    vals['warning']['message'],
                )
                return formatResult(warn_txt)

            sw_params.update(vals['value'])
            #si no tenim ref_contracte, ens l'inventem (de moment)
            if not sw_params['ref_contracte']:
                sw_params['ref_contracte'] = '111111111'

            # Arreglem el CUPS
            result = self.arregla_cups(cursor, uid, polissa_id,
                                       context=context)
            if result:
                #warning: s'ha modificat el CUPS
                formatResult(result)

            sw_id = None
            pas_id = None
            info_id = None
            try:
                # Config Step Validation for more specific case creation
                # Suitable for third party applications
                # TODO: It may be more generic
                if config_vals:
                    if proces_name == 'M1':
                        m101_obj = self.pool.get('giscedata.switching.m1.01')
                        m101_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context)
                    elif proces_name == 'B1':
                        b101_obj = self.pool.get('giscedata.switching.b1.01')
                        b101_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context)
                    elif proces_name == 'A3':
                        a301_obj = self.pool.get('giscedata.switching.a3.01')
                        a301_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context)
                    elif proces_name == 'C1':
                        c101_obj = self.pool.get('giscedata.switching.c1.01')
                        c101_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context)
                    elif proces_name == 'C2':
                        c201_obj = self.pool.get('giscedata.switching.c2.01')
                        c201_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context)
                    elif proces_name == 'R1':
                        r101_obj = self.pool.get('giscedata.switching.r1.01')
                        r101_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context
                        )
                    elif proces_name == 'D1':
                        d101_obj = self.pool.get('giscedata.switching.d1.01')
                        d101_obj.config_step_validation(
                            cursor, uid, [], config_vals, context=context
                        )
                    # Sets retail_tariff if available
                    if 'retail_tariff' in config_vals:
                        sw_params.update({
                            'tarifa_comer_id': config_vals['retail_tariff'],
                            'actualitzar_tarifa_comer': config_vals.get('change_retail_tariff', False),
                            'mode_facturacio': config_vals['mode_facturacio'],
                            'actualitzar_mode_facturacio': config_vals.get('change_mode_facturacio', False),
                        })

                # creem el Cas
                sw_id = sw_obj.create(cursor, uid, sw_params)

                # creeem el pas
                pas_id = swpas_obj.get_step(cursor, uid, '01', proces_name)
                #Creant info ja crea automaticament tota la info del pas
                info_vals = {
                    'sw_id': sw_id,
                    'proces_id': proces_id,
                    'step_id': pas_id,
                }
                info_id = swinfo_obj.create(cursor, uid, info_vals)
                info = swinfo_obj.browse(cursor, uid, info_id)
                model_obj, model_id = info.pas_id.split(',')
                pas_obj = self.pool.get(model_obj)
                pas = pas_obj.browse(cursor, uid, int(model_id))

                # specific M1-01 vals
                # TODO: It may be more generic
                if config_vals:
                    if proces_name == 'M1':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )
                    elif proces_name == 'B1':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )
                    elif proces_name == 'A3':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )
                    elif proces_name == 'C1':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )
                    elif proces_name == 'C2':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )
                    elif proces_name == 'R1':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )
                    elif proces_name == 'D1':
                        pas_obj.config_step(
                            cursor, uid, pas.id, config_vals, context=context
                        )

                if not sw_id or not pas_id or not info_id:
                    msg_err = _(u'Error creant cas c:%s p:%s i: %s') % (
                        sw_id, pas_id, info_id
                    )
                    log_params['info'] = msg_err
                    swlog_obj.create(cursor, uid, log_params, context=context)
                    return formatResult(msg_err, sw_id)

                message = _(u'Cas %s creat correctament') % sw_id

                if pas.validacio_pendent:
                    message += _(u'. WARN Nom: %s, %s %s') % (
                        pas.nom, pas.cognom_1, pas.cognom_2)

                result = formatResult(message, sw_id)
                # Obrir o deixar en Borrador el cas segons variable de
                # configuració
                conf_obj = self.pool.get('res.config')
                case_state = conf_obj.get(
                    cursor, uid, 'sw_from_polissa_case_state_open', '1'
                )
                if case_state != '0':
                    sw_obj.case_open(cursor, uid, sw_id)
                # Update Aditional Info
                pas_obj.write(cursor, uid, pas.id, {"date_created": pas.date_created})
                log_params['info'] = message
                log_params['status'] = 'correcte'
                sw_inst = sw_obj.browse(cursor, uid, sw_id, context=context)
                log_params['request_code'] = sw_inst.codi_sollicitud or ''
                swlog_obj.create(cursor, uid, log_params, context=context)
                return result
            except SwitchingException, e:
                msg_err = _(u"ERROR: No s'ha creat el Cas per problemes amb la "
                            u"configuració del Pas 01: "
                            u"{0} -> {1}").format(e, e.error_fields)
                log_params['info'] = msg_err
                log_params['status'] = 'error'
                # With this param will force crate of switching logs
                ctx = context.copy()
                ctx['force_log'] = True
                swlog_obj.create(cursor, uid, log_params, context=ctx)
                return formatResult(msg_err, sw_id)
            except Exception, e:
                msg_err = _(u'ERROR Creant el Cas: (%s)') % (e,)
                traceback.print_stack()
                if sw_id:
                    # Delete created case
                    sw_obj.write(cursor, uid, sw_id, {'state': 'draft'})
                    sw_obj.unlink(cursor, uid, sw_id)
                if not sw_id or not pas_id or not info_id:
                    msg_err = _(u"ERROR: No s'ha creat Cas: %s") % (e,)
                    log_params['info'] = msg_err
                    log_params['status'] = 'error'
                    # With this param will force crate of switching logs
                    ctx = context.copy()
                    ctx['force_log'] = True
                    swlog_obj.create(cursor, uid, log_params, context=ctx)
                    return formatResult(msg_err, sw_id)
                log_params['info'] = msg_err
                log_params['status'] = 'error'
                # With this param will force crate of switching logs
                ctx = context.copy()
                ctx['force_log'] = True
                swlog_obj.create(cursor, uid, log_params, context=ctx)
                return formatResult(msg_err, sw_id)
        except Exception, e:
            if not msg_err:
                msg_err = _(u'ERROR Creant el Cas: (%s)') % (e,)
            log_params['info'] = msg_err
            log_params['status'] = 'error'
            swlog_obj = osv.TransactionExecute(cursor.dbname, uid, 'giscedata.switching.log')
            swlog_obj.create(log_params, context=context)
            raise e

    def get_address_with_phone(self, cursor, uid, polissa_id, context=None):
        if context is None:
            context = {}

        if isinstance(polissa_id, (list, tuple)):
            polissa_id = polissa_id[0]

        polissa = self.browse(cursor, uid, polissa_id, context=context)

        # First we get the addres from pagador
        direccio = polissa.direccio_pagament

        # If we don't have pagador addres or it doesn't have phones,
        # we get notification address
        if not direccio or \
                (direccio and not direccio.phone and not direccio.mobile):
            direccio = polissa.direccio_notificacio

            # If we don't have notification address or it doesn't have phones
            # we search in the addresses of the titular until we find a phone
            if not direccio or \
                    (direccio and not direccio.phone and not direccio.mobile):
                direccio = False
                for dir in polissa.titular.address:
                    if dir.phone or dir.mobile:
                        direccio = dir
                        break

        return direccio

    def escull_llista_preus(self, cursor, uid, id, llista, context=None):
        """ A partir d'una llista de preus, necessitem la tarifa més
        adequada i única. Retornem la llista si només n'hi ha una, si no,
        False. S'ha de modificar segons convingui a cada client."""
        pricelist_obj = self.pool.get('product.pricelist')
        if llista and isinstance(llista[0], int):
            llista = pricelist_obj.browse(cursor, uid, llista, context)
        if not llista or len(llista) > 1:
            llista_preus = False
        else:
            llista_preus = llista[0]

        return llista_preus

    def get_consum_anual_Cn_02(self, cursor, uid, polissa_id, context=None):
        """ Agafem el consum anual de la pólissa segons el cas Cn-02 dels
        processos finalitzats"""
        res = False

        if isinstance(polissa_id, (tuple, list)):
            polissa_id = polissa_id[0]

        sw_obj = self.pool.get('giscedata.switching')

        # busquem els Cn relacionats amb la polissa
        search_vals = [('cups_polissa_id', '=', polissa_id),
                       ('proces_id.name', 'in', ['C1', 'C2']),
                       ('state', '=', 'done'),
                       ]
        sw_ids = sw_obj.search(cursor, uid, search_vals)

        for sw_id in sw_ids:
            sw_vals = sw_obj.read(cursor, uid, sw_id, ['proces_id'])
            proces = sw_vals['proces_id'][1].lower()
            pas_obj = self.pool.get('giscedata.switching.%s.02' % proces)

            pas_ids = pas_obj.search(cursor, uid, [('sw_id', '=', sw_id)])
            if pas_ids:
                pas_vals = pas_obj.read(cursor, uid, pas_ids[0],
                                        ['rebuig', 'consum_anual'])
                if not pas_vals['rebuig']:
                    # Available only in 0.2 ATR format (to be erased)
                    consum_anual = pas_vals.get('consum_anual', False)
                    # si ja tenim la lectura, ja estem
                    if consum_anual:
                        return consum_anual

        return res

    def get_last_selfmetering(self, cursor, uid, polissa_id, context=None):
        """
         Gets last self-metering date from contract active meter
        """
        if isinstance(polissa_id, (list,tuple)):
            polissa_id = polissa_id[0]

        measure_obj = self.pool.get('giscedata.lectures.lectura.pool')
        if not measure_obj:
            return False
        # autolectura
        origen_obj = self.pool.get('giscedata.lectures.origen')
        origen_autolectura_id = origen_obj.search(cursor, uid,
                                                  [('codi', '=', '50')])[0]

        res = False
        pol_vals = self.read(cursor, uid, polissa_id, ['comptadors'],
                             context=context)

        comptadors = pol_vals['comptadors']
        search_vals = [('comptador', 'in', comptadors),
                       ('origen_id', '=', origen_autolectura_id)]
        measures_ids = measure_obj.search(cursor, uid, search_vals,
                                          order='name DESC')
        if measures_ids:
            measure_vals = measure_obj.read(cursor, uid, measures_ids[0],
                                            ['name'], context=context)
            date = measure_vals['name']
            search_vals.append(('name', '=', date))
            res = measure_obj.search(cursor, uid, search_vals,
                                     context=context)

        return res

    def get_last_self_measure_vals(self, cursor, uid, polissa_id, context=None):
        """ Returns self measures to object (R1) from invoice start date. We
        select only pool measures with retail origin:
            Manual (MA),
            Self-Measure (AL),
            Virtual Office (OV)
        :param invoice_id: invoice id
        :param from_date: Initial search date not include (until today)
        :param origins: origins dict by id
        :return: measures tuple of id lists ([energy], [maximeters]) with all
        available periods found for more recent measure group
        """
        if isinstance(polissa_id, (tuple, list)):
            polissa_id = polissa_id[0]

        orig_obj = self.pool.get('giscedata.lectures.origen_comer')
        measure_obj = self.pool.get('giscedata.lectures.lectura.pool')
        maximeter_obj = self.pool.get('giscedata.lectures.potencia.pool')

        orig_codes = ['AL', 'MA', 'OV']
        orig_ids = orig_obj.search(
            cursor, uid, [('codi', 'in', orig_codes)]
        )

        polissa_vals = self.read(
            cursor, uid, polissa_id, ['comptadors']
        )

        energy_measures = []
        maximeter_measures = []

        # search origins
        comptadors = polissa_vals['comptadors']
        search_vals = [('comptador', 'in', comptadors),
                       ('origen_comer_id', 'in', orig_ids)]

        measures_ids = measure_obj.search(
            cursor, uid, search_vals, order='name DESC'
        )
        if measures_ids:
            measure_vals = measure_obj.read(
                cursor, uid, measures_ids[0], ['name'], context=context
            )
            date = measure_vals['name']
            search_vals.append(('name', '=', date))
            energy_measures = measure_obj.search(
                cursor, uid, search_vals, context=context
            )
            maximeter_measures = maximeter_obj.search(
                cursor, uid, search_vals, context=context
            )

        return energy_measures, maximeter_measures

    def wkf_baixa(self, cursor, uid, ids):
        res = super(GiscedataPolissa, self).wkf_baixa(cursor, uid, ids)
        conf_obj = self.pool.get('res.config')
        proces_obj = self.pool.get("giscedata.switching.proces")
        sw_obj = self.pool.get("giscedata.switching")

        proces_list = eval(conf_obj.get(cursor, uid, 'sw_baixa_contract_close_cases', "[]"))
        proces_ids = proces_obj.search(cursor, uid, [('name', 'in', proces_list)])

        sw_ids = sw_obj.search(
            cursor, uid,
            [('proces_id', 'in', proces_ids),
             ('state', '!=', 'done'),
             ('cups_polissa_id', 'in', ids)]
        )

        sw_obj.write(cursor, uid, sw_ids, {'state': 'done'})

        return res

    def pre_canceling_behaviour(self, cursor, uid, ids, context=None):
        """
        Executa un comportament desitjat abans de cancelar el contracte.
        En aquest cas, no interessa fer res en especial.
        :returns: None
        """
        pass

GiscedataPolissa()


class GiscedataPolissaTarifa(osv.osv):
    """Funcions utilitzades pel switching
    """
    _name = 'giscedata.polissa.tarifa'
    _inherit = 'giscedata.polissa.tarifa'

    def get_codi_dh(self, cursor, uid, ids, context=None):
        """Retorna codi DH segons taula SW 35 (Tipo discriminacion horaria)"""
        if isinstance(ids, (list, tuple)):
            tar_id = ids[0]
        else:
            tar_id = ids

        tarifa_vals = self.read(cursor, uid, tar_id, ['name'])
        tarifa_id = tarifa_vals['id']
        num_periodes = self.get_num_periodes(cursor, uid, [tarifa_id])
        #Nomes retorna el numero agrupat
        #Per tant s'ha de multiplicar
        #per 2 per obtindre el numero real en les 3.X
        if tarifa_vals['name'].startswith('3.'):
            num_periodes *= 2
        if tarifa_vals['name'] in ('2.0DHS', '2.1DHS'):
            dh = '8'
        else:
            dh = str(num_periodes)

        return dh

GiscedataPolissaTarifa()
