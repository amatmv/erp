# -*- coding: utf-8 -*-
from osv import osv, fields


class res_comunitat_autonoma(osv.osv):
    """Comunitat Autònomes"""
    _name = 'res.comunitat_autonoma'
    _inherit = 'res.comunitat_autonoma'

    _columns = {
        'partner_id': fields.many2one('res.partner', 'Partner'),
    }


res_comunitat_autonoma()
