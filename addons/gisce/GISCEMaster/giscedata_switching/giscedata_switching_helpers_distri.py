# -*- coding: utf-8 -*-

import logging
from tools.translate import _
from osv import osv
from datetime import datetime, timedelta
from giscedata_switching_helpers import GiscedataSwitchingException


logger = logging.getLogger('openerp.switching')


class GiscedataSwitchingHelpersDistri(osv.osv):

    _name = 'giscedata.switching.helpers.distri'
    _auto = False

    def crear_polissa_from_a3(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        if sw.cups_polissa_id:
            info = _(
                u'Aquest {2}-{3} ({0}) ja te un contracte assignat ({1}). '
                u'AQUEST CANVI JA HA ESTAT ACTIVAT?'
            ).format(sw.name, sw.cups_polissa_id.id,
                     sw.proces_id.name, sw.step_id.name)
            raise GiscedataSwitchingException(info, _(u'ERROR'))
        polissa_obj = self.pool.get('giscedata.polissa')
        partner_obj = self.pool.get('res.partner')
        partner_address_obj = self.pool.get('res.partner.address')
        tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        pas = sw_obj.get_pas(cursor, uid, sw, context=None)
        vat_prefix = pas.tipus_document == 'PS' and 'PS' or 'ES'
        vat = pas.codi_document
        if not vat.startswith(vat_prefix):
            vat = "{0}{1}".format(vat_prefix, pas.codi_document)
        partner_id = partner_obj.search(cursor, uid, [
            ('vat', '=', vat)
        ])
        if not partner_id:
            if pas.persona == 'F':
                name_as_dict = {
                    "N": pas.nom,
                    "C1": pas.cognom_1,
                    "C2": pas.cognom_2
                }
                partner_name = self.pool.get("res.partner").ajunta_cognoms(cursor, uid, name_as_dict, context=context)
            else:
                partner_name = '{p.nom}'.format(p=pas)
            partner_id = partner_obj.create(cursor, uid, {
                'name': partner_name,
                'vat': vat,
            })
        else:
            if len(partner_id) > 1:
                raise osv.except_osv(_('Error'), _('El ID del titular está duplicado. Para completar el alta, '
                                                   'primero se deben eliminar los duplicados del ID.'))
            else:
                partner_id = partner_id[0]
                partner_name = partner_obj.read(
                    cursor, uid, partner_id, ['name']
                )['name']
        if pas.ind_direccio_fiscal == 'F' and pas.fiscal_address_id:
            address_id = pas.fiscal_address_id.id
        else:
            municipi_id, country_id, state_id = False, False, False
            if sw.cups_id.id_municipi:
                municipi_id = sw.cups_id.id_municipi.id
                if sw.cups_id.id_municipi.state:
                    state_id = sw.cups_id.id_municipi.state.id
                    if sw.cups_id.id_municipi.state.country_id:
                        country_id = sw.cups_id.id_municipi.state.country_id.id
            addr_vals = {
                'nv': sw.cups_id.nv,
                'pnp': sw.cups_id.pnp,
                'aclarador': sw.cups_id.aclarador,
                'zip': sw.cups_id.dp,
                'id_municipi': municipi_id,
                'country_id': country_id,
                'state_id': state_id,
                'tv': sw.cups_id.tv and sw.cups_id.tv.id or False,
                'es': sw.cups_id.es,
                'pt': sw.cups_id.pt,
                'pu': sw.cups_id.pu
            }
            if len(pas.telefons) > 0:
                addr_vals['phone'] = pas.telefons[0].numero
            if len(pas.telefons) > 1:
                addr_vals['mobile'] = pas.telefons[1].numero
            address_id = partner_address_obj.create(
                cursor, uid, addr_vals, context=context
            )
        partner_info = partner_obj.read(cursor, uid, partner_id, ['address'])
        if address_id not in partner_info['address']:
            partner_address_obj.write(
                cursor, uid, address_id, {'partner_id': partner_id, 'name': partner_name}
            )
        pot_max = (max([p.potencia for p in pas.pot_ids]) / 1000.0)
        tarif = tarifa_obj.search(cursor, uid, [
            ('codi_ocsum', '=', pas.tarifaATR)
        ])[0]
        tnorm = False
        aux = polissa_obj.onchange_potencia(cursor, uid, [], pot_max, tarif, context=context)
        if aux.get('value') and aux['value'].get('tensio_normalitzada'):
            tnorm = aux['value']['tensio_normalitzada']
        vals = {
            'titular': partner_id,
            'cnae': pas.cnae.id,
            'tarifa': tarif,
            'tensio_normalitzada': tnorm,
            'potencia': pot_max,
            'comercialitzadora': pas.emisor_id.id,
            'cups': sw.cups_id.id,
            'data_alta': pas.data_accio,
            'autoconsumo': pas.tipus_autoconsum,
            'contract_type': pas.tipus_contracte,
        }
        # Eventual without meter
        if pas.tipus_contracte == '09':
            if pas.expected_consumption:
                vals['expected_consumption'] = pas.expected_consumption
        # Eventuals has end of contract date
        if pas.tipus_contracte in ('02', '09'):
            vals['renovacio_auto'] = False
            vals['data_baixa'] = pas.data_final
        pagador_vals = polissa_obj.onchange_pagador_sel(
            cursor, uid, [], 'comercialitzadora', titular=partner_id,
            pagador=None, direccio_pagament=None, tipo_pago=None,
            comercialitzadora=pas.emisor_id.id, notificacio='titular',
            context=context
        )
        vals.update(pagador_vals['value'])
        fiscal_vals = self.get_fiscal_and_payment_vals_from_comer(cursor, uid, vals['pagador'], context=context)
        vals.update(fiscal_vals)
        noti_vals = polissa_obj.onchange_notificacio(
            cursor, uid, [], 'titular', partner_id, None, None,
            context=context
        )
        noti_vals['value']['notificacio'] = 'titular'
        vals.update(noti_vals['value'])
        pol_id = polissa_obj.create(cursor, uid, vals, context=context)
        polissa = polissa_obj.browse(cursor, uid, pol_id)
        polissa.generar_periodes_potencia(
            context={'force_genpot': True}
        )
        # omplim amb les potències del pas [kW]
        for periode in polissa.potencies_periode:
            potencia = max([
                p.potencia for p in pas.pot_ids
                if p.name == periode.periode_id.name
            ]) / 1000.0
            periode.write({'potencia': potencia})
        sw.write({'cups_polissa_id': pol_id})
        # Eventual without meter
        if pas.tipus_contracte == '09':
            meter_serial = 'EV{0}'.format(polissa.name or polissa.id)
            imd_obj = self.pool.get('ir.model.data')
            meter_fake_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_lectures', 'meter_fake'
            )[1]
            stock_prod_lot_obj = self.pool.get('stock.production.lot')
            params = {
                'name': meter_serial,
                'product_id': meter_fake_id
            }
            stock_prod_lot_id = stock_prod_lot_obj.create(
                cursor, uid, params, context=context
            )
            lectura_obj = self.pool.get('giscedata.lectures.lectura')
            origen_obj = self.pool.get('giscedata.lectures.origen')
            origen_estimada_id = origen_obj.search(
                cursor, uid, [('codi', '=', '40')]
            )[0]
            comptador_obj = self.pool.get(
                'giscedata.lectures.comptador'
            )
            data_alta = datetime.strptime(polissa.data_alta, "%Y-%m-%d")
            params = {
                'name': meter_serial,
                'data_alta': data_alta,
                'data_baixa': polissa.data_baixa,
                'serial': stock_prod_lot_id,
                'polissa': polissa.id
            }
            comptador_id = comptador_obj.create(
                cursor, uid, params, context=context
            )
            periodes = tarifa_obj.get_periodes(
                cursor, uid, [polissa.tarifa.id], context=context
            )
            data_alta_cnmc = (data_alta - timedelta(days=1))
            params_ini = {
                'name': data_alta_cnmc,
                'lectura': 0,
                'tipus': 'A',
                'periode': None,
                'origen_id': origen_estimada_id,
                'comptador': comptador_id
            }
            params_end = {
                'name': polissa.data_baixa,
                'lectura': 0,
                'tipus': 'A',
                'periode': None,
                'origen_id': origen_estimada_id,
                'comptador': comptador_id
            }
            if len(periodes) == 1:
                multiplier_periods = {'P1': 1}
            elif len(periodes) == 2:
                multiplier_periods = {'P1': 0.4, 'P2': 0.6}
            else:
                multiplier_periods = {'P1': 0.25, 'P2': 0.55, 'P3': 0.2}
            for periode_name, periode_id in periodes.items():
                params_ini['periode'] = periode_id
                params_end['periode'] = periode_id
                lectura_multiplier = multiplier_periods.get(
                    periode_name, 0
                )
                params_end['lectura'] = pas.expected_consumption * lectura_multiplier
                lectura_obj.create(cursor, uid, params_ini, context=context)
                lectura_obj.create(cursor, uid, params_end, context=context)
        res = ('OK', _(u'Pòlissa creada en esborrany correctament'))
        return res

    def activar_polissa_from_m1(self, cursor, uid, sw_id, context=None):
        m101_obj = self.pool.get('giscedata.switching.m1.01')
        sw_obj = self.pool.get('giscedata.switching')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        res = False
        try:
            pas01 = m101_obj.browse(
                cursor, uid,
                m101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )

            if pas01.sollicitudadm in ['A', 'S']:
                res = self.activar_polissa_from_m1_canvi_titular(
                    cursor, uid, sw_id, context=context
                )
        except Exception as e:
            raise osv.except_osv("error", e.message)
        return res

    def activar_polissa_from_m1_canvi_titular(self, cursor, uid, sw_id, context=None):
        m101_obj = self.pool.get('giscedata.switching.m1.01')
        sw_obj = self.pool.get('giscedata.switching')
        partner_obj = self.pool.get('res.partner')

        sw = sw_obj.browse(cursor, uid, sw_id, context=context)

        try:
            pas01 = m101_obj.browse(
                cursor, uid,
                m101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )
            if pas01.sollicitudadm not in ['A', 'S']:
                return _(u"ERROR"), _(u"No es tracta d'un canvi de titular.")
            if not pas01.fiscal_address_id:
                return _(u"ERROR"), _(u"No hi ha adreça associada al pas ATR.")

            vat = pas01.codi_document
            vat_prefix = pas01.tipus_document == 'PS' and 'PS' or 'ES'
            if not vat.startswith(vat_prefix):
                vat = "{0}{1}".format(vat_prefix, pas01.codi_document)
            # pot existir > 1 partner amb el mateix cifnif?
            existeix = partner_obj.search(cursor, uid, [('vat', '=', vat)])
            if existeix:
                # si el partner ja existeix, llavors afegim l'adreça, però
                # primer ens haurem d'assegurar que l'@ no la tingui ja lligada
                partner_addresses = partner_obj.read(
                    cursor, uid, existeix[0], ['address'], context=context
                )['address']
                equal = pas01.fiscal_address_id.equal(
                    partner_addresses, context=context
                )[pas01.fiscal_address_id.id]
                if not equal:
                    params = {
                        'address': [(4, pas01.fiscal_address_id.id)]
                    }
                    partner_obj.write(
                        cursor, uid, existeix[0], params, context=context
                    )
                    return (
                        _(u'EXIT'),
                        _(u"S'ha actualitzat correctament l'adreça del client"
                          u"amb codi document {}").format(vat)
                    )
                else:
                    return (
                        _(u'EXIT'),
                        _(u"No s'ha actualitzat l'adreça del client"
                          u"amb codi document {} ja que ja la tenia"
                          u"assignada").format(vat)
                    )
            else:
                if partner_obj.vat_es_empresa(cursor, uid, vat):
                    nom = pas01.nom
                else:
                    name_as_dict = {
                        "N": pas01.nom,
                        "C1": pas01.cognom_1,
                        "C2": pas01.cognom_2
                    }
                    nom = self.pool.get("res.partner").ajunta_cognoms(cursor, uid, name_as_dict, context=context)

                params = {
                    'vat': vat,
                    'name': nom,
                    'address': [(6, 0, [pas01.fiscal_address_id.id])]
                }
                partner_obj.create(
                    cursor, uid, params, context=context
                )
                return (
                    _(u'EXIT'),
                    _(u"S'ha creat el client {} amb codi document {}").format(
                        nom, vat
                    )
                )
        except Exception as e:
            return _(u'ERROR'), e.message

    def get_ultimes_lectura_from_days(self, cursor, uid, polissa_id, dies,
                                      context=None):
        data_actual = datetime.today()
        data_desde = data_actual + timedelta(days=-dies)
        data_actual = datetime.strftime(data_actual, '%Y-%m-%d')
        data_desde = datetime.strftime(data_desde, '%Y-%m-%d')

        sql = '''SELECT MAX(l.name) as last_date
        FROM giscedata_lectures_lectura AS l
        LEFT JOIN giscedata_lectures_comptador AS c ON (l.comptador = c.id)
        LEFT JOIN giscedata_polissa AS p ON (c.polissa=p.id)
        WHERE p.id = %(polissa_id)s and c.active 
        and (l.name BETWEEN (Date %(data_i)s) and (Date %(data_f)s))
        '''

        cursor.execute(
            sql,
            {'polissa_id': polissa_id,
             'data_i': data_desde,
             'data_f': data_actual
             }
        )

        res = cursor.dictfetchall()

        return res[0]['last_date'] if res[0]['last_date'] else False

    def canvi_de_comercialitzadora_c1_02(self, cursor, uid, polissa_id, partner_id, dies, pas01, context=None):
        pol_obj = self.pool.get('giscedata.polissa')

        if isinstance(polissa_id, (tuple, list)):
            polissa_id = polissa_id[0]

        pol_obj.send_signal(cursor, uid, [polissa_id], ['modcontractual'])

        try:
            vals = self.get_fiscal_and_payment_vals_from_comer(cursor, uid, partner_id, context=context)
            vals.update({
                'comercialitzadora': partner_id,
            })
            if pas01.bono_social:
                vals.update({
                    'bono_social': pas01.bono_social,
                })

            pol_obj.write(cursor, uid, [polissa_id], vals, context=context)

            wz_crear_mc_obj = self.pool.get('giscedata.polissa.crear.contracte')

            params = {
                'accio': 'nou',
                'duracio': 'actual'
            }

            ctx = {'active_id': polissa_id}

            if context.get('lang', False):
                ctx.update({'lang': context['lang']})

            data_ultima_lectura = self.get_ultimes_lectura_from_days(
                cursor, uid, polissa_id, dies, context=context
            )

            if not data_ultima_lectura:
                pol_obj.send_signal(
                    cursor, uid, [polissa_id], ['undo_modcontractual']
                )
                return False, 'No s\'han trobat lectures'

            data_ultima_lectura = datetime.strftime(
                datetime.strptime(data_ultima_lectura, '%Y-%m-%d') + timedelta(days=1),
                '%Y-%m-%d'
            )

            params.update(
                {'data_inici': data_ultima_lectura}
            )

            wz_id = wz_crear_mc_obj.create(cursor, uid, params, context=ctx)

            res_msg = wz_crear_mc_obj.onchange_duracio(
                cursor, uid, [wz_id], data_ultima_lectura, 'actual',
                context=ctx
            )

            if res_msg.get('warning', False):
                pol_obj.send_signal(
                    cursor, uid, [polissa_id], ['undo_modcontractual']
                )
                return False, _('La data final de la modificació contractual '
                                'és inferior a la data de última lectura')

            # Check wiz dates if not changed to false
            else:
                value = res_msg.get('value', False)
                if value:
                    data_final = value.get('data_final', False)
                    if data_final:
                        wz_crear_mc_obj.write(
                            cursor, uid, [wz_id], {'data_final': data_final}
                        )
                        wz_crear_mc_obj.action_crear_contracte(
                            cursor, uid, [wz_id], ctx
                        )
                        return True, _('S\'ha realitzat el canvi '
                                       'de comercialitzadora correctament')

                pol_obj.send_signal(
                    cursor, uid, [polissa_id], ['undo_modcontractual']
                )
                return False, _('La data final de la modificació contractual '
                                'és inferior a la data de última lectura')
        except Exception as e:
            pol_obj.send_signal(
                cursor, uid, [polissa_id], ['undo_modcontractual']
            )
            return False, e.message

    def get_fiscal_and_payment_vals_from_comer(self, cursor, uid, partner_id, context=None):
        if context is None:
            context = {}
        partner_obj = self.pool.get("res.partner")
        bank_obj = self.pool.get("res.partner.bank")
        partner_info = partner_obj.read(cursor, uid, partner_id, ['name', 'payment_type_customer', 'bank_ids'], context=context)
        if not partner_info.get("payment_type_customer"):
            raise Exception(
                _(u"L'empresa {0} no te cap tipus de pagament a client "
                  u"configurat. S'ha de configurar el tipus de pagament a "
                  u"client per poder realitzar l'activació automàtica del "
                  u"cas ATR.").format(partner_info['name'])
            )
        payment_type = partner_info.get("payment_type_customer")[0]
        payment_account = False
        for bank in partner_info.get('bank_ids', []):
            bank_info = bank_obj.read(cursor, uid, bank, ['default_bank'])
            if bank_info.get('default_bank'):
                payment_account = bank
                break
            elif not payment_account:
                payment_account = bank
        fiscal_dir = partner_obj.address_get(cursor, uid, [partner_id], adr_pref=['invoice']).get('invoice')
        return {
            'tipo_pago': payment_type,
            'bank': payment_account,
            'pagador_sel': 'comercialitzadora',
            'pagador': partner_id,
            'direccio_pagament': fiscal_dir
        }

    def activa_polissa_from_cn(self, cursor, uid, sw_id, context=None):
        succes = False
        try:
            sw_obj = self.pool.get('giscedata.switching')
            polissa_obj = self.pool.get('giscedata.polissa')
            c101_obj = self.pool.get('giscedata.switching.c1.01')
            wiz_create_step_obj = self.pool.get("wizard.create.step")

            sw = sw_obj.browse(cursor, uid, sw_id)

            days_before = context.get('days_before', False)

            pas01 = c101_obj.browse(
                cursor, uid,
                c101_obj.search(cursor, uid, [('sw_id', '=', sw.id)])[0]
            )

            if sw.rebuig:
                info = _(u'Aquest cas (%s) és un '
                         u'rebuig (%s-%s)') % (sw.name, sw.proces_id.name,
                                               sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            polissa = polissa_obj.browse(
                cursor, uid, sw.cups_polissa_id.id, context=context
            )

            if not polissa.state == 'activa':
                # ERROR, la Pólissa ha d'estar activa
                info = _(u"Aquesta pólissa (%s) del cas '%s' ha "
                         u"d'estar activa") % (sw.cups_polissa_id.name,
                                                     sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            succes = self.canvi_de_comercialitzadora_c1_02(
                cursor, uid, polissa.id, pas01.header_id.emisor_id.id,
                days_before, pas01, context=context
            )
            if succes[0]:
                wiz_create_step_id = wiz_create_step_obj.create(
                    cursor, uid, {}, context={'active_ids': [sw_id]}
                )
                wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id],
                                          {'step': '05'})
                wiz_create_step_obj.action_create_steps(
                    cursor, uid, [wiz_create_step_id],
                    context={'active_ids': [sw_id]}
                )
            else:
                raise GiscedataSwitchingException(succes[1], _(u'ERROR'))

            return 'OK', succes[1]

        except Exception as e:
            if succes:
                return 'ERROR', succes[1]
            else:
                return 'ERROR', e.message

    def activa_polissa_from_a3(self, cursor, uid, sw_id, context=None):

        sw_obj = self.pool.get('giscedata.switching')
        pol_obj = self.pool.get('giscedata.polissa')
        wiz_create_step_obj = self.pool.get("wizard.create.step")

        try:
            sw = sw_obj.browse(cursor, uid, sw_id)

            if sw.rebuig:
                info = _(u'Aquest cas (%s) és un '
                         u'rebuig (%s-%s)') % (sw.name, sw.proces_id.name,
                                               sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            days_before = context.get('days_before', False)

            polissa = pol_obj.browse(
                cursor, uid, sw.cups_polissa_id.id, context=context
            )

            if not polissa.state == 'esborrany':
                # ERROR, la Pólissa ha d'estar en esborrany
                info = _(u"Aquesta pólissa (%s) del cas '%s' "
                         u"ja està activa") % (sw.cups_polissa_id.name,
                                               sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            data_ultima_lectura = self.get_ultimes_lectura_from_days(
                cursor, uid, polissa.id, days_before, context=context
            )

            if not data_ultima_lectura:
                info = _(u"No s'han trobat lectures per el rang especificat")
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            data_ultima_lectura = datetime.strftime(
                datetime.strptime(data_ultima_lectura, '%Y-%m-%d') + timedelta(days=1),
                '%Y-%m-%d'
            )

            pol_obj.write(
                cursor, uid, polissa.id, {'data_alta': data_ultima_lectura},
                context=context
            )

            # Si la polissa no te informat el camp 'data_firma_contracte' s'emplena amb la data d'avui
            if not polissa.data_firma_contracte:
                today = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                pol_obj.write(cursor, uid, polissa.id, {'data_firma_contracte': today})
            if not polissa.tensio_normalitzada:
                raise osv.except_osv(_(u"Falten Dades"), _(u"S'ha de emplenar la 'Tensió Normalitzada' del contracte per poder activar el cas ATR."))

            pol_obj.send_signal(
                cursor, uid, [polissa.id], ['validar', 'contracte']
            )

            pol_state = pol_obj.read(
                cursor, uid, polissa.id, ['state'], context=context
            )['state']

            if pol_state == 'activa':
                wiz_create_step_id = wiz_create_step_obj.create(
                    cursor, uid, {}, context={'active_ids': [sw_id]}
                )
                wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id],
                                          {'step': '05'})
                wiz_create_step_obj.action_create_steps(
                    cursor, uid, [wiz_create_step_id],
                    context={'active_ids': [sw_id]}
                )

                return 'OK', _(u'S\'ha generat el pas 05 i activat '
                               u'correctament el contracte')
            else:
                return 'ERROR', _(u'No s\ha pogut activar el conntracte')

        except Exception as e:
            return 'ERROR', e.message

    def activar_r1_01_038(self, cursor, uid, sw_id, context=None):
        logger.info('Activating R1-038 Step 01 for SW (id: {})'.format(sw_id))
        sw_obj = self.pool.get('giscedata.switching')
        sw = sw_obj.browse(cursor, uid, sw_id, context=context)
        sw.cups_polissa_id.write({'no_cessio_sips': 'requested'})
        return (
            'OK',
            _(u'Contracte {} sol·licitat amb no cesió de SIPS').format(
                sw.cups_polissa_id.name
            )
        )

    def tall_from_b1_01(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        pol_obj = self.pool.get('giscedata.polissa')
        wiz_create_step_obj = self.pool.get("wizard.create.step")

        try:
            sw = sw_obj.browse(cursor, uid, sw_id)

            polissa = pol_obj.browse(
                cursor, uid, sw.cups_polissa_id.id, context=context
            )

            if not polissa.state == 'activa':
                info = _(u"Aquesta pólissa (%s) del cas '%s' "
                         u"no està activa") % (sw.cups_polissa_id.name, sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            sw01_obj = self.pool.get('giscedata.switching.b1.01')

            motiu = sw01_obj.get_motiu_baixa(
                cursor, uid, sw.id, context=context
            )

            if motiu == '03':
                pol_obj.send_signal(
                    cursor, uid, [polissa.id], ['impagament', 'tallar']
                )
            else:
                info = _(u"No es pot tallar el contracte (%s) ja que "
                         u"el motiu del pas 01 del cas %s no es un 03"
                         ) % (sw.cups_polissa_id.name, sw.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            pol_state = pol_obj.read(
                cursor, uid, polissa.id, ['state'], context=context
            )['state']

            if pol_state == 'tall':
                wiz_create_step_id = wiz_create_step_obj.create(
                    cursor, uid, {}, context={'active_ids': [sw_id]}
                )
                wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id],
                                          {'step': '02'})
                wiz_create_step_obj.action_create_steps(
                    cursor, uid, [wiz_create_step_id],
                    context={'active_ids': [sw_id]}
                )

                return 'OK', _(u'S\'ha generat el pas 02 i '
                               u'pasat el contracte %s a tall') % polissa.name
            else:
                return 'ERROR', _(u'El conntracte no s\'ha pogut '
                                  u'canviar d\'estat'
                                  )
        except Exception as e:
            return 'ERROR', e.message

    def baixa_b1(self, cursor, uid, sw_id, context=None):
        sw_obj = self.pool.get('giscedata.switching')
        pol_obj = self.pool.get('giscedata.polissa')
        wiz_create_step_obj = self.pool.get("wizard.create.step")

        try:
            sw = sw_obj.browse(cursor, uid, sw_id)

            if sw.rebuig:
                info = _(u'Aquest cas (%s) és un '
                         u'rebuig (%s-%s)') % (sw.name, sw.proces_id.name,
                                               sw.step_id.name)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            days_before = context.get('days_before', False)

            polissa = pol_obj.browse(
                cursor, uid, sw.cups_polissa_id.id, context=context
            )

            if polissa.state not in ('activa', 'tall'):
                # ERROR, la Pólissa ha d'estar en esborrany
                info = _(u"Aquesta pólissa (%s) del cas '%s' "
                         u"no està activa ni en tall Estat: %s"
                         ) % (sw.cups_polissa_id.name, sw.name, polissa.state)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            data_ultima_lectura = self.get_ultimes_lectura_from_days(
                cursor, uid, polissa.id, days_before, context=context
            )

            if not data_ultima_lectura:
                info = _(u"No s'han trobat lectures per el rang especificat")
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            sw01_obj = self.pool.get('giscedata.switching.b1.01')
            motiu = sw01_obj.get_motiu_baixa(
                cursor, uid, sw.id, context=context
            )

            if motiu in ('01', '02', '03', '04'):
                compt_obj = self.pool.get('giscedata.lectures.comptador')

                pol_obj.write(cursor, uid, [polissa.id], {
                    'renovacio_auto': False,
                    'data_baixa': data_ultima_lectura
                })

                compt_ids = pol_obj.read(
                    cursor, uid, polissa.id, ['comptadors']
                )['comptadors']

                compt_obj.write(cursor, uid, compt_ids, {
                    'data_baixa': data_ultima_lectura,
                    'active': False
                })
                pol_obj.send_signal(
                    cursor, uid, [polissa.id], ['baixa']
                )
            else:
                info = _(u"Motiu del pas erroni {}").format(motiu)
                raise GiscedataSwitchingException(info, _(u'ERROR'))

            pol_state = pol_obj.read(
                cursor, uid, polissa.id, ['state'], context=context
            )['state']

            if pol_state == 'baixa':
                wiz_create_step_id = wiz_create_step_obj.create(
                    cursor, uid, {}, context={'active_ids': [sw_id]}
                )
                wiz_create_step_obj.write(cursor, uid, [wiz_create_step_id],
                                          {'step': '05'})
                wiz_create_step_obj.action_create_steps(
                    cursor, uid, [wiz_create_step_id],
                    context={'active_ids': [sw_id]}
                )

                return 'OK', _(u'S\'ha generat el pas 05 i '
                               u'donat de baixa el contracte')
            else:
                return 'ERROR', _(u'El conntracte no s\'ha pogut '
                                  u'canviar d\'estat')

        except Exception as e:
            return 'ERROR', e.message

GiscedataSwitchingHelpersDistri()
