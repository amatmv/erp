# -*- coding: utf-8 -*-
{
    "name": "Facturació per Telemesura",
    "description": """Facturació (Telemesura)""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "giscedata_telemesura",
        "giscedata_facturacio_distri",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_export_ree_f1_curve_view.xml",
        "giscedata_facturacio_validation_data.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}