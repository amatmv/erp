# -*- coding: utf-8 -*-
import time
from tools.translate import _
from osv import osv, fields
from giscedata_telemesura.utils import *


class WizardExportReeF1CurveInvoice(osv.osv_memory):

    _name = 'wizard.export.ree.f1.curve.invoice'

    def export_ree_f1_curve_file(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        tm_profile = self.pool.get('telemesures.files')
        inv_obj = self.pool.get('giscedata.facturacio.factura')
        invoices_ids = context.get('active_ids', False)

        file_data = {
            'date_from': '',
            'date_to': '',
            'file_type': 'f1',
            'all_profiles': False,
            'r1_code': wizard.r1,
            'version': wizard.version,
            'origin': 'invoice',
        }

        result = tm_profile.create_file_ree_from_invoices(
            cursor, uid, invoices_ids, file_data, context=context
        )
        prefix = 'F1_'
        date_from = inv_obj.read(cursor, uid, invoices_ids[0], ['data_inici'])[
            'data_inici']

        if result.get('data', False):
            data = base64.b64encode(result['data'].getvalue())
            f_name = prefix + str(wizard.r1) + '_' + date_from.replace('-', '')\
                     + '_' + str(time.strftime("%Y%m%d")) + '.zip'
            wizard.write({'fileF1': data,
                          'filenameF1': f_name,
                          'state': 'end',
                          'info': result.get('info', 'File created correctly.')})
        else:
            wizard.write({'info': _('No file generated. Either we didn\'t find '
                                    'the profiles or the values are wrong.'),
                          'state': 'end'})

    def _default_r1(self, cursor, uid, context=None):
        ''' Gets REE code from company if defined '''
        if not context:
            context = {}

        user_obj = self.pool.get('res.users')
        user = user_obj.browse(cursor, uid, uid, context=context)

        r1 = user.company_id.partner_id.ref

        return str(r1)

    _columns = {
        'filenameF1': fields.char('Name', size=256),
        'fileF1': fields.binary(_('Result file')),
        'version': fields.integer(_('Version:')),
        'r1': fields.char(_('REE code'), size=12,
                          help=_('Automatically takes the ref2 field from the '
                                 'company as value.')),
        'state': fields.selection([('init', 'Init'), ('end', 'End'),
                                   ('on', 'On')], 'State'),
        'info': fields.text('Information', readonly=True),
    }

    _defaults = {
        'version': 1,
        'r1': _default_r1,
        'state': lambda *a: 'init',
        'info': lambda *a: _(
            'Wizard to export F1 file from the meter profiles.\n\n '
            '- F1 files contain the invoiced and validated profiles.\n'
        ),
    }


WizardExportReeF1CurveInvoice()
