# -*- coding: utf-8 -*-
from osv import osv
from tools.translate import _
from osv.expression import OOQuery
from datetime import datetime, timedelta

TECH_TYPES = ['telemeasure', 'electronic']


class GiscedataFacturacioValidationValidator(osv.osv):

    _name = 'giscedata.facturacio.validation.validator'
    _inherit = 'giscedata.facturacio.validation.validator'
    _desc = 'Validador de factures de telemesura'

    @staticmethod
    def patch_date_to_complete_period(start_date, end_date):
        """
        Get first and last tg.profile slot timestamp from a period.

        :param start_date: first date of the period LIKE 'YYYY-MM-DD'
        :param end_date: last date of the period LIKE 'YYYY-MM-DD'
        :return: two str date_periods LIKE 'YYYY-MM-DD HH:mm:ss'
        """

        di = "{0} 01:00:00".format(start_date)
        df = "{0} 00:00:00".format(
            (datetime.strptime(end_date, '%Y-%m-%d') + timedelta(days=1)
             ).strftime("%Y-%m-%d")
        )

        return di, df

    def check_curve(self, cursor, uid, clot, date_from, date_to, parameters):
        """
        Check existence of curve on tm_profiles when tech type is tm or
        electronic
        """

        tm_obj = self.pool.get('tm.profile')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        # meter info
        meter_ids = clot.polissa_id.comptadors_actius(date_from, date_to)
        for meter in meter_obj.read(
                cursor, uid, meter_ids, ['name', 'technology_type']):
            meter_name = meter['name']
            technology_type = meter['technology_type']

            if technology_type in TECH_TYPES:
                di_complete, df_complete = self.patch_date_to_complete_period(
                    date_from, date_to
                )
                tm_ids = tm_obj.search(cursor, uid, [
                    ('timestamp', '>=', di_complete),
                    ('timestamp', '<=', df_complete),
                    ('name', '=', meter_name),
                    ('type', '=', 'p')
                ])
                if not tm_ids:
                    return {
                        'message': _(u'Si es salta es salta la validació es '
                                     u'perfilará'),
                        'meter': meter_name,
                        'date_from': date_from,
                        'date_to': date_to}
        return None


GiscedataFacturacioValidationValidator()
