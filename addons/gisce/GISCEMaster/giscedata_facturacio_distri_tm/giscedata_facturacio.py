from osv import osv, fields
from oorq.decorators import job, create_jobs_group
import logging
try:
    from collections import Counter
except ImportError:
    from backport_collections import Counter
import re
from datetime import datetime, timedelta, date
from rq import Queue, get_current_job

from giscedata_telemesures_base.giscedata_facturacio import CCH_FIX_TECHNOLOGY

if 'telemeasure' not in dict(CCH_FIX_TECHNOLOGY).keys():
    CCH_FIX_TECHNOLOGY += [('telemeasure', 'Telemeasure')]

if 'electronic' not in dict(CCH_FIX_TECHNOLOGY).keys():
    CCH_FIX_TECHNOLOGY += [('electronic', 'Electronic')]

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

def fix_dates(comptador, start, end):
    """Try to fix common dates problems with measures and meters.
    """

    end = datetime.strptime(end[0:10], '%Y-%m-%d')
    start = datetime.strptime(start[0:10], '%Y-%m-%d')

    modcons = comptador.polissa.get_modcontractual_intervals(
        (start - timedelta(days=1)).strftime('%Y-%m-%d'),
        end.strftime('%Y-%m-%d'), {'ffields': ['tarifa']}
    )
    changes = modcons.get(
        start.strftime('%Y-%m-%d'), {}
    ).get('changes', [])

    data_alta = datetime.strptime(comptador.data_alta[0:10], '%Y-%m-%d')
    # End is the end date + 1 day at our 00:00
    end += timedelta(days=1)
    end = end.strftime('%Y-%m-%d 00:00:00')

    if start != data_alta and 'tarifa' not in changes:
        # Start is and old measure date, we have to sum one day
        # and start at hour 01:00
        start += timedelta(days=1)

    start = start.strftime('%Y-%m-%d 01:00:00')

    return start, end


class GiscedataFacturacioFactura(osv.osv):
    """Classe base de les factures
    """

    _name = 'giscedata.facturacio.factura'
    _inherit = 'giscedata.facturacio.factura'

    def check_invoice_tm_details(self, cursor, uid, ids, factura, context=None):
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        to_fix = False
        if factura.tipo_factura == '01' and factura.journal_id.code.startswith('ENERGIA'):
            if factura.tipo_rectificadora not in ['BRA', 'B', 'A']:
                for meter in factura.comptadors:
                    tech = meter_obj.read(cursor, uid, meter.id,
                                          ['technology_type'],
                                          context=context)['technology_type']
                    tariff_name = factura.tarifa_acces_id.name
                    if tech in ['telemeasure', 'electronic'] and not \
                            tariff_name.startswith('6.'):
                        to_fix = True
            if factura.tipo_rectificadora == 'A':
                self.delete_nullified_profiles(cursor, uid, factura,
                                               context=context)
        return to_fix

    def invoice_open(self, cursor, uid, ids, context=None):
        """Open the invoice. Check if the meters of the invoice are TM type
        """

        res = super(GiscedataFacturacioFactura, self).invoice_open(
            cursor, uid, ids, context=context
        )
        invoices_to_fix = []
        for factura in self.browse(cursor, uid, ids, context):
            to_fix = self.check_invoice_tm_details(cursor, uid, ids, factura)
            if to_fix:
                invoices_to_fix.append(factura.id)
        if invoices_to_fix:
            for invoice in invoices_to_fix:
                self.enqueue_fix_cch_tm(
                    cursor, uid, [invoice], context=context
                )
        return res

    def delete_nullified_profiles(self, cursor, uid, factura, context=None):
        tm_profile_obj = self.pool.get('tm.profile')
        date_start, date_end = self.get_readings_dates_range(cursor, uid, factura)
        if not date_start and not date_end:
            return False
        date_end = (
                datetime.strptime(date_end, DATETIME_FORMAT) + timedelta(days=1)
        ).strftime(DATETIME_FORMAT)

        p_to_delete = tm_profile_obj.search(cursor, uid, [('invoice_num', '=',
                                            factura.rectifying_id.number),
                                            ('timestamp', '>=', date_start),
                                            ('timestamp', '<=', date_end),
                                            ('cch_bruta', '=', False),
                                            ('type', '=', 'p'),
                                            ], context=context)
        tm_profile_obj.unlink(cursor, uid, p_to_delete)
        p_to_modify = tm_profile_obj.search(cursor, uid, [('invoice_num', '=',
                                            factura.rectifying_id.number),
                                            ('timestamp', '>=', date_start),
                                            ('timestamp', '<=', date_end),
                                            ('cch_bruta', '=', True),
                                            ('cch_fact', '=', True),
                                            ('type', '=', 'p'),
                                            ], context=context)
        tm_profile_obj.write(cursor, uid, p_to_modify, {'cch_fact': False,
                                                        'ai_fact': 0,
                                                        'ae_fact': 0,
                                                        'r1_fact': 0,
                                                        'r2_fact': 0,
                                                        'r3_fact': 0,
                                                        'r4_fact': 0,
                                                        })
        return True

    def get_readings_dates_range(self, cursor, uid, factura):
        date_start = ''
        date_end = ''
        first = True
        for lectura in factura.lectures_energia_ids:
            if lectura.tipus == 'reactiva':
                continue
            lect_dates = fix_dates(lectura.comptador_id,
                                   lectura.data_anterior,
                                   lectura.data_actual)
            if first:
                first = False
                date_start = lect_dates[0]
                date_end = lect_dates[1]
            else:
                if lect_dates[0] < date_start:
                    date_start = lect_dates[0]
                if lect_dates[1] > date_end:
                    date_end = lect_dates[1]
        return date_start, date_end

    def get_tm_balance_per_meter(self, cursor, uid, ids, context=None):
        """Obtain the balance of an invoice.

        The goal is obtain a dict with the following structure:
        {invoice_id: {
            'dates': {
                'meter_serial_number': (min_date, max_date)
            },
            'balance': {
                'meter_serial_number': {
                    'Period_Number': Consum
                }
            },
            'origin': {
                'meter_serial_number': 'codi-subcodi'
            },
            'tariff': 'Tariff name'
        }
        """
        res = {}
        for invoice in self.browse(cursor, uid, ids, context=context):
            meters = {
                'balance': {},
                'dates': {},
                'tariff': None,
                'origin': {}
            }
            balances = meters['balance']
            dates = meters['dates']
            meters['tariff'] = invoice.tarifa_acces_id.name
            for lectura in invoice.lectures_energia_ids:
                if lectura.tipus == 'reactiva':
                    continue
                comptador = lectura.comptador_id.name
                if comptador not in balances:
                    balances[comptador] = Counter()
                period = re.findall('.*(P[1-6]).*', lectura.name)[0]
                balances[comptador][period] += lectura.consum
                meters['origin'][comptador] = '{0}-{1}'.format(
                    lectura.origen_id.codi, lectura.origen_id.subcodi
                )
                lect_dates = fix_dates(lectura.comptador_id,
                                       lectura.data_anterior,
                                       lectura.data_actual)
                if comptador not in dates:
                    dates[comptador] = lect_dates
                else:
                    start_meter, end_meter = dates[comptador]
                    start_lect, end_lect = lect_dates
                    start = min(start_meter, start_lect)
                    end = max(end_meter, end_lect)
                    dates[comptador] = start, end
            res[invoice.id] = meters.copy()
        return res

    @job(queue='fix_cch_tm')
    def enqueue_fix_cch_tm(self, cursor, uid, ids, context=None):
        """
            Auxiliar fix_cch_tm method that executes the fix of an invoice. It
            also detects errors on the process and can move the failed jobs to
            an special queue if the error is controlled.
        """
        logger = logging.getLogger('openerp.{}'.format(__name__))
        logger.info("Processing invoice. ID: {}".format(ids[0]))
        result = False
        try:
            result = self.fix_cch_tm(cursor, uid, ids, context=context)
        except Exception as e:
            logger.error("Exception raised: {}".format(e))

            if e.message == 'Profiles from REE not found':
                self.enqueue_fix_cch_tm_to_pending_queue(cursor, uid, ids,
                                                         context=context)
            else:
                raise
        return result

    def enqueue_fix_cch_tm_to_pending_queue(self, cursor, uid, ids, context=None):
        """
            It enqueues the current job to the pending profiles for
            fix_cch_tm queue.
        """
        logger = logging.getLogger('openerp.{}'.format(__name__))
        logger.error("Task moved to the pending queue for future processing. "
                     "Invoice ID: {}".format(ids))
        pending_cofs_queue = 'pending_cofs_fix_cch_tm'
        current_job = get_current_job()
        pending_queue = Queue(pending_cofs_queue)

        pending_queue.enqueue_job(current_job)
        return True

    def fix_cch_tm(self, cursor, uid, ids, context=None):
        tm_profile_obj = self.pool.get('tm.profile')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        logger = logging.getLogger('openerp.{}'.format(__name__))

        # Filter... maybe some ids are removed when job is executed
        invoices_to_fix = []
        for factura in self.browse(cursor, uid, ids, context):
            to_fix = self.check_invoice_tm_details(cursor, uid, ids, factura)
            if to_fix:
                invoices_to_fix.append(factura.id)
        ids = invoices_to_fix
        for invoice in self.browse(cursor, uid, ids, context=context):
            meters = invoice.get_tm_balance_per_meter()[invoice.id]
            logger.info("Fixing cch for {} invoice".format(invoice.number))
            if meters['balance']:
                for meter, balance in meters['balance'].items():
                    start, end = meters['dates'][meter]
                    tariff = meters['tariff']
                    origin = meters['origin'][meter]

                    meter_id = meter_obj.search(cursor, uid, [
                        ('name', '=', meter),
                        ('tg', '=', False),
                        ('technology_type', 'in', ['telemeasure',
                                                   'electronic']),
                        ('polissa', '=', invoice.polissa_id.id)],
                                                context={'active_test': False})
                    if meter_id:
                        tm_profile_obj.fix(
                            cursor,
                            uid,
                            meter,
                            start,
                            end,
                            tariff,
                            balance,
                            origin,
                            invoice.number
                        )
                        invoice.write({'cch_fact_available': 1})
            else:
                logger.warning(
                    'No balance for invoice {}, not fixing CCH_FACT'.format(
                        invoice.number
                    )
                )
                invoice.write({'cch_fact_available': 0})

        return True

GiscedataFacturacioFactura()
