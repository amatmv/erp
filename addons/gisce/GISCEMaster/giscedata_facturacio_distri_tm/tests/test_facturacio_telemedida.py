# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction


class TestFacturacioTelemedida(testing.OOTestCase):

    def setUp(self):
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.lot_obj = self.pool.get('giscedata.facturacio.contracte_lot')
        self.pol_obj = self.pool.get('giscedata.polissa')
        self.meter_obj = self.pool.get('giscedata.lectures.comptador')
        self.tarifa_obj = self.pool.get('giscedata.polissa.tarifa')
        self.validator_obj = self.pool.get(
            'giscedata.facturacio.validation.validator')
        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def get_contracte_lot_id(self, cursor, uid):
        lot_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'cont_lot_0001'
        )[1]

        return lot_id

    def get_tariff_id_by_name(self, cursor, uid, tariff_name):
        tariff_id = self.tarifa_obj.search(
            cursor, uid, [('name', '=', tariff_name)]
        )

        return int(tariff_id[0])

    def test_get_checks_returns_validations(self):
        """
        Test TM validations are correctly set
        """
        cursor = self.txn.cursor
        uid = self.txn.user

        checks = [
            check['code']
            for check in self.validator_obj.get_checks(
                cursor, uid, validation_type='lot')
        ]
        self.assertIn('TM01', checks)

    def test_curve_validation(self):
        """
        Test que comprova que existeixi corba
        """
        cursor = self.txn.cursor
        uid = self.txn.user

        lot_id = self.get_contracte_lot_id(cursor, uid)
        clot = self.lot_obj.browse(cursor, uid, lot_id)

        check_result = self.validator_obj.check_curve(
            cursor, uid, clot, '2016-10-01', '2016-11-02', {})

        self.assertIsNone(check_result)

        tariff_id = self.get_tariff_id_by_name(cursor, uid, '3.0A')
        meter_id = self.meter_obj.search(
            cursor, uid, [('name', '=', clot.polissa_id.comptador)]
        )[0]
        self.meter_obj.write(
            cursor, uid, meter_id, {'technology_type': 'telemeasure'}
        )
        self.pol_obj.write(
            cursor, uid, clot.polissa_id.id, {'tarifa': tariff_id}
        )
        # Refresh clot
        lot_id = self.get_contracte_lot_id(cursor, uid)
        clot = self.lot_obj.browse(cursor, uid, lot_id)
        check_result = self.validator_obj.check_curve(
            cursor, uid, clot, '2016-10-01', '2016-11-02', {})

        self.assertIn('message', check_result)
        self.assertIn('date_from', check_result)
        self.assertIn('date_to', check_result)
        self.assertIn('meter', check_result)
