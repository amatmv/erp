# -*- coding: utf-8 -*-
{
    "name": "Suministros no cortables",
    "description": """Módulo para independizar nocutoff de bono social
    si hace falta
    Art.52 P.4: https://www.boe.es/boe/dias/2013/12/27/pdfs/BOE-A-2013-13645.pdf
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends": [
        "giscedata_facturacio_impagat_comer",
        "giscedata_suministros_nocutoff"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_facturacio_impagat_comer_nocutoff_data.xml"
    ],
    "active": False,
    "installable": True
}
