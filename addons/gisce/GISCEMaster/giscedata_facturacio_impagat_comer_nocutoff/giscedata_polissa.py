# -*- coding: utf-8 -*-
import logging

from osv import fields, osv
from tools.translate import _
from osv.orm import OnlyFieldsConstraint
from giscedata_polissa_comer.giscedata_polissa import (
    get_polissa_ids
)


STORE_UNPAYD_PROCESS = {
        'giscedata.polissa': (
            lambda self, cursor, uid, ids, context=None: ids,
            ['potencia', 'tipus_vivenda', 'cnae', 'titular', 'nocutoff'], 20),
        'res.partner': (get_polissa_ids, ['vat'], 20),
    }


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def get_nocutoff_process(self, cursor, uid, context=None):
        return self.pool.get('ir.model.data').get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'nocutoff_pending_state_process'
        )[1]

    def get_polisses_force_process_id(self, cursor, uid, ids, field_name, args,
                                      context=None):
        """
        Funció que retorna el procés de tall a les pòlisses que se'ls ha de
        forçar el process_id.
        Retorna un diccionari amb les ids on el valor és el procés de tall
        """

        res = super(GiscedataPolissa, self).get_polisses_force_process_id(
            cursor, uid, ids, field_name, args, context=context
        )

        logger = logging.getLogger(
            'openerp.{}.get_polisses_force_process_id'.format(
                __name__
            ))
        nocutoff_process_id = self.get_nocutoff_process(
            cursor, uid, context=context
        )
        if res:
            for pol in self.read(cursor, uid, res.keys(), ['nocutoff', 'name']):
                if pol['nocutoff']:
                    res[pol['id']] = nocutoff_process_id
                    logger.info((
                        u'Se a pasado el contrato {}, al flujo de No Cortables'
                    ).format(
                        pol['name']
                    ))
        else:
            for pol in self.read(cursor, uid, ids, ['nocutoff', 'name']):
                if pol['nocutoff']:
                    res[pol['id']] = nocutoff_process_id
                    logger.info((
                        u'Se a pasado el contrato {}, al flujo de No Cortables'
                    ).format(
                        pol['name']
                    ))

        return res

    def wrapper_constraint_process_id_nocutoff(self, cursor, uid, ids):
        nocutoff_process_id = self.get_nocutoff_process(cursor, uid)
        res = True

        for pol in self.read(cursor, uid, ids, ['process_id', 'nocutoff']):
            if pol['process_id'] and pol['process_id'][0] == nocutoff_process_id:
                res = bool(pol['nocutoff'])
                if not res:
                    break
        return res

    def _check_process_id(self, cursor, uid, ids):
        """
        Check if some contract is in nocutoff process and doesn't has
        nocutoff motive.

        :return: Returns False if som contract has nocutoff as process_id
        and doesn't have nocutoff motive.
        """
        res = super(GiscedataPolissa, self)._check_process_id(cursor, uid, ids)

        if not res:
            return res
        else:
            return self.wrapper_constraint_process_id_nocutoff(cursor, uid, ids)

    def _ff_process_id(self, cursor, uid, ids, field_name, args, context=None):
        res = super(GiscedataPolissa, self)._ff_process_id(
            cursor, uid, ids, field_name, args, context=context)

        return res

    def _fnct_inv_set_process(self, cursor, uid, ids, name, value, fnct_inv_arg,
                              context=None):
        return super(GiscedataPolissa, self)._fnct_inv_set_process(
            cursor, uid, ids, name, value, fnct_inv_arg, context=context
        )

    def _get_default_process(self, cursor, uid, ids, context=None):
        return super(GiscedataPolissa, self)._get_default_process(
            cursor, uid, ids, context=context
        )

    _columns = {
        'process_id': fields.function(
            fnct=_ff_process_id, fnct_inv=_fnct_inv_set_process,
            relation='account.invoice.pending.state.process',
            method=True, type='many2one', string=u'Procés de tall',
            required=True, store=STORE_UNPAYD_PROCESS, readonly=True,
            states={
                'esborrany': [('readonly', False)],
                'validar': [('readonly', False)],
                'modcontractual': [('readonly', False)]
            },
        )
    }

    _defaults = {
        'process_id': _get_default_process
    }

    _constraints = [
        OnlyFieldsConstraint(
            _check_process_id,
            _(
                "La pólissa actual no pot accedir a aquest procés de "
                "tall ja que no compleix les condicions"
            ), ['process_id']
        ),
    ]


GiscedataPolissa()
