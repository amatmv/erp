# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from json import loads


class TestPendingstatesNocutoff(testing.OOTestCase):
    def setUp(self):
        # En el año 2100 dejara de funcionar este test
        current_year = datetime.today().year % 1000 + 1
        for year in range(16, current_year):
            self.openerp.install_module(
                'giscedata_tarifas_peajes_20{}0101'.format(str(year).zfill(2))
            )

        self.txn = Transaction().start(self.database)

    def tearDown(self):
        self.txn.stop()

    def create_fiscal_year(self, cursor, uid, fiscal_year, context=None):
        fyear_obj = self.openerp.pool.get('account.fiscalyear')
        fy_id = fyear_obj.create(cursor, uid, {
            'name': fiscal_year,
            'code': fiscal_year,
            'date_start': '%s-01-01' % fiscal_year,
            'date_stop': '%s-12-31' % fiscal_year
        })
        fyear_obj.create_period(cursor, uid, [fy_id])

    def activar_polissa(self, cursor, uid, polissa_id, context=None):
        pol_obj = self.openerp.pool.get('giscedata.polissa')
        pol = pol_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['validar', 'contracte'])

    def create_modcon_with_nocutoff_pending_process(
            self, cursor, uid, polissa_id=None, date_from=None, date_to=None, context=None):

        imd_obj = self.openerp.pool.get('ir.model.data')
        pol_obj = self.openerp.pool.get('giscedata.polissa')

        if not date_from:
            date_from = datetime(year=2017, month=2, day=1).strftime('%Y-%m-%d')

        if not date_to:
            date_to = datetime.today().strftime('%Y-%m-%d')

        if not polissa_id:
            polissa_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'polissa_0001'
            )[1]

        nocutoff_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'd8_nocutoff'
        )[1]

        nocutoff_process_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'nocutoff_pending_state_process'
        )[1]

        pol = pol_obj.browse(cursor, uid, polissa_id)
        pol.send_signal(['modcontractual'])

        pol.write(
            {'nocutoff': nocutoff_id}
        )

        wz_crear_mc_obj = self.openerp.pool.get(
            'giscedata.polissa.crear.contracte'
        )

        ctx = {'active_id': polissa_id}
        params = {'duracio': 'nou'}

        wz_id_mod = wz_crear_mc_obj.create(cursor, uid, params, ctx)
        wiz_mod = wz_crear_mc_obj.browse(cursor, uid, wz_id_mod, ctx)
        res = wz_crear_mc_obj.onchange_duracio(
            cursor, uid, [wz_id_mod], wiz_mod.data_inici, wiz_mod.duracio,
            ctx
        )

        mod_create = {
            'data_inici': date_from,
            'data_final': date_to
        }

        mod_create.update(res.get('value', {}))

        wiz_mod.write(mod_create)

        wiz_mod.action_crear_contracte(ctx)

        res = pol_obj.read(
            cursor, uid, polissa_id, ['process_id', 'nocutoff']
        )
        self.assertEqual(res['nocutoff'][0], nocutoff_id)
        self.assertEqual(res['process_id'][0], nocutoff_process_id)

    def generate_manual_invoice(self, cursor, uid, polissa_id, date_start, date_end, journal_id):
        imd_obj = self.openerp.pool.get('ir.model.data')
        wiz_fact_manual_obj = self.openerp.pool.get('wizard.manual.invoice')
        wiz_fact_manual_id = wiz_fact_manual_obj.create(cursor, uid, vals={
            'polissa_id': polissa_id,
            'date_start': date_start,
            'date_end': date_end,
            'date_invoice': datetime.today().strftime('%Y-%m-%d'),
            'journal_id': journal_id
        })

        pricelist_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio',
            'pricelist_tarifas_electricidad'
        )[1]

        context = {'llista_preu': pricelist_id}

        #   -   Create regular invoice within ExtraLine's dates

        wiz_fact_manual_obj.action_manual_invoice(
            cursor, uid, [wiz_fact_manual_id], context)

        fact_id = wiz_fact_manual_obj.read(
            cursor, uid, wiz_fact_manual_id, ['invoice_ids']
        )[0]['invoice_ids']
        fact_id = loads(fact_id)[0]

        return fact_id

    def get_poliza_ready_fror_bono_social_disponible(self, cursor, uid, polissa_id):
        polissa_obj = self.openerp.pool.get('giscedata.polissa')
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner = polissa_obj.read(
            cursor, uid, polissa_id, ['titular']
        )['titular'][0]

        # Set vat as no enterprise
        res_obj = self.openerp.pool.get('res.partner')
        res_obj.write(cursor, uid, partner, {'vat': 'ES71895522Y'})

        cnae_9820 = imd_obj.get_object_reference(
            cursor, uid, 'giscemisc_cnae', 'cnae_9820'
        )[1]

        # Here current owner vat is enterprise and power is under 10 kW
        polissa_obj.write(
            cursor, uid, polissa_id,
            {'cnae': cnae_9820, 'tipus_vivenda': 'habitual'}
        )

    def test_pending_change_of_nocutoff_contract(self):
        """
        - Test auto change process to nocutoff when motive nocutoff is set on
        contract
        - Test auto change state to nocutoff correct when motive nocutoff is
        set on contract
        - Test generate invoice from nocutoff contract sets as nocutoff
        """
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn

        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        fact_obj = pool.get('giscedata.facturacio.factura')
        pol_obj = pool.get('giscedata.polissa')
        pend_state_obj = pool.get('account.invoice.pending.state')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        nocutoff_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'd8_nocutoff'
        )[1]

        nocutoff_process_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'nocutoff_pending_state_process'
        )[1]

        nocutoff_pending_correct_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'correcto_nocutoff_pending_state'
        )[1]

        self.activar_polissa(cursor, uid, polissa_id)

        pol_reads = pol_obj.read(
            cursor, uid, polissa_id, ['nocutoff', 'process_id', 'state']
        )

        # Check process distinct before test change

        self.assertNotEqual(pol_reads['process_id'][0], nocutoff_process_id)
        self.assertFalse(pol_reads['nocutoff'])

        self.create_modcon_with_nocutoff_pending_process(
            cursor, uid, polissa_id
        )

        pol_reads = pol_obj.read(
            cursor, uid, polissa_id, ['nocutoff', 'process_id', 'pending_state']
        )

        # Check process change to nocutoff when nocutoff sets on contract

        self.assertEqual(pol_reads['process_id'][0], nocutoff_process_id)
        self.assertEqual(pol_reads['nocutoff'][0], nocutoff_id)

        nocutoff_pending_correct_name = pend_state_obj.read(
            cursor, uid, nocutoff_pending_correct_id, ['name']
        )['name']

        self.assertEqual(
            pol_reads['pending_state'],
            nocutoff_pending_correct_name
        )

        self.create_fiscal_year(cursor, uid, 2017)

        journal_obj = self.openerp.pool.get('account.journal')
        journal_id = journal_obj.search(
            cursor, uid, [('code', '=', 'ENERGIA')]
        )[0]
        fact_id = self. generate_manual_invoice(
            cursor, uid, polissa_id,
            '2017-06-01',
            '2017-06-30',
            journal_id
        )

        fact_obj.invoice_open(cursor, uid, [fact_id])

        fact_reads = fact_obj.read(
            cursor, uid, fact_id, ['pending_state', 'state']
        )

        self.assertEqual(
            fact_reads['pending_state'][0],
            nocutoff_pending_correct_id
        )

    def test_pending_change_of_nocutoff_more_priority_than_bono_social(self):
        """
        - Test higher priority of nocutoff than bono social
        """
        cursor = self.txn.cursor
        uid = self.txn.user
        txn = self.txn

        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        fact_obj = pool.get('giscedata.facturacio.factura')
        pol_obj = pool.get('giscedata.polissa')
        pend_state_obj = pool.get('account.invoice.pending.state')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        nocutoff_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'd8_nocutoff'
        )[1]

        nocutoff_process_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'nocutoff_pending_state_process'
        )[1]

        nocutoff_pending_correct_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_suministros_nocutoff',
            'correcto_nocutoff_pending_state'
        )[1]

        bono_process_id = imd_obj.get_object_reference(
            cursor, uid,
            'giscedata_facturacio_comer_bono_social',
            'bono_social_pending_state_process'
        )[1]

        self.get_poliza_ready_fror_bono_social_disponible(
            cursor, uid, polissa_id
        )

        self.activar_polissa(cursor, uid, polissa_id)

        pol_reads = pol_obj.read(
            cursor, uid, polissa_id,
            [
                'nocutoff', 'process_id',
                'state', 'bono_social_disponible'
            ]
        )

        self.assertNotEqual(pol_reads['process_id'][0], nocutoff_process_id)
        self.assertEqual(pol_reads['process_id'][0], bono_process_id)
        self.assertTrue(pol_reads['bono_social_disponible'])
        self.assertFalse(pol_reads['nocutoff'])
        self.assertEqual(pol_reads['state'], 'activa')

        # Check process distinct before test change
        self.assertNotEqual(pol_reads['process_id'][0], nocutoff_process_id)
        self.assertFalse(pol_reads['nocutoff'])

        self.create_modcon_with_nocutoff_pending_process(
            cursor, uid, polissa_id
        )

        pol_reads = pol_obj.read(
            cursor, uid, polissa_id,
            [
                'nocutoff', 'process_id',
                'pending_state', 'bono_social_disponible'
            ]
        )

        # Check process change to nocutoff when nocutoff sets on contract
        self.assertTrue(pol_reads['bono_social_disponible'])
        self.assertEqual(pol_reads['process_id'][0], nocutoff_process_id)
        self.assertEqual(pol_reads['nocutoff'][0], nocutoff_id)

        nocutoff_pending_correct_name = pend_state_obj.read(
            cursor, uid, nocutoff_pending_correct_id, ['name']
        )['name']

        self.assertEqual(
            pol_reads['pending_state'],
            nocutoff_pending_correct_name
        )
