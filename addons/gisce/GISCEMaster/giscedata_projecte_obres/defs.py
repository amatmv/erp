# -*- coding: utf-8 -*-
from __future__ import unicode_literals


TABLA_CECO = [
    ('c101', 'Planificación de activos e instalaciones'),
    ('c102', 'Planificación de activos e instalaciones'),
    ('c103', 'Inspección y control de nuevas instalaciones propias'),
    ('c104', 'Inspección y control de nuevas instalaciones'
             ' cedidas por tercero'),
    ('c106', 'Gastos financieros de planificación y desarrollo de red'),
    ('c107', 'Gastos por desmantelamiento de instalaciones'),
    ('c201', 'Gestión de las solicitudes de nuevos suministros'),
    ('c202', 'Inspección y control de operación'),
    ('c203', 'Operación de centros de control y operación local'),
    ('c311', 'Mantenimiento preventivo de instalaciones'),
    ('c321', 'Mantenimiento correctivo de instalaciones'),
    ('c701', 'Enganche de instalaciones'),
    ('c704', 'Costes de realización de acometidas'),
    ('c705', 'Costes de contratación de nuevos suministros'),
    ('c706', 'Retranqueos y trabajos por cuenta de terceros'),
    ('c711', 'Costes por gestión de compra de equipos de medida'
             ' (ofertas, evaluación, adjudicación y recepción)'),
    ('c712', 'Costes por verificación inicial de equipos de medida'),
    ('c713', 'Costes por conexión, precintado y pruebas de'
             ' puesta en servicio de equipos de medida'),
    ('c714', 'Otros costes de adquisición de equipos de medida'
             ' (gestión de stocks, almacenamiento y transporte)'),
    ('c715', 'Costes de actualización anual de calendario de'
             ' festivos y cambio de horario de verano a invierno en'
             ' equipos de medida'),
    ('c716', 'Costes de reparametrización de equipos de medida'
             ' ante cambios en las condiciones del contrato'),
]


def merge_code_in_name(values):
    return [
        (code, '{} - {}'.format(code, value))
        for code, value in values
    ]


# Tablas del BOE: nº 99 25 abril de 2019. Sec. III.

TABLA_TIPO_SUELO = [
    ('0', u'0: URBANO'),
    ('1', u'1: RURAL')
]

TABLA_PLANIFICACION = [
    ('1', u'1: Sí'),
    ('2', u'2: No')
]

TABLA_CAUSA_BAJA = [
    ('0', u'0: No causa baja.'),
    ('1', u'1: Finalización vida útil con reemplazo.'),
    ('2', u'2: Finalización vida útil sin reemplazo.'),
    ('3', u'3: Antes de finalización de vida útil.'),
]

TABLA_TIPO_INVERSION = [
    ('0', u'0: Infraestructuras eléctricas realizadas a coste completo.'),
    ('1', u'1: Infraestructuras eléctricas no realizadas a coste completo'),
    ('2', u'2: Inversiones sin nuevas unidades físicas.'),
]
