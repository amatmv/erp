# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields
from tools.translate import _

from .defs import *


def filter_none_values(d):
    """
    Filter the None-value registers in the dict given.
    :return: dict filtered
    """
    return {
        field_name: field_value for field_name, field_value in d.items()
        if field_value is not None
    }


class GiscedataProjecteObraTi(osv.osv):

    _name = 'giscedata.projecte.obra.ti'
    _description = 'TI Obra'
    _prefetch = False

    # Fields Function

    def _ff_calc_auditat(
            self, cursor, uid, ids, field_name, arg, context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        for ti in self.read(
                cursor, uid, ids, [
                    'im_ingenieria', 'im_trabajos', 'im_obracivil',
                    'im_materiales',
                    'subvenciones_europeas', 'subvenciones_nacionales'
                ]
        ):
            inc = ti['subvenciones_europeas'] + ti['subvenciones_nacionales']
            out = (
                    ti['im_ingenieria'] +
                    ti['im_trabajos'] +
                    ti['im_obracivil'] +
                    ti['im_materiales']
            )
            res[ti['id']] = out - inc
        return res

    def _ff_calc_auditat_search(
            self, cursor, uid, obj, name, args, context=None):
        if context is None:
            context = {}
        op = args[0][1]
        val = args[0][2]
        res = []
        elem_ids = self.search(cursor, uid, [], context=context)
        for ti in self.read(
                cursor, uid, elem_ids, [
                    'im_ingenieria', 'im_trabajos', 'im_obracivil',
                    'im_materiales',
                    'subvenciones_europeas', 'subvenciones_nacionales'
                ]
        ):
            inc = ti['subvenciones_europeas'] + ti['subvenciones_nacionales']
            out = (
                    ti['im_ingenieria'] +
                    ti['im_trabajos'] +
                    ti['im_obracivil'] +
                    ti['im_materiales']
            )
            diff = out - inc
            if (
                    ('>' in op and diff > val) or
                    ('=' in op and diff == val) or
                    ('<' in op and diff < val)
            ):
                res.append(ti['id'])
        return [('id', 'in', res)]

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Returns a dict indexed by TI id and a value of True if exists a
        detall.economic with the same CCUU code.
        """
        res = {}
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        for ti in self.read(cursor, uid, ids, ['ccuu', 'obra_id']):
            if ti['ccuu']:
                equivalent = detall_obj.search(cursor, uid, [
                    ('obra_id', '=', ti['obra_id'][0]),
                    ('ti_ccuu', '=', ti['ccuu'][0])
                ])
                res[ti['id']] = bool(equivalent)
        return res

    def _get_descripcio_obra(self, cursor, uid, ids, field_name, arg,
                             context=None):
        """
        Returns the description of the obra associated with the TI.
        """
        res = dict.fromkeys(ids, False)
        q = self.q(cursor, uid)
        for ti in q.read(['obra_id.descripcio']).where([('id', 'in', ids)]):
            name = ti['obra_id.descripcio']
            if name:
                res[ti['id']] = name
        return res

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg,
                               context=None):
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        ti_fields = ['bloquear_contabilidad', 'valor_auditado',
                     'valor_contabilidad']
        for ti in self.read(cursor, uid, ids, ti_fields):
            if ti['bloquear_contabilidad']:
                res[ti['id']] = ti['valor_auditado']
            else:
                res[ti['id']] = ti['valor_contabilidad']
        return res

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                                     context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute("""
            UPDATE {} SET valor_contabilidad=%s
            WHERE id IN %s AND NOT bloquear_contabilidad
        """.format(self._table), (value or 0.0, tuple(ids)))

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name,
                                          arg, context=None):
        """
        Set the porcentaje modificación of the installation line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids:
        :param field_name:
        :param arg:
        :param context:
        :return:
        """
        if context is None:
            context = {}
        res = dict.fromkeys(ids, False)
        ti_fields = ['valor_auditado', 'porcentaje_modificacion',
                     'precio_inversion_unitario']
        for ti in self.read(cursor, uid, ids, ti_fields):
            if all((
                ti['precio_inversion_unitario'],
                ti['valor_auditado']
            )):
                additional_factor = self._get_investing_additional_factor(
                    cursor, uid, ti['id'])
                division = (
                        ti['valor_auditado']
                        / (ti['precio_inversion_unitario'] * additional_factor)
                )
                res[ti['id']] = round(division * 100, 2)
            else:
                res[ti['id']] = ti['porcentaje_modificacion']
        return res

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value,
                                          args, context=None):
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        cursor.execute("""
            UPDATE {} SET porcentaje_modificacion=%s
            WHERE id IN %s AND NOT bloquear_porcentaje_modificacion
        """.format(self._table), (value or 0.0, tuple(ids)))

    @staticmethod
    def _fnct_set_price_unit_investment(obj, cursor, uid, ids, field_name, args,
                                   context=None):
        """
        Sets the price of investment that corresponds to the installation of
        the element TI and is established by the CNMC in the model
        giscedata.tipus.installacio.
        :param obj: instance of any of the childs of the
        <giscedata.projecte.obra.ti> ORM object.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: ids of the param obj
        :param field_name: name of the field that triggers the function.
        :type field_name: str
        :param args: additional args
        :param context: OpenERP context
        :type context: dict
        :return: dict where the key is a obj ID and the value is the
        investment unit price
        """
        if context is None:
            context = {}

        res = dict.fromkeys(ids, 0.0)
        q = obj.q(cursor, uid)
        field_investment = 'element_ti_id.tipus_instalacio_cnmc_id.investment'
        if obj._table == 'giscedata_projecte_obra_ti_subestacions':
            field_investment = 'element_ti_id.ct_id.tipus_instalacio_cnmc_id.' \
                               'investment'
        elif obj._table == 'giscedata_projecte_obra_ti_despatx':
            return res
        element_fields = [field_investment, 'id']
        element_investment = q.read(element_fields).where([('id', 'in', ids)])

        for element_line in element_investment:
            if element_line[field_investment]:
                res[element_line['id']] = element_line[field_investment]
        return res

    @staticmethod
    def _fnct_set_price_investment(obj, cursor, uid, ids, field_name, args,
                                   context=None):
        """
        Sets the price of investment that corresponds to the installation of
        the element TI, multiplied by the factor of the element; whether it's
        km, or MVAs.
        :param obj: instance of any of the childs of the
        <giscedata.projecte.obra.ti> ORM object.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: ids of the param obj
        :param field_name: name of the field that triggers the function.
        :type field_name: str
        :param args: additional args
        :param context: OpenERP context
        :type context: dict
        :return: dict where the key is a obj ID and the value is the
        investment price
        """
        if context is None:
            context = {}

        res = dict.fromkeys(ids, 0.0)
        q = obj.q(cursor, uid)

        element_fields = ['precio_inversion_unitario', 'id']
        element_investment = q.read(element_fields).where([('id', 'in', ids)])

        for element_line in element_investment:
            if element_line['precio_inversion_unitario']:
                additional_factor = obj._get_investing_additional_factor(
                    cursor, uid, element_line['id'])
                res[element_line['id']] = (
                    element_line['precio_inversion_unitario']
                    * additional_factor
                )
        return res

    # Class functions

    def create(self, cursor, uid, vals, context=None):
        """
        Override the create method to write the values that come from the
        element_ti_id.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will have the TI
        :type vals: dict
        :param context: OpenERP context
        :type context: dict
        :return: the id of the created TI
        :return long:
        """
        if vals.get('element_ti_id', False):
            ti_vals = self.get_ti_vals(cursor, uid, {}, vals['element_ti_id'],
                                       context=context)
            vals.update(ti_vals)
        return super(GiscedataProjecteObraTi, self).create(
            cursor, uid, vals, context=context)

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        return vals

    _get_ti_vals = get_ti_vals

    def cast_value_if_necessary(self, cursor, uid, field_value,
                                field_name, model):
        """
        Cast value according to the field type definition of the model.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param field_value: value of the field to write
        :type: --
        :param field_name: name of the field to writem, that belongs to the
                              model definition.
        :type: str
        :param model: model that has the field definition.
        :type: str
        :return: value of the *field_value* casted according to the model
                 definition.
        """
        if model and field_value:
            model_obj = self.pool.get(model)
            fields_definition = model_obj.fields_get(cursor, uid)
            model_field_definition = fields_definition.get(field_name, False)

            if model_field_definition:
                model_field_type = model_field_definition['type']
                type_mapping = {
                    'char': 'str',
                    'selection': 'str',
                    'date': 'str',
                    'integer': 'int',
                    'many2one': 'int',
                    'float': 'float',
                    'boolean': 'bool'
                }
                type_to_cast = type_mapping.get(model_field_type, False)
                if type_to_cast:
                    return eval('{}'.format(type_to_cast))(field_value)
        return field_value

    def refresh_ti_vals(self, cursor, uid, ids, context=None):
        """
        Writes to the element the values that has the element_ti_id to which is
        associated with.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti> ids
        :param context:
        :return: None
        """
        if context is None:
            context = {}
        q = self.q(cursor, uid)
        ti_ids = q.read(['element_ti_id', 'id']).where([('id', '=', ids)])
        for ti_id_values in ti_ids:
            new_ti_values = self.get_ti_vals(cursor, uid, ti_id_values,
                                             ti_id_values['element_ti_id'])
            new_ti_values = filter_none_values(new_ti_values)
            new_ti_values.pop('id')
            self.write(cursor, uid, ti_id_values['id'],
                       new_ti_values, context=context)

    def onchange_element_ti(self, cursor, uid, ids, element_ti_id, context=None):
        """
        We obtain the values that are to be changed in the TI view.
        We are only interested in changing them when it's the first time to
        assign the element_ti_id.
        The values that we get are the ones that get_ti_vals provides.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti> ids
        :type ids: list
        :param element_ti_id: id of the element at the list
        :type element_ti_id: long
        :return: dict
        """
        if ids and isinstance(ids, (tuple, list)):
            ids = ids[0]
        res = {'value': {}, 'domain': {}, 'warning': {}}
        current_element_ti = self.read(
            cursor, uid, ids, ['element_ti_id'])
        if element_ti_id:
            if current_element_ti and current_element_ti['element_ti_id']:
                if current_element_ti['element_ti_id'][0] == element_ti_id:
                    return res
            values = self.get_ti_vals(cursor, uid, {}, element_ti_id)
            values = filter_none_values(values)
            res.update({'value': values})
        return res

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti> ids on which we want to get
        the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        return 1

    __get_investing_additional_factor = _get_investing_additional_factor

    _columns = {
        # Obra
        'obra_id': fields.many2one('giscedata.projecte.obra', 'Obra',
                                   ondelete='cascade'),
        'desc_obra': fields.function(
            _get_descripcio_obra, method=True, type='char', size=128,
            string='Descripción Obra'
        ),
        # Common Data
        'name': fields.char('Identificador', size=22),
        'cini': fields.char('CINI', size=8),
        'ccuu': fields.many2one('giscedata.tipus.installacio', 'TI'),
        'tipo_inversion': fields.selection(TABLA_TIPO_INVERSION,
                                           'Tipo de inversión'),
        'fecha_aps': fields.date('Fecha APS'),
        'fecha_baja': fields.date('Fecha Baja'),
        'causa_baja': fields.selection(TABLA_CAUSA_BAJA, 'Causa Baja'),
        'financiado': fields.float('Financiado', digits=(6, 2)),
        # Costos
        'im_ingenieria': fields.float('Ingeniería (€)'),
        'im_trabajos': fields.float('Trabajos (€)'),
        'im_obracivil': fields.float('Obra civil (€)'),
        'im_materiales': fields.float('Materiales (€)'),
        # Subvenciones
        'subvenciones_europeas': fields.float('Sub. Europeas (€)'),
        'subvenciones_nacionales': fields.float('Sub. Nacionales (€)'),
        # CC
        'cuenta_contable': fields.char('Cuenta contable', size=128),
        # Comparativa
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'bloquear_contabilidad': fields.boolean('Bloquear V. Contable'),
        'valor_auditado': fields.function(
            _ff_calc_auditat, fnct_search=_ff_calc_auditat_search,
            string='Auditado (€)', method=True, type='float',
            readonly=True,
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2),
            method=True, store={
                'giscedata.projecte.obra.ti': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 de "
                 "diciembre."
        ),
        'bloquear_porcentaje_modificacion': fields.boolean(
            'Bloquear Porcentaje Modificación'
        ),
        'codi_ins': fields.many2one(
            'giscedata.codi.installacio', string='Código INS',
            help='"COD_INS" definido por el BOE-A-2015-8624'
        ),
        'sincronizable': fields.boolean('Sincronizable'),
    }

    _defaults = {
        'sincronizable': lambda *args: False,
        'bloquear_contabilidad': lambda *args: True,
        'bloquear_porcentaje_modificacion': lambda *args: True
    }


GiscedataProjecteObraTi()


class GiscedataProjecteObraTiAt(osv.osv):

    _name = 'giscedata.projecte.obra.ti.at'
    _inherit = 'giscedata.projecte.obra.ti'
    
    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ti: <giscedata.projecte.obra.ti.at> browse_record on which we
        want to get the factor
        :param context:
        :return: the factor
        """
        if isinstance(ids, (tuple, list)):
            ids = ids[0]
        q = self.q(cursor, uid)
        read_values = q.read(['longitud']).where([('id', '=', ids)])[0]
        if read_values['longitud']:
            return read_values['longitud'] / 1000
        else:
            return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        if isinstance(element_ti_id, (tuple, list)):
            element_ti_id = element_ti_id[0]

        at_obj = self.pool.get('giscedata.at.tram')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        # Dict where each value for the field TI AT will be executed inside an
        # eval func to get the result
        at_fields = {
            'name': 'ti.name',
            'cini': 'ti.cini',
            'destino': 'ti.final',
            'origen': 'ti.origen',
            'ccuu': 'ti.tipus_instalacio_cnmc_id.id',
            'codigo_ccaa_1': 'ti.linia.municipi.state.comunitat_autonoma.codi',
            'codigo_ccaa_2': 'ti.linia.municipi.state.comunitat_autonoma.codi',
            'nivel_tension_explotacion': 'int(ti.tensio_max_disseny_id.name.replace(\'.\', \'\')) / 1000',
            'numero_circuitos': 'ti.circuits',
            'numero_conductores': 'ti.conductors',
            'longitud': 'ti.longitud_cad',
            'intensidad_maxima': 'ti.cable.intensitat_admisible',
            'seccion': 'ti.cable.seccio',
            # 'financiado': 'perc_financament'
            'fecha_aps': 'ti.data_pm',
            'fecha_baja': 'ti.data_baixa',
            'invest_factor': 'str(ti.longitud_cad or 0.0)'
        }

        ti = at_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, at_field in at_fields.items():
            field_value = False
            try:
                field_value = eval(at_field)
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.at'
                )

        if errors:
            msg_errors = 'Errores:\n {}'.format('\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiAt, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiAt, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiAt, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiAt, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiAt, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args,
                                        context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _fnct_set_price_investment(self, cursor, uid, ids, name, args,
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _get_ti_at_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.at> that belongs to one
                  obra from a list of <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['lat_ids']):
            res += obra['lat_ids']
        return res

    def _fnct_get_tipo_cable(self, cursor, uid, ids, name, args, context=None):
        """
        Get the cable type of the giscedata.at.tram.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.at> ids
        :type ids: list[long]
        :param name: name of the field that triggers the method
        :param args: additional args
        :param context: OpenERP context
        :type context: dict
        :return: dict where the key is an <giscedata.projecte.obra.ti.at> and
        the value is the name of the cable type.
        """
        res = {}
        at_tram_q = self.pool.get('giscedata.at.tram').q(cursor, uid)
        for ti in self.read(cursor, uid, ids, ['element_ti_id']):
            if ti['element_ti_id']:
                tipo_cable = at_tram_q.read(['cable.tipus.id']).where([
                    ('id', '=', ti['element_ti_id'][0])
                ])

                if tipo_cable and tipo_cable[0]['cable.tipus.id']:
                    res[ti['id']] = tipo_cable[0]['cable.tipus.id']
        return res

    _columns = {
        'element_ti_id': fields.many2one('giscedata.at.tram', 'Tramo AT'),
        'origen': fields.char('Origen', size=64),
        'destino': fields.char('Destino', size=64),
        'codigo_ccaa_1': fields.integer('Comunidad autónoma origen'),
        'codigo_ccaa_2': fields.integer('Comunidad autónoma destino'),
        'num_apoyo_total': fields.integer('Num apoyo total'),
        'num_apoyo_suspension': fields.integer('Num apoyo suspension'),
        'num_apoyo_amarre': fields.integer('Num apoyo amarre'),
        'velocidad_viento': fields.float('Velocidad viento'),
        'nivel_tension_explotacion': fields.float('Nivel tensión explotación (kV)',
                                                  digits=(16, 2)),
        'numero_circuitos': fields.integer('Número circuitos'),
        'numero_conductores': fields.integer('Número conductores'),
        'longitud': fields.float('Longitud', readonly=1),
        'intensidad_maxima': fields.integer('Intensidad máxima'),
        'seccion': fields.integer('Sección'),
        'tipo_suelo': fields.selection(TABLA_TIPO_SUELO, 'Tipo suelo'),
        'planificacion': fields.selection(TABLA_PLANIFICACION, 'Planificación'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.at': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_at_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.at': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2), method=True,
            store={
                'giscedata.projecte.obra.ti.at': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€/km)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
        'invest_factor': fields.char(string='Factor inversión', size=128),
        'precio_inversion': fields.function(
            _fnct_set_price_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión (€)',
            help="Precio de inversión calculado como la multiplicación del"
                 " precio unitario según BOE por la longitud del tramo "
                 "(en kilómetros)."
        ),
        'tipo_cable': fields.function(
            _fnct_get_tipo_cable, string='Tipo Cable', type='many2one',
            relation='giscedata.at.tipuscable', method=True
        )
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id,
        'invest_factor': lambda *args: '0.0'
    }


GiscedataProjecteObraTiAt()


class GiscedataProjecteObraTiBt(osv.osv):

    _name = 'giscedata.projecte.obra.ti.bt'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.bt> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        if isinstance(ids, (tuple, list)):
            ids = ids[0]
        q = self.q(cursor, uid)
        read_values = q.read(['longitud']).where([('id', '=', ids)])[0]
        if read_values['longitud']:
            return read_values['longitud'] / 1000
        else:
            return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        if isinstance(element_ti_id, (tuple, list)):
            element_ti_id = element_ti_id[0]

        bt_obj = self.pool.get('giscedata.bt.element')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        bt_fields = {
            'name': 'ti.name',
            'cini': 'ti.cini',
            'origen': 'False',
            'destino': 'False',
            'ccuu': 'ti.tipus_instalacio_cnmc_id.id',
            'codigo_ccaa_1': 'ti.municipi.state.comunitat_autonoma.codi',
            'codigo_ccaa_2': 'ti.municipi.state.comunitat_autonoma.codi',
            'nivel_tension_explotacion': 'int(ti.voltatge.replace(\'.\', \'\')) / 1000',
            'numero_circuitos': 'False',
            'numero_conductores': 'False',
            'longitud': 'ti.longitud_cad',
            'intensidad_maxima': 'ti.cable.intensitat_admisible',
            'seccion': 'ti.cable.seccio',
            # 'financiado': 'perc_financament'
            'fecha_aps': 'ti.data_pm',
            'fecha_baja': 'ti.data_baixa',
            'invest_factor': 'str(ti.longitud_cad or 0.0)'
        }
        ti = bt_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, bt_field in bt_fields.items():
            field_value = False
            try:
                field_value = eval(bt_field)
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.bt'
                )

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiBt, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiBt, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiBt, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiBt, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiBt, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args, 
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _fnct_set_price_investment(self, cursor, uid, ids, name, args,
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _fnct_get_tipo_cable(self, cursor, uid, ids, field_name, arg,
                              context=None):
        """
        Get the cable type of the giscedata.at.tram.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.bt> ids
        :type ids: list[long]
        :param name: name of the field that triggers the method
        :param args: additional args
        :param context: OpenERP context
        :type context: dict
        :return: dict where the key is an <giscedata.projecte.obra.ti.bt> and
        the value is the name of the cable type.
        """
        res = {}
        at_tram_q = self.pool.get('giscedata.bt.element').q(cursor, uid)
        for ti in self.read(cursor, uid, ids, ['element_ti_id']):
            if ti['element_ti_id']:
                tipo_cable = at_tram_q.read(['cable.tipus.id']).where([
                    ('id', '=', ti['element_ti_id'][0])
                ])

                if tipo_cable and tipo_cable[0]['cable.tipus.id']:
                    res[ti['id']] = tipo_cable[0]['cable.tipus.id']
        return res

    def _get_ti_bt_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.bt> that
                  belongs to one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['lbt_ids']):
            res += obra['lbt_ids']
        return res

    _columns = {
        'element_ti_id': fields.many2one('giscedata.bt.element', 'Elemento BT'),
        'origen': fields.char('Origen', size=64),
        'destino': fields.char('Destino', size=64),
        'codigo_ccaa_1': fields.integer('Comunidad autónoma origen'),
        'codigo_ccaa_2': fields.integer('Comunidad autónoma destino'),
        'nivel_tension_explotacion': fields.float('Nivel tensión explotación (kV)',
                                                  digits=(16, 2)),
        'numero_circuitos': fields.integer('Número circuitos'),
        'numero_conductores': fields.integer('Número conductores'),
        'longitud': fields.float('Longitud'),
        'intensidad_maxima': fields.integer('Intensidad máxima'),
        'seccion': fields.integer('Sección'),
        'tipo_suelo': fields.selection(TABLA_TIPO_SUELO, 'Tipo suelo'),
        'planificacion': fields.selection(TABLA_PLANIFICACION, 'Planificación'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.bt': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_bt_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.bt': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2), method=True,
            store={
                'giscedata.projecte.obra.ti.bt': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€/km)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
        'invest_factor': fields.char(string='Factor inversión', size=128),
        'precio_inversion': fields.function(
            _fnct_set_price_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión (€)',
            help="Precio de inversión calculado como la multiplicación del"
                 " precio unitario según BOE por la longitud del tramo "
                 "(en kilómetros)."
        ),
        'tipo_cable': fields.function(
            _fnct_get_tipo_cable, string='Tipo Cable', type='many2one',
            relation='giscedata.bt.tipuscable', method=True
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id,
        'invest_factor': lambda *args: '0.0'
    }


GiscedataProjecteObraTiBt()


class GiscedataProjecteObraTiSubestacions(osv.osv):

    _name = 'giscedata.projecte.obra.ti.subestacions'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.subestacions> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals
        if isinstance(element_ti_id, (tuple, list)):
            element_ti_id = element_ti_id[0]

        sub_obj = self.pool.get('giscedata.cts.subestacions')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        se_fields = {
            'name': 'name',
            'cini': 'cini',
            'denominacion': 'name',
            'fecha_aps': 'data_pm',
            'fecha_baja': 'data_baixa',
            'ccuu': 'ct_id.tipus_instalacio_cnmc_id.id',
            # 'financiado': 'perc_financament'
        }

        ti = sub_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, se_field in se_fields.items():
            field_value = False
            try:
                field_value = eval('ti.{}'.format(se_field))
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.subestacions'
                )

        # Llegim la informacio dels parcs de la SE.
        for i, parc in enumerate(ti.parc_id):
            vals.update({
                'num_posiciones_parque_{}'.format(i + 1): (
                    len(parc.posicio_ids)
                ),
                'identificador_parque_{}'.format(i + 1): str(parc.name),
                'cini_parque_{}'.format(i + 1): str(parc.cini),
            })

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiSubestacions, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiSubestacions, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiSubestacions, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiSubestacions, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiSubestacions, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args, 
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _get_ti_se_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.subestacions> that
                  belongs to one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['se_ids']):
            res += obra['se_ids']
        return res

    _columns = {
        'denominacion': fields.char('Denominación', size=128),
        'element_ti_id': fields.many2one('giscedata.cts.subestacions',
                                         'Subestación'),
        'tipo_suelo': fields.selection(TABLA_TIPO_SUELO, 'Tipo suelo'),
        'planificacion': fields.selection(TABLA_PLANIFICACION, 'Planificación'),
        'identificador_parque_1': fields.char(
            'Identificador parque 1', size=32),
        'identificador_parque_2': fields.char(
            'Identificador parque 2', size=32),
        'identificador_parque_3': fields.char(
            'Identificador parque 3', size=32),
        'identificador_parque_4': fields.char(
            'Identificador parque 4', size=32),
        'cini_parque_1': fields.char('CINI Parque 1', size=8),
        'cini_parque_2': fields.char('CINI Parque 2', size=8),
        'cini_parque_3': fields.char('CINI Parque 3', size=8),
        'cini_parque_4': fields.char('CINI Parque 4', size=8),
        'num_posiciones_parque_1': fields.integer('Nº posición parque 1'),
        'num_posiciones_parque_2': fields.integer('Nº posición parque 2'),
        'num_posiciones_parque_3': fields.integer('Nº posición parque 3'),
        'num_posiciones_parque_4': fields.integer('Nº posición parque 4'),
        'pn_transformacion': fields.integer('PN Transformación'),
        'pn_reactancias': fields.integer('PN Reactancias'),
        'pn_condensadores': fields.integer('PN Condensadores'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.subestacions': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_se_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.subestacions': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2), method=True,
            store={
                'giscedata.projecte.obra.ti.subestacions': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id
    }


GiscedataProjecteObraTiSubestacions()


class GiscedataProjecteObraTiPosicio(osv.osv):

    _name = 'giscedata.projecte.obra.ti.posicio'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.posicio> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        pos_obj = self.pool.get('giscedata.cts.subestacions.posicio')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        pos_fields = {
            'name': 'name',
            'cini': 'cini',
            'denominacion': 'name',
            'ccuu': 'tipus_instalacio_cnmc_id.id',
            'codigo_ccaa': 'subestacio_id.ct_id.id_municipi.state.comunitat_autonoma.codi',
            'identificador_parque': 'parc_id.name',
            'nivel_tension_explotacion': 'tensio.tensio / 1000',
            # 'financiado': 'perc_financament'
            'fecha_aps': 'data_pm',
            'fecha_baja': 'data_baixa',
        }
        ti = pos_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, pos_field in pos_fields.items():
            field_value = False
            try:
                field_value = eval('ti.{}'.format(pos_field))
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.posicio'
                )

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiPosicio, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiPosicio, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiPosicio, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiPosicio, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiPosicio, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args, 
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _get_ti_pos_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.posicio> that belongs to
                  one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['pos_ids']):
            res += obra['pos_ids']
        return res

    _columns = {
        'denominacion': fields.char('Denominación', size=128),
        'element_ti_id': fields.many2one(
            'giscedata.cts.subestacions.posicio', 'Posición'),
        'codigo_ccaa': fields.integer('Comunidad autónoma'),
        'identificador_parque': fields.char('Identificador parque', size=20),
        'nivel_tension_explotacion': fields.float('Nivel tensión explotación (kV)',
                                                  digits=(16, 2)),
        'planificacion': fields.selection(TABLA_PLANIFICACION, 'Planificación'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.posicio': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_pos_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.posicio': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2), method=True,
            store={
                'giscedata.projecte.obra.ti.posicio': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id
    }


GiscedataProjecteObraTiPosicio()


class GiscedataProjecteObraTiTransformador(osv.osv):

    _name = 'giscedata.projecte.obra.ti.transformador'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.transformador> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        if isinstance(ids, (tuple, list)):
            ids = ids[0]
        q = self.q(cursor, uid)
        read_values = q.read(['potencia_instalada']).where([('id', '=', ids)])[0]
        # The unity of the field potencia_instalada is KVA, so we divide it by
        # 1000 to get the MVAs.
        if read_values['potencia_instalada']:
            return read_values['potencia_instalada'] / 1000
        else:
            return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        trafo_obj = self.pool.get('giscedata.transformador.trafo')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        maq_fields = {
            'name': 'ti.name',
            'cini': 'ti.cini',
            'identificador_emplazamiento': 'ti.name',
            'ccuu': 'ti.tipus_instalacio_cnmc_id.id',
            'codigo_ccaa': 'ti.ct.id_municipi.state.comunitat_autonoma.codi',
            'nivel_tension_explotacion': 'ti.tensio_primari_actual',
            'potencia_instalada': 'ti.potencia_nominal',
            # 'financiado': 'ti.perc_financament'
            'fecha_aps': 'ti.data_pm',
            'fecha_baja': 'ti.data_baixa',
            'invest_factor': 'str(ti.potencia_nominal or 0.0)'
        }
        ti = trafo_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, trafo_field in maq_fields.items():
            field_value = False
            try:
                field_value = eval(trafo_field)
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.transformador'
                )

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiTransformador, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiTransformador, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiTransformador, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiTransformador, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiTransformador, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args, 
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _fnct_set_price_investment(self, cursor, uid, ids, name, args,
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _get_ti_maq_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.transformador> that
                  belongs to one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['maq_ids']):
            res += obra['maq_ids']
        return res

    _columns = {
        'element_ti_id': fields.many2one(
            'giscedata.transformador.trafo', 'Transformador'),
        'identificador_emplazamiento': fields.integer(
            'Identificador Emplazamiento', size=32),
        'codigo_ccaa': fields.integer('Comunidad autónoma'),
        'nivel_tension_explotacion': fields.float('Nivel tensión explotación (kV)',
                                                  digits=(16, 2)),
        'potencia_instalada': fields.float('Potencia instalada (KVA)'),
        'planificacion': fields.selection(TABLA_PLANIFICACION, 'Planificación'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.transformador': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_maq_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.transformador': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2), method=True,
            store={
                'giscedata.projecte.obra.ti.transformador': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€/MVA)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
        'invest_factor': fields.char(string='Factor inversión', size=128),
        'precio_inversion': fields.function(
            _fnct_set_price_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión (€)',
            help="Precio de inversión calculado como la multiplicación del"
                 " precio unitario según BOE por la potencia del transformador "
                 "(en MVA)."
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id,
        'invest_factor': lambda *args: '0.0'
    }


GiscedataProjecteObraTiTransformador()


class GiscedataProjecteObraTiDespatx(osv.osv):

    _name = 'giscedata.projecte.obra.ti.despatx'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.despatx> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        des_obj = self.pool.get('giscedata.despatx')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        des_fields = {
            'name': 'denominacio',
            'cini': 'cini',
            'codigo_ccaa': 'municipi.state.comunitat_autonoma.codi',
            'fecha_aps': 'data_apm',
            'fecha_baja': 'data_baixa',
        }
        ti = des_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, des_field in des_fields.items():
            field_value = False
            try:
                field_value = eval('ti.{}'.format(des_field))
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.despatx'
                )

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiDespatx, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiDespatx, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiDespatx, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiDespatx, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiDespatx, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _get_ti_des_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.despatx> that
                  belongs to one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['des_ids']):
            res += obra['des_ids']
        return res

    _columns = {
        'element_ti_id': fields.many2one('giscedata.despatx', 'Despacho'),
        'codigo_ccaa': fields.integer('Comunidad autónoma'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.despatx': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_des_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.despatx': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id
    }


GiscedataProjecteObraTiDespatx()


class GiscedataProjecteObraTiCelles(osv.osv):

    _name = 'giscedata.projecte.obra.ti.celles'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.celles> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        fia_obj = self.pool.get('giscedata.celles.cella')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        fia_fields = {
            'name': 'ti.name',
            'cini': 'ti.cini',
            'ccuu': 'ti.tipus_instalacio_cnmc_id.id',
            'nivel_tension_explotacion': 'ti.tensio.tensio / 1000',
            'elemento_act': 'int(ti.cini[-1])',
            'fecha_aps': 'ti.data_pm',
            'fecha_baja': 'ti.data_baixa',
            # 'financiado': 'perc_financament'
        }
        ti = fia_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, fia_field in fia_fields.items():
            field_value = False
            try:
                field_value = eval(fia_field)
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.celles'
                )

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        # Encadenem les relacions per arribar a obtenir el municipi
        # depenent del model relacionat amb les instal·lacions de la
        # cel·la
        model, ref = ti.installacio.split(',')
        if model == 'giscedata.at.suport':
            chain_keys = 'linia.municipi.state.comunitat_autonoma.codi'
        elif model == 'giscedata.cts':
            chain_keys = 'id_municipi.state.comunitat_autonoma.codi'
        else:
            chain_keys = False

        if chain_keys:
            q = self.pool.get(model).q(cursor, uid)
            ccaa = q.read([chain_keys]).where([
                ('id', '=', int(ref))
            ])
            if ccaa:
                vals['codigo_ccaa'] = ccaa[0][chain_keys]

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiCelles, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiCelles, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiCelles, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiCelles, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiCelles, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args, 
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _get_ti_fia_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.celles> that
                  belongs to one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['fia_ids']):
            res += obra['fia_ids']
        return res

    _columns = {
        'element_ti_id': fields.many2one('giscedata.celles.cella', 'Celda'),
        'codigo_ccaa': fields.integer('Comunidad autónoma'),
        'nivel_tension_explotacion': fields.float('Nivel tensión explotación (kV)',
                                                  digits=(16, 2)),
        'elemento_act': fields.integer('Nivel ACT'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.celles': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_fia_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.celles': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2), method=True,
            store={
                'giscedata.projecte.obra.ti.celles': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id
    }


GiscedataProjecteObraTiCelles()


class GiscedataProjecteObraTiCts(osv.osv):

    _name = 'giscedata.projecte.obra.ti.cts'
    _inherit = 'giscedata.projecte.obra.ti'

    def _get_investing_additional_factor(self, cursor, uid, ids, context=None):
        """
        Obtains the additional factor that is added to investing one TI.
        e.g. in case of the ATs, the unity of the investing is €/km, so the
        additional factor are the longitude of the line.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.ti.cts> ids on which we
        want to get the factor
        :type ids: list
        :param context:
        :return: the factor
        """
        return 1

    def get_ti_vals(self, cursor, uid, vals, element_ti_id, context=None):
        """
        Given a list of values that are to be written on a
        <giscedata.projecte.obra.ti>, this method updates them with the values
        of its corresponding installation element; whether it's a AT, BT, or
        another type of installation.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: dict containing values that will be updated
        :type vals: dict
        :param element_ti_id: id of the installation associated with
        :type element_ti_id: long
        :param context:
        :return: the vals dict with the values updated
        """
        if not element_ti_id:
            return vals

        ct_obj = self.pool.get('giscedata.cts')
        ti_obj = self.pool.get('giscedata.projecte.obra.ti')
        ct_fields = {
            'name': 'ti.name',
            'cini': 'ti.cini',
            'ccuu': 'ti.tipus_instalacio_cnmc_id.id',
            'codigo_ccaa': 'ti.id_municipi.state.comunitat_autonoma.codi',
            'nivel_tension_explotacion': 'int(ti.tensio_p.replace(\'.\', \'\')) / 1000',
            # 'financiado': 'perc_financament'
            'fecha_aps': 'ti.data_pm',
            'fecha_baja': 'ti.data_baixa',
        }
        ti = ct_obj.browse(cursor, uid, element_ti_id)
        if not ti:
            return vals

        errors = []
        for obra_ti_field, ct_field in ct_fields.items():
            field_value = False
            try:
                field_value = eval(ct_field)
            except AttributeError:
                # In case the field doesn't exist in the browse_record
                pass
            except Exception as e:
                msg = 'Error: %s' % e.message
                errors.append(_('Campo {field}: {error_message}').format(
                    field=obra_ti_field, error_message=msg))
                sentry = self.pool.get('sentry.setup')
                if sentry:
                    sentry.client.captureException()

            if field_value:
                vals[obra_ti_field] = ti_obj.cast_value_if_necessary(
                    cursor, uid, field_value, obra_ti_field,
                    'giscedata.projecte.obra.ti.cts'
                )

        if errors:
            msg_errors = 'Errores durante la sincronización: \n {}'.format(
                '\n'.join(errors))
            raise osv.except_osv(_('Atención'), msg_errors)

        return vals

    _get_ti_vals = get_ti_vals

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiCts, self)._fnct_is_synced(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_set_contabilidad(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiCts, self)._fnct_set_contabilidad(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_valor_contabilidad(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiCts, self)._fnct_inv_valor_contabilidad(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_porcentaje_modificacion(self, cursor, uid, ids, field_name, arg, context=None):
        return super(GiscedataProjecteObraTiCts, self)._fnct_set_porcentaje_modificacion(
            cursor, uid, ids, field_name, arg, context=context)

    def _fnct_inv_porcentaje_modificacion(self, cursor, uid, ids, name, value, args,
                          context=None):
        return super(GiscedataProjecteObraTiCts, self)._fnct_inv_porcentaje_modificacion(
            cursor, uid, ids, name, value, args, context=context)

    def _fnct_set_price_unit_investment(self, cursor, uid, ids, name, args, 
                                   context=None):
        return GiscedataProjecteObraTi._fnct_set_price_unit_investment(
            self, cursor, uid, ids, name, args, context=context)

    def _get_ti_cts_ids(self, cursor, uid, ids, context=None):
        """
        :returns: a list of <giscedata.projecte.obra.ti.cts> that
                  belongs to one obra from a list of
                  <giscedata.projecte.obra.detall.economic>.
        """
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obres_ids = detall_obj.get_obres_ids(cursor, uid, ids)
        res = []
        for obra in obra_obj.read(cursor, uid, obres_ids, ['ct_ids']):
            res += obra['ct_ids']
        return res

    _columns = {
        'element_ti_id': fields.many2one('giscedata.cts', 'CT'),
        'codigo_ccaa': fields.integer('Comunidad autónoma'),
        'nivel_tension_explotacion': fields.float('Nivel tensión explotación (kV)',
                                                  digits=(16, 2)),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store={
                'giscedata.projecte.obra.ti.cts': (
                    lambda self, cr, uid, ids, ctx=None: ids, ['ccuu'], 10
                ),
                'giscedata.projecte.obra.detall.economic': (
                    _get_ti_cts_ids, ['ti_ccuu'], 10
                ),
            }
        ),
        'valor_contabilidad': fields.function(
            _fnct_set_contabilidad, type='float', method=True, store={
                'giscedata.projecte.obra.ti.cts': (
                    lambda self, cr, uid, ids, c=None: ids, [
                        'valor_auditado', 'bloquear_contabilidad',
                        'im_ingenieria', 'im_trabajos', 'im_obracivil',
                        'im_materiales'
                    ], 10
                ),
            }, fnct_inv=_fnct_inv_valor_contabilidad, string='Contabilidad (€)'
        ),
        'porcentaje_modificacion': fields.function(
            _fnct_set_porcentaje_modificacion, type='float', digits=(6, 2),
            method=True, store={
                'giscedata.projecte.obra.ti.cts': (
                    lambda self, cr, uid, ids, c=None: ids,
                    ['valor_contabilidad', 'valor_auditado', 'ccuu',
                     'im_ingenieria', 'im_trabajos', 'im_obracivil',
                     'im_materiales'], 10
                ),
            }, fnct_inv=_fnct_inv_porcentaje_modificacion,
            string='Porcentaje Modificación',
            help="En caso de instalación a coste parcial o sin nuevas unidades"
                 " físicas, proporción de modificación de la instalación a que"
                 " hace referencia el campo IDENTIFICADOR respecto a la unidad"
                 " completa definida en la Orden IET/2660/2015, de 11 "
                 "de diciembre."
        ),
        'precio_inversion_unitario': fields.function(
            _fnct_set_price_unit_investment, type='float', digits=(6, 2),
            method=True, string='P. Inversión Unit. (€)',
            help="Precio de inversión establecido por la  "
                 "órden vigente."
        ),
    }

    def _default_obra_id(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _defaults = {
        'obra_id': _default_obra_id
    }


GiscedataProjecteObraTiCts()


class GiscedataProjecteObraTiView(osv.osv):

    _name = 'giscedata.projecte.obra.ti.view'
    _inherit = 'giscedata.projecte.obra.ti'
    _auto = False

    _additional_fields = ','.join(['synced'])

    # Obtenim els camps del model pare de manera dinàmica, de manera que si
    # algun camp canvia o se n'afegeix algun, podem construir el model
    # 'ti.view' d'igual manera.
    # Seleccionem els camps que no son funció o, si ho són, que siguin 'stored'.
    _camps_generic = ','.join(dict(
        filter(
            lambda (field, field_desc): type(field_desc) is not fields.function or field_desc.store,
            GiscedataProjecteObraTi._columns.items()
        )
    )) + ',' + _additional_fields

    _query = """
        SELECT 
        ROW_NUMBER () OVER (ORDER BY ti.obra_id, ti.ref_element_ti) AS "id",
        ti.*
        FROM (
            (
                SELECT 
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.at.tram,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.at,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_at
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL 
                        ELSE 'giscedata.bt.element,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.bt,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_bt
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.cts.subestacions,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.subestacions,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_subestacions
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.cts.subestacions.posicio,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.posicio,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_posicio
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.transformador.trafo,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.transformador,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_transformador
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.despatx,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.despatx,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_despatx
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.celles.cella,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.celles,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_celles
                ORDER BY id
            ) UNION (
                SELECT
                    CASE WHEN element_ti_id IS NULL
                        THEN NULL
                        ELSE 'giscedata.cts,'||element_ti_id
                    END AS ref_element,
                    'giscedata.projecte.obra.ti.cts,'||id AS ref_element_ti,
                    {camps_element_ti}
                FROM giscedata_projecte_obra_ti_cts
                ORDER BY id
            ) 
        ) AS ti
    """.format(camps_element_ti=_camps_generic,)

    def _obtenir_element(self, cursor, uid, context=None):
        return [
            ('giscedata.at.tram', _('Tramo AT')),
            ('giscedata.bt.element', _('Tramo BT')),
            ('giscedata.cts.subestacions', _('Subestación')),
            ('giscedata.cts.subestacions.posicio', _('Posición')),
            ('giscedata.transformador.trafo', _('Transformador')),
            ('giscedata.despatx', _('Despacho')),
            ('giscedata.celles.cella', _('Celda')),
            ('giscedata.cts', _('CT'))
        ]

    def _obtenir_element_ti(self, cursor, uid, context=None):
        return [
            ('giscedata.projecte.obra.ti.at', _('Tramo AT')),
            ('giscedata.projecte.obra.ti.bt', _('Tramo BT')),
            ('giscedata.projecte.obra.ti.subestacions', _('Subestación')),
            ('giscedata.projecte.obra.ti.posicio', _('Posición')),
            ('giscedata.projecte.obra.ti.transformador', _('Transformador')),
            ('giscedata.projecte.obra.ti.despatx', _('Despacho')),
            ('giscedata.projecte.obra.ti.celles', _('Celda')),
            ('giscedata.projecte.obra.ti.cts', _('CT')),
        ]

    def init(self, cursor):
        query = "DROP VIEW IF EXISTS {view_name}".format(**{
            'view_name': self._name.replace('.', '_')
        })
        cursor.execute(query)

        query = (
            "CREATE VIEW public.{view_name}\nAS {ti_query}"
        ).format(**{
            'view_name': self._name.replace('.', '_'),
            'ti_query': self._query,
        })
        cursor.execute(query)

    _columns = {
        'ref_element': fields.reference(
            'Elemento', selection=_obtenir_element, size=128,
            help='Elemento real'
        ),
        'ref_element_ti': fields.reference(
            'Elemento TI', selection=_obtenir_element_ti, size=128,
            help='Elemento TI dentro la obra'
        ),
        'synced': fields.boolean('Sincronizado')
    }


GiscedataProjecteObraTiView()


class GiscedataProjecteObraDetallEconomic(osv.osv):

    _name = 'giscedata.projecte.obra.detall.economic'
    _description = 'Detalle Económico'

    def return_existing_detalls(self, cursor, uid, values, context=None):
        """
        Know if there exists any object <giscedata.projcte.obra.detall.economic>
        with matching obra_id and ti_ccuu.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param values: fields that will be checked.
        :type values: dict[str, str | int]
        :param context: OpenERP context
        :type context: dict
        :return: The result of search count
        :rtype: int
        """
        if context is None:
            context = {}
        ti_import_obj = self.pool.get('giscedata.projecte.obra.detall.economic')
        search_params = [
            (detall_field, '=', field_value)
            for detall_field, field_value in values.items()
        ]
        return ti_import_obj.search(cursor, uid, search_params, context=context)

    def _fnct_is_synced(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Sets the value for the field 'synced'. That is, whether there is any
        <giscedata.projecte.obra.ti> in that obra with same CCUU code or not.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.detall.economic> ids
        :type ids: list[long]
        :param field_name:
        :param arg:
        :param context: OpenERP Context
        :type context: dict
        :return: dict where the key is the id of the detall economic and the
        value is the value of the 'synced' field.
        """
        res = dict.fromkeys(ids, False)
        ti_obj = self.pool.get('giscedata.projecte.obra.ti.view')
        for detall in self.read(cursor, uid, ids, ['ti_ccuu', 'obra_id']):
            if detall['ti_ccuu'] and detall['obra_id']:
                equivalent = ti_obj.search(cursor, uid, [
                    ('obra_id', '=', detall['obra_id'][0]),
                    ('ccuu', '=', detall['ti_ccuu'][0])
                ])
                if equivalent:
                    res[detall['id']] = True
        return res

    def obtenir_tipus_ti(self, *args):
        """
        Returns the type of installations that can be associated with an "Obra".
        :return: list of tuples of the object and the name
        :type: list[(str | unicode, str | unicode)]
        """
        return [
            ('giscedata.projecte.obra.ti.at', _('Tramo AT')),
            ('giscedata.projecte.obra.ti.bt', _('Tramo BT')),
            ('giscedata.projecte.obra.ti.subestacions', _('Subestación')),
            ('giscedata.projecte.obra.ti.posicio', _('Posición')),
            ('giscedata.projecte.obra.ti.transformador', _('Transformador')),
            ('giscedata.projecte.obra.ti.despatx', _('Despacho')),
            ('giscedata.projecte.obra.ti.celles', _('Celda')),
            ('giscedata.projecte.obra.ti.cts', _('CT'))
        ]

    def get_obres_ids(self, cursor, uid, ids, context=None):
        """
        Obtains the list of <giscedata.projecte.obra> associated with a list of
        detall economic objects.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.detall.economic> ids
        :type ids: list[long]
        :param context:
        :return: the list of <giscedata.projecte.obra> ids
        :rtype: list
        """
        res = []
        for detall_values in self.read(cursor, uid, ids, ['obra_id']):
            if detall_values['obra_id']:
                res.append(detall_values['obra_id'][0])
        return res

    def _calc_im_total(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Obtains the field 'im_total' of the detall economic, that is, the sum
        of all the other im_%.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra.detall.economic> ids
        :type ids: list[long]
        :param field_name:
        :param arg:
        :param context:
        :return: the dict that holds the values, indexed by detall economic ids
        :rtype: dict
        """
        res = dict.fromkeys(ids, 0.0)
        for detall in self.read(cursor, uid, ids, [
            'im_ingenieria', 'im_trabajos', 'im_obracivil', 'im_materiales'
        ]):
            res[detall['id']] = (
                (detall['im_ingenieria'] or 0.0) +
                (detall['im_trabajos'] or 0.0) +
                (detall['im_obracivil'] or 0.0) +
                (detall['im_materiales'] or 0.0)
            )
        return res

    def _default_obra(self, cursor, uid, context=None):
        if context is None:
            context = {}
        return context.get('obra_id', False)

    _STORE_SYNCED = {
        'giscedata.projecte.obra.detall.economic': (
            lambda self, cr, uid, ids, c=None: ids, ['obra_id', 'ti_ccuu'], 20
        ),
    }

    _columns = {
        'obra_id': fields.many2one('giscedata.projecte.obra', 'Obra',
                                   ondelete='cascade'),
        'ti_ccuu': fields.many2one('giscedata.tipus.installacio', 'TI',
                                   ondelete='cascade'),
        'codi_ins': fields.many2one(
            'giscedata.codi.installacio', string='Código INS',
            help='"COD_INS" definido por el BOE-A-2015-8624'
        ),
        'financiado': fields.float('Financiado (%)', digits=(6, 2)),
        'im_ingenieria': fields.float('Ingeniería (€)', digits=(16, 2)),
        'im_trabajos': fields.float('Trabajos (€)', digits=(16, 2)),
        'im_obracivil': fields.float('Obra civil (€)', digits=(16, 2)),
        'im_materiales': fields.float('Materiales (€)', digits=(16, 2)),
        'im_total': fields.function(
            _calc_im_total, string='Total (€)', type='float', digits=(16, 2),
            readonly=True, method=True
        ),
        'tipo_inversion': fields.selection(TABLA_TIPO_INVERSION,
                                           'Tipo de inversión'),
        'tipus_ti': fields.selection(obtenir_tipus_ti, 'Tipo TI'),
        'synced': fields.function(
            _fnct_is_synced, type='boolean', method=True, string="Sincronizado",
            store=_STORE_SYNCED
        ),
        'actividad': fields.char('Actividad', size=128),
        'sincronizable': fields.boolean('Sincronizable'),
    }

    _defaults = {
        'sincronizable': lambda *args: True,
        'obra_id': _default_obra
    }


GiscedataProjecteObraDetallEconomic()
