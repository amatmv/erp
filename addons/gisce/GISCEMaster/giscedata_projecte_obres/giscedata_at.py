# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields
from .giscedata_projecte_obres import GiscedataProjecteObra


class GiscedataAtTram(osv.osv):

    _name = 'giscedata.at.tram'
    _inherit = 'giscedata.at.tram'

    def search(self, cursor, uid, args, offset=0,
               limit=None, order=None, context=None, count=False):
        """
        Sobreescritura del método de búsqueda para que busque los elementos que
        tienen asociadas las líneas coincidentes a la obra que viene por
        parámetro "args"
        """
        new_args = GiscedataProjecteObra.search_elements(
            self, cursor, uid, args
        )
        return super(GiscedataAtTram, self).search(
            cursor, uid, new_args, offset, limit, order, context, count
        )

    _columns = {
        'ti_obres_ids': fields.one2many(
            'giscedata.projecte.obra.ti.at', 'element_ti_id', 'Obras')
    }


GiscedataAtTram()
