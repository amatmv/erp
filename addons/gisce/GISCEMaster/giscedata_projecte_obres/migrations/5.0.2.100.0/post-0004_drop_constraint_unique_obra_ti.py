# coding=utf-8

import logging


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')
    logger.info('Deleting the constraint in detall economic unique(obra, ti) '
                'if exists')
    q = """
        ALTER TABLE giscedata_projecte_obra_detall_economic
        DROP CONSTRAINT IF EXISTS giscedata_projecte_obra_detall_economic_ccuu_unique;
    """

    cursor.execute(q)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up