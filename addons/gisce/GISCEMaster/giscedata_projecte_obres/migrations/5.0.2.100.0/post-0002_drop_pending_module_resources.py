# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    def get_resources_from_module(module):
        cursor.execute('''
            SELECT id, model, res_id 
            FROM ir_model_data 
            WHERE module=%s;
        ''', (module,))
        return cursor.dictfetchall()

    logger = logging.getLogger('openerp.migration')
    modules = (
        'giscedata_projecte_obres_stock', 'giscedata_projecte_obres_tasks',
        'giscedata_projecte_obres_invoices',
        'giscedata_projecte_obres_extended',
        'giscedata_projecte_obres_circulars'
    )
    info_msg = '''
        Dropping data from modules {} that hasn't been moved from module to the base.
    '''.format(', '.join(modules))

    logger.info(info_msg)

    pool = pooler.get_pool(cursor.dbname)

    for module in modules:
        module_data = get_resources_from_module(module)

        for model_data_info in module_data:
            # Obtenim la taula del model a borrar
            table = pool.get(model_data_info['model'])._table

            logger.info('Dropping res #{} from table {}.'.format(
                model_data_info['res_id'], table))

            # Finalment borrem el registre que tingui el res_id
            cursor.execute('''
                DELETE FROM {}
                WHERE id = %s
            '''.format(table), (model_data_info['res_id'],))
            cursor.execute('''
                DELETE FROM ir_model_data
                WHERE id = %s
            ''', (model_data_info['id'],))

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
