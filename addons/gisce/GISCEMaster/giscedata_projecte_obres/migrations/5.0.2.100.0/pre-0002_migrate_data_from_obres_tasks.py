# coding=utf-8
import logging


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('''
        Moving data from obres_tasks module to obres base module.
    ''')

    cursor.execute("""
        UPDATE ir_model_data 
        SET module='giscedata_projecte_obres' 
        WHERE id IN (
            SELECT id 
            FROM ir_model_data 
            WHERE module='giscedata_projecte_obres_tasks' 
            AND name NOT IN (
                SELECT name 
                FROM ir_model_data 
                WHERE module='giscedata_projecte_obres'
            )
        );
    """)

    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
