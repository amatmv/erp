# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('''
        Migrate the data from the obra to the new architecture, that is, a
        inherits of project.project.
    ''')

    pool = pooler.get_pool(cursor.dbname)
    uid = 1
    project_o = pool.get('project.project')
    obra_o = pool.get('giscedata.projecte.obra')
    task_o = pool.get('project.task')
    imd_o = pool.get('ir.model.data')

    parent_project_id = imd_o.get_object_reference(
            cursor, uid, 'giscedata_projecte_obres', 'parent_project_obres'
    )[1]
    project_cols = [
        'name', 'active', 'category_id', 'priority', 'manager', 'warn_manager',
        'parent_id', 'date_start', 'date_end', 'partner_id', 'contact_id',
        'warn_customer', 'warn_header', 'warn_footer', 'notes', 'timesheet_id',
        'state', 'id'
    ]

    q = """
        SELECT {} FROM giscedata_projecte_obra
    """.format(', '.join(project_cols))
    cursor.execute(q)
    obra_vs = cursor.dictfetchall()

    for obra_v in obra_vs:
        obra_id = obra_v.pop('id')
        project_id = project_o.create(cursor, uid, obra_v)
        obra_o.write(cursor, uid, obra_id, {
            'project_id': project_id,
            'parent_id': parent_project_id,
        })
        q = """
            SELECT id FROM project_task WHERE obra_id = %s 
        """
        cursor.execute(q, (obra_id,))
        task_ids = [task[0] for task in cursor.fetchall()]
        task_o.write(cursor, uid, task_ids, {
            'project_id': project_id
        })

    cursor.execute('ALTER TABLE project_task DROP COLUMN IF EXISTS obra_id')
    project_cols.remove('id')
    for col in project_cols:
        q = '''
            ALTER TABLE giscedata_projecte_obra DROP COLUMN IF EXISTS {}
        '''.format(col)
        cursor.execute(q)

    logger.info('Migration successful!')


def down(cursor, installed_version):
    pass


migrate = up
