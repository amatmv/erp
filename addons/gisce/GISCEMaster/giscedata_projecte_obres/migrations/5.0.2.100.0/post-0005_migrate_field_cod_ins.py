# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('''
        Migrating the value of the cod_ins field from char to many2one
    ''')

    cursor.execute("""
        UPDATE giscedata_projecte_obra_detall_economic AS detall
        SET codi_ins = installacio.id
        FROM giscedata_codi_installacio AS installacio
        WHERE detall.codi_ins = installacio.name;
    """)

    cursor.execute("""
        UPDATE giscedata_projecte_obra_ti AS obra_ti
        SET codi_ins = installacio.id
        FROM giscedata_codi_installacio AS installacio
        WHERE obra_ti.codi_ins = installacio.name;
    """)

    cursor.execute("""
        ALTER TABLE giscedata_projecte_obra_detall_economic
        ALTER COLUMN codi_ins TYPE INTEGER USING codi_ins::integer;
    """)

    cursor.execute("""
        ALTER TABLE giscedata_projecte_obra_ti
        ALTER COLUMN codi_ins TYPE INTEGER USING codi_ins::integer;
    """)

    logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
