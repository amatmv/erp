# coding=utf-8

import logging
from oopgrade import oopgrade


def up(cursor, installed_version):
    if not installed_version:
        return

    logger = logging.getLogger('openerp.migration')

    logger.info('Migrating ot_out_id to obra_id if exists...')
    if oopgrade.column_exists(cursor, 'stock_picking', 'ot_out_id'):
        q = '''
            UPDATE stock_picking SET obra_id = ot_out_id WHERE ot_out_id IS NOT NULL
        '''
        cursor.execute(q)
        q = '''
            ALTER TABLE stock_picking DROP COLUMN ot_out_id
        '''
        cursor.execute(q)

    logger.info('Migrating ot_in_id to obra_id if exists...')
    if oopgrade.column_exists(cursor, 'stock_picking', 'ot_in_id'):
        q = '''
            UPDATE stock_picking SET obra_id = ot_in_id WHERE ot_in_id IS NOT NULL
        '''
        cursor.execute(q)
        q = '''
            ALTER TABLE stock_picking DROP COLUMN  ot_in_id
        '''
        cursor.execute(q)

    logger.info('Migrating successful!')

def down(cursor, installed_version):
    pass


migrate = up