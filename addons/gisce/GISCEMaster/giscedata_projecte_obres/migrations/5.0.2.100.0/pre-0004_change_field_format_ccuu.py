# coding=utf-8
import logging
import pooler


def up(cursor, installed_version):
    logger = logging.getLogger('openerp.migration')

    logger.info('''
        Migrating type of field CCUU of the TIs and de detall economic.
    ''')

    models = {
        'giscedata_projecte_obra_detall_economic': 'ti_ccuu',
        'giscedata_projecte_obra_ti_at': 'ccuu',
        'giscedata_projecte_obra_ti_bt': 'ccuu',
        'giscedata_projecte_obra_ti_subestacions': 'ccuu',
        'giscedata_projecte_obra_ti_posicio': 'ccuu',
        'giscedata_projecte_obra_ti_transformador': 'ccuu',
        'giscedata_projecte_obra_ti_despatx': 'ccuu',
        'giscedata_projecte_obra_ti_celles': 'ccuu',
        'giscedata_projecte_obra_ti_cts': 'ccuu',
    }

    cursor.execute("""
        DROP VIEW IF EXISTS giscedata_projecte_obra_ti_view;
    """)
    errors = []
    for model, field in models.items():
        db = pooler.get_db(cursor.dbname)
        tmp_cursor = db.cursor()
        try:
            tmp_cursor.execute("""
                UPDATE
                    {model}
                SET
                    {field}= ti.id
                FROM (
                    SELECT id, name
                    FROM giscedata_tipus_installacio
                ) AS ti
                WHERE ti.name={field};
            """.format(model=model, field=field))
            tmp_cursor.execute("""
                ALTER TABLE {model} 
                ALTER COLUMN {field} 
                TYPE INTEGER USING {field}::INTEGER;
            """.format(model=model, field=field))
            tmp_cursor.commit()
        except Exception as e:
            tmp_cursor.rollback()
            errors.append(e.message)
        finally:
            tmp_cursor.close()

    if errors:
        logger.info('Migration NOT successful.\nErrors:\n{}.'.format('\n'.join(errors)))
    else:
        logger.info('Migration successful.')


def down(cursor, installed_version):
    pass


migrate = up
