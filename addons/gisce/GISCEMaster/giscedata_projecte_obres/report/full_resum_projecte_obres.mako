## -*- coding: utf-8 -*-
<%
    from datetime import datetime
%>

<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <head>
      <style>
            @font-face {
                font-family: "Roboto-Regular";
                src: url("${assets_path}/fonts/Roboto/Roboto-Regular.ttf") format('truetype');
                font-weight: normal;
            }
        ${css}
      </style>
      <link rel="stylesheet" href="${addons_path}/giscedata_projecte_obres/report/full_resum_projecte_obres.css"/>
    </head>
    <body>
    %for obra in objects:
        <%
            work_obj = pool.get('project.task.work')
            obra_obj = pool.get('giscedata.projecte.obra')

            setLang(obra.manager.company_id.partner_id.lang)

            type_label = ''
            if obra.work_type:
                work_types = obra_obj.get_work_types(cursor, uid, context=context)
                for work_type in work_types:
                    if work_type[0] == obra.work_type:
                        type_label = work_type[1]
                        break

            tasks = {}
            total_untaxed_amount = 0.0
            total_tasks_cost = 0.0

        %>
        <div>
            <table id="capcalera">
                <tr class="header_tr">
                    <td rowspan="5" id="logo_cell"><img id="logo" src="data:image/jpeg;base64,${obra.manager.company_id.logo}"></td>
                    <td class="header_center"><h5>${_(u"HOJA RESUMEN OBRA")}</h5></td>
                    <td class="header_right_label">${_(u"Fecha inicio")}</td>
                    <td class="header_right_value">${formatLang(obra.date_start, date=True) or ''}</td>
                </tr>
                <tr class="header_tr">
                    <td rowspan="3" class="header_center"><p>${_(u"Descripción:")} ${obra.descripcio or ''}</td>
                    <td class="header_right_label">${_(u"Fecha finalización")}</td>
                    <td class="header_right_value">${formatLang(obra.data_finalitzacio, date=True) or ''}</td>
                </tr>
                <tr class="header_tr">
                    <td class="header_right_label">${_(u"Referencia")}</td>
                    <td class="header_right_value">${obra.name or ''}</td>
                </tr>
                <tr class="header_tr">
                    <td class="header_right_label">${_(u"Coste planificado")}</td>
                    <td class="header_right_value">${formatLang(obra.planned_cost, monetary=True, digits=2)+'€' or ''}</td>
                </tr>
                <tr class="header_tr">
                    <td class="header_center"><p>${(obra.manager and obra.manager.company_id and obra.manager.company_id.name) or ''}<p></td>
                    <td class="header_right_label">${_(u"Tipo de trabajo")}</td>
                    <td class="header_right_value"><p>${_(type_label)}<p></td>
                </tr>
            </table>
        </div>
        % if not obra.tasks:
            <h6> No s'han trobat tasques associades </h6>
        % endif
        % for task in obra.tasks:
            <%
                tasks[task.name] = 0
                total_hours = 0.0
                total_cost = 0.00
                workers = {}
            %>
            <h6> ${task.name or ''} </h6>
            <table class="taula_dades">
                <thead>
                    <tr>
                        <th class="min">${_(u"Fecha")}</th>
                        <th class="min">${_(u"Tiempo dedicado (h)")}</th>
                        <th class="min">${_(u"Precio/hora (€)")}</th>
                        <th>${_(u"Resumen del trabajo")}</th>
                        <th style="width:20%">${_(u"Operario")}</th>
                    </tr>
                </thead>
                <tbody>
                     % if not task.work_ids:
                        <tr>
                            <td colspan="100%">${_(u"No se ha entrado ninguna hora para contabilizar")}</td>
                        </tr>
                    % endif
                    %for work in task.work_ids:
                        <%
                            total_hours += (work.hours or 0)
                            total_cost += work.hours * (work.hour_cost or 0)
                            tasks[task.name] = total_cost
                            if work.user_id:
                               worker_name = work.employee_id.name
                               if worker_name in workers:
                                   workers[worker_name]['hours'] += work.hours
                                   workers[worker_name]['amount'] += work.hours * (work.hour_cost or 0)
                               else:
                                   workers[worker_name] = {
                                       'hours': work.hours,
                                       'amount': work.hours * (work.hour_cost or 0)
                                   }
                        %>
                        <tr>
                            <td class="min">${work.date.split(' ')[0] or ''}</td>
                            <td class="min">${'{0:02.0f}:{1:02.0f}'.format(*divmod(work.hours * 60, 60))}</td>
                            <td class="min">${formatLang(work.hour_cost, monetary=True, digits=2) or 0.00}</td>
                            <td>${work.name or ''}</td>
                            <td style="width:20%">${work.employee_id.name or ''}</td>
                         </tr>
                    %endfor
                </tbody>
            </table>
            % if task.work_ids:
                <table class="taula_resum">
                    <thead>
                        <tr>
                            <th>${_(u"Operario")}</th>
                            <th class="cost">${_(u"Total horas")} (h)</th>
                            <th class="cost">${_(u"Coste total")} (€)</th>
                        </tr>
                    </thead>
                    <tbody>
                        %for key in workers:
                        <tr>
                            <td style="width:60%">${key}</td>
                            <td style="width:20%" class="cost">${'{0:02.0f}:{1:02.0f}'.format(*divmod(workers[key]['hours'] * 60, 60))}</td>
                            <td style="width:20%">${formatLang(workers[key]['amount'], monetary=True, digits=2)}</td>
                        </tr>
                        % endfor
                    </tbody>
                </table>
                <table id="total-horas">
                    <tr>
                        <th colspan="2" style="text-align:left">${_(u"Duración total de los trabajos")} </th>
                        <td colspan="100%" style="text-align:left">${'{0:02.0f}:{1:02.0f}'.format(*divmod(total_hours * 60, 60))}</td>
                        <th colspan="2" style="text-align:left">${_(u"Coste total de les horas")} </th>
                        <td colspan="100%" style="text-align:left">${formatLang(total_cost, monetary=True, digits=2)}€</td>
                    <tr>
                </table>
            %endif
        % endfor
        <h6> ${_(u"Materiales")} </h6>
        <table class="taula_dades">
            <thead>
                <tr>
                    <th style="width:50%">${_(u"Producto")}</th>
                    <th class="cost">${_(u"Cantidad")}</th>
                    <th class="cost">${_(u"Precio medio")} (€/u)</th>
                    <th class="cost">${_(u"Coste (€)")}</th>
                </tr>
            </thead>
            <tbody>
                % if not obra.llista_materials_ids:
                    <tr>
                        <td colspan="100%">${_(u"No se han encontrado materiales")}</td>
                    </tr>
                % endif
                % for material in obra.llista_materials_ids:
                    <%
                        product = material.product_id
                        quantitat = material.amount or 0
                        cost_material = material.price
                    %>
                    <tr>
                        % if product.code:
                            <td style="width:50%">${'[%s] %s' % (product.code, product.name or '')}</td>
                        % else:
                            <td style="width:50%">${product.name or ''}</td>
                        % endif
                        <td class="cost">${quantitat}</td>
                        %if cost_material > 0:
                            <td class="cost">${formatLang(cost_material/quantitat, monetary=True, digits=2) or 0.00}</td>
                        %endif
                        <td class="cost">${formatLang(cost_material, monetary=True, digits=2) or 0.00}</td>
                    </tr>
                % endfor
                <tr class="top_lined">
                    <td colspan="2" style="text-align:left">${_(u"Coste total de los materiales")}</td>
                    <td class="cost">${formatLang(obra.stock_subtotal, monetary=True, digits=2)}€</td>
                <tr>
             </tbody>
        </table>
        <h6> ${_(u"Facturas de proveedores")} </h6>
        <table class="taula_dades">
            <thead>
                <tr>
                    <th class="min">${_(u"Nº de factura")}</th>
                    <th>${_(u"Origen")}</th>
                    <th>${_(u"Proveedor")}</th>
                    <th style="width:25%">${_(u"Descripción")}</th>
                    <th class="min">${_(u"Fecha fra. proveedor")}</th>
                    <th class="cost">${_(u"Subtotal (€)")}</th>
                </tr>
            </thead>
            <tbody>
                 % if not obra.supplier_invoices:
                    <tr>
                        <td colspan="100%">${_(u"No se han encontrado facturas")}</td>
                    </tr>
                % endif
                % for invoice in obra.supplier_invoices:
                    <% total_untaxed_amount += invoice.amount_untaxed %>
                    <tr>
                        <td class="min">${invoice.number or ''}</td>
                        <td>${invoice.origin or _(u"Sin num.")}</td>
                        <td>${invoice.partner_id and invoice.partner_id.name or _(u"Proveedor no informado")}</td>
                        <td style="width:25%">${invoice.name or ''}</td>
                        <td>${invoice.origin_date_invoice or ''}</td>
                        <td class="cost">${formatLang(invoice.amount_untaxed, monetary=True, digits=2) or 0.00}</td>
                    </tr>
                % endfor
                <tr class="top_lined">
                    <td colspan="5" style="text-align:left">${_(u"Importe total de las facturas:")}</td>
                    <td class="cost">${formatLang(total_untaxed_amount, monetary=True, digits=2)}€</td>
                <tr>
             </tbody>
        </table>
        <h6> ${_(u"Resumen de los datos")} </h6>
        <table class="taula_resum">
            <tr>
                <td style="width:70%">${_(u"Tareas:")}</td>
            </tr>
            % for task in obra.tasks:
                <% total_tasks_cost += (tasks[task.name] or 0.0) %>
                <tr>
                    <td style="width:70%">${'&emsp;%s' % (task.name or '')}</td>
                    <td style="width:30%" class="cost">${formatLang(tasks[task.name], monetary=True, digits=2)}€</td>
                </tr>
            % endfor
            <tr>
                <td style="width:70%">${_(u"Coste total de materiales")}</td>
                <td style="width:30%" class="cost">${formatLang(obra.stock_subtotal, monetary=True, digits=2)}€</td>
            </tr>
            <tr>
                <td style="width:70%">${_(u"Facturas proveedores:")}</td>
                <td style="width:30%" class="cost">${formatLang(total_untaxed_amount, monetary=True, digits=2)}€</td>
            </tr>
            <tr class="top_lined">
                <td style="width:70%">${_(u"Total:")}</td>
                <td style="width:30%" class="cost">${formatLang((obra.stock_subtotal + total_tasks_cost + total_untaxed_amount), monetary=True, digits=2)}€</td>
            <tr>
        </table>
    % endfor
    </body>
</html>
