# -*- coding: utf-8 -*-
from c2c_webkit_report import webkit_report
from report import report_sxw

webkit_report.WebKitParser(
    'report.giscedata.projecte.obra.full_resum',
    'giscedata.projecte.obra',
    'giscedata_projecte_obres/report/full_resum_projecte_obres.mako',
    parser=report_sxw.rml_parse
)
