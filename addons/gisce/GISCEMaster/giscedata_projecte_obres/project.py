# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields
from tools.translate import _


class ProjectProject(osv.osv):

    _name = 'project.project'
    _inherit = 'project.project'

    def set_open(self, cursor, uid, ids, context=None):
        """
        We want to open each task for all projects that have a obra associated
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param ids: <project.project> ids.
        :type ids: list[long]
        :param context:
        :return:
        """

        super(ProjectProject, self).set_open(
            cursor, uid, ids, context=context
        )

        # Activates the tasks for the project that have an obra associated with
        obra_q = self.pool.get('giscedata.projecte.obra').q(cursor, uid)
        task_obj = self.pool.get('project.task')

        obres_vals = obra_q.read(['project_id']).where([
            ('project_id', 'in', ids)
        ])

        project_ids = map(lambda o: o['project_id'], obres_vals)

        for obra in self.read(cursor, uid, project_ids, ['task']):
            task_ids = obra.get('task', [])
            if task_ids:
                task_obj.do_open(cursor, uid, task_ids)

        return True


ProjectProject()


class ProjectTask(osv.osv):

    _name = 'project.task'
    _inherit = 'project.task'

    def create(self, cursor, uid, vals, context=None):
        if context is None:
            context = {}
        if 'project_id' in vals:
            project_id = vals['project_id']
            obra_obj = self.pool.get('giscedata.projecte.obra')
            if obra_obj.search(
                cursor, uid, [
                    ('project_id', '=', project_id),
                    ('state', '=', 'done')
                ], context={'active_test': False}
            ):
                raise osv.except_osv(
                    _('Obra Finalizada'),
                    _(
                        'No se puede crear una nueva tarea para '
                        'una obra finalizada'
                    )
                )
        return super(ProjectTask, self).create(
            cursor, uid, vals, context=context)


ProjectTask()


class ProjectTaskWork(osv.osv):

    _name = 'project.task.work'
    _inherit = 'project.task.work'

    # DEFAULTS

    def get_tipos_hora(self, cursor, uid, context=None):
        return [
            ('normal', _('Normal')),
            ('festiva', _('Festivas')),
            ('nocturna', _('Nocturnas')),
        ]

    def _default_task_id(self, cursor, uid, context=None):
        return context.get('task_id', False)

    def _ff_obra_id(self, cursor, uid, ids, field_name, arg, context=None):
        if not context:
            context = {}
        task_obj = self.pool.get('project.task')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        res = {}
        task_vs = task_obj.read(
            cursor, uid, ids, ['project_id', 'work_ids'], context=context)
        for task in task_vs:
            if task['project_id']:
                obra_id = obra_obj.search(cursor, uid, [
                    ('project_id', '=', task['project_id'][0])
                ])
                obra_id = obra_id[0] if obra_id else False
                for wkd in task['work_ids']:
                    res[wkd] = obra_id
        return res

    def create(self, cursor, uid, vals, context=None):
        """
        Associates a obra with the task when this is created
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param vals: vals to create the task
        :type vals: dict
        :param context: None
        :return: *create* method return
        """
        task_obj = self.pool.get('project.task')
        obra_obj = self.pool.get('giscedata.projecte.obra')
        task_id = vals['task_id']
        project_id = task_obj.read(
            cursor, uid, task_id, ['project_id']
        )['project_id']

        obra_id = False
        if project_id:
            obra_id = obra_obj.search(cursor, uid, [
                ('project_id', '=', project_id[0])
            ])
            if obra_id:
                obra_id = obra_id[0]
        vals.update({
            'obra_id': obra_id
        })
        return super(ProjectTaskWork, self).create(
            cursor, uid, vals, context=context
        )

    _columns = {
        'hour_cost': fields.float('Precio/hora', required=True,
                                  digits=(16, 6)),
        'type_id': fields.selection(get_tipos_hora, 'Tipo de hora'),
        'obra_id': fields.function(
            _ff_obra_id, type='many2one', string='Obra', readonly=True,
            method=True, obj='giscedata.projecte.obra', store={
                'project.task': (
                    lambda self, cr, uid, ids, c=None: ids, ['project_id'], 10
                ),
            }, ondelete='cascade'
        ),
        'product_id': fields.many2one('product.product',
                                      'Producto de Mano de Obra'),
        'employee_id': fields.many2one('hr.employee', 'Hecho por')
    }

    _defaults = {
        'hour_cost': lambda *args: 0.0,
        'task_id': _default_task_id
    }


ProjectTaskWork()
