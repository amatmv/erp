# -*- coding: utf-8 -*-
{
    "name": "GISCE Módulo de Obras",
    "description": """
        Mòdul dels Projectes d'Obres:
        - Afegeix codi CECO per Obres de Manteniment.
        - Afegeix codi INS per circular 4/2015.
        Projectes d'Obres amb Factures proveïdor:
        - Afegeix la dependencia a account
        - Permet assignar factures de proveïdor com a costos a una OT
        Projectes d'Obres amb Materials:
        - Afegeix productes en una Obra.
        - Afegeix moviments de stock de productes en una Obra.
        Projectes d'Obres amb gestió de tasques:
        - Afegeix el càlcul d'hores per Obres.
        - Afegeix enllaços i vistes per la gestió de les tasques del projecte d'Obres
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends": [
        "account",
        "product",
        "product_fabricant",
        "product_caracteristiques",
        "giscedata_stock",
        "giscedata_purchase",
        "giscedata_sale",
        "hr",
        "account",
        "project",
        "giscedata_at",
        "giscedata_bt",
        "giscedata_cts",
        "giscedata_transformadors",
        "giscedata_celles",
        "giscedata_despatx",
        "giscedata_cts_subestacions",
    ],
    "init_xml": [
        "product_data.xml",
    ],
    "demo_xml": [
        "giscedata_projecte_obres_demo.xml",
        "product_demo.xml",
        "purchase_demo.xml",
        "hr_demo.xml",
        "stock_demo.xml",
        "sale_demo.xml",
    ],
    "update_xml": [
        "wizard/wizard_input_hours_view.xml",
        "wizard/wizard_obra_report_excel_export_view.xml",
        "wizard/wizard_crear_ti_view.xml",
        "project_view.xml",
        "sale_view.xml",
        "purchase_view.xml",
        "hr_view.xml",
        "invoice_view.xml",
        "stock_view.xml",
        "giscedata_projecte_obres_view.xml",
        "giscedata_projecte_obres_report.xml",
        "giscedata_projecte_obres_data.xml",
        "giscedata_obra_ti_view.xml",
        "giscedata_despatx_view.xml",
        "giscedata_bt_view.xml",
        "giscedata_at_view.xml",
        "giscedata_celles_view.xml",
        "giscedata_cts_view.xml",
        "giscedata_cts_subestacions_view.xml",
        "giscedata_transformadors_view.xml",
        "security/giscedata_projecte_obres_security.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
