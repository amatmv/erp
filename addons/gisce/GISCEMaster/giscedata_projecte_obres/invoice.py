# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields
from tools.translate import _


class AccountInvoice(osv.osv):

    _name = 'account.invoice'
    _inherit = 'account.invoice'

    _columns = {
        'obra_ids': fields.many2many(
            'giscedata.projecte.obra', 'giscedata_ot_supplier_invoices_rel',
            'invoice_id', 'ot_id', 'Obras'
        ),
    }

    def onchange_type(self, cursor, uid, ids, invoice_type):
        for invoice_id in ids:
            curr_inv_type = self.read(cursor, uid, invoice_id, ['type', 'state'])
            if curr_inv_type['state'] != 'draft':
                return {
                    'warning': {
                        'title': _('Atención'),
                        'message': _('No se puede cambiar el tipo en una factura '
                                     'que no está en borrador.')
                    }
                }
            if curr_inv_type and curr_inv_type['type']:
                if 'out_' in curr_inv_type['type'] and not 'out_' in invoice_type:
                    return {
                        'warning': {
                            'title': _('Atención'),
                            'message': _('La factura debe ser de cliente.')
                        }
                    }
                elif 'in_' in curr_inv_type['type'] and not 'in_' in invoice_type:
                    return {
                        'warning': {
                            'title': _('Atención'),
                            'message': _('La factura debe ser de proveedor.')
                        }
                    }


AccountInvoice()
