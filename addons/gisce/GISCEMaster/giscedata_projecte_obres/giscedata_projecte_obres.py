# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from datetime import datetime

from osv import osv, fields
from tools.translate import _
import netsvc

from .defs import TABLA_CECO, merge_code_in_name

from collections import Counter
from itertools import chain
import traceback


def get_codigos_ceco():
    return merge_code_in_name(TABLA_CECO)


def understandable_exceptions(msg=False):
    """
    Decorator to show a understandable mesage whenever a uncontrolled exception
    is raised.
    The real exception will be shown to the log server.
    :param msg: message to show
    :return:
    """
    def understandable_exceptions_decorator(f):
        def func_wrapper(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                # The exception will be raised if it's a ORM one
                if issubclass(type(e), osv.orm.except_orm) \
                        or issubclass(type(e), osv.except_osv):
                    raise e

                if msg is False:
                    except_msg = _('Error desconocido')
                else:
                    except_msg = msg
                traceback.print_exc()
                raise osv.except_osv(_('Error'), except_msg)
        return func_wrapper
    return understandable_exceptions_decorator


class GiscedataProjecteObra(osv.osv):

    _name = "giscedata.projecte.obra"
    _inherits = {"project.project": 'project_id'}
    _description = "Projecte d'Obra"
    _prefetch = False

    def create_obra_unique_constraint_invoice(self, cursor, *args):
        """
        Create a unique constraint between the table account_invoice and
        giscedata_projecte_obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        """

        logger = netsvc.Logger()

        cursor.execute("""
            SELECT con.conname
            FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp ON nsp.oid = connamespace
            WHERE conname = 'giscedata_ot_supplier_invoices_rel_unique_ot_invoice';
        """)

        if not cursor.rowcount:
            logger.notifyChannel(
                'init', netsvc.LOG_INFO,
                "Creating UNIQUE constraint between obres and account_invoice"
            )
            unique_query = """
                ALTER TABLE giscedata_ot_supplier_invoices_rel
                ADD CONSTRAINT giscedata_ot_supplier_invoices_rel_unique_ot_invoice
                UNIQUE (ot_id, invoice_id);
            """
            cursor.execute(unique_query)

    def init(self, cursor):
        self.create_obra_unique_constraint_invoice(cursor)
        return True

    @staticmethod
    def obres_models_correspondence(key='', reverse=False):
        correspondencia = {
            'giscedata.at.tram': 'giscedata.projecte.obra.ti.at',
            'giscedata.bt.element': 'giscedata.projecte.obra.ti.bt',
            'giscedata.cts.subestacions': 'giscedata.projecte.obra.ti.subestacions',
            'giscedata.despatx': 'giscedata.projecte.obra.ti.despatx',
            'giscedata.cts.subestacions.posicio': 'giscedata.projecte.obra.ti.posicio',
            'giscedata.transformador.trafo': 'giscedata.projecte.obra.ti.transformador',
            'giscedata.celles.cella': 'giscedata.projecte.obra.ti.celles',
            'giscedata.cts': 'giscedata.projecte.obra.ti.cts',
        }
        if reverse:
            res = next(
                real_model for (real_model, obra_model) in correspondencia.items()
                if obra_model == key
            )
        else:
            res = correspondencia.get(key, False)
        return res

    @staticmethod
    def search_elements(inst, cursor, uid, args):
        """
        Maps a list of search params (args) and enables them to be used to
        search the elements (LAT, LBT,...) that have an certain obra associatad.
        :param inst: element (LAT, LBT, ...) instance
        :type inst: T
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param args: the initial arguments
        :type args: list[tuple[str, str, str|int]]
        :return: the arguments
        :rtype: list[tuple[str, str, str|int]]
        """
        new_args = args[:]
        for idx, arg in enumerate(new_args):
            if len(arg) == 3:
                field, operator, match = arg
                if field == 'ti_obres_ids' and isinstance(match, (unicode, str)):
                    model_correspondence = GiscedataProjecteObra.obres_models_correspondence(inst._name)
                    ti_at_obj = inst.pool.get(model_correspondence)

                    search_params = [(
                        ('obra_id.name', operator, match)
                    )]
                    ti_at_ids = ti_at_obj.search(
                        cursor, uid, search_params,
                        context={'active_test': False}
                    )

                    if ti_at_ids:
                        operator, match = 'in', list(set(ti_at_ids))
                    else:
                        field, operator, match = 'id', '=', 0

                    new_args[idx] = (field, operator, match)
        return new_args

    def _get_base_ids(self, cursor, uid, ids, context=None):
        """
        Given a list of obres, returns the list of projects associated with
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param context: OpenERP Context
        :return: <project.project> ids
        :rtype list[long]
        """
        if context is None:
            context = {}
        sql = """
            SELECT project_id
            FROM giscedata_projecte_obra
            WHERE id in %s
            ORDER BY project_id
        """
        cursor.execute(sql, (tuple(ids),))
        res = cursor.fetchall()
        return list(chain.from_iterable(res))

    def create_default_task(self, cursor, uid, ids, context=None):
        """
        Creates a task with default values and associates it with the obra.
        The task will be only created if there isn't one.

        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param context: {}
        :return: None
        :rtype: NoneType
        """
        if context is None:
            context = {}
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        task_obj = self.pool.get('project.task')
        task_fields = task_obj.fields_get(cursor, uid)
        default_vals = task_obj.default_get(cursor, uid, task_fields)

        obra_vals = self.read(cursor, uid, ids, ['name', 'descripcio', 'tasks'])
        for vals in obra_vals:
            if not vals['tasks']:
                obra_name = '[{number}] {name}'.format(**{
                    'number': vals.get('name', '') or '',
                    'name': vals.get('descripcio', '') or '',
                })
                obra_project_id = self.read(
                    cursor, uid, vals['id'], ['project_id'])['project_id']

                company_id = self.pool.get('res.users').get_company(cursor, uid)
                default_vals.update({
                    'obra_id': vals['id'],
                    'name': obra_name,
                    'planned_hours': 0,
                    'project_id': obra_project_id[0],
                    'date_start': datetime.now().strftime('%Y-%m-%d'),
                    'partner_id': company_id
                })

                # We create the task and we link it to the obra
                self.write(cursor, uid, vals['id'], {
                    'tasks': [(0, 0, default_vals)],
                })

        return None

    @understandable_exceptions(_('Error cambiando el estado a abierto'))
    def set_reopen(self, cursor, uid, ids, context=None):
        proj_obj = self.pool.get('project.project')
        project_ids = self._get_base_ids(cursor, uid, ids, context=context)
        return proj_obj.write(cursor, uid, project_ids, {'state': 'open'},
                              context=context)

    @understandable_exceptions(_('Error cambiando el estado a borrador'))
    def set_template(self, cursor, uid, ids, context=None):
        proj_obj = self.pool.get('project.project')
        ids = self._get_base_ids(cursor, uid, ids, context=context)
        return proj_obj.set_template(cursor, uid, ids, context)

    @understandable_exceptions(_('Error cambiando el estado a abierto'))
    def set_open(self, cursor, uid, ids, context=None):
        proj_obj = self.pool.get('project.project')
        self.create_default_task(cursor, uid, ids, context=context)
        project_ids = self._get_base_ids(cursor, uid, ids, context=context)
        return proj_obj.set_open(cursor, uid, project_ids, context)

    @understandable_exceptions(_('Error cambiando el estado a pendiente'))
    def set_pending(self, cursor, uid, ids, context=None):
        proj_obj = self.pool.get('project.project')
        project_ids = self._get_base_ids(cursor, uid, ids, context=context)
        return proj_obj.set_pending(cursor, uid, project_ids, context)

    @understandable_exceptions(_('Error cambiando el estado a cancelada'))
    def set_cancel(self, cursor, uid, ids, context=None):
        proj_obj = self.pool.get('project.project')
        project_ids = self._get_base_ids(cursor, uid, ids, context=context)
        return proj_obj.set_cancel(cursor, uid, project_ids, context)

    @understandable_exceptions(_('Error cambiando el estado a realizado'))
    def set_done(self, cursor, uid, ids, context=None):
        proj_obj = self.pool.get('project.project')
        project_ids = self._get_base_ids(cursor, uid, ids, context=context)
        return proj_obj.set_done(cursor, uid, project_ids, context)

    # Fields Function

    def _compute_obra_total_price(self, cursor, uid, ids, obra_f, context=None):
        """
        Obtains the total cost of the obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param obra_f: obra's fields to be contemplated when calculating the
        total cost.
        :type obra_f: [str]
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the obra id and the value is the
        total cost of it.
        :rtype: dict[long, float]
        """
        res = dict.fromkeys(ids, 0.0)
        obra_vs = self.read(cursor, uid, ids, obra_f, context=context)

        for obra_v in obra_vs:
            obra_id = obra_v['id']
            for f in obra_f:
                res[obra_id] += obra_v[f]

        return res

    def _cost_limits(self, cursor, uid, ids, field_names, arg, context=None):
        """
        Obtains the values for the fields 'cost_over_limit' and 'cost_over_half'
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_names: name of the fields that triggers the function.
        :type field_names: list[str]
        :param arg: ??
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the obra id and the value is a
        dict containing the values of those fields.
        :rtype: dict[long, dict[str, bool]]
        """
        res = {}
        for id in ids:
            res[id] = {}.fromkeys(field_names, False)
        obra_f = ['obra_subtotal', 'planned_cost']
        obra_vals = self.read(cursor, uid, ids, obra_f, context=context)
        for obra in obra_vals:
            percent_50 = obra['planned_cost'] * 50.0 / 100.0
            if obra['planned_cost'] > 0.0:
                if obra['obra_subtotal'] > obra['planned_cost']:
                    res[obra['id']]['cost_over_limit'] = True

                if obra['obra_subtotal'] >= percent_50:
                    res[obra['id']]['cost_over_half'] = True

        return res

    def get_inst_ids(self, cursor, uid, obra_id, obra_ti_field):
        """
        Obtains the list of the element_ti_ids of the TI given the relational
        field of the obra TI.

        E.g. for the case of the field 'lat_ids', the relation is the
        <giscedata.projecte.obra.ti.at>, whose values of the element_ti_id
        field is a <giscedata.at.tram> id.
        So if the value of the obra_ti_field param is 'lat_ids', this method
        returns a list of the element_ti_id associated with them.

        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :param obra_id: the <giscedata.projecte.obra> id
        :type obra_id: long
        :param obra_ti_field: the field <giscedata.projecte.obra> that has the
        relation with the <giscedata.projecte.obra.ti>
        :type obra_ti_field: str
        :return: the element_ti_id ids
        :rtype: list
        """
        if isinstance(obra_id, (tuple, list)):
            obra_id = obra_id[0]
        res = []
        object_associated = self._columns[obra_ti_field]._obj
        obj = self.pool.get(object_associated)
        ti_ids = self.read(cursor, uid, obra_id, [obra_ti_field])
        if ti_ids[obra_ti_field]:
            ti_vals = obj.read(cursor, uid, ti_ids[obra_ti_field],
                               ['element_ti_id'])
            res = filter(None, [
                ti_v['element_ti_id'][0] if ti_v['element_ti_id'] else None
                for ti_v in ti_vals
            ])
        return res

    def _fnct_get_instalaciones(self, cursor, uid, ids, field_names, arg,
                                context=None):
        """
        Obtains the ids of the ids of the elements associated with the obra
        lines.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_names: list of the name of the field that triggers the
        function.
        :type field_names: list
        :param arg: ??
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the obta TI id and the value is a
        list containing the elements to which is associated with.
        :rtype: dict[long, float]
        """

        res = dict.fromkeys(ids, dict())
        obra_ti_fields = {
            'instalaciones_at_ids': 'lat_ids',
            'instalaciones_bt_ids': 'lbt_ids',
            'instalaciones_se_ids': 'se_ids',
            'instalaciones_pos_ids': 'pos_ids',
            'instalaciones_trafo_ids': 'maq_ids',
            'instalaciones_des_ids': 'des_ids',
            'instalaciones_celles_ids': 'fia_ids',
            'instalaciones_cts_ids': 'ct_ids',
        }
        for obra_id, instalaciones_vals in res.items():
            for inst_field, obra_ti_field in obra_ti_fields.items():
                inst_ids = self.get_inst_ids(cursor, uid, obra_id,
                                             obra_ti_field)
                instalaciones_vals[inst_field] = inst_ids

        return res

    def _fnct_move_from_stock_ids(self, cursor, uid, ids, field_name, args,
                                  context=False):
        """
        Obtains all the stock moves related to the pickings that sends products
        from the warehouses.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids
        :param ids: list[long]
        :param field_name: field name that triggers the function.
        :type field_name: str
        :param args: ??
        :param context: OpenERP context.
        :type context: {}
        :return: <stock.move> ids.
        :rtype: list[long]
        """
        picking_o = self.pool.get('stock.picking')
        picking_f = ['move_lines']
        obra_f = ['picking_from_stock_ids']
        obra_vs = self.read(cursor, uid, ids, obra_f, context=context)
        res = {}

        for obra_v in obra_vs:
            obra_id = obra_v['id']
            picking_ids = obra_v['picking_from_stock_ids']
            picking_vs = picking_o.read(
                cursor, uid, picking_ids, picking_f, context=context
            )
            move_ids = []
            for picking_v in picking_vs:
                move_ids += picking_v['move_lines']

            res[obra_id] = move_ids

        return res

    def _fnct_supplier_invoices_subtotal(self, cursor, uid, ids, field_name,
                                         args, context=None):
        """
        Calculate the amount total of the invoices to save it in the obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> of the user executing the method.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name:
        :type field_name: list[str]
        :param args: ?
        :param context: OpenERP context
        :type context: {}
        :return: Dictionary where the key is the <giscedata.projecte.obra> id
        and the value is the amount total of the invoices in that obra.
        :rtype: dict[long, int]
        """
        if context is None:
            context = {}
        res = Counter(dict.fromkeys(ids, 0))
        q = self.q(cursor, uid)
        sql = q.select(['id', 'supplier_invoices.amount_total']).where([
            ('id', 'in', ids)
        ])
        cursor.execute(*sql)
        for result in cursor.dictfetchall():
            res[result['id']] += result['supplier_invoices.amount_total'] or 0.0
        return res

    def _fnct_supplier_invoices_subtotal_no_taxes(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Calculate the amount total without taxes of the invoices to save it in
        the obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> of the user executing the method.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name:
        :type field_name: list[str]
        :param args: ?
        :param context: OpenERP context
        :type context: {}
        :return: Dictionary where the key is the <giscedata.projecte.obra> id
        and the value is the amount total of the invoices in that obra.
        :rtype: dict[long, int]
        """
        if context is None:
            context = {}
        res = Counter(dict.fromkeys(ids, 0))
        q = self.q(cursor, uid)
        sql = q.select(['id', 'supplier_invoices.amount_untaxed']).where([
            ('id', 'in', ids)
        ])
        cursor.execute(*sql)
        for result in cursor.dictfetchall():
            res[result['id']] += result['supplier_invoices.amount_untaxed'] or 0.0
        return res

    def _ff_ot_task_price(self, cursor, uid, ids, field_name, arg, context=None):

        if context is None:
            context = {}

        work_obj = self.pool.get('project.task.work')

        cols = ['project_workdone_ids']
        cols_work = ['user_id', 'hours', 'hour_cost']
        res = {}

        for obra_data in self.read(cursor, uid, ids, cols, context=context):
            obra_id = obra_data['id']
            workdone_ids = obra_data['project_workdone_ids']
            res[obra_id] = 0.0

            tasks_works = work_obj.read(
                cursor, uid, workdone_ids, cols_work, context=context
            )

            res[obra_id] = sum([w['hours'] * w['hour_cost'] for w in tasks_works])

        return res

    def _fnct_picking_from_stock_ids(self, cursor, uid, ids, field_name, args, context=None):
        """
        Obtains the pickings related to the obra that are sent from the stock
        locations.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name: name of the field that triggers this function.
        :type field_name: str
        :param args: ??
        :param context: OpenERP context.
        :type context: dict
        :return: dict[long, list[long]]
        """
        res = {}

        picking_o = self.pool.get('stock.picking')
        stock_location_ids = self.get_stock_locations(cursor, uid, context=context)
        for obra_id in ids:
            dmn = [
                ('obra_id', 'in', ids),
                '|',
                ('location_id', 'in', stock_location_ids),
                '&',
                ('sale_id.location_id', 'in', stock_location_ids),
                ('return', '=', False)
            ]
            picking_ids = picking_o.search(cursor, uid, dmn, context=context)
            res[obra_id] = picking_ids

        return res

    def _ff_out_stock_ids(self, cursor, uid, ids, field_name, args, context=None):
        """
        Obtains the purchase data related to the obra. Its purchases will be
        restricted to the field that calls the function: if called
        from 'order_to_stock_ids' then it will obtain the purchases
        whose final location is one of the locations configured under the stock
        location of all the warehouses. if called from
        'order_to_stock_ids' then it will obtain the purcahses whose
        final location is NOT one of the locations previously described.

        The same will be applied to the pickings and its moves.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid:
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name: Fields names that calls the function. Allowed values:
        order_to_stock_ids, order_not_from_stock_ids,
        picking_to_stock_ids, picking_not_from_stock_ids,
        move_to_stock_ids and move_not_from_stock_ids
        :type field_name: list[str]
        :param args: ?
        :param context: OpenERP context
        :type context: dict
        :return: Dictionary where the key is the <giscedata.projecte.obra> id
        and the value is a Dictionary where the keys are the 'field_name'
        parameter values and its value is a list that contains the
        <purchase.order> ids.
        :rtype: dict[long, dict[str, list[long]]]
        """
        move_o = self.pool.get('stock.move')
        picking_o = self.pool.get('stock.picking')
        purchase_o = self.pool.get('purchase.order')

        stock_location_ids = self.get_stock_locations(cursor, uid, context=context)

        res = {}

        for obra_id in ids:
            value = {}
            for field_n in field_name:
                if 'order' in field_n:
                    if field_n == 'order_to_stock_ids':
                        location_dmn = 'in'
                    elif field_n == 'order_not_from_stock_ids':
                        location_dmn = 'not in'
                    else:
                        raise NotImplementedError

                    dmn = [
                        ('ot_in_id', '=', obra_id),
                        ('location_id', location_dmn, stock_location_ids)
                    ]
                    stock_o = purchase_o

                elif 'picking' in field_n:
                    dmn = [('obra_id', '=', obra_id)]
                    if field_n == 'picking_not_from_stock_ids':
                        dmn += [
                            '|',
                            '&',
                            ('location_id', 'not in', stock_location_ids),
                            ('location_dest_id', 'not in', stock_location_ids),
                            ('purchase_id.location_id', 'not in', stock_location_ids)
                        ]
                    elif field_n == 'picking_to_stock_ids':
                        dmn += [
                            '|',
                            '|',
                            ('location_dest_id', 'in', stock_location_ids),
                            ('purchase_id.location_id', 'in', stock_location_ids),
                            '&',
                            ('sale_id.location_id', 'in', stock_location_ids),
                            ('return', '=', True)
                        ]
                    stock_o = picking_o

                elif 'move' in field_n:
                    picking_field_name = field_n.replace('move', 'picking')
                    picking_f = [picking_field_name]
                    obra_v = self.read(cursor, uid, obra_id, picking_f, context=context)
                    picking_ids = obra_v[picking_field_name]
                    dmn = [('picking_id', 'in', picking_ids)]
                    stock_o = move_o

                else:
                    raise NotImplementedError

                field_ids = stock_o.search(cursor, uid, dmn, context=context)
                value[field_n] = field_ids

            res[obra_id] = value

        return res

    def _fnct_inv_picking(self, cursor, uid, obj_id, name, value, arg, context=None):
        """
        Writes/Updates/Unlinks the purchase order or the stock picking field
        function in function of the field that calls it.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :param obj_id: <giscedata.projecte.obra> id from wich the purchase order
        or picking is being modified.  Allowed values:
        order_to_stock_ids, order_not_from_stock_ids,
        picking_to_stock_ids and picking_not_from_stock_ids.
        :type obj_id: long
        :param name: Field name that calls the function.
        :type name: str
        :param value: one2many values to write/update/unlink.
        :param arg: unused parameter.
        :type arg: ?
        :param context: OpenERP context.
        :type context: dict
        """
        if 'order' in name:
            model_name = 'purchase.order'
        elif 'picking' in name:
            model_name = 'stock.picking'
        else:
            raise NotImplementedError

        model_o = self.pool.get(model_name)
        o2m = fields.one2many(model_name, '')
        obj_id = 'giscedata.projecte.obra,{}'.format(obj_id)
        o2m.set(cursor, model_o, obj_id, name, value, user=uid, context=context)

    def _fnct_stock_subtotal(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Obtains the total cost of the material used in the obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name: name of the field that triggers the function.
        :type field_name: str
        :param arg: ??
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the obra id and the value is the
        cost of the material used in the obra.
        :rtype: dict[long, float]
        """
        resum_stock_o = self.pool.get('giscedata.projecte.obres.resum.stock')

        res = dict.fromkeys(ids, 0)

        obra_f = ['llista_materials_ids']
        obra_vs = self.read(cursor, uid, ids, obra_f, context=context)

        for obra_v in obra_vs:
            obra_id = obra_v['id']

            resum_stock_ids = obra_v['llista_materials_ids']
            resum_stock_f = ['price']
            resum_stock_vs = resum_stock_o.read(
                cursor, uid, resum_stock_ids, resum_stock_f, context=context
            )
            for resum_stock_v in resum_stock_vs:
                res[obra_id] += resum_stock_v['price']

        return res

    def _obra_total_price(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Obtains the total cost of the obra in function of the task, supplier
        invoices and stock subtotal.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name: name of the field that triggers the function.
        :type field_name: str
        :param arg: ??
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the obra id and the value is the
        total cost of it.
        :rtype: dict[long, float]
        """
        obra_f = ['task_subtotal', 'supplier_invoices_subtotal',
                  'stock_subtotal']
        res = self._compute_obra_total_price(cursor, uid, ids, obra_f, context=context)
        return res

    def _obra_total_price_no_taxes(self, cursor, uid, ids, field_name, arg, context=None):
        """
        Obtains the total cost of the obra in function of the task, supplier
        invoices and stock subtotal.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> ids.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param field_name: name of the field that triggers the function.
        :type field_name: str
        :param arg: ??
        :param context: OpenERP context.
        :type context: dict
        :return: Dictionary where the key is the obra id and the value is the
        total cost of it.
        :rtype: dict[long, float]
        """
        obra_f = ['task_subtotal', 'supplier_invoices_subtotal_no_taxes',
                  'stock_subtotal']
        res = self._compute_obra_total_price(cursor, uid, ids, obra_f, context=context)
        return res

    # Class methods

    def create(self, cursor, uid, vals, context=None):
        """
        Creates a project and a task when the obra is created
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param vals: vals for the new obra
        :param vals: dict
        :param context: OpenERP context
        :return: None
        :rtype: NoneType
        """
        vals = vals.copy()

        users_obj = self.pool.get('res.users')
        imd_obj = self.pool.get('ir.model.data')

        company_id = users_obj.get_company(cursor, uid)
        __, obra_dflt_project = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_projecte_obres', 'parent_project_obres')

        vals.update({
            'parent_id': obra_dflt_project,
            'date_start': datetime.today().strftime('%Y-%m-%d'),
            'manager': uid,
            'partner_id': company_id
        })

        obra_id = super(GiscedataProjecteObra, self).create(
            cursor, uid, vals, context=context)

        return obra_id

    def unlink(self, cursor, uid, ids, context=None):
        """
        Erases all tasks associated with the obra and the project
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param context: OpenERP Context
        :return: True
        :rtype: bool
        """
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        if context is None:
            context = {}
        task_obj = self.pool.get('project.task')
        proj_obj = self.pool.get('project.project')

        obra_fields = ['tasks', 'project_id']
        for obra in self.read(cursor, uid, ids, obra_fields):
            for task in obra['tasks']:
                task_obj.write(cursor, uid, task, {'active': False})
            proj_obj.unlink(cursor, uid, [obra['project_id'][0]])

        return super(GiscedataProjecteObra, self).unlink(
            cursor, uid, ids, context=context)

    def get_work_types(self, cursor, uid, context=None):
        return [
            ('inversions', _('Inversiones')),
            ('manteniment', _('Mantenimiento')),
        ]

    def sync_detall_economic(self, cursor, uid, ids, context=None):
        """
        Distributes the amounts of the <giscedata.projecte.obra.detall.economic>
        object with the obra TIs that have the same CCUU code in base of a
        certain criteria:
        - The criteria of the AT and BT lines is the length it (in kms).
        - The criteria for the rest are the unities.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param context: OpenERP context
        :type context: dict
        :return: None
        """
        if context is None:
            context = {}

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        ti_view_obj = self.pool.get('giscedata.projecte.obra.ti.view')
        detall_obj = self.pool.get('giscedata.projecte.obra.detall.economic')

        for obra_id in ids:
            detall_ids = detall_obj.search(cursor, uid, [
                ('obra_id', '=', obra_id),
                ('sincronizable', '=', True)
            ])
            detall_fields = [
                'im_ingenieria', 'im_trabajos', 'im_obracivil', 'im_materiales',
                'im_total', 'ti_ccuu', 'obra_id', 'financiado',
                'tipo_inversion', 'codi_ins'
            ]
            for detall in detall_obj.read(cursor, uid, detall_ids,
                                          detall_fields):
                if not detall['ti_ccuu'] or not detall['obra_id']:
                    continue
                # Primer busquem els TIs que tinguin el mateix ccuu que la linia
                # de detall
                tis = ti_view_obj.search(cursor, uid, [
                    ('ccuu', '=', detall['ti_ccuu'][0]),
                    ('synced', '=', True),
                    ('obra_id', '=', detall['obra_id'][0])
                ])

                # Per a cada TI, obtenim el model al qual esta associat
                tis = ti_view_obj.read(cursor, uid, tis,
                                       ['ref_element_ti', 'sincronizable'])

                ti_model = False
                ti_ids = []
                for ti in tis:
                    if not ti['ref_element_ti'] or not ti['sincronizable']:
                        continue
                    if not ti_model:
                        ti_model = ti['ref_element_ti'].split(',')[0]
                    elif ti['ref_element_ti'].split(',')[0] != ti_model:
                        raise osv.except_osv(
                            'Error', 'Hi ha elements amb el TI: {} de dos '
                                     'tipus diferents'.format(detall['ti_ccuu'][0])
                        )
                    ti_ids.append(int(ti['ref_element_ti'].split(',')[1]))

                # Per a ponderar els imports per a cada TI, primer necessitem
                # saber la quanitat total de unitats o de longitud
                sum_longitud = (
                        ti_model == 'giscedata.projecte.obra.ti.at' or
                        ti_model == 'giscedata.projecte.obra.ti.bt'
                )
                model_obj = self.pool.get(ti_model)
                if sum_longitud:
                    values = {
                        ti['id']: ti['longitud']
                        for ti in model_obj.read(cursor, uid, ti_ids,
                                                 ['longitud'])
                    }
                else:
                    values = dict.fromkeys(ti_ids, 1)

                total = sum(values.values())
                if not total:
                    continue
                for ti_id, value in values.items():
                    rate = value / float(total)
                    model_obj.write(cursor, uid, ti_id, {
                        'codi_ins': (detall['codi_ins'][0] if detall['codi_ins']
                                     else False),
                        'tipo_inversion': detall['tipo_inversion'],
                        'financiado': detall['financiado'],
                        'im_ingenieria': detall['im_ingenieria'] * rate,
                        'im_trabajos': detall['im_trabajos'] * rate,
                        'im_obracivil': detall['im_obracivil'] * rate,
                        'im_materiales': detall['im_materiales'] * rate,
                    })

    def _default_parent_project_obres(self, cursor, uid, ids, context=None):
        """
        Obtains the parent project by default for the obres.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids.
        :type ids: list[long]
        :param context: OpenERP context
        :return: the id of the <project.project>
        :rtpe: long
        """
        if context is None:
            context = {}
        imd_obj = self.pool.get('ir.model.data')
        __, project_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_projecte_obres', 'parent_project_obres')
        return project_id

    def action_recalcular_materials(self, cursor, uid, ids, context=None):
        """
        Creates the cost stock summary from the stock movements associated to
        the obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids
        :type ids: int
        :param context: OpenERP context.
        :type context: dict
        """
        if context is None:
            context = {}

        if not isinstance(ids, list):
            ids = [ids]

        obra_f = ['llista_materials_ids', 'move_to_stock_ids',
                  'move_not_from_stock_ids', 'move_from_stock_ids']

        obra_vs = self.read(cursor, uid, ids, obra_f, context=context)

        for obra_v in obra_vs:
            if not obra_v or not (obra_v['move_to_stock_ids']
                                  or obra_v['move_not_from_stock_ids']
                                  or obra_v['move_from_stock_ids']):
                raise osv.except_osv(
                    _(u'Aviso'),
                    _(u'No hay movimientos de material!')
                )

        move_o = self.pool.get('stock.move')
        resum_obj = self.pool.get('giscedata.projecte.obres.resum.stock')

        location_stock_ids = self.get_stock_locations(cursor, uid, context=context)

        for obra_v in obra_vs:
            obra_id = obra_v['id']

            # Filter those whose picking has a sale order associated
            dmn = [('id', 'in', obra_v['move_from_stock_ids']),
                   ('picking_id.sale_id', '!=', False)]
            move_from_stock_ids = move_o.search(cursor, uid, dmn, context=context)

            # Filter those whose picking:
            #   - has a purchase order associated
            #   - has a sale order associated and its marked as a return picking
            dmn = [('id', 'in', obra_v['move_to_stock_ids']),
                   '|',
                   ('picking_id.purchase_id', '!=', False),
                   '&',
                   ('picking_id.sale_id', '!=', False),
                   ('picking_id.return', '=', True)]
            move_to_stock_ids = move_o.search(cursor, uid, dmn, context=context)

            all_move_ids = (
                    move_to_stock_ids +
                    obra_v['move_not_from_stock_ids'] +
                    move_from_stock_ids
            )
            dmn = [('id', 'in', all_move_ids), ('state', 'in', ['done'])]
            move_ids = move_o.search(cursor, uid, dmn, context=context)
            moves = move_o.browse(cursor, uid, move_ids, context=context)

            res = {}

            for move in moves:
                product_id = move.product_id.id
                product_brief_v = res.get(product_id, False)
                if not product_brief_v:
                    product_brief_default_values = {
                        'obra_id': obra_id, 'product_id': product_id,
                        'inc_amount': 0, 'out_amount': 0, 'amount': 0,
                        'inc_price': 0, 'out_price': 0, 'price': 0,
                    }
                    product_brief_v = res.setdefault(
                        product_id, product_brief_default_values
                    )

                location_source_id = move.location_id.id
                location_dest_id = move.location_dest_id.id
                qty = move.product_qty
                if move.purchase_line_id:
                    unit_price = move.purchase_line_id.price_unit
                elif move.sale_line_id:
                    unit_price = move.sale_line_id.price_unit
                else:
                    raise osv.except_osv(
                        _(u'Error'),
                        _(u'El moviment no té associat una línia de compra o '
                          u'venda')
                    )

                subtotal = unit_price * move.product_qty

                if location_dest_id in location_stock_ids:
                    product_brief_v['out_amount'] += qty
                    product_brief_v['out_price'] += subtotal
                if location_source_id in location_stock_ids:
                    product_brief_v['inc_amount'] += qty
                    product_brief_v['inc_price'] += subtotal
                if location_source_id not in location_stock_ids and location_dest_id not in location_stock_ids:
                    product_brief_v['inc_amount'] += qty
                    product_brief_v['inc_price'] += subtotal

            resum_obj.unlink(cursor, uid, obra_v['llista_materials_ids'], context=context)

            for product_id in res:
                product_brief_v = res[product_id]
                product_brief_v['amount'] = product_brief_v['inc_amount'] - product_brief_v['out_amount']
                product_brief_v['price'] = product_brief_v['inc_price'] - product_brief_v['out_price']
                resum_obj.create(cursor, uid, product_brief_v, context=context)

        return True

    def get_stock_locations(self, cursor, uid, context=None):
        """
        Obtains the stock locations of the configured warehouses.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: user identifier
        :type uid: long
        :param context: OpenERP context
        :type context: dict
        :return: list that contains the <stock.location> ids.
        :rtype: list[long]
        """
        warehouse_o = self.pool.get('stock.warehouse')
        location_o = self.pool.get('stock.location')
        warehouse_ids = warehouse_o.search(cursor, uid, [], context=context)
        warehouse_f = ['lot_stock_id']
        warehouse_vs = warehouse_o.read(
            cursor, uid, warehouse_ids, warehouse_f, context=context
        )
        stock_location_ids = [x['lot_stock_id'][0] for x in warehouse_vs]

        dmn = [('id', 'child_of', stock_location_ids)]
        location_ids = location_o.search(cursor, uid, dmn, context=context)

        return location_ids

    def _check_repeated(self, cursor, uid, ids, context=None):
        """
        Check if there is some repeated obra
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <giscedata.projecte.obra> ids
        :type ids: long
        :param context: None
        :return: False if there is any obra with the same name
        :rtype: bool
        """
        if not isinstance(ids, (tuple, list)):
            ids = [ids]
        obres_vals = self.read(cursor, uid, ids, ['name'])
        check_result = True
        for obra in obres_vals:
            repeated_params = [
                ('name', '=', obra['name']), ('id', '!=', obra['id'])
            ]
            repeated_obra = self.search(cursor, uid, repeated_params)
            if repeated_obra:
                check_result = False
                break

        return check_result

    def default_get(self, cr, uid, fields_list, context=None):
        vals = super(GiscedataProjecteObra, self).default_get(
            cr, uid, fields_list, context=context
        )
        vals['state'] = 'template'
        return vals

    _columns = {
        'project_id': fields.many2one(
            'project.project', 'Projecte associat',
            required=True, ondelete='cascade',
            select=True
        ),
        # Dades d'utilitat interna
        'descripcio': fields.char('Descripción', size=128),
        'data_finalitzacio': fields.date(
            'Fecha finalización',
            help='Fecha de cierre administrativo de la obra.'
        ),
        # Costs
        #   Càlcul preu Factures 3rs
        'supplier_invoices_subtotal': fields.function(
            _fnct_supplier_invoices_subtotal, type='float', method=True,
            string='Subtotal facturas'
        ),
        'supplier_invoices_subtotal_no_taxes': fields.function(
            _fnct_supplier_invoices_subtotal_no_taxes, type='float',
            method=True, string='Subtotal facturas sin impuestos'
        ),
        #   Càlcul preu hores
        'task_subtotal': fields.function(
            _ff_ot_task_price, method=True, type="float",
            string='Subtotal por horas', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        #   Càlcul preu total
        'obra_subtotal': fields.function(
            _obra_total_price, method=True, type="float",
            string="Subtotal de la Obra",
        ),
        'obra_subtotal_no_taxes': fields.function(
            _obra_total_price_no_taxes, method=True, type="float",
            string="Subtotal de la Obra sin impuestos",
        ),
        #   Coste planificat
        'planned_cost': fields.float('Coste Planificado (€)', digits=(16, 2)),
        'cost_over_limit': fields.function(
            _cost_limits, method=True,
            string="Límite de costes previstos superado", type="boolean",
            multi="True"
        ),
        'cost_over_half': fields.function(
            _cost_limits, method=True, type="boolean", multi="True",
            string="Se ha superado en un 50% los costes planificados",
        ),
        # Elements
        'ti_ids': fields.one2many('giscedata.projecte.obra.ti.view',
                                  'obra_id', 'TIs', readonly=True),
        'lat_ids': fields.one2many(
            'giscedata.projecte.obra.ti.at', 'obra_id', 'LATs', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'lbt_ids': fields.one2many(
            'giscedata.projecte.obra.ti.bt', 'obra_id', 'LBTs', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'se_ids': fields.one2many(
            'giscedata.projecte.obra.ti.subestacions', 'obra_id',
            'Subestaciones', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'pos_ids': fields.one2many(
            'giscedata.projecte.obra.ti.posicio', 'obra_id', 'Posiciones', 
            readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        'maq_ids': fields.one2many(
            'giscedata.projecte.obra.ti.transformador', 'obra_id',
            'Transformadores', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'des_ids': fields.one2many(
            'giscedata.projecte.obra.ti.despatx', 'obra_id', 'Despachos', 
            readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'fia_ids': fields.one2many(
            'giscedata.projecte.obra.ti.celles', 'obra_id', 'Celdas', 
            readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'ct_ids': fields.one2many(
            'giscedata.projecte.obra.ti.cts', 'obra_id', 'CTs', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        'work_type': fields.selection(
            get_work_types, string="Tipo de Trabajo", required=True
        ),
        'importacio_ti_ids': fields.one2many(
            'giscedata.projecte.obra.detall.economic', 'obra_id',
            'Detalles económicos', readonly=False,
            states={'template': [('readonly', True)]}
        ),
        # Codigos
        'codi_ceco': fields.selection(
            get_codigos_ceco(), string='Código CECO'
        ),
        # Relaciones a las instalaciones reales
        'instalaciones_at_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.at.tram', multi='instalaciones'
        ),
        'instalaciones_bt_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.bt.element', multi='instalaciones'
        ),
        'instalaciones_se_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.cts.subestacions', multi='instalaciones'
        ),
        'instalaciones_pos_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.cts.subestacions.posicio', multi='instalaciones'
        ),
        'instalaciones_trafo_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.transformador.trafo', multi='instalaciones'
        ),
        'instalaciones_des_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.despatx', multi='instalaciones'
        ),
        'instalaciones_celles_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.celles.cella', multi='instalaciones'
        ),
        'instalaciones_cts_ids': fields.function(
            _fnct_get_instalaciones, method=True, type='one2many',
            obj='giscedata.cts', multi='instalaciones'
        ),
        'supplier_invoices': fields.many2many(
            'account.invoice', 'giscedata_ot_supplier_invoices_rel',
            'ot_id', 'invoice_id', 'Facturas', readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        # Materials
        #   Pedidos
        'order_to_stock_ids': fields.function(
            fnct=_ff_out_stock_ids, fnct_inv=_fnct_inv_picking,
            type='one2many', method=True, obj='purchase.order',
            multi='_ff_out_stock_ids',
            string="Pedidos de retorno de material", readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        'order_not_from_stock_ids': fields.function(
            fnct=_ff_out_stock_ids, fnct_inv=_fnct_inv_picking,
            type='one2many', method=True, obj='purchase.order',
            multi='_ff_out_stock_ids',
            string="Pedidos de compra a red de distribución", readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        'order_from_stock_ids': fields.one2many(
            'sale.order', 'ot_out_id',
            string='Pedidos a red de distribución desde Stock', readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        #   Albarans
        'picking_to_stock_ids': fields.function(
            fnct=_ff_out_stock_ids, fnct_inv=_fnct_inv_picking,
            type='one2many', method=True, obj='stock.picking',
            multi='_ff_out_stock_ids',
            string="Albaranes de retorno de material", readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        'picking_not_from_stock_ids': fields.function(
            fnct=_ff_out_stock_ids, fnct_inv=_fnct_inv_picking,
            type='one2many', method=True, obj='stock.picking',
            multi='_ff_out_stock_ids',
            string="Albaranes de envío de material desde fuera de Stock",
            readonly=False, states={'template': [('readonly', True)]}
        ),
        'picking_from_stock_ids': fields.function(
            fnct=_fnct_picking_from_stock_ids, fnct_inv=_fnct_inv_picking,
            type='one2many', method=True, obj='stock.picking',
            string="Albaranes de envío de material desde Stock", readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        #   Moviments
        'move_to_stock_ids': fields.function(
            fnct=_ff_out_stock_ids, multi='_ff_out_stock_ids',
            type='one2many', method=True, obj='stock.move',
            string='Movimientos hacia Stock', readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        'move_not_from_stock_ids': fields.function(
            fnct=_ff_out_stock_ids, multi='_ff_out_stock_ids',
            type='one2many', method=True, obj='stock.move',
            string='Movimientos desde fuera de Stock', readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        'move_from_stock_ids': fields.function(
            fnct=_fnct_move_from_stock_ids,
            type='one2many', method=True, obj='stock.move',
            string='Movimientos hacia Stock', readonly=False, 
            states={'template': [('readonly', True)]}
        ),
        #   Llistat resum de materials
        'llista_materials_ids': fields.one2many(
            'giscedata.projecte.obres.resum.stock', 'obra_id',
            string='Resumen de materiales usados en la Obra',
            readonly=True
        ),
        #   Càlcul preu stock
        'stock_subtotal': fields.function(
            fnct=_fnct_stock_subtotal, string="Subtotal en materiales",
            readonly=True, method=True, type='float'
        ),
        'project_workdone_ids': fields.one2many(
            'project.task.work', 'obra_id', string='Trabajos del proyecto',
            readonly=False, states={'template': [('readonly', True)]}
        ),
    }

    _defaults = {
        'work_type': lambda *args: 'inversions',
    }

    _constraints = [
        (
            _check_repeated,
            _('No pueden existir dos obras con el mismo identificador'),
            ['name']
        ),
    ]


GiscedataProjecteObra()


class GiscedataProjecteObresResumStock(osv.osv):

    _name = 'giscedata.projecte.obres.resum.stock'

    _columns = {
        'obra_id': fields.many2one(
            'giscedata.projecte.obra', string='Obra', ondelete='cascade'
        ),
        'product_id': fields.many2one('product.product', string='Producto'),
        'amount': fields.integer(string='Cantidad'),
        'inc_amount': fields.integer(string='Cantidad Entrante'),
        'out_amount': fields.integer(string='Cantidad Saliente'),
        'price': fields.float(string='Coste (€)'),
        'inc_price': fields.float(string='Coste (€) Entrante'),
        'out_price': fields.float(string='Coste (€) Saliente'),
    }


GiscedataProjecteObresResumStock()
