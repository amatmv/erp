# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from tools.translate import _
from osv import osv, fields


class stock_picking(osv.osv):

    _name = 'stock.picking'
    _inherit = 'stock.picking'

    def create(self, cr, user, vals, context=None):
        purchase_id = vals.get('purchase_id', False)
        sale_id = vals.get('sale_id', False)

        if 'obra_id' not in vals.keys():
            if purchase_id:
                purchase_obj = self.pool.get('purchase.order')
                ot_id = purchase_obj.read(cr, user, purchase_id, ['ot_in_id'], context=context)
                if ot_id and ot_id['ot_in_id'] and ot_id['ot_in_id'][0]:
                    vals.update({'obra_id': ot_id['ot_in_id'][0]})
            elif sale_id:
                sale_obj = self.pool.get('sale.order')
                ot_id = sale_obj.read(cr, user, sale_id, ['ot_out_id'], context=context)
                if ot_id and ot_id['ot_out_id'] and ot_id['ot_out_id'][0]:
                    vals.update({'obra_id': ot_id['ot_out_id'][0]})

        return super(stock_picking, self).create(cr, user, vals, context)

    def action_move(self, cursor, uid, ids, context=None):
        """
        Recalculates the material cost when performing the moves of the picking.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <stock.picking> ids.
        :type ids: list[long]
        :param context: OpenERP context.
        :type context: dict
        :return: True if the workflow flows correctly.
        :rtype: bool
        """
        res = super(stock_picking, self).action_move(cursor, uid, ids, context=None)
        if res:
            picking_f = ['obra_id']
            picking_vs = self.read(cursor, uid, ids, picking_f, context=context)
            obra_ids = set()
            for picking_v in picking_vs:
                obra_id = picking_v['obra_id']
                if obra_id:
                    obra_id = obra_id[0]
                    obra_ids.add(obra_id)

            obra_o = self.pool.get('giscedata.projecte.obra')
            obra_ids = list(obra_ids)
            if obra_ids:
                obra_o.action_recalcular_materials(cursor, uid, obra_ids, context=context)

        return res

    _columns = {
        'obra_id': fields.many2one(
            'giscedata.projecte.obra', 'Obra relacionada', ondelete='set null'
        ),
    }


stock_picking()


class stock_location(osv.osv):
    _name = "stock.location"
    _inherit = "stock.location"

    def get_location_usage(self, cursor, uid, context=None):
        res = super(stock_location, self).get_location_usage(
            cursor, uid, context
        )
        res.append(('obras', _('Trabajos en campo')))
        return res

    _columns = {
        'usage': fields.selection(
            get_location_usage, 'Tipo de localización', required=True),
    }


stock_location()
