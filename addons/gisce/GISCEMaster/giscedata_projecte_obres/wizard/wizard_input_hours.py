# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from osv import fields, osv
from datetime import datetime

from tools.translate import _


class WizardAddProjectObresHours(osv.osv_memory):
    """
    Wizard per realitzar moviments de Productes en Ordres de Treball
    """
    _name = 'wizard.add.project.obres.hours'
    _description = 'Asistente para imputar horas a la Obra'

    def _default_obra(self, cursor, uid, context=None):
        """
        Obtains the obra from which the wizard is open
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param context: OpenERP context.
        :type context: {}
        :return: <giscedata.projecte.obra> id.
        :rtype: long
        """
        return context['active_id']

    def _default_task(self, cursor, uid, context=None):
        """
        Obtains a task to be related to the work task
        :param cursor: DB cursor.
        :param uid: <res.users> id.
        :type uid: long
        :param context: OpenERP context.
        :type context: {}
        :return: <project.task> id.
        :rtype: long
        """
        obra_id = context.get('active_id', False)
        task_id = False

        if obra_id:
            if context is None:
                context = {}
            obra_obj = self.pool.get('giscedata.projecte.obra')
            task_obj = self.pool.get('project.task')
            task_ids = obra_obj.read(cursor, uid, obra_id, ['tasks'])

            if task_ids and task_ids['tasks']:
                task_ids = task_ids['tasks']

                dmn = [('id', 'in', task_ids),
                       ('state', 'not in', ['cancelled', 'done'])]
                filtered_task_ids = task_obj.search(cursor, uid, dmn, context=context)
                task_id = sorted(filtered_task_ids)[0]

        return task_id

    def _default_user(self, cursor, uid, context=None):
        """
        Obtains the user that must be related to the work task.
        :param cursor: DB cursor.
        :param uid: <res.users> id.
        :type uid: long
        :param context: OpenERP context.
        :type context: {}
        :return: <res.users> id.
        :rtype: long
        """
        return uid

    def _default_employee(self, cursor, uid, context=None):
        """
        Obtains the employee related to the user.
        :param cursor: DB cursor.
        :param uid: <res.users> id.
        :type uid: long
        :param context: OpenERP context.
        :type context: {}
        :return: <hr.employee> id
        :rtype: long
        """
        employee_o = self.pool.get('hr.employee')
        dmn = [('user_id', '=', uid)]
        employee_ids = employee_o.search(cursor, uid, dmn, context=context)
        employee_id = False
        if len(employee_ids) == 1:
            employee_id = employee_ids[0]

        return employee_id

    def _default_hour_cost(self, cursor, uid, context=None):
        return 0

    def get_employees(self, cursor, uid, context=None):
        """

        :param cursor: DB cursor.
        :param uid: <res.users> id.
        :param context:
        :return:
        """
        user_o = self.pool.get('res.users')
        employee_o = self.pool.get('hr.employee')
        dmn = [
            ('id', '=', uid),
            ('groups_id.name', '=', 'GISCEDATA Projecte Obres  / Manager')
        ]

        user_ids = user_o.search(cursor, uid, dmn, context=context)
        dmn = []

        if not user_ids:
            dmn = [('user_id', '=', uid)]

        employee_ids = employee_o.search(cursor, uid, dmn, context=context)
        return employee_ids

    def onchange_obra_id(self, cursor, uid, ids, obra_id, context=None):
        """
        Changes the value of the task_id field when the obra changes. If the
        obra has one task, then the selected task will be this one. If the obra
        has > 1 task, then the selected task will be none of them.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <wizard.input.hours> ids (unused parameter)
        :type ids: list[long]
        :param obra_id: Selected <giscedata.projecte.obra> id by the user.
        :type obra_id: long
        :param context: OpenERP context.
        :type context: {}
        :return: The standard return of the onchange functions.
        :rtype: dict
        """
        template = {
            'value': {'task_id': False},
            'domain': {},
            'warning': {}
        }
        task_o = self.pool.get('project.task')
        dmn = []

        if obra_id:
            obra_o = self.pool.get('giscedata.projecte.obra')

            project_id = obra_o.read(cursor, uid, obra_id, ['project_id'])
            project_id = project_id['project_id']

            if project_id:
                dmn = [('project_id', '=', project_id[0])]

        task_ids = task_o.search(cursor, uid, dmn, context=context)
        template['domain']['task_id'] = [('id', 'in', task_ids)]

        if len(task_ids) == 1:
            template['value']['task_id'] = task_ids[0]

        return template

    def onchange_task_id(self, cursor, uid, ids, task_id, context=None):
        """
        Changes the value and domain of the obra_id field when the task changes.
        If the task has obra_id, then it will be the obra.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <wizard.input.hours> ids (unused parameter)
        :type ids: list[long]
        :param task_id: Selected <project.task> id by the user.
        :type task_id: long
        :param context: OpenERP context.
        :type context: {}
        :return: The standard return of the onchange functions.
        :rtype: dict
        """
        template = {'value': {}, 'domain': {}, 'warning': {}}
        if task_id:
            task_o = self.pool.get('project.task')
            obra_o = self.pool.get('giscedata.projecte.obra')
            task_v = task_o.read(cursor, uid, task_id, ['project_id'],
                                 context=context)

            if not task_v['project_id']:
                raise osv.except_osv(
                    _('Error'),
                    _('La tarea seleccionada no está asignada a ninguna obra')
                )

            obra_id = obra_o.search(cursor, uid, [
                ('project_id', '=', task_v['project_id'][0])
            ])
            template['value']['obra_id'] = obra_id[0]

        return template

    def onchange_employee_id(self, cursor, uid, ids, employee_id, product_id, context=None):
        """
        Adds a domain to the product field: only show those who are a service
        and belongs to the product category that the employee defines.
        If there's only one available product, then it will be selected.
        :param cursor: DB cursor.
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <wizard.add.project.obres.hours> ids
        :type ids: [long]
        :param employee_id: <hr.employee> id.
        :type employee_id: [long]
        :param product_id: Selected <product.product> id at the wizard.
        :type product_id: long
        :param context: OpenERP context
        :type context: {}
        :return: OpenERP onchange return type.
        :rtype: {str: {}}
        """
        template = {'value': {}, 'warning': {}, 'domain': {}}
        template['domain']['product_id'] = [('type', '=', 'service')]

        if employee_id:
            employee_o = self.pool.get('hr.employee')
            employee_f = ['product_categ_id']
            employee_v = employee_o.read(
                cursor, uid, employee_id, employee_f, context=context
            )
            categ_id = employee_v['product_categ_id']
            if categ_id:
                product_o = self.pool.get('product.product')
                dmn = ('categ_id', 'child_of', [categ_id[0]])
                product_ids = product_o.search(cursor, uid, [dmn], context=context)
                product_changed = False
                if len(product_ids) == 1:
                    product_id = product_ids[0]
                    template['value']['product_id'] = product_id
                    product_changed = True
                elif not product_ids or product_id not in product_ids:
                    product_id = False
                    template['value']['product_id'] = product_id
                    product_changed = True
                if product_changed:
                    res = self.onchange_product_id(
                        cursor, uid, ids, product_id, context=context
                    )
                    template['value'].update(res['value'])

                template['domain']['product_id'].append(dmn)

        return template

    def onchange_product_id(self, cursor, uid, ids, product_id, context=None):
        """
        Updates the hour price in function of the product using the obra
        pricelist.
        :param cursor: DB cursor.
        :param uid: <res.users> id..
        :type uid: long
        :param ids: <wizard.add.project.obres.hours> ids.
        :type ids: [long]
        :param product_id: <product.product> id selected by the user.
        :param context: OpenERP context.
        :type context: {}
        :return: OpenERP onchange return type.
        :rtype: {str: {}}
        """
        template = {
            'value': {
                'hour_cost': 0,
                'description': ''
            },
            'warning': {}
        }

        if context is None:
            context = {}

        if product_id:
            pricelist_o = self.pool.get('product.pricelist')
            product_o = self.pool.get('product.product')

            dmn = [('type', '=', 'obra')]
            pricelist_id = pricelist_o.search(cursor, uid, dmn, context=context)
            pricelist_id = pricelist_id[0]

            qty = 1
            product_pricelist_prices = pricelist_o.price_get(
                cursor, uid, [pricelist_id], product_id, qty, context=context
            )
            price = product_pricelist_prices[pricelist_id]

            product_f = ['name']
            product_v = product_o.read(cursor, uid, product_id, product_f, context=context)

            template['value']['hour_cost'] = price
            template['value']['description'] = product_v['name']

        return template

    @staticmethod
    def check_values(vals, ignore_keys):
        for ignore_key in ignore_keys:
            if ignore_key in vals:
                vals.pop(ignore_key)
        if not vals['hours']:
            raise osv.except_osv(
                u'Error!',
                _(u"Hay que entrar horas.")
            )
        elif vals['hours'] < 0:
            raise osv.except_osv(
                u'Error!',
                _(u'Las horas deben ser positivas.')
            )

    def get_task_id(self, cursor, uid, wiz_id):
        task_id = self.read(cursor, uid, [wiz_id], ['task_id'])[0]['task_id']

        if not task_id:
            raise osv.except_osv(
                u'Error!',
                _(u"Hay que asigar alguna tarea.")
            )

        return task_id

    def action_input_hours(self, cursor, uid, ids, context=None):
        """
        Creates the task work using the wizard values.
        :param cursor: DB cursor.
        :param uid: <res.users> id..
        :type uid: long
        :param ids: <wizard.add.project.obres.hours> ids.
        :type ids: [long]
        :param context: OpenERP context.
        :type context: {}
        """
        if context is None:
            context = {}

        work_obj = self.pool.get('project.task.work')

        task_id = self.get_task_id(cursor, uid, ids[0])

        obra_id = context.get('active_id', False)

        cols = ['description', 'hours', 'product_id', 'date',
                'hour_cost', 'employee_id', 'user_id', 'type_id']
        vals = self.read(cursor, uid, ids, cols, context=context)[0]
        ignore_keys = ['name', 'id']
        WizardAddProjectObresHours.check_values(vals, ignore_keys)

        user_id = vals['user_id']
        employee_id = vals['employee_id']
        vals = {
            'user_id': user_id,             'employee_id': employee_id,
            'date': vals['date'],           'task_id': task_id,
            'obra_id': obra_id,             'name': vals['description'],
            'hour_cost': vals['hour_cost'], 'hours': vals['hours'],
            'product_id': vals['product_id'],
        }
        work_obj.create(cursor, uid, vals)

        default_hour_cost = self._default_hour_cost(
            cursor, user_id, context=context
        )

        wizard_wv = {
            'description': '',
            'hours': 0.0,
            'hour_cost': default_hour_cost,
            'product_id': False,
        }
        self.write(cursor, uid, ids, wizard_wv, context=context)

    def get_tipos_hora(self, cursor, uid, context=None):
        return [
            ('normal', _('Normal')),
            ('festiva', _('Festivas')),
            ('nocturna', _('Nocturnas')),
        ]

    _columns = {
        'obra_id': fields.many2one('giscedata.projecte.obra', 'Obra', required=True),
        'task_id': fields.many2one('project.task', 'Tarea', required=True),
        'user_id': fields.many2one('res.users', 'Usuario', required=True),
        'type_id': fields.selection(get_tipos_hora, 'Tipo de hora'),
        'date': fields.date('Fecha', required=True),
        'description': fields.char('Descripción', size=150),
        'hours': fields.integer('Horas', required=True),
        'hour_cost': fields.float(
            'Precio/hora', required=True, digits=(8, 2),
            help=_("Se usa el precio de mano de obra del Usuario, si tiene, "
                   "cuando se inicia el asistente.")
        ),
        'employee_id': fields.many2one('hr.employee', 'Empleado', required=True),
        'product_id': fields.many2one('product.product', 'Producto'),
    }

    _defaults = {
        'obra_id': _default_obra,
        'task_id': _default_task,
        'user_id': _default_user,
        'date': lambda *args: datetime.today().strftime('%Y-%m-%d'),
        'employee_id': _default_employee,
    }


WizardAddProjectObresHours()
