# coding=utf-8
from __future__ import absolute_import, unicode_literals

import base64
from io import StringIO
from osv import osv, fields
from tools.translate import _

import pandas as pd


class WizardObraReportExcelExport(osv.osv_memory):

    _name = 'wizard.obra.report.excel.export'
    _description = 'Asistente para generar resumen excel de la Obra'

    def genera_excel(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)

        obra_obj = self.pool.get('giscedata.projecte.obra')
        obra_id = context['active_id']
        obra = obra_obj.browse(cursor, uid, obra_id, context=context)

        excel_file = StringIO()
        writer = pd.ExcelWriter('/tmp/tmp_report.xls')

        if not obra.tasks:
            no_task_df = pd.DataFrame({
                _('Tareas'): [_('No hay tareas asociadas')]
            })
            no_task_df.to_excel(writer, sheet_name=_('Tareas'))
        else:
            task_counter = 0
            for task in obra.tasks:
                date_vals = []
                hour_type_vals = []
                hours_vals = []
                hour_cost_vals = []
                name_vals = []
                worker_vals = []
                workers = {}

                total_hours = 0
                total_hour_cost = 0
                for work in task.work_ids:
                    date_vals.append(work.date.split(' ')[0] or '')

                    prod_name = ''
                    if work.product_id:
                        prod_obj = self.pool.get('product.product')
                        prod_name = prod_obj.read(
                            cursor, uid, work.product_id.id, ['name'])['name']
                    hour_type_vals.append(prod_name)
                    hours_vals.append(work.hours or '')
                    hour_cost_vals.append(work.hour_cost or 0.00)
                    name_vals.append(work.name or '')
                    worker_vals.append(work.employee_id.name or '')

                    if work.employee_id:
                        worker_name = work.employee_id.name
                        if worker_name in workers:
                            workers[worker_name]['hours'] += work.hours
                            workers[worker_name]['amount'] += work.hours * (work.hour_cost or 0)
                        else:
                            workers[worker_name] = {
                                'hours': work.hours,
                                'amount': work.hours * (work.hour_cost or 0)
                            }

                    total_hours += work.hours
                    total_hour_cost += work.hours * work.hour_cost

                date_vals += ['', _('Total horas'), _('Coste total horas')]
                hour_type_vals += ['', '', '']
                hours_vals += ['', total_hours, total_hour_cost]
                hour_cost_vals += ['', '', '']
                name_vals += ['', '', '']
                worker_vals += ['', '', '']

                task_df_dict = {
                    _('Fecha'): date_vals,
                    _('Tipo hora'): hour_type_vals,
                    _('Tiempo dedicado (h)'): hours_vals,
                    _('Precio/hora (€)'): hour_cost_vals,
                    _('Resumen del trabajo'): name_vals,
                    _('Operario'): worker_vals,
                }
                
                df = pd.DataFrame(task_df_dict, columns=task_df_dict.keys())

                worker_name_vals = []
                worker_hours_vals = []
                worker_hours_cost_vals = []
                total_worker_hours = 0
                total_hours_cost = 0
                for key in workers:
                    worker_name_vals.append(key)
                    worker_hours_vals.append(workers[key]['hours'])
                    worker_hours_cost_vals.append(workers[key]['amount'])
                    total_worker_hours += workers[key]['hours']
                    total_hours_cost += workers[key]['amount']

                worker_name_vals += ['', 'Total horas operarios']
                worker_hours_vals += ['', total_worker_hours]
                worker_hours_cost_vals += ['', total_hours_cost]

                worker_tasks_df_dict = {
                    _('Operario'): worker_name_vals,
                    _('Total horas'): worker_hours_vals,
                    _('Coste total'): worker_hours_cost_vals
                }
                df_resume = pd.DataFrame(worker_tasks_df_dict,
                                         columns=worker_tasks_df_dict)

                task_name = ''.join(
                    ch for ch in task.name 
                    if ch.isalnum() or ch.isspace()
                )[:31]
                df.to_excel(writer, index=None, 
                            sheet_name=task_name.format(task_counter))
                df_resume.to_excel(writer, index=None, 
                                   sheet_name=task_name.format(task_counter),
                                   startrow=df.shape[0] + 3, startcol=0)

        if not obra.llista_materials_ids:
            no_material_df = pd.DataFrame({
                _('Materiales'): [_('No hay materiales associados')]
            })
            no_material_df.to_excel(writer, sheet_name=_('Materiales'))
        else:
            date_vals = []
            product_vals = []
            amount_vals = []
            cost_vals = []
            price_vals = []

            for material in obra.llista_materials_ids:
                data_comanda = ''
                product = material.product_id
                product_id = material.product_id and material.product_id.id
                for order in obra.order_from_stock_ids:
                    for order_line in order.order_line:
                        if order_line.product_id and (order_line.product_id.id == product_id):
                            data_comanda = order.date_order
                            break

                if product.code:
                    product_name = '[%s] %s' % (product.code, product.name or '')
                else:
                    product_name = product.name or ''

                date_vals.append(data_comanda or '')
                product_vals.append(product_name)
                amount = material.amount or 0
                amount_vals.append(amount)
                subtotal_product = material.price or 0
                cost_vals.append(subtotal_product)
                product_price = 0
                if amount > 0:
                    product_price = subtotal_product / amount
                price_vals.append(product_price)

            date_vals += ['', _('Total materiales')]
            product_vals += ['', '']
            amount_vals += ['', '']
            cost_vals += ['', obra.stock_subtotal]
            price_vals += ['', '']
            
            prod_df_dict = {
                _('Fecha'): date_vals,
                _('Producto'): product_vals,
                _('Cantidad'): amount_vals,
                _('Precio medio'): price_vals,
                _('Coste (€)'): cost_vals
            }
            df = pd.DataFrame(prod_df_dict, columns=prod_df_dict.keys())

            df.to_excel(writer, index=None, sheet_name=_('Materiales'))

        if not obra.supplier_invoices:
            no_invoice_df = pd.DataFrame({
                _('Facturas'): [_('No hay facturas asociadas')]
            })
            no_invoice_df.to_excel(writer, sheet_name=_('Facturas'))
        else:
            invoice_vals = []
            prov_invoice_vals = []
            provider_vals = []
            description_vals = []
            date_vals = []
            amount_vals = []

            total_invoice_amount = 0
            for invoice in obra.supplier_invoices:
                invoice_vals.append(invoice.number or '')
                prov_invoice_vals.append(invoice.origin or _('Sin num.'))
                provider_vals.append(invoice.partner_id and invoice.partner_id.name or _('Proveedor no informado'))
                description_vals.append(invoice.name or '')
                date_vals.append(invoice.origin_date_invoice or '')
                amount_vals.append(invoice.amount_untaxed or 0.00)
                total_invoice_amount += invoice.amount_untaxed

            invoice_vals += ['', _('Total facturas')]
            prov_invoice_vals += ['', '']
            provider_vals += ['', '']
            description_vals += ['', '']
            date_vals += ['', '']
            amount_vals += ['', total_invoice_amount]
            
            fact_df_dict = {
                _('Num. de factura'): invoice_vals,
                _('Num. factura proveedor'): prov_invoice_vals,
                _('Proveedor'): provider_vals,
                _('Descripción'): description_vals,
                _('Fecha de factura proveedor'): date_vals,
                _('Subtotal (€)'): amount_vals
            }
            df = pd.DataFrame(fact_df_dict, columns=fact_df_dict.keys())

            df.to_excel(writer, index=None, sheet_name=_('Facturas'))

        writer.book.save(excel_file)
        excel_file.seek(0)
        xls_data = base64.b64encode(excel_file.getvalue())
        writer.close()

        wizard.write({'excel_file': xls_data})

    def _get_file_name(self, cursor, uid, ids, context=None):
        obra_obj = self.pool.get('giscedata.projecte.obra')
        obra_id = ids['active_id']
        obra_name = obra_obj.read(cursor, uid, obra_id, ['name'])['name']

        return 'obra_{}.xls'.format(obra_name.replace('/', '_'))

    _columns = {
        'name': fields.char(string="file_name", size=64),
        'excel_file': fields.binary(string="Archivo Excel"),
    }

    _defaults = {
        'name': _get_file_name,
    }


WizardObraReportExcelExport()
