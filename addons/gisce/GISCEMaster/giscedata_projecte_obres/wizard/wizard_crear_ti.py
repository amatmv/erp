# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from six import StringIO
import base64
import csv

import pooler
from osv import osv, fields
from tools.translate import _
from giscedata_projecte_obres.giscedata_projecte_obres import \
    GiscedataProjecteObra


class WizardCrearTi(osv.osv_memory):

    _name = 'wizard.crear.ti'
    _description = 'Asistente para crear TIs dentro de la Obra'
    _wizard_states = [('init', 'Inicio'), ('end', 'Fin')]
    _load_methods = [
        ('csv', _('Desde fichero CSV')),
        ('selected', _('Vincular elementos seleccionados'))
    ]

    def onchange_obra_id(self, cursor, uid, ids, obra_id, context=None):
        """
        Warns if the obra type isn't 'inversions' because the elements shouln't
        be linked in other cases.
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id.
        :type uid: long
        :param ids: <wizard.input.hours> ids (unused parameter)
        :type ids: list[long]
        :param obra_id: Selected <giscedata.projecte.obra> id by the user.
        :type obra_id: long
        :param context: OpenERP context.
        :type context: {}
        :return: The standard return of the onchange functions.
        :rtype: dict
        """
        template = {'value': {}, 'domain': {}, 'warning': {}}
        if obra_id:
            obra_q = self.pool.get('giscedata.projecte.obra').q(cursor, uid)
            obra_v = obra_q.read(['work_type']).where([
                ('id', '=', obra_id)
            ])[0]

            if obra_v['work_type'] != 'inversions':
                template['warning'] = {
                    'title': _('Atención'),
                    'message': _('La obra no es de inversiones y no debe '
                                 'tener elementos asociados.')
                }
        return template

    def check_exists(self, cursor, uid, record_id, record_model):
        """
        Check that record with `record_id` exists as the model `record_model`.

        If it doesn't exist, it raises an exception
        :param cursor: OpenERP Cursor
        :type cursor: sql_db.Cursor
        :param uid: User ID
        :type uid: long
        :param record_id: record ID
        :type record_id: long
        :param record_model: record model
        :type record_model: str | unicode
        :return: None
        """
        record_model_obj = self.pool.get(record_model)
        search_res = record_model_obj.search(cursor, uid, [
            ('id', '=', record_id)
        ])
        if not search_res:
            msg = _("El elemento con ID: {record} no existe en el "
                    "modelo {model}").format(
                record=record_id, model=record_model
            )
            raise Exception(msg)

    def link_elements(self, cursor, uid, obra_id, element_ids, context=None):
        """
        Creates the link between the element and the obra through the many2many
        field model.
        In the case of the giscedata.at.tram, the created model will be a
        <giscedata.projecte.obra.ti.at>.

        :param cursor: OpenERP Cursor
        :type cursor: sql_db.Cursor
        :param uid: User ID
        :type uid: long
        :param obra_id: <giscedata.projecte.obra> id
        :type obra_id: long
        :param element_ids: elements ids
        :type element_ids: list[long]
        :param context: OpenERP Context.
        :return: tuple of lists where the first list if the list of the linked
        elements and the second is the list of the elements that haven't been
        linked because of exceptions
        :rtype: tuple(list[long], list[long])
        """
        if context is None:
            context = {}
        element_model = context.get('model', False)

        real_model = GiscedataProjecteObra.obres_models_correspondence(
            element_model, reverse=True
        )
        linked = []
        exceptions = []
        if element_model:
            model_obj = self.pool.get(element_model)
            for element_id in element_ids:
                db = pooler.get_db(cursor.dbname)
                tmp_cursor = db.cursor()
                try:
                    self.check_exists(cursor, uid, element_id, real_model)
                    self.check_exists(cursor, uid, obra_id,
                                      'giscedata.projecte.obra')
                    values = {
                        'element_ti_id': element_id,
                        'obra_id': obra_id
                    }
                    created_ti_id = model_obj.create(tmp_cursor, uid, values)
                    model_obj.refresh_ti_vals(tmp_cursor, uid, created_ti_id)
                    linked.append((element_id, obra_id))
                    tmp_cursor.commit()
                except Exception as ex:
                    tmp_cursor.rollback()
                    sentry = self.pool.get('sentry.setup')
                    if sentry is not None:
                        sentry.client.captureException()
                    exceptions.append(element_id)
                finally:
                    tmp_cursor.close()

        return linked, exceptions

    def link_elements_via_csv(self, cursor, uid, csvfile, context=None):
        """
        Links elements to obres processing the CSV file. The format must be the
        following: ELEMENT_ID;OBRA_ID

        :param cursor: OpenERP Cursor
        :type cursor: sql_db.Cursor
        :param uid: User ID
        :type uid: long
        :param csvfile: CSV file encoded with base64
        :param context: str
        :return: tuple of lists where the first list if the list of the linked
        elements and the second is the list of the elements that haven't been
        linked because of exceptions
        :rtype: tuple(list[long], list[long])
        """
        txt = base64.decodestring(str(csvfile))
        input_csv_data = StringIO(txt)

        csvreader = csv.reader(input_csv_data, delimiter=str(';'))

        linked, exceptions = [], []
        for line in csvreader:
            element_id, obra_id = line[:2]
            auxlinked, auxexceptions = self.link_elements(
                cursor, uid, obra_id, [element_id], context=context
            )
            linked += auxlinked
            exceptions += auxexceptions

        return linked, exceptions

    def action_crear_ti(self, cursor, uid, ids, context=None):
        """
        Creates the link between the element and the obra through the many2many
        field model.
        In the case of the giscedata.at.tram, the created model will be a
        <giscedata.projecte.obra.ti.at>.

        :param cursor: OpenERP Cursor
        :type cursor: sql_db.Cursor
        :param uid: User ID
        :type uid: long
        :param ids: <wizard.crear.ti> ids
        :type ids: list[long]
        :param context: OpenERP Context.
                        This must have the model that links the obra with the
                        element defined with the 'model' key.
                        Also, the 'active_ids' must be passed to indicate the
                        elements that might be linked.
        :return: Nothing
        """
        if context is None:
            context = {}

        wiz_vals = self.read(cursor, uid, ids[0], [], context=context)[0]
        active_ids = context.get('active_ids', [])

        model = context.get('model', False)
        assert model, "L'assistent s'ha de cridar amb un model"

        linked, exceptions = [], []
        if wiz_vals['load_method'] == 'csv':
            linked, exceptions = self.link_elements_via_csv(
                cursor, uid, wiz_vals['input_file'], context=context
            )
        elif wiz_vals['load_method'] == 'selected':
            linked, exceptions = self.link_elements(
                cursor, uid, wiz_vals['obra_id'], active_ids, context=context
            )

        if linked:
            elements_list = map(
                lambda (elem, obra): _('Elemento: {}, obra: {}').format(elem, obra),
                linked
            )
            text_vinculats = _("Lista de elementos vinculados:\n{}\n").format(
                ',\n'.join(elements_list)
            )
        else:
            text_vinculats = ''

        if exceptions:
            exceptions_list = map(lambda elem: '[{}]'.format(elem), exceptions)
            text_excepcions = _("Lista de elementos no vinculados: {}\n"
                                "\nPara más información contacte con el técnico"
                                " resposable").format(
                ', '.join(exceptions_list)
            )
        else:
            text_excepcions = ''

        result_info = _("Vinculaciones hechas.\n\n{text_vinculats}"
                        "\n{text_excepcions}").format(
            text_vinculats=text_vinculats,
            text_excepcions=text_excepcions
        )

        self.write(cursor, uid, ids, {
            'state': 'end',
            'result_info': result_info
        })

    _columns = {
        'obra_id': fields.many2one('giscedata.projecte.obra', 'Obra'),
        'state': fields.selection(_wizard_states, 'Estado'),
        'input_file': fields.binary('Fichero CSV'),
        'input_filename': fields.char(
            'Nombre del fichero de entrada', size=256
        ),
        'load_method': fields.selection(
            _load_methods, 'Método de carga',
            help=_("'Vincular elementos seleccionados': vincula una obra a los "
                   "elementos seleccionados del listado.")
        ),
        'result_info': fields.text('Result Info')
    }

    _defaults = {
        'state': lambda *args: 'init'
    }


WizardCrearTi()
