# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import fields, osv


class hr_employee(osv.osv):
    _name = "hr.employee"
    _inherit = "hr.employee"

    def _default_product_category(self, cursor, uid, context=None):
        imd_obj = self.pool.get('ir.model.data')
        try:
            def_categ = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_ot_project', 'euros_hora_project'
            )[1]
            return def_categ
        except ValueError:
            # If data was not loaded
            return False

    _columns = {
        'product_categ_id': fields.many2one(
            'product.category', 'Categoria de producto'
        )
    }

    _defaults = {
        "product_categ_id": _default_product_category,
    }


hr_employee()
