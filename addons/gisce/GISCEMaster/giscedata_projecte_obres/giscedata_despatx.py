# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields
from .giscedata_projecte_obres import GiscedataProjecteObra


class GiscedataDespatx(osv.osv):

    _name = 'giscedata.despatx'
    _inherit = 'giscedata.despatx'

    def search(self, cursor, uid, args, offset=0,
               limit=None, order=None, context=None, count=False):
        """
        Sobreescritura del método de búsqueda para que busque los elementos que
        tienen asociadas las líneas coincidentes a la obra que viene por
        parámetro "args"
        """
        new_args = GiscedataProjecteObra.search_elements(
            self, cursor, uid, args
        )
        return super(GiscedataDespatx, self).search(
            cursor, uid, new_args, offset, limit, order, context, count
        )

    _columns = {
        'municipi': fields.many2one('res.municipi', 'Municipio'),
        'ti_obres_ids': fields.one2many(
            'giscedata.projecte.obra.ti.despatx', 'element_ti_id',
            'Obras')
    }

    _defaults = {
        'municipi': lambda *a: False,
    }


GiscedataDespatx()
