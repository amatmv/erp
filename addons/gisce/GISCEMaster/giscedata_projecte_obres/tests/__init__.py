# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from destral import testing
from destral.transaction import Transaction
from osv import osv

import logging


class TestProjecteObra(testing.OOTestCase):
    def setUp(self):
        self.logger = logging.getLogger(__name__)
        self.pool = self.openerp.pool
        self.obra_obj = self.pool.get('giscedata.projecte.obra')

    def test_subtotal_calc(self):

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ir_md_obj = self.pool.get('ir.model.data')
            obra_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_0001'
            )[1]
            
            amounts = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'supplier_invoices_subtotal',
                    'stock_subtotal',
                    'task_subtotal',
                    'obra_subtotal',
                ]
            )
            read_subtotal = amounts['obra_subtotal']
            subtotal_amounts = [
                x for fieldname, x in amounts.items()
                if fieldname in (
                    'supplier_invoices_subtotal',
                    'stock_subtotal',
                    'task_subtotal',
                )
            ]
            subtotal = sum(subtotal_amounts)
            self.assertEqual(
                read_subtotal, subtotal,
                msg='OBRA SUBTOTAL Must be the SUM of all other SUBTOTALS'
            )

    def test_check_boxs_planed_costs(self):
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            ir_md_obj = self.pool.get('ir.model.data')
            obra_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_0001'
            )[1]

            check_costs = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'cost_over_limit',
                    'cost_over_half'
                ]
            )

            # planned_cost == 0
            self.assertFalse(
                check_costs['cost_over_limit'] or check_costs['cost_over_half'],
                msg="when PLANNED_COST is 0 BOTH vars should be FALSE"
            )

            write_params = {
                'planned_cost': 100.0,
            }

            # cost < 50% * planned_cost

            self.obra_obj.write(cursor, uid, [obra_id], write_params)

            check_costs = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'cost_over_limit',
                    'cost_over_half'
                ]
            )

            # self.assertTrue(
            #     check_costs['cost_over_half'],
            #     msg="when ot_subtotal is less than 50 percent planned cost "
            #         "cost_over_half should be FALSE"
            # )
            #
            # self.assertFalse(
            #     check_costs['cost_over_limit'],
            #     msg="when ot_subtotal is less than 50 percent planned cost "
            #         "cost_over_limit should be FALSE"
            # )

            # cost = 50% * planned_cost

            check_costs = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'cost_over_limit',
                    'cost_over_half'
                ]
            )

            # self.assertTrue(
            #     check_costs['cost_over_half'],
            #     msg="when ot_subtotal is equal to 50 percent planned cost "
            #         "cost_over_half should be TRUE"
            # )
            #
            # self.assertFalse(
            #     check_costs['cost_over_limit'],
            #     msg="when ot_subtotal is equal to 50 percent planned cost "
            #         "cost_over_limit should be FALSE"
            # )

            # cost > 50% * planned_cost < 100% * planned_cost

            check_costs = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'cost_over_limit',
                    'cost_over_half'
                ]
            )

            # self.assertTrue(
            #     check_costs['cost_over_half'],
            #     msg="when ot_subtotal is greater than 50 percent planned cost"
            #         " cost_over_half should be TRUE"
            # )
            #
            # self.assertFalse(
            #     check_costs['cost_over_limit'],
            #     msg="when ot_subtotal is greater than 50 percent planned cost"
            #         " cost_over_limit should be FALSE"
            # )

            # cost = 100% * planned_cost

            check_costs = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'cost_over_limit',
                    'cost_over_half'
                ]
            )

            # self.assertTrue(
            #     check_costs['cost_over_half'],
            #     msg="when ot_subtotal is equal to 100 percent planned cost "
            #         "cost_over_half should be TRUE"
            # )
            #
            # self.assertFalse(
            #     check_costs['cost_over_limit'],
            #     msg="when ot_subtotal is equal to 100 percent planned cost "
            #         "cost_over_limit should be TRUE"
            # )

            # cost > 100% * planned_cost

            check_costs = self.obra_obj.read(
                cursor, uid, obra_id, [
                    'cost_over_limit',
                    'cost_over_half'
                ]
            )

            # self.assertTrue(
            #     check_costs['cost_over_half'],
            #     msg="when ot_subtotal is greater than 100"
            #         " percent planned cost cost_over_half should be TRUE"
            # )
            #
            # self.assertFalse(
            #     check_costs['cost_over_limit'],
            #     msg="when ot_subtotal is greater than 100"
            #         " percent planned cost cost_over_limit should be TRUE"
            # )

    def test_calc_invoices_subtotal(self):
        ir_md_obj = self.pool.get('ir.model.data')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            obra_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_0001'
            )[1]
            obra = self.obra_obj.read(
                cursor, uid, [obra_id], ['supplier_invoices_subtotal'])[0]

            q = self.obra_obj.q(cursor, uid)
            sql = q.select([
                'id', 'supplier_invoices.amount_total'
            ]).where([
                ('id', '=', obra_id)
            ])
            cursor.execute(*sql)
            amount = 0
            for result in cursor.dictfetchall():
                amount += result['supplier_invoices.amount_total']
            self.assertEqual(
                amount, obra['supplier_invoices_subtotal'],
                msg='Supplier invoices subtotal should be the'
                    ' SUM of related invoices\' subtotals'
            )

    def test_stock_subtotal_calc(self):
        ir_md_obj = self.pool.get('ir.model.data')
        purchase_o = self.pool.get('purchase.order')
        sale_o = self.pool.get('sale.order')
        picking_o = self.pool.get('stock.picking')
        obra_o = self.pool.get('giscedata.projecte.obra')
        move_o = self.pool.get('stock.move')
        resum_producte_o = self.pool.get('giscedata.projecte.obres.resum.stock')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            obra_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_0001'
            )[1]

            sale_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_sale_01'
            )[1]

            purchase_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_purchase_01'
            )[1]

            def recalcular_materials():
                obra_o.action_recalcular_materials(cursor, uid, obra_id)

            # Calcular els costos i stock de materials a l'obra
            self.assertRaisesRegexp(
                osv.except_osv,
                ".*No hay movimientos de material!.*",
                recalcular_materials
            )

            obra_f = ['llista_materials_ids']
            obra_v = self.obra_obj.read(cursor, uid, obra_id, obra_f)

            resum_producte_ids = obra_v['llista_materials_ids']
            self.assertEqual(len(resum_producte_ids), 0)

            # Realitzem les comandes
            # - Virtualize workflow
            #   order_confirm
            sale_ids = [sale_id]
            sale_o.action_wait(cursor, uid, sale_ids)
            sale_o.action_ship_create(cursor, uid, sale_ids)
            sale_o.test_state(cursor, uid, sale_ids, 'finished')
            sale_o.write(cursor, uid, sale_ids, {'state': 'done'})

            # Realitzem entrada de material (po_id)
            # - Virtualize workflow
            #   purchase_confirm
            purchase_ids = [purchase_id]
            purchase_o.wkf_confirm_order(cursor, uid, purchase_ids)
            #   purchase_approve
            purchase_o.wkf_approve_order(cursor, uid, purchase_ids)
            purchase_o.action_picking_create(cursor, uid, purchase_ids)
            purchase_o.write(cursor, uid, purchase_ids, {'shipped': 1, 'state': 'approved'})

            dmn = [('obra_id', '=', obra_id)]
            picking_ids = picking_o.search(cursor, uid, dmn)

            # Per simplificar el test, forçarem l'estat dels moviments simulant
            # que s'han enviat/rebut correctament
            dmn = [('picking_id', 'in', picking_ids)]
            move_ids = move_o.search(cursor, uid, dmn)

            move_o.write(cursor, uid, move_ids, {'state': 'done'})

            # Tornem a calcular els costos i stock
            obra_o.action_recalcular_materials(cursor, uid, obra_id)

            obra_v = self.obra_obj.read(cursor, uid, obra_id, obra_f)

            resum_producte_ids = obra_v['llista_materials_ids']

            # Hi han d'haver 2 productes: [PC1] Basic PC i
            # [MB1] Mainboard ASUStek A7N8X
            self.assertEqual(len(resum_producte_ids), 2)

            resum_producte_f = ['price']
            resum_producte_vs = resum_producte_o.read(
                cursor, uid, resum_producte_ids, resum_producte_f
            )

            # cost entrant: 1666,65
            # cost sortint: 1044,439

            cost_materials = sum([x['price'] for x in resum_producte_vs])
            self.assertEqual(cost_materials, 622.211)

            return True

    def test_projecte_tasks_relations(self):
        """
        Check that the tasks associated with the obra are the same once the obra
        is opened. But when is in draft, there shouldn't be any.
        """
        ir_md_obj = self.pool.get('ir.model.data')
        task_obj = self.pool.get('project.task')
        workdone_obj = self.pool.get('project.task.work')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            obra_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_0002'
            )[1]

            obra = self.obra_obj.read(cursor, uid, [obra_id], [
                'tasks',
                'project_id'
            ])[0]

            self.assertFalse(
                obra['tasks'],
                msg='Obra\'s should not have tasks when its not opened'
            )

            task_ids = task_obj.search(cursor, uid, [
                ('project_id', '=', obra['project_id'][0])
            ])

            self.assertFalse(
                task_ids,
                msg='The project associated with the obra should not have tasks'
                    ' when its not opened'
            )

            self.obra_obj.set_open(cursor, uid, [obra_id])

            obra = self.obra_obj.read(cursor, uid, [obra_id], [
                'tasks',
                'project_workdone_ids',
                'task_subtotal',
                'project_id'
            ])[0]

            task_ids = task_obj.search(cursor, uid, [
                ('project_id', '=', obra['project_id'][0])
            ])

            self.assertItemsEqual(
                task_ids, obra['tasks'],
                msg='Obra\'s task ids should be the same as the tasks '
                    'pointing to the obra'
            )

            workdone_ids = workdone_obj.search(cursor, uid, [
                ('task_id', 'in', task_ids)
            ])

            self.assertItemsEqual(
                workdone_ids, obra['project_workdone_ids'],
                msg='Obra\'s workdone ids should be the same as the workdones'
                    ' pointing to the obra\'s tasks'
            )

    def test_calc_task_subtotal(self):
        ir_md_obj = self.pool.get('ir.model.data')
        workdone_obj = self.pool.get('project.task.work')

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            obra_id = ir_md_obj.get_object_reference(
                cursor, uid, 'giscedata_projecte_obres', 'obra_0001'
            )[1]
            obra = self.obra_obj.read(cursor, uid, [obra_id], [
                'project_workdone_ids',
                'task_subtotal',
            ])[0]

            task_subtotal = 0
            for workdone in workdone_obj.read(
                cursor, uid, obra['project_workdone_ids'], ['hours', 'hour_cost']
            ):
                task_subtotal += workdone['hours'] * workdone['hour_cost']

            self.assertEqual(
                task_subtotal, obra['task_subtotal'],
                msg='Task subtotal should be the sum of obra\'s'
                    ' task\'s Workdones\' hours * hours_cost'
            )
