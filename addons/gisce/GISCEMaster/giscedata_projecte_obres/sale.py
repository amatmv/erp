# -*- encoding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from osv import osv, fields


class sale_order(osv.osv):
    _name = 'sale.order'
    _inherit = 'sale.order'

    def write(self, cr, user, ids, vals, context=None):
        ot_out_id = vals.get('ot_out_id', None)
        if ot_out_id is not None:
            picking_o = self.pool.get('stock.picking')
            dmn = [('purchase_id', 'in', ids)]
            picking_ids = picking_o.search(cr, user, dmn, context=context)
            picking_wv = {'obra_id': ot_out_id}
            picking_o.write(cr, user, picking_ids, picking_wv, context=context)

        return super(sale_order, self).write(
            cr, user, ids, vals, context=context
        )

    def _default_ot_id(self, cursor, uid, context=None):
        return context.get('ot_out_id', False)

    _columns = {
        'ot_out_id': fields.many2one(
            'giscedata.projecte.obra', 'Obra relacionada', ondelete='set null'
        ),
    }

    _defaults = {
        'ot_out_id': _default_ot_id
    }


sale_order()
