# -*- coding: utf-8 -*-

from osv import osv, fields
from tools.translate import _


class GiscedataLecturesImportacio(osv.osv):
    """Agrupació d'importacions de lectures"""

    _name = 'giscedata.lectures.importacio'
    _description = "Llistat de fitxers zip importats"

    def unlink(self, cursor, uid, ids, context=None):
        """S'esborren les importacions que es poden esborrar totes les
           seves lectures."""
        lin = self.pool.get('giscedata.lectures.importacio.linia')
        for iid in ids:
            ivals = self.read(cursor, uid, iid, ['linia_ids'])
            if lin.unlink(cursor, uid, ivals['linia_ids']):
                super(GiscedataLecturesImportacio, self).unlink(cursor, uid,
                                                          iid, context=context)

    def _ff_check_status(self, cursor, uid, ids, field_name, arg,
                         context=None):
        res = {}
        lin_pool = self.pool.get('giscedata.lectures.importacio.linia')
        for i_id in ids:
            linies = self.read(cursor, uid, i_id, ['linia_ids'], context)
            linies_estat = lin_pool.read(cursor, uid, linies.get('linia_ids'),
                                         ['state', 'linialecturaene_id', 
                                          'linialecturapot_id'], context)
            complet = True
            xml_count = len(linies_estat)
            lect_count = 0
            for fitxer in linies_estat:
                lect_count += len(fitxer['linialecturaene_id'])
                lect_count += len(fitxer['linialecturapot_id'])
                if complet and fitxer['state'] != 'valid':
                    complet = False
            res[i_id] = {
                'importat_ok': complet,
                'num_xml': xml_count,
                'num_lect_creades': lect_count,
            }
        return res

    def _get_importacio_ids(self, cursor, uid, ids, context=None):
        l_obj = self.pool.get('giscedata.lectures.importacio.linia')
        i_id = []
        for a in l_obj.read(cursor, uid, ids, ['importacio_id']):
            i_id.append(a['importacio_id'][0])
        return i_id

    _STORE_PROC_AGREE_FIELDS = {
        'giscedata.lectures.importacio.linia': (_get_importacio_ids,
                              ['state'], 20),
    }

    _columns = {
        'info': fields.char('Informació', size=64),
        'name': fields.char('Nom Fitxer', size=64, requiered=True),
        'linia_ids': fields.one2many(
                            'giscedata.lectures.importacio.linia',
                            'importacio_id', 'Linies', ondelete='cascade'),
        'progres': fields.float('Progrés', readonly=True),
        'partner_id': fields.many2one('res.partner', 'Empresa emisora',
                                      required=False),
        'hash': fields.char('Hash', size=32, requiered=True, readonly=True),
        'importat_ok': fields.function(_ff_check_status, method=True,
                                       string="Sense errors", type='boolean',
                                       store=_STORE_PROC_AGREE_FIELDS,
                                       multi='proc'),
        'num_xml': fields.function(_ff_check_status, method=True,
                                       string="Total XMLs", type='integer',
                                       store=_STORE_PROC_AGREE_FIELDS,
                                       multi='proc'),
        'num_lect_creades': fields.function(_ff_check_status, method=True,
                                       string="Total lectures entrades", type='integer',
                                       store=_STORE_PROC_AGREE_FIELDS,
                                       multi='proc'),
    }

    _defaults = {
        'progres': lambda *a: 0,
    }

    _order = "id desc"

GiscedataLecturesImportacio()


class GiscedataLecturesImportacioLinia(osv.osv):
    """Línies d'importació de lectures"""

    _name = 'giscedata.lectures.importacio.linia'
    _description = "Llistat de fitxers xml importats"

    def unlink(self, cursor, uid, ids, context=None):
        """Al esborrar una línia prova d'esborrar les lectures associades,
           si pot aleshores esborra la línia.
           Retorna:
           + True si esborra totes les línies.
           + False en cas contrari
        """
        imp = self.pool.get('giscedata.lectures.importacio')
        if not context:
            context = {}
        u_linia = []
        u_imp = []
        for lid in ids:
            lvals = self.read(cursor, uid, lid, ['importacio_id'])
            try:
                self.unlink_lectures(cursor, uid, lid)
                self.unlink_adjunts(cursor, uid, lid)
                u_linia.append(lid)
                if lvals['importacio_id'][0] not in u_imp:
                    u_imp.append(lvals['importacio_id'][0])
            except osv.except_osv, e:
                pass
        super(GiscedataLecturesImportacioLinia, self).\
                                unlink(cursor, uid, u_linia, context=context)
        # actualitzar info de les importacions
        for iid in u_imp:
            ivals = imp.read(cursor, uid, iid,
                                        ['num_xml', 'num_lect_creades'])
            i_info = ("%d XML importats, %d lectures creades" %
                                 (ivals['num_xml'], ivals['num_lect_creades']))
            imp.write(cursor, uid, iid, {'info': i_info})
        return u_linia == ids

    def unlink_adjunts(self, cursor, uid, lid, context=None):
        """Eliminar els adjunts vinculats a la línia
        """
        if not context:
            context = {}
        att = self.pool.get('ir.attachment')
        att_ids = att.search(cursor, uid,
                [('res_id', '=', lid),
                 ('res_model','=','giscedata.lectures.importacio.linia')])
        if att_ids:
            att.unlink(cursor, uid, att_ids)

    def unlink_lectures(self, cursor, uid, lid, context=None):
        """Eliminar les lectures de comptador associades a la línia
        """
        if not context:
            context = {}
        lin_lect = self.pool.get(
                    'giscedata.lectures.importacio.linia.lecturaenergia')
        lin_pot = self.pool.get(
                    'giscedata.lectures.importacio.linia.lecturapotencia')
        lect_lect = self.pool.get('giscedata.lectures.lectura.pool')
        lect_pot = self.pool.get('giscedata.lectures.potencia.pool')
        lin_lect_id = lin_lect.search(cursor, uid, [('linia_id', '=', lid)])
        lin_pot_id = lin_pot.search(cursor, uid, [('linia_id', '=', lid)])
        if lin_lect_id:
            lect_id_obj = lin_lect.read(cursor, uid, lin_lect_id, ['lectura_id'])
            lect_ids = [x['lectura_id'][0] for x in lect_id_obj]
            lect_lect.unlink(cursor, uid, lect_ids, context)
        if lin_pot_id:
            pot_id_obj = lin_pot.read(cursor, uid, lin_pot_id, ['lectura_id'])
            pot_ids = [x['lectura_id'][0] for x in pot_id_obj]
            lect_pot.unlink(cursor, uid, pot_ids, context)

    def _ff_check_lectures(self, cursor, uid, ids, field_name, arg,
                           context=None):
        res = {}
        for l_id in ids:
            lin = self.read(cursor, uid, l_id, ['linialecturaene_id',
                                                'linialecturapot_id'], context)
            existeix = False
            if (lin.get('linialecturaene_id', []) or 
                               lin.get('linialecturapot_id', [])):
                existeix = True
            res[l_id] = existeix
        return res

    _states_selection = [
        ('erroni', 'Error en la importació'),
        ('valid', 'Importat correctament'),
        ('incident', 'Incidents en la importació'),
    ]

    _columns = {
        'name': fields.char('Fitxer', size=128, requiered=True),
        'state': fields.selection(_states_selection, 'Estat', size=32),
        'importacio_id': fields.many2one(
                                    'giscedata.lectures.importacio', 
                                    'Importacio', requiered=True),
        'info': fields.text('Info', readonly=True),
        'linialecturaene_id': fields.one2many(
                              'giscedata.lectures.importacio.linia.'
                              'lecturaenergia', 'linia_id', 'Factures', 
                               ondelete='cascade'),
        'linialecturapot_id': fields.one2many(
                              'giscedata.lectures.importacio.linia.'
                              'lecturapotencia', 'linia_id', 'Factures', 
                               ondelete='cascade'),
        'conte_lectures': fields.function(_ff_check_lectures, method=True,
                                       string="Conté lectures", type='boolean'),
        'codi_sollicitud': fields.char('Codi sol·licitud', size=12, 
                                       readonly=True), 
        'seq_sollicitud': fields.char('Seqüencial de sol·licitud', size=2, 
                                       readonly=True),
        'cups_id': fields.many2one('giscedata.cups.ps', 'CUPS afectat',
                                   required=False, size=22),
    }

    _order = 'create_date desc, write_date desc'

GiscedataLecturesImportacioLinia()

class GiscedataLecturesImportacioLiniaLecturaenergia(osv.osv):
    """Model relacional linia-lecturaenergia"""

    _name = 'giscedata.lectures.importacio.linia.lecturaenergia'
    _description = 'relació entre linies d\'importació i lectures d\'energia'

    _columns = {
        'linia_id': fields.many2one('giscedata.lectures.importacio.linia', 
                                    'Linia', requiered=True, 
                                     ondelete="cascade"),
        'lectura_id': fields.many2one('giscedata.lectures.lectura.pool',
                                 'Lectura', requiered=True, ondelete="cascade"),
        'lectura': fields.related('lectura_id', 'lectura', type='integer',
                                  string='Lectura', readonly=True),
        'consum': fields.related('lectura_id', 'consum', type='integer',
                                  string='Consum', readonly=True),
        'comptador': fields.related('lectura_id', 'comptador', 'name', type='char',
                                relation='giscedata_lectures_comptador',
                                string='Comptador', readonly=True),
        'tipus': fields.related('lectura_id', 'tipus', type='char',
                                  string='Tipus', readonly=True),
        'periode': fields.related('lectura_id', 'periode', 'name', type='char',
                                  relation='giscedata_polissa_tarifa_periodes',
                                  string='Periode', readonly=True),
        'name': fields.related('lectura_id', 'name', type='char',
                                string='Data', readonly=True),
    }

GiscedataLecturesImportacioLiniaLecturaenergia()

class GiscedataLecturesImportacioLiniaLecturapotencia(osv.osv):
    """Model relacional linialectura-lecturapotencia"""

    _name = 'giscedata.lectures.importacio.linia.lecturapotencia'
    _description = 'relació entre linies d\'importació i lectures de potencia'

    _columns = {
        'linia_id': fields.many2one('giscedata.lectures.importacio.linia',
                                    'Linia', requiered=True, 
                                    ondelete="cascade"),
        'lectura_id': fields.many2one('giscedata.lectures.potencia.pool',
                                 'Lectura', requiered=True, ondelete="cascade"),
        'lectura': fields.related('lectura_id', 'lectura', type='integer',
                                  string='Lectura', readonly=True),
        'comptador': fields.related('lectura_id', 'comptador', 'name', type='char',
                                relation='giscedata_lectures_comptador',
                                string='Comptador', readonly=True),
        'periode': fields.related('lectura_id', 'periode', 'name', type='char',
                                  relation='giscedata_polissa_tarifa_periodes',
                                  string='Periode', readonly=True),
        'name': fields.related('lectura_id', 'name', type='char',
                                string='Data', readonly=True),
    }

GiscedataLecturesImportacioLiniaLecturapotencia()
