# -*- coding: utf-8 -*-

import netsvc

def log(msg, level=netsvc.LOG_INFO):
    logger = netsvc.Logger()
    logger.notifyChannel('migration', level, msg)

def migrate(cursor, installed_version):
    """copiem les lectures al pool per no trencar la vinculació entre les
       lectures i les importacions """
    log('Migration from %s' % installed_version)
    log('Eliminant constraints')
    cursor.execute("ALTER TABLE "
        "giscedata_lectures_importacio_linia_lecturaenergia DROP CONSTRAINT "
        "giscedata_lectures_importacio_linia_lecturaener_lectura_id_fkey"
    )
    cursor.execute("ALTER TABLE "
        "giscedata_lectures_importacio_linia_lecturapotencia DROP CONSTRAINT "
        "giscedata_lectures_importacio_linia_lecturapote_lectura_id_fkey"
    )
    log("Migrant lectures al pool per mantenir vinculació amb línies de "
        "importació de Q1's")
    cursor.execute("""INSERT INTO giscedata_lectures_lectura_pool (
	id
	,create_uid
	,create_date
	,write_date
	,write_uid
	,periode
	,NAME
	,observacions
	,consum
	,incidencia_id
	,comptador
	,origen_id
	,lectura
	,tipus
	,origen_comer_id
	)
SELECT l.id
	,l.create_uid
	,l.create_date
	,l.write_date
	,l.write_uid
	,l.periode
	,l.NAME
	,l.observacions
	,l.consum
	,l.incidencia_id
	,l.comptador
	,l.origen_id
	,l.lectura
	,l.tipus
	,l.origen_comer_id
FROM giscedata_lectures_lectura l
LEFT JOIN giscedata_lectures_lectura_pool p ON p.id=l.id
WHERE (l.origen_comer_id IN (
  SELECT id
  FROM giscedata_lectures_origen_comer
  WHERE name SIMILAR TO '%(F|Q)1')
OR l.id IN (
 SELECT lectura_id
 FROM giscedata_lectures_importacio_linia_lecturaenergia)
)
AND p.id IS NULL """)
    cursor.execute("""INSERT INTO giscedata_lectures_potencia_pool (
	id
	,create_uid
	,create_date
	,write_date
	,write_uid
	,incidencia_id
	,periode
	,name
	,observacions
	,comptador
	,exces
	,lectura
	,origen_comer_id
	)
SELECT l.id
	,l.create_uid
	,l.create_date
	,l.write_date
	,l.write_uid
	,l.incidencia_id
	,l.periode
	,l.name
	,l.observacions
	,l.comptador
	,l.exces
	,l.lectura
	,l.origen_comer_id
FROM giscedata_lectures_potencia l
LEFT JOIN giscedata_lectures_potencia_pool p ON p.id=l.id
WHERE (l.origen_comer_id in (
  SELECT id
  FROM giscedata_lectures_origen_comer
  WHERE name SIMILAR TO '%(F|Q)1')
OR l.id in (
 SELECT lectura_id
 FROM giscedata_lectures_importacio_linia_lecturapotencia)
)
AND p.id IS NULL""")

    cursor.execute("select max(id)+1 from giscedata_lectures_lectura_pool")
    seq = cursor.fetchone()[0]
    log("setup correct sequence to giscedata_lectures_lectura_pool_id_seq %s"
        % seq)
    cursor.execute("ALTER SEQUENCE "
        "giscedata_lectures_lectura_pool_id_seq RESTART %s", (seq, )
    )
    cursor.execute("select max(id)+1 from giscedata_lectures_potencia_pool")
    seq = cursor.fetchone()[0]
    log("setup correct sequence to giscedata_lectures_potencia_pool_id_seq %s"
        % seq)
    cursor.execute("ALTER SEQUENCE "
        "giscedata_lectures_potencia_pool_id_seq RESTART %s", (seq, )
    )
