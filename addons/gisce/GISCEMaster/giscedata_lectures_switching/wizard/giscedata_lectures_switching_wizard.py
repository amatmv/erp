# -*- coding: utf-8 -*-
import traceback
import tempfile
import os
import base64
import hashlib
from tools.translate import _

import pooler
from osv import osv, fields
from base_extended.base_extended import MultiprocessBackground
from gestionatr.input.messages import Q1, message

def get_md5(data):
    _data = base64.decodestring(data)
    cleaned_data = _data.replace(" ", "")
    cleaned_data = cleaned_data.replace("\n", "")
    cleaned_data = cleaned_data.replace("\t", "")
    md5 = hashlib.md5(cleaned_data)
    return md5.hexdigest()


class GiscedataLecturesSwitchingWizard(osv.osv_memory):
    """Wizard per facturar via Q1"""
    _name = 'giscedata.lectures.switching.wizard'

    def ja_processat(self, cursor, uid, data, context=None):
        """Comprova si ja ha estat processat un fitxer amb el mateix
           hash anteriorment"""
        md5 = get_md5(data)
        importacio = self.pool.get('giscedata.lectures.importacio')
        search_params = [('hash', '=', md5)]
        imp_id = importacio.search(cursor, uid, search_params)
        return imp_id

    def clean_linia_and_commit(self, cursor, uid, lid, context=None):
        """Eliminar lectures, factures i comitar"""
        lin = self.pool.get('giscedata.lectures.importacio.linia')
        db = pooler.get_db_only(cursor.dbname)
        cr = db.cursor()
        try:
            lin.unlink_lectures(cr, uid, lid)
            lin.unlink_adjunts(cr, uid, lid)
            cr.commit()
        except Exception, e:
            traceback.print_exc() 
            cr.rollback()
        finally:
            cr.close()

    def adjuntar_xml(self, cursor, uid, lid, nom, xml, context=None):
        """Adjuntar l'xml a la línia d'importació"""
        if not context:
            context = {}

        self.pool.get('ir.attachment').create(cursor, uid, {
            'name': nom,
            'datas': xml,
            'datas_fname': nom,
            'res_model': 'giscedata.lectures.importacio.linia',
            'res_id': lid,
        }, context=context)

    def parse_lectures(self, cursor, uid, lid, lectures, fitxer, context=None):
        """Parsejar el diccionari lectures amb el segÜent format:
           - Lectures errònies:
               {<ref>: [False, <msg d'error>, <b64>]}
           - Lectures vàlides:
               {<ref>: [True, <msg>, [(tipus, lectura_id), ...], <b64>]}
           Tal que:
                <ref> = CodigoDeSolicitud | SecuencialDeSolicitud
                <b64> = xml en base64
        """
        if not context:
            context = {}
        lin_lect = self.pool.get(
                        'giscedata.lectures.importacio.linia.'\
                        'lecturaenergia')
        lin_pot = self.pool.get(
                        'giscedata.lectures.importacio.linia.'\
                        'lecturapotencia')
        info = []
        res = 'valid'
        for ref in lectures:
            if lectures[ref][0]:
                self.adjuntar_xml(cursor, uid, lid, fitxer,
                                        lectures[ref][3], context)
                info.append('Lectures importades: %d' % len(lectures[ref][2]))
                for lect in lectures[ref][2]:
                    vals = {'linia_id': lid, 'lectura_id': lect[1]}
                    if lect[0] in ['A', 'R']:
                        lin_lect.create(cursor, uid, vals)
                    else:
                        lin_pot.create(cursor, uid, vals)
            else:
                res = 'erroni'
                self.adjuntar_xml(cursor, uid, lid, fitxer,
                                        lectures[ref][2], context)
                info.append(u'%s' % lectures[ref][1])
            info.insert(0, _(u'Codi sol·licitud: %s, seqüencial:  %s') %\
                                                (ref[:12], ref[-2:]))
        info = u'\n'.join(info)
        return res, info

    def importar_xml_q1(self, cursor, uid, ids, context):
        """Reimportar un fitxer xml"""
        if not context:
            context = {}
        cups_id = ''
        wizard = self.browse(cursor, uid, ids[0], context)
        att = self.pool.get('ir.attachment')
        q1 = self.pool.get('giscedata.lectures.comptador')
        importacio = self.pool.get('giscedata.lectures.importacio')
        linia = self.pool.get('giscedata.lectures.importacio.linia')
        cups_obj = self.pool.get('giscedata.cups.ps')
        partner = self.pool.get('res.partner')

        lid = context.get('active_id', False)
        linval = linia.read(cursor, uid, lid, ['importacio_id'], context)
        if not linval['importacio_id']:
            msg = _(u'La línia no pertany a cap lot d\'importació')
            return msg
        i_id = linval['importacio_id'][0]
        context.update({'importacio_id': i_id})
        imp = importacio.browse(cursor, uid, i_id, context)
        emisor_lot = imp.partner_id.ref if imp.partner_id.ref != False else \
                                                              wizard.emisor.ref
        if wizard.origen == 'nou':
            xml = wizard.file
            fname = wizard.filename
        else:
            att_ids = att.search(cursor, uid,
                   [('res_id','=',lid),
                    ('res_model','=','giscedata.lectures.importacio.linia')])
            if not att_ids:
                raise osv.except_osv('Error',
                                        _("No s'ha trobat el fitxer adjunt"))
            xml_obj = att.read(cursor, uid, att_ids, ['datas', 'name'])[0]
            xml = xml_obj['datas']
            fname = xml_obj['name']
        data = base64.decodestring(xml)
        #Agafem el CUPS per registrar-lo a la bd
        try:
            f1_xml = Q1(data, 'Q1')
            f1_xml.parse_xml()
            cups = f1_xml.get_codi[:20]
            cupses = cups_obj.search(cursor, uid, [('name', 'like', cups)],
                                     context={'active_test': False})
            if cupses:
                cups_id = cupses[0]
        except message.except_f1, e:
            return e.value, ''
        # eliminem lectures associades a la línia d'importació
        self.clean_linia_and_commit(cursor, uid, lid, context)
        try:
            lect, emisor = q1.from_xml(cursor, uid, data, emisor_lot, context)
        except osv.except_osv, e:
            return e.value
        # si l'xml té errors de format
        if not emisor:
            msg = lect
            return msg
        # Actualitzar el partner_id del lot si cal
        i_vals = {}
        p_id = partner.search(cursor, uid, [('ref', '=', emisor)])
        if not imp.partner_id:
            i_vals.update({'partner_id': p_id[0]})
        # vincular lectures a la línia i lot
        if type(lect) != dict:
            linia.write(cursor, uid, lid, {'state': 'erroni',
                                           'name': fname,
                                           'info': lect,
                                           'cups_id': cups_id})
            return lect
        res, l_info = self.parse_lectures(cursor, uid, lid, lect,
                                                      fname, context)
        ref = lect.keys()[0]
        l_vals = {'state': res, 'info': l_info, 'name': fname,
                  'codi_sollicitud': ref[:12], 'seq_sollicitud': ref[-2:],
                  'cups_id': cups_id}
        linia.write(cursor, uid, lid, l_vals)
        # Actualitzar el lot d'importació
        i_num = importacio.read(cursor, uid, i_id,
                                ['num_xml', 'num_lect_creades'], context)
        i_info = (_(u"%d XML importats, %d lectures creades") %
                            (i_num['num_xml'], i_num['num_lect_creades']))
        i_vals.update({'progres': 100, 'info': i_info})
        importacio.write(cursor, uid, i_id, i_vals)
        return 'Importat el fitxer %s' % fname

    @MultiprocessBackground.background()
    def importar_zip_q1(self, cursor, uid, ids, context=None):
        """Importació del fitxer"""

        _(u"Importació del fitxer")

        if not context:
            context = {}
        wizard = context.get('wizard', {})
        if not wizard:
            raise Exception('No wizard found')
        del context['wizard']
        q1 = self.pool.get('giscedata.lectures.comptador')
        importacio = self.pool.get('giscedata.lectures.importacio')
        linia = self.pool.get('giscedata.lectures.importacio.linia')
        partner = self.pool.get('res.partner')

        i_id = context.get('active_id', False)
        imp = importacio.browse(cursor, uid, i_id, context)
        emisor_lot = imp.partner_id.ref
        wizard_emisor = partner.browse(cursor, uid, wizard['emisor']).ref
        if not emisor_lot:
            emisor_lot = wizard_emisor
        else:
            if (emisor_lot != wizard_emisor):
                return _(u"L'emisor no coincideix amb el del lot d'importacio")
        context.update({'importacio_id': i_id})
        with open(wizard['file'], 'r') as f:
            wizard['file'] = f.read()
            f.close()
            os.remove(f.name)
        md5 = get_md5(wizard['file'])
        filename = wizard['filename']
        i_vals = {'name': filename, 'hash': md5}
        try:
            data, emisor = q1.from_zip(cursor, uid, wizard['file'], emisor_lot,
                                                                    context)
        except osv.except_osv, e:
            i_vals.update({'info': e.value})
            importacio.write(cursor, uid, i_id, i_vals)
            return e.value
        # comprovar emisor
        if not emisor:
            i_vals.update({'info': _(u'Errors en la importació. Veure request.')})
            importacio.write(cursor, uid, i_id, i_vals)
            return data
        # comprovar que l'emisor del zip coincideix amb el del lot d'importacio
        p_id = partner.search(cursor, uid, [('ref', '=', emisor)])
        ip_id = importacio.read(cursor, uid, i_id, ['partner_id'], context)
        if ip_id['partner_id'] and ip_id['partner_id'][0] != p_id[0]:
            msg = _(u"L'empresa emisora no coincideix amb l'emisor del lot "
                    u"d'importació")
            return msg
        if not ip_id['partner_id']:
            i_vals.update({'partner_id': p_id[0], 'info': ''})
        l_vals = {}
        # crear les línies d'importacio
        for fitxer in data.keys():
            cups_id = data[fitxer]['cups_id']
            l_vals.update({'importacio_id': i_id,
                           'name': fitxer})
            lid = linia.create(cursor, uid, l_vals, context=context)
            # comprovar si l'xml ha resultat ser invàlid
            if not isinstance(data[fitxer]['res'], dict):
                linia.write(cursor, uid, lid, {'state': 'erroni',
                                               'info': data[fitxer]['res'],
                                               'cups_id': cups_id})
                continue
            res, l_info = self.parse_lectures(cursor, uid, lid,
                                              data[fitxer]['res'], fitxer, context)
            ref = data[fitxer]['res'].keys()[0]
            linia.write(cursor, uid, lid, {'state': res, 'info': l_info,
                    'codi_sollicitud': ref[:12], 'seq_sollicitud': ref[-2:],
                    'cups_id': cups_id})
        i_num = importacio.read(cursor, uid, i_id,
                                ['num_xml', 'num_lect_creades'], context)
        i_info = (_(u"%d XML importats, %d lectures creades") %
                            (i_num['num_xml'], i_num['num_lect_creades']))
        i_vals.update({'progres': 100, 'info': i_info})
        importacio.write(cursor, uid, i_id, i_vals)
        return _(u"Fitxer %s importat correctament") % filename
    
    def action_importar_q1(self, cursor, uid, ids, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, ids[0], context)
        fitxer_xml = context.get('fitxer_xml', False)
        if wizard.file or wizard.origen == 'exist':
            if fitxer_xml:
                wiz_msg = self.importar_xml_q1(cursor, uid, ids, context)
                wizard.write({'state': 'done', 'info': wiz_msg})
                return 0
            if self.ja_processat(cursor, uid, wizard.file):
                wiz_msg = _(u'El fitxer ja ha estat processat anteriorment')
                wizard.write({'state': 'done', 'info': wiz_msg})
                return 0
            wiz_msg = _(u"Ha començat el procés d'importació en segon pla."
                        u" En finalitzar rebrà una request.")
            wizard.write({'state': 'done', 'info': wiz_msg})
            try:
                ctx = context.copy()
                wizard = wizard.read()[0]
                with tempfile.NamedTemporaryFile(prefix='switching-q1-',
                                                 delete=False) as f:
                    f.write(wizard['file'])
                    f.close()
                    wizard['file'] = f.name
                ctx['wizard'] = wizard
                self.importar_zip_q1(cursor, uid, ids, ctx)
            except osv.except_osv:
                pass

    def action_cancel(self, cursor, uid, ids, context=None):
        """Cancel·lem.
        """
        return {
            'type': 'ir.actions.act_window_close',
        }

    def _default_state(self, cursor, uid, context=None):
        if not context:
            context = {}
        return 'init'

    def _default_emisor(self, cursor, uid, context=None):
        if not context:
            context = {}
        wizard = self.browse(cursor, uid, context['active_id'], context)
        importacio = self.pool.get('giscedata.lectures.importacio')
        linia = self.pool.get('giscedata.lectures.importacio.linia')
        partner = self.pool.get('res.partner')
        m_id = context.get('active_id', False)
        if 'fitxer_xml' in context:
            linval = linia.read(cursor, uid, m_id, ['importacio_id'], context)
            if linval['importacio_id']:
                i_id = linval['importacio_id'][0]
                imp = importacio.browse(cursor, uid, i_id, context)
            else:
                return False
        else:
            imp = importacio.browse(cursor, uid, m_id, context)
        return imp.partner_id.id

    def _default_xml(self, cursor, uid, context=None):
        if not context:
            context = {}
        conte_xml = False
        if context.get('fitxer_xml', False):
            fitxer = self.pool.get('ir.attachment').search(cursor, uid,
                  [('res_id','=',context['active_id']),
                   ('res_model','=','giscedata.lectures.importacio.linia')])
            if fitxer:
                conte_xml = True
        return conte_xml

    def _default_origen(self, cursor, uid, context=None):
        if not context:
            context = {}
        val = 'nou'
        if context.get('fitxer_xml', False):
            fitxer = self.pool.get('ir.attachment').search(cursor, uid,
                  [('res_id','=',context['active_id']),
                   ('res_model','=','giscedata.lectures.importacio.linia')])
            if fitxer:
                val = 'exist'
        return val

    ORIGEN_SELECTION = [('exist', 'Reimportar el fitxer existent'),
                       ('nou', 'Importar un fitxer nou')]

    _columns = {
        'file': fields.binary('Fitxer'),
        'filename': fields.char('Nom', size=128),
        'emisor': fields.many2one('res.partner', 'Empresa emisora',
                      required=True),
        'state': fields.char('State', size=16),
        'info': fields.text('Info'),
        'xml': fields.boolean('XML'),
        'origen': fields.selection(ORIGEN_SELECTION, 'Tipus')
    }

    _defaults = {
        'state': _default_state,
        'emisor': _default_emisor,
        'xml': _default_xml,
        'origen': _default_origen,
    }

GiscedataLecturesSwitchingWizard()
