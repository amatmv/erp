
Documentació d’usuari del mòdul de switching de lectures
==========================================================


El mòdul de Switching de lectures permet processar els fitxers XML de
lectures (Q1), ja sigui agrupats en un sol fitxer zip o individualment.

Aquest document descriu les funcionalitats del mòdul de Switching de lectures.


Lot d'importació i línies 
-------------------------

Denominem com a **lot d'importació** aquell conjunt de fitxers XML generats per
un mateix emisor i que ens convé tenir agrupats. El lot d'importació ens
permetrà importar el fitxer zip i/o xmls individualment. 

Cada un dels fitxers xml importats, ja sigui provinents d'un fitxer zip o
individualment, genera una **línia d'importació**. La línia d'importació
conté informació relacionada al fitxer xml importat i ens permet llistar les 
factures associades al fitxer xml o reimportar el fitxer en cas d'error.

El mòdul de switching de facturació  es troba en el submenú: Facturació >
Mercat Lliure > Lectures > Importacions Q1 (veure `Figura 1`_). Aquest
desplegarà una vista amb el llistat de lots d'importació  (veure `Figura 2`_).
El llistat ens mostra informació relacionada a cada lot d'importació com ara el
nom dels fitxers importats, l'empresa emisora i altre informació relacionada
amb el resultat. Per crear un nou lot d'importació cal prémer el botó *NEW*.

També podem accedir al llistat de tots els fitxers XML individuals importats (o
línia d'importació) des del su-menú *Fitxers Q1 importats* 

En prémer en un lot d'importació veurem el formulari del lot en qüestió (veure
`Figura 3`_). El formulari mostra juntament amb la informació del llistat,
una sèrie d'opcions a la dreta: Importar Q1 i Llistat de fitxers que es 
detallen a continuació.

* Importar Q1:

    Ens permetrà importar un nou fitxer zip i associar-lo al lot d'importació. El
    widget que ens apareixerà es mostra en la `Figura 4`_. Aquest ens
    demanarà el nom de l'emprea emisora, i ens permetrà sel·leccionar el fitxer a
    importar.
    
    En el cas de voler importar un nou fitxer zip en un lot d'importació existent,
    l'empresa emisora haurà de coincidir amb l'empresa emisora associada a lot
    d'importació i que haurà quedat vinculada a partir de l'importació anterior.
    
    En prémer Importar, es realitzarà la importació en *background*. El que
    significa que podrem seguir treballant mentre l'ERP va important els xmls. En
    finalitzar el procés se'ns notificarà a través d'una nova sol·licitud (*Request*
    que es mostra abaix a la dreta de la `Figura 3`_).
    
    També podem anar prement el refrescar del formulari veure l'estat de la barra
    de progrés.
    
    En finalitzar la importació se'ns actualitzarà la informació del lot
    d'importació, mostrant en el cas que no hi hagin hagut incidents, el nombre de
    fitxers xmls importats i factures creades. Veure `Figura 5`_.

* Llistat de fitxers:

    Ens permet mostrar els fitxers xml que hi ha associats al lot d'importació
    després d'importar-los individualment o a través d'un fitxer zip. En prémer es
    mostra el llistat de línies d'improtació. Veure `Figura 6`_. El llistat
    ens mostra informació relacionada amb cadascun dels fitxers xml processats.
    
    En prémer en una línia d'importació podrem veure un resum de la informació
    de les lectures generades en el cas d'haver-se importat de manera correcte.
    veure `Figura 7`_.
    
    Si la importació ha estat errònia, el que veurem és el que es mostra en la
    `Figura 8`_. En aquest cas ens permet reimportar el fitxer xml
    individualment. Es pot veure el wizard que ens apareixerà en la 
    `Figura 9`_.

Accés a fitxers Q1 d'un CUPS
----------------------------

Cada vegada que es processa una línia d'importació s'emmagatzema la informació
del CUPS al qual fa referència. Això ens permet accedir des d'un CUPS a tots
els fitxers Q1 (o línies d'importació) que hi fan referència. Veure `Figura
10`_.

Figures
-------

.. _`Figura 1`:
.. figure::  ./img/menu_principal.png
   :align:  center
   
   Figura 1: Ubicació del formulari d'importacions.

.. _`Figura 2`:
.. figure::  ./img/lot_tree.png
   :align:  center

   Figura 2: Llistat de lots d'importació.

.. _`Figura 3`:
.. figure:: ./img/lot_form_inicial.png
   :align: center

   Figura 3: Formulari d'un lot d'importació.

.. _`Figura 4`:
.. figure:: ./img/wiz_importar_q1.png
   :align: center

   Figura 4: Importació d'un zip.

.. _`Figura 5`:
.. figure:: ./img/lot_form_importat.png
   :align: center

   Figura 5:   Llistat de línies d'importació.

.. _`Figura 6`:
.. figure:: ./img/linia_tree_importat.png
   :align: center

   Figura 6: Llistat de línies d'importació.

.. _`Figura 7`:
.. figure:: ./img/linia_form_correcte.png
   :align: center

   Figura 7: Formulari d'una línia d'importació correcte.

.. _`Figura 8`:
.. figure:: ./img/linia_form_erroni.png
   :align: center

   Figura 8: Formulari d'una línia d'importació errònia.

.. _`Figura 9`:
.. figure:: ./img/wiz_importar_xml.png
   :align: center

   Figura 9: Importació d'un XML.

.. _`Figura 10`:
.. figure:: ./img/cups.png
   :align: center

   Figura 10: Accés a Q1's des de CUPS

