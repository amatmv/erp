# -*- coding: utf-8 -*-
{
    "name": "Lectures switching",
    "description": """Switching de lectures. 
                    Importació Q1.""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_lectures",
        "giscedata_lectures_pool"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/giscedata_lectures_switching_wizard.xml",
        "giscedata_lectures_switching_view.xml",
        "giscedata_lectures_switching_data.xml",
        "ir.model.access.csv",
        "giscedata_cups_ps_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
