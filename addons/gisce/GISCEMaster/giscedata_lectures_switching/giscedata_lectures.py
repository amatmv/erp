# -*- coding: utf-8 -*-

import zipfile
import base64
import traceback
import StringIO
import sys
from datetime import datetime, timedelta
from os.path import basename

import pooler
from osv import osv, fields, orm
from tools.translate import _

from switching.input.messages import F1, Q1, message
from switching.helpers.funcions import *
from libfacturacioatr import tarifes
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES


class GiscedataLecturesSwitchingHelper(osv.osv):
    """Funcions d'ajuda de switching"""

    _name = 'giscedata.lectures.switching.helper'

    def check_emisor_polissa(self, cursor, uid, polissa_id, emisor):
        pol = self.pool.get('giscedata.polissa')
        p_data = pol.browse(cursor, uid, polissa_id)
        ref_cups = p_data.cups.distribuidora_id.ref
        if (ref_cups != emisor):
            msg = (_(u"L'empresa distribuidora del cups %s no es correspon "
                     u"amb l'emisor especificat %s") % (p_data.cups.name, emisor))
            raise osv.except_osv('Error', _(msg))

    def check_data_ultima_lectura(self, cursor, uid, polissa_id, data_inici):
        """Es generar l'excepció en el cas que la data d'inici es trobi dins
           de l'últim periode facturat"""
        pol = self.pool.get('giscedata.polissa')
        p_br = pol.browse(cursor, uid, polissa_id)
        dul = p_br.data_ultima_lectura
        if not dul:
            return
        if (datetime.strptime(dul, '%Y-%m-%d') >
                                datetime.strptime(data_inici, '%Y-%m-%d')):
            msg = (_(u"Data d'ultima lectura de la polissa %s amb valor %s "
                     u"posterior a la data de lectura inicial %s") %
                   (p_br.name, dul, data_inici))
            raise osv.except_osv('Error', _(msg))

    def get_baixa_comptador(self, data):
        """Retorna el dia anterior a la data"""
        return (datetime.strptime(data, '%Y-%m-%d') -
                    timedelta(days=1)).strftime('%Y-%m-%d')

    def check_gir(self, gir, name):
        """Comprova que el gir hi quep en un enter"""
        sturn = "%0.f" % gir
        # MAX pg int is 2147483648 (9 zeros)
        if len(sturn) > 10:
            msg = _(u"El gir del comptador a crear amb número de sèrie %s "
                    u"supera tamany de l'enter") % name
            raise osv.except_osv('Error', _(msg))
        return True

    def calculate_price_from_rent_prices(self, cursor, uid, comptadors_xml,
                                         lloguers_xml, num_compt, context=None):
        if context is None:
            context = {}

        num_llogers = len(lloguers_xml)
        num_comptadors = len(comptadors_xml)

        if num_compt < num_llogers:
            # By default, we get the price from the same position
            preu = lloguers_xml[num_compt].precio_dia
        elif lloguers_xml:
            # If we don't have enough prices, we get the last one
            preu = lloguers_xml[-1].precio_dia
        else:
            # If the list is empty, we return 0
            preu = 0

        if num_comptadors < num_llogers and num_compt == num_comptadors - 1:
            # If we have more rents than meters and it's the last one, we get
            # the weighted average of the prices by number of days
            preu_total = sum(
                [
                    l.precio_dia * l.numero_dias
                    for l in lloguers_xml[num_compt:]
                ]
            )

            # Check in context if rents are simultanious. With simultanious
            # rents the numbers of days must be equal for all rents and they
            # are not summed (because rents are simultanious, occurs in the
            # same period of time)
            equal_rent_days = False
            rent_days = lloguers_xml[num_compt].numero_dias
            for l in lloguers_xml[num_compt:]:
                equal_rent_days = rent_days == l.numero_dias
                if not equal_rent_days:
                    break

            if context.get("simultanious_rents", False) and equal_rent_days:
                num_dies = lloguers_xml[num_compt].numero_dias
            else:
                num_dies = sum(
                    [
                        l.numero_dias
                        for l in lloguers_xml[num_compt:]
                    ]
                )

            # This will be correct if the prices are broken down, because the
            # days will be the same in each case so we will get the sum of all
            # of them.
            # It will also work if we have two different prices for a single
            # meter because we will be calculating the weighted average. If we
            # invoice the same number of days the price will be the same.
            if num_dies == 0:
                preu = 0
            else:
                preu = preu_total / num_dies

        return preu

    def update_current_meter(self, cursor, uid, fact, compt_id, start_date,
                             end_date, context=None):
        if context is None:
            context = {}

        compt_obj = self.pool.get('giscedata.lectures.comptador')

        compt_vals = compt_obj.read(
            cursor, uid, compt_id,
            ['preu_lloguer', 'lloguer', 'data_alta', 'data_baixa', 'polissa', 'active']
        )
        data_baixa_pol = self.pool.get("giscedata.polissa").read(
            cursor, uid, compt_vals['polissa'][0], ['data_baixa']
        )['data_baixa']

        vals = {}

        if compt_vals['data_alta'] > start_date:
            vals.update({'data_alta': start_date})

        if compt_vals.get('data_baixa', end_date) > end_date and compt_vals.get('data_baixa', end_date) != data_baixa_pol:
            last_date = self.get_last_reading_from_meter(cursor, uid, compt_id)
            if not last_date or (last_date and last_date <= end_date):
                vals.update({'data_baixa': end_date})
        if compt_vals['data_alta'] and not compt_vals['active'] and not vals.get('data_baixa') and not compt_vals['data_baixa']:
            # Si tenim un comptador amb data de alta,
            # sense data de baixa (i que tampoc n'hi posarem)
            # i que no està actiu, l'activem.
            vals.update({'active': True})
        compt_obj.write(cursor, uid, compt_id, vals)

    def get_last_reading_from_meter(self, cursor, uid, compt_id, context=None):
        if context is None:
            context = {}

        lect_obj = self.pool.get("giscedata.lectures.lectura")
        lect_pot_obj = self.pool.get("giscedata.lectures.potencia")
        lect_pool_obj = self.pool.get("giscedata.lectures.lectura.pool")
        lect_pool_pot_obj = self.pool.get("giscedata.lectures.potencia.pool")
        params = [('comptador', '=', compt_id)]
        ctx = context.copy()
        ctx['active_test'] = False

        dates = []
        for obj in [lect_obj, lect_pot_obj, lect_pool_obj, lect_pool_pot_obj]:
            obj_id = obj.search(cursor, uid, params, limit=1, order="name DESC", context=ctx)
            if not obj_id:
                continue
            obj_info = obj.read(cursor, uid, obj_id[0], ['name'])
            if obj_info['name']:
                dates.append(obj_info['name'])

        return len(dates) and max(dates) or False

    def create_new_meter(self, cursor, uid, polissa_vals, meter_name, turn,
                         start_date, end_date, context=None):
        if context is None:
            context = {}

        compt_obj = self.pool.get('giscedata.lectures.comptador')
        sturn = "%0.f" % turn
        if len(sturn) > 9:
            turn = float('1' + '0' * 9)

        # Check is the start date from meter is 1 day before data alta of
        # contract. This happens with new contracts with it's first F1.
        cstart_date = datetime.strptime(start_date, "%Y-%m-%d")
        pol_obj = self.pool.get("giscedata.polissa")
        polissa_data_alta = pol_obj.read(
            cursor, uid, polissa_vals['id'], ['data_alta']
        )['data_alta']
        polissa_data_alta = datetime.strptime(polissa_data_alta, "%Y-%m-%d")
        if (polissa_data_alta - cstart_date).days == 1:
            start_date = cstart_date + timedelta(days=1)
            start_date = start_date.strftime("%Y-%m-%d")

        self.check_gir(turn, meter_name)
        vals = {
            'name': meter_name,
            'polissa': polissa_vals['id'],
            'giro': turn,
            'data_alta': start_date,
            'active': True,
        }

        # We also need to check if we already have one and handle that
        search_params = [('polissa.id', '=', polissa_vals['id'])]
        # Si estem treballant amb un comptador de autoconsum nomes treballem amb altres contadors de autoconsum i viceversa
        if context.get('es_autoconsum'):
            search_params += [('name', 'in', context.get('comptadors_autoconsum', []))]
        else:
            search_params += [('name', 'not in', context.get('comptadors_autoconsum', []))]

        current_meter_id = compt_obj.search(cursor, uid, search_params)

        if current_meter_id:
            compt = compt_obj.browse(cursor, uid, current_meter_id[0])
            date_type = '%Y-%m-%d'
            d_alta_compt = datetime.strptime(compt.data_alta, date_type)
            d_lect_ini = datetime.strptime(start_date, date_type)
            d_lect_fi = datetime.strptime(end_date, date_type)
            if d_alta_compt < d_lect_ini:
                # Donar de baixa el comptador actiu
                # Es dona de baixa en la data anterior a l'alta del nou
                # comptador.
                data_baixa = datetime.strptime(start_date, "%Y-%m-%d")
                data_baixa -= timedelta(days=1)
                data_baixa = data_baixa.strftime("%Y-%m-%d")
                current_meter_vals = {
                    'active': False,
                    'data_baixa': data_baixa
                }
                compt_obj.write(
                    cursor, uid, current_meter_id, current_meter_vals
                )
            elif d_lect_ini <= d_alta_compt < d_lect_fi:
                # If we already have a meter during the period where we want
                # to import the new one this means that we would end up
                # having to meters active during the same time, which we
                # don't want
                ############################################################
                # IMPORTANT
                ############################################################
                # This should never happen since we have a critical
                # validation on phase 3 to prevent this. Despite that, this
                # code will be kept just in case
                msg = (_(
                    u"La pòlissa %s té assignat el comptador %s diferent "
                    u"al de l'xml (%s) amb data d'alta dins els periodes "
                    u"de lectura"
                ) % (polissa_vals['name'], compt.name, meter_name))
                raise osv.except_osv('Error', msg)
            elif d_alta_compt >= d_lect_fi:
                # If the new meter goes before the currently active one we
                # need to make it so that the new one has active at False
                # and data_baixa 1 day before the date in which the currently
                # active one has the data_alta
                data_baixa = datetime.strptime(compt.data_alta, "%Y-%m-%d")
                data_baixa -= timedelta(days=1)
                data_baixa = data_baixa.strftime("%Y-%m-%d")
                vals.update(
                    {'active': False, 'data_baixa': data_baixa}
                )

        # And we finally create the new meter
        return compt_obj.create(cursor, uid, vals, context=context)

    def check_meter(self, cursor, uid, meter_name, turn, polissa_id, start_date,
                    end_date, num_compt, fact=None, context=None):

        if context is None:
            context = {}

        f1cfg_obj = self.pool.get('giscedata.facturacio.switching.config')
        polissa_obj = self.pool.get('giscedata.polissa')
        cups_obj = self.pool.get('giscedata.cups.ps')
        res_partner = self.pool.get('res.partner')
        compt_obj = self.pool.get('giscedata.lectures.comptador')

        polissa_vals = polissa_obj.read(
            cursor, uid, polissa_id, ['cups', 'name']
        )

        compt_id = compt_obj.search_with_contract(
            cursor, uid, meter_name, polissa_id
        )

        if compt_id:
            compt_id = compt_id[0]
            # If we find the meter
            self.update_current_meter(
                cursor, uid, fact, compt_id, start_date, end_date, context
            )
        else:
            # If we didn't find the meter
            # We will have to look for the same name with REPE prefix

            repeated_meter_name = 'repe{}'.format(meter_name)
            compt_id = compt_obj.search_with_contract(
                cursor, uid, repeated_meter_name, polissa_id
            )

            if compt_id:
                # If we find the repeated meter
                compt_id = compt_id[0]
                self.update_current_meter(
                    cursor, uid, fact, compt_id, start_date, end_date, context
                )
            else:
                # If we didn't find the meter so far
                # We will have to create a new one
                compt_id = self.create_new_meter(
                    cursor, uid, polissa_vals, meter_name, turn, start_date,
                    end_date, context
                )

        return compt_id

    def check_comptador(self, cursor, uid, nom, gir, polissa_id, data_ini,
                                            data_fi, fact=None, context=None):
        """Comprova que existeix el comptador i en cas contrari crea el que
           s'especifica en l'xml i l'assigna a la pòlissa
        """
        f1cfg_obj = self.pool.get('giscedata.facturacio.switching.config')
        uom_obj = self.pool.get('product.uom')
        polissa_obj = self.pool.get('giscedata.polissa')
        polissa = polissa_obj.browse(cursor, uid, polissa_id)
        emissor = polissa.cups.distribuidora_id.ref
        f1config = f1cfg_obj.get_config(cursor, uid, emissor)
        preu = 0
        if fact:
            Lloguer = fact.get_info_lloguers()
        else:
            Lloguer = None
        if Lloguer:
            # Utilitzem les UOM per llegir en què ens ve i en què ho volem
            # transformar
            uom_pot = uom_obj.browse(cursor, uid, f1config['preu_lloguer_f'])
            unitat = uom_pot.name.split('/')[1] if '/' in uom_pot.name else 'mes'
            multi = tarifes.repartir_potencia({Lloguer.data_inici: 1},
                           Lloguer.data_inici, Lloguer.data_final, base=unitat)\
                                                             [Lloguer.data_inici]
            quant_contador_individual = float(Lloguer.quantitat) / context[
                'num_comptadors']
            preu = quant_contador_individual / multi
            preu = uom_obj._compute_price(cursor, uid, f1config['preu_lloguer_f'],
                                        preu, f1config['preu_lloguer_t'])
        count_pool = self.pool.get('giscedata.lectures.comptador')
        search_params = [('polissa.id', '=', polissa_id),
                         ('name', '=', nom)]
        count_id = count_pool.search(cursor, uid, search_params,
                                        context={'active_test': False})

        # mirem si el mateix número de comptador (traien els zeros)
        # existeix
        if not count_id:
            for meter in polissa.comptadors:
                try:
                    num_serie_int = int(nom)
                    meter_name = int(meter.name)
                except:
                    meter_name = False
                    num_serie_int = False

                if meter_name and meter_name == num_serie_int:
                    count_id = [meter.id]
                    nom = meter.name
                    break

        if count_id:
            count_id = count_id[0]
            cont = count_pool.browse(cursor, uid, count_id)
            if ((((preu != cont.preu_lloguer) and cont.lloguer) or
                                    (preu > 0 and not cont.lloguer)) and fact):
                vals = {'preu_lloguer': preu,
                        'lloguer': True,
                        'uom_id': f1config['preu_lloguer_t']}
                if cont.data_alta > data_ini:
                    vals.update({'data_alta': data_ini})
                count_pool.write(cursor, uid, count_id, vals)
        else:
            search_params = [('polissa.id', '=', polissa_id)]
            count_id = count_pool.search(cursor, uid, search_params)
            if count_id:
                compt = count_pool.browse(cursor, uid, count_id[0])
                date_type = '%Y-%m-%d'
                d_alta_compt = datetime.strptime(compt.data_alta, date_type)
                d_lect_ini = datetime.strptime(data_ini, date_type)
                d_lect_fi = datetime.strptime(data_fi, date_type)
                if d_alta_compt >= d_lect_ini and d_alta_compt < d_lect_fi:
                    msg = (_(u"La pòlissa %s té assignat el comptador %s diferent "
                             u"al de l'xml (%s) amb data d'alta dins els periodes "
                             u"de lectura") % (polissa.name, compt.name, nom))
                    raise osv.except_osv('Error', msg)
                if d_alta_compt < d_lect_ini:
                    # Donar de baixa el comptador actiu
                    # Es dona de baixa en la data de lectura inicial del nou
                    # comptador. Coincidirà amb la data d'alta del nou 
                    # comptador. Es corregirà la superposició d'un dia en el 
                    # procés de facturació.
                    vals = {'active': False,
                            'data_baixa': data_ini}
                    count_pool.write(cursor, uid, count_id, vals)
            # Afegir el nou comptador
            self.check_gir(gir, nom)
            vals = {'name': nom,
                    'polissa': polissa_id,
                    'giro': gir,
                    'data_alta': data_ini,
                    'preu_lloguer': preu,
                    'active': True,
                    'lloguer': False}
            if preu > 0:
                vals.update({'lloguer': True,
                             'uom_id': f1config['preu_lloguer_t']})
            if count_id and d_alta_compt >= d_lect_fi:
                vals.update({'active': False, 'data_baixa': compt.data_alta})
            count_id = count_pool.create(cursor, uid, vals, context=context)

        return count_id

    def find_polissa(self, cursor, uid, cups, data_inici, data_final):
        """Buscar la pòlissa associada al cups
        """
        search_params = [('name', 'ilike',  '%s%%' % cups[:20])]
        cups_pool = self.pool.get('giscedata.cups.ps')
        cups_id = cups_pool.search(cursor, uid, search_params,
                                        context={'active_test': False})
        if not cups_id:
            msg = _(u"No s'ha trobat el CUPS '%s'") % cups[:20]
            raise osv.except_osv('Error', msg)

        return self.find_polissa_with_cups_id(
            cursor, uid, cups_id[0], data_inici, data_final
        )

    def find_polissa_with_cups_id(self, cursor, uid, cups_id, data_inici,
                                  data_final):
        search_params = [
            # First we search active contracts
            ('state', 'not in', CONTRACT_IGNORED_STATES + ['baixa']),
            ('cups.id', '=', cups_id), ('data_alta', '<=', data_inici),
            '|', ('data_baixa', '>=', data_final), ('data_baixa', '=', False)
        ]
        polisses_pool = self.pool.get('giscedata.polissa')
        polissa_id = polisses_pool.search(cursor, uid, search_params,
                                            context={'active_test': False})
        if not polissa_id:
            # If we don't find contracts we search in any state
            search_params = [
                ('cups.id', '=', cups_id), ('data_alta', '<=', data_inici),
                '|', ('data_baixa', '>=', data_final),
                ('data_baixa', '=', False)
            ]
            cups_obj = self.pool.get('giscedata.cups.ps')
            polissa_id = polisses_pool.search(cursor, uid, search_params,
                                              context={'active_test': False})
            if not polissa_id:
                cups = cups_obj.read(cursor, uid, cups_id, ['name'])['name']
                msg = _(u"No s'ha trobat la pòlissa vinculada al cups %s en "
                        u"el periode de %s a %s") % (cups[:20], data_inici,
                                                                    data_final)
                raise osv.except_osv('Error', msg)
        return polissa_id[0]

    def get_num_periodes(self, cursor, uid, ids, tipus, context=None):
        """Retorna el número de periodes reals (no agrupats de la tarifa).
        """
        periodes_obj = self.pool.get('giscedata.polissa.tarifa.periodes')
        if tipus in ['A', 'R']:
            _tipus = 'te'
        else:
            _tipus = 'tp'
        for tid in ids:
            search_params = [('tarifa.id', '=', tid),
                             ('tipus', '=', _tipus)]
            return periodes_obj.search_count(cursor, uid, search_params)

    def get_id_periode(self, cursor, uid, lect, tarifa, en_baixa=False):
        search_params = [('codi_ocsum', '=', tarifa)]
        if en_baixa and tarifa == '011':
            search_params += [('mesura_ab', '=', 'B')]
        tarifa_pool = self.pool.get('giscedata.polissa.tarifa')
        tarifa_id = tarifa_pool.search(cursor, uid, search_params)

        if lect.tipus in ['A', 'R', 'S']:
            tipus = 'te'
        elif lect.tipus in ['M', 'EP']:
            tipus = 'tp'

        search_params = [('tarifa.id', '=', tarifa_id[0]),
                         ('tipus', '=', tipus),
                         ('name', '=', lect.periode)]
        periodes_pool = self.pool.get('giscedata.polissa.tarifa.periodes')
        periodes_id = periodes_pool.search(cursor, uid, search_params)
        if not periodes_id:
            raise osv.except_osv(
                'Error',
                _("No s'ha trobat el periode %s de tipus %s pel codi de "
                  "tarifa OCSUM %s") % (lect.periode, lect.tipus, tarifa))
        return periodes_id[0]

    def get_config_by_compt(self, cursor, uid, compt, context=None):
        """Retorna l'objecte de configuració de switching vinculat
           a l'empresa distribuidora de la pòlissa del comptador
        """
        f1cfg_obj = self.pool.get('giscedata.facturacio.switching.config')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        obj = compt_obj.browse(cursor, uid, compt, context=context)
        emissor = obj.polissa.cups.distribuidora_id.ref
        return f1cfg_obj.get_config(cursor, uid, emissor)

    def set_uom_lectura(self, cursor, uid, valor, tipus, compt, context=None):
        """Retorna el valor de la lectura segons la configuració
           de switching de l'emisor.
        """
        if not context:
            context = {}

        uom_obj = self.pool.get('product.uom')
        imd_obj = self.pool.get('ir.model.data')

        uom_kwatt_hour = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'uom_eneg_elec'
        )[1]
        uom_kvarh = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'uom_eneg_react'
        )[1]
        uom_watt = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'uom_pot_elec_wat'
        )[1]
        uom_kwatt = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'uom_pot_elec'
        )[1]

        # Assignem les referències de conversió per Q1 i per F1
        # EP uom the same of M
        f1_uoms = {
            'A': {
                'from': uom_kwatt_hour,
                'to': uom_kwatt_hour,
            },
            'S': {
                'from': uom_kwatt_hour,
                'to': uom_kwatt_hour,
            },
            'R': {
                'from': uom_kvarh,
                'to': uom_kvarh,
            },
            'M': {
                'from': uom_watt,
                'to': uom_kwatt,
            },
            'EP': {
                'from': uom_watt,
                'to': uom_kwatt,
            }
        }
        q1_uoms = f1_uoms

        # Si venim d'un F1 fem servir les unitats de conversió dels F1 si no
        # fem servir les unitats de conversió del Q1
        if context.get('from_f1', False):
            uom_map = f1_uoms
        else:
            uom_map = q1_uoms
        ref = uom_map[tipus]
        return uom_obj._compute_qty(cursor, uid, ref['from'], valor, ref['to'])

    def vals_lectura(self, cursor, uid, lect, ordre,  compt,
                     tarifa=None, en_baixa=False, context=None):
        """Retorna els valors de la lectura"""
        if ordre == 'inicial':
            reading = lect.lectura_desde
            consum = 0
        else:
            reading = lect.lectura_hasta
            consum = lect.consumo_calculado
        codi = reading.procedencia
        name = reading.fecha
        lec_ = reading.lectura

        lec_ = self.set_uom_lectura(
            cursor, uid, lec_, lect.tipus, compt, context
        )

        periode = self.get_id_periode(
            cursor, uid, lect, tarifa, en_baixa=en_baixa
        )
        search_params = [('codi', '=', 'F1')]
        origen_pool = self.pool.get('giscedata.lectures.origen_comer')
        orig_comer = origen_pool.search(cursor, uid, search_params)

        vals = {}
        vals['name'] = name
        vals['periode'] = periode
        vals['comptador'] = compt
        vals['origen_comer_id'] = orig_comer[0]
        vals['lectura'] = lec_
        vals['consum'] = consum
        vals['tipus'] = lect.tipus
        vals['magnitud'] = lect.magnitud

        if lect.tipus in ['A', 'R', 'S']:
            search_params = [('codi', '=', codi)]
            origen_pool = self.pool.get('giscedata.lectures.origen')

            orig = origen_pool.search(cursor, uid, search_params)
            vals['origen_id'] = orig[0]
            # The reading has to be an int
            vals['lectura'] = int(vals['lectura'])
            vals['consum'] = int(vals['consum'])
            if lect.ajuste and ordre != 'inicial':
                vals['ajust'] = lect.ajuste.ajuste_por_integrador
                vals['motiu_ajust'] = lect.ajuste.codigo_motivo

        elif lect.tipus == 'M':
            vals['exces'] = 0
            # Protecció maxímetre en watts
            if 0 < vals['consum'] < 1:
                meter_obj = self.pool.get('giscedata.lectures.comptador')
                meter = meter_obj.browse(cursor, uid, compt, context=context)
                power = meter.polissa.potencia
                if 1 < (vals['consum'] * 1000.0) / power < 2:
                    vals['consum'] *= 1000
            elif vals['consum'] >= 1000:
                vals['consum'] /= 1000.0
        elif lect.tipus == 'EP':
            vals['lectura'] = 0
            vals['exces'] = lec_

        return vals

    def importar_lect_periode(self, cursor, uid, tipus, periode,
                              rectificadora=False, overwrite_atr=False,
                              context=None):
        if context is None:
            context = {}

        if tipus in ['A', 'R']:
            lect_pool = self.pool.get('giscedata.lectures.lectura.pool')
        else:
            lect_pool = self.pool.get('giscedata.lectures.potencia.pool')

        lect = []

        for lectura in periode:
            existeix = lectura[1]
            vals = lectura[0].copy()
            del vals['consum']
            l_id = None
            if not existeix:
                l_id = lect_pool.create(
                    cursor, uid, vals, context
                )
            elif existeix:
                if rectificadora:
                    l_id = existeix
                    lect_ori_vals = lect_pool.read(
                        cursor, uid, l_id, ['observacions', 'lectura']
                    )
                    obs = lect_ori_vals['observacions'] or ''
                    if obs:
                        obs += '\n'
                    vals['observacions'] = obs + _(
                        'Valor de lectura sobreescrit pel valor d\'una '
                        'rectificadora. '
                        'Valor antic: {0}. Valor nou: {1}'
                    ).format(lect_ori_vals['lectura'], vals['lectura'])
                    lect_pool.write(
                        cursor, uid, [l_id], vals, context
                    )
                elif overwrite_atr and existeix > 1:
                    # existeix may be a 1, meaning that the measure is
                    # already present in the dictionary and nothing has to
                    # be done or it can be the id of the actual "lectura"
                    l_id = existeix
                    lect_bw = lect_pool.browse(cursor, uid, l_id)
                    old_ori_code = lect_bw.origen_comer_id.codi
                    has_at_ori = old_ori_code == 'AT'

                    old_ori_id = lect_bw.origen_comer_id.id
                    new_ori_id = vals['origen_comer_id']
                    if has_at_ori and new_ori_id != old_ori_id:
                        obs = lect_bw.observacions or ''
                        if obs:
                            obs += '\n'
                        vals['observacions'] = obs + _(
                            'Valor de lectura de gestió ATR sobreescrit '
                            'pel valor d\'un F1. '
                            'Valor antic: {0}. Valor nou: {1}'
                        ).format(lect_bw.lectura, vals['lectura'])
                        lect_pool.write(
                            cursor, uid, [l_id], vals, context
                        )
            if not l_id:
                lect.append((tipus, l_id))
        return lect

    def importar_lectures_compt(self, cursor, uid, lect_data, context):
        """Importar les lectures que conté el diccionari lect_data"""
        cfg = self.pool.get('res.config')

        overwrite_atr = bool(int(
            cfg.get(
                cursor, uid, 'lect_sw_overwrite_atr_measure_when_found', '0'
            )
        ))

        lect = []
        if context.get('tipus_rectificadora', 'N') in ('R', 'RA'):
            rectificadora = True
        else:
            rectificadora = False
        for tipus in lect_data:
            if tipus in ['A', 'R']:
                # lectures inicials
                for periode in lect_data[tipus]['inicial'].values():
                    try:
                        self.importar_lect_periode(
                            cursor, uid, tipus, periode,
                            rectificadora=rectificadora,
                            overwrite_atr=overwrite_atr, context=context
                        )
                    except orm.except_orm as e:
                        raise osv.except_osv(
                            'Error', _(
                                "Error introduint lectures"
                                " en data inicial.\n%s" %
                                e.value
                            )
                        )
            # lectures finals
            for periode in lect_data[tipus]['final'].values():
                try:
                    self.importar_lect_periode(
                        cursor, uid, tipus, periode,
                        rectificadora=rectificadora,
                        overwrite_atr=overwrite_atr, context=context
                    )
                except orm.except_orm as e:
                    raise osv.except_osv(
                        'Error', _(
                            "Error introduint lectures"
                            " en data final.\n %s" %
                            e.value
                        )
                    )
        return lect

    def check_reading_exists(self, cursor, uid, lect_vals, context=None):
        if context is None:
            context = {}

        search_params = [
            ('name', '=', lect_vals['name']),
            ('comptador', '=', lect_vals['comptador']),
            ('periode', '=', lect_vals['periode'])
        ]

        if lect_vals['tipus'] in ['A', 'R', 'S']:
            model = 'giscedata.lectures.lectura.pool'
            search_params.append(('tipus', '=', lect_vals['tipus']))
        else:
            model = 'giscedata.lectures.potencia.pool'

        lect_pool = self.pool.get(model)
        lect_id = lect_pool.search(
            cursor, uid, search_params, context={'active_test': False}
        )
        if lect_id:
            return True, lect_id[0]
        else:
            return False, None

    def import_reading(self, cursor, uid, lect_vals, rectificadora=False,
                       overwrite_atr=False, context=None):
        if context is None:
            context = {}

        lect_vals = lect_vals.copy()
        tipus = lect_vals['tipus']
        if tipus in ['A', 'R', 'S']:
            lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        else:
            lect_obj = self.pool.get('giscedata.lectures.potencia.pool')
        origen_comer_obj = self.pool.get('giscedata.lectures.origen_comer')
        compt_obj = self.pool.get('giscedata.lectures.comptador')

        if tipus == 'S':
            return self.import_reading_sortint(cursor, uid, lect_vals, rectificadora=rectificadora, context=context)
        elif tipus == 'A' and context.get('lect_point', 'inicial') == 'final' and context.get('ajust_balancejat'):
            nou_ajust = context.get('ajust_balancejat')
            lect_vals['ajust'] = nou_ajust.ajuste_por_integrador
            lect_vals['motiu_ajust'] = nou_ajust.codigo_motivo

        existeix, lect_id = self.check_reading_exists(
            cursor, uid, lect_vals, context
        )

        del lect_vals['consum']
        if 'magnitud' in lect_vals:
            del lect_vals['magnitud']
        if lect_vals['tipus'] not in ['A', 'R']:
            del lect_vals['tipus']
        if not existeix:
            lect_id = lect_obj.create(
                cursor, uid, lect_vals, context
            )
        elif existeix:
            overwrite_existing = False
            overwrite_message = ''
            check_ajust = context.get('lect_point', 'inicial') == 'final'

            read_fields = ['origen_comer_id', 'observacions', 'lectura', 'ajust']
            lect_ini_vals = lect_obj.read(
                cursor, uid, lect_id, read_fields
            )

            if rectificadora:
                overwrite_existing = True
                overwrite_message = _(
                    'Valor de lectura sobreescrit pel valor d\'una '
                    'rectificadora. '
                    'Valor antic: {0}. Valor nou: {1}'
                ).format(lect_ini_vals['lectura'], lect_vals['lectura'])
            elif overwrite_atr:
                ini_origen_comer_id = lect_ini_vals['origen_comer_id'][0]
                ini_origen_comer_code = origen_comer_obj.read(
                    cursor, uid, ini_origen_comer_id, ['codi']
                )['codi']

                new_origen_comer_id = lect_vals['origen_comer_id']

                ini_has_at_origen = ini_origen_comer_code == 'AT'
                new_origen_is_diff = new_origen_comer_id != ini_origen_comer_id
                if ini_has_at_origen and new_origen_is_diff:
                    overwrite_existing = True
                    overwrite_message = _(
                        'Valor de lectura de gestió ATR sobreescrit '
                        'pel valor d\'un F1. '
                        'Valor antic: {0}. Valor nou: {1}'
                    ).format(lect_ini_vals['lectura'], lect_vals['lectura'])

            # If we have to overwrite the existing reading
            if overwrite_existing:
                obs = lect_ini_vals['observacions'] or ''
                if obs:
                    obs += '\n'
                lect_vals['observacions'] = obs + overwrite_message
                lect_obj.write(
                    cursor, uid, [lect_id], lect_vals, context
                )
            else:
                # We check if the current reading has the same value we would
                # import. If it is the same we don't do anything. If it's
                # different we give a warning
                lect_imp = lect_vals['lectura']
                lect_imp_aj = lect_vals.get('ajust', 0)
                lect_ori = lect_ini_vals['lectura']
                lect_ori_aj = lect_ini_vals.get('ajust', 0)
                if tipus in ['A', 'R']:
                    # If they are of energy type they cannot have decimals, so
                    # if they do we ignore them
                    lect_imp = int(lect_imp)
                    lect_ori = int(lect_ori)

                if lect_imp != lect_ori \
                        or (check_ajust and (lect_ori_aj != lect_imp_aj)):
                    compt_vals = compt_obj.read(
                        cursor, uid, lect_vals['comptador'], ['name']
                    )
                    msg = _(
                        u"Divergència en el valor de lectura existent."
                        u" Comptador: %s Data: %s. Període: %s. Tipus: %s"
                        u" valor: XML: %s (ajust: %s) BBDD:%s (ajust: %s)."
                    ) % (
                        compt_vals['name'], lect_vals['name'],
                        lect_vals['periode'], tipus, lect_imp, lect_imp_aj,
                        lect_ori, lect_ori_aj
                    )
                    raise osv.except_osv('Error', msg)
        return lect_id

    def import_reading_sortint(self, cursor, uid, lect_vals, rectificadora=False, context=None):
        if context is None:
            context = {}

        lect_obj = self.pool.get('giscedata.lectures.lectura.pool')
        compt_obj = self.pool.get('giscedata.lectures.comptador')
        lect_vals = lect_vals.copy()
        tipus = lect_vals['tipus']
        if tipus != 'S':
            raise Exception("Error", "Aquest s'ha de proporcionar una lectura de exportacio")

        del lect_vals['consum']
        del lect_vals['magnitud']
        lect_vals['tipus'] = 'A'

        existeix, existing_reading_id = self.check_reading_exists(
            cursor, uid, lect_vals, context
        )

        # Cas 2.4 - Si no n'hi ha cap crea una nova lectura amb els valors de lectura_sortint i ajust_sortint corresponents.
        # Com a valors de lectura_entrant es posen els valors que tingui la lectura anterior.
        if not existing_reading_id:
            lect_id = lect_obj.create_lectura_sortint(
                cursor, uid, lect_vals, context
            )
            return lect_id

        # Cas 2.3 - Si ja n'hi ha i el valor de lectura_sortint es 0 escriu els valors de lectura_sortint i ajust_sortint
        read_fields = ['origen_comer_id', 'observacions', 'lectura_exporta', 'ajust_exporta']
        lect_ini_vals = lect_obj.read(
            cursor, uid, existing_reading_id, read_fields
        )
        if not lect_ini_vals['lectura_exporta'] and not lect_ini_vals['ajust_exporta']:
            lect_obj.afegir_lectura_sortint_a_lectura(
                cursor, uid, existing_reading_id, lect_vals, context
            )
            return existing_reading_id

        # Cas 2.2 - Si ja n'hi ha una pero estem en una rectificadora, es sobreescriuen els valor se la lectura_exporta
        if rectificadora:
            overwrite_message = _(
                'Valor de lectura sortint sobreescrit pel valor d\'una '
                'rectificadora. '
                'Valor antic: {0}. Valor nou: {1}'
            ).format(lect_ini_vals['lectura_exporta'], lect_vals['lectura'])

            obs = lect_ini_vals['observacions'] or ''
            if obs:
                obs += '\n'
            lect_vals['observacions'] = obs + overwrite_message
            lect_obj.afegir_lectura_sortint_a_lectura(
                cursor, uid, existing_reading_id, lect_vals, context
            )
            return existing_reading_id

        # Cas 2.1 - Si ja n'hi ha una pero el seu valor de lectura_sortint es != 0 falla per divergencia de lectures
        check_ajust = context.get('lect_point', 'inicial') == 'final'
        lect_imp = lect_vals['lectura']
        lect_imp_aj = lect_vals.get('ajust', 0)
        lect_ori = lect_ini_vals['lectura_exporta']
        lect_ori_aj = lect_ini_vals.get('ajust_exporta', 0)

        if lect_imp != lect_ori or (check_ajust and (lect_ori_aj != lect_imp_aj)):
            compt_vals = compt_obj.read(
                cursor, uid, lect_vals['comptador'], ['name']
            )
            msg = _(
                u"Divergència en el valor de lectura existent."
                u" Comptador: %s Data: %s. Període: %s. Tipus: %s"
                u" valor: XML: %s (ajust: %s) BBDD:%s (ajust: %s)."
            ) % (
                compt_vals['name'], lect_vals['name'],
                lect_vals['periode'], tipus, lect_imp, lect_imp_aj,
                lect_ori, lect_ori_aj
            )
            raise osv.except_osv('Error', msg)
        return None

    def esborrar_lectures(self, cursor, uid, lect, context=None):
        """Esborrar les lectures"""
        lect_ene = self.pool.get('giscedata.lectures.lectura.pool')
        lect_pot = self.pool.get('giscedata.lectures.potencia.pool')
        for i in lect:
            if i[0] in ['A', 'R', 'S']:
                lect_ene.unlink(cursor, uid, [i[1]], context)
            else:
                lect_pot.unlink(cursor, uid, [i[1]], context)

    def carregat_en_dic_lectures(self, cursor, uid, lect, dic,
                                 compt, tarifa, ordre, en_baixa=False):
        """Comprova si existeix la lectura al diccionari i per tant
           ja s'ha esbrinat amb antel·lació si calia carregar-la"""
        if ordre == 'inicial':
            data = lect.data_lectura_inicial
        elif ordre == 'final':
            data = lect.data_lectura_final
        for x in dic[lect.tipus]:
            if isinstance(dic[lect.tipus][x], dict):
                for y in dic[lect.tipus][x]:
                    for z in dic[lect.tipus][x][y]:
                        periode = self.get_id_periode(
                            cursor, uid, lect, tarifa, en_baixa=en_baixa)
                        checks = [('name', data),
                                  ('comptador', compt),
                                  ('periode', periode)]
                        if lect.tipus in ['A', 'R']:
                            checks.append(('tipus', lect.tipus))
                        if all([z[0][j] == k for j, k in checks]):
                            return True
        return False

    def get_reading_model_id(self, cursor, uid, compt_id, data_lectura, periode,
                             tipus, tarifa_id, context=None):
        if context is None:
            context = {}

        search_params = [
            ('periode.name', '=', periode),
            ('name', '=', data_lectura),
            ('comptador', '=', compt_id),
            ('periode.tarifa', '=', tarifa_id)
        ]

        if tipus in ('A', 'R'):
            # If we are searching for energy readings
            model = 'giscedata.lectures.lectura.pool'
            search_params.append(('tipus', '=', tipus))
        else:
            # If we are searching for power readings
            model = 'giscedata.lectures.potencia.pool'
        lect_obj = self.pool.get(model)

        lectura_ids = lect_obj.search(cursor, uid, search_params)

        if not lectura_ids:
            return None
        if len(lectura_ids) != 1:
            raise osv.except_osv(
                'Error',
                _(
                    u"Hi ha més d'una lectura per el comptador {0}, "
                    u"en data {1}, periode {2}, tipus {3}"
                ).format(compt_id, data_lectura, periode, tipus)
            )
        res = {'model': model, 'id': lectura_ids[0]}
        return res

    def check_existeix_lectura(self, cursor, uid, compt, lect, tarifa, ordre,
                               en_baixa=False, context=None):
        """ Es comprova si la lectura ja existeix a la bd
        """
        if context.get('tipus_rectificadora', 'N') in ('R', 'RA'):
            rectificadora = True
        else:
            rectificadora = False
        if ordre == 'inicial':
            data = lect.data_lectura_inicial
            valor = lect.valor_lectura_inicial
        else:
            data = lect.data_lectura_final
            valor = lect.valor_lectura_final
        valor = self.set_uom_lectura(cursor, uid, valor, lect.tipus, compt,
                                     context)
        periode = self.get_id_periode(
            cursor, uid, lect, tarifa, en_baixa=en_baixa)
        if lect.tipus in ['A', 'R']:
            model = 'giscedata.lectures.lectura.pool'
            search_params = [('name', '=', data),
                             ('comptador', '=', compt),
                             ('tipus', '=', lect.tipus),
                             ('periode.id', '=', periode)]
        else:
            # Protecció maxímetre en watts
            if 0 < valor < 1:
                meter_obj = self.pool.get('giscedata.lectures.comptador')
                meter = meter_obj.browse(cursor, uid, compt, context=context)
                power = meter.polissa.potencia
                if 1 < (valor * 1000.0) / power < 2:
                    valor *= 1000
            elif valor >= 1000:
                valor /= 1000.0
            model = 'giscedata.lectures.potencia.pool'
            search_params = [('name', '=', data),
                             ('comptador', '=', compt),
                             ('periode.id', '=', periode)]

        lect_pool = self.pool.get(model)
        lect_id = lect_pool.search(
            cursor, uid, search_params, context={'active_test': False}
        )
        if lect_id and not rectificadora:
            lect_bw = lect_pool.browse(cursor, uid, lect_id[0])
            if valor != lect_bw.lectura:
                energy_reading = lect.tipus in ['A', 'R']
                if energy_reading and int(valor) == int(lect_bw.lectura):
                    return lect_id
                else:
                    c_pool = self.pool.get('giscedata.lectures.comptador')
                    c_obj = c_pool.browse(cursor, uid, compt)
                    _msg = _(
                        u"Divergència en el valor de lectura existent."
                        u" Comptador: %s Data: %s. Període: %s. Tipus: %s"
                        u" valor: XML: %s BBDD:%s"
                    ) % (
                        c_obj.name, data, lect.periode, lect.tipus, valor,
                        lect_bw.lectura
                    )
                    raise osv.except_osv('Error', _msg)
            return lect_id
        elif lect_id and rectificadora:
            return lect_id
        else:
            return None

    def apilar_lectura(self, dic, periode, vals, existeix):
        """Afegeix la lectura al diccionari ordenada per data
        """
        if not periode in dic:
            dic.update({periode: [(vals, existeix)]})
            return

        date_type = '%Y-%m-%d'
        n_dat = datetime.strptime(vals['name'], date_type)
        pos = 0
        while (pos < len(dic[periode]) and
                 datetime.strptime(dic[periode][pos][0]['name'], date_type)
                                                                    < n_dat):
            pos += 1
        dic[periode].insert(pos, (vals, existeix))

    def processar_lectures(self, cursor, uid, lectures, compt, tarifa,
                           en_baixa=False, check=True, context=None):
        """Retorna un diccionari amb la informació de les lectures amb el format
           següent:
           {tipus: {temporalitat: {periode: [(valors, existeix), ...]}}}
           on:
                tipus: A, R, M
                temporalitat: inicial, final
                periode: P1, ... P6
                valors: objecte retornat per self.vals_lectura()
                existeix: 0 (no existeix la lectura), 1 (ja existeix la lectura)

          El tipus M, pot contenir informació d'EP en el cas d'existir excessos
          de potència pel periode.
        """
        tipus = ''
        dic = {}
        proc_tipus = []
        pos_ep = []
        for i, lect in enumerate(lectures):
            if lect.ometre:
                continue
            if lect.tipus == 'EP':
                pos_ep.append(i)
                tipus = lect.tipus
                proc_tipus.append(lect.tipus)
                continue
            if lect.tipus != tipus:
                tipus = lect.tipus
                if not tipus in proc_tipus:
                    proc_tipus.append(lect.tipus)
                    dic.update({tipus: {'inicial': {}, 'final': {}}})
            # lectura final
            existeix = 1
            if not self.carregat_en_dic_lectures(
                    cursor, uid, lect, dic, compt, tarifa, 'final',
                    en_baixa=en_baixa):
                if check:
                    lect_id = self.check_existeix_lectura(
                        cursor, uid, compt, lect, tarifa, 'final',
                        en_baixa=en_baixa, context=context)
                    if not lect_id:
                        existeix = 0
                    else:
                        existeix = lect_id[0]
            vals = self.vals_lectura(
                cursor, uid, lect, 'final', compt,
                tarifa=tarifa, en_baixa=en_baixa, context=context)
            self.apilar_lectura(dic[tipus]['final'], lect.periode,
                                vals, existeix)
            dic[tipus]['magnitud'] = lect.magnitud
            # no es processen les lectures inicials de potència
            if tipus not in ['A', 'R']:
                continue
            # lectura inicial
            existeix = 1
            if not self.carregat_en_dic_lectures(
                    cursor, uid, lect, dic, compt, tarifa, 'inicial',
                    en_baixa=en_baixa):
                if check:
                    lect_id = self.check_existeix_lectura(
                        cursor, uid, compt, lect, tarifa, 'inicial',
                        en_baixa=en_baixa, context=context)
                    if not lect_id:
                        existeix = 0
                    else:
                        existeix = lect_id[0]
            vals = self.vals_lectura(
                cursor, uid, lect, 'inicial', compt,
                tarifa=tarifa, en_baixa=en_baixa, context=context)
            self.apilar_lectura(dic[tipus]['inicial'], lect.periode,
                                    vals, existeix)
        if not len(pos_ep):
            return dic
        elif 'M' not in dic:
            # Some files includes 'EP' measures but not 'M' measures
            dic.update({'M': {'inicial': {}, 'final': {}}})
        for i in pos_ep:
            lect = lectures[i]
            periode = lect.periode
            # Some files includes 'EP' measures but not 'PM' measures
            # final
            vals = self.vals_lectura(
                cursor, uid, lect, 'final', compt,
                tarifa=tarifa, en_baixa=en_baixa, context=context)
            existeix = 0
            if periode not in dic['M']['final']:
                vals['tipus'] = 'M'
                self.apilar_lectura(dic['M']['final'], periode, vals,
                                    existeix)
            else:
                vals = {'exces': vals['exces']}
                dic['M']['final'][periode][0].update(vals)
        return dic

    def get_partner_id(self, cursor, uid, emisor):
        """Obtenir el partner_id de l'emisor"""
        p_pool = self.pool.get('res.partner')
        p_id = p_pool.search(cursor, uid, [('ref', '=', emisor)])
        return p_id

    def cerca_dates_lect(self, lect_data, dates_lect):
        """Actualitza 'dates' amb les dates inici i final de lectures
           d'energia"""
        dates = dates_lect.copy()
        if not 'inicial' in dates:
            dates.update({'inicial': '', 'final': ''})
        if not lect_data:
            return {}
        d_ini = dates['inicial']
        d_fi = dates['final']
        magnituds = ['A', 'R']
        for mag in magnituds:
            if not mag in lect_data:
                continue
            for periode in lect_data[mag]['inicial']:
                for lect in lect_data[mag]['inicial'][periode]:
                    if d_ini:
                        d_ini = min(d_ini, lect[0]['name'])
                    else:
                        d_ini = lect[0]['name']
                for lect in lect_data[mag]['final'][periode]:
                    d_fi = max(d_fi, lect[0]['name'])
        if d_ini:
            dates['inicial'] = d_ini
        if d_fi:
            dates['final'] = d_fi
        return dates

    def check_dh_comptador(self, tarifa, cdh, context=None):
        """ Comprova si CodigoDH correspon a la tarifa de la pólissa"""
        #Cas CodigoDH=0
        if cdh == '0' and codi_dh(tarifa) == '2':
            return
        cdh_tarifa = codi_dh(tarifa)
        if cdh_tarifa != cdh:
            msg = (_(u"El codi de Discriminació Horària (%s) no correspon a "
                     u"la tarifa de la pólissa. Repassi que la pólissa "
                     u"tingui la tarifa correcte") % cdh)
            raise osv.except_osv('Error', _(msg))

    def move_meters_of_contract(self, cursor, uid, old_contract, new_contract, limit_date, context=None):
        """
            Copy meters which have readings after limit_date from old contract to new contract.
            Readings after limit_date are moved to meters of new contract (they are deleted from old meter).
            If a meter in old_contract is left without any reading, it's deleted.
        """
        if not context:
            context = {}
        lpool_obj = self.pool.get('giscedata.lectures.lectura.pool')
        lpotpool_obj = self.pool.get('giscedata.lectures.potencia.pool')
        meter_obj = self.pool.get("giscedata.lectures.comptador")
        new_meters = []
        for meter in old_contract.comptadors:
            # If meter have lects (not in pool) after limit_date we can't move it
            after_limit_lect = [l for l in meter.lectures if l.name > limit_date]
            if len(after_limit_lect):
                inf = _(u"No es pot moure el comptador {0} de la pólissa {1} "
                        u"perqué hi ha lectures facturades (no son del pool) "
                        u"posteriors al {2}").format(meter.name, old_contract.name, limit_date)
                raise osv.except_osv(_(u'ERROR'), inf)
            after_limit_lect = [l for l in meter.lectures_pot if l.name > limit_date]
            if len(after_limit_lect):
                inf = _(u"No es pot moure el comptador {0} de la pólissa {1} "
                        u"perqué hi ha lectures de poténcia facturades (no son "
                        u"del pool) posteriors al {2}").format(meter.name, old_contract.name, limit_date)
                raise osv.except_osv(_(u'ERROR'), inf)

            # Check if we need to work with this meter
            pool_lects_to_move = [l for l in meter.pool_lectures if l.name >= limit_date]
            pool_lects_pot_to_move = [l for l in meter.pool_lectures_pot if l.name >= limit_date]
            if not len(pool_lects_pot_to_move) and not len(pool_lects_to_move):
                continue

            # Create new meter in new_contract using old_meter vals
            limit_date_after = (datetime.strptime(limit_date, "%Y-%m-%d") + timedelta(days=1)).strftime("%Y-%m-%d")
            new_meter_vals = meter_obj.copy_data(cursor, uid, meter.id, context=context)[0]
            new_meter_vals.update({
                'polissa': new_contract.id,
                'data_alta': limit_date_after,
                'active': True,
                'lectures': [],
                'lectures_pot': [],
                'pool_lectures': [],
                'pool_lectures_pot': []
            })
            new_meter_id = meter_obj.create(cursor, uid, new_meter_vals)
            new_meters.append(new_meter_id)

            for l in pool_lects_to_move:
                if l.name > limit_date:  # Move to new meter
                    l.write({'comptador': new_meter_id})
                elif l.name == limit_date:  # Init. lect. must be in both meters
                    new_lect_vals = lpool_obj.copy_data(cursor, uid, l.id, context=context)[0]
                    new_lect_vals.update({'comptador': new_meter_id})
                    lpool_obj.create(cursor, uid, new_lect_vals)

            for l in pool_lects_pot_to_move:
                if l.name > limit_date:  # Move to new meter
                    l.write({'comptador': new_meter_id})
                elif l.name == limit_date:  # Init. lect. must be in both meters
                    new_lect_vals = lpotpool_obj.copy_data(cursor, uid, l.id, context=context)[0]
                    new_lect_vals.update({'comptador': new_meter_id})
                    lpotpool_obj.create(cursor, uid, new_lect_vals)

            # Deactivate old meter
            meter.write({
                'active': False,
                'data_baixa': limit_date
            })

        return new_meters

GiscedataLecturesSwitchingHelper()


class GiscedataLecturesComptador(osv.osv):
    """Classe per importar Q1
    """
    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'


    def from_xml(self, cursor, uid, xml, emisor_lot, context=None):
        """Importa les lectures de comptador.

           La primera variable de retorn, conté el missatge de l'excepció de
           la llibreria de switching en el cas que l'xml sigui invàlid o bé,
           en el cas de ser vàlid,  un diccionari amb la referència de l'xml
           per clau i en el valor els resultats de l'importació:
           - Lectures errònies:
               {<ref>: [False, <msg d'error>, <b64>]}
           - Lectures vàlides:
               {<ref>: [True, <msg> , [(tipus, lectura_id), ...], <b64>]}
           Tal que:
                <ref> =  CodigoDeSolicitud | SecuencialDeSolicitud
                <b64> = xml en base64
        """
        if context is None:
            context = {}

        db = pooler.get_db_only(cursor.dbname)
        sw = self.pool.get('giscedata.lectures.switching.helper')
        pol = self.pool.get('giscedata.polissa')
        try:
            q1_xml = Q1(xml, 'Q1')
            q1_xml.parse_xml()
            xml_b64 = base64.encodestring(xml)
            emisor = q1_xml.get_codi_emisor
            cups = q1_xml.get_codi
            codi_cups_distri = cups[2:6]
            ref = '%s%s' % (q1_xml.codi_sollicitud, q1_xml.seq_sollicitud)
        except message.except_f1, e:
            return e.value, ''
        if emisor != emisor_lot:
            emisor = codi_cups_distri
        if emisor != emisor_lot and emisor_lot != '':
            return _("L'Empresa emisora no correspon a l'emisor del lot"), ''
        res = {}
        n_lect = 0
        lect_id = []
        cr = db.cursor()
        try:
            comptadors = q1_xml.get_comptadors()
            if not comptadors:
                raise osv.except_osv('Error', _("Fitxer sense lectures"))
            context['num_comptadors'] = len(comptadors)
            for cmpt in comptadors:
                lectures = cmpt.get_lectures()
                if not lectures:
                    raise osv.except_osv('Error',
                                         _("Comptador %s sense lectures" %
                                            cmpt.nom_comptador))
                n_lect += len(lectures)
                data_inici = lectures[0].data_lectura_inicial
                data_final = lectures[0].data_lectura_final
                dict_lect = Q1.agrupar_lectures_per_data(lectures)
                data_inici_l, data_final_l = Q1.\
                                      obtenir_data_inici_i_final(dict_lect)
                polissa_id = sw.find_polissa(cr, uid, cups, data_inici_l,
                                                                data_final_l)
                sw.check_emisor_polissa(cr, uid, polissa_id, emisor)
                compt = sw.check_comptador(cr, uid, cmpt.nom_comptador,
                                           cmpt.gir_comptador, polissa_id,
                                           data_inici_l, data_final_l,
                                           context=context)
                ctxt = context.copy()
                for data_lect in [data_inici_l, data_final_l]:
                    # Check that file tariff and contract tariff are the same
                    ctxt.update({'date': data_lect})
                    p_br = pol.browse(cr, uid, polissa_id, context=ctxt)
                    sw.check_dh_comptador(p_br.tarifa.codi_ocsum, cmpt.codiDH)
                en_baixa = False
                if 'LB' in p_br.tarifa.name:
                    en_baixa = True
                lect_data = sw.processar_lectures(
                    cr, uid, lectures, compt, p_br.tarifa.codi_ocsum,
                    en_baixa=en_baixa, check=True, context=context
                )
                lect_id += sw.importar_lectures_compt(cr, uid, lect_data, context)
            cr.commit()
            msg = 'Lectures existents: %d' % n_lect
            res.update({ref: [True, msg, lect_id, xml_b64]})
        except (osv.except_osv, orm.except_orm, message.except_f1), e:
            cr.rollback()
            _info = e.value if isinstance(e.value, unicode) else \
                                                       unicode(e.value, 'utf8')
            res.update({ref: [False, _info, xml_b64]})
        except Exception as exc:
            traceback.print_exc()
            cr.rollback()
            _info = exc[0] if isinstance(exc[0], unicode) else \
                                                        unicode(exc[0], 'utf8')
            res.update({ref: [False, _info, xml_b64]})
        finally:
            cr.close()
        return res, emisor

    def from_zip(self, cursor, uid, data, emisor_lot=None, context=None):
        """Processar un zip de fitxers xml.

           La funció retorna en la variable val el diccionari en
           que les claus són els noms dels fitxers que conté el
           zip i els valors el resultat de la funció from_xml()
        """
        imp = self.pool.get('giscedata.lectures.importacio')
        cups_obj = self.pool.get('giscedata.cups.ps')
        if not context:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        _data = base64.decodestring(data)
        fileHandle = StringIO.StringIO(_data)
        try:
            zfile = zipfile.ZipFile(fileHandle, "r")
        except zipfile.BadZipfile, e:
            raise osv.except_osv('Error', 'El fitxer no es un fitxer zip')
        val = {}
        emisor = emisor_lot and emisor_lot or ''
        i_id = context.get('importacio_id')
        pb_total = len(zfile.infolist())
        pbcr = db.cursor()
        try:
            for i, info in enumerate(zfile.infolist()):
                prog = i * 100.0 / pb_total
                imp.write(pbcr, uid, i_id, {'progres': prog})
                pbcr.commit()
                fname = info.filename
                xml = zfile.read(fname)
                cups_id = ''
                #Agafem el CUPS per registrar-lo a la bd
                try:
                    q1_xml = Q1(xml, 'Q1')
                    q1_xml.parse_xml()
                    cups = q1_xml.get_codi[:20]
                    cupses = cups_obj.search(cursor, uid, [('name', 'like', cups)],
                                             context={'active_test': False})
                    if cupses:
                        cups_id = cupses[0]
                except message.except_f1, e:
                    pass
                res, e_fact = self.from_xml(cursor, uid, xml, emisor, context)
                retorn = {'res': res, 'cups_id': cups_id}
                if not emisor:
                    emisor = e_fact
                val.update({basename(fname): retorn})
        finally:
            pbcr.close()
        return val, emisor


GiscedataLecturesComptador()
