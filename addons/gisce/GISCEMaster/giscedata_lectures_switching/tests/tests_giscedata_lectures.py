# -*- coding: utf-8 -*-
import unittest
from addons import get_module_resource
from destral import testing
from destral.transaction import Transaction
from tools.misc import cache


class TestImporting(testing.OOTestCase):
    def switch(self, txn, where):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')
        partner_obj = self.openerp.pool.get('res.partner')
        cups_obj = self.openerp.pool.get('giscedata.cups.ps')
        partner_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'main_partner'
        )[1]
        other_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_agrolait'
        )[1]
        another_id = imd_obj.get_object_reference(
            cursor, uid, 'base', 'res_partner_c2c'
        )[1]
        codes = {'distri': '1234', 'comer': '4321'}
        partner_obj.write(cursor, uid, [partner_id], {
            'ref': codes.pop(where)
        })
        partner_obj.write(cursor, uid, [other_id], {
            'ref': codes.values()[0]
        })
        partner_obj.write(cursor, uid, [another_id], {
            'ref': '5555'
        })
        cups_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_cups', 'cups_01'
        )[1]
        distri_ids = {'distri': partner_id, 'comer': other_id}
        cups_obj.write(cursor, uid, [cups_id], {
            'distribuidora_id': distri_ids[where]
        })
        cache.clean_caches_for_db(cursor.dbname)

    def activar_polissa_CUPS(self, txn):
        cursor = txn.cursor
        uid = txn.user
        imd_obj = self.openerp.pool.get('ir.model.data')

        polissa_obj = self.openerp.pool.get('giscedata.polissa')

        polissa_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )[1]

        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

    @unittest.skip("Won't work due to cursor change in from_xml")
    def test_q1_from_xml_imports_adjustments(self):
        lect_compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')

        q1_xml_path = get_module_resource(
            'giscedata_lectures_switching', 'tests', 'fixtures', 'Q1_ajuste.xml'
        )
        with open(q1_xml_path, 'r') as f:
            q1_xml = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.switch(txn, 'comer')
            self.activar_polissa_CUPS(txn)

            res = lect_compt_obj.from_xml(cursor, uid, q1_xml, '1234')

            search_params = [
                ('polissa.cups.name', '=', u'ES1234000000000001JN0F'),
            ]
            lect_compt_id = lect_compt_obj.search(cursor, uid, search_params)

            lect_ids = lect_obj.search(
                cursor, uid, [('comptador', 'in', lect_compt_id)]
            )

            assert lect_ids
            params = ['ajust', 'motiu_ajust']
            for lectura in lect_obj.read(cursor, uid, lect_ids, params):
                assert lectura['ajust'] == 678
                assert lectura['motiu_ajust'] == '99'

    @unittest.skip("Won't work due to cursor change in from_xml")
    def test_q1_from_xml_adjustments_are_0_by_default(self):
        lect_compt_obj = self.openerp.pool.get('giscedata.lectures.comptador')
        lect_obj = self.openerp.pool.get('giscedata.lectures.lectura.pool')

        q1_xml_path = get_module_resource(
            'giscedata_lectures_switching', 'tests', 'fixtures', 'Q1.xml'
        )
        with open(q1_xml_path, 'r') as f:
            q1_xml = f.read()

        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user

            self.switch(txn, 'comer')
            self.activar_polissa_CUPS(txn)

            res = lect_compt_obj.from_xml(cursor, uid, q1_xml, '1234')

            search_params = [
                ('polissa.cups.name', '=', u'ES1234000000000001JN0F'),
            ]
            lect_compt_id = lect_compt_obj.search(cursor, uid, search_params)

            lect_ids = lect_obj.search(
                cursor, uid, [('comptador', 'in', lect_compt_id)]
            )

            assert lect_ids
            params = ['ajust', 'motiu_ajust']
            for lectura in lect_obj.read(cursor, uid, lect_ids, params):
                assert lectura['ajust'] == 0
                assert not lectura['motiu_ajust']
