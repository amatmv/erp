# -*- coding: utf-8 -*-
{
    "name": "GISCE Data Expedients Observacions a primera pàgina",
    "description": """Posa les Observacions de l'expedient a primera pàgina""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_expedients"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_expedients_obspripa_view.xml"
    ],
    "active": False,
    "installable": True
}
