# -*- coding: utf-8 -*-
{
    "name": "Obres per Elements BT",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Obres a Elements BT
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_bt",
        "giscedata_obres"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_bt_view.xml"
    ],
    "active": False,
    "installable": True
}
