# -*- coding: utf-8 -*-

from osv import osv, fields


class GiscedataBtElement(osv.osv):
    _name = 'giscedata.bt.element'
    _inherit = 'giscedata.bt.element'

    _columns = {
        'obres': fields.many2many(
            'giscedata.obres.obra',
            'giscedata_obres_obra_bt_element_rel',
            'element_id',
            'obra_id',
            'Obres'
        )
    }

GiscedataBtElement()