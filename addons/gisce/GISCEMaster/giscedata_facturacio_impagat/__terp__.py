# -*- coding: utf-8 -*-
{
    "name": "Pending invoices management",
    "description": """
Pending invoices management:
    * Pending state per invoice
    * Pending amount and worst pending state of polissa associated invoices
    * Configurable pending states
    """,
    "version": "0-dev",
    "author": "Joan M. Grande",
    "category": "Facturació",
    "depends": [
        "giscedata_facturacio",
        "giscedata_switching",
        "impagat_base",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [
        "wizard/wizard_unpay_view.xml",
        "wizard/wizard_antiquity_debt_view.xml",
        "giscedata_facturacio_view.xml",
        "giscedata_polissa_view.xml",
        "giscedata_facturacio_impagat_data.xml",
        "giscedata_polissa_data.xml",
        "giscedata_polissa_workflow.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
