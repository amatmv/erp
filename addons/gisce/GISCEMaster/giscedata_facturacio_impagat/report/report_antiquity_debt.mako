<%
    from babel.numbers import format_currency
%>
<!doctype html>
<html>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<head>
    	<style type="text/css">
body
{
    padding: 5px;
	line-height: 1.6em;
    font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
}

#hor-minimalist-a
{
	font-size: 12px;
	background: #fff;
	margin: 45px;
	border-collapse: collapse;
	text-align: left;
}
#hor-minimalist-a th
{
	font-size: 14px;
	font-weight: normal;
	color: #039;
	padding: 10px 8px;
	border-bottom: 2px solid #6678b1;
}
#hor-minimalist-a td
{
	color: #669;
	padding: 9px 8px 0px 8px;
    min-width: 100px;
}
            #hor-minimalist-a tfoot td
            {
                padding-top: 15px;
                font-weight: bold;
            }

        </style>
</head>
${company.name}
<h1>Antigüedad de la deuda a fecha ${data['form'][0]['date']}</h1>
%if data['between_dates']:
    <h2>Rango de fechas  ${data['start_date']} y ${data['end_date']}</h2>
%endif

<table id="hor-minimalist-a">
    <thead>
        <th></th>
        %for days in ('30', '60', '90', '120', '150', '180', '210', '240', '270', '300', '330', '360', '>360'):
            <th>${days}</th>
        %endfor
    </thead>
    <tfoot>
        <tr>
            <td>Total</td>
            %for days in ('30', '60', '90', '120', '150', '180', '210', '240', '270', '300', '330', '360', '>360'):
            <td>${format_currency(sum(x.get(days, 0) for x in data['debt'].values()), 'EUR', locale='es_ES')}</td>
            %endfor
        </tr>
    </tfoot>
    <tbody>
    %for k, v in data['debt'].items():
        <tr>
            <td>${k}</td>
            %for days in ('30', '60', '90', '120', '150', '180', '210', '240', '270', '300', '330', '360', '>360'):
                <td>${format_currency(v.get(days, 0), 'EUR', locale='es_ES')}</td>
            %endfor
        </tr>
    %endfor
    </tbody>
</table>
<dl>
  %for k, v in data['debt'].items():
  <dt>${k}</dt>
  <dd>${format_currency(sum(v.values()), 'EUR', locale='es_ES')}</dd>
  %endfor
</dl>
</html>