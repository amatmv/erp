# -*- coding: utf-8 -*-
from __future__ import absolute_import

import logging
from osv import osv


class GiscedataSwitchingB1(osv.osv):

    _name = 'giscedata.switching'
    _inherit = 'giscedata.switching'

    def must_cancel_B1(self, cursor, uid, factura_id, state_id, context=None):
        """Check if we can cancel a B1 checking if there are more pending invoices.

        :param cursor: Database cursor
        :param uid: User Id
        :param factura_id: Invoice id
        :param state_id: Next pending state id
        :param context: Context application
        :returns True if we can cancel the process
        """
        if context is None:
            context = {}
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        pstate_obj = self.pool.get('account.invoice.pending.state')
        state = pstate_obj.browse(cursor, uid, state_id, context=context)
        factura = fact_obj.browse(cursor, uid, factura_id, context=context)
        actual_state = factura.pending_state
        ctx = context.copy()
        ctx['active_test'] = False
        if actual_state.is_last and state.weight < actual_state.weight:
            return True
        else:
            return False


    def find_B1(self, cursor, uid, factura_id, step=None, context=None):
        """Find the B1 process

        Only search for process which are in 'open' or 'draft' state and linked
        with the same contract.

        :param cursor: Database cursor
        :param uid: User id
        :param factura_id: Infoice id
        :param step: Step name (optional)
        :param context: Application context
        :returns ids of B1 process
        """
        sw_obj = self.pool.get('giscedata.switching')
        factura_obj = self.pool.get('giscedata.facturacio.factura')
        factura = factura_obj.browse(cursor, uid, factura_id, context=context)
        if factura.group_move_id:
            polissa_id = factura.polissa_id.id
            ref_factura = []
            invoice_ids = [
                l.invoice.id for l in factura.group_move_id.line_id if l.invoice
            ]
            factura_ids = factura_obj.search(cursor, uid, [
                ('invoice_id.id', 'in', invoice_ids),
                ('polissa_id.id', '=', polissa_id)
            ])
            for factura_id in factura_ids:
                ref = 'giscedata.facturacio.factura,{0}'.format(factura_id)
                ref_factura.append(ref)
        else:
            ref_factura = ['giscedata.facturacio.factura,{0}'.format(factura_id)]
        if ref_factura:
            search_params = [
                ('proces_id.name', '=', 'B1'),
                ('state', 'in', ('open', 'draft', 'pending')),
                ('cups_polissa_id.id', '=', factura.polissa_id.id),
                '|',
                ('ref', 'in', ref_factura),
                ('ref2', 'in', ref_factura)
            ]
            if step:
                search_params.append(('step_id.name', '=', step))
            return sw_obj.search(cursor, uid, search_params, context={'active_test': False})
        return []

    def cancel_B1(self, cursor, uid, sw_id, context=None):
        """Cancel the B1 process.

        The steps are the following:
            - If the ATR process in in draft state it can be cancelled directly.
            - If ATR is ended is not possible to cancel.
            - If ATR is in step 01 and is not marked as sent it can be cancelled
              directly.
            - In other cases step 03 is generated.

        :param cursor: Database cursor.
        :param uid: User identifier.
        :param sw_id: ATR Case id.
        :param context: Application context.
        :returns True if has been cancelled or False if not.
        """
        if context is None:
            context = {}
        logger = logging.getLogger('openerp.{0}.cancel_B1'.format(__name__))
        sw_obj = self.pool.get('giscedata.switching')
        swpas_obj = self.pool.get('giscedata.switching.step')
        swinfo_obj = self.pool.get('giscedata.switching.step.info')
        atr = sw_obj.browse(cursor, uid, sw_id, context=context)
        if atr.state == 'draft':
            logger.info('ATR (id: {atr.id}) in draft cancel direct'.format(
                atr=atr
            ))
            atr.case_id.case_cancel()
            return True
        if atr.finalitzat:
            logger.info(
                'ATR (id: {atr.id}) is ended. Is not possible to cancel'.format(
                    atr=atr
            ))
            return False
        if atr.step_id.name == '01' and atr.enviament_pendent:
            logger.info(
                'ATR (id: {atr.id}) is in step 01 and is not sended. '
                'Cancel direct'.format(
                    atr=atr
                )
            )
            atr.case_id.case_cancel()
            return True
        else:
            logger.info(
                'ATR (id: {atr.id}) is running we have to generate step 03'.format(
                    atr=atr
                ))
            pas_id = swpas_obj.get_step(cursor, uid, '03', 'B1')
            # Creant info ja crea automaticament tota la info del pas
            info_vals = {
                'sw_id': atr.id,
                'proces_id': atr.proces_id.id,
                'step_id': pas_id,
            }
            swinfo_obj.create(cursor, uid, info_vals)
            return True

GiscedataSwitchingB1()

