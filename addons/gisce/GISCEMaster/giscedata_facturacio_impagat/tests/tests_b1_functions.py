# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction

from giscedata_facturacio_impagat.giscedata_facturacio import find_B1


class TestB1Functions(testing.OOTestCase):
    def test_find_b1(self):
        sw_obj = self.openerp.pool.get('giscedata.switching')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Enable contact query
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            found = sw_obj.find_B1(cursor, uid, factura_id)
            assert not found

    def test_wrapper_find_b1_equal(self):
        sw_obj = self.openerp.pool.get('giscedata.switching')
        imd_obj = self.openerp.pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Enable contact query
            factura_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0001'
            )[1]
            found = sw_obj.find_B1(cursor, uid, factura_id)
            assert not found
            found_b1 = find_B1(cursor, uid, factura_id)
            assert found == found_b1
