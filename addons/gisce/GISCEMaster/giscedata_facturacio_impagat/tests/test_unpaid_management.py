# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from destral import testing
from destral.transaction import Transaction
from osv.orm import ValidateException
from osv.osv import except_osv

from giscedata_switching.tests import TestSwitchingImport
from giscedata_facturacio.tests.utils import prepare_fiscal_year

from workalendar.europe import Spain
from collections import namedtuple
from datetime import date

import netsvc


class TestUnpaidManagement(testing.OOTestCase):
    def test_go_on_pending_with_extra_line(self):
        """Test pending state from invoice is going on.
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pstate_obj = pool.get('account.invoice.pending.state')
        extra_obj = pool.get('giscedata.facturacio.extra')
        fact_obj = pool.get('giscedata.facturacio.factura')
        config_obj = pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            # Preprare fiscal year

            prepare_fiscal_year(pool, cursor, uid)

            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0007'
            )
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            # configure unpaid management
            config_unpaid_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_impagat',
                'config_unpaid_management_expenses')[1]
            config_obj.write(cursor, uid, [config_unpaid_id], {'value': '1'})

            invoice_id = fact_obj.copy(cursor, uid, ref)
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice.invoice_id.id, 'invoice_open', cursor
            )
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.state, 'open')
            invoice.set_pending(correct_state_id)
            # Refresh invoice object
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, correct_state_id)

            id_last = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            invoice.set_pending(id_last)
            # Refresh invoice object
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, id_last)
            unpayment_fee_product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_impagat',
                'product_unpaid_management')[1]
            extra_lines = extra_obj.search(
                cursor, uid, [('product_id', '=', unpayment_fee_product_id)]
            )
            self.assertIs(1, len(extra_lines))

    def test_go_on_pending_without_extra_line(self):
        """Test pending state from invoice is going on.
        """
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        pstate_obj = pool.get('account.invoice.pending.state')
        extra_obj = pool.get('giscedata.facturacio.extra')
        fact_obj = pool.get('giscedata.facturacio.factura')
        config_obj = pool.get('res.config')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            prepare_fiscal_year(pool, cursor, uid)
            model, ref = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio', 'factura_0007'
            )
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            # without configure unpaid management
            config_unpaid_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_impagat',
                'config_unpaid_management_expenses')[1]
            config_obj.write(cursor, uid, [config_unpaid_id], {'value': '0'})

            invoice_id = fact_obj.copy(cursor, uid, ref)
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', invoice.invoice_id.id, 'invoice_open',
                cursor
            )
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.state, 'open')
            invoice.set_pending(correct_state_id)
            # Refresh invoice object
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, correct_state_id)

            id_last = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            invoice.set_pending(id_last)
            # Refresh invoice object
            invoice = fact_obj.browse(cursor, uid, invoice_id)
            self.assertEqual(invoice.pending_state.id, id_last)
            unpayment_fee_product_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_facturacio_impagat',
                'product_unpaid_management')[1]
            extra_lines = extra_obj.search(
                cursor, uid, [('product_id', '=', unpayment_fee_product_id)]
            )
            self.assertIs(0, len(extra_lines))


class TestUnpaidManagementB1Polissa(testing.OOTestCase):

    def clear_cache_pending(self, txn):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        account_pending_obj = pool.get('account.invoice.pending.state')
        cursor = txn.cursor
        account_pending_obj.get_next.clear_cache(cursor.dbname)
        polissa_obj._get_pending_states.clear_cache(cursor.dbname)

    def calc_action_day(self, start_day):
        not_allowed_days = [4, 5, 6]
        allowed_days = [0, 1, 2, 3]
        allowed_days = list(set(allowed_days) - set(not_allowed_days))
        calendar = Spain()
        action_day = start_day
        while action_day.weekday() not in allowed_days:
            action_day = calendar.add_working_days(
                action_day, 1)
        return action_day

    def prepare_for_test(self, txn):
        pool = self.openerp.pool
        polissa_obj = pool.get('giscedata.polissa')
        imd_obj = pool.get('ir.model.data')

        cursor = txn.cursor
        uid = txn.user

        model, ref = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0007'
        )
        # Create a new invoice (giscedata.facturacio.factura)
        fact_obj = pool.get('giscedata.facturacio.factura')
        self.factura_id = fact_obj.copy(cursor, uid, ref)
        factura = fact_obj.browse(cursor, uid, self.factura_id)

        prepare_fiscal_year(pool, cursor, uid)

        partner_obj = pool.get('res.partner')
        partner_obj.write(cursor, uid, [factura.partner_id.id], {
            'ref': '4321'
        })
        partner_obj.write(cursor, uid, [factura.company_id.id], {
            'ref': '1234'
        })
        cups_obj = pool.get('giscedata.cups.ps')
        cups_obj.write(cursor, uid, [factura.cups_id.id], {
            'distribuidora_id': factura.partner_id.id
        })

        # Activate the polissa
        polissa_id = factura.polissa_id.id
        polissa_obj.write(
            cursor, uid, [polissa_id], {'comercialitzadora': 1})
        polissa_obj.send_signal(cursor, uid, [polissa_id], [
            'validar', 'contracte'
        ])

        # Allow to create B1 from
        config_b1_from_pending_state_id = imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio_impagat',
            'config_atr_b1_from_pending_state')[1]
        config_obj = pool.get('res.config')
        config_obj.write(
            cursor, uid, [config_b1_from_pending_state_id], {'value': '1'})

        # Create request link
        rl_obj = pool.get('res.request.link')
        rl_obj.create(cursor, uid, {
            'priority': 30, 'object': 'giscedata.facturacio.factura',
            'name': 'Factura'
        })

    def test_dont_generate_B1_if_not_cutoff(self):
        """Test if it doesn't generate B1 if polissa is not cutoff."""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        fact_obj = pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_test(txn)
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]

            # Create the invoice last pending state
            pstate_obj = pool.get('account.invoice.pending.state')
            last_pending_state_id = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            # Clear cache
            self.clear_cache_pending(txn)
            # Create a new invoice (giscedata.facturacio.factura)
            factura_id = self.factura_id
            factura = fact_obj.browse(cursor, uid, factura_id)

            # Make the polissa no-cutoff
            nocutoff_id = imd_obj.get_object_reference(
                cursor, uid, 'giscedata_polissa', 'd2_nocutoff'
            )[1]
            factura.polissa_id.write({'nocutoff': nocutoff_id})

            # Open the invoice
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', factura.invoice_id.id,
                'invoice_open', cursor
            )
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.state, 'open')

            # Set the invoice pending state to correct
            factura.set_pending(correct_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, correct_state_id)

            # Set the invoice pending state to last
            factura.set_pending(last_pending_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, last_pending_state_id)

            # Check B1 was created for the invoice
            sw_obj = pool.get('giscedata.switching')
            B1_found = sw_obj.find_B1(cursor, uid, factura_id)
            self.assertTrue(len(B1_found) == 0)

    def test_go_on_pending_with_B1_generated(self):
        """Test last pending state from invoice generates a B1 if the
        polissa is active"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        fact_obj = pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_test(txn)
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]

            # Create the invoice last pending state
            pstate_obj = pool.get('account.invoice.pending.state')
            last_pending_state_id = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            # Clear cache
            self.clear_cache_pending(txn)
            # Create a new invoice (giscedata.facturacio.factura)
            factura_id = self.factura_id
            factura = fact_obj.browse(cursor, uid, factura_id)

            # Open the invoice
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', factura.invoice_id.id,
                'invoice_open', cursor
            )
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.state, 'open')

            # Set the invoice pending state to correct
            factura.set_pending(correct_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, correct_state_id)

            # Set the invoice pending state to last
            factura.set_pending(last_pending_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, last_pending_state_id)

            # Check B1 was created for the invoice
            sw_obj = pool.get('giscedata.switching')
            B1_found = sw_obj.find_B1(cursor, uid, factura_id)
            self.assertTrue(len(B1_found) > 0)

    def test_go_on_pending_without_B1_generated(self):
        """Test last pending state from invoice doesn't generate a B1 if the
        polissa is not active"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        fact_obj = pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_test(txn)
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]

            # Create the invoice last pending state
            pstate_obj = pool.get('account.invoice.pending.state')
            last_pending_state_id = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            # Clear cache
            self.clear_cache_pending(txn)
            # Create a new invoice (giscedata.facturacio.factura)
            factura_id = self.factura_id
            factura = fact_obj.browse(cursor, uid, factura_id)

            # Open the invoice
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', factura.invoice_id.id,
                'invoice_open', cursor
            )
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.state, 'open')

            # Set the invoice pending state to correct
            factura.set_pending(correct_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, correct_state_id)

            # Activate the polissa
            polissa_id = factura.polissa_id.id
            polissa_obj = pool.get('giscedata.polissa')
            polissa_obj.write(
                cursor, uid, [polissa_id], {'renovacio_auto': False,
                                            'data_baixa': '2017-01-01'})
            polissa = polissa_obj.browse(cursor, uid, polissa_id)
            for comptador in polissa.comptadors:
                if comptador.active:
                    comptador.write({'active': False})
            polissa_obj.send_signal(cursor, uid, [polissa_id], [
                'baixa', 'contracte'
            ])

            # Set the invoice pending state to last
            factura.set_pending(last_pending_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, last_pending_state_id)

            # Check B1 was created for the invoice
            sw_obj = pool.get('giscedata.switching')
            B1_found = sw_obj.find_B1(cursor, uid, factura_id)
            self.assertTrue(len(B1_found) == 0)

    def test_process_calculate_natural_days(self):
        """Test last pending state from invoice generates a B1 if the
        polissa is active"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_test(txn)
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            pprocess_obj = pool.get('account.invoice.pending.state.process')
            pprocess_obj.write(cursor, uid, [default_process_id], {
                'cutoff_days': 14, 'days_type': 'natural'
            })
            action_day = datetime.now()
            action_day = action_day + timedelta(days=14)
            b1_action_day = self.calc_action_day(action_day)
            process_action_day = pprocess_obj.get_cutoff_day(
                cursor, uid, default_process_id, not_allowed_days=[4, 5, 6])
            self.assertGreaterEqual(process_action_day, action_day)
            self.assertEqual(
                b1_action_day.strftime('%Y-%m-%d'),
                process_action_day.strftime('%Y-%m-%d')
            )

    def test_process_calculate_bussines_days(self):
        """Test last pending state from invoice generates a B1 if the
        polissa is active"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_test(txn)
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]
            pprocess_obj = pool.get('account.invoice.pending.state.process')
            pprocess_obj.write(cursor, uid, [default_process_id], {
                'cutoff_days': 14, 'days_type': 'business'
            })
            action_day = datetime.now()
            calendar = Spain()
            action_day = calendar.add_working_days(action_day, 14)
            b1_action_day = self.calc_action_day(action_day)
            process_action_day = pprocess_obj.get_cutoff_day(
                cursor, uid, default_process_id, not_allowed_days=[4, 5, 6])
            self.assertGreaterEqual(
                process_action_day, b1_action_day
            )
            self.assertEqual(
                b1_action_day.strftime('%Y-%m-%d'),
                process_action_day.strftime('%Y-%m-%d')
            )

    def test_go_on_pending_with_B1_generated_correct_day(self):
        """Test last pending state from invoice generates a B1 if the
        polissa is active"""
        pool = self.openerp.pool
        imd_obj = pool.get('ir.model.data')
        fact_obj = pool.get('giscedata.facturacio.factura')
        with Transaction().start(self.database) as txn:
            cursor = txn.cursor
            uid = txn.user
            self.prepare_for_test(txn)
            correct_state_id = imd_obj.get_object_reference(
                cursor, uid,
                'account_invoice_pending', 'default_invoice_pending_state'
            )[1]
            default_process_id = imd_obj.get_object_reference(
                cursor, uid, 'account_invoice_pending',
                'default_pending_state_process'
            )[1]

            # Create the invoice last pending state
            pstate_obj = pool.get('account.invoice.pending.state')
            last_pending_state_id = pstate_obj.create(cursor, uid, {
                'name': 'Last state 10',
                'weight': 10,
                'process_id': default_process_id
            })
            pprocess_obj = pool.get('account.invoice.pending.state.process')
            pprocess_obj.write(cursor, uid, [default_process_id], {
                'cutoff_days': 14, 'days_type': 'business'
            })
            # Clear cache
            self.clear_cache_pending(txn)
            # Create a new invoice (giscedata.facturacio.factura)
            factura_id = self.factura_id
            factura = fact_obj.browse(cursor, uid, factura_id)

            # Open the invoice
            wf_service = netsvc.LocalService("workflow")
            wf_service.trg_validate(
                uid, 'account.invoice', factura.invoice_id.id,
                'invoice_open', cursor
            )
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.state, 'open')

            # Set the invoice pending state to correct
            factura.set_pending(correct_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, correct_state_id)

            # Set the invoice pending state to last
            factura.set_pending(last_pending_state_id)
            # Refresh invoice object
            factura = fact_obj.browse(cursor, uid, factura_id)
            self.assertEqual(factura.pending_state.id, last_pending_state_id)

            # Check B1 was created for the invoice
            sw_obj = pool.get('giscedata.switching')
            b1_01_obj = pool.get('giscedata.switching.b1.01')
            B1_found = sw_obj.find_B1(cursor, uid, factura_id)
            self.assertTrue(len(B1_found) > 0)

            b1_01 = b1_01_obj.search(cursor, uid, [('sw_id', '=', B1_found[0])])
            b1_01_info = b1_01_obj.read(cursor, uid, b1_01[0], ['data_accio'])
            action_day = datetime.now()
            calendar = Spain()
            action_day = calendar.add_working_days(action_day, 14)
            b1_action_day = self.calc_action_day(action_day)
            self.assertEqual(b1_01_info['data_accio'], b1_action_day.strftime('%Y-%m-%d'))


class AbstractTestContractCreationWithDebt(TestSwitchingImport):

    def setUp(self):
        self.pool = self.openerp.pool
        self.imd_obj = self.pool.get('ir.model.data')
        self.sw_obj = self.pool.get('giscedata.switching')
        self.conf_obj = self.pool.get('res.config')
        self.contract_obj = self.pool.get('giscedata.polissa')
        self.journal_obj = self.pool.get('account.journal')
        self.partner_obj = self.pool.get('res.partner')
        self.acc_obj = self.pool.get('account.account')

        self.txn = Transaction().start(self.database)

        cursor = self.txn.cursor
        uid = self.txn.user

        model, factura_id = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_facturacio', 'factura_0007'
        )

        self.fact_obj = self.pool.get(model)

        _, self.demo_contract_01 = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001')

        self.fact_obj.write(cursor, uid, factura_id, {
            'polissa_id': self.demo_contract_01
        })

        journal_id = self.journal_obj.search(cursor, uid, [
            ('code', '=', 'CAJA')
        ])[0]

        # We set the account debtor of the partner to allow unpayments
        self.partner_id = self.fact_obj.read(
            cursor, uid, factura_id, ['partner_id']
        )['partner_id'][0]

        debt_account = self.acc_obj.search(cursor, uid, [
            ('code', '=', '431500')
        ])[0]
        self.partner_obj.write(cursor, uid, self.partner_id, {
            'property_account_debtor': debt_account
        })

        self.activation_error_msg = u"El titular té una quantitat de deute " \
                                    u"pendent"

        Environment = namedtuple('Environment', [
            'cursor', 'uid', 'factura_id', 'journal_id', 'partner_id',
            'debt_account'
        ])

        self.environment_variables = Environment(**{
            # We set the environment variables to use them on each test
            'cursor': cursor,
            'uid': uid,
            'factura_id': factura_id,
            'journal_id': journal_id,
            'partner_id': self.partner_id,
            'debt_account': debt_account,
        })

    def tearDown(self):
        self.txn.stop()

    def pay_invoice(self, cursor, uid, fact_id, journal_id, amount=False,
                    context=None):
        if context is None:
            context = {}

        wizard_pay = self.openerp.pool.get('facturacio.pay.invoice')

        context.update({
            'active_id': fact_id, 'active_ids': [fact_id]
        })
        values = {
            'state': 'init',
            'name': 'Partial payment',
            'journal_id': journal_id
        }
        if amount:
            values.update({'amount': amount})

        wiz_id = wizard_pay.create(cursor, uid, values, context=context)
        wizard_pay.action_pay_and_reconcile(
            cursor, uid, wiz_id, context=context
        )

    def unpay_invoice(self, cursor, uid, fact_id, pay_account_id,
                      pay_journal_id, amount=0, context=None):
        if context is None:
            context = {}

        wizard_unpay = self.openerp.pool.get('wizard.unpay')
        context.update({
            'active_id': fact_id, 'active_ids': [fact_id]
        })
        values = {
            'go_on_pending_state': True,
            'pay_account_id': pay_account_id,
            'pay_journal_id': pay_journal_id,
        }
        if amount > 0:
            values.update({'amount': amount})
        # And then we unpay it to have some debt
        wiz_id = wizard_unpay.create(cursor, uid, values, context=context)
        context.update({'model': 'giscedata.facturacio.factura'})
        wizard_unpay.unpay(cursor, uid, [wiz_id], context=context)


class TestContractCreationWithDebt(AbstractTestContractCreationWithDebt):

    def test_check_debt_while_contract_creation(self):
        """
        Test that contract creation with an indebted partner raises a
        constraint.
        """

        cursor, uid, fact_id, journal_id, partner_id, debt_account = self.environment_variables

        self.fact_obj.invoice_open(cursor, uid, [fact_id])

        # We pay the invoice
        self.pay_invoice(cursor, uid, fact_id, journal_id)

        # And we unpay it in order to have some debt amount
        amount_unpaid = 5
        self.unpay_invoice(
            cursor, uid, fact_id, debt_account, journal_id, amount=amount_unpaid
        )

        total_debt = self.partner_obj.read(
            cursor, uid, partner_id, ['total_debt']
        )['total_debt']

        self.assertEquals(total_debt, amount_unpaid)

        # We try to create a new contract

        _, self.demo_contract_01 = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )

        vals, _ = self.contract_obj.copy_data(cursor, uid, self.demo_contract_01)

        vals.update({
            'titular': partner_id,
            'data_alta': date.today(),
            'data_firma_contracte': date.today()
        })
        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'crear'}
        )

        with self.assertRaises(ValidateException):
            self.contract_obj.create(cursor, uid, vals)

        self.partner_obj.write(cursor, uid, partner_id, {
            'credit_limit': total_debt + 10
        })

        self.contract_obj.create(cursor, uid, vals)

    def test_check_debt_while_contract_validation(self):
        """
        Test that contract creation with an indebted partner raises a
        constraint.
        """

        cursor, uid, fact_id, journal_id, partner_id, debt_account = self.environment_variables

        self.fact_obj.invoice_open(cursor, uid, [fact_id])

        # We pay the invoice
        self.pay_invoice(cursor, uid, fact_id, journal_id)

        # And we unpay it in order to have some debt amount
        self.unpay_invoice(
            cursor, uid, fact_id, debt_account, journal_id, amount=5
        )

        # We try to create and validate a new contract
        _, self.demo_contract_01 = self.imd_obj.get_object_reference(
            cursor, uid, 'giscedata_polissa', 'polissa_0001'
        )

        vals, _ = self.contract_obj.copy_data(cursor, uid, self.demo_contract_01)

        vals.update({
            'titular': partner_id,
            'data_alta': date.today(),
            'data_firma_contracte': date.today()
        })
        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'validar'}
        )

        new_contract_id = self.contract_obj.create(cursor, uid, vals)

        with self.assertRaises(except_osv):
            self.contract_obj.send_signal(cursor, uid, [new_contract_id], [
                'validar', 'contracte'
            ])

        total_debt = self.partner_obj.read(
            cursor, uid, partner_id, ['total_debt']
        )['total_debt']

        self.partner_obj.write(cursor, uid, partner_id, {
            'credit_limit': total_debt + 10
        })

        self.contract_obj.send_signal(cursor, uid, [new_contract_id], [
            'validar', 'contracte'
        ])

    def test_check_debt_on_contract_c101_creation(self):
        cursor, uid, fact_id, journal_id, partner_id, debt_account = self.environment_variables

        c1_wizard_obj = self.pool.get('giscedata.switching.c101.wizard')

        contract_id = self.fact_obj.read(
            cursor, uid, fact_id, ['polissa_id']
        )['polissa_id'][0]

        self.switch(self.txn, 'comer', other_id_name='res_partner_asus')

        asus = self.imd_obj.get_object_reference(
            cursor, uid, 'base',  'res_partner_asus'
        )[1]

        self.partner_obj.write(cursor, uid, asus, {
            'vat': 'ES06938188P'
        })
        self.fact_obj.write(cursor, uid, fact_id, {
            'partner_id': asus
        })
        self.contract_obj.write(cursor, uid, contract_id, {
            'titular': asus
        })
        self.fact_obj.invoice_open(cursor, uid, [fact_id])

        # We pay the invoice
        self.pay_invoice(cursor, uid, fact_id, journal_id)

        # And we unpay it in order to have some debt amount
        self.unpay_invoice(
            cursor, uid, fact_id, debt_account, journal_id, amount=5
        )

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'ATR'}
        )

        ctx = {'contract_id': contract_id}
        wiz_id = c1_wizard_obj.create(cursor, uid, {}, context=ctx)

        c1_wizard_obj.genera_casos_atr(cursor, uid, [wiz_id], context=ctx)

        res_info = c1_wizard_obj.read(cursor, uid, [wiz_id],
                                      ['info'])[0]['info']
        self.assertIn(self.activation_error_msg, res_info)

        created_cases = self.sw_obj.search(
            cursor, uid, [('cups_polissa_id', '=', contract_id),
                          ('proces_id.name', '=', 'C1')]
        )

        self.assertEqual(len(created_cases), 0)

    def test_check_debt_on_contract_a301_creation(self):
        cursor, uid, fact_id, journal_id, partner_id, debt_account = self.environment_variables

        contract_id = self.fact_obj.read(
            cursor, uid, fact_id, ['polissa_id']
        )['polissa_id'][0]

        self.switch(self.txn, 'comer', other_id_name='res_partner_asus')

        asus = self.imd_obj.get_object_reference(
            cursor, uid, 'base',  'res_partner_asus'
        )[1]

        self.partner_obj.write(cursor, uid, asus, {
            'vat': 'ES06938188P'
        })
        self.fact_obj.write(cursor, uid, fact_id, {
            'partner_id': asus
        })
        self.contract_obj.write(cursor, uid, contract_id, {
            'titular': asus
        })
        self.fact_obj.invoice_open(cursor, uid, [fact_id])

        # We pay the invoice
        self.pay_invoice(cursor, uid, fact_id, journal_id)

        # And we unpay it in order to have some debt amount
        self.unpay_invoice(
            cursor, uid, fact_id, debt_account, journal_id, amount=5
        )

        conf_id = self.conf_obj.search(
            cursor, uid, [('name', '=', 'check_partner_can_contract')]
        )
        self.conf_obj.write(
            cursor, uid, conf_id, {'value': 'ATR'}
        )

        wiz_obj = self.openerp.pool.get('giscedata.switching.mod.con.wizard')

        wiz_id = wiz_obj.create(
            cursor, uid, {}, context={'cas': 'A3', 'pol_id': contract_id})

        context = {
            'pol_id': contract_id, 'cas': 'A3', 'active_ids': [1],
            'active_id': False, 'contract_id': contract_id
        }

        wiz_obj.write(cursor, uid, [wiz_id], {
            'owner': 4, 'change_atr': False, 'change_adm': True
        })

        wiz_obj.genera_casos_atr(cursor, uid, [wiz_id], context=context)

        created_cases = self.sw_obj.search(
            cursor, uid, [('cups_polissa_id', '=', contract_id),
                          ('proces_id.name', '=', 'ATR')]
        )

        res_info = wiz_obj.read(cursor, uid, wiz_id, ['info'])[0]['info']

        self.assertIn(self.activation_error_msg, res_info)

        self.assertEqual(len(created_cases), 0)


