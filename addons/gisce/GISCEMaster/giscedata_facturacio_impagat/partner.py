# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class ResPartner(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def can_contract(self, cursor, uid, ids, context=None):
        """
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <res.partner> ids
        :type ids: long | list
        :param context: OpenERP context.
        :type context: dict
        :return: whether the partner can open new contracts of not and the
        description of the reason because it can't
        :rtype: bool, str
        """
        if context is None:
            context = {}

        if isinstance(ids, (tuple, list)):
            partner_id = ids[0]
        else:
            partner_id = ids

        can_contract = not self.has_debt(cursor, uid, partner_id)
        description = False
        if not can_contract:
            description = _(u"El titular té una quantitat de deute pendent")

        return can_contract, description

    def has_debt(self, cursor, uid, ids, context=None):
        """
        :param cursor: DB cursor.
        :type cursor: sql_db.Cursor
        :param uid: <res.users> id
        :type uid: long
        :param ids: <res.partner> ids
        :type ids: long | list
        :param context: OpenERP context.
        :type context: dict
        :return: whether the partner has pending debt or has unpaid invoices
        :rtype: bool
        """
        if context is None:
            context = {}
        if isinstance(ids, (tuple, list)):
            partner_id = ids[0]
        else:
            partner_id = ids

        polissa_obj = self.pool.get('giscedata.polissa')

        values = self.read(
            cursor, uid, partner_id, ['total_debt', 'credit_limit']
        )

        polisses = polissa_obj.search(cursor, uid, [
            ('pagador', '=', partner_id)
        ], context={'active_test': False})

        deute_polisses = sum([
            polissa['debt_amount']
            for polissa in polissa_obj.read(
                cursor, uid, polisses, ['debt_amount']
            )
        ])

        return (
            values['total_debt'] > values['credit_limit'] or
            deute_polisses > values['credit_limit']
        )

    _columns = {
        'credit_limit': fields.float(
            string='Credit Limit',
            write=['giscedata_facturacio_impagat.group_credit_management'],
        ),
    }


ResPartner()
