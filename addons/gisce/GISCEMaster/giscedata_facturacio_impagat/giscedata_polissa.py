# -*- encoding: utf-8 -*-

from __future__ import absolute_import
from osv import osv, fields
from osv.orm import OnlyFieldsConstraint
from tools.translate import _
from tools import cache
from giscedata_polissa.giscedata_polissa import CONTRACT_IGNORED_STATES
from addons.giscedata_facturacio.giscedata_polissa import _get_polissa_from_invoice


class GiscedataPolissa(osv.osv):

    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    def copy(self, cursor, uid, id, default=None, context=None):
        """Change copy funcionality
        Do not copy pending_amount and pending_state"""

        default.update({'pending_amount': 0,
                        'pending_state': False,
                       })

        res_id = super(GiscedataPolissa, self).copy(cursor, uid, id, default,
                                                                context)
        return res_id

    @cache(timeout=300)
    def _get_pending_states(self, cursor, uid, polissa_id=None, context=None):
        '''Returns dictionary with pending default values'''

        if not context:
            context = {}

        state_obj = self.pool.get('account.invoice.pending.state')
        res = {}

        state_ids = state_obj.search(cursor, uid, [], context=context)
        for state in state_obj.browse(cursor, uid, state_ids, context=context):
            res.update({state.weight: state.name})
        return res

    @staticmethod
    def get_max_weight(weight, factura):
        return max(weight, factura.pending_state.weight)

    def _get_pending(self, cursor, uid, ids, name, arg, context=None):

        if context is None:
            context = {}

        ctx = context.copy()
        ctx['active_test'] = False

        res = {}
        factura_obj = self.pool.get('giscedata.facturacio.factura')

        for polissa in self.browse(cursor, uid, ids, context=context):
            states = self._get_pending_states(
                cursor, uid, polissa_id=polissa.id, context=ctx)
            # Default return values if no values
            res[polissa.id] = {
                'pending_state': states[0],
                'pending_amount': 0,
                'debt_amount': 0,
                'unpaid_invoices': 0,
            }
            # If not activa, return default values
            if polissa.state in CONTRACT_IGNORED_STATES:
                continue

            search_params = [('polissa_id', '=', polissa.id),
                             ('state', '=', 'open'),
                             ('type', 'in', ('out_invoice', 'out_refund'))]

            factura_ids = factura_obj.search(cursor, uid, search_params,
                                             context=ctx)
            weight = 0
            for factura in factura_obj.browse(cursor, uid, factura_ids,
                                              context=context):
                if factura.type == 'out_invoice':
                    sign = 1
                else:
                    sign = -1
                res[polissa.id]['pending_amount'] += sign * factura.residual
                if factura.pending_state.weight > 0:
                    res[polissa.id]['debt_amount'] += sign * factura.residual
                    res[polissa.id]['unpaid_invoices'] += 1
                weight = self.get_max_weight(weight, factura)
            res[polissa.id]['pending_state'] = states[weight]

        return res

    def _get_polissa_from_factura(self, cursor, uid, ids, context=None):
        """Returns ids of polissa in facturas passed as ids"""

        factura_obj = self.pool.get('giscedata.facturacio.factura')

        vals = factura_obj.read(cursor, uid, ids, ['polissa_id'])
        return [val['polissa_id'][0] for val in vals if val['polissa_id']]

    def change_state(self, cursor, uid, ids, context):
        values = self.read(cursor, uid, ids, ['invoice_id'])
        return _get_polissa_from_invoice(self,
            cursor, uid, [value['invoice_id'][0] for value in values])

    def _cnt_can_contract(self, cursor, uid, ids):
        """
        Check that the partner can contract. The conditions are specified in the
        method `can_contract`; the reason is specified as the second parameter.

        :return: False if the contract can't be created.
        """

        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        conf_obj = self.pool.get('res.config')
        condition = conf_obj.get(cursor, uid, 'check_partner_can_contract')
        if condition == 'crear':
            partner_obj = self.pool.get('res.partner')

            q = self.q(cursor, uid)
            res = q.read(['titular']).where([('id', 'in', ids)])

            for contract_values in res:
                partner_can_contract, description = partner_obj.can_contract(
                    cursor, uid, contract_values['titular']
                )

                if not partner_can_contract:
                    return False

        return True

    def cnd_esborrany_validar(self, cursor, uid, ids):
        """
        Condition to stop from draft to validate state in the contract.
        """
        if not isinstance(ids, (tuple, list)):
            ids = [ids]

        conf_obj = self.pool.get('res.config')
        condition = conf_obj.get(cursor, uid, 'check_partner_can_contract')
        if condition == 'validar':
            partner_obj = self.pool.get('res.partner')

            q = self.q(cursor, uid)
            res = q.read(['titular']).where([('id', 'in', ids)])

            for contract_values in res:
                partner_can_contract, description = partner_obj.can_contract(
                    cursor, uid, contract_values['titular']
                )

                if not partner_can_contract:
                    raise osv.except_osv(_(u"Error"), description)

        return True

    _columns = {
        'pending_state': fields.function(_get_pending, method=True,
            type='char', size=64, string='Pending State',
            store={'account.invoice': (_get_polissa_from_invoice,
                                       ['state', 'residual',
                                        'pending_state'], 20),
                   'account.invoice.pending.history': (
                        change_state, ['change_date'], 10
                    ),
                   'giscedata.polissa': (lambda self, cursor,
                                         uid, ids, c=None: ids,
                                         ['potencies_periode'], 20),
                   },
            multi='pending',),
        'pending_amount': fields.function(_get_pending, method=True,
            type='float', digits=(16, 2), string='Pending amount',
            store={'account.invoice': (_get_polissa_from_invoice,
                                       ['state', 'residual',
                                        'pending_state'], 20),
                   'account.invoice.pending.history': (
                       change_state, ['change_date'], 10
                   ),
                   'giscedata.polissa': (lambda self, cursor,
                                         uid, ids, c=None: ids,
                                         ['potencies_periode'], 20),
                   },
            multi='pending'),
        'debt_amount': fields.function(
            _get_pending,
            method=True,
            type='float',
            digits=(16, 2),
            string='Debt amount',
            store={
                'account.invoice': (
                    _get_polissa_from_invoice,
                    ['state', 'residual', 'pending_state'],
                    20
                ),
                'account.invoice.pending.history': (
                    change_state, ['change_date'], 10
                ),
                'giscedata.polissa': (
                    lambda self, cursor, uid, ids, c=None: ids,
                    ['potencies_periode'],
                    20
                ),
            },
            multi='pending'
        ),
        'unpaid_invoices': fields.function(
            _get_pending,
            method=True,
            type='integer',
            string='Unpaid invoices',
            store={
                'account.invoice': (
                    _get_polissa_from_invoice,
                    ['state', 'residual', 'pending_state'],
                    20
                ),
                'account.invoice.pending.history': (
                    change_state, ['change_date'], 10
                ),
                'giscedata.polissa': (
                    lambda self, cursor, uid, ids, c=None: ids,
                    ['potencies_periode'],
                    20
                ),
            },
            multi='pending'
        ),
    }

    _constraints = [
        OnlyFieldsConstraint(
            _cnt_can_contract,
            _(u'Error: la pòlissa no pot ser creada perquè no li està permès '
              u'al titular.'),
            ['titular']
        )
    ]


GiscedataPolissa()
