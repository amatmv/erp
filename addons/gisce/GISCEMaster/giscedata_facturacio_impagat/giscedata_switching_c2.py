# -*- coding: utf-8 -*-

from osv import osv
from giscedata_switching.giscedata_switching import SwitchingException

from tools.translate import _


class GiscedataSwitchingC2_01(osv.osv):

    _name = 'giscedata.switching.c2.01'
    _inherit = 'giscedata.switching.c2.01'

    def config_step_validation(self, cursor, uid, ids, vals, context=None):
        if context is None:
            context = {}

        res = super(GiscedataSwitchingC2_01, self).config_step_validation(
            cursor, uid, ids, vals, context=context
        )

        conf_obj = self.pool.get('res.config')
        condition = conf_obj.get(cursor, uid, 'check_partner_can_contract',
                                 'desactivado')
        if condition == 'ATR':
            partner_obj = self.pool.get('res.partner')

            q = self.pool.get('giscedata.polissa').q(cursor, uid)
            contract_values = q.read(['titular']).where([
                ('id', 'in', [context['contract_id']])])

            for values in contract_values:
                partner_can_contract, description = partner_obj.can_contract(
                    cursor, uid, values['titular']
                )

                if not partner_can_contract:
                    raise SwitchingException(description)

        return res


GiscedataSwitchingC2_01()
