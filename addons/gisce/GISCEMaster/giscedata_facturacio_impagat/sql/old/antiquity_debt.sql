select
  sum(deuda) as cantidad,
  days as n_dias,
  cuenta || organismo_oficial as cuenta
from (
  select
    deuda,
    cuenta,
    organismo_oficial,
    case
      when days <= 30 then '30'
      when days <= 60 then '60'
      when days <= 90 then '90'
      when days <= 120 then '120'
      when days <= 150 then '150'
      when days <= 180 then '180'
      when days <= 210 then '210'
      when days <= 240 then '240'
      when days <= 270 then '270'
      when days <= 300 then '300'
      when days <= 330 then '330'
      when days <= 360 then '360'
      else '>360'
    end as days,
    diario
  from (
    select
        sum(l.debit - l.credit) as deuda,
        %s::date - i.date_invoice as days,
        j.name as diario,
        a.name as cuenta,
        case when lower(p.vat) similar to 'es(p|q|s)%%' then
          ' Org. oficial'
        else
          ''
        end as organismo_oficial
      from
        account_move_line l
        inner join account_account a on (l.account_id = a.id)
        inner join account_journal j on (l.journal_id = j.id)
        inner join res_partner p on (l.partner_id = p.id)
        inner join (
           select number, date_invoice
           from account_invoice i
           where i.group_move_id is null
            and i.type in ('out_refund','out_invoice')
           UNION
            select g.ref as number, i.date_invoice
            from account_invoice i, account_move g
            where i.group_move_id = g.id
           and i.type in ('out_refund','out_invoice')

        ) i on (l.ref = i.number)
      where
        a.type = 'receivable'
        and (a.code = '431500' or %s)
        and l.date <= %s
      group by
        j.name,
        days,
        cuenta,
        p.vat
    ) as foo
  ) as q_days
group by
  days,
  cuenta,
  organismo_oficial
having sum(deuda) > 0