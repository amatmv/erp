select foo.*,
    case when lower(p.vat) similar to 'es(q|s)%%' then
        ' Org. oficial'
      else
        ''
    end as organismo_oficial,
    p.name as razon_fiscal,
    replace(p.vat, 'ES','') as cif_nif,
    %s::date - case when i.date_invoice is not null then i.date_invoice else foo.min_date end  as days
  from (
  select
      a.code as cuenta,
      a.name as nombre_cuenta,
      l.partner_id,
      l.ref as number,
      sum(l.debit - l.credit) as deuda,
      min(l.date) as min_date
    from
      account_move_line l
      inner join account_account a on (l.account_id = a.id)
      inner join account_journal j on (l.journal_id = j.id)
    where
      a.type = 'receivable'
      and (a.code = '431500' or %s)
      and l.date <= %s
    group by
      l.partner_id,
      cuenta,
      nombre_cuenta,
      l.ref
    having sum(l.debit - l.credit)!=0
  ) foo
  inner join res_partner p on (foo.partner_id = p.id)
  left join (
         select number, date_invoice, amount_total * (CASE WHEN i.type like '%%refund' THEN -1 ELSE 1 END) as amount_total
         from account_invoice i
         where
          i.type in ('out_refund','out_invoice')
      ) i on (foo.number = i.number)
  order by foo.number
