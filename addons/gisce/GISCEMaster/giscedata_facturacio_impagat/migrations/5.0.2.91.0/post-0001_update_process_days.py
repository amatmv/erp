# coding=utf-8
import logging

import pooler


def up(cursor, installed_version):
    if not installed_version:
        return
    uid = 1
    pool = pooler.get_pool(cursor.dbname)
    logger = logging.getLogger('openerp.migration')
    pprocess_obj = pool.get('account.invoice.pending.state.process')
    imd_obj = pool.get('ir.model.data')
    cfg_obj = pool.get('res.config')

    action_days = int(cfg_obj.get(cursor, uid, 'atr_b1_data_accio', '14'))
    logger.info('Updating Default process with {} natural days'
                ' res.config atr_b1_data_accio'.format(
                    action_days
                ))
    default_process_id = imd_obj.get_object_reference(
        cursor, uid, 'account_invoice_pending',
        'default_pending_state_process'
    )[1]
    pprocess_obj.write(cursor, uid, [default_process_id], {
        'cutoff_days': action_days, 'days_type': 'natural'
    })
    logger.info('All tasks finished')


def down(cursor, installed_version):
    pass


migrate = up
