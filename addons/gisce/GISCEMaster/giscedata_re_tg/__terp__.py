# -*- coding: utf-8 -*-
{
    "name": "Mòdul de Telegestió Règim Especial",
    "description": """
    * Perfilacio automatica al importar curves a regim especial desde telegestió
    * Preparació de curves per generar fitxers de regim especial (MAGRE, INMERE, MEDIDAS).""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_re",
        "giscedata_telegestio",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
