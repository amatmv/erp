# -*- coding: utf-8 -*-
{
    "name": "Index obres transformadors",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index per Obres Transformadors
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_transformadors",
        "base_index",
        "giscedata_obres_transformadors"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
