from base_index.base_index import BaseIndex


class GiscedataTransformadorTrafo(BaseIndex):
    _name = "giscedata.transformador.trafo"
    _inherit = "giscedata.transformador.trafo"
    _index_fields = {
        "obres": lambda self, data: ' '.join(['obra{0}'.format(d.name) or '' for d in data])
    }

GiscedataTransformadorTrafo()

