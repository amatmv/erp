# -*- encoding: utf-8 -*-
from osv import fields, osv
from tools import config
from tools.misc import ustr
from tools.translate import _


class product_pricelist(osv.osv):

    _inherit = "product.pricelist"

    _columns = {
        'llista_comissions': fields.many2one(
            'product.pricelist', 'Comissions Associades', domain=[('type', '=', 'comissio')]
        ),
    }

product_pricelist()
