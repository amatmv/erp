# -*- coding: utf-8 -*-

import csv
import base64
import StringIO
from datetime import datetime
from datetime import timedelta
import json
from osv import osv
from osv import fields
from tools.translate import _


class WizardPricelistFromFile(osv.osv_memory):

    _inherit = 'wizard.pricelist.from.file'

    def compute_extra_columns(self, cr, uid, wizard, list_id, row, current_pos, context=None):
        new_pos = super(WizardPricelistFromFile, self).compute_extra_columns(cr, uid, wizard, list_id, row, current_pos, context=context)
        if wizard.pricelist_type != "comissio":
            return new_pos
        # Si sindica que es carrega una llista de comissions,
        # la columna despres de tarifa base indica a quina tarifa se li
        # apliquen les comissions
        prod_list_obj = self.pool.get('product.pricelist')
        if row[new_pos]:
            comissio_on_pricelist_id = prod_list_obj.search(
                cr, uid, [('name', '=', row[new_pos])]
            )
            if len(comissio_on_pricelist_id):
                comissio_on_pricelist_id = comissio_on_pricelist_id[0]
            else:
                raise osv.except_osv(
                    "Error en el fitxer",
                    _(u'Tarifa a la que aplicar comissions no trobada: {0}\n'
                      u'No pricelist was created.').format(row[new_pos])
                )
            prod_list_obj.write(cr, uid, comissio_on_pricelist_id, {'llista_comissions': list_id})
        return new_pos+1


WizardPricelistFromFile()
