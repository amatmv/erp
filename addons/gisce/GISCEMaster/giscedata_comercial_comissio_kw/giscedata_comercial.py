# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _
from datetime import datetime


class GiscedataComercialComissioKw(osv.osv):
    _name = 'giscedata.comercial.comissio.kw'
    _inherit = 'giscedata.comercial.comissio'
    _description = "Comissió per Kw Facturats"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['data_inici'], context=context)
        return _(u"Comissio per cada kw facturat a partir del {0}").format(vals['data_inici'])

    def get_comissio_polissa_vals(self, cursor, uid, comissio_id, context=None):
        vals = self.read(cursor, uid, comissio_id, ['comercial_id', 'data_inici'], context=context)
        if vals.get('comercial_id'):
            vals['comercial_id'] = vals.get('comercial_id')[0]
        del vals['id']
        return vals

    _columns = {
        'data_inici': fields.date(
            "Data Inici Comissions",
            help=u"Només es cobraran les comissions de les factures "
                 u"generades a partir d'aquesta data."
        ),
    }

    _defaults = {
        'data_inici': lambda *a: datetime.today().strftime("%Y-%m-%d")
    }


GiscedataComercialComissioKw()


class GiscedataPolissaComissioKw(osv.osv):
    _name = 'giscedata.polissa.comissio.kw'
    _inherit = 'giscedata.polissa.comissio'
    _description = "Comissió per Kw Facturats"

    def generar_descripcio(self, cursor, uid, cid, context=None):
        if not cid:
            return ""
        vals = self.read(cursor, uid, cid, ['polissa_id', 'data_inici'], context=context)
        return _(u"Comissio per cada kw facturat ({0} - a partir del {1})").format(vals['polissa_id'][1], vals['data_inici'])

    def check_cobrar_comissio(self, cursor, uid, comissio_polissa_id, context=None):
        fact_obj = self.pool.get("giscedata.facturacio.factura")
        info = self.read(cursor, uid, comissio_polissa_id, ['polissa_id', 'data_inici', 'factures_ids'])
        factures_a_pagar = fact_obj.search(cursor, uid, [
            ('date_invoice', '>=', info['data_inici']),
            ('polissa_id', '=', info['polissa_id'][0]),
            ('id', 'not in', info['factures_ids']),
            ('state', '=', 'paid'),
            ('type', 'in', ['out_refund', 'out_invoice']),
        ])
        if not len(factures_a_pagar):
            return False, _(u"* Contracte {0}: no queda cap factura a pagar.\n").format(info['polissa_id'][1])
        else:
            return True, ""

    def calc_linia_expense_vals(self, cursor, uid, comissio_polissa_id, context=None):
        if context is None:
            context = {}
        fact_obj = self.pool.get("giscedata.facturacio.factura")
        pricelist_obj = self.pool.get('product.pricelist')
        info = self.read(cursor, uid, comissio_polissa_id, ['polissa_id', 'data_inici', 'factures_ids'])
        factures_a_pagar = fact_obj.search(cursor, uid, [
            ('date_invoice', '>=', info['data_inici']),
            ('polissa_id', '=', info['polissa_id'][0]),
            ('id', 'not in', info['factures_ids']),
            ('state', '=', 'paid'),
            ('type', 'in', ['out_refund', 'out_invoice']),
        ])
        vals = []
        for factura in fact_obj.browse(cursor, uid, factures_a_pagar, context=context):
            if not factura.llista_preu.llista_comissions:
                raise osv.except_osv(
                    _(u"Error Usuari"),
                    _(u"Falta configurar la llista de preus de comissions per "
                      u"la tarifa {0}").format(factura.llista_preu.name))
            pricelist = factura.llista_preu.llista_comissions.id
            ctx = context.copy()
            ctx['date'] = factura.date_invoice
            for linia in factura.linia_ids:
                if linia.tipus not in ['energia']:
                    continue
                product_id = linia.product_id.id
                kw = linia.quantity
                price = pricelist_obj.price_get(
                    cursor, uid, [pricelist], product_id, 1.0, context=ctx
                )[pricelist]
                if factura.type == 'out_refund':
                    price = price * -1
                vals.append({
                    'product_id': product_id,
                    'unit_amount': price,
                    'unit_quantity': kw,
                    'name': "{2} / {3} / {0} / {1}".format(factura.partner_id.name, factura.polissa_id.name, factura.number, linia.name)
                })
        self.write(cursor, uid, comissio_polissa_id, {'factures_ids': [(6, 0, factures_a_pagar+info['factures_ids'])]})
        return vals

    _columns = {
        'factures_ids': fields.many2many(
            'giscedata.facturacio.factura',
            'giscedata_polissa_comissio_kw_factura_rel',
            'comissio_polissa_id', 'factura_id', 'Factures Pagades'
        ),
        'data_inici': fields.date(
            "Data Inici Comissions",
            help=u"Només es cobraran les comissions de les factures "
                 u"generades a partir d'aquesta data."
        ),
    }


GiscedataPolissaComissioKw()
