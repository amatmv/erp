# -*- coding: utf-8 -*-
{
    "name": "Modul Comercial: Comissions per kW facturat",
    "description": """
    Afageix la comissió de modalitat '% per kw facturat': es paguen un X% al comercial per cada kw facturat als seus contractes.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_comercial_comissio",
        "giscedata_facturacio_comer",
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "pricelist_view.xml",
        "pricelist_data.xml",
        "giscedata_comercial_view.xml",
        "security/ir.model.access.csv",
    ],
    "active": False,
    "installable": True
}
