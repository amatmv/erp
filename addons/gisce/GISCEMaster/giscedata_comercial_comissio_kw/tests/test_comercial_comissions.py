# -*- coding: utf-8 -*-
from destral import testing
from destral.transaction import Transaction
from datetime import datetime
from osv import osv
import json


class TestComercials(testing.OOTestCase):

    def setUp(self):
        self.openerp.install_module("giscedata_tarifas_peajes_20160101")
        self.txn = Transaction().start(self.database)
        self.cursor = self.txn.cursor
        self.uid = self.txn.user
        self.pool = self.openerp.pool
        self.comercial_obj = self.pool.get("hr.employee")
        self.polissa_obj = self.pool.get("giscedata.polissa")
        self.factura_obj = self.pool.get("giscedata.facturacio.factura")
        self.comissio_kw_obj = self.pool.get("giscedata.comercial.comissio.kw")
        self.pol_comissio_kw_obj = self.pool.get("giscedata.polissa.comissio.kw")
        self.imd_obj = self.pool.get('ir.model.data')
        self.comercial_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'hr', 'employee1')[1]
        self.polissa_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0001')[1]
        self.polissa2_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0002')[1]
        self.polissa3_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'polissa_0003')[1]
        # Preparar les factures
        self.factura_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_facturacio', 'factura_0001')[1]
        self.factura2_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_facturacio', 'factura_0002')[1]
        fline_o = self.pool.get("giscedata.facturacio.factura.linia")
        product_o = self.pool.get("product.product")
        linia1_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_facturacio', 'linia_factura_0001')[1]
        categ1_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'categ_e_t20A_new')[1]
        linia2_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_facturacio', 'linia_factura_0002')[1]
        categ2_id = self.imd_obj.get_object_reference(self.cursor, self.uid, 'giscedata_polissa', 'categ_e_t20A')[1]
        fline_o.write(
            self.cursor, self.uid, linia1_id, {
                'product_id': product_o.search(self.cursor, self.uid, [('name', '=', 'P1'), ('categ_id', '=', categ1_id)])[0]
            }
        )
        fline_o.write(
            self.cursor, self.uid, linia2_id, {
                'product_id': product_o.search(self.cursor, self.uid, [('name', '=', 'P1'), ('categ_id', '=', categ2_id)])[0]
            }
        )
        # Preparar les tarifes per fer comissions
        self.list_id = self.pool.get("ir.model.data").get_object_reference(
            self.cursor, self.uid, "giscedata_facturacio", "pricelist_tarifas_electricidad"
        )[1]
        versio_id = self.pool.get("ir.model.data").get_object_reference(
            self.cursor, self.uid, "giscedata_tarifas_peajes_20160101", "boe_302_2015"
        )[1]
        self.pool.get("product.pricelist.version").write(self.cursor, self.uid, versio_id, {'date_end': False})
        pricelist_o = self.pool.get("product.pricelist")
        self.comissio_list_id = pricelist_o.copy(self.cursor, self.uid, self.list_id, {}, {})
        pricelist_o.write(self.cursor, self.uid, self.comissio_list_id, {'type': 'comissio'})
        pricelist_o.write(self.cursor, self.uid, self.list_id, {'llista_comissions': self.comissio_list_id})

    def tearDown(self):
        self.txn.stop()

    def test_facturar_comissio_kw(self):
        # Donem per "pagada" la factura 1
        self.factura_obj.write(self.cursor, self.uid, self.factura_id, {'state': 'paid', 'date_invoice': datetime.today().strftime("%Y-%m-%d")})
        # Creem una comissio de 10 i l'asignem, la cambiem a 15 i l'asisgnem a un altre contracte
        exp_obj = self.pool.get("hr.expense.expense")
        comissio_id = self.comissio_kw_obj.create(self.cursor, self.uid, {})
        self.comercial_obj.write(self.cursor, self.uid, self.comercial_id, {'comissio': "giscedata.comercial.comissio.kw, {0}".format(comissio_id)})
        self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa_id)
        self.polissa_obj.action_asignar_comissio_from_comercial(self.cursor, self.uid, self.polissa2_id)
        # Facturem els contractes 1,2 i 3. S'ha de generar una ordre amb 1 linies i donar un missatge de avis per el segon i tercer contracte
        wiz_obj = self.pool.get("wizard.facturar.comissio.from.polissa")
        context = {'active_ids': [self.polissa_id, self.polissa2_id, self.polissa3_id]}
        wiz_id = wiz_obj.create(self.cursor, self.uid, {}, context=context)
        wiz_obj.action_facturar_comissio_from_polissa(self.cursor, self.uid, wiz_id, context=context)
        exp_ids = wiz_obj.read(self.cursor, self.uid, wiz_id, ['ordres_ids'])[0]['ordres_ids']
        exp_ids = json.loads(exp_ids or "{}")
        exp_ids = list(set(exp_ids))
        self.assertEqual(len(exp_ids), 1)
        exp = exp_obj.browse(self.cursor, self.uid, exp_ids[0])
        self.assertEqual(exp.amount, 0.044027)
        self.assertEqual(len(exp.line_ids), 1)
