# -*- coding: utf-8 -*-
{
    "name": "Número de centralització pel CUPS",
    "description": """
    This module provide :
        * Afegeix el número de centralització al CUPS
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_cups"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_cups_view.xml"
    ],
    "active": False,
    "installable": True
}
