# -*- coding: utf-8 -*-

from osv import fields, osv


class GiscedataCupsPs(osv.osv):
    """CUPS
    """

    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _columns = {
        'central_ordre': fields.char('Centralització', size=10),
    }

giscedata_cups_ps()
