# -*- coding: utf-8 -*-
{
    "name": "Index CUPS (distri)",
    "description": """Aquest mòdul afegeix les següents funcionalitats:
  * Index for CUPS
""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "base",
        "giscedata_cups_index",
        "giscedata_polissa_distri"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml": [],
    "active": False,
    "installable": True
}
