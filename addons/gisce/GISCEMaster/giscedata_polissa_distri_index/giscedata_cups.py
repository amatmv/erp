from base_index.base_index import BaseIndex


class GiscedataCupsPs(BaseIndex):
    _name = 'giscedata.cups.ps'
    _inherit = 'giscedata.cups.ps'

    _index_fields = {
        'polissa_comptador': True,
        'polissa_polissa.name': True
    }

GiscedataCupsPs()