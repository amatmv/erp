# -*- coding: utf-8 -*-
{
    "name": "Resum de facturació per client",
    "description": """
    This module provide :
      * Informe a entregar al client.
    """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_facturacio",
        "giscedata_lectures"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/giscedata_resum_factures_client_view.xml",
        "ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
