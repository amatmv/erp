# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* giscedata_resum_factures_client
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 5.0.14\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2013-05-23 15:50:17+0000\n"
"PO-Revision-Date: 2013-05-23 15:50:17+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:136
#, python-format
msgid "Total impostos (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: field:wizard.resum.factures.client,name:0
msgid "Filename"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:93
#, python-format
msgid "Consum facturat P2 (kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: model:ir.module.module,shortdesc:giscedata_resum_factures_client.module_meta_information
msgid "Resum de facturació per client"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:98
#, python-format
msgid "Subtotal energia P3 (€) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: constraint:ir.actions.act_window:0
msgid "Invalid model name in the action definition."
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:53
#, python-format
msgid "Consum P3 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:110
#, python-format
msgid "Preu energia P1 (€/kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:76
#, python-format
msgid "Lectura anterior P5 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:66
#, python-format
msgid "Consum P1 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:61
#, python-format
msgid "Lectura actual P6 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:34
#, python-format
msgid "Potència P2 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:120
#, python-format
msgid "Subtotal energia P4 (€) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: view:wizard.resum.factures.client:0
msgid "Ok"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:71
#, python-format
msgid "Lectura actual P3 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:49
#, python-format
msgid "Lectura actual P2 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:59
#, python-format
msgid "Consum P5 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:68
#, python-format
msgid "Lectura actual P2 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:36
#, python-format
msgid "Potència P3 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:26
#, python-format
msgid "Tarifa"
msgstr ""

#. module: giscedata_resum_factures_client
#: model:ir.actions.act_window,name:giscedata_resum_factures_client.action_wizard_resum_factures_client
msgid "Generar resum factura client"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:97
#, python-format
msgid "Preu energia P3 (€/kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:70
#, python-format
msgid "Lectura anterior P3 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:47
#, python-format
msgid "Consum P1 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:111
#, python-format
msgid "Subtotal energia P1 (€) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:81
#, python-format
msgid "Consum P6 (kVAr)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:114
#, python-format
msgid "Subtotal energia P2 (€) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: field:wizard.resum.factures.client,file:0
msgid "Fitxer CSV"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:105
#, python-format
msgid "Consum facturat P6 (kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: field:wizard.resum.factures.client,state:0
msgid "State"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:145
#, python-format
msgid "CUPS"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:132
#, python-format
msgid "Total energia reactiva €"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:72
#, python-format
msgid "Consum P3 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:77
#, python-format
msgid "Lectura actual P5 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:51
#, python-format
msgid "Lectura anterior P3 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:116
#, python-format
msgid "Preu energia P3 (€/kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:99
#, python-format
msgid "Consum facturat P4 (kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:40
#, python-format
msgid "Potència P5 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:100
#, python-format
msgid "Preu energia P4 (€/kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:32
#, python-format
msgid "Potència P1 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: constraint:ir.ui.view:0
msgid "Invalid XML for View Architecture!"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:22
#, python-format
msgid "Referència contracte"
msgstr ""

#. module: giscedata_resum_factures_client
#: model:ir.model,name:giscedata_resum_factures_client.model_wizard_resum_factures_client
msgid "wizard.resum.factures.client"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:126
#, python-format
msgid "Subtotal energia P6 (€) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:67
#, python-format
msgid "Lectura anterior P2 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:30
#, python-format
msgid "Mesos facturats"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:45
#, python-format
msgid "Lectura anterior P1 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:139
#, python-format
msgid "Total base (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: model:ir.module.module,description:giscedata_resum_factures_client.module_meta_information
msgid "\n"
"    This module provide :\n"
"      * Informe a entregar al client.\n"
"    "
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:91
#, python-format
msgid "Preu energia P1 (€/kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:79
#, python-format
msgid "Lectura anterior P6 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:25
#, python-format
msgid "Adreça del CUPS"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:103
#, python-format
msgid "Preu energia P5 (€/kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:87
#, python-format
msgid "Lectura maxímetre P5 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:55
#, python-format
msgid "Lectura actual P4 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:118
#, python-format
msgid "Consum facturat P4 (kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: view:wizard.resum.factures.client:0
msgid "                                                                                                                                                       "
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:106
#, python-format
msgid "Preu energia P6 (€/kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:101
#, python-format
msgid "Subtotal energia P4 (€) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:39
#, python-format
msgid "Preu potència P4 (€/kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:75
#, python-format
msgid "Consum P4 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:42
#, python-format
msgid "Potència P6 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:128
#, python-format
msgid "Total energia (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:46
#, python-format
msgid "Lectura actual P1 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:122
#, python-format
msgid "Preu energia P5 (€/kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:123
#, python-format
msgid "Subtotal energia P5 (€) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:64
#, python-format
msgid "Lectura anterior P1 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:74
#, python-format
msgid "Lectura actual P4 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:35
#, python-format
msgid "Preu potència P2 (€/kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:38
#, python-format
msgid "Potència P4 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:69
#, python-format
msgid "Consum P2 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:90
#, python-format
msgid "Consum facturat P1 (kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:23
#, python-format
msgid "Data facturació"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:134
#, python-format
msgid "IESE (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:29
#, python-format
msgid "Data lectura actual"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:95
#, python-format
msgid "Subtotal energia P2 (€) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:50
#, python-format
msgid "Consum P2 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:65
#, python-format
msgid "Lectura actual P1 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:52
#, python-format
msgid "Lectura actual P3 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:57
#, python-format
msgid "Lectura anterior P5 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:113
#, python-format
msgid "Preu energia P2 (€/kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: view:wizard.resum.factures.client:0
msgid "Generar resum client"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:58
#, python-format
msgid "Lectura actual P5 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:140
#, python-format
msgid "Total (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:28
#, python-format
msgid "Data lectura anterior"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:56
#, python-format
msgid "Consum P4 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:86
#, python-format
msgid "Lectura maxímetre P4 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: view:wizard.resum.factures.client:0
msgid "Generar resum"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:112
#, python-format
msgid "Consum facturat P2 (kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:115
#, python-format
msgid "Consum facturat p3 (kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:85
#, python-format
msgid "Lectura maxímetre P3 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:43
#, python-format
msgid "Preu potència P6 (€/kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:88
#, python-format
msgid "Lectura maxímetre P6 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: field:wizard.resum.factures.client,_6_p:0
msgid "6 periodes"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:146
#, python-format
msgid "CIF emisor"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:119
#, python-format
msgid "Preu energia P4 (€/kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:109
#, python-format
msgid "Consum facturat P1 (kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: constraint:ir.model:0
msgid "The Object name must start with x_ and not contain any special character !"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:48
#, python-format
msgid "Lectura anterior P2 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:54
#, python-format
msgid "Lectura anterior P4 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:94
#, python-format
msgid "Preu energia P2 (€/kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:107
#, python-format
msgid "Subtotal energia P6 (€) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:60
#, python-format
msgid "Lectura anterior P6 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:130
#, python-format
msgid "Total potencia €"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:121
#, python-format
msgid "Consum facturat P5 (kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: help:wizard.resum.factures.client,_6_p:0
msgid "Mostrar 6 periodes en l'apartat de línies de factura"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:138
#, python-format
msgid "Altres (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:96
#, python-format
msgid "Consum facturat p3 (kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:142
#, python-format
msgid "Comptador 1"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:83
#, python-format
msgid "Lectura maxímetre P1 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:125
#, python-format
msgid "Preu energia P6 (€/kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:24
#, python-format
msgid "Número de factura"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:27
#, python-format
msgid "Potència contractada (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:92
#, python-format
msgid "Subtotal energia P1 (€) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:33
#, python-format
msgid "Preu potència P1 (€/kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:131
#, python-format
msgid "Total energia activa €"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:117
#, python-format
msgid "Subtotal energia P3 (€) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:102
#, python-format
msgid "Consum facturat P5 (kWh) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:124
#, python-format
msgid "Consum facturat P6 (kWh) [B]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:84
#, python-format
msgid "Lectura maxímetre P2 (kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:135
#, python-format
msgid "IVA (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:137
#, python-format
msgid "Total lloguer (€)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:104
#, python-format
msgid "Subtotal energia P5 (€) [A]"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:73
#, python-format
msgid "Lectura anterior P4 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:37
#, python-format
msgid "Preu potència P3 (€/kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: view:wizard.resum.factures.client:0
msgid "Cancelar"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:41
#, python-format
msgid "Preu potència P5 (€/kW)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:62
#, python-format
msgid "Consum P6 (kWh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:143
#, python-format
msgid "Comptador 2"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:144
#, python-format
msgid "Comptador 3"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:78
#, python-format
msgid "Consum P5 (kVArh)"
msgstr ""

#. module: giscedata_resum_factures_client
#: code:addons/giscedata_resum_factures_client/wizard/giscedata_resum_factures_client.py:80
#, python-format
msgid "Lectura actual P6 (kVArh)"
msgstr ""

