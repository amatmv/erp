# -*- coding: utf-8 -*-
"""Resum de factures per enviar al client
"""
import csv
import StringIO
import base64
import copy
from datetime import datetime

try:
    from collections import OrderedDict
except:
    from ordereddict import OrderedDict

from osv import osv, fields
from tools.translate import _


class WizardResumFacturesClient(osv.osv_memory):
    """Wizard per generar el resum de factures a enviar al client
    """
    _name = 'wizard.resum.factures.client'

    vals_row = OrderedDict([
        # format:
        # (ref, [nom del camp en el CSV, actiu])
        ('ref_ctr', [_(u'Referència contracte'), True]),
        ('data_fact', [_(u'Data facturació'), True]),
        ('ref_ftr', [_(u'Número de factura'), True]),
        ('addr', [_(u'Adreça del CUPS'), True]),
        ('tarifa', [_(u'Tarifa'), True]),
        ('pot_ctr', [_(u'Potència contractada (kW)'), True]),
        ('d_ant', [_(u'Data lectura anterior'), True]),
        ('d_act', [_(u'Data lectura actual'), True]),
        ('mesos', [_(u'Mesos facturats'), True]),
        # potencies
        ('pot_p1', [_(u'Potència P1 (kW)'), True]),
        ('pot_preu_p1', [_(u'Preu potència P1 (€/kW)'), True]),
        ('pot_p2', [_(u'Potència P2 (kW)'), True]),
        ('pot_preu_p2', [_(u'Preu potència P2 (€/kW)'), True]),
        ('pot_p3', [_(u'Potència P3 (kW)'), True]),
        ('pot_preu_p3', [_(u'Preu potència P3 (€/kW)'), True]),
        ('pot_p4', [_(u'Potència P4 (kW)'), True]),
        ('pot_preu_p4', [_(u'Preu potència P4 (€/kW)'), True]),
        ('pot_p5', [_(u'Potència P5 (kW)'), True]),
        ('pot_preu_p5', [_(u'Preu potència P5 (€/kW)'), True]),
        ('pot_p6', [_(u'Potència P6 (kW)'), True]),
        ('pot_preu_p6', [_(u'Preu potència P6 (€/kW)'), True]),
        # lectures energia activa
        ('l_act_pre_p1', [_(u'Lectura anterior P1 (kWh)'), True]),
        ('l_act_cur_p1', [_(u'Lectura actual P1 (kWh)'), True]),
        ('consum_act_p1', [_(u'Consum P1 (kWh)'), True]),
        ('l_act_pre_p2', [_(u'Lectura anterior P2 (kWh)'), True]),
        ('l_act_cur_p2', [_(u'Lectura actual P2 (kWh)'), True]),
        ('consum_act_p2', [_(u'Consum P2 (kWh)'), True]),
        ('l_act_pre_p3', [_(u'Lectura anterior P3 (kWh)'), True]),
        ('l_act_cur_p3', [_(u'Lectura actual P3 (kWh)'), True]),
        ('consum_act_p3', [_(u'Consum P3 (kWh)'), True]),
        ('l_act_pre_p4', [_(u'Lectura anterior P4 (kWh)'), True]),
        ('l_act_cur_p4', [_(u'Lectura actual P4 (kWh)'), True]),
        ('consum_act_p4', [_(u'Consum P4 (kWh)'), True]),
        ('l_act_pre_p5', [_(u'Lectura anterior P5 (kWh)'), True]),
        ('l_act_cur_p5', [_(u'Lectura actual P5 (kWh)'), True]),
        ('consum_act_p5', [_(u'Consum P5 (kWh)'), True]),
        ('l_act_pre_p6', [_(u'Lectura anterior P6 (kWh)'), True]),
        ('l_act_cur_p6', [_(u'Lectura actual P6 (kWh)'), True]),
        ('consum_act_p6', [_(u'Consum P6 (kWh)'), True]),
        # lectures energia reactiva
        ('l_rea_pre_p1', [_(u'Lectura anterior P1 (kVArh)'), True]),
        ('l_rea_cur_p1', [_(u'Lectura actual P1 (kVArh)'), True]),
        ('consum_rea_p1', [_(u'Consum P1 (kVArh)'), True]),
        ('l_rea_pre_p2', [_(u'Lectura anterior P2 (kVArh)'), True]),
        ('l_rea_cur_p2', [_(u'Lectura actual P2 (kVArh)'), True]),
        ('consum_rea_p2', [_(u'Consum P2 (kVArh)'), True]),
        ('l_rea_pre_p3', [_(u'Lectura anterior P3 (kVArh)'), True]),
        ('l_rea_cur_p3', [_(u'Lectura actual P3 (kVArh)'), True]),
        ('consum_rea_p3', [_(u'Consum P3 (kVArh)'), True]),
        ('l_rea_pre_p4', [_(u'Lectura anterior P4 (kVArh)'), True]),
        ('l_rea_cur_p4', [_(u'Lectura actual P4 (kVArh)'), True]),
        ('consum_rea_p4', [_(u'Consum P4 (kVArh)'), True]),
        ('l_rea_pre_p5', [_(u'Lectura anterior P5 (kVArh)'), True]),
        ('l_rea_cur_p5', [_(u'Lectura actual P5 (kVArh)'), True]),
        ('consum_rea_p5', [_(u'Consum P5 (kVArh)'), True]),
        ('l_rea_pre_p6', [_(u'Lectura anterior P6 (kVArh)'), True]),
        ('l_rea_cur_p6', [_(u'Lectura actual P6 (kVArh)'), True]),
        ('consum_rea_p6', [_(u'Consum P6 (kVAr)'), True]),
        # lectures maxímetres
        ('l_max_p1', [_(u'Lectura maxímetre P1 (kW)'), True]),
        ('l_max_p2', [_(u'Lectura maxímetre P2 (kW)'), True]),
        ('l_max_p3', [_(u'Lectura maxímetre P3 (kW)'), True]),
        ('l_max_p4', [_(u'Lectura maxímetre P4 (kW)'), True]),
        ('l_max_p5', [_(u'Lectura maxímetre P5 (kW)'), True]),
        ('l_max_p6', [_(u'Lectura maxímetre P6 (kW)'), True]),
        # línies energia A
        ('ene_A_p1', [_(u'Consum facturat P1 (kWh) [A]'), True]),
        ('ene_A_preu_p1', [_(u'Preu energia P1 (€/kWh) [A]'), True]),
        ('ene_A_subt_p1', [_(u'Subtotal energia P1 (€) [A]'), True]),
        ('ene_A_p2', [_(u'Consum facturat P2 (kWh) [A]'), True]),
        ('ene_A_preu_p2', [_(u'Preu energia P2 (€/kWh) [A]'), True]),
        ('ene_A_subt_p2', [_(u'Subtotal energia P2 (€) [A]'), True]),
        ('ene_A_p3', [_(u'Consum facturat p3 (kWh) [A]'), True]),
        ('ene_A_preu_p3', [_(u'Preu energia P3 (€/kWh) [A]'), True]),
        ('ene_A_subt_p3', [_(u'Subtotal energia P3 (€) [A]'), True]),
        ('ene_A_p4', [_(u'Consum facturat P4 (kWh) [A]'), False]),
        ('ene_A_preu_p4', [_(u'Preu energia P4 (€/kWh) [A]'), False]),
        ('ene_A_subt_p4', [_(u'Subtotal energia P4 (€) [A]'), False]),
        ('ene_A_p5', [_(u'Consum facturat P5 (kWh) [A]'), False]),
        ('ene_A_preu_p5', [_(u'Preu energia P5 (€/kWh) [A]'), False]),
        ('ene_A_subt_p5', [_(u'Subtotal energia P5 (€) [A]'), False]),
        ('ene_A_p6', [_(u'Consum facturat P6 (kWh) [A]'), False]),
        ('ene_A_preu_p6', [_(u'Preu energia P6 (€/kWh) [A]'), False]),
        ('ene_A_subt_p6', [_(u'Subtotal energia P6 (€) [A]'), False]),
        # línies energia B
        ('ene_B_p1', [_(u'Consum facturat P1 (kWh) [B]'), True]),
        ('ene_B_preu_p1', [_(u'Preu energia P1 (€/kWh) [B]'), True]),
        ('ene_B_subt_p1', [_(u'Subtotal energia P1 (€) [B]'), True]),
        ('ene_B_p2', [_(u'Consum facturat P2 (kWh) [B]'), True]),
        ('ene_B_preu_p2', [_(u'Preu energia P2 (€/kWh) [B]'), True]),
        ('ene_B_subt_p2', [_(u'Subtotal energia P2 (€) [B]'), True]),
        ('ene_B_p3', [_(u'Consum facturat p3 (kWh) [B]'), True]),
        ('ene_B_preu_p3', [_(u'Preu energia P3 (€/kWh) [B]'), True]),
        ('ene_B_subt_p3', [_(u'Subtotal energia P3 (€) [B]'), True]),
        ('ene_B_p4', [_(u'Consum facturat P4 (kWh) [B]'), False]),
        ('ene_B_preu_p4', [_(u'Preu energia P4 (€/kWh) [B]'), False]),
        ('ene_B_subt_p4', [_(u'Subtotal energia P4 (€) [B]'), False]),
        ('ene_B_p5', [_(u'Consum facturat P5 (kWh) [B]'), False]),
        ('ene_B_preu_p5', [_(u'Preu energia P5 (€/kWh) [B]'), False]),
        ('ene_B_subt_p5', [_(u'Subtotal energia P5 (€) [B]'), False]),
        ('ene_B_p6', [_(u'Consum facturat P6 (kWh) [B]'), False]),
        ('ene_B_preu_p6', [_(u'Preu energia P6 (€/kWh) [B]'), False]),
        ('ene_B_subt_p6', [_(u'Subtotal energia P6 (€) [B]'), False]),
        # total facturat
        ('total_kwh', [_(u'Total energia (kWh)'), True]),
        # subtotals
        ('subt_pot', [_(u'Total potencia €'), True]),
        ('subt_ene_act', [_(u'Total energia activa €'), True]),
        ('subt_ene_rea', [_(u'Total energia reactiva €'), True]),
        # Impostos i totals
        ('iese', [_(u'IESE (€)'), True]),
        ('iva', [_(u'IVA (€)'), True]),
        ('total_impost', [_(u'Total impostos (€)'), True]),
        ('lloguer', [_(u'Total lloguer (€)'), True]),
        ('altres', [_(u'Altres (€)'), True]),
        ('base', [_(u'Total base (€)'), True]),
        ('total', [_(u'Total (€)'), True]),
        # Altres dades
        ('compt_1', [_(u'Comptador 1'), True]),
        ('compt_2', [_(u'Comptador 2'), True]),
        ('compt_3', [_(u'Comptador 3'), True]),
        ('cups', [_(u'CUPS'), True]),
        ('cif', [_(u'CIF emisor'), True]),
    ])

    n_elems = 2
    pos_head = 0
    pos_bool = 1
    pos_dada = 2

    def mod_data(self, data):
        """Modifica el format de la data
        """
        return datetime.strptime(data, "%Y-%m-%d").strftime("%d/%m/%Y")

    def calc_mesos(self, inici, final):
        """Retorna el nombre de mesos compresos entre les dues dates
        """
        d_ini = datetime.strptime(inici, '%Y-%m-%d')
        d_fi = datetime.strptime(final, '%Y-%m-%d')
        return (d_fi - d_ini).days/30

    def linies_altres(self, cursor, uid, fact, context=None):
        """Retorna les línies de tipus altres
        """
        if not context:
            context = {}
        ids = fact._ff_linies_de('', {'tipus': 'altres'})[fact.id]
        lin = self.pool.get('giscedata.facturacio.factura.linia')
        return lin.browse(cursor, uid, ids, context)

    def action_generar_dades(self, cursor, uid, ids, context=None):
        """Genera el CSV amb el resum de les factures ids
        """
        n_elems = self.n_elems
        pos_bool = self.pos_bool
        pos_head = self.pos_head
        pos_dada = self.pos_dada
        output = StringIO.StringIO()
        if isinstance(ids, list):
            ids = ids[0]
        wiz = self.browse(cursor, uid, ids, context)
        _6_p = wiz._6_p
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE,
                            escapechar='"')
        if not context:
            context = {}
        head_row = [i[pos_head] for i in self.vals_row.values()
                                            if (_6_p or i[pos_bool])]
        writer.writerow(head_row)
        fact_obj = self.pool.get('giscedata.facturacio.factura')
        f_id = context.get('active_ids')
        for fact in fact_obj.browse(cursor, uid, f_id, context):
            row = copy.deepcopy(self.vals_row)
            row['ref_ctr'].append(fact.polissa_id.name)
            row['data_fact'].append(self.mod_data(fact.date_invoice))
            row['ref_ftr'].append(fact.number)
            row['addr'].append(fact.cups_id.direccio)
            row['tarifa'].append(fact.polissa_id.tarifa.name)
            row['pot_ctr'].append(fact.polissa_id.potencia)
            if fact.lectures_energia_ids:
                d_ant = self.mod_data(
                            fact.lectures_energia_ids[0].data_anterior)
                d_act = self.mod_data(
                            fact.lectures_energia_ids[0].data_actual)
            else:
                d_ant = ''
                d_act = ''
            row['d_ant'].append(d_ant)
            row['d_act'].append(d_act)
            row['mesos'].append(self.calc_mesos(fact.data_inici,
                                                fact.data_final))
            # línies de potència
            subtotal_pot = 0
            for pot in fact.linies_potencia:
                key = '%s' % pot.product_id.name.lower()
                row['pot_%s' % key].append(pot.quantity)
                row['pot_preu_%s' % key].append(pot.price_unit_multi)
                subtotal_pot += pot.price_subtotal
            row['subt_pot'].append(subtotal_pot)
            # lectures d'energia
            for ene in fact.lectures_energia_ids:
                compt = ene.comptador_id.name
                for i in range(1, 4):
                    compt_key = 'compt_%s' % i
                    if len(row[compt_key]) == n_elems:
                        row[compt_key].append(compt)
                        break
                    if ((len(row[compt_key]) > n_elems) and
                                    (compt == row[compt_key][n_elems])):
                        break
                key = ene.name[ene.name.find('(')+1:ene.name.find(')')].lower()
                lect_pre = ene.lect_anterior
                lect_cur = ene.lect_actual
                consum = ene.consum
                if ene.tipus == 'activa':
                    row['l_act_pre_%s' % key].append(lect_pre)
                    row['l_act_cur_%s' % key].append(lect_cur)
                    row['consum_act_%s' % key].append(consum)
                else:
                    row['l_rea_pre_%s' % key].append(lect_pre)
                    row['l_rea_cur_%s' % key].append(lect_cur)
                    row['consum_rea_%s' % key].append(consum)
            # lectures maxímetres
            for p_max in fact.lectures_potencia_ids:
                periode = p_max.name.lower()
                row['l_max_%s' % periode].append(p_max.pot_maximetre)
            # línies d'energia
            subtotal_ene_activa = 0
            total_kwh = 0
            for ene in fact.linies_energia:
                key = '%s' % ene.product_id.name.lower()
                if len(row['ene_A_%s' % key]) > n_elems:
                    ordre = 'B'
                else:
                    ordre = 'A'
                row['ene_%s_%s' % (ordre, key)].append(ene.quantity)
                total_kwh += ene.quantity
                row['ene_%s_preu_%s' % (ordre, key)].append(
                    ene.price_unit_multi
                )
                row['ene_%s_subt_%s' % (ordre, key)].append(ene.price_subtotal)
                subtotal_ene_activa += ene.price_subtotal
            row['total_kwh'].append(total_kwh)
            row['subt_ene_act'].append(subtotal_ene_activa)
            subtotal_ene_reactiva = 0
            for ene in fact.linies_reactiva:
                subtotal_ene_reactiva += ene.price_subtotal
            row['subt_ene_rea'].append(subtotal_ene_reactiva)
            # impostos i totals
            iva = 0
            iese = 0
            for tax in fact.tax_line:
                if 'iva' in tax.name.lower():
                    iva += tax.amount
                else:
                    iese += tax.amount
            row['iva'].append(iva)
            row['iese'].append(iese)
            row['total_impost'].append(fact.amount_tax)
            lloguer = 0
            for lin in fact.linies_lloguer:
                lloguer += lin.price_subtotal
            row['lloguer'].append(lloguer)
            altres = 0
            for lin in self.linies_altres(cursor, uid, fact):
                altres += lin.price_subtotal
            row['altres'].append(altres)
            row['base'].append(fact.amount_untaxed)
            row['total'].append(fact.amount_total)
            # Altres
            row['cups'].append(fact.cups_id.name)
            row['cif'].append(fact.company_id.partner_id.vat)
            tmp_row = [len(i) > n_elems and str(i[pos_dada]) or ""
                            for i in row.values() if (_6_p or i[pos_bool])]
            writer.writerow(tmp_row)
        data = base64.b64encode(output.getvalue())
        wiz.write({'state': 'done', 'name': 'resum.csv', 'file': data})

    _columns = {
        'name': fields.char('Filename', size=128),
        'file': fields.binary('Fitxer CSV'),
        'state': fields.char('State', size=16),
        '_6_p': fields.boolean('6 periodes', help=u"Mostrar 6 periodes en "
                                                  u"l'apartat de línies de"
                                                  u" factura")
    }

    _defaults = {
        'state': lambda *a: 'init',
        '_6_p': lambda *a: False,
    }

WizardResumFacturesClient()
