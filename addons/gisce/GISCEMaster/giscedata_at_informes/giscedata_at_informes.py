# -*- coding: iso-8859-1 -*-
from osv import osv, fields


class giscedata_at_informes_trams_per_cable(osv.osv):
    _name = "giscedata.at.informes.trams.per.cable"
    _description = "Informe de trams per tipus de cable"
    _auto = False
    _columns = {
      'id': fields.integer('Id', readonly=True),
      'cable': fields.char('Cable', size=50, readonly=True),
      'seccio': fields.integer('Secció', readonly=True),
      'material': fields.char('Material', size=50, readonly=True),
      'n_trams': fields.integer('Nº de trams', readonly=True),
    }

    _order = "n_trams asc"

    def init(self, cr):
        cr.execute("""
          create or replace view giscedata_at_informes_trams_per_cable as (
           select t.cable as id, c.name as cable, c.seccio as seccio, m.name as material, count(c.id) as n_trams from giscedata_at_tram t, giscedata_at_cables c, giscedata_at_material m where t.cable = c.id and c.material = m.id group by t.cable, c.name, c.seccio, m.name order by count(c.id), c.name, c.seccio, m.name)""")

giscedata_at_informes_trams_per_cable()
