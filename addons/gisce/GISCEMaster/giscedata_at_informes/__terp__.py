# -*- coding: utf-8 -*-
{
    "name": "GISCE Data AT Informes",
    "description": """Informes de linies alta""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Linies Alta",
    "depends":[
        "giscedata_at"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_at_informes_view.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
