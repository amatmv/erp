# -*- coding: utf-8 -*-
{
    "name": "Giscedata correos Bono Social",
    "description": """
Modulo de integración del bono social al sisteme sicer y correos españa :
    * Workflow de los estados de la notificación
    """,
    "version": "0-dev",
    "author": "Gisce",
    "category": "GISCEMaster",
    "depends":[
        "giscedata_correos_base_flux",
        "giscedata_facturacio_impagat_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "giscedata_correos_bono_social_data.xml",
    ],
    "active": False,
    "installable": True
}