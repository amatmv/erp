# -*- coding: utf-8 -*-
{
    "name": "giscemis_participants link facturacio_comer",
    "description": """Aquest mòdul defineix el pricelist type""",
    "version": "0-dev",
    "author": "GISCE",
    "category": "Master",
    "depends":[
        "giscemisc_participant",
        "giscedata_polissa_comer"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
    ],
    "active": False,
    "installable": True
}
