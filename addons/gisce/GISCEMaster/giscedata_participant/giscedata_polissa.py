# -*- coding: utf-8 -*-
from osv import osv, fields
from tools.translate import _


class GiscedataPolissa(osv.osv):
    _name = 'giscedata.polissa'
    _inherit = 'giscedata.polissa'

    _columns = {
        'distribuidora': fields.many2one('res.partner', 'Distribuidora',
                                         domain=[('supplier', '=', 1), ('energy_sector', 'in', ['electric', 'elegas'])],
                                         readonly=True,
                                         states={
                                             'esborrany': [('readonly', False)],
                                             'validar': [('readonly', False),
                                                         ('required', True)],
                                             'modcontractual': [
                                                 ('readonly', False),
                                                 ('required', True)]
                                         }),
    }

GiscedataPolissa()
