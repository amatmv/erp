# -*- coding: utf-8 -*-
{
    "name": "Mòdul de Agregacions i Intercanvi de fitxers amb REE (Regim Especial)",
    "description": """
Ens permet generar els diferents fitxers per comunicar-nos amb REE (Red Electrica España) de Règim Especial.
  * MAGRE
  * INMERE
  * MEDIDAS
  """,
    "version": "0-dev",
    "author": "GISCE",
    "category": "GISCEMaster",
    "depends":[
        "base",
        "giscedata_re",
        "giscedata_administracio_publica_ree"
    ],
    "init_xml": [],
    "demo_xml": [],
    "update_xml":[
        "wizard/medidas_ree_view.xml",
        "giscedata_agree_re_wizard.xml",
        "security/ir.model.access.csv"
    ],
    "active": False,
    "installable": True
}
