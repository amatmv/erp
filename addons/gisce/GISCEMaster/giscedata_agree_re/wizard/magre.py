# -*- coding: utf-8 -*-

import wizard
import pooler
import base64
import csv
import StringIO
import time
import bz2

def _selection_mes(self, cr, uid, context={}):
    cr.execute("""select distinct to_char(inici, 'MM/YYYY') as month, to_char(inici, 'YYYYMM') as ts from giscedata_re_curva where compact = True order by ts desc""")
    return [(a[0], a[0]) for a in cr.fetchall()]

_triar_mes_form = """<?xml version="1.0"?>
<form string="MAGRE" col="4">
  <field name="mes" />
</form>"""

_triar_mes_fields = {
  'mes': {'string': 'Mes', 'type': 'selection', 'selection': _selection_mes},
}

def _check_mes(self, cr, uid, data, context={}):
    # Comprovem que totes les instal·lacions tenen la curva de càrrega entrada y compactada
    # pel mes que anem a fer
    ok = []
    txt = []
    cr.execute("""select
    i.name,
    c.compact
  from
    giscedata_re i
  left join giscedata_re_curva c
    on (c.inst_re = i.id and to_char(c.inici, 'MM/YYYY') = %s)
  where i.active = True""", (data['form']['mes'],))

    insts = cr.dictfetchall()

    if not len(insts):
        ok.append(False)

    for inst in insts:
        if inst['compact'] == True:
            txt.append(u"* La instalación "
                       u" %s está lista" % inst['name'])
            ok.append(True)
        if inst['compact'] == False:
            txt.append(u"* La instalación %s no tiene "
                       u" la curva compactada" % inst['name'])
            ok.append(False)
        if inst['compact'] == None:
            txt.append(u"* La instalación %s no tiene "
                       u" curva de carga" % inst['name'])
            ok.append(False)

    if reduce(lambda a,b: a and b, ok):
        # Tot correcte, podem generar el MAGRE
        self.states['check_mes']['result']['state'] = [('gen_fitxer', 'Generar MAGRE')]
    else:
        # No es pot generar el magre
        self.states['check_mes']['result']['state'] = [('gen_fitxer', 'Generar MAGRE')]

    return {
      'text': '\n'.join(txt),
    }

_check_mes_form = """<?xml version="1.0"?>
<form string="MAGRE" col="4">
  <field name="text" readonly="1" colspan="4" nolabel="1" width="500"/>
</form>"""

_check_mes_fields = {
  'text': {'string': 'Texto', 'type': 'text', 'readonly': True},
}


def _gen_fitxer(self, cr, uid, data, context={}):
    perf_obj = pooler.get_pool(cr.dbname).get('giscedata.re.curva.lectura')
    user_obj = pooler.get_pool(cr.dbname).get('res.users')
    distri = user_obj.browse(cr, uid, uid).company_id.cups_code
    output = StringIO.StringIO()
    month,year = data['form']['mes'].split('/')
    output = StringIO.StringIO()
    writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
    # Busquem les diferents agregacions que hi ha per aquest mes en els perfils
    cr.execute("""select distinct distribuidora,tipo,magnitud,upr,provincia 
    from giscedata_re_curva_lectura 
    where year = %s and month = %s
    and magnitud <> 'AE'
    order by magnitud""", (year, month))
    agregs = cr.dictfetchall()

    # Mirem els dies que te aquest mes en la perfilacio
    cr.execute("""select distinct day from giscedata_re_curva_lectura 
    where year = %s and month = %s
    order by day""", (year, month))
    for day in cr.fetchall():
        day = day[0]
        for val in agregs:
            line = []
            energia = {}
            energia_reg = {}
            quant = {}
            quant_reg = {}
            # Construim un diccionari amb 25 hores
            for hora in range (1,26):
                energia[str(hora).zfill(2)] = 0
                quant[str(hora).zfill(2)] = 0
                energia_reg[str(hora).zfill(2)] = 0
                quant_reg[str(hora).zfill(2)] = 0
            search = [('month', '=', month), ('year', '=', year), ('day', '=', day)]
            for key in val.keys():
                search.append((key, '=', val[key]))

            perf_ids = perf_obj.search(cr, uid, search)

            if len(perf_ids):
                estacio = perf_obj.browse(cr, uid, perf_ids[0]).estacio
                extra = 0
                for perf in perf_obj.browse(cr, uid, perf_ids):
                    desde_reg = False # de moment mai vindra del registrador
                    if estacio == 1 and perf.estacio == 0:
                        extra = 1
                    if perf.hour == 0:
                        energia_reg['24'] += perf.lectura
                        quant_reg['24'] += 1
                        energia['24'] += perf.lectura
                        quant['24'] += 1
                    else:
                        energia_reg[str(perf.hour + extra).zfill(2)] += perf.lectura
                        quant_reg[str(perf.hour + extra).zfill(2)] += 1
                        energia[str(perf.hour + extra).zfill(2)] += perf.lectura
                        quant[str(perf.hour + extra).zfill(2)] += 1
                    estacio = perf.estacio

                # Creem la linia del fitxer (Només si existeixen valors per aquell perfilat?)
                line.append(day)
                line.append(month)
                line.append(year)
                line.append(val['distribuidora'])
                line.append(val['upr'])
                line.append(val['tipo'])
                line.append(val['provincia'])
                line.append(val['magnitud'])
                for hora in sorted(energia):
                    line.append(energia[hora]) # No sé què és camp Li
                    line.append(quant[hora])
                    line.append(energia_reg[hora]) # prové de curva de carrega
                    line.append(quant_reg[hora]) # Numero de PS sense estimacio
                    line.append(1) # Medida agregada provisional (0) o definitiva (1)
                writer.writerow(line)

    comp = bz2.BZ2Compressor()
    comp.compress(output.getvalue())
    output.close()
    file = base64.b64encode(comp.flush())
    # Agafem el nom del fitxer
    #filename = '%s.bz2' % pooler.get_pool(cr.dbname).get('ir.sequence').get(cr, uid, 'giscedata.agree.file.magr3')
    yearmonth = '%s%s' % (year,month)
    return {'name': 'MAGRE_%s_%s_%s.0.bz2'  % (distri, yearmonth, time.strftime('%Y%m%d')), 'file': file}

_gen_fitxer_form = """<?xml version="1.0"?>
<form string="MAGRE" col="4">
  <field name="name" readonly="1" colspan="4"/>
  <field name="file" readonly="1" nolabel="1" colspan="4"/>
</form>"""

_gen_fitxer_fields = {
  'name': {'string': 'Fichero', 'type': 'char', 'size': 255, 'readonly': True},
  'file': {'string': 'Contenido Fichero', 'type': 'binary', 'readonly': True},
}

class wizard_re_magre(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'triar_mes'},
      },
      'triar_mes': {
        'actions': [],
        'result': {'type': 'form', 'arch': _triar_mes_form, 'fields': _triar_mes_fields, 'state': [('check_mes', 'Siguiente')]}
      },
      'check_mes': {
        'actions': [_check_mes],
        'result': {'type': 'form', 'arch': _check_mes_form, 'fields': _check_mes_fields, 'state': [('gen_fitxer', 'Generar MAGRE')]}
      },
      'gen_fitxer': {
        'actions': [_gen_fitxer],
        'result': {'type': 'form', 'arch': _gen_fitxer_form, 'fields': _gen_fitxer_fields, 'state': [('end', 'Finalizar')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'},
      }
    }

wizard_re_magre('giscedata.agree.re.magre')
