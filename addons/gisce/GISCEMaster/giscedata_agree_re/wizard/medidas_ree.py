# -*- coding: utf-8 -*-
import wizard
import pooler
import base64
import csv
import StringIO
import bz2
import time
from tools.translate import _
from osv import osv, fields

class WizardMedidasREE(osv.osv_memory):
    """Assistent per generar fitxers medidas.
    """
    _name = 'wizard.medidas.ree'

    def _selection_mes(self, cursor, uid, ids, context=None):
        cursor.execute(
            """select distinct to_char(inici, 'MM/YYYY') as month, to_char(inici, 'YYYYMM') as ts from giscedata_re_curva where compact = True order by ts desc""")
        return [(a[0], a[0]) for a in cursor.fetchall()]

    def _check_mes(self, cursor, uid, mes):
        # Comprovem que totes les instal·lacions tenen la curva de càrrega entrada y compactada
        # pel mes que anem a fer
        ok = []
        txt = []
        cursor.execute("""select
            i.name,
            c.compact
          from
            giscedata_re i
          left join giscedata_re_curva c
            on (c.inst_re = i.id and to_char(c.inici, 'MM/YYYY') = %s)
          where i.active = True""", (mes,))

        insts = cursor.dictfetchall()

        if not len(insts):
            ok.append(False)

        for inst in insts:
            if inst['compact'] == True:
                txt.append(u"* La instalación "
                           u" %s está lista" % inst['name'])
                ok.append(True)
            if inst['compact'] == False:
                txt.append(u"* La instalación %s no tiene "
                           u" la curva compactada" % inst['name'])
                ok.append(False)
            if inst['compact'] == None:
                txt.append(u"* La instalación %s no tiene "
                           u" curva de carga" % inst['name'])
                ok.append(False)

        if reduce(lambda a, b: a and b, ok):
            # Tot correcte, podem generar el MEDIDAS
            return True
        else:
            # No es pot generar el MEDIDAS
            _msg = _('No es pot generar el fitxer MEDIDAS')
            raise osv.except_osv('Error', _msg)

    def _gen_fitxer(self, cursor, uid, ids, context=None):
        wizard = self.browse(cursor, uid, ids[0], context)
        perf_obj = pooler.get_pool(cursor.dbname).get('giscedata.re.curva.lectura')
        user_obj = pooler.get_pool(cursor.dbname).get('res.users')
        distri = user_obj.browse(cursor, uid, uid).company_id.cups_code
        output = StringIO.StringIO()
        month, year = wizard.mes.split('/')
        output = StringIO.StringIO()
        writer = csv.writer(output, delimiter=';', quoting=csv.QUOTE_NONE)
        # Busquem les diferents agregacions que hi ha per aquest mes en els perfils
        cursor.execute(
            "select distinct cups from giscedata_re_curva_lectura where year = %s and month = %s order by cups asc",
            (year, month))
        cils = [a[0] for a in cursor.fetchall()]

        # Per cada CIL creem la seva curva horaria
        for cil in cils:
            cursor.execute("""select
              cups,
              (substring(name, 1, 4) || '-' || substring(name, 5, 2) || '-' || substring(name, 7, 2) || ' ' || substring(name, 9, 2) || ':00:00')::timestamp as timestamp,
              estacio,
              sum(l_liq) as l_liq,
              sum(l_r2) as l_r2,
              sum(l_r3) as l_r3
            from (
              (select
                cups,
                name,
                estacio,
                lectura as l_liq,
                0 as l_r2,
                0 as l_r3
              from giscedata_re_curva_lectura
              where
                cups = %s
                and month = %s
                and year = %s
                and magnitud = 'AS'
              )
              union
              (select
                cups,
                name,
                estacio,
                -lectura as l_liq,
                0 as l_r2,
                0 as l_r3
              from giscedata_re_curva_lectura
              where
                cups = %s
                and month = %s
                and year = %s
                and magnitud = 'AE'
              )
              union
              (select
                cups,
                name,
                estacio,
                0 as l_liq,
                lectura as l_r2,
                0 as l_r3
               from giscedata_re_curva_lectura
               where
                cups = %s
                and month = %s
                and year = %s
                and magnitud = 'R2'
              )
              union
              (select
                cups,
                name,
                estacio,
                0 as l_liq,
                0 as l_r2,
                lectura as l_r3
               from giscedata_re_curva_lectura
               where
                cups = %s
                and month = %s
                and year = %s
                and magnitud = 'R3'
              )
            ) as foo
            group by
              cups,
              name,
              estacio
            order by
              name asc""", (cil, month, year) * 4)
            for r in cursor.dictfetchall():
                line = []
                line.append('%s001' % r['cups'])
                line.append(r['timestamp'].replace('-', '/'))
                line.append(r['estacio'])
                line.append(r['l_liq'])
                line.append(r['l_r2'])
                line.append(r['l_r3'])
                line.append(None)
                line.append(None)
                line.append('R')
                line.append(None)  # Això és el punt i coma de final de línia

                writer.writerow(line)

        bzdata = bz2.compress(output.getvalue())
        medidas_file = base64.b64encode(bzdata)
        output.close()

        yearmonth = '%s%s' % (year, month)
        filename = 'medidas_{distri}_{date}_2_{date_now}.0.bz2'.format(
            distri=distri, date=yearmonth, date_now=time.strftime('%Y%m%d')
        )
        wizard.write({'name': filename, 'file': medidas_file, 'state': 'done'})

    _columns = {
        'name': fields.char('Nom', size=50),
        'file': fields.binary('Fitxer'),
        'mes': fields.selection(_selection_mes, 'Mes'),
        'state': fields.char('State', size=4),
    }

    _defaults = {
        'state': lambda *a: 'init',
    }

WizardMedidasREE()