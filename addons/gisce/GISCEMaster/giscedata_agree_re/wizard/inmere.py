# -*- coding: utf-8 -*-

import wizard
import pooler
import base64
import tarfile
import bz2
from datetime import datetime, timedelta
from osv import osv
from tools.translate import _
try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO
from zipfile import ZipFile, BadZipfile, ZIP_DEFLATED


def _selection_mes(self, cursor, uid, context=None):
    cursor.execute("SELECT "
                   "distinct month::varchar||'/'||year::varchar as mes, "
                   "year::varchar||month::varchar as ts from "
                   "giscedata_re_curva_lectura order by ts desc")
    return [(a[0], a[0]) for a in cursor.fetchall()]

_triar_mes_form = """<?xml version="1.0"?>
<form string="INMERE" col="4">
  <field name="mes" required="1"/>
  <field name="bz2" />
</form>"""

_triar_mes_fields = {
  'mes': {'string': 'Mes', 'type': 'selection', 'selection': _selection_mes},
  'bz2': {'string': 'bz2', 'type': 'boolean', 'readonly': False},
}


def _gen_fitxer(self, cursor, uid, data, context=None):
    user_obj = pooler.get_pool(cursor.dbname).get('res.users')
    upr_obj = pooler.get_pool(cursor.dbname).get('giscedata.re.uprs')
    distri = user_obj.browse(cursor, uid, uid).company_id.cups_code
    month, year = data['form']['mes'].split('/')
    sql = """select
    cups,
    distribuidora,
    upr,
    tipo,
    provincia,
    min(data_inici) as data_inici,
    max(data_final) as data_final,
    sum(l_ae) as l_ae,
    sum(l_as) as l_as,
    sum(l_r1) as l_r1,
    sum(l_r2) as l_r2,
    sum(l_r3) as l_r3,
    sum(l_r4) as l_r4
FROM (
    select
        l.cups,
        l.distribuidora,
        l.upr,
        l.tipo,
        l.provincia,
        min(l.name) as data_inici,
        max(l.name) as data_final,
        sum(l.lectura)::int as l_ae,
        0 as l_as,
        0 as l_r1,
        0 as l_r2,
        0 as l_r3,
        0 as l_r4
    FROM
        giscedata_re_curva_lectura l
    WHERE
        l.magnitud = 'AE'
        and l.year = %s
        and l.month = %s
    GROUP BY l.cups,l.distribuidora,l.upr,l.tipo,l.provincia
    UNION
    select
        l.cups,
        l.distribuidora,
        l.upr,
        l.tipo,
        l.provincia,
        min(l.name) as data_inici,
        max(l.name) as data_final,
        0 as l_ae,
        sum(l.lectura)::int as l_as,
        0 as l_r1,
        0 as l_r2,
        0 as l_r3,
        0 as l_r4
    FROM
        giscedata_re_curva_lectura l
    WHERE
        l.magnitud = 'AS'
        and l.year = %s
        and l.month = %s
    GROUP BY l.cups,l.distribuidora,l.upr,l.tipo,l.provincia
    UNION
    select
        l.cups,
        l.distribuidora,
        l.upr,
        l.tipo,
        l.provincia,
        min(l.name) as data_inici,
        max(l.name) as data_final,
        0 as l_ae,
        0 as l_as,
        sum(l.lectura)::int as l_r1,
        0 as l_r2,
        0 as l_r3,
        0 as l_r4
    FROM
        giscedata_re_curva_lectura l
    WHERE
        l.magnitud = 'R1'
        and l.year = %s
        and l.month = %s
    GROUP BY l.cups,l.distribuidora,l.upr,l.tipo,l.provincia
    UNION
    select
        l.cups,
        l.distribuidora,
        l.upr,
        l.tipo,
        l.provincia,
        min(l.name) as data_inici,
        max(l.name) as data_final,
        0 as l_ae,
        0 as l_as,
        0 as l_r1,
        sum(l.lectura)::int as l_r2,
        0 as l_r3,
        0 as l_r4
    FROM
        giscedata_re_curva_lectura l
    WHERE
        l.magnitud = 'R2'
        and l.year = %s
        and l.month = %s
    GROUP BY l.cups,l.distribuidora,l.upr,l.tipo,l.provincia
    UNION
    select
        l.cups,
        l.distribuidora,
        l.upr,
        l.tipo,
        l.provincia,
        min(l.name) as data_inici,
        max(l.name) as data_final,
        0 as l_ae,
        0 as l_as,
        0 as l_r1,
        0 as l_r2,
        sum(l.lectura)::int as l_r3,
        0 as l_r4
    FROM
        giscedata_re_curva_lectura l
    WHERE
        l.magnitud = 'R3'
        and l.year = %s
        and l.month = %s
    GROUP BY l.cups,l.distribuidora,l.upr,l.tipo,l.provincia
    UNION
    select
        l.cups,
        l.distribuidora,
        l.upr,
        l.tipo,
        l.provincia,
        min(l.name) as data_inici,
        max(l.name) as data_final,
        0 as l_ae,
        0 as l_as,
        0 as l_r1,
        0 as l_r2,
        0 as l_r3,
        sum(l.lectura)::int as l_r4
    FROM
        giscedata_re_curva_lectura l
    WHERE
        l.magnitud = 'R4'
        and l.year = %s
        and l.month = %s
    GROUP BY l.cups,l.distribuidora,l.upr,l.tipo,l.provincia
) as foo GROUP by cups, distribuidora, upr, tipo, provincia"""
    cursor.execute(sql, (year, month) * 6)
    result = cursor.fetchall()
    # Mapeig de representants i UPRs
    repre = {}
    upr_ids = upr_obj.search(cursor, uid, [])
    for upr in upr_obj.browse(cursor, uid, upr_ids):
        repre[upr.name] = upr.representante.ref
    # Classifiquem segons UPR
    inmere_files = {}
    for line in result:
        line = list(line)
        upr = line[2]
        if upr not in repre:
            raise osv.except_osv(
                _('Error'),
                _(u"No s'ha trobat la unitat de programació %s.") % upr)
        inmere_files.setdefault(repre[upr], [])
        for pos in (5, 6):
            day, hour = line[pos][0:8], line[pos][8:10]
            d = datetime.strptime(day, '%Y%m%d')
            d += timedelta(hours=int(hour))
            line[pos] = d.strftime('%Y/%m/%d %H')
        inmere_files[repre[upr]].append(';'.join([str(a) for a in line]))
    # Generem els fitxers
    if data['form']['bz2']:
        memory_zip = StringIO()
        # Create zip file to write bz2s
        filename = 'INMERE_%s%s.zip' % (year, month)
        zf = ZipFile(memory_zip, mode='w', compression=ZIP_DEFLATED)

        for repre in inmere_files:
            inmere_f = StringIO('\n'.join(inmere_files[repre]))
            inmere_f.seek(0)
            bzdata = bz2.compress(inmere_f.getvalue())
            # ffile = base64.b64encode(bzdata)
            bz_filename = "INMERE_%s_%s_%s%s_%s.0.bz2" % (
                distri, repre, year, month, datetime.now().strftime('%Y%m%d')
            )
            zf.writestr(bz_filename, bzdata)
        zf.close()

        ffile = base64.b64encode(memory_zip.getvalue())

    else:
        tf = StringIO()
        tar = tarfile.open(name='INMERE_%s%s.tar' % (year, month),
                           mode='w', fileobj=tf)
        for repre in inmere_files:
            inmere_f = StringIO('\n'.join(inmere_files[repre]))
            inmere_f.seek(0)
            info = tarfile.TarInfo(name="INMERE_%s_%s_%s%s_%s.0" %
                                   (distri, repre, year, month,
                                    datetime.now().strftime('%Y%m%d')))
            info.size = len(inmere_f.getvalue())
            tar.addfile(tarinfo=info, fileobj=inmere_f)
        tar.close()
        ffile = base64.b64encode(tf.getvalue())
        filename = 'INMERE_%s%s.tar' % (year, month)

    return {'name': filename, 'file': ffile}

_gen_fitxer_form = """<form string="INMERE" col="4">
  <field name="name" readonly="1" colspan="4"/>
  <field name="file" readonly="1" nolabel="1" colspan="4"/>
</form>"""

_gen_fitxer_fields = {
  'name': {'string': 'Fichero', 'type': 'char', 'size': 255, 'readonly': True},
  'file': {'string': 'Contenido Fichero', 'type': 'binary', 'readonly': True},
}


class wizard_inmere(wizard.interface):

    states = {
      'init': {
        'actions': [],
        'result': {'type': 'state', 'state': 'triar_mes'},
      },
      'triar_mes': {
        'actions': [],
        'result': {'type': 'form', 'arch': _triar_mes_form, 'fields': _triar_mes_fields, 'state': [('gen_fitxer', 'Generar INMERE')]}
      },
      'gen_fitxer': {
        'actions': [_gen_fitxer],
        'result': {'type': 'form', 'arch': _gen_fitxer_form, 'fields': _gen_fitxer_fields, 'state': [('end', 'Finalizar')]}
      },
      'end': {
        'actions': [],
        'result': {'type': 'state', 'state': 'end'},
      }
    }

wizard_inmere('giscedata.agree.re.inmere')
