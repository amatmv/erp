import sys
import click
import logging
from iec870ree_moxa import Moxa
from iec870ree.ip import Ip
from iec870ree.protocol import LinkLayer, AppLayer
from datetime import datetime
from dateutil.relativedelta import relativedelta
from erppeek import Client
from pyreemote.telemeasure import parse_profiles, parse_billings


def import_from_tm(c, meter, option, date_to):
    try:
        meter_obj = c.GiscedataLecturesComptador
        meter_id = meter_obj.search([('name', '=', meter)])
        meter_dates = meter_obj.read(meter_id[0], [
            'name', 'tm_last_load_billing', 'tm_last_load_profile',
            'tm_import_hour_profiles', 'tm_import_quarter_profiles',
            'tm_import_mbilling', 'tm_last_load_quarter_profile',
            'tm_password', 'tm_measuring_point', 'tm_link_address',
            'tm_port', 'tm_ip_address', 'tm_phone_number',
            'tm_contract_import'])
        logging.info(meter_dates)
        ip = meter_dates['tm_ip_address']
        port = meter_dates['tm_port']
        tlf = meter_dates['tm_phone_number']
        der = meter_dates['tm_link_address']
        dir_pm = meter_dates['tm_measuring_point']
        clave_pm = meter_dates['tm_password']
        contract = meter_dates['tm_contract_import']
        ip_layer = Ip((ip, port))
        # Enviar comandes AT (modem)
        physical_layer = Moxa(str(tlf), ip_layer)
        link_layer = LinkLayer(der, dir_pm)
        link_layer.initialize(physical_layer)
        app_layer = AppLayer()
        app_layer.initialize(link_layer)

        physical_layer.connect()
        link_layer.link_state_request(retries=0)
        link_layer.remote_link_reposition()
        logging.info("before authentication")
        resp = app_layer.authenticate(clave_pm)
        logging.info("CLIENTE authenticate response {}".format(resp))
        logging.info("before read")
        resp = app_layer.get_info()
        logging.info("read response {}".format(resp))
        logging.info("Gathering data...")

        if 'p' in option:
            gather_profiles(c, meter_dates, app_layer, option, date_to)
        elif option == 'b':
            gather_billings(c, meter_dates, app_layer, contract, date_to)
        else:
            logging.info("Wrong option! [p, p4, b]")

    except Exception as e:
        logging.info(e)
        raise e
    finally:
        app_layer.finish_session()
        physical_layer.disconnect()
        sys.exit(1)


def gather_profiles(c, meter_dates, app_layer, option, date_to):
    values = []
    if option == 'p4':
        curve_option = 'quarter_hour'
        field = 'tm_last_load_quarter_profile'
    else:
        curve_option = 'profiles'
        field = 'tm_last_load_profile'
    df = datetime.strptime(meter_dates[field], '%Y-%m-%d %H:%M:%S')
    dt = df + relativedelta(days=2)
    for resp in app_layer.read_incremental_values(df, dt, register=curve_option):
        values.append(resp.content.valores)
        logging.info("Receiving data...")
    result = parse_profiles(values, meter_dates['name'],
                            df.strftime('%Y-%m-%d %H:%M:%S'),
                            dt.strftime('%Y-%m-%d %H:%M:%S'))
    logging.info(result)
    dt = c.TmProfile.create_requested_profiles(result, date_to, option)
    logging.info("last profile: {}".format(dt))
    if dt:
        c.GiscedataLecturesComptador.write(meter_dates['id'], {field: dt})
        logging.info(dt)


def gather_billings(c, meter_dates, app_layer, contract, date_to):
    values = []
    field = 'tm_last_load_billing'
    df = datetime.strptime(meter_dates[field] + ' 00:00:00', '%Y-%m-%d %H:%M:%S')
    dt = datetime.strptime(date_to, '%Y-%m-%d %H:%M:%S')
    for resp in app_layer.stored_tariff_info(df, dt, register=contract):
        values.extend(resp.content.valores)
        logging.info("Receiving data...")
    result = parse_billings(values, contract, meter_dates['name'],
                            df.strftime('%Y-%m-%d %H:%M:%S'),
                            dt.strftime('%Y-%m-%d %H:%M:%S'))
    logging.info(result)
    dt = c.TmBilling.create_requested_billings([result], meter_dates['id'], date_to)
    c.GiscedataLecturesComptador.write(meter_dates['id'], {field: dt})
    logging.info(dt)


@click.command()
@click.option('-h', '--host',
              help='Host to connect to via ERP Peek',
              type=str, default='http://localhost', show_default=True)
@click.option('-p', '--port',
              help='Port to connect via ERP Peek',
              type=int, default=8069, show_default=True)
@click.option('-d', '--database',
              help='Database to connect to the ERP',
              type=str, default='distri_test_tg', show_default=True)
@click.option('-u', '--user',
              help='User to connect to the ERP',
              type=str, default='root', show_default=True)
@click.option('-w', '--password',
              help='Password to connect to the ERP',
              type=str)
@click.option('-m', '--meter',
              help='TM meter name',
              type=str, default='', show_default=True)
@click.option('-o', '--option',
              help='p for profiles, p4 for quarters, b for billings',
              type=str, default='p', show_default=True)
@click.option('-t', '--to-date',
              help='last date to get data to [YYYY-MM-DD HH:MM:SS]',
              type=str, default=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
              show_default=True)
def connect_host(host, port, database, user, password, meter, option, to_date):
    logging.basicConfig(level=logging.INFO)
    server_url = '{host}:{port}'.format(**locals())
    c = Client(server=server_url, db=database, user=user, password=password)
    import_from_tm(c, meter, option, to_date)

if __name__ == '__main__':
    connect_host()
