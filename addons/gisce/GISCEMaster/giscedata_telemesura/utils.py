# -*- coding: utf-8 -*-
from enerdata.profiles.profile import ProfileHour, Coefficent
from enerdata.datetime.timezone import TIMEZONE
from datetime import datetime
from collections import namedtuple
from time import sleep
import datetime as dtt
from tools.translate import _
import base64
import zipfile
import cStringIO as StringIO
from decimal import Decimal
import calendar


WIZARD_STATES = [
    ('standby', 'Stand by'),
    ('request', 'Request data'),
    ('end', 'Finished request')
]


def get_file_content(wizard):
    zip_filenames = []
    content = {'file_type': ''}
    # If zip, gotta unzip the file
    _data = base64.decodestring(wizard.file)
    file_handler = StringIO.StringIO(_data)
    try:
        zip_file = zipfile.ZipFile(file_handler, "r")
        for name in zip_file.namelist():
            zip_filenames.append(name)
            content[name] = zip_file.read(name)
        content.update({'file_type': 'zip',
                        'name': wizard.f_name})
    except zipfile.BadZipfile as e:
        # L'arxiu no es zip
        content['file'] = base64.b64decode(wizard.file)
        content.update({'file_type': 'raw',
                        'name': wizard.f_name})

    return zip_filenames, content


def localize_season(dt, season):
    if isinstance(dt, basestring):
        dt = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
    dst = bool(season == 'S')
    return TIMEZONE.normalize(TIMEZONE.localize(dt, is_dst=dst))


def get_season(dt):
    if dt.dst().seconds > 0:
        return 'S'
    else:
        return 'W'


def num_to_season(s):
    if s == '1':
        return 'S'
    elif s == '0':
        return 'W'
    else:
        raise ValueError(_("Cannot covert {} to a season").format(s))


def get_hours_amount(date_from, date_to):
    date_from_tz = datetime.strptime(date_from, '%Y-%m-%d %H:%M:%S')
    date_to_tz = datetime.strptime(date_to, '%Y-%m-%d %H:%M:%S')
    delta = date_to_tz.date() - date_from_tz.date()
    hours = delta.days * 24
    season_ini = get_season(TIMEZONE.localize(date_from_tz))
    season_end = get_season(TIMEZONE.localize(date_to_tz))
    if season_ini != season_end:
        if season_ini == 'W':
            hours -= 1
        else:
            hours += 1
    return hours


def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + dtt.timedelta(days=4)
    return next_month - dtt.timedelta(days=next_month.day)


def last_sunday(year, month):
    """
    Get last sunday of month to check hour change
    :param year: int year
    :param month: int month
    :return: int day
    """
    for day in reversed(range(1, calendar.monthrange(year, month)[1] + 1)):
        if calendar.weekday(year, month, day) == calendar.SUNDAY:
            return day


def check_response_header(response):
    res = {
        'type': '',
        'id': '',
        'error': False,
        'error_message': '',
        'message': ''
    }
    id_job = response.get('id', False)
    if id_job:
        res.update({'id': id_job, 'type': 'API'})
    else:
        res.update({'id': id_job, 'type': 'FILE'})
    if response['message']:
        res.update({'message': response.get('message', False)})
    if response['error']:
        res.update({'error': True,
                    'error_message': response.get('error_message',
                                                  'API connection error')})
    return res


def get_content_from_api_response(response, reew, retries):
    res = False
    job_id = response['id']
    status = reew.get_status(job_id)['status']
    counter = 0
    while status.get("state", "unfinished") not in ("finished","failed") \
            and counter < retries:
        sleep(5)
        status = reew.get_status(job_id)['status']
        counter += 1
    final_state = status.get("state", "unfinished")
    if final_state == "finished":
        res = status.get('result', False)
        if not res.get('error', True):
            res = res['message']
    return res


def convert_to_mongodb(measure):
    assert isinstance(measure, (ProfileHour, TMProfileHour))
    return dict(
        timestamp=TIMEZONE.normalize(measure.date).strftime('%Y-%m-%d %H:00:00'),
        ai_fact=measure.measure,
        valid=True,
        valid_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        cch_fact=True,
        magn=1000,
        season=get_season(measure.date)
    )


def convert_to_profilehour(measure):
    ph = TMProfileHour(
        localize_season(measure['timestamp'], measure['season']),
        measure['ai'],
        measure['valid'],
        Decimal(0),
        meta=measure
    )
    return ph


class TMProfileHour(namedtuple('TMProfileHour', ProfileHour._fields + (
        'meta', ))):
    __slots__ = ()

    def __lt__(self, other):
        return self.date < other.date

    def __le__(self, other):
        return self.date <= other.date

    def __gt__(self, other):
        return self.date > other.date

    def __ge__(self, other):
        return self.date >= other.date
