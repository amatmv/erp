from giscedata_lectures_tecnologia.giscedata_lectures import TECHNOLOGY_TYPE_SEL
from osv import osv, fields
import pooler
from datetime import datetime, timedelta
from oorq.decorators import job, create_jobs_group
import pandas as pd
from mongodb_backend.mongodb2 import mdbpool
import types
from tools.translate import _
from tools import config
from tools.misc import cache
import logging
from dateutil.relativedelta import relativedelta
from ast import literal_eval
from tqdm import tqdm


if 'telemeasure' not in dict(TECHNOLOGY_TYPE_SEL).keys():
    TECHNOLOGY_TYPE_SEL += [('telemeasure', 'Telemeasure')]

if 'electronic' not in dict(TECHNOLOGY_TYPE_SEL).keys():
    TECHNOLOGY_TYPE_SEL += [('electronic', 'Electronic')]

CONTRACT_IMPORT_SELECTION = [(1, 'Contract 1'),
                             (2, 'Contract 2'),
                             (3, 'Contract 3')]


def update_invalid(dst, src):
    """ Append all the values of dst with the values of src
    " using a \n as separator. Create dst key if.
    """
    for key, value in src.iteritems():
        if key in dst:
            dst[key] += "\n %s" % value
        else:
            dst[key] = value
    return dst


def get_format_date(date, hour=False, default_format=True):
    if isinstance(date, types.StringTypes):
        if len(date) > 10:
            date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        else:
            date = datetime.strptime(date, '%Y-%m-%d')

    date_format = '%Y-%m-%d'
    if not default_format:
        date_format = '%d/%m/%Y'
    if hour:
        date_format += ' %H'

    return date.strftime(date_format)


class TmLecturesComptador(osv.osv):

    _name = 'giscedata.lectures.comptador'
    _inherit = 'giscedata.lectures.comptador'

    # @cache(timeout=60)
    def check_case_open(self, cursor, uid, meter_id, section=False,
                        category=False, context=None):
        '''checks if there is another case in open or pending state'''

        case_obj = self.pool.get('crm.case')
        section_id = False
        categ_id = False
        # search for section
        if section:
            section_obj = self.pool.get('crm.case.section')
            search_params = [('code', '=', section)]
            section_ids = section_obj.search(cursor, uid, search_params)
            section_id = section_ids and section_ids[0] or False
        # Search for category only if section was found
        if section_id and category:
            categ_obj = self.pool.get('crm.case.categ')
            search_params = [('categ_code', '=', category),
                             ('section_id', '=', section_id)]
            categ_ids = categ_obj.search(cursor, uid, search_params)
            categ_id = categ_ids and categ_ids[0] or False

        # Search for cases
        ref2 = 'giscedata.lectures.comptador,%s' % meter_id
        search_params = [('ref2', '=', ref2),
                         ('state', 'in', ('open', 'pending'))]
        if section_id:
            search_params.append(('section_id', '=', section_id))
        if categ_id:
            search_params.append(('categ_id', '=', categ_id))
        case_ids = case_obj.search(cursor, uid, search_params)
        return case_ids and True or False

    def create_read_from_tm(self, cursor, uid, ids, context=None):
        '''Create reads from validated tm billing info'''
        if not context:
            context = {}
        db = pooler.get_db_only(cursor.dbname)
        global_date_limit = context.get('date_limit', '3000-01-01')
        force_tg_read = context.get('force_tg_read', False)
        billing_obj = self.pool.get('tm.billing')
        origin_obj = self.pool.get('giscedata.lectures.origen')
        polissa_obj = self.pool.get('giscedata.polissa')
        config = self.pool.get('res.config')
        case_obj = self.pool.get('crm.case')
        global_reactive_insert = 'always'
        logger = logging.getLogger('openerp.{0}.request_tm_profiles'.format(
            __name__))
        res = {}
        for meter in self.browse(cursor, uid, ids, context=context):
            try:
                logger.info("Creating reads from TM for {} meter".format(
                    meter.name))
                date_limit = global_date_limit
                tmp_cr = db.cursor()
                res[meter.id] = {'date_read': False,
                                 'existing': False}
                #If meter is not active, billing info cannot be
                #greater than meter data_baixa
                if meter.data_baixa:
                    date_limit = min(meter.data_baixa,
                                     date_limit)
                #Billing date is one day ahead so
                #date_limit must be + 1 day
                date_limit_plus1 = datetime.\
                                    strftime(datetime.strptime(date_limit,
                                                               '%Y-%m-%d')
                                             + timedelta(days=1), '%Y-%m-%d')
                #Build full name from serial if present
                meter_name = meter.name
                contract = meter.tm_contract_import
                billing_dates = billing_obj.get_last_billing_date(tmp_cr, uid,
                                                              meter_name,
                                                              date_limit_plus1,
                                                              contract,
                                                              context=context)
                if not billing_dates['month'] and not billing_dates['day']:
                    #No valid billing info before date_limit
                    #Cannot insert any read
                    continue
                elif billing_dates['month'] and not billing_dates['day']:
                    type = 'month'
                    date = billing_dates['month']
                elif not billing_dates['month'] and billing_dates['day']:
                    type = 'day'
                    date = billing_dates['day']
                elif billing_dates['month'] >= billing_dates['day']:
                    type = 'month'
                    date = billing_dates['month']
                else:
                    type = 'day'
                    date = billing_dates['day']
                #Check for polissa values in date
                polissa_id = meter.polissa.id
                polissa = polissa_obj.browse(tmp_cr, uid, polissa_id,
                                             context={'date': date})

                # If polissa is integrated with CCH we can only go 3 days
                # ago
                days_back = int(
                    config.get(cursor, uid, 'tm_max_days_back', 3)
                )

                max_date_ago_tm = (
                    datetime.strptime(date_limit, '%Y-%m-%d') -
                    timedelta(days=days_back)
                ).strftime('%Y-%m-%d')
                if days_back and date < max_date_ago_tm:
                    continue

                num_periods = (polissa.tarifa.
                               get_num_periodes(context={'agrupar_periodes':
                                                          False}))
                billing_data = [
                    {'tariff_id': polissa.tarifa.id,
                     'data': billing_obj.get_billing(tmp_cr, uid,
                                                     meter_name, date,
                                                     type, num_periods,
                                                     contract,
                                                     context=context)
                     }
                ]
                if not billing_data:
                    continue
                #TODO: Insert billing data as reads
                #Telelectura origin is the one with codi 10
                origin_id = origin_obj.search(tmp_cr, uid,
                                              [('codi', '=', '10')])[0]
                meter_id = meter.id
                #only get date part from datetime response
                #minus 1 day for invoice date
                date_read_plus1 = billing_data[0]['data'][0]['date_end'][:10]
                date_read = datetime.\
                                strftime(datetime.strptime(date_read_plus1,
                                                           '%Y-%m-%d')
                                         - timedelta(days=1), '%Y-%m-%d')
                #Assign values from polissa that will be used later
                tarifa_id = polissa.tarifa.id
                potencia = polissa.potencia

                #Check if polissa has a tarifa or pot change between
                #demanded read date and real read date to be inserted
                tariff_change = False
                if date[:10] != date_read:
                    try:
                        polissa_date_read = polissa_obj.browse(tmp_cr, uid,
                                                               polissa_id,
                                                               context={'date':
                                                                    date_read})
                    except Exception as e:
                        date_after = datetime.strptime(
                            date_read, '%Y-%m-%d') + relativedelta(days=1)
                        polissa_date_read = polissa_obj.browse(tmp_cr, uid,
                                                               polissa_id,
                                                               context={'date':
                                                               date_after})
                    if (polissa.tarifa.id != polissa_date_read.tarifa.id):
                        #If tarifa has changed, recompute num_periods and
                        #get again billing_data because original is not correct
                        tariff_change = True
                        tarifa_id = polissa_date_read.tarifa.id
                        num_periods = (polissa_date_read.tarifa.
                           get_num_periodes(context={'agrupar_periodes':
                                                     False}))

                        new_billing_data = [
                            {'tariff_id': polissa_date_read.tarifa.id,
                             'data': billing_obj.get_billing(tmp_cr, uid,
                                                             meter_name,
                                                             date_read_plus1,
                                                             type, num_periods,
                                                             context=context)
                             }
                        ]

                        billing_data.extend(new_billing_data)

                    if potencia != polissa_date_read.potencia:
                        potencia = polissa_date_read.potencia

                res[meter_id].update({'date_read': date_read})
                #If date_read before last meter read do not insert
                #Do not check if we want to force creation
                if not force_tg_read:
                    last_read = meter.data_ultima_lectura(tarifa_id)
                    if last_read >= date_read:
                        res[meter_id].update({'existing': True})
                        continue
                #If yet another read present, skip meter
                if self.existing_read(tmp_cr, uid, meter_id,
                                      date_read, tarifa_id,
                                      context=context):
                    res[meter_id].update({'existing': True})
                    continue
                # Unload meter from TPL when TM read is inserted
                if self.has_in_tpl(tmp_cr, uid):
                    meter_obj = self.pool.get('giscedata.lectures.comptador')
                    in_tpl = meter_obj.read(tmp_cr, uid, meter_id, ['in_tpl'])
                    if in_tpl['in_tpl']:
                        vals = {'in_tpl': False}
                        meter_obj.write(tmp_cr, uid, [meter_id], vals,
                                        context={'sync': False})
                # common values for all reads
                common_vals = {'comptador': meter_id,
                               'name': date_read,
                               'origen_id': origin_id}

                new_billing_data = list(billing_data)
                pot_periods = 0
                energy_periods = 0
                for tariff in new_billing_data:
                    tariff['periods'] = self.get_periods(tmp_cr, uid,
                                                         tariff['tariff_id'],
                                                         context=context)
                    pot_periods += len(tariff['periods']['tp'].keys())
                    energy_periods += len(tariff['periods']['te'].keys())
                # If only one pot period and more than one energy period
                # max is the max for all periods
                all_periods_max = 0
                if pot_periods == 2 and energy_periods > 1:
                    all_periods_max = -1
                    for tariff in new_billing_data:
                        all_periods_max = \
                            max([all_periods_max] + [x['max'] for x in
                                                     tariff['data']])
                reactive_insert = global_reactive_insert

                for tariff in new_billing_data:
                    for billing in tariff['data']:
                        self.create_energy_readings_tm(
                            tmp_cr, uid, billing, common_vals, tariff['periods']
                            , potencia, reactive_insert)
                        self.create_power_readings_tm(
                            tmp_cr, uid, billing, common_vals, tariff['periods']
                            , all_periods_max)
                tmp_cr.commit()
            except Exception:
                #TODO. Create case with error entering reads
                # Do not open another case if already one in open or pending state
                # This way we let automatic checks to be closed manually when done
                # and do not create many cases with the same incidences
                if self.check_case_open(cursor, uid, meter.meter_id, section='TM', context=context):
                    continue

                # Create case for this meter
                extra_vals = {'description': _(u'Error with entering reads in TM meter {}.').format(meter.name),
                              'ref2': 'giscedata.lectures.comptador,%i'
                                      % last_read['id']}
                description = _(u"Error with entering reads in meter %s") % last_read['name']
                case_id = case_obj.create_case_generic(cursor, uid,
                                                       [last_read['polissa'][0]],
                                                       context=context,
                                                       description=description,
                                                       section='TM',
                                                       extra_vals=extra_vals)
                # Open the case
                case_obj.case_open(cursor, uid, case_id)
                tmp_cr.rollback()
            finally:
                tmp_cr.close()

        return res

    def create_energy_readings_tm(self, cursor, uid, billing, common_vals,
                               tarifa_periods, potencia, reactive_insert):
        lect_obj = self.pool.get('giscedata.lectures.lectura')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        config = self.pool.get('res.config')
        meter_id = common_vals.get('comptador', [])
        tariff = meter_obj.q(cursor, uid).read([
            'polissa.tarifa.name'
        ]).where([('id', '=', meter_id)])

        if tariff and tariff[0]['polissa.tarifa.name'].startswith('RE'):
            period_name = 'P0'
        elif billing['period'] == 0:
            period_name = 'P1'
        else:
            period_name = 'P%s' % billing['period']
        vals = common_vals.copy()
        # Insert active values
        vals.update({'tipus': 'A',
                     'periode': tarifa_periods['te'][period_name],
                     'lectura': billing['ai'],
                     })
        lect_obj.create(cursor, uid, vals)
        # Insert reactive values. Mandatory if potencia > 15
        vals = common_vals.copy()
        if potencia > 15:
            reactive_insert = 'always'
        if reactive_insert != 'never':
            tg_react_quads = config.get(
                cursor,
                uid,
                'tg_react_quads',
                '[1]'
            )
            tg_react_quads = literal_eval(tg_react_quads)
            tg_react = 0
            for tg_quad in tg_react_quads:
                tg_react += billing['r%s' % tg_quad]
            insert_option = {
                'always': tg_react,
                'zero': 0
            }
            reactive_read = insert_option[reactive_insert]
            vals.update({'tipus': 'R',
                         'periode': tarifa_periods['te'][period_name],
                         'lectura': reactive_read,
                         })
            lect_obj.create(cursor, uid, vals)

    def create_power_readings_tm(self, cursor, uid, billing, common_vals,
                              tarifa_periods, all_periods_max):
        lect_pot_obj = self.pool.get('giscedata.lectures.potencia')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        meter_id = common_vals.get('comptador', [])
        tariff = meter_obj.q(cursor, uid).read([
            'polissa.tarifa.name'
        ]).where([('id', '=', meter_id)])

        if tariff and tariff[0]['polissa.tarifa.name'].startswith('RE'):
            period_name = 'P0'
        elif billing['period'] == 0:
            period_name = 'P1'
        else:
            period_name = 'P%s' % billing['period']

        # Insert max values. Max in billing comes in kwatts
        if tarifa_periods['tp'].get(period_name, False):
            vals = common_vals.copy()
            max_value = all_periods_max or billing['max']
            vals.update({'periode': tarifa_periods['tp'][period_name],
                         'lectura': float(max_value),
                         'exces': billing['excess'],
                         })
            lect_pot_obj.create(cursor, uid, vals)

    def _check_selected_contracts(self, cr, uid, ids):
        meter_data = self.read(
            cr, uid, ids, ['tm_contract_import', 'registrador_id', 'technology_type']
        )
        if meter_data[0]['technology_type'] == 'telemeasure':
            if not meter_data[0]['tm_contract_import']:
                return True
            elif meter_data[0]['registrador_id']:
                reg_obj = self.pool.get('giscedata.registrador')
                reg_data = reg_obj.read(
                    cr, uid, meter_data[0]['registrador_id'][0],
                    ['tm_contract_1', 'tm_contract_2', 'tm_contract_3']
                )
                imp_contract = meter_data[0]['tm_contract_import']
                if reg_data['tm_contract_{}'.format(imp_contract)]:
                    return True
            return False
        else:
            return True

    _columns = {
        'tm_last_read': fields.date('Last validated billing'),
        'tm_last_profile': fields.datetime('Last validated profile'),
        'tm_last_quarter_profile': fields.datetime('Last validated quarter '
                                                   'profile'),
        'tm_last_load_billing': fields.date('Last loaded billing'),
        'tm_last_load_profile': fields.datetime('Last loaded profile'),
        'tm_last_load_quarter_profile': fields.datetime('Last loaded quarter '
                                                        'profile'),
        'tm_contract_import': fields.selection(CONTRACT_IMPORT_SELECTION,
                                               'Importing'),
    }

    _constraints = [(_check_selected_contracts,
                     'This contract is not selected to be requested. First '
                     'select the checkbox in the registrator to be able to '
                     'import the contract.',
                     ['tm_contract_import']), ]

TmLecturesComptador()


def get_all_meter_names(model):
    tm_model_mdb = mdbpool.get_collection(model)
    return tm_model_mdb.distinct('name')


def has_pending_validation(meter_name, model):
    '''check if we have at least one invalid
    read associated to meter_name'''

    tm_model_mdb = mdbpool.get_collection(model)
    profile = tm_model_mdb.find_one({'name': meter_name,
                                     'valid': False})
    return profile and True or False


class TmLecturesValidate(osv.osv):

    _name = 'tm.lectures.validate'
    _auto = False

    def get_previous_read(self, cursor, uid, vals, context=None):
        '''return values of the previous valid read in vals'''
        tm_billing_obj = self.pool.get('tm.billing')
        search_params = [('name', '=', vals['name']),
                         ('type', '=', vals['type']),
                         ('value', '=', vals['value']),
                         ('period', '=', vals['period']),
                         ('date_begin', '<', vals['date_begin']),
                         ('valid', '=', True),
                         ('contract', '=', vals['contract'])]
        read_id = tm_billing_obj.search(cursor, uid, search_params,
                                        limit=1, order='date_begin desc')
        if read_id:
            return tm_billing_obj.read(cursor, uid, read_id)[0]
        return {}

    def get_last_valid_read(self, cursor, uid, meter_name, full_read=False,
                            context=None):

        tm_billing_obj = self.pool.get('tm.billing')

        search_params = [('name', '=', meter_name),
                         ('valid', '=', True)]
        tm_billing_ids = tm_billing_obj.search(cursor, uid, search_params,
                                               limit=1, order='date_end desc')
        res = None
        if tm_billing_ids:
            tm_billing = tm_billing_obj.read(cursor, uid, tm_billing_ids)[0]
            if full_read:
                res = tm_billing
            else:
                res = get_format_date(tm_billing['date_end'])

        return res

    @job(queue='tg_validate', timeout=600, result_ttl=24 * 3600)
    def meters_without_read(self, cursor, uid, context=None):
        '''Checks if a meter do not have received
        a read for more than X days or never'''

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        cfg_obj = self.pool.get('res.config')
        case_obj = self.pool.get('crm.case')
        if not context:
            context = {}
        context.update({'model': 'giscedata.polissa',
                        'active_test': True})
        # Search for meters to check
        days = int(cfg_obj.get(cursor, uid, 'tg_last_read_advice', '2'))
        query = open(
            '%s/giscedata_telemesura/sql/check_without_read.sql' % config[
                'addons_path']).read()
        cursor.execute(query, (days,))

        for meter_id, days_last_read in cursor.fetchall():
            message = ''
            last_read = meter_obj.read(cursor, uid, meter_id,
                                       ['tg_last_read', 'name', 'polissa'],
                                       context=context)
            if days_last_read == 99999:
                message += (_(u"TG meter %s without any read")
                            % last_read['name'])
            else:
                read_date = datetime.strptime(last_read['tg_last_read'],
                                              '%Y-%m-%d').strftime('%d/%m/%Y')
                message += _(u"TG meter %s last read was "
                             u"more than %s days ago. %s")
                message %= (last_read['name'], days, read_date)

            if not message:
                continue
            # Create case for this meter
            extra_vals = {'description': message,
                          'ref2': 'giscedata.lectures.comptador,%i'
                                  % last_read['id']}
            description = _(u"Unreaded meter %s") % last_read['name']
            case_id = case_obj.create_case_generic(cursor, uid,
                                                   [last_read['polissa'][0]],
                                                   context=context,
                                                   description=description,
                                                   section='TM',
                                                   extra_vals=extra_vals)
            # Open the case
            case_obj.case_open(cursor, uid, case_id)

        return True

    def create_case_invalid(self, cursor, uid, invalids, context=None):
        '''Create case with invalid measures'''
        case_obj = self.pool.get('crm.case')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        billing_obj = self.pool.get('tm.billing')
        billing_mdb = mdbpool.get_collection('tm_billing')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': 'giscedata.polissa'})

        #Order invalids by timestamp first
        search_params = [('id', 'in', invalids.keys())]
        ordered_ids = billing_obj.search(cursor, uid, search_params,
                                         order='date_end')
        meters = {}
        # Search meter_id for each invalid and group then
        for billing_id in ordered_ids:
            tg_billing = billing_mdb.find_one({'id': billing_id},
                                              {'date_end': 1, 'name': 1})
            date_meter = get_format_date(tg_billing['date_end'])
            meter_id = meter_obj.get_meter_on_date(cursor, uid, tg_billing['name'],
                                                   date_meter, context=context)
            meters.setdefault(meter_id, [])
            if invalids[billing_id] != '':
                meters[meter_id].append(invalids[billing_id])

        for meter_id in meters:
            if not meter_id:
                continue
            #Do not open another case if already one in open or pending state
            #This way we let automatic checks to be closed manually when done
            #and do not create many cases with the same incidences
            if meter_obj.check_case_open(cursor, uid, meter_id, section='TM',
                                         category='TMBI', context=context):
                continue
            meter = meter_obj.browse(cursor, uid, meter_id)
            case_message = '\n'.join(meters[meter_id])
            case_vals = {
                'description': case_message,
                'ref2': 'giscedata.lectures.comptador,%i' % meter.id,
                'name': (_('TM Billing checks for %s') % meter.name),
                'polissa_id': meter.polissa.id,
            }
            case_id = case_obj.create_case_generic(cursor, uid,
                                                   [meter.polissa.id],
                                                   context=ctx,
                                                   section='TM',
                                                   category='TMBI',
                                                   extra_vals=case_vals)
            case_obj.case_open(cursor, uid, case_id)
        return True

    def create_case_notfound(self, cursor, uid, notfound, meter_name,
                             context=None):
        '''Create case with notfound measures'''
        case_obj = self.pool.get('crm.case')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': False})

        inserted_dates = []
        msg = ''
        only_days = [get_format_date(date) for date in notfound.itervalues()]
        #If we have no days to show, do not create any case
        if not only_days:
            return True
        sorted_days = sorted(list(set(only_days)))
        _not_found_msg = _('Meter %s not found on %s\n')
        for timestamp in sorted_days:
            date = get_format_date(timestamp, default_format=False)
            msg += _not_found_msg % (meter_name, date)

        case_name = _('Meter {} not found validating TM Billings').format(
            meter_name)

        return case_obj.append_description_to_case_by_name(
            cursor,
            uid,
            None,
            case_name,
            msg,
            'TM',
            'TMBI',
            context=context
        )

    def update_last_valid_read(self, cursor, uid, meter_names, context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if not context:
            context = {}
        context.update({'sync': False})

        if not isinstance(meter_names, (list, tuple)):
            meter_names = [meter_names]

        for meter_name in meter_names:
            last_valid = self.get_last_valid_read(cursor, uid, meter_name,
                                                  full_read=True,
                                                  context=context)
            if last_valid is None:
                continue

            last_valid_datetime = datetime.strptime(
                last_valid['date_end'],
                '%Y-%m-%d %H:%M:%S'
            )
            if last_valid_datetime.hour == 0:
                last_valid_datetime -= timedelta(days=1)

            date_last_valid_read = last_valid_datetime.strftime('%Y-%m-%d')

            meter_id = meter_obj.get_meter_on_date(
                cursor,
                uid,
                meter_name,
                date_last_valid_read,
                context=context
            )
            if not meter_id:
                continue

            vals = {'tm_last_read': last_valid['date_end'][:10],
                    'technology_type': 'telemeasure'}

            meter_obj.write(cursor, uid, [meter_id],
                            vals, context=context)
        return {'badids': {}, 'notfound': {}}

    def total_equal_sum(self, cursor, uid, meter_name, context=None):
        '''checks if the period 0 is equal to the sum
        of all other periods with a margin of 1'''

        tm_billing_obj = self.pool.get('tm.billing')
        meter_obj = self.pool.get('giscedata.lectures.comptador')

        if not context:
            context = {}

        result = {'badids': {}, 'notfound': {}}

        last_valid_read = self.get_last_valid_read(cursor, uid, meter_name,
                                                   context=context)

        meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                               last_valid_read, context=context)

        if not meter_id:
            return result

        # Exits if meter marked as not to check totalizer (TT)
        # if 'BT' in meter_obj.get_exceptions(cursor, uid, meter_id):
        #     return result

        # meter = meter_obj.browse(cursor, uid, meter_id)
        search_params = [('name', '=', meter_name),
                         ('date_end', '<=', last_valid_read),
                         ('value', '=', 'a'),
                         ('valid', '=', True)]

        # if meter.tg_last_read:
        #     search_params.extend([('date_end', '>', meter.tg_last_read)])

        read_ids = tm_billing_obj.search(cursor, uid, search_params)
        reads = tm_billing_obj.read(cursor, uid, read_ids)
        read_vals = {}
        # Compare sum and total by date and type
        for read in reads:
            date = read['date_end'][:10]
            type = read['type']
            read_vals.setdefault(date, {})
            read_vals[date].setdefault(type, {})
            read_vals[date][type].setdefault('total', 0)
            read_vals[date][type].setdefault('suma', 0)
            read_vals[date][type].setdefault('periodos', 0)
            read_vals[date][type].setdefault('id', 0)
            if read['period'] == 0:
                read_vals[date][type]['total'] += read['ai']
            else:
                read_vals[date][type]['suma'] += read['ai']
                read_vals[date][type]['periodos'] += 1
            if not read_vals[date][type]['id']:
                read_vals[date][type]['id'] = read['id']
        message = ''
        for date in read_vals:
            for type, value in read_vals[date].iteritems():
                # The difference can be as much as the number of periods
                # That is because each period is an isolated register
                # so rounding problems when adding all periods multiplies
                # by the number of periods
                if abs(value['total'] - value['suma']) > value['periodos']:
                    date_format = get_format_date(date, default_format=False)
                    message += _(u"Total not equal Date to: %s, Total %s, Sum "
                                 u"%s\n")
                    message %= (date_format, value['total'], value['suma'])
                    read_id = value['id']
                    result['badids'][value['id']] = message
        return result

    def impossible_read(self, cursor, uid, vals, polissa, context=None):
        """
        Checks if a consumer consumption is too high. Also checks for turn
        back reads
        """
        message = ''
        res = {}

        date_format = get_format_date(vals['date_begin'], default_format=False)
        if vals['value'] == 'a':
            # absolute read
            previous_vals = self.get_previous_read(cursor, uid, vals, context)
            if not previous_vals:
                return res
            # Max watts consumption = power * 24 hours a day
            # checking only in daily billing
            time_between_reads = (datetime.strptime(vals['date_end'],
                                                    '%Y-%m-%d %H:%M:%S') -
                                  datetime.strptime(previous_vals['date_end'],
                                                    '%Y-%m-%d %H:%M:%S'))
            # In many cases initial read it is not from 00 hour
            # so check max_kw in real hours
            hours_between_reads = ((time_between_reads.days * 24) +
                                   (time_between_reads.seconds / 3600))
            max_kw = polissa.potencia * hours_between_reads

            # If OL not in exceptions, we have to check consumption
            if vals['ai'] - previous_vals['ai'] >= max_kw:
                message += _(u"Impossible read: "
                             u"Consumption over limit\n")
                message += _(u"Date: %s, Period: %s, AI: %s")
                message %= (date_format, vals['period'], vals['ai'])
            if vals['ai'] - previous_vals['ai'] < 0:
                message += _(u"Turn back read:\n")
                message += _(u"Date: %s, Period: %s, AI: %s")
                message %= (date_format, vals['period'], vals['ai'])
        else:
            # Incremental read
            # Max watts consumption = power * 24 hours a day
            time_between_reads = ((datetime.strptime(vals['date_end'],
                                                     '%Y-%m-%d %H:%M:%S') -
                                   datetime.strptime(vals['date_begin'],
                                                     '%Y-%m-%d %H:%M:%S')))
            # In many cases initial read it is not from 00 hour
            # so check max_kw in real hours
            hours_between_reads = ((time_between_reads.days * 24) +
                                   (time_between_reads.seconds / 3600))
            if hours_between_reads == 0:
                return message
            max_kw = polissa.potencia * hours_between_reads
            # If OL not in exceptions, we have to check consumption
            if vals['ai'] >= max_kw:
                message += _(u"Impossible read: "
                             u"Consumption over limit\n")
                message += _(u"Date: %s, Period: %s, AI: %s")
                message %= (date_format, vals['period'], vals['ai'])
        if message:
            res[vals['id']] = message
        return res

    def check_same_time_many_measures(self, cursor, uid, meter_name,
                                      context=None):
        """ Look for measures that has same meter, type, value,
        " period and date_begin but different ai reads
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_billing = mdbpool.get_collection('tm_billing')

        result = {'badids': {}, 'notfound': {}}

        dups = tm_billing.aggregate([
            {'$match': {'name': meter_name}},
            {'$group': {'_id': {'name': '$name',
                                'type': '$type',
                                'value': '$value',
                                'period': '$period',
                                'date_begin': '$date_begin',
                                'contract': '$contract',
                                },
                        'reads': {'$push': {'ai': '$ai',
                                            'id': '$id',
                                            'valid': '$valid'}},
                        'count': {'$sum': 1}
                        }
             },
            {'$match': {'count': {'$gt': 1}}},
        ])['result']

        for dup in dups:
            date_begin = dup['_id']['date_begin']
            date_meter = get_format_date(date_begin)
            meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                                   date_meter, context=context)
            if not meter_id:
                for read in dup['reads']:
                    id = read['id']
                    result['notfound'][id] = date_begin
                continue
            date_msg = get_format_date(date_begin, default_format=False)
            # Look for a valid read among reads found
            valid_read = None
            to_delete = []
            for read in dup['reads']:
                if read['valid']:
                    valid_read = read
                    break
            # If we have a valid read, we can delete all
            # the other ones with a difference less than 1 in ai
            if valid_read is not None:
                for read in dup['reads']:
                    if (read['id'] != valid_read['id']
                        and abs(read['ai'] - valid_read['ai']) <= 1):
                        to_delete.append(read['id'])
            check_reads = [read for read in dup['reads']
                           if read['id'] not in to_delete]
            check_read_ids = [read['id'] for read in check_reads]
            # Set the error message only in the first duplied read, but
            # all of them should be in the invalid list
            measure_list = ', '.join([str(read['ai'])
                                     for read in check_reads])
            for read_id in check_read_ids:
                msg = ''
                if check_read_ids.index(read_id) == 0:
                    msg = _(u"%s different reads found "
                            u"on %s for period %s. AI: %s")
                    msg %= (len(check_read_ids), date_msg,
                            dup['_id']['period'], measure_list)
                result['badids'][read_id] = msg
            if to_delete:
                tm_billing.remove({'id': {'$in': to_delete}})
        return result

    def check_negative_read(self, cursor, uid, meter_name, context=None):
        """ Check if the consumptions has negative values """

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_billing = mdbpool.get_collection('tm_billing')

        result = {'badids': {}, 'notfound': {}}
        negatives = tm_billing.find(
            {'name': meter_name, 'valid': False,
             '$or': [{'ai': {'$lt': 0}},
                     {'ae': {'$lt': 0}},
                     {'r1': {'$lt': 0}},
                     {'r2': {'$lt': 0}},
                     {'r3': {'$lt': 0}},
                     {'r4': {'$lt': 0}},
                     ],
             },
            {'_id': 0, 'ai': 1, 'ae': 1, 'period': 1,
             'r1': 1, 'r2': 1, 'r3': 1, 'r4': 1,
             'id': 1, 'date_begin': 1}
        )

        for negative in negatives:
            date_meter = get_format_date(negative['date_begin'])
            meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                                   date_meter, context=context)
            if not meter_id:
                result['notfound'][negative['id']] = negative['date_begin']
                continue
            # Format date for printing
            date = get_format_date(negative['date_begin'], default_format=False)
            # Extract negative values for message
            msg_values = ['%s: %s, ' % (i.upper(), negative[i])
                          for i in ('ai', 'ae', 'r1', 'r2', 'r3', 'r4')
                          if negative[i] < 0]
            message = (_(u"Negative read "
                         u"Date %s, period %s, %s\n"))
            message %= (date, negative['period'],
                        ', '.join(msg_values))
            result['badids'][negative['id']] = message
        return result

    def get_duplicates(self, cursor, uid, meter_name):
        """
        Get all duplicated billings of the meter with name = meter_name
        :param cursor:
        :param uid:
        :param meter_name: The name of the target meter
        :return: List of dictionaries with all fields on return_keys for all
        the duplicated ids
        """

        tm_billing_obj = self.pool.get('tm.billing')

        read_keys = ['name', 'type', 'value', 'period', 'date_begin', 'ai',
                     'contract', 'date_end', 'valid']
        keywords = ['name', 'type', 'value', 'period', 'date_begin', 'ai',
                    'contract']
        return_keys = ['id', 'date_begin', 'date_end', 'valid']
        billings = tm_billing_obj.search(cursor, uid, [
            ('name', '=', meter_name)])
        read_billings = tm_billing_obj.read(cursor, uid, billings, read_keys)

        df = pd.DataFrame(read_billings)
        df.sort_values(['name', 'type', 'value', 'period', 'date_begin', 'ai',
                        'contract', 'valid'], ascending=False, inplace=True)
        df_duplicated = df.duplicated(subset=keywords)

        return df[df_duplicated.values][return_keys].T.to_dict().values()

    def clean_duplicates(self, cursor, uid, meter_name):
        """ From one meter_name check all the measures deleting duplicates
        " if there is some duplicate that has been validated is the one that
        " remains
        """
        # tm_billing_obj = self.pool.get('tm.billing')
        tm_billing_obj = mdbpool.get_collection('tm_billing')
        meter_dups = self.get_duplicates(cursor, uid, meter_name)

        for dup in meter_dups:
            # Look for all the duplicated measures matching criteria

            tm_billing_obj.remove({'id': dup['id']})

    _block_invalid_check_funcs = ['check_negative_read',
                                  'check_same_time_many_measures',
                                  ]

    _single_check_funcs = [('impossible_read', True)]

    _post_funcs = ['total_equal_sum',
                   'update_last_valid_read',
                   ]

    @job(queue='tm_validate', timeout=600, result_ttl=24 * 3600)
    def validate_individual(self, cursor, uid, ids, meter_name, context=None):
        '''This function has to validate the billing
        information that comes from smart meters
        '''
        tm_billing_obj = self.pool.get('tm.billing')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        logger = logging.getLogger('openerp.{0}.validate_tm_billings'.format(
            __name__))
        if not context:
            context = {}

        self.clean_duplicates(cursor, uid, meter_name)

        search_params = [('name', '=', meter_name), ('valid', '=', False)]

        tm_billing_ids = tm_billing_obj.search(cursor, uid, search_params,
                                               order='date_end')
        if not tm_billing_ids:
            return True
        logger.info("Validation of billings for the {} meter".format(
            meter_name))
        invalids = {}
        notfound = {}
        # Block checks
        # ['check_negative_read', 'check_same_time_many_measures']
        for func_name in self._block_invalid_check_funcs:
            func = getattr(self, func_name)
            res = func(cursor, uid, meter_name, context=context)
            invalids = update_invalid(invalids, res['badids'])
            notfound.update(res['notfound'])

        invalid_ids = list(set(invalids.keys() + notfound.keys()))
        to_be_validated = list(set(tm_billing_ids) - set(invalid_ids))

        for tm_billing_id in tm_billing_ids:
            tm_billing = tm_billing_obj.read(cursor, uid, [tm_billing_id],
                                             [])[0]

            # get meter active in date_end
            date_meter = tm_billing['date_end']
            meter_id = meter_obj.get_meter_on_date(cursor, uid, tm_billing['name'],
                                                   date_meter, context=context)
            if not meter_id:
                notfound[tm_billing_id] = date_meter
                continue
            meter_vals = meter_obj.read(cursor, uid, meter_id, ['polissa'],
                                        context)
            polissa_id = meter_vals['polissa'][0]
            try:
                polissa = polissa_obj.browse(cursor, uid, polissa_id,
                                             context={'date': date_meter})
            except Exception as e:
                if context.get('validate', False):
                    day_before_meter = (
                        datetime.strptime(date_meter, '%Y-%m-%d %H:%M:%S') +
                        relativedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S')
                    polissa = polissa_obj.browse(cursor, uid, polissa_id,
                                                 context={'date':
                                                          day_before_meter})
                else:
                    raise
            num_periodes = (polissa.tarifa.get_num_periodes(
                context={'agrupar_periodes': False}))

            # For one period fares, we will store
            # period 0, 1 and 2 for checking
            if num_periodes == 1:
                num_periodes = 2
            # Remove not necessary periods
            if tm_billing['period'] > num_periodes:
                tm_billing_obj.unlink(cursor, uid, tm_billing['id'])
                continue
            # Store meter_id in tm_billing for later processing
            tm_billing_obj.write(cursor, uid, [tm_billing_id],
                                 {'meter_id': meter_id})
            invalid = False
            # Single checks
            # ['impossible_read']
            for func_name, mark_invalid in self._single_check_funcs:
                func = getattr(self, func_name)
                res = func(cursor, uid, tm_billing, polissa, context=context)
                if res:
                    invalids = update_invalid(invalids, res)
                    # Mark as invalid only if not invalid before
                    if mark_invalid and not invalid:
                        invalid = mark_invalid

            # Exclude invalid by block test validation (to_be_validated)
            if not invalid and tm_billing['id'] in to_be_validated:
                # If we arrive here the measure is valid,
                # no inserts during the process
                tm_billing_obj.validate(cursor, uid, [tm_billing['id']],
                                        action='validate', context=context)
        # Post functions
        # ['total_equal_sum', 'update_last_valid_read']
        for func_name in self._post_funcs:
            func = getattr(self, func_name)
            res = func(cursor, uid, meter_name, context=context)
            invalids = update_invalid(invalids, res['badids'])
            notfound.update(res['notfound'])

        # Create cases
        if invalids:
            self.create_case_invalid(cursor, uid, invalids,
                                     context=context)
        if notfound:
            self.create_case_notfound(cursor, uid, notfound, meter_name,
                                      context=context)
        return True

    def validate_billing(self, cursor, uid, ids, meter_names=None,
                         context=None):
        '''This function has to validate the billing
        information that comes from telemeasured meters
        '''
        if not context:
            context = {}
        context.update({'validate': True})
        if 'lang' not in context:
            user_obj = self.pool.get('res.users')
            lang = user_obj.read(cursor, uid, uid, ['context_lang'])[
                'context_lang']
            if lang:
                context.update({'lang': lang})
        if not meter_names:
            meter_names = get_all_meter_names('tm_billing')

        jobs_ids = []

        for meter_name in meter_names:
            j = self.validate_individual(cursor, uid, ids, meter_name,
                                         context=context)
            jobs_ids.append(j.id)

        if len(jobs_ids) > 1:
            create_jobs_group(
                cursor.dbname, uid, _('Validate billing'),
                'tm.validate.billing', jobs_ids
            )

        # if context.get('tg_check_unreaded', True):
        #     self.meters_without_read(cursor, uid, context)
        return True

TmLecturesValidate()


class TmProfilesValidate(osv.osv):

    _name = 'tm.profiles.validate'
    _auto = False

    def create_case_invalid(self, cursor, uid, invalids, context=None):
        '''Create case with invalid measures'''
        case_obj = self.pool.get('crm.case')
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        profile_obj = self.pool.get('tm.profile')
        config_obj = self.pool.get('res.config')
        profile_mdb = mdbpool.get_collection('tm_profile')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': 'giscedata.polissa'})

        #Order invalids by timestamp first
        search_params = [('id', 'in', invalids.keys())]
        ordered_profile_ids = profile_obj.search(cursor, uid, search_params,
                                                 order='timestamp')

        today = datetime.now().strftime('%Y-%m-%d')
        date_check_from = config_obj.get(cursor, uid,
                                         'tm_profile_check_from', today)

        meters = {}
        # Search meter_id for each invalid and group then
        for profile_id in ordered_profile_ids:
            profile = profile_mdb.find_one({'id': profile_id},
                                           {'timestamp': 1, 'name': 1})
            date_meter = get_format_date(profile['timestamp'])
            #Do not notify this ones in cases. They are too old.
            if date_meter < date_check_from:
                continue
            meter_id = meter_obj.get_meter_on_date(cursor, uid, profile['name'],
                                                   date_meter)
            meters.setdefault(meter_id, [])
            meters[meter_id].append(invalids[profile_id])

        for meter_id in meters:
            #Do not open another case if already one in open or pending state
            #This way we let automatic checks to be closed manually when done
            #and do not create many cases with the same incidences
            if meter_obj.check_case_open(cursor, uid, meter_id, section='TM',
                                         category='TMPR', context=context):
                continue
            meter = meter_obj.browse(cursor, uid, meter_id)
            case_message = '\n'.join(meters[meter_id])
            case_vals = {
                'description': case_message,
                'ref2': 'giscedata.lectures.comptador,%i' % meter.id,
                'name': (_('TM Profile checks for %s') % meter.name),
                'polissa_id': meter.polissa.id,
            }
            case_id = case_obj.create_case_generic(cursor, uid,
                                                   [meter.polissa.id],
                                                   context=ctx,
                                                   section='TM',
                                                   category='TMPR',
                                                   extra_vals=case_vals)
            case_obj.case_open(cursor, uid, case_id)
        return True

    def create_case_notfound(self, cursor, uid, notfound, meter_name,
                             context=None):
        '''Create case with notfound measures'''
        case_obj = self.pool.get('crm.case')
        config_obj = self.pool.get('res.config')

        if not context:
            context = {}
        ctx = context.copy()
        ctx.update({'model': False})

        today = datetime.now().strftime('%Y-%m-%d')
        date_check_from = config_obj.get(cursor, uid,
                                         'tm_profile_check_from', today)
        date_check_from = datetime.strptime(date_check_from,
                                            '%Y-%m-%d').date()
        msg = ''
        only_days = [date.date() for date in notfound.itervalues()
                     if date.date() >= date_check_from]
        #If we have no days to show, do not create any case
        if not only_days:
            return True
        sorted_days = sorted(list(set(only_days)))
        _not_found_msg = _('Meter %s not found on %s\n')
        for timestamp in sorted_days:
            date = get_format_date(timestamp, hour=False, default_format=False)
            msg += _not_found_msg % (meter_name, date)

        case_name = _('TM Profile out of range for {}').format(meter_name)

        return case_obj.append_description_to_case_by_name(
            cursor,
            uid,
            None,
            case_name,
            msg,
            'TM',
            'TMPR',
            context=context
        )

    def check_control_bits(self, cursor, uid, meter_name, context=None):
        """ Check control bit """

        control_messages = {
            7: _(u"Bit 7. Invalid measure"),
            6: _(u"Bit 6. Synchronized meter during period"),
            5: _(u"Bit 5. Overflow"),
            4: _(u"Bit 4. Time verification during period"),
            3: _(u"Bit 3. Modified parameters during period"),
            2: _(u"Bit 2. Intrusion detected"),
            1: _(u"Bit 1. Incomplete period due to power failure"),
            0: ''
        }

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_profile = mdbpool.get_collection('tm_profile')
        conf_obj = self.pool.get('res.config')

        bc_exclude = eval(conf_obj.get(cursor, uid,
                                       'tm_profile_bc_exclude', '[]')
                          )

        result = {'badids': {}, 'notfound': {}}
        bad_bcs = tm_profile.find(
            {'name': meter_name, 'valid': False, 'type': 'p',
             '$or': [
                 {'quality_ai': {'$ne': 0}},
                 {'quality_ae': {'$ne': 0}},
                 {'quality_r1': {'$ne': 0}},
                 {'quality_r2': {'$ne': 0}},
                 {'quality_r3': {'$ne': 0}},
                 {'quality_r4': {'$ne': 0}},
             ],
             },
            {'_id': 0, 'id': 1, 'quality_ai': 1, 'quality_ae': 1,
             'quality_r1': 1, 'quality_r2': 1, 'quality_r3': 1, 'quality_r4': 1,
             'timestamp': 1})

        for measure in bad_bcs:
            date_meter = get_format_date(measure['timestamp'])
            meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                                   date_meter)
            if not meter_id:
                result['notfound'][measure['id']] = measure['timestamp']
                continue
            # bit 7 = 1 is an invalid measure
            # bit 1 or 2 or 3 or 5 or 6 = 1 can be an invalid measure
            # Otherwise we can consider it valid
            # We reverse the result.
            # Otherwise bit 7 corresponds to 0 position in list
            qualities = ['quality_ai', 'quality_r2', 'quality_r3', 'quality_r1',
                         'quality_r4', 'quality_ae']
            quality_error = ''
            for quality in qualities:
                if measure[quality] and measure[quality] != 0:
                    quality_error = quality
                    break

            if not quality_error:
                continue
            # Check if bc has been excluded from checking
            if measure[quality_error] in bc_exclude:
                continue

            control_bits = bin(int(measure[quality_error]))[2:].zfill(8)[::-1]
            messages = []
            invalid_msg = ''
            for bit, control_message in control_messages.items():
                if control_bits[bit] == '1' and bit == 7:
                    messages.append(control_message)
                    invalid_msg = _(u"Invalid")
                elif control_bits[bit] == '1' and bit in (1, 2, 3, 5, 6):
                    messages.append(control_message)
                    invalid_msg = _(u"Possible invalid")
            if messages:
                date = get_format_date(measure['timestamp'], hour=True,
                                       default_format=False)
                msg = _("{} measure {} CB: {} ({})\n{}").format(invalid_msg,
                                       date, measure['quality_ai'],
                                       control_bits[::-1], '\n'.join(messages))
                result['badids'][measure['id']] = msg
        return result

    def check_same_time_many_measures(self, cursor, uid, meter_name,
                                      context=None):
        """ Look for measures that has same timestamp and meter but different
        " mesures
        """
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_profile = mdbpool.get_collection('tm_profile')

        result = {'badids': {}, 'notfound': {}}
        # We also group by season, the day with hour change we must
        # two equal measures at the same hour but with different season value
        measure_dups = tm_profile.aggregate([
            {'$match': {'name': meter_name,
                        'type': 'p'}},
            {'$group': {'_id': {'name': '$name',
                                'timestamp': '$timestamp',
                                'season': '$season'
                                },
                        'diff': {'$addToSet': '$ai'},
                        'ids': {'$addToSet': '$id'},
                        'count': {'$sum': 1}
                        }
             },
            {'$match': {'count': {'$gt': 1}}},
        ])['result']

        for dup in measure_dups:
            timestamp = dup['_id']['timestamp']
            date_meter = get_format_date(timestamp)
            meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name, date_meter)
            if not meter_id:
                for id in dup['ids']:
                    result['notfound'][id] = timestamp
                continue
            date_msg = get_format_date(timestamp, hour=True,
                                       default_format=False)
            # Set the error message only in the first duplicated measure, but
            # all of them should be in the invalid list
            measure_list = ','.join([str(x) for x in dup['diff']])
            for measure_id in dup['ids']:
                msg = ''
                if dup['ids'].index(measure_id) == 0:
                    msg = _("%s different reads found on %s. AI: %s")
                    msg = msg % (len(dup['ids']), date_msg, measure_list)
                result['badids'][measure_id] = msg
        return result

    def single_check_more_than_pot(self, cursor, uid, measure, polissa,
                                   context=None):
        """
        Check if measure is higher than the consumption allowed by the policy \
            with tolerance. The tolerance is an absolute percentage and if is \
            0 this check is disabled. The tolerance is stored in \
            tg_profile_power_tolerance configuration variable. E.g. if power \
            is 10kWh and tolerance 120% then 12kWh are allowed. The default \
            tolerance value is 200.

        :param cursor: database cursor
        :param uid: user identifier
        :param measure: the measure as tg.profile
        :param polissa: the policy as osv.orm.browse_record object
        :param context: optional context
        :return: dict with the measure id in the key and the error \
            description in the value
        """
        result = {}

        res_config = self.pool.get('res.config')
        tm_profile_power_tolerance = float(res_config.get(cursor, uid,
                                           'tm_profile_power_tolerance', '200')
                                           )

        if tm_profile_power_tolerance == 0:
            return result

        #Ensure we are getting the right day. When 0 hour
        #it is 24 hour from previous day
        timestamp = measure['timestamp']
        hour = timestamp.hour
        if timestamp.hour == 0:
            timestamp -= timedelta(days=1)
            hour = 24
        day = get_format_date(timestamp)
        #We use the same function as profiles module for getting
        #the period, so hour - 1 as done when profiling
        hour = '%02i:00:00' % (hour - 1)
        season = '0' if measure['season'] == 'W' else '1'
        if polissa.tarifa.get_num_periodes('tp') < 3:
            pot = polissa.potencia
        else:
            periode = polissa.tarifa.get_periode_ts(day, hour, season)
            # Converts grouped periods
            grouped_periods = polissa.tarifa.get_grouped_periods()
            periode = grouped_periods.get(periode, periode)
            for periode_pot in polissa.potencies_periode:
                if periode_pot.periode_id.name == periode:
                    pot = periode_pot.potencia
                    break
        power_consumed = measure['ai'] * measure['magn']
        # power_hourly_limit = pot * tg_profile_power_tolerance * 10
        power_hourly_limit = pot * 1000 * (tm_profile_power_tolerance / 100)
        if power_consumed > power_hourly_limit:
            date = get_format_date(measure['timestamp'], hour=True,
                                   default_format=False)
            msg = _("Impossible consumption at {} ai: {}, Power: {}").format(
                    date, power_consumed, pot)
            result[measure['id']] = msg

        return result

    def single_check_high_reactive(self, cursor, uid, measure, polissa,
                                   context=None):
        """ Individual check if more reactive than 75% of active """
        conf_obj = self.pool.get('res.config')
        result = {}
        if not int(conf_obj.get(cursor, uid, 'tm_reactive_check', '1')):
            return result
        if measure['r1'] > measure['ai'] * 0.75:
            date = get_format_date(measure['timestamp'], hour=True,
                                   default_format=False)
            msg = _("High reactive at {} ai: {}, r1: {}").format(date,
                                                measure['ai'] * measure['magn'],
                                                measure['r1'] * measure['magn'])
            result[measure['id']] = msg
        return result

    def check_impossible_measure(self, cursor, uid, meter_name, context=None):
        """ Check if the consumptions has negative values (ai or r1) """

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_profile = mdbpool.get_collection('tm_profile')
        conf_obj = self.pool.get('res.config')

        impossible = int(conf_obj.get(cursor, uid,
                                      'tm_profile_impossible',
                                      '999999999'
                                      ))

        result = {'badids': {}, 'notfound': {}}
        impossibles = tm_profile.find(
            {'name': meter_name, 'valid': False, 'type': 'p',
             '$or': [{'ai': {'$gt': impossible}},
                     {'ae': {'$gt': impossible}},
                     {'r1': {'$gt': impossible}},
                     {'r2': {'$gt': impossible}},
                     {'r3': {'$gt': impossible}},
                     {'r4': {'$gt': impossible}},
                     ],
             },
            {'_id': 0}
            )
        for impossible in impossibles:
            date_meter = get_format_date(impossible['timestamp'])
            meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                                   date_meter)
            if not meter_id:
                result['notfound'][impossible['id']] = impossible['timestamp']
                continue
            date = get_format_date(impossible['timestamp'], hour=True,
                                   default_format=False)
            msg = _(u"Impossible measure at {} ai: {ai}, ae: {ae},"
                    u"r1: {r1}, r2: {r2}, r3: {r3}, r4: {r4}").format(date,
                                                                   **impossible)
            result['badids'][impossible['id']] = msg
        return result

    def check_negative_read(self, cursor, uid, meter_name, context=None):
        """ Check if the consumptions has negative values (ai or r1) """

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        tm_profile = mdbpool.get_collection('tm_profile')

        result = {'badids': {}, 'notfound': {}}
        negatives = tm_profile.find(
            {'name': meter_name, 'valid': False, 'type': 'p',
             '$or': [{'ai': {'$lt': 0}},
                     {'ae': {'$lt': 0}},
                     {'r1': {'$lt': 0}},
                     {'r2': {'$lt': 0}},
                     {'r3': {'$lt': 0}},
                     {'r4': {'$lt': 0}},
                     ],
             },
            {'_id': 0, 'ai': 1, 'ae': 1, 'r1': 1, 'r2': 1, 'r3': 1, 'r4': 1,
             'id': 1, 'magn': 1, 'timestamp': 1}
            )
        for negative in negatives:
            date_meter = get_format_date(negative['timestamp'])
            meter_id = meter_obj.get_meter_on_date(cursor, uid,
                                           meter_name, date_meter)
            if not meter_id:
                result['notfound'][negative['id']] = negative['timestamp']
                continue
            date = get_format_date(negative['timestamp'], hour=True,
                                   default_format=False)
            msg = _("Meter has negative measure at {} ai: {ai}, ae: {ae}, "
                    "r1: {r1}, r2: {r2}, r3: {r3}, r4: {r4}").format(date,
                                                                     **negative)
            result['badids'][negative['id']] = msg
        return result

    def get_duplicates(self, cursor, uid, meter_name):
        """
        Get all duplicated billings of the meter with name = meter_name
        :param cursor:
        :param uid:
        :param meter_name: The name of the target meter
        :return: List of dictionaries with all fields on return_keys for all
        the duplicated ids
        """

        tm_profile_obj = self.pool.get('tm.profile')

        read_keys = ['name', 'timestamp', 'ai', 'season', 'valid', 'type']
        keywords = ['name', 'timestamp', 'ai', 'season', 'type']
        return_keys = ['id', 'timestamp', 'ai', 'valid']
        profiles = tm_profile_obj.search(cursor, uid, [
            ('name', '=', meter_name)])
        read_profiles = tm_profile_obj.read(cursor, uid, profiles, read_keys)

        df = pd.DataFrame(read_profiles)
        df.sort_values(read_keys, ascending=False, inplace=True)
        df_duplicated = df.duplicated(subset=keywords)

        return df[df_duplicated.values][return_keys].T.to_dict().values()

    def clean_duplicates(self, cursor, uid, meter_name):
        """ From one meter_name check all the measures deleting duplicates
        " if there is some duplicate that has been validated is the one that
        " remains
        """
        tm_profile_obj = mdbpool.get_collection('tm_profile')
        meter_dups = self.get_duplicates(cursor, uid, meter_name)

        for dup in meter_dups:
            # Look for all the duplicated measures matching criteria

            tm_profile_obj.remove({'id': dup['id']})

    def update_last_valid_profile(self, cursor, uid, meter_names,
                                  context=None):

        meter_obj = self.pool.get('giscedata.lectures.comptador')
        profile_obj = self.pool.get('tm.profile')

        if not context:
            context = {}
        context.update({'sync': False})

        if not isinstance(meter_names, (list, tuple)):
            meter_names = [meter_names]

        for meter_name in meter_names:
            search_params = [('name', '=', meter_name),
                             ('valid', '=', True),
                             ('type', '=', 'p')]
            profile_id = profile_obj.search(cursor, uid, search_params,
                                            limit=1, order='timestamp desc')
            if not profile_id:
                continue
            date = profile_obj.read(cursor, uid, profile_id,
                                    ['timestamp'])[0]['timestamp']
            meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                                   date[:10], context=context)
            if not meter_id:
                continue
            meter_obj.write(cursor, uid, meter_id,
                            {'tm_last_profile': date},
                            context=context)
        return True

    def has_pending_validation(self, cursor, uid, meter_name, context=None):
        '''check if we have at least one invalid
        read associated to meter_name'''

        tm_profile_mdb = mdbpool.get_collection('tm_profile')
        profile = tm_profile_mdb.find_one({'name': meter_name,
                                           'valid': False})
        return profile and True or False

    _block_invalid_check_funcs = ['check_same_time_many_measures',
                                  'check_impossible_measure',
                                  'check_negative_read',
                                  'check_control_bits',
                                  ]
    _single_check_funcs = [('single_check_high_reactive', False),
                           ('single_check_more_than_pot', True)]

    @job(queue='tm_validate', timeout=1200, result_ttl=24*3600)
    def validate_individual(self, cursor, uid, ids, meter_name, context=None):
        '''This function has to validate the profile
        information that comes from smart meters
        '''
        meter_obj = self.pool.get('giscedata.lectures.comptador')
        polissa_obj = self.pool.get('giscedata.polissa')
        tm_profile_obj = self.pool.get('tm.profile')
        conf_obj = self.pool.get('res.config')

        tm_profile_mdb = mdbpool.get_collection('tm_profile')
        logger = logging.getLogger('openerp.{0}.validate_tm_profiles'.format(
            __name__))

        #check if the meter has invalid profiles. If not, do not continue
        if not self.has_pending_validation(cursor, uid, meter_name):
            return True
        logger.info("Validation of profiles for the {} meter".format(
            meter_name))
        if not context:
            context = {}

        invalids = {}
        notfound = {}
        validated = []
        self.clean_duplicates(cursor, uid, meter_name)

        # Block checks
        for func_name in self._block_invalid_check_funcs:
            logger.info(" * {}".format(func_name))
            func = getattr(self, func_name)
            res = func(cursor, uid, meter_name, context=context)
            invalids = update_invalid(invalids, res['badids'])
            notfound.update(res['notfound'])

        # After block checks search for reads not included in invalids
        invalid_ids = list(set(invalids.keys() + notfound.keys()))
        to_be_validated = tm_profile_mdb.find({
                            'name': meter_name,
                            'valid': False,
                            'type': 'p',
                            'id': {'$nin': invalid_ids}
        })
        # Individual measure checks
        check_individual = int(conf_obj.get(cursor, uid,
                                            'tm_profile_check_individual', '0'))
        if check_individual:
            logger.info(" * check_individual measures")
            for measure in tqdm(to_be_validated):
                timestamp = measure['timestamp']
                date_meter = get_format_date(timestamp)
                meter_id = meter_obj.get_meter_on_date(cursor, uid, meter_name,
                                                       date_meter)
                if not meter_id:
                    notfound[measure['id']] = timestamp
                    continue

                meter = meter_obj.browse(cursor, uid, meter_id)
                polissa = polissa_obj.browse(cursor, uid, meter.polissa.id,
                                             context={'date': date_meter})
                invalid = False
                for func_name, mark_invalid in self._single_check_funcs:
                    func = getattr(self, func_name)
                    res = func(cursor, uid, measure, polissa, context=context)
                    if res:
                        invalids = update_invalid(invalids, res)
                        #Mark as invalid only if not invalid before
                        if mark_invalid and not invalid:
                            invalid = mark_invalid

                if not invalid:
                    # If we arrive here the measure is valid,
                    # no inserts during the process
                    validated.append(measure['id'])
                    tm_profile_obj.validate(cursor, uid, [measure['id']])
        else:
            validated = [x['id'] for x in to_be_validated]
            tm_profile_obj.validate(cursor, uid, validated)

        self.update_last_valid_profile(cursor, uid, meter_name,
                                       context=context)

        if validated:
            logger.info(" * Validate p4 measures: {}".format(len(validated)))
            validated_hours = tm_profile_obj.read(
                cursor, uid, validated, ['name', 'timestamp', 'ai']
            )
            for hour in tqdm(validated_hours):
                dt = datetime.strptime(hour['timestamp'], '%Y-%m-%d %H:%M:%S')
                df = (dt - timedelta(hours=1)).strftime('%Y-%m-%d %H:%M:%S')
                quarters = tm_profile_obj.search(cursor, uid, [
                    ('name', '=', hour['name']), ('type', '=', 'p4'),
                    ('timestamp', '<=', hour['timestamp']),
                    ('timestamp', '>', df)])
                if quarters and len(quarters) == 4:
                    quarters_r = tm_profile_obj.read(cursor, uid, quarters, [
                        'name', 'timestamp', 'ai'])
                    ai_sum = 0
                    for quarter in quarters_r:
                        ai_sum += quarter.get('ai', 0)
                    if ai_sum == hour['ai']:
                        tm_profile_obj.validate(cursor, uid, quarters)

        create_cases = int(conf_obj.get(cursor, uid,
                                        'tm_create_profile_cases', '1'))
        if create_cases:
            if invalids:
                logger.info(" * Create case invalids: {} items".format(len(invalids)))
                self.create_case_invalid(cursor, uid, invalids,
                                         context=context)
            if notfound:
                logger.info(" * Create case not found: {} items".format(len(notfound)))
                self.create_case_notfound(cursor, uid, notfound, meter_name,
                                          context=context)
        return True

    def validate_profile(self, cursor, uid, ids, meter_names=[],
                         context=None):
        '''This function has to validate the profile
        information that comes from smart meters
        '''

        if not meter_names:
            meter_names = get_all_meter_names('tm_profile')

        jobs_ids = []
        for meter_name in meter_names:
            j = self.validate_individual(cursor, uid, ids, meter_name,
                                         context=context)
            jobs_ids.append(j.id)

        if len(jobs_ids) > 1:
            create_jobs_group(
                cursor.dbname, uid, _('Validate profile'),
                'tm.validate.profile', jobs_ids
            )
        return True

TmProfilesValidate()
